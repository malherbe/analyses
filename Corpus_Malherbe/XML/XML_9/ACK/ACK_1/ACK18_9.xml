<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PREMIÈRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>588 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Diverses</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/louiseackermaNpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1862">1862</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK18" modus="cm" lm_max="10" metProfile="4+6" form="suite de strophes" schema="3[abab]" er_moy="1.0" er_max="2" er_min="0" er_mode="0(3/6)" er_moy_et="1.0" qr_moy="0.83" qr_max="N5" qr_mode="0(5/6)" qr_moy_et="1.86">
				<head type="number">XVIII</head>
				<head type="main">Hébé</head>
				<lg n="1" type="regexp" rhyme="ababab">
					<l n="1" num="1.1" lm="10" met="4+6"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="1.2">y<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="1.3" punct="vg:4">b<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3" mp="M">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409" place="4" punct="vg" caesura="1">é</seg>s</w>,<caesura></caesura> <w n="1.4">r<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="6" mp="M">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="1.6" punct="vg:10">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg>d<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="a" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="2" num="1.2" lm="10" met="4+6"><w n="2.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64" place="1" mp="P">e</seg>rs</w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407" place="2" mp="C">eu</seg>r</w> <w n="2.3">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="3" mp="M">an</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190" place="4" caesura="1">e</seg>t</w><caesura></caesura> <w n="2.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="2.5">H<seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="7">é</seg></w> <w n="2.6">s</w>’<w n="2.7" punct="vg:10"><seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="M">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="9" mp="M">an</seg><pgtc id="2" weight="2" schema="CR">ç<rhyme label="b" id="2" gender="m" type="a" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="vg">ai</seg>t</rhyme></pgtc></w>,</l>
					<l n="3" num="1.3" lm="10" met="4+6"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161" place="1" mp="C">e</seg>s</w> <w n="3.2">Di<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="3.3">ch<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409" place="4" caesura="1">é</seg>s</w><caesura></caesura> <w n="3.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="306" place="6">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.5">l<seg phoneme="œ" type="vs" value="1" rule="407" place="7" mp="C">eu</seg>r</w> <w n="3.6">c<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="3.7" punct="vg:10">v<pgtc id="1" weight="0" schema="R"><rhyme label="a" id="1" gender="f" type="e" stanza="1" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="4" num="1.4" lm="10" met="4+6"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189" place="1">E</seg>t</w> <w n="4.2">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="Pem">e</seg></w> <w n="4.3">n<seg phoneme="ɛ" type="vs" value="1" rule="358" place="3" mp="M">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="340" place="4" caesura="1">a</seg>r</w><caesura></caesura> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="5" mp="M">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="6">an</seg>t</w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="4.7" punct="pt:10">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="8" mp="M">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="2" weight="2" schema="CR">ss<rhyme label="b" id="2" gender="m" type="e" stanza="1" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="10" punct="pt">ai</seg>t</rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="10" met="4+6"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425" place="2">ou</seg>s</w> <w n="5.3" punct="vg:4"><seg phoneme="o" type="vs" value="1" rule="318" place="3" mp="M">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468" place="4" punct="vg" caesura="1">i</seg></w>,<caesura></caesura> <w n="5.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5">an</seg>d</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="5.7" punct="vg:10">J<seg phoneme="œ" type="vs" value="1" rule="407" place="9" mp="M">eu</seg>n<pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="a" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="10" met="4+6"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="6.2">lu<seg phoneme="i" type="vs" value="1" rule="491" place="2" mp="C">i</seg></w> <w n="6.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" caesura="1">on</seg>s</w><caesura></caesura> <w n="6.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439" place="5">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24" place="6" mp="Fc">e</seg></w> <w n="6.5">c<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342" place="8" mp="P">à</seg></w> <w n="6.7">l</w>’<w n="6.8" punct="pt:10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="9" mp="M">en</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="b" id="4" gender="m" type="a" stanza="2" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="regexp" rhyme="ababab">
					<l n="7" num="2.1" lm="10" met="4+6"><w n="7.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199" place="2">e</seg>st</w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="7.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="4" caesura="1">in</seg></w><caesura></caesura> <w n="7.5">qu</w>’<w n="7.6"><seg phoneme="i" type="vs" value="1" rule="497" place="5">y</seg></w> <w n="7.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24" place="7" mp="F">e</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="7.9" punct="pi:10">d<seg phoneme="e" type="vs" value="1" rule="409" place="9" mp="M">é</seg><pgtc id="3" weight="0" schema="R"><rhyme label="a" id="3" gender="f" type="e" stanza="2" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="352" place="10">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
					<l n="8" num="2.2" lm="10" met="4+6"><w n="8.1">N<seg phoneme="u" type="vs" value="1" rule="425" place="1" mp="C">ou</seg>s</w> <w n="8.2">l</w>’<w n="8.3" punct="pv:4"><seg phoneme="i" type="vs" value="1" rule="468" place="2" mp="M">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444" place="3" mp="M">o</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" punct="pv" caesura="1">on</seg>s</w> ;<caesura></caesura> <w n="8.4"><seg phoneme="i" type="vs" value="1" rule="468" place="5" mp="C">i</seg>l</w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="360" place="6" mp="M">en</seg><seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189" place="8">e</seg>t</w> <w n="8.7" punct="pt:10">r<seg phoneme="a" type="vs" value="1" rule="340" place="9" mp="M">a</seg><pgtc id="4" weight="2" schema="CR">v<rhyme label="b" id="4" gender="m" type="e" stanza="2" qr="N5"><seg phoneme="i" type="vs" value="1" rule="468" place="10" punct="pt">i</seg>t</rhyme></pgtc></w>.</l>
					<l n="9" num="2.3" lm="10" met="4+6"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="339" place="1" mp="M">A</seg>y<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>t</w> <w n="9.2">s<seg phoneme="u" type="vs" value="1" rule="425" place="3" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="4" caesura="1">i</seg></w><caesura></caesura> <w n="9.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="5" mp="P">an</seg>s</w> <w n="9.4">s<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="9.5">gr<seg phoneme="a" type="vs" value="1" rule="340" place="7">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.6" punct="vg:10"><seg phoneme="i" type="vs" value="1" rule="467" place="8" mp="M">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439" place="9" mp="M">o</seg>rt<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="a" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="10" num="2.4" lm="10" met="4+6"><w n="10.1">H<seg phoneme="e" type="vs" value="1" rule="409" place="1" mp="M">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409" place="2">é</seg></w> <w n="10.2">s</w>’<w n="10.3" punct="pv:4"><seg phoneme="e" type="vs" value="1" rule="409" place="3" mp="M">é</seg>l<seg phoneme="wa" type="vs" value="1" rule="420" place="4" punct="pv" caesura="1">oi</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ;<caesura></caesura> <w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="10.6">r<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358" place="8">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="9">en</seg></w> <w n="10.8" punct="pt:10"><pgtc id="6" weight="2" schema="[CR">v<rhyme label="b" id="6" gender="m" type="a" stanza="3" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="303" place="10" punct="pt">ain</seg></rhyme></pgtc></w>.</l>
					<l n="11" num="2.5" lm="10" met="4+6"><w n="11.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="1" mp="M">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2">em</seg>ps</w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443" place="4" caesura="1">o</seg>r</w><caesura></caesura> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="P">u</seg>r</w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340" place="6" mp="C">a</seg></w> <w n="11.5">r<seg phoneme="u" type="vs" value="1" rule="425" place="7">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6" punct="vg:10"><seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>rn<pgtc id="5" weight="0" schema="R"><rhyme label="a" id="5" gender="f" type="e" stanza="3" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="10">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5" place="11" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="2.6" lm="10" met="4+6"><w n="12.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439" place="1" mp="C">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="œ" type="vs" value="1" rule="286" place="2">œ</seg>il</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174" place="3">en</seg></w> <w n="12.4">pl<seg phoneme="œ" type="vs" value="1" rule="407" place="4" caesura="1">eu</seg>rs</w><caesura></caesura> <w n="12.5">su<seg phoneme="i" type="vs" value="1" rule="491" place="5">i</seg>t</w> <w n="12.6">l</w>’<w n="12.7"><seg phoneme="e" type="vs" value="1" rule="409" place="6" mp="M">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="7" mp="M">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8">on</seg></w> <w n="12.8" punct="pt:10">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg><pgtc id="6" weight="2" schema="CR">v<rhyme label="b" id="6" gender="m" type="e" stanza="3" qr="C0"><seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="10" punct="pt">in</seg></rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>