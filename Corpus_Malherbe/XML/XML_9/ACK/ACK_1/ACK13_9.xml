<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PREMIÈRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>588 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Diverses</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/louiseackermaNpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1862">1862</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK13" modus="cp" lm_max="12" metProfile="8, 6+6" form="suite périodique" schema="3(aabccb)" er_moy="0.67" er_max="2" er_min="0" er_mode="0(6/9)" er_moy_et="0.94" qr_moy="0.0" qr_max="C0" qr_mode="0(9/9)" qr_moy_et="0.0">
				<head type="number">XIII</head>
				<head type="main">Deux vers D’Alcée</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Ίόπλοχ´ άγνά µειλιχόµειδε Σαπφοί, <lb></lb>
								θέλω τι Fείπñν, άλλά µε xωλύει αίδώς.
							</quote>
							<bibl>
								<name>Alcée</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1" type="sizain" rhyme="aabccb">
					<l n="1" num="1.1" lm="12" met="6+6"><w n="1.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346" place="1">e</seg>l</w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409" place="2" mp="M">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="1.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="1.4">d<seg phoneme="e" type="vs" value="1" rule="409" place="5" mp="M">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468" place="6" caesura="1">i</seg>r</w><caesura></caesura> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="1.6">t<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="1.7">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="9">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24" place="10" mp="F">e</seg></w> <w n="1.8" punct="pi:12">s<seg phoneme="ə" type="em" value="1" rule="e-19" place="11" mp="Mem">e</seg><pgtc id="1" weight="2" schema="CR">cr<rhyme label="a" id="1" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pi" mp="F">e</seg></rhyme></pgtc></w> ?</l>
					<l n="2" num="1.2" lm="12" met="6+6"><w n="2.1" punct="pe:1">Qu<seg phoneme="wa" type="vs" value="1" rule="281" place="1" punct="pe">oi</seg></w> ! <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="2.3">v<seg phoneme="ø" type="vs" value="1" rule="248" place="3">œu</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="2.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="5" mp="C">on</seg></w> <w n="2.6" punct="vg:6">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" punct="vg" caesura="1">œu</seg>r</w>,<caesura></caesura> <w n="2.7">t<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="C">a</seg></w> <w n="2.8">M<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24" place="9" mp="F">e</seg></w> <w n="2.9">tr<seg phoneme="o" type="vs" value="1" rule="433" place="10">o</seg>p</w> <w n="2.10">d<seg phoneme="i" type="vs" value="1" rule="468" place="11" mp="M">i</seg>s<pgtc id="1" weight="2" schema="CR">cr<rhyme label="a" id="1" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="410" place="12">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="3" num="1.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="3.1">R<seg phoneme="u" type="vs" value="1" rule="425" place="1">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>t</w>-<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="3">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="4">e</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="5">e</seg></w> <w n="3.4">l</w>’<w n="3.5" punct="pi:8"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="6">e</seg>xpr<seg phoneme="i" type="vs" value="1" rule="467" place="7">i</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="a" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pi">er</seg></rhyme></pgtc></w> ?</l>
					<l n="4" num="1.4" lm="12" met="6+6"><w n="4.1" punct="vg:2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">A</seg>lc<seg phoneme="e" type="vs" value="1" rule="409" place="2" punct="vg">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="3" mp="C">on</seg></w> <w n="4.3">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="4" mp="Mem">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435" place="5" mp="M">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6" caesura="1">aî</seg>t</w><caesura></caesura> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="8">ou</seg>r</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342" place="9" mp="P">à</seg></w> <w n="4.7">c<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="4.8" punct="pt:12">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="11" mp="M">an</seg>g<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pt" mp="F">e</seg></rhyme></pgtc></w>.</l>
					<l n="5" num="1.5" lm="12" met="6+6"><w n="5.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="M">a</seg>ph<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg></w> <w n="5.2">f<seg phoneme="ɛ̃" type="vs" value="1" rule="386" place="3">ein</seg>t</w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="305" place="4" mp="M">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19" place="5" mp="Mem">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368" place="6" caesura="1">en</seg>t</w><caesura></caesura> <w n="5.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="7">e</seg></w> <w n="5.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="5.6">d<seg phoneme="i" type="vs" value="1" rule="468" place="9" mp="M">i</seg>sc<seg phoneme="u" type="vs" value="1" rule="425" place="10">ou</seg>rs</w> <w n="5.7">l</w>’<w n="5.8" punct="vg:12"><seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>tr<pgtc id="3" weight="0" schema="R"><rhyme label="c" id="3" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="340" place="12">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="6" num="1.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg>ph<seg phoneme="o" type="vs" value="1" rule="444" place="2">o</seg></w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308" place="3">ai</seg>t</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="4">e</seg></w> <w n="6.4">t<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg></w> <w n="6.5">v<seg phoneme="a" type="vs" value="1" rule="340" place="6">a</seg>s</w> <w n="6.6">l</w>’<w n="6.7" punct="pt:8"><seg phoneme="ɛ" type="vs" value="1" rule="305" place="7">ai</seg><pgtc id="2" weight="2" schema="CR">m<rhyme label="b" id="2" gender="m" type="e" qr="C0"><seg phoneme="e" type="vs" value="1" rule="347" place="8" punct="pt">er</seg></rhyme></pgtc></w>.</l>
				</lg>
				<lg n="2" type="sizain" rhyme="aabccb">
					<l n="7" num="2.1" lm="12" met="6+6"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450" place="1" mp="C">u</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="2" mp="M">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="3" mp="M">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308" place="4">ai</seg>s</w> <w n="7.4" punct="vg:6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="5" mp="M">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347" place="6" punct="vg" caesura="1">er</seg></w>,<caesura></caesura> <w n="7.5">t<seg phoneme="y" type="vs" value="1" rule="450" place="7" mp="C">u</seg></w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340" place="8" mp="C">a</seg></w> <w n="7.7">v<seg phoneme="wa" type="vs" value="1" rule="440" place="9" mp="M">o</seg>y<seg phoneme="ɛ" type="vs" value="1" rule="308" place="10">ai</seg>s</w> <w n="7.8" punct="vg:12">s<seg phoneme="u" type="vs" value="1" rule="425" place="11" mp="M">ou</seg>r<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="12">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="8" num="2.2" lm="12" met="6+6"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="8.2">f<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12" place="4" mp="Pem">e</seg></w> <w n="8.4" punct="vg:6">L<seg phoneme="ɛ" type="vs" value="1" rule="358" place="5" mp="M">e</seg>sb<seg phoneme="o" type="vs" value="1" rule="438" place="6" punct="vg" caesura="1">o</seg>s</w>,<caesura></caesura> <w n="8.5">S<seg phoneme="a" type="vs" value="1" rule="340" place="7" mp="M">a</seg>ph<seg phoneme="o" type="vs" value="1" rule="444" place="8">o</seg></w> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="8.7">s<seg phoneme="y" type="vs" value="1" rule="450" place="10" mp="P">u</seg>r</w> <w n="8.8">s<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="C">a</seg></w> <w n="8.9">l<pgtc id="4" weight="0" schema="R"><rhyme label="a" id="4" gender="f" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="493" place="12">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="9" num="2.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="9.1">R<seg phoneme="e" type="vs" value="1" rule="409" place="1">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="2">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="3">i</seg>t</w> <w n="9.2">s<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg></w> <w n="9.3">gr<seg phoneme="a" type="vs" value="1" rule="340" place="5">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="9.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="9.6" punct="pt:8">f<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="a" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
					<l n="10" num="2.4" lm="12" met="6+6"><w n="10.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="C">a</seg></w> <w n="10.2">v<seg phoneme="wa" type="vs" value="1" rule="420" place="2">oi</seg>x</w> <w n="10.3">t<seg phoneme="ə" type="em" value="1" rule="e-12" place="3" mp="C">e</seg></w> <w n="10.4" punct="vg:4">tr<seg phoneme="u" type="vs" value="1" rule="425" place="4" punct="vg">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.5" punct="vg:6"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">A</seg>lc<seg phoneme="e" type="vs" value="1" rule="409" place="6" punct="vg" caesura="1">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>,<caesura></caesura> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189" place="7">e</seg>t</w> <w n="10.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="8" mp="C">on</seg></w> <w n="10.8">r<seg phoneme="ə" type="em" value="1" rule="e-19" place="9" mp="Mem">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340" place="10">a</seg>rd</w> <w n="10.9">t</w>’<w n="10.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>fl<pgtc id="6" weight="0" schema="R"><rhyme label="c" id="6" gender="f" type="a" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="12">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="11" num="2.5" lm="12" met="6+6"><w n="11.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313" place="1" mp="M">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468" place="2">i</seg>s</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="3">e</seg></w> <w n="11.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4" mp="C">e</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340" place="5" mp="M">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212" place="6" caesura="1">en</seg>ts</w><caesura></caesura> <w n="11.5">p<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409" place="8" mp="M">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="306" place="9">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="10" mp="P">an</seg>s</w> <w n="11.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="11" mp="C">on</seg></w> <w n="11.8" punct="vg:12"><pgtc id="6" weight="0" schema="[R"><rhyme label="c" id="6" gender="f" type="e" qr="C0"><seg phoneme="a" type="vs" value="1" rule="341" place="12">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="vg" mp="F">e</seg></rhyme></pgtc></w>,</l>
					<l n="12" num="2.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="12.1">S<seg phoneme="a" type="vs" value="1" rule="340" place="1">a</seg></w> <w n="12.2">b<seg phoneme="o" type="vs" value="1" rule="315" place="2">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409" place="3">é</seg></w> <w n="12.3">r<seg phoneme="a" type="vs" value="1" rule="340" place="4">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308" place="6">ai</seg>t</w> <w n="12.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="7">e</seg>s</w> <w n="12.5" punct="pt:8">y<pgtc id="5" weight="0" schema="R"><rhyme label="b" id="5" gender="m" type="e" qr="C0"><seg phoneme="ø" type="vs" value="1" rule="398" place="8" punct="pt">eu</seg>x</rhyme></pgtc></w>.</l>
				</lg>
				<lg n="3" type="sizain" rhyme="aabccb">
					<l n="13" num="3.1" lm="12" met="6+6"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12" place="1">e</seg></w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-19" place="2" mp="Mem">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466" place="3">in</seg>t</w> <w n="13.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418" place="4" mp="C">on</seg></w> <w n="13.4" punct="pi:6"><seg phoneme="a" type="vs" value="1" rule="341" place="5" mp="M">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425" place="6" punct="pi" caesura="1">ou</seg>r</w> ?<caesura></caesura> <w n="13.5">L</w>’<w n="13.6">h<seg phoneme="œ" type="vs" value="1" rule="407" place="7">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24" place="8" mp="F">e</seg></w> <w n="13.7">qu<seg phoneme="i" type="vs" value="1" rule="491" place="9">i</seg></w> <w n="13.8">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="10" mp="C">e</seg></w> <w n="13.9">v<seg phoneme="i" type="vs" value="1" rule="468" place="11">i</seg>t</w> <w n="13.10">n<pgtc id="7" weight="0" schema="R"><rhyme label="a" id="7" gender="f" type="a" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="308" place="12">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="14" num="3.2" lm="12" met="6+6"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340" place="1" mp="Lp">a</seg></w>-<w n="14.3">t</w>-<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="358" place="2">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24" place="3" mp="F">e</seg></w> <w n="14.5">v<seg phoneme="y" type="vs" value="1" rule="450" place="4">u</seg></w> <w n="14.6" punct="pi:6">m<seg phoneme="u" type="vs" value="1" rule="425" place="5" mp="M">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468" place="6" punct="pi" caesura="1">i</seg>r</w> ?<caesura></caesura> <w n="14.7">V<seg phoneme="e" type="vs" value="1" rule="409" place="7" mp="M">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450" place="8">u</seg>s</w> <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="307" place="9" mp="M">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407" place="10">eu</seg>rs</w> <w n="14.9">p<seg phoneme="ø" type="vs" value="1" rule="398" place="11" mp="Lc">eu</seg>t</w>-<w n="14.10"><pgtc id="7" weight="0" schema="[R"><rhyme label="a" id="7" gender="f" type="e" qr="C0"><seg phoneme="ɛ" type="vs" value="1" rule="412" place="12">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="15" num="3.3" lm="8" met="8"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="1">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439" place="2">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340" place="3">a</seg></w> <w n="15.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161" place="4">e</seg>s</w> <w n="15.3">v<seg phoneme="ø" type="vs" value="1" rule="248" place="5">œu</seg>x</w> <w n="15.4" punct="pt:8">f<seg phoneme="y" type="vs" value="1" rule="450" place="6">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468" place="7">i</seg><pgtc id="8" weight="2" schema="CR">t<rhyme label="b" id="8" gender="m" type="a" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>fs</rhyme></pgtc></w>.</l>
					<l n="16" num="3.4" lm="12" met="6+6"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308" place="1">ai</seg>s</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="2" mp="C">e</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340" place="3" mp="M">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268" place="4">um</seg></w> <w n="16.4">d<seg phoneme="y" type="vs" value="1" rule="450" place="5" mp="C">u</seg></w> <w n="16.5">c<seg phoneme="œ" type="vs" value="1" rule="249" place="6" caesura="1">œu</seg>r</w><caesura></caesura> <w n="16.6">j<seg phoneme="a" type="vs" value="1" rule="341" place="7" mp="M">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308" place="8">ai</seg>s</w> <w n="16.7">n<seg phoneme="ə" type="em" value="1" rule="e-12" place="9" mp="C">e</seg></w> <w n="16.8">s</w>’<w n="16.9" punct="pv:12"><seg phoneme="e" type="vs" value="1" rule="409" place="10" mp="M">é</seg>v<seg phoneme="a" type="vs" value="1" rule="340" place="11" mp="M">a</seg>p<pgtc id="9" weight="0" schema="R"><rhyme label="c" id="9" gender="f" type="a" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" punct="pv" mp="F">e</seg></rhyme></pgtc></w> ;</l>
					<l n="17" num="3.5" lm="12" met="6+6"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412" place="1">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340" place="2" mp="M">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410" place="3">è</seg>s</w> <w n="17.3">d<seg phoneme="ø" type="vs" value="1" rule="398" place="4">eu</seg>x</w> <w n="17.4">m<seg phoneme="i" type="vs" value="1" rule="468" place="5">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="6" caesura="1">an</seg>s</w><caesura></caesura> <w n="17.6">j<seg phoneme="ə" type="em" value="1" rule="e-12" place="7" mp="C">e</seg></w> <w n="17.7">l<seg phoneme="ə" type="em" value="1" rule="e-12" place="8" mp="C">e</seg></w> <w n="17.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358" place="9" mp="M">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468" place="10">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364" place="11" mp="M">en</seg>c<pgtc id="9" weight="0" schema="R"><rhyme label="c" id="9" gender="f" type="e" qr="C0"><seg phoneme="ɔ" type="vs" value="1" rule="443" place="12">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5" place="13" mp="F">e</seg></rhyme></pgtc></w></l>
					<l n="18" num="3.6" lm="8" met="8"><space unit="char" quantity="8"></space><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312" place="1">an</seg>s</w> <w n="18.2">d<seg phoneme="ø" type="vs" value="1" rule="398" place="2">eu</seg>x</w> <w n="18.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64" place="3">e</seg>rs</w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409" place="4">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450" place="5">u</seg>s</w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189" place="6">e</seg>t</w> <w n="18.6" punct="pt:8">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302" place="7">ain</seg><pgtc id="8" weight="2" schema="CR">t<rhyme label="b" id="8" gender="m" type="e" qr="C0"><seg phoneme="i" type="vs" value="1" rule="468" place="8" punct="pt">i</seg>fs</rhyme></pgtc></w>.</l>
				</lg>
			</div></body></text></TEI>