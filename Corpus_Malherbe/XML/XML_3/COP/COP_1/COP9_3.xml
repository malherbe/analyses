<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PROMENADES ET INTÉRIEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="COP">
					<name>
						<forename>François</forename>
						<surname>COPPÉE</surname>
					</name>
					<date from="1842" to="1908">1842-1908</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1057 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">COP_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">PROMENADES ET INTÉRIEURS</title>
						<author>François Coppée</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ebooksgratuits.com</publisher>
						<idno type="URL">http://www.ebooksgratuits.com/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Promenades et intérieurs</title>
						<author>François Coppée</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Lemerre</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La date de création est celle donnée par l’Académie Française pour le recueil "Les Humbles"</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="COP9">
					<head type="main">La cueillette des cerises</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="1.2">j</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="1.5">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="1.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.10">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w> <w n="2.6">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="3.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="3.5">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>-<w n="3.6">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.8">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.9">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="4" num="1.4"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="4.5">j</w>’<w n="4.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bs<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>. <w n="4.7"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">fr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="5.5">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>rs</w>, <w n="5.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w>, <w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.4">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="8" num="1.8"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.8"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="9" num="1.9"><w n="9.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.2">qu</w>’<w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="9.5">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.8">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.9">j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
						<l n="10" num="1.10"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="408">e</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.5">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.7">bl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
						<l n="11" num="1.11"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2">l</w>’<w n="11.3">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.9">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="11.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.11"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="1.12"><w n="12.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="12.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.6">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.8">d</w>’<w n="12.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="13" num="1.13"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="13.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="13.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.7">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.8">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="14" num="1.14"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="14.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="14.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="14.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ;</l>
						<l n="15" num="1.15"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="15.3">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="15.5">j<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="15.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="16" num="1.16"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>-<w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="16.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.8">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="17" num="1.17"><w n="17.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="17.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="17.3">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>, <w n="17.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.8">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
						<l n="18" num="1.18"><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="18.6">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="18.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
						<l n="19" num="1.19"><w n="19.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.8">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="20" num="1.20"><w n="20.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="20.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="20.4">bl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>, <w n="20.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="20.8">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>