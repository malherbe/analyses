<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR19">
				<head type="main">Sur Son Album</head>
				<opener>
					<salute>À Armance.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="4" unit="char"></space><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">Fr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="2.2">d</w>’<w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="2.6">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="2.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.9">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> ?</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="3.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="3.5">n</w>’<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="3.7">t</w>-<w n="3.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.10">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.11"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.3">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="4.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> !</l>
					<l n="5" num="1.5"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="5.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="5.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="6.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="6.4">qu</w>’<w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.6">r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.9">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.10">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.2">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="7.4">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.7">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="8" num="1.8"><space quantity="12" unit="char"></space><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.4"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="8.5">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.7">l</w>’<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
				</lg>
			</div></body></text></TEI>