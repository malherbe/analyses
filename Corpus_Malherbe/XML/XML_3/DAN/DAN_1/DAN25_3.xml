<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret de Crusoé</title>
				<title type="medium">Édition électronique</title>
				<author key="DAN">
					<name>
						<forename>Louis</forename>
						<surname>DANTIN</surname>
					</name>
					<date from="1865" to="1945">1865-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2468 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">DAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/louisdantinlecofretdecrusoee.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Le Coffret de Crusoé</title>
						<author>Louis Dantin</author>
						<imprint>
							<publisher>ÉDITIONS ALBERT LÉVESQUE</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">CHANSON INTIME</head><div type="poem" key="DAN25">
						<head type="main">Sympathie astrale</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="1.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.9">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">cr<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">l<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.9">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="3.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>rs</w> <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="4.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>rs</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="4.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="5.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.7">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="6.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
							<l n="7" num="2.3"><w n="7.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="7.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
							<l n="8" num="2.4"><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="8.8">s</w>’<w n="8.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.3">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>rs</w> <w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>, <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
							<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="11" num="3.3"><w n="11.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.2">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="11.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.7">l</w>’<w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="13.4">j</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.7">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="13.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.9">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="13.10">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.11">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="14" num="4.2"><w n="14.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="14.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>,</l>
							<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.6">d</w>’<w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.8"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.6">l</w>’<w n="16.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="17.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.7">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
							<l n="18" num="5.2"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="18.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.6"><seg phoneme="y" type="vs" value="1" rule="251">eû</seg>t</w> <w n="18.7">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="18.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.9">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="19" num="5.3"><w n="19.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.6">d</w>’<w n="19.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ctr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.8">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="20" num="5.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="20.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.7">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.8">d</w>’<w n="20.9"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="21.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="21.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="21.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="21.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="21.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="21.9">pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="22" num="6.2"><w n="22.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="22.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="22.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="22.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.7">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="22.8">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w>,</l>
							<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="23.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="23.6">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w></l>
							<l n="24" num="6.4"><w n="24.1">S</w>’<w n="24.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="24.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="24.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						</lg>
						<ab type="dot">… ... … ... … ... … ... … ... … ... … ... … ... … ... …</ab>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="25.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="25.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.7"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
							<l n="26" num="7.2"><w n="26.1">L</w>’<w n="26.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="26.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="26.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="26.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
							<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="27.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.6">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="27.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="28" num="7.4"><w n="28.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="28.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="28.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.9">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>…</l>
						</lg>
					</div></body></text></TEI>