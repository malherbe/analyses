<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN73">
					<head type="main">À Madame Des-Houillières.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="œ" type="vs" value="1" rule="407">EU</seg>N<seg phoneme="ə" type="ee" value="0" rule="e-23">E</seg></w> <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w>-<w n="1.5">H<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>,</l>
						<l n="4" num="1.4"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="5.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>, <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">j</w>’<w n="6.7">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
						<l n="8" num="1.8"><w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="8.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="8.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.7">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
						<l n="9" num="1.9"><w n="9.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="1.10"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="12" num="1.12"><w n="12.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
						<l n="13" num="1.13"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.2">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="14" num="1.14"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="15" num="1.15"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="354">E</seg>x<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> :</l>
						<l n="16" num="1.16"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="16.3">B<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.7">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="17" num="1.17"><w n="17.1">B<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.4">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="17.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="1.18"><w n="18.1">Qu</w>’<w n="18.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="18.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> ; <w n="18.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.7">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="18.8">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="1.19">(<w n="19.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rv<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.2">qu</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>)</l>
						<l n="20" num="1.20"><w n="20.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.5">j<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						<l n="21" num="1.21"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="21.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="1.22"><w n="22.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="22.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">qu</w>’<w n="22.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="22.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="23" num="1.23"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>, <w n="23.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">t<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="23.4">Qu<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>,</l>
						<l n="24" num="1.24"><w n="24.1">C</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="24.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="24.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="24.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="24.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.8">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w>.</l>
						<l n="25" num="1.25"><w n="25.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w>-<w n="25.2">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="25.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="1.26"><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="26.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.7">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="1.27"><w n="27.1">M<seg phoneme="wa" type="vs" value="1" rule="424">oy</seg></w> <w n="27.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="27.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.5">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.6">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>.</l>
						<l n="28" num="1.28"><w n="28.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.2">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="28.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>,</l>
						<l n="29" num="1.29"><w n="29.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="29.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.6">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="1.30"><w n="30.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="30.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">s<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="30.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.7">j</w>’<w n="30.8"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>pp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="31" num="1.31"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="31.2">l</w>’<w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="31.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="32" num="1.32"><w n="32.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="32.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="32.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="32.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="32.7">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="33" num="1.33"><w n="33.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="33.2">qu</w>’<w n="33.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="33.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="33.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="33.7">s</w>’<w n="33.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="34" num="1.34"><w n="34.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="34.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="34.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ils</w> <w n="34.5">qu</w>’<w n="34.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="34.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="35" num="1.35"><w n="35.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>, <w n="35.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="35.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="35.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="35.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="35.7">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></l>
						<l n="36" num="1.36"><w n="36.1">N</w>’<w n="36.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="36.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.4">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="36.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>,</l>
						<l n="37" num="1.37"><w n="37.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="37.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="37.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="37.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.6">s<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="38" num="1.38"><w n="38.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="38.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="38.3">n</w>’<w n="38.4"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="38.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="38.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="38.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="39" num="1.39"><w n="39.1">J</w>’<w n="39.2"><seg phoneme="ɛ" type="vs" value="1" rule="323">ay</seg></w> <w n="39.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="39.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="39.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="39.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="39.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.9">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="40" num="1.40"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="40.2">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.4">n</w>’<w n="40.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="40.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="40.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
						<l n="41" num="1.41"><w n="41.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.2">n</w>’<w n="41.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="41.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="41.5">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="41.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="41.7">j</w>’<w n="41.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="41.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="42" num="1.42"><w n="42.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="42.2">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="42.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="42.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="42.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="42.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="43" num="1.43"><w n="43.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.2">Cl<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc</w> <w n="43.3">n</w>’<w n="43.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="43.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="43.6">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="43.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="43.8">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="44" num="1.44"><w n="44.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="44.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="44.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>t</w> <w n="44.5">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						<l n="45" num="1.45"><w n="45.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="45.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="45.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
						<l n="46" num="1.46"><w n="46.1">L</w>’<w n="46.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="46.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="46.4">t<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="46.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="46.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="46.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="47" num="1.47"><w n="47.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="47.2">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="47.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="47.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="47.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.7">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
						<l n="48" num="1.48"><w n="48.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="48.2">qu</w>’<w n="48.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="48.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="48.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="48.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="48.7">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>