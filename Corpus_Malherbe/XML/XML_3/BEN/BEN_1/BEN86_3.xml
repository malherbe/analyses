<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN86">
					<head type="main">Sur une nouvelle affection, <lb></lb>aprés la mort d’une Maîtresse.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">E</seg></w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="1.5">j<seg phoneme="ə" type="ee" value="0" rule="e-14">e</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w></l>
						<l n="5" num="1.5"><w n="5.1">V<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.4">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>lv<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.7">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ?</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w>, <w n="7.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
						<l n="8" num="2.2"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="9" num="2.3"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">fl<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="10" num="2.4"><w n="10.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.5">m</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
						<l n="11" num="2.5"><w n="11.1">D</w>’<w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="11.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="2.6"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">B<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="13.2"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="13.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="13.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="13.6">d</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="14.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.7">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
						<l n="15" num="3.3"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="3.4"><w n="16.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="16.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>,</l>
						<l n="17" num="3.5"><w n="17.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="17.5">qu</w>’<w n="17.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.7">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="17.8">d</w>’<w n="17.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="3.6"><w n="18.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.2">j</w>’<w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">cr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="20" num="4.2"><w n="20.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="21" num="4.3"><w n="21.1">N</w>’<w n="21.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="21.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.8">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="22" num="4.4"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="23" num="4.5"><w n="23.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">t</w>’<w n="23.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="4.6"><w n="24.1">S<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="24.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="25.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="25.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="26" num="5.2"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.4">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.6">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="27" num="5.3"><w n="27.1">M</w>’<w n="27.2"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.5">d</w>’<w n="27.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="28" num="5.4"><w n="28.1">J</w>’<w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="28.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.4">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.6">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="29" num="5.5"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="29.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="29.3">n</w>’<w n="29.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="29.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="5.6"><w n="30.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="30.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="30.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="30.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="31.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="31.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="31.5">m</w>’<w n="31.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w>,</l>
						<l n="32" num="6.2"><w n="32.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="a" type="vs" value="1" rule="316">a</seg></w> <w n="32.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w>,</l>
						<l n="33" num="6.3"><w n="33.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.2">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="34" num="6.4"><w n="34.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="34.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="34.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.6">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="35" num="6.5"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="35.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="35.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="35.4">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="35.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="36" num="6.6"><w n="36.1">D</w>’<w n="36.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="36.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="38" num="7.2"><w n="38.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.2">l</w>’<w n="38.3">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="38.6">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w>,</l>
						<l n="39" num="7.3"><w n="39.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.3">s<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="39.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="39.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="40" num="7.4"><w n="40.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="40.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="40.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="40.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.6">l<seg phoneme="wa" type="vs" value="1" rule="424">oy</seg></w>,</l>
						<l n="41" num="7.5"><w n="41.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="41.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="41.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.4">j</w>’<w n="41.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="41.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="41.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="41.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="42" num="7.6"><w n="42.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="42.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="42.4">n</w>’<w n="42.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="42.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="42.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="42.8">m<seg phoneme="wa" type="vs" value="1" rule="424">oy</seg></w>.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><w n="43.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="43.2">qu</w>’<w n="43.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="43.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="44" num="8.2"><w n="44.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="44.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="44.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="44.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
						<l n="45" num="8.3"><w n="45.1">Tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="45.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="45.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="46" num="8.4"><w n="46.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="46.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="46.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="46.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="46.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w></l>
						<l n="47" num="8.5"><w n="47.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="47.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.3">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="47.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.6">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="48" num="8.6"><w n="48.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="48.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="48.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="48.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="48.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="48.7">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>.</l>
					</lg>
				</div></body></text></TEI>