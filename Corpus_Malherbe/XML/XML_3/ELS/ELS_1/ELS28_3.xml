<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">GROTESQUES</head><div type="poem" key="ELS28">
					<head type="number">III</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="1.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w></l>
						<l n="2" num="1.2"><w n="2.1">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.2">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="6" num="2.2"><w n="6.1">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.4">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.2">F<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">F<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="7.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w> <w n="9.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>ph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>ph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="3.2"><w n="10.1">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="10.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="10.5">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.2">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.4">B<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>f</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>,</l>
						<l n="14" num="4.2"><w n="14.1">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="14.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.3">C<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>q</w> <w n="14.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.2">M<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>rs</w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w></l>
						<l n="16" num="4.4"><w n="16.1">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.2">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="17.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.5">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="5.2"><w n="18.1">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.2">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="19.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.4">R<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="19.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="20.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w>.</l>
					</lg>
				</div></body></text></TEI>