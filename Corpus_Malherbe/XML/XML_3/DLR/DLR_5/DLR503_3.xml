<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA ROUTE</head><div type="poem" key="DLR503">
					<head type="main">TCHÉLÉBI MÉHÉMET KHAN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lcr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="1.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.10">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="2.3">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> ,</l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">j<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="3.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.5">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.8">Br<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="5.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="5.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> ?</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="6.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">cr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">l</w>’<w n="6.9"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.6">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="7.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="7.8">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">j<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="8.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.8">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="9.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.10">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">f<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="j" type="sc" value="0" rule="475">ï</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="10.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
						<l n="11" num="3.3"><w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="11.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="11.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.9">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
						<l n="12" num="3.4"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rb<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="12.4">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>t</w>, <w n="13.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="13.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w>, <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="14.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="14.7">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="14.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.9">sc<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="15.6">d</w>’<w n="15.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">f<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="j" type="sc" value="0" rule="475">ï</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="16.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="16.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>.</l>
					</lg>
				</div></body></text></TEI>