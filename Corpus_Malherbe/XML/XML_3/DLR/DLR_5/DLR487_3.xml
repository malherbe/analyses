<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MER</head><div type="poem" key="DLR487">
					<head type="main">MARI STELLA</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w>, <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.6">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="2.2">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.4">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.10">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.5">b<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.8">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.10">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="5.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>, <w n="5.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="5.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.3">s<seg phoneme="ɔ" type="vs" value="1" rule="317">au</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.8">l</w>’<w n="6.9"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.10">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.11">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.12">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="7.2">n</w>’<w n="7.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.9">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.10">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="7.11"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.3">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.5">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="9.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.7">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="9.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.10">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="9.11">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.9">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> ;</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="11.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="11.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
						<l n="12" num="3.4"><w n="12.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.5">n</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="12.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="12.9">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="12.10">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="12.11">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.12"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">R<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="13.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>… <w n="13.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="13.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.7">qu</w>’<w n="13.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.9">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.10">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
						<l n="14" num="4.2"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.4">s<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w>, <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.9">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1">Qu</w>’<w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="15.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.8">s<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r</w> <w n="15.9">d</w>’<w n="15.10"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="16" num="4.4"><w n="16.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="16.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.8">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="17.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="17.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.5">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="17.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.8">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w></l>
						<l n="18" num="5.2"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="18.5">d</w>’<w n="18.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="18.7">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="18.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.10">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="19.2">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="19.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.4">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="19.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="19.7">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="19.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.9">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lv<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>,</l>
						<l n="20" num="5.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="20.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>’ <w n="20.8">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="20.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="20.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.11">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>