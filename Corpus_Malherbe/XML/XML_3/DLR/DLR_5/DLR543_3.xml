<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHEZ NOUS</head><div type="poem" key="DLR543">
					<head type="main">DON D’HIVER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.3">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.4">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.5">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>’<w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">l</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">h<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.8">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
						<l n="6" num="2.2"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.3">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.4">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.5">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="9.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>. <w n="9.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w>-<w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>,</l>
						<l n="10" num="3.2"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="11" num="3.3"><w n="11.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="11.2">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="11.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="12.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="12.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">t</w>’<w n="12.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.7">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
						<l n="13" num="3.5"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.3">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.4">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.5">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>