<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">PAR VENTS ET MARÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, OCR, Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1987 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2020</date>
				<idno type="local">DLR_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">PAR VENTS ET MARÉES</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER</publisher>
							<date when="1910">1910</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1910">1910</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA ROUTE</head><div type="poem" key="DLR505">
					<head type="main">VOYAGES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="3.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="3.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="3.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="3.9">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="3.10">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.3">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="4.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.8">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.11">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.12">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.6">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.8">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.3">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="6.5">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">gr<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="7.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="8.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.6">sm<seg phoneme="i" type="vs" value="1" rule="493">y</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.6">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="9.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.8">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rth<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.8">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">d</w>’<w n="12.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.5">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="13.7">d</w>’<w n="13.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="13.9">qu</w>’<w n="13.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.11">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">P<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="15.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.2">L</w>’<w n="16.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="16.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="16.5">d</w>’<w n="16.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.8">d</w>’<w n="16.9"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p</w> <w n="17.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.3">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>, <w n="17.6">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.3">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.6">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
						<l n="19" num="5.3"><w n="19.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w>-<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">d</w>’<w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="20.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.8">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="22.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="23.3">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="23.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="24.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="24.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.5">d</w>’<w n="24.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="25.2">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="25.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.7">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="26" num="7.2"><w n="26.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="26.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="26.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.9">l</w>’<w n="26.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.11">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> !</l>
						<l n="27" num="7.3"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w>, <w n="27.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="27.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="27.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.7">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="7.4"><w n="28.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="28.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>, <w n="28.3">cl<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.5">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="28.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rc</w>, <w n="28.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="28.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.9">B<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sph<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> ?</l>
					</lg>
				</div></body></text></TEI>