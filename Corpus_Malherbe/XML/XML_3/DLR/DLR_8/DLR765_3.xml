<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR765">
				<head type="main">Les crapauds</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="1.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="1.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="1.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ds</w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="3" num="1.3">«<w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="3.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>-<w n="3.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>, <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.7">p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="4.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>. »</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">-<w n="5.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>. <w n="5.2">n</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.6">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="5.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.9">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.10"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ?</l>
					<l n="6" num="2.2"><w n="6.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.5">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.6">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.3">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.4"><w n="8.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="8.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="9.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="10.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.6">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>sf<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
					<l n="11" num="3.3"><w n="11.1">D<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.2">d</w>’<w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="11.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="11.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="11.6">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="12" num="3.4"><w n="12.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="12.2">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.3">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="12.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.2">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>, <w n="13.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.6">h<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.5">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w> <w n="15.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.7">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="16.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="16.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="16.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="16.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.8">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="17.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.5">n</w>’<w n="17.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="18.5">d</w>’<w n="18.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>.</l>
					<l n="19" num="5.3"><w n="19.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="19.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="20" num="5.4">‒ <w n="20.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="20.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>, <w n="20.6">c</w>’<w n="20.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.8">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="20.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.10">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.11">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>,</l>
				</lg>
			</div></body></text></TEI>