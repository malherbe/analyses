<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR782">
				<head type="main">L’Ange Gardien</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="1.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="3.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.4">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.2">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w></l>
					<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="7.6">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
					<l n="8" num="2.4"><space unit="char" quantity="10"></space><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="9.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w></l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="10.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="11.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="11.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.7">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>,</l>
					<l n="12" num="3.4"><space unit="char" quantity="10"></space><w n="12.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w> : <w n="13.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.4">n</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="13.6">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> !</l>
					<l n="14" num="4.2"><w n="14.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="15.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="15.7">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>,</l>
					<l n="16" num="4.4"><space unit="char" quantity="10"></space><w n="16.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">Om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="17.2">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.4">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">l</w>’<w n="17.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="18" num="5.2"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.2">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="18.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="18.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="20" num="5.4"><space unit="char" quantity="10"></space><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="22" num="6.2"><w n="22.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> : « <w n="22.2">Ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.3">m</w>’<w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> ! »</l>
					<l n="23" num="6.3"><w n="23.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="23.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="23.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w></l>
					<l n="24" num="6.4"><space unit="char" quantity="10"></space><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="24.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.3">t</w>’<w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="25.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w>,</l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="26.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="26.4">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="27" num="7.3"><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="27.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="27.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="28" num="7.4"><space unit="char" quantity="10"></space><w n="28.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="28.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="29.3">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="29.5">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w></l>
					<l n="30" num="8.2"><w n="30.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="30.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="30.4">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="30.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="31" num="8.3"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="31.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="31.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="31.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.5">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w></l>
					<l n="32" num="8.4"><space unit="char" quantity="10"></space><w n="32.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="33.2">J<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="33.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> ! <w n="33.4">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="33.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w></l>
					<l n="34" num="9.2"><w n="34.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="34.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.3">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="34.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="34.5">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="35" num="9.3"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="35.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="35.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="35.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="35.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="35.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="35.7">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>,</l>
					<l n="36" num="9.4"><space unit="char" quantity="10"></space><w n="36.1">J<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="36.3">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1"><seg phoneme="ɛ" type="vs" value="1" rule="305">Ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="37.2">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>, <w n="37.3">c</w>’<w n="37.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="37.5">l</w>’<w n="37.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
					<l n="38" num="10.2"><w n="38.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="38.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="39" num="10.3"><w n="39.1"><seg phoneme="ɛ" type="vs" value="1" rule="305">Ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="39.2">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>, <w n="39.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.4">j<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="39.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="39.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
					<l n="40" num="10.4"><space unit="char" quantity="10"></space><w n="40.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.2">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="41.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="41.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>, <w n="41.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="41.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.6">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="41.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="41.8">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
					<l n="42" num="11.2"><w n="42.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="42.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.3">n</w>’<w n="42.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="42.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="42.6">dr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="43" num="11.3"><w n="43.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="43.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="43.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="43.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
					<l n="44" num="11.4"><space unit="char" quantity="10"></space><w n="44.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="44.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>