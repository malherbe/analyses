<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR756">
				<head type="main">Le Ballon Rouge</head>
				<lg n="1">
					<l n="1" num="1.1">« <w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="2" num="1.2"><w n="2.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="2.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="3.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ! »</l>
					<l n="4" num="1.4"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="4.3">qu</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="4.5">t</w>-<w n="4.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ?</l>
					<l n="5" num="1.5"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="5.7">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4">cr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					<l n="7" num="1.7"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! <w n="7.4">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.5">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="7.6">n</w>’<w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="8" num="1.8"><w n="8.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l n="9" num="1.9"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="9.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="9.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1">N</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">br<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.3">qu</w>’<w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="11.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="2.2"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="12.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="13" num="2.3"><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.3">cr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
					<l n="14" num="2.4"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="14.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="14.3">t</w>-<w n="14.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.5">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="15" num="2.5"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="16" num="2.6"><w n="16.1">R<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.2"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="16.3">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w>, <w n="16.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="16.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="16.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w>,</l>
					<l n="17" num="2.7"><w n="17.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="17.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="18" num="2.8"><w n="18.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="18.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="18.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="18.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="18.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l n="19" num="2.9"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="19.4">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="2.10"><w n="20.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="20.3">cr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="20.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="21" num="2.11"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="21.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ?</l>
				</lg>
				<lg n="3">
					<l n="22" num="3.1"><w n="22.1">Fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="22.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="23" num="3.2"><w n="23.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.2">qu</w>’<w n="23.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="23.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="23.7">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
					<l n="24" num="3.3"><w n="24.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="24.4">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="24.5">m<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w>,</l>
					<l n="25" num="3.4"><w n="25.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="25.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
				</lg>
			</div></body></text></TEI>