<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES MIGNONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1223 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÈMES MIGNONS</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÈMES MIGNONS</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Honfleur</pubPlace>
									<publisher>Éditions de la Lieutenance</publisher>
									<date when="2002">2002</date>
								</imprint>
							</monogr>
							<note>L’édition de 2002 est la source de cette version électronique comme le prouve le choix des poèmes et les erreurs d’édition.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
				<sourceDesc>
					<biblStruct>
						<monogr>
							<title>POÈMES MIGNONS</title>
							<author>Lucie Delarue-Mardrus</author>
							<imprint>
								<pubPlace>Paris</pubPlace>
								<publisher>LIBRAIRIE GEDALGE</publisher>
								<date when="1929">1929</date>
							</imprint>
						</monogr>
						<note>Édition de référence (numérisée)</note>
					</biblStruct>
				</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1929">1929</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le texte a été mis en conformité avec l’édition originale de 1929.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Les poèmes manquants de l’édition 2002 qui est à l’origine de la version électronique disponible on été ajoutés afin d’être conforme à l’édition de 1929.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR775">
				<head type="main">L’Avion</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.7">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>,</l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="3" num="1.3"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="6" num="2.2"><w n="6.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w></l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>-<w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.4"><space unit="char" quantity="10"></space><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="8.2">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.4">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="10" num="3.2"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rf</w>-<w n="11.4">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="12" num="3.4"><space unit="char" quantity="10"></space><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.4">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="13.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>, <w n="13.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="15" num="4.3"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ‒ <w n="15.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ‒</l>
					<l n="16" num="4.4"><space unit="char" quantity="10"></space><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="16.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>