<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN PLEIN VENT</head><div type="poem" key="DLR3">
					<head type="main">AU MATIN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="1.6">tr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="2.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.9">l</w>’<w n="2.10"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="3.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">n</w>’<w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.7">qu</w>’<w n="3.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.11">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.9">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.4">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="5.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.10">l</w>’<w n="5.11"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="5.12">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.13">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="6.7">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="6.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.10">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="6.11">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w></l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w></l>
						<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="8.5">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">m<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="8.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.9">s</w>’<w n="8.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.11"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="9.6">d</w>’<w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.8">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
						<l n="10" num="3.2"><w n="10.1">P<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="10.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.9">cr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="11.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9">gr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="12" num="3.4"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.7">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="14" num="4.2"><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="480">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>nt</w> <w n="14.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w></l>
						<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.8">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w></l>
						<l n="16" num="4.4"><w n="16.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.3">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.9">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.8">j</w>’<w n="17.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
						<l n="18" num="5.2"><w n="18.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="18.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.8">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="18.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.10">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="19.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="19.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.2">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="20.8">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>.</l>
					</lg>
				</div></body></text></TEI>