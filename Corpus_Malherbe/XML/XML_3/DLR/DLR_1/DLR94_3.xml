<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ENCENSOIR</head><div type="poem" key="DLR94">
					<head type="main">PETITS PIEDS</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="12"></space><w n="1.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="1.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.8">fl<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Qu</w>’<w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">em</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="3.4">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>ts</w> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="2.2"><space unit="char" quantity="12"></space><w n="4.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">tr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="4.4">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w></l>
						<l n="5" num="2.3"><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">c<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="5.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.10">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w></l>
						<l n="6" num="2.4"><w n="6.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="6.2">l</w>’<w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sph<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
						<l n="7" num="2.5"><space unit="char" quantity="12"></space><w n="7.1">Fl<seg phoneme="a" type="vs" value="1" rule="341">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="7.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="7.4">g<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="8" num="2.6"><w n="8.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.8">fl<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="9" num="2.7"><space unit="char" quantity="12"></space><w n="9.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="9.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ;</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1"><space unit="char" quantity="12"></space><w n="10.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="10.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="11" num="3.2"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.5">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w></l>
						<l n="12" num="3.3"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="12.2">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ls</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ls</w>, <w n="12.8">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="12.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.10">f<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="13" num="3.4"><space unit="char" quantity="12"></space><w n="13.1">Pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="13.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
						<l n="14" num="3.5"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="14.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="14.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.5">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
						<l n="15" num="3.6"><w n="15.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ls<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="15.2">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.7">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="16" num="3.7"><space unit="char" quantity="12"></space><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.4">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="17" num="3.8"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.8">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>,</l>
						<l n="18" num="3.9"><space unit="char" quantity="12"></space><w n="18.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="18.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ;</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><space unit="char" quantity="12"></space><w n="19.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="19.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="20" num="4.2"><w n="20.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="20.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.7">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="20.8">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
						<l n="21" num="4.3"><w n="21.1">P<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="21.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.8">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="22" num="4.4"><space unit="char" quantity="12"></space><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ds</w> <w n="23.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="23.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.7"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="23.8">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="24" num="4.6"><w n="24.1">J<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="24.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="24.5">d</w>’<w n="24.6"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.7">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="24.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.9">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> ;</l>
						<l n="25" num="4.7"><w n="25.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.3">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">d<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="25.8">b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="26" num="4.8"><w n="26.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="26.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="26.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.7">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="26.8">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
						<l n="27" num="4.9"><space unit="char" quantity="12"></space><w n="27.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="27.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ;</l>
					</lg>
					<lg n="5">
						<l n="28" num="5.1"><space unit="char" quantity="12"></space><w n="28.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="28.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="28.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="29" num="5.2"><w n="29.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="29.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="29.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="29.7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="29.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.9">gu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="30" num="5.3"><w n="30.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="30.4">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="30.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="30.6">d</w>’<w n="30.7">h<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.8">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="31" num="5.4"><w n="31.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.2">qu</w>’<w n="31.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="31.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.5">s</w>’<w n="31.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.8">s</w>’<w n="31.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.10">l</w>’<w n="31.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
						<l n="32" num="5.5"><space unit="char" quantity="12"></space><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="32.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
						<l n="33" num="5.6"><w n="33.1">Sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="33.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="33.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="33.4">qu</w>’<w n="33.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="33.6">qu</w>’<w n="33.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="33.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="33.9">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
						<l n="34" num="5.7"><w n="34.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="34.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="34.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="34.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.9">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="35" num="5.8"><w n="35.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="35.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="35.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="35.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="35.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="35.7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="35.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.9">gu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="36" num="5.9"><space unit="char" quantity="12"></space><w n="36.1">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="36.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="36.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>