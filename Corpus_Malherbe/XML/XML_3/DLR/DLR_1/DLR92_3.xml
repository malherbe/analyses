<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">L’ENCENSOIR</head><div type="poem" key="DLR92">
					<head type="main">OPALES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="1.6">qu</w>’<w n="1.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="1.9">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="1.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.2">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">S</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="3.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="3.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.7">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="6"></space><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="4.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="4.3">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="5" num="1.5"><space unit="char" quantity="20"></space><w n="5.1">B<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="6" num="1.6"><w n="6.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.8">pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.9">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="7" num="1.7"><space unit="char" quantity="20"></space><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1"><w n="8.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="8.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="8.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="8.7">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="8.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="9" num="2.2"><space unit="char" quantity="8"></space><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="9.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="9.5">qu</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="10" num="2.3"><space unit="char" quantity="8"></space><w n="10.1">L</w>’<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.6">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="2.4"><space unit="char" quantity="8"></space><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="2.5"><space unit="char" quantity="20"></space><w n="12.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="2.6"><w n="13.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="13.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.4">ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="13.8">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="14" num="2.7"><space unit="char" quantity="20"></space><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="14.2">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>,</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">s</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="16" num="3.2"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.5">gl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="17" num="3.3"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="17.4">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
						<l n="18" num="3.4"><space unit="char" quantity="20"></space><w n="18.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>, <w n="18.2">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="19" num="3.5"><w n="19.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="19.2">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w> <w n="19.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.8">l</w>’<w n="19.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="3.6"><space unit="char" quantity="20"></space><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="21" num="4.1"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="21.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="21.5">m<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w></l>
						<l n="22" num="4.2"><space unit="char" quantity="8"></space><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="22.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="22.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.5">d</w>’<w n="22.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w>,</l>
						<l n="23" num="4.3"><space unit="char" quantity="8"></space><w n="23.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="24" num="4.4"><space unit="char" quantity="8"></space><w n="24.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="24.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">ph<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sph<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="25" num="4.5"><space unit="char" quantity="20"></space><w n="25.1">Gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="4.6"><w n="26.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="26.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="26.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.9"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="27" num="4.7"><space unit="char" quantity="20"></space><w n="27.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
					<lg n="5">
						<l n="28" num="5.1"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="28.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="28.3">l</w>’<w n="28.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="28.9"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="29" num="5.2"><space unit="char" quantity="8"></space><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="29.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.3">pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.4">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="29.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="29.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="5.3"><space unit="char" quantity="8"></space><w n="30.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="30.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="30.5"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="30.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="31" num="5.4"><space unit="char" quantity="8"></space><w n="31.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="31.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.4">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="32" num="5.5"><space unit="char" quantity="20"></space><w n="32.1">H<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="33" num="5.6"><w n="33.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="33.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="33.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="33.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="33.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="33.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="33.8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="33.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="33.10">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="34" num="5.7"><space unit="char" quantity="20"></space><w n="34.1">D</w>’<w n="34.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>