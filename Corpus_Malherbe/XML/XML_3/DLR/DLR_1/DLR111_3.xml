<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENT</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3151 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Occident</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueoccident.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Occident</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1901">1901</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1901">1901</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
					<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VESPÉRALES</head><div type="poem" key="DLR111">
					<head type="main">PREMIER NOCTURNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">d</w>’<w n="1.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.5">s</w>’<w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.11"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.12">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w>, <w n="2.2">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="2.6">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.9">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.10"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">N</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.3">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.4">n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.5">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>th<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.4">s</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">d</w>’<w n="5.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="5.8">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.9">d</w>’<w n="5.10"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.4">l</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.9">d</w>’<w n="6.10"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w>, <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="7.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.10">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.4">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="8.5">b<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="8.7">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="9.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.5">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="9.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.9">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="10" num="3.2"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="10.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.3">qu</w>’<w n="11.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="11.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="11.10">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.11">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="12.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.4"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="12.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.9">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>