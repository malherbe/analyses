<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DE MOI-MÊME</head><div type="poem" key="DLR325">
					<head type="main">AMIE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="1.3">v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w></l>
						<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.3">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.7">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.9">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ! <w n="3.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="3.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="3.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="3.10"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w></l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="4.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="4.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.10"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cs</w> <w n="5.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="5.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.10"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="7.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.8">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.10">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w></l>
						<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="8.8">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="9.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="10" num="3.2"><w n="10.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="10.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w>…</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.3">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">s</w>’<w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.10">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">— <w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">j</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="14.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="14.9">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.8">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.2">l</w>’<w n="16.3">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.8">d</w>’<w n="16.9"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>