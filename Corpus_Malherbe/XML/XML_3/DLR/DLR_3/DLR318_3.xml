<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">HORIZONS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1794 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Horizons</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>BIBLIOTHÈQUE-CHARPENTIER, EUGÈNE FASQUELLE, ÉDITEUR</publisher>
							<date when="1905">1905</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1905">1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DE MOI-MÊME</head><div type="poem" key="DLR318">
					<head type="main">REFUS</head>
					<opener>
						<epigraph>
							<cit>
								<quote>… Et c’est pourquoi la femme a été déclarée impure.</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ; <w n="1.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> ; <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.8"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ; <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">d</w>’<w n="2.7"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, « <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> »,</l>
						<l n="4" num="1.4"><w n="4.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.5">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.8">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>… <w n="5.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.7">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.9">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="6.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w></l>
						<l n="7" num="2.3"><w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="7.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="8.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="8.7">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="10.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="11.9">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.10"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="12" num="3.4"><w n="12.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="12.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="12.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.10">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="339">A</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.2">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="13.6">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="14.6">l</w>’<w n="14.7"><seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.4">m</w>’<w n="15.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>-<w n="15.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.9">l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
						<l n="16" num="4.4"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w> <w n="16.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.5">l</w>’<w n="16.6">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.7">d</w>’<w n="16.8"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.10">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">qu</w>’<w n="17.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.5">n</w>’<w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.8"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
						<l n="18" num="5.2"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">qu</w>’<w n="18.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="18.5">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="18.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.9">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="19" num="5.3">— <w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="19.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="19.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="19.6">d</w>’<w n="19.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.9">m</w>’<w n="19.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="5.4"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
					</lg>
				</div></body></text></TEI>