<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR908">
				<head type="main">À LA NUIT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>x</w>, <w n="2.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.6">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">P<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="3.2">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-42">e</seg></w>, <w n="3.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">S<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="4.2">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="4.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="4.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="4.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">H<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <subst hand="RR" reason="analysis" type="phonemization"><del>X</del><add rend="hidden"><w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ks</w></add></subst> <w n="5.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">n</w>’<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.6">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="7.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>,</l>
					<l n="8" num="2.4"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="8.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="8.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">N<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="9.2">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="9.3">qu<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="9.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w></l>
					<l n="10" num="3.2"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">n</w>’<w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">N<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="11.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="11.3">s</w>’<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.7">vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="12.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">N<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="15.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
				</lg>	
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">N<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="17.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="18.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.2">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="19.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ll<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="19.6">f<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w>,</l>
					<l n="20" num="5.4"><w n="20.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="20.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">N<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="21.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">t</w>’<w n="21.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="21.5">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="21.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.8">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">t</w>’<w n="22.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.7">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="23" num="6.3"><w n="23.1">N<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="23.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="23.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="23.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="23.5">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="6.4"><w n="24.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.2">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.3">d</w>’<w n="24.4"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="24.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.6">l<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
				</lg>
			</div></body></text></TEI>