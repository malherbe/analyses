<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR896">
				<head type="main">LE MONSTRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">H<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="2.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="2.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="3.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="5.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rts</w> <w n="5.3">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="5.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="6.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="8.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.6">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
					<l n="11" num="3.3"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="11.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.6">l</w>’<w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="13.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="13.5">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="13.6">qu</w>’<w n="13.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.8">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="14.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="14.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="14.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="15.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="15.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.5">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="16.6">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="17.3">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="18.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>,</l>
					<l n="19" num="5.3"><w n="19.1">F<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! <w n="19.2">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="20" num="5.4"><w n="20.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="20.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.2">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="21.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.4">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="22.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="22.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> ! <w n="23.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">d</w>’<w n="23.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> !</l>
					<l n="24" num="6.4"><w n="24.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="24.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="24.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="24.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.2">n</w>’<w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="25.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.6">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="25.7">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>.</l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="26.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="26.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="26.5">qu</w>’<w n="26.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="27" num="7.3"><w n="27.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="27.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="27.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="28.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rts</w>, <w n="29.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="29.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="30" num="8.2"><w n="30.1">P<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.3">p<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="30.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
					<l n="31" num="8.3"><w n="31.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="31.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="31.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="31.6">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="32" num="8.4"><w n="32.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="32.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="32.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.6">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="33.4">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="34" num="9.2"><w n="34.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.2">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="35" num="9.3"><w n="35.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="35.2">t<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="35.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.4">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="36" num="9.4"><w n="36.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> ! <hi rend="ital"><w n="36.2">R<seg phoneme="e" type="vs" value="1" rule="DLR896_1">e</seg>qu<seg phoneme="i" type="vs" value="1" rule="DLR896_2">i</seg><seg phoneme="e" type="vs" value="1" rule="DLR896_3">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="36.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="36.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="DLR896_4">e</seg></w></hi> !</l>
				</lg>
			</div></body></text></TEI>