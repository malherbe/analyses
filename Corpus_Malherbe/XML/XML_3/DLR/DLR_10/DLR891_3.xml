<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MORT ET PRINTEMPS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2448 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruemortetprintemps.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>MORT ET PRINTEMPS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Albert Messein</publisher>
							<date when="1932">1932</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques.</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1932">1932</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les titres des poèmes ont été corrigés en fonction de l’édition de référence.</p>
				<p>Les parties manquantes des poèmes "À SAINT GERMAIN" et "À LA NUIT" ont été ajoutées</p>
				<p>Des corrections métriques qui s’imposent ont été faites (erreurs d’édition).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les retraits introduits automatiquement ont été modifiés conformément à l’édition de référence.</p>
				<p>Des corrections métriques ont été faites ; introduction de licences poétiques ; certe, encor...</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-09-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR891">
				<head type="main">NOTRE DAME LA GRANDE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="1.4">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.4">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.5">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="3.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1">H<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="5.2">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					<l n="6" num="2.2"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409">é</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="9.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1">Gr<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w></l>
					<l n="11" num="3.3"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.2">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="11.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.4">gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>-<w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w></l>
					<l n="12" num="3.4"><w n="12.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.5">c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="13.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="13.4">b<seg phoneme="i" type="vs" value="1" rule="493">y</seg>z<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>r<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="14.6">s</w>’<w n="14.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="15.5">cl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
					<l n="16" num="4.4"><w n="16.1">N<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">l</w>’<w n="16.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.6">s</w>’<w n="16.7"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="16.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">R<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lpt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="18.4">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.5">D<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="19.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="5.4"><w n="20.1">R<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
			</div></body></text></TEI>