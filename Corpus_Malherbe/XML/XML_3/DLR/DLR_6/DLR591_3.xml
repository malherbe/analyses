<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">CHEVAUX DE LA MER</head><div type="poem" key="DLR591">
					<head type="main">ORAISON</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="1.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="3.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">r<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.7">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="3.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.10">r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.7">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="5.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="6.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.9">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1">S<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="8.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.6">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">V<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.8">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">V<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="11.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.4">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="13.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="13.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1">V<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="14.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.5">d</w>’<w n="14.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>.</l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="15.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="16.5">d</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.3">l</w>’<w n="17.4">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1">J</w>’<w n="18.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="18.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="18.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.7">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="18.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.10">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.11">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu</w>’<w n="18.12"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>.</l>
						<l n="19" num="5.3"><w n="19.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.2">l<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="19.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.8">m</w>’<w n="19.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.10">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="19.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">J</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="20.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="20.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="20.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="8"></space><w n="21.1">B<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="21.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">j</w>’<w n="21.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="6.2"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">t</w>’<w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="22.4">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="22.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.9">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="22.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.11">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> !</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="23.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="201">e</seg>chs</w> <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.9">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x</w> <w n="24.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><space unit="char" quantity="8"></space><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="25.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="25.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="25.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.7">cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="26" num="7.2"><w n="26.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ! <w n="26.3">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.4">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ! <w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="26.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="26.7">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.8">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.9">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="26.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
						<l n="27" num="7.3"><w n="27.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="27.2">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="27.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="27.4">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="27.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="28.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> !</l>
					</lg>
				</div></body></text></TEI>