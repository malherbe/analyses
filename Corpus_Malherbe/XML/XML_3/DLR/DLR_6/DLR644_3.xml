<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">LA GUERRE</head><div type="poem" key="DLR644">
					<head type="main">REFRAIN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">p<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">l</w>’<w n="3.6">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>br<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ts</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">l</w>’<w n="4.5">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="5.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">S<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">m</w>’<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.5">s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
						<l n="7" num="2.2"><w n="7.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.3"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="9" num="2.4"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="9.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.8">l</w>’<w n="9.9"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="2.5"><w n="10.1">Ch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.2">d</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.4">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>, <w n="10.5">fl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.6">d</w>’<w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ;</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="11.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="11.3">d</w>’<w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.5">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.6">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="12" num="3.2"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="13" num="3.3"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="13.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="13.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w></l>
						<l n="14" num="3.4"><w n="14.1">R<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="14.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="15" num="3.5"><w n="15.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="15.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="15.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="16.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.7">bl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w>,</l>
						<l n="17" num="4.2"><w n="17.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="17.3">qu<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.5">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="4.3"><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.3">s</w>’<w n="18.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="18.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="18.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="19" num="4.4"><w n="19.1">Br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="19.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">M<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch</w>,</l>
						<l n="20" num="4.5"><w n="20.1">J</w>’<w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.7">S<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w>,</l>
						<l n="22" num="5.2"><w n="22.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="22.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="22.5">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="23" num="5.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="23.4">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> ?</l>
						<l n="24" num="5.4"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="24.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.3">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="24.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
						<l n="25" num="5.5">‒ <w n="25.1">J</w>’<w n="25.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="25.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					</lg>
				</div></body></text></TEI>