<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">SOUFFLES DE TEMPÊTE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3271 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">https://www.poesies.net/delaruemardruesoufledetempete.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes de guerre extraits de Souffles de tempêtes (1918)</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poetesses.blog4ever.com</publisher>
						<idno type="URL">https://poetesses.blog4ever.com/nouveaux-poemes-de-guerre-dans-souffles-de-tempete-1918</idno>
					</publicationStmt>
					<sourceDesc>
						<p>Textes complémentaires à ceux du site précédent</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>SOUFFLES DE TEMPÊTE</title>
					<author>Lucie Delarue-Mardrus</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>E. Fasquelle</publisher>
						<date when="1918">1918</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1918">1918</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes manquants ( "Mes Bonnes routes" -> fin du recueil") ont été ajoutés à partir des poèmes du site : https://poetesses.blog4ever.com.</p>
				<p>Les textes des deux sources numériques comportent de nombreuses erreurs de numérisation ; une relecture complète s’impose.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Les corrections métriques ont été faites à partir d’une version numérisée du texte effectuée par la Bibliothèque de Caen le mer.</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">ADMIRATIONS</head><div type="poem" key="DLR566">
					<head type="main">INCANTATION</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="16"></space><w n="1.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="1.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="2.7">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">Pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="3" num="1.3"><space unit="char" quantity="16"></space><w n="3.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="4.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ; <w n="4.6">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="4.7">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
						<l n="5" num="1.5"><w n="5.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="5.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.6">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.9">n</w>’<w n="5.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.12">d</w>’<w n="5.13"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="6" num="1.6"><space unit="char" quantity="16"></space><w n="6.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="6.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><space unit="char" quantity="16"></space><w n="7.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="8" num="2.2"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="8.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="8.7">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="8.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.10">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> ;</l>
						<l n="9" num="2.3"><space unit="char" quantity="16"></space><w n="9.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="10" num="2.4"><w n="10.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="10.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.9">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="11" num="2.5"><w n="11.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="11.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="11.6">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>,</l>
						<l n="12" num="2.6"><space unit="char" quantity="16"></space><w n="12.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="12.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><space unit="char" quantity="16"></space><w n="13.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="13.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						<l n="14" num="3.2"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.7">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="15" num="3.3"><space unit="char" quantity="16"></space><w n="15.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="15.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						<l n="16" num="3.4"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.2">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="16.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.7">p<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.10">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="17.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="17.5">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="17.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.8">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="3.6"><space unit="char" quantity="16"></space><w n="18.1">F<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="18.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
					</lg>
				</div></body></text></TEI>