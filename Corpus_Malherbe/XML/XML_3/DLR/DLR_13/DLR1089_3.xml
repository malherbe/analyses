<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHOIX DE POÈMES</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1917 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DLR_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Choix de poèmes</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie Alphonse Lemerre</publisher>
							<date when="1951">1951</date>
						</imprint>
					</monogr>
					<note>Édition numérisée à partir d’un ouvrage prêté par la BPU de neuchatel</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1951">1951</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie en prose (FRAGMENT D’UN TEXTE DE L’AUTEUR PRÉFAÇANT DEUX POÈMES D’EDGAR POE) n’est incluse.</p>
				<p>Les indications de la première édition d’un poème données dans la table des matières ont été reportées comme sous-titre des poèmes. L’année d’édition a été ajoutée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell (correction d’erreur d’océrisation).</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-01-14" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-01-14" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHAPITRE VII</head><head type="main_part">Rencontres</head><div type="poem" key="DLR1089">
					<head type="main">En Mer</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="1.5">d</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">n</w>’<w n="2.4"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="2.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="2.6">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
							<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="3.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="3.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="5.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="5.5">c</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.7">l</w>’<w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="6" num="2.2"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="7" num="2.3"><w n="7.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="8" num="2.4"><w n="8.1">Pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">l</w>’<w n="8.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="9" num="1.1"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="9.3">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w></l>
							<l n="10" num="1.2"><w n="10.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="10.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="10.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="11" num="1.3"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.4">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w>,</l>
							<l n="12" num="1.4"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="13" num="2.1"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="13.3">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
							<l n="14" num="2.2"><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="15" num="2.3"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w></l>
							<l n="16" num="2.4"><w n="16.1">N</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="16.4">n<seg phoneme="ø" type="vs" value="1" rule="401">eu</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="16.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> !’<w n="17.2"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="17.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
							<l n="18" num="3.2"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="19" num="3.3"><w n="19.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.3">n<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.6">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="20" num="3.4"><w n="20.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="20.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.5">h<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="21" num="4.1"><w n="21.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="22" num="4.2"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="22.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.3">c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
							<l n="23" num="4.3"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="23.3">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="23.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="23.5">n</w>’<w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="24" num="4.4"><w n="24.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="24.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1"><w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="25.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.4">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="25.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="26" num="5.2"><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="26.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.5">tr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="26.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w></l>
							<l n="27" num="5.3"><w n="27.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="27.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="27.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="28" num="5.4"><w n="28.1">D</w>’<w n="28.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="28.4">d</w>’<w n="28.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="28.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="28.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>.</l>
						</lg>
					</div>
				</div></body></text></TEI>