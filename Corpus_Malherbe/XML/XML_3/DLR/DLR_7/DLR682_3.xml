<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">EN DEUIL</head><div type="poem" key="DLR682">
					<head type="main">ENCORE LE NOUVEL AN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.3">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="3.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.6">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="4" num="1.4"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : « <w n="5.4">B<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="6.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="7.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w>, <w n="8.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="8.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.6">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="11.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="11.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="12.5">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="14.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.5">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
						<l n="15" num="4.3"><w n="15.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="15.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.6">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="17.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">b<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="18.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.3">t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
						<l n="19" num="5.3"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : « <w n="19.3">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ! <w n="19.4">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ! »</l>
						<l n="20" num="5.4"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="21.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="21.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : « <w n="21.6">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ! »</l>
						<l n="22" num="6.2"><w n="22.1">M<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="22.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.6">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="50">E</seg>s</w>-<w n="23.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w>-<w n="23.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="23.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="ɛ" type="vs" value="1" rule="50">E</seg>s</w>-<w n="25.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="25.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="25.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="26" num="7.2"><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="26.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.6">c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="7.3"><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="27.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="27.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="27.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="7.4"><w n="28.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="28.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="28.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="29.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="29.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="29.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="30" num="8.2"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.4">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="30.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> !</l>
						<l n="31" num="8.3"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="31.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="31.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="31.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="31.6">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></l>
						<l n="32" num="8.4"><w n="32.1">Qu</w>’<w n="32.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="32.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="32.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="32.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.8">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="33.2">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="33.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>,</l>
						<l n="34" num="9.2"><w n="34.1">P<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.2">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="34.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="34.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="35" num="9.3"><w n="35.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="35.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="35.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="35.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="36" num="9.4"><w n="36.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="36.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">f<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t</w> <w n="36.4">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="36.6">l</w>’<w n="36.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="36.8">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> ?…</l>
					</lg>
				</div></body></text></TEI>