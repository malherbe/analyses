<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">A MAMAN</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1250 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">A MAMAN</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LIBRAIRIE CHARPENTIER ET FASQUELLE</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						Édition numérisée.
						Merci à la bibliothèque de l’université de Denver (USA)
						qui a bien voulu prêté un exemplaire de cet ouvrage ; aucune bibliothèque
						de France n’ayant accepté le prêt ou la reproduction sur place.
					</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">SOUVENIRS</head><div type="poem" key="DLR683">
					<head type="main">COMME TOI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.4">ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="12"></space><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">qu</w>’<w n="7.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.8">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.9">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="10"></space><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w>, <w n="9.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="3.2"><space unit="char" quantity="12"></space><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
						<l n="12" num="3.4"><space unit="char" quantity="10"></space><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.6">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><space unit="char" quantity="12"></space><w n="14.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="15.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="15.4">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.7">l<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
						<l n="16" num="4.4"><space unit="char" quantity="10"></space><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w></l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="17.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="17.6">qu</w>’<w n="17.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.8">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="5.2"><space unit="char" quantity="12"></space><w n="18.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.2">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="19" num="5.3"><w n="19.1">M</w>’<w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="10"></space><w n="20.1">D</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					</lg>
				</div></body></text></TEI>