<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TEMPS PRÉSENTS</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>818 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">TEMPS PRÉSENTS</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Les Cahiers d’art et d’amitié, P. Mourousy</publisher>
							<date when="1939">1939</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur demande (BnF)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1939">1939</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR990">
				<head type="main">BALLADE DES DOMINOS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, <w n="1.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="1.5">qu</w>’<w n="1.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">H<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>,</l>
					<l n="3" num="1.3"><w n="3.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.2">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>cl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
					<l n="5" num="1.5"><w n="5.1">J<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> ! <hi rend="ital"><w n="5.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">qu<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></hi> !</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="y" type="vs" value="1" rule="450">U</seg>s</w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="6.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.5">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.3">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="7.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.2"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="10.2">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="10.7">H<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss</w> !</l>
					<l n="11" num="2.3"><w n="11.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="11.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.6">n</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="2.4"><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.3">p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="12.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>. <w n="13.4">C<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! <w n="13.5">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="14" num="2.6"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.4">s</w>’<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.7">R<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="2.7"><w n="15.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="15.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.6">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="16" num="2.8"><w n="16.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="16.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="17.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">n<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="3.2"><w n="18.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="18.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="18.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="19" num="3.3"><w n="19.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="19.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="19.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="19.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="19.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.8">m<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="20" num="3.4"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss</w>,</l>
					<l n="21" num="3.5"><w n="21.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.2">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="21.4">l</w>’<w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="22" num="3.6"><w n="22.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="22.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">j<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>f</w> <w n="22.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.6">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="23" num="3.7"><w n="23.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.2">n</w>’<w n="23.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="23.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="23.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="24.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<head type="main">ENVOI</head>
					<l n="25" num="4.1"><w n="25.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="25.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="25.4">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, (<w n="25.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w>-<w n="25.6">c<seg phoneme="ə" type="ef" value="1" rule="e-1">e</seg></w></l>
					<l n="26" num="4.2"><w n="26.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.2">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <hi rend="ital"><w n="26.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></hi>,</l>
					<l n="27" num="4.3"><w n="27.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.4">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.5">l</w>’<w n="27.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,)</l>
					<l n="28" num="4.4"><w n="28.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="28.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="28.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				</lg>
			</div></body></text></TEI>