<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Nos Secrètes Amours</title>
				<title type="sub">édition "Les Iles", 1951</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>754 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher></publisher>
						<idno type="URL">http://romanslesbiens.canalblog.com/archives/2007/11/10/6841446.html</idno>
					</publicationStmt>
					<sourceDesc>
						<p>édition "Les Iles", Paris, Imprimerie Nicolas — Niort, 1951, n° 14 sur 700 exemplaires. 48 pages.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nos Secrètes Amours</title>
						<author>Lucie Delarue-Mardrus</author>
						<editor>texte établi et annoté par Mirande Lucien</editor>
						<edition>Deuxième tirage revu (première édition : 2008)</edition>
						<imprint>
							<pubPlace>Cassaniouze</pubPlace>
							<publisher>ErosOnyx</publisher>
							<date when="2018">2018</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1902" to="1905">1902-1905</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition électronique est a priori conforme avec l’édition de Natalie Barney de 1951.</p>
				<p>Une vérification avec un exemplaire imprimé ou numérisé de l’édition de 1951 ne serait pas superflue.</p>
				<p>Des vers ont été retirés pour avoir une édition conforme avec celle de 1951 selon les notes de l’éditeur de l’édition de 2008/2018 (poème : Contradiction).</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Des retraits introduits automatiquement ont été supprimés conformément au document de l’édition source et selon l’édition imprimée de 2018.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-09-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2020-12-03" who="RR">Vérification du texte avec l’édition de 2018.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DLR1029">
				<head type="main">TON RIRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="1.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ; <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.9">r<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">j</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.8">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="2.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.4">r<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="3.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.9">gl<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.5">qu</w>’<w n="4.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.7">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.8">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. <w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="5.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.10">qu</w>’<w n="5.11"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.12">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.13">v<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="6.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.10">d</w>’<w n="6.11"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>. <w n="7.4"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.7">d</w>’<w n="7.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.9">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.11">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.5">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.7">n</w>’<w n="8.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.10">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.11">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">— <w n="9.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="9.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.8">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.9">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.10">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.11">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.8">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.6">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="11.7">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.9">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w></l>
					<l n="12" num="3.4"><w n="12.1">Pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.8">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
				</lg>
			</div></body></text></TEI>