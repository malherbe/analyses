<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN MARGE</head><div type="poem" key="DLR437">
					<head type="main">PRÉSENCE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="1.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.6">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.9">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="3.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>pp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w>, <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="5.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
						<l n="6" num="2.2"><w n="6.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="6.2">j</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="6.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">ë</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.4">s</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="7.9">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="8.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="8.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="8.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="9.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.9">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="10" num="3.2"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">t</w>’<w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="10.8">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
						<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="11.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.7">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.9">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w></l>
						<l n="12" num="3.4"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="12.4"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> <w n="12.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="14.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="14.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.6">l</w>’<w n="14.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.9">J<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="487">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.4">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="16.5">l</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="17.4">j</w>’<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.7">h<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ppl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="5.2"><w n="18.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="18.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="18.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
						<l n="19" num="5.3"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">l</w>’<w n="19.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="19.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.9">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w></l>
						<l n="20" num="5.4"><w n="20.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="20.6">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="20.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="21.4">j</w>’<w n="21.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="21.7">l</w>’<w n="21.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.10">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w></l>
						<l n="22" num="6.2"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="22.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="23" num="6.3"><w n="23.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="23.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="23.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="23.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">Im</seg>p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="24" num="6.4"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">l</w>’<w n="24.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="25.4">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="25.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="26" num="7.2"><w n="26.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.5">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="26.6">l</w>’<w n="26.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="27" num="7.3"><w n="27.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="27.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="27.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="27.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="27.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						<l n="28" num="7.4"><w n="28.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="28.2">m</w>’<w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.4">l</w>’<w n="28.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.9">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">— <w n="29.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="29.2"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="29.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="29.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="29.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="29.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="29.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.8">ch<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w></l>
						<l n="30" num="8.2"><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="30.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.4">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="30.5">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="30.7">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="31" num="8.3"><w n="31.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="31.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="31.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="31.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="31.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.8">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="32" num="8.4"><w n="32.1">Pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="32.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>, <w n="32.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="32.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.8"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="33.2">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="33.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="33.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.7">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="9.2"><w n="34.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="34.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.4">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w>, <w n="34.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="34.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="35" num="9.3"><w n="35.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="35.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.3">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="35.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="35.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="35.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.9">n<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="35.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="35.11">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="35.12"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="36" num="9.4"><w n="36.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="36.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="36.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="36.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="36.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="36.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>