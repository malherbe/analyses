<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">EN KROUMIRIE</head><div type="poem" key="DLR388">
					<head type="main">RENCONTRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="1.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w>, <w n="2.6">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.9">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">M<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>dj<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="3.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="4.6">cr<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> ; <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.7">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="6.9">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
						<l n="7" num="2.3"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rth<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.3">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.10">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w></l>
						<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="9.5">Kr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w></l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="10.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.7">l</w>’<w n="10.8"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="11" num="3.3"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="12" num="3.4"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="12.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>,</l>
						<l n="14" num="4.2"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.9">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="4.3"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w> <w n="15.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w>, <w n="15.5">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.7">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="16" num="4.4"><w n="16.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="16.3">gl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="16.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="16.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="17.3">d</w>’<w n="17.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="5.2"><w n="18.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="18.4">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="18.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.8">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="18.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="18.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.11">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
						<l n="19" num="5.3"><w n="19.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>lg<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="5.4"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="20.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>cc<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w> <w n="20.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="20.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>’<w n="21.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="21.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="22" num="6.2"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.3">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="22.6">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="22.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.8">n</w>’<w n="22.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
						<l n="23" num="6.3"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="23.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="23.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.7">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
						<l n="24" num="6.4"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="24.2">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rk<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="24.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.4">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="25.5">s</w>’<w n="25.6"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="25.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w></l>
						<l n="26" num="7.2"><w n="26.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="26.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">qu</w>’<w n="27.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="27.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="27.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.7">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>-<w n="27.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="290">aon</seg></w>.</l>
						<l n="28" num="7.4"><w n="28.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.4">s</w>’<w n="28.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="29" num="7.5"><w n="29.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="29.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>, <w n="29.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="29.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="29.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="29.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.9">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>, <w n="29.10">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="29.11">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.12">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w></l>
						<l n="30" num="7.6"><w n="30.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="30.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.3">n</w>’<w n="30.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="30.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="30.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="30.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.9">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					</lg>
				</div></body></text></TEI>