<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FIGURE DE PROUE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2836 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/delaruemardruepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Figure de Proue</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/stream/lafiguredeproue00dela/lafiguredeproue00dela_djvu.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Figure de Proue</title>
								<author>Lucie Delarue-Mardrus</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
							<note>Source des poèmes ajoutés et édition de référence pour les corrections métriques.</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
				<p>Des poèmes ont été ajoutés afin d’être conforme à l’édition de référence (source : archive.org)</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">BARBARESQUES</head><div type="poem" key="DLR379">
					<head type="main">RÊVANT D’ALGER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.2">d</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>lg<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.6">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="1.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.8">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="1.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="2.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>-<w n="2.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.3">s</w>’<w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="3.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="4" num="1.4"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="4.2">n</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w> <w n="4.7">qu</w>’<w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.9">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="4.10">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.11">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>f</w>,</l>
						<l n="5" num="1.5"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.8">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.4">sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lpt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.10">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
						<l n="7" num="1.7"><w n="7.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="7.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="7.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1">— <w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="8.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> : « <w n="8.7">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">n</w>’<w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="8.11">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>… »</l>
					</lg>
				</div></body></text></TEI>