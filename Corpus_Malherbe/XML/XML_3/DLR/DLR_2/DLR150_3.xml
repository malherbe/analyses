<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES TERRESTRES</head><div type="poem" key="DLR150">
					<head type="main">PÂQUES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r</w> <w n="1.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="1.5">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="2.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.4">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.6">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.6">j</w>’<w n="5.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.10">p<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w></l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.2">s<seg phoneme="i" type="vs" value="1" rule="493">y</seg>lv<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.3">c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.5"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="8.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.8">l</w>’<w n="8.9"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="10.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="10.3">j</w>’<w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="10.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="11" num="3.3"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">d</w>’<w n="12.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					</lg>
				</div></body></text></TEI>