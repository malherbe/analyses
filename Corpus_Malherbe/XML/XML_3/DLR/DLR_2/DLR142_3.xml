<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FERVEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>DELARUE-MARDRUS</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2031 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DLR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Ferveur</title>
						<author>Lucie Delarue-Mardrus</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k9691679h?rk=21459;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LA REVUE BLANCHE</publisher>
							<date when="1902">1902</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1902">1902</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÈMES TERRESTRES</head><div type="poem" key="DLR142">
					<head type="main">ENSEIGNEMENT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="1.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.10"><seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="2.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="2.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="2.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="4.2">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="4.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.6">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="5.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.5">n</w>’<w n="5.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>nt</w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.9"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w></l>
						<l n="6" num="1.6"><w n="6.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w> <w n="6.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.7">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.9">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>…</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> !… <w n="7.2">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.2"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ct<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="9" num="2.3"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.4">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.8">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>qu<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="10" num="2.4"><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="10.3">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="10.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="10.5">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> :</l>
						<l n="11" num="2.5"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">fl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.7">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="11.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.9">f<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="2.6"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>nt</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.8">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="13.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.6">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
						<l n="14" num="3.2"><w n="14.1">L</w>’<w n="14.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="14.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.4">l</w>’<w n="14.5">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="14.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="14.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="14.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.10">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="14.11">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.12">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w></l>
						<l n="15" num="3.3"><w n="15.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>-<w n="15.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.5">qu</w>’<w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="15.8">d</w>’<w n="15.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.10">t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="3.4"><w n="16.1">T</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.3">qu</w>’<w n="16.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.5">n</w>’<w n="16.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="16.8">d</w>’<w n="16.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.10">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.11">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="17" num="3.5"><w n="17.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="17.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="17.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.9">ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w></l>
						<l n="18" num="3.6"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct</w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.8">l</w>’<w n="18.9"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>