<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES SEPT DOULEURS D’OCTOBRE</title>
				<title type="medium">Édition électronique</title>
				<author key="DLR">
					<name>
						<forename>Lucie</forename>
						<surname>Delarue-Mardrus</surname>
					</name>
					<date from="1874" to="1945">1874-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2316 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DLR_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/luciedelaruemardrueferveur.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sept douleurs d’Octobre</title>
						<author>Lucie Delarue-Mardrus</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>J. Ferenczi et fils, éditeurs</publisher>
							<date when="1930">1930</date>
						</imprint>
					</monogr>
					<note>Édition de référence</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1930">1930</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Il manque de très nombreux poèmes dans le texte source.</p>
				<p>Les nombreux poèmes manquants ont été ajoutés à partir d’une numérisation de l’édition de référence.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-11-28" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-11-28" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VII</head><head type="main_part">L’ANGOISSE</head><div type="poem" key="DLR846">
					<head type="main">UN RÊVE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.3">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>. <w n="1.4">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">N<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="2.2">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lcr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>.</l>
						<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>. <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="3.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="3.6">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="3.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.8">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.10">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>. <w n="5.3">J</w>’<w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.6">d</w>’<w n="5.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.8"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.10">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> :</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space>« <w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> ! <w n="6.2">C</w>’<w n="6.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="6.5">S<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.9">s<seg phoneme="ɛ" type="vs" value="1" rule="355">e</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="8.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">C</w>’<w n="9.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="9.4">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.9">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> : « <w n="9.10">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.11">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="10.3">n</w>’<w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="10.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.7">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
						<l n="11" num="3.3"><w n="11.1">T</w> <w n="11.2"><seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.6">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.11">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>,</l>
						<l n="12" num="3.4"><w n="12.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.9">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« <w n="13.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">t</w>’<w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="13.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.8">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>. <w n="13.9">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.10">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="15" num="4.3"><w n="15.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="15.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="15.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="15.7">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.9">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="16" num="4.4"><w n="16.1">J</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="16.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="16.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="16.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« <w n="17.1">R<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.8">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">T</w> <w n="18.2"><seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="18.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="18.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
						<l n="19" num="5.3"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.2">d</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="19.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.8">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.9">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="19.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="20" num="5.4"><w n="20.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="20.7">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« <w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="21.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="21.6">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.8">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Qu</w>’<w n="22.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						<l n="23" num="6.3"><w n="23.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> ! <w n="23.2">C</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="23.5">C</w>’<w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !… <w n="23.8">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.10">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="6.4"><w n="24.1">V<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="24.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="24.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="24.7">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ! »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">— « <w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="25.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !… <w n="25.3">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !… » <w n="25.4">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="25.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="25.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space>« <w n="26.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="26.2">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="26.3">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="26.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="26.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> ?</l>
						<l n="27" num="7.3"><w n="27.1">D<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> !… » <w n="27.4">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="27.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.7">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="27.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="7.4"><w n="28.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="28.2">l</w>’<w n="28.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="28.7">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="28.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.9">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.10">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w>.</l>
					</lg>
				</div></body></text></TEI>