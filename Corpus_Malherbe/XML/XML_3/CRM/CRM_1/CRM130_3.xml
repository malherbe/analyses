<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM130">
				<head type="main">NON, TU NE SAVAIS PAS, MA MÈRE…</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="2.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.6">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="4.2">l</w>’<w n="4.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="4.4">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.6">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.3">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="6.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">Br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="7.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="7.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.5">n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="10.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.5">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="11" num="3.3"><w n="11.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="11.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="12" num="3.4"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="12.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.5">d</w>’<w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="14" num="4.2"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="15.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.4">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="i" type="vs" value="1" rule="480">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w>,</l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="16.2">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">j<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="16.8">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="17.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.7">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1">Qu</w>’<w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="18.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="5.4"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="20.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="20.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.6">d</w>’<w n="20.7">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>.</l>
				</lg>
			</div></body></text></TEI>