<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM83" rhyme="none">
				<head type="main">LE FERMIER</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.9">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.7">bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.2">l</w>’<w n="3.3"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="3.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">f<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="4.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.8">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.10"><seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>fs</w>,</l>
					<l n="5" num="2.2"><w n="5.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.7">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="5.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.9">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.3"><w n="6.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.3">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="6.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.10">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">l</w>’<w n="7.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="7.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.8">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="8" num="3.2"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ppr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w>, <w n="8.4">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="9" num="3.3"><w n="9.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="9.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="9.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.4">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="4.2"><w n="11.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="11.3">b<seg phoneme="ø" type="vs" value="1" rule="247">œu</seg>fs</w> <w n="11.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="11.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.9">l</w>’<w n="11.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="4.3"><w n="12.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>f<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">br<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="12.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.9">br<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>