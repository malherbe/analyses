<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">BRABANT</title>
				<title type="medium">Une édition électronique</title>
				<author key="CRM">
					<name>
						<forename>Maurice</forename>
						<surname>CARÊME</surname>
					</name>
					<date from="1899" to="1978">1899-1978</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, océrisation, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3326 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRM_1</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Brabant</title>
						<author>Maurice Carême</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS OUVRIÈRES</publisher>
							<date when="1976">1976</date>
						</imprint>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1967">1967</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-02-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-02-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRM89">
				<head type="main">SUR L’ORÉE ENNEIGÉE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="1.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="2.3">s</w>’<w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="2.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.3">pl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w>-<w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.7">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="4.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="4.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.9">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>,</l>
					<l n="5" num="2.2"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="6" num="2.3"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.4">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="6.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="7.2">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w> <w n="7.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="7.4">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.7">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>g</w> <w n="7.8">g<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="8" num="3.2"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.5">g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="9" num="3.3"><w n="9.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ec" value="0" rule="e-32">e</seg>s</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.6">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.9">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w>.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.4">n</w>’<w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="10.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="11" num="4.2"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.4">d</w>’<w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="11.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
					<l n="12" num="4.3"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>, <w n="12.5">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
				</lg>
			</div></body></text></TEI>