<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU122">
				<head type="number">LV</head>
				<head type="main">ABRUTISSEMENT</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.5">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="2.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">m</w>’<w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.7">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">h<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">n</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="5.5">qu</w>’<w n="5.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="6" num="1.6"><space unit="char" quantity="10"></space><w n="6.1">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="7.2">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="8" num="2.2"><w n="8.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="9" num="2.3"><space unit="char" quantity="10"></space><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="10" num="2.4"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="10.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>gu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="11" num="2.5"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.7">gu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="12" num="2.6"><space unit="char" quantity="10"></space><w n="12.1">Qu</w>’<w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">J</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="13.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
					<l n="14" num="3.2"><w n="14.1">J</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w></l>
					<l n="15" num="3.3"><space unit="char" quantity="10"></space><w n="15.1">Fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">j</w>’<w n="16.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="16.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="16.6">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>,</l>
					<l n="17" num="3.5"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.3">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="17.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="17.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w></l>
					<l n="18" num="3.6"><space unit="char" quantity="10"></space><w n="18.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.2">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.2">d</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="19.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.5">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="20" num="4.2"><w n="20.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="20.3">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="20.5">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="21" num="4.3"><space unit="char" quantity="10"></space><w n="21.1">M<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="22" num="4.4"><w n="22.1">J</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="22.3">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="22.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="22.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w></l>
					<l n="23" num="4.5"><w n="23.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="23.2">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.6">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w></l>
					<l n="24" num="4.6"><space unit="char" quantity="10"></space><w n="24.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="25.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.7">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></l>
					<l n="26" num="5.2"><w n="26.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="26.2">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="26.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="26.7">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></l>
					<l n="27" num="5.3"><space unit="char" quantity="10"></space><w n="27.1">D</w>’<w n="27.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="28" num="5.4"><w n="28.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">d</w>’<w n="28.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="28.4">cr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.5">f<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="29" num="5.5"><w n="29.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="29.3">s</w>’<w n="29.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="29.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="30" num="5.6"><space unit="char" quantity="10"></space><w n="30.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="30.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="31.2">d</w>’<w n="31.3"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="31.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="31.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="32" num="6.2"><w n="32.1">S<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="32.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="32.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="33" num="6.3"><space unit="char" quantity="10"></space><w n="33.1">L</w>’<w n="33.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="34" num="6.4"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="34.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.3">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.5">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="35" num="6.5"><w n="35.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="35.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.3">st<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
					<l n="36" num="6.6"><space unit="char" quantity="10"></space><w n="36.1">D</w>’<w n="36.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="37.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="37.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="38" num="7.2"><w n="38.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="38.2">d</w>’<w n="38.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="38.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="38.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="39" num="7.3"><space unit="char" quantity="10"></space><w n="39.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="39.2">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="40" num="7.4"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="40.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="40.3">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="40.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="40.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="40.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="41" num="7.5"><w n="41.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="41.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="41.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="41.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="42" num="7.6"><space unit="char" quantity="10"></space><w n="42.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="42.2">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1869">juin 1869.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>