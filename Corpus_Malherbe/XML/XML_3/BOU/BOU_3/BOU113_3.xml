<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DERNIÈRES CHANSONS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOU">
					<name>
						<forename>Louis</forename>
						<surname>BOUILHET</surname>
					</name>
					<date from="1822" to="1869">1822-1869</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2222 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BOU_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>DERNIÈRES CHANSONS</title>
						<title>POÉSIES POSTHUMES</title>
						<author>Louis Bouilhet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">M253</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>DERNIÈRES CHANSONS</title>
								<title>POÉSIES POSTHUMES</title>
								<author>Louis Bouilhet</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ŒUVRES DE LOUIS BOUILHET</title>
						<title>FESTONS ET ASTRAGALES, MELÆNIS, DERNIÈRES CHANSONS</title>
						<author>Louis Bouilhet</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1891">1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1869">1869</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules en début de vers ont été restituées.</p>
					<p>Les chiffres romains de la numérotation des poèmes ont été rétablis.</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-09-20" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.4.xsd)</change>
				<change when="2021-09-20" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOU113">
				<head type="number">XLVI</head>
				<head type="main">L’HÉRITIER DE YANG-TI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="1.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.8">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="2.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="2.7">qu</w>’<w n="2.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2"><seg phoneme="j" type="sc" value="0" rule="496">Y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w>-<w n="3.3">T<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="3.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.8">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.9">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.10">d</w>’ <w n="3.11">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
					<l n="4" num="1.4"><w n="4.1">G<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t</w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.6">tr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.10">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.11">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.2">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="5.3">qu</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="5.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="6.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.6">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="6.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="7.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="7.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="7.9">F<seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> : « <w n="7.10">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
				</lg>
				<lg n="3">
					<l n="8" num="3.1"><w n="8.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.8">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.9">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="9" num="3.2"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="9.7">d</w>’<w n="9.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! »</l>
				</lg>
			</div></body></text></TEI>