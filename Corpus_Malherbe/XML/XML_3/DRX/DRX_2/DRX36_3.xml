<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX36">
				<head type="main">L’ORGUEIL</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="1.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="1.3">dr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.4">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="1.5">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>cs</w> <w n="1.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="2.6">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="2.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="2.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> !</l>
					<l n="3" num="1.3"><w n="3.1">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>, <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="4.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lc<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="4.8"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">— <w n="5.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="5.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w>, <w n="5.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="6.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.10">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rf<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					<l n="7" num="2.3"><w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="7.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="7.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.5">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="8.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="8.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">— <w n="9.1">L</w>’<w n="9.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="9.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> ! <w n="9.7">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.8">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="9.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">c<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="11.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>cs</w> <w n="12.4">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="12.5">qu</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">— <w n="13.1"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w>, <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="13.4">l</w>’<w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.7">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="13.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.9">gr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="4.2"><w n="14.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.4">cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.8">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="15.2">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="16.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="16.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">— <w n="17.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>, <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w>, <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="18" num="5.2"><w n="18.1">P<seg phoneme="i" type="vs" value="1" rule="468">i</seg>cs</w> <w n="18.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="18.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="18.5">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					<l n="19" num="5.3"><w n="19.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.2">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="19.6"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="19.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> ! <w n="19.8">V<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.10">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="20" num="5.4"><w n="20.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="20.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="20.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="20.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.7">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">— <w n="21.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>, <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>, <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="22" num="6.2"><w n="22.1">F<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="22.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w>, <w n="22.3">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="22.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.5">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.6">d</w>’<w n="22.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
					<l n="23" num="6.3"><w n="23.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.2">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="23.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="23.7">r<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="23.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="24" num="6.4"><w n="24.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="24.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="24.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="24.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
				</lg>
			</div></body></text></TEI>