<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER91">
				<head type="number">XCI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.8">tr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.5">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="2.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.4">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="3.6">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.7">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.10">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ;</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="6.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.5">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.9">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w></l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.8">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="8" num="2.1"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.4">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="9" num="2.2"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="9.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.5">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.9">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.10">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.11">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="10" num="2.3"><w n="10.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="10.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w></l>
					<l n="11" num="2.4"><w n="11.1">C<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.8">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="12" num="3.1"><w n="12.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.2">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="13" num="3.2"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.10">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
					<l n="14" num="3.3"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.7">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.8">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
					<l n="15" num="3.4"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="15.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="15.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="15.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.8">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="16" num="3.5"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="16.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.9">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="17" num="3.6"><w n="17.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="17.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w></l>
				</lg>
				<lg n="4">
					<l n="18" num="4.1"><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="18.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.3">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="18.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.7">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="18.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w></l>
					<l n="19" num="4.2"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="19.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.6">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="20" num="4.3"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="20.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">l</w>’<w n="20.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="20.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.9">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.3">n</w>’<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.8">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="21.9">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.11">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.12">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w>,</l>
					<l n="22" num="5.2"><w n="22.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="22.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="22.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rge<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="22.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="22.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.9">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="23" num="5.3"><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.3">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.8">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="24" num="5.4"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="24.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.8">s</w>’<w n="24.9"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="25" num="5.5"><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="25.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.3">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rgn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ; <w n="25.4">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="25.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.8">m<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> ;</l>
					<l n="26" num="5.6"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="26.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="26.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.7">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w></l>
					<l n="27" num="5.7"><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="27.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="27.6">d</w>’<w n="27.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="27.8">gr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="6">
					<l n="28" num="6.1"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="28.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="28.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="28.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="28.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>