<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER21">
				<head type="number">XXI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.2">l</w>’<w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="1.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.6">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w>, <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
					<l n="4" num="1.4"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>.</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="5.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="5.3">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">H<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dj<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="497">y</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="7.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">f<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="8.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="8.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="445">û</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="8.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
					<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="10.2">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>-<subst hand="RR" reason="analysis" type="phonemization"><del>18</del><add rend="hidden"><w n="10.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="10.5">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></add></subst> <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
				</lg>
			</div></body></text></TEI>