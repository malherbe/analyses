<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR7">
						<head type="main">Le Médaillon d’Iseult</head>
						<opener>
							<salute>À JEHAN DUSEIGNEUR, statuaire.</salute>
							<epigraph>
								<cit>
									<quote>
										L’amour chaste agrandit les âmes.
									</quote>
									<bibl>
										<name>HUGO</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Br<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.3">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.4">d</w>’<w n="1.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1">V<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="2.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="2.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="3.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>lt</w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="4.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="4.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.5">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="5.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="5.3">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
						</lg>
						<lg n="2">
							<l n="6" num="2.1"><w n="6.1">V<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.7">j</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="7" num="2.2"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gt</w> <w n="7.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">n</w>’<w n="7.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
							<l n="8" num="2.3"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="8.4">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">l</w>’<w n="8.7"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
							<l n="9" num="2.4"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="10" num="2.5"><space unit="char" quantity="4"></space><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.3">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="10.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.5">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="11" num="3.1"><w n="11.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.8">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="12" num="3.2"><w n="12.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="12.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="13" num="3.3"><w n="13.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="14" num="3.4"><w n="14.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="15" num="3.5"><space unit="char" quantity="4"></space><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="15.6">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="16" num="4.1"><w n="16.1">J<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.2">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.3">n</w>’<w n="16.4"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>t</w> <w n="16.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="16.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.7">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="17" num="4.2"><w n="17.1">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
							<l n="18" num="4.3"><w n="18.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="18.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.6">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
							<l n="19" num="4.4"><w n="19.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="19.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="19.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="19.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct</w> <w n="19.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="20" num="4.5"><space unit="char" quantity="4"></space><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="20.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.4">l</w>’<w n="20.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						</lg>
						<lg n="5">
							<l n="21" num="5.1"><w n="21.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="21.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ! <w n="21.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.6"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="21.7">s</w>’<w n="21.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="22" num="5.2"><w n="22.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="22.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="22.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>, <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="22.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="22.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> !</l>
							<l n="23" num="5.3"><w n="23.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="23.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="23.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.7">t</w>’<w n="23.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.9">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>,</l>
							<l n="24" num="5.4"><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="24.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>, <w n="24.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="24.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.7">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="25" num="5.5"><space unit="char" quantity="4"></space><w n="25.1">Qu</w>’<w n="25.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="25.3">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>.</l>
						</lg>
						<lg n="6">
							<l n="26" num="6.1"><w n="26.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="26.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="26.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.6">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="27" num="6.2"><w n="27.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="27.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ! <w n="27.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="27.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="27.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="28" num="6.3"><w n="28.1">C</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="28.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="28.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="29" num="6.4"><w n="29.1">C</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="29.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="29.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="29.6">s<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.7">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="30" num="6.5"><space unit="char" quantity="4"></space><w n="30.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="30.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="30.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
						</lg>
						<lg n="7">
							<l n="31" num="7.1"><w n="31.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.2">t</w>’<w n="31.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ? <w n="31.5">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w>, <w n="31.6">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.7">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="32" num="7.2"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="32.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="32.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="32.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.6">l</w>’<w n="32.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ?… — <w n="32.9">c</w>’<w n="32.10"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="32.11">Jeh<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> !</l>
							<l n="33" num="7.3"><w n="33.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="33.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="33.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="33.5">l</w>’<w n="33.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="33.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="34" num="7.4"><w n="34.1">S<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="34.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="34.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="34.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.5">l</w>’<w n="34.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="35" num="7.5"><w n="35.1">H<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="35.2">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lv<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="35.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sm<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ld<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="35.4">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>,</l>
							<l n="36" num="7.6"><space unit="char" quantity="4"></space><w n="36.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="36.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="36.3">d</w>’<w n="36.4">H<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="36.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="36.6">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>