<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Friperies</title>
				<title type="medium">Une édition électronique</title>
				<author key="FLE">
					<name>
						<forename>Fernand</forename>
						<surname>FLEURET</surname>
					</name>
					<date from="1883" to="1945">1883-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>455 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">FLE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Friperies</title>
						<author>Fernand Fleuret</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/friperies00fleu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Friperies</title>
								<author>Fernand Fleuret</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>CHEZ EUGÈNE REY, LIBRAIRE</publisher>
									<date when="1907">1907</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			</fileDesc>
		<profileDesc>
			<creation>
				<date when="1907">1907</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2019-08-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2019-08-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FLE9">
				<head type="main">FIANÇAILLES</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								« J’aimerais que la rose<lb></lb>
								Tût encore au rosier !… » 
							</quote>
							<bibl>
								(Vieille chanson.)
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="1.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="1.5">J</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="1.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>… <w n="1.9">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.10">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.11">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.7"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="3" num="1.3"><w n="3.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.2">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">P<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="3.6">qu</w>’<w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.8">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <hi rend="ital"><w n="3.9">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></hi>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.3">m</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>chs</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>bl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="6.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.4">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>, <w n="7.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.7">L<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="3.2"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.3">k<seg phoneme="i" type="vs" value="1" rule="125">ee</seg>ps<seg phoneme="a" type="vs" value="1" rule="340">a</seg>k<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="8.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> : <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>nn<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l n="9" num="3.3"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg>s</w>, <w n="9.2"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="9.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.5"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
					<l n="10" num="3.4"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> ? <w n="10.6">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
					<l n="11" num="3.5"><w n="11.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="11.8">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="12" num="3.6"><w n="12.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="12.3">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="12.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
					<l n="13" num="3.7"><w n="13.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="13.7">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="13.8">d</w>’<w n="13.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="14" num="3.8"><w n="14.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="14.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">s</w>’<w n="14.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="15" num="3.9"><w n="15.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="15.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.4">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="15.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.6">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rf<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="16" num="3.10"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.4">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.9">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> !</l>
					<l n="17" num="3.11"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.3">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="17.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="17.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.9">qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="18" num="3.12"><w n="18.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="18.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="18.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.9">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="19" num="3.13"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">l</w>’<w n="19.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="19.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="19.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="19.9">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="20" num="3.14"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="20.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="20.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="20.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.8">s</w>’<w n="20.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="21" num="3.15"><w n="21.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="21.2">J<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.7">gl<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="j" type="sc" value="0" rule="475">ï</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w></l>
					<l n="22" num="3.16"><w n="22.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="22.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="22.5">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w> <w n="22.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.7">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.8">d</w>’<w n="22.9"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l n="23" num="3.17"><w n="23.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="23.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.9"><seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="j" type="sc" value="0" rule="475">ï</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="24" num="3.18"><hi rend="ital"><w n="24.1">J</w>’<w n="24.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="24.7">f<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t</w> <w n="24.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="24.9">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>…</hi></l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">J</w>’<w n="25.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="25.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.4">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="25.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.6">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="25.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="25.8">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.10">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="26" num="4.2"><w n="26.1">J</w>’<w n="26.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="26.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4"><seg phoneme="j" type="sc" value="0" rule="496">Y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="26.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="26.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.8">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="26.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="27" num="4.3"><w n="27.1">J</w>’<w n="27.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="27.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="27.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="27.8">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="27.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.10">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.11">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> :</l>
				</lg>
				<lg n="5">
					<l n="28" num="5.1"><w n="28.1">J</w>’<w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="28.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.4">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="28.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.6"><seg phoneme="j" type="sc" value="0" rule="496">Y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="28.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.8">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>, <w n="28.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.10">Cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>..</l>
				</lg>
			</div></body></text></TEI>