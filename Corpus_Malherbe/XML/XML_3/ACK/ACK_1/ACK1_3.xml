<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">PREMIÈRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ACK">
					<name>
						<forename>Louise-Victorine</forename>
						<surname>ACKERMANN</surname>
					</name>
					<date from="1813" to="1890">1813-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>588 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ACK_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies Diverses</title>
						<author>Louise-Victorine Ackermann</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">http://www.poesies.net/louiseackermaNpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de L. Ackermann</title>
						<author>Louise-Victorine Ackermann</author>
						<imprint>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1862">1862</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ACK1">
				<head type="number">I</head>
				<head type="main">Adieux à la Poésie</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="1.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.6">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="1.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.5">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.3"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">j</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ç<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.7">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="6.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ;</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="2.4"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="wa" type="vs" value="1" rule="257">eoi</seg>r</w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="8.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.6">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.5">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
					<l n="11" num="3.3"><w n="11.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.4">qu</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Paris</placeName>,
						<date when="1835">1835</date>
						</dateline>
				</closer>
			</div></body></text></TEI>