<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre puéril</head><head type="main_subpart">III</head><head type="main_subpart">L’Inspiration et la Pensée</head><head type="sub_1">… et le rêve se mêle aux pensées qu’il caresse</head><div type="poem" key="HEU5">
						<head type="main">LA MUSE JUVÉNILE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="2.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.8">fl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.10">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="3.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="3.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct</w> <w n="3.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">fr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="4.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.8">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.10">cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.11">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
							<l n="5" num="1.5"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="5.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">m</w>’<w n="5.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.11">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="6" num="1.6"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w>, <w n="6.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.11">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="7" num="1.7"><w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="7.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="7.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w>, <w n="7.6">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>, <w n="7.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
							<l n="8" num="1.8"><w n="8.1">Br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.3">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="8.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>cr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="8.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !</l>
							<l n="9" num="1.9"><w n="9.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="9.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?…</l>
							<l n="10" num="1.10"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="10.6">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="10.7">fr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.10">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
							<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="11.9">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w></l>
							<l n="12" num="1.12"><w n="12.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="12.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="290">aon</seg></w>, <w n="12.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>.</l>
							<l n="13" num="1.13"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">t</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="13.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.8">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="14" num="1.14"><w n="14.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.7">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="15" num="1.15"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="15.5">s</w>’<w n="15.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.8">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>,</l>
							<l n="16" num="1.16"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="16.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.10">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.11"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.12">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.13">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>…</l>
							<l n="17" num="1.17"><w n="17.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.2">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="17.3">n</w>’<w n="17.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="17.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
							<l n="18" num="1.18"><w n="18.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ds</w> <w n="18.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="18.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="18.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="19" num="1.19"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.2">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="19.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="19.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="19.7">d</w>’<w n="19.8"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
							<l n="20" num="1.20"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.4">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.8"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
							<l n="21" num="1.21"><w n="21.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="21.2">qu</w>’<w n="21.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.4">tr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.9">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="22" num="1.22"><w n="22.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="22.3">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="22.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="22.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="22.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="22.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						</lg>
					</div></body></text></TEI>