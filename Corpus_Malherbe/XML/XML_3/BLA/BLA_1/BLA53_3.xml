<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">POÈMES ET POÉSIES</head><div type="poem" key="BLA53">
					<head type="main">DÉDICACE DE LA CHANSON D’AUTREFOIS</head>
					<opener>
						<salute>Au docteur Max. Durand-Fardel.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><hi rend="ital"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="1.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.8">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</hi></l>
						<l n="2" num="1.2"><hi rend="ital"><w n="2.1">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.6">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.10">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</hi></l>
						<l n="3" num="1.3"><hi rend="ital"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">V</w> <w n="3.9"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</hi></l>
						<l n="4" num="1.4"><hi rend="ital"><w n="4.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="4.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="4.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</hi></l>
						<l n="5" num="1.5"><hi rend="ital"><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.5">n<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="5.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</hi></l>
						<l n="6" num="1.6"><hi rend="ital"><w n="6.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.5">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</hi></l>
						<l n="7" num="1.7"><hi rend="ital"><w n="7.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="7.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.7"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="7.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">l</w>’<w n="7.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.11">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.12">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</hi></l>
						<l n="8" num="1.8"><hi rend="ital"><w n="8.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</hi></l>
						<l n="9" num="1.9"><hi rend="ital"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ils</w> <w n="9.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.5">s</w>’<w n="9.6"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></hi></l>
						<l n="10" num="1.10"><hi rend="ital"><w n="10.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> :</hi></l>
						<l n="11" num="1.11"><hi rend="ital"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="11.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">qu</w>’<w n="11.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="11.8">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.10">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</hi></l>
						<l n="12" num="1.12"><hi rend="ital"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="12.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="12.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</hi></l>
						<l n="13" num="1.13"><hi rend="ital"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="13.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="13.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="13.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="13.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="13.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></hi></l>
						<l n="14" num="1.14"><hi rend="ital"><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="14.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</hi></l>
						<l n="15" num="1.15"><hi rend="ital"><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">l</w>’<w n="15.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.4">d</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">d</w>’<w n="15.9">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</hi></l>
						<l n="16" num="1.16"><hi rend="ital"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.8">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</hi></l>
						<l n="17" num="1.17"><hi rend="ital"><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="17.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="17.3">l</w>’<w n="17.4"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="17.5">V</w> <w n="17.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.9">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="17.10">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>,</hi></l>
						<l n="18" num="1.18"><hi rend="ital"><w n="18.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="18.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="18.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>. <w n="18.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="18.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="18.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.10">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>.</hi></l>
						<l n="19" num="1.19"><hi rend="ital"><w n="19.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="19.4">m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="19.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</hi></l>
						<l n="20" num="1.20"><hi rend="ital"><w n="20.1">C</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.4">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.6">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>… <w n="20.7">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="20.8">qu</w>’<w n="20.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.10"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.11">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</hi></l>
						<l n="21" num="1.21"><hi rend="ital"><w n="21.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="21.4">s</w>’<w n="21.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="21.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w></hi></l>
						<l n="22" num="1.22"><hi rend="ital"><w n="22.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.3">l</w>’<w n="22.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.5">L<seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="22.6">CH<seg phoneme="ɑ̃" type="vs" value="1" rule="313">AN</seg>S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">ON</seg></w> <w n="22.7">D</w>’<w n="22.8"><seg phoneme="o" type="vs" value="1" rule="318">AU</seg>TR<seg phoneme="ə" type="em" value="1" rule="e-19">E</seg>F<seg phoneme="wa" type="vs" value="1" rule="420">OI</seg>S</w>.</hi></l>
					</lg>
					<closer>
						<dateline>
						<placeName>Vichy</placeName>,
							<date when="1871">août 1871.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>