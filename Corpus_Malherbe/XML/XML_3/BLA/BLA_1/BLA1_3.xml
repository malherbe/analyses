<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="BLA1">
				<head type="main"><hi rend="ital">A MARIE DÉSIRÉE</hi></head>
				<head type="sub_1"><hi rend="ital">OFFRANDE</hi></head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><hi rend="ital"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="1.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</hi></l>
					<l n="2" num="1.2"><hi rend="ital"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="2.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="2.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="2.8">s</w>’<w n="2.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</hi></l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><hi rend="ital"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> ;</hi></l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><hi rend="ital"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="4.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="4.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.5">j</w>’<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.7">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>. <w n="4.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">j</w>’<w n="4.10"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</hi></l>
					<l n="5" num="1.5"><hi rend="ital"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.5">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">Cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</hi></l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><hi rend="ital"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="6.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="6.3">l</w>’<w n="6.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> !</hi></l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><space unit="char" quantity="8"></space><hi rend="ital"><w n="7.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>. <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.3">j</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="7.7">d</w>’<w n="7.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.9">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</hi></l>
					<l n="8" num="2.2"><hi rend="ital"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="8.3">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="8.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</hi></l>
					<l n="9" num="2.3"><space unit="char" quantity="8"></space><hi rend="ital"><w n="9.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.2">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="9.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w> <w n="9.6">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> ;</hi></l>
					<l n="10" num="2.4"><space unit="char" quantity="8"></space><hi rend="ital"><w n="10.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</hi></l>
					<l n="11" num="2.5"><hi rend="ital"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="11.3">l</w>’<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="11.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="11.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</hi></l>
					<l n="12" num="2.6"><space unit="char" quantity="8"></space><hi rend="ital"><w n="12.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="12.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>.</hi></l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><space unit="char" quantity="8"></space><hi rend="ital"><w n="13.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="13.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="13.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</hi></l>
					<l n="14" num="3.2"><hi rend="ital"><w n="14.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">d</w>’<w n="14.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</hi></l>
					<l n="15" num="3.3"><space unit="char" quantity="8"></space><hi rend="ital"><w n="15.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="15.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</hi></l>
					<l n="16" num="3.4"><space unit="char" quantity="8"></space><hi rend="ital"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="16.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="16.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="16.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</hi></l>
					<l n="17" num="3.5"><hi rend="ital"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="17.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="17.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="17.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="17.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.8">t</w>’<w n="17.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.10">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</hi></l>
					<l n="18" num="3.6"><space unit="char" quantity="8"></space><hi rend="ital"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="18.3">qu</w>’<w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="18.6">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.8">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> !</hi></l>
				</lg>
			</div></body></text></TEI>