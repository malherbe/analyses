<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub_2">TOME PREMIER</title>
				<title type="medium">Édition électronique</title>
				<author key="BLA">
					<name>
						<forename>Prosper</forename>
						<surname>BLANCHEMAIN</surname>
					</name>
					<date from="1816" to="1879">1816-1879</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>7196 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">BLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME PREMIER</title>
						<author>Prosper Blanchemain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/pomesetposie01blan</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
								<title>TOME PREMIER</title>
								<author>Prosper Blanchemain</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE ANCIENNE ET MODERNE - ÉDOUARD ROUVEYRE</publisher>
									<date when="1880">1880</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME I</title>
						<title>POÈMES ET POÉSIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES DE PROSPER BLANCHEMAIN</title>
						<title>TOME V</title>
						<title>SONNETS ET FANTAISIES</title>
						<author>Prosper Blanchemain</author>
						<idno type="URL">https://archive.org/details/posies01blan</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>AUGUSTE AUBRY</publisher>
							<date when="1866">1866</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1845" to="1858">1845-1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDÉAL</head><div type="poem" key="BLA78">
					<head type="main">L’ÉTOILE DU SOIR</head>
					<head type="form">SONNET</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="1.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>, <w n="2.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.5">s</w>’<w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.2">l</w>’<w n="3.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="4.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						<l n="5" num="1.5"><space quantity="4" unit="char"></space><w n="5.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.6">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="5.7">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">T<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">qu</w>’<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.4">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						<l n="9" num="1.9"><space quantity="4" unit="char"></space><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.2">c</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.4">l</w>’<w n="9.5">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.6"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="9.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="9.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="1.10"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="10.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="10.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="1.11"><w n="11.1">T<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="11.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
						<l n="12" num="1.12"><space quantity="4" unit="char"></space><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="12.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="1.13"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.5"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="13.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="1.14"><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="14.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>