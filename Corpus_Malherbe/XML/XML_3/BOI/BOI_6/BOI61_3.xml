<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI61">
				<head type="main">ODE</head>
				<head type="sub">sur un bruit qui courut en que Cromnwell <lb></lb>et les Anglais allaient faire <lb></lb>la guerre à la France</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>gl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.3">tr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="4" num="1.4"><w n="4.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="4.2">t</w>-<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="4.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="5" num="1.5"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.4">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">N</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.5">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.7">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.8">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ?</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="7.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.2"><w n="8.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="9" num="2.3"><w n="9.1">V<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="9.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
					<l n="10" num="2.4"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="ø" type="vs" value="1" rule="405">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="2.5"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="11.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="2.6"><w n="12.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.3">l</w>’<w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="13.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="13.3">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ; <w n="13.4">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="14" num="3.2"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="15" num="3.3"><w n="15.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="170">e</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="15.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.5">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> ;</l>
					<l n="16" num="3.4"><w n="16.1">S<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.5">t</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="17" num="3.5"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="17.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="3.6"><w n="18.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.3">qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="19.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="19.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="20" num="4.2"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="308">Ai</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="20.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rf<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="21" num="4.3"><w n="21.1">Ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="21.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="21.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="21.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">l</w>’<w n="21.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="22.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="23" num="4.5"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">g<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w> <w n="23.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="24" num="4.6"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.4">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.6">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="25.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w>, <w n="25.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="25.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="26" num="5.2"><w n="26.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="26.4">d</w>’<w n="26.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="26.6">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.7">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="27" num="5.3"><w n="27.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="27.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="28" num="5.4"><w n="28.1">B<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="28.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="28.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="28.6">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="29" num="5.5"><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="29.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="29.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w>, <w n="29.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="29.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="29.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="29.7">pl<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="30" num="5.6"><w n="30.1">N</w>’<w n="30.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="30.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="30.4">qu</w>’<w n="30.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="30.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="30.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>