<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">APPENDICE</title>
				<title type="sub_2">À L’ÉDITION DES ŒUVRES POÉTIQUES (édition Hachette, 1889)</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>125 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1654" to="1705">1654-1705</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI98">
				<head type="number">XIII</head>
				<head type="main">Épitaphe de M. Arnauld, docteur de Sorbonne</head>
				<opener>
					<dateline>
						<date when="1695">(1695)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="1.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">str<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">gr<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">G<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t</w> <w n="2.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.3">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">b<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.6">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> :</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rn<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ld</w>, <w n="4.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>str<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="4.8">J<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>-<w n="4.9">Chr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st</w>,</l>
					<l n="5" num="1.5"><w n="5.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.3">l</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="5.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.7">l</w>’<w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="6.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.3">d</w>’<w n="6.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.8">d</w>’<w n="6.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.10"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="7" num="1.7"><w n="7.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.3">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="7.4">qu</w>’<w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="7.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.9">l</w>’<w n="7.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.11">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="8.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.5">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lv<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.6">z<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="10.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.8">l</w>’<w n="10.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.10">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.11">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="11" num="1.11"><w n="11.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="11.2">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="11.3">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>ppr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="1.12"><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="12.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="12.3">b<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="12.4">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>scr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="12.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="13" num="1.13"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="13.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="13.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="13.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.7">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="1.14"><w n="14.1">N</w>’<w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>,</l>
					<l n="15" num="1.15"><w n="15.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.2">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="15.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="15.4">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="15.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="15.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.8"><seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.9">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="1.16"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="16.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="16.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="16.5">n</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.9"><seg phoneme="o" type="vs" value="1" rule="432">o</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>