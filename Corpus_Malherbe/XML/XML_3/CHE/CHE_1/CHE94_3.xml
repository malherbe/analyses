<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHE94">
					<head type="number">VII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rg<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="1.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="2.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.9">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="3" num="1.3"><w n="3.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">S<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.5">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>, <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.8">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="3.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>, <w n="4.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="4.3">qu</w>’<w n="4.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.6">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">M<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="1.6"><w n="6.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>

				</div></body></text></TEI>