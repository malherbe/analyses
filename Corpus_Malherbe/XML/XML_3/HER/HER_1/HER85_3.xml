<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><head type="main_subpart">Les conquérants</head><div type="poem" key="HER85">
						<head type="main">Au Même</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Qu</w>’<w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="1.7">l</w>’<w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>zt<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.10">H<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>,</l>
							<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w>, <w n="2.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.7"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.3">n</w>’<w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.9">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.10">pr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w>, <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.5">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.9"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="4.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.11">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="5.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="5.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="5.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>,</l>
							<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="6.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rth<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.3">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gd<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.6">D<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="7.7">qu</w>’<w n="7.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>, <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="8.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.8">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="8.9">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="8.10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.4"><seg phoneme="i" type="vs" value="1" rule="468">î</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="9.6">l</w>’<w n="9.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="9.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="10" num="3.2"><w n="10.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="10.4">l</w>’<w n="10.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.8">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.11">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>,</l>
							<l n="11" num="3.3"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.2">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.3">dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="11.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.10">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> ;</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="12.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>, <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>fl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.8">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="13" num="4.2"><w n="13.1">T<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>-<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="13.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.5">d</w>’<w n="13.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w></l>
							<l n="14" num="4.3"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="14.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">d</w>’<w n="14.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
						</lg>
					</div></body></text></TEI>