<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉTOILES FILANTES</head><div type="poem" key="GIL36">
					<head type="main">Premier Amour III</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="1.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.4">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="1.7">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="1.8">d</w>’<w n="1.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> !</l>
						<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="3.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>scr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgn<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">br<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.4">l</w>’<w n="5.5">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w></l>
						<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="6.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.8">br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="7.8">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
						<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w> <w n="8.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.9">g<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="8.10">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">F<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="9.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="9.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="10" num="3.2"><w n="10.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="11.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="11.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="11.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="12.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.6">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="13.2">sc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="13.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="13.6">d</w>’<w n="13.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.8">d</w>’<w n="13.9"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
						<l n="14" num="4.3"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="14.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.6">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="14.7">L<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="14.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.9">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					</lg>
				</div></body></text></TEI>