<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FEMMES RÊVÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>234 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Femmes Rêvées</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/12365</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Femmes Rêvées</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://archive.org/details/femmesrves00ferl</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Chez l’auteur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1899">1899</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FEMMES RÊVÉES</head><div type="poem" key="FER50">
					<head type="main">Les Bois</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="1.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.4">qu</w>’<w n="1.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="1.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.9">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="1.10">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="2.7">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="493">y</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>-<w n="5.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="5.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="5.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="6.2">qu</w>’<w n="6.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="6.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">m<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.7">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.5">s</w>’<w n="7.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="12"></space><w n="8.1">L</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="9.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="3.2"><w n="10.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>, <w n="10.2">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="10.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="10.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="11" num="3.3"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.9">v<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="12"></space><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.3">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="13.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="13.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="13.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.7">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="13.8">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="14" num="4.2"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="14.4">d</w>’<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.9">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="15" num="4.3"><w n="15.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>, <w n="15.2"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="15.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="15.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="15.9">ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="12"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="16.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
					</lg>
				</div></body></text></TEI>