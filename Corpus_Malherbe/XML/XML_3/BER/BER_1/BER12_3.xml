<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER12">
				<head type="main">LE PETIT HOMME GRIS</head>
				<head type="tune">AIR : Toto, Carabo</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.4">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="14"></space><w n="3.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">J<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">p<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="5.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="5.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="14"></space><w n="6.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="10"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="7.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="7.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">m</w>’<w n="7.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="8" num="1.8"><space unit="char" quantity="10"></space><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="8.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="8.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">m</w>’<w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.2">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">m</w>’<w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="10.2">Qu</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.5">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="10.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.9">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><space unit="char" quantity="8"></space><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="11.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="12" num="2.2"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="12.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="13" num="2.3"><space unit="char" quantity="14"></space><w n="13.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="13.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="14" num="2.4"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="14.2">s</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="15" num="2.5"><space unit="char" quantity="8"></space><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="15.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="15.4">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="16" num="2.6"><space unit="char" quantity="14"></space><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="16.2">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
					<l n="17" num="2.7"><space unit="char" quantity="10"></space><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="17.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="17.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="17.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">m</w>’<w n="17.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="18" num="2.8"><space unit="char" quantity="10"></space><w n="18.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="18.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="18.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="18.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">m</w>’<w n="18.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="19" num="2.9"><space unit="char" quantity="8"></space><w n="19.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.2">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="19.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="19.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">m</w>’<w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
					<l n="20" num="2.10"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="20.2">Qu</w>’<w n="20.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.5">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="20.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="20.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.9">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1"><space unit="char" quantity="8"></space><w n="21.1">Qu</w>’<w n="21.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="21.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="22" num="3.2"><space unit="char" quantity="8"></space><w n="22.1">Qu</w>’<w n="22.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="22.3">s</w>’<w n="22.4"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="22.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
					<l n="23" num="3.3"><space unit="char" quantity="14"></space><w n="23.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="23.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ;</l>
					<l n="24" num="3.4"><space unit="char" quantity="8"></space><w n="24.1">Qu</w>’<w n="24.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="24.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.4">f<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="24.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="25" num="3.5"><space unit="char" quantity="8"></space><w n="25.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="25.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="26" num="3.6"><space unit="char" quantity="14"></space><w n="26.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.3">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w>,</l>
					<l n="27" num="3.7"><space unit="char" quantity="10"></space><w n="27.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="27.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="27.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="27.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.5">m</w>’<w n="27.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="28" num="3.8"><space unit="char" quantity="10"></space><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="28.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="28.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="28.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">m</w>’<w n="28.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="29" num="3.9"><space unit="char" quantity="8"></space><w n="29.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.2">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="29.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="29.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.5">m</w>’<w n="29.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="29.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
					<l n="30" num="3.10"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="30.2">Qu</w>’<w n="30.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="30.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="30.5">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="30.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="30.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.9">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="4">
					<l n="31" num="4.1"><space unit="char" quantity="8"></space><w n="31.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.2">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="31.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="31.4">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="32" num="4.2"><space unit="char" quantity="8"></space><w n="32.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="32.2">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
					<l n="33" num="4.3"><space unit="char" quantity="14"></space><w n="33.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="33.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
					<l n="34" num="4.4"><space unit="char" quantity="8"></space><w n="34.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="34.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="34.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="35" num="4.5"><space unit="char" quantity="8"></space><w n="35.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="35.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="35.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="35.6">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gt</w>.</l>
					<l n="36" num="4.6"><space unit="char" quantity="14"></space><w n="36.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="36.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>,</l>
					<l n="37" num="4.7"><space unit="char" quantity="10"></space><w n="37.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="37.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="37.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="37.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.5">m</w>’<w n="37.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="38" num="4.8"><space unit="char" quantity="10"></space><w n="38.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="38.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : <w n="38.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="38.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.5">m</w>’<w n="38.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="39" num="4.9"><space unit="char" quantity="8"></space><w n="39.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.2">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="39.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="39.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.5">m</w>’<w n="39.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="39.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
					<l n="40" num="4.10"><w n="40.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="40.2">Qu</w>’<w n="40.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="40.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="40.5">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="40.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="40.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="40.9">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="5">
					<l n="41" num="5.1"><space unit="char" quantity="8"></space><w n="41.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="41.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="41.3">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="41.4">l</w>’<w n="41.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="42" num="5.2"><space unit="char" quantity="8"></space><w n="42.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="42.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="42.3">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="42.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="43" num="5.3"><space unit="char" quantity="14"></space><w n="43.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.2">c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="44" num="5.4"><space unit="char" quantity="8"></space><w n="44.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="44.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="44.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="44.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="44.6">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="45" num="5.5"><space unit="char" quantity="8"></space><w n="45.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="45.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="45.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.4">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>,</l>
					<l n="46" num="5.6"><space unit="char" quantity="14"></space><w n="46.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="46.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> :</l>
					<l n="47" num="5.7"><space unit="char" quantity="10"></space><w n="47.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="47.2">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="47.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="47.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.5">m</w>’<w n="47.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="48" num="5.8"><space unit="char" quantity="10"></space><w n="48.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="48.2">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="48.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="48.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="48.5">m</w>’<w n="48.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>…</l>
					<l n="49" num="5.9"><space unit="char" quantity="8"></space><w n="49.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="49.2">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="49.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="49.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="49.5">m</w>’<w n="49.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="49.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
					<l n="50" num="5.10"><w n="50.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="50.2">Qu</w>’<w n="50.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="50.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="50.5">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="50.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="50.8">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="50.9">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
			</div></body></text></TEI>