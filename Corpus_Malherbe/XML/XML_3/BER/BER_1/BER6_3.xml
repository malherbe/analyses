<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER6">
				<head type="main">ROGER BONTEMPS</head>
				<opener>
					<dateline>
						<date when="1814">1814</date>
					</dateline>
				</opener>
				<head type="tune">AIR : Ronde du camp de Grandpré</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="1.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.2">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.4">n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> ;</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="7.2">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ! <w n="7.3">C</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.2">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="8.3">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.4">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.5">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="2.2"><w n="10.1">C<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="10.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
					<l n="11" num="2.3"><w n="11.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="2.4"><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>j<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="12.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
					<l n="13" num="2.5"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.3">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="2.6"><w n="14.1">V<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> ;</l>
					<l n="15" num="2.7"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="15.2">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ! <w n="15.3">C</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="2.8"><w n="16.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.2">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="16.3">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.4">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">P<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="17.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="3.2"><w n="18.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="18.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.4">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
					<l n="19" num="3.3"><w n="19.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="19.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">fl<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="3.4"><w n="20.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="20.2">br<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w> <w n="20.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="20.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
					<l n="21" num="3.5"><w n="21.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="21.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="3.6"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="22.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> ;</l>
					<l n="23" num="3.7"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="23.2">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ! <w n="23.3">C</w>’<w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="3.8"><w n="24.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.2">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="24.3">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.4">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="25.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="25.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="26" num="4.2"><w n="26.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="26.4">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
					<l n="27" num="4.3"><w n="27.1"><seg phoneme="ɛ" type="vs" value="1" rule="412">Ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.3">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="27.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="28" num="4.4"><w n="28.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.3">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
					<l n="29" num="4.5"><w n="29.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="29.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="30" num="4.6"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">d</w>’<w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>chs</w> <w n="30.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ;</l>
					<l n="31" num="4.7"><w n="31.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="31.2">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ! <w n="31.3">C</w>’<w n="31.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="31.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.6">sc<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="32" num="4.8"><w n="32.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.2">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="32.3">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.4">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">F<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="33.4">d</w>’<w n="33.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="34" num="5.2"><w n="34.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="34.2">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="34.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="34.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
					<l n="35" num="5.3"><w n="35.1">Pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="35.2">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="36" num="5.4"><w n="36.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="36.2">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="36.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="36.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="36.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
					<l n="37" num="5.5"><w n="37.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.2">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="37.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="37.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="38" num="5.6"><w n="38.1">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="38.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="38.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="38.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ;</l>
					<l n="39" num="5.7"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="39.2">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ! <w n="39.3">C</w>’<w n="39.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="39.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="40" num="5.8"><w n="40.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="40.2">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="40.3">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="40.4">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>.</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="41.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="41.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> : <w n="41.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.6">f<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="42" num="6.2"><w n="42.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="42.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="42.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="42.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="42.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="43" num="6.3"><w n="43.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.3">ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="44" num="6.4"><w n="44.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="44.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="44.3">g<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="45" num="6.5"><w n="45.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="45.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="45.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="45.4">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="46" num="6.6"><w n="46.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="46.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="46.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="46.4">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> ;</l>
					<l n="47" num="6.7"><w n="47.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="47.2">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ! <w n="47.3">C</w>’<w n="47.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="47.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="47.6">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="48" num="6.8"><w n="48.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="48.2">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="48.3">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="48.4">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>.</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1"><w n="49.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="49.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="49.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="49.4">d</w>’<w n="49.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="50" num="7.2"><w n="50.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="50.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="50.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="51" num="7.3"><w n="51.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="51.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="51.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="51.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="52" num="7.4"><w n="52.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="52.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="52.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="52.4">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
					<l n="53" num="7.5"><w n="53.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="53.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="53.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rdr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="53.4">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="53.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="54" num="7.6"><w n="54.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="54.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="54.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="55" num="7.7"><w n="55.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="55.2">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ! <w n="55.3">Pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="55.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="55.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="56" num="7.8"><w n="56.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="56.2">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="56.3">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="56.4">B<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>.</l>
				</lg>
			</div></body></text></TEI>