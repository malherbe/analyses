<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER40">
				<head type="main">LES BOXEURS</head>
				<head type="sub_2">OU</head>
				<head type="sub_1"> L’ANGLOMANE</head>
				<opener>
					<dateline>
						<date when="1814">août 1814</date>
					</dateline>
				</opener>
				<head type="tune">AIR : A coups d’pied, à coups de poing</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="1.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="1.4">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="1.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="1.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ds</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">G<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d</w> <w n="2.2">d<seg phoneme="a" type="vs" value="1" rule="145">a</seg>m</w> ! <w n="2.3">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="2.4">j</w>’<w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> :</l>
					<l n="3" num="1.3"><space unit="char" quantity="2"></space><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="3.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="4.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.4">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ! <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>-<w n="4.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w></l>
					<l n="5" num="1.5"><space unit="char" quantity="2"></space><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.7">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> !</l>
					<l n="6" num="1.6"><space unit="char" quantity="10"></space><w n="6.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="6.2">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="6.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="6.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="6"></space><w n="7.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>g</w></l>
					<l n="8" num="1.8"><w n="8.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.4">d</w>’<w n="8.5">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.7">l</w>’<w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="2"></space><w n="9.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>x<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.5">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> :</l>
					<l n="10" num="2.2"><space unit="char" quantity="2"></space><w n="10.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="10.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="11" num="2.3"><space unit="char" quantity="2"></space><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>-<w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.5">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="12" num="2.4"><space unit="char" quantity="2"></space><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="12.2">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> ;</l>
					<l n="13" num="2.5"><space unit="char" quantity="2"></space><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.4">c</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.7">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>.</l>
					<l n="14" num="2.6"><space unit="char" quantity="10"></space><w n="14.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="14.2">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="14.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="14.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>,</l>
					<l n="15" num="2.7"><space unit="char" quantity="6"></space><w n="15.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>g</w></l>
					<l n="16" num="2.8"><w n="16.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.4">d</w>’<w n="16.5">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.7">l</w>’<w n="16.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space unit="char" quantity="2"></space><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="17.2">sc<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">d</w>’<w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="18" num="3.2"><space unit="char" quantity="2"></space><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.2">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.6">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="19" num="3.3"><space unit="char" quantity="2"></space><w n="19.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">s</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="20" num="3.4"><space unit="char" quantity="2"></space><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="20.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.6">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w> :</l>
					<l n="21" num="3.5"><space unit="char" quantity="2"></space><w n="21.1">P<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w>.</l>
					<l n="22" num="3.6"><space unit="char" quantity="10"></space><w n="22.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="22.2">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="22.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="22.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>,</l>
					<l n="23" num="3.7"><space unit="char" quantity="6"></space><w n="23.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>g</w></l>
					<l n="24" num="3.8"><w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.4">d</w>’<w n="24.5">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="24.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="24.7">l</w>’<w n="24.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="2"></space><w n="25.1">Ç<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="25.2">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sd<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="25.3">qu</w>’<w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="25.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ?</l>
					<l n="26" num="4.2"><space unit="char" quantity="2"></space><w n="26.1">C</w>’<w n="26.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="26.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w>.</l>
					<l n="27" num="4.3"><space unit="char" quantity="2"></space><w n="27.1">Qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ! <w n="27.2">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="28" num="4.4"><space unit="char" quantity="2"></space><w n="28.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="28.3">j<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>… <w n="28.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="28.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>.</l>
					<l n="29" num="4.5"><space unit="char" quantity="2"></space><w n="29.1">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ! <w n="29.2">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="29.6">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> !</l>
					<l n="30" num="4.6"><space unit="char" quantity="10"></space><w n="30.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="30.2">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="30.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="30.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>,</l>
					<l n="31" num="4.7"><space unit="char" quantity="6"></space><w n="31.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="31.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="31.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>g</w></l>
					<l n="32" num="4.8"><w n="32.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="32.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="32.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="32.4">d</w>’<w n="32.5">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="32.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="32.7">l</w>’<w n="32.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><space unit="char" quantity="2"></space><w n="33.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> ! <w n="33.2"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="33.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="33.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="33.5">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="33.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="33.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
					<l n="34" num="5.2"><space unit="char" quantity="2"></space><w n="34.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="34.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.3">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="34.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.5">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="34.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="34.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.8">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w>,</l>
					<l n="35" num="5.3"><space unit="char" quantity="2"></space><w n="35.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="35.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="35.4">l</w>’<w n="35.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="35.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="36" num="5.4"><space unit="char" quantity="2"></space><w n="36.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="36.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="36.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="36.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>,</l>
					<l n="37" num="5.5"><space unit="char" quantity="2"></space><w n="37.1">N</w>’<w n="37.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="37.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="37.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="37.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="37.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>.</l>
					<l n="38" num="5.6"><space unit="char" quantity="10"></space><w n="38.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="38.2">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="38.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="38.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>,</l>
					<l n="39" num="5.7"><space unit="char" quantity="6"></space><w n="39.1">P<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="39.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="39.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>g</w></l>
					<l n="40" num="5.8"><w n="40.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="40.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="40.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="40.4">d</w>’<w n="40.5">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="40.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="40.7">l</w>’<w n="40.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
			</div></body></text></TEI>