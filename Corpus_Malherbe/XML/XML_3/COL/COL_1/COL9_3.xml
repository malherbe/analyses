<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL9">
				<head type="main">AVIS A LA BELLE JEUNESSE,</head>
				<head type="form">VAUDEVILLE.</head>
				<head type="tune">Air : Boire à son tirelire, lire, etc.</head>
				<head type="tune">Noté, N°. 9.</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>L</w> <w n="1.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="1.3">s</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">s</w>’<w n="2.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="3.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="3.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.2">C<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="4.3">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="4.4">N<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="5.2">M<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>rs</w>.</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">D</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> :</l>
					<l rhyme="none" n="7" num="1.7"><w n="7.1">N</w>’<w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.5">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l rhyme="none" n="8" num="1.8"><w n="8.1">N</w>’<w n="8.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.3">qu</w>’<w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.7">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">N</w>’<w n="9.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="9.3">qu</w>’<w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="10" num="2.1"><space unit="char" quantity="4"></space><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>ST</w> <w n="10.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="10.6">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w>,</l>
					<l n="11" num="2.2"><space unit="char" quantity="4"></space><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">G<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="11.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="480">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w> :</l>
					<l n="12" num="2.3"><space unit="char" quantity="4"></space><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w>-<w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="12.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w>,</l>
					<l n="13" num="2.4"><space unit="char" quantity="4"></space><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3">B<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="13.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w>.</l>
					<l n="14" num="2.5"><space unit="char" quantity="8"></space><w n="14.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="14.2">M<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>rs</w>,</l>
					<l n="15" num="2.6"><space unit="char" quantity="8"></space><w n="15.1">D</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> :</l>
					<l rhyme="none" n="16" num="2.7"><w n="16.1">N</w>’<w n="16.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="16.3">qu</w>’<w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.5">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="16.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l rhyme="none" n="17" num="2.8"><w n="17.1">N</w>’<w n="17.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="17.3">qu</w>’<w n="17.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="17.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="17.7">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="18" num="2.9"><space unit="char" quantity="8"></space><w n="18.1">N</w>’<w n="18.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="18.3">qu</w>’<w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="18.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="19" num="3.1"><space unit="char" quantity="4"></space><w n="19.1">P<seg phoneme="u" type="vs" value="1" rule="425">OU</seg>RQU<seg phoneme="wa" type="vs" value="1" rule="281">OI</seg></w> <w n="19.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="20" num="3.2"><space unit="char" quantity="4"></space><w n="20.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">F<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="21" num="3.3"><space unit="char" quantity="4"></space><w n="21.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.5">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="22" num="3.4"><space unit="char" quantity="4"></space><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="22.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.4">n<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
					<l n="23" num="3.5"><space unit="char" quantity="8"></space><w n="23.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="23.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="23.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
					<l n="24" num="3.6"><space unit="char" quantity="8"></space><w n="24.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="24.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="24.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
					<l rhyme="none" n="25" num="3.7"><w n="25.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="25.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="25.3">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="25.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l rhyme="none" n="26" num="3.8"><w n="26.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="26.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="26.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="26.4">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="26.5">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="27" num="3.9"><space unit="char" quantity="8"></space><w n="27.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="27.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="27.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w>.</l>
				</lg>
			</div></body></text></TEI>