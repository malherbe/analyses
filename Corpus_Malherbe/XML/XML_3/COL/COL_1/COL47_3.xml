<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL47">
				<head type="main">LA RENCONTRE.</head>
				<head type="tune">Air : Noté, N°. 27.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">E</seg></w> <w n="1.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.4">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">J<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.4">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">n<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="4.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="a" type="vs" value="1" rule="365">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> :</l>
					<l n="5" num="1.5"><w n="5.1">C<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">M<seg phoneme="wa" type="vs" value="1" rule="423">OI</seg></w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.3">sç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
					<l n="8" num="2.2"><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.2">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="8.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
					<l n="9" num="2.3"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">sç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.4">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="10" num="2.4"><w n="10.1">J</w>’<w n="10.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>, <w n="10.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.6">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> :</l>
					<l n="11" num="2.5"><w n="11.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="12" num="2.6"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">O</seg>MM<seg phoneme="ɑ̃" type="vs" value="1" rule="369">EN</seg>T</w>, <w n="13.2">m</w>’<w n="13.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="13.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="13.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="14" num="3.2"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ?</l>
					<l n="15" num="3.3"><w n="15.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="15.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="15.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.6">v<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
					<l n="16" num="3.4"><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.6">t</w>’<w n="16.7"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> ?</l>
					<l n="17" num="3.5"><w n="17.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="17.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="18" num="3.6"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="18.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">C<seg phoneme="o" type="vs" value="1" rule="444">O</seg>MM<seg phoneme="ə" type="em" value="1" rule="e-19">E</seg>R<seg phoneme="ə" type="ee" value="0" rule="e-23">E</seg></w>, <w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="19.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="19.4">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="19.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
					<l n="20" num="4.2"><w n="20.1">J</w>’<w n="20.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="20.3">d</w>’<w n="20.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w> <w n="20.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <choice reason="analysis" type="false_verse" hand="RR"><sic>sols</sic><corr source="édition 1864"><w n="20.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></corr></choice> :</l>
					<l n="21" num="4.3"><w n="21.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">v<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">n<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="22" num="4.4"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">n</w>’<w n="22.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="22.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="22.6">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> ;</l>
					<l n="23" num="4.5"><w n="23.1">J</w>’<w n="23.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="23.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.4"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="23.5">b<seg phoneme="y" type="vs" value="1" rule="445">û</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.6">t</w>’<w n="23.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w>,</l>
					<l n="24" num="4.6"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="24.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">QU<seg phoneme="ɑ̃" type="vs" value="1" rule="313">AN</seg>D</w> <w n="25.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="25.3"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg>t</w> <w n="25.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
					<l n="26" num="5.2"><w n="26.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="26.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.6">S<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
					<l n="27" num="5.3"><w n="27.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="27.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.7">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="28" num="5.4"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="28.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="28.4">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="29" num="5.5"><w n="29.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="29.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="29.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="29.4">h<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="29.5">d</w>’<w n="29.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="30" num="5.6"><w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="30.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="30.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>L</w> <w n="31.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="31.3">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="31.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="32" num="6.2"><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.4">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> ;</l>
					<l n="33" num="6.3"><w n="33.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="33.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>’ <w n="33.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct</w>, <w n="33.4">j</w>’<w n="33.5">l</w>’<w n="33.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="33.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="33.8">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="34" num="6.4"><w n="34.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="34.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="34.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="34.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="34.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="35" num="6.5"><w n="35.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.4">d</w>’<w n="35.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="35.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="36" num="6.6"><w n="36.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="36.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="36.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>L</w> <w n="37.2">m</w>’<w n="37.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="37.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.6">f<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
					<l n="38" num="7.2"><w n="38.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="38.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="38.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.4">m</w>’<w n="38.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> :</l>
					<l n="39" num="7.3"><w n="39.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="39.2">sç<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="39.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="39.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.5">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="39.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="40" num="7.4"><w n="40.1">Qu</w>’<w n="40.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="40.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="40.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="40.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="40.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> ;</l>
					<l n="41" num="7.5"><w n="41.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="41.2">qu</w>’<w n="41.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="41.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="41.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="41.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="41.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w></l>
					<l n="42" num="7.6"><w n="42.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="42.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="42.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>