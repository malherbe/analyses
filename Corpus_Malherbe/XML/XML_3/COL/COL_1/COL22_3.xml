<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL22">
				<head type="main">LA SURPRISE AGRÉABLE,</head>
				<head type="form">PARODIE.</head>
				<head type="tune">Air : Les Folies d’Espagne,</head>
				<head type="tune">Noté dans l’Anth. Franç. Tome II, page 291.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="1.2">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">ON</seg></w> <w n="1.3"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="1.7">Gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="2" num="1.2"><w n="2.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.8">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">s</w>’<w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.5">s</w>’<w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> : <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="3.8">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>