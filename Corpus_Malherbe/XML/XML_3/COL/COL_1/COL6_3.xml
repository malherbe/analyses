<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL6">
				<head type="main">L’AGE D’OR.</head>
				<head type="form">PARODIE.</head>
				<head type="tune">Air : Tout de fil en aiguille.</head>
				<head type="tune">Noté, N°. 6.</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">ON</seg></w> <w n="1.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="1.3">d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">d<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>x<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="COL6_1">e</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="3.2">s</w>’<w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.5">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="COL6_1">e</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="4.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.3">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="COL6_2">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="COL6_1">e</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="COL6_1">e</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">N</w>’<w n="6.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="6.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
					<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="7.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="7.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="COL6_1">e</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="8.2">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="COL6_1">e</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="9.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.3">t<seg phoneme="ɛ" type="vs" value="1" rule="361">e</seg>ms</w>, <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">Ch<seg phoneme="ɛ" type="vs" value="1" rule="COL6_1">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="1.10"><space unit="char" quantity="4"></space><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="10.3">t</w>-<w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> ?</l>
					<l n="11" num="1.11"><w n="11.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="11.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="11.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
					<l n="12" num="1.12"><space unit="char" quantity="4"></space><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="COL6_1">e</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
			</div></body></text></TEI>