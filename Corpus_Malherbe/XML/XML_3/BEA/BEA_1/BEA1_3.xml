<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’ÉTERNELLE CHANSON</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>192 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Horizontales</title>
						<author>Henri Beauclair</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/henribeauclairleshorizontales.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>L’Éternelle chanson</title>
						<author>Henri Beauclair</author>
						<imprint>
							<publisher>Léon Vanier,Paris</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEA1">
				<head type="number">I</head>
				<head type="main">Billet doux</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.2">j</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="2.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="2.4">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="3" num="1.3"><w n="3.1">C<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="3.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="3.4">l</w>’<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ?</l>
					<l n="4" num="1.4"><w n="4.1">Bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="4.2">j</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
					<l n="5" num="1.5"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="5.4">d</w>’<w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="5.6">l</w>’<w n="5.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="5.8">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
					<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> : <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="6.4">n</w>’<w n="6.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.9">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="7" num="1.7"><w n="7.1">Bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.2">j</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="8.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="8.4">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">S<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.5">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="9.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ?</l>
					<l n="10" num="2.2"><w n="10.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="10.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="10.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pç<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="11" num="2.3"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="11.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ;</l>
					<l n="12" num="2.4"><w n="12.1">S<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.5">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="12.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ?</l>
					<l n="13" num="2.5"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
					<l n="14" num="2.6"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="15" num="2.7"><w n="15.1">S<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="15.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.5">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ?</l>
					<l n="16" num="2.8"><w n="16.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="16.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="16.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pç<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="18" num="3.2"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="18.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.3">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
					<l n="20" num="3.4"><w n="20.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ;</l>
					<l n="21" num="3.5"><w n="21.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w>, <w n="21.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">d<seg phoneme="y" type="vs" value="1" rule="445">û</seg></w> <w n="21.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.7">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="22" num="3.6"><w n="22.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.2">s</w>’<w n="22.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="22.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="22.6">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="23" num="3.7"><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="23.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="24" num="3.8"><w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="24.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w>-<w n="25.2">t</w>-<w n="25.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="25.4"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg></w> <w n="25.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="25.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="25.7">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="25.8">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="26" num="4.2"><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>squ<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.4">t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="26.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="26.6">d</w>’<w n="26.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="27" num="4.3"><w n="27.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="27.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="27.4">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="27.5">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="28" num="4.4"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w>-<w n="28.2">t</w>-<w n="28.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="28.4"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg></w> <w n="28.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="28.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="28.7">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="28.8">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					<l n="29" num="4.5"><w n="29.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="29.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="29.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="30" num="4.6"><w n="30.1">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="30.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="30.3">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="30.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.7">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="31" num="4.7"><w n="31.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w>-<w n="31.2">t</w>-<w n="31.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="31.4"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg></w> <w n="31.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="31.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="31.7">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="31.8">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="32" num="4.8"><w n="32.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>squ<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="32.4">t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="32.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="32.6">d</w>’<w n="32.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
			</div></body></text></TEI>