<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><head type="main_subpart">XL</head><head type="main_subpart">Un fantôme</head><div type="poem" key="BAU42">
						<head type="number">II</head>
						<head type="main">Le Parfum</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>-<w n="1.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.5">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
							<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.5">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="3.3">d</w>’<w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="3.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.6">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="4.2">d</w>’<w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="4.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc</w> <w n="4.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ?</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>, <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.6">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="6" num="2.2"><w n="6.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
							<l n="7" num="2.3"><w n="7.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.2">l</w>’<w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
							<l n="8" num="2.4"><w n="8.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="8.3">c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xqu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w>,</l>
							<l n="10" num="3.2"><w n="10.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">l</w>’<w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="11" num="3.3"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="11.4">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w>, <w n="12.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="12.6">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
							<l n="13" num="4.2"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="13.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gn<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="13.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="14" num="4.3"><w n="14.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>t</w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
					</div></body></text></TEI>