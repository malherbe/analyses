<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU38">
					<head type="number">XXXVII</head>
					<head type="main" lang="LAT">Duellum</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="1.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">l</w>’<w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ; <w n="1.10">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="1.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w>.</l>
						<l n="3" num="1.3">— <w n="3.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="3.3">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">cl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.6">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.5">pr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.7">l</w>’<w n="4.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.9">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> ! <w n="5.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="6.3">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>, <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1">V<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="7.3">l</w>’<w n="7.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="8" num="2.4">— <w n="8.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="8.2">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="8.5">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>rs</w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="8.7">l</w>’<w n="8.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="8.9"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>lc<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="9.4">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w>-<w n="9.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="10" num="3.2"><w n="10.1">N<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="10.2">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, <w n="10.3">s</w>’<w n="10.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="10.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.7">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.3">p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="11.4">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1">— <w n="12.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="12.3">c</w>’<w n="12.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="12.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.8">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="12.10">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
						<l n="13" num="4.2"><w n="13.1">R<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="13.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w>, <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="o" type="vs" value="1" rule="442">o</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nh<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.3"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="14.2">d</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.4">l</w>’<w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="14.8">h<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
				</div></body></text></TEI>