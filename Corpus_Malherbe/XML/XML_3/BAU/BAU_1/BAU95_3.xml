<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">TABLEAUX PARISIENS</head><div type="poem" key="BAU95">
					<head type="number">XCI</head>
					<head type="main">À une mendiante rousse</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="4" num="1.4"><space quantity="6" unit="char"></space><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="5.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w>,</l>
						<l n="6" num="2.2"><w n="6.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.2">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="6.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="8" num="2.4"><space quantity="6" unit="char"></space><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="8.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="9.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
						<l n="10" num="3.2"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">r<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w></l>
						<l n="11" num="3.3"><w n="11.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>th<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
						<l n="12" num="3.4"><space quantity="6" unit="char"></space><w n="12.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="12.3">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rds</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="13.2">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.3">d</w>’<w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.5">h<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.6">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="13.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="14.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="15" num="4.3"><w n="15.1">Tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.3">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="15.4">br<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w></l>
						<l n="16" num="4.4"><space quantity="6" unit="char"></space><w n="16.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="17.2">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.5">tr<seg phoneme="u" type="vs" value="1" rule="427">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">r<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="19" num="5.3"><w n="19.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">j<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.5">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="19.6">d</w>’<w n="19.7"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
						<l n="20" num="5.4"><space quantity="6" unit="char"></space><w n="20.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3">n<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>ds</w> <w n="21.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="22" num="6.2"><w n="22.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="22.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="22.4">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w></l>
						<l n="23" num="6.3"><w n="23.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="23.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="23.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w>, <w n="23.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="24" num="6.4"><space quantity="6" unit="char"></space><w n="24.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="25.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>sh<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="26" num="7.2"><w n="26.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.2">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="26.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="26.5">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="27" num="7.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="27.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ps</w> <w n="27.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
						<l n="28" num="7.4"><space quantity="6" unit="char"></space><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="28.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="29.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="30" num="8.2"><w n="30.1">S<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="30.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.4">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="31" num="8.3"><w n="31.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="31.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="31.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="31.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="31.6">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w></l>
						<l n="32" num="8.4"><space quantity="6" unit="char"></space><w n="32.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="32.2">c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="32.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="34" num="9.2"><w n="34.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="34.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="34.4">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="35" num="9.3"><w n="35.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="35.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="35.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="35.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="36" num="9.4"><space quantity="6" unit="char"></space><w n="36.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="36.2">l</w>’<w n="36.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="37.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="37.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="37.5">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>,</l>
						<l n="38" num="10.2"><w n="38.1">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="38.2">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="38.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="38.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="38.5">R<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w></l>
						<l n="39" num="10.3"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>p<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="39.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="39.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
						<l n="40" num="10.4"><space quantity="6" unit="char"></space><w n="40.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="40.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="40.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="41.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="41.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="41.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="41.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w></l>
						<l n="42" num="11.2"><w n="42.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="42.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="42.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="42.6">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w></l>
						<l n="43" num="11.3"><w n="43.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="43.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="43.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="43.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="43.5">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
						<l n="44" num="11.4"><space quantity="6" unit="char"></space><w n="44.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="44.2">d</w>’<w n="44.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="44.4">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">— <w n="45.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="45.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="45.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="45.4">gu<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="46" num="12.2"><w n="46.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="46.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="46.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="46.4">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="47" num="12.3"><w n="47.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="47.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="47.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="47.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.5">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="48" num="12.4"><space quantity="6" unit="char"></space><w n="48.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="48.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ;</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="49.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="49.3">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rgn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="49.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="49.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="50" num="13.2"><w n="50.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="50.2">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="50.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w>-<w n="50.5">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>f</w> <w n="50.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="51" num="13.3"><w n="51.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="51.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.4">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="51.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>h</w> ! <w n="51.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
						<l n="52" num="13.4"><space quantity="6" unit="char"></space><w n="52.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="52.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="52.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="53.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="53.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="53.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="53.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
						<l n="54" num="14.2"><w n="54.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w>, <w n="54.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="54.3">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="55.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="55.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="55.4">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="56" num="14.4"><space quantity="6" unit="char"></space><w n="56.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="56.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="56.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					</lg>
				</div></body></text></TEI>