<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU17">
					<head type="number">XVI</head>
					<head type="main">Châtiment de l’orgueil</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="1.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="1.4">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">Th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">Fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8">d</w>’<w n="2.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="3.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">qu</w>’<w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.10">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>,</l>
						<l n="4" num="1.4">— <w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="4.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="4.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>,</l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="5.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="5.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.3">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.7">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="7.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="7.6">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="8.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="8.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="8.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="8.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.9">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
						<l n="9" num="1.9">— <w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.5">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="9.6">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>, <w n="9.7">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.9">p<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>, —</l>
						<l n="10" num="1.10"><w n="10.1">S</w>’<w n="10.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="10.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>sp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w> <w n="10.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="11" num="1.11">« <w n="11.1">J<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="11.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.3">J<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ! <w n="11.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">t</w>’<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.8">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="11.9">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> !</l>
						<l n="12" num="1.12"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="12.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.3">j</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.6">t</w>’<w n="12.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w></l>
						<l n="13" num="1.13"><w n="13.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">l</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="13.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.5">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.8">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="1.14"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.6">qu</w>’<w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.8">f<seg phoneme="e" type="vs" value="1" rule="250">œ</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
					</lg>
					<lg n="2">
						<l n="15" num="2.1"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>mm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.4">s</w>’<w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
						<l n="16" num="2.2"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="16.6">d</w>’<w n="16.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.8">cr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.9">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.10">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> ;</l>
						<l n="17" num="2.3"><w n="17.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">ch<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="17.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="2.4"><w n="18.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="18.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="18.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="18.5">d</w>’<w n="18.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.8">d</w>’<w n="18.9"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="2.5"><w n="19.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w> <w n="19.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="19.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="19.9">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						<l n="20" num="2.6"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="20.6">s</w>’<w n="20.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="20.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.9">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
						<l n="21" num="2.7"><w n="21.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="21.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="21.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.7">cl<seg phoneme="e" type="vs" value="1" rule="148">e</seg>f</w> <w n="21.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="21.9">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="22" num="3.1"><w n="22.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="22.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="22.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="22.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="22.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="22.7">b<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.10">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="23" num="3.2"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="23.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="23.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.4">s</w>’<w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.8">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="23.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="23.10"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.11">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w></l>
						<l n="24" num="3.3"><w n="24.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w>, <w n="24.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gu<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="24.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.8">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
						<l n="25" num="3.4"><w n="25.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="25.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>d</w> <w n="25.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.7">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.8"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="3.5"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="26.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="26.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="26.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">j<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="26.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.9">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>