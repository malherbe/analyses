<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT235">
				<head type="number">X</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="1.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.10">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.11">n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="2" num="2.1"><w n="2.1">J</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="2.5">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w> <w n="2.6">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">l</w>’<w n="2.10"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="3" num="3.1"><w n="3.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="3.6">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w> <w n="3.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.9">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.11">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="4" num="4.1"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="4.3">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="4.6">l</w>’<w n="4.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">M<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="5" num="5.1"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="5.2">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>, <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="5.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">s<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="6" num="5.2"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="6.2">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="6.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="6.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.6">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="6.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="6.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ NÉANT<lb></lb>▪ ÉOLIE<lb></lb>▪ ALCON<lb></lb>▪ NIORT<lb></lb>▪ TENTE</note>
					</closer>
			</div></body></text></TEI>