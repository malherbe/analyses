<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉNIGMES</head><head type="sub_1">BOTANIQUE<lb></lb>(<hi rend="smallcap">EMBLÈMES</hi>.)</head><div type="poem" key="BRT156">
				<head type="number">VI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>,</l>
					<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">fr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="4.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.7">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>.</l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.2">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w> <w n="5.3">N<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="175">ë</seg>l</w>, <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>gl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="6.3">l</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct</w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.4">m</w>’<w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.7">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w>.</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="8.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="8.3">j</w>’<w n="8.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="9" num="1.9"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="9.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="9.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="9.5">cl<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="10" num="1.10"><w n="10.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="10.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !</l>
					<l n="11" num="1.11"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="11.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="12" num="1.12"><w n="12.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="12.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.5">m</w>’<w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> :</l>
					<l n="13" num="1.13"><hi rend="ital"><w n="13.1">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>thr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></hi>.</l>
					<l n="14" num="1.14"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>… <w n="14.3">l</w>’<w n="14.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">s</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Houx.</note>
					</closer>
			</div></body></text></TEI>