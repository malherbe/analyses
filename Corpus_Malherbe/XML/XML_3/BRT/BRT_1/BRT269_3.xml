<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS EN TRIANGLES</head><div type="poem" key="BRT269">
				<head type="number">XIX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <subst hand="ML" reason="analysis" type="completion"><del>.....</del><add rend="hidden"><w n="1.8">t<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></add></subst> ;</l>
					<l n="2" num="1.2"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.2">n</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="2.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <subst hand="ML" reason="analysis" type="completion"><del>....</del><add rend="hidden"><w n="2.10"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></add></subst> ;</l>
					<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.2">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="4.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.5">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.7">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
					<l n="5" num="1.5"><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <subst hand="ML" reason="analysis" type="completion"><del>...</del><add rend="hidden"><w n="5.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></add></subst> <w n="5.7">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">cr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.10">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.11">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> :</l>
					<l n="6" num="1.6"><w n="6.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="6.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="6.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="7" num="1.7"><w n="7.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="7.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="7.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="7.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="7.8">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="7.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.10">s<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r</w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="8.2">qu</w>’<w n="8.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="8.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.8">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>, <w n="8.9">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.10">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w></l>
					<l n="9" num="1.9"><w n="9.1">D</w>’<subst hand="ML" reason="analysis" type="completion"><del>..</del><add rend="hidden"><w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>s</w></add></subst>, <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>gl<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="10.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <subst hand="ML" reason="analysis" type="completion"><del>.</del><add rend="hidden"><w n="10.6"><seg phoneme="ɛ" type="vs" value="1" rule="50">è</seg>s</w></add></subst>, <w n="10.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <subst hand="ML" reason="analysis" type="completion"><del>t</del><add rend="hidden"><w n="10.9">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></add></subst> <w n="10.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.11">l</w>’<subst hand="ML" reason="analysis" type="completion"><del>r</del><add rend="hidden"><w n="10.12"><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r</w></add></subst></l>
					<l n="11" num="1.11"><w n="11.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="11.5">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="11.8">n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.10">l</w>’<w n="11.11"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>,</l>
					<l n="12" num="1.12"><w n="12.1">C<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="12.2">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w>, <w n="12.3">h<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Tamis<lb></lb>▪ amis<lb></lb>▪ mis<lb></lb>▪ is<lb></lb>▪ s</note>
					</closer>
			</div></body></text></TEI>