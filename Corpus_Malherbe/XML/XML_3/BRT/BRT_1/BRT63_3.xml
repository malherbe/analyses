<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT63">
				<head type="number">LXIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.8">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="2.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w>, <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="4.2">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w>.</l>
					<l n="5" num="1.5"><w n="5.1">N<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.7">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">n</w>’<w n="6.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.5">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w>… <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="6.7">d</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="6.9">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.10">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="6.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="7" num="1.7"><w n="7.1">Fr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>str<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>pr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> :</l>
					<l n="9" num="1.9"><w n="9.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.3">l</w>’<w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="9.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">d<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.9">r<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="10" num="1.10"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="10.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.6">s</w>’<w n="10.7"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.9">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ;</l>
					<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="290">aon</seg></w> <w n="11.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.11">pr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="12" num="1.12"><space unit="char" quantity="12"></space><w n="12.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="12.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="12.4">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> !</l>
					<l n="13" num="1.13"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="13.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w>, <w n="13.7">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ff<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>, <w n="13.9">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.10">t<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="1.14"><w n="14.1">Ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="14.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="15" num="1.15"><w n="15.1">S</w>’<w n="15.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.5">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="?" type="va" value="1" rule="34">er</seg></w> <w n="15.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.8">d</w>’<w n="15.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.10">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>q</w> <w n="15.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="16" num="1.16"><space unit="char" quantity="12"></space><w n="16.1">H<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ La poule ne doit pas chanter plus haut que le coq.</note>
					</closer>
			</div></body></text></TEI>