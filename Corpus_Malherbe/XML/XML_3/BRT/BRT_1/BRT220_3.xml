<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HOMONYMES</head><div type="poem" key="BRT220">
				<head type="number">XLV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3">cr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.4">l</w>’<w n="1.5">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="1.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="1.9"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="2.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xh<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="3.7">t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="18"></space><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="5.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="5.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="5.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.7">l</w>’<w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="5.10">c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="6.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.8">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">c<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="7.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.4"><space unit="char" quantity="18"></space><w n="8.1">M<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.7">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">m<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>cc<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ; <w n="10.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>.</l>
					<l n="11" num="3.3"><w n="11.1">N</w>’<w n="11.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.5">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="11.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.8">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.4"><space unit="char" quantity="18"></space><w n="12.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.5">s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.7">m<seg phoneme="a" type="vs" value="1" rule="343">a</seg>n<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.8">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="14.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.8">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>,</l>
					<l n="15" num="4.3"><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.8">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="16" num="4.4"><space unit="char" quantity="18"></space><w n="16.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="17.4">l</w>’<w n="17.5"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w>, <w n="17.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="17.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.9">v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="18.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="18.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.4">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">s<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="18.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="18.9">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="18.10"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> ! »</l>
					<l n="19" num="5.3"><w n="19.1">Pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="19.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.4">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="19.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.8">n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="5.4"><space unit="char" quantity="18"></space><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="21.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.5">l</w>’<w n="21.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.8">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.10">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="22.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.3">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.7"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="22.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="23.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="23.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>, <w n="23.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="23.7">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.11">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="6.4"><space unit="char" quantity="18"></space><w n="24.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="24.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="25.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="25.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.7">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.8">st<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="26.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="26.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.4">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="26.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.9">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
					<l n="27" num="7.3"><w n="27.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="27.2">d</w>’<w n="27.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="27.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="27.6">d</w>’<w n="27.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.8">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="27.9">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="28" num="7.4"><space unit="char" quantity="18"></space><w n="28.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Cannes, cane, canne, canne, canne, canne, kane.</note>
					</closer>
			</div></body></text></TEI>