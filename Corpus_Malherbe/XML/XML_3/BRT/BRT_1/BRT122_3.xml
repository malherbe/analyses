<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT122">
				<head type="number">XXII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="1.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="2.2">f<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="3.2">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w>, <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.5">l</w>’<w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="16"></space><w n="4.1">R<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>…</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="6.2">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="6.8">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="7" num="2.3"><w n="7.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="7.5">d</w>’<w n="7.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.10">d</w>’<w n="7.11"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.12">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="8" num="2.4"><space unit="char" quantity="16"></space><w n="8.1">L</w>’<w n="8.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.4">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="9.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="10.4">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="10.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="10.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="10.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>.</l>
					<l n="11" num="3.3"><w n="11.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="11.2">n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.3">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="11.5">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="12" num="3.4"><space unit="char" quantity="16"></space><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> ?</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ Fou-lure.</note>
					</closer>
			</div></body></text></TEI>