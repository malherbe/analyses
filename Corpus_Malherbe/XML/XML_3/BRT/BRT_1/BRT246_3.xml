<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MOTS CARRÉS</head><div type="poem" key="BRT246">
				<head type="number">XXI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.9">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">s<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.7">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>, <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.8">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="4.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.3">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="4.4">d</w>’<w n="4.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="5.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="5.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ld<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w>, <w n="5.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="5.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ?</l>
					<l n="6" num="2.2"><w n="6.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>, <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.3">P<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ? <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.6">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ?</l>
					<l n="7" num="2.3"><w n="7.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="7.2">j</w>’<w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="7.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.9">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.10">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.4"><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="8.4">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="8.5">d</w>’<w n="8.6"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rgn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="8.8">pl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.10">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ct<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="9.4">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ</w>’<w n="9.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.7">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.8">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">B<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">h<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> :</l>
					<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bst<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="11.6">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1">D<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">d</w>’<w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.8">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="13.2">n</w>’<w n="13.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.4">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.9">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="14.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.7">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.10">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> !</l>
					<l n="15" num="4.3"><w n="15.1">T<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="15.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.5">qu</w>’<w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.9">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w>, <w n="16.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.5">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="16.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.8">s</w>’<w n="16.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>.</l>
				</lg>
					<closer>
						<note type="footnote" id="none">▪ ADAM<lb></lb>▪ DÔME<lb></lb>▪ AMER<lb></lb>▪ MÈRE</note>
					</closer>
			</div></body></text></TEI>