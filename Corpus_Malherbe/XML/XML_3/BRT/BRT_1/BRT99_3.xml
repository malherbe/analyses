<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PROVERBES</head><div type="poem" key="BRT99">
				<head type="number">XCIX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.8">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.9">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rd<seg phoneme="a" type="vs" value="1" rule="365">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="2.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l part="I" n="3" num="1.3"><w n="3.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! </l>
					<l part="F" n="3" num="1.3">— <w n="3.3">Br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> ! <w n="3.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="3.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="3.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
					<l n="4" num="1.4">— <w n="4.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ; <w n="4.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="4.5">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="4.8">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
					<l part="I" n="5" num="1.5">— <w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>. </l>
					<l part="F" n="5" num="1.5">— <w n="5.5">C</w>’<w n="5.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.8">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="6.9">m</w>’<w n="6.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xpl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					<l part="I" n="7" num="1.7">— <w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="7.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>. </l>
					<l part="F" n="7" num="1.7">— <w n="7.4">J</w>’<w n="7.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="7.7">d</w>’<w n="7.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="7.9">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="8" num="1.8"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="8.3">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>. <w n="8.4">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="8.6">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.7">qu</w>’<w n="8.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.9">v<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.10">s</w>’<w n="8.11"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="8.12">pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ?</l>
					<l part="I" n="9" num="1.9">— <w n="9.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>. </l>
					<l part="F" n="9" num="1.9">— <w n="9.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="9.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.9">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="1.10"><w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="10.4">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> !</l>
					<l part="I" n="11" num="1.11">— <w n="11.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>… </l>
					<l part="F" n="11" num="1.11">— <w n="11.2">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.4">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="11.7">m</w>’<w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.9">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="12" num="1.12"><w n="12.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="12.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="12.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.8">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> !</l>
					<l part="I" n="13" num="1.13">— <w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>… </l>
					<l part="F" n="13" num="1.13">— <w n="13.4">J</w>’<w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>. <w n="13.6">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.9">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="1.14"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.4">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.6">n<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ?</l>
					<l n="15" num="1.15">— <w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>. <w n="15.4">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>-<w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.7">m</w>’<w n="15.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="16" num="1.16"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="16.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="16.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="16.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.8">l</w>’<w n="16.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.10">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> ! »</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Ce que femme veut, Dieu le veut.</note>
					</closer>
			</div></body></text></TEI>