<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le sphynx au foyer</title>
				<title type="medium">Édition électronique</title>
				<author key="BRT">
					<name>
						<forename>Mélanie</forename>
						<surname>Bourotte</surname>
					</name>
					<date from="1834" to="1890">1834-1890</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3978 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">BRT_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
						<author>Mélanie Bourotte</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k6208381n#</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le sphynx au foyer: proverbes, charades, énigmes, homonymes, mots carrés, mots en triangle, sonnets-portraits / par Mme Mélanie Bourotte</title>
								<author>Mélanie Bourotte</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. HENNUYER, IMPRIMEUR-ÉDITEUR</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques : corpus Malherbə, CRISCO, université de Caen.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>Les tirets demi-cadratins (—) ont été restitués.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Ajout d’éléments de substitution pour la phonétisation des lettres.</change>
			<change when="2021-06-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			<change when="2021-06-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHARADES</head><div type="poem" key="BRT123">
				<head type="number">XXIII</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="1.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="1.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="1.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="2.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> ;</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="3.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="3.7"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="3.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="3.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.10">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">n</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.5">qu</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.7">l</w>’<w n="4.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.10">cl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.11"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="2">
					<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="5.2">l</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ? <w n="5.4">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="5.6">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ff<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.2">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3">s</w>’<w n="7.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.6">d</w>’<w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.8">l<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="8.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.7">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
				</lg>
				<ab type="star">⁂</ab>
				<lg n="3">
					<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="9.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.5">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> ;</l>
					<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6">l</w>’<w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> ;</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.2">s</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> ! … <w n="11.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="11.7">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.9">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.10">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.7">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="12.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.10">r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="13" num="3.5"><space unit="char" quantity="8"></space><w n="13.1">L</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="13.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ff<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> ;</l>
					<l n="14" num="3.6"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h</w> <w n="14.2">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sf<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> !</l>
				</lg>
					<closer>
						<note id="none" type="footnote">▪ Cime-terre.</note>
					</closer>
			</div></body></text></TEI>