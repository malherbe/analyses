<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO18">
					<head type="main">LA CALOMNIE FOUDROYÉE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="4"></space>« <w n="1.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.3">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="1.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space>» <w n="2.1">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>bb<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="2.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="2.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="3" num="1.3">» <w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="3.2">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="3.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="3.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="3.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.8">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space>» <w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="4.5">n</w>’<w n="4.6"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="4.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.8">d</w>’<w n="4.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>.</l>
						<l n="5" num="1.5">» ‒ <w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="5.2">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.3">j<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.8">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>…</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space>» ‒ <w n="6.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.2">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.5">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
						<l n="7" num="1.7">» ‒ <w n="7.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="7.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> ?</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space>» <w n="8.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> <w n="8.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> ! <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
						<l n="9" num="1.9"><space unit="char" quantity="4"></space>» <w n="9.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.2">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="9.7">d</w>’<w n="9.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>» <w n="10.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="10.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="10.5">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space>» <w n="11.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">n</w>’<w n="11.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t</w> ! »</l>
					</lg>
				</div></body></text></TEI>