<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS</head><div type="poem" key="CAO48">
					<head type="main">RÉPONSE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="1.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="2.2">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.4">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="3.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">j<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="3.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b</w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="4.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="4.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="5.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.7">l</w>’<w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.4">z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w>, <w n="6.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.8">b<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1">S</w>’<w n="7.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="8.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="8.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>th</w> <w n="9.4">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="9.5">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="10" num="3.2"><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="10.4">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.5">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="10.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.7">d</w>’<w n="10.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="3.3"><w n="11.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="11.5">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.2">h<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="12.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="12.5">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.6">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ffl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="13.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.8">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.9">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="14" num="4.3"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">fr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> !</l>
					</lg>
					<closer>
						<signed>C…</signed>
						<dateline>
							<date when="1885">15 septembre 1885</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>