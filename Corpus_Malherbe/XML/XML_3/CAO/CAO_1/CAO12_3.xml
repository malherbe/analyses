<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO12">
					<head type="main">LE FAUBOURG SAINT-ROCH</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="1.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rg</w> <w n="1.4">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="1.5">R<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch</w> <w n="1.6">s</w>’<w n="1.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w></l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="2.8">Ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.2">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="3.3">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="3.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="4" num="1.4"><w n="4.1">C</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.9">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="5.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> ‒ <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="5.10">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ‒</l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">C</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t</w>, <w n="7.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.9">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.10">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
						<l n="8" num="2.2"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">En</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="8.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.5"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="8.6">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.8">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w></l>
						<l n="9" num="2.3"><space unit="char" quantity="12"></space><w n="9.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.5">h<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="10" num="2.4"><w n="10.1">C</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>ts</w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.10">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
						<l n="11" num="2.5"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.4">qu</w>’<w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.8">v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="11.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
						<l n="12" num="2.6"><space unit="char" quantity="12"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="12.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="13.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="13.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.10">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
						<l n="14" num="3.2"><w n="14.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="14.5">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w>, <w n="14.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w></l>
						<l n="15" num="3.3"><space unit="char" quantity="12"></space><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="3.4"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="16.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> <w n="16.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.8">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="16.9">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>…</l>
						<l n="17" num="3.5"><w n="17.1">C</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="17.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="17.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="17.8">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
						<l n="18" num="3.6"><space unit="char" quantity="12"></space><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.2">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">J</w>’<w n="19.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="19.5">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rg</w> <w n="19.6">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="19.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.8">fl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="20" num="4.2"><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="20.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="21" num="4.3"><space unit="char" quantity="12"></space><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="22" num="4.4"><w n="22.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.2">c</w>’<w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.6">s</w>’<w n="22.7"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="22.9">s<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="23" num="4.5"><w n="23.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">l<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">l</w>’<w n="23.6">h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="24" num="4.6"><space unit="char" quantity="12"></space><w n="24.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="24.2">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="24.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="25.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">t</w>’<w n="25.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="25.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="25.6">S<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w>-<w n="25.7">R<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch</w> ! <w n="25.8"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="25.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.11">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="26" num="5.2"><w n="26.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="26.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="26.4"><seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>r</w> <w n="26.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.6">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="27" num="5.3"><space unit="char" quantity="12"></space><w n="27.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">r<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="27.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.5">l</w>’<w n="27.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="5.4"><w n="28.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="28.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.5">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w> <w n="28.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.8">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.9">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="29" num="5.5"><w n="29.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="29.2">l</w>’<w n="29.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="29.4">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pl<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="29.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="30" num="5.6"><space unit="char" quantity="12"></space><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">V<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="30.5">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.2">t</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="31.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="31.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.7">qu</w>’<w n="31.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.9">l</w>’<w n="31.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.11">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.12">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.13">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w></l>
						<l n="32" num="6.2"><w n="32.1">V<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="32.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="32.3">l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="32.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="32.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.6">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="32.7">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w></l>
						<l n="33" num="6.3"><space unit="char" quantity="12"></space><w n="33.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.3">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="34" num="6.4"><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="34.2">qu</w>’<w n="34.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="34.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="34.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="34.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="34.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.9">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.10">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
						<l n="35" num="6.5"><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="35.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="35.3">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="35.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="36" num="6.6"><space unit="char" quantity="12"></space><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="36.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="36.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1880">Mai 1880</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>