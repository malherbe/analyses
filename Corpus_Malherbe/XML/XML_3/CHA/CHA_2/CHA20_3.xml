<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES DIVERSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="CHA">
					<name>
						<forename>François-René</forename>
						<nameLink>de</nameLink>
						<surname>CHATEAUBRIAND</surname>
					</name>
					<date from="1768" to="1848">1768-1848</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>735 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2019">2019</date>
				<idno type="local">CHA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies diverses</title>
						<author>François-René de Chateaubriand</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/chateaubriandpoesiesdiverses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1797" to="1827">1797-1827</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas incluse.</p>
				<p>Les notes de l’auteur ont été intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Des corrections métriques ont été apportées sans autres attestations publiées.</p>
				<p>Les vers de la strophe illisible du poème "SERMENT — LES NOIRS DEVANT LE GIBET DE JOHN BROWN" ont été marqués comme inanalysables.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
 					<p>L’orthographe a été vérifiée avec le correcteur Aspell.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CHA20">
				<head type="number">X</head>
				<head type="main">Peinture de Dieu</head>
				<head type="sub">Tirée de l’Écriture</head>
				<opener>
					<dateline>
						<placeName>Paris</placeName>,
						<date when="1810">1810</date>.
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="1.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="1.4">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! <w n="1.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="1.9">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">l</w>’<w n="2.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.8">l</w>’<w n="3.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.6">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.9">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="5.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="5.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="5.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.7">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">S</w>’<w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="7" num="1.7"><w n="7.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.8">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="9.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.10">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w></l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="10.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="10.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ; <w n="10.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>, <w n="10.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.11">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>,</l>
					<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>b<seg phoneme="i" type="vs" value="1" rule="467">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="11.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="12" num="1.12"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.3">cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="12.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="13" num="1.13"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="13.2">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="13.7">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! »</l>
					<l n="14" num="1.14"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">c</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.6">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="14.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="14.8">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.10">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
				</lg>
			</div></body></text></TEI>