<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CONTES</head><div type="poem" key="CHF33">
					<head type="main">LE CONNAISSEUR</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">s<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="1.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="1.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.6">l</w>’<w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="1.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w>,</l>
						<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.3"><seg phoneme="y" type="vs" value="1" rule="391">eu</seg></w> <w n="2.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w>, <w n="2.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.9">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="2.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.11">l</w>’<w n="2.12"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="3.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="3.7">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="3.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w></l>
						<l n="4" num="1.4"><w n="4.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="4.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="4.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rf<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.4">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>lfl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="6" num="1.6"><w n="6.1">N<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="6.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c</w>, <w n="6.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.8">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="7.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="8" num="1.8"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="8.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.8">th<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="9" num="1.9"><w n="9.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">br<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>z<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="9.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.8"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> :</l>
						<l n="10" num="1.10"><space unit="char" quantity="12"></space><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="10.5">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="12" num="1.12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.3">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.8">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="13" num="1.13"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>j<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">d<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ; <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.7">s</w>’<w n="13.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> :</l>
						<l n="14" num="1.14"><space unit="char" quantity="12"></space>« <w n="14.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="14.2">s</w>’<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <hi rend="ital"><w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</hi> »</l>
						<l n="16" num="1.16"><space unit="char" quantity="12"></space><w n="16.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w>, <w n="16.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="17.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="17.3">qu</w>’<w n="17.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="17.7"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="1.18"><w n="18.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">qu</w>’<w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="18.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>,</l>
						<l n="19" num="1.19"><space unit="char" quantity="12"></space><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">qu</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ;</l>
						<l n="20" num="1.20"><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ffl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="20.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rts</w> <w n="20.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rf<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="21" num="1.21"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="22" num="1.22"><space unit="char" quantity="12"></space><w n="22.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="22.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="22.3">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
						<l n="23" num="1.23"><space unit="char" quantity="12"></space><w n="23.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="23.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="24" num="1.24"><w n="24.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="24.4">L<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="476">ï</seg>s</w>, <w n="24.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="24.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="24.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="24.9">s</w>’<w n="24.10"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="24.11">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> ;</l>
						<l n="25" num="1.25"><w n="25.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="25.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>t</w> <w n="25.5">l</w>’<w n="25.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="25.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
						<l n="26" num="1.26"><w n="26.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.2">M<seg phoneme="i" type="vs" value="1" rule="468">i</seg>lfl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="26.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="26.4">br<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="1.27"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="27.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="27.3">b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="27.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="27.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="27.6">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>, <w n="27.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="27.8">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="27.9">b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="28" num="1.28"><space unit="char" quantity="12"></space><w n="28.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>ph<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="28.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
						<l n="29" num="1.29"><space unit="char" quantity="12"></space><w n="29.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="29.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="29.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="29.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>,</l>
						<l n="30" num="1.30"><space unit="char" quantity="4"></space><w n="30.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="30.2">l</w>’<w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pp<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.6">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="30.8">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space>« <w n="31.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.2">n</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="31.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <hi rend="ital"><w n="31.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.7">l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</hi></l>
					</lg>
				</div></body></text></TEI>