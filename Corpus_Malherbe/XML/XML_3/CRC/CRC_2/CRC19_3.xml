<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Petits airs</title>
				<title type="medium">Édition électronique</title>
				<author key="CRC">
					<name>
						<forename>Francis</forename>
						<surname>Carco</surname>
					</name>
					<date from="1886" to="1958">1886-1958</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>280 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CRC_2</idno>
				<availability status="restricted">
					<p>texte sous droits</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Petits airs</title>
						<author>Francis Carco</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/petitsairspome00carc/page/24/mode/2up</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies complètes</title>
								<author>Francis Carco</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>RONALD DAVIS ET CIE</publisher>
									<date when="1920">1920</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1920">1920</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
			<change when="2021-06-03" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-06-04" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="CRC19" rhyme="none">
		<head type="main">Personnages</head>
			<opener>
				<salute><hi rend="smallcap">À MAURICE ASSELIN</hi>.</salute>
			</opener>
		<lg n="1">
			<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">l</w>’<w n="1.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>r<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
			<l n="2" num="1.2"><w n="2.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="2.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="2.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">j</w>’<w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
			<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="308">Ai</seg>gr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.6">r<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
			<l n="4" num="1.4"><w n="4.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="370">e</seg>n</w>.</l>
		</lg>
		<lg n="2">
			<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
			<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.4">t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="6.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
			<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="7.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="7.7">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
			<l n="8" num="2.4"><w n="8.1">Qu</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="8.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
		</lg>
		<lg n="3">
			<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="9.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="9.6">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
			<l n="10" num="3.2"><w n="10.1">Qu</w>’<w n="10.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="10.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
			<l n="11" num="3.3"><w n="11.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w></l>
			<l n="12" num="3.4"><w n="12.1">Tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
		</lg>
	</div></body></text></TEI>