<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES436">
					<head type="main">À MADEMOISELLE MARS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="1.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">M<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ! <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="415">Ô</seg></w> <w n="1.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.9">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.8">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.9">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="3.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="4.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.6">l</w>’<w n="4.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bt<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.9">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">R<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">t</w>’<w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="5.6">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.8">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">l</w>’<w n="5.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="6" num="2.2"><w n="6.1">H<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.5">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.9">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.3">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ! <w n="7.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="7.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
						<l n="8" num="2.4"><w n="8.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="8.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>h</w> ! <w n="8.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
					</lg>
				</div></body></text></TEI>