<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES397">
					<head type="main">L’ÂME ERRANTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="2.5">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="2.6">n</w>’<w n="2.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.9">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ;</l>
						<l n="3" num="1.3"><w n="3.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.6">l</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="4.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="4.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ffl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1">Gl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">J</w>’<w n="7.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.6">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="7.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="1.8"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sp<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="2.2"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.6">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w></l>
						<l n="11" num="2.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="2.4"><w n="12.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w>.</l>
						<l n="13" num="2.5"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.4">l<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="2.6"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
						<l n="15" num="2.7"><w n="15.1">M</w>’<w n="15.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="2.8"><w n="16.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="16.2">l</w>’<w n="16.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="17.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="17.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="17.6">d</w>’<w n="17.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="17.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="18" num="3.2"><w n="18.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
						<l n="19" num="3.3"><w n="19.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.4">qu</w>’<w n="19.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.7">d</w>’<w n="19.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.9">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="20" num="3.4"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.2">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>. <w n="20.6">S<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
						<l n="21" num="3.5"><w n="21.1">J</w>’<w n="21.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="21.3">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="21.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">ph<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="22" num="3.6"><w n="22.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.2">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.3">t<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="22.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="22.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.6">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w>,</l>
						<l n="23" num="3.7"><w n="23.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.2">s</w>’<w n="23.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="24" num="3.8"><w n="24.1">B<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="24.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="24.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>…</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="25.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="25.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="25.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.5">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="25.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="26" num="4.2"><w n="26.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="26.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w></l>
						<l n="27" num="4.3"><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="27.3">fr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="27.4">d</w>’<w n="27.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="27.7"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="4.4"><w n="28.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="28.3">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="28.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="28.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
						<l n="29" num="4.5"><w n="29.1">Cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="29.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="29.4">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="29.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="30" num="4.6"><w n="30.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="30.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.5">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="30.6">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>,</l>
						<l n="31" num="4.7"><w n="31.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="31.4">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="32" num="4.8"><w n="32.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="32.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="32.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> !</l>
					</lg>
				</div></body></text></TEI>