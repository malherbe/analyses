<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES349">
					<head type="main">UNE LETTRE DE FEMME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.2">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="1.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="1.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="1.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space quantity="10" unit="char"></space><w n="2.1">J</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="3.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="3.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.7">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="3.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.9">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.10">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><space quantity="10" unit="char"></space><w n="4.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="5.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="5.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.9">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>-<w n="5.10">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="6" num="2.2"><space quantity="10" unit="char"></space><w n="6.1">B<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="6.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> :</l>
						<l n="7" num="2.3"><w n="7.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="7.4">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="7.5">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="7.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="7.7">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.10">qu</w>’<w n="7.11"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.12"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><space quantity="10" unit="char"></space><w n="8.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.6">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! <w n="9.7">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.9">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.10"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.11">l</w>’<w n="9.12"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><space quantity="10" unit="char"></space><w n="10.1">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="10.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">m</w>’<w n="11.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.7">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="11.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="11.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.11">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.12"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><space quantity="10" unit="char"></space><w n="12.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="13.5">s</w>’<w n="13.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="13.9">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><space quantity="10" unit="char"></space><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="15.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">c</w>’<w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="15.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="15.10">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><space quantity="10" unit="char"></space><w n="16.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.2">t</w>’<w n="17.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="17.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.6">s</w>’<w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ! <w n="17.9">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.10">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.11">m<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="17.12"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.13">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><space quantity="10" unit="char"></space><w n="18.1">L<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">b<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="19.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="19.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.7">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.9">l</w>’<w n="19.10"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="20" num="5.4"><space quantity="10" unit="char"></space><w n="20.1">L<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.3">l</w>’<w n="21.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.7">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.9">d</w>’<w n="21.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="21.11"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.12">d</w>’<w n="21.13"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="22" num="6.2"><space quantity="10" unit="char"></space><w n="22.1">C<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="23" num="6.3"><w n="23.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="23.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> : <w n="23.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="23.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="23.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="24" num="6.4"><space quantity="10" unit="char"></space><w n="24.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">l</w>’<w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="25.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="25.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.8">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.9">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="25.10"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="7.2"><space quantity="10" unit="char"></space><w n="26.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="26.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> :</l>
						<l n="27" num="7.3"><w n="27.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="27.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.3">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="27.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="27.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.6">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="27.7">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="28" num="7.4"><space quantity="10" unit="char"></space><w n="28.1">C</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">h<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>r</w>.</l>
					</lg>
				</div></body></text></TEI>