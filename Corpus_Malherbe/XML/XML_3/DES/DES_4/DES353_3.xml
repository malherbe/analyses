<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES353">
					<head type="main">UN CRI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="1.3">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="1.4">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="2.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="2.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="3.2">s</w>’<w n="3.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>, <w n="3.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="3.8">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="3.9">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="4.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="5.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="2.2"><w n="6.1">Gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="6.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.6">f<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w> <w n="6.7">l</w>’<w n="6.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>,</l>
						<l n="7" num="2.3"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.4"><w n="8.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="8.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">l</w>’<w n="8.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>.</l>
						<l n="9" num="2.5"><w n="9.1">H<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="9.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="9.3">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="10" num="2.6"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="10.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="11" num="2.7"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="11.2">s</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>, <w n="11.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="11.8">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="11.9">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
						<l n="12" num="2.8"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.3">qu</w>’<w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="13.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.8">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="3.2"><w n="14.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w> <w n="14.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="14.5">tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="14.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>.</l>
						<l n="15" num="3.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.4">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="3.4"><w n="16.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ts</w> <w n="16.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> !</l>
						<l n="17" num="3.5"><w n="17.1">H<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="17.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="17.3">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="18" num="3.6"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="18.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="18.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="19" num="3.7"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="19.2">s</w>’<w n="19.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="19.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>, <w n="19.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="19.8">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="19.9">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="20" num="3.8"><w n="20.1">J</w>’<w n="20.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="20.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="20.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="21" num="4.1"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>, <w n="21.5">j</w>’<w n="21.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>sp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="4.2"><w n="22.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.4">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="22.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.7">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
						<l n="23" num="4.3"><w n="23.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="23.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="4.4"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="24.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="24.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
						<l n="25" num="4.5"><w n="25.1">H<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="25.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="25.3">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="26" num="4.6"><w n="26.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="26.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="26.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="26.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="27" num="4.7"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="27.2">s</w>’<w n="27.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="27.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="27.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="27.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>, <w n="27.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="27.8">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="27.9">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="28" num="4.8"><w n="28.1">J</w>’<w n="28.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="28.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="28.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="29" num="5.1"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="29.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="29.3">l</w>’<w n="29.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.5">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="30" num="5.2"><w n="30.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="30.2">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w>, <w n="30.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="30.4">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d</w>, <w n="30.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="30.6">l</w>’<w n="30.7"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>r<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> :</l>
						<l n="31" num="5.3"><w n="31.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="31.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="31.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">l</w>’<w n="31.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="31.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="5.4"><w n="32.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="32.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="32.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						<l n="33" num="5.5"><w n="33.1">H<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="33.2">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! <w n="33.3">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="34" num="5.6"><w n="34.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="34.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="34.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="34.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="34.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="34.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="35" num="5.7"><w n="35.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="35.2">s</w>’<w n="35.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="35.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="35.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="35.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>, <w n="35.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="35.8">l<seg phoneme="ə" type="em" value="1" rule="e-6">e</seg></w>-<w n="35.9">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
						<l n="36" num="5.8"><w n="36.1">J</w>’<w n="36.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="36.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="36.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="36.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
					</lg>
				</div></body></text></TEI>