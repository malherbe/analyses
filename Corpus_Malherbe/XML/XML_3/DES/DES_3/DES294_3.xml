<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES294">
				<head type="main">MOI JE LE SAIS</head>
				<opener>
					<salute>À mademoiselle Louise Grombach.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="1.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="2.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> :</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ls</w> <w n="3.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.7">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="4.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>-<w n="4.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ?</l>
					<l n="5" num="1.5"><w n="5.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="5.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="467">î</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="6.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.5">d</w>’<w n="6.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="7.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="7.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="8" num="2.2"><w n="8.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : <w n="8.5">j</w>’<w n="8.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.7">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>. <w n="8.8">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="9" num="2.3"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="9.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.9">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="10" num="2.4"><w n="10.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="10.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.6">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> !</l>
					<l n="11" num="2.5"><w n="11.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="12" num="2.6"><w n="12.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="12.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : <w n="12.5">j</w>’<w n="12.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.7">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>. <w n="12.8">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.9">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="13.4">d</w>’<w n="13.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.6">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.8">s</w>’<w n="13.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="14" num="3.2"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="14.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="14.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.9">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> :</l>
					<l n="15" num="3.3"><w n="15.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="15.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.5">l</w>’<w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.9">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="16" num="3.4"><w n="16.1">D</w>’<w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.3">t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="16.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="16.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="16.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> ?</l>
					<l n="17" num="3.5"><w n="17.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="17.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.6">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="17.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.8">s</w>’<w n="17.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="18" num="3.6"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="18.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="18.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.6">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="18.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.9">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="19.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.7">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="20" num="4.2"><w n="20.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="20.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="20.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> : <w n="20.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="20.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="20.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					<l n="21" num="4.3"><w n="21.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.3">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="21.4">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="21.5">gu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.7">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="22" num="4.4"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="22.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.7">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="22.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					<l n="23" num="4.5"><w n="23.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="23.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="23.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.7">bl<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="24" num="4.6"><w n="24.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="24.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="24.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> : <w n="24.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.5">pl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>, <w n="24.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.8">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="24.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.5">d</w>’<w n="25.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.7">gl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="26" num="5.2"><w n="26.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="26.2">s</w>’<w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="26.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ;</l>
					<l n="27" num="5.3"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="27.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.4">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="27.5">l</w>’<w n="27.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.7">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="5.4"><w n="28.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.2">m<seg phoneme="y" type="vs" value="1" rule="445">û</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="28.3">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="28.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="28.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="28.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="29" num="5.5"><w n="29.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="29.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="29.5">d</w>’<w n="29.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.7">gl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="30" num="5.6"><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.2">s</w>’<w n="30.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="30.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="30.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> !</l>
				</lg>
			</div></body></text></TEI>