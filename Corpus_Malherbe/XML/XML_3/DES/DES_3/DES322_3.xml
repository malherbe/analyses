<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES322">
				<head type="main">RAHEL LA CRÉOLE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.2">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="1.4">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ! <w n="1.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="1.7">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="2" num="1.2"><w n="2.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-27">e</seg></w> <w n="2.2">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="2.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.8">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="3" num="1.3"><w n="3.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.7">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.9">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w> <w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> : <w n="4.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="4.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="4.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="5.4">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rdr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.8">l</w>’<w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">C</w>’<w n="6.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">sc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="6.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="6.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.10">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.4">d</w>’<w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> :</l>
					<l n="8" num="2.4"><w n="8.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="8.2"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="8.3">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="8.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="8.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="9.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="9.3">l<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="10" num="3.2"><w n="10.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="10.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="10.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="11.6">d</w>’<w n="11.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> :</l>
					<l n="12" num="3.4"><w n="12.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="12.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="12.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="12.4">F<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="12.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="13.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.5">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="13.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.7">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1">S</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="14.3">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="14.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.9">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="15.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="15.4">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ? <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="15.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.8">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">m<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="16.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.8">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
				</lg>
			</div></body></text></TEI>