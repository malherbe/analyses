<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES277">
				<head type="main">UNE PLACE POUR DEUX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>-<w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">j</w>’<w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="2.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="3.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">S<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="6.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> :</l>
					<l n="7" num="2.3"><w n="7.1">Pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">n</w>’<w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="9.2">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="9.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="11" num="3.3"><w n="11.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>… <w n="12.3">j</w>’<w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
					<l n="15" num="4.3"><w n="15.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="15.4">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="16" num="4.4"><w n="16.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.3">l</w>’<w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> :</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L</w>’<w n="17.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="17.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.4">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="5.2"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
					<l n="19" num="5.3"><w n="19.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="19.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">j</w>’<w n="19.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="5.4"><w n="20.1">C</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="20.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> ! <w n="21.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="21.4">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="21.5">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="6.2"><w n="22.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="22.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
					<l n="23" num="6.3"><w n="23.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.2">l</w>’<w n="23.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="23.4">qu</w>’<w n="23.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="6.4"><w n="24.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="24.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.3">cl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.2">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> ! <w n="25.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="25.5">j<seg phoneme="ə" type="ef" value="1" rule="e-1">e</seg></w> :</l>
					<l n="26" num="7.2"><w n="26.1">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="26.2">l</w>’<w n="26.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="26.5">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ?</l>
					<l n="27" num="7.3"><w n="27.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.2">l</w>’<w n="28.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="28.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="28.5">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="29.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.4">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="30" num="8.2"><w n="30.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.3">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="30.4">d</w>’<w n="30.5"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="31" num="8.3"><w n="31.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="31.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.4">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="32" num="8.4"><w n="32.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="32.2">l</w>’<w n="32.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.4">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
				</lg>
			</div></body></text></TEI>