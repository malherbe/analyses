<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES105">
						<head type="main">L’ADIEU</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="1.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
							<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
							<l n="3" num="1.3"><space quantity="2" unit="char"></space><w n="3.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
							<l n="4" num="1.4"><w n="4.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="4.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="4.5">d</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
							<l n="5" num="1.5"><w n="5.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="5.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ;</l>
							<l n="6" num="1.6"><space quantity="4" unit="char"></space><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="6.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
							<l n="7" num="1.7"><space quantity="2" unit="char"></space><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
							<l n="8" num="1.8"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.3">d</w>’<w n="8.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="8.5">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><space quantity="2" unit="char"></space><w n="9.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="9.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
							<l n="10" num="2.2"><w n="10.1">V<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
							<l n="11" num="2.3"><space quantity="2" unit="char"></space><w n="11.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="11.2">n</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="11.4">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="11.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
							<l n="12" num="2.4"><w n="12.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="12.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rfl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ;</l>
							<l n="13" num="2.5"><space quantity="2" unit="char"></space><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
							<l n="14" num="2.6"><w n="14.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="14.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="14.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
							<l n="15" num="2.7"><space quantity="2" unit="char"></space><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="15.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="15.3">s</w>’<w n="15.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w>,</l>
							<l n="16" num="2.8"><w n="16.1">C<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="16.3">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w> <w n="16.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t</w>.</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="17.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
							<l n="18" num="3.2"><space quantity="4" unit="char"></space><w n="18.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
							<l n="19" num="3.3"><space quantity="2" unit="char"></space><w n="19.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
							<l n="20" num="3.4"><w n="20.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="20.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="20.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="20.5">d</w>’<w n="20.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
							<l n="21" num="3.5"><w n="21.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="21.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ;</l>
							<l n="22" num="3.6"><space quantity="4" unit="char"></space><w n="22.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
							<l n="23" num="3.7"><space quantity="2" unit="char"></space><w n="23.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="23.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
							<l n="24" num="3.8"><w n="24.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="24.3">d</w>’<w n="24.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="24.5">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1"><space quantity="2" unit="char"></space><w n="25.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="25.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w></l>
							<l n="26" num="4.2"><w n="26.1">M</w>’<w n="26.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="26.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> ;</l>
							<l n="27" num="4.3"><space quantity="2" unit="char"></space><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="27.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="27.3">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
							<l n="28" num="4.4"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="28.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="28.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
							<l n="29" num="4.5"><space quantity="2" unit="char"></space><w n="29.1">D<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="29.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>,</l>
							<l n="30" num="4.6"><w n="30.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.2">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>… <w n="30.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
							<l n="31" num="4.7"><space quantity="2" unit="char"></space><w n="31.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="31.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w></l>
							<l n="32" num="4.8"><w n="32.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="32.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="32.5">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>.</l>
						</lg>
						<lg n="5">
							<l n="33" num="5.1"><w n="33.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="33.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="33.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
							<l n="34" num="5.2"><space quantity="4" unit="char"></space><w n="34.1">M<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ;</l>
							<l n="35" num="5.3"><space quantity="2" unit="char"></space><w n="35.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="35.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
							<l n="36" num="5.4"><w n="36.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="36.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="36.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="36.5">d</w>’<w n="36.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
							<l n="37" num="5.5"><w n="37.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="37.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="37.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="37.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ;</l>
							<l n="38" num="5.6"><space quantity="4" unit="char"></space><w n="38.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="38.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
							<l n="39" num="5.7"><space quantity="2" unit="char"></space><w n="39.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="39.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="39.3">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
							<l n="40" num="5.8"><w n="40.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="40.3">d</w>’<w n="40.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="40.5">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>.</l>
						</lg>
					</div></body></text></TEI>