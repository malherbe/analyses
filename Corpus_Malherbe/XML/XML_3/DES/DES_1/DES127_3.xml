<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES127">
						<head type="main">À M<hi rend="sup">ELLE</hi> MARS</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="6" unit="char"></space><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">Th<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="2" num="1.2"><space quantity="6" unit="char"></space><w n="2.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.2">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.2">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.5">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="4" num="1.4"><space quantity="6" unit="char"></space><w n="4.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="5" num="1.5"><space quantity="6" unit="char"></space><w n="5.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.2">s</w>’<w n="5.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="6" num="1.6"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><space quantity="6" unit="char"></space><w n="7.1">L</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="8" num="2.2"><space quantity="6" unit="char"></space><w n="8.1">D</w>’<w n="8.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="9" num="2.3"><w n="9.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
							<l n="10" num="2.4"><space quantity="6" unit="char"></space><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="11" num="2.5"><space quantity="6" unit="char"></space><w n="11.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="11.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.3">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="12" num="2.6"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><space quantity="6" unit="char"></space><w n="13.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="14" num="3.2"><space quantity="6" unit="char"></space><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="15" num="3.3"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.2">r<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="15.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
							<l n="16" num="3.4"><space quantity="6" unit="char"></space><w n="16.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="17" num="3.5"><space quantity="6" unit="char"></space><w n="17.1">S<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="17.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.3">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
							<l n="18" num="3.6"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="18.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.5">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1"><space quantity="6" unit="char"></space><w n="19.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.2"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="20" num="4.2"><space quantity="6" unit="char"></space><w n="20.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="20.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="21" num="4.3"><w n="21.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="21.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.6">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>,</l>
							<l n="22" num="4.4"><space quantity="6" unit="char"></space><w n="22.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="23" num="4.5"><space quantity="6" unit="char"></space><w n="23.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="23.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="24" num="4.6"><w n="24.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="24.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
						</lg>
					</div></body></text></TEI>