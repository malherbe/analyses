<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES49">
						<head type="main">ÉLÉGIE</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="2" unit="char"></space><w n="1.1">P<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="1.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="2" num="1.2"><space quantity="2" unit="char"></space><w n="2.1">M</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.6">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> ;</l>
							<l n="3" num="1.3"><space quantity="2" unit="char"></space><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="3.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="4" num="1.4"><w n="4.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="4.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="4.5">j</w>’<w n="4.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w>.</l>
							<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="5.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="6" num="1.6"><w n="6.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="6.3">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.8">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg>x</w> <w n="6.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rfl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
							<l n="7" num="1.7"><w n="7.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="7.2">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ! <w n="7.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="8" num="1.8"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.4">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w>, <w n="8.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.8">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>.</l>
							<l n="9" num="1.9"><w n="9.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ! <w n="9.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">l</w>’<w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ; <w n="9.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.10"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="10" num="1.10"><w n="10.1">Br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="10.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ;</l>
							<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="11.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.6">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.8">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="12" num="1.12"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="12.7">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="12.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.9">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
							<l n="13" num="1.13"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="13.2">qu</w>’<w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.4">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.7">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.9">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="14" num="1.14"><w n="14.1">Qu</w>’<w n="14.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.3"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="14.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.8">l</w>’<w n="14.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.11">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.12">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> !</l>
							<l n="15" num="1.15"><w n="15.1">Qu</w>’<w n="15.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="15.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : « <w n="15.4">C</w>’<w n="15.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="15.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="15.8">qu</w>’<w n="15.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="15.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.11">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="15.12">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.13">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="16" num="1.16"><w n="16.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="16.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">n</w>’<w n="16.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="16.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.11">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>. »</l>
							<l n="17" num="1.17"><w n="17.1">Qu</w>’<w n="17.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : « <w n="17.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="17.5">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="17.7">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="17.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.10">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="17.11">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.12">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="18" num="1.18"><w n="18.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="18.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">l<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ;</l>
							<l n="19" num="1.19"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="19.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="19.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.6">l</w>’<w n="19.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.8">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="20" num="1.20"><w n="20.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>. <w n="20.2">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="20.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.5">n</w>’<w n="20.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="20.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="20.8">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.10">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ! »</l>
							<l n="21" num="1.21"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="21.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="21.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="21.5">l</w>’<w n="21.6">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="22" num="1.22"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="22.4">d</w>’<w n="22.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="22.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.8">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.10"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="23" num="1.23"><w n="23.1">S</w>’<w n="23.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="23.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w> <w n="23.7">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="24" num="1.24"><w n="24.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="24.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.3">pr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="24.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="24.8">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
							<l n="25" num="1.25"><w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="25.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="25.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>s</w> <w n="25.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.9">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="26" num="1.26"><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="26.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="26.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w> <w n="26.7">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
							<l n="27" num="1.27"><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.2">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="27.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="27.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.6">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="28" num="1.28"><space quantity="10" unit="char"></space><w n="28.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="28.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						</lg>
						<lg n="2">
							<l n="29" num="2.1"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="29.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="29.4">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="29.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.6">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="30" num="2.2"><w n="30.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="30.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="30.4">l</w>’<w n="30.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pr<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="30.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
							<l n="31" num="2.3"><w n="31.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="31.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="31.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.9">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="32" num="2.4"><w n="32.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="32.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">l</w>’<w n="32.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>… <w n="32.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="32.6">s</w>’<w n="32.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="32.8">n</w>’<w n="32.9"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="32.10">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="32.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
						</lg>
					</div></body></text></TEI>