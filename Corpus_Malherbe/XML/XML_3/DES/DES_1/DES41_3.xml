<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES41">
						<head type="main">À DÉLIE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="2.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">g<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
							<l n="5" num="1.5"><w n="5.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="6" num="1.6"><w n="6.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w>,</l>
							<l n="7" num="1.7"><w n="7.1">S</w>’<w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">s</w>’<w n="7.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="8" num="1.8"><w n="8.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="8.4">d</w>’<w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w> !</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="10" num="2.2"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> ;</l>
							<l n="11" num="2.3"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.4"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="12" num="2.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="12.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>.</l>
							<l n="13" num="2.5"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="14" num="2.6"><w n="14.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> ;</l>
							<l n="15" num="2.7"><w n="15.1">C</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">d</w>’<w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
							<l n="16" num="2.8"><w n="16.1">Qu</w>’<w n="16.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.3">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> !</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="17.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.4">l</w>’<w n="17.5">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="18" num="3.2"><w n="18.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> ?</l>
							<l n="19" num="3.3"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="19.4">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="20" num="3.4"><w n="20.1">Qu</w>’<w n="20.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.4">cr<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>.</l>
							<l n="21" num="3.5"><w n="21.1"><seg phoneme="w" type="sc" value="0" rule="431">Ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="21.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>, <w n="21.4">l</w>’<w n="21.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="22" num="3.6"><w n="22.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="22.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
							<l n="23" num="3.7"><w n="23.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="23.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="23.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="24" num="3.8"><w n="24.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="24.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1"><w n="25.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="25.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.3">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="25.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="26" num="4.2"><w n="26.1">N</w>’<w n="26.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="26.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
							<l n="27" num="4.3"><w n="27.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="27.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="28" num="4.4"><w n="28.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.3">g<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
							<l n="29" num="4.5"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>, <w n="29.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.3">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
							<l n="30" num="4.6"><w n="30.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="30.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="30.5">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
							<l n="31" num="4.7"><w n="31.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="31.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.4">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="32" num="4.8"><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="32.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="32.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="32.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
						</lg>
					</div></body></text></TEI>