<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANTS DE PALESTINE</head><div type="poem" key="HRV45">
				<head type="main">LE MIROIR D’ARGENT</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="1.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">m</w>’<w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.7">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
						<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.3">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="2.6">d</w>’<w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.11">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.12">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.13">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.8">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.9">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Qu</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>-<w n="5.3">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="5.4">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.7">d</w>’<w n="5.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> ?</l>
						<l n="6" num="2.2"><w n="6.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">S<seg phoneme="a" type="vs" value="1" rule="340">â</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="6.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="6.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="7" num="2.3"><w n="7.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.4">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.6">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="7.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> !</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">em</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="9.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="10" num="3.2"><w n="10.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.2">t</w>’<w n="10.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="10.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.8">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="3.3"><w n="11.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.3">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>… <w n="11.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="11.6">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.9">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>…</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="12.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.5">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="13.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="13.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.4">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
						<l n="14" num="4.2"><w n="14.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="14.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><w n="15.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.4">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="15.5">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rtr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.9">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cs</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="16.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space><w n="17.1">V<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="17.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.6">br<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="18" num="5.2"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="18.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="19" num="5.3"><w n="19.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.5">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="19.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.4">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="8"></space><w n="21.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>, <w n="21.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="21.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.5">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w>…</l>
						<l n="22" num="6.2"><w n="22.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.5">qu</w>’<w n="22.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="22.8">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="22.9">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="23" num="6.3"><w n="23.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.2">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="23.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">l</w>’<w n="23.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="23.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.8">d</w>’<w n="23.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="24.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">l</w>’<w n="24.4"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="24.5">d</w>’<w n="24.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.7">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><space unit="char" quantity="8"></space><w n="25.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="25.2">j</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="25.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="25.7">s</w>’<w n="25.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="26" num="7.2"><w n="26.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="26.2">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="26.3">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="26.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="26.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rf<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.9">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="27" num="7.3"><w n="27.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="27.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="28.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>… <w n="28.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="28.5">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>-<w n="28.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					</lg>
				</div></body></text></TEI>