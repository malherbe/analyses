<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR74">
						<head type="main">LE BEAUPRÉ</head>
						<opener>
							<salute>À JOSEPH CARRIÈS</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rcl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="1.9">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="2" num="1.2"><w n="2.1">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="2.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="2.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.7">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>qu<seg phoneme="i" type="vs" value="1" rule="487">i</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="3" num="1.3"><w n="3.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.3">l</w>’<w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.7">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="3.8">cr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="4" num="1.4"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="4.2">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="4.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">f<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="4.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">pr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
							<l n="5" num="1.5"><w n="5.1">C<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.2">r<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="5.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
							<l n="6" num="1.6"><w n="6.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ts</w> <w n="6.3">br<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>s</w>, <w n="6.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>, <w n="6.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">dr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="6.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="7" num="1.7"><w n="7.1">T<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="7.4">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="7.7">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ds</w> <w n="7.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
							<l n="8" num="1.8"><w n="8.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="8.6">d</w>’<w n="8.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="8.8">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="8.9">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="8.10">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
							<l n="9" num="1.9"><w n="9.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">pr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="9.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.10">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="10" num="1.10"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.3">z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="10.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.7">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>…</l>
						</lg>
						<lg n="2">
							<l n="11" num="2.1"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="11.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.5">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
							<l n="12" num="2.2"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.5">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="12.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.10">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
							<l n="13" num="2.3"><w n="13.1">L</w>’<w n="13.2">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="13.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="13.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						</lg>
						<lg n="3">
							<l n="14" num="3.1"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="14.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.4">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="14.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="15" num="3.2"><w n="15.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.3">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sp<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="15.4">l</w>’<w n="15.5"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="15.6">f<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w>, <w n="15.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.8">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.11">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> :</l>
							<l n="16" num="3.3"><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="16.4">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.8">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>…</l>
						</lg>
						<lg n="4">
							<l n="17" num="4.1"><w n="17.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="17.4">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="17.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.9">r<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
							<l n="18" num="4.2"><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>, <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="18.4">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="18.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.7">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
							<l n="19" num="4.3"><w n="19.1">Gu<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> ; <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.4">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="19.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> <w n="19.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="19.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.8">l</w>’<w n="19.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>gl<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
							<l n="20" num="4.4"><w n="20.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="20.2">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="20.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="20.6">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>mm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="20.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.8">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="20.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w></l>
							<l n="21" num="4.5"><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="21.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.7">pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="22" num="4.6"><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="22.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
							<l n="23" num="4.7"><w n="23.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="23.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.3">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="23.4">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="23.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.6">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="23.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="23.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.9">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="23.10">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ?…</l>
						</lg>
						<lg n="5">
							<l n="24" num="5.1">— <w n="24.1">P<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ! <w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="24.3">dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="24.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="24.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="24.7">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.8">n</w>’<w n="24.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
						</lg>
					</div></body></text></TEI>