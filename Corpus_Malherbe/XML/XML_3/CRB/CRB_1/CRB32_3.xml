<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB32">
					<head type="main">PAUVRE GARÇON</head>
					<opener>
						<epigraph>
							<cit>
								<quote>La Bête féroce</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.5">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>, <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">t<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.2">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="2.3">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ; <w n="2.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.8">qu</w>’<w n="2.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="2.10">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> …</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> … <w n="3.6">j</w>’<w n="3.7"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="3.10">b<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.4">n</w>’<w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.8">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.9">qu</w>’<w n="4.10"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.11">m</w>’<w n="4.12"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.8">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="5.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.10">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="6.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>… <w n="6.4">Vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w>, <w n="6.5">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ?…</l>
						<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="7.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>str<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="7.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.5">j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="7.6">qu</w>’<w n="7.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.8">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !…</l>
						<l n="8" num="2.4"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.4">j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>. <w n="8.5">Vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> — <w n="8.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> — <w n="8.7">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.8">m</w>’<w n="8.9"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>-<w n="9.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> ?… — <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> — <w n="9.5">c</w>’<w n="9.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="9.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.8">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.10">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.11">dr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="10.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="10.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="10.5">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="10.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="10.7">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.9">r<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> … <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.6">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w>. — <w n="11.7">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="11.11">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.12">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> ?…</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="12.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.5">fl<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="13" num="4.2"><w n="13.1">S<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="13.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <hi rend="ital"><w n="13.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c</w></hi>, <w n="13.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="13.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="13.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.10">phth<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l part="I" n="14" num="4.3"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w>, <w n="14.2">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w>-<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> : <w n="14.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> … </l>
						<l part="F" n="14" num="4.3"><w n="14.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="14.9">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="14.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.11">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					</lg>
				</div></body></text></TEI>