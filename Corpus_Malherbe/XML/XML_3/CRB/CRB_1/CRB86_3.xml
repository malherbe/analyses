<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GENS DE MER</head><div type="poem" key="CRB86">
					<head type="main">CAP’TAINE LEDOUX</head>
					<head type="sub_1">À LA BONNE RELÂCHE DES CABOTEURS <lb></lb>VEUVE-CAP’TAINE GALMICHE <lb></lb>CHAUDIÈRE POUR LES MARINS — COOK-HOUSE <lb></lb>BRANDY — LIQŒUR <lb></lb>— POULIAGE —</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">T<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="1.2">c</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">l</w>’<w n="1.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p</w>’<w n="1.6">t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>’ <w n="1.7">L<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> !… <w n="1.8"><seg phoneme="e" type="vs" value="1" rule="133">e</seg>h</w> <w n="1.9">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="1.10">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.11">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="1.12">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.13">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="2" num="1.2">— <w n="2.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <hi rend="ital"><w n="2.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></hi>, <w n="2.4">m</w>’<w n="2.5"><seg phoneme="a" type="vs" value="1" rule="145">a</seg>m</w>’ <w n="2.6">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w>’ <w n="2.9">pl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.11">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="3" num="1.3"><w n="3.1">R</w>’<w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="3.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <hi rend="ital"><w n="3.9">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></hi>…</l>
						<l n="4" num="1.4">— <w n="4.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>ss</w> ! <w n="4.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss</w>’ <w n="4.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="4.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mme</w> <w n="4.7">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.10">g</w>’<w n="4.11">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
						<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p</w>’<w n="5.3">t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>’<w n="5.4">s</w> !… — <w n="5.5"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w>, <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="5.7">l</w>’<w n="5.8">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ! <w n="5.9">c</w>’<w n="5.10"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.11"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.12">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> :</l>
						<l part="I" n="6" num="1.6">— <w n="6.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.3">cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.4">h<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> ?… — <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w>… </l>
						<l part="F" n="6" num="1.6"><w n="6.6"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="6.7">l</w>’<w n="6.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.9">p</w>’<w n="6.10">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="6.11">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>gr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<closer>
						<placeName>Saint-Mâlo-de-l’Isle</placeName>.
					</closer>
				</div></body></text></TEI>