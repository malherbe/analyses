<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB15">
					<head type="main">GENTE DAME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="1.5"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="1.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">D<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> !…</l>
						<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bst<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="5" num="1.5"><w n="5.1">M<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.4">sp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ct<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><space quantity="8" unit="char"></space><w n="6.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> !…</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <hi rend="ital"><w n="7.3">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">N<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></hi></l>
						<l n="8" num="2.2"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <hi rend="ital"><w n="8.3">Ch<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">Pr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></hi>,</l>
						<l n="9" num="2.3"><space quantity="8" unit="char"></space><w n="9.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="9.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
						<l n="10" num="2.4"><w n="10.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.6">S<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="11" num="2.5"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="11.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="11.5">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="12" num="2.6"><space quantity="8" unit="char"></space> — <w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>. —</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <hi rend="ital"><w n="13.4">Fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></hi>,</l>
						<l n="14" num="3.2"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="14.2">n</w>’<w n="14.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.6">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="15" num="3.3"><space quantity="8" unit="char"></space><w n="15.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !…</l>
						<l n="16" num="3.4"><w n="16.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="16.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="17" num="3.5"><w n="17.1">P<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="17.2">R<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>dt</w> <w n="17.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="3.6"><space quantity="8" unit="char"></space><w n="18.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Qu</w>’<w n="19.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.4">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="4.2"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="20.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="20.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">l</w>’<w n="20.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="21" num="4.3"><space quantity="8" unit="char"></space><w n="21.1">Fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="22" num="4.4"><w n="22.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.2">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="22.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>-<w n="22.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="23" num="4.5"><w n="23.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="23.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.5">l</w>’<w n="23.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="4.6"><space quantity="8" unit="char"></space><w n="24.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">— <w n="25.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="25.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ! — <w n="25.3">J</w>’<w n="25.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.6">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="26" num="5.2"><w n="26.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.2">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="26.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="27" num="5.3"><space quantity="8" unit="char"></space><w n="27.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="27.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,</l>
						<l n="28" num="5.4"><w n="28.1">B<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="28.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="29" num="5.5"><w n="29.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.2">st<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.5">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="30" num="5.6"><space quantity="8" unit="char"></space> — <w n="30.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> !…</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1">— <w n="31.1">Qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>-<w n="31.2">d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="?" type="va" value="1" rule="162">en</seg>t</w> — <w n="31.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="6.2"><w n="32.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="32.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="32.3">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="32.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.5">l</w>’<w n="32.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="33" num="6.3"><space quantity="8" unit="char"></space><w n="33.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="33.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
						<l n="34" num="6.4">— <w n="34.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="34.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> — <w n="34.3"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> ! <w n="34.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="35" num="6.5"><w n="35.1">C</w>’<w n="35.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="35.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="35.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="35.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="35.6"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="36" num="6.6"><space quantity="8" unit="char"></space><w n="36.1">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="36.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="37.2">gr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="37.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="38" num="7.2"><w n="38.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="38.2">g<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="38.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.4">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="39" num="7.3"><space quantity="8" unit="char"></space><w n="39.1">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
						<l n="40" num="7.4"><w n="40.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="40.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="40.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="41" num="7.5"><w n="41.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="41.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="41.4">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="42" num="7.6"><space quantity="8" unit="char"></space><hi rend="ital"><w n="42.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w>-<w n="42.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> !</hi></l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><w n="43.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="43.2">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="43.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="43.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="43.5">h<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="44" num="8.2"><w n="44.1">J<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="44.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="44.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="44.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="45" num="8.3"><space quantity="8" unit="char"></space><w n="45.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="45.2">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
						<l n="46" num="8.4"><w n="46.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="46.2">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="46.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="46.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="47" num="8.5"><w n="47.1">L<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="47.2">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="47.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="47.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="47.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="48" num="8.6"><space quantity="8" unit="char"></space><w n="48.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="48.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1"><w n="49.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="49.2">B<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="49.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="49.4">D<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="50" num="9.2"><w n="50.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="50.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="50.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="50.4">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="50.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="51" num="9.3"><space quantity="8" unit="char"></space><w n="51.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="51.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> …</l>
						<l n="52" num="9.4"><w n="52.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="52.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="52.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="52.4">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="52.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="53" num="9.5"><w n="53.1">F<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="53.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="53.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="53.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="53.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="53.6">dr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="54" num="9.6"><space quantity="8" unit="char"></space><w n="54.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="54.2">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1"><w n="55.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="55.2"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="55.3">m<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="55.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="56" num="10.2"><w n="56.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>-<w n="56.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="56.3">Br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="57" num="10.3"><space quantity="8" unit="char"></space><w n="57.1">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !</l>
						<l n="58" num="10.4"><w n="58.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sch<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="58.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="58.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="59" num="10.5"><w n="59.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rgu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="59.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="59.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="59.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="59.5">br<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="60" num="10.6"><space quantity="8" unit="char"></space><w n="60.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="60.2">gu<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> !</l>
					</lg>
				</div></body></text></TEI>