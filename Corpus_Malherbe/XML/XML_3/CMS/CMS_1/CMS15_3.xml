<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS15">
				<head type="main">AUSCULTATION</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">O</seg>MM<seg phoneme="ɑ̃" type="vs" value="1" rule="369">EN</seg>T</w> ! <w n="1.2">C</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="1.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> ?</l>
					<l n="2" num="1.2">— « <w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.2"><seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="2.3">mʼs<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="2.4">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l</w>, <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.6">j</w>’<w n="2.7">m</w>’<w n="2.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="3" num="1.3">« <w n="3.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="3.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="3.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w>’ <w n="3.5">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v</w>’ <w n="3.6">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="4" num="1.4">« <w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.2">qu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="4.3">qu</w>’<w n="4.4">j</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.6">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="4.7">b<seg phoneme="ɛ̃" type="vs" value="1" rule="221">en</seg></w> <w n="4.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.9">l</w>’<w n="4.10">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">« <w n="5.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rv<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.2">qu</w>’<w n="5.3">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.4">sʼr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.6">qu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>qu</w>’ <w n="5.7">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> !</l>
					<l n="6" num="2.2">« <w n="6.1">Ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">m</w>’<w n="6.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.5">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="6.6">ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.7">m</w>’<w n="6.8">gr<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>il</w>’ <w n="6.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.10">l</w>’<w n="6.11">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tʼ<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="7" num="2.3">« <w n="7.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.2">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm</w>’ <w n="7.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="7.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ; <w n="7.5">j</w>’<w n="7.6">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="7.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="8" num="2.4">«<w n="8.1">Pʼt</w>-<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="8.3">b<seg phoneme="ɛ̃" type="vs" value="1" rule="221">en</seg></w> <w n="8.4">qu</w>’<w n="8.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="8.6">m</w>’<w n="8.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.9">l</w>’<w n="8.10">f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="8.11">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w>, »</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">— « … <w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> ! <w n="9.3">B<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="9.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !… »</l>
					<l n="10" num="3.2"><w n="10.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="10.2">l</w>’<w n="10.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="11" num="3.3"><w n="11.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.2">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.3">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="11.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="11.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="11.6">qu</w>’<w n="11.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nh<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> ;</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="12.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.5">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="13" num="4.2"><w n="13.1">L</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">pl<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w></l>
					<l n="14" num="4.3"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="14.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>