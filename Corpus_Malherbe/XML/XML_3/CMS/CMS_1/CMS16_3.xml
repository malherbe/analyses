<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES SONNETS DU DOCTEUR</title>
				<title type="medium">Édition électronique</title>
				<author key="CMS">
					<name>
						<forename>Georges</forename>
						<surname>CAMUSET</surname>
					</name>
					<date from="1840" to="1885">1840-1885</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>522 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CMS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/BIUSante_80646</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Sonnets du docteur</title>
								<author>Georges Camuset</author>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Chez la plupart des libraires</publisher>
									<date when=" 1884">1884</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Sonnets du docteur</title>
						<author>Georges Camuset</author>
						<idno type="URL">https ://www.google.fr/books/edition/Les_sonnets_du_docteur/bsv60_A0jdUC</idno>
						<imprint>
							<pubPlace>PARIS</pubPlace>
							<publisher>Chez la plupart des libraires</publisher>
							<date when=" 1884">1884</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poémes absents de l’édition numérique initiale ("Les Préservatifs" et "Du Signe certain de la mort") ont été ajoutés à partir du texte disponible sur Google books.</p>
				<p>Les notes qui suivent les titres dans la table des matières ont été mis en fin de poème.</p>
				<p>La lettre manuscrite de Charles Monselet n’est pas incluse dans la présente édition</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="CMS16">
				<head type="main">STRABISME</head>
				<opener>
					<salute>A M<hi rend="sup">lle</hi> C…, artiste dramatique.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="1.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="1.5">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.8">l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="2.4">l</w>’<w n="2.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rb<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w></l>
					<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="3.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> :</l>
					<l n="4" num="1.4"><w n="4.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.3">s</w>’<w n="4.4">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.6">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="4.8">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="6.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="6.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.8">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.10">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
					<l n="7" num="2.3"><w n="7.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.4">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>.</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="8.2">l</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="8.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="8.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">D<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="9.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="9.4">d</w>’<w n="9.5"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="9.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="9.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.10">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
					<l n="10" num="3.2"><w n="10.1">C<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.5">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>. — <w n="10.8">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
					<l n="11" num="3.3"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.4">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.6">v<seg phoneme="ø" type="vs" value="1" rule="248">œu</seg></w> <w n="11.7">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="11.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> — <w n="12.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.5">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="12.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.8">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> —</l>
					<l n="13" num="4.2"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="412">Ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="13.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="4.3"><w n="14.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="14.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w>, <w n="14.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="14.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.10">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
				</lg>
			</div></body></text></TEI>