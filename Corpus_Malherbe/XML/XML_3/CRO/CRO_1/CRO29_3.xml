<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO29">
					<head type="main">Scherzo</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="1.2">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="1.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="2.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="3.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="4" num="1.4"><w n="4.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>,<w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="2.2"><w n="6.1">J<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="6.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="7.3">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.4">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">L<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="8.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w>, <w n="8.3">l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s</w>, <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="8.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> !</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.2">j</w>’<w n="9.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">l</w>’<w n="9.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">d<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="11" num="3.3"><w n="11.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>squ</w>’<w n="11.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="11.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.4">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="12.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="12.5">fl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.6">s<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="13.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="14.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="14.5">n</w>’<w n="14.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="14.7">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="14.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ?</l>
						<l n="15" num="4.3"><w n="15.1">Qu</w>’<w n="15.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="15.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.5">t</w>’<w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="15.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><w n="16.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">d</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.5">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="18" num="5.2"><w n="18.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.5">pr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="20" num="5.4"><w n="20.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">m<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.5">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w>,</l>
						<l n="22" num="6.2"><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="22.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.5">l</w>’<w n="22.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="22.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>,</l>
						<l n="23" num="6.3"><w n="23.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="23.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w></l>
						<l n="24" num="6.4"><w n="24.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="24.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="24.3">n<seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="24.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.2">n</w>’<w n="25.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="25.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="25.5">l</w>’<w n="25.6">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.8">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="26" num="7.2"><w n="26.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="26.2">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="26.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.4">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="26.5">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>.</l>
						<l n="27" num="7.3"><w n="27.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="27.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="27.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="28" num="7.4"><w n="28.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="28.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="29.2">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="29.3">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="8.2"><w n="30.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="30.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="30.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="30.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
						<l n="31" num="8.3"><w n="31.1">L<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="31.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.4">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="8.4"><w n="32.1">Fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="32.3">l</w>’<w n="32.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="32.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rh<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Qu</w>’<w n="33.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="33.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="33.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="33.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="34" num="9.2"><w n="34.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="34.2">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="34.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="34.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="34.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.6">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="35" num="9.3"><w n="35.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="35.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="35.3">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="35.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="35.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="35.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="36" num="9.4"><w n="36.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pp<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="36.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.3">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ?</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="37.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="37.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="37.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="37.7">p<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="38" num="10.2"><w n="38.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="38.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rb<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="38.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="39" num="10.3"><w n="39.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>-<w n="39.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="39.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="39.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="39.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="40" num="10.4"><w n="40.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="40.4">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="40.5">c</w>’<w n="40.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="40.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>.</l>
					</lg>
				</div></body></text></TEI>