<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO34">
					<head type="main">A une attristée d’ambition</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="1.2">h<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="1.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>pl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="2" num="1.2"><space quantity="12" unit="char"></space><w n="2.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="wa" type="vs" value="1" rule="188">ua</seg>rs</w>,</l>
						<l n="3" num="1.3"><w n="3.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="3.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.3">d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="3.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="3.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.7"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.8">fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><space quantity="12" unit="char"></space><w n="4.1">L</w>’<w n="4.2"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">V<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="5.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="5.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="5.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.5">h<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="5.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="5.8">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.9">d</w>’<w n="5.10"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="6" num="2.2"><space quantity="12" unit="char"></space><w n="6.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.2"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xpl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ;</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="7.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="8" num="2.4"><space quantity="12" unit="char"></space><w n="8.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="8.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.4">fr<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="9.2">h<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w>, <w n="9.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.8">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="10" num="3.2"><space quantity="12" unit="char"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
						<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="11.4">s</w>’<w n="11.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="11.8">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg><space quantity="12" unit="char"></space>ch<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.4">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="13.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.7">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="13.8">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="13.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.2"><space quantity="12" unit="char"></space><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="14.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
						<l n="15" num="4.3"><w n="15.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="15.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="15.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.6"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><space quantity="12" unit="char"></space><w n="16.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.3">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="16.4">d</w>’<w n="16.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="16.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccr<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="5.2"><space quantity="12" unit="char"></space><w n="18.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="18.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="18.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.8">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
						<l n="19" num="5.3"><w n="19.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="19.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="19.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.8">s</w>’<w n="19.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="5.4"><space quantity="12" unit="char"></space><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="20.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="21.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="21.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="21.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="21.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.8"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="21.9">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="6.2"><space quantity="12" unit="char"></space><w n="22.1">N</w>’<w n="22.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.3">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="22.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="22.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
						<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="23.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="23.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.9">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="6.4"><space quantity="12" unit="char"></space><w n="24.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="24.2"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="24.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rdr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="25.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="25.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="25.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="25.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.6">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="7.2"><space quantity="12" unit="char"></space><w n="26.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="26.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="26.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="26.5">v<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="26.6">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="27.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="27.3">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="27.6">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="27.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.8">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="27.9">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="27.10">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="7.4"><space quantity="12" unit="char"></space><w n="28.1">S<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="28.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="28.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="28.6">n<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
				</div></body></text></TEI>