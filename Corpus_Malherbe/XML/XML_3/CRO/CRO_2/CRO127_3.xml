<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO127">
					<head type="main">A la plus belle</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="1.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">l</w>’<w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.5">v<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="1.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>,</l>
						<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="3" num="1.3">(<w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="3.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !)</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="4.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="4.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">l</w>’<w n="4.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>,</l>
						<l n="6" num="2.2"><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="6.2">m</w>’<w n="6.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="6.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="6.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : « <w n="6.6">C</w>’<w n="6.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="6.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.10">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ! »</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="7.3">j</w>’<w n="7.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="7.5">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.8">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w></l>
						<l n="8" num="2.4"><w n="8.1">C<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>h</w> ! <w n="9.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.6">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> !</l>
						<l n="10" num="3.2"><w n="10.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="10.3">cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>sc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="11" num="3.3"><w n="11.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">qu</w>’<w n="11.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="11.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
						<l n="12" num="3.4"><w n="12.1">J</w>’<w n="12.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="12.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.5">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="12.6">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.7">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>,</l>
						<l n="14" num="4.2"><w n="14.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
						<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">l</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>h</w> ! <w n="15.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">l</w>’<w n="15.8"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
						<l n="16" num="4.4"><w n="16.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="16.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="16.6">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.7">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J</w>’<w n="17.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="17.3">h<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
						<l n="18" num="5.2"><w n="18.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="18.2">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l</w> <w n="18.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.5">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="5.3"><w n="19.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.2">l</w>’<w n="19.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="19.4">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
						<l n="20" num="5.4"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="20.5">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="21.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="21.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>,</l>
						<l n="22" num="6.2"><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="425">Ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, (<w n="22.2">l</w>’<w n="22.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>)</l>
						<l n="23" num="6.3"><w n="23.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="23.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">l<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w></l>
						<l n="24" num="6.4"><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="24.2">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="24.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.5">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="24.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>