<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO131">
					<head type="main">Nocturne</head>
					<div type="section" n="1">
						<head type="main">Elle</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="1.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.7">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">t</w>’<w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.4">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w>, <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="2.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.11">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.12">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="2">
							<l n="3" num="2.1"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="3.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="3.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="3.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>.</l>
							<l n="4" num="2.2"><w n="4.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.2">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="4.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.5">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="4.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.7">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.8">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.9"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w></l>
						</lg>
						<lg n="3">
							<l n="5" num="3.1"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.5">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="5.7">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">l</w>’<w n="5.10"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="6" num="3.2"><w n="6.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.3">qu</w>’<w n="6.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.6">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.8">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="6.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.10">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.11">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="4">
							<l n="7" num="4.1"><w n="7.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="7.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>. <w n="7.3">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">t<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w>.</l>
							<l n="8" num="4.2"><w n="8.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="8.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="8.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="8.7">h<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="8.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="8.9">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.10">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
						</lg>
						<lg n="5">
							<l n="9" num="5.1"><w n="9.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="9.2">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="9.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="10" num="5.2"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">qu</w>’<w n="10.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d</w>, <w n="10.6">c</w>’<w n="10.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.11">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.12">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="6">
							<l n="11" num="6.1"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="11.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="11.7">pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>,</l>
							<l n="12" num="6.2"><w n="12.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="12.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="12.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="12.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="12.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="12.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>.</l>
						</lg>
						<lg n="7">
							<l n="13" num="7.1"><w n="13.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">t</w>’<w n="13.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="13.5"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="13.6">ch<seg phoneme="a" type="vs" value="1" rule="340">â</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="13.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>-<w n="13.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.9">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.10">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
							<l n="14" num="7.2"><w n="14.1">C</w>’<w n="14.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.6">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="14.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.8">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.10">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.11">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="8">
							<l n="15" num="8.1"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="15.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> !… <w n="15.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg></w> <w n="15.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.8">d</w>’<w n="15.9"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>.</l>
							<l n="16" num="8.2"><w n="16.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">t</w>’<w n="16.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="16.4">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="16.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.9">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.11">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> ?</l>
						</lg>
						<lg n="9">
							<l n="17" num="9.1"><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="17.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>. <w n="17.3"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="17.4">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="17.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w>. <w n="17.7">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.8">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="17.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.10">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="18" num="9.2"><w n="18.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">t</w>’<w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="18.4">j<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="18.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="18.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.9">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.11">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						</lg>
						<lg n="10">
							<l n="19" num="10.1"><w n="19.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">l</w>’<w n="19.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>… <w n="19.7">C</w>’<w n="19.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="19.9">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.11">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
							<l n="20" num="10.2"><w n="20.1">C</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="20.5">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="20.6">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="20.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>.</l>
						</lg>
						<lg n="11">
							<l n="21" num="11.1"><w n="21.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="21.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="21.6">d</w>’<w n="21.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="21.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="21.10">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>, <w n="21.11">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.12">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
							<l n="22" num="11.2"><w n="22.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="22.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="22.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.4">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg></w>… <w n="22.6">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">m</w>’<w n="22.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ? <w n="22.9">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.10">t</w>’<w n="22.11"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="12">
							<l n="23" num="12.1"><w n="23.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.2">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="23.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="23.6">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="23.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.9">b<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
							<l n="24" num="12.2"><w n="24.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="24.7">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="24.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.9">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d</w>.</l>
						</lg>
						<lg n="13">
							<l n="25" num="13.1"><w n="25.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="25.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="25.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="25.9">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
							<l n="26" num="13.2"><w n="26.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="26.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="26.7">n</w>’<w n="26.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="26.9">qu</w>’<w n="26.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.11">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						</lg>
						<lg n="14">
							<l n="27" num="14.1"><w n="27.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>. <w n="27.4">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="27.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="27.6">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="27.8">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>.</l>
							<l n="28" num="14.2"><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="28.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>. <w n="28.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> — <w n="28.7"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="28.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="28.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>.</l>
						</lg>
						<lg n="15">
							<l n="29" num="15.1"><w n="29.1">J</w>’<w n="29.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="29.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="29.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.5">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="29.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="29.7">qu</w>’<w n="29.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.9">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="29.10">d</w>’<w n="29.11"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
							<l n="30" num="15.2"><w n="30.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>. <w n="30.2">S<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="30.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="30.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.8">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="30.9">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="30.10">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						</lg>
						<lg n="16">
							<l n="31" num="16.1"><w n="31.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>. <w n="31.2">Qu</w>’<w n="31.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="31.4">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="31.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="31.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="31.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.10">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ?</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">Lui</head>
						<lg n="1">
							<l n="32" num="1.1"><w n="32.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="32.2">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>… <w n="32.3">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="32.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="32.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
						</lg>
					</div>
				</div></body></text></TEI>