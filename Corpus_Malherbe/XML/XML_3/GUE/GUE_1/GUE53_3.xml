<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">A ÉMile Krantz</head><div type="poem" key="GUE53">
					<head type="number">LIII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w> <w n="1.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w>, <w n="1.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w> <w n="1.6">d</w>’<w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="2.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="2.3">l</w>’<w n="2.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ?</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="3.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">d<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="5.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="5.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1"><seg phoneme="i" type="vs" value="1" rule="467">I</seg>n<seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="2.3"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="9.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g</w> <w n="9.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="342">À</seg></w> <w n="10.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
						<l n="11" num="3.3"><w n="11.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.7">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="12.3">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
					</lg>
				</div></body></text></TEI>