<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><div type="poem" key="GUE6" rhyme="none">
					<head type="number">VI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɛ" type="vs" value="1" rule="305">Ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.8">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="2.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">en</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w></l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="4.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="j" type="sc" value="0" rule="475">ï</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.7">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="6" num="2.2"><w n="6.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.2"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>. <w n="6.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="6.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="7" num="2.3"><w n="7.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.2">t</w>’<w n="7.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ffr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.9"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="8" num="2.4"><w n="8.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="8.4">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="8.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.2">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="9.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="9.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="9.7">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> : <w n="9.8">pr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="10.2">qu</w>’<w n="10.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.6">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.9">pl<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-3">e</seg>nt</w></l>
						<l n="11" num="3.3"><w n="11.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">fr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w>, <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ccr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.9">r<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="12.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="12.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.8">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.9">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.10">c<seg phoneme="œ" type="vs" value="1" rule="389">oeu</seg>r</w></l>
						<l n="13" num="4.2"><w n="13.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="13.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="13.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="13.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="13.8">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.3"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="14.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					</lg>
				</div></body></text></TEI>