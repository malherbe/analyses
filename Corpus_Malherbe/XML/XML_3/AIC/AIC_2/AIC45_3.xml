<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC45">
					<head type="number">XI</head>
					<head type="main">VISITE À L’ARSENAL DE TOULON</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="1.6">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.7">d</w>’<w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>cl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="2" num="1.2"><w n="2.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.2">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="2.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="2.4">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="2.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="2.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.9"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>,</l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="3.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="3.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.9">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="4.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.4">f<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.6">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="4.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="5.5">r<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="5.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.8">s</w>’<w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="6.5">v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.7">l</w>’<w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rs<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w>…</l>
						<l n="7" num="2.3"><w n="7.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="7.2">d</w>’<w n="7.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ! <w n="7.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="7.5">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="7.6"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="7.7">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.9">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="7.10">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.11">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="8" num="2.4"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">Sc<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="378">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="8.5">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="8.6">tr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.8">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="9.2">j</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="9.6">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.7">j</w>’<w n="9.8"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="9.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.10"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="10" num="3.2"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.3"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, <w n="10.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="10.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.10">l</w>’<w n="10.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="11" num="3.3"><w n="11.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="11.4">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.7">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="11.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1">« <w n="12.1"><seg phoneme="ø" type="vs" value="1" rule="398">Eu</seg>x</w> <w n="12.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>ls</w> <w n="12.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="12.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.5">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg>s</w> <w n="12.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="4.2"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="13.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="13.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="200">om</seg>s</w> <w n="13.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.9">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="14" num="4.3"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="14.6">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.10">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> ! »</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>