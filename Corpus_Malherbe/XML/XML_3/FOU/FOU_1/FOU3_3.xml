<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SIX PSEUDO-SONNETS <lb></lb>TRUCULENTS ET ALLÉGORIQUES</head><div type="poem" key="FOU3">
					<head type="main">PSEUDO-SONNET <lb></lb> PLUS SPÉCIALEMENT TRUCULENT ET ALLÉGORIQUE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Nargue Legrand-du-Saule et sois un Grand-du-Cédre.
								</quote>
								<bibl>
									<name>X. FLUMEN</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="1.2">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> : « <w n="1.3">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.4">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.7">chr<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>l</w> !</l>
						<l n="2" num="1.2"><w n="2.1">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="2.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.4">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="2.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ldsp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>th</w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.8">d</w>’<w n="2.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">p<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="3.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="3.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="3.8">d</w>’<w n="3.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.10">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.2">c</w>’<w n="4.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.4">d</w>’<hi rend="ital"><w n="4.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="4.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w></hi> <w n="4.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">Z<seg phoneme="ø" type="vs" value="1" rule="403">EU</seg>S</w> <w n="4.9">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.11">m<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.12">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ! <ref target="1" type="noteAnchor">1</ref></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <hi rend="ital"><w n="5.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>§ <w n="5.3">l</w>’<w n="5.4"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>B<seg phoneme="i" type="vs" value="1" rule="468">I</seg>S</w> <w n="5.5">N<seg phoneme="wa" type="vs" value="1" rule="420">OI</seg>R</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> §<w n="5.8">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w></hi> <w n="5.9">l</w>’<w n="5.10"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>T<seg phoneme="wa" type="vs" value="1" rule="420">OI</seg>L<seg phoneme="ə" type="ef" value="1" rule="e-5">E</seg></w></l>
						<l n="6" num="2.2"><hi rend="ital"><w n="6.1">G<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>§ <w n="6.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> §<w n="6.3">P<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="6.4">Ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></hi> <w n="6.5">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="6.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> :</l>
						<l n="7" num="2.3"><w n="7.1">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="7.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">h<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.5">l</w>’<w n="7.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.7">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ;</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <hi rend="ital"><w n="8.2">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rz<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w></hi> <w n="8.4">j</w>’<w n="8.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.6">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="8.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.10">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <hi rend="ital"><w n="9.1">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w></hi> ! <w n="9.2">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="9.3">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="9.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.5">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="9.7">qu</w>’<w n="9.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.9">pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.10"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.11">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> ! !</l>
						<l n="10" num="3.2">« <hi rend="ital"><w n="10.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></hi> ! ! ! ! <w n="10.2"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="10.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="10.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ss<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> ! ! !</l>
						<l n="11" num="3.3">« <w n="11.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="11.2">j</w>’<w n="11.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="11.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <hi rend="ital"><w n="11.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">L<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w></hi> <w n="11.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.9">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.10">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1">« <w n="12.1">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.3">s<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.5">j<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="12.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !… »</l>
						<l n="13" num="4.2"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="13.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.8">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="14" num="4.3"><w n="14.1">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> : <hi rend="ital"><w n="14.5">Qu</w>’<w n="14.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.8">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.9"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.11">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></hi> ! !</l>
					</lg>
					<closer>
						<note type="footnote" id="1">
							On tient à affirmer hautement qu’il n’est fait ici nulle allusion déplacée à l’éminent maëstro Ch. M. Widor. <lb></lb>
							(Note de l’Auteur.)
						</note>
					</closer>
				</div></body></text></TEI>