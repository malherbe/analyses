<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA NÉGRESSE BLONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="FOU">
					<name>
						<forename>Georges</forename>
						<surname>FOUREST</surname>
					</name>
					<date from="1864" to="1945">1864-1945</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1172 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FOU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>La Négresse blonde</title>
						<author>Georges Fourest</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/La_N%C3%A9gresse_blonde_(recueil)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Négresse blonde</title>
								<author>Georges Fourest</author>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1269960g</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A MESSEIN</publisher>
									<date when="1909">1909</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1909">1909</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-02" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-02" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SIX PSEUDO-SONNETS <lb></lb>TRUCULENTS ET ALLÉGORIQUES</head><div type="poem" key="FOU6">
					<head type="main">PSEUDO-SONNET <lb></lb>IMBRIAQUE ET DÉSESPÉRÉ</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Que fait pourtant un pauvre ivrogne ? <lb></lb>
									Il se couche et n’occit personne !
								</quote>
								<bibl>
									<name>Olivier BASSELIN</name>.
								</bibl>
							</cit>
							<cit>
								<quote>
									Let us have wine : and women, mirthand laughter : <lb></lb>
									Sermons and soda-water the day after ! <lb></lb>
									Man, being reasonable, must get drunk ; <lb></lb>
									The best of life is but intoxication !
								</quote>
								<bibl>
									<name>Lord BYRON</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">G<seg phoneme="i" type="vs" value="1" rule="140">i</seg>n</w> ! <w n="1.2">H<seg phoneme="i" type="vs" value="1" rule="493">y</seg>dr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ! ! <w n="1.3">K<seg phoneme="y" type="vs" value="1" rule="453">u</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ! ! ! <w n="1.4">W<seg phoneme="i" type="vs" value="1" rule="497">i</seg>sk<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w> ! ! ! ! <w n="1.5">Z<seg phoneme="i" type="vs" value="1" rule="493">y</seg>th<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ! ! ! !</l>
						<l n="2" num="1.2"><w n="2.1">j</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="2.3">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> ! <w n="2.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="2.7">s<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>l</w> <w n="2.8">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.10">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="3" num="1.3"><w n="3.1">l</w>’<w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rch<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>c</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">W<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.5">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="3.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">d</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.4">J<seg phoneme="o" type="vs" value="1" rule="444">o</seg>h<seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sb<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>-<w n="4.7">ch<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="4.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.9">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">cr<seg phoneme="a" type="vs" value="1" rule="341">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lp<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="361">e</seg>m</w> <w n="5.7">K<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w>-<w n="5.8">G<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>-<w n="5.9">R<seg phoneme="u" type="vs" value="1" rule="FOU6_1">oo</seg></w></l>
						<l n="6" num="2.2"><w n="6.1">B<seg phoneme="u" type="vs" value="1" rule="FOU6_1">oo</seg></w>-<w n="6.2">L<seg phoneme="u" type="vs" value="1" rule="FOU6_1">oo</seg></w>, <w n="6.3">j</w>’<w n="6.4"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.9">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="6.10">d</w>’<w n="6.11"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="7" num="2.3"><w n="7.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="7.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.4">gr<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g</w> <w n="7.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">l</w>’<w n="7.7"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>lf<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="8" num="2.4"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.7">d</w>’<w n="8.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.9">k<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="u" type="vs" value="1" rule="FOU6_1">oo</seg></w> ! <ref target="1" type="noteAnchor">1</ref></l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">(<w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.2">p<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.8">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
						<l n="10" num="3.2"><w n="10.1">c</w>’<w n="10.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.4">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.5">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! <w n="10.6">J</w>’<w n="10.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.8">qu<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.9">z<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="11" num="3.3"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.4">d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>g</w>-<w n="11.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">m</w>’<w n="11.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.10">v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="11.11"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="11.12">tr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> !)</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w> <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="12.3">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.4">n<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="12.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">P<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.8">d</w>’<w n="12.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bs<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="13" num="4.2"><w n="13.1">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="13.3">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.7">J<seg phoneme="ɑ̃" type="vs" value="1" rule="309">ean</seg></w> <w n="13.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
						<l n="14" num="4.3"><w n="14.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ! <w n="14.3">n</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="14.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.6">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> !</l>
					</lg>
					<closer>
						<note type="footnote" id="1">
							Un kanguroo femelle, bien entendu. <lb></lb>
							(Note de l’auteur.)
						</note>
					</closer>
				</div></body></text></TEI>