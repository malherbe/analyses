<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Inattentions et Sollicitudes</title>
				<title type="medium">Édition électronique</title>
				<author key="FRN">
					<name>
						<forename>Maurice Étienne</forename>
						<surname>LEGRAND</surname>
						<addName type="pen_name">FRANC-NOHAIN</addName>
					</name>
					<date from="1872" to="1934">1872-1934</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Préparation du texte, encodage XML</resp>
					<name id="ML">
						<forename>Manon</forename>
						<surname>Lavergne</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Validation du document, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>649 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FRN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Les Inattentions et Sollicitudes</title>
						<author>Franc-Nohain</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5438658j.texteImage</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Inattentions et Sollicitudes</title>
								<author>Franc-Nohain</author>
								<repository></repository>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>L. VANIER</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1894">1894</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Seules les parties versifiées du texte ont été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ) ont été restituées.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-06-04" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRN7">
		<head type="main">BERCEUSE OBSCÈNE</head>
			<lg n="1">
				<l n="1" num="1.1"><w n="1.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="1.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.4">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="1.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
				<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.4">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="2.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="2.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
			</lg>
			<div type="section" n="1">
			<head type="number">I</head>
			<lg n="1">
				<l n="3" num="1.1"><space unit="char" quantity="12"></space><w n="3.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="3.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="4" num="1.2"><space unit="char" quantity="6"></space><w n="4.1">B<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.2">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.5">g<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
				<l n="5" num="1.3"><space unit="char" quantity="6"></space><w n="5.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>n<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="5.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.4">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				<l n="6" num="1.4"><space unit="char" quantity="12"></space>— <w n="6.1">T<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> —</l>
				<l n="7" num="1.5"><space unit="char" quantity="6"></space><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.3">qu</w>’<w n="7.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.7">n<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ?</l>
				<l n="8" num="1.6"><space unit="char" quantity="6"></space><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="8.6">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>.</l>
				<l n="9" num="1.7"><space unit="char" quantity="6"></space><w n="9.1">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="9.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="9.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="9.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
				<l n="10" num="1.8"><space unit="char" quantity="12"></space>— <w n="10.1">T<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> —</l>
			</lg>
			</div>
			<div type="section" n="2">
			<head type="number">II</head>
			<lg n="1">
				<l n="11" num="1.1"><space unit="char" quantity="12"></space><w n="11.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.2">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>,</l>
				<l n="12" num="1.2"><space unit="char" quantity="6"></space><w n="12.1">Ç<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p</w> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ;</l>
				<l n="13" num="1.3"><space unit="char" quantity="6"></space><w n="13.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>n<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="13.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				<l n="14" num="1.4"><space unit="char" quantity="12"></space>— <w n="14.1">T<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> —</l>
				<l n="15" num="1.5"><space unit="char" quantity="6"></space><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.3">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="15.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				<l n="16" num="1.6"><space unit="char" quantity="6"></space><w n="16.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.3">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w>, <w n="16.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="16.6">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> ;</l>
				<l n="17" num="1.7"><w n="17.1">M<seg phoneme="œ" type="vs" value="1" rule="151">on</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="397">eu</seg>r</w> <w n="17.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.5">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.6">d</w>’<w n="17.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
				<l n="18" num="1.8"><space unit="char" quantity="12"></space>— <w n="18.1">T<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> —</l>
			</lg>
			<lg n="2">
				<l n="19" num="2.1"><w n="19.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="19.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.6">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ec" value="0" rule="e-35">e</seg></w> <w n="19.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
				<l n="20" num="2.2"><w n="20.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="20.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.5">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="20.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
			</lg>
			</div>
	</div></body></text></TEI>