<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Troisième partie</head><head type="sub_part">(1871-1885)</head><div type="poem" key="DUC106">
					<head type="main">La Reddition de Paris</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="1.3">c</w>’<w n="1.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.6">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="1.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.8">l</w>’<w n="1.9"><seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.10"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="445">û</seg></w> <w n="2.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rb<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> !</l>
						<l n="3" num="1.3"><w n="3.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="3.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="3.3">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg>s</w>, <w n="3.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="3.7">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l</w>’<w n="4.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="5.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.10">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="6.2">qu</w>’<w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.5">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
						<l n="7" num="2.3"><w n="7.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="8.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="8.3">v<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>f</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">s</w>’<w n="9.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.8">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.3">t</w>’<w n="11.4"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lt<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> ; <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.7">qu</w>’<w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.9">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.2">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.3">r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="13.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ! <w n="13.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="13.4">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="13.5">f<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="13.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ct<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.2">Pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="14.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">B<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> !</l>
						<l n="15" num="4.3"><w n="15.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.2">s<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t</w> <w n="15.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="15.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="15.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.10">s<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w>, <w n="16.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="17.2">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> ! <w n="17.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="17.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.5">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> ! — <w n="17.6">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="17.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.9">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ls</w> <w n="18.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="18.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="18.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ?</l>
						<l n="19" num="5.3"><w n="19.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="19.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="19.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="19.4">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="19.5">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.8">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="19.9">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">N</w>’<w n="20.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="20.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="20.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="20.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> ?…</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="21.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="21.3">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="21.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ts</w>, <w n="21.5">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.7">st<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="22.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
						<l n="23" num="6.3"><w n="23.1">Cr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> : — « <w n="23.2">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="23.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ! » — <w n="23.4">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="23.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="23.6">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="24.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="24.3"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.4">tr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>ph<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="25.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="25.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="25.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="25.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ct<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> ; <w n="25.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="25.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="25.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : — « <w n="25.9">Qu</w>’<w n="25.10"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="26" num="7.2"><space unit="char" quantity="4"></space><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">f<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ?</l>
						<l n="27" num="7.3"><w n="27.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="27.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="27.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="27.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, <w n="27.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.6">Fr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.7">n</w>’<w n="27.8"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="27.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="27.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="28.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> ! <w n="28.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="28.4">l</w>’<w n="28.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ! »</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="29.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="29.5">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="30" num="8.2"><space unit="char" quantity="12"></space><w n="30.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="30.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="30.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w></l>
						<l n="31" num="8.3"><w n="31.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="31.2">l</w>’<w n="31.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="31.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.7">m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="32.2">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.3">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="32.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="32.5">G<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> ?… »</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1871">29 janvier 1871</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>