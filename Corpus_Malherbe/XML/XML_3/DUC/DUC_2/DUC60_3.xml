<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC60">
				<head type="main">Benjamine</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="1.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.6">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="1.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>,</l>
					<l n="2" num="1.2"><w n="2.1">T<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>, <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.6">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="2.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.8">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="3.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="3.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>,</l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="4.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="4.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.6">qu</w>’<w n="4.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.9">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="5.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.6">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.7">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="5.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w>,</l>
					<l n="6" num="1.6"><w n="6.1">F<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rp<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.5">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.4">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w>, <w n="7.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.6">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="7.7">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w> <w n="7.8">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.2"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="8.2">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="8.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="8.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w></l>
					<l n="9" num="2.3"><w n="9.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="9.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w>, <w n="9.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="9.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.9">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.3">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="10.4">l</w>’<w n="10.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="11" num="2.5"><w n="11.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.4">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w>, <w n="11.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.6">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.7">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w> <w n="11.8">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="2.6"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.3">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="12.4">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.5">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.6">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="13.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="13.5">pl<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="14" num="3.2"><w n="14.1">L<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="14.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="15" num="3.3"><w n="15.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="15.2">m<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ttr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="15.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.6">ch<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="16" num="3.4"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="16.2">r<seg phoneme="i" type="vs" value="1" rule="493">y</seg>thm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.6">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="17" num="3.5"><w n="17.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.5">pl<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="17.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.7">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="18" num="3.6"><w n="18.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.2">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="18.4">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w> <w n="18.5">pl<seg phoneme="ø" type="vs" value="1" rule="404">eu</seg>vr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="18.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1886">1886</date>
					</dateline>
				</closer>
			</div></body></text></TEI>