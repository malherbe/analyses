<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI56">
					<head type="main">À l’Avenir</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">P<seg phoneme="u" type="vs" value="1" rule="425">OU</seg>RQU<seg phoneme="wa" type="vs" value="1" rule="281">OI</seg></w> <w n="1.2">m</w>’<w n="1.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="1.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="2.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="2.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="2.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.9">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="3" num="1.3"><w n="3.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>s</w> <w n="3.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.7">s</w>’<w n="3.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="3.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.10">c<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="4.2">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="4.6">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>-<w n="4.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> ?</l>
						<l n="5" num="1.5"><w n="5.1">H<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="5.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="5.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.6">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="5.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.10">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w>, <w n="6.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="6.3">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="6.5">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>, <w n="6.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.7">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.10">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="7" num="1.7"><w n="7.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="7.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.5">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="7.6">gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="7.8">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="7.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pr<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> :</l>
						<l n="8" num="1.8"><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="8.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>nn<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.6">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="9" num="1.9"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="9.3">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">g<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="9.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.10">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.5">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
					</lg>
				</div></body></text></TEI>