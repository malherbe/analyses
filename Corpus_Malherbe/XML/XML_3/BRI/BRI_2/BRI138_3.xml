<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA FLEUR D’OR</title>
				<title type="medium">Une édition électronique</title>
				<author key="BRI">
					<name>
						<forename>Auguste</forename>
						<surname>BRIZEUX</surname>
					</name>
					<date from="1803" to="1858">1803-1858</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2992 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BRI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA FLEUR D’OR</title>
						<author>Auguste Brizeux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/La_Fleur_d%E2%80%99Or</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres de Auguste Brizeux — LA FLEUR D’OR</title>
								<author>Auguste Brizeux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1981">1981</date>
								</imprint>
								<biblScope unit="tome">3</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les deux préfaces ne sont pas reprises.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-02-05" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-02-06" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE NEUVIÈME</head><head type="sub_part">EN BRETAGNE</head><div type="poem" key="BRI138">
					<head type="main">Les Courants</head>
					<opener>
						<salute>À Pélage Lenir</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">S<seg phoneme="y" type="vs" value="1" rule="450">U</seg>R</w> <w n="1.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rtr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.5">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>f<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="1.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.7">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">Br<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">S</w>’<w n="2.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="2.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.5">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>bs<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="3.3">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="4.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="4.4">j<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="4.7">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="5.3">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.5">s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="5.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">L</w>’<w n="6.2"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="6.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">s</w>’<w n="6.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="7.2">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>pp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="8" num="2.4"><w n="8.1">C<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.2">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="8.4">l</w>’<w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w>, <w n="8.6">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="8.8">l</w>’<w n="8.9"><seg phoneme="o" type="vs" value="1" rule="435">O</seg>cc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="9.2">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="9.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="9.8">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>-<w n="10.2">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.2">l</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>,</l>
						<l n="12" num="3.4"><w n="12.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.2">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="12.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="12.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.5">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="12.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.7">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="12.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="469">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="13.3">l</w>’<w n="13.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="13.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="13.7"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>pp<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="14.2">fl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>x</w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="14.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="4.4"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="16.7">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.9">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="17.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.4">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w>, <w n="17.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="17.6">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="17.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.9">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="18.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w></l>
						<l n="20" num="5.4"><w n="20.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="20.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.5">l</w>’<w n="20.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.9">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="20.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.11">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.12"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>