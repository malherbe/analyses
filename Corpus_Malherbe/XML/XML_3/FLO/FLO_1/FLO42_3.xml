<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SECOND</head><div type="poem" key="FLO42">
					<head type="number">FABLE XX</head>
					<head type="main">Le Chat et le Moineau *</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">s<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ;</l>
						<l n="2" num="1.2"><w n="2.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.4">tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="2.5">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="2.6"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="2.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">L</w>’<w n="3.2"><seg phoneme="e" type="vs" value="1" rule="354">e</seg>x<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="3.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.6">f<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.2">m<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="4.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.6">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="5" num="2.2"><w n="5.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="5.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="5.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>, <w n="5.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="5.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.9">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
						<l n="6" num="2.3"><space unit="char" quantity="8"></space><w n="6.1">L<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="6.2">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.4">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.6">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
						<l n="7" num="2.4"><w n="7.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2">m<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="7.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w> <w n="7.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.9">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="8" num="2.5"><w n="8.1"><seg phoneme="o" type="vs" value="1" rule="435">O</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="8.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">gr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>.</l>
						<l n="9" num="2.6"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.3">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="9.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.5">l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="9.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.7">gu<seg phoneme="e" type="vs" value="1" rule="353">e</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.8">d</w>’<w n="9.9"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="10" num="2.7"><w n="10.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> ; <w n="10.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.7">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="10.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="11" num="2.8"><w n="11.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="11.2">qu</w>’<w n="11.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="11.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="11.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.6">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.7">s</w>’<w n="11.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>.</l>
						<l n="12" num="2.9"><w n="12.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ? <w n="12.4">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.5">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="12.7"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="12.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="2.10"><space unit="char" quantity="8"></space><w n="13.1">M<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="13.2">f<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.5">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
						<l n="14" num="2.11"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="14.6">n<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>f</w>. <w n="14.7"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="14.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.9">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.10">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.11">l</w>’<w n="14.12"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w></l>
						<l n="15" num="2.12"><space unit="char" quantity="8"></space><w n="15.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="15.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="16" num="2.13"><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.3">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="16.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.5">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="16.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="16.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="16.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.9">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="17" num="2.14"><space unit="char" quantity="8"></space><w n="17.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="17.3">s</w>’<w n="17.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="17.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>.</l>
						<l n="18" num="2.15"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="18.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.3">cl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="18.4">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w>, <w n="18.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.6">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="18.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="18.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>,</l>
						<l n="19" num="2.16"><space unit="char" quantity="8"></space><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="19.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w>, <w n="19.6">s</w>’<w n="19.7"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="19.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="20" num="2.17"><space unit="char" quantity="8"></space><w n="20.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.4">l</w>’<w n="20.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="20.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.9">d<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>,</l>
						<l n="21" num="2.18"><space unit="char" quantity="8"></space><w n="21.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> <w n="21.3">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.5">qu</w>’<w n="21.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="22" num="2.19"><w n="22.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.5">l</w>’<w n="22.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="22.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="22.9">gr<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> ;</l>
						<l n="23" num="2.20"><w n="23.1">L</w>’<w n="23.2"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="23.3">s</w>’<w n="23.4"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="23.5">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="23.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="23.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ppr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="23.9">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="2.21"><w n="24.1">V<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="24.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>cqu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">l</w>’<w n="24.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="24.8">Cr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w> !</l>
						<l n="25" num="2.22"><space unit="char" quantity="8"></space><w n="25.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="25.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="25.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="25.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c</w>.</l>
						<l n="26" num="2.23"><space unit="char" quantity="8"></space><w n="26.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="26.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="26.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="26.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w> <w n="26.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="26.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.7">f<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="27" num="2.24"><w n="27.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="27.2">m<seg phoneme="wa" type="vs" value="1" rule="421">oi</seg>n<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="27.3">s</w>’<w n="27.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="27.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.6">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.7">sc<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>,</l>
						<l n="28" num="2.25"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="a" type="vs" value="1" rule="365">e</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="28.3">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="28.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="29" num="2.26"><space unit="char" quantity="8"></space><w n="29.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="29.2">d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="29.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="29.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="29.6">s</w>’<w n="29.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="30" num="2.27"><w n="30.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="30.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.5">gr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="30.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="30.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.9">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w>.</l>
						<l n="31" num="2.28"><space unit="char" quantity="8"></space><w n="31.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="31.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="31.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.5">tr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="31.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="32" num="2.29"><space unit="char" quantity="8"></space><w n="32.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="32.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="32.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="32.6">n</w>’<w n="32.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="32.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>,</l>
						<l n="33" num="2.30"><space unit="char" quantity="8"></space><w n="33.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.3">f<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w>, <w n="33.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="34" num="2.31"><space unit="char" quantity="8"></space><w n="34.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="34.2">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rt</w> <w n="34.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="34.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="34.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>.</l>
					</lg>
				</div></body></text></TEI>