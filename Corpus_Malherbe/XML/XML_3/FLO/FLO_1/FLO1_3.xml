<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="FLO1">
					<head type="number">FABLE PREMIÈRE</head>
					<head type="main">La Fable et la Vérité</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.2">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="1.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1">S<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w>.</l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.5">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.8">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="3.9">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> ;</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">J<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.4">f<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.7">v<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.3">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.4">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rf<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="6.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="6.7">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="7.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.4">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="7.5">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">F<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.3">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="8.4">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">P<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="9.2">pl<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.4">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="10.3">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w>, <w n="10.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>-<w n="10.6">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w>.</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="133">E</seg>h</w> ! <w n="11.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> ! <w n="11.4">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="11.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="11.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="12" num="1.12"><w n="12.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>-<w n="12.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="12.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ?</l>
						<l n="13" num="1.13"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.3">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> : <w n="13.4">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.6">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="13.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">g<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="14.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.3">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="16" num="1.16"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.4">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="16.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>. <w n="16.7">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="16.8">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.10">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="16.11">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>,</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1">V<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.2">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.3">n</w>’<w n="17.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bt<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="17.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.6">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>.</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="18.2"><seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="18.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="19.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w>, <w n="19.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="19.6">v<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>-<w n="20.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="20.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="20.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="20.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="20.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="21.3">d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="22.2">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="22.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.5">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
						<l n="23" num="1.23"><w n="23.1">C<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.2">n</w>’<w n="23.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="23.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="23.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> : <w n="23.6">T<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="23.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="23.8">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ;</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Qu</w>’<w n="24.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="24.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="24.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w> <w n="24.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="25" num="1.25"><w n="25.1">V<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="25.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="25.4">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="25.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="25.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="26.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="26.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="26.5">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.7">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>,</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="27.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="27.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space><w n="28.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="28.2">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="28.5">ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="28.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.7">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w></l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="29.3">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="29.4">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="29.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ltr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
						<l n="30" num="1.30"><w n="30.1">S<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="30.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="30.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.4">m<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="363">en</seg></w> <w n="30.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="30.6">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="30.8">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w>,</l>
						<l n="31" num="1.31"><w n="31.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.3">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.4">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="31.6">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="31.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="31.9">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space><w n="32.1">V<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="32.2">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="32.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.4">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="32.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>-<w n="32.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w></l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space><w n="33.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="33.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="33.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
				</div></body></text></TEI>