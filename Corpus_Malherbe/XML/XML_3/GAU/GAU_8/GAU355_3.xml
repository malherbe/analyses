<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU355">
				<head type="main">L’ORESTIE</head>
				<head type="sub_1">TRAGÉDIE ANTIQUE</head>
				<head type="sub_1">FRAGMENT</head>
				<div type="section" n="1">
					<head type="main">LYNCÉE (LE VEILLEUR), sur la tour.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x</w> <w n="1.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.7">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="1.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.10">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">Gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>cs</w> <w n="2.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w></l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="3.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="3.4">qu</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.6">l</w>’<w n="3.7">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.8">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>b<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="4.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">Tr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="5.2">j</w>’<w n="5.3"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="5.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="5.5">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> <w n="5.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.9">l</w>’<w n="5.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>,</l>
						<l n="6" num="1.6"><w n="6.1">R<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="6.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">s</w>’<w n="6.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="6.6">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.10"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>.</l>
						<l n="7" num="1.7"><w n="7.1">N<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="7.5">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
						<l n="8" num="1.8">— <w n="8.1">C</w>’<w n="8.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="8.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="8.7">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="8.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="8.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="9" num="1.9"><w n="9.1">V<seg phoneme="e" type="vs" value="1" rule="383">e</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="9.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="9.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="9.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="10" num="1.10"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>xc<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>pt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="10.4">d</w>’<w n="10.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="10.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.7">s</w>’<w n="10.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.9"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.10">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
						<l n="11" num="1.11"><w n="11.1">Tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="11.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="11.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="11.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="11.7">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="11.8">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.9">l</w>’<w n="11.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="12" num="1.12"><w n="12.1">D</w>’<w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.5">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="12.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ç<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="12.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.9">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="1.13"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="13.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="13.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.5">l</w>’<w n="13.6"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="13.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="13.8">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="13.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="13.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.11">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w>,</l>
						<l n="14" num="1.14"><w n="14.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">j<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="14.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>,</l>
						<l n="15" num="1.15"><w n="15.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="15.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="15.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.8">d</w>’<w n="15.9"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="1.16"><w n="16.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w> <w n="16.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.5">g<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="16.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="16.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="16.9">t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="17" num="1.17"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="17.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="17.6">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="17.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="17.8">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
						<l n="18" num="1.18"><w n="18.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="18.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="18.5">s</w>’<w n="18.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="18.8">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
						<l n="19" num="1.19"><w n="19.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="19.2">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="19.3">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> ! <w n="19.5">Qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.7">t<seg phoneme="a" type="vs" value="1" rule="340">â</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="main">CLYTEMNESTRE, au pied de la tour.</head>
					<lg n="1">
						<l n="20" num="1.1"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>-<w n="20.5">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> ? — <w n="20.6">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.7">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="20.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="20.9">l</w>’<w n="20.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="21" num="1.2"><w n="21.1">C</w>’<w n="21.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="21.3">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ? — <w n="21.4">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="21.5">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>tt<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="21.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.8">g<seg phoneme="i" type="vs" value="1" rule="468">î</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.9"><seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>,</l>
						<l n="22" num="1.3"><w n="22.1">D<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>. <w n="22.2"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="22.3">l</w>’<w n="22.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="22.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.8">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>,</l>
						<l n="23" num="1.4"><w n="23.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="23.2">s<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="23.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="23.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="23.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="23.7">d</w>’<w n="23.8"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ph<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="24" num="1.5"><w n="24.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.2">fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="24.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.5">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="24.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="24.8"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.9">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
						<l n="25" num="1.6"><w n="25.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.2">Gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>cs</w> <w n="25.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.4">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>, <w n="25.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="25.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="25.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="25.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="25.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.10">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
						<l n="26" num="1.7"><w n="26.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="26.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="26.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>, <w n="26.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="26.6">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="26.7">l</w>’<w n="26.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> !</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="main">ÉLECTRE</head>
					<lg n="1">
						<l n="27" num="1.1"><w n="27.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>h</w> ! <w n="27.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="27.3">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="27.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="27.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="27.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="27.7">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sc<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="27.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="27.9">L<seg phoneme="ɛ̃" type="vs" value="1" rule="494">yn</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="28" num="1.2"><w n="28.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.2">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="28.3">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="28.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="28.6">l</w>’<w n="28.7">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.8">n</w>’<w n="28.9"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.10">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="28.11">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					</lg>
				</div>
			</div></body></text></TEI>