<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES NOUVELLES, POÉSIES INÉDITES ET POÉSIES POSTHUMES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2661 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_8</idno>

				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1831-1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU300">
				<head type="main">LE GLAS INTÉRIEUR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="1.3">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="2.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="2.4">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w> <w n="2.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">cr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="4.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
					<l n="5" num="1.5"><w n="5.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.4">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.8">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="6" num="1.6"><w n="6.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="6.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="6.5">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.7">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ;</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="7.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.6">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="8" num="1.8"><w n="8.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">phr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="8.3">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="9.2">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">B<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>,</l>
					<l n="10" num="2.2"><w n="10.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="10.6">n</w>’<w n="10.7"><seg phoneme="o" type="vs" value="1" rule="435">o</seg>cc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="2.3"><w n="11.1">S<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="11.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="11.5">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="2.4"><w n="12.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.2">l</w>’<w n="12.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w>.</l>
					<l n="13" num="2.5"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="13.5">m</w>’<w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="2.6"><w n="14.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="14.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="15" num="2.7"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="15.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.6">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="16" num="2.8"><w n="16.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">phr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="16.3">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="16.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="o" type="vs" value="1" rule="444">O</seg>p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="17.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="18" num="3.2"><w n="18.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.2">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="18.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.7">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rgn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="19" num="3.3"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.2">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.4">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="3.4"><w n="20.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="20.2">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg>pr<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="20.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="20.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w>.</l>
					<l n="21" num="3.5"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="21.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="21.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="21.6">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="22" num="3.6"><w n="22.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.2">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.5">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> !</l>
					<l n="23" num="3.7"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="23.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="23.3">s<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="23.6">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="24" num="3.8"><w n="24.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.2">phr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> : <w n="24.3">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.4">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="24.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="24.6">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1848">1848</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>