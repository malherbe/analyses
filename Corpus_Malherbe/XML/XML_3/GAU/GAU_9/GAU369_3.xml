<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
				<title type="sub">Poèmes absents des précédentes éditions</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>504 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">GAU_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Th._Gautier_qui_ne_figureront_pas_dans_ses_%C5%93uvres</idno>
						<p>Exporté de Wikisource le 26 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Th. Gautier qui ne figureront pas dans ses œuvres</title>
								<author>Théophile Gautier</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k72036w</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Impr. particulière</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
				<p>Cette édition ne contient que les poèmes de Th. Gautier non inclus dans les précédentes éditions.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU369">
				<head type="main">QUESTION</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">qu</w>’<w n="1.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="2.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.6">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.8">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">Fl<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.3">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>gt</w> <w n="5.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="6.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="7" num="2.3"><w n="7.1">H<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.5">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="8.2">d</w>’<w n="8.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.4">b<seg phoneme="a" type="vs" value="1" rule="307">â</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.6">tr<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="8.7">gl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.3">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="10.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="10.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.5">d<seg phoneme="i" type="vs" value="1" rule="467">î</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="10.7">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tf<seg phoneme="o" type="vs" value="1" rule="318">au</seg>c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="11.4">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w> <w n="11.5">gr<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="11.6">s</w>’<w n="11.7"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="12.2">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ls</w> <w n="12.3">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="12.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="12.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.3">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="13.4">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>…</l>
					<l n="14" num="4.2"><w n="14.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">v<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>-<w n="14.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="14.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="14.5">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.6">c<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="14.7">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w></l>
					<l n="15" num="4.3"><w n="15.1">M<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rv<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">p<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6">c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>q</w> <w n="16.7">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> ?</l>
				</lg>
			</div></body></text></TEI>