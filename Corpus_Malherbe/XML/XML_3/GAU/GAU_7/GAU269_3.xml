<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ESPAÑA</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1374 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Second</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://gutenberg.org/files/45886/45886-h/45886-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Second</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1890">1890</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1845">1845</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">ESPAÑA</head><div type="poem" key="GAU269">
					<head type="main">DANS LA SIERRA</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">d</w>’<w n="1.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.5">f<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w> <w n="1.6"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="1.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.8">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ts</w> <w n="1.9">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="1.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="1.11">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
						<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.2">pl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">n</w>’<w n="2.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.6">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="2.8">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="2.9">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
						<l n="3" num="1.3"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="3.4">d</w>’<w n="3.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="3.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.7">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.9">c<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">s<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c</w> <w n="4.3">s</w>’<w n="4.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="4.7">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>cs</w> <w n="4.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">N<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="5.4">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="5.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w>, <w n="5.6">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.7">bl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="5.8">d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="5.9">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.10">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">R<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="6.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="6.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.4">l</w>’<w n="6.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w> <w n="6.9">m<seg phoneme="o" type="vs" value="1" rule="318">au</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>.</l>
						<l n="7" num="2.3"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="7.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="7.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.6">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.7">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.9"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="302">aim</seg>s</w> <w n="7.10">d</w>’<w n="7.11"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">l</w>’<w n="8.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="8.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.5">r<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.7">l</w>’<w n="8.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="8.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.10">b<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="9.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>pp<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="9.4">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="9.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="9.9"><seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
						<l n="10" num="3.2"><w n="10.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="10.2">n</w>’<w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.6">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>, <w n="10.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.9">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="10.10">c</w>’<w n="10.11"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="10.12">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="10.13">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> ;</l>
						<l n="11" num="3.3"><w n="11.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="11.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="11.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.5">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="11.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>ps</w> <w n="11.8">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.10">f<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="12.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="12.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="12.7">qu</w>’<w n="12.8"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.9">n</w>’<w n="12.10"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="12.11">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="12.12">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="12.13">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
					</lg>
					<closer>
						<dateline>
						<placeName>Sierra-Nevada</placeName>,
							<date when="1840">184.</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>