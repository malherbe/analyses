<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU149">
				<head type="main">SOLEIL COUCHANT</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Notre-Dame, <lb></lb>
								Que c’est beau !
							</quote>
							<bibl>
								<name>Victor Hugo.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="1.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.9"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="1.10">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>,</l>
					<l n="2" num="1.2"><w n="2.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.3">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="2.7">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.8">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w></l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="3.3">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="3.5">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.6">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>-<w n="3.7">D<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="4.2">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">spl<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.5">l</w>’<w n="4.6">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="4.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.8">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1">T<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="5.2">qu</w>’<w n="5.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="5.5">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.7">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.9">l</w>’<w n="5.10"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="6" num="1.6"><w n="6.1">D</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="6.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="6.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="6.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.7">l</w>’<w n="6.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="6.9"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.10">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.11"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.12">d</w>’<w n="6.13"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="7" num="1.7">— <w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">c</w>’<w n="7.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="7.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.5">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="7.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.9">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="8" num="1.8"><w n="8.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.5"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="9" num="1.9"><w n="9.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="9.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="9.6">f<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w></l>
					<l n="10" num="1.10"><w n="10.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="10.2">s</w>’<w n="10.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ils</w> <w n="10.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="10.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w>,</l>
					<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="11.3">t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="11.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="11.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="12" num="1.12"><w n="12.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="12.2">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="12.3">r<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w>, <w n="12.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="y" type="vs" value="1" rule="448">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="13" num="1.13"><w n="13.1">D</w>’<w n="13.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="13.4">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="13.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="13.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.7">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> ; <w n="13.8">l</w>’<w n="13.9"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>rch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="14" num="1.14"><w n="14.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="14.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.8">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.10">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="15" num="1.15"><w n="15.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">d<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.4">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">l</w>’<w n="15.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="15.9">l</w>’<w n="15.10"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="1.16"><w n="16.1">S</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɛ" type="vs" value="1" rule="301">ai</seg>t</w> <w n="16.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="16.4">l</w>’<w n="16.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="16.6">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="17" num="1.17">— <w n="17.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="17.2">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>, <w n="17.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="17.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.8">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w></l>
					<l n="18" num="1.18"><w n="18.1">D</w>’<w n="18.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> ; — <w n="18.6">l</w>’<w n="18.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="18.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> ; <w n="18.10">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.11"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w></l>
					<l n="19" num="1.19"><w n="19.1">S<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.2">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="19.3">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">l</w>’<w n="19.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="19.8">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w>, <w n="19.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.11">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="20" num="1.20"><w n="20.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.4">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="20.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rç<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.6">l</w>’<w n="20.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="21" num="1.21"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="21.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="21.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t</w> <w n="21.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="22" num="1.22"><w n="22.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.3">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="22.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="22.8">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>.</l>
				</lg>
			</div></body></text></TEI>