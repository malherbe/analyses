<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU111">
				<head type="main">PAYSAGE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
							..... omnia plenis <lb></lb>
							Rura natant fossis.
							</quote>
							<bibl>
								<name>P. Virgilius Maro.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="2.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.3">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="2.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="3.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">l</w>’<w n="3.5">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="4.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="4.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">D</w>’<w n="5.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="5.3">c<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="5.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.5">br<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="6" num="2.2"><w n="6.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="6.4">n<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>,</l>
					<l n="7" num="2.3"><w n="7.1">P<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.4"><w n="8.1">S<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.2">n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.4">pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">l</w>’<w n="9.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="9.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.5">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p</w> <w n="9.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">f<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="10.5">d</w>’<w n="10.6"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.2">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="11.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="12.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="12.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="13.2">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="13.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.6">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="4.2"><w n="14.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.3">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="14.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5">c<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="14.6">bl<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.4">r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>b<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="15.5">s</w>’<w n="15.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="16.2">m<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="16.3">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="16.4"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
			</div></body></text></TEI>