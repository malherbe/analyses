<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU143">
				<head type="main">MARIA</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
									 … meæ puellæ <lb></lb>
								Flendo turgiduli rubent ocelli.
							</quote>
							<bibl>
								<name>V. Catullus.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Ne pleure pas…
							</quote>
							<bibl>
								<name>Dovalle.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="1.4">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">j<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.7">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="1.10">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.11"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.4">d</w>’<w n="2.5"><seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.6">d</w>’<w n="2.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="2.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.11">pl<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="3.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="3.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="3.6">p<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t</w> <w n="3.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.8">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="3.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.10">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
					<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="4.4"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>f</w> <w n="4.5">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="4.6">j</w>’<w n="4.7"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="4.8">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.9">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.10">pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
					<l n="5" num="1.5"><w n="5.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.3">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="5.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.8">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="6" num="1.6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="6.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="6.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">g<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.6">l</w>’<w n="6.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="6.8"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.9">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="6.10">j<seg phoneme="u" type="vs" value="1" rule="426">ou</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="7" num="1.7"><w n="7.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.4">br<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>s</w> ; <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="7.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.8">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.9">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.10">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">D</w>’<w n="8.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>qu<seg phoneme="j" type="sc" value="0" rule="489">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.3">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="8.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Qu</w>’<w n="9.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="9.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="9.4">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>, <w n="9.5">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="9.6">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.8">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="10.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="10.4">br<seg phoneme="y" type="vs" value="1" rule="454">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.7">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="10.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="11.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="11.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.5">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="11.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.8">l</w>’<w n="11.9">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="12" num="1.12"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.4">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.5">l</w>’<w n="12.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="a" type="vs" value="1" rule="307">a</seg>il</w> <w n="12.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="12.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="12.9">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="13" num="1.13"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">pr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>vr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.7">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="1.14"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="14.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="14.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="14.6">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="14.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.9">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="1.15"><w n="15.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="15.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="15.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="15.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="15.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fr<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ?</l>
					<l n="16" num="1.16"><w n="16.1">T<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="16.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="16.3">n</w>’<w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="16.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="16.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.8">qu</w>’<w n="16.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="16.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.11">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="17" num="1.17"><w n="17.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="17.2">l</w>’<w n="17.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="17.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="17.5">pl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>. <w n="17.6">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ds</w>-<w n="17.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="17.8">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="17.9"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="18" num="1.18"><w n="18.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="18.2">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="18.3">l</w>’<w n="18.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.7">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="18.8">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.9">n<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
					<l n="19" num="1.19"><w n="19.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="19.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="19.3">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="19.4">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="19.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.6">l</w>’<w n="19.7"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="19.8">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="20" num="1.20"><w n="20.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.2">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="20.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="20.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="20.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="20.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="20.9">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="20.10">s<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="21" num="1.21"><w n="21.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.2">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="21.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="21.4">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w>, <w n="21.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="21.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.7">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="21.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="21.9">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.10">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="22" num="1.22"><w n="22.1">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="22.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="22.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg>s</w>, <w n="22.4">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="22.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Qu</w>’<w n="23.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="23.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w>, <w n="23.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ? <w n="23.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="23.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="23.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w></l>
					<l n="24" num="1.24"><w n="24.1">T<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="24.3">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ? — <w n="24.4">H<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="24.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">t</w>’<w n="24.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.9">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="24.10">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>.</l>
				</lg>
			</div></body></text></TEI>