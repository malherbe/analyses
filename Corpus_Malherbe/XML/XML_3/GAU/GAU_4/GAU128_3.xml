<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="sub">édition Maurice Dreyfous</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2184 vers2184 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GAU_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">The Project Gutenberg’s eBook of Poésies Complètes, Tome Premier</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/files/44180/44180-h/44180-h.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Complètes, Tome Premier</title>
								<author>Théophile Gautier</author>
								<edition>éd. par Maurice Dreyfous</edition>
								<idno type="URL">http://gallica.bnf.fr/</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Introduction, préface et notes ont été écartées dans cette édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Quelques erreurs orthographiques ont été repérées avec le correcteur orthogrphique Gedit.</p>
				<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="GAU128">
				<head type="main">RÊVE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Et nous voulons mourir quand le rêve finit.
							</quote>
							<bibl>
								<name>A. Guiraud.</name>
							</bibl>
						</cit>
						<cit>
							<quote>
								Tout la nuict je ne pense qu’en celle <lb></lb>
								Qui ha le cors plus gent qu’une pucelle <lb></lb>
											 De quatorze ans.
							</quote>
							<bibl>
								<name>Maître Clément Marot.</name>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">j</w>’<w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="1.6">v<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.7">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.10">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> :</l>
					<l n="2" num="1.2"><w n="2.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.5">l</w>’<w n="2.6">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w></l>
					<l n="3" num="1.3"><w n="3.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="3.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="3.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.9">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.3">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>, <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="4.6">br<seg phoneme="ɥ" type="sc" value="0" rule="455">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="4.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="4.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.10">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.11">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="5.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w>, <w n="5.5">j</w>’<w n="5.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="5.7">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.9">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w></l>
					<l n="6" num="1.6"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc</w>-<w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w>-<w n="6.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="6.6">t<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.9">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>.</l>
					<l n="7" num="1.7"><w n="7.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="7.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="7.4">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="7.5">fr<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="7.6">l</w>’<w n="7.7">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ; <w n="7.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.9">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">j</w>’<w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.4">d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="8.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.7">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="8.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.9">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.10"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="9" num="1.9"><w n="9.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.3">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="9.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rt</w> <w n="9.6">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="9.7">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ; <w n="9.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.9"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="10.2">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="10.3">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="10.5">c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="10.6">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.10">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="11" num="1.11"><w n="11.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> ; <w n="11.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="11.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.10">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="12" num="1.12"><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="12.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.4">d</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="12.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.8">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="12.9">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="13" num="1.13"><w n="13.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="13.2">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="13.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg>t<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>t</w> <w n="13.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="13.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.7">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.8">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w></l>
					<l n="14" num="1.14"><w n="14.1">M<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>, <w n="14.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.5">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.7">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.8">d</w>’<w n="14.9"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>,</l>
					<l n="15" num="1.15"><w n="15.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="15.3">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="15.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.5">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.6">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">s</w>’<w n="15.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> : —</l>
					<l n="16" num="1.16"><w n="16.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="16.2">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="16.3">tr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>p<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> ! — <w n="16.4">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="16.5">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rqu<seg phoneme="wa" type="vs" value="1" rule="281">oi</seg></w> <w n="16.6">n</w>’<w n="16.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w>-<w n="16.8">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="16.9">qu</w>’<w n="16.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.11">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
			</div></body></text></TEI>