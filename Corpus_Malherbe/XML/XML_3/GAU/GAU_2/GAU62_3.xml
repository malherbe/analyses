<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La comédie de la mort</title>
				<title type="medium">Édition électronique</title>
				<author key="GAU">
					<name>
						<forename>Théophile</forename>
						<surname>GAUTIER</surname>
					</name>
					<date from="1811" to="1872">1811-1872</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5120 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">GAU_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Comédie de la mort</title>
						<author>Théophile Gautier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>inlibroveritas.net</publisher>
						<idno type="URL">http://www.inlibroveritas.net</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Comédie de la mort</title>
						<author>Théophile Gautier</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k70716q.r=th%C3%A9ophile+gautier.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>DESESSART, ÉDITEUR</publisher>
							<date when="1838">1838</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1838">1838</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2014-04-11" who="RR">Un vers faux dans GAU54 (Thébaïde), vers 85 a été corrigé avec l’édition Bartillat (préparée par Michel Brix)</change>
			<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GAU62">
				<head type="main">LA CHIMÈRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.2">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.3">ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="1.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.8">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="2.2">l</w>’<w n="2.3"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w>, <w n="2.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">d<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="2.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.9">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="2.10">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w></l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="3.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="3.5">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>, <w n="3.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.7">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.8">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.10">cr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>d<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.3">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rr<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="4.4">l</w>’<w n="4.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="4.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="4.9">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.3">d</w>’<w n="5.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="5.5">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="5.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="5.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="6" num="2.2"><w n="6.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.3">s</w>’<w n="6.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.5">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="6.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="6.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.9">r<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> ;</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.3">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="7.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="7.5">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.6">pl<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="7.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.8">c<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="7.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.10">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.4"><w n="8.1">J</w>’<w n="8.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ç<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="8.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="8.5">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.7">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="8.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.9">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.10">cr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.2">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="9.4">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.6">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="10.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>. <w n="10.4">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">br<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="10.6">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="10.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.10">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> ;</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="11.5">d</w>’<w n="11.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.7">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="11.8">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1">Pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.2">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">l</w>’<w n="12.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> : <w n="12.6">M<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="12.7"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="12.8">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="12.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="12.10">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>-<w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="13.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w>-<w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.8">l</w>’<w n="13.9"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="14.2">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="14.3">n</w>’<w n="14.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.5">qu</w>’<w n="14.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="14.7">l</w>’<w n="14.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> ;</l>
					<l n="15" num="4.3"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="15.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.6">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="15.7">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="15.8"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.9">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> :</l>
					<l n="16" num="4.4"><w n="16.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="16.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="16.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="16.8">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.9">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
			</div></body></text></TEI>