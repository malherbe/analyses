<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">AMÉTHYSTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>319 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">AMÉTHYSTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleamethystes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1860-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN186">
				<head type="main">Le Rossignol</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="1.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="1.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="1.4">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="2" num="1.2"><w n="2.1">Br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="2.2">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w>,</l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.3">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="4.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="4.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="4.5">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="5.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.4">v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="6.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">r<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>-<w n="7.4">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="8" num="2.2"><w n="8.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="8.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.3">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ; <w n="8.4">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>,</l>
					<l n="9" num="2.3"><w n="9.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="9.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="9.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="10" num="2.4"><w n="10.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="10.2">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.4">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w></l>
					<l n="11" num="2.5"><w n="11.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">m<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w> <w n="11.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.5">m</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="11.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w>,</l>
					<l n="12" num="2.6"><w n="12.1">N<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="12.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.2">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="14" num="3.2"><w n="14.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="14.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="14.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.5">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>,</l>
					<l n="15" num="3.3"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="15.3">m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="3.4"><w n="16.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.3">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gs</w> <w n="16.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w></l>
					<l n="17" num="3.5"><w n="17.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="17.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.4">d</w>’<w n="17.5"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					<l n="18" num="3.6"><w n="18.1">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> ! <w n="18.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="19.2">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="19.3">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>-<w n="19.4">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="19.5">j</w>’<w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>dm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="4.2"><w n="20.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="20.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.3">g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
					<l n="21" num="4.3"><w n="21.1">T<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="4.4"><w n="22.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.2"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="22.4">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="22.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> !</l>
					<l n="23" num="4.5"><w n="23.1">C</w>’<w n="23.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="23.3">h<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> ! <w n="23.4">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w></l>
					<l n="24" num="4.6"><w n="24.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.2">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="25.2">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ! <w n="25.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="25.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="26" num="5.2"><w n="26.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.3">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> !</l>
					<l n="27" num="5.3"><w n="27.1">N<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>, <w n="27.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.3">n</w>’<w n="27.4"><seg phoneme="ɛ" type="vs" value="1" rule="50">e</seg>s</w> <w n="27.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="27.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rj<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="5.4"><w n="28.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.2">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="28.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="28.5">d</w>’<w n="28.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>,</l>
					<l n="29" num="5.5"><w n="29.1">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="29.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> !</l>
					<l n="30" num="5.6"><w n="30.1">J</w>’<w n="30.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="30.3">f<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w>. <w n="30.4">J</w>’<w n="30.5"><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w> <w n="30.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1861">Juin 1860.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>