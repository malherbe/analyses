<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="BAN44">
					<head type="main">VÉNUS COUCHÉE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>D’un plus hault vol, d’aile mieux emplumee <lb></lb>
								Ne la pouuoit rauir ce petit Dieu ; <lb></lb>
								Et ne pouuoit encor’ en plus hault lieu, <lb></lb>
								Ny en plus seur sa flamme estre allumée.</quote>
								<bibl>
									<name>JOACHIM DV BELLAY</name>, <hi rend="ital">Inscriptions</hi>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.3">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ; <w n="1.4">Ph<seg phoneme="e" type="vs" value="1" rule="250">œ</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="1.5">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.7">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w>,</l>
						<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="2.2">h<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.5">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>, <w n="2.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.7">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.9">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.4">fl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w> <w n="3.5">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">fl<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="3.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>g<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="4.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="4.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.6">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="4.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="5" num="1.5"><w n="5.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="5.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="5.4">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="5.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.7">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="5.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.9">l</w>’<w n="5.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
						<l n="6" num="1.6"><w n="6.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.2">d</w>’<w n="6.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="6.4"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɛ̃" type="vs" value="1" rule="303">aim</seg></w>, <w n="6.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.6">b<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>f</w> <w n="6.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w></l>
						<l n="7" num="1.7"><w n="7.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="7.4">br<seg phoneme="y" type="vs" value="1" rule="445">û</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.7">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="8" num="1.8"><w n="8.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="8.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>cc<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> : <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.4">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="8.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.6">n<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg>ph<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="8.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.8">c<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="9" num="1.9"><w n="9.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="9.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="9.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="9.8">fl<seg phoneme="e" type="vs" value="1" rule="409">é</seg>tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
						<l n="10" num="1.10"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.2">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="10.3">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="10.4">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="10.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.7">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>,</l>
						<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="11.2">f<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="11.3">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.8">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.9">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="12" num="1.12"><w n="12.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="12.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.8">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
						<l n="13" num="1.13"><space quantity="2" unit="char"></space><w n="13.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="13.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.5">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w>, <w n="13.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="13.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.8">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="13.9">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.11">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="14" num="1.14"><w n="14.1">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>, <w n="14.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="14.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="14.9">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
						<l n="15" num="1.15"><w n="15.1">S</w>’<w n="15.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.3">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="15.5">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="15.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.8">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ff<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="15.9">d</w>’<w n="15.10">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="16" num="1.16"><w n="16.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">Ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="16.2">qu</w>’<w n="16.3"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.5"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="17" num="1.17"><w n="17.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.3">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>th<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="17.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.6">m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="17.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w>,</l>
						<l n="18" num="1.18"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="18.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.6">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="18.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="18.8">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w></l>
						<l n="19" num="1.19"><w n="19.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.2">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="19.3">r<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="19.4">d</w>’<w n="19.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="19.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.8">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="19.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="19.10">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.11">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="20" num="1.20"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="20.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.4">fr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="20.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.7">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rp<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="21" num="1.21"><w n="21.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="21.2">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="21.3">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rm<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="21.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="21.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="21.6">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.8">gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>,</l>
						<l n="22" num="1.22"><w n="22.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="22.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.3">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="22.4">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="22.5">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="22.6">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="22.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.8">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w>.</l>
						<l n="23" num="1.23"><space quantity="2" unit="char"></space><w n="23.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="23.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="23.3">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="23.4">s<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg></w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">n<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="23.8">m<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.10">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="24" num="1.24"><w n="24.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="24.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="24.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="24.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.7">cr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.8">bl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="25" num="1.25"><w n="25.1">Br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="25.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="25.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="25.4">qu</w>’<w n="25.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="25.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="25.7">z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w> <w n="25.8"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
						<l n="26" num="1.26"><w n="26.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.3">gu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="26.6">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="26.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="27" num="1.27"><w n="27.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="27.2">d</w>’<w n="27.3"><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="27.4">g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="27.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="27.6">l</w>’<w n="27.7">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.8">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.9"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="27.10">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="27.11">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="28" num="1.28"><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w> <w n="28.3">s<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="28.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.8">t<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.9">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1841">février 1841</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>