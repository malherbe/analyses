<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN442">
				<head type="main">Chien perdu</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="1.2">s</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.4">c<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="1.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7">h<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="2.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="2.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="2.4">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="2.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">B<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sm<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rck</w> <w n="3.2">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="3.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="3.5">Gu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="o" type="vs" value="1" rule="318">au</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">M<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ltk<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="4.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.2">pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.5">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>, <w n="6.3">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.2">Pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="7.3">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.5">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="7.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="7.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w></l>
					<l n="8" num="2.4"><w n="8.1">L<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="8.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="9.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="9.3">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.6">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.4">W<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w>, <w n="11.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.5"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="3.4"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>ds</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rge<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="13.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">f<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="14" num="4.2"><w n="14.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="14.2">cr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="14.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.5">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="15.2">f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>dr<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="15.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="15.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="15.5">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="16.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="16.3">s</w>’<w n="16.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="16.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rpr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="17.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="17.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="17.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">dr<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="18" num="5.2"><w n="18.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.4">qu</w>’<w n="18.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="18.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>pr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="19.5">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="20" num="5.4"><w n="20.1">Qu</w>’<w n="20.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="20.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="20.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w> <w n="21.2">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="21.3">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="21.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.5">g<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="22" num="6.2"><w n="22.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="22.2">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.4">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="22.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="22.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ts</w>,</l>
					<l n="23" num="6.3"><w n="23.1">V<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="23.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.3">f<seg phoneme="a" type="vs" value="1" rule="193">e</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="24" num="6.4"><w n="24.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="24.4"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ll<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="25.2">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu</w>’<w n="25.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="25.4">v<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="25.5"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="25.6"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ss<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w></l>
					<l n="26" num="7.2"><w n="26.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="26.3">d</w>’<w n="26.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="26.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="26.6">c<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
					<l n="27" num="7.3"><w n="27.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.2">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="27.3">pr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>ss<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg>s</w> <w n="27.4">s</w>’<w n="27.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>nt</w> ;</l>
					<l n="28" num="7.4"><w n="28.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="28.2">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="28.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>ss<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="28.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.5">B<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rl<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>sp<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w> <w n="29.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.3">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="29.4">h<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="30" num="8.2"><w n="30.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>-<w n="30.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.3">j</w>’<w n="30.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="30.5">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="30.6">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="30.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>s</w></l>
					<l n="31" num="8.3"><w n="31.1">Fl<seg phoneme="a" type="vs" value="1" rule="341">â</seg>n<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="31.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="31.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="31.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="31.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>-<w n="31.6">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="32" num="8.4"><w n="32.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.2">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>rs</w>, <w n="32.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.4">j<seg phoneme="o" type="vs" value="1" rule="318">au</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="32.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="32.7">br<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>s</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="33.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rt<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>, — <w n="33.3"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>h</w> ! <w n="33.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="33.5">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="33.6">S<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="34" num="9.2"><w n="34.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.2">qu<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="34.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="34.4">g<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w>. <w n="34.5"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="34.6">t<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
					<l n="35" num="9.3"><w n="35.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.2">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="35.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.5">gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="36" num="9.4"><w n="36.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="36.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="36.3">D<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="37.2">m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="37.3">d</w>’<w n="37.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.5">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ç<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="37.6">n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="38" num="10.2"><w n="38.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="38.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="38.3">qu</w>’<w n="38.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="38.5"><seg phoneme="y" type="vs" value="1" rule="251">eû</seg>t</w> <w n="38.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="38.7">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w></l>
					<l n="39" num="10.3"><w n="39.1">J<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="39.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="39.3"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w> <w n="39.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="39.5">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="40" num="10.4"><w n="40.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="40.2">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="40.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.4">r<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="40.5">Z<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s</w> <w n="40.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="40.7">t<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">C<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="41.2"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l</w> <w n="41.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="41.4">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="42" num="11.2"><w n="42.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="42.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="42.3">g<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="42.4">vr<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> <w n="42.5">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>,</l>
					<l n="43" num="11.3"><w n="43.1">B<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="43.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="43.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="43.4">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="43.6">s<seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="44" num="11.4"><w n="44.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="44.2">pr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="44.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="44.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="44.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="44.7">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1">— <w n="45.1">P<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="45.2">ch<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="45.3">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w>, <w n="45.4">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="45.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>-<w n="45.6">j<seg phoneme="ə" type="ef" value="1" rule="e-1">e</seg></w>,</l>
					<l n="46" num="12.2"><w n="46.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="46.3">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> ? <w n="46.4">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="46.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.6">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="46.7">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w>-<w n="46.8"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> ?</l>
					<l n="47" num="12.3"><w n="47.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="47.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>d<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w>, — <w n="47.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="47.4">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.5">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="48" num="12.4"><w n="48.1">P<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="48.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="48.3">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.4">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="48.5">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, —</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">J</w>’<w n="49.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="49.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="49.4">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="49.5">c<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="50" num="13.2"><w n="50.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="50.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="50.3">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.4">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="50.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="50.6">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w></l>
					<l n="51" num="13.3"><w n="51.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="51.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="51.3">r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="51.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="51.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.6">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="52" num="13.4"><w n="52.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="52.2">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> : — <w n="52.3">C<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>q</w> <w n="52.4">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rds</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Novembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>