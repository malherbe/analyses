<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN463">
				<head type="main">Un vieux Monarque</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="1.2">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.4">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="1.5">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w>,</l>
					<l n="2" num="1.2"><w n="2.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rb<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w>, <w n="2.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>gn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.4">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="3.4">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">p<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>tr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="4.6">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.5">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.6">v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>.</l>
					<l n="6" num="2.2"><w n="6.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="6.2">B<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> ! <w n="6.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w>-<w n="6.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w>, <w n="6.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.6">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="7" num="2.3"><w n="7.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="7.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w>, <w n="7.4">l</w>’<w n="7.5">h<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> !</l>
					<l n="8" num="2.4"><w n="8.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="8.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">v<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> ! <w n="8.4">m<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>, <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rgr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>, <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="9.6">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="306">Ai</seg></w> <w n="10.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="10.3">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2"><seg phoneme="ø" type="vs" value="1" rule="405">Eu</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="11.5">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="12.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="12.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="12.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">M<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w> <w n="13.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="13.3">p<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="13.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="13.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.6">gr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.7">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w></l>
					<l n="14" num="4.2"><w n="14.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="14.3">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="14.4">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.5">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.6">fl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="15.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.4">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rpr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">T<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w>,</l>
					<l n="16" num="4.4"><w n="16.1">L</w>’<w n="16.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.3">c<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="16.4"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="16.5">l</w>’<w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">V<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="17.2">j</w>’<w n="17.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="17.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w></l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="18.2">d</w>’<w n="18.3"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="18.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.6">n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
					<l n="19" num="5.3"><w n="19.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.2">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="19.3">p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rdr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w> <w n="19.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.7">p<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> ?</l>
					<l n="20" num="5.4"><w n="20.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.2">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>dr<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>-<w n="20.3">t</w>-<w n="20.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>c</w> <w n="21.3">m</w>’<w n="21.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.5">c<seg phoneme="o" type="vs" value="1" rule="318">au</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="21.6">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="21.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="359">en</seg>n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> ?</l>
					<l n="22" num="6.2"><w n="22.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.2">d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="22.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>rr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg>d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="23.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.3">p<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="23.4">d<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="23.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd</w>’<w n="23.6">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w>,</l>
					<l n="24" num="6.4"><w n="24.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.3">t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ec" value="0" rule="e-20">e</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg></w>, <w n="24.4">f<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t</w>-<w n="24.5">c<seg phoneme="ə" type="ef" value="1" rule="e-13">e</seg></w> <w n="24.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.7">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w> <w n="25.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>-<w n="25.3">v<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="25.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.6"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w></l>
					<l n="26" num="7.2"><w n="26.1">D</w>’<w n="26.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="26.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>x</w> <w n="26.4">r<seg phoneme="o" type="vs" value="1" rule="318">au</seg>qu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.6">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="194">e</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="27" num="7.3"><w n="27.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="27.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="27.4">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="27.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="27.6">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ?</l>
					<l n="28" num="7.4"><w n="28.1">C</w>’<w n="28.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="28.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.4">s<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="28.5">P<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">S</w>’<w n="29.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.4">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="29.5">c<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="29.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="29.7"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w></l>
					<l n="30" num="8.2"><w n="30.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="30.2">f<seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="30.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="30.4"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="30.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">C</w>’<w n="31.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="31.3">qu</w>’<w n="31.4"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="31.5">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="31.7">Gu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>gn<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l</w>,</l>
					<l n="32" num="8.4"><w n="32.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w>, <w n="32.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.4">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="32.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.6">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="33.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="33.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="33.4"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>b<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="33.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w></l>
					<l n="34" num="9.2"><w n="34.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="34.2">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="34.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="34.4">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.5">gl<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="35" num="9.3"><w n="35.1">L</w>’<w n="35.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.3">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w>, <w n="35.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="35.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="35.6">m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="35.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w></l>
					<l n="36" num="9.4"><w n="36.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>f<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="36.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.3">l</w>’<w n="36.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>bs<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>