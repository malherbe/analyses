<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN436">
				<head type="main">Rouge et Bleu</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="1.2">R<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> ! <w n="1.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="1.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>l</w> <w n="2.2">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="2.5">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w></l>
					<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="3.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="3.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.4">d<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="3.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.6">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="3.7">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="4" num="1.4"><w n="4.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="4.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.6">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>-<w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w>, <w n="5.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="5.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="5.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.6">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.7">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="6.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.3">pl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="6.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="6.6">g<seg phoneme="u" type="vs" value="1" rule="425">oû</seg>t</w>,</l>
					<l n="7" num="2.3"><w n="7.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="7.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="8.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="8.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="8.7">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="9.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.4">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="9.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="10" num="3.2"><w n="10.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.2">j<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, — <w n="10.3">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="10.4">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="10.5">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ds</w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.3">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>lsh<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="11.4">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> :</l>
					<l n="12" num="3.4"><w n="12.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="12.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="12.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ! <w n="12.4">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w>-<w n="13.3">cr<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="13.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.5">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="14" num="4.2"><w n="14.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.2"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="14.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="14.5">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="14.7">m<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
					<l n="15" num="4.3"><w n="15.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="15.2">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="306">ai</seg><seg phoneme="ə" type="ei" value="0" rule="e-31">e</seg>nt</w> <w n="15.3">d</w>’<w n="15.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">O</seg>rph<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="15.6">d</w>’<w n="15.7">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="4.4"><w n="16.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="16.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.3">Phr<seg phoneme="i" type="vs" value="1" rule="497">y</seg>n<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="16.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.5">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.3">r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="17.5">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.6">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="18" num="5.2"><w n="18.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="18.2">l</w>’<w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.4">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.5">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>t</w> <w n="18.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.7">l</w>’<w n="18.8"><seg phoneme="e" type="vs" value="1" rule="353">e</seg>ffr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>,</l>
					<l n="19" num="5.3"><w n="19.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.3">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="19.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="19.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="20" num="5.4"><w n="20.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="20.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> ! <w n="20.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="20.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.6">t<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">C<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.2">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="21.3">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="21.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="21.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="21.6">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="22" num="6.2"><w n="22.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="22.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="22.5">m<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> ;</l>
					<l n="23" num="6.3"><w n="23.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="23.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="23.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.4">M<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.5"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="23.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.7">F<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>bvr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="24" num="6.4"><w n="24.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="24.2">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="24.3">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="24.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.5">M<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>tm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="i" type="vs" value="1" rule="493">y</seg></w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="25.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="25.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="25.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="25.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.6">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="25.7">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="25.8">gl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="26" num="7.2"><w n="26.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="26.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="26.4">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="26.5">s<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>l</w> <w n="26.6">b<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>.</l>
					<l n="27" num="7.3"><w n="27.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.2">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w>, <w n="27.3"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="27.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="27.5">P<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="28" num="7.4"><w n="28.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="28.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="28.4">v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="28.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.6">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w></l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">On</seg>t</w> <w n="29.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="29.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="29.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="29.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.7">n<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="30" num="8.2"><w n="30.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="30.2">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="30.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="30.4">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="30.5">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="30.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>,</l>
					<l n="31" num="8.3"><w n="31.1">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="31.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w>, <w n="31.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>s</w> <w n="31.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="31.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="31.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="32" num="8.4"><w n="32.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="32.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="32.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="32.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="32.5">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="33.3">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="33.4">d</w>’<w n="33.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="33.6">st<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="34" num="9.2"><w n="34.1"><seg phoneme="ɛ" type="vs" value="1" rule="339">A</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="34.2">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="34.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="34.4">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="34.5">l<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>,</l>
					<l n="35" num="9.3"><w n="35.1">S</w>’<w n="35.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="35.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="35.4">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="35.5">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="i" type="vs" value="1" rule="477">ï</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="36" num="9.4"><w n="36.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="36.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="36.3">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="36.4">m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="36.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w>-<w n="36.6">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="37.2">v<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="37.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="37.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="37.5">pl<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b</w> <w n="37.6">t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-37">e</seg></w> <w n="37.7"><seg phoneme="u" type="vs" value="1" rule="426">ou</seg></w> <w n="37.8">bl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="38" num="10.2"><w n="38.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="38.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg>s</w> <w n="38.3">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="38.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w>, <w n="38.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="38.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="38.7">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
					<l n="39" num="10.3"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="39.2">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="39.3">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="39.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="39.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="40" num="10.4"><w n="40.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="40.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="40.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="40.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.5">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="40.6"><seg phoneme="a" type="vs" value="1" rule="343">a</seg><seg phoneme="j" type="sc" value="0" rule="475">ï</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="41.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="41.3">p<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="41.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="41.5">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="41.6">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="42" num="11.2"><w n="42.1">J<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cqu<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="42.2">B<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nh<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="42.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="42.4">R<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>,</l>
					<l n="43" num="11.3"><w n="43.1"><seg phoneme="a" type="vs" value="1" rule="341">A</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="43.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="43.3">G<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="43.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="43.5">G<seg phoneme="a" type="vs" value="1" rule="340">a</seg>vr<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="44" num="11.4"><w n="44.1">V<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="44.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="44.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="44.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t</w> <w n="44.5">h<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>,</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="45.2">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>qu<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="45.3">d</w>’<w n="45.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="45.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="46" num="12.2"><w n="46.1">D<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="46.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="46.3">b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>tt<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="46.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="46.5">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w>,</l>
					<l n="47" num="12.3"><w n="47.1">M<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w>, <w n="47.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="47.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="47.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ts</w> <w n="47.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="47.6">b<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="48" num="12.4"><w n="48.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="48.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="48.3">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="48.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="48.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="48.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g</w> <w n="48.7">bl<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Novembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>