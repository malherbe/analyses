<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN486">
					<head type="number">XII</head>
					<head type="main">Ballade de Banville aux Enfants perdus</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.3">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.4">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w> <w n="1.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.6">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="1.9">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.2">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="2.3">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w>, <w n="2.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="2.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="2.6">l</w>’<w n="2.7"><seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="3.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="3.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="3.4">n</w>’<w n="3.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="3.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="3.7">qu</w>’<w n="3.8"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="3.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.10"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w></l>
						<l n="4" num="1.4"><w n="4.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>g<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="4.5">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.4">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="5.5">r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="6" num="1.6"><w n="6.1">Qu</w>’<w n="6.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="6.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="6.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s</w> <w n="6.7">f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ct<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w> !</l>
						<l n="7" num="1.7"><w n="7.1">Ch<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rch<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="7.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.3">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.4"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="7.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="7.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rs</w> <w n="7.7"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w></l>
						<l n="8" num="1.8"><w n="8.1">S</w>’<w n="8.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.6">m<seg phoneme="i" type="vs" value="1" rule="493">y</seg>st<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="9" num="1.9"><w n="9.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="9.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="9.3"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.4">ch<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="9.5">d</w>’<w n="9.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="9.7">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w> :</l>
						<l n="10" num="1.10"><w n="10.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="10.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="10.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="10.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="11.4">s<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.5">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="11.6">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w> ;</l>
						<l n="12" num="2.2"><w n="12.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="12.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rv<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">pr<seg phoneme="wa" type="vs" value="1" rule="423">oi</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="12.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="12.6">n<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="12.7">n<seg phoneme="o" type="vs" value="1" rule="318">au</seg>fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="13" num="2.3"><w n="13.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.2">f<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="13.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="13.5">p<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="13.6">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="13.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w></l>
						<l n="14" num="2.4"><w n="14.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="14.2">l</w>’<w n="14.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="i" type="vs" value="1" rule="487">i</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.7"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="15" num="2.5"><w n="15.1">Qu</w>’<w n="15.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="465">im</seg>p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> ! <w n="15.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ll<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="15.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="15.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="15.7">p<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="i" type="vs" value="1" rule="321">y</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
						<l n="16" num="2.6"><w n="16.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lgr<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="16.4">j<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="16.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.7">r<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w>,</l>
						<l n="17" num="2.7"><w n="17.1">V<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w>, <w n="17.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="17.3">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w>,</l>
						<l n="18" num="2.8"><w n="18.1">L<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="18.6">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="19" num="2.9"><w n="19.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="19.2">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="19.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>rs</w> <w n="19.5">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="19.6">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.7">r<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="19.8">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w>,</l>
						<l n="20" num="2.10"><w n="20.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="20.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.6">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.2">s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rp<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="21.3">gr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="21.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.5">tr<seg phoneme="ɛ" type="vs" value="1" rule="305">aî</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="21.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="21.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.8">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w></l>
						<l n="22" num="3.2"><w n="22.1"><seg phoneme="u" type="vs" value="1" rule="426">Où</seg></w> <w n="22.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="22.3">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w>, <w n="22.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.5">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="22.6"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="23" num="3.3"><w n="23.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="23.2">tr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="23.3">d</w>’<w n="23.4"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="23.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.6">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rg<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="23.8">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="23.9"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>cc<seg phoneme="œ" type="vs" value="1" rule="345">ue</seg>il</w> !</l>
						<l n="24" num="3.4"><w n="24.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="24.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="24.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="24.6">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="24.7">h<seg phoneme="o" type="vs" value="1" rule="318">au</seg>t</w> <w n="24.8">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="25" num="3.5"><w n="25.1">N<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="25.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="25.3">l</w>’<w n="25.4">h<seg phoneme="i" type="vs" value="1" rule="493">y</seg>mn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.6">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.7">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="26" num="3.6"><w n="26.1">H<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="26.2">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="26.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.5">c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rps</w> <w n="26.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w>,</l>
						<l n="27" num="3.7"><w n="27.1">F<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="27.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="27.3">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="27.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="27.5">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="27.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>squ<seg phoneme="i" type="vs" value="1" rule="491">i</seg>fs</w>,</l>
						<l n="28" num="3.8"><w n="28.1">V<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="28.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="28.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="28.4">b<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="28.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="28.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="28.7">p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="29" num="3.9"><w n="29.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.2">d</w>’<w n="29.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="29.4">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="29.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="29.6">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="29.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>sc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w> :</l>
						<l n="30" num="3.10"><w n="30.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="30.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="30.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="30.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="30.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.6">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="31" num="4.1"><w n="31.1">R<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="31.2">d</w>’<w n="31.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="31.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="31.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="31.6">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w> !</l>
						<l n="32" num="4.2"><w n="32.1"><seg phoneme="wa" type="vs" value="1" rule="420">Oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="32.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="32.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="32.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.5">br<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="32.6"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>xp<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w>,</l>
						<l n="33" num="4.3"><w n="33.1">N<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.2">s<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="33.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="33.4">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="33.5"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="33.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="33.7">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="33.8">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="34" num="4.4"><w n="34.1">V<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>, <w n="34.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w>, <w n="34.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="34.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="34.5">D<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="34.6">pr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w> !</l>
						<l n="35" num="4.5"><w n="35.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">Em</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rqu<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>-<w n="35.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="35.3">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="35.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="35.5">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="35.6">C<seg phoneme="i" type="vs" value="1" rule="493">y</seg>th<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1861">Mai 1861.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>