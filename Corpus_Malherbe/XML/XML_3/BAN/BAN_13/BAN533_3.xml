<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN533">
				<head type="main">La Satyresse</head>
				<head type="sub_1">Frontispice, à Léopold Flameng</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">n</w>’<w n="1.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="1.5">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="1.6"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt</w> <w n="2.4">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.5">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="2.6">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Pr<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="3.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>, <w n="3.3">qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.4">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.5">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="3.6">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="4" num="1.4"><w n="4.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.2">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>r</w> <w n="4.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.6">R<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="5.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4">pr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="5.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="5.6">fl<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w></l>
					<l n="6" num="2.2"><w n="6.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.5">fl<seg phoneme="y" type="vs" value="1" rule="445">û</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="6.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="6.7">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> !</l>
					<l n="7" num="2.3"><w n="7.1"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">In</seg>d<seg phoneme="o" type="vs" value="1" rule="444">o</seg>c<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.4">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="8" num="2.4"><w n="8.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.2">s</w>’<w n="8.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="8.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.6">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">j<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="9.3">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="a" type="vs" value="1" rule="340">â</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="9.4"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="3.2"><w n="10.1"><seg phoneme="o" type="vs" value="1" rule="435">O</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="10.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">g<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.5">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="11.2">z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>ph<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r</w> <w n="11.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.5">l<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.6">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.2">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="i" type="vs" value="1" rule="493">y</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="12.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="12.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="12.5">f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="13" num="4.2"><w n="13.1">F<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="13.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="13.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="13.4">j<seg phoneme="w" type="sc" value="0" rule="431">ou</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>rs</w>,</l>
					<l n="14" num="4.3"><w n="14.1"><seg phoneme="y" type="vs" value="1" rule="453">U</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.2">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.4">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ts</w> <w n="14.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>squ<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1869">Mars 1869.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>