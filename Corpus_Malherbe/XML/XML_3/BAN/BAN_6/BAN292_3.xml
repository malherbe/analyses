<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN292">
				<head type="main">A Charles Asselineau</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">V<seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>n<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="1.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.3">l<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="1.4">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ffr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>,</l>
					<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">V<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">br<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.3">m</w>’<w n="2.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="3.4">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.6">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="4.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.5">M<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="5.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="5.3">f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="5.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="5.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> ;</l>
					<l n="6" num="2.2"><space quantity="4" unit="char"></space><w n="6.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="6.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.4">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="6.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.6">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="2.3"><w n="7.1">V<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="7.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>t</w> <w n="7.5">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="7.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="7.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>,</l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space><w n="8.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">pl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="8.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="8.5">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="9.2">f<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="9.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="9.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.6">l<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w> <w n="9.7">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w></l>
					<l n="10" num="3.2"><space quantity="4" unit="char"></space><w n="10.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="10.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.3">tr<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.5">c<seg phoneme="i" type="vs" value="1" rule="493">y</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="11" num="3.3"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="11.2">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.3">m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="11.4">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="11.5">l</w>’<w n="11.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt</w> <w n="11.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.8">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w></l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space><w n="12.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="12.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.3">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="12.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="12.5">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="13.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.3">t</w>’<w n="13.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.6">j<seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="14" num="4.2"><space quantity="4" unit="char"></space><w n="14.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="14.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.3">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.4">bl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="15" num="4.3"><w n="15.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.2">m<seg phoneme="o" type="vs" value="1" rule="444">o</seg>d<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="15.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="15.4">b<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.5">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space><w n="16.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.2">f<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="16.3">pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w>-<w n="16.4"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="16.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">P<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="17.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.3">scr<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="17.4"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ge<seg phoneme="ɑ̃" type="vs" value="1" rule="311">an</seg>ts</w>,</l>
					<l n="18" num="5.2"><space quantity="4" unit="char"></space><w n="18.1">Tr<seg phoneme="o" type="vs" value="1" rule="433">o</seg>p</w> <w n="18.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="18.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="18.4">c<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="19" num="5.3"><w n="19.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="19.2">f<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="19.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.5">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.6">g<seg phoneme="ɑ̃" type="vs" value="1" rule="362">en</seg>s</w></l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space><w n="20.1">C<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ff<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="20.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">t<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="20.4">d</w>’<w n="20.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">F<seg phoneme="i" type="vs" value="1" rule="468">i</seg>rd<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="21.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="21.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="21.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="21.5"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.6">Th<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> !</l>
					<l n="22" num="6.2"><space quantity="4" unit="char"></space><w n="22.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="22.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="22.3">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w> <w n="22.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="22.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="23" num="6.3"><w n="23.1">V<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w> <w n="23.2">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="23.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="23.4">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="23.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.6">l<seg phoneme="o" type="vs" value="1" rule="444">o</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w></l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space><w n="24.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="24.2">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rds</w> <w n="24.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="24.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.5">G<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">L</w>’<w n="25.2">h<seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>th<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.3">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>nn<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="25.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
					<l n="26" num="7.2"><space quantity="4" unit="char"></space><w n="26.1">D<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="26.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="26.3">cl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="26.4">l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
					<l n="27" num="7.3"><w n="27.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="27.2">f<seg phoneme="œ" type="vs" value="1" rule="304">ai</seg>s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="27.4">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="27.5">d</w>’<w n="27.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w></l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space><w n="28.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.2">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="28.3">ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="28.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.2">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="29.3"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="29.4">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="29.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="29.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="30" num="8.2"><space quantity="4" unit="char"></space><w n="30.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="360">En</seg><seg phoneme="i" type="vs" value="1" rule="468">i</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="30.2">P<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="493">y</seg>mn<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="31" num="8.3"><w n="31.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="31.3">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="31.4">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="31.5">d</w>’<w n="31.6"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="31.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>t<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space><w n="32.1">Qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="32.2">n</w>’<w n="32.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="32.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="32.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="32.6">g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">V<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> ! <w n="33.2">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="33.3">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="33.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="33.5">g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="a" type="vs" value="1" rule="341">a</seg>mm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w></l>
					<l n="34" num="9.2"><space quantity="4" unit="char"></space><w n="34.1">N<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="34.2">g<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="34.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>scr<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="35" num="9.3"><w n="35.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg>x</w> <w n="35.2">s<seg phoneme="o" type="vs" value="1" rule="438">o</seg>ts</w>, <w n="35.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="35.4"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w></l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space><w n="36.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.2">r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="36.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="36.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="36.5">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="37.2">m<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg>s</w>, <w n="37.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="37.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="37.5">l<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></l>
					<l n="38" num="10.2"><space quantity="4" unit="char"></space><w n="38.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="38.2"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="38.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>nn<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="38.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="39" num="10.3"><w n="39.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="39.2">d</w>’<w n="39.3"><seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="39.4"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>m<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>, <w n="39.5">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="39.6">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="39.7">b<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="375">en</seg></w></l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space><w n="40.1">Ch<seg phoneme="e" type="vs" value="1" rule="347">ez</seg></w> <w n="40.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="40.3">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="40.4">F<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="41.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="41.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="41.4"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="41.5">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> ;</l>
					<l n="42" num="11.2"><space quantity="4" unit="char"></space><w n="42.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="42.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="42.3">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="43" num="11.3"><w n="43.1">D<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="43.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>qu<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s</w> ; <w n="43.3"><seg phoneme="e" type="vs" value="1" rule="409">É</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="43.4">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w></l>
					<l n="44" num="11.4"><space quantity="4" unit="char"></space><w n="44.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="44.2"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="44.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="44.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>pr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>dr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1"><seg phoneme="o" type="vs" value="1" rule="318">Au</seg></w> <w n="45.2">m<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="45.3"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="45.4">s</w>’<w n="45.5"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="45.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="45.7">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="46" num="12.2"><space quantity="4" unit="char"></space><w n="46.1">T<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="46.2">c<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="46.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="46.4">l</w>’<w n="46.5"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="46.6"><seg phoneme="ɛ" type="vs" value="1" rule="305">ai</seg>m<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="47" num="12.3"><w n="47.1">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="366">e</seg>nn<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="47.2">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="47.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="47.4">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="48" num="12.4"><space quantity="4" unit="char"></space><w n="48.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="48.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="48.3">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="48.4">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1"><seg phoneme="ɔ" type="vs" value="1" rule="443">O</seg>r</w>, <w n="49.2">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>st<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>, <w n="49.3">pr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="49.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="49.5">l</w>’<w n="49.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w></l>
					<l n="50" num="13.2"><space quantity="4" unit="char"></space><w n="50.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="50.2">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="50.3">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="50.4">pr<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="51" num="13.3"><w n="51.1">J</w>’<w n="51.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="51.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rr<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="51.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="51.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="51.6">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>t</w></l>
					<l n="52" num="13.4"><space quantity="4" unit="char"></space><w n="52.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="52.2"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lf<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="52.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="52.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="52.5">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>x<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w> ;</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="53.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="53.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="53.4">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rc</w> <w n="53.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="53.6">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="53.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>ge<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w>,</l>
					<l n="54" num="14.2"><space quantity="4" unit="char"></space><w n="54.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="54.2">s<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="54.3">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="54.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="54.5">f<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="55" num="14.3"><w n="55.1">Cr<seg phoneme="wa" type="vs" value="1" rule="420">oî</seg>t</w>, <w n="55.2">g<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>fl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="55.3">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>j<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="55.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="55.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rge<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w></l>
					<l n="56" num="14.4"><space quantity="4" unit="char"></space><w n="56.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>ts</w> <w n="56.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="56.3">r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="56.4">l</w>’<w n="56.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rc<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>