<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN387">
				<head type="main">Le Démêloir</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Quelle est celle-ci qui s’avance <lb></lb>
								comme l’Aurore lorsqu’elle se lève, <lb></lb>
								qui est belle comme la Lune et <lb></lb>
								éclatante comme le Soleil, <lb></lb>
								et qui est terrible comme une <lb></lb>
								armée rangée en bataille ? <lb></lb>
							</quote>
							<bibl>
								<hi rend="ital">Cantique des cantiques.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="4" unit="char"></space><w n="1.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.2">s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.3">qu</w>’<w n="1.4"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="1.6">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="1.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="1.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="1.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.10">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="2" num="1.2"><w n="2.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.3">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.4">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="2.5">d</w>’<w n="2.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>str<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.7"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="2.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="2.9">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.10">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="3" num="1.3"><w n="3.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="3.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="3.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="3.5">c<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.8">p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="e" type="vs" value="1" rule="241">e</seg>d</w> <w n="3.9">c<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.10"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.11">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w></l>
					<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="4.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rch<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="4.5">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gt<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w> <w n="4.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="4.8">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="4.9">d</w>’<w n="4.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>.</l>
					<l n="5" num="1.5"><w n="5.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.2">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="5.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="o" type="vs" value="1" rule="444">o</seg>n<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="5.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.6">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="5.7"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="6" num="1.6"><w n="6.1">S<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="6.3">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="6.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>x</w> <w n="6.5">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rf<seg phoneme="œ̃" type="vs" value="1" rule="268">um</seg></w> <w n="6.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.7">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.9">J<seg phoneme="y" type="vs" value="1" rule="450">u</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="7" num="1.7"><w n="7.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg>v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="7.2">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.4">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="7.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="7.6">n<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="7.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.8">C<seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>,</l>
					<l n="8" num="1.8"><w n="8.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="8.2">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.7">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lv<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.8">R<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w>.</l>
					<l n="9" num="1.9"><w n="9.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="9.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="9.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.7">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rc<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="9.9">f<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="10" num="1.10"><w n="10.1">L<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.3">f<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="10.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="10.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.9">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="11" num="1.11"><w n="11.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.2">r<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>g<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w>, <w n="11.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>t</w> <w n="11.5">pl<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>s</w> <w n="11.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.7">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>f<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="11.8">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="12" num="1.12"><w n="12.1">L</w>’<w n="12.2"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.3">fr<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>ch<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="12.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="12.7">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>sc<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w>,</l>
					<l n="13" num="1.13"><w n="13.1">C<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.2"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gl<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="13.3">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.5">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.6">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>ts</w> <w n="13.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>gr<seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="14" num="1.14"><w n="14.1"><seg phoneme="a" type="vs" value="1" rule="340">A</seg></w> <w n="14.2">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.3"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>st<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="14.4">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>fs</w> <w n="14.5">tr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>h<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="14.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="14.7">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ɛ" type="vs" value="1" rule="352">e</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="15" num="1.15"><space quantity="4" unit="char"></space><w n="15.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w>, <w n="15.2">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="15.4">V<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="15.5">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.6">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.7">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="15.9">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.10">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w>,</l>
					<l n="16" num="1.16"><w n="16.1">S<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="16.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="16.3">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="16.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>m<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w> <w n="16.5">d</w>’<w n="16.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="16.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>x</w> <w n="16.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>fl<seg phoneme="ɛ" type="vs" value="1" rule="190">e</seg>ts</w> <w n="16.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rts</w></l>
					<l n="17" num="1.17"><w n="17.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.2">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="17.3">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="17.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="17.5">s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.7">l</w>’<w n="17.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>lc<seg phoneme="o" type="vs" value="1" rule="415">ô</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="18" num="1.18"><w n="18.1">C<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>tt<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>pl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="18.3">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>l<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="18.5">l</w>’<w n="18.6"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="18.7">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.8"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="18.9">f<seg phoneme="o" type="vs" value="1" rule="318">au</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="19" num="1.19"><w n="19.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="19.2">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="19.3">m<seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s</w> <w n="19.4">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.5">st<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="19.6"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>ch<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="19.7">d</w>’<w n="19.8"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="19.9">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rs<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="20" num="1.20"><w n="20.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.2">fl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="20.3">d</w>’<w n="20.4">h<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w>, <w n="20.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.7">s<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>il</w> <w n="20.8">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.9">gl<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w></l>
					<l n="21" num="1.21"><w n="21.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="21.2">c<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340">â</seg>pr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.4">tr<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>, <w n="21.5">qu</w>’<w n="21.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.7">l<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="21.8"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.9">b<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="22" num="1.22"><w n="22.1"><seg phoneme="œ̃" type="vs" value="1" rule="452">Un</seg></w> <w n="22.2">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.3">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="22.5">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="22.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.7">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>qu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="22.9">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="22.10">p<seg phoneme="ɛ" type="vs" value="1" rule="384">ei</seg>gn<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Février 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>