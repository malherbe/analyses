<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ROSES DE NOËL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>600 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_15</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ROSES DE NOËL</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1878">1878</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN623">
				<head type="main">Zélie enfant</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="1.2">j</w>’<w n="1.3"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="1.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.5">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="1.6"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="j" type="sc" value="0" rule="471">i</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="1.7">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="1.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.9">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
					<l n="2" num="1.2"><w n="2.1">Cr<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="2.3">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="2.4">n<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>tr<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.5"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="2.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="2.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w> <w n="2.9">h<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w></l>
					<l n="3" num="1.3"><w n="3.1">S<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="3.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">t<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.4">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="3.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="3.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.7">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="3.8">fl<seg phoneme="ø" type="vs" value="1" rule="405">eu</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="4" num="1.4"><w n="4.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">p<seg phoneme="ɛ̃" type="vs" value="1" rule="386">ein</seg>dr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="4.3">p<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="4.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="4.5">d<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="4.6"><seg phoneme="o" type="vs" value="1" rule="444">o</seg></w> <w n="4.7">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="4.8">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="4.9">ch<seg phoneme="e" type="vs" value="1" rule="409">é</seg>r<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="5" num="1.5"><w n="5.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.2">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rtr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.3">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.5">s<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="5.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w>, <w n="5.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="5.8">j</w>’<w n="5.9"><seg phoneme="i" type="vs" value="1" rule="497">y</seg></w> <w n="5.10">m<seg phoneme="e" type="vs" value="1" rule="353">e</seg>ttr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w></l>
					<l n="6" num="1.6"><w n="6.1">S<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.2">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="6.5">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>t<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="6.6">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.8">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.9">tr<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ts</w>,</l>
					<l n="7" num="1.7"><w n="7.1">S<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.2">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="7.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="7.4">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="7.5">p<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rs</w> <w n="7.6">qu</w>’<w n="7.7"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.8">cl<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rt<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="7.9">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="7.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="8" num="1.8"><w n="8.1">Fl<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="8.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="8.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="8.5">j<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="8.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="8.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>rn<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="9" num="1.9"><space quantity="2" unit="char"></space><w n="9.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="9.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="9.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="9.5">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.6">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="9.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="9.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="9.10">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="9.11">pr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>pt</w> !</l>
					<l n="10" num="1.10"><w n="10.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.2"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="10.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="10.4">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="10.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="10.7">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.8">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>,</l>
					<l n="11" num="1.11"><w n="11.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="11.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="11.3">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> : <w n="11.4">V<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="11.5">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="11.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="11.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="11.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="11.9">f<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="12" num="1.12"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="12.4">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="12.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>f<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> ! <w n="12.6">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="12.7">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="12.8">Z<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="13" num="1.13"><w n="13.1">B<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="13.3">h<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd</w> <w n="13.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.5">l</w>’<w n="13.6">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="13.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.8">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="13.9">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.10">th<seg phoneme="ɛ̃" type="vs" value="1" rule="494">ym</seg></w>,</l>
					<l n="14" num="1.14"><w n="14.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.2"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="14.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.4"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="14.5">l</w>’<w n="14.6"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="14.7">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="14.8">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> ;</l>
					<l n="15" num="1.15"><w n="15.1"><seg phoneme="ɛ" type="vs" value="1" rule="358">E</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.2">c<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="15.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="15.4">l</w>’<w n="15.5">h<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rb<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.7">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="15.8">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="15.9">s<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
					<l n="16" num="1.16"><w n="16.1">D<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="16.2">r<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w>, <w n="16.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.4">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.5">fl<seg phoneme="o" type="vs" value="1" rule="435">o</seg>tt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> <w n="16.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="16.7">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.8"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Gr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="17.2">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="17.3">p<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rt<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="17.4">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.5">fl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>rs</w> <w n="17.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="17.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="17.8">br<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="17.9">n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
					<l n="18" num="1.18"><w n="18.1">L<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="18.2">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>b<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="18.4">s<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.5">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="18.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.7"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>g<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w>,</l>
					<l n="19" num="1.19"><w n="19.1">D<seg phoneme="i" type="vs" value="1" rule="468">i</seg>str<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="19.3">c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>d<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.4">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="19.5">qu<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>lqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="19.6">ch<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="20" num="1.20"><w n="20.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="20.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.3">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="20.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="20.5">l</w>’<w n="20.6"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>r</w> <w n="20.7">d</w>’<w n="20.8"><seg phoneme="y" type="vs" value="1" rule="453">u</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.9">p<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>t<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="20.10">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1869"> 18 novembre 1869.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>