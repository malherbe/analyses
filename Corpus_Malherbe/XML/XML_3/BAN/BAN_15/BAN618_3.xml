<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ROSES DE NOËL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>600 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_15</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ROSES DE NOËL</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1878">1878</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN618">
				<head type="main">Les Oiseaux</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1"><seg phoneme="o" type="vs" value="1" rule="444">O</seg></w> <w n="1.2">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="1.3">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="1.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="1.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>d<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="1.7"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rg<seg phoneme="œ" type="vs" value="1" rule="344">ue</seg>il</w> !</l>
					<l n="2" num="1.2"><w n="2.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="2.2">p<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>s<seg phoneme="e" type="vs" value="1" rule="409">é</seg><seg phoneme="ə" type="ee" value="0" rule="e-34">e</seg></w> <w n="2.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="2.4">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="2.5">s</w>’<w n="2.6"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>v<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.7">j<seg phoneme="y" type="vs" value="1" rule="450">u</seg>squ</w>’<w n="2.8"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.9">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w></l>
					<l n="3" num="1.3"><w n="3.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="3.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="3.4">r<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="3.5"><seg phoneme="u" type="vs" value="1" rule="426">où</seg></w> <w n="3.6">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.7">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>t</w> <w n="3.8">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="3.9">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>p<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="4" num="1.4"><w n="4.1">L<seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="4.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.3">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="4.5">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="4.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.7">m<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="4.8">v<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.10">r<seg phoneme="o" type="vs" value="1" rule="444">o</seg>s<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="5" num="1.5"><w n="5.1"><seg phoneme="u" type="vs" value="1" rule="426">Ou</seg></w> <w n="5.2">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="5.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="5.4"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>rbr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="5.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="5.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.7">f<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>ill<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="5.8">m<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="6" num="1.6"><w n="6.1">Pl<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="6.2"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.3">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="6.4">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="6.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg></w> <w n="6.6">fr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ss<seg phoneme="o" type="vs" value="1" rule="435">o</seg>nn<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="6.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="6.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w></l>
					<l n="7" num="1.7"><w n="7.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="7.2">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w>, <w n="7.3"><seg phoneme="a" type="vs" value="1" rule="341">a</seg>n<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="7.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.5">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.6">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="7.7">gr<seg phoneme="a" type="vs" value="1" rule="340">â</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="7.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>f<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="8" num="1.8"><w n="8.1">M<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="8.2">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="8.3"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="8.4">b<seg phoneme="o" type="vs" value="1" rule="315">eau</seg></w> <w n="8.5">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="8.6">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="8.7"><seg phoneme="a" type="vs" value="1" rule="341">â</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="8.8">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="8.9">b<seg phoneme="e" type="vs" value="1" rule="409">é</seg>n<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w> !</l>
					<l n="9" num="1.9"><w n="9.1">C<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.2">n</w>’<w n="9.3"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="9.4">qu</w>’<w n="9.5"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="9.6">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>g<seg phoneme="ə" type="ee" value="0" rule="e-28">e</seg></w>, <w n="9.7">h<seg phoneme="e" type="vs" value="1" rule="409">é</seg>l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> ! <w n="9.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">En</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.9">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="9.10"><seg phoneme="o" type="vs" value="1" rule="415">ô</seg></w> <w n="9.11">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rm<seg phoneme="ɑ̃" type="vs" value="1" rule="369">en</seg>t</w> !</l>
					<l n="10" num="1.10"><w n="10.1">S<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="10.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.3">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="10.4">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="10.5">n<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>br<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="10.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="10.7">l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="10.8">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rd<seg phoneme="o" type="vs" value="1" rule="444">o</seg>nn<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w>,</l>
					<l n="11" num="1.11"><w n="11.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.2">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>ps</w>, <w n="11.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.4">n<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>ts</w>, <w n="11.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="11.6">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="11.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.8">s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="11.9">l</w>’<w n="11.10"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>sp<seg phoneme="a" type="vs" value="1" rule="340">a</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="12" num="1.12"><w n="12.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.2">c<seg phoneme="o" type="vs" value="1" rule="435">o</seg>ll<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="12.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.4">b<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w>, <w n="12.5">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="12.6">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="12.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.8">v<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="12.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="12.10">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="13" num="1.13"><w n="13.1">M<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s</w> <w n="13.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="13.3"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="13.4">l<seg phoneme="e" type="vs" value="1" rule="409">é</seg>g<seg phoneme="e" type="vs" value="1" rule="347">er</seg>s</w>, <w n="13.5">v<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="13.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.7">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="13.8">s<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="13.9">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w></l>
					<l n="14" num="1.14"><w n="14.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.2">m<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.3">n<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d</w>, <w n="14.4">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="14.5"><seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s<seg phoneme="o" type="vs" value="1" rule="315">eau</seg>x</w> <w n="14.6">r<seg phoneme="a" type="vs" value="1" rule="340">a</seg>p<seg phoneme="i" type="vs" value="1" rule="468">i</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="14.7"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="14.8">s<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w></l>
					<l n="15" num="1.15"><w n="15.1">D<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="15.2">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg>l<seg phoneme="ɥ" type="sc" value="0" rule="458">u</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="15.3">f<seg phoneme="ɥi" type="vs" value="1" rule="462">u</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="15.4">v<seg phoneme="ɛ" type="vs" value="1" rule="64">e</seg>rs</w> <w n="15.5">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.6">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="15.7">c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
					<l n="16" num="1.16"><w n="16.1">D<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="16.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="16.3">v<seg phoneme="a" type="vs" value="1" rule="307">a</seg>ill<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="16.5">m<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="16.6"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="16.7">d</w>’<w n="16.8"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="16.9"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>t<seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>c<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
					<l n="17" num="1.17"><w n="17.1"><seg phoneme="i" type="vs" value="1" rule="468">I</seg>ls</w> <w n="17.2">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> : <w n="17.3">C<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.4">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w>, <w n="17.5">l</w>’<w n="17.6">h<seg phoneme="œ̃" type="vs" value="1" rule="261">um</seg>bl<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="17.7">p<seg phoneme="o" type="vs" value="1" rule="444">o</seg><seg phoneme="ɛ" type="vs" value="1" rule="414">ë</seg>t<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w> <w n="17.8"><seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>bsc<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w></l>
					<l n="18" num="1.18"><w n="18.1"><seg phoneme="ɛ" type="vs" value="1" rule="199">E</seg>st</w> <w n="18.2"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.3"><seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>spr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="18.4"><seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>l<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="18.5">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="18.6">s</w>’<w n="18.7"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="18.8">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="18.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="18.10">l</w>’<w n="18.11"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>z<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w>.</l>
					<l n="19" num="1.19"><w n="19.1">Pr<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="19.2"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="19.3">c<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="19.4">r<seg phoneme="i" type="vs" value="1" rule="467">i</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="19.5">n<seg phoneme="o" type="vs" value="1" rule="438">o</seg>s</w> <w n="19.6">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="19.7">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>.</l>
					<l n="20" num="1.20"><w n="20.1">P<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="20.2">l</w>’<w n="20.3"><seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg></w> <w n="20.4">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.5">v<seg phoneme="j" type="sc" value="0" rule="371">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>t</w>, <w n="20.6"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>l</w> <w n="20.7">n<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="20.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="20.9">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.11">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="20.12">b<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
					<l n="21" num="1.21"><w n="21.1">C<seg phoneme="a" type="vs" value="1" rule="340">a</seg>r</w> <w n="21.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.3"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>b<seg phoneme="ɛ" type="vs" value="1" rule="382">e</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="21.4">d</w>’<w n="21.5"><seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w> <w n="21.6">v<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>lt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="21.7">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="21.8">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.9">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
					<l n="22" num="1.22"><w n="22.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="22.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="22.3">s<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.4">b<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ch<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>. <w n="22.5">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w>, <w n="22.6">m<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ee" value="0" rule="e-25">e</seg></w>, <w n="22.7"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="22.8">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>g<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rd<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>r<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w></l>
					<l n="23" num="1.23"><w n="23.1">L</w>’<w n="23.2"><seg phoneme="o" type="vs" value="1" rule="318">au</seg>r<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.3">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.5">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>v<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="23.6"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="23.7">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="23.8">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="23.9">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="23.10">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="23.11">n<seg phoneme="ɛ" type="vs" value="1" rule="308">aî</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
					<l n="24" num="1.24"><w n="24.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w>, <w n="24.2">j<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="24.3"><seg phoneme="i" type="vs" value="1" rule="468">i</seg>ls</w> <w n="24.4">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>dr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.5">v<seg phoneme="o" type="vs" value="1" rule="444">o</seg>l<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="24.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="24.7">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="24.8">f<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>tr<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1862"> 18 novembre 1862.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>