<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA HAINE</head><div type="poem" key="BRS44">
					<head type="main">APPARITION</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Hélas, si nous savions la fin de la journée.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="1.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="1.3">ch<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.4">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="1.5">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="1.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.7">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>p<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>t<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="1.8">n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="2" num="1.2"><w n="2.1">D<seg phoneme="e" type="vs" value="1" rule="409">é</seg>p<seg phoneme="u" type="vs" value="1" rule="428">ou</seg>ill<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="2.2">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="2.3">m<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>s</w> <w n="2.4"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="2.5">b<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rd</w> <w n="2.6">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="2.7">l</w>’<w n="2.8"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>n<seg phoneme="y" type="vs" value="1" rule="457">u</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="3" num="1.3"><w n="3.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="3.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.3">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="3.4">ch<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>br<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.5">f<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="3.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="3.7">l</w>’<w n="3.8">h<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="3.9">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="3.10"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>br<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>,</l>
						<l n="4" num="1.4"><w n="4.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="4.2">f<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>d</w> <w n="4.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="4.4">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> <w n="4.5"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="4.6">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.7"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="4.8">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt</w> <w n="4.9">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="4.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="4.11">cr<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w>.</l>
						<l n="5" num="1.5"><w n="5.1">Qu<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="5.2">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="5.3">r<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="5.4">s<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="5.5">t<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w> <w n="5.6">l<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>ss<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="5.7">d<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rm<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r</w> <w n="5.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>qu<seg phoneme="i" type="vs" value="1" rule="485">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="6" num="1.6"><w n="6.1">L<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.2">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="6.3">r<seg phoneme="ɛ" type="vs" value="1" rule="411">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg>s</w> <w n="6.5">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="6.6">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="6.7">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="6.8">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>ll<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w></l>
						<l n="7" num="1.7"><w n="7.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="7.2">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="7.3">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>t</w> <w n="7.4">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="7.5"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="7.6">d<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="7.7">s</w>’<w n="7.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="466">in</seg>cl<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="7.9">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="7.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="7.11">c<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>,</l>
						<l n="8" num="1.8"><w n="8.1">J<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.2">t<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="8.4">t</w>’<w n="8.5"><seg phoneme="e" type="vs" value="1" rule="409">é</seg>b<seg phoneme="o" type="vs" value="1" rule="318">au</seg>ch<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w>, <w n="8.6">r<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>v<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="8.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="8.8">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w> <w n="8.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="8.10">l<seg phoneme="wɛ̃" type="vs" value="1" rule="417">oin</seg></w>.</l>
						<l n="9" num="1.9"><w n="9.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="9.2">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="9.3">bl<seg phoneme="ɛ" type="vs" value="1" rule="412">ê</seg>m<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.4">d<seg phoneme="e" type="vs" value="1" rule="409">é</seg>c<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>r</w>, <w n="9.5">l<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rsqu<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="9.6">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="9.7">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="9.8">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="9.9">ch<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="10" num="1.10"><w n="10.1">T<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.2">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w>, <w n="10.3"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="10.4">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>r<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>t</w>, <w n="10.5">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>nt</w> <w n="10.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="10.7">d<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="10.8">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg>rm<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg>s</w>,</l>
						<l n="11" num="1.11"><w n="11.1">C</w>’<w n="11.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w>, <w n="11.6">c</w>’<w n="11.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="11.8">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.9">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="11.10">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="11.11">l<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>gu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w>,</l>
						<l n="12" num="1.12"><w n="12.1">C</w>’<w n="12.2"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="12.3">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.4">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>r</w> <w n="12.5"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="12.6">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="12.7">s<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>r</w>, <w n="12.8">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.9">fr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-22">e</seg>s</w> <w n="12.10">s<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="12.11">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w> !</l>
						<l n="13" num="1.13"><w n="13.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="13.2">fr<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="13.3">l<seg phoneme="ɑ̃" type="vs" value="1" rule="212">en</seg>t</w> <w n="13.4"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="13.5">br<seg phoneme="u" type="vs" value="1" rule="427">ou</seg>ill<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="13.6">n</w>’<w n="13.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="13.8">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="13.9">qu</w>’<w n="13.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="13.11">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c</w> <w n="13.12">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="14" num="1.14"><w n="14.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="14.2"><seg phoneme="œ" type="vs" value="1" rule="286">œ</seg>il</w> <w n="14.3">n</w>’<w n="14.4"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="14.5">pl<seg phoneme="y" type="vs" value="1" rule="450">u</seg>s</w> <w n="14.6">qu<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="14.7">tr<seg phoneme="i" type="vs" value="1" rule="468">i</seg>st<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="14.8"><seg phoneme="ɛ̃" type="vs" value="1" rule="302">ain</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="14.9">qu</w>’<w n="14.10"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="14.11">v<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="14.12">v<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rt<seg phoneme="i" type="vs" value="1" rule="468">i</seg>g<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="15" num="1.15"><w n="15.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="15.2">s<seg phoneme="y" type="vs" value="1" rule="450">u</seg>r</w> <w n="15.3">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="15.4">l<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="15.5">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="15.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="15.7">l</w>’<w n="15.8"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w> <w n="15.9">pl<seg phoneme="i" type="vs" value="1" rule="468">i</seg></w> <w n="15.10">m<seg phoneme="ɔ" type="vs" value="1" rule="443">o</seg>qu<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w></l>
						<l n="16" num="1.16"><w n="16.1">S</w>’<w n="16.2"><seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>tr</w>’<w n="16.3"><seg phoneme="u" type="vs" value="1" rule="425">ou</seg>vr<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="16.4">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="16.5">l<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.6">s<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>gl<seg phoneme="o" type="vs" value="1" rule="438">o</seg>t</w> <w n="16.7">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="16.8">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="16.9">c<seg phoneme="œ" type="vs" value="1" rule="249">œu</seg>r</w>…</l>
						<l n="17" num="1.17"><w n="17.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="17.2">j<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="17.3">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>s</w> <w n="17.4">l<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.5">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="17.6">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="17.7">v<seg phoneme="i" type="vs" value="1" rule="468">i</seg>t</w> <w n="17.8">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="17.9">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="17.10">p<seg phoneme="o" type="vs" value="1" rule="318">au</seg>p<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="18" num="1.18"><w n="18.1">T<seg phoneme="ɔ̃" type="vs" value="1" rule="418">om</seg>b<seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="18.2">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.3">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="18.4">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ds</w> <w n="18.5"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="18.6">c<seg phoneme="ɔ" type="vs" value="1" rule="419">o</seg>mm<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="18.7"><seg phoneme="œ̃" type="vs" value="1" rule="452">un</seg></w> <w n="18.8">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg></w> <w n="18.9">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="18.10">l<seg phoneme="y" type="vs" value="1" rule="453">u</seg>m<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>r<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="19" num="1.19"><w n="19.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="19.2">v<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="373">en</seg>s</w>, <w n="19.3">tr<seg phoneme="ɛ" type="vs" value="1" rule="410">è</seg>s</w> <w n="19.4">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg>lh<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r<seg phoneme="ø" type="vs" value="1" rule="403">eu</seg>s<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="19.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="19.6">f<seg phoneme="wa" type="vs" value="1" rule="440">o</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="e" type="vs" value="1" rule="347">er</seg></w> <w n="19.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="19.8">f<seg phoneme="y" type="vs" value="1" rule="450">u</seg>t</w> <w n="19.9">t<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w>,</l>
						<l n="20" num="1.20"><w n="20.1">T<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="20.2">m<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.3">t<seg phoneme="ɑ̃" type="vs" value="1" rule="364">en</seg>ds</w> <w n="20.4">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>gu<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɑ̃" type="vs" value="1" rule="368">en</seg>t</w> <w n="20.5">t<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="20.6">m<seg phoneme="ɛ̃" type="vs" value="1" rule="303">ain</seg></w> <w n="20.7">qu<seg phoneme="i" type="vs" value="1" rule="491">i</seg></w> <w n="20.8">n<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="20.9">p<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>t</w> <w n="20.10">r<seg phoneme="j" type="sc" value="0" rule="481">i</seg><seg phoneme="ɛ̃" type="vs" value="1" rule="377">en</seg></w></l>
						<l n="21" num="1.21"><w n="21.1"><seg phoneme="e" type="vs" value="1" rule="189">E</seg>t</w> <w n="21.2">d<seg phoneme="ɑ̃" type="vs" value="1" rule="312">an</seg>s</w> <w n="21.3">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="21.4"><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="21.5">t<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>rn<seg phoneme="i" type="vs" value="1" rule="468">i</seg>s</w> <w n="21.6"><seg phoneme="a" type="vs" value="1" rule="342">à</seg></w> <w n="21.7">p<seg phoneme="ɛ" type="vs" value="1" rule="385">ei</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="21.8">l</w>’<w n="21.9"><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="21.10">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="22" num="1.22"><w n="22.1">L<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="22.2">fr<seg phoneme="a" type="vs" value="1" rule="340">a</seg>g<seg phoneme="i" type="vs" value="1" rule="468">i</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="22.3">r<seg phoneme="ɛ" type="vs" value="1" rule="339">a</seg><seg phoneme="j" type="sc" value="0" rule="496">y</seg><seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="22.4">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="22.5">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="22.6">l<seg phoneme="ɑ̃" type="vs" value="1" rule="313">am</seg>p<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="22.7"><seg phoneme="ɛ" type="vs" value="1" rule="199">e</seg>st</w> <w n="22.8">d<seg phoneme="i" type="vs" value="1" rule="468">i</seg>v<seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>.</l>
						<l n="23" num="1.23"><w n="23.1">P<seg phoneme="ɥ" type="sc" value="0" rule="460">u</seg><seg phoneme="i" type="vs" value="1" rule="491">i</seg>s</w> <w n="23.2">t<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.3">t</w>’<w n="23.4"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">en</seg></w> <w n="23.5">v<seg phoneme="a" type="vs" value="1" rule="340">a</seg>s</w> <w n="23.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w>, <w n="23.7">s<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>ffr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>c<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="23.8">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="23.9">d<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>h<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rs</w>.</l>
					</lg>
					<lg n="2">
						<l n="24" num="2.1"><w n="24.1">D<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>l<seg phoneme="œ" type="vs" value="1" rule="407">eu</seg>r</w> <w n="24.2">p<seg phoneme="a" type="vs" value="1" rule="340">â</seg>l<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="24.3">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="24.4">c<seg phoneme="j" type="sc" value="0" rule="484">i</seg><seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>l</w> <w n="24.5">d<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w>, <w n="24.6">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>s</w> <w n="24.7">l<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="24.8">j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="24.9">s<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg>t</w> <w n="24.10">m<seg phoneme="ɔ" type="vs" value="1" rule="439">o</seg>rts</w>,</l>
						<l n="25" num="2.2"><w n="25.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="313">An</seg>g<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>ss<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="25.2">d<seg phoneme="y" type="vs" value="1" rule="450">u</seg></w> <w n="25.3">p<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="e" type="vs" value="1" rule="409">é</seg></w> <w n="25.4">t<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>j<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>rs</w> <w n="25.5"><seg phoneme="i" type="vs" value="1" rule="467">i</seg>n<seg phoneme="a" type="vs" value="1" rule="340">a</seg>ss<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w>,</l>
						<l n="26" num="2.3"><w n="26.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w>, <w n="26.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="26.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="26.4">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w>, <w n="26.5"><seg phoneme="o" type="vs" value="1" rule="318">au</seg></w> <w n="26.6">gr<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>d</w> <w n="26.7">s<seg phoneme="œ" type="vs" value="1" rule="406">eu</seg>il</w> <w n="26.8">d<seg phoneme="ə" type="em" value="1" rule="e-12">e</seg></w> <w n="26.9">m<seg phoneme="a" type="vs" value="1" rule="340">a</seg></w> <w n="26.10">v<seg phoneme="i" type="vs" value="1" rule="482">i</seg><seg phoneme="ə" type="ef" value="1" rule="e-5">e</seg></w></l>
						<l n="27" num="2.4"><w n="27.1"><seg phoneme="ɑ̃" type="vs" value="1" rule="174">En</seg></w> <w n="27.2">r<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>m<seg phoneme="ɥ" type="sc" value="0" rule="456">u</seg><seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>t</w> <w n="27.3">t<seg phoneme="ɔ̃" type="vs" value="1" rule="418">on</seg></w> <w n="27.4">v<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>l<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="27.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="27.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="27.7">d<seg phoneme="wa" type="vs" value="1" rule="420">oi</seg>gts</w> <w n="27.8">tr<seg phoneme="ɑ̃" type="vs" value="1" rule="364">em</seg>bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>ts</w> :</l>
						<l n="28" num="2.5"><w n="28.1">R<seg phoneme="ɛ" type="vs" value="1" rule="358">e</seg>st<seg phoneme="ə" type="ef" value="1" rule="e-24">e</seg></w> <w n="28.2">d<seg phoneme="u" type="vs" value="1" rule="425">ou</seg>c<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.3"><seg phoneme="e" type="vs" value="1" rule="189">e</seg>t</w> <w n="28.4">p<seg phoneme="ɛ" type="vs" value="1" rule="308">ai</seg>s<seg phoneme="i" type="vs" value="1" rule="468">i</seg>bl<seg phoneme="ə" type="ee" value="0" rule="e-23">e</seg></w> <w n="28.5"><seg phoneme="a" type="vs" value="1" rule="340">a</seg>v<seg phoneme="ɛ" type="vs" value="1" rule="346">e</seg>c</w> <w n="28.6">t<seg phoneme="ɛ" type="vs" value="1" rule="161">e</seg>s</w> <w n="28.7">ch<seg phoneme="ə" type="em" value="1" rule="e-19">e</seg>v<seg phoneme="ø" type="vs" value="1" rule="398">eu</seg>x</w> <w n="28.8">bl<seg phoneme="ɑ̃" type="vs" value="1" rule="313">an</seg>cs</w>.</l>
					</lg>
				</div></body></text></TEI>