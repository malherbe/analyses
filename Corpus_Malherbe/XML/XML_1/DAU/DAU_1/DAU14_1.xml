<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOUREUSES</title>
				<title type="sub">POEMES ET FANTAISIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DAU">
					<name>
						<forename>Alphonse</forename>
						<surname>DAUDET</surname>
					</name>
					<date from="1840" to="1897">1840-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
						<author>Alphonse Daudet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Amoureuses</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
								<author>Alphonse Daudet</author>
								<edition>NOUVELLE ÉDITION</edition>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BIBLIOTHÈQUE-CHARPENTIER, Eugène Fasquelle, éditeur</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1858">1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties en prose ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Le recueil a été découpé en parties et sous parties conformément à l’édition de 1882.</p>
				<correction>
					<p>Les majuscules des noms propres ont été restituées</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-12-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOUREUSES</head><div type="poem" key="DAU14">
					<head type="main">LES PRUNES</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Si</w> <w n="1.2">vous</w> <w n="1.3">voulez</w> <w n="1.4">savoir</w> <w n="1.5">comment</w></l>
							<l n="2" num="1.2"><w n="2.1">Nous</w> <w n="2.2">nous</w> <w n="2.3">aimâmes</w> <w n="2.4">pour</w> <w n="2.5">des</w> <w n="2.6">prunes</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">vous</w> <w n="3.3">le</w> <w n="3.4">dirai</w> <w n="3.5">doucement</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Si</w> <w n="4.2">vous</w> <w n="4.3">voulez</w> <w n="4.4">savoir</w> <w n="4.5">comment</w>.</l>
							<l n="5" num="1.5"><w n="5.1">L</w>’<w n="5.2">amour</w> <w n="5.3">vient</w> <w n="5.4">toujours</w> <w n="5.5">en</w> <w n="5.6">dormant</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Chez</w> <w n="6.2">les</w> <w n="6.3">bruns</w> <w n="6.4">comme</w> <w n="6.5">chez</w> <w n="6.6">les</w> <w n="6.7">brunes</w> ;</l>
							<l n="7" num="1.7"><w n="7.1">En</w> <w n="7.2">quelques</w> <w n="7.3">mots</w> <w n="7.4">voici</w> <w n="7.5">comment</w></l>
							<l n="8" num="1.8"><w n="8.1">Nous</w> <w n="8.2">nous</w> <w n="8.3">aimâmes</w> <w n="8.4">pour</w> <w n="8.5">des</w> <w n="8.6">prunes</w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="9" num="1.1"><w n="9.1">Mon</w> <w n="9.2">oncle</w> <w n="9.3">avait</w> <w n="9.4">un</w> <w n="9.5">grand</w> <w n="9.6">verger</w></l>
							<l n="10" num="1.2"><w n="10.1">Et</w> <w n="10.2">moi</w> <w n="10.3">j</w>’<w n="10.4">avais</w> <w n="10.5">une</w> <w n="10.6">cousine</w> ;</l>
							<l n="11" num="1.3"><w n="11.1">Nous</w> <w n="11.2">nous</w> <w n="11.3">aimions</w> <w n="11.4">sans</w> <w n="11.5">y</w> <w n="11.6">songer</w>,</l>
							<l n="12" num="1.4"><w n="12.1">Mon</w> <w n="12.2">oncle</w> <w n="12.3">avait</w> <w n="12.4">un</w> <w n="12.5">grand</w> <w n="12.6">verger</w>.</l>
							<l n="13" num="1.5"><w n="13.1">Les</w> <w n="13.2">oiseaux</w> <w n="13.3">venaient</w> <w n="13.4">y</w> <w n="13.5">manger</w>,</l>
							<l n="14" num="1.6"><w n="14.1">Le</w> <w n="14.2">printemps</w> <w n="14.3">faisait</w> <w n="14.4">leur</w> <w n="14.5">cuisine</w> ;</l>
							<l n="15" num="1.7"><w n="15.1">Mon</w> <w n="15.2">oncle</w> <w n="15.3">avait</w> <w n="15.4">un</w> <w n="15.5">grand</w> <w n="15.6">verger</w>,</l>
							<l n="16" num="1.8"><w n="16.1">Et</w> <w n="16.2">moi</w> <w n="16.3">j</w>’<w n="16.4">avais</w> <w n="16.5">une</w> <w n="16.6">cousine</w>.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="17" num="1.1"><w n="17.1">Un</w> <w n="17.2">matin</w> <w n="17.3">nous</w> <w n="17.4">nous</w> <w n="17.5">promenions</w></l>
							<l n="18" num="1.2"><w n="18.1">Dans</w> <w n="18.2">le</w> <w n="18.3">verger</w>, <w n="18.4">avec</w> <w n="18.5">Mariette</w> :</l>
							<l n="19" num="1.3"><w n="19.1">Tout</w> <w n="19.2">gentils</w>, <w n="19.3">tout</w> <w n="19.4">frais</w>, <w n="19.5">tout</w> <w n="19.6">mignons</w>,</l>
							<l n="20" num="1.4"><w n="20.1">Un</w> <w n="20.2">matin</w> <w n="20.3">nous</w> <w n="20.4">nous</w> <w n="20.5">promenions</w>.</l>
							<l n="21" num="1.5"><w n="21.1">Les</w> <w n="21.2">cigales</w> <w n="21.3">et</w> <w n="21.4">les</w> <w n="21.5">grillons</w></l>
							<l n="22" num="1.6"><w n="22.1">Nous</w> <w n="22.2">fredonnaient</w> <w n="22.3">une</w> <w n="22.4">ariette</w> :</l>
							<l n="23" num="1.7"><w n="23.1">Un</w> <w n="23.2">matin</w> <w n="23.3">nous</w> <w n="23.4">nous</w> <w n="23.5">promenions</w></l>
							<l n="24" num="1.8"><w n="24.1">Dans</w> <w n="24.2">le</w> <w n="24.3">verger</w> <w n="24.4">avec</w> <w n="24.5">Mariette</w>.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">De</w> <w n="25.2">tous</w> <w n="25.3">côtés</w>, <w n="25.4">d</w>’<w n="25.5">ici</w>, <w n="25.6">de</w> <w n="25.7">là</w>,</l>
							<l n="26" num="1.2"><w n="26.1">Les</w> <w n="26.2">oiseaux</w> <w n="26.3">chantaient</w> <w n="26.4">dans</w> <w n="26.5">les</w> <w n="26.6">branches</w>,</l>
							<l n="27" num="1.3"><w n="27.1">En</w> <w n="27.2">si</w> <w n="27.3">bémol</w>, <w n="27.4">en</w> <w n="27.5">ut</w>, <w n="27.6">en</w> <w n="27.7">la</w>,</l>
							<l n="28" num="1.4"><w n="28.1">De</w> <w n="28.2">tous</w> <w n="28.3">côtés</w>, <w n="28.4">d</w>’<w n="28.5">ici</w>, <w n="28.6">de</w> <w n="28.7">là</w>.</l>
							<l n="29" num="1.5"><w n="29.1">Les</w> <w n="29.2">prés</w> <w n="29.3">en</w> <w n="29.4">habit</w> <w n="29.5">de</w> <w n="29.6">gala</w></l>
							<l n="30" num="1.6"><w n="30.1">Étaient</w> <w n="30.2">pleins</w> <w n="30.3">de</w> <w n="30.4">fleurettes</w> <w n="30.5">blanches</w>.</l>
							<l n="31" num="1.7"><w n="31.1">De</w> <w n="31.2">tous</w> <w n="31.3">côtés</w>, <w n="31.4">d</w>’<w n="31.5">ici</w>, <w n="31.6">de</w> <w n="31.7">là</w>,</l>
							<l n="32" num="1.8"><w n="32.1">Les</w> <w n="32.2">oiseaux</w> <w n="32.3">chantaient</w> <w n="32.4">dans</w> <w n="32.5">les</w> <w n="32.6">branches</w>.</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">V</head>
						<lg n="1">
							<l n="33" num="1.1"><w n="33.1">Fraîche</w> <w n="33.2">sous</w> <w n="33.3">son</w> <w n="33.4">petit</w> <w n="33.5">bonnet</w>,</l>
							<l n="34" num="1.2"><w n="34.1">Belle</w> <w n="34.2">à</w> <w n="34.3">ravir</w>, <w n="34.4">et</w> <w n="34.5">point</w> <w n="34.6">coquette</w>,</l>
							<l n="35" num="1.3"><w n="35.1">Ma</w> <w n="35.2">cousine</w> <w n="35.3">se</w> <w n="35.4">démenait</w>,</l>
							<l n="36" num="1.4"><w n="36.1">Fraîche</w> <w n="36.2">sous</w> <w n="36.3">son</w> <w n="36.4">petit</w> <w n="36.5">bonnet</w>.</l>
							<l n="37" num="1.5"><w n="37.1">Elle</w> <w n="37.2">sautait</w>, <w n="37.3">allait</w>, <w n="37.4">venait</w>,</l>
							<l n="38" num="1.6"><w n="38.1">Comme</w> <w n="38.2">un</w> <w n="38.3">volant</w> <w n="38.4">sur</w> <w n="38.5">la</w> <w n="38.6">raquette</w> :</l>
							<l n="39" num="1.7"><w n="39.1">Fraîche</w> <w n="39.2">sous</w> <w n="39.3">son</w> <w n="39.4">petit</w> <w n="39.5">bonnet</w>,</l>
							<l n="40" num="1.8"><w n="40.1">Belle</w> <w n="40.2">à</w> <w n="40.3">ravir</w> <w n="40.4">et</w> <w n="40.5">point</w> <w n="40.6">coquette</w>.</l>
						</lg>
					</div>
					<div type="section" n="6">
						<head type="number">VI</head>
						<lg n="1">
							<l n="41" num="1.1"><w n="41.1">Arrivée</w> <w n="41.2">au</w> <w n="41.3">fond</w> <w n="41.4">du</w> <w n="41.5">verger</w>,</l>
							<l n="42" num="1.2"><w n="42.1">Ma</w> <w n="42.2">cousine</w> <w n="42.3">lorgne</w> <w n="42.4">les</w> <w n="42.5">prunes</w> ;</l>
							<l n="43" num="1.3"><w n="43.1">Et</w> <w n="43.2">la</w> <w n="43.3">gourmande</w> <w n="43.4">en</w> <w n="43.5">veut</w> <w n="43.6">manger</w>,</l>
							<l n="44" num="1.4"><w n="44.1">Arrivée</w> <w n="44.2">au</w> <w n="44.3">fond</w> <w n="44.4">du</w> <w n="44.5">verger</w>.</l>
							<l n="45" num="1.5"><w n="45.1">L</w>’<w n="45.2">arbre</w> <w n="45.3">est</w> <w n="45.4">bas</w> ; <w n="45.5">sans</w> <w n="45.6">se</w> <w n="45.7">déranger</w></l>
							<l n="46" num="1.6"><w n="46.1">Elle</w> <w n="46.2">en</w> <w n="46.3">fait</w> <w n="46.4">tomber</w> <w n="46.5">quelques</w>-<w n="46.6">unes</w> :</l>
							<l n="47" num="1.7"><w n="47.1">Arrivée</w> <w n="47.2">au</w> <w n="47.3">fond</w> <w n="47.4">du</w> <w n="47.5">verger</w>,</l>
							<l n="48" num="1.8"><w n="48.1">Ma</w> <w n="48.2">cousine</w> <w n="48.3">lorgne</w> <w n="48.4">les</w> <w n="48.5">prunes</w>.</l>
						</lg>
					</div>
					<div type="section" n="7">
						<head type="number">VII</head>
						<lg n="1">
							<l n="49" num="1.1"><w n="49.1">Elle</w> <w n="49.2">en</w> <w n="49.3">prend</w> <w n="49.4">une</w>, <w n="49.5">elle</w> <w n="49.6">la</w> <w n="49.7">mord</w>,</l>
							<l n="50" num="1.2"><w n="50.1">Et</w>, <w n="50.2">me</w> <w n="50.3">l</w>’<w n="50.4">offrant</w> : « <w n="50.5">Tiens</w> !… » <w n="50.6">me</w> <w n="50.7">dit</w>-<w n="50.8">elle</w>.</l>
							<l n="51" num="1.3"><w n="51.1">Mon</w> <w n="51.2">pauvre</w> <w n="51.3">cœur</w> <w n="51.4">battait</w> <w n="51.5">bien</w> <w n="51.6">fort</w> !</l>
							<l n="52" num="1.4"><w n="52.1">Elle</w> <w n="52.2">en</w> <w n="52.3">prend</w> <w n="52.4">une</w>, <w n="52.5">elle</w> <w n="52.6">la</w> <w n="52.7">mord</w>.</l>
							<l n="53" num="1.5"><w n="53.1">Ses</w> <w n="53.2">petites</w> <w n="53.3">dents</w> <w n="53.4">sur</w> <w n="53.5">le</w> <w n="53.6">bord</w></l>
							<l n="54" num="1.6"><w n="54.1">Avaient</w> <w n="54.2">fait</w> <w n="54.3">des</w> <w n="54.4">points</w> <w n="54.5">de</w> <w n="54.6">dentelle</w>…</l>
							<l n="55" num="1.7"><w n="55.1">Elle</w> <w n="55.2">en</w> <w n="55.3">prend</w> <w n="55.4">une</w>, <w n="55.5">elle</w> <w n="55.6">la</w> <w n="55.7">mord</w>,</l>
							<l n="56" num="1.8"><w n="56.1">Et</w>, <w n="56.2">me</w> <w n="56.3">l</w>’<w n="56.4">offrant</w> : « <w n="56.5">Tiens</w> !… » <w n="56.6">me</w> <w n="56.7">dit</w>-<w n="56.8">elle</w>.</l>
						</lg>
					</div>
					<div type="section" n="8">
						<head type="number">VIII</head>
						<lg n="1">
							<l n="57" num="1.1"><w n="57.1">Ce</w> <w n="57.2">fut</w> <w n="57.3">tout</w>, <w n="57.4">mais</w> <w n="57.5">ce</w> <w n="57.6">fut</w> <w n="57.7">assez</w> ;</l>
							<l n="58" num="1.2"><w n="58.1">Ce</w> <w n="58.2">seul</w> <w n="58.3">fruit</w> <w n="58.4">disait</w> <w n="58.5">bien</w> <w n="58.6">des</w> <w n="58.7">choses</w></l>
							<l n="59" num="1.3">(<w n="59.1">Si</w> <w n="59.2">j</w>’<w n="59.3">avais</w> <w n="59.4">su</w> <w n="59.5">ce</w> <w n="59.6">que</w> <w n="59.7">je</w> <w n="59.8">sais</w> !…)</l>
							<l n="60" num="1.4"><w n="60.1">Ce</w> <w n="60.2">fut</w> <w n="60.3">tout</w>, <w n="60.4">mais</w> <w n="60.5">ce</w> <w n="60.6">fut</w> <w n="60.7">assez</w>.</l>
							<l n="61" num="1.5"><w n="61.1">Je</w> <w n="61.2">mordis</w>, <w n="61.3">comme</w> <w n="61.4">vous</w> <w n="61.5">pensez</w>,</l>
							<l n="62" num="1.6"><w n="62.1">Sur</w> <w n="62.2">la</w> <w n="62.3">trace</w> <w n="62.4">des</w> <w n="62.5">lèvres</w> <w n="62.6">roses</w> :</l>
							<l n="63" num="1.7"><w n="63.1">Ce</w> <w n="63.2">fut</w> <w n="63.3">tout</w>, <w n="63.4">mais</w> <w n="63.5">ce</w> <w n="63.6">fut</w> <w n="63.7">assez</w> ;</l>
							<l n="64" num="1.8"><w n="64.1">Ce</w> <w n="64.2">seul</w> <w n="64.3">fruit</w> <w n="64.4">disait</w> <w n="64.5">bien</w> <w n="64.6">des</w> <w n="64.7">choses</w>.</l>
						</lg>
					</div>
					<div type="section" n="9">
						<head type="number">IX</head>
						<head type="main">À MES LECTRICES</head>
						<lg n="1">
							<l n="65" num="1.1"><w n="65.1">Oui</w>, <w n="65.2">mesdames</w>, <w n="65.3">voilà</w> <w n="65.4">comment</w></l>
							<l n="66" num="1.2"><w n="66.1">Nous</w> <w n="66.2">nous</w> <w n="66.3">aimâmes</w> <w n="66.4">pour</w> <w n="66.5">des</w> <w n="66.6">prunes</w> :</l>
							<l n="67" num="1.3"><w n="67.1">N</w>’<w n="67.2">allez</w> <w n="67.3">pas</w> <w n="67.4">l</w>’<w n="67.5">entendre</w> <w n="67.6">autrement</w> ;</l>
							<l n="68" num="1.4"><w n="68.1">Oui</w>, <w n="68.2">mesdames</w>, <w n="68.3">voilà</w> <w n="68.4">comment</w>.</l>
							<l n="69" num="1.5"><w n="69.1">Si</w> <w n="69.2">parmi</w> <w n="69.3">vous</w>, <w n="69.4">pourtant</w>, <w n="69.5">d</w>’<w n="69.6">aucunes</w></l>
							<l n="70" num="1.6"><w n="70.1">Le</w> <w n="70.2">comprenaient</w> <w n="70.3">différemment</w>,</l>
							<l n="71" num="1.7"><w n="71.1">Ma</w> <w n="71.2">foi</w>, <w n="71.3">tant</w> <w n="71.4">pis</w> ! <w n="71.5">voilà</w> <w n="71.6">comment</w></l>
							<l n="72" num="1.8"><w n="72.1">Nous</w> <w n="72.2">nous</w> <w n="72.3">aimâmes</w> <w n="72.4">pour</w> <w n="72.5">des</w> <w n="72.6">prunes</w>.</l>
						</lg>
					</div>
				</div></body></text></TEI>