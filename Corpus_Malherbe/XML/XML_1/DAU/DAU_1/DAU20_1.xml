<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOUREUSES</title>
				<title type="sub">POEMES ET FANTAISIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DAU">
					<name>
						<forename>Alphonse</forename>
						<surname>DAUDET</surname>
					</name>
					<date from="1840" to="1897">1840-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1472 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">DAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
						<author>Alphonse Daudet</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Amoureuses</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
							<title type="main">LES AMOUREUSES, POEMES ET FANTAISIES</title>
								<author>Alphonse Daudet</author>
								<edition>NOUVELLE ÉDITION</edition>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>BIBLIOTHÈQUE-CHARPENTIER, Eugène Fasquelle, éditeur</publisher>
									<date when="1908">1908</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1858">1858</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties en prose ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Le recueil a été découpé en parties et sous parties conformément à l’édition de 1882.</p>
				<correction>
					<p>Les majuscules des noms propres ont été restituées</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-12-17" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-12-17" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="drama" key="DAU20">
					<head type="main">LES AVENTURES D’UN PAPILLON <lb></lb>ET D’UNE BÊTE à BON DIEU</head>
					<div type="body">
						<set>
							Le théâtre représente la campagne. Il est six heures du soir ; 
							le soleil s’en va. Au lever du rideau, un Papillon bleu et une 
							jeune Bête à bon Dieu, du sexe mâle, causent à cheval sur un 
							brin de fougère. Ils se sont rencontrés le matin, et ont passé 
							la journée ensemble. Comme il est tard, la Bête à bon Dieu fait
							mine de se retirer.
						</set>
						<div type="scene" n="1">
							<sp n="1">
								<speaker>LE PAPILLON.</speaker>
								<l part="I" n="1"><w n="1.1">Quoi</w> !… <w n="1.2">tu</w> <w n="1.3">t</w>’<w n="1.4">en</w> <w n="1.5">vas</w> <w n="1.6">déjà</w> ?… </l>
							</sp>
							<sp n="2">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="F" n="1"><w n="1.7">Dame</w> ! <w n="1.8">il</w> <w n="1.9">faut</w> <w n="1.10">que</w> <w n="1.11">je</w> <w n="1.12">rentre</w> :</l>
								<l part="I" n="2"><w n="2.1">Il</w> <w n="2.2">est</w> <w n="2.3">tard</w>, <w n="2.4">songez</w> <w n="2.5">donc</w> ! </l>
							</sp>
							<sp n="3">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="2"><w n="2.6">Attends</w> <w n="2.7">un</w> <w n="2.8">peu</w>, <w n="2.9">que</w> <w n="2.10">diantre</w> !</l>
								<l n="3"><w n="3.1">Il</w> <w n="3.2">n</w>’<w n="3.3">est</w> <w n="3.4">jamais</w> <w n="3.5">trop</w> <w n="3.6">tard</w> <w n="3.7">pour</w> <w n="3.8">retourner</w> <w n="3.9">chez</w> <w n="3.10">soi</w>…</l>
								<l n="4"><w n="4.1">Moi</w> <w n="4.2">d</w>’<w n="4.3">abord</w>, <w n="4.4">je</w> <w n="4.5">m</w>’<w n="4.6">ennuie</w> <w n="4.7">à</w> <w n="4.8">ma</w> <w n="4.9">maison</w>, <w n="4.10">et</w> <w n="4.11">toi</w> ?</l>
								<l n="5"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">si</w> <w n="5.4">bête</w> <w n="5.5">une</w> <w n="5.6">porte</w>, <w n="5.7">un</w> <w n="5.8">mur</w>, <w n="5.9">une</w> <w n="5.10">croisée</w>.</l>
								<l n="6"><w n="6.1">Quand</w> <w n="6.2">au</w> <w n="6.3">dehors</w> <w n="6.4">on</w> <w n="6.5">a</w> <w n="6.6">le</w> <w n="6.7">soleil</w>, <w n="6.8">la</w> <w n="6.9">rosée</w>,</l>
								<l n="7"><w n="7.1">Et</w> <w n="7.2">les</w> <w n="7.3">coquelicots</w>, <w n="7.4">et</w> <w n="7.5">le</w> <w n="7.6">grand</w> <w n="7.7">air</w>, <w n="7.8">et</w> <w n="7.9">tout</w>.</l>
								<l n="8"><w n="8.1">Si</w> <w n="8.2">les</w> <w n="8.3">coquelicots</w> <w n="8.4">ne</w> <w n="8.5">sont</w> <w n="8.6">pas</w> <w n="8.7">de</w> <w n="8.8">ton</w> <w n="8.9">goût</w>,</l>
								<l part="I" n="9"><w n="9.1">Il</w> <w n="9.2">faut</w> <w n="9.3">le</w> <w n="9.4">dire</w>… </l>
							</sp>
							<sp n="4">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="F" n="9"><w n="9.5">Hélas</w> ! <w n="9.6">monsieur</w>, <w n="9.7">je</w> <w n="9.8">les</w> <w n="9.9">adore</w>.</l>
							</sp>
							<sp n="5">
								<speaker>LE PAPILLON.</speaker>
								<l n="10"><w n="10.1">Eh</w> <w n="10.2">bien</w> ! <w n="10.3">alors</w>, <w n="10.4">nigaud</w>, <w n="10.5">ne</w> <w n="10.6">t</w>’<w n="10.7">en</w> <w n="10.8">vas</w> <w n="10.9">pas</w> <w n="10.10">encore</w> ;</l>
								<l n="11"><w n="11.1">Reste</w> <w n="11.2">avec</w> <w n="11.3">moi</w>. <w n="11.4">Tu</w> <w n="11.5">vois</w>, <w n="11.6">il</w> <w n="11.7">fait</w> <w n="11.8">bon</w> ; <w n="11.9">l</w>’<w n="11.10">air</w> <w n="11.11">est</w> <w n="11.12">doux</w>…</l>
							</sp>
							<sp n="6">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="I" n="12"><w n="12.1">Oui</w>, <w n="12.2">mais</w>… </l>
							</sp>
							<sp n="7">
								<speaker>LE PAPILLON, la poussant dans l’herbe.</speaker>
								<l part="F" n="12"><w n="12.3">Eh</w> ! <w n="12.4">roule</w>-<w n="12.5">toi</w> <w n="12.6">dans</w> <w n="12.7">l</w>’<w n="12.8">herbe</w> ; <w n="12.9">elle</w> <w n="12.10">est</w> <w n="12.11">à</w> <w n="12.12">nous</w>.</l>
							</sp>
							<sp n="8">
								<speaker>LA BÊTE À BON DIEU, se débattant.</speaker>
								<l n="13"><w n="13.1">Non</w> ! <w n="13.2">laissez</w>-<w n="13.3">moi</w> ; <w n="13.4">parole</w> ! <w n="13.5">il</w> <w n="13.6">faut</w> <w n="13.7">que</w> <w n="13.8">je</w> <w n="13.9">m</w>’<w n="13.10">en</w> <w n="13.11">aille</w></l>
							</sp>
							<sp n="9">
								<speaker>LE PAPILLON.</speaker>
								<l part="I" n="14"><w n="14.1">Chut</w> ! <w n="14.2">entends</w>-<w n="14.3">tu</w> ? </l>
							</sp>
							<sp n="10">
								<speaker>la bête à bon dieu, effrayée.</speaker>
								<l part="M" n="14"><w n="14.4">Quoi</w> <w n="14.5">donc</w> ? </l>
							</sp>
							<sp n="11">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="14"><w n="14.6">Cette</w> <w n="14.7">petite</w> <w n="14.8">caille</w></l>
								<l n="15"><w n="15.1">Qui</w> <w n="15.2">chante</w> <w n="15.3">en</w> <w n="15.4">se</w> <w n="15.5">grisant</w> <w n="15.6">dans</w> <w n="15.7">la</w> <w n="15.8">vigne</w> <w n="15.9">à</w> <w n="15.10">côté</w>…</l>
								<l n="16"><w n="16.1">Hein</w> ! <w n="16.2">la</w> <w n="16.3">bonne</w> <w n="16.4">chanson</w> <w n="16.5">pour</w> <w n="16.6">ce</w> <w n="16.7">beau</w> <w n="16.8">soir</w> <w n="16.9">d</w>’<w n="16.10">été</w>,</l>
								<l n="17"><w n="17.1">Et</w> <w n="17.2">comme</w> <w n="17.3">c</w>’<w n="17.4">est</w> <w n="17.5">joli</w> <w n="17.6">de</w> <w n="17.7">la</w> <w n="17.8">place</w> <w n="17.9">où</w> <w n="17.10">nous</w> <w n="17.11">sommes</w> !</l>
							</sp>
							<sp n="12">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="I" n="18"><w n="18.1">Sans</w> <w n="18.2">doute</w>, <w n="18.3">mais</w>… </l>
							</sp>
							<sp n="13">
								<speaker>LE PAPILLON.</speaker>
								<l part="M" n="18"><w n="18.4">Tais</w>-<w n="18.5">toi</w>. </l>
							</sp>
							<sp n="14">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="M" n="18"><w n="18.6">Quoi</w> <w n="18.7">donc</w> ? </l>
							</sp>
							<sp n="15">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="18"><w n="18.8">Voilà</w> <w n="18.9">des</w> <w n="18.10">hommes</w>,</l>
								<stage>(Passent des hommes.)</stage>
							</sp>
							<sp n="16">
								<speaker>LA BÊTE À BON DIEU, après un silence.</speaker>
								<l part="I" n="19"><w n="19.1">L</w>’<w n="19.2">homme</w>, <w n="19.3">c</w>’<w n="19.4">est</w> <w n="19.5">très</w> <w n="19.6">méchant</w>, <w n="19.7">n</w>’<w n="19.8">est</w>-<w n="19.9">ce</w> <w n="19.10">pas</w> ? </l>
							</sp>
							<sp n="17">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="19"><w n="19.11">Très</w> <w n="19.12">méchant</w>.</l>
							</sp>
							<sp n="18">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l n="20"><w n="20.1">J</w>’<w n="20.2">ai</w> <w n="20.3">toujours</w> <w n="20.4">peur</w> <w n="20.5">qu</w>’<w n="20.6">un</w> <w n="20.7">d</w>’<w n="20.8">eux</w> <w n="20.9">m</w>’<w n="20.10">aplatisse</w> <w n="20.11">en</w> <w n="20.12">marchant</w> ;</l>
								<l n="21"><w n="21.1">Ils</w> <w n="21.2">ont</w> <w n="21.3">de</w> <w n="21.4">si</w> <w n="21.5">gros</w> <w n="21.6">pieds</w> <w n="21.7">et</w> <w n="21.8">moi</w> <w n="21.9">des</w> <w n="21.10">reins</w> <w n="21.11">si</w> <w n="21.12">frêles</w> !</l>
								<l n="22"><w n="22.1">Vous</w>, <w n="22.2">vous</w> <w n="22.3">n</w>’<w n="22.4">êtes</w> <w n="22.5">pas</w> <w n="22.6">grand</w>, <w n="22.7">mais</w> <w n="22.8">vous</w> <w n="22.9">avez</w> <w n="22.10">des</w> <w n="22.11">ailes</w> ;</l>
								<l part="I" n="23"><w n="23.1">C</w>’<w n="23.2">est</w> <w n="23.3">énorme</w> ! </l>
							</sp>
							<sp n="19">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="23"><w n="23.4">Pardieu</w> ! <w n="23.5">mon</w> <w n="23.6">cher</w>, <w n="23.7">si</w> <w n="23.8">ces</w> <w n="23.9">lourdauds</w></l>
								<l n="24"><w n="24.1">De</w> <w n="24.2">paysans</w> <w n="24.3">te</w> <w n="24.4">font</w> <w n="24.5">peur</w>, <w n="24.6">grimpe</w>-<w n="24.7">moi</w> <w n="24.8">sur</w> <w n="24.9">le</w> <w n="24.10">dos</w> ;</l>
								<l n="25"><w n="25.1">Je</w> <w n="25.2">suis</w> <w n="25.3">très</w> <w n="25.4">fort</w> <w n="25.5">des</w> <w n="25.6">reins</w>, <w n="25.7">moi</w> ; <w n="25.8">je</w> <w n="25.9">n</w>’<w n="25.10">ai</w> <w n="25.11">pas</w> <w n="25.12">des</w> <w n="25.13">ailes</w></l>
								<l n="26"><w n="26.1">En</w> <w n="26.2">pelure</w> <w n="26.3">d</w>’<w n="26.4">oignon</w> <w n="26.5">comme</w> <w n="26.6">les</w> <w n="26.7">demoiselles</w>,</l>
								<l n="27"><w n="27.1">Et</w> <w n="27.2">je</w> <w n="27.3">peux</w> <w n="27.4">te</w> <w n="27.5">porter</w> <w n="27.6">où</w> <w n="27.7">tu</w> <w n="27.8">voudras</w>, <w n="27.9">aussi</w></l>
								<l part="I" n="28"><w n="28.1">Longtemps</w> <w n="28.2">que</w> <w n="28.3">tu</w> <w n="28.4">voudras</w>. </l>
							</sp>
							<sp n="20">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="F" n="28"><w n="28.5">Oh</w> ! <w n="28.6">non</w>, <w n="28.7">monsieur</w>, <w n="28.8">merci</w>.</l>
								<l part="I" n="29"><w n="29.1">Je</w> <w n="29.2">n</w>’<w n="29.3">oserai</w> <w n="29.4">jamais</w>… </l>
							</sp>
							<sp n="21">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="29"><w n="29.5">C</w>’<w n="29.6">est</w> <w n="29.7">donc</w> <w n="29.8">bien</w> <w n="29.9">difficile</w></l>
								<l part="I" n="30"><w n="30.1">De</w> <w n="30.2">grimper</w> <w n="30.3">là</w> ? </l>
							</sp>
							<sp n="22">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="M" n="30"><w n="30.4">Non</w> ! <w n="30.5">mais</w>… </l>
							</sp>
							<sp n="23">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="30"><w n="30.6">Grimpe</w> <w n="30.7">donc</w>, <w n="30.8">imbécile</w> !</l>
							</sp>
							<sp n="24">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l n="31"><w n="31.1">Vous</w> <w n="31.2">me</w> <w n="31.3">ramènerez</w> <w n="31.4">chez</w> <w n="31.5">moi</w>, <w n="31.6">bien</w> <w n="31.7">entendu</w> ;</l>
								<l part="I" n="32"><w n="32.1">Car</w>, <w n="32.2">sans</w> <w n="32.3">cela</w>… </l>
							</sp>
							<sp n="25">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="32"><w n="32.4">Sitôt</w> <w n="32.5">parti</w>, <w n="32.6">sitôt</w> <w n="32.7">rendu</w>.</l>
							</sp>
							<sp n="26">
								<speaker>LA BÊTE À BON DIEU, grimpant sur son camarade.</speaker>
								<l n="33"><w n="33.1">C</w>’<w n="33.2">est</w> <w n="33.3">que</w> <w n="33.4">le</w> <w n="33.5">soir</w>, <w n="33.6">chez</w> <w n="33.7">nous</w>, <w n="33.8">nous</w> <w n="33.9">faisons</w> <w n="33.10">la</w> <w n="33.11">prière</w>.</l>
								<l part="I" n="34"><w n="34.1">Vous</w> <w n="34.2">comprenez</w> ? </l>
							</sp>
							<sp n="27">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="34"><w n="34.3">Sans</w> <w n="34.4">doute</w>… <w n="34.5">Un</w> <w n="34.6">peu</w> <w n="34.7">plus</w> <w n="34.8">en</w> <w n="34.9">arrière</w> !</l>
								<l n="35"><w n="35.1">Là</w>… <w n="35.2">maintenant</w> <w n="35.3">j</w>’<w n="35.4">ai</w> <w n="35.5">tout</w> <w n="35.6">lâché</w> ! <w n="35.7">silence</w> <w n="35.8">à</w> <w n="35.9">bord</w>.</l>
								<stage>(Prrt ! Ils s’envolent ; le dialogue continue en l’air.)</stage>
								<l n="36"><w n="36.1">Jamais</w> <w n="36.2">je</w> <w n="36.3">n</w>’<w n="36.4">aurais</w> <w n="36.5">cru</w> <w n="36.6">que</w> <w n="36.7">j</w>’<w n="36.8">étais</w> <w n="36.9">aussi</w> <w n="36.10">fort</w>.</l>
							</sp>
							<sp n="28">
								<speaker>LA BÊTE À BON DIEU, effrayée.</speaker>
								<l part="I" n="37"><w n="37.1">Ah</w> ! <w n="37.2">monsieur</w>… </l>
							</sp>
							<sp n="29">
								<speaker>LE PAPILLON.</speaker>
								<l part="M" n="37"><w n="37.3">Eh</w> <w n="37.4">bien</w> ! <w n="37.5">quoi</w> ? </l>
							</sp>
							<sp n="30">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="F" n="37"><w n="37.6">Je</w> <w n="37.7">n</w>’<w n="37.8">y</w> <w n="37.9">vois</w> <w n="37.10">plus</w>… <w n="37.11">la</w> <w n="37.12">tête</w></l>
								<l part="I" n="38"><w n="38.1">Me</w> <w n="38.2">tourne</w> ; <w n="38.3">je</w> <w n="38.4">voudrais</w> <w n="38.5">bien</w> <w n="38.6">descendre</w>… </l>
							</sp>
							<sp n="31">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="38"><w n="38.7">Es</w>-<w n="38.8">tu</w> <w n="38.9">bête</w> !</l>
								<l n="39"><w n="39.1">Si</w> <w n="39.2">la</w> <w n="39.3">tête</w> <w n="39.4">te</w> <w n="39.5">tourne</w>, <w n="39.6">il</w> <w n="39.7">faut</w> <w n="39.8">fermer</w> <w n="39.9">les</w> <w n="39.10">yeux</w>.</l>
								<l part="I" n="40"><w n="40.1">Les</w> <w n="40.2">as</w>-<w n="40.3">tu</w> <w n="40.4">fermés</w> ? </l>
							</sp>
							<sp n="32">
								<speaker>LA BÊTE À BON DIEU, fermant les yeux.</speaker>
								<l part="M" n="40"><w n="40.5">Oui</w>… </l>
							</sp>
							<sp n="33">
								<speaker>LE PAPILLON.</speaker>
								<l part="M" n="40"><w n="40.6">Ça</w> <w n="40.7">va</w> <w n="40.8">mieux</w> ? </l>
							</sp>
							<sp n="34">
								<speaker>LA BÊTE À BON DIEU, avec effort.</speaker>
								<l part="F" n="40"><w n="40.9">Un</w> <w n="40.10">peu</w> <w n="40.11">mieux</w>.</l>
							</sp>
							<sp n="35">
								<speaker>LA BÊTE À BON DIEU, riant sous cape.</speaker>
								<l n="41"><w n="41.1">Décidément</w>, <w n="41.2">on</w> <w n="41.3">est</w> <w n="41.4">mauvais</w> <w n="41.5">aéronaute</w></l>
								<l part="I" n="42"><w n="42.1">Dans</w> <w n="42.2">ta</w> <w n="42.3">famille</w>… </l>
							</sp>
							<sp n="36">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="M" n="42"><w n="42.4">Oh</w> ! <w n="42.5">oui</w>… </l>
							</sp>
							<sp n="37">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="42"><w n="42.6">Ce</w> <w n="42.7">n</w>’<w n="42.8">est</w> <w n="42.9">pas</w> <w n="42.10">votre</w> <w n="42.11">faute</w></l>
								<l n="43"><w n="43.1">Si</w> <w n="43.2">le</w> <w n="43.3">guide</w>-<w n="43.4">ballon</w> <w n="43.5">n</w>’<w n="43.6">est</w> <w n="43.7">pas</w> <w n="43.8">encor</w> <w n="43.9">trouvé</w>.</l>
							</sp>
							<sp n="38">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="I" n="44"><w n="44.1">Oh</w> ! <w n="44.2">non</w>… </l>
							</sp>
							<sp n="39">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="44"><w n="44.3">Çà</w>, <w n="44.4">monseigneur</w>, <w n="44.5">vous</w> <w n="44.6">êtes</w> <w n="44.7">arrivé</w></l>
								<stage>(Il se pose sur un Muguet.)</stage>
							</sp>
							<sp n="40">
								<speaker>LA BÊTE À BON DIEU, ouvrant les yeux.</speaker>
								<l n="45"><w n="45.1">Pardon</w>, <w n="45.2">mais</w> <w n="45.3">ce</w> <w n="45.4">n</w>’<w n="45.5">est</w> <w n="45.6">pas</w> <w n="45.7">ici</w> <w n="45.8">que</w> <w n="45.9">je</w> <w n="45.10">demeure</w>.</l>
							</sp>
							<sp n="41">
								<speaker>LE PAPILLON.</speaker>
								<l n="46"><w n="46.1">Je</w> <w n="46.2">sais</w> ; <w n="46.3">mais</w> <w n="46.4">comme</w> <w n="46.5">il</w> <w n="46.6">est</w> <w n="46.7">encor</w> <w n="46.8">de</w> <w n="46.9">très</w> <w n="46.10">bonne</w> <w n="46.11">heure</w>,</l>
								<l n="47"><w n="47.1">Je</w> <w n="47.2">t</w>’<w n="47.3">ai</w> <w n="47.4">mené</w> <w n="47.5">chez</w> <w n="47.6">un</w> <w n="47.7">Muguet</w> <w n="47.8">de</w> <w n="47.9">mes</w> <w n="47.10">amis</w>.</l>
								<l n="48"><w n="48.1">On</w> <w n="48.2">va</w> <w n="48.3">se</w> <w n="48.4">rafraîchir</w> <w n="48.5">le</w> <w n="48.6">bec</w> ; — <w n="48.7">c</w>’<w n="48.8">est</w> <w n="48.9">bien</w> <w n="48.10">permis</w>…</l>
							</sp>
							<sp n="42">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="I" n="49"><w n="49.1">Oh</w> ! <w n="49.2">je</w> <w n="49.3">n</w>’<w n="49.4">ai</w> <w n="49.5">pas</w> <w n="49.6">le</w> <w n="49.7">temps</w>… </l>
							</sp>
							<sp n="43">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="49"><w n="49.8">Bah</w> ! <w n="49.9">rien</w> <w n="49.10">qu</w>’<w n="49.11">une</w> <w n="49.12">seconde</w>…</l>
							</sp>
							<sp n="44">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l n="50"><w n="50.1">Et</w> <w n="50.2">puis</w>, <w n="50.3">je</w> <w n="50.4">ne</w> <w n="50.5">suis</w> <w n="50.6">pas</w> <w n="50.7">reçu</w>, <w n="50.8">moi</w>, <w n="50.9">dans</w> <w n="50.10">le</w> <w n="50.11">monde</w>…</l>
							</sp>
							<sp n="45">
								<speaker>LE PAPILLON.</speaker>
								<l n="51"><w n="51.1">Viens</w> <w n="51.2">donc</w> ! <w n="51.3">je</w> <w n="51.4">te</w> <w n="51.5">ferai</w> <w n="51.6">passer</w> <w n="51.7">pour</w> <w n="51.8">mon</w> <w n="51.9">bâtard</w> ;</l>
								<l part="I" n="52"><w n="52.1">Tu</w> <w n="52.2">seras</w> <w n="52.3">bien</w> <w n="52.4">reçu</w>, <w n="52.5">va</w> !… </l>
							</sp>
							<sp n="46">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="F" n="52"><w n="52.6">Puis</w>, <w n="52.7">c</w>’<w n="52.8">est</w> <w n="52.9">qu</w>’<w n="52.10">il</w> <w n="52.11">est</w> <w n="52.12">tard</w></l>
							</sp>
							<sp n="47">
								<speaker>LE PAPILLON.</speaker>
								<l n="53"><w n="53.1">Eh</w> <w n="53.2">non</w> ! <w n="53.3">il</w> <w n="53.4">n</w>’<w n="53.5">est</w> <w n="53.6">pas</w> <w n="53.7">tard</w> ; <w n="53.8">écoute</w> <w n="53.9">la</w> <w n="53.10">Cigale</w>…</l>
							</sp>
							<sp n="48">
								<speaker>LA BÊTE À BON DIEU, à voix basse.</speaker>
								<l part="I" n="54"><w n="54.1">Puis</w>… <w n="54.2">je</w>… <w n="54.3">n</w>’<w n="54.4">ai</w> <w n="54.5">pas</w> <w n="54.6">d</w>’<w n="54.7">argent</w>… </l>
							</sp>
							<sp n="49">
								<speaker>LE PAPILLON, l’entraînant.</speaker>
								<l part="F" n="54"><w n="54.8">Viens</w> ! <w n="54.9">le</w> <w n="54.10">Muguet</w> <w n="54.11">régale</w>.</l>
								<stage>(Ils entrent chez le Muguet. — La toile tombe.)</stage>
							</sp>
						</div>
						<div type="scene" n="2">
							<stage>
								Au second acte, quand le rideau se lève, il fait presque nuit… <lb></lb>
								On voit les deux camarades sortir de chez le Muguet… La Bête <lb></lb>
								à bon Dieu est légèrement ivre.
							</stage>
							<sp n="50">
								<speaker>LE PAPILLON, tendant le dos</speaker>
								<l part="I" n="55"><w n="55.1">Et</w> <w n="55.2">maintenant</w>, <w n="55.3">en</w> <w n="55.4">route</w> ! </l>
								<stage>(Prrt ! Ils s’envolent… Le dialogue continue en l’air.)</stage>
							</sp>
							<sp n="51">
								<speaker>LA BÊTE À BON DIEU, grimpant bravement.</speaker>
								<l part="M" n="55"><w n="55.5">En</w> <w n="55.6">route</w> ! </l>
							</sp>
							<sp n="52">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="55"><w n="55.7">Eh</w> <w n="55.8">bien</w> ! <w n="55.9">comment</w></l>
								<l part="I" n="56"><w n="56.1">Trouves</w>-<w n="56.2">tu</w> <w n="56.3">mon</w> <w n="56.4">Muguet</w> ? </l>
							</sp>
							<sp n="53">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="F" n="56"><w n="56.5">Mon</w> <w n="56.6">cher</w>, <w n="56.7">il</w> <w n="56.8">est</w> <w n="56.9">charmant</w> ;</l>
								<l n="57"><w n="57.1">Il</w> <w n="57.2">vous</w> <w n="57.3">livre</w> <w n="57.4">sa</w> <w n="57.5">cave</w> <w n="57.6">et</w> <w n="57.7">tout</w>, <w n="57.8">sans</w> <w n="57.9">vous</w> <w n="57.10">connaître</w>…</l>
							</sp>
							<sp n="54">
								<speaker>LE PAPILLON, regardant le ciel.</speaker>
								<l n="58"><w n="58.1">Oh</w> ! <w n="58.2">oh</w> ! <w n="58.3">Phœbé</w> <w n="58.4">qui</w> <w n="58.5">met</w> <w n="58.6">le</w> <w n="58.7">nez</w> <w n="58.8">à</w> <w n="58.9">la</w> <w n="58.10">fenêtre</w> ;</l>
								<l part="I" n="59"><w n="59.1">Il</w> <w n="59.2">faut</w> <w n="59.3">nous</w> <w n="59.4">dépêcher</w>… </l>
							</sp>
							<sp n="55">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="F" n="59"><w n="59.5">Nous</w> <w n="59.6">dépêcher</w>, <w n="59.7">pourquoi</w> ?</l>
							</sp>
							<sp n="56">
								<speaker>LE PAPILLON.</speaker>
								<l n="60"><w n="60.1">Tu</w> <w n="60.2">n</w>’<w n="60.3">es</w> <w n="60.4">donc</w> <w n="60.5">plus</w> <w n="60.6">pressé</w> <w n="60.7">de</w> <w n="60.8">retourner</w> <w n="60.9">chez</w> <w n="60.10">toi</w> ?</l>
							</sp>
							<sp n="57">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l n="61"><w n="61.1">Oh</w> ! <w n="61.2">pourvu</w> <w n="61.3">que</w> <w n="61.4">j</w>’<w n="61.5">arrive</w> <w n="61.6">à</w> <w n="61.7">temps</w> <w n="61.8">pour</w> <w n="61.9">la</w> <w n="61.10">prière</w>…</l>
								<l n="62"><w n="62.1">D</w>’<w n="62.2">ailleurs</w>, <w n="62.3">ce</w> <w n="62.4">n</w>’<w n="62.5">est</w> <w n="62.6">pas</w> <w n="62.7">loin</w>, <w n="62.8">chez</w> <w n="62.9">nous</w>… <w n="62.10">c</w>’<w n="62.11">est</w> <w n="62.12">là</w> <w n="62.13">derrière</w></l>
							</sp>
							<sp n="58">
								<speaker>LE PAPILLON.</speaker>
								<l n="63"><w n="63.1">Si</w> <w n="63.2">tu</w> <w n="63.3">n</w>’<w n="63.4">es</w> <w n="63.5">pas</w> <w n="63.6">pressé</w>, <w n="63.7">je</w> <w n="63.8">ne</w> <w n="63.9">le</w> <w n="63.10">suis</w> <w n="63.11">pas</w>, <w n="63.12">moi</w>.</l>
							</sp>
							<sp n="59">
								<speaker>LA BÊTE À BON DIEU, avec effusion.</speaker>
								<l n="64"><w n="64.1">Quel</w> <w n="64.2">bon</w> <w n="64.3">enfant</w> <w n="64.4">tu</w> <w n="64.5">fais</w> !… <w n="64.6">Vrai</w> ! <w n="64.7">je</w> <w n="64.8">ne</w> <w n="64.9">sais</w> <w n="64.10">pourquoi</w></l>
								<l n="65"><w n="65.1">Tout</w> <w n="65.2">le</w> <w n="65.3">monde</w> <w n="65.4">n</w>’<w n="65.5">est</w> <w n="65.6">pas</w> <w n="65.7">ton</w> <w n="65.8">ami</w> <w n="65.9">sur</w> <w n="65.10">la</w> <w n="65.11">terre</w>.</l>
								<l n="66"><w n="66.1">On</w> <w n="66.2">dit</w> <w n="66.3">de</w> <w n="66.4">toi</w> : « <w n="66.5">C</w>’<w n="66.6">est</w> <w n="66.7">un</w> <w n="66.8">bohême</w> ! <w n="66.9">un</w> <w n="66.10">réfractaire</w> !</l>
								<l part="I" n="67"><w n="67.1">Un</w> <w n="67.2">poète</w> ! <w n="67.3">un</w> <w n="67.4">sauteur</w> !… » </l>
							</sp>
							<sp n="60">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="67"><w n="67.5">Tiens</w> ! <w n="67.6">tiens</w> ! <w n="67.7">et</w> <w n="67.8">qui</w> <w n="67.9">dit</w> <w n="67.10">ça</w> ?</l>
							</sp>
							<sp n="61">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="I" n="68"><w n="68.1">Mon</w> <w n="68.2">Dieu</w> ! <w n="68.3">le</w> <w n="68.4">Scarabée</w>… </l>
							</sp>
							<sp n="62">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="68"><w n="68.5">Ah</w> ! <w n="68.6">oui</w>, <w n="68.7">ce</w> <w n="68.8">gros</w> <w n="68.9">poussah</w> !</l>
								<l n="69"><w n="69.1">Il</w> <w n="69.2">m</w>’<w n="69.3">appelle</w> <w n="69.4">sauteur</w>, <w n="69.5">parce</w> <w n="69.6">qu</w>’<w n="69.7">il</w> <w n="69.8">a</w> <w n="69.9">du</w> <w n="69.10">ventre</w>.</l>
							</sp>
							<sp n="63">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="I" n="70"><w n="70.1">C</w>’<w n="70.2">est</w> <w n="70.3">qu</w>’<w n="70.4">il</w> <w n="70.5">n</w>’<w n="70.6">est</w> <w n="70.7">pas</w> <w n="70.8">le</w> <w n="70.9">seul</w> <w n="70.10">qui</w> <w n="70.11">te</w> <w n="70.12">déteste</w>… </l>
							</sp>
							<sp n="64">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="70"><w n="70.13">Ah</w> ! <w n="70.14">diantre</w> !</l>
							</sp>
							<sp n="65">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l n="71"><w n="71.1">Ainsi</w> <w n="71.2">les</w> <w n="71.3">Escargots</w> <w n="71.4">ne</w> <w n="71.5">sont</w> <w n="71.6">pas</w> <w n="71.7">tes</w> <w n="71.8">amis</w>.</l>
								<l n="72"><w n="72.1">Va</w> ! <w n="72.2">ni</w> <w n="72.3">les</w> <w n="72.4">Scorpions</w>, <w n="72.5">pas</w> <w n="72.6">même</w> <w n="72.7">les</w> <w n="72.8">Fourmis</w>.</l>
							</sp>
							<sp n="66">
								<speaker>LE PAPILLON.</speaker>
								<l part="I" n="73"><w n="73.1">Vraiment</w> ! </l>
							</sp>
							<sp n="67">
								<speaker>la BÊTE À BON DIEU, confidentiellement.</speaker>
								<l part="F" n="73"><w n="73.2">Ne</w> <w n="73.3">fais</w> <w n="73.4">jamais</w> <w n="73.5">la</w> <w n="73.6">cour</w> <w n="73.7">à</w> <w n="73.8">l</w>’<w n="73.9">Araignée</w> ;</l>
								<l part="I" n="74"><w n="74.1">Elle</w> <w n="74.2">te</w> <w n="74.3">trouve</w> <w n="74.4">affreux</w>. </l>
							</sp>
							<sp n="68">
								<speaker>LE PAPILLON.</speaker>
								<l part="F" n="74"><w n="74.5">On</w> <w n="74.6">l</w>’<w n="74.7">a</w> <w n="74.8">mal</w> <w n="74.9">renseignée</w>,</l>
							</sp>
							<sp n="69">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l n="75"><w n="75.1">Hé</w> ! <w n="75.2">les</w> <w n="75.3">Chenilles</w> <w n="75.4">sont</w> <w n="75.5">un</w> <w n="75.6">peu</w> <w n="75.7">de</w> <w n="75.8">son</w> <w n="75.9">avis</w>…</l>
							</sp>
							<sp n="70">
								<speaker>LE PAPILLON.</speaker>
								<l n="76"><w n="76.1">Je</w> <w n="76.2">crois</w> <w n="76.3">bien</w> !… <w n="76.4">mais</w>, <w n="76.5">dis</w>-<w n="76.6">moi</w>, <w n="76.7">dans</w> <w n="76.8">le</w> <w n="76.9">monde</w> <w n="76.10">où</w> <w n="76.11">tu</w> <w n="76.12">vis</w>,</l>
								<l n="77"><w n="77.1">Car</w> <w n="77.2">enfin</w> <w n="77.3">tu</w> <w n="77.4">n</w>’<w n="77.5">es</w> <w n="77.6">pas</w> <w n="77.7">du</w> <w n="77.8">monde</w> <w n="77.9">des</w> <w n="77.10">Chenilles</w>.</l>
								<l part="I" n="78"><w n="78.1">Suis</w>-<w n="78.2">je</w> <w n="78.3">aussi</w> <w n="78.4">mal</w> <w n="78.5">vu</w> ?… </l>
							</sp>
							<sp n="71">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="F" n="78"><w n="78.6">Dam</w> ! <w n="78.7">c</w>’<w n="78.8">est</w> <w n="78.9">selon</w> <w n="78.10">les</w> <w n="78.11">familles</w> ;</l>
								<l n="79"><w n="79.1">La</w> <w n="79.2">jeunesse</w> <w n="79.3">est</w> <w n="79.4">pour</w> <w n="79.5">toi</w>. <w n="79.6">Les</w> <w n="79.7">vieux</w>, <w n="79.8">en</w> <w n="79.9">général</w>,</l>
								<l n="80"><w n="80.1">Trouvent</w> <w n="80.2">que</w> <w n="80.3">lu</w> <w n="80.4">n</w>’<w n="80.5">as</w> <w n="80.6">pas</w> <w n="80.7">assez</w> <w n="80.8">de</w> <w n="80.9">sens</w> <w n="80.10">moral</w>.</l>
							</sp>
							<sp n="72">
								<speaker>LE PAPILLON, tristement.</speaker>
								<l n="81"><w n="81.1">le</w> <w n="81.2">vois</w> <w n="81.3">que</w> <w n="81.4">je</w> <w n="81.5">n</w>’<w n="81.6">ai</w> <w n="81.7">pas</w> <w n="81.8">beaucoup</w> <w n="81.9">de</w> <w n="81.10">sympathies</w>,</l>
								<l part="I" n="82"><w n="82.1">En</w> <w n="82.2">somme</w>… </l>
							</sp>
							<sp n="73">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l part="F" n="82"><w n="82.3">Ma</w> <w n="82.4">foi</w> ! <w n="82.5">non</w>, <w n="82.6">mon</w> <w n="82.7">pauvre</w>. <w n="82.8">Les</w> <w n="82.9">Orties</w></l>
								<l n="83"><w n="83.1">T</w>’<w n="83.2">en</w> <w n="83.3">veulent</w>. <w n="83.4">Le</w> <w n="83.5">Crapaud</w> <w n="83.6">te</w> <w n="83.7">hait</w> ; <w n="83.8">jusqu</w>’<w n="83.9">au</w> <w n="83.10">Grillon</w>,</l>
								<l n="84"><w n="84.1">Quand</w> <w n="84.2">il</w> <w n="84.3">parle</w> <w n="84.4">de</w> <w n="84.5">toi</w>, <w n="84.6">qui</w> <w n="84.7">dit</w> : « <w n="84.8">Ce</w>… <w n="84.9">p</w>… <w n="84.10">p</w>… <w n="84.11">Papillon</w> ! »</l>
							</sp>
							<sp n="74">
								<speaker>LE PAPILLON.</speaker>
								<l n="85"><w n="85.1">Est</w>-<w n="85.2">ce</w> <w n="85.3">que</w> <w n="85.4">tu</w> <w n="85.5">me</w> <w n="85.6">hais</w>, <w n="85.7">toi</w>, <w n="85.8">comme</w> <w n="85.9">tous</w> <w n="85.10">ces</w> <w n="85.11">drôles</w> ?</l>
							</sp>
							<sp n="75">
								<speaker>LA BÊTE À BON DIEU.</speaker>
								<l n="86"><w n="86.1">Moi</w> !… <w n="86.2">je</w> <w n="86.3">t</w>’<w n="86.4">adore</w> ; <w n="86.5">on</w> <w n="86.6">est</w> <w n="86.7">si</w> <w n="86.8">bien</w> <w n="86.9">sur</w> <w n="86.10">tes</w> <w n="86.11">épaules</w> !</l>
								<l n="87"><w n="87.1">Et</w> <w n="87.2">puis</w> <w n="87.3">tu</w> <w n="87.4">me</w> <w n="87.5">conduis</w> <w n="87.6">toujours</w> <w n="87.7">chez</w> <w n="87.8">les</w> <w n="87.9">Muguets</w>,</l>
								<l n="88"><w n="88.1">C</w>’<w n="88.2">est</w> <w n="88.3">amusant</w> !… <w n="88.4">Dis</w> <w n="88.5">donc</w>, <w n="88.6">si</w> <w n="88.7">je</w> <w n="88.8">te</w> <w n="88.9">fatiguais</w>,</l>
								<l n="89"><w n="89.1">Nous</w> <w n="89.2">pourrions</w> <w n="89.3">faire</w> <w n="89.4">encore</w> <w n="89.5">une</w> <w n="89.6">petite</w> <w n="89.7">pause</w></l>
								<l n="90"><w n="90.1">Quelque</w> <w n="90.2">part</w>… <w n="90.3">tu</w> <w n="90.4">n</w>’<w n="90.5">es</w> <w n="90.6">pas</w> <w n="90.7">fatigué</w>, <w n="90.8">je</w> <w n="90.9">suppose</w> ?</l>
							</sp>
							<sp n="76">
								<speaker>LE PAPILLON.</speaker>
								<l n="91"><w n="91.1">Je</w> <w n="91.2">te</w> <w n="91.3">trouve</w> <w n="91.4">un</w> <w n="91.5">peu</w> <w n="91.6">lourd</w>, <w n="91.7">ce</w> <w n="91.8">n</w>’<w n="91.9">est</w> <w n="91.10">pas</w> <w n="91.11">l</w>’<w n="91.12">embarras</w>.</l>
							</sp>
							<sp n="77">
								<speaker>LA BÊTE À BON DIEU, montrant des Muguets.</speaker>
								<l n="92"><w n="92.1">Alors</w>, <w n="92.2">entrons</w> <w n="92.3">ici</w> ; <w n="92.4">tu</w> <w n="92.5">te</w> <w n="92.6">reposeras</w>.</l>
							</sp>
							<sp n="78">
								<speaker>LE PAPILLON.</speaker>
								<l n="93"><w n="93.1">Ah</w> ! <w n="93.2">merci</w> ! <w n="93.3">des</w> <w n="93.4">Muguets</w> ! <w n="93.5">toujours</w> <w n="93.6">la</w> <w n="93.7">même</w> <w n="93.8">chose</w>.</l>
								<stage>(Bas, d’un ton libertin.)</stage>
								<l part="I" n="94"><w n="94.1">J</w>’<w n="94.2">aime</w> <w n="94.3">bien</w> <w n="94.4">mieux</w> <w n="94.5">entrer</w> <w n="94.6">à</w> <w n="94.7">côté</w>… </l>
							</sp>
							<sp n="79">
								<speaker>LA BÊTE À BON DIEU, toute rouge.</speaker>
								<l part="F" n="94"><w n="94.8">Chez</w> <w n="94.9">la</w> <w n="94.10">Rose</w> ?…</l>
								<l part="I" n="95"><w n="95.1">Oh</w> ! <w n="95.2">non</w>, <w n="95.3">jamais</w>… </l>
							</sp>
							<sp n="80">
								<speaker>LE PAPILLON, l’entraînant.</speaker>
								<l part="F" n="95"><w n="95.4">Viens</w> <w n="95.5">donc</w> ! <w n="95.6">on</w> <w n="95.7">ne</w> <w n="95.8">nous</w> <w n="95.9">verra</w> <w n="95.10">pas</w>.</l>
							</sp>
							<stage>(Ils entrent discrètement chez la Rose. — La toile tombe.)</stage>
							<p>
								Au troisième acte, il est nuit tout à fait… Les deux camarades sortent ensemble de 
								chez la Rose… Le Papillon veut ramener la Bête à bon Dieu chez ses parents, mais 
								celle-ci s’y refuse ; elle est complètement ivre, fait des cabrioles sur l’herbe et 
								pousse des cris séditieux… Le Papillon est obligé de l’emporter chez elle. On se 
								sépare sur la porte en se promettant de se revoir bientôt… Et alors le Papillon 
								s’en va tout seul, dans la nuit. Il est un peu ivre, lui aussi ; mais son ivresse 
								est triste : il se rappelle les confidences de la Bête à bon Dieu, et se demande 
								amèrement pourquoi tant de monde le déteste, lui qui jamais n’a fait de mal à 
								personne… Ciel sans lune ! Le vent souffle, la campagne est toute noire… Le Papillon 
								a peur, il a froid ; mais il se console en songeant que son camarade est en sûreté, 
								au fond d’une couchette bien chaude… Cependant on entrevoit dans l’ombre de grands 
								oiseaux de nuit qui traversent la scène d’un vol silencieux. L’éclair brille ! Des 
								bètes méchantes, embusquées sous des pierres, ricanent en se montrant le Papillon : 
								« Nous le tenons ! » disent-elles ; et tandis que l’infortuné va de droite et de 
								gauche, plein d’efTroi, un Chardon au passage le larde d’un grand coup d’épée, un 
								Scorpion l’éventre avec ses pinces, une grosse Araignée velue lui arrache un pan 
								de son manteau de satin bleu, et, pour finir, une Chauve-Souris lui casse les reins 
								d’un coup d’aile. Le Papillon tombe blessé à mort… Tandis qu’il râle sur l’herbe, 
								les Orties se réjouissent et les Crapauds disent : « C’est bien fait ! »
								À l’aube, les Fourmis, qui vont au travail avec leurs saquettes et leurs gourdes, 
								trouvent le cadavre au bord du chemin. Elles le regardent à peine et s’éloignent 
								sans vouloir l’enterrer. Les Fourmis ne travaillent pas pour rien… Heureusement, 
								une confrérie de Nécrophores vient à passer par là. Ce sont, comme vous savez, de 
								petites bêtes noires qui ont fait vœu d’ensevelir les morts. Pieusement, elles 
								s’attellent au Papillon défunt et le traînent vers le cimetière… Une foule curieuse 
								se presse sur leur passage et chacun fait des réflexions à haute voix… Les petits 
								Grillons bruns, assis au soleil devant leurs portes, disent gravement : « Il aimait 
								trop les fleurs ! » — « Il courait trop la nuit ! » ajoutent les Escargots, et les 
								Scarabées à gros ventre se dandinent dans leurs habits d’or en grommelant : « Trop 
								bohême ! trop bohême ! » Parmi toute cette foule, pas un mot de regret pour le pauvre 
								mort ; seulement, dans les plaines d’alentour, les grands Lis ont fermé et les Cigales 
								ne chantent pas. 
								La dernière scène se passe dans le cimetière des Papillons. Après que les Nécrophores 
								ont fait leur œuvre, un Hanneton solennel, qui a suivi le convoi, s’approche de la fosse, 
								et, se mettant sur le dos, commence l’éloge du défunt. Malheureusement, la mémoire lui 
								manque ; il reste là les pattes en l’air, gesticulant pendant une heure, et s’entortillant 
								dans ses périodes… Quand l’orateur a fini, chacun se retire, et alors, dans le cimetière 
								désert, on voit la Bête à bon Dieu des premières scènes sortir de derrière une tombe. 
								Tout en larmes, elle s’agenouille sur la terre fraîche de la fosse et dit une prière 
								touchante pour son pauvre petit camarade qui est là !…
								fin des aventures d’un papillon.
							</p>
						</div>
					</div>
				</div></body></text></TEI>