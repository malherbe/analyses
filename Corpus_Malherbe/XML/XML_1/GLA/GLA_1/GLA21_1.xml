<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA21">
				<head type="number">XXI</head>
				<head type="main">À GARIBALDI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">n</w>’<w n="1.3">avons</w> <w n="1.4">demandé</w> <w n="1.5">le</w> <w n="1.6">secours</w> <w n="1.7">d</w>’<w n="1.8">aucun</w> <w n="1.9">roi</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Mais</w> <w n="2.2">on</w> <w n="2.3">te</w> <w n="2.4">peut</w> <w n="2.5">tout</w> <w n="2.6">dire</w> <w n="2.7">et</w> <w n="2.8">confier</w>, <w n="2.9">à</w> <w n="2.10">toi</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Soldat</w> <w n="3.2">républicain</w>, <w n="3.3">cœur</w> <w n="3.4">loyal</w>, <w n="3.5">homme</w> <w n="3.6">juste</w></l>
					<l n="4" num="1.4"><w n="4.1">Dont</w> <w n="4.2">rien</w> <w n="4.3">n</w>’<w n="4.4">a</w> <w n="4.5">pu</w> <w n="4.6">lasser</w> <w n="4.7">le</w> <w n="4.8">dévoûment</w> <w n="4.9">robuste</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Nous</w> <w n="5.2">sommes</w> <w n="5.3">en</w> <w n="5.4">danger</w>, <w n="5.5">Garibaldi</w> ! <w n="5.6">Depuis</w></l>
					<l n="6" num="1.6"><w n="6.1">Deux</w> <w n="6.2">sombres</w> <w n="6.3">mois</w> <w n="6.4">plus</w> <w n="6.5">noirs</w> <w n="6.6">et</w> <w n="6.7">plus</w> <w n="6.8">lourds</w> <w n="6.9">que</w> <w n="6.10">les</w> <w n="6.11">nuits</w>,</l>
					<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">invasion</w> <w n="7.3">est</w> <w n="7.4">là</w>, <w n="7.5">piétinant</w> <w n="7.6">sur</w> <w n="7.7">la</w> <w n="7.8">France</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Cris</w> <w n="8.2">de</w> <w n="8.3">rage</w>, <w n="8.4">sanglots</w> <w n="8.5">qu</w>’<w n="8.6">arrache</w> <w n="8.7">la</w> <w n="8.8">souffrance</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Voilà</w> <w n="9.2">ce</w> <w n="9.3">qu</w>’<w n="9.4">on</w> <w n="9.5">entend</w> <w n="9.6">sur</w> <w n="9.7">le</w> <w n="9.8">sol</w> <w n="9.9">désolé</w></l>
					<l n="10" num="1.10"><w n="10.1">De</w> <w n="10.2">ce</w> <w n="10.3">pays</w>, <w n="10.4">hier</w> <w n="10.5">encor</w>, <w n="10.6">joyeux</w>, <w n="10.7">ailé</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Qui</w>, <w n="11.2">par</w> <w n="11.3">toutes</w> <w n="11.4">ses</w> <w n="11.5">voix</w>, <w n="11.6">malgré</w> <w n="11.7">son</w> <w n="11.8">maître</w> <w n="11.9">immonde</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Chantant</w> <w n="12.2">la</w> <w n="12.3">liberté</w>, <w n="12.4">régénérait</w> <w n="12.5">le</w> <w n="12.6">monde</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Mêlant</w> <w n="13.2">un</w> <w n="13.3">nouveau</w> <w n="13.4">crime</w> <w n="13.5">à</w> <w n="13.6">leurs</w> <w n="13.7">crimes</w> <w n="13.8">anciens</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Les</w> <w n="14.2">Corses</w> <w n="14.3">ont</w> <w n="14.4">livré</w> <w n="14.5">la</w> <w n="14.6">France</w> <w n="14.7">aux</w> <w n="14.8">Prussiens</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">La</w> <w n="15.2">lutte</w> <w n="15.3">est</w> <w n="15.4">douloureuse</w>, <w n="15.5">et</w> <w n="15.6">l</w>’<w n="15.7">ogre</w> <w n="15.8">en</w> <w n="15.9">son</w> <w n="15.10">repaire</w></l>
					<l n="16" num="1.16"><w n="16.1">Rit</w> <w n="16.2">de</w> <w n="16.3">joie</w>, <w n="16.4">en</w> <w n="16.5">voyant</w> <w n="16.6">que</w> <w n="16.7">plus</w> <w n="16.8">d</w>’<w n="16.9">un</w> <w n="16.10">désespère</w>.</l>
					<l n="17" num="1.17"><w n="17.1">À</w> <w n="17.2">notre</w> <w n="17.3">aide</w>, <w n="17.4">ô</w> <w n="17.5">héros</w> ! <w n="17.6">à</w> <w n="17.7">notre</w> <w n="17.8">aide</w> ! <w n="17.9">Viens</w>-<w n="17.10">t</w>-<w n="17.11">en</w></l>
					<l n="18" num="1.18"><w n="18.1">Parmi</w> <w n="18.2">nous</w>, <w n="18.3">et</w> <w n="18.4">surgis</w>, <w n="18.5">victorieux</w> <w n="18.6">Titan</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Dressant</w> <w n="19.2">ton</w> <w n="19.3">front</w> <w n="19.4">superbe</w> <w n="19.5">au</w> <w n="19.6">milieu</w> <w n="19.7">de</w> <w n="19.8">l</w>’<w n="19.9">orage</w>.</l>
					<l n="20" num="1.20"><w n="20.1">Toi</w> <w n="20.2">seul</w> ! <w n="20.3">Cela</w> <w n="20.4">suffit</w> <w n="20.5">pour</w> <w n="20.6">rendre</w> <w n="20.7">le</w> <w n="20.8">courage</w></l>
					<l n="21" num="1.21"><w n="21.1">À</w> <w n="21.2">qui</w> <w n="21.3">pouvait</w> <w n="21.4">douter</w>, <w n="21.5">et</w> <w n="21.6">raffermir</w> <w n="21.7">encor</w></l>
					<l n="22" num="1.22"><w n="22.1">Les</w> <w n="22.2">vaillants</w> <w n="22.3">dont</w> <w n="22.4">l</w>’<w n="22.5">audace</w> <w n="22.6">est</w> <w n="22.7">la</w> <w n="22.8">cuirasse</w> <w n="22.9">d</w>’<w n="22.10">or</w>.</l>
					<l n="23" num="1.23">« <w n="23.1">Ce</w> <w n="23.2">qui</w> <w n="23.3">reste</w> <w n="23.4">de</w> <w n="23.5">moi</w>, <w n="23.6">disais</w>-<w n="23.7">tu</w>, <w n="23.8">je</w> <w n="23.9">le</w> <w n="23.10">donne</w>. »</l>
					<l n="24" num="1.24"><w n="24.1">Merci</w>, <w n="24.2">nous</w> <w n="24.3">acceptons</w> <w n="24.4">cette</w> <w n="24.5">splendide</w> <w n="24.6">aumône</w>.</l>
					<l n="25" num="1.25"><w n="25.1">Ce</w> <w n="25.2">qui</w> <w n="25.3">reste</w> <w n="25.4">de</w> <w n="25.5">toi</w>, <w n="25.6">preux</w> <w n="25.7">sauveur</w> ! <w n="25.8">C</w>’<w n="25.9">est</w> <w n="25.10">l</w>’<w n="25.11">amour</w>,</l>
					<l n="26" num="1.26"><w n="26.1">C</w>’<w n="26.2">est</w> <w n="26.3">l</w>’<w n="26.4">abnégation</w>, <w n="26.5">c</w>’<w n="26.6">est</w> <w n="26.7">la</w> <w n="26.8">foi</w> <w n="26.9">sans</w> <w n="26.10">retour</w></l>
					<l n="27" num="1.27"><w n="27.1">Dans</w> <w n="27.2">la</w> <w n="27.3">liberté</w> <w n="27.4">sainte</w> <w n="27.5">et</w> <w n="27.6">l</w>’<w n="27.7">auguste</w> <w n="27.8">justice</w>.</l>
					<l n="28" num="1.28"><w n="28.1">Tu</w> <w n="28.2">fais</w> <w n="28.3">évanouir</w> <w n="28.4">toute</w> <w n="28.5">gloire</w> <w n="28.6">factice</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Ton</w> <w n="29.2">nom</w> <w n="29.3">veut</w> <w n="29.4">dire</w> <w n="29.5">Exemple</w> <w n="29.6">et</w> <w n="29.7">veut</w> <w n="29.8">dire</w> <w n="29.9">Devoir</w>.</l>
					<l n="30" num="1.30"><w n="30.1">Nos</w> <w n="30.2">soldats</w> <w n="30.3">de</w> <w n="30.4">vingt</w> <w n="30.5">ans</w>, <w n="30.6">guerrier</w>, <w n="30.7">n</w>’<w n="30.8">ont</w> <w n="30.9">qu</w>’<w n="30.10">à</w> <w n="30.11">te</w> <w n="30.12">voir</w></l>
					<l n="31" num="1.31"><w n="31.1">Pour</w> <w n="31.2">monter</w> <w n="31.3">au</w> <w n="31.4">niveau</w> <w n="31.5">de</w> <w n="31.6">leurs</w> <w n="31.7">aînés</w> <w n="31.8">stoïques</w> ;</l>
					<l n="32" num="1.32"><w n="32.1">Bénis</w> <w n="32.2">leurs</w> <w n="32.3">jeunes</w> <w n="32.4">fronts</w> <w n="32.5">de</w> <w n="32.6">tes</w> <w n="32.7">mains</w> <w n="32.8">héroïques</w>,</l>
					<l n="33" num="1.33"><w n="33.1">Et</w> <w n="33.2">Marceau</w> <w n="33.3">sortira</w> <w n="33.4">de</w> <w n="33.5">leurs</w> <w n="33.6">rangs</w>. <w n="33.7">Ton</w> <w n="33.8">nom</w> <w n="33.9">seul</w></l>
					<l n="34" num="1.34"><w n="34.1">Faisait</w> <w n="34.2">battre</w> <w n="34.3">nos</w> <w n="34.4">cœurs</w> <w n="34.5">déjà</w> <w n="34.6">sous</w> <w n="34.7">le</w> <w n="34.8">linceul</w></l>
					<l n="35" num="1.35"><w n="35.1">Dont</w> <w n="35.2">Bonaparte</w> <w n="35.3">avait</w> <w n="35.4">couvert</w> <w n="35.5">notre</w> <w n="35.6">patrie</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Et</w> <w n="36.2">nous</w> <w n="36.3">le</w> <w n="36.4">répétions</w> <w n="36.5">avec</w> <w n="36.6">idolâtrie</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Sans</w> <w n="37.2">prévoir</w> <w n="37.3">que</w> <w n="37.4">ce</w> <w n="37.5">nom</w> <w n="37.6">libérateur</w> <w n="37.7">serait</w></l>
					<l n="38" num="1.38"><w n="38.1">Le</w> <w n="38.2">cri</w> <w n="38.3">de</w> <w n="38.4">rallîment</w> <w n="38.5">qui</w> <w n="38.6">nous</w> <w n="38.7">délivrerait</w>.</l>
					<l n="39" num="1.39"><w n="39.1">Garibaldi</w> ! <w n="39.2">Ce</w> <w n="39.3">poids</w> <w n="39.4">jeté</w> <w n="39.5">dans</w> <w n="39.6">la</w> <w n="39.7">balance</w></l>
					<l n="40" num="1.40"><w n="40.1">Où</w> <w n="40.2">de</w> <w n="40.3">l</w>’<w n="40.4">autre</w> <w n="40.5">côté</w> <w n="40.6">pèse</w> <w n="40.7">la</w> <w n="40.8">violence</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Rétablit</w> <w n="41.2">l</w>’<w n="41.3">équilibre</w> <w n="41.4">en</w> <w n="41.5">faveur</w> <w n="41.6">du</w> <w n="41.7">bon</w> <w n="41.8">droit</w> ;</l>
					<l n="42" num="1.42"><w n="42.1">La</w> <w n="42.2">lumière</w> <w n="42.3">se</w> <w n="42.4">fait</w> <w n="42.5">partout</w> <w n="42.6">où</w> <w n="42.7">l</w>’<w n="42.8">on</w> <w n="42.9">te</w> <w n="42.10">voit</w>,</l>
					<l n="43" num="1.43"><w n="43.1">Et</w> <w n="43.2">les</w> <w n="43.3">rois</w> <w n="43.4">effarés</w> <w n="43.5">voient</w> <w n="43.6">trembler</w>, <w n="43.7">de</w> <w n="43.8">leur</w> <w n="43.9">bouge</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Leur</w> <w n="44.2">chute</w> <w n="44.3">dans</w> <w n="44.4">les</w> <w n="44.5">plis</w> <w n="44.6">de</w> <w n="44.7">ta</w> <w n="44.8">chemise</w> <w n="44.9">rouge</w> !</l>
				</lg>
				<closer>
					<dateline> 12 octobre.</dateline>
				</closer>
			</div></body></text></TEI>