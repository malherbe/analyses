<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA1">
				<head type="number">I</head>
				<head type="main">LE RETOUR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">toi</w>, <w n="1.4">chère</w> <w n="1.5">exilée</w> ! <w n="1.6">Oh</w> ! <w n="1.7">Laisse</w> <w n="1.8">que</w> <w n="1.9">j</w>’<w n="1.10">adore</w></l>
					<l n="2" num="1.2"><w n="2.1">Ta</w> <w n="2.2">figure</w> <w n="2.3">divine</w> <w n="2.4">où</w> <w n="2.5">rayonne</w> <w n="2.6">l</w>’<w n="2.7">aurore</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ô</w> <w n="3.2">république</w>, <w n="3.3">amour</w> <w n="3.4">vivace</w> <w n="3.5">de</w> <w n="3.6">nos</w> <w n="3.7">cœurs</w> !</l>
					<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">fosse</w> <w n="4.3">où</w>, <w n="4.4">dix</w>-<w n="4.5">huit</w> <w n="4.6">ans</w>, <w n="4.7">de</w> <w n="4.8">sinistres</w> <w n="4.9">vainqueurs</w></l>
					<l n="5" num="1.5"><w n="5.1">T</w>’<w n="5.2">ont</w> <w n="5.3">murée</w>, <w n="5.4">est</w> <w n="5.5">ouverte</w>, <w n="5.6">et</w> <w n="5.7">tu</w> <w n="5.8">viens</w>, <w n="5.9">souriante</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Claire</w> <w n="6.2">étoile</w> <w n="6.3">aux</w> <w n="6.4">rayons</w> <w n="6.5">de</w> <w n="6.6">qui</w> <w n="6.7">tout</w> <w n="6.8">s</w>’<w n="6.9">oriente</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Les</w> <w n="7.2">tombeaux</w> <w n="7.3">ne</w> <w n="7.4">t</w>’<w n="7.5">ont</w> <w n="7.6">rien</w> <w n="7.7">laissé</w> <w n="7.8">de</w> <w n="7.9">leur</w> <w n="7.10">pâleur</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Tu</w> <w n="8.2">viens</w> <w n="8.3">la</w> <w n="8.4">lèvre</w> <w n="8.5">fière</w> <w n="8.6">et</w> <w n="8.7">le</w> <w n="8.8">visage</w> <w n="8.9">en</w> <w n="8.10">fleur</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Tes</w> <w n="9.2">beaux</w> <w n="9.3">cheveux</w> <w n="9.4">au</w> <w n="9.5">vent</w>, <w n="9.6">comme</w> <w n="9.7">en</w> <w n="9.8">quatre</w>-<w n="9.9">vingt</w>-<w n="9.10">douze</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Dire</w> <w n="10.2">au</w> <w n="10.3">monde</w> : « <w n="10.4">ouvre</w>-<w n="10.5">moi</w> <w n="10.6">tes</w> <w n="10.7">bras</w>, <w n="10.8">je</w> <w n="10.9">suis</w> <w n="10.10">l</w>’<w n="10.11">épouse</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Je</w> <w n="11.2">suis</w> <w n="11.3">la</w> <w n="11.4">fiancée</w> ! <w n="11.5">Aimons</w>-<w n="11.6">nous</w> ! <w n="11.7">Nous</w> <w n="11.8">allons</w></l>
					<l n="12" num="1.12"><w n="12.1">Par</w> <w n="12.2">le</w> <w n="12.3">bois</w>, <w n="12.4">par</w> <w n="12.5">la</w> <w n="12.6">plaine</w> <w n="12.7">et</w> <w n="12.8">par</w> <w n="12.9">les</w> <w n="12.10">noirs</w> <w n="12.11">vallons</w></l>
					<l n="13" num="1.13"><w n="13.1">Épouvanter</w> <w n="13.2">encor</w> <w n="13.3">ceux</w> <w n="13.4">qui</w> <w n="13.5">me</w> <w n="13.6">croyaient</w> <w n="13.7">morte</w>.</l>
					<l n="14" num="1.14"><w n="14.1">Nous</w> <w n="14.2">allons</w> <w n="14.3">retrouver</w> <w n="14.4">la</w> <w n="14.5">France</w> <w n="14.6">libre</w> <w n="14.7">et</w> <w n="14.8">forte</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Dont</w> <w n="15.2">le</w> <w n="15.3">regard</w>, <w n="15.4">hâtant</w> <w n="15.5">les</w> <w n="15.6">lenteurs</w> <w n="15.7">du</w> <w n="15.8">berceau</w>,</l>
					<l n="16" num="1.16"><w n="16.1">En</w> <w n="16.2">tirait</w> <w n="16.3">ces</w> <w n="16.4">enfants</w> <w n="16.5">sacrés</w>, <w n="16.6">Hoche</w> <w n="16.7">et</w> <w n="16.8">Marceau</w> !</l>
					<l n="17" num="1.17"><w n="17.1">Les</w> <w n="17.2">rois</w> <w n="17.3">font</w> <w n="17.4">leur</w> <w n="17.5">métier</w> <w n="17.6">en</w> <w n="17.7">vendant</w> <w n="17.8">la</w> <w n="17.9">patrie</w> ;</l>
					<l n="18" num="1.18"><w n="18.1">Nous</w> <w n="18.2">la</w> <w n="18.3">leur</w> <w n="18.4">reprendrons</w>, <w n="18.5">toujours</w> <w n="18.6">belle</w>, <w n="18.7">inflétrie</w>.</l>
					<l n="19" num="1.19"><w n="19.1">Nous</w> <w n="19.2">balaîrons</w> <w n="19.3">encor</w> <w n="19.4">ces</w> <w n="19.5">louches</w> <w n="19.6">majestés</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Ces</w> <w n="20.2">demi</w>-<w n="20.3">dieux</w> <w n="20.4">poussahs</w>, <w n="20.5">aux</w> <w n="20.6">doigts</w> <w n="20.7">ensanglantés</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Qu</w>’<w n="21.2">on</w> <w n="21.3">appelle</w> <w n="21.4">césars</w>, <w n="21.5">rois</w>, <w n="21.6">empereurs</w>, <w n="21.7">que</w> <w n="21.8">sais</w>-<w n="21.9">je</w> ?</l>
					<l n="22" num="1.22"><w n="22.1">Le</w> <w n="22.2">sol</w> <w n="22.3">redeviendra</w> <w n="22.4">vierge</w> <w n="22.5">comme</w> <w n="22.6">la</w> <w n="22.7">neige</w></l>
					<l n="23" num="1.23"><w n="23.1">Des</w> <w n="23.2">glaciers</w> <w n="23.3">éternels</w>, <w n="23.4">partout</w> <w n="23.5">où</w> <w n="23.6">nous</w> <w n="23.7">aurons</w></l>
					<l n="24" num="1.24"><w n="24.1">Fait</w> <w n="24.2">retentir</w> <w n="24.3">le</w> <w n="24.4">chant</w> <w n="24.5">triomphal</w> <w n="24.6">des</w> <w n="24.7">clairons</w> !</l>
					<l n="25" num="1.25"><w n="25.1">Oh</w> ! <w n="25.2">Lorsqu</w>’<w n="25.3">on</w> <w n="25.4">entendra</w> <w n="25.5">mon</w> <w n="25.6">rire</w> <w n="25.7">de</w> <w n="25.8">gauloise</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Ce</w> <w n="26.2">rire</w> <w n="26.3">dont</w> <w n="26.4">l</w>’<w n="26.5">éclat</w> <w n="26.6">printanier</w> <w n="26.7">apprivoise</w></l>
					<l n="27" num="1.27"><w n="27.1">Les</w> <w n="27.2">lions</w> <w n="27.3">du</w> <w n="27.4">désert</w>, <w n="27.5">comme</w> <w n="27.6">l</w>’<w n="27.7">espoir</w> <w n="27.8">joyeux</w></l>
					<l n="28" num="1.28"><w n="28.1">Rentrera</w> <w n="28.2">dans</w> <w n="28.3">les</w> <w n="28.4">cœurs</w> <w n="28.5">sombres</w> <w n="28.6">et</w> <w n="28.7">soucieux</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Et</w> <w n="29.2">comme</w> <w n="29.3">on</w> <w n="29.4">redira</w> <w n="29.5">follement</w> <w n="29.6">sous</w> <w n="29.7">les</w> <w n="29.8">chênes</w> :</l>
					<l n="30" num="1.30"><w n="30.1">Les</w> <w n="30.2">tyrans</w> <w n="30.3">sont</w> <w n="30.4">vaincus</w>, <w n="30.5">l</w>’<w n="30.6">homme</w> <w n="30.7">n</w>’<w n="30.8">a</w> <w n="30.9">plus</w> <w n="30.10">de</w> <w n="30.11">chaînes</w> ! »</l>
					<l n="31" num="1.31"><w n="31.1">Oui</w>, <w n="31.2">c</w>’<w n="31.3">est</w> <w n="31.4">toi</w> ! <w n="31.5">C</w>’<w n="31.6">est</w> <w n="31.7">ta</w> <w n="31.8">voix</w> <w n="31.9">pure</w> <w n="31.10">qui</w>, <w n="31.11">ce</w> <w n="31.12">matin</w>,</l>
					<l n="32" num="1.32"><w n="32.1">A</w> <w n="32.2">réveillé</w> <w n="32.3">l</w>’<w n="32.4">écho</w> <w n="32.5">de</w> <w n="32.6">son</w> <w n="32.7">timbre</w> <w n="32.8">argentin</w>.</l>
					<l n="33" num="1.33"><w n="33.1">Oh</w> ! <w n="33.2">Je</w> <w n="33.3">doutais</w> ! <w n="33.4">En</w> <w n="33.5">proie</w> <w n="33.6">à</w> <w n="33.7">l</w>’<w n="33.8">angoisse</w> <w n="33.9">mortelle</w> ;</l>
					<l n="34" num="1.34"><w n="34.1">Nous</w> <w n="34.2">demandions</w> <w n="34.3">depuis</w> <w n="34.4">si</w> <w n="34.5">longtemps</w> « <w n="34.6">viendra</w>-<w n="34.7">t</w>-<w n="34.8">elle</w> ? »</l>
					<l n="35" num="1.35"><w n="35.1">Hélas</w> ! <w n="35.2">Nous</w> <w n="35.3">t</w>’<w n="35.4">attendions</w> <w n="35.5">si</w> <w n="35.6">désespérément</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Que</w> <w n="36.2">nous</w> <w n="36.3">disions</w> : « <w n="36.4">encore</w> <w n="36.5">un</w> <w n="36.6">songe</w> <w n="36.7">qui</w> <w n="36.8">nous</w> <w n="36.9">ment</w> ! »</l>
					<l n="37" num="1.37"><w n="37.1">C</w>’<w n="37.2">était</w> <w n="37.3">bien</w> <w n="37.4">toi</w> <w n="37.5">pourtant</w>, <w n="37.6">république</w>, <w n="37.7">ô</w> <w n="37.8">guerrière</w> !</l>
					<l n="38" num="1.38"><w n="38.1">Qui</w> <w n="38.2">nous</w> <w n="38.3">apparaissais</w> <w n="38.4">dans</w> <w n="38.5">un</w> <w n="38.6">flot</w> <w n="38.7">de</w> <w n="38.8">lumière</w>.</l>
					<l n="39" num="1.39"><w n="39.1">Tu</w> <w n="39.2">savais</w> <w n="39.3">ton</w> <w n="39.4">pays</w> <w n="39.5">presque</w> <w n="39.6">désespéré</w> ;</l>
					<l n="40" num="1.40"><w n="40.1">Alors</w>, <w n="40.2">brisant</w> <w n="40.3">du</w> <w n="40.4">poing</w> <w n="40.5">le</w> <w n="40.6">sépulcre</w> <w n="40.7">effaré</w></l>
					<l n="41" num="1.41"><w n="41.1">Qu</w>’<w n="41.2">avait</w> <w n="41.3">fermé</w> <w n="41.4">sur</w> <w n="41.5">toi</w> <w n="41.6">la</w> <w n="41.7">main</w> <w n="41.8">d</w>’<w n="41.9">un</w> <w n="41.10">bandit</w> <w n="41.11">corse</w>,</l>
					<l n="42" num="1.42"><w n="42.1">Tu</w> <w n="42.2">surgis</w> <w n="42.3">dans</w> <w n="42.4">ta</w> <w n="42.5">grâce</w> <w n="42.6">auguste</w> <w n="42.7">et</w> <w n="42.8">dans</w> <w n="42.9">ta</w> <w n="42.10">force</w>,</l>
					<l n="43" num="1.43"><w n="43.1">En</w> <w n="43.2">criant</w> : « <w n="43.3">me</w> <w n="43.4">voici</w> ! <w n="43.5">Peuple</w>, <w n="43.6">espère</w> <w n="43.7">et</w> <w n="43.8">combats</w> ! »</l>
					<l n="44" num="1.44"><w n="44.1">Va</w>, <w n="44.2">nous</w> <w n="44.3">te</w> <w n="44.4">garderons</w> ! <w n="44.5">Va</w>, <w n="44.6">si</w> <w n="44.7">tu</w> <w n="44.8">succombas</w></l>
					<l n="45" num="1.45"><w n="45.1">Pour</w> <w n="45.2">avoir</w>, <w n="45.3">dans</w> <w n="45.4">ta</w> <w n="45.5">foi</w> <w n="45.6">divinement</w> <w n="45.7">sincère</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Pensé</w> <w n="46.2">qu</w>’<w n="46.3">un</w> <w n="46.4">prince</w> <w n="46.5">peut</w> <w n="46.6">n</w>’<w n="46.7">être</w> <w n="46.8">pas</w> <w n="46.9">un</w> <w n="46.10">corsaire</w>,</l>
					<l n="47" num="1.47"><w n="47.1">Qu</w>’<w n="47.2">un</w> <w n="47.3">serment</w> <w n="47.4">est</w> <w n="47.5">sacré</w>, <w n="47.6">que</w> <w n="47.7">l</w>’<w n="47.8">honneur</w> <w n="47.9">luit</w> <w n="47.10">pour</w> <w n="47.11">tous</w>,</l>
					<l n="48" num="1.48"><w n="48.1">Sois</w> <w n="48.2">tranquille</w>, <w n="48.3">à</w> <w n="48.4">présent</w> <w n="48.5">nous</w> <w n="48.6">prendrons</w> <w n="48.7">garde</w> <w n="48.8">à</w> <w n="48.9">nous</w>.</l>
					<l n="49" num="1.49"><w n="49.1">Te</w> <w n="49.2">voilà</w> <w n="49.3">revenue</w>. <w n="49.4">Il</w> <w n="49.5">suffit</w>. <w n="49.6">Qu</w>’<w n="49.7">on</w> <w n="49.8">te</w> <w n="49.9">voie</w></l>
					<l n="50" num="1.50"><w n="50.1">Encor</w>, <w n="50.2">encor</w>, <w n="50.3">toujours</w>, <w n="50.4">messagère</w> <w n="50.5">de</w> <w n="50.6">joie</w> !</l>
					<l n="51" num="1.51"><w n="51.1">Que</w> <w n="51.2">mon</w> <w n="51.3">regard</w> <w n="51.4">s</w>’<w n="51.5">enivre</w> <w n="51.6">à</w> <w n="51.7">force</w> <w n="51.8">de</w> <w n="51.9">te</w> <w n="51.10">voir</w> !</l>
					<l n="52" num="1.52"><w n="52.1">Rappelle</w>-<w n="52.2">nous</w> <w n="52.3">les</w> <w n="52.4">mots</w> <w n="52.5">presque</w> <w n="52.6">oubliés</w> : <hi rend="ital"> <w n="52.7">devoir</w>,</hi></l>
					<l n="53" num="1.53"><hi rend="ital"> <w n="53.1">liberté</w>, <w n="53.2">dévoûment</w>, <w n="53.3">amour</w>, <w n="53.4">paix</w> <w n="53.5">et</w> <w n="53.6">concorde</w>. </hi></l>
					<l n="54" num="1.54"><w n="54.1">Ô</w> <w n="54.2">bonheur</w> <w n="54.3">du</w> <w n="54.4">retour</w> ! <w n="54.5">Comme</w> <w n="54.6">le</w> <w n="54.7">cœur</w> <w n="54.8">déborde</w>,</l>
					<l n="55" num="1.55"><w n="55.1">Et</w> <w n="55.2">comme</w> <w n="55.3">l</w>’<w n="55.4">air</w> <w n="55.5">se</w> <w n="55.6">teint</w> <w n="55.7">d</w>’<w n="55.8">azur</w>, <w n="55.9">de</w> <w n="55.10">pourpre</w> <w n="55.11">et</w> <w n="55.12">d</w>’<w n="55.13">or</w> ! …</l>
					<l n="56" num="1.56"><w n="56.1">Ô</w> <w n="56.2">république</w> ! <w n="56.3">Si</w> <w n="56.4">Barbès</w> <w n="56.5">vivait</w> <w n="56.6">encor</w> !</l>
				</lg>
				<closer>
					<dateline>5 septembre.</dateline>
				</closer>
			</div></body></text></TEI>