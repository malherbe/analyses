<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA19">
				<head type="number">XIX</head>
				<head type="main">CEUX QUI RESTENT</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Approchez-vous. Ceci c’est le tas des dévots.
							</quote>
							<bibl>
								<name>Victor Hugo</name>, <hi rend="ital">Châtiments</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="8" unit="char"></space><w n="1.1">Les</w> <w n="1.2">hauteurs</w> <w n="1.3">du</w> <w n="1.4">ciel</w> <w n="1.5">pur</w> <w n="1.6">et</w> <w n="1.7">clair</w></l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">Sont</w> <w n="2.2">lugubrement</w> <w n="2.3">enfumées</w> ;</l>
						<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">La</w> <hi rend="ital"> <w n="3.2">marseillaise</w> </hi> <w n="3.3">embrase</w> <w n="3.4">l</w>’<w n="3.5">air</w>,</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Comme</w> <w n="4.2">au</w> <w n="4.3">temps</w> <w n="4.4">des</w> <w n="4.5">quatorze</w> <w n="4.6">armées</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space quantity="8" unit="char"></space><w n="5.1">Sa</w> <w n="5.2">voix</w> <w n="5.3">dit</w> : « <w n="5.4">l</w>’<w n="5.5">Étranger</w> <w n="5.6">est</w> <w n="5.7">là</w> ! »</l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1">Et</w> <w n="6.2">voici</w> <w n="6.3">que</w> <w n="6.4">la</w> <w n="6.5">France</w> <w n="6.6">entière</w>,</l>
						<l n="7" num="2.3"><space quantity="8" unit="char"></space><w n="7.1">À</w> <w n="7.2">cette</w> <w n="7.3">voix</w> <w n="7.4">qui</w> <w n="7.5">l</w>’<w n="7.6">appela</w>,</l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">Court</w> <w n="8.2">purifier</w> <w n="8.3">la</w> <w n="8.4">frontière</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space quantity="8" unit="char"></space><w n="9.1">Au</w> <w n="9.2">premier</w> <w n="9.3">et</w> <w n="9.4">vibrant</w> <w n="9.5">appel</w>,</l>
						<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">L</w>’<w n="10.2">étudiant</w> <w n="10.3">quitte</w> <w n="10.4">son</w> <w n="10.5">livre</w>,</l>
						<l n="11" num="3.3"><space quantity="8" unit="char"></space><w n="11.1">L</w>’<w n="11.2">étude</w> <w n="11.3">austère</w>, <w n="11.4">le</w> <w n="11.5">scalpel</w> ;</l>
						<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">Le</w> <w n="12.2">pays</w> <w n="12.3">veut</w> <w n="12.4">qu</w>’<w n="12.5">on</w> <w n="12.6">le</w> <w n="12.7">délivre</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space quantity="8" unit="char"></space><w n="13.1">L</w>’<w n="13.2">ouvrier</w> <w n="13.3">railleur</w> <w n="13.4">qui</w> <w n="13.5">chantait</w></l>
						<l n="14" num="4.2"><space quantity="8" unit="char"></space><w n="14.1">Comme</w> <w n="14.2">un</w> <w n="14.3">moineau</w> <w n="14.4">franc</w> <w n="14.5">à</w> <w n="14.6">l</w>’<w n="14.7">aurore</w>,</l>
						<l n="15" num="4.3"><space quantity="8" unit="char"></space><w n="15.1">A</w> <w n="15.2">senti</w> <w n="15.3">son</w> <w n="15.4">cœur</w> <w n="15.5">qui</w> <w n="15.6">battait</w></l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">Au</w> <w n="16.2">rhythme</w> <w n="16.3">du</w> <w n="16.4">tambour</w> <w n="16.5">sonore</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space quantity="8" unit="char"></space><w n="17.1">Aujourd</w>’<w n="17.2">hui</w> <w n="17.3">nul</w> <w n="17.4">recul</w> <w n="17.5">bâtard</w>,</l>
						<l n="18" num="5.2"><space quantity="8" unit="char"></space><w n="18.1">La</w> <w n="18.2">campagne</w> <w n="18.3">est</w> <w n="18.4">avec</w> <w n="18.5">la</w> <w n="18.6">rue</w> ;</l>
						<l n="19" num="5.3"><space quantity="8" unit="char"></space><w n="19.1">Le</w> <w n="19.2">sillon</w> <w n="19.3">s</w>’<w n="19.4">ouvrira</w> <w n="19.5">plus</w> <w n="19.6">tard</w></l>
						<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1">Aux</w> <w n="20.2">morsures</w> <w n="20.3">de</w> <w n="20.4">la</w> <w n="20.5">charrue</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space quantity="8" unit="char"></space><w n="21.1">Adieu</w>, <w n="21.2">famille</w> ! <w n="21.3">Adieu</w>, <w n="21.4">foyer</w> !</l>
						<l n="22" num="6.2"><space quantity="8" unit="char"></space><w n="22.1">Ardents</w>, <w n="22.2">pleins</w> <w n="22.3">d</w>’<w n="22.4">une</w> <w n="22.5">mâle</w> <w n="22.6">joie</w>,</l>
						<l n="23" num="6.3"><space quantity="8" unit="char"></space><w n="23.1">Ils</w> <w n="23.2">vont</w> <w n="23.3">où</w> <w n="23.4">l</w>’<w n="23.5">on</w> <w n="23.6">voit</w> <w n="23.7">flamboyer</w></l>
						<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">Les</w> <w n="24.2">yeux</w> <w n="24.3">du</w> <w n="24.4">noir</w> <w n="24.5">oiseau</w> <w n="24.6">de</w> <w n="24.7">proie</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><space quantity="8" unit="char"></space><w n="25.1">Ils</w> <w n="25.2">portent</w> <w n="25.3">tous</w> <w n="25.4">au</w> <w n="25.5">front</w> <w n="25.6">le</w> <w n="25.7">sceau</w></l>
						<l n="26" num="7.2"><space quantity="8" unit="char"></space><w n="26.1">Des</w> <w n="26.2">vaillants</w> <w n="26.3">sans</w> <w n="26.4">peur</w> <w n="26.5">ni</w> <w n="26.6">reproche</w> ;</l>
						<l n="27" num="7.3"><space quantity="8" unit="char"></space><w n="27.1">Ils</w> <w n="27.2">ont</w> <w n="27.3">ta</w> <w n="27.4">jeunesse</w>, <w n="27.5">ô</w> <w n="27.6">Marceau</w> !</l>
						<l n="28" num="7.4"><space quantity="8" unit="char"></space><w n="28.1">Ils</w> <w n="28.2">ont</w> <w n="28.3">le</w> <w n="28.4">dévoûment</w> <w n="28.5">de</w> <w n="28.6">Hoche</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><space quantity="8" unit="char"></space><w n="29.1">Ceux</w> <w n="29.2">que</w> <w n="29.3">l</w>’<w n="29.4">on</w> <w n="29.5">croyait</w> <w n="29.6">engloutis</w></l>
						<l n="30" num="8.2"><space quantity="8" unit="char"></space><w n="30.1">Dans</w> <w n="30.2">l</w>’<w n="30.3">imbécillité</w> <w n="30.4">des</w> <w n="30.5">filles</w>,</l>
						<l n="31" num="8.3"><space quantity="8" unit="char"></space><w n="31.1">Criant</w> : « <w n="31.2">Liberté</w> ! » <w n="31.3">sont</w> <w n="31.4">partis</w></l>
						<l n="32" num="8.4"><space quantity="8" unit="char"></space><w n="32.1">Avec</w> <w n="32.2">les</w> <w n="32.3">preneurs</w> <w n="32.4">de</w> <w n="32.5">bastilles</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><space quantity="8" unit="char"></space><w n="33.1">On</w> <w n="33.2">voit</w>, <w n="33.3">plein</w> <w n="33.4">de</w> <w n="33.5">fraternité</w>,</l>
						<l n="34" num="9.2"><space quantity="8" unit="char"></space><w n="34.1">L</w>’<w n="34.2">habit</w> <w n="34.3">marcher</w> <w n="34.4">avec</w> <w n="34.5">la</w> <w n="34.6">blouse</w>,</l>
						<l n="35" num="9.3"><space quantity="8" unit="char"></space><w n="35.1">Sous</w> <w n="35.2">ta</w> <w n="35.3">lumineuse</w> <w n="35.4">clarté</w>,</l>
						<l n="36" num="9.4"><space quantity="8" unit="char"></space><w n="36.1">Ô</w> <w n="36.2">soleil</w> <w n="36.3">de</w> <w n="36.4">quatre</w>-<w n="36.5">vingt</w>-<w n="36.6">douze</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><space quantity="8" unit="char"></space><w n="37.1">Rangs</w> <w n="37.2">confondus</w>, <w n="37.3">tous</w> <w n="37.4">citoyens</w>,</l>
						<l n="38" num="10.2"><space quantity="8" unit="char"></space><w n="38.1">Un</w> <w n="38.2">cœur</w> <w n="38.3">pour</w> <w n="38.4">tous</w>, <w n="38.5">rien</w> <w n="38.6">qu</w>’<w n="38.7">une</w> <w n="38.8">fibre</w>,</l>
						<l n="39" num="10.3"><space quantity="8" unit="char"></space><w n="39.1">Qu</w>’<w n="39.2">une</w> <w n="39.3">voix</w> <w n="39.4">pour</w> <w n="39.5">dire</w> <w n="39.6">aux</w> <w n="39.7">anciens</w> :</l>
						<l n="40" num="10.4"><space quantity="8" unit="char"></space>« <w n="40.1">Nous</w> <w n="40.2">vous</w> <w n="40.3">rendrons</w> <w n="40.4">votre</w> <w n="40.5">sol</w> <w n="40.6">libre</w> ! »</l>
					</lg>
					</div>
				<div type="section" n="2">
				<head type="number">II</head>
					<lg n="1">
						<l n="41" num="1.1"><w n="41.1">Regardez</w> <w n="41.2">ces</w> <w n="41.3">maisons</w> <w n="41.4">hautes</w>, <w n="41.5">aux</w> <w n="41.6">sombres</w> <w n="41.7">murs</w>,</l>
						<l n="42" num="1.2"><w n="42.1">Si</w> <w n="42.2">calmes</w> <w n="42.3">au</w> <w n="42.4">milieu</w> <w n="42.5">du</w> <w n="42.6">fracas</w> <w n="42.7">des</w> <w n="42.8">tonnerres</w>,</l>
						<l n="43" num="1.3"><w n="43.1">Pleines</w> <w n="43.2">de</w> <w n="43.3">beaux</w> <w n="43.4">jardins</w> <w n="43.5">où</w> <w n="43.6">pendent</w> <w n="43.7">des</w> <w n="43.8">fruits</w> <w n="43.9">mûrs</w>,</l>
						<l n="44" num="1.4"><w n="44.1">Tout</w> <w n="44.2">est</w> <w n="44.3">silencieux</w> : <w n="44.4">ce</w> <w n="44.5">sont</w> <w n="44.6">les</w> <w n="44.7">séminaires</w>.</l>
					</lg>
					<lg n="2">
						<l n="45" num="2.1"><w n="45.1">Là</w>, <w n="45.2">pendant</w> <w n="45.3">que</w> <w n="45.4">la</w> <w n="45.5">femme</w>, <w n="45.6">acceptant</w> <w n="45.7">le</w> <w n="45.8">devoir</w>,</l>
						<l n="46" num="2.2"><w n="46.1">Reste</w> <w n="46.2">au</w> <w n="46.3">logis</w> <w n="46.4">pleurant</w>, <w n="46.5">attendant</w> <w n="46.6">des</w> <w n="46.7">nouvelles</w>,</l>
						<l n="47" num="2.3"><w n="47.1">Et</w> <w n="47.2">fait</w> <w n="47.3">de</w> <w n="47.4">la</w> <w n="47.5">charpie</w>, <w n="47.6">et</w> <w n="47.7">regarde</w> <w n="47.8">sans</w> <w n="47.9">voir</w></l>
						<l n="48" num="2.4"><w n="48.1">Le</w> <w n="48.2">pavé</w> <w n="48.3">qu</w>’<w n="48.4">en</w> <w n="48.5">volant</w> <w n="48.6">rasent</w> <w n="48.7">les</w> <w n="48.8">hirondelles</w> ;</l>
					</lg>
					<lg n="3">
						<l n="49" num="3.1"><w n="49.1">Pendant</w> <w n="49.2">que</w> <w n="49.3">le</w> <w n="49.4">vieillard</w> <w n="49.5">dit</w> <w n="49.6">à</w> <w n="49.7">l</w>’<w n="49.8">enfant</w> : « <w n="49.9">Grandis</w> ! »</l>
						<l n="50" num="3.2"><w n="50.1">Et</w> <w n="50.2">lui</w> <w n="50.3">conte</w> <w n="50.4">comment</w> <w n="50.5">jadis</w> <w n="50.6">les</w> <w n="50.7">volontaires</w></l>
						<l n="51" num="3.3"><w n="51.1">Entraient</w>, <w n="51.2">tambour</w> <w n="51.3">battant</w>, <w n="51.4">chez</w> <w n="51.5">les</w> <w n="51.6">rois</w> <w n="51.7">interdits</w>,</l>
						<l n="52" num="3.4"><w n="52.1">En</w> <w n="52.2">sabots</w>, <w n="52.3">et</w> <w n="52.4">brisaient</w> <w n="52.5">les</w> <w n="52.6">jougs</w> <w n="52.7">héréditaires</w> ;</l>
					</lg>
					<lg n="4">
						<l n="53" num="4.1"><w n="53.1">Oui</w>, <w n="53.2">pendant</w> <w n="53.3">que</w> <w n="53.4">brisés</w> <w n="53.5">et</w> <w n="53.6">mordant</w> <w n="53.7">leurs</w> <w n="53.8">chevets</w>,</l>
						<l n="54" num="4.2"><w n="54.1">Les</w> <w n="54.2">malades</w>, <w n="54.3">songeant</w> <w n="54.4">à</w> <w n="54.5">la</w> <w n="54.6">sainte</w> <w n="54.7">patrie</w>,</l>
						<l n="55" num="4.3"><w n="55.1">Disent</w> <w n="55.2">en</w> <w n="55.3">agitant</w> <w n="55.4">leurs</w> <w n="55.5">bras</w> : « <w n="55.6">Si</w> <w n="55.7">je</w> <w n="55.8">pouvais</w> ! »</l>
						<l n="56" num="4.4"><w n="56.1">Et</w> <w n="56.2">retombent</w> <w n="56.3">vaincus</w> <w n="56.4">sur</w> <w n="56.5">leur</w> <w n="56.6">couche</w> <w n="56.7">meurtrie</w> ;</l>
					</lg>
					<lg n="5">
						<l n="57" num="5.1"><w n="57.1">Quand</w> <w n="57.2">le</w> <w n="57.3">glas</w> <w n="57.4">du</w> <w n="57.5">tocsin</w> <w n="57.6">emplit</w> <w n="57.7">l</w>’<w n="57.8">air</w> <w n="57.9">frissonnant</w>,</l>
						<l n="58" num="5.2"><w n="58.1">Quand</w> <w n="58.2">la</w> <w n="58.3">France</w> <w n="58.4">à</w> <w n="58.5">nos</w> <w n="58.6">cœurs</w> <w n="58.7">jette</w> <w n="58.8">le</w> <w n="58.9">cri</w> <w n="58.10">d</w>’<w n="58.11">alarme</w> ;</l>
						<l n="59" num="5.3"><w n="59.1">Pour</w> <w n="59.2">chasser</w> <w n="59.3">l</w>’<w n="59.4">ennemi</w> <w n="59.5">trop</w> <w n="59.6">vite</w> <w n="59.7">rayonnant</w>,</l>
						<l n="60" num="5.4"><w n="60.1">Quand</w> <w n="60.2">on</w> <w n="60.3">se</w> <w n="60.4">sert</w> <w n="60.5">partout</w> <w n="60.6">de</w> <w n="60.7">n</w>’<w n="60.8">importe</w> <w n="60.9">quelle</w> <w n="60.10">arme</w> ;</l>
					</lg>
					<lg n="6">
						<l n="61" num="6.1"><w n="61.1">Derrière</w> <w n="61.2">ces</w> <w n="61.3">grands</w> <w n="61.4">murs</w>, <w n="61.5">roses</w>, <w n="61.6">gaillards</w>, <w n="61.7">dispos</w>,</l>
						<l n="62" num="6.2"><w n="62.1">Des</w> <w n="62.2">hommes</w> <w n="62.3">de</w> <w n="62.4">vingt</w> <w n="62.5">ans</w>, <w n="62.6">vêtus</w> <w n="62.7">de</w> <w n="62.8">robes</w> <w n="62.9">noires</w>,</l>
						<l n="63" num="6.3"><w n="63.1">Reprennent</w> <w n="63.2">doucement</w> <w n="63.3">de</w> <w n="63.4">ravissants</w> <w n="63.5">propos</w></l>
						<l n="64" num="6.4"><w n="64.1">Interrompus</w> <w n="64.2">pendant</w> <w n="64.3">l</w>’<w n="64.4">heure</w> <w n="64.5">des</w> <w n="64.6">offertoires</w>.</l>
					</lg>
					<lg n="7">
						<l n="65" num="7.1"><w n="65.1">Oh</w> ! <w n="65.2">Les</w> <w n="65.3">chastes</w> <w n="65.4">discours</w> ! « <w n="65.5">Dimanche</w>, <w n="65.6">Monseigneur</w></l>
						<l n="66" num="7.2"><w n="66.1">Officiait</w> <w n="66.2">avec</w> <w n="66.3">d</w>’<w n="66.4">admirables</w> <w n="66.5">dentelles</w>.</l>
						<l n="67" num="7.3">— <w n="67.1">Il</w> <w n="67.2">dîne</w> <w n="67.3">ici</w> <w n="67.4">jeudi</w>. — <w n="67.5">Vraiment</w> ! — <w n="67.6">Ah</w> ! <w n="67.7">Quel</w> <w n="67.8">honneur</w> ! »</l>
						<l n="68" num="7.4"><w n="68.1">C</w>’<w n="68.2">est</w> <w n="68.3">comme</w> <w n="68.4">un</w> <w n="68.5">gazouillis</w> <w n="68.6">de</w> <w n="68.7">jeunes</w> <w n="68.8">demoiselles</w>.</l>
					</lg>
					<lg n="8">
						<l n="69" num="8.1"><w n="69.1">Leur</w> <w n="69.2">oreille</w> <w n="69.3">est</w> <w n="69.4">fermée</w> <w n="69.5">aux</w> <w n="69.6">clameurs</w> <w n="69.7">du</w> <w n="69.8">dehors</w>.</l>
						<l n="70" num="8.2"><w n="70.1">On</w> <w n="70.2">leur</w> <w n="70.3">crie</w> <w n="70.4">au</w> <w n="70.5">secours</w> : <w n="70.6">ils</w> <w n="70.7">chantent</w> <w n="70.8">des</w> <w n="70.9">cantiques</w>.</l>
						<l n="71" num="8.3"><w n="71.1">On</w> <w n="71.2">meurt</w> <w n="71.3">à</w> <w n="71.4">leurs</w> <w n="71.5">côtés</w> : <w n="71.6">ils</w> <w n="71.7">restent</w>, <w n="71.8">sages</w>, <w n="71.9">forts</w>,</l>
						<l n="72" num="8.4"><w n="72.1">Cloîtrés</w> <w n="72.2">dévotement</w> <w n="72.3">dans</w> <w n="72.4">leurs</w> <w n="72.5">ardeurs</w> <w n="72.6">mystiques</w>.</l>
					</lg>
					<lg n="9">
						<l n="73" num="9.1"><w n="73.1">Ils</w> <w n="73.2">n</w>’<w n="73.3">ont</w> <w n="73.4">donc</w> <w n="73.5">pas</w> <w n="73.6">de</w> <w n="73.7">cœur</w>, <w n="73.8">ils</w> <w n="73.9">n</w>’<w n="73.10">ont</w> <w n="73.11">donc</w> <w n="73.12">pas</w> <w n="73.13">de</w> <w n="73.14">sang</w>,</l>
						<l n="74" num="9.2"><w n="74.1">Ces</w> <w n="74.2">êtres</w> <w n="74.3">patelins</w> <w n="74.4">aux</w> <w n="74.5">figures</w> <w n="74.6">placides</w>,</l>
						<l n="75" num="9.3"><w n="75.1">Et</w> <w n="75.2">la</w> <w n="75.3">dévotion</w>, <w n="75.4">cet</w> <w n="75.5">agent</w> <w n="75.6">tout</w>-<w n="75.7">puissant</w>,</l>
						<l n="76" num="9.4"><w n="76.1">Les</w> <w n="76.2">a</w> <w n="76.3">donc</w> <w n="76.4">tous</w> <w n="76.5">fondus</w> <w n="76.6">en</w> <w n="76.7">ses</w> <w n="76.8">mielleux</w> <w n="76.9">acides</w> !</l>
					</lg>
					<lg n="10">
						<l n="77" num="10.1"><w n="77.1">Le</w> <w n="77.2">seigneur</w> <w n="77.3">leur</w> <w n="77.4">défend</w> <w n="77.5">d</w>’<w n="77.6">ensanglanter</w> <w n="77.7">leurs</w> <w n="77.8">mains</w>,</l>
						<l n="78" num="10.2"><w n="78.1">Disent</w>-<w n="78.2">ils</w>, <w n="78.3">en</w> <w n="78.4">baissant</w> <w n="78.5">les</w> <w n="78.6">yeux</w> <w n="78.7">vers</w> <w n="78.8">les</w> <w n="78.9">guipures</w></l>
						<l n="79" num="10.3"><w n="79.1">De</w> <w n="79.2">leur</w> <w n="79.3">surplis</w>. <w n="79.4">Ô</w> <w n="79.5">vous</w> ! <w n="79.6">Martyrs</w> <w n="79.7">des</w> <w n="79.8">droits</w> <w n="79.9">romains</w>,</l>
						<l n="80" num="10.4"><w n="80.1">Croient</w>-<w n="80.2">ils</w> <w n="80.3">que</w> <w n="80.4">Mentana</w> <w n="80.5">leur</w> <w n="80.6">fasse</w> <w n="80.7">les</w> <w n="80.8">mains</w> <w n="80.9">pures</w> ?</l>
					</lg>
					<lg n="11">
						<l n="81" num="11.1"><w n="81.1">Ils</w> <w n="81.2">ont</w> <w n="81.3">horreur</w> <w n="81.4">du</w> <w n="81.5">sang</w> ! <w n="81.6">Pourquoi</w> <w n="81.7">donc</w>, <w n="81.8">quand</w> <w n="81.9">il</w> <w n="81.10">faut</w></l>
						<l n="82" num="11.2"><w n="82.1">Protéger</w> <w n="82.2">les</w> <w n="82.3">chiffons</w> <w n="82.4">dont</w> <w n="82.5">leur</w> <w n="82.6">orgueil</w> <w n="82.7">s</w>’<w n="82.8">affuble</w>,</l>
						<l n="83" num="11.3"><w n="83.1">Les</w> <w n="83.2">temples</w>, <w n="83.3">les</w> <w n="83.4">trésors</w> <w n="83.5">qu</w>’<w n="83.6">ils</w> <w n="83.7">gardent</w> <w n="83.8">en</w> <w n="83.9">dépôt</w>,</l>
						<l n="84" num="11.4"><w n="84.1">Leur</w> <w n="84.2">dais</w> <w n="84.3">et</w> <w n="84.4">son</w> <w n="84.5">panache</w> <w n="84.6">et</w> <w n="84.7">leur</w> <w n="84.8">lourde</w> <w n="84.9">chasuble</w>,</l>
					</lg>
					<lg n="12">
						<l n="85" num="12.1"><w n="85.1">Sont</w>-<w n="85.2">ils</w> <w n="85.3">donc</w> <w n="85.4">les</w> <w n="85.5">premiers</w> <w n="85.6">à</w> <w n="85.7">crier</w> : « <w n="85.8">Guerre</w> ! <w n="85.9">Mort</w> ! »</l>
						<l n="86" num="12.2"><w n="86.1">Ils</w> <w n="86.2">ont</w> <w n="86.3">horreur</w> <w n="86.4">du</w> <w n="86.5">sang</w> <w n="86.6">qui</w> <w n="86.7">coule</w> <w n="86.8">de</w> <w n="86.9">leurs</w> <w n="86.10">veines</w>,</l>
						<l n="87" num="12.3"><w n="87.1">Mais</w> <w n="87.2">font</w> <w n="87.3">verser</w> <w n="87.4">celui</w> <w n="87.5">des</w> <w n="87.6">autres</w> <w n="87.7">sans</w> <w n="87.8">remord</w>,</l>
						<l n="88" num="12.4"><w n="88.1">Ô</w> <w n="88.2">bûcher</w> <w n="88.3">de</w> <w n="88.4">Jean</w> <w n="88.5">Huss</w> ! <w n="88.6">Ô</w> <w n="88.7">gorges</w> <w n="88.8">des</w> <w n="88.9">Cévennes</w> !</l>
					</lg>
					<lg n="13">
						<l n="89" num="13.1"><w n="89.1">Quoi</w> <w n="89.2">donc</w> ! <w n="89.3">Ils</w> <w n="89.4">resteront</w> <w n="89.5">priant</w>, <w n="89.6">croisant</w> <w n="89.7">les</w> <w n="89.8">bras</w>,</l>
						<l n="90" num="13.2"><w n="90.1">Sans</w> <w n="90.2">vouloir</w> <w n="90.3">secouer</w> <w n="90.4">leur</w> <w n="90.5">torpeur</w> <w n="90.6">animale</w> !</l>
						<l n="91" num="13.3"><w n="91.1">Tartufe</w> <w n="91.2">ne</w> <w n="91.3">saurait</w> <w n="91.4">imiter</w>, <w n="91.5">n</w>’<w n="91.6">est</w>-<w n="91.7">ce</w> <w n="91.8">pas</w> ?</l>
						<l n="92" num="13.4"><w n="92.1">L</w>’<w n="92.2">exemple</w> <w n="92.3">fier</w> <w n="92.4">donné</w> <w n="92.5">par</w> <w n="92.6">l</w>’<w n="92.7">école</w> <w n="92.8">normale</w>.</l>
					</lg>
					<lg n="14">
						<l n="93" num="14.1"><w n="93.1">Restez</w> <w n="93.2">donc</w>, <w n="93.3">prolongez</w> <w n="93.4">vos</w> <w n="93.5">pieux</w> <w n="93.6">nonchaloirs</w>,</l>
						<l n="94" num="14.2"><w n="94.1">Capucins</w>, <w n="94.2">cordeliers</w>, <w n="94.3">jésuites</w>, <w n="94.4">lazaristes</w> !</l>
						<l n="95" num="14.3"><w n="95.1">Qu</w>’<w n="95.2">on</w> <w n="95.3">puisse</w> <w n="95.4">voir</w> <w n="95.5">mêlés</w> <w n="95.6">les</w> <w n="95.7">uniformes</w> <w n="95.8">noirs</w></l>
						<l n="96" num="14.4"><w n="96.1">Des</w> <w n="96.2">agents</w> <w n="96.3">de</w> <w n="96.4">police</w> <w n="96.5">et</w> <w n="96.6">des</w> <w n="96.7">séminaristes</w>.</l>
					</lg>
					<lg n="15">
						<l n="97" num="15.1"><w n="97.1">Et</w> <w n="97.2">plus</w> <w n="97.3">tard</w>, <w n="97.4">n</w>’<w n="97.5">est</w>-<w n="97.6">ce</w> <w n="97.7">pas</w> ? <w n="97.8">Quand</w> <w n="97.9">nos</w> <w n="97.10">loyaux</w> <w n="97.11">enfants</w>,</l>
						<l n="98" num="15.2"><w n="98.1">Ayant</w> <w n="98.2">fait</w> <w n="98.3">devant</w> <w n="98.4">eux</w> <w n="98.5">évanouir</w> <w n="98.6">la</w> <w n="98.7">horde</w></l>
						<l n="99" num="15.3"><w n="99.1">Des</w> <w n="99.2">esclaves</w> <w n="99.3">du</w> <w n="99.4">nord</w>, <w n="99.5">reviendront</w> <w n="99.6">triomphants</w>,</l>
						<l n="100" num="15.4"><w n="100.1">Ramenant</w> <w n="100.2">parmi</w> <w n="100.3">nous</w> <w n="100.4">l</w>’<w n="100.5">éternelle</w> <w n="100.6">concorde</w>,</l>
					</lg>
					<lg n="16">
						<l n="101" num="16.1"><w n="101.1">Ô</w> <w n="101.2">fuyards</w> <w n="101.3">de</w> <w n="101.4">la</w> <w n="101.5">lutte</w> ! <w n="101.6">ô</w> <w n="101.7">fuyards</w> <w n="101.8">du</w> <w n="101.9">forum</w> !</l>
						<l n="102" num="16.2"><w n="102.1">Dans</w> <w n="102.2">votre</w> <w n="102.3">église</w> <w n="102.4">alors</w>, <w n="102.5">superbe</w> <w n="102.6">de</w> <w n="102.7">lumières</w>,</l>
						<l n="103" num="16.3"><w n="103.1">Vous</w> <w n="103.2">direz</w>, <w n="103.3">en</w> <w n="103.4">chantant</w> <w n="103.5">un</w> <w n="103.6">hardi</w> <hi rend="ital"> <w n="103.7">te</w> <w n="103.8">deum</w> : </hi></l>
						<l n="104" num="16.4">« <w n="104.1">Si</w> <w n="104.2">vous</w> <w n="104.3">avez</w> <w n="104.4">vaincu</w>, <w n="104.5">c</w>’<w n="104.6">est</w> <w n="104.7">grâce</w> <w n="104.8">à</w> <w n="104.9">nos</w> <w n="104.10">prières</w> ! »</l>
					</lg>
				</div>
				<closer>
					<dateline> 15 août.</dateline>
				</closer>
			</div></body></text></TEI>