<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA11">
				<head type="number">XI</head>
				<head type="main">MÊME ÉPROUVÉE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Même</w> <w n="1.2">éprouvée</w> <w n="1.3">ainsi</w> <w n="1.4">que</w> <w n="1.5">je</w> <w n="1.6">te</w> <w n="1.7">vois</w>, <w n="1.8">ô</w> <w n="1.9">France</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">ces</w> <w n="2.3">temps</w> <w n="2.4">douloureux</w> <w n="2.5">où</w> <w n="2.6">tes</w> <w n="2.7">plus</w> <w n="2.8">jeunes</w> <w n="2.9">fils</w></l>
					<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">Vont</w> <w n="3.2">mourir</w> <w n="3.3">pour</w> <w n="3.4">ta</w> <w n="3.5">délivrance</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">lancent</w> <w n="4.3">aux</w> <w n="4.4">échos</w> <w n="4.5">les</w> <w n="4.6">suprêmes</w> <w n="4.7">défis</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Avec</w> <w n="5.2">tes</w> <w n="5.3">champs</w> <w n="5.4">brûlés</w>, <w n="5.5">tes</w> <w n="5.6">forêts</w> <w n="5.7">sépulcrales</w></l>
					<l n="6" num="2.2"><w n="6.1">Où</w> <w n="6.2">reviennent</w>, <w n="6.3">le</w> <w n="6.4">soir</w>, <w n="6.5">des</w> <w n="6.6">fantômes</w> <w n="6.7">sanglants</w>,</l>
					<l n="7" num="2.3"><space quantity="8" unit="char"></space><w n="7.1">Avec</w> <w n="7.2">tes</w> <w n="7.3">hameaux</w> <w n="7.4">pleins</w> <w n="7.5">de</w> <w n="7.6">râles</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Que</w> <w n="8.2">lèche</w> <w n="8.3">l</w>’<w n="8.4">incendie</w> <w n="8.5">aux</w> <w n="8.6">reflets</w> <w n="8.7">aveuglants</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Sanglotant</w>, <w n="9.2">par</w> <w n="9.3">moments</w>, <w n="9.4">comme</w> <w n="9.5">une</w> <w n="9.6">pauvre</w> <w n="9.7">femme</w></l>
					<l n="10" num="3.2"><w n="10.1">Qui</w> <w n="10.2">se</w> <w n="10.3">lamente</w> <w n="10.4">auprès</w> <w n="10.5">de</w> <w n="10.6">son</w> <w n="10.7">foyer</w> <w n="10.8">désert</w>,</l>
					<l n="11" num="3.3"><space quantity="8" unit="char"></space><w n="11.1">Montrant</w> <w n="11.2">le</w> <w n="11.3">couteau</w> <w n="11.4">que</w> <w n="11.5">l</w>’<w n="11.6">infâme</w></l>
					<l n="12" num="3.4"><w n="12.1">En</w> <w n="12.2">fuyant</w> <w n="12.3">a</w> <w n="12.4">laissé</w> <w n="12.5">dans</w> <w n="12.6">ton</w> <w n="12.7">flanc</w> <w n="12.8">entr</w>’<w n="12.9">ouvert</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Oui</w>, <w n="13.2">France</w> ! <w n="13.3">Même</w> <w n="13.4">en</w> <w n="13.5">deuil</w> <w n="13.6">et</w> <w n="13.7">sur</w> <w n="13.8">tant</w> <w n="13.9">de</w> <w n="13.10">victimes</w></l>
					<l n="14" num="4.2"><w n="14.1">Promenant</w> <w n="14.2">lentement</w> <w n="14.3">ton</w> <w n="14.4">regard</w> <w n="14.5">triste</w> <w n="14.6">et</w> <w n="14.7">fier</w>,</l>
					<l n="15" num="4.3"><space quantity="8" unit="char"></space><w n="15.1">Et</w> <w n="15.2">penchée</w> <w n="15.3">au</w> <w n="15.4">bord</w> <w n="15.5">des</w> <w n="15.6">abîmes</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Je</w> <w n="16.2">te</w> <w n="16.3">préfère</w> <w n="16.4">encore</w> <w n="16.5">à</w> <w n="16.6">la</w> <w n="16.7">France</w> <w n="16.8">d</w>’<w n="16.9">hier</w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">À</w> <w n="17.2">la</w> <w n="17.3">France</w> <w n="17.4">joyeuse</w>, <w n="17.5">à</w> <w n="17.6">la</w> <w n="17.7">France</w> <w n="17.8">éclatante</w></l>
					<l n="18" num="5.2"><w n="18.1">Où</w>, <w n="18.2">comme</w> <w n="18.3">des</w> <w n="18.4">serpents</w>, <w n="18.5">rampaient</w> <w n="18.6">les</w> <w n="18.7">délateurs</w>,</l>
					<l n="19" num="5.3"><space quantity="8" unit="char"></space><w n="19.1">Où</w> <w n="19.2">la</w> <w n="19.3">vénalité</w> <w n="19.4">contente</w></l>
					<l n="20" num="5.4"><w n="20.1">Mêlait</w> <w n="20.2">dans</w> <w n="20.3">son</w> <w n="20.4">bazar</w> <w n="20.5">filles</w> <w n="20.6">et</w> <w n="20.7">sénateurs</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">pourtant</w>, <w n="21.3">cette</w> <w n="21.4">France</w> <w n="21.5">à</w> <w n="21.6">voir</w> <w n="21.7">était</w> <w n="21.8">superbe</w> ;</l>
					<l n="22" num="6.2"><w n="22.1">Elle</w> <w n="22.2">gaspillait</w> <w n="22.3">l</w>’<w n="22.4">or</w>, <w n="22.5">elle</w> <w n="22.6">chantait</w> <w n="22.7">gaîment</w>,</l>
					<l n="23" num="6.3"><space quantity="8" unit="char"></space><w n="23.1">Elle</w> <w n="23.2">avait</w> <w n="23.3">au</w> <w n="23.4">front</w> <w n="23.5">une</w> <w n="23.6">gerbe</w></l>
					<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">strass</w> <w n="24.3">qui</w> <w n="24.4">remplissait</w> <w n="24.5">l</w>’<w n="24.6">œil</w> <w n="24.7">d</w>’<w n="24.8">éblouissement</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Musiques</w>, <w n="25.2">danses</w>, <w n="25.3">chants</w>, <w n="25.4">personnages</w> <w n="25.5">obliques</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Ministres</w> <w n="26.2">frauduleux</w> <w n="26.3">décorant</w> <w n="26.4">des</w> <w n="26.5">forçats</w> ;</l>
					<l n="27" num="7.3"><space quantity="8" unit="char"></space><w n="27.1">L</w>’<w n="27.2">honneur</w>, <w n="27.3">les</w> <w n="27.4">libertés</w> <w n="27.5">publiques</w></l>
					<l n="28" num="7.4"><w n="28.1">Ayant</w> <w n="28.2">pour</w> <w n="28.3">tout</w> <w n="28.4">refuge</w> <w n="28.5">ou</w> <w n="28.6">Bicêtre</w> <w n="28.7">ou</w> <w n="28.8">Mazas</w> ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">La</w> <w n="29.2">presse</w> <w n="29.3">basse</w> <w n="29.4">et</w> <w n="29.5">vile</w> <w n="29.6">ou</w> <w n="29.7">sinon</w> <w n="29.8">muselée</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Ayant</w> <w n="30.2">pour</w> <w n="30.3">noms</w> <w n="30.4">Tarbé</w>, <w n="30.5">Wolff</w>, <w n="30.6">Aurélien</w> <w n="30.7">Scholl</w>,</l>
					<l n="31" num="8.3"><space quantity="8" unit="char"></space><w n="31.1">Ainsi</w> <w n="31.2">qu</w>’<w n="31.3">une</w> <w n="31.4">grue</w> <w n="31.5">affolée</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Riant</w> <w n="32.2">de</w> <w n="32.3">voir</w> <w n="32.4">tomber</w> <w n="32.5">les</w> <w n="32.6">vaincus</w> <w n="32.7">sur</w> <w n="32.8">le</w> <w n="32.9">sol</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Fard</w>, <w n="33.2">paillettes</w>, <w n="33.3">clinquant</w>, <w n="33.4">velours</w>, <w n="33.5">robes</w> <w n="33.6">de</w> <w n="33.7">soie</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Orgie</w>, <w n="34.2">oubli</w> <w n="34.3">de</w> <w n="34.4">tout</w>, <w n="34.5">ni</w> <w n="34.6">pudeur</w>, <w n="34.7">ni</w> <w n="34.8">remord</w> ;</l>
					<l n="35" num="9.3"><space quantity="8" unit="char"></space><w n="35.1">Oui</w>, <w n="35.2">mais</w> <w n="35.3">sous</w> <w n="35.4">toute</w> <w n="35.5">cette</w> <w n="35.6">joie</w>,</l>
					<l n="36" num="9.4"><w n="36.1">On</w> <w n="36.2">sentait</w> <w n="36.3">vaguement</w> <w n="36.4">comme</w> <w n="36.5">une</w> <w n="36.6">odeur</w> <w n="36.7">de</w> <w n="36.8">mort</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">que</w> <w n="37.4">tout</w> <w n="37.5">était</w> <w n="37.6">mort</w> <w n="37.7">en</w> <w n="37.8">effet</w>, <w n="37.9">ou</w> <w n="37.10">sénile</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Et</w> <w n="38.2">rien</w> <w n="38.3">ne</w> <w n="38.4">réveillait</w> <w n="38.5">ces</w> <w n="38.6">obstinés</w> <w n="38.7">dormants</w>,</l>
					<l n="39" num="10.3"><space quantity="8" unit="char"></space><w n="39.1">Même</w> <w n="39.2">quand</w> <w n="39.3">du</w> <w n="39.4">fond</w> <w n="39.5">de</w> <w n="39.6">son</w> <w n="39.7">île</w></l>
					<l n="40" num="10.4"><w n="40.1">Victor</w> <w n="40.2">Hugo</w> <w n="40.3">faisait</w> <w n="40.4">tonner</w> <w n="40.5">les</w> <hi rend="ital"> <w n="40.6">châtiments</w> ! </hi></l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Par</w> <w n="41.2">instants</w>, <w n="41.3">un</w> <w n="41.4">vieillard</w>, <w n="41.5">pénible</w> <w n="41.6">à</w> <w n="41.7">voir</w>, <w n="41.8">teint</w> <w n="41.9">blême</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Chancelant</w>, <w n="42.2">fatigué</w>, <w n="42.3">jaune</w>, <w n="42.4">faisant</w> <w n="42.5">horreur</w>,</l>
					<l n="43" num="11.3"><space quantity="8" unit="char"></space><w n="43.1">Dans</w> <w n="43.2">ce</w> <w n="43.3">bal</w> <w n="43.4">de</w> <w n="43.5">la</w> <w n="43.6">mi</w>-<w n="43.7">carême</w></l>
					<l n="44" num="11.4"><w n="44.1">Passait</w>, <w n="44.2">et</w> <w n="44.3">l</w>’<w n="44.4">on</w> <w n="44.5">disait</w> <w n="44.6">tout</w> <w n="44.7">bas</w> : « <w n="44.8">c</w>’<w n="44.9">est</w> <w n="44.10">l</w>’<w n="44.11">empereur</w>. »</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Sa</w> <w n="45.2">femme</w> <w n="45.3">et</w> <w n="45.4">son</w> <w n="45.5">enfant</w> <w n="45.6">suivaient</w>, <w n="45.7">comme</w> <w n="45.8">des</w> <w n="45.9">ombres</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Ce</w> <w n="46.2">spectre</w> <w n="46.3">dégradé</w> <w n="46.4">qu</w>’<w n="46.5">un</w> <w n="46.6">archange</w> <w n="46.7">poursuit</w>,</l>
					<l n="47" num="12.3"><space quantity="8" unit="char"></space><w n="47.1">Et</w> <w n="47.2">les</w> <w n="47.3">chants</w> <w n="47.4">devenaient</w> <w n="47.5">plus</w> <w n="47.6">sombres</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Et</w> <w n="48.2">l</w>’<w n="48.3">on</w> <w n="48.4">sentait</w> <w n="48.5">passer</w> <w n="48.6">le</w> <w n="48.7">vent</w> <w n="48.8">froid</w> <w n="48.9">de</w> <w n="48.10">la</w> <w n="48.11">nuit</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Ô</w> <w n="49.2">France</w> ! <w n="49.3">Ta</w> <w n="49.4">douleur</w> <w n="49.5">vaut</w> <w n="49.6">mieux</w> <w n="49.7">que</w> <w n="49.8">cette</w> <w n="49.9">joie</w>.</l>
					<l n="50" num="13.2"><w n="50.1">Tu</w> <w n="50.2">saignes</w>, <w n="50.3">mais</w> <w n="50.4">tu</w> <w n="50.5">vis</w>, <w n="50.6">mais</w> <w n="50.7">tu</w> <w n="50.8">dresses</w> <w n="50.9">le</w> <w n="50.10">front</w></l>
					<l n="51" num="13.3"><space quantity="8" unit="char"></space><w n="51.1">Sous</w> <w n="51.2">l</w>’<w n="51.3">orage</w> <w n="51.4">qui</w> <w n="51.5">le</w> <w n="51.6">foudroie</w> ;</l>
					<l n="52" num="13.4"><w n="52.1">Mais</w> <w n="52.2">à</w> <w n="52.3">tes</w> <w n="52.4">ennemis</w> <w n="52.5">tu</w> <w n="52.6">rejettes</w> <w n="52.7">l</w>’<w n="52.8">affront</w> ;</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Mais</w> <w n="53.2">tu</w> <w n="53.3">comprends</w> <w n="53.4">les</w> <w n="53.5">mots</w> <w n="53.6">d</w>’<w n="53.7">honneur</w> <w n="53.8">et</w> <w n="53.9">de</w> <w n="53.10">patrie</w> ;</l>
					<l n="54" num="14.2"><w n="54.1">Ton</w> <w n="54.2">courage</w> <w n="54.3">s</w>’<w n="54.4">accroît</w> <w n="54.5">de</w> <w n="54.6">tous</w> <w n="54.7">les</w> <w n="54.8">maux</w> <w n="54.9">soufferts</w> ;</l>
					<l n="55" num="14.3"><space quantity="8" unit="char"></space><w n="55.1">D</w>’<w n="55.2">autant</w> <w n="55.3">plus</w> <w n="55.4">forte</w> <w n="55.5">que</w> <w n="55.6">meurtrie</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Tu</w> <w n="56.2">fais</w> <w n="56.3">une</w> <w n="56.4">arme</w> <w n="56.5">avec</w> <w n="56.6">les</w> <w n="56.7">débris</w> <w n="56.8">de</w> <w n="56.9">tes</w> <w n="56.10">fers</w> !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">L</w>’<w n="57.2">épreuve</w> <w n="57.3">sera</w> <w n="57.4">courte</w>. <w n="57.5">Un</w> <w n="57.6">nouveau</w> <w n="57.7">sang</w> <w n="57.8">afflue</w></l>
					<l n="58" num="15.2"><w n="58.1">Dans</w> <w n="58.2">tes</w> <w n="58.3">veines</w>, <w n="58.4">ô</w> <w n="58.5">France</w> ! <w n="58.6">Un</w> <w n="58.7">sang</w> <w n="58.8">pur</w> <w n="58.9">et</w> <w n="58.10">vermeil</w>.</l>
					<l n="59" num="15.3"><space quantity="8" unit="char"></space><w n="59.1">Tes</w> <w n="59.2">fils</w> <w n="59.3">ont</w> <w n="59.4">l</w>’<w n="59.5">âme</w> <w n="59.6">résolue</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Et</w> <w n="60.2">sauront</w> <w n="60.3">triompher</w> <w n="60.4">demain</w>, <w n="60.5">au</w> <w n="60.6">grand</w> <w n="60.7">soleil</w> !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Ô</w> <w n="61.2">France</w> ! <w n="61.3">Entends</w> <w n="61.4">chanter</w> <w n="61.5">les</w> <w n="61.6">voix</w> <w n="61.7">libératrices</w> ;</l>
					<l n="62" num="16.2"><w n="62.1">L</w>’<w n="62.2">avenir</w> <w n="62.3">est</w> <w n="62.4">prochain</w> <w n="62.5">qui</w>, <w n="62.6">d</w>’<w n="62.7">un</w> <w n="62.8">doigt</w> <w n="62.9">enchanté</w>,</l>
					<l n="63" num="16.3"><space quantity="8" unit="char"></space><w n="63.1">Ferme</w> <w n="63.2">tes</w> <w n="63.3">nobles</w> <w n="63.4">cicatrices</w></l>
					<l n="64" num="16.4"><w n="64.1">D</w>’<w n="64.2">où</w> <w n="64.3">jaillira</w> <w n="64.4">pour</w> <w n="64.5">tous</w> <w n="64.6">la</w> <w n="64.7">jeune</w> <w n="64.8">liberté</w> ;</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Et</w> <w n="65.2">si</w>, <w n="65.3">que</w> <w n="65.4">ce</w> <w n="65.5">penser</w> <w n="65.6">sur</w> <w n="65.7">les</w> <w n="65.8">lâches</w> <w n="65.9">retombe</w> !</l>
					<l n="66" num="17.2"><w n="66.1">On</w> <w n="66.2">doit</w> <w n="66.3">voir</w> <w n="66.4">brusquement</w> <w n="66.5">s</w>’<w n="66.6">éteindre</w> <w n="66.7">ton</w> <w n="66.8">flambeau</w>,</l>
					<l n="67" num="17.3"><space quantity="8" unit="char"></space><w n="67.1">Ô</w> <w n="67.2">France</w> ! <w n="67.3">Descends</w> <w n="67.4">dans</w> <w n="67.5">la</w> <w n="67.6">tombe</w>,</l>
					<l n="68" num="17.4"><w n="68.1">Et</w> <w n="68.2">meurs</w> <w n="68.3">libre</w> ! <w n="68.4">Ton</w> <w n="68.5">sort</w> <w n="68.6">n</w>’<w n="68.7">en</w> <w n="68.8">sera</w> <w n="68.9">pas</w> <w n="68.10">moins</w> <w n="68.11">beau</w> !</l>
				</lg>
				<closer>
					<dateline> 5 octobre.</dateline>
				</closer>
			</div></body></text></TEI>