<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Fer rouge</title>
				<title type="medium">Édition électronique</title>
				<author key="GLA">
					<name>
						<forename>Albert</forename>
						<surname>GLATIGNY</surname>
					</name>
					<date from="1839" to="1873">1839-1873</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1218 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GLA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Fer rouge</title>
						<author>Albert Glatigny</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L928</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Fer rouge</title>
								<author>Albert Glatigny</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54519653</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Poulet-Malassis</publisher>
									<date when="1870">1870</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1870">1870</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Le formatage strophique a été rétabli.</p>
				<p>Les majuscules en début de vers ont été restituées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GLA4">
				<head type="number">IV</head>
				<head type="main">LÂCHE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Un tel homme suffit pour qu’un siècle pourrisse.</quote>
							<bibl>
								<name>Victor Hugo</name>, <hi rend="ital">Légende des siècles</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">On</w> <w n="1.2">ne</w> <w n="1.3">le</w> <w n="1.4">croyait</w> <w n="1.5">pas</w> <w n="1.6">si</w> <w n="1.7">lâche</w>, <w n="1.8">en</w> <w n="1.9">vérité</w> !</l>
					<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">le</w> <w n="2.3">savait</w> <w n="2.4">menteur</w>, <w n="2.5">voleur</w>, <w n="2.6">fourbe</w>, <w n="2.7">effronté</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Assassin</w>. <w n="3.2">On</w> <w n="3.3">n</w>’<w n="3.4">avait</w> <w n="3.5">pas</w> <w n="3.6">oublié</w> <w n="3.7">ses</w> <w n="3.8">crimes</w>.</l>
					<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">le</w> <w n="4.3">savait</w> <w n="4.4">expert</w> <w n="4.5">en</w> <w n="4.6">ces</w> <w n="4.7">louches</w> <w n="4.8">escrimes</w></l>
					<l n="5" num="1.5"><w n="5.1">Des</w> <w n="5.2">trahisons</w> <w n="5.3">par</w> <w n="5.4">qui</w> <w n="5.5">l</w>’<w n="5.6">on</w> <w n="5.7">se</w> <w n="5.8">sent</w> <w n="5.9">enlacé</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Bien</w> <w n="6.2">que</w>, <w n="6.3">Morny</w> <w n="6.4">défunt</w>, <w n="6.5">il</w> <w n="6.6">eût</w> <w n="6.7">parfois</w> <w n="6.8">baissé</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">bien</w> <w n="7.3">qu</w>’<w n="7.4">il</w> <w n="7.5">se</w> <w n="7.6">montrât</w> <w n="7.7">un</w> <w n="7.8">peu</w> <w n="7.9">moins</w> <w n="7.10">fataliste</w>,</l>
					<l n="8" num="1.8"><w n="8.1">N</w>’<w n="8.2">étant</w> <w n="8.3">plus</w> <w n="8.4">soutenu</w> <w n="8.5">par</w> <w n="8.6">ce</w> <w n="8.7">vaudevilliste</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Il</w> <w n="9.2">donnait</w> <w n="9.3">sa</w> <w n="9.4">parole</w> <w n="9.5">assez</w> <w n="9.6">impudemment</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Il</w> <w n="10.2">ne</w> <w n="10.3">reculait</w> <w n="10.4">pas</w> <w n="10.5">devant</w> <w n="10.6">le</w> <w n="10.7">faux</w> <w n="10.8">serment</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">les</w> <w n="11.3">jours</w> <w n="11.4">où</w> <w n="11.5">Ricord</w> <w n="11.6">lui</w> <w n="11.7">disait</w> : « <w n="11.8">le</w> <w n="11.9">mercure</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Sire</w>, <w n="12.2">accélérera</w> <w n="12.3">promptement</w> <w n="12.4">votre</w> <w n="12.5">cure</w>, »</l>
					<l n="13" num="1.13"><w n="13.1">Il</w> <w n="13.2">disait</w> <w n="13.3">assez</w> <w n="13.4">bien</w>, <w n="13.5">de</w> <w n="13.6">l</w>’<w n="13.7">ordre</w> : « <w n="13.8">j</w>’<w n="13.9">en</w> <w n="13.10">réponds</w> ! »</l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">Rouher</w>, <w n="14.3">Ollivier</w>, <w n="14.4">tout</w> <w n="14.5">le</w> <w n="14.6">clan</w> <w n="14.7">de</w> <w n="14.8">fripons</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Se</w> <w n="15.2">sentaient</w> <w n="15.3">plus</w> <w n="15.4">d</w>’<w n="15.5">aplomb</w> <w n="15.6">devant</w> <w n="15.7">son</w> <w n="15.8">assurance</w>.</l>
					<l n="16" num="1.16"><w n="16.1">Ils</w> <w n="16.2">le</w> <w n="16.3">flairaient</w>. <w n="16.4">L</w>’<w n="16.5">odeur</w> <w n="16.6">n</w>’<w n="16.7">en</w> <w n="16.8">semblait</w> <w n="16.9">pas</w> <w n="16.10">trop</w> <w n="16.11">rance</w>.</l>
					<l n="17" num="1.17"><w n="17.1">On</w> <w n="17.2">pouvait</w> <w n="17.3">supposer</w> <w n="17.4">encor</w> <w n="17.5">dans</w> <w n="17.6">ce</w> <w n="17.7">vieux</w> <w n="17.8">cœur</w></l>
					<l n="18" num="1.18"><w n="18.1">Un</w> <w n="18.2">restant</w> <w n="18.3">d</w>’<w n="18.4">énergie</w>, <w n="18.5">un</w> <w n="18.6">soupçon</w> <w n="18.7">de</w> <w n="18.8">vigueur</w>.</l>
					<l n="19" num="1.19"><w n="19.1">On</w> <w n="19.2">a</w> <w n="19.3">vu</w> <w n="19.4">des</w> <w n="19.5">brigands</w> <w n="19.6">courageux</w>, <w n="19.7">et</w> <w n="19.8">Cartouche</w></l>
					<l n="20" num="1.20"><w n="20.1">Se</w> <w n="20.2">défendait</w> <w n="20.3">avec</w> <w n="20.4">une</w> <w n="20.5">âpreté</w> <w n="20.6">farouche</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Mandrin</w> <w n="21.2">faisait</w> <w n="21.3">payer</w> <w n="21.4">cher</w> <w n="21.5">sa</w> <w n="21.6">peau</w> ; <w n="21.7">les</w> <w n="21.8">bandits</w></l>
					<l n="22" num="1.22"><w n="22.1">De</w> <w n="22.2">Corse</w> <w n="22.3">sont</w> <w n="22.4">des</w> <w n="22.5">gueux</w> <w n="22.6">atroces</w> <w n="22.7">mais</w> <w n="22.8">hardis</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Qui</w> <w n="23.2">crèvent</w> <w n="23.3">dans</w> <w n="23.4">les</w> <w n="23.5">trous</w> <w n="23.6">des</w> <w n="23.7">rochers</w> <w n="23.8">mais</w> <w n="23.9">les</w> <w n="23.10">armes</w></l>
					<l n="24" num="1.24"><w n="24.1">À</w> <w n="24.2">la</w> <w n="24.3">main</w>, <w n="24.4">et</w> <w n="24.5">faisant</w> <w n="24.6">reculer</w> <w n="24.7">les</w> <w n="24.8">gendarmes</w>.</l>
					<l n="25" num="1.25"><w n="25.1">Eh</w> <w n="25.2">bien</w> ! <w n="25.3">On</w> <w n="25.4">se</w> <w n="25.5">trompait</w> : <w n="25.6">il</w> <w n="25.7">est</w> <w n="25.8">lâche</w>. <w n="25.9">Il</w> <w n="25.10">se</w> <w n="25.11">rend</w></l>
					<l n="26" num="1.26"><w n="26.1">Sans</w> <w n="26.2">avoir</w> <w n="26.3">combattu</w>, <w n="26.4">furtif</w>, <w n="26.5">honteux</w>, <w n="26.6">foirant</w></l>
					<l n="27" num="1.27"><w n="27.1">Dans</w> <w n="27.2">son</w> <w n="27.3">habit</w> <w n="27.4">doré</w> <w n="27.5">d</w>’<w n="27.6">écuyer</w> <w n="27.7">quadrumane</w>.</l>
					<l n="28" num="1.28"><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">un</w> <w n="28.4">lâche</w>. <w n="28.5">Voilà</w> <w n="28.6">ce</w> <w n="28.7">qu</w>’<w n="28.8">il</w> <w n="28.9">est</w>. <w n="28.10">Il</w> <w n="28.11">émane</w></l>
					<l n="29" num="1.29"><w n="29.1">Des</w> <w n="29.2">miasmes</w> <w n="29.3">de</w> <w n="29.4">lui</w> <w n="29.5">trahissant</w> <w n="29.6">le</w> <w n="29.7">poltron</w>.</l>
					<l n="30" num="1.30"><w n="30.1">C</w>’<w n="30.2">est</w> <w n="30.3">un</w> <w n="30.4">lâche</w>. <w n="30.5">Son</w> <w n="30.6">air</w>, <w n="30.7">sa</w> <w n="30.8">face</w> <w n="30.9">de</w> <w n="30.10">citron</w></l>
					<l n="31" num="1.31"><w n="31.1">Le</w> <w n="31.2">disaient</w>. <w n="31.3">Il</w> <w n="31.4">n</w>’<w n="31.5">avait</w> <w n="31.6">que</w> <w n="31.7">cela</w> <w n="31.8">de</w> <w n="31.9">sincère</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Mais</w> <w n="32.2">il</w> <w n="32.3">est</w> <w n="32.4">lâche</w> <w n="32.5">plus</w> <w n="32.6">qu</w>’<w n="32.7">il</w> <w n="32.8">n</w>’<w n="32.9">était</w> <w n="32.10">nécessaire</w>.</l>
					<l n="33" num="1.33"><w n="33.1">Ceux</w> <w n="33.2">que</w> <w n="33.3">son</w> <w n="33.4">nom</w> <w n="33.5">dupait</w> <w n="33.6">encor</w> <w n="33.7">sont</w> <w n="33.8">confondus</w>.</l>
					<l n="34" num="1.34"><w n="34.1">Les</w> <w n="34.2">aigles</w> <w n="34.3">du</w> <w n="34.4">premier</w> <w n="34.5">Bonaparte</w>, <w n="34.6">éperdus</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Désertent</w> <w n="35.2">la</w> <w n="35.3">colonne</w> <w n="35.4">et</w> <w n="35.5">cherchent</w> <w n="35.6">une</w> <w n="35.7">mare</w>.</l>
					<l n="36" num="1.36"><w n="36.1">Napoléon</w>, <w n="36.2">ce</w> <w n="36.3">bruit</w> <w n="36.4">d</w>’<w n="36.5">airain</w>, <w n="36.6">ce</w> <w n="36.7">tintamarre</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Ce</w> <w n="37.2">fracas</w>, <w n="37.3">aujourd</w>’<w n="37.4">hui</w>, <w n="37.5">répond</w> <w n="37.6">à</w> <w n="37.7">lâcheté</w> !</l>
					<l n="38" num="1.38"><w n="38.1">Les</w> <hi rend="ital"> <w n="38.2">châtiments</w>, </hi> <w n="38.3">ce</w> <w n="38.4">livre</w> <w n="38.5">où</w> <w n="38.6">le</w> <w n="38.7">vers</w> <w n="38.8">irrité</w></l>
					<l n="39" num="1.39"><w n="39.1">Devient</w> <w n="39.2">un</w> <w n="39.3">justicier</w>, <w n="39.4">et</w> <w n="39.5">prend</w> <w n="39.6">cet</w> <w n="39.7">affreux</w> <w n="39.8">drôle</w>,</l>
					<l n="40" num="1.40"><w n="40.1">Et</w> <w n="40.2">le</w> <w n="40.3">cingle</w> <w n="40.4">au</w> <w n="40.5">visage</w>, <w n="40.6">et</w> <w n="40.7">le</w> <w n="40.8">marque</w> <w n="40.9">à</w> <w n="40.10">l</w>’<w n="40.11">épaule</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Les</w> <hi rend="ital"> <w n="41.2">châtiments</w> </hi> <w n="41.3">se</w> <w n="41.4">sont</w> <w n="41.5">trompés</w>. <w n="41.6">Non</w>, <w n="41.7">ce</w> <w n="41.8">n</w>’<w n="41.9">est</w> <w n="41.10">pas</w></l>
					<l n="42" num="1.42"><w n="42.1">La</w> <w n="42.2">nuit</w> <w n="42.3">où</w>, <w n="42.4">retiré</w> <w n="42.5">brusquement</w> <w n="42.6">du</w> <w n="42.7">trépas</w>,</l>
					<l n="43" num="1.43"><w n="43.1">L</w>’<w n="43.2">empereur</w> <w n="43.3">contempla</w> <w n="43.4">l</w>’<w n="43.5">ignoble</w> <w n="43.6">mascarade</w></l>
					<l n="44" num="1.44"><w n="44.1">Dans</w> <w n="44.2">laquelle</w> <w n="44.3">il</w> <w n="44.4">faisait</w> <w n="44.5">lui</w>-<w n="44.6">même</w> <w n="44.7">la</w> <w n="44.8">parade</w>,</l>
					<l n="45" num="1.45"><w n="45.1">Que</w> <w n="45.2">l</w>’<w n="45.3">expiation</w> <w n="45.4">commença</w>. <w n="45.5">Ce</w> <w n="45.6">n</w>’<w n="45.7">était</w></l>
					<l n="46" num="1.46"><w n="46.1">Pas</w> <w n="46.2">suffisant</w> <w n="46.3">encor</w>, <w n="46.4">pas</w> <w n="46.5">complet</w>. <w n="46.6">Il</w> <w n="46.7">restait</w></l>
					<l n="47" num="1.47"><w n="47.1">Une</w> <w n="47.2">coupe</w> <w n="47.3">à</w> <w n="47.4">vider</w>, <w n="47.5">plus</w> <w n="47.6">lourde</w> <w n="47.7">et</w> <w n="47.8">plus</w> <w n="47.9">amère</w>,</l>
					<l n="48" num="1.48"><w n="48.1">Pour</w> <w n="48.2">expier</w> <w n="48.3">le</w> <w n="48.4">crime</w> <w n="48.5">immense</w> <w n="48.6">de</w> <w n="48.7">brumaire</w>.</l>
					<l n="49" num="1.49"><w n="49.1">Il</w> <w n="49.2">fallait</w> <w n="49.3">que</w> <w n="49.4">ton</w> <w n="49.5">nom</w>, <w n="49.6">ô</w> <w n="49.7">vainqueur</w> <w n="49.8">d</w>’<w n="49.9">Iéna</w> !</l>
					<l n="50" num="1.50"><w n="50.1">Ton</w> <w n="50.2">nom</w> <w n="50.3">sanglant</w> <w n="50.4">mais</w> <w n="50.5">fier</w>, <w n="50.6">ton</w> <w n="50.7">nom</w> <w n="50.8">qui</w> <w n="50.9">rayonna</w></l>
					<l n="51" num="1.51"><w n="51.1">Avec</w> <w n="51.2">l</w>’<w n="51.3">éclat</w> <w n="51.4">d</w>’<w n="51.5">un</w> <w n="51.6">astre</w> <w n="51.7">effrayant</w> <w n="51.8">sur</w> <w n="51.9">le</w> <w n="51.10">monde</w>,</l>
					<l n="52" num="1.52"><w n="52.1">Roulât</w> <w n="52.2">jusqu</w>’<w n="52.3">au</w> <w n="52.4">dernier</w> <w n="52.5">échelon</w> <w n="52.6">de</w> <w n="52.7">l</w>’<w n="52.8">immonde</w> ;</l>
					<l n="53" num="1.53"><w n="53.1">Il</w> <w n="53.2">fallait</w> <w n="53.3">que</w> <w n="53.4">ce</w> <w n="53.5">nom</w> <w n="53.6">aux</w> <w n="53.7">échos</w> <w n="53.8">triomphants</w>,</l>
					<l n="54" num="1.54"><w n="54.1">Fît</w> <w n="54.2">rire</w> <w n="54.3">de</w> <w n="54.4">pitié</w> <w n="54.5">les</w> <w n="54.6">tout</w> <w n="54.7">petits</w> <w n="54.8">enfants</w>.</l>
					<l n="55" num="1.55"><w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">fait</w> ! <w n="55.4">Napoléon</w>, <w n="55.5">cela</w> <w n="55.6">veut</w> <w n="55.7">dire</w> <w n="55.8">lâche</w> !</l>
					<l n="56" num="1.56"><w n="56.1">Ces</w> <w n="56.2">combats</w>, <w n="56.3">ces</w> <w n="56.4">travaux</w> <w n="56.5">accomplis</w> <w n="56.6">sans</w> <w n="56.7">relâche</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Cette</w> <w n="57.2">gloire</w> <w n="57.3">aboutit</w> <w n="57.4">aux</w> <w n="57.5">hontes</w> <w n="57.6">de</w> <w n="57.7">Sedan</w> !</l>
					<l n="58" num="1.58"><w n="58.1">Lâche</w> ! <w n="58.2">Ce</w> <w n="58.3">mot</w> <w n="58.4">t</w>’<w n="58.5">étreint</w>, <w n="58.6">implacable</w> <w n="58.7">carcan</w> ;</l>
					<l n="59" num="1.59"><w n="59.1">Le</w> <w n="59.2">gueux</w> <w n="59.3">qui</w> <w n="59.4">sur</w> <w n="59.5">ton</w> <w n="59.6">nom</w> <w n="59.7">indigné</w> <w n="59.8">caracole</w>,</l>
					<l n="60" num="1.60"><w n="60.1">Est</w> <w n="60.2">un</w> <w n="60.3">lâche</w>, <w n="60.4">entends</w>-<w n="60.5">tu</w>, <w n="60.6">fauve</w> <w n="60.7">soldat</w> <w n="60.8">d</w>’<w n="60.9">Arcole</w> ?</l>
					<l n="61" num="1.61"><w n="61.1">Il</w> <w n="61.2">se</w> <w n="61.3">rend</w>, <w n="61.4">il</w> <w n="61.5">se</w> <w n="61.6">vend</w>, <w n="61.7">il</w> <w n="61.8">vend</w> <w n="61.9">la</w> <w n="61.10">France</w>, <w n="61.11">il</w> <w n="61.12">vend</w></l>
					<l n="62" num="1.62"><w n="62.1">Ta</w> <w n="62.2">défroque</w> <w n="62.3">d</w>’<w n="62.4">honneur</w> <w n="62.5">et</w> <w n="62.6">la</w> <w n="62.7">disperse</w> <w n="62.8">au</w> <w n="62.9">vent</w>.</l>
					<l n="63" num="1.63"><w n="63.1">C</w>’<w n="63.2">est</w> <w n="63.3">un</w> <w n="63.4">lâche</w>. <w n="63.5">Il</w> <w n="63.6">pouvait</w> <w n="63.7">mourir</w>, <w n="63.8">mais</w> <w n="63.9">c</w>’<w n="63.10">est</w> <w n="63.11">un</w> <w n="63.12">lâche</w> !</l>
					<l n="64" num="1.64"><w n="64.1">Pourvu</w> <w n="64.2">qu</w>’<w n="64.3">il</w> <w n="64.4">vive</w> <w n="64.5">et</w> <w n="64.6">soit</w> <w n="64.7">riche</w>, <w n="64.8">rien</w> <w n="64.9">ne</w> <w n="64.10">le</w> <w n="64.11">fâche</w>.</l>
					<l n="65" num="1.65"><w n="65.1">Et</w> <w n="65.2">pendant</w> <w n="65.3">qu</w>’<w n="65.4">arrachant</w> <w n="65.5">la</w> <w n="65.6">croix</w> <w n="65.7">de</w> <w n="65.8">ton</w> <w n="65.9">plastron</w>,</l>
					<l n="66" num="1.66"><w n="66.1">Tu</w> <w n="66.2">pleures</w> <w n="66.3">d</w>’<w n="66.4">avoir</w> <w n="66.5">pu</w> <w n="66.6">connaître</w> <w n="66.7">ce</w> <w n="66.8">poltron</w>,</l>
					<l n="67" num="1.67"><w n="67.1">Lui</w>, <w n="67.2">gai</w>, <w n="67.3">frottant</w> <w n="67.4">ses</w> <w n="67.5">yeux</w> <w n="67.6">fatigués</w> <w n="67.7">de</w> <w n="67.8">ribotes</w>,</l>
					<l n="68" num="1.68"><w n="68.1">Sourit</w> <w n="68.2">à</w> <w n="68.3">son</w> <w n="68.4">vainqueur</w>, <w n="68.5">et</w> <w n="68.6">lui</w> <w n="68.7">cire</w> <w n="68.8">ses</w> <w n="68.9">bottes</w> !</l>
				</lg>
				<closer>
					<dateline> 14 septembre.</dateline>
				</closer>
			</div></body></text></TEI>