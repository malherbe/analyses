<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">CHANSONS JOYEUSES</title>
				<title type="sub_2">MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
				<title type="sub_1">NOUVELLE ÉDITION</title>
				<title type="medium">Édition électronique</title>
				<author key="COL">
					<name>
						<forename>Charles</forename>
						<surname>COLLÉ</surname>
					</name>
					<date from="1709" to="1783">1709-1783</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>851 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">COL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>CHANSONS JOYEUSES</title>
						<author>Charles Collé</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="BNF">ark:/12148/bpt6k1073478c</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>CHANSONS JOYEUSES</title>
								<title>MISES AU JOUR PAR UN ANE-ONYME, ONISSIME</title>
								<author>Charles Collé</author>
								<edition>NOUVELLE ÉDITION</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k1073478c?rk=107296;4</idno>
								<imprint>
									<pubPlace>A Paris ; à Londres, et à Ispahan seulement</pubPlace>
									<publisher>De l’Imprimerie de l’Académie de Troyes</publisher>
									<date when="1765">1765</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>RECUEIL COMPLET DES CHANSONS DE COLLÉ</title>
						<author>Charles Collé</author>
						<edition>NOUVELLE ÉDITION , REVUE ET CORRIGÉE</edition>
						<idno type="URI">https://books.google.fr/books?id=W1U7AAAAcAAJ</idno>
						<imprint>
							<pubPlace>HAMBOURG ET PARIS</pubPlace>
							<publisher>CHEZ LES PRINCIPAUX LIBRAIRES</publisher>
							<date when="1864">1864</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1765">1765</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de fin de page ont été reportées en fin de poème/</p>
				<correction>
					<p></p>
				</correction>
				<normalization>
					<p> points</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2023-06-23" who="RR">Validation XML-TEI (TEI_Corpus_Malherbe_1.6.xsd)</change>
				<change when="2023-06-23" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="COL28">
				<head type="main">CHANSON DE PARADE,</head>
				<head type="sub_1">Chantée par Gilles le Niais.</head>
				<head type="tune">Air : Vive les Grecs.</head>
				<head type="tune">Noté, N°. 20.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">SI</w> <w n="1.2">j</w>’<w n="1.3">sçavois</w> <w n="1.4">tromper</w> <w n="1.5">les</w> <w n="1.6">Meres</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="6"></space><w n="2.1">Et</w> <w n="2.2">les</w> <w n="2.3">Argus</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Si</w> <w n="3.2">j</w>’<w n="3.3">sçavois</w> <w n="3.4">d</w>’<w n="3.5">tous</w> <w n="3.6">mes</w> <w n="3.7">Comperes</w></l>
					<l n="4" num="1.4"><space unit="char" quantity="6"></space><w n="4.1">Fair</w>’ <w n="4.2">des</w> <w n="4.3">Cocus</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Diroit</w>-<w n="5.2">on</w> <w n="5.3">que</w> <w n="5.4">je</w> <w n="5.5">serais</w> <ref target="1" type="noteAnchor">(1)</ref></l>
					<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">Gilles</w> <w n="6.2">le</w> <w n="6.3">Niais</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">SI</w> <w n="7.2">j</w>’<w n="7.3">sçavois</w> <w n="7.4">des</w> <w n="7.5">Femm</w>’ <w n="7.6">prudentes</w>,</l>
					<l n="8" num="2.2"><space unit="char" quantity="6"></space><w n="8.1">Jouir</w> <w n="8.2">sans</w> <w n="8.3">bruit</w> ;</l>
					<l n="9" num="2.3"><w n="9.1">SI</w> <w n="9.2">j</w>’<w n="9.3">sçavois</w> <w n="9.4">des</w> <w n="9.5">innocentes</w></l>
					<l n="10" num="2.4"><space unit="char" quantity="6"></space><w n="10.1">Ouvrir</w> <w n="10.2">l</w>’<w n="10.3">esprit</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Diroit</w>-<w n="11.2">on</w> <w n="11.3">que</w> <w n="11.4">je</w> <w n="11.5">serais</w></l>
					<l n="12" num="2.6"><space unit="char" quantity="4"></space><w n="12.1">Gilles</w> <w n="12.2">le</w> <w n="12.3">Niais</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Si</w> <w n="13.2">j</w>’<w n="13.3">sçavois</w> <w n="13.4">ben</w> <w n="13.5">d</w>’<w n="13.6">autres</w> <w n="13.7">choses</w> ;</l>
					<l n="14" num="3.2"><space unit="char" quantity="6"></space><w n="14.1">Qui</w> <w n="14.2">font</w> <w n="14.3">plaisir</w> ;</l>
					<l n="15" num="3.3"><w n="15.1">Si</w> <w n="15.2">j</w>’<w n="15.3">sçavois</w> <w n="15.4">cueillir</w> <w n="15.5">les</w> <w n="15.6">Roses</w>,</l>
					<l n="16" num="3.4"><space unit="char" quantity="6"></space><w n="16.1">Sans</w> <w n="16.2">les</w> <w n="16.3">flétrir</w> ;</l>
					<l n="17" num="3.5"><w n="17.1">Diroit</w>-<w n="17.2">on</w> <w n="17.3">que</w> <w n="17.4">je</w> <w n="17.5">ferais</w></l>
					<l n="18" num="3.6"><space unit="char" quantity="4"></space><w n="18.1">Gilles</w> <w n="18.2">le</w> <w n="18.3">Niais</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Si</w> <w n="19.2">j</w>’<w n="19.3">sçavois</w> <w n="19.4">prendre</w> <w n="19.5">les</w> <w n="19.6">Femmes</w>,</l>
					<l n="20" num="4.2"><space unit="char" quantity="6"></space><w n="20.1">Par</w> <w n="20.2">mes</w> <w n="20.3">exploits</w> ;</l>
					<l n="21" num="4.3"><w n="21.1">Si</w> <w n="21.2">j</w>’<w n="21.3">sçavois</w> <w n="21.4">compter</w>, <w n="21.5">mes</w> <w n="21.6">Dames</w>,</l>
					<l n="22" num="4.4"><space unit="char" quantity="6"></space><w n="22.1">Par</w> <w n="22.2">mes</w> <w n="22.3">dix</w> <w n="22.4">doigts</w> ;</l>
					<l n="23" num="4.5"><w n="23.1">Diroit</w>-<w n="23.2">on</w> <w n="23.3">que</w> <w n="23.4">je</w> <w n="23.5">serais</w></l>
					<l n="24" num="4.6"><space unit="char" quantity="4"></space><w n="24.1">Gilles</w> <w n="24.2">le</w> <w n="24.3">Niais</w> ?</l>
				</lg>
				<ab type="star">❉</ab>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">TOUT</w> <w n="25.2">ce</w> <w n="25.3">que</w> <w n="25.4">je</w> <w n="25.5">viens</w> <w n="25.6">de</w> <w n="25.7">dire</w></l>
					<l n="26" num="5.2"><space unit="char" quantity="6"></space><w n="26.1">Dans</w> <w n="26.2">ces</w> <w n="26.3">Couplets</w>,</l>
					<l n="27" num="5.3"><w n="27.1">N</w>’<w n="27.2">a</w> <w n="27.3">point</w> <w n="27.4">été</w> <w n="27.5">dit</w> <w n="27.6">pour</w> <w n="27.7">rire</w>,</l>
					<l n="28" num="5.4"><space unit="char" quantity="6"></space><w n="28.1">Ce</w> <w n="28.2">sont</w> <w n="28.3">mes</w> <w n="28.4">faits</w> ;</l>
					<l n="29" num="5.5"><w n="29.1">Peut</w>-<w n="29.2">on</w> <w n="29.3">m</w>’<w n="29.4">appeller</w> <w n="29.5">après</w></l>
					<l n="30" num="5.6"><space unit="char" quantity="4"></space><w n="30.1">Gilles</w> <w n="30.2">le</w> <w n="30.3">Niais</w> ?</l>
				</lg>
				<closer>
					<note type="footnote" id="(1)">On remarquera que ce mot est écrit <lb></lb>conformement à l’Orthographe de M. de Voltaire.</note>
				</closer>
			</div></body></text></TEI>