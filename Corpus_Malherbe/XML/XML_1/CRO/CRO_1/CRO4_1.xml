<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS PERPÉTUELLES</head><div type="poem" key="CRO4">
					<head type="main">L’Orgue</head>
					<opener>
						<salute>A André Gill</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Sous</w> <w n="1.2">un</w> <w n="1.3">roi</w> <w n="1.4">d</w>’<w n="1.5">Allemagne</w>, <w n="1.6">ancien</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Est</w> <w n="2.2">mort</w> <w n="2.3">Gottlieb</w> <w n="2.4">le</w> <w n="2.5">musicien</w>.</l>
						<l n="3" num="1.3"><space quantity="4" unit="char"></space><w n="3.1">Un</w> <w n="3.2">l</w>’<w n="3.3">a</w> <w n="3.4">cloué</w> <w n="3.5">sous</w> <w n="3.6">les</w> <w n="3.7">planches</w>.</l>
						<l rhyme="none" n="4" num="1.4"><space quantity="12" unit="char"></space><w n="4.1">Hou</w> ! <w n="4.2">hou</w> ! <w n="4.3">hou</w> !</l>
						<l n="5" num="1.5"><space quantity="4" unit="char"></space><w n="5.1">Le</w> <w n="5.2">vent</w> <w n="5.3">souffle</w> <w n="5.4">dans</w> <w n="5.5">les</w> <w n="5.6">branches</w>.</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">Il</w> <w n="6.2">est</w> <w n="6.3">mort</w> <w n="6.4">pour</w> <w n="6.5">avoir</w> <w n="6.6">aimé</w></l>
						<l n="7" num="2.2"><w n="7.1">La</w> <w n="7.2">petite</w> <w n="7.3">Rose</w>-<w n="7.4">de</w>-<w n="7.5">Mai</w>.</l>
						<l n="8" num="2.3"><space quantity="4" unit="char"></space><w n="8.1">Les</w> <w n="8.2">filles</w> <w n="8.3">ne</w> <w n="8.4">sont</w> <w n="8.5">pas</w> <w n="8.6">franches</w>.</l>
						<l rhyme="none" n="9" num="2.4"><space quantity="12" unit="char"></space><w n="9.1">Hou</w> ! <w n="9.2">hou</w> ! <w n="9.3">hou</w> !</l>
						<l n="10" num="2.5"><space quantity="4" unit="char"></space><w n="10.1">Le</w> <w n="10.2">vent</w> <w n="10.3">souffle</w> <w n="10.4">dans</w> <w n="10.5">les</w> <w n="10.6">branches</w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">Elle</w> <w n="11.2">s</w>’<w n="11.3">est</w> <w n="11.4">mariée</w>, <w n="11.5">un</w> <w n="11.6">jour</w>,</l>
						<l n="12" num="3.2"><w n="12.1">Avec</w> <w n="12.2">un</w> <w n="12.3">autre</w>, <w n="12.4">sans</w> <w n="12.5">amour</w>.</l>
						<l n="13" num="3.3"><space quantity="4" unit="char"></space>« <w n="13.1">Repassez</w> <w n="13.2">les</w> <w n="13.3">robes</w> <w n="13.4">blanches</w> ! »</l>
						<l rhyme="none" n="14" num="3.4"><space quantity="12" unit="char"></space><w n="14.1">Hou</w> ! <w n="14.2">hou</w> ! <w n="14.3">hou</w> !</l>
						<l n="15" num="3.5"><space quantity="4" unit="char"></space><w n="15.1">Le</w> <w n="15.2">vent</w> <w n="15.3">souffle</w> <w n="15.4">dans</w> <w n="15.5">les</w> <w n="15.6">branches</w>.</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">Quand</w> <w n="16.2">à</w> <w n="16.3">l</w>’<w n="16.4">église</w> <w n="16.5">ils</w> <w n="16.6">sont</w> <w n="16.7">venus</w>,</l>
						<l n="17" num="4.2"><w n="17.1">Gottlieb</w> <w n="17.2">à</w> <w n="17.3">l</w>’<w n="17.4">orgue</w> <w n="17.5">n</w>’<w n="17.6">était</w> <w n="17.7">plus</w>,</l>
						<l n="18" num="4.3"><space quantity="4" unit="char"></space><w n="18.1">Comme</w> <w n="18.2">les</w> <w n="18.3">autres</w> <w n="18.4">dimanches</w>.</l>
						<l rhyme="none" n="19" num="4.4"><space quantity="12" unit="char"></space><w n="19.1">Hou</w> ! <w n="19.2">hou</w> !’<w n="19.3">hou</w> !</l>
						<l n="20" num="4.5"><space quantity="4" unit="char"></space><w n="20.1">Le</w> <w n="20.2">vent</w> <w n="20.3">souffle</w> <w n="20.4">dans</w> <w n="20.5">les</w> <w n="20.6">branches</w>.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">Car</w> <w n="21.2">depuis</w> <w n="21.3">lors</w>, <w n="21.4">à</w> <w n="21.5">minuit</w> <w n="21.6">noir</w>,</l>
						<l n="22" num="5.2"><w n="22.1">Dans</w> <w n="22.2">la</w> <w n="22.3">forêt</w> <w n="22.4">on</w> <w n="22.5">peut</w> <w n="22.6">le</w> <w n="22.7">voir</w></l>
						<l n="23" num="5.3"><space quantity="4" unit="char"></space><w n="23.1">A</w> <w n="23.2">l</w>’<w n="23.3">époque</w> <w n="23.4">des</w> <w n="23.5">pervenches</w>.</l>
						<l rhyme="none" n="24" num="5.4"><space quantity="12" unit="char"></space><w n="24.1">Hou</w> ! <w n="24.2">hou</w> ! <w n="24.3">hou</w> !</l>
						<l n="25" num="5.5"><space quantity="4" unit="char"></space><w n="25.1">Le</w> <w n="25.2">vent</w> <w n="25.3">souffle</w> <w n="25.4">dans</w> <w n="25.5">les</w> <w n="25.6">branches</w>.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1"><w n="26.1">Son</w> <w n="26.2">orgue</w> <w n="26.3">a</w> <w n="26.4">les</w> <w n="26.5">pins</w> <w n="26.6">pour</w> <w n="26.7">tuyaux</w>.</l>
						<l n="27" num="6.2"><w n="27.1">Il</w> <w n="27.2">fait</w> <w n="27.3">peur</w> <w n="27.4">aux</w> <w n="27.5">petits</w> <w n="27.6">oiseaux</w>.</l>
						<l n="28" num="6.3"><space quantity="4" unit="char"></space><w n="28.1">Orts</w> <w n="28.2">d</w>’<w n="28.3">amour</w> <w n="28.4">ont</w> <w n="28.5">leurs</w> <w n="28.6">revanches</w>.</l>
						<l rhyme="none" n="29" num="6.4"><space quantity="12" unit="char"></space><w n="29.1">Hou</w> ! <w n="29.2">hou</w> ! <w n="29.3">hou</w> !</l>
						<l n="30" num="6.5"><space quantity="4" unit="char"></space><w n="30.1">Le</w> <w n="30.2">vent</w> <w n="30.3">souffle</w> <w n="30.4">dans</w> <w n="30.5">les</w> <w n="30.6">branches</w>.</l>
					</lg>
				</div></body></text></TEI>