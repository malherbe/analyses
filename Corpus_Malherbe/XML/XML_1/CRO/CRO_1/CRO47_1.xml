<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO47">
					<head type="main">Vers amoureux</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">en</w> <w n="1.3">un</w> <w n="1.4">préau</w> <w n="1.5">d</w>’<w n="1.6">hôpital</w> <w n="1.7">de</w> <w n="1.8">fous</w></l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">monde</w> <w n="2.3">anxieux</w> <w n="2.4">s</w>’<w n="2.5">empresse</w> <w n="2.6">et</w> <w n="2.7">s</w>’<w n="2.8">agite</w></l>
						<l n="3" num="1.3"><w n="3.1">Autour</w> <w n="3.2">de</w> <w n="3.3">mes</w> <w n="3.4">yeux</w>, <w n="3.5">poursuivant</w> <w n="3.6">au</w> <w n="3.7">gîte</w></l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">rêve</w> <w n="4.3">que</w> <w n="4.4">j</w>’<w n="4.5">ai</w> <w n="4.6">quand</w> <w n="4.7">je</w> <w n="4.8">pense</w> <w n="4.9">à</w> <w n="4.10">vous</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">n</w>’<w n="5.3">en</w> <w n="5.4">pouvant</w> <w n="5.5">plus</w>, <w n="5.6">pourtant</w>, <w n="5.7">je</w> <w n="5.8">m</w>’<w n="5.9">isole</w></l>
						<l n="6" num="2.2"><w n="6.1">En</w> <w n="6.2">mes</w> <w n="6.3">souvenirs</w>. <w n="6.4">Je</w> <w n="6.5">ferme</w> <w n="6.6">les</w> <w n="6.7">yeux</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">vous</w> <w n="7.3">vois</w> <w n="7.4">passer</w> <w n="7.5">dans</w> <w n="7.6">les</w> <w n="7.7">lointains</w> <w n="7.8">bleus</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">j</w>’<w n="8.3">entends</w> <w n="8.4">le</w> <w n="8.5">son</w> <w n="8.6">de</w> <w n="8.7">votre</w> <w n="8.8">parole</w>.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Pour</w> <w n="9.2">moi</w>, <w n="9.3">je</w> <w n="9.4">m</w>’<w n="9.5">ennuie</w> <w n="9.6">en</w> <w n="9.7">ces</w> <w n="9.8">temps</w> <w n="9.9">railleurs</w>.</l>
						<l n="10" num="3.2"><w n="10.1">Je</w> <w n="10.2">sais</w> <w n="10.3">que</w> <w n="10.4">la</w> <w n="10.5">terre</w> <w n="10.6">aussi</w> <w n="10.7">vous</w> <w n="10.8">obsède</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Voulez</w>-<w n="11.2">vous</w> <w n="11.3">tenter</w> (<w n="11.4">étant</w> <w n="11.5">deux</w> <w n="11.6">on</w> <w n="11.7">s</w>’<w n="11.8">aide</w>)</l>
						<l n="12" num="3.4"><w n="12.1">Une</w> <w n="12.2">évasion</w> <w n="12.3">vers</w> <w n="12.4">des</w> <w n="12.5">cieux</w> <w n="12.6">meilleurs</w> ?</l>
					</lg>
				</div></body></text></TEI>