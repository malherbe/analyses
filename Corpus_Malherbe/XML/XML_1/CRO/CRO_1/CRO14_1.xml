<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS PERPÉTUELLES</head><div type="poem" key="CRO14">
					<head type="main">Chant éthiopien</head>
					<opener>
						<salute>A Émile Wroblewski</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Apportez</w>-<w n="1.2">moi</w> <w n="1.3">des</w> <w n="1.4">fleurs</w> <w n="1.5">odorantes</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">me</w> <w n="2.3">parer</w>, <w n="2.4">compagnes</w> <w n="2.5">errantes</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">te</w> <w n="3.3">charmer</w>, <w n="3.4">ô</w> <w n="3.5">mon</w> <w n="3.6">bien</w>-<w n="3.7">aimé</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Déjà</w> <w n="4.2">le</w> <w n="4.3">vent</w> <w n="4.4">s</w>’<w n="4.5">élève</w> <w n="4.6">embaumé</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">vent</w> <w n="5.3">du</w> <w n="5.4">soir</w> <w n="5.5">fait</w> <w n="5.6">flotter</w> <w n="5.7">vos</w> <w n="5.8">pagnes</w>.</l>
						<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">vos</w> <w n="6.3">cheveux</w>, <w n="6.4">pourquoi</w>, <w n="6.5">mes</w> <w n="6.6">compagnes</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Entrelacer</w> <w n="7.2">ces</w> <w n="7.3">perles</w> <w n="7.4">de</w> <w n="7.5">lait</w> ?</l>
						<l n="8" num="2.4"><w n="8.1">Mon</w> <w n="8.2">cou</w> — <w n="8.3">dit</w>-<w n="8.4">il</w> — <w n="8.5">sans</w> <w n="8.6">perles</w> <w n="8.7">lui</w> <w n="8.8">plaît</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mon</w> <w n="9.2">cou</w> <w n="9.3">qu</w>’<w n="9.4">il</w> <w n="9.5">prend</w> <w n="9.6">entre</w> <w n="9.7">ses</w> <w n="9.8">bras</w> <w n="9.9">souples</w></l>
						<l n="10" num="3.2"><w n="10.1">Frémit</w> <w n="10.2">d</w>’<w n="10.3">amour</w>. <w n="10.4">Nous</w> <w n="10.5">voyons</w> <w n="10.6">par</w> <w n="10.7">couples</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Tout</w> <w n="11.2">près</w> <w n="11.3">de</w> <w n="11.4">nous</w>, <w n="11.5">entre</w> <w n="11.6">les</w> <w n="11.7">roseaux</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">le</w> <w n="12.3">muguet</w>, <w n="12.4">jouer</w> <w n="12.5">les</w> <w n="12.6">oiseaux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">blanc</w> <w n="13.3">muguet</w> <w n="13.4">fait</w> <w n="13.5">des</w> <w n="13.6">perles</w> <w n="13.7">blanches</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Mon</w> <w n="14.2">bien</w>-<w n="14.3">aimé</w> <w n="14.4">rattache</w> <w n="14.5">à</w> <w n="14.6">mes</w> <w n="14.7">hanches</w></l>
						<l n="15" num="4.3"><w n="15.1">Mon</w> <w n="15.2">pagne</w> <w n="15.3">orné</w> <w n="15.4">de</w> <w n="15.5">muguet</w> <w n="15.6">en</w> <w n="15.7">fleur</w> ;</l>
						<l n="16" num="4.4"><w n="16.1">Mes</w> <w n="16.2">dents</w> — <w n="16.3">dit</w>-<w n="16.4">il</w> — <w n="16.5">en</w> <w n="16.6">ont</w> <w n="16.7">la</w> <w n="16.8">pâleur</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Mes</w> <w n="17.2">blanches</w> <w n="17.3">dents</w> <w n="17.4">et</w> <w n="17.5">mon</w> <w n="17.6">sein</w> <w n="17.7">qui</w> <w n="17.8">cède</w></l>
						<l n="18" num="5.2"><w n="18.1">Mes</w> <w n="18.2">longs</w> <w n="18.3">cheveux</w>, <w n="18.4">lui</w> <w n="18.5">seul</w> <w n="18.6">les</w> <w n="18.7">possède</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Depuis</w> <w n="19.2">le</w> <w n="19.3">soir</w> <w n="19.4">où</w> <w n="19.5">son</w> <w n="19.6">œil</w> <w n="19.7">m</w>’<w n="19.8">a</w> <w n="19.9">lui</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Il</w> <w n="20.2">est</w> <w n="20.3">à</w> <w n="20.4">moi</w> ; <w n="20.5">moi</w> <w n="20.6">je</w> <w n="20.7">suis</w> <w n="20.8">à</w> <w n="20.9">lui</w>.</l>
					</lg>
				</div></body></text></TEI>