<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO28">
					<head type="main">Triolets fantaisistes</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Sidonie</w> <w n="1.2">a</w> <w n="1.3">plus</w> <w n="1.4">d</w>’<w n="1.5">un</w> <w n="1.6">amant</w>,</l>
							<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2">est</w> <w n="2.3">une</w> <w n="2.4">chose</w> <w n="2.5">bien</w> <w n="2.6">connue</w></l>
							<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">elle</w> <w n="3.3">avoue</w>, <w n="3.4">elle</w>, <w n="3.5">fièrement</w>.</l>
							<l n="4" num="1.4"><w n="4.1">Sidonie</w> <w n="4.2">a</w> <w n="4.3">plus</w> <w n="4.4">d</w>’<w n="4.5">un</w> <w n="4.6">amant</w></l>
							<l n="5" num="1.5"><w n="5.1">Parce</w> <w n="5.2">que</w>, <w n="5.3">pour</w> <w n="5.4">elle</w>, <w n="5.5">être</w> <w n="5.6">nue</w></l>
							<l n="6" num="1.6"><w n="6.1">Est</w> <w n="6.2">son</w> <w n="6.3">plus</w> <w n="6.4">charmant</w> <w n="6.5">vêtement</w>.</l>
							<l n="7" num="1.7"><w n="7.1">C</w>’<w n="7.2">est</w> <w n="7.3">une</w> <w n="7.4">chose</w> <w n="7.5">bien</w> <w n="7.6">connue</w>,</l>
							<l n="8" num="1.8"><w n="8.1">Sidonie</w> <w n="8.2">a</w> <w n="8.3">plus</w> <w n="8.4">d</w>’<w n="8.5">un</w> <w n="8.6">amant</w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="9" num="1.1"><w n="9.1">Elle</w> <w n="9.2">en</w> <w n="9.3">prend</w> <w n="9.4">à</w> <w n="9.5">ses</w> <w n="9.6">cheveux</w> <w n="9.7">blonds</w></l>
							<l n="10" num="1.2"><w n="10.1">Comme</w>, <w n="10.2">à</w> <w n="10.3">sa</w> <w n="10.4">toile</w>, <w n="10.5">l</w>’<w n="10.6">araignée</w></l>
							<l n="11" num="1.3"><w n="11.1">Prend</w> <w n="11.2">les</w> <w n="11.3">mouches</w> <w n="11.4">et</w> <w n="11.5">les</w> <w n="11.6">frelons</w>.</l>
							<l n="12" num="1.4"><w n="12.1">Elle</w> <w n="12.2">en</w> <w n="12.3">prend</w> <w n="12.4">à</w> <w n="12.5">ses</w> <w n="12.6">cheveux</w> <w n="12.7">blonds</w>.</l>
							<l n="13" num="1.5"><w n="13.1">Vers</w> <w n="13.2">sa</w> <w n="13.3">prunelle</w> <w n="13.4">ensoleillée</w></l>
							<l n="14" num="1.6"><w n="14.1">Ils</w> <w n="14.2">volent</w>, <w n="14.3">pauvres</w> <w n="14.4">papillons</w>.</l>
							<l n="15" num="1.7"><w n="15.1">Comme</w>, <w n="15.2">à</w> <w n="15.3">sa</w> <w n="15.4">toile</w>, <w n="15.5">l</w>’<w n="15.6">araignée</w></l>
							<l n="16" num="1.8"><w n="16.1">Elle</w> <w n="16.2">en</w> <w n="16.3">prend</w> <w n="16.4">à</w> <w n="16.5">ses</w> <w n="16.6">cheveux</w> <w n="16.7">blonds</w>.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="17" num="1.1"><w n="17.1">Elle</w> <w n="17.2">en</w> <w n="17.3">attrape</w> <w n="17.4">avec</w> <w n="17.5">les</w> <w n="17.6">dents</w></l>
							<l n="18" num="1.2"><w n="18.1">Quand</w> <w n="18.2">le</w> <w n="18.3">rire</w> <w n="18.4">entr</w>’<w n="18.5">ouvre</w> <w n="18.6">sa</w> <w n="18.7">bouche</w></l>
							<l n="19" num="1.3"><w n="19.1">Et</w> <w n="19.2">dévore</w> <w n="19.3">les</w> <w n="19.4">imprudents</w>.</l>
							<l n="20" num="1.4"><w n="20.1">Elle</w> <w n="20.2">en</w> <w n="20.3">attrape</w> <w n="20.4">avec</w> <w n="20.5">les</w> <w n="20.6">dents</w>.</l>
							<l n="21" num="1.5"><w n="21.1">Sa</w> <w n="21.2">bouche</w>, <w n="21.3">quand</w> <w n="21.4">elle</w> <w n="21.5">se</w> <w n="21.6">couche</w>,</l>
							<l n="22" num="1.6"><w n="22.1">Reste</w> <w n="22.2">rose</w> <w n="22.3">et</w> <w n="22.4">ses</w> <w n="22.5">dents</w> <w n="22.6">dedans</w>.</l>
							<l n="23" num="1.7"><w n="23.1">Quand</w> <w n="23.2">le</w> <w n="23.3">rire</w> <w n="23.4">entr</w>’<w n="23.5">ouvre</w> <w n="23.6">sa</w> <w n="23.7">bouche</w></l>
							<l n="24" num="1.8"><w n="24.1">Elle</w> <w n="24.2">en</w> <w n="24.3">attrape</w> <w n="24.4">avec</w> <w n="24.5">les</w> <w n="24.6">dents</w>.</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">Elle</w> <w n="25.2">les</w> <w n="25.3">mène</w> <w n="25.4">par</w> <w n="25.5">le</w> <w n="25.6">nez</w>,</l>
							<l n="26" num="1.2"><w n="26.1">Comme</w> <w n="26.2">fait</w>, <w n="26.3">dit</w>-<w n="26.4">on</w>, <w n="26.5">le</w> <w n="26.6">crotale</w></l>
							<l n="27" num="1.3"><w n="27.1">Des</w> <w n="27.2">oiseaux</w> <w n="27.3">qu</w>’<w n="27.4">il</w> <w n="27.5">a</w> <w n="27.6">fascinés</w>.</l>
							<l n="28" num="1.4"><w n="28.1">Elle</w> <w n="28.2">les</w> <w n="28.3">mène</w> <w n="28.4">par</w> <w n="28.5">le</w> <w n="28.6">nez</w>.</l>
							<l n="29" num="1.5"><w n="29.1">Quand</w> <w n="29.2">dans</w> <w n="29.3">une</w> <w n="29.4">moue</w> <w n="29.5">elle</w> <w n="29.6">étale</w></l>
							<l n="30" num="1.6"><w n="30.1">Sa</w> <w n="30.2">langue</w> <w n="30.3">à</w> <w n="30.4">leurs</w> <w n="30.5">yeux</w> <w n="30.6">étonnés</w>,</l>
							<l n="31" num="1.7"><w n="31.1">Comme</w> <w n="31.2">fait</w>, <w n="31.3">dit</w>-<w n="31.4">on</w>, <w n="31.5">le</w> <w n="31.6">crotale</w></l>
							<l n="32" num="1.8"><w n="32.1">Elle</w> <w n="32.2">les</w> <w n="32.3">mène</w> <w n="32.4">par</w> <w n="32.5">le</w> <w n="32.6">nez</w>.</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">V</head>
						<lg n="1">
							<l n="33" num="1.1"><w n="33.1">Sidonie</w> <w n="33.2">a</w> <w n="33.3">plus</w> <w n="33.4">d</w>’<w n="33.5">un</w> <w n="33.6">amant</w>,</l>
							<l n="34" num="1.2"><w n="34.1">Qu</w>’<w n="34.2">on</w> <w n="34.3">le</w> <w n="34.4">lui</w> <w n="34.5">reproche</w> <w n="34.6">ou</w> <w n="34.7">l</w>’<w n="34.8">en</w> <w n="34.9">loue</w></l>
							<l n="35" num="1.3"><w n="35.1">Elle</w> <w n="35.2">s</w>’<w n="35.3">en</w> <w n="35.4">moque</w> <w n="35.5">également</w>.</l>
							<l n="36" num="1.4"><w n="36.1">Sidonie</w> <w n="36.2">a</w> <w n="36.3">plus</w> <w n="36.4">d</w>’<w n="36.5">un</w> <w n="36.6">amant</w>.</l>
							<l n="37" num="1.5"><w n="37.1">Aussi</w>, <w n="37.2">jusqu</w>’<w n="37.3">à</w> <w n="37.4">ce</w> <w n="37.5">qu</w>’<w n="37.6">on</w> <w n="37.7">la</w> <w n="37.8">cloue</w></l>
							<l n="38" num="1.6"><w n="38.1">Au</w> <w n="38.2">sapin</w> <w n="38.3">de</w> <w n="38.4">l</w>’<w n="38.5">enterrement</w>,</l>
							<l n="39" num="1.7"><w n="39.1">Qu</w>’<w n="39.2">on</w> <w n="39.3">le</w> <w n="39.4">lui</w> <w n="39.5">reproche</w> <w n="39.6">ou</w> <w n="39.7">l</w>’<w n="39.8">en</w> <w n="39.9">loue</w>,</l>
							<l n="40" num="1.8"><w n="40.1">Sidoine</w> <w n="40.2">aura</w> <w n="40.3">plus</w> <w n="40.4">d</w>’<w n="40.5">un</w> <w n="40.6">amant</w>.</l>
						</lg>
					</div>
				</div></body></text></TEI>