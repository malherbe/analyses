<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO97">
					<head type="main">Songe d’été</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">d</w>’<w n="1.3">autres</w> <w n="1.4">les</w> <w n="1.5">ciels</w> <w n="1.6">bleus</w> <w n="1.7">ou</w> <w n="1.8">les</w> <w n="1.9">ciels</w> <w n="1.10">tourmentés</w>,</l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">neige</w> <w n="2.3">des</w> <w n="2.4">hivers</w>, <w n="2.5">le</w> <w n="2.6">parfum</w> <w n="2.7">des</w> <w n="2.8">étés</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">monts</w> <w n="3.3">où</w> <w n="3.4">vous</w> <w n="3.5">grimpez</w>, <w n="3.6">fiertés</w> <w n="3.7">aventurières</w></l>
						<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">Anglaises</w>. <w n="4.3">Mes</w> <w n="4.4">yeux</w> <w n="4.5">aiment</w> <w n="4.6">mieux</w> <w n="4.7">les</w> <w n="4.8">clairières</w></l>
						<l n="5" num="1.5"><w n="5.1">Où</w> <w n="5.2">la</w> <w n="5.3">charcuterie</w> <w n="5.4">a</w> <w n="5.5">laissé</w> <w n="5.6">ses</w> <w n="5.7">papiers</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">sentiers</w> <w n="6.3">où</w> <w n="6.4">l</w>’<w n="6.5">on</w> <w n="6.6">sent</w> <w n="6.7">encor</w> <w n="6.8">l</w>’<w n="6.9">odeur</w> <w n="6.10">des</w> <w n="6.11">pieds</w></l>
						<l n="7" num="1.7"><w n="7.1">Des</w> <w n="7.2">soldats</w> <w n="7.3">avec</w> <w n="7.4">leurs</w> <w n="7.5">payses</w>, <w n="7.6">la</w> <w n="7.7">presqu</w>’<w n="7.8">île</w></l>
						<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">Gennevilliers</w>, <w n="8.3">où</w> <w n="8.4">croît</w> <w n="8.5">l</w>’<w n="8.6">asperge</w> <w n="8.7">tranquille</w></l>
						<l n="9" num="1.9"><w n="9.1">Sous</w> <w n="9.2">l</w>’<w n="9.3">irrigation</w> <w n="9.4">puante</w> <w n="9.5">des</w> <w n="9.6">égouts</w>…</l>
						<l n="10" num="1.10"><w n="10.1">On</w> <w n="10.2">ne</w> <w n="10.3">dispute</w> <w n="10.4">pas</w> <w n="10.5">des</w> <w n="10.6">couleurs</w> <w n="10.7">ni</w> <w n="10.8">des</w> <w n="10.9">goûts</w>.</l>
					</lg>
				</div></body></text></TEI>