<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO27">
					<head type="main">Souvenir d’avril</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">rhythme</w> <w n="1.3">argentin</w> <w n="1.4">de</w> <w n="1.5">ta</w> <w n="1.6">voix</w></l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">mes</w> <w n="2.3">rêves</w> <w n="2.4">gazouille</w> <w n="2.5">et</w> <w n="2.6">tinte</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Chant</w> <w n="3.2">d</w>’<w n="3.3">oiseau</w>, <w n="3.4">bruit</w> <w n="3.5">de</w> <w n="3.6">source</w> <w n="3.7">au</w> <w n="3.8">bois</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">réveillent</w> <w n="4.3">ma</w> <w n="4.4">joie</w> <w n="4.5">éteinte</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">les</w> <w n="5.3">bois</w> <w n="5.4">n</w>’<w n="5.5">ont</w> <w n="5.6">pas</w> <w n="5.7">de</w> <w n="5.8">frissons</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Ni</w> <w n="6.2">les</w> <w n="6.3">harpes</w> <w n="6.4">éoliennes</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">soient</w> <w n="7.3">si</w> <w n="7.4">doux</w> <w n="7.5">que</w> <w n="7.6">tes</w> <w n="7.7">chansons</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Que</w> <w n="8.2">tes</w> <w n="8.3">chansons</w> <w n="8.4">tyroliennes</w>.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Parfois</w> <w n="9.2">le</w> <w n="9.3">vent</w> <w n="9.4">m</w>’<w n="9.5">apporte</w> <w n="9.6">encor</w></l>
						<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2">odeur</w> <w n="10.3">de</w> <w n="10.4">ta</w> <w n="10.5">blonde</w> <w n="10.6">crinière</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">je</w> <w n="11.3">revois</w> <w n="11.4">tout</w> <w n="11.5">le</w> <w n="11.6">décor</w></l>
						<l n="12" num="3.4"><w n="12.1">D</w>’<w n="12.2">une</w> <w n="12.3">folle</w> <w n="12.4">nuit</w> <w n="12.5">printanière</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">D</w>’<w n="13.2">une</w> <w n="13.3">des</w> <w n="13.4">nuits</w>, <w n="13.5">où</w> <w n="13.6">tes</w> <w n="13.7">baisers</w></l>
						<l n="14" num="4.2"><w n="14.1">S</w>’<w n="14.2">entremêlaient</w> <w n="14.3">d</w>’<w n="14.4">historiettes</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Pendant</w> <w n="15.2">que</w> <w n="15.3">de</w> <w n="15.4">tes</w> <w n="15.5">doigts</w> <w n="15.6">rosés</w></l>
						<l n="16" num="4.4"><w n="16.1">Tu</w> <w n="16.2">te</w> <w n="16.3">roulais</w> <w n="16.4">des</w> <w n="16.5">cigarettes</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Où</w> <w n="17.2">ton</w> <w n="17.3">babil</w>, <w n="17.4">tes</w> <w n="17.5">mouvements</w></l>
						<l n="18" num="5.2"><w n="18.1">Prenaient</w> <w n="18.2">l</w>’<w n="18.3">étrange</w> <w n="18.4">caractère</w></l>
						<l n="19" num="5.3"><w n="19.1">D</w>’<w n="19.2">inquiétants</w> <w n="19.3">miaulements</w>,</l>
						<l n="20" num="5.4"><w n="20.1">De</w> <w n="20.2">mordillements</w> <w n="20.3">de</w> <w n="20.4">panthère</w>.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Puis</w> <w n="21.2">tu</w> <w n="21.3">livrais</w> <w n="21.4">tes</w> <w n="21.5">trésors</w> <w n="21.6">blancs</w></l>
						<l n="22" num="6.2"><w n="22.1">Avec</w> <w n="22.2">des</w> <w n="22.3">poses</w> <w n="22.4">languissantes</w></l>
						<l n="23" num="6.3"><w n="23.1">Le</w> <w n="23.2">frisson</w> <w n="23.3">emperlait</w> <w n="23.4">tes</w> <w n="23.5">flancs</w></l>
						<l n="24" num="6.4"><w n="24.1">Émus</w> <w n="24.2">des</w> <w n="24.3">voluptés</w> <w n="24.4">récentes</w>.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Ainsi</w> <w n="25.2">ton</w> <w n="25.3">image</w> <w n="25.4">me</w> <w n="25.5">suit</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Réconfort</w> <w n="26.2">aux</w> <w n="26.3">heures</w> <w n="26.4">glacées</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Sereine</w> <w n="27.2">étoile</w> <w n="27.3">de</w> <w n="27.4">la</w> <w n="27.5">nuit</w></l>
						<l n="28" num="7.4"><w n="28.1">Où</w> <w n="28.2">dorment</w> <w n="28.3">mes</w> <w n="28.4">splendeurs</w> <w n="28.5">passées</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ainsi</w>, <w n="29.2">dans</w> <w n="29.3">les</w>, <w n="29.4">pays</w> <w n="29.5">fictifs</w></l>
						<l n="30" num="8.2"><w n="30.1">Où</w> <w n="30.2">mon</w> <w n="30.3">âme</w> <w n="30.4">erre</w> <w n="30.5">vagabonde</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Les</w> <w n="31.2">fonds</w> <w n="31.3">noirs</w> <w n="31.4">de</w> <w n="31.5">cyprès</w> <w n="31.6">et</w> <w n="31.7">d</w>’<w n="31.8">ifs</w>,</l>
						<l n="32" num="8.4"><w n="32.1">S</w>’<w n="32.2">égayent</w> <w n="32.3">de</w> <w n="32.4">ta</w> <w n="32.5">beauté</w> <w n="32.6">blonde</w>.</l>
					</lg>
					<ab type="star">※</ab>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Et</w>, <w n="33.2">dans</w> <w n="33.3">l</w>’<w n="33.4">écrin</w> <w n="33.5">du</w> <w n="33.6">souvenir</w></l>
						<l n="34" num="9.2"><w n="34.1">Précieusement</w> <w n="34.2">enfermée</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Perle</w> <w n="35.2">que</w> <w n="35.3">rien</w> <w n="35.4">ne</w> <w n="35.5">peut</w> <w n="35.6">ternir</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Tu</w> <w n="36.2">demeures</w> <w n="36.3">la</w> <w n="36.4">plus</w> <w n="36.5">aimée</w>.</l>
					</lg>
				</div></body></text></TEI>