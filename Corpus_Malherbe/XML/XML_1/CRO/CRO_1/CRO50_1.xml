<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO50">
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Elle</w> <w n="1.2">s</w>’<w n="1.3">est</w> <w n="1.4">endormie</w> <w n="1.5">un</w> <w n="1.6">soir</w>, <w n="1.7">croisant</w> <w n="1.8">ses</w> <w n="1.9">bras</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ses</w> <w n="2.2">bras</w> <w n="2.3">souples</w> <w n="2.4">et</w> <w n="2.5">blancs</w> <w n="2.6">sur</w> <w n="2.7">sa</w> <w n="2.8">poitrine</w> <w n="2.9">frêle</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">fermant</w> <w n="3.3">pour</w> <w n="3.4">toujours</w> <w n="3.5">ses</w> <w n="3.6">yeux</w> <w n="3.7">clairs</w>, <w n="3.8">déjà</w> <w n="3.9">las</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">regarder</w> <w n="4.3">ce</w> <w n="4.4">monde</w>, <w n="4.5">exil</w> <w n="4.6">trop</w> <w n="4.7">lourd</w> <w n="4.8">pour</w> <w n="4.9">Elle</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Elle</w> <w n="5.2">vivait</w> <w n="5.3">de</w> <w n="5.4">fleurs</w>, <w n="5.5">de</w> <w n="5.6">rêves</w>, <w n="5.7">d</w>’<w n="5.8">idéal</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Âme</w>, <w n="6.2">incarnation</w> <w n="6.3">de</w> <w n="6.4">la</w> <w n="6.5">Ville</w> <w n="6.6">éternelle</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Lentement</w> <w n="7.2">étouffée</w>, <w n="7.3">et</w> <w n="7.4">d</w>’<w n="7.5">un</w> <w n="7.6">semblable</w> <w n="7.7">mal</w>,</l>
						<l n="8" num="2.4"><w n="8.1">La</w> <w n="8.2">splendeur</w> <w n="8.3">de</w> <w n="8.4">Paris</w> <w n="8.5">s</w>’<w n="8.6">est</w> <w n="8.7">éteinte</w> <w n="8.8">avec</w> <w n="8.9">Elle</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">pendant</w> <w n="9.3">que</w> <w n="9.4">son</w> <w n="9.5">corps</w> <w n="9.6">attend</w> <w n="9.7">pâle</w> <w n="9.8">et</w> <w n="9.9">glacé</w></l>
						<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">résurrection</w> <w n="10.3">de</w> <w n="10.4">sa</w> <w n="10.5">beauté</w> <w n="10.6">chamelle</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Dans</w> <w n="11.2">ce</w> <w n="11.3">monde</w> <w n="11.4">où</w>, <w n="11.5">royale</w> <w n="11.6">et</w> <w n="11.7">douce</w>, <w n="11.8">Elle</w> <w n="11.9">a</w> <w n="11.10">passé</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Nous</w> <w n="12.2">ne</w> <w n="12.3">pouvons</w> <w n="12.4">rester</w> <w n="12.5">qu</w>’<w n="12.6">en</w> <w n="12.7">nous</w> <w n="12.8">souvenant</w> <w n="12.9">d</w>’<w n="12.10">Elle</w>.</l>
					</lg>
				</div></body></text></TEI>