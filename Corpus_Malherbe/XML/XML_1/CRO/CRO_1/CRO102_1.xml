<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO102">
					<head type="main">Cœur simple</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">les</w> <w n="1.3">douces</w> <w n="1.4">tiédeurs</w> <w n="1.5">des</w> <w n="1.6">chambres</w> <w n="1.7">d</w>’<w n="1.8">accouchées</w></l>
						<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">à</w> <w n="2.3">peine</w>, <w n="2.4">à</w> <w n="2.5">travers</w> <w n="2.6">les</w> <w n="2.7">fenêtres</w> <w n="2.8">bouchées</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Entre</w> <w n="3.2">un</w> <w n="3.3">filet</w> <w n="3.4">de</w> <w n="3.5">jour</w>, <w n="3.6">j</w>’<w n="3.7">aime</w>, <w n="3.8">humble</w> <w n="3.9">visiteur</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">bruit</w> <w n="4.3">de</w> <w n="4.4">l</w>’<w n="4.5">eau</w> <w n="4.6">qu</w>’<w n="4.7">on</w> <w n="4.8">verse</w> <w n="4.9">en</w> <w n="4.10">un</w> <w n="4.11">irrigateur</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">les</w> <w n="5.3">cuvettes</w> <w n="5.4">à</w> <w n="5.5">l</w>’<w n="5.6">odeur</w> <w n="5.7">de</w> <w n="5.8">cataplasme</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Puis</w> <w n="6.2">la</w> <w n="6.3">garde</w>-<w n="6.4">malade</w> <w n="6.5">avec</w> <w n="6.6">son</w> <w n="6.7">accès</w> <w n="6.8">d</w>’<w n="6.9">asthme</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Les</w> <w n="7.2">couches</w>, <w n="7.3">où</w> <w n="7.4">s</w>’<w n="7.5">étend</w> <w n="7.6">l</w>’<w n="7.7">or</w> <w n="7.8">des</w> <w n="7.9">déjections</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Qui</w> <w n="8.2">sèchent</w> <w n="8.3">en</w> <w n="8.4">fumant</w> <w n="8.5">devant</w> <w n="8.6">les</w> <w n="8.7">clairs</w> <w n="8.8">tisons</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Me</w> <w n="9.2">rappellent</w> <w n="9.3">ma</w> <w n="9.4">mère</w> <w n="9.5">aux</w> <w n="9.6">jours</w> <w n="9.7">de</w> <w n="9.8">mon</w> <w n="9.9">enfance</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">je</w> <w n="10.3">bénis</w> <w n="10.4">ma</w> <w n="10.5">mère</w>, <w n="10.6">et</w> <w n="10.7">le</w> <w n="10.8">ciel</w>, <w n="10.9">et</w> <w n="10.10">la</w> <w n="10.11">France</w> !</l>
					</lg>
				</div></body></text></TEI>