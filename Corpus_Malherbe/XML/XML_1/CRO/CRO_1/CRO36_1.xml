<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PASSÉ</head><div type="poem" key="CRO36">
					<head type="main">Sur un miroir</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Toutes</w> <w n="1.2">les</w> <w n="1.3">fois</w>, <w n="1.4">miroir</w>, <w n="1.5">que</w> <w n="1.6">tu</w> <w n="1.7">lui</w> <w n="1.8">serviras</w></l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">se</w> <w n="2.3">mettre</w> <w n="2.4">du</w> <w n="2.5">noir</w> <w n="2.6">aux</w> <w n="2.7">yeux</w> <w n="2.8">ou</w> <w n="2.9">sur</w> <w n="2.10">sa</w> <w n="2.11">joue</w></l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">poudre</w> <w n="3.3">parfumée</w>, <w n="3.4">ou</w> <w n="3.5">bien</w> <w n="3.6">dans</w> <w n="3.7">une</w> <w n="3.8">moue</w></l>
						<l n="4" num="1.4"><w n="4.1">Charmante</w>, <w n="4.2">son</w> <w n="4.3">carmin</w> <w n="4.4">aux</w> <w n="4.5">lèvres</w>, <w n="4.6">tu</w> <w n="4.7">diras</w> :</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">Je</w> <w n="5.2">dormais</w> <w n="5.3">reflétant</w> <w n="5.4">les</w> <w n="5.5">vers</w>, <w n="5.6">que</w> <w n="5.7">sur</w> <w n="5.8">l</w>’<w n="5.9">ivoire</w></l>
						<l n="6" num="2.2"><w n="6.1">Il</w> <w n="6.2">écrivit</w>… <w n="6.3">1</w> <w n="6.4">Pourquoi</w> <w n="6.5">de</w> <w n="6.6">vos</w> <w n="6.7">yeux</w> <w n="6.8">de</w> <w n="6.9">velours</w>,</l>
						<l n="7" num="2.3"><w n="7.1">De</w> <w n="7.2">votre</w> <w n="7.3">chair</w>, <w n="7.4">de</w> <w n="7.5">vos</w> <w n="7.6">lèvres</w>, <w n="7.7">par</w> <w n="7.8">ces</w> <w n="7.9">atours</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Rendre</w> <w n="8.2">plus</w> <w n="8.3">éclatante</w> <w n="8.4">encore</w> <w n="8.5">la</w> <w n="8.6">victoire</w> ? »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Alors</w>, <w n="9.2">si</w> <w n="9.3">tu</w> <w n="9.4">surprends</w> <w n="9.5">quelque</w> <w n="9.6">regard</w> <w n="9.7">pervers</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Si</w> <w n="10.2">de</w> <w n="10.3">l</w>’<w n="10.4">amour</w> <w n="10.5">présent</w> <w n="10.6">elle</w> <w n="10.7">est</w> <w n="10.8">distraite</w> <w n="10.9">ou</w> <w n="10.10">lasse</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Brise</w>-<w n="11.2">toi</w>, <w n="11.3">mais</w> <w n="11.4">ne</w> <w n="11.5">lui</w> <w n="11.6">sers</w> <w n="11.7">pas</w>, <w n="11.8">petite</w> <w n="11.9">glace</w>,</l>
						<l n="12" num="3.4"><w n="12.1">A</w> <w n="12.2">s</w>’<w n="12.3">orner</w> <w n="12.4">pour</w> <w n="12.5">un</w> <w n="12.6">autre</w>, <w n="12.7">en</w> <w n="12.8">riant</w> <w n="12.9">de</w> <w n="12.10">mes</w> <w n="12.11">vers</w>.</l>
					</lg>
				</div></body></text></TEI>