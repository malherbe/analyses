<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GRAINS DE SEL</head><div type="poem" key="CRO103">
					<head type="main">Tableau</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Enclavé</w> <w n="1.2">dans</w> <w n="1.3">les</w> <w n="1.4">rails</w>, <w n="1.5">engraissé</w> <w n="1.6">de</w> <w n="1.7">scories</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Leur</w> <w n="2.2">petit</w> <w n="2.3">potager</w> <w n="2.4">plaît</w> <w n="2.5">à</w> <w n="2.6">mes</w> <w n="2.7">rêveries</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">père</w> <w n="3.3">est</w> <w n="3.4">aiguilleur</w> <w n="3.5">à</w> <w n="3.6">la</w> <w n="3.7">gare</w> <w n="3.8">de</w> <w n="3.9">Lyon</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">fait</w> <w n="4.3">honnêtement</w> <w n="4.4">et</w> <w n="4.5">sans</w> <w n="4.6">rébellion</w></l>
						<l n="5" num="1.5"><w n="5.1">Son</w> <w n="5.2">dur</w> <w n="5.3">métier</w>. <w n="5.4">Sa</w> <w n="5.5">femme</w>, <w n="5.6">hélas</w> ! <w n="5.7">qui</w> <w n="5.8">serait</w> <w n="5.9">blonde</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Sans</w> <w n="6.2">le</w> <w n="6.3">sombre</w> <w n="6.4">glacis</w> <w n="6.5">du</w> <w n="6.6">charbon</w>, <w n="6.7">le</w> <w n="6.8">seconde</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Leur</w> <w n="7.2">enfant</w>, <w n="7.3">ange</w> <w n="7.4">rose</w> <w n="7.5">éclos</w> <w n="7.6">dans</w> <w n="7.7">cet</w> <w n="7.8">enfer</w></l>
						<l n="8" num="1.8"><w n="8.1">Fait</w> <w n="8.2">des</w> <w n="8.3">petits</w> <w n="8.4">châteaux</w> <w n="8.5">avec</w> <w n="8.6">du</w> <w n="8.7">mâchefer</w>.</l>
						<l n="9" num="1.9"><w n="9.1">A</w> <w n="9.2">quinze</w> <w n="9.3">ans</w> <w n="9.4">il</w> <w n="9.5">vendra</w> <w n="9.6">des</w> <w n="9.7">journaux</w>, <w n="9.8">des</w> <w n="9.9">cigares</w> :</l>
						<l n="10" num="1.10"><w n="10.1">Peut</w>-<w n="10.2">être</w> <w n="10.3">le</w> <w n="10.4">bonheur</w> <w n="10.5">n</w>’<w n="10.6">est</w>-<w n="10.7">il</w> <w n="10.8">que</w> <w n="10.9">dans</w> <w n="10.10">les</w> <w n="10.11">gares</w> !</l>
					</lg>
				</div></body></text></TEI>