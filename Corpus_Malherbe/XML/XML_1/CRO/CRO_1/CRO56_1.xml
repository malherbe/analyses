<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">DRAMES ET FANTAISIES</head><head type="main_subpart">Drame en trois ballades</head><div type="poem" key="CRO56">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Pour</w> <w n="1.2">fuir</w> <w n="1.3">l</w>’<w n="1.4">ennui</w> <w n="1.5">que</w> <w n="1.6">son</w> <w n="1.7">départ</w> <w n="1.8">me</w> <w n="1.9">laisse</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Pendant</w> <w n="2.2">le</w> <w n="2.3">jour</w>, <w n="2.4">je</w> <w n="2.5">m</w>’<w n="2.6">en</w> <w n="2.7">vais</w> <w n="2.8">au</w> <w n="2.9">travers</w></l>
							<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">bois</w>, <w n="3.3">cherchant</w> <w n="3.4">les</w> <w n="3.5">abris</w> <w n="3.6">bien</w> <w n="3.7">couverts</w>.</l>
							<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">deux</w> <w n="4.3">chiens</w> <w n="4.4">qu</w>’<w n="4.5">on</w> <w n="4.6">a</w> <w n="4.7">couplés</w> <w n="4.8">en</w> <w n="4.9">laisse</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Deux</w> <w n="5.2">papillons</w> <w n="5.3">courent</w> <w n="5.4">les</w> <w n="5.5">taillis</w> <w n="5.6">verts</w>.</l>
							<l n="6" num="1.6"><w n="6.1">Lors</w>, <w n="6.2">je</w> <w n="6.3">m</w>’<w n="6.4">étends</w> <w n="6.5">dans</w> <w n="6.6">l</w>’<w n="6.7">herbe</w> <w n="6.8">caressante</w>.</l>
							<l n="7" num="1.7"><w n="7.1">Les</w> <w n="7.2">moucherons</w>, <w n="7.3">les</w> <w n="7.4">faucheux</w>, <w n="7.5">les</w> <w n="7.6">fourmis</w></l>
							<l n="8" num="1.8"><w n="8.1">Passent</w> <w n="8.2">sur</w> <w n="8.3">moi</w>, <w n="8.4">sans</w> <w n="8.5">que</w> <w n="8.6">mon</w> <w n="8.7">corps</w> <w n="8.8">les</w> <w n="8.9">sente</w>.</l>
							<l n="9" num="1.9"><w n="9.1">Les</w> <w n="9.2">rossignols</w> <w n="9.3">là</w>-<w n="9.4">haut</w> <w n="9.5">sont</w> <w n="9.6">endormis</w>.</l>
							<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">moi</w>, <w n="10.3">je</w> <w n="10.4">pense</w> <w n="10.5">à</w> <w n="10.6">ma</w> <w n="10.7">maîtresse</w> <w n="10.8">absente</w>.</l>
						</lg>
						<lg n="2">
							<l n="11" num="2.1"><w n="11.1">Le</w> <w n="11.2">soir</w>, <w n="11.3">traînant</w> <w n="11.4">la</w> <w n="11.5">flèche</w> <w n="11.6">qui</w> <w n="11.7">me</w> <w n="11.8">blesse</w>,</l>
							<l n="12" num="2.2"><w n="12.1">Je</w> <w n="12.2">vais</w>, <w n="12.3">longeant</w> <w n="12.4">la</w> <w n="12.5">rue</w> <w n="12.6">aux</w> <w n="12.7">bruits</w> <w n="12.8">divers</w>.</l>
							<l n="13" num="2.3"><w n="13.1">Le</w> <w n="13.2">gaz</w> <w n="13.3">qui</w> <w n="13.4">brille</w> <w n="13.5">aux</w> <w n="13.6">cafés</w> <w n="13.7">grands</w> <w n="13.8">ouverts</w>,</l>
							<l n="14" num="2.4"><w n="14.1">Les</w> <w n="14.2">bals</w> <w n="14.3">publics</w>, <w n="14.4">flots</w> <w n="14.5">d</w>’<w n="14.6">obscène</w> <w n="14.7">souplesse</w>,</l>
							<l n="15" num="2.5"><w n="15.1">Montrent</w> <w n="15.2">des</w> <w n="15.3">chairs</w>, <w n="15.4">bons</w> <w n="15.5">repas</w> <w n="15.6">pour</w> <w n="15.7">les</w> <w n="15.8">vers</w>.</l>
							<l n="16" num="2.6"><w n="16.1">Mais</w>, <w n="16.2">que</w> <w n="16.3">parfois</w>, <w n="16.4">accablé</w>, <w n="16.5">je</w> <w n="16.6">consente</w>,</l>
							<l n="17" num="2.7"><w n="17.1">Muet</w>, <w n="17.2">à</w> <w n="17.3">boire</w> <w n="17.4">avec</w> <w n="17.5">vous</w>, <w n="17.6">mes</w> <w n="17.7">amis</w>,</l>
							<l n="18" num="2.8"><w n="18.1">La</w> <w n="18.2">bière</w> <w n="18.3">blonde</w>, <w n="18.4">ivresse</w> <w n="18.5">alourdissante</w>,</l>
							<l n="19" num="2.9"><w n="19.1">Parlez</w>, <w n="19.2">chantez</w> ! <w n="19.3">Rire</w> <w n="19.4">vous</w> <w n="19.5">est</w> <w n="19.6">permis</w>.</l>
							<l n="20" num="2.10"><w n="20.1">Et</w> <w n="20.2">moi</w>, <w n="20.3">je</w> <w n="20.4">pense</w> <w n="20.5">à</w> <w n="20.6">ma</w> <w n="20.7">maîtresse</w> <w n="20.8">absente</w>.</l>
						</lg>
						<lg n="3">
							<l n="21" num="3.1"><w n="21.1">Mais</w> <w n="21.2">il</w> <w n="21.3">est</w> <w n="21.4">tard</w>… <w n="21.5">Dormons</w>. <w n="21.6">Rêvons</w> <w n="21.7">d</w>’<w n="21.8">Elle</w>. <w n="21.9">Est</w>-<w n="21.10">ce</w></l>
							<l n="22" num="3.2"><w n="22.1">Le</w> <w n="22.2">souvenir</w> <w n="22.3">des</w> <w n="22.4">scintillants</w> <w n="22.5">hivers</w></l>
							<l n="23" num="3.3"><w n="23.1">Qui</w> <w n="23.2">se</w> <w n="23.3">déroule</w> <w n="23.4">en</w> <w n="23.5">fantômes</w> <w n="23.6">pervers</w>,</l>
							<l n="24" num="3.4"><w n="24.1">Dans</w> <w n="24.2">mon</w> <w n="24.3">cerveau</w> <w n="24.4">que</w> <w n="24.5">le</w> <w n="24.6">sommeil</w> <w n="24.7">délaisse</w>,</l>
							<l n="25" num="3.5"><w n="25.1">Au</w> <w n="25.2">rhythme</w> <w n="25.3">lent</w> <w n="25.4">et</w> <w n="25.5">poignant</w> <w n="25.6">d</w>’<w n="25.7">anciens</w> <w n="25.8">vers</w> ?</l>
							<l n="26" num="3.6"><w n="26.1">Enfin</w>, <w n="26.2">la</w> <w n="26.3">fièvre</w> <w n="26.4">et</w> <w n="26.5">la</w> <w n="26.6">nuit</w> <w n="26.7">fraîchissante</w>,</l>
							<l n="27" num="3.7"><w n="27.1">Ferment</w> <w n="27.2">mes</w> <w n="27.3">yeux</w>, <w n="27.4">domptent</w> <w n="27.5">mes</w> <w n="27.6">flancs</w> <w n="27.7">blêmis</w></l>
							<l n="28" num="3.8"><w n="28.1">Quand</w> <w n="28.2">reparait</w> <w n="28.3">l</w>’<w n="28.4">aurore</w> <w n="28.5">éblouissante</w>,</l>
							<l n="29" num="3.9"><w n="29.1">Voici</w> <w n="29.2">crier</w> <w n="29.3">les</w> <w n="29.4">oiseaux</w> <w n="29.5">insoumis</w>.</l>
							<l n="30" num="3.10"><w n="30.1">Et</w> <w n="30.2">moi</w>, <w n="30.3">je</w> <w n="30.4">pense</w> <w n="30.5">à</w> <w n="30.6">ma</w> <w n="30.7">maîtresse</w> <w n="30.8">absente</w>.</l>
						</lg>
						<lg n="4">
							<head type="form">ENVOI</head>
							<l n="31" num="4.1"><w n="31.1">A</w> <w n="31.2">ton</w> <w n="31.3">lever</w>, <w n="31.4">soleil</w>, <w n="31.5">à</w> <w n="31.6">ta</w> <w n="31.7">descente</w></l>
							<l n="32" num="4.2"><w n="32.1">Que</w> <w n="32.2">suit</w> <w n="32.3">la</w> <w n="32.4">nuit</w> <w n="32.5">au</w> <w n="32.6">splendide</w> <w n="32.7">semis</w>,</l>
							<l n="33" num="4.3"><w n="33.1">L</w>’<w n="33.2">homme</w>, <w n="33.3">oubliant</w> <w n="33.4">sa</w> <w n="33.5">pioche</w> <w n="33.6">harassante</w>,</l>
							<l n="34" num="4.4"><w n="34.1">Sourit</w> <w n="34.2">de</w> <w n="34.3">voir</w> <w n="34.4">mûrir</w> <w n="34.5">les</w> <w n="34.6">fruits</w> <w n="34.7">promis</w>,</l>
							<l n="35" num="4.5"><w n="35.1">Et</w> <w n="35.2">moi</w>, <w n="35.3">je</w> <w n="35.4">pense</w> <w n="35.5">à</w> <w n="35.6">ma</w> <w n="35.7">maîtresse</w> <w n="35.8">absente</w>.</l>
						</lg>
					</div></body></text></TEI>