<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Coffret De Santal</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2548 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlecroslecofretdesental.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Coffret De Santal</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>TRESSE ÉDITEUR</publisher>
							<date when="1879">1879</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1879">1879</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
				<p>La partie non métrique du poème ’Le Hareng saur’ est délimitée par la balise ’del’ </p>
				<p>Le refrain de fin de strophe du poème ’Brave Homme’ a été rétabli pour chaque strophe, en lieu et place de ’etc’.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VINGT SONNETS</head><div type="poem" key="CRO72">
					<head type="main">Conseil</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">sur</w> <w n="1.3">vos</w> <w n="1.4">cheveux</w> <w n="1.5">blonds</w>, <w n="1.6">et</w> <w n="1.7">fauves</w> <w n="1.8">au</w> <w n="1.9">soleil</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Vous</w> <w n="2.2">mettez</w> <w n="2.3">des</w> <w n="2.4">rubans</w> <w n="2.5">de</w> <w n="2.6">velours</w> <w n="2.7">noir</w>, <w n="2.8">méchante</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">pense</w> <w n="3.3">au</w> <w n="3.4">tigre</w> <w n="3.5">dont</w> <w n="3.6">le</w> <w n="3.7">pelage</w> <w n="3.8">est</w> <w n="3.9">pareil</w> :</l>
						<l n="4" num="1.4"><w n="4.1">Fond</w> <w n="4.2">roux</w>, <w n="4.3">rayé</w> <w n="4.4">de</w> <w n="4.5">noir</w>, <w n="4.6">splendeur</w> <w n="4.7">de</w> <w n="4.8">l</w>’<w n="4.9">épouvante</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Quand</w> <w n="5.2">le</w> <w n="5.3">rire</w> <w n="5.4">fait</w> <w n="5.5">luire</w>, <w n="5.6">au</w> <w n="5.7">calice</w> <w n="5.8">vermeil</w></l>
						<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">vos</w> <w n="6.3">lèvres</w>, <w n="6.4">l</w>’<w n="6.5">éclair</w> <w n="6.6">de</w> <w n="6.7">nacre</w> <w n="6.8">inquiétante</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Quand</w> <w n="7.2">s</w>’<w n="7.3">émeut</w> <w n="7.4">votre</w> <w n="7.5">joue</w> <w n="7.6">en</w> <w n="7.7">feu</w>, <w n="7.8">c</w>’<w n="7.9">est</w> <w n="7.10">un</w> <w n="7.11">réveil</w></l>
						<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">tigre</w> : <w n="8.3">miaulements</w>, <w n="8.4">dents</w> <w n="8.5">blanches</w>, <w n="8.6">mort</w> <w n="8.7">qui</w> <w n="8.8">tente</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">puis</w>, <w n="9.3">regardez</w>-<w n="9.4">vous</w>. <w n="9.5">Même</w> <w n="9.6">sans</w> <w n="9.7">ce</w> <w n="9.8">velours</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Quoique</w> <w n="10.2">plus</w> <w n="10.3">belle</w>, <w n="10.4">enfin</w> <w n="10.5">vous</w> <w n="10.6">ressemblez</w> <w n="10.7">toujours</w></l>
						<l n="11" num="3.3"><w n="11.1">A</w> <w n="11.2">celui</w> <w n="11.3">que</w> <w n="11.4">parfois</w> <w n="11.5">votre</w> <w n="11.6">bouche</w> <w n="11.7">dénigre</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">D</w>’<w n="12.2">ailleurs</w> <w n="12.3">si</w> <w n="12.4">vous</w> <w n="12.5">tombiez</w> <w n="12.6">sous</w> <w n="12.7">sa</w> <w n="12.8">griffe</w>, <w n="12.9">une</w> <w n="12.10">fois</w> ?</l>
						<l n="13" num="4.2"><w n="13.1">On</w> <w n="13.2">ne</w> <w n="13.3">peut</w> <w n="13.4">pas</w> <w n="13.5">savoir</w> <w n="13.6">qui</w> <w n="13.7">l</w>’<w n="13.8">on</w> <w n="13.9">rencontre</w> <w n="13.10">au</w> <w n="13.11">bois</w> :</l>
						<l n="14" num="4.3"><w n="14.1">Madame</w>, <w n="14.2">il</w> <w n="14.3">ne</w> <w n="14.4">faut</w> <w n="14.5">pas</w> <w n="14.6">dire</w> <w n="14.7">de</w> <w n="14.8">mal</w> <w n="14.9">du</w> <w n="14.10">tigre</w>.</l>
					</lg>
				</div></body></text></TEI>