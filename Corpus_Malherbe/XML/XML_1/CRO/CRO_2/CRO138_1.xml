<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FANTAISIES TRAGIQUES</head><div type="poem" key="CRO138">
					<head type="main">École buissonnière</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ma</w> <w n="1.2">pensée</w> <w n="1.3">est</w> <w n="1.4">une</w> <w n="1.5">églantine</w></l>
						<l n="2" num="1.2"><w n="2.1">Éclose</w> <w n="2.2">trop</w> <w n="2.3">tôt</w> <w n="2.4">en</w> <w n="2.5">avril</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Moqueuse</w> <w n="3.2">au</w> <w n="3.3">moucheron</w> <w n="3.4">subtil</w></l>
						<l n="4" num="1.4"><w n="4.1">Ma</w> <w n="4.2">pensée</w> <w n="4.3">est</w> <w n="4.4">une</w> <w n="4.5">églantine</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Si</w> <w n="5.2">parfois</w> <w n="5.3">tremble</w> <w n="5.4">son</w> <w n="5.5">pistil</w></l>
						<l n="6" num="1.6"><w n="6.1">Sa</w> <w n="6.2">corolle</w> <w n="6.3">s</w>’<w n="6.4">ouvre</w> <w n="6.5">mutine</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Ma</w> <w n="7.2">pensée</w> <w n="7.3">est</w> <w n="7.4">une</w> <w n="7.5">églantine</w></l>
						<l n="8" num="1.8"><w n="8.1">Éclose</w> <w n="8.2">trop</w> <w n="8.3">tôt</w> <w n="8.4">en</w> <w n="8.5">avril</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Ma</w> <w n="9.2">pensée</w> <w n="9.3">est</w> <w n="9.4">comme</w> <w n="9.5">un</w> <w n="9.6">chardon</w></l>
						<l n="10" num="2.2"><w n="10.1">Piquant</w> <w n="10.2">sous</w> <w n="10.3">les</w> <w n="10.4">fleurs</w> <w n="10.5">violettes</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Un</w> <w n="11.2">peu</w> <w n="11.3">rude</w> <w n="11.4">au</w> <w n="11.5">doux</w> <w n="11.6">abandon</w></l>
						<l n="12" num="2.4"><w n="12.1">Ma</w> <w n="12.2">pensée</w> <w n="12.3">est</w> <w n="12.4">comme</w> <w n="12.5">un</w> <w n="12.6">chardon</w> ;</l>
						<l n="13" num="2.5"><w n="13.1">Tu</w> <w n="13.2">viens</w> <w n="13.3">le</w> <w n="13.4">visiter</w>, <w n="13.5">bourdon</w> ?</l>
						<l n="14" num="2.6"><w n="14.1">Ma</w> <w n="14.2">fleur</w> <w n="14.3">plaît</w> <w n="14.4">à</w> <w n="14.5">beaucoup</w> <w n="14.6">de</w> <w n="14.7">bêtes</w>.</l>
						<l n="15" num="2.7"><w n="15.1">Ma</w> <w n="15.2">pensée</w> <w n="15.3">est</w> <w n="15.4">comme</w> <w n="15.5">un</w> <w n="15.6">chardon</w></l>
						<l n="16" num="2.8"><w n="16.1">Piquant</w> <w n="16.2">sous</w> <w n="16.3">les</w> <w n="16.4">fleurs</w> <w n="16.5">violettes</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Ma</w> <w n="17.2">pensée</w> <w n="17.3">est</w> <w n="17.4">une</w> <w n="17.5">insensée</w></l>
						<l n="18" num="3.2"><w n="18.1">Qui</w> <w n="18.2">s</w>’<w n="18.3">égare</w> <w n="18.4">dans</w> <w n="18.5">les</w> <w n="18.6">roseaux</w></l>
						<l n="19" num="3.3"><w n="19.1">Aux</w> <w n="19.2">chants</w> <w n="19.3">des</w> <w n="19.4">eaux</w> <w n="19.5">et</w> <w n="19.6">des</w> <w n="19.7">oiseaux</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Ma</w> <w n="20.2">pensée</w> <w n="20.3">est</w> <w n="20.4">une</w> <w n="20.5">insensée</w>.</l>
						<l n="21" num="3.5"><w n="21.1">Les</w> <w n="21.2">roseaux</w> <w n="21.3">font</w> <w n="21.4">de</w> <w n="21.5">verts</w> <w n="21.6">réseaux</w>,</l>
						<l n="22" num="3.6"><w n="22.1">Lotus</w> <w n="22.2">sans</w> <w n="22.3">tige</w> <w n="22.4">sur</w> <w n="22.5">les</w> <w n="22.6">eaux</w></l>
						<l n="23" num="3.7"><w n="23.1">Ma</w> <w n="23.2">pensée</w> <w n="23.3">est</w> <w n="23.4">une</w> <w n="23.5">insensée</w></l>
						<l n="24" num="3.8"><w n="24.1">Qui</w> <w n="24.2">s</w>’<w n="24.3">égare</w> <w n="24.4">dans</w> <w n="24.5">les</w> <w n="24.6">roseaux</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Ma</w> <w n="25.2">pensée</w> <w n="25.3">est</w> <w n="25.4">l</w>’<w n="25.5">âcre</w> <w n="25.6">poison</w></l>
						<l n="26" num="4.2"><w n="26.1">Qu</w>’<w n="26.2">on</w> <w n="26.3">boit</w> <w n="26.4">à</w> <w n="26.5">la</w> <w n="26.6">dernière</w> <w n="26.7">fête</w></l>
						<l n="27" num="4.3"><w n="27.1">Couleur</w>, <w n="27.2">parfum</w> <w n="27.3">et</w> <w n="27.4">trahison</w>,</l>
						<l n="28" num="4.4"><w n="28.1">Ma</w> <w n="28.2">pensée</w> <w n="28.3">est</w> <w n="28.4">l</w>’<w n="28.5">âcre</w> <w n="28.6">poison</w>,</l>
						<l n="29" num="4.5"><w n="29.1">Fleur</w> <w n="29.2">frêle</w>, <w n="29.3">pourprée</w> <w n="29.4">et</w> <w n="29.5">coquette</w></l>
						<l n="30" num="4.6"><w n="30.1">Qu</w>’<w n="30.2">on</w> <w n="30.3">trouve</w> <w n="30.4">à</w> <w n="30.5">l</w>’<w n="30.6">arrière</w>-<w n="30.7">saison</w></l>
						<l n="31" num="4.7"><w n="31.1">Ma</w> <w n="31.2">pensée</w> <w n="31.3">est</w> <w n="31.4">l</w>’<w n="31.5">âcre</w> <w n="31.6">poison</w></l>
						<l n="32" num="4.8"><w n="32.1">Qu</w>’<w n="32.2">on</w> <w n="32.3">boit</w> <w n="32.4">à</w> <w n="32.5">la</w> <w n="32.6">dernière</w> <w n="32.7">fête</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Ma</w> <w n="33.2">pensée</w> <w n="33.3">est</w> <w n="33.4">un</w> <w n="33.5">perce</w>-<w n="33.6">neige</w></l>
						<l n="34" num="5.2"><w n="34.1">Qui</w> <w n="34.2">pousse</w> <w n="34.3">et</w> <w n="34.4">rit</w> <w n="34.5">malgré</w> <w n="34.6">le</w> <w n="34.7">froid</w></l>
						<l n="35" num="5.3"><w n="35.1">Sans</w> <w n="35.2">souci</w> <w n="35.3">d</w>’<w n="35.4">heure</w> <w n="35.5">ni</w> <w n="35.6">d</w>’<w n="35.7">endroit</w></l>
						<l n="36" num="5.4"><w n="36.1">Ma</w> <w n="36.2">pensée</w> <w n="36.3">est</w> <w n="36.4">un</w> <w n="36.5">perce</w>-<w n="36.6">neige</w>.</l>
						<l n="37" num="5.5"><w n="37.1">Si</w> <w n="37.2">son</w> <w n="37.3">terrain</w> <w n="37.4">est</w> <w n="37.5">bien</w> <w n="37.6">étroit</w></l>
						<l n="38" num="5.6"><w n="38.1">La</w> <w n="38.2">feuille</w> <w n="38.3">morte</w> <w n="38.4">le</w> <w n="38.5">protège</w>,</l>
						<l n="39" num="5.7"><w n="39.1">Ma</w> <w n="39.2">pensée</w> <w n="39.3">est</w> <w n="39.4">un</w> <w n="39.5">perce</w>-<w n="39.6">neige</w></l>
						<l n="40" num="5.8"><w n="40.1">Qui</w> <w n="40.2">pousse</w> <w n="40.3">et</w> <w n="40.4">rit</w> <w n="40.5">malgré</w> <w n="40.6">le</w> <w n="40.7">froid</w>.</l>
					</lg>
				</div></body></text></TEI>