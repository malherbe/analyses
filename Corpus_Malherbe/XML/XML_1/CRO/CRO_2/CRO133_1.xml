<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO133">
					<head type="main">Ballade de la Ruine</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">viens</w> <w n="1.3">de</w> <w n="1.4">revoir</w> <w n="1.5">le</w> <w n="1.6">pays</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">beau</w> <w n="2.3">domaine</w> <w n="2.4">imaginaire</w></l>
						<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">des</w> <w n="3.3">horizons</w> <w n="3.4">éblouis</w></l>
						<l n="4" num="1.4"><w n="4.1">Me</w> <w n="4.2">venaient</w> <w n="4.3">des</w> <w n="4.4">parfums</w> <w n="4.5">exquis</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Ces</w> <w n="5.2">parfums</w> <w n="5.3">et</w> <w n="5.4">cette</w> <w n="5.5">lumière</w></l>
						<l n="6" num="1.6"><w n="6.1">Je</w> <w n="6.2">ne</w> <w n="6.3">tes</w> <w n="6.4">ai</w> <w n="6.5">pas</w> <w n="6.6">retrouvés</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Au</w> <w n="7.2">château</w> <w n="7.3">s</w>’<w n="7.4">émiette</w> <w n="7.5">la</w> <w n="7.6">pierre</w>.</l>
						<l n="8" num="1.8"><w n="8.1">L</w>’<w n="8.2">herbe</w> <w n="8.3">pousse</w> <w n="8.4">entre</w> <w n="8.5">les</w> <w n="8.6">pavés</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">La</w> <w n="9.2">galerie</w> <w n="9.3">où</w> <w n="9.4">les</w> <w n="9.5">amis</w></l>
						<l n="10" num="2.2"><w n="10.1">Venaient</w> <w n="10.2">faire</w> <w n="10.3">joyeuse</w> <w n="10.4">chère</w></l>
						<l n="11" num="2.3"><w n="11.1">Abrite</w> <w n="11.2">en</w> <w n="11.3">ses</w> <w n="11.4">lambris</w> <w n="11.5">moisis</w></l>
						<l n="12" num="2.4"><w n="12.1">Cloportes</w> <w n="12.2">et</w> <w n="12.3">chauves</w>-<w n="12.4">souris</w> ;</l>
						<l n="13" num="2.5"><w n="13.1">L</w>’<w n="13.2">ortie</w> <w n="13.3">a</w> <w n="13.4">tué</w> <w n="13.5">jusqu</w>’<w n="13.6">au</w> <w n="13.7">lierre</w>.</l>
						<l n="14" num="2.6"><w n="14.1">Les</w> <w n="14.2">beaux</w> <w n="14.3">lévriers</w> <w n="14.4">sont</w> <w n="14.5">crevés</w></l>
						<l n="15" num="2.7"><w n="15.1">Qui</w> <w n="15.2">jappaient</w> <w n="15.3">d</w>’<w n="15.4">une</w> <w n="15.5">voix</w> <w n="15.6">si</w> <w n="15.7">claire</w>.</l>
						<l n="16" num="2.8"><w n="16.1">L</w>’<w n="16.2">herbe</w> <w n="16.3">pousse</w> <w n="16.4">entre</w> <w n="16.5">les</w> <w n="16.6">pavés</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Tous</w> <w n="17.2">les</w> <w n="17.3">serments</w> <w n="17.4">furent</w> <w n="17.5">trahis</w>.</l>
						<l n="18" num="3.2"><w n="18.1">Les</w> <w n="18.2">souvenirs</w> <w n="18.3">sont</w> <w n="18.4">en</w> <w n="18.5">poussière</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Les</w> <w n="19.2">midis</w> <w n="19.3">éteints</w> <w n="19.4">et</w> <w n="19.5">les</w> <w n="19.6">nuits</w></l>
						<l n="20" num="3.4"><w n="20.1">Pleines</w> <w n="20.2">de</w> <w n="20.3">terreurs</w> <w n="20.4">et</w> <w n="20.5">de</w> <w n="20.6">bruits</w>.</l>
						<l n="21" num="3.5"><w n="21.1">Qui</w> <w n="21.2">fut</w> <w n="21.3">la</w> <w n="21.4">châtelaine</w> <w n="21.5">altière</w> ?</l>
						<l n="22" num="3.6"><w n="22.1">Pastels</w> <w n="22.2">que</w> <w n="22.3">la</w> <w n="22.4">pluie</w> <w n="22.5">a</w> <w n="22.6">lavés</w></l>
						<l n="23" num="3.7"><w n="23.1">Restez</w> <w n="23.2">muets</w> <w n="23.3">sur</w> <w n="23.4">ce</w> <w n="23.5">mystère</w>.</l>
						<l n="24" num="3.8"><w n="24.1">L</w>’<w n="24.2">herbe</w> <w n="24.3">pousse</w> <w n="24.4">entre</w> <w n="24.5">les</w> <w n="24.6">pavés</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">ENVOI</head>
						<l n="25" num="4.1"><w n="25.1">Prince</w>, <w n="25.2">à</w> <w n="25.3">jamais</w> <w n="25.4">faites</w>-<w n="25.5">moi</w> <w n="25.6">taire</w> ;</l>
						<l n="26" num="4.2"><w n="26.1">Rasez</w> <w n="26.2">tous</w> <w n="26.3">ces</w> <w n="26.4">murs</w> <w n="26.5">excavés</w></l>
						<l n="27" num="4.3"><w n="27.1">Et</w> <w n="27.2">semez</w> <w n="27.3">du</w> <w n="27.4">sel</w> <w n="27.5">dans</w> <w n="27.6">la</w> <w n="27.7">terre</w>.</l>
						<l n="28" num="4.4"><w n="28.1">L</w>’<w n="28.2">herbe</w> <w n="28.3">pousse</w> <w n="28.4">entre</w> <w n="28.5">les</w> <w n="28.6">pavés</w>.</l>
					</lg>
				</div></body></text></TEI>