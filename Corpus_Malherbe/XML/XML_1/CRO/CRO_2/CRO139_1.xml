<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FANTAISIES TRAGIQUES</head><div type="poem" key="CRO139">
					<head type="main">Berceuse</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">y</w> <w n="1.3">a</w> <w n="1.4">une</w> <w n="1.5">heure</w> <w n="1.6">bête</w></l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">Où</w> <w n="2.2">il</w> <w n="2.3">faut</w> <w n="2.4">dormir</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">y</w> <w n="3.3">a</w> <w n="3.4">aussi</w> <w n="3.5">la</w> <w n="3.6">fête</w></l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Où</w> <w n="4.2">il</w> <w n="4.3">faut</w> <w n="4.4">jouir</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">quand</w> <w n="5.3">tu</w> <w n="5.4">penches</w> <w n="5.5">la</w> <w n="5.6">tête</w></l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1">Avec</w> <w n="6.2">un</w> <w n="6.3">soupir</w></l>
						<l n="7" num="2.3"><w n="7.1">Sur</w> <w n="7.2">mon</w> <w n="7.3">cœur</w>, <w n="7.4">mon</w> <w n="7.5">cœur</w> <w n="7.6">s</w>’<w n="7.7">arrête</w></l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">Et</w> <w n="8.2">je</w> <w n="8.3">vais</w> <w n="8.4">mourir</w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Non</w> ! <w n="9.2">ravi</w> <w n="9.3">de</w> <w n="9.4">tes</w> <w n="9.5">mensonges</w>,</l>
						<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">Ô</w> <w n="10.2">fille</w> <w n="10.3">des</w> <w n="10.4">loups</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">m</w>’<w n="11.3">endors</w> <w n="11.4">noyé</w> <w n="11.5">de</w> <w n="11.6">songes</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><space quantity="8" unit="char"></space><w n="12.1">Entre</w> <w n="12.2">tes</w> <w n="12.3">genoux</w>.</l>
						<l n="13" num="4.2"><w n="13.1">Après</w> <w n="13.2">mon</w> <w n="13.3">cœur</w> <w n="13.4">que</w> <w n="13.5">tu</w> <w n="13.6">ronges</w></l>
						<l n="14" num="4.3"><space quantity="8" unit="char"></space><w n="14.1">Que</w> <w n="14.2">mangerons</w>-<w n="14.3">nous</w> ?</l>
					</lg>
				</div></body></text></TEI>