<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO129">
					<head type="main">Rêve</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Oh</w> ! <w n="1.2">la</w> <w n="1.3">fleur</w> <w n="1.4">de</w> <w n="1.5">lys</w> !</l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">noble</w> <w n="2.3">fleur</w> <w n="2.4">blanche</w>,</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">fleur</w> <w n="3.3">qui</w> <w n="3.4">se</w> <w n="3.5">penche</w></l>
						<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">nos</w> <w n="4.3">fronts</w> <w n="4.4">pâlis</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Son</w> <w n="5.2">parfum</w> <w n="5.3">suave</w></l>
						<l n="6" num="2.2"><w n="6.1">Plus</w> <w n="6.2">doux</w> <w n="6.3">que</w> <w n="6.4">le</w> <w n="6.5">miel</w></l>
						<l n="7" num="2.3"><w n="7.1">Raconte</w> <w n="7.2">le</w> <w n="7.3">ciel</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Console</w> <w n="8.2">l</w>’<w n="8.3">esclave</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Son</w> <w n="9.2">luxe</w> <w n="9.3">éclatant</w></l>
						<l n="10" num="3.2"><w n="10.1">Dans</w> <w n="10.2">la</w> <w n="10.3">saison</w> <w n="10.4">douce</w></l>
						<l n="11" num="3.3"><w n="11.1">Pousse</w>, <w n="11.2">pousse</w>, <w n="11.3">pousse</w>.</l>
						<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">nous</w> <w n="12.3">orne</w> <w n="12.4">autant</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">rose</w> <w n="13.3">est</w> <w n="13.4">coquette</w> ;</l>
						<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">glaïeul</w> <w n="14.3">sanglant</w></l>
						<l n="15" num="4.3"><w n="15.1">Mais</w> <w n="15.2">le</w> <w n="15.3">lys</w> <w n="15.4">est</w> <w n="15.5">blanc</w></l>
						<l n="16" num="4.4"><w n="16.1">Pour</w> <w n="16.2">la</w> <w n="16.3">grande</w> <w n="16.4">fête</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Oh</w> ! <w n="17.2">le</w> <w n="17.3">temps</w> <w n="17.4">des</w> <w n="17.5">rois</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Des</w> <w n="18.2">grands</w> <w n="18.3">capitaines</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Des</w> <w n="19.2">phrases</w> <w n="19.3">hautaines</w></l>
						<l n="20" num="5.4"><w n="20.1">Aux</w> <w n="20.2">étrangers</w> <w n="20.3">froids</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Le</w> <w n="21.2">printemps</w> <w n="21.3">s</w>’<w n="21.4">apprête</w> ;</l>
						<l n="22" num="6.2"><w n="22.1">Les</w> <w n="22.2">lys</w> <w n="22.3">vont</w> <w n="22.4">fleurir</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Oh</w> ! <w n="23.2">ne</w> <w n="23.3">pas</w> <w n="23.4">mourir</w></l>
						<l n="24" num="6.4"><w n="24.1">Avant</w> <w n="24.2">cette</w> <w n="24.3">fête</w>.</l>
					</lg>
				</div></body></text></TEI>