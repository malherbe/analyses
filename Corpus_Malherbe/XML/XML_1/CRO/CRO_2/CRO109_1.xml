<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VISIONS</head><div type="poem" key="CRO109">
					<head type="main">Inscription</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">âme</w> <w n="1.3">est</w> <w n="1.4">comme</w> <w n="1.5">un</w> <w n="1.6">ciel</w> <w n="1.7">sans</w> <w n="1.8">bornes</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Elle</w> <w n="2.2">a</w> <w n="2.3">des</w> <w n="2.4">immensités</w> <w n="2.5">mornes</w></l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">d</w>’<w n="3.3">innombrables</w> <w n="3.4">soleils</w> <w n="3.5">clairs</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Aussi</w>, <w n="4.2">malgré</w> <w n="4.3">le</w> <w n="4.4">mal</w>, <w n="4.5">ma</w> <w n="4.6">vie</w></l>
						<l n="5" num="1.5"><w n="5.1">De</w> <w n="5.2">tant</w> <w n="5.3">de</w> <w n="5.4">diamants</w> <w n="5.5">ravie</w></l>
						<l n="6" num="1.6"><w n="6.1">Se</w> <w n="6.2">mire</w> <w n="6.3">au</w> <w n="6.4">ruisseau</w> <w n="6.5">de</w> <w n="6.6">mes</w> <w n="6.7">vers</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Je</w> <w n="7.2">dirai</w> <w n="7.3">donc</w> <w n="7.4">en</w> <w n="7.5">ces</w> <w n="7.6">paroles</w></l>
						<l n="8" num="2.2"><w n="8.1">Mes</w> <w n="8.2">visions</w> <w n="8.3">qu</w>’<w n="8.4">on</w> <w n="8.5">croyait</w> <w n="8.6">folles</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Ma</w> <w n="9.2">réponse</w> <w n="9.3">aux</w> <w n="9.4">mondes</w> <w n="9.5">lointains</w></l>
						<l n="10" num="2.4"><w n="10.1">Qui</w> <w n="10.2">nous</w> <w n="10.3">adressaient</w> <w n="10.4">leurs</w> <w n="10.5">messages</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Éclairs</w> <w n="11.2">incompris</w> <w n="11.3">de</w> <w n="11.4">nos</w> <w n="11.5">sages</w></l>
						<l n="12" num="2.6"><w n="12.1">Et</w> <w n="12.2">qui</w>, <w n="12.3">lassés</w>, <w n="12.4">se</w> <w n="12.5">sont</w> <w n="12.6">éteints</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Dans</w> <w n="13.2">ma</w> <w n="13.3">recherche</w> <w n="13.4">coutumière</w></l>
						<l n="14" num="3.2"><w n="14.1">Tous</w> <w n="14.2">les</w> <w n="14.3">secrets</w> <w n="14.4">de</w> <w n="14.5">la</w> <w n="14.6">lumière</w>,</l>
						<l n="15" num="3.3"><w n="15.1">Tous</w> <w n="15.2">les</w> <w n="15.3">mystères</w> <w n="15.4">du</w> <w n="15.5">cerveau</w>,</l>
						<l n="16" num="3.4"><w n="16.1">J</w>’<w n="16.2">ai</w> <w n="16.3">tout</w> <w n="16.4">fouillé</w>, <w n="16.5">j</w>’<w n="16.6">ai</w> <w n="16.7">su</w> <w n="16.8">tout</w> <w n="16.9">dire</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Faire</w> <w n="17.2">pleurer</w> <w n="17.3">et</w> <w n="17.4">faire</w> <w n="17.5">rire</w></l>
						<l n="18" num="3.6"><w n="18.1">Et</w> <w n="18.2">montrer</w> <w n="18.3">le</w> <w n="18.4">monde</w> <w n="18.5">nouveau</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">J</w>’<w n="19.2">ai</w> <w n="19.3">voulu</w> <w n="19.4">que</w> <w n="19.5">les</w> <w n="19.6">tons</w>, <w n="19.7">la</w> <w n="19.8">grâce</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Tout</w> <w n="20.2">ce</w> <w n="20.3">que</w> <w n="20.4">reflète</w> <w n="20.5">une</w> <w n="20.6">glace</w>,</l>
						<l n="21" num="4.3"><w n="21.1">L</w>’<w n="21.2">ivresse</w> <w n="21.3">d</w>’<w n="21.4">un</w> <w n="21.5">bal</w> <w n="21.6">d</w>’<w n="21.7">opéra</w>,</l>
						<l n="22" num="4.4"><w n="22.1">Les</w> <w n="22.2">soirs</w> <w n="22.3">de</w> <w n="22.4">rubis</w>, <w n="22.5">l</w>’<w n="22.6">ombre</w> <w n="22.7">verte</w></l>
						<l n="23" num="4.5"><w n="23.1">Se</w> <w n="23.2">fixent</w> <w n="23.3">sur</w> <w n="23.4">la</w> <w n="23.5">plaque</w> <w n="23.6">inerte</w>.</l>
						<l n="24" num="4.6"><w n="24.1">Je</w> <w n="24.2">l</w>’<w n="24.3">ai</w> <w n="24.4">voulu</w>, <w n="24.5">cela</w> <w n="24.6">sera</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Comme</w> <w n="25.2">les</w> <w n="25.3">traits</w> <w n="25.4">dans</w> <w n="25.5">les</w> <w n="25.6">camées</w></l>
						<l n="26" num="5.2"><w n="26.1">J</w>’<w n="26.2">ai</w> <w n="26.3">voulu</w> <w n="26.4">que</w> <w n="26.5">les</w> <w n="26.6">voix</w> <w n="26.7">aimées</w></l>
						<l n="27" num="5.3"><w n="27.1">Soient</w> <w n="27.2">un</w> <w n="27.3">bien</w>, <w n="27.4">qu</w>’<w n="27.5">on</w> <w n="27.6">garde</w> <w n="27.7">à</w> <w n="27.8">jamais</w>,</l>
						<l n="28" num="5.4"><w n="28.1">Et</w> <w n="28.2">puissent</w> <w n="28.3">répéter</w> <w n="28.4">le</w> <w n="28.5">rêve</w></l>
						<l n="29" num="5.5"><w n="29.1">Musical</w> <w n="29.2">de</w> <w n="29.3">l</w>’<w n="29.4">heure</w> <w n="29.5">trop</w> <w n="29.6">brève</w> ;</l>
						<l n="30" num="5.6"><w n="30.1">Le</w> <w n="30.2">temps</w> <w n="30.3">veut</w> <w n="30.4">fuir</w>, <w n="30.5">je</w> <w n="30.6">le</w> <w n="30.7">soumets</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Et</w> <w n="31.2">les</w> <w n="31.3">hommes</w>, <w n="31.4">sans</w> <w n="31.5">ironie</w>,</l>
						<l n="32" num="6.2"><w n="32.1">Diront</w> <w n="32.2">que</w> <w n="32.3">j</w>’<w n="32.4">avais</w> <w n="32.5">du</w> <w n="32.6">génie</w></l>
						<l n="33" num="6.3"><w n="33.1">Et</w>, <w n="33.2">dans</w> <w n="33.3">les</w> <w n="33.4">siècles</w> <w n="33.5">apaisés</w>,</l>
						<l n="34" num="6.4"><w n="34.1">Les</w> <w n="34.2">femmes</w> <w n="34.3">diront</w> <w n="34.4">que</w> <w n="34.5">mes</w> <w n="34.6">lèvres</w>,</l>
						<l n="35" num="6.5"><w n="35.1">Malgré</w> <w n="35.2">les</w> <w n="35.3">luttes</w> <w n="35.4">et</w> <w n="35.5">les</w> <w n="35.6">fièvres</w>,</l>
						<l n="36" num="6.6"><w n="36.1">Savaient</w> <w n="36.2">les</w> <w n="36.3">suprêmes</w> <w n="36.4">baisers</w>.</l>
					</lg>
				</div></body></text></TEI>