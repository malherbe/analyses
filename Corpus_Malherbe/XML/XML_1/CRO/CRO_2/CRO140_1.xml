<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FANTAISIES TRAGIQUES</head><div type="poem" key="CRO140">
					<head type="main">Liberté</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">vent</w> <w n="1.3">impur</w> <w n="1.4">des</w> <w n="1.5">étables</w></l>
						<l n="2" num="1.2"><w n="2.1">Vient</w> <w n="2.2">d</w>’<w n="2.3">Ouest</w>, <w n="2.4">d</w>’<w n="2.5">Est</w>, <w n="2.6">du</w> <w n="2.7">Sud</w>, <w n="2.8">du</w> <w n="2.9">Nord</w>.</l>
						<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">ne</w> <w n="3.3">s</w>’<w n="3.4">assied</w> <w n="3.5">plus</w> <w n="3.6">aux</w> <w n="3.7">tables</w></l>
						<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">heureux</w>, <w n="4.3">puisqu</w>’<w n="4.4">on</w> <w n="4.5">est</w> <w n="4.6">mort</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">princesses</w> <w n="5.3">aux</w> <w n="5.4">beaux</w> <w n="5.5">râbles</w></l>
						<l n="6" num="2.2"><w n="6.1">Offrent</w> <w n="6.2">leurs</w> <w n="6.3">plus</w> <w n="6.4">doux</w> <w n="6.5">trésors</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">on</w> <w n="7.3">s</w>’<w n="7.4">en</w> <w n="7.5">va</w> <w n="7.6">dans</w> <w n="7.7">les</w> <w n="7.8">sables</w></l>
						<l n="8" num="2.4"><w n="8.1">Oublié</w>, <w n="8.2">méprisé</w>, <w n="8.3">fort</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">On</w> <w n="9.2">peut</w> <w n="9.3">regarder</w> <w n="9.4">la</w> <w n="9.5">lune</w></l>
						<l n="10" num="3.2"><w n="10.1">Tranquille</w> <w n="10.2">dans</w> <w n="10.3">le</w> <w n="10.4">ciel</w> <w n="10.5">noir</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">quelle</w> <w n="11.3">morale</w> ?… <w n="11.4">aucune</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Je</w> <w n="12.2">me</w> <w n="12.3">console</w> <w n="12.4">à</w> <w n="12.5">vous</w> <w n="12.6">voir</w>,</l>
						<l n="13" num="4.2"><w n="13.1">À</w> <w n="13.2">vous</w> <w n="13.3">étreindre</w> <w n="13.4">ce</w> <w n="13.5">soir</w></l>
						<l n="14" num="4.3"><w n="14.1">Amie</w> <w n="14.2">éclatante</w> <w n="14.3">et</w> <w n="14.4">brune</w>.</l>
					</lg>
				</div></body></text></TEI>