<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSE</head><div type="poem" key="CRO163">
					<head type="main">Lilas</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ma</w> <w n="1.2">maîtresse</w> <w n="1.3">me</w> <w n="1.4">fait</w> <w n="1.5">des</w> <w n="1.6">scènes</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Paradis</w> <w n="2.2">fleuri</w> <w n="2.3">de</w> <w n="2.4">lilas</w></l>
						<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">viens</w> <w n="3.3">humer</w> <w n="3.4">tes</w> <w n="3.5">odeurs</w> <w n="3.6">saines</w>.</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">Les</w> <w n="4.2">moribonds</w> <w n="4.3">disent</w> : <w n="4.4">Hélas</w> !</l>
						<l n="5" num="2.2"><w n="5.1">Les</w> <w n="5.2">vieux</w> <w n="5.3">disent</w> <w n="5.4">des</w> <w n="5.5">mots</w> <w n="5.6">obscènes</w></l>
						<l n="6" num="2.3"><w n="6.1">Pour</w> <w n="6.2">couvrir</w> <w n="6.3">le</w> <w n="6.4">bruit</w> <w n="6.5">de</w> <w n="6.6">leurs</w> <w n="6.7">glas</w>.</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">Dans</w> <w n="7.2">le</w> <w n="7.3">bois</w> <w n="7.4">de</w> <w n="7.5">pins</w> <w n="7.6">et</w> <w n="7.7">de</w> <w n="7.8">chênes</w></l>
						<l n="8" num="3.2"><w n="8.1">Les</w> <w n="8.2">obus</w> <w n="8.3">jettent</w> <w n="8.4">leurs</w> <w n="8.5">éclats</w>.</l>
						<l n="9" num="3.3"><w n="9.1">Victoire</w> ? <w n="9.2">Défaite</w> ? <w n="9.3">Phalènes</w>.</l>
						<l n="10" num="3.4"><w n="10.1">Pluie</w> <w n="10.2">améthyste</w> <w n="10.3">les</w> <w n="10.4">lilas</w>,</l>
						<l n="11" num="3.5"><w n="11.1">Sans</w> <w n="11.2">souci</w> <w n="11.3">d</w>’<w n="11.4">ambitions</w> <w n="11.5">vaines</w>,</l>
						<l n="12" num="3.6"><w n="12.1">Offrent</w> <w n="12.2">aux</w> <w n="12.3">plus</w> <w n="12.4">gueux</w> <w n="12.5">leurs</w> <w n="12.6">galas</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">mer</w>, <w n="13.3">les</w> <w n="13.4">montagnes</w>, <w n="13.5">les</w> <w n="13.6">plaines</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Tout</w> <w n="14.2">est</w> <w n="14.3">oublié</w>. <w n="14.4">Je</w> <w n="14.5">suis</w> <w n="14.6">las</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Las</w> <w n="15.2">de</w> <w n="15.3">la</w> <w n="15.4">bêtise</w> <w n="15.5">et</w> <w n="15.6">des</w> <w n="15.7">haines</w>.</l>
					</lg>
					<lg n="5">
						<l n="16" num="5.1"><w n="16.1">Mais</w> <w n="16.2">mon</w> <w n="16.3">cœur</w> <w n="16.4">renaît</w> <w n="16.5">aux</w> <w n="16.6">lilas</w>.</l>
					</lg>
				</div></body></text></TEI>