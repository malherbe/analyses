<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE COLLIER DE GRIFFES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRO">
					<name>
						<forename>Charles</forename>
						<surname>CROS</surname>
					</name>
					<date from="1842" to="1888">1842-1888</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1358 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">CRO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/charlescroscoliersdegriffes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LE COLLIER DE GRIFFES</title>
						<author>Charles Cros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>P.-V. STOCK, ÉDITEUR</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1908">1908</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-14" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TENDRESSE</head><div type="poem" key="CRO165">
					<head type="main">A ma femme endormie</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">dors</w> <w n="1.3">en</w> <w n="1.4">croyant</w> <w n="1.5">que</w> <w n="1.6">mes</w> <w n="1.7">vers</w></l>
						<l n="2" num="1.2"><w n="2.1">Vont</w> <w n="2.2">encombrer</w> <w n="2.3">tout</w> <w n="2.4">l</w>’<w n="2.5">univers</w></l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">désastres</w> <w n="3.3">et</w> <w n="3.4">d</w>’<w n="3.5">incendies</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Elles</w> <w n="4.2">sont</w> <w n="4.3">si</w> <w n="4.4">rares</w> <w n="4.5">pourtant</w></l>
						<l n="5" num="1.5"><w n="5.1">Mes</w> <w n="5.2">chansons</w> <w n="5.3">au</w> <w n="5.4">soleil</w> <w n="5.5">couchant</w></l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">mes</w> <w n="6.3">lointaines</w> <w n="6.4">mélodies</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Mais</w> <w n="7.2">si</w> <w n="7.3">je</w> <w n="7.4">dérange</w> <w n="7.5">parfois</w></l>
						<l n="8" num="2.2"><w n="8.1">La</w> <w n="8.2">sérénité</w> <w n="8.3">des</w> <w n="8.4">cieux</w> <w n="8.5">froids</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Si</w> <w n="9.2">des</w> <w n="9.3">sons</w> <w n="9.4">d</w>’<w n="9.5">acier</w> <w n="9.6">et</w> <w n="9.7">de</w> <w n="9.8">cuivre</w></l>
						<l n="10" num="2.4"><w n="10.1">Ou</w> <w n="10.2">d</w>’<w n="10.3">or</w>, <w n="10.4">vibrent</w> <w n="10.5">dans</w> <w n="10.6">mes</w> <w n="10.7">chansons</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Pardonne</w> <w n="11.2">ces</w> <w n="11.3">hautes</w> <w n="11.4">façons</w>,</l>
						<l n="12" num="2.6"><w n="12.1">C</w>’<w n="12.2">est</w> <w n="12.3">que</w> <w n="12.4">je</w> <w n="12.5">me</w> <w n="12.6">hâte</w> <w n="12.7">de</w> <w n="12.8">vivre</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Et</w> <w n="13.2">puis</w> <w n="13.3">tu</w> <w n="13.4">m</w>’<w n="13.5">aimeras</w> <w n="13.6">toujours</w>.</l>
						<l n="14" num="3.2"><w n="14.1">Éternelles</w> <w n="14.2">sont</w> <w n="14.3">les</w> <w n="14.4">amours</w></l>
						<l n="15" num="3.3"><w n="15.1">Dont</w> <w n="15.2">ma</w> <w n="15.3">mémoire</w> <w n="15.4">est</w> <w n="15.5">le</w> <w n="15.6">repaire</w></l>
						<l n="16" num="3.4"><w n="16.1">Nos</w> <w n="16.2">enfants</w> <w n="16.3">seront</w> <w n="16.4">de</w> <w n="16.5">fiers</w> <w n="16.6">gas</w></l>
						<l n="17" num="3.5"><w n="17.1">Qui</w> <w n="17.2">répareront</w> <w n="17.3">les</w> <w n="17.4">dégâts</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Que</w> <w n="18.2">dans</w> <w n="18.3">ta</w> <w n="18.4">vie</w> <w n="18.5">a</w> <w n="18.6">fait</w> <w n="18.7">leur</w> <w n="18.8">père</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Ils</w> <w n="19.2">dorment</w> <w n="19.3">sans</w> <w n="19.4">rêver</w> <w n="19.5">à</w> <w n="19.6">rien</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Dans</w> <w n="20.2">le</w> <w n="20.3">nuage</w> <w n="20.4">aérien</w></l>
						<l n="21" num="4.3"><w n="21.1">Des</w> <w n="21.2">cheveux</w> <w n="21.3">sur</w> <w n="21.4">leurs</w> <w n="21.5">fines</w> <w n="21.6">têtes</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">toi</w>, <w n="22.3">près</w> <w n="22.4">d</w>’<w n="22.5">eux</w>, <w n="22.6">tu</w> <w n="22.7">dors</w> <w n="22.8">aussi</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Ayant</w> <w n="23.2">oublié</w> <w n="23.3">le</w> <w n="23.4">souci</w></l>
						<l n="24" num="4.6"><w n="24.1">De</w> <w n="24.2">tout</w> <w n="24.3">travail</w>, <w n="24.4">de</w> <w n="24.5">toutes</w> <w n="24.6">dettes</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Moi</w> <w n="25.2">je</w> <w n="25.3">veille</w> <w n="25.4">et</w> <w n="25.5">je</w> <w n="25.6">fais</w> <w n="25.7">ces</w> <w n="25.8">vers</w></l>
						<l n="26" num="5.2"><w n="26.1">Qui</w> <w n="26.2">laisseront</w> <w n="26.3">tout</w> <w n="26.4">l</w>’<w n="26.5">univers</w></l>
						<l n="27" num="5.3"><w n="27.1">Sans</w> <w n="27.2">désastre</w> <w n="27.3">et</w> <w n="27.4">sans</w> <w n="27.5">incendie</w> ;</l>
						<l n="28" num="5.4"><w n="28.1">Et</w> <w n="28.2">demain</w>, <w n="28.3">au</w> <w n="28.4">soleil</w> <w n="28.5">montant</w></l>
						<l n="29" num="5.5"><w n="29.1">Tu</w> <w n="29.2">souriras</w> <w n="29.3">en</w> <w n="29.4">écoutant</w></l>
						<l n="30" num="5.6"><w n="30.1">Cette</w> <w n="30.2">tranquille</w> <w n="30.3">mélodie</w>.</l>
					</lg>
				</div></body></text></TEI>