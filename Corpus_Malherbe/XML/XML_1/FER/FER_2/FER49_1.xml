<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FEMMES RÊVÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>234 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Femmes Rêvées</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/12365</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Femmes Rêvées</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://archive.org/details/femmesrves00ferl</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Chez l’auteur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1899">1899</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FEMMES RÊVÉES</head><div type="poem" key="FER49">
					<head type="main">Chant des Pleureuses</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ayons</w> <w n="1.2">comme</w> <w n="1.3">les</w> <w n="1.4">jours</w> <w n="1.5">de</w> <w n="1.6">la</w> <w n="1.7">triste</w> <w n="1.8">saison</w></l>
						<l n="2" num="1.2"><w n="2.1">Nos</w> <w n="2.2">heures</w> <w n="2.3">de</w> <w n="2.4">soleil</w> <w n="2.5">et</w> <w n="2.6">de</w> <w n="2.7">mélancolie</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Autant</w> <w n="3.2">qu</w>’<w n="3.3">il</w> <w n="3.4">nous</w> <w n="3.5">est</w> <w n="3.6">doux</w> <w n="3.7">de</w> <w n="3.8">rire</w> <w n="3.9">à</w> <w n="3.10">la</w> <w n="3.11">folie</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">il</w> <w n="4.3">nous</w> <w n="4.4">plaise</w> <w n="4.5">parfois</w> <w n="4.6">de</w> <w n="4.7">pleurer</w> <w n="4.8">sans</w> <w n="4.9">raison</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="4"></space><w n="5.1">Pleurons</w>, <w n="5.2">pleurons</w> <w n="5.3">pleureuses</w> <w n="5.4">que</w> <w n="5.5">nous</w> <w n="5.6">sommes</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1">Pleurons</w>, <w n="6.2">pleurons</w>, <w n="6.3">loin</w> <w n="6.4">du</w> <w n="6.5">regard</w> <w n="6.6">des</w> <w n="6.7">hommes</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Pleurons</w> <w n="7.2">quand</w> <w n="7.3">la</w> <w n="7.4">tristesse</w> <w n="7.5">enténèbre</w> <w n="7.6">nos</w> <w n="7.7">yeux</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Pleurons</w> <w n="8.2">lorsque</w> <w n="8.3">le</w> <w n="8.4">cœur</w> <w n="8.5">s</w>’<w n="8.6">énamoure</w> <w n="8.7">et</w> <w n="8.8">s</w>’<w n="8.9">ennuie</w> ;</l>
						<l n="9" num="2.5"><w n="9.1">Que</w> <w n="9.2">nos</w> <w n="9.3">chagrins</w>, <w n="9.4">pareils</w> <w n="9.5">aux</w> <w n="9.6">nuages</w> <w n="9.7">des</w> <w n="9.8">cieux</w>,</l>
						<l n="10" num="2.6"><w n="10.1">Se</w> <w n="10.2">dissipent</w> <w n="10.3">en</w> <w n="10.4">pleurs</w> <w n="10.5">comme</w> <w n="10.6">ils</w> <w n="10.7">tombent</w> <w n="10.8">en</w> <w n="10.9">pluie</w> !</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">Qu</w>’<w n="11.2">il</w> <w n="11.3">est</w> <w n="11.4">plaisant</w> <w n="11.5">de</w> <w n="11.6">voir</w>, <w n="11.7">ainsi</w> <w n="11.8">que</w> <w n="11.9">brusquement</w></l>
						<l n="12" num="3.2"><w n="12.1">S</w>’<w n="12.2">ensoleille</w> <w n="12.3">en</w> <w n="12.4">avril</w> <w n="12.5">l</w>’<w n="12.6">azur</w> <w n="12.7">après</w> <w n="12.8">l</w>’<w n="12.9">ondée</w>,</l>
						<l n="13" num="3.3"><w n="13.1">Une</w> <w n="13.2">pleureuse</w> <w n="13.3">encor</w> <w n="13.4">de</w> <w n="13.5">larmes</w> <w n="13.6">inondée</w></l>
						<l n="14" num="3.4"><w n="14.1">S</w>’<w n="14.2">illuminer</w> <w n="14.3">soudain</w> <w n="14.4">d</w>’<w n="14.5">un</w> <w n="14.6">sourire</w> <w n="14.7">charmant</w> !</l>
					</lg>
					<lg n="4">
						<l n="15" num="4.1"><space unit="char" quantity="4"></space><w n="15.1">Pleurons</w>, <w n="15.2">pleurons</w> <w n="15.3">pleureuses</w> <w n="15.4">que</w> <w n="15.5">nous</w> <w n="15.6">sommes</w></l>
						<l n="16" num="4.2"><space unit="char" quantity="4"></space><w n="16.1">Pleurons</w>, <w n="16.2">pleurons</w>, <w n="16.3">loin</w> <w n="16.4">du</w> <w n="16.5">regard</w> <w n="16.6">des</w> <w n="16.7">hommes</w>,</l>
						<l n="17" num="4.3"><w n="17.1">Pleurons</w> <w n="17.2">quand</w> <w n="17.3">la</w> <w n="17.4">tristesse</w> <w n="17.5">enténèbre</w> <w n="17.6">nos</w> <w n="17.7">yeux</w>,</l>
						<l n="18" num="4.4"><w n="18.1">Pleurons</w> <w n="18.2">lorsque</w> <w n="18.3">le</w> <w n="18.4">cœur</w> <w n="18.5">s</w>’<w n="18.6">énamoure</w> <w n="18.7">et</w> <w n="18.8">s</w>’<w n="18.9">ennuie</w> ;</l>
						<l n="19" num="4.5"><w n="19.1">Que</w> <w n="19.2">nos</w> <w n="19.3">chagrins</w>, <w n="19.4">pareils</w> <w n="19.5">aux</w> <w n="19.6">nuages</w> <w n="19.7">des</w> <w n="19.8">cieux</w>,</l>
						<l n="20" num="4.6"><w n="20.1">Se</w> <w n="20.2">dissipent</w> <w n="20.3">en</w> <w n="20.4">pleurs</w> <w n="20.5">comme</w> <w n="20.6">ils</w> <w n="20.7">tombent</w> <w n="20.8">en</w> <w n="20.9">pluie</w> !</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">Dolentes</w> <w n="21.2">et</w> <w n="21.3">les</w> <w n="21.4">yeux</w> <w n="21.5">empreints</w> <w n="21.6">de</w> <w n="21.7">nonchaloir</w></l>
						<l n="22" num="5.2"><w n="22.1">Sachons</w> <w n="22.2">parfois</w>, <w n="22.3">ainsi</w> <w n="22.4">qu</w>’<w n="22.5">à</w> <w n="22.6">l</w>’<w n="22.7">ombre</w> <w n="22.8">des</w> <w n="22.9">platanes</w></l>
						<l n="23" num="5.3"><w n="23.1">Le</w> <w n="23.2">cœur</w> <w n="23.3">alangouri</w> <w n="23.4">soupirent</w> <w n="23.5">les</w> <w n="23.6">sultanes</w>,</l>
						<l n="24" num="5.4"><w n="24.1">Même</w> <w n="24.2">au</w> <w n="24.3">doux</w> <w n="24.4">mois</w> <w n="24.5">des</w> <w n="24.6">fleurs</w> <w n="24.7">gémir</w> <w n="24.8">et</w> <w n="24.9">nous</w> <w n="24.10">doloir</w>.</l>
					</lg>
					<lg n="6">
						<l n="25" num="6.1"><space unit="char" quantity="4"></space><w n="25.1">Pleurons</w>, <w n="25.2">pleurons</w> <w n="25.3">pleureuses</w> <w n="25.4">que</w> <w n="25.5">nous</w> <w n="25.6">sommes</w></l>
						<l n="26" num="6.2"><space unit="char" quantity="4"></space><w n="26.1">Pleurons</w>, <w n="26.2">pleurons</w>, <w n="26.3">loin</w> <w n="26.4">du</w> <w n="26.5">regard</w> <w n="26.6">des</w> <w n="26.7">hommes</w>,</l>
						<l n="27" num="6.3"><w n="27.1">Pleurons</w> <w n="27.2">quand</w> <w n="27.3">la</w> <w n="27.4">tristesse</w> <w n="27.5">enténèbre</w> <w n="27.6">nos</w> <w n="27.7">yeux</w>,</l>
						<l n="28" num="6.4"><w n="28.1">Pleurons</w> <w n="28.2">lorsque</w> <w n="28.3">le</w> <w n="28.4">cœur</w> <w n="28.5">s</w>’<w n="28.6">énamoure</w> <w n="28.7">et</w> <w n="28.8">s</w>’<w n="28.9">ennuie</w> ;</l>
						<l n="29" num="6.5"><w n="29.1">Que</w> <w n="29.2">nos</w> <w n="29.3">chagrins</w>, <w n="29.4">pareils</w> <w n="29.5">aux</w> <w n="29.6">nuages</w> <w n="29.7">des</w> <w n="29.8">cieux</w>,</l>
						<l n="30" num="6.6"><w n="30.1">Se</w> <w n="30.2">dissipent</w> <w n="30.3">en</w> <w n="30.4">pleurs</w> <w n="30.5">comme</w> <w n="30.6">ils</w> <w n="30.7">tombent</w> <w n="30.8">en</w> <w n="30.9">pluie</w> !</l>
					</lg>
				</div></body></text></TEI>