<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">FEMMES RÊVÉES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>234 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Femmes Rêvées</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/12365</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Femmes Rêvées</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://archive.org/details/femmesrves00ferl</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Chez l’auteur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1899">1899</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FEMMES RÊVÉES</head><div type="poem" key="FER51">
					<head type="main">Les Préceptes de l’Amour</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Adolescent</w> <w n="1.2">ta</w> <w n="1.3">chair</w> <w n="1.4">dompteras</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Afin</w> <w n="2.2">de</w> <w n="2.3">vivre</w> <w n="2.4">longuement</w>.</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">Vierge</w> <w n="3.2">ton</w> <w n="3.3">corps</w> <w n="3.4">tu</w> <w n="3.5">garderas</w></l>
						<l n="4" num="2.2"><w n="4.1">Jusqu</w>’<w n="4.2">à</w> <w n="4.3">l</w>’<w n="4.4">hymen</w> <w n="4.5">jalousement</w></l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">Honnête</w> <w n="5.2">point</w> <w n="5.3">ne</w> <w n="5.4">marcheras</w></l>
						<l n="6" num="3.2"><w n="6.1">Devers</w> <w n="6.2">la</w> <w n="6.3">tombe</w> <w n="6.4">isolément</w>.</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">Nulle</w> <w n="7.2">femme</w> <w n="7.3">ne</w> <w n="7.4">connaîtras</w></l>
						<l n="8" num="4.2"><w n="8.1">Hors</w> <w n="8.2">de</w> <w n="8.3">l</w>’<w n="8.4">hymen</w> <w n="8.5">charnellement</w>.</l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1">Selon</w> <w n="9.2">ton</w> <w n="9.3">cœur</w> <w n="9.4">tu</w> <w n="9.5">choisiras</w></l>
						<l n="10" num="5.2"><w n="10.1">Une</w> <w n="10.2">femme</w> <w n="10.3">discrètement</w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1"><w n="11.1">Chrétien</w> <w n="11.2">tu</w> <w n="11.3">te</w> <w n="11.4">multiplieras</w></l>
						<l n="12" num="6.2"><w n="12.1">Par</w> <w n="12.2">le</w> <w n="12.3">sang</w> <w n="12.4">et</w> <w n="12.5">l</w>’<w n="12.6">enseignement</w>.</l>
					</lg>
				</div></body></text></TEI>