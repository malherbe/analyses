<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VERS L’IDÉAL</head><div type="poem" key="FER3">
					<head type="main">AU CIEL</head>
					<opener>
						<salute>À M J. N. FERLAND, P<hi rend="sup">tre</hi></salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Si</w>, <w n="1.2">comme</w> <w n="1.3">la</w> <w n="1.4">fumée</w> <w n="1.5">errante</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">l</w>’<w n="2.3">on</w> <w n="2.4">aime</w> <w n="2.5">à</w> <w n="2.6">voir</w> <w n="2.7">devenir</w></l>
						<l n="3" num="1.3"><w n="3.1">Plus</w> <w n="3.2">volage</w> <w n="3.3">et</w> <w n="3.4">plus</w> <w n="3.5">transparente</w></l>
						<l n="4" num="1.4"><w n="4.1">Sous</w> <w n="4.2">les</w> <w n="4.3">caresses</w> <w n="4.4">du</w> <w n="4.5">zéphyr</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Si</w>, <w n="5.2">comme</w> <w n="5.3">la</w> <w n="5.4">perle</w> <w n="5.5">brillante</w></l>
						<l n="6" num="2.2"><w n="6.1">Dont</w> <w n="6.2">on</w> <w n="6.3">vit</w> <w n="6.4">l</w>’<w n="6.5">aurore</w> <w n="6.6">embellir</w></l>
						<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">cou</w> <w n="7.3">de</w> <w n="7.4">la</w> <w n="7.5">timide</w> <w n="7.6">plante</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">le</w> <w n="8.3">rayon</w> <w n="8.4">vint</w> <w n="8.5">la</w> <w n="8.6">cueillir</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Si</w>, <w n="9.2">comme</w> <w n="9.3">l</w>’<w n="9.4">oiseau</w> <w n="9.5">de</w> <w n="9.6">la</w> <w n="9.7">grève</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Tu</w> <w n="10.2">veux</w> <w n="10.3">que</w> <w n="10.4">vers</w> <w n="10.5">toi</w> <w n="10.6">je</w> <w n="10.7">m</w>’<w n="10.8">élève</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Abaisse</w> <w n="11.2">l</w>’<w n="11.3">azur</w> <w n="11.4">jusqu</w>’<w n="11.5">à</w> <w n="11.6">moi</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Pour</w> <w n="12.2">que</w> <w n="12.3">dans</w> <w n="12.4">ses</w> <w n="12.5">franges</w> <w n="12.6">si</w> <w n="12.7">belles</w></l>
						<l n="13" num="4.2"><w n="13.1">Je</w> <w n="13.2">puisse</w> <w n="13.3">découper</w> <w n="13.4">les</w> <w n="13.5">ailes</w></l>
						<l n="14" num="4.3"><w n="14.1">Qu</w>’<w n="14.2">il</w> <w n="14.3">me</w> <w n="14.4">faut</w> <w n="14.5">pour</w> <w n="14.6">voler</w> <w n="14.7">vers</w> <w n="14.8">toi</w>.</l>
					</lg>
				</div></body></text></TEI>