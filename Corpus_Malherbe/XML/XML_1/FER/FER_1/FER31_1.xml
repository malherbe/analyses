<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VOIX INTÉRIEURES</head><div type="poem" key="FER31">
					<head type="main">STANCES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Comment</w> ! <w n="1.2">je</w> <w n="1.3">suis</w> <w n="1.4">poète</w> <w n="1.5">et</w> <w n="1.6">je</w> <w n="1.7">n</w>’<w n="1.8">oserai</w> <w n="1.9">dire</w>,</l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">peur</w> <w n="2.3">que</w> <w n="2.4">les</w> <w n="2.5">pervers</w>, <w n="2.6">les</w> <w n="2.7">sots</w> <w n="2.8">puissent</w> <w n="2.9">en</w> <w n="2.10">rire</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">je</w> <w n="3.3">reconnais</w> <w n="3.4">Dieu</w> <w n="3.5">pour</w> <w n="3.6">le</w> <w n="3.7">Maître</w> <w n="3.8">éternel</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">j</w>’<w n="4.3">adore</w> <w n="4.4">son</w> <w n="4.5">nom</w>, <w n="4.6">que</w> <w n="4.7">je</w> <w n="4.8">le</w> <w n="4.9">crains</w> <w n="4.10">et</w> <w n="4.11">l</w>’<w n="4.12">aime</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Que</w> <w n="5.2">j</w>’<w n="5.3">espère</w> <w n="5.4">toujours</w> <w n="5.5">en</w> <w n="5.6">sa</w> <w n="5.7">bonté</w> <w n="5.8">suprême</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">daigne</w> <w n="6.3">à</w> <w n="6.4">l</w>’<w n="6.5">homme</w> <w n="6.6">juste</w> <w n="6.7">ouvrir</w> <w n="6.8">son</w> <w n="6.9">vaste</w> <w n="6.10">ciel</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Non</w>, <w n="7.2">non</w>, <w n="7.3">mortels</w>, <w n="7.4">jamais</w> <w n="7.5">le</w> <w n="7.6">Dieu</w> <w n="7.7">saint</w> <w n="7.8">que</w> <w n="7.9">j</w>’<w n="7.10">adore</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Et</w> <w n="8.2">qu</w>’<w n="8.3">on</w> <w n="8.4">doit</w> <w n="8.5">respecter</w> <w n="8.6">du</w> <w n="8.7">couchant</w> <w n="8.8">à</w> <w n="8.9">l</w>’<w n="8.10">aurore</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Ne</w> <w n="9.2">me</w> <w n="9.3">verra</w> <w n="9.4">rougir</w> <w n="9.5">disant</w> <w n="9.6">son</w> <w n="9.7">nom</w> <w n="9.8">si</w> <w n="9.9">grand</w> !</l>
						<l n="10" num="2.4"><w n="10.1">Avec</w> <w n="10.2">le</w> <w n="10.3">jour</w>, <w n="10.4">la</w> <w n="10.5">nuit</w>, <w n="10.6">le</w> <w n="10.7">feu</w>, <w n="10.8">les</w> <w n="10.9">vents</w>, <w n="10.10">les</w> <w n="10.11">ondes</w>,</l>
						<l n="11" num="2.5"><w n="11.1">La</w> <w n="11.2">terre</w>, <w n="11.3">les</w> <w n="11.4">cieux</w> <w n="11.5">bleus</w>, <w n="11.6">les</w> <w n="11.7">soleils</w> <w n="11.8">et</w> <w n="11.9">les</w> <w n="11.10">mondes</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Je</w> <w n="12.2">le</w> <w n="12.3">dirai</w> <w n="12.4">toujours</w> <w n="12.5">et</w> <w n="12.6">toujours</w> <w n="12.7">fièrement</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Eh</w> ! <w n="13.2">pourquoi</w> <w n="13.3">rougirai</w>-<w n="13.4">je</w> <w n="13.5">en</w> <w n="13.6">n</w>’<w n="13.7">étant</w> <w n="13.8">que</w> <w n="13.9">poussière</w>,</l>
						<l n="14" num="3.2"><w n="14.1">De</w> <w n="14.2">Celui</w> <w n="14.3">qui</w> <w n="14.4">des</w> <w n="14.5">cieux</w> <w n="14.6">épanche</w> <w n="14.7">la</w> <w n="14.8">lumière</w> ?</l>
						<l n="15" num="3.3"><w n="15.1">Est</w>-<w n="15.2">ce</w> <w n="15.3">parce</w> <w n="15.4">qu</w>’<w n="15.5">il</w> <w n="15.6">est</w> <w n="15.7">le</w> <w n="15.8">Maître</w> <w n="15.9">tout</w>-<w n="15.10">puissant</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Celui</w> <w n="16.2">qui</w> <w n="16.3">fit</w> <w n="16.4">l</w>’<w n="16.5">azur</w>, <w n="16.6">l</w>’<w n="16.7">astre</w>, <w n="16.8">le</w> <w n="16.9">mont</w> <w n="16.10">superbe</w>,</l>
						<l n="17" num="3.5"><w n="17.1">L</w>’<w n="17.2">aigle</w> <w n="17.3">fier</w>, <w n="17.4">l</w>’<w n="17.5">oiselet</w> <w n="17.6">qui</w> <w n="17.7">se</w> <w n="17.8">cache</w> <w n="17.9">dans</w> <w n="17.10">l</w>’<w n="17.11">herbe</w>,</l>
						<l n="18" num="3.6"><w n="18.1">L</w>’<w n="18.2">invisible</w> <w n="18.3">ciron</w>, <w n="18.4">le</w> <w n="18.5">lion</w> <w n="18.6">rugissant</w> ?</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Dans</w> <w n="19.2">l</w>’<w n="19.3">immense</w> <w n="19.4">désert</w>, <w n="19.5">sur</w> <w n="19.6">les</w> <w n="19.7">plus</w> <w n="19.8">vastes</w> <w n="19.9">cimes</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Au</w> <w n="20.2">bord</w> <w n="20.3">des</w> <w n="20.4">océans</w>, <w n="20.5">au</w> <w n="20.6">fond</w> <w n="20.7">des</w> <w n="20.8">cieux</w> <w n="20.9">sublimes</w>,</l>
						<l n="21" num="4.3"><w n="21.1">S</w>’<w n="21.2">il</w> <w n="21.3">est</w> <w n="21.4">un</w> <w n="21.5">être</w> <w n="21.6">bon</w>, <w n="21.7">digne</w> <w n="21.8">de</w> <w n="21.9">notre</w> <w n="21.10">amour</w>,</l>
						<l n="22" num="4.4"><w n="22.1">Que</w> <w n="22.2">c</w>’<w n="22.3">est</w> <w n="22.4">bien</w> <w n="22.5">ce</w> <w n="22.6">grand</w> <w n="22.7">Dieu</w> <w n="22.8">qui</w> <w n="22.9">remplit</w> <w n="22.10">l</w>’<w n="22.11">étendue</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Dont</w> <w n="23.2">la</w> <w n="23.3">gloire</w> <w n="23.4">éternelle</w> <w n="23.5">est</w> <w n="23.6">partout</w> <w n="23.7">répandue</w>,</l>
						<l n="24" num="4.6"><w n="24.1">Et</w> <w n="24.2">qui</w>, <w n="24.3">d</w>’<w n="24.4">un</w> <w n="24.5">seul</w> <w n="24.6">regard</w>, <w n="24.7">a</w> <w n="24.8">fait</w> <w n="24.9">jaillir</w> <w n="24.10">le</w> <w n="24.11">jour</w> !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Respectez</w>-<w n="25.2">le</w>, <w n="25.3">mortels</w>, <w n="25.4">et</w> <w n="25.5">gardez</w>-<w n="25.6">vous</w> <w n="25.7">d</w>’<w n="25.8">en</w> <w n="25.9">rire</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Car</w> <w n="26.2">ce</w> <w n="26.3">n</w>’<w n="26.4">est</w> <w n="26.5">pas</w> <w n="26.6">en</w> <w n="26.7">vain</w> <w n="26.8">qu</w>’<w n="26.9">il</w> <w n="26.10">m</w>’<w n="26.11">enflamme</w>, <w n="26.12">m</w>’<w n="26.13">inspire</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Et</w> <w n="27.2">verse</w> <w n="27.3">dans</w> <w n="27.4">mon</w> <w n="27.5">cœur</w> <w n="27.6">un</w> <w n="27.7">juste</w>, <w n="27.8">et</w> <w n="27.9">saint</w> <w n="27.10">courroux</w>.</l>
						<l n="28" num="5.4"><w n="28.1">Craignez</w> <w n="28.2">de</w> <w n="28.3">soulever</w> <w n="28.4">les</w> <w n="28.5">flots</w> <w n="28.6">de</w> <w n="28.7">sa</w> <w n="28.8">colère</w>,</l>
						<l n="29" num="5.5"><w n="29.1">Oui</w>, <w n="29.2">tremblez</w> <w n="29.3">et</w> <w n="29.4">courbez</w> <w n="29.5">votre</w> <w n="29.6">tête</w> <w n="29.7">si</w> <w n="29.8">fière</w>,</l>
						<l n="30" num="5.6"><w n="30.1">Car</w> <w n="30.2">il</w> <w n="30.3">est</w> <w n="30.4">tout</w>-<w n="30.5">puissant</w> <w n="30.6">pour</w> <w n="30.7">se</w> <w n="30.8">venger</w> <w n="30.9">de</w> <w n="30.10">vous</w> !</l>
					</lg>
				</div></body></text></TEI>