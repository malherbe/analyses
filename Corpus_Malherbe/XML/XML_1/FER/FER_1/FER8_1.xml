<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CROQUIS ET PASTELS</head><div type="poem" key="FER8">
					<head type="main">HYMNE MATINAL</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">le</w> <w n="1.4">printemps</w>, <w n="1.5">ma</w> <w n="1.6">douce</w> <w n="1.7">bien</w>-<w n="1.8">aimée</w>,</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">astre</w> <w n="2.3">du</w> <w n="2.4">jour</w> <w n="2.5">s</w>’<w n="2.6">élève</w> <w n="2.7">à</w> <w n="2.8">l</w>’<w n="2.9">horizon</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">l</w>’<w n="3.3">oiselet</w> <w n="3.4">caché</w> <w n="3.5">dans</w> <w n="3.6">la</w> <w n="3.7">ramée</w>,</l>
						<l n="4" num="1.4"><w n="4.1">En</w> <w n="4.2">sons</w> <w n="4.3">joyeux</w>, <w n="4.4">égrène</w> <w n="4.5">sa</w> <w n="4.6">chanson</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Tout</w> <w n="5.2">nous</w> <w n="5.3">invite</w> <w n="5.4">à</w> <w n="5.5">nous</w> <w n="5.6">ravir</w>, <w n="5.7">ma</w> <w n="5.8">chère</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Tout</w> <w n="6.2">nous</w> <w n="6.3">convie</w> <w n="6.4">à</w> <w n="6.5">nous</w> <w n="6.6">charmer</w> <w n="6.7">le</w> <w n="6.8">cœur</w> :</l>
						<l n="7" num="1.7"><w n="7.1">Chantons</w>, <w n="7.2">chantons</w> <w n="7.3">l</w>’<w n="7.4">aurore</w> <w n="7.5">printanière</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Ouvrons</w> <w n="8.2">nos</w> <w n="8.3">cœurs</w> <w n="8.4">à</w> <w n="8.5">l</w>’<w n="8.6">espoir</w>, <w n="8.7">au</w> <w n="8.8">bonheur</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Le</w> <w n="9.2">jour</w> <w n="9.3">au</w> <w n="9.4">loin</w> <w n="9.5">sème</w> <w n="9.6">ses</w> <w n="9.7">rayons</w> <w n="9.8">pâles</w> ;</l>
						<l n="10" num="2.2"><w n="10.1">Mille</w> <w n="10.2">reflets</w> <w n="10.3">s</w>’<w n="10.4">harmonisent</w> <w n="10.5">aux</w> <w n="10.6">cieux</w> ;</l>
						<l n="11" num="2.3"><w n="11.1">Le</w> <w n="11.2">nid</w> <w n="11.3">tressaille</w> <w n="11.4">aux</w> <w n="11.5">clartés</w> <w n="11.6">matinales</w></l>
						<l n="12" num="2.4"><w n="12.1">Et</w> <w n="12.2">de</w> <w n="12.3">partout</w> <w n="12.4">monte</w> <w n="12.5">un</w> <w n="12.6">chant</w> <w n="12.7">radieux</w>.</l>
						<l n="13" num="2.5"><w n="13.1">Les</w> <w n="13.2">papillons</w> <w n="13.3">glissent</w> <w n="13.4">dans</w> <w n="13.5">la</w> <w n="13.6">lumière</w> ;</l>
						<l n="14" num="2.6"><w n="14.1">Les</w> <w n="14.2">doux</w> <w n="14.3">zéphyrs</w> <w n="14.4">vont</w> <w n="14.5">caresser</w> <w n="14.6">la</w> <w n="14.7">fleur</w>.</l>
						<l n="15" num="2.7"><w n="15.1">Chantons</w>, <w n="15.2">chantons</w> <w n="15.3">l</w>’<w n="15.4">aurore</w> <w n="15.5">printanière</w>,</l>
						<l n="16" num="2.8"><w n="16.1">Ouvrons</w> <w n="16.2">nos</w> <w n="16.3">cœurs</w> <w n="16.4">à</w> <w n="16.5">l</w>’<w n="16.6">espoir</w>, <w n="16.7">au</w> <w n="16.8">bonheur</w>.</l>
					</lg>
				</div></body></text></TEI>