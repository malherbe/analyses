<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES FIBRES DU CŒUR</head><div type="poem" key="FER27">
					<head type="main">TIMIDITÉ VIRGINALE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">ton</w> <w n="1.3">bel</w> <w n="1.4">œil</w> <w n="1.5">rêveur</w> <w n="1.6">dont</w> <w n="1.7">le</w> <w n="1.8">charme</w> <w n="1.9">m</w>’<w n="1.10">enflamme</w></l>
						<l n="2" num="1.2"><w n="2.1">Laisse</w>-<w n="2.2">moi</w> <w n="2.3">donc</w> <w n="2.4">plonger</w> <w n="2.5">mon</w> <w n="2.6">regard</w> <w n="2.7">amoureux</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Afin</w> <w n="3.2">qu</w>’<w n="3.3">en</w> <w n="3.4">l</w>’<w n="3.5">admirant</w> <w n="3.6">j</w>’<w n="3.7">interroge</w> <w n="3.8">ton</w> <w n="3.9">âme</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">te</w> <w n="4.3">dise</w> <w n="4.4">comment</w> <w n="4.5">tu</w> <w n="4.6">peux</w> <w n="4.7">me</w> <w n="4.8">rendre</w> <w n="4.9">heureux</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Soulève</w> <w n="5.2">avec</w> <w n="5.3">amour</w> <w n="5.4">ta</w> <w n="5.5">paupière</w> <w n="5.6">baissée</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Qu</w>’<w n="6.2">effleure</w> <w n="6.3">mollement</w> <w n="6.4">le</w> <w n="6.5">baiser</w> <w n="6.6">des</w> <w n="6.7">rayons</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Afin</w> <w n="7.2">qu</w>’<w n="7.3">en</w> <w n="7.4">ton</w> <w n="7.5">regard</w> <w n="7.6">je</w> <w n="7.7">sème</w> <w n="7.8">la</w> <w n="7.9">pensée</w></l>
						<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">chante</w> <w n="8.3">dans</w> <w n="8.4">mon</w> <w n="8.5">cœur</w> <w n="8.6">durant</w> <w n="8.7">que</w> <w n="8.8">nous</w> <w n="8.9">causons</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Oui</w>, <w n="9.2">tandis</w> <w n="9.3">que</w> <w n="9.4">la</w> <w n="9.5">nuit</w> <w n="9.6">ceint</w> <w n="9.7">l</w>’<w n="9.8">azur</w> <w n="9.9">de</w> <w n="9.10">ses</w> <w n="9.11">voiles</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">verse</w> <w n="10.3">pour</w> <w n="10.4">l</w>’<w n="10.5">amant</w> <w n="10.6">une</w> <w n="10.7">pâle</w> <w n="10.8">clarté</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Qui</w> <w n="11.2">semble</w> <w n="11.3">sur</w> <w n="11.4">ton</w> <w n="11.5">front</w> <w n="11.6">le</w> <w n="11.7">reflet</w> <w n="11.8">des</w> <w n="11.9">étoiles</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Dont</w> <w n="12.2">j</w>’<w n="12.3">aime</w> <w n="12.4">à</w> <w n="12.5">contempler</w> <w n="12.6">la</w> <w n="12.7">sereine</w> <w n="12.8">beauté</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Tandis</w> <w n="13.2">que</w> <w n="13.3">dans</w> <w n="13.4">ses</w> <w n="13.5">jeux</w> <w n="13.6">un</w> <w n="13.7">volage</w> <w n="13.8">zéphyre</w></l>
						<l n="14" num="4.2"><w n="14.1">Frôle</w>, <w n="14.2">en</w> <w n="14.3">les</w> <w n="14.4">parfumant</w>, <w n="14.5">tes</w> <w n="14.6">noirs</w> <w n="14.7">et</w> <w n="14.8">beaux</w> <w n="14.9">cheveux</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">te</w> <w n="15.3">jette</w> <w n="15.4">un</w> <w n="15.5">accord</w> <w n="15.6">qui</w> <w n="15.7">longuement</w> <w n="15.8">soupire</w>,</l>
						<l n="16" num="4.4"><w n="16.1">En</w> <w n="16.2">devenant</w> <w n="16.3">plus</w> <w n="16.4">vague</w> <w n="16.5">et</w> <w n="16.6">plus</w> <w n="16.7">harmonieux</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Tandis</w> <w n="17.2">que</w> <w n="17.3">dans</w> <w n="17.4">la</w> <w n="17.5">nuit</w> <w n="17.6">des</w> <w n="17.7">plaintives</w> <w n="17.8">ramures</w></l>
						<l n="18" num="5.2"><w n="18.1">On</w> <w n="18.2">entend</w> <w n="18.3">roucouler</w> <w n="18.4">de</w> <w n="18.5">gracieux</w> <w n="18.6">oiseaux</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">que</w> <w n="19.3">le</w> <w n="19.4">vent</w> <w n="19.5">du</w> <w n="19.6">soir</w> <w n="19.7">mêle</w> <w n="19.8">ses</w> <w n="19.9">longs</w> <w n="19.10">murmures</w></l>
						<l n="20" num="5.4"><w n="20.1">Au</w> <w n="20.2">tendre</w> <w n="20.3">gazouillis</w> <w n="20.4">de</w> <w n="20.5">l</w>’<w n="20.6">onde</w> <w n="20.7">et</w> <w n="20.8">des</w> <w n="20.9">roseaux</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Comme</w> <w n="21.2">un</w> <w n="21.3">lis</w> <w n="21.4">qu</w>’<w n="21.5">une</w> <w n="21.6">brise</w> <w n="21.7">en</w> <w n="21.8">jouant</w> <w n="21.9">berce</w> <w n="21.10">et</w> <w n="21.11">penche</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Relève</w> <w n="22.2">un</w> <w n="22.3">peu</w> <w n="22.4">ton</w> <w n="22.5">front</w> <w n="22.6">qu</w>’<w n="22.7">incline</w> <w n="22.8">la</w> <w n="22.9">pudeur</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">laisse</w> <w n="23.3">ta</w> <w n="23.4">prunelle</w>, <w n="23.5">où</w> <w n="23.6">mon</w> <w n="23.7">aveu</w> <w n="23.8">s</w>’<w n="23.9">épanche</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Me</w> <w n="24.2">faire</w> <w n="24.3">sous</w> <w n="24.4">ses</w> <w n="24.5">feux</w> <w n="24.6">tressaillir</w> <w n="24.7">de</w> <w n="24.8">bonheur</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Mais</w> <w n="25.2">tu</w> <w n="25.3">baisses</w> <w n="25.4">tes</w> <w n="25.5">yeux</w> !… <w n="25.6">Pourquoi</w> <w n="25.7">donc</w>, <w n="25.8">jeune</w> <w n="25.9">fille</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Ne</w> <w n="26.2">veux</w>-<w n="26.3">tu</w> <w n="26.4">pas</w> <w n="26.5">laisser</w> <w n="26.6">les</w> <w n="26.7">miens</w>, <w n="26.8">un</w> <w n="26.9">seul</w> <w n="26.10">moment</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Errer</w> <w n="27.2">dans</w> <w n="27.3">l</w>’<w n="27.4">infini</w> <w n="27.5">de</w> <w n="27.6">ta</w> <w n="27.7">douce</w> <w n="27.8">pupille</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Que</w> <w n="28.2">semble</w> <w n="28.3">contempler</w> <w n="28.4">la</w> <w n="28.5">lune</w> <w n="28.6">au</w> <w n="28.7">firmament</w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">As</w>-<w n="29.2">tu</w> <w n="29.3">peur</w> <w n="29.4">que</w> <w n="29.5">j</w>’<w n="29.6">effleure</w> <w n="29.7">un</w> <w n="29.8">penser</w> <w n="29.9">trop</w> <w n="29.10">timide</w></l>
						<l n="30" num="8.2"><w n="30.1">Que</w> <w n="30.2">je</w> <w n="30.3">pourrais</w> <w n="30.4">saisir</w> <w n="30.5">si</w> <w n="30.6">tu</w> <w n="30.7">me</w> <w n="30.8">regardais</w> ?</l>
						<l n="31" num="8.3"><w n="31.1">Ou</w> <w n="31.2">crains</w>-<w n="31.3">tu</w> <w n="31.4">que</w> <w n="31.5">ton</w> <w n="31.6">âme</w>, <w n="31.7">ainsi</w> <w n="31.8">qu</w>’<w n="31.9">un</w> <w n="31.10">flot</w> <w n="31.11">limpide</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Contre</w> <w n="32.2">ton</w> <w n="32.3">gré</w>, <w n="32.4">s</w>’<w n="32.5">épanche</w> <w n="32.6">avec</w> <w n="32.7">tous</w> <w n="32.8">ses</w> <w n="32.9">secrets</w> ?…</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Ah</w> ! <w n="33.2">si</w> <w n="33.3">tu</w> <w n="33.4">ne</w> <w n="33.5">peux</w> <w n="33.6">pas</w>, <w n="33.7">sans</w> <w n="33.8">qu</w>’<w n="33.9">une</w> <w n="33.10">rougeur</w> <w n="33.11">vive</w></l>
						<l n="34" num="9.2"><w n="34.1">Ne</w> <w n="34.2">fasse</w> <w n="34.3">sur</w> <w n="34.4">ton</w> <w n="34.5">front</w> <w n="34.6">rayonner</w> <w n="34.7">sa</w> <w n="34.8">splendeur</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Soutenir</w> <w n="35.2">mon</w> <w n="35.3">regard</w> <w n="35.4">que</w> <w n="35.5">ta</w> <w n="35.6">beauté</w> <w n="35.7">captive</w>,</l>
						<l n="36" num="9.4"><w n="36.1">C</w>’<w n="36.2">est</w> <w n="36.3">que</w> <w n="36.4">d</w>’<w n="36.5">amour</w> <w n="36.6">pour</w> <w n="36.7">moi</w> <w n="36.8">tu</w> <w n="36.9">sens</w> <w n="36.10">battre</w> <w n="36.11">ton</w> <w n="36.12">cœur</w> !</l>
					</lg>
				</div></body></text></TEI>