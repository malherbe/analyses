<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MÉLANCOLIES</head><div type="poem" key="FER18">
					<head type="main">LES CŒURS</head>
					<opener>
						<salute>À MA MÈRE</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">pense</w> <w n="1.3">que</w> <w n="1.4">les</w> <w n="1.5">cœurs</w> <w n="1.6">si</w> <w n="1.7">généreux</w> <w n="1.8">et</w> <w n="1.9">doux</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">chantaient</w> <w n="2.3">et</w> <w n="2.4">pleuraient</w> <w n="2.5">comme</w> <w n="2.6">ceux</w> <w n="2.7">qui</w> <w n="2.8">demeurent</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">le</w> <w n="3.3">tombeau</w> <w n="3.4">muet</w> <w n="3.5">songent</w> <w n="3.6">toujours</w> <w n="3.7">à</w> <w n="3.8">nous</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="18"></space><w n="4.1">Et</w> <w n="4.2">n</w>’<w n="4.3">y</w> <w n="4.4">meurent</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Oui</w>, <w n="5.2">ces</w> <w n="5.3">cœurs</w> <w n="5.4">disparus</w> <w n="5.5">doivent</w> <w n="5.6">être</w> <w n="5.7">encor</w> <w n="5.8">bons</w> !…</l>
						<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">le</w> <w n="6.3">sein</w> <w n="6.4">de</w> <w n="6.5">la</w> <w n="6.6">tombe</w>, <w n="6.7">où</w> <w n="6.8">l</w>’<w n="6.9">on</w> <w n="6.10">croit</w> <w n="6.11">qu</w>’<w n="6.12">ils</w> <w n="6.13">expirent</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Ils</w> <w n="7.2">doivent</w> <w n="7.3">quelquefois</w> <w n="7.4">se</w> <w n="7.5">rappeler</w> <w n="7.6">des</w> <w n="7.7">noms</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="18"></space><w n="8.1">Qu</w>’<w n="8.2">ils</w> <w n="8.3">soupirent</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Car</w> <w n="9.2">tous</w> <w n="9.3">ces</w> <w n="9.4">nobles</w> <w n="9.5">cœurs</w> <w n="9.6">qu</w>’<w n="9.7">on</w> <w n="9.8">ose</w> <w n="9.9">nous</w> <w n="9.10">ravir</w></l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">qu</w>’<w n="10.3">aux</w> <w n="10.4">champs</w> <w n="10.5">du</w> <w n="10.6">repos</w> <w n="10.7">les</w> <w n="10.8">durs</w> <w n="10.9">fossoyeurs</w> <w n="10.10">sèment</w>,</l>
						<l n="11" num="3.3"><w n="11.1">En</w> <w n="11.2">eux</w> <w n="11.3">ont</w> <w n="11.4">conservé</w> <w n="11.5">des</w> <w n="11.6">leurs</w> <w n="11.7">le</w> <w n="11.8">souvenir</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="18"></space><w n="12.1">Et</w> <w n="12.2">les</w> <w n="12.3">aiment</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Lorsque</w> <w n="13.2">nous</w> <w n="13.3">en</w> <w n="13.4">parlons</w>, <w n="13.5">les</w> <w n="13.6">larmes</w> <w n="13.7">dans</w> <w n="13.8">les</w> <w n="13.9">yeux</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Les</w> <w n="14.2">plaintes</w> <w n="14.3">qu</w>’<w n="14.4">on</w> <w n="14.5">perçoit</w> <w n="14.6">au</w> <w n="14.7">sein</w> <w n="14.8">des</w> <w n="14.9">vents</w> <w n="14.10">qui</w> <w n="14.11">grondent</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Sont</w>, <w n="15.2">sans</w> <w n="15.3">doute</w>, <w n="15.4">leurs</w> <w n="15.5">voix</w>, <w n="15.6">oui</w>, <w n="15.7">ce</w> <w n="15.8">doivent</w> <w n="15.9">être</w> <w n="15.10">eux</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="18"></space><w n="16.1">Qui</w> <w n="16.2">répondent</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ah</w> ! <w n="17.2">qu</w>’<w n="17.3">on</w> <w n="17.4">pense</w> <w n="17.5">à</w> <w n="17.6">ces</w> <w n="17.7">cœurs</w> <w n="17.8">et</w> <w n="17.9">que</w> <w n="17.10">l</w>’<w n="17.11">on</w> <w n="17.12">prie</w> <w n="17.13">aussi</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Car</w> <w n="18.2">si</w>, <w n="18.3">comme</w> <w n="18.4">aux</w> <w n="18.5">cieux</w> <w n="18.6">gris</w> <w n="18.7">les</w> <w n="18.8">automnes</w> <w n="18.9">se</w> <w n="18.10">plaignent</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">soir</w>, <w n="19.3">à</w> <w n="19.4">notre</w> <w n="19.5">oreille</w>, <w n="19.6">ils</w> <w n="19.7">gémissent</w> <w n="19.8">ainsi</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="18"></space><w n="20.1">C</w>’<w n="20.2">est</w> <w n="20.3">qu</w>’<w n="20.4">ils</w> <w n="20.5">saignent</w> !…</l>
					</lg>
				</div></body></text></TEI>