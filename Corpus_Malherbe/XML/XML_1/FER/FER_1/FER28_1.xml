<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">MÉLODIES POÉTIQUES</title>
				<title type="medium">Édition électronique</title>
				<author key="FER">
					<name>
						<forename>Albert</forename>
						<surname>FERLAND</surname>
					</name>
					<date from="1872" to="1943">1872-1943</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1014 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">FER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Mélodies poétiques</title>
						<author>Albert Ferland</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/albertferlandmelodiespoetiques.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Mélodies poétiques</title>
								<author>Albert Ferland</author>
								<idno type="URI">https://books.google.fr/books?id=TBENAAAAYAAJ</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Pierre J. Bédard, Imprimeur-relieur</publisher>
									<date when="1893">1893</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUR LES FIBRES DU CŒUR</head><div type="poem" key="FER28">
					<head type="main">ALTERNATIVE ÉROTIQUE</head>
					<head type="sub_1">À L’OCCASION D’UN PÈLERINAGE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Laisse</w> <w n="1.2">donc</w> <w n="1.3">en</w> <w n="1.4">ce</w> <w n="1.5">jour</w>, <w n="1.6">ô</w> <w n="1.7">douce</w> <w n="1.8">bien</w>-<w n="1.9">aimée</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ma</w> <w n="2.2">bouche</w>, <w n="2.3">que</w> <w n="2.4">ta</w> <w n="2.5">lèvre</w> <w n="2.6">a</w> <w n="2.7">si</w> <w n="2.8">souvent</w> <w n="2.9">charmée</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Savourer</w> <w n="3.2">sur</w> <w n="3.3">la</w> <w n="3.4">tienne</w> <w n="3.5">un</w> <w n="3.6">suave</w> <w n="3.7">baiser</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">doux</w> <w n="4.3">fruit</w> <w n="4.4">de</w> <w n="4.5">l</w>’<w n="4.6">amour</w> <w n="4.7">que</w> <w n="4.8">donne</w> <w n="4.9">toute</w> <w n="4.10">bouche</w></l>
						<l n="5" num="1.5"><w n="5.1">À</w> <w n="5.2">celle</w> <w n="5.3">qui</w>, <w n="5.4">timide</w>, <w n="5.5">en</w> <w n="5.6">tressaillant</w>, <w n="5.7">la</w> <w n="5.8">touche</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Ne</w> <w n="6.2">semblant</w> <w n="6.3">presque</w> <w n="6.4">pas</w> <w n="6.5">s</w>’<w n="6.6">y</w> <w n="6.7">poser</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Laisse</w> <w n="7.2">aussi</w> <w n="7.3">mon</w> <w n="7.4">regard</w>, <w n="7.5">que</w> <w n="7.6">ton</w> <w n="7.7">regard</w> <w n="7.8">attire</w>,</l>
						<l n="8" num="2.2"><w n="8.1">S</w>’<w n="8.2">attendrir</w> <w n="8.3">aux</w> <w n="8.4">douceurs</w> <w n="8.5">de</w> <w n="8.6">ton</w> <w n="8.7">gentil</w> <w n="8.8">sourire</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Qui</w> <w n="9.2">semble</w> <w n="9.3">un</w> <w n="9.4">reflet</w> <w n="9.5">d</w>’<w n="9.6">âme</w> <w n="9.7">au</w> <w n="9.8">coin</w> <w n="9.9">de</w> <w n="9.10">ton</w> <w n="9.11">œil</w> <w n="9.12">noir</w>,</l>
						<l n="10" num="2.4"><w n="10.1">Contempler</w> <w n="10.2">longuement</w> <w n="10.3">ton</w> <w n="10.4">front</w> <w n="10.5">pâle</w> <w n="10.6">et</w> <w n="10.7">candide</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Où</w> <w n="11.2">le</w> <w n="11.3">souffle</w> <w n="11.4">des</w> <w n="11.5">ans</w> <w n="11.6">ne</w> <w n="11.7">traçant</w> <w n="11.8">nulle</w> <w n="11.9">ride</w>,</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">N</w>’<w n="12.2">a</w> <w n="12.3">pas</w> <w n="12.4">encor</w> <w n="12.5">cueilli</w> <w n="12.6">l</w>’<w n="12.7">espoir</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Oh</w> ! <w n="13.2">je</w> <w n="13.3">veux</w> <w n="13.4">t</w>’<w n="13.5">embrasser</w> !… <w n="13.6">Mais</w>, <w n="13.7">quoi</w> <w n="13.8">donc</w>, <w n="13.9">je</w> <w n="13.10">ne</w> <w n="13.11">l</w>’<w n="13.12">ose</w> !…</l>
						<l n="14" num="3.2"><w n="14.1">Ah</w> ! <w n="14.2">c</w>’<w n="14.3">est</w> <w n="14.4">que</w> <w n="14.5">le</w> <w n="14.6">Seigneur</w> <w n="14.7">a</w> <w n="14.8">sur</w> <w n="14.9">ta</w> <w n="14.10">lèvre</w> <w n="14.11">rose</w></l>
						<l n="15" num="3.3"><w n="15.1">Descendu</w> <w n="15.2">ce</w> <w n="15.3">matin</w> <w n="15.4">pour</w> <w n="15.5">se</w> <w n="15.6">donner</w> <w n="15.7">à</w> <w n="15.8">toi</w> !</l>
						<l n="16" num="3.4"><w n="16.1">Ce</w> <w n="16.2">Dieu</w> <w n="16.3">permettra</w>-<w n="16.4">t</w>-<w n="16.5">il</w> <w n="16.6">que</w> <w n="16.7">je</w> <w n="16.8">baise</w> <w n="16.9">la</w> <w n="16.10">vierge</w></l>
						<l n="17" num="3.5"><w n="17.1">Qui</w> <w n="17.2">vient</w> <w n="17.3">de</w> <w n="17.4">recevoir</w>, <w n="17.5">sous</w> <w n="17.6">les</w> <w n="17.7">regards</w> <w n="17.8">du</w> <w n="17.9">cierge</w>,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">Le</w> <w n="18.2">baiser</w> <w n="18.3">de</w> <w n="18.4">l</w>’<w n="18.5">éternel</w> <w n="18.6">Roi</w> ?</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Oui</w>, <w n="19.2">le</w> <w n="19.3">Seigneur</w> <w n="19.4">permet</w> <w n="19.5">à</w> <w n="19.6">l</w>’<w n="19.7">amant</w> <w n="19.8">qui</w> <w n="19.9">l</w>’<w n="19.10">adore</w>,</l>
						<l n="20" num="4.2"><w n="20.1">A</w> <w n="20.2">chaque</w> <w n="20.3">crépuscule</w> <w n="20.4">ainsi</w> <w n="20.5">qu</w>’<w n="20.6">à</w> <w n="20.7">chaque</w> <w n="20.8">aurore</w>,</l>
						<l n="21" num="4.3"><w n="21.1">De</w> <w n="21.2">mettre</w> <w n="21.3">un</w> <w n="21.4">baiser</w> <w n="21.5">pur</w> <w n="21.6">près</w> <w n="21.7">du</w> <w n="21.8">baiser</w> <w n="21.9">divin</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">c</w>’<w n="22.3">est</w> <w n="22.4">bien</w> <w n="22.5">sans</w> <w n="22.6">remords</w> <w n="22.7">que</w> <w n="22.8">j</w>’<w n="22.9">effleure</w>, <w n="22.10">ô</w> <w n="22.11">ma</w> <w n="22.12">chère</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Ta</w> <w n="23.2">bouche</w> <w n="23.3">par</w> <w n="23.4">où</w> <w n="23.5">Dieu</w>, <w n="23.6">rencontrant</w> <w n="23.7">ta</w> <w n="23.8">prière</w>,</l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">A</w> <w n="24.2">daigné</w> <w n="24.3">descendre</w> <w n="24.4">en</w> <w n="24.5">ton</w> <w n="24.6">sein</w>.</l>
					</lg>
				</div></body></text></TEI>