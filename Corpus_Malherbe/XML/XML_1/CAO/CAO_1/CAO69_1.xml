<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">UNE GERBE D’ACROSTICHES</head><div type="poem" key="CAO69">
					<head type="main">A M. VICTOR BILLAUD</head>
					<head type="sub_1">Secrétaire de l’Académie des Muses Santones, à Royan, France</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Asile</w> <w n="1.2">du</w> <w n="1.3">poète</w>, <w n="1.4">ô</w> <w n="1.5">belle</w> <w n="1.6">Académie</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Congrès</w> <w n="2.2">où</w> <w n="2.3">siège</w> <w n="2.4">seul</w> <w n="2.5">le</w> <w n="2.6">talent</w> <w n="2.7">reconnu</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ah</w> ! <w n="3.2">tu</w> <w n="3.3">daignes</w> <w n="3.4">offrir</w>, <w n="3.5">trop</w> <w n="3.6">généreuse</w> <w n="3.7">amie</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">ton</w> <w n="4.3">temple</w> <w n="4.4">un</w> <w n="4.5">fauteuil</w> <w n="4.6">à</w> <w n="4.7">moi</w>, <w n="4.8">barde</w> <w n="4.9">inconnu</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Eh</w> ! <w n="5.2">que</w> <w n="5.3">pourrais</w>-<w n="5.4">je</w> <w n="5.5">faire</w> <w n="5.6">au</w> <w n="5.7">milieu</w> <w n="5.8">de</w> <w n="5.9">confrères</w></l>
						<l n="6" num="1.6"><w n="6.1">Mûris</w> <w n="6.2">par</w> <w n="6.3">la</w> <w n="6.4">science</w> <w n="6.5">et</w> <w n="6.6">le</w> <w n="6.7">rude</w> <w n="6.8">labeur</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Imberbe</w> <w n="7.2">que</w> <w n="7.3">je</w> <w n="7.4">suis</w> ? ‒ <w n="7.5">J</w>’<w n="7.6">oubliais</w> : <w n="7.7">leurs</w> <w n="7.8">lumières</w></l>
						<l n="8" num="1.8"><w n="8.1">Éclaireront</w> <w n="8.2">la</w> <w n="8.3">voie</w> <w n="8.4">de</w> <w n="8.5">mon</w> <w n="8.6">esprit</w> <w n="8.7">rêveur</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Du</w> <w n="9.2">reste</w>, <w n="9.3">pour</w> <w n="9.4">avoir</w> <w n="9.5">un</w> <w n="9.6">titre</w> <w n="9.7">à</w> <w n="9.8">leur</w> <w n="9.9">estime</w></l>
						<l n="10" num="2.2"><w n="10.1">Et</w> <w n="10.2">le</w> <w n="10.3">droit</w> <w n="10.4">précieux</w> <w n="10.5">de</w> <w n="10.6">suivre</w> <w n="10.7">leurs</w> <w n="10.8">leçons</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Souvent</w> <w n="11.2">je</w> <w n="11.3">leur</w> <w n="11.4">dirai</w> <w n="11.5">dans</w> <w n="11.6">le</w> <w n="11.7">langage</w> <w n="11.8">intime</w> :</l>
						<l n="12" num="2.4"><w n="12.1">Ma</w> <w n="12.2">lyre</w> <w n="12.3">pour</w> <w n="12.4">la</w> <w n="12.5">France</w> <w n="12.6">aura</w> <w n="12.7">toujours</w> <w n="12.8">des</w> <w n="12.9">sons</w> !</l>
						<l n="13" num="2.5"><w n="13.1">Unissant</w> <w n="13.2">mes</w> <w n="13.3">accords</w> <w n="13.4">à</w> <w n="13.5">ceux</w> <w n="13.6">de</w> <w n="13.7">nos</w> <w n="13.8">poètes</w>,</l>
						<l n="14" num="2.6"><w n="14.1">Sulte</w>, <w n="14.2">Gingras</w>, <w n="14.3">Gauvreau</w>, <w n="14.4">Fréchette</w> <w n="14.5">et</w> <w n="14.6">Beauchemin</w>,</l>
						<l n="15" num="2.7"><w n="15.1">En</w> <w n="15.2">chœur</w> <w n="15.3">nous</w> <w n="15.4">chanterons</w> <w n="15.5">ses</w> <w n="15.6">brillantes</w> <w n="15.7">conquêtes</w>,</l>
						<l n="16" num="2.8"><w n="16.1">Sa</w> <w n="16.2">grandeur</w>, <w n="16.3">sa</w> <w n="16.4">richesse</w> <w n="16.5">et</w> <w n="16.6">son</w> <w n="16.7">heureux</w> <w n="16.8">destin</w> !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Sait</w>-<w n="17.2">elle</w> <w n="17.3">assez</w> <w n="17.4">comment</w> <w n="17.5">nous</w> <w n="17.6">l</w>’<w n="17.7">aimons</w>, <w n="17.8">cette</w> <w n="17.9">France</w> ?</l>
						<l n="18" num="3.2"><w n="18.1">Ah</w> ! <w n="18.2">nous</w> <w n="18.3">le</w> <w n="18.4">lui</w> <w n="18.5">dirons</w> <w n="18.6">avec</w> <w n="18.7">un</w> <w n="18.8">fier</w> <w n="18.9">accent</w>.</l>
						<l n="19" num="3.3"><w n="19.1">Nous</w> <w n="19.2">avons</w> <w n="19.3">partagé</w> <w n="19.4">sa</w> <w n="19.5">gloire</w> <w n="19.6">et</w> <w n="19.7">sa</w> <w n="19.8">souffrance</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Terrassé</w> <w n="20.2">ses</w> <w n="20.3">rivaux</w>, <w n="20.4">lutté</w> <w n="20.5">vingt</w> <w n="20.6">contre</w> <w n="20.7">cent</w>…</l>
						<l n="21" num="3.5"><w n="21.1">Oui</w>, <w n="21.2">j</w>’<w n="21.3">accepte</w>, <w n="21.4">Monsieur</w>, <w n="21.5">vos</w> <w n="21.6">offres</w> <w n="21.7">gracieuses</w> !</l>
						<l n="22" num="3.6"><w n="22.1">Nos</w> <w n="22.2">muses</w> <w n="22.3">désormais</w> <w n="22.4">franchiront</w> <w n="22.5">l</w>’<w n="22.6">océan</w> ;</l>
						<l n="23" num="3.7"><w n="23.1">Et</w> <w n="23.2">voyageant</w> <w n="23.3">ensemble</w> <w n="23.4">elles</w> <w n="23.5">diront</w>, <w n="23.6">joyeuses</w> :</l>
						<l n="24" num="3.8"><w n="24.1">Succès</w>, <w n="24.2">gloire</w> <w n="24.3">à</w> <w n="24.4">Québec</w> ! <w n="24.5">Succès</w>, <w n="24.6">gloire</w> <w n="24.7">à</w> <w n="24.8">Royan</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1886">10 avril 1886</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>