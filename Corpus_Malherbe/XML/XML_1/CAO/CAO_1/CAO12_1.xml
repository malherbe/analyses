<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO12">
					<head type="main">LE FAUBOURG SAINT-ROCH</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">vieux</w> <w n="1.3">faubourg</w> <w n="1.4">Saint</w>-<w n="1.5">Roch</w> <w n="1.6">s</w>’<w n="1.7">incline</w> <w n="1.8">sur</w> <w n="1.9">le</w> <w n="1.10">bord</w></l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">l</w>’<w n="2.3">anse</w> <w n="2.4">sablonneuse</w> <w n="2.5">où</w> <w n="2.6">le</w> <w n="2.7">Saint</w>-<w n="2.8">Charle</w> <w n="2.9">endort</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">Son</w> <w n="3.2">flot</w> <w n="3.3">bleu</w> <w n="3.4">qui</w> <w n="3.5">palpite</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">C</w>’<w n="4.2">est</w> <w n="4.3">là</w> <w n="4.4">que</w> <w n="4.5">la</w> <w n="4.6">vertu</w> <w n="4.7">romaine</w> <w n="4.8">vit</w> <w n="4.9">toujours</w></l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">que</w> <w n="5.3">sa</w> <w n="5.4">mâle</w> <w n="5.5">voix</w> ‒ <w n="5.6">sa</w> <w n="5.7">voix</w> <w n="5.8">des</w> <w n="5.9">anciens</w> <w n="5.10">jours</w> ‒</l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Parle</w> <w n="6.2">à</w> <w n="6.3">des</w> <w n="6.4">cœurs</w> <w n="6.5">d</w>’<w n="6.6">élite</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">C</w>’<w n="7.2">est</w> <w n="7.3">là</w> <w n="7.4">que</w> <w n="7.5">Cartier</w> <w n="7.6">vint</w>, <w n="7.7">pour</w> <w n="7.8">la</w> <w n="7.9">première</w> <w n="7.10">fois</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Ennoblir</w> <w n="8.2">notre</w> <w n="8.3">sol</w> <w n="8.4">en</w> <w n="8.5">y</w> <w n="8.6">plantant</w> <w n="8.7">la</w> <w n="8.8">croix</w></l>
						<l n="9" num="2.3"><space unit="char" quantity="12"></space><w n="9.1">Sous</w> <w n="9.2">l</w>’<w n="9.3">ombrage</w> <w n="9.4">des</w> <w n="9.5">hêtres</w> ;</l>
						<l n="10" num="2.4"><w n="10.1">C</w>’<w n="10.2">est</w> <w n="10.3">là</w> <w n="10.4">que</w> <w n="10.5">sont</w> <w n="10.6">empreints</w> <w n="10.7">les</w> <w n="10.8">pas</w> <w n="10.9">des</w> <w n="10.10">découvreurs</w>,</l>
						<l n="11" num="2.5"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">là</w> <w n="11.4">qu</w>’<w n="11.5">ont</w> <w n="11.6">abordé</w> <w n="11.7">nos</w> <w n="11.8">vaillants</w> <w n="11.9">laboureurs</w></l>
						<l n="12" num="2.6"><space unit="char" quantity="12"></space><w n="12.1">Avec</w> <w n="12.2">nos</w> <w n="12.3">premiers</w> <w n="12.4">prêtres</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">là</w> <w n="13.4">d</w>’<w n="13.5">où</w> <w n="13.6">sont</w> <w n="13.7">partis</w> <w n="13.8">ces</w> <w n="13.9">humbles</w> <w n="13.10">conquérants</w></l>
						<l n="14" num="3.2"><w n="14.1">Qui</w> <w n="14.2">portaient</w> <w n="14.3">à</w> <w n="14.4">travers</w> <w n="14.5">forêts</w>, <w n="14.6">monts</w> <w n="14.7">et</w> <w n="14.8">torrents</w></l>
						<l n="15" num="3.3"><space unit="char" quantity="12"></space><w n="15.1">La</w> <w n="15.2">parole</w> <w n="15.3">bénie</w></l>
						<l n="16" num="3.4"><w n="16.1">A</w> <w n="16.2">l</w>’<w n="16.3">enfant</w> <w n="16.4">des</w> <w n="16.5">déserts</w> <w n="16.6">que</w> <w n="16.7">la</w> <w n="16.8">foi</w> <w n="16.9">réclamait</w>…</l>
						<l n="17" num="3.5"><w n="17.1">C</w>’<w n="17.2">est</w> <w n="17.3">enfin</w> <w n="17.4">le</w> <w n="17.5">berceau</w> <w n="17.6">grandiose</w> <w n="17.7">où</w> <w n="17.8">germait</w></l>
						<l n="18" num="3.6"><space unit="char" quantity="12"></space><w n="18.1">La</w> <w n="18.2">noble</w> <w n="18.3">colonie</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">J</w>’<w n="19.2">aime</w> <w n="19.3">ce</w> <w n="19.4">vieux</w> <w n="19.5">faubourg</w> <w n="19.6">coquet</w> <w n="19.7">et</w> <w n="19.8">florissant</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Où</w> <w n="20.2">le</w> <w n="20.3">riche</w> <w n="20.4">à</w> <w n="20.5">sa</w> <w n="20.6">table</w> <w n="20.7">accueille</w> <w n="20.8">le</w> <w n="20.9">passant</w></l>
						<l n="21" num="4.3"><space unit="char" quantity="12"></space><w n="21.1">Qui</w> <w n="21.2">demande</w> <w n="21.3">une</w> <w n="21.4">obole</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">Car</w> <w n="22.2">c</w>’<w n="22.3">est</w> <w n="22.4">là</w> <w n="22.5">que</w> <w n="22.6">s</w>’<w n="22.7">exerce</w> <w n="22.8">avec</w> <w n="22.9">simplicité</w></l>
						<l n="23" num="4.5"><w n="23.1">La</w> <w n="23.2">bienfaisante</w> <w n="23.3">loi</w> <w n="23.4">de</w> <w n="23.5">l</w>’<w n="23.6">hospitalité</w></l>
						<l n="24" num="4.6"><space unit="char" quantity="12"></space><w n="24.1">Qui</w> <w n="24.2">ravit</w> <w n="24.3">et</w> <w n="24.4">console</w> !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Oui</w>, <w n="25.2">je</w> <w n="25.3">t</w>’<w n="25.4">aime</w>, <w n="25.5">ô</w> <w n="25.6">Saint</w>-<w n="25.7">Roch</w> ! <w n="25.8">A</w> <w n="25.9">ton</w> <w n="25.10">passé</w> <w n="25.11">rêvant</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Parfois</w> <w n="26.2">je</w> <w n="26.3">crois</w> <w n="26.4">ouïr</w> <w n="26.5">un</w> <w n="26.6">poème</w> <w n="26.7">émouvant</w></l>
						<l n="27" num="5.3"><space unit="char" quantity="12"></space><w n="27.1">Dans</w> <w n="27.2">la</w> <w n="27.3">rumeur</w> <w n="27.4">de</w> <w n="27.5">l</w>’<w n="27.6">onde</w></l>
						<l n="28" num="5.4"><w n="28.1">Où</w> <w n="28.2">se</w> <w n="28.3">mirent</w> <w n="28.4">les</w> <w n="28.5">toits</w> <w n="28.6">de</w> <w n="28.7">la</w> <w n="28.8">fière</w> <w n="28.9">cité</w></l>
						<l n="29" num="5.5"><w n="29.1">Dont</w> <w n="29.2">l</w>’<w n="29.3">immortel</w> <w n="29.4">Champlain</w> <w n="29.5">devina</w> <w n="29.6">la</w> <w n="29.7">beauté</w></l>
						<l n="30" num="5.6"><space unit="char" quantity="12"></space><w n="30.1">Qui</w> <w n="30.2">charme</w> <w n="30.3">le</w> <w n="30.4">Vieux</w>-<w n="30.5">Monde</w> !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Je</w> <w n="31.2">t</w>’<w n="31.3">aime</w> ! <w n="31.4">car</w> <w n="31.5">je</w> <w n="31.6">sais</w> <w n="31.7">qu</w>’<w n="31.8">à</w> <w n="31.9">l</w>’<w n="31.10">ombre</w> <w n="31.11">de</w> <w n="31.12">la</w> <w n="31.13">croix</w></l>
						<l n="32" num="6.2"><w n="32.1">Vaillamment</w> <w n="32.2">tu</w> <w n="32.3">luttas</w> <w n="32.4">pour</w> <w n="32.5">défendre</w> <w n="32.6">nos</w> <w n="32.7">droits</w></l>
						<l n="33" num="6.3"><space unit="char" quantity="12"></space><w n="33.1">Contre</w> <w n="33.2">le</w> <w n="33.3">despotisme</w> ;</l>
						<l n="34" num="6.4"><w n="34.1">Et</w> <w n="34.2">qu</w>’<w n="34.3">en</w> <w n="34.4">toi</w> <w n="34.5">bat</w> <w n="34.6">le</w> <w n="34.7">cœur</w> <w n="34.8">de</w> <w n="34.9">notre</w> <w n="34.10">nation</w> ;</l>
						<l n="35" num="6.5"><w n="35.1">O</w> <w n="35.2">boulevard</w> <w n="35.3">béni</w> <w n="35.4">de</w> <w n="35.5">la</w> <w n="35.6">religion</w></l>
						<l n="36" num="6.6"><space unit="char" quantity="12"></space><w n="36.1">Et</w> <w n="36.2">du</w> <w n="36.3">patriotisme</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1880">Mai 1880</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>