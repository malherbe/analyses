<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HYMNES, ROMANCES ET CHANSONNETTES</head><div type="poem" key="CAO56">
					<head type="main">AUX RAQUETTEURS DE SHERBROOKE</head>
					<head type="tune">Air : « Hiouppe ! Hiouppe ! sur la rivière, etc. »</head>
					<div type="section" n="1">
						<head type="main">I</head>
						<lg n="1">
							<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">Sherbrooke</w>, <w n="1.2">c</w>’<w n="1.3">est</w> <w n="1.4">la</w> <w n="1.5">ville</w></l>
							<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Où</w> <w n="2.2">la</w> <w n="2.3">franche</w> <w n="2.4">gaîté</w></l>
							<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Sur</w> <w n="3.2">tous</w> <w n="3.3">les</w> <w n="3.4">fronts</w> <w n="3.5">scintille</w>,</l>
							<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">L</w>’<w n="4.2">hiver</w> <w n="4.3">comme</w> <w n="4.4">l</w>’<w n="4.5">été</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="5" num="2.1"><w n="5.1">Hiouppe</w> ! <w n="5.2">Hiouppe</w> ! <w n="5.3">sur</w> <w n="5.4">la</w> <w n="5.5">raquette</w></l>
							<l rhyme="none" n="6" num="2.2"><space unit="char" quantity="4"></space><w n="6.1">Chantant</w> <w n="6.2">la</w> <w n="6.3">chansonnette</w>,</l>
							<l rhyme="none" n="7" num="2.3"><w n="7.1">Hiouppe</w> ! <w n="7.2">Hiouppe</w> ! <w n="7.3">sur</w> <w n="7.4">la</w> <w n="7.5">raquette</w></l>
							<l rhyme="none" n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1">Nous</w> <w n="8.2">ne</w> <w n="8.3">fatiguons</w> <w n="8.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1">
							<l n="9" num="1.1"><space unit="char" quantity="4"></space><w n="9.1">L</w>’<w n="9.2">on</w> <w n="9.3">vante</w> <w n="9.4">sa</w> <w n="9.5">largesse</w>,</l>
							<l n="10" num="1.2"><space unit="char" quantity="4"></space><w n="10.1">Son</w> <w n="10.2">hospitalité</w>,</l>
							<l n="11" num="1.3"><space unit="char" quantity="4"></space><w n="11.1">Sa</w> <w n="11.2">grande</w> <w n="11.3">politesse</w></l>
							<l n="12" num="1.4"><space unit="char" quantity="4"></space><w n="12.1">Et</w> <w n="12.2">son</w> <w n="12.3">urbanité</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="13" num="2.1"><w n="13.1">Hiouppe</w> ! <w n="13.2">Hiouppe</w> ! <w n="13.3">sur</w> <w n="13.4">la</w> <w n="13.5">raquette</w></l>
							<l rhyme="none" n="14" num="2.2"><space unit="char" quantity="4"></space><w n="14.1">Chantant</w> <w n="14.2">la</w> <w n="14.3">chansonnette</w>,</l>
							<l rhyme="none" n="15" num="2.3"><w n="15.1">Hiouppe</w> ! <w n="15.2">Hiouppe</w> ! <w n="15.3">sur</w> <w n="15.4">la</w> <w n="15.5">raquette</w></l>
							<l rhyme="none" n="16" num="2.4"><space unit="char" quantity="4"></space><w n="16.1">Nous</w> <w n="16.2">ne</w> <w n="16.3">fatiguons</w> <w n="16.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">III</head>
						<lg n="1">
							<l n="17" num="1.1"><space unit="char" quantity="4"></space><w n="17.1">Ses</w> <w n="17.2">habitants</w> <w n="17.3">s</w>’<w n="17.4">amusent</w></l>
							<l n="18" num="1.2"><space unit="char" quantity="4"></space><w n="18.1">Avec</w> <w n="18.2">moralité</w>,</l>
							<l n="19" num="1.3"><space unit="char" quantity="4"></space><w n="19.1">Mais</w> <w n="19.2">jamais</w> <w n="19.3">ne</w> <w n="19.4">refusent</w></l>
							<l n="20" num="1.4"><space unit="char" quantity="4"></space><w n="20.1">De</w> <w n="20.2">boire</w> <w n="20.3">une</w> <w n="20.4">santé</w> !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="21" num="2.1"><w n="21.1">Hiouppe</w> ! <w n="21.2">Hiouppe</w> ! <w n="21.3">sur</w> <w n="21.4">la</w> <w n="21.5">raquette</w></l>
							<l rhyme="none" n="22" num="2.2"><space unit="char" quantity="4"></space><w n="22.1">Chantant</w> <w n="22.2">la</w> <w n="22.3">chansonnette</w>,</l>
							<l rhyme="none" n="23" num="2.3"><w n="23.1">Hiouppe</w> ! <w n="23.2">Hiouppe</w> ! <w n="23.3">sur</w> <w n="23.4">la</w> <w n="23.5">raquette</w></l>
							<l rhyme="none" n="24" num="2.4"><space unit="char" quantity="4"></space><w n="24.1">Nous</w> <w n="24.2">ne</w> <w n="24.3">fatiguons</w> <w n="24.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="main">IV</head>
						<lg n="1">
							<l n="25" num="1.1"><space unit="char" quantity="4"></space><w n="25.1">Ils</w> <w n="25.2">aiment</w> <w n="25.3">la</w> <w n="25.4">raquette</w></l>
							<l n="26" num="1.2"><space unit="char" quantity="4"></space><w n="26.1">Puis</w> <w n="26.2">savent</w> <w n="26.3">la</w> <w n="26.4">porter</w> ;</l>
							<l n="27" num="1.3"><space unit="char" quantity="4"></space><w n="27.1">Leur</w> <w n="27.2">gentille</w> <w n="27.3">toilette</w></l>
							<l n="28" num="1.4"><space unit="char" quantity="4"></space><w n="28.1">Fait</w> <w n="28.2">plus</w> <w n="28.3">d</w>’<w n="28.4">un</w> <w n="28.5">cœur</w> <w n="28.6">sauter</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="29" num="2.1"><w n="29.1">Hiouppe</w> ! <w n="29.2">Hiouppe</w> ! <w n="29.3">sur</w> <w n="29.4">la</w> <w n="29.5">raquette</w></l>
							<l rhyme="none" n="30" num="2.2"><space unit="char" quantity="4"></space><w n="30.1">Chantant</w> <w n="30.2">la</w> <w n="30.3">chansonnette</w>,</l>
							<l rhyme="none" n="31" num="2.3"><w n="31.1">Hiouppe</w> ! <w n="31.2">Hiouppe</w> ! <w n="31.3">sur</w> <w n="31.4">la</w> <w n="31.5">raquette</w></l>
							<l rhyme="none" n="32" num="2.4"><space unit="char" quantity="4"></space><w n="32.1">Nous</w> <w n="32.2">ne</w> <w n="32.3">fatiguons</w> <w n="32.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="main">V</head>
						<lg n="1">
							<l n="33" num="1.1"><space unit="char" quantity="4"></space><w n="33.1">Ils</w> <w n="33.2">sont</w> <w n="33.3">déjà</w> <w n="33.4">quarante</w>,</l>
							<l n="34" num="1.2"><space unit="char" quantity="4"></space><w n="34.1">A</w> <w n="34.2">part</w> <w n="34.3">le</w> <w n="34.4">comité</w>,</l>
							<l n="35" num="1.3"><space unit="char" quantity="4"></space><w n="35.1">Et</w> <w n="35.2">compteront</w> <w n="35.3">soixante</w></l>
							<l n="36" num="1.4"><space unit="char" quantity="4"></space><w n="36.1">Avant</w> <w n="36.2">la</w> <w n="36.3">Trinité</w> !</l>
						</lg>
					</div>
					<div type="section" n="6">
						<head type="main">REFRAIN :</head>
						<lg n="1">
							<l rhyme="none" n="37" num="1.1"><w n="37.1">Hiouppe</w> ! <w n="37.2">Hiouppe</w> ! <w n="37.3">sur</w> <w n="37.4">la</w> <w n="37.5">raquette</w></l>
							<l rhyme="none" n="38" num="1.2"><space unit="char" quantity="4"></space><w n="38.1">Chantant</w> <w n="38.2">la</w> <w n="38.3">chansonnette</w>,</l>
							<l rhyme="none" n="39" num="1.3"><w n="39.1">Hiouppe</w> ! <w n="39.2">Hiouppe</w> ! <w n="39.3">sur</w> <w n="39.4">la</w> <w n="39.5">raquette</w></l>
							<l rhyme="none" n="40" num="1.4"><space unit="char" quantity="4"></space><w n="40.1">Nous</w> <w n="40.2">ne</w> <w n="40.3">fatiguons</w> <w n="40.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="7">
						<head type="main">VI</head>
						<lg n="1">
							<l n="41" num="1.1"><space unit="char" quantity="4"></space><w n="41.1">Car</w> <w n="41.2">toute</w> <w n="41.3">la</w> <w n="41.4">jeunesse</w></l>
							<l n="42" num="1.2"><space unit="char" quantity="4"></space><w n="42.1">Désire</w> <hi rend="ital"><w n="42.2">raquetter</w></hi> ;</l>
							<l n="43" num="1.3"><space unit="char" quantity="4"></space><w n="43.1">Elle</w> <w n="43.2">comprend</w> <w n="43.3">l</w>’<w n="43.4">ivresse</w></l>
							<l n="44" num="1.4"><space unit="char" quantity="4"></space><w n="44.1">Qu</w>’<w n="44.2">on</w> <w n="44.3">éprouve</w> <w n="44.4">à</w> <w n="44.5">trotter</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="45" num="2.1"><w n="45.1">Hiouppe</w> ! <w n="45.2">Hiouppe</w> ! <w n="45.3">sur</w> <w n="45.4">la</w> <w n="45.5">raquette</w></l>
							<l rhyme="none" n="46" num="2.2"><space unit="char" quantity="4"></space><w n="46.1">Chantant</w> <w n="46.2">la</w> <w n="46.3">chansonnette</w>,</l>
							<l rhyme="none" n="47" num="2.3"><w n="47.1">Hiouppe</w> ! <w n="47.2">Hiouppe</w> ! <w n="47.3">sur</w> <w n="47.4">la</w> <w n="47.5">raquette</w></l>
							<l rhyme="none" n="48" num="2.4"><space unit="char" quantity="4"></space><w n="48.1">Nous</w> <w n="48.2">ne</w> <w n="48.3">fatiguons</w> <w n="48.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="8">
						<head type="main">VII</head>
						<lg n="1">
							<l n="49" num="1.1"><space unit="char" quantity="4"></space><w n="49.1">Et</w>, <w n="49.2">bravant</w> <w n="49.3">la</w> <w n="49.4">tempête</w>,</l>
							<l n="50" num="1.2"><space unit="char" quantity="4"></space><w n="50.1">Le</w> <w n="50.2">froid</w>, <w n="50.3">l</w>’<w n="50.4">humidité</w>,</l>
							<l n="51" num="1.3"><space unit="char" quantity="4"></space><w n="51.1">Elle</w> <w n="51.2">dit</w> <w n="51.3">et</w> <w n="51.4">répète</w> :</l>
							<l n="52" num="1.4"><space unit="char" quantity="4"></space><w n="52.1">Courir</w>, <w n="52.2">c</w>’<w n="52.3">est</w> <w n="52.4">la</w> <w n="52.5">santé</w> !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="53" num="2.1"><w n="53.1">Hiouppe</w> ! <w n="53.2">Hiouppe</w> ! <w n="53.3">sur</w> <w n="53.4">la</w> <w n="53.5">raquette</w></l>
							<l rhyme="none" n="54" num="2.2"><space unit="char" quantity="4"></space><w n="54.1">Chantant</w> <w n="54.2">la</w> <w n="54.3">chansonnette</w>,</l>
							<l rhyme="none" n="55" num="2.3"><w n="55.1">Hiouppe</w> ! <w n="55.2">Hiouppe</w> ! <w n="55.3">sur</w> <w n="55.4">la</w> <w n="55.5">raquette</w></l>
							<l rhyme="none" n="56" num="2.4"><space unit="char" quantity="4"></space><w n="56.1">Nous</w> <w n="56.2">ne</w> <w n="56.3">fatiguons</w> <w n="56.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="9">
						<head type="main">VIII</head>
						<lg n="1">
							<l n="57" num="1.1"><space unit="char" quantity="4"></space><w n="57.1">Honneur</w> <w n="57.2">à</w> <w n="57.3">la</w> <w n="57.4">raquette</w></l>
							<l n="58" num="1.2"><space unit="char" quantity="4"></space><w n="58.1">A</w> <w n="58.2">son</w> <w n="58.3">ancienneté</w>,</l>
							<l n="59" num="1.3"><space unit="char" quantity="4"></space><w n="59.1">A</w> <w n="59.2">sa</w> <w n="59.3">forme</w> <w n="59.4">coquette</w>,</l>
							<l n="60" num="1.4"><space unit="char" quantity="4"></space><w n="60.1">A</w> <w n="60.2">son</w> <w n="60.3">utilité</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="61" num="2.1"><w n="61.1">Hiouppe</w> ! <w n="61.2">Hiouppe</w> ! <w n="61.3">sur</w> <w n="61.4">la</w> <w n="61.5">raquette</w></l>
							<l rhyme="none" n="62" num="2.2"><space unit="char" quantity="4"></space><w n="62.1">Chantant</w> <w n="62.2">la</w> <w n="62.3">chansonnette</w>,</l>
							<l rhyme="none" n="63" num="2.3"><w n="63.1">Hiouppe</w> ! <w n="63.2">Hiouppe</w> ! <w n="63.3">sur</w> <w n="63.4">la</w> <w n="63.5">raquette</w></l>
							<l rhyme="none" n="64" num="2.4"><space unit="char" quantity="4"></space><w n="64.1">Nous</w> <w n="64.2">ne</w> <w n="64.3">fatiguons</w> <w n="64.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="10">
						<head type="main">IX</head>
						<lg n="1">
							<l n="65" num="1.1"><space unit="char" quantity="4"></space><w n="65.1">Ce</w> <w n="65.2">soulier</w> <w n="65.3">poétique</w></l>
							<l n="66" num="1.2"><space unit="char" quantity="4"></space><w n="66.1">Fut</w> <w n="66.2">jadis</w> <w n="66.3">inventé</w>,</l>
							<l n="67" num="1.3"><space unit="char" quantity="4"></space><w n="67.1">Sur</w> <w n="67.2">le</w> <w n="67.3">sol</w> <w n="67.4">d</w>’<w n="67.5">Amérique</w></l>
							<l n="68" num="1.4"><space unit="char" quantity="4"></space><w n="68.1">Par</w> <w n="68.2">un</w> <w n="68.3">homme</w> <w n="68.4">futé</w> !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="69" num="2.1"><w n="69.1">Hiouppe</w> ! <w n="69.2">Hiouppe</w> ! <w n="69.3">sur</w> <w n="69.4">la</w> <w n="69.5">raquette</w></l>
							<l rhyme="none" n="70" num="2.2"><space unit="char" quantity="4"></space><w n="70.1">Chantant</w> <w n="70.2">la</w> <w n="70.3">chansonnette</w>,</l>
							<l rhyme="none" n="71" num="2.3"><w n="71.1">Hiouppe</w> ! <w n="71.2">Hiouppe</w> ! <w n="71.3">sur</w> <w n="71.4">la</w> <w n="71.5">raquette</w></l>
							<l rhyme="none" n="72" num="2.4"><space unit="char" quantity="4"></space><w n="72.1">Nous</w> <w n="72.2">ne</w> <w n="72.3">fatiguons</w> <w n="72.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="11">
						<head type="main">X</head>
						<lg n="1">
							<l n="73" num="1.1"><space unit="char" quantity="4"></space><w n="73.1">Il</w> <w n="73.2">légua</w> <w n="73.3">son</w> <w n="73.4">ouvrage</w></l>
							<l n="74" num="1.2"><space unit="char" quantity="4"></space><w n="74.1">A</w> <w n="74.2">la</w> <w n="74.3">postérité</w>,</l>
							<l n="75" num="1.3"><space unit="char" quantity="4"></space><w n="75.1">Qui</w>, <w n="75.2">depuis</w> <w n="75.3">d</w>’<w n="75.4">âge</w> <w n="75.5">en</w> <w n="75.6">âge</w>,</l>
							<l n="76" num="1.4"><space unit="char" quantity="4"></space><w n="76.1">L</w>’<w n="76.2">a</w> <w n="76.3">toujours</w> <w n="76.4">imité</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="77" num="2.1"><w n="77.1">Hiouppe</w> ! <w n="77.2">Hiouppe</w> ! <w n="77.3">sur</w> <w n="77.4">la</w> <w n="77.5">raquette</w></l>
							<l rhyme="none" n="78" num="2.2"><space unit="char" quantity="4"></space><w n="78.1">Chantant</w> <w n="78.2">la</w> <w n="78.3">chansonnette</w>,</l>
							<l rhyme="none" n="79" num="2.3"><w n="79.1">Hiouppe</w> ! <w n="79.2">Hiouppe</w> ! <w n="79.3">sur</w> <w n="79.4">la</w> <w n="79.5">raquette</w></l>
							<l rhyme="none" n="80" num="2.4"><space unit="char" quantity="4"></space><w n="80.1">Nous</w> <w n="80.2">ne</w> <w n="80.3">fatiguons</w> <w n="80.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="12">
						<head type="main">XI</head>
						<lg n="1">
							<l n="81" num="1.1"><space unit="char" quantity="4"></space><w n="81.1">O</w> <w n="81.2">raquette</w>, <w n="81.3">nos</w> <w n="81.4">pères</w></l>
							<l n="82" num="1.2"><space unit="char" quantity="4"></space><w n="82.1">Aiment</w> <w n="82.2">à</w> <w n="82.3">te</w> <w n="82.4">porter</w> ;</l>
							<l n="83" num="1.3"><space unit="char" quantity="4"></space><w n="83.1">Ils</w> <w n="83.2">ne</w> <w n="83.3">te</w> <w n="83.4">laissent</w> <w n="83.5">guères</w></l>
							<l n="84" num="1.4"><space unit="char" quantity="4"></space><w n="84.1">Qu</w>’<w n="84.2">un</w> <w n="84.3">instant</w> <w n="84.4">pour</w> <w n="84.5">lutter</w> !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="85" num="2.1"><w n="85.1">Hiouppe</w> ! <w n="85.2">Hiouppe</w> ! <w n="85.3">sur</w> <w n="85.4">la</w> <w n="85.5">raquette</w></l>
							<l rhyme="none" n="86" num="2.2"><space unit="char" quantity="4"></space><w n="86.1">Chantant</w> <w n="86.2">la</w> <w n="86.3">chansonnette</w>,</l>
							<l rhyme="none" n="87" num="2.3"><w n="87.1">Hiouppe</w> ! <w n="87.2">Hiouppe</w> ! <w n="87.3">sur</w> <w n="87.4">la</w> <w n="87.5">raquette</w></l>
							<l rhyme="none" n="88" num="2.4"><space unit="char" quantity="4"></space><w n="88.1">Nous</w> <w n="88.2">ne</w> <w n="88.3">fatiguons</w> <w n="88.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="13">
						<head type="main">XII</head>
						<lg n="1">
							<l n="89" num="1.1"><space unit="char" quantity="4"></space><w n="89.1">Et</w> <w n="89.2">nos</w> <w n="89.3">bons</w> <w n="89.4">missionnaires</w>,</l>
							<l n="90" num="1.2"><space unit="char" quantity="4"></space><w n="90.1">Prêchant</w> <w n="90.2">la</w> <w n="90.3">vérité</w>,</l>
							<l n="91" num="1.3"><space unit="char" quantity="4"></space><w n="91.1">Sur</w> <w n="91.2">raquettes</w> <w n="91.3">légères</w></l>
							<l n="92" num="1.4"><space unit="char" quantity="4"></space><w n="92.1">Ont</w> <w n="92.2">mainte</w> <w n="92.3">fois</w> <w n="92.4">monté</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="93" num="2.1"><w n="93.1">Hiouppe</w> ! <w n="93.2">Hiouppe</w> ! <w n="93.3">sur</w> <w n="93.4">la</w> <w n="93.5">raquette</w></l>
							<l rhyme="none" n="94" num="2.2"><space unit="char" quantity="4"></space><w n="94.1">Chantant</w> <w n="94.2">la</w> <w n="94.3">chansonnette</w>,</l>
							<l rhyme="none" n="95" num="2.3"><w n="95.1">Hiouppe</w> ! <w n="95.2">Hiouppe</w> ! <w n="95.3">sur</w> <w n="95.4">la</w> <w n="95.5">raquette</w></l>
							<l rhyme="none" n="96" num="2.4"><space unit="char" quantity="4"></space><w n="96.1">Nous</w> <w n="96.2">ne</w> <w n="96.3">fatiguons</w> <w n="96.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="14">
						<head type="main">XIII</head>
						<lg n="1">
							<l n="97" num="1.1"><space unit="char" quantity="4"></space><w n="97.1">Nous</w> <w n="97.2">sommes</w> <w n="97.3">de</w> <w n="97.4">leur</w> <w n="97.5">race</w> :</l>
							<l n="98" num="1.2"><space unit="char" quantity="4"></space><w n="98.1">C</w>’<w n="98.2">est</w> <w n="98.3">là</w> <w n="98.4">notre</w> <w n="98.5">fierté</w> !</l>
							<l n="99" num="1.3"><space unit="char" quantity="4"></space><w n="99.1">Comme</w> <w n="99.2">eux</w>, <w n="99.3">fendons</w> <w n="99.4">l</w>’<w n="99.5">espace</w></l>
							<l n="100" num="1.4"><space unit="char" quantity="4"></space><w n="100.1">Avec</w> <w n="100.2">agilité</w> !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="101" num="2.1"><w n="101.1">Hiouppe</w> ! <w n="101.2">Hiouppe</w> ! <w n="101.3">sur</w> <w n="101.4">la</w> <w n="101.5">raquette</w></l>
							<l rhyme="none" n="102" num="2.2"><space unit="char" quantity="4"></space><w n="102.1">Chantant</w> <w n="102.2">la</w> <w n="102.3">chansonnette</w>,</l>
							<l rhyme="none" n="103" num="2.3"><w n="103.1">Hiouppe</w> ! <w n="103.2">Hiouppe</w> ! <w n="103.3">sur</w> <w n="103.4">la</w> <w n="103.5">raquette</w></l>
							<l rhyme="none" n="104" num="2.4"><space unit="char" quantity="4"></space><w n="104.1">Nous</w> <w n="104.2">ne</w> <w n="104.3">fatiguons</w> <w n="104.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="15">
						<head type="main">XIV</head>
						<lg n="1">
							<l n="105" num="1.1"><space unit="char" quantity="4"></space><w n="105.1">Que</w> <w n="105.2">le</w> <w n="105.3">vieux</w> <w n="105.4">et</w> <w n="105.5">le</w> <w n="105.6">jeune</w></l>
							<l n="106" num="1.2"><space unit="char" quantity="4"></space><w n="106.1">Exempts</w> <w n="106.2">d</w>’<w n="106.3">infirmité</w>,</l>
							<l n="107" num="1.3"><space unit="char" quantity="4"></space><w n="107.1">Se</w> <w n="107.2">présentent</w> <w n="107.3">sans</w> <w n="107.4">gêne</w></l>
							<l n="108" num="1.4"><space unit="char" quantity="4"></space><w n="108.1">Devant</w> <w n="108.2">le</w> <w n="108.3">comité</w>.</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="109" num="2.1"><w n="109.1">Hiouppe</w> ! <w n="109.2">Hiouppe</w> ! <w n="109.3">sur</w> <w n="109.4">la</w> <w n="109.5">raquette</w></l>
							<l rhyme="none" n="110" num="2.2"><space unit="char" quantity="4"></space><w n="110.1">Chantant</w> <w n="110.2">la</w> <w n="110.3">chansonnette</w>,</l>
							<l rhyme="none" n="111" num="2.3"><w n="111.1">Hiouppe</w> ! <w n="111.2">Hiouppe</w> ! <w n="111.3">sur</w> <w n="111.4">la</w> <w n="111.5">raquette</w></l>
							<l rhyme="none" n="112" num="2.4"><space unit="char" quantity="4"></space><w n="112.1">Nous</w> <w n="112.2">ne</w> <w n="112.3">fatiguons</w> <w n="112.4">pas</w> !</l>
						</lg>
					</div>
					<div type="section" n="16">
						<head type="main">XV</head>
						<lg n="1">
							<l n="113" num="1.1"><space unit="char" quantity="4"></space><w n="113.1">Nous</w> <w n="113.2">leur</w> <w n="113.3">disons</w> <w n="113.4">d</w>’<w n="113.5">avance</w> :</l>
							<l n="114" num="1.2"><space unit="char" quantity="4"></space><w n="114.1">Vous</w> <w n="114.2">serez</w> <w n="114.3">acceptés</w>.</l>
							<l n="115" num="1.3"><space unit="char" quantity="4"></space><w n="115.1">Car</w> <w n="115.2">les</w> <w n="115.3">fils</w> <w n="115.4">de</w> <w n="115.5">la</w> <w n="115.6">France</w></l>
							<l n="116" num="1.4"><space unit="char" quantity="4"></space><w n="116.1">Par</w> <w n="116.2">nous</w> <w n="116.3">sont</w> <w n="116.4">bien</w> <w n="116.5">traités</w> !</l>
						</lg>
						<lg n="2">
							<head type="main">REFRAIN :</head>
							<l rhyme="none" n="117" num="2.1"><w n="117.1">Hiouppe</w> ! <w n="117.2">Hiouppe</w> ! <w n="117.3">sur</w> <w n="117.4">la</w> <w n="117.5">raquette</w></l>
							<l rhyme="none" n="118" num="2.2"><space unit="char" quantity="4"></space><w n="118.1">Chantant</w> <w n="118.2">la</w> <w n="118.3">chansonnette</w>,</l>
							<l rhyme="none" n="119" num="2.3"><w n="119.1">Hiouppe</w> ! <w n="119.2">Hiouppe</w> ! <w n="119.3">sur</w> <w n="119.4">la</w> <w n="119.5">raquette</w></l>
							<l rhyme="none" n="120" num="2.4"><space unit="char" quantity="4"></space><w n="120.1">Nous</w> <w n="120.2">ne</w> <w n="120.3">fatiguons</w> <w n="120.4">pas</w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>