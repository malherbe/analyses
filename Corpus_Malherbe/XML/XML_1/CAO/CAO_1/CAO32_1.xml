<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO32">
					<head type="main">AUX CÉLIBATAIRES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Allons</w>, <w n="1.2">debout</w> ! <w n="1.3">pauvres</w> <w n="1.4">célibataires</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Vous</w> <w n="2.2">que</w> <w n="2.3">la</w> <w n="2.4">femme</w> <w n="2.5">abreuve</w> <w n="2.6">de</w> <w n="2.7">mépris</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Abandonnez</w> <w n="3.2">vos</w> <w n="3.3">gîtes</w> <w n="3.4">solitaire</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">l</w>’<w n="4.3">on</w> <w n="4.4">ne</w> <w n="4.5">voit</w> <w n="4.6">que</w> <w n="4.7">des</w> <w n="4.8">chats</w> <w n="4.9">favoris</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">De</w> <w n="5.2">votre</w> <w n="5.3">cœur</w> <w n="5.4">bannissez</w> <w n="5.5">la</w> <w n="5.6">souffrance</w> :</l>
						<l n="6" num="2.2"><w n="6.1">Ne</w> <w n="6.2">soyez</w> <w n="6.3">plus</w> <w n="6.4">désormais</w> <w n="6.5">soucieux</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">saluez</w> <w n="7.3">avec</w> <w n="7.4">joie</w>, <w n="7.5">espérance</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">nouvel</w> <w n="8.3">an</w> <w n="8.4">qui</w> <w n="8.5">brille</w> <w n="8.6">au</w> <w n="8.7">front</w> <w n="8.8">des</w> <w n="8.9">cieux</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Car</w> <w n="9.2">en</w> <w n="9.3">ce</w> <w n="9.4">jour</w> <w n="9.5">de</w> <w n="9.6">fête</w> <w n="9.7">universelle</w>,</l>
						<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">fille</w> <w n="10.3">d</w>’<w n="10.4">Ève</w> <w n="10.5">absout</w> <w n="10.6">les</w> <w n="10.7">amoureux</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Sa</w> <w n="11.2">douce</w> <w n="11.3">voix</w> <w n="11.4">attendrit</w> <w n="11.5">l</w>’<w n="11.6">infidèle</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">son</w> <w n="12.3">regard</w> <w n="12.4">rend</w> <w n="12.5">les</w> <w n="12.6">hommes</w> <w n="12.7">heureux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">En</w> <w n="13.2">votre</w> <w n="13.3">honneur</w> <w n="13.4">elle</w> <w n="13.5">fait</w> <w n="13.6">sa</w> <w n="13.7">toilette</w> ;</l>
						<l n="14" num="4.2"><w n="14.1">Elle</w> <w n="14.2">embellit</w> <w n="14.3">de</w> <w n="14.4">fleurs</w> <w n="14.5">ses</w> <w n="14.6">longs</w> <w n="14.7">cheveux</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">A</w> <w n="15.2">son</w> <w n="15.3">faux</w> <w n="15.4">col</w> <w n="15.5">rayonne</w> <w n="15.6">l</w>’<w n="15.7">épinglette</w></l>
						<l n="16" num="4.4"><w n="16.1">Qu</w>’<w n="16.2">elle</w> <w n="16.3">reçut</w> <w n="16.4">un</w> <w n="16.5">soir</w> <w n="16.6">avec</w> <w n="16.7">vos</w> <w n="16.8">vœux</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Vite</w>, <w n="17.2">debout</w> ! <w n="17.3">accourez</w> <w n="17.4">donc</w> <w n="17.5">vers</w> <w n="17.6">elle</w></l>
						<l n="18" num="5.2"><w n="18.1">Vous</w> <w n="18.2">que</w> <w n="18.3">l</w>’<w n="18.4">ennui</w> <w n="18.5">torture</w> <w n="18.6">tous</w> <w n="18.7">les</w> <w n="18.8">jours</w> !</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">dites</w>-<w n="19.3">lui</w> : « <w n="19.4">Ma</w> <w n="19.5">tendre</w> <w n="19.6">demoiselle</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Je</w> <w n="20.2">pleure</w> <w n="20.3">encor</w> <w n="20.4">mes</w> <w n="20.5">premières</w> <w n="20.6">amours</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« <w n="21.1">Je</w> <w n="21.2">suis</w> <w n="21.3">cruel</w>, <w n="21.4">barbare</w> <w n="21.5">et</w> <w n="21.6">bien</w> <w n="21.7">coupable</w></l>
						<l n="22" num="6.2"><w n="22.1">D</w>’<w n="22.2">avoir</w> <w n="22.3">blessé</w> <w n="22.4">vos</w> <w n="22.5">nobles</w> <w n="22.6">sentiments</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Mais</w> <w n="23.2">mon</w> <w n="23.3">offense</w> <w n="23.4">est</w>-<w n="23.5">elle</w> <w n="23.6">impardonnable</w> ?</l>
						<l n="24" num="6.4"><w n="24.1">Oh</w> ! <w n="24.2">non</w> ; <w n="24.3">alors</w>, <w n="24.4">reprenez</w> <w n="24.5">mes</w> <w n="24.6">serments</w>. »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Mariez</w>-<w n="25.2">vous</w> ! <w n="25.3">l</w>’<w n="25.4">Évangile</w> <w n="25.5">l</w>’<w n="25.6">ordonne</w> ;</l>
						<l n="26" num="7.2"><w n="26.1">C</w>’<w n="26.2">est</w> <w n="26.3">un</w> <w n="26.4">devoir</w> <w n="26.5">sacré</w> <w n="26.6">pour</w> <w n="26.7">le</w> <w n="26.8">chrétien</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Aux</w> <w n="27.2">bons</w> <w n="27.3">époux</w> <w n="27.4">parfois</w> <w n="27.5">le</w> <w n="27.6">Seigneur</w> <w n="27.7">donne</w></l>
						<l n="28" num="7.4"><w n="28.1">La</w> <w n="28.2">paix</w> <w n="28.3">de</w> <w n="28.4">l</w>’<w n="28.5">âme</w> <w n="28.6">et</w> <w n="28.7">le</w> <w n="28.8">pain</w> <w n="28.9">quotidien</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">C</w>’<w n="29.2">est</w> <w n="29.3">le</w> <w n="29.4">souhait</w>, <w n="29.5">braves</w> <w n="29.6">célibataires</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Que</w> <w n="30.2">je</w> <w n="30.3">formule</w> <w n="30.4">en</w> <w n="30.5">ce</w> <w n="30.6">beau</w> <w n="30.7">jour</w> <w n="30.8">de</w> <w n="30.9">l</w>’<w n="30.10">an</w></l>
						<l n="31" num="8.3"><w n="31.1">A</w> <w n="31.2">l</w>’<w n="31.3">avenir</w>, <w n="31.4">soyez</w> <w n="31.5">moins</w> <w n="31.6">solitaires</w> ;</l>
						<l n="32" num="8.4"><w n="32.1">Rendez</w> <w n="32.2">des</w> <w n="32.3">points</w> <w n="32.4">aux</w> <w n="32.5">plus</w> <w n="32.6">jeunes</w> <w n="32.7">galants</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1883">1er janvier 1883</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>