<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO17">
					<head type="main">LE MAUVAIS ARTISAN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">le</w> <w n="1.4">samedi</w> <w n="1.5">soir</w>. <w n="1.6">Au</w> <w n="1.7">sein</w> <w n="1.8">d</w>’<w n="1.9">une</w> <w n="1.10">chaumière</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">pénètre</w> <w n="2.3">le</w> <w n="2.4">froid</w>, <w n="2.5">quatre</w> <w n="2.6">jeunes</w> <w n="2.7">enfants</w></l>
						<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">pressent</w>, <w n="3.3">tout</w> <w n="3.4">pâlis</w>, <w n="3.5">aux</w> <w n="3.6">genoux</w> <w n="3.7">de</w> <w n="3.8">leur</w> <w n="3.9">mère</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">âtre</w> <w n="4.3">n</w>’<w n="4.4">a</w> <w n="4.5">plus</w> <w n="4.6">de</w> <w n="4.7">feu</w>, <w n="4.8">la</w> <w n="4.9">table</w> <w n="4.10">d</w>’<w n="4.11">aliments</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">J</w>’<w n="5.2">ai</w> <w n="5.3">faim</w> ! <w n="5.4">J</w>’<w n="5.5">ai</w> <w n="5.6">froid</w> ! » <w n="5.7">Ces</w> <w n="5.8">mots</w>, <w n="5.9">mêlés</w> <w n="5.10">de</w> <w n="5.11">pleurs</w> <w n="5.12">étranges</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Résonnent</w> <w n="6.2">comme</w> <w n="6.3">un</w> <w n="6.4">glas</w> <w n="6.5">dans</w> <w n="6.6">ce</w> <w n="6.7">foyer</w> <w n="6.8">malsain</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">la</w> <w n="7.3">mère</w> <w n="7.4">répond</w> : « <w n="7.5">Ne</w> <w n="7.6">pleurez</w> <w n="7.7">pas</w>, <w n="7.8">mes</w> <w n="7.9">anges</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Votre</w> <w n="8.2">père</w> <w n="8.3">bientôt</w> <w n="8.4">vous</w> <w n="8.5">donnera</w> <w n="8.6">du</w> <w n="8.7">pain</w>… »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">l</w>’<w n="9.3">horloge</w> <w n="9.4">là</w>-<w n="9.5">haut</w> <w n="9.6">sonne</w> <w n="9.7">déjà</w> <w n="9.8">dix</w> <w n="9.9">heures</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">le</w> <w n="10.3">père</w> <w n="10.4">et</w> <w n="10.5">le</w> <w n="10.6">pain</w> <w n="10.7">surtout</w> <w n="10.8">n</w>’<w n="10.9">arrivent</w> <w n="10.10">pas</w> !</l>
						<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">marmaille</w>, <w n="11.3">apaisée</w> <w n="11.4">un</w> <w n="11.5">instant</w> <w n="11.6">par</w> <w n="11.7">des</w> <w n="11.8">leurres</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Saute</w> <w n="12.2">à</w> <w n="12.3">faire</w> <w n="12.4">crouler</w> <w n="12.5">le</w> <w n="12.6">parquet</w> <w n="12.7">sous</w> <w n="12.8">ses</w> <w n="12.9">pas</w>…</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« <w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">faim</w> ! <w n="13.4">J</w>’<w n="13.5">ai</w> <w n="13.6">froid</w> ! <w n="13.7">du</w> <w n="13.8">feu</w> ! » <w n="13.9">Ce</w> <w n="13.10">chant</w> <w n="13.11">de</w> <w n="13.12">la</w> <w n="13.13">misère</w> ‒</l>
						<l n="14" num="4.2"><w n="14.1">Douloureuse</w> <w n="14.2">clameur</w> ‒ <w n="14.3">retenti</w> <w n="14.4">de</w> <w n="14.5">nouveau</w>.</l>
						<l n="15" num="4.3"><w n="15.1">L</w>’<w n="15.2">un</w> <w n="15.3">des</w> <w n="15.4">jeunes</w> <w n="15.5">martyrs</w> <w n="15.6">sollicite</w> <w n="15.7">sa</w> <w n="15.8">mère</w></l>
						<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">réduire</w> <w n="16.3">en</w> <w n="16.4">brasier</w> <w n="16.5">les</w> <w n="16.6">planches</w> <w n="16.7">du</w> <w n="16.8">berceau</w>…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Écoutez</w> ! <w n="17.2">au</w> <w n="17.3">dehors</w> <w n="17.4">des</w> <w n="17.5">voix</w> <w n="17.6">sourdes</w> <w n="17.7">murmurent</w> :</l>
						<l n="18" num="5.2"><w n="18.1">Aux</w> <w n="18.2">malheureux</w> <w n="18.3">sans</w> <w n="18.4">doute</w> <w n="18.5">on</w> <w n="18.6">vient</w> <w n="18.7">porter</w> <w n="18.8">secours</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Prêtez</w> <w n="19.2">l</w>’<w n="19.3">oreille</w> <w n="19.4">encor</w> ! <w n="19.5">mais</w> <w n="19.6">qu</w>’<w n="19.7">est</w>-<w n="19.8">ce</w> ? <w n="19.9">ces</w> <w n="19.10">voix</w> <w n="19.11">jurent</w></l>
						<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">maudissent</w> <w n="20.3">le</w> <w n="20.4">Dieu</w> <w n="20.5">qui</w> <w n="20.6">veille</w> <w n="20.7">sur</w> <w n="20.8">nos</w> <w n="20.9">jours</w> !…</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Qui</w> <w n="21.2">donc</w> <w n="21.3">ose</w> <w n="21.4">approcher</w>, <w n="21.5">le</w> <w n="21.6">blasphème</w> <w n="21.7">à</w> <w n="21.8">la</w> <w n="21.9">bouche</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Du</w> <w n="22.2">seuil</w> <w n="22.3">où</w> <w n="22.4">la</w> <w n="22.5">misère</w> <w n="22.6">étend</w> <w n="22.7">son</w> <w n="22.8">voile</w> <w n="22.9">noir</w> ?</l>
						<l n="23" num="6.3">‒ <w n="23.1">Ce</w> <w n="23.2">sont</w> <w n="23.3">deux</w> <w n="23.4">artisans</w>, <w n="23.5">avinés</w>, <w n="23.6">l</w>’<w n="23.7">œil</w> <w n="23.8">farouche</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Qui</w> <w n="24.2">traîne</w> <w n="24.3">sur</w> <w n="24.4">le</w> <w n="24.5">sol</w> <w n="24.6">un</w> <w n="24.7">homme</w> <w n="24.8">affreux</w> <w n="24.9">à</w> <w n="24.10">voir</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">cet</w> <w n="25.3">homme</w> <w n="25.4">est</w> <w n="25.5">le</w> <w n="25.6">chef</w> <w n="25.7">de</w> <w n="25.8">la</w> <w n="25.9">pauvre</w> <w n="25.10">famille</w> ‒</l>
						<l n="26" num="7.2"><w n="26.1">C</w>’<w n="26.2">est</w> <w n="26.3">le</w> <w n="26.4">père</w> <w n="26.5">annoncé</w> <w n="26.6">tantôt</w> <w n="26.7">comme</w> <w n="26.8">un</w> <w n="26.9">sauveur</w> ! ‒</l>
						<l n="27" num="7.3"><w n="27.1">Voyez</w>-<w n="27.2">le</w>, <w n="27.3">sous</w> <w n="27.4">les</w> <w n="27.5">feux</w> <w n="27.6">de</w> <w n="27.7">la</w> <w n="27.8">lune</w> <w n="27.9">qui</w> <w n="27.10">brille</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Étendu</w> <w n="28.2">sur</w> <w n="28.3">le</w> <w n="28.4">seuil</w> <w n="28.5">sans</w> <w n="28.6">voix</w> <w n="28.7">et</w> <w n="28.8">sans</w> <w n="28.9">vigueur</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">La</w> <w n="29.2">femme</w> <w n="29.3">ouvre</w> <w n="29.4">la</w> <w n="29.5">porte</w>, <w n="29.6">et</w>, <w n="29.7">tremblante</w>, <w n="29.8">s</w>’<w n="29.9">empresse</w></l>
						<l n="30" num="8.2"><w n="30.1">Auprès</w> <w n="30.2">du</w> <w n="30.3">malheureux</w> <w n="30.4">dont</w> <w n="30.5">les</w> <w n="30.6">traits</w> <w n="30.7">sont</w> <w n="30.8">flétris</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">Paraissant</w> <w n="31.2">oublier</w> <w n="31.3">sa</w> <w n="31.4">peine</w> <w n="31.5">et</w> <w n="31.6">sa</w> <w n="31.7">détresse</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Elle</w> <w n="32.2">lui</w> <w n="32.3">parle</w> <w n="32.4">même</w> <w n="32.5">avec</w> <w n="32.6">un</w> <w n="32.7">doux</w> <w n="32.8">souris</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">L</w>’<w n="33.2">ivrogne</w> <w n="33.3">veut</w> <w n="33.4">répondre</w> <w n="33.5">à</w> <w n="33.6">ces</w> <w n="33.7">élans</w> <w n="33.8">sublimes</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Mais</w> <w n="34.2">de</w> <w n="34.3">profonds</w> <w n="34.4">soupirs</w> <w n="34.5">entrecoupent</w> <w n="34.6">sa</w> <w n="34.7">voix</w>.</l>
						<l n="35" num="9.3"><w n="35.1">A</w> <w n="35.2">leur</w> <w n="35.3">tour</w> <w n="35.4">ses</w> <w n="35.5">enfants</w>, <w n="35.6">ou</w> <w n="35.7">plutôt</w> <w n="35.8">ses</w> <w n="35.9">victimes</w></l>
						<l n="36" num="9.4"><w n="36.1">Lui</w> <w n="36.2">demandent</w> <w n="36.3">du</w> <w n="36.4">pain</w>, <w n="36.5">des</w> <w n="36.6">vêtements</w>, <w n="36.7">du</w> <w n="36.8">bois</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Hélas</w> ! <w n="37.2">pauvres</w> <w n="37.3">petits</w>, <w n="37.4">votre</w> <w n="37.5">prière</w> <w n="37.6">est</w> <w n="37.7">vaine</w> !</l>
						<l n="38" num="10.2"><w n="38.1">Vains</w> <w n="38.2">aussi</w> <w n="38.3">vos</w> <w n="38.4">sanglots</w>, <w n="38.5">vos</w> <w n="38.6">plaintes</w>, <w n="38.7">vos</w> <w n="38.8">douleurs</w> !</l>
						<l n="39" num="10.3"><w n="39.1">Car</w> <w n="39.2">votre</w> <w n="39.3">père</w> <w n="39.4">à</w> <w n="39.5">mis</w> <w n="39.6">l</w>’<w n="39.7">argent</w> <w n="39.8">de</w> <w n="39.9">la</w> <w n="39.10">semaine</w></l>
						<l n="40" num="10.4"><w n="40.1">Au</w> <w n="40.2">cabaret</w>… <w n="40.3">Séchez</w> <w n="40.4">ces</w> <w n="40.5">inutiles</w> <w n="40.6">pleurs</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Que</w> <w n="41.2">dis</w>-<w n="41.3">je</w> ? <w n="41.4">oh</w>, <w n="41.5">non</w>, <w n="41.6">pleurez</w> ! <w n="41.7">et</w> <w n="41.8">les</w> <w n="41.9">nombreuses</w> <w n="41.10">larmes</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Que</w> <w n="42.2">votre</w> <w n="42.3">âme</w> <w n="42.4">innocente</w> <w n="42.5">en</w> <w n="42.6">priant</w> <w n="42.7">versera</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Toucheront</w> <w n="43.2">votre</w> <w n="43.3">père</w> ‒ <w n="43.4">Employez</w> <w n="43.5">donc</w> <w n="43.6">ces</w> <w n="43.7">armes</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">la</w> <w n="44.3">victoire</w>, <w n="44.4">enfants</w>, <w n="44.5">un</w> <w n="44.6">jour</w> <w n="44.7">vous</w> <w n="44.8">restera</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Du</w> <w n="45.2">mauvais</w> <w n="45.3">artisan</w> <w n="45.4">cet</w> <w n="45.5">ivrogne</w> <w n="45.6">est</w> <w n="45.7">l</w>’<w n="45.8">image</w>,</l>
						<l n="46" num="12.2"><w n="46.1">Car</w> <w n="46.2">l</w>’<w n="46.3">ivresse</w> <w n="46.4">affaiblit</w> <w n="46.5">les</w> <w n="46.6">cœurs</w> <w n="46.7">les</w> <w n="46.8">plus</w> <w n="46.9">vaillants</w> ;</l>
						<l n="47" num="12.3"><w n="47.1">Elle</w> <w n="47.2">étend</w> <w n="47.3">sur</w> <w n="47.4">notre</w> <w n="47.5">âme</w> <w n="47.6">un</w> <w n="47.7">lugubre</w> <w n="47.8">nuage</w></l>
						<l n="48" num="12.4"><w n="48.1">Qui</w> <w n="48.2">lui</w> <w n="48.3">cache</w> <w n="48.4">du</w> <w n="48.5">ciel</w> <w n="48.6">les</w> <w n="48.7">horizons</w> <w n="48.8">brillants</w> ;</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Elle</w> <w n="49.2">éloigne</w> <w n="49.3">l</w>’<w n="49.4">époux</w> <w n="49.5">du</w> <w n="49.6">foyer</w> <w n="49.7">domestique</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Où</w> <w n="50.2">longtemps</w> <w n="50.3">il</w> <w n="50.4">goûta</w> <w n="50.5">la</w> <w n="50.6">joie</w> <w n="50.7">et</w> <w n="50.8">le</w> <w n="50.9">bonheur</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Et</w> <w n="51.2">lorsqu</w>’<w n="51.3">il</w> <w n="51.4">y</w> <w n="51.5">revient</w>, <w n="51.6">sombre</w> <w n="51.7">et</w> <w n="51.8">mélancolique</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Il</w> <w n="52.2">porte</w> <w n="52.3">sur</w> <w n="52.4">le</w> <w n="52.5">front</w> <w n="52.6">le</w> <w n="52.7">sceau</w> <w n="52.8">du</w> <w n="52.9">déshonneur</w> !</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Ce</w> <w n="53.2">homme</w> <w n="53.3">était</w> <w n="53.4">jadis</w> <w n="53.5">un</w> <w n="53.6">artisan</w> <w n="53.7">modèle</w> ;</l>
						<l n="54" num="14.2"><w n="54.1">On</w> <w n="54.2">vantait</w> <w n="54.3">sa</w> <w n="54.4">sagesse</w> <w n="54.5">et</w> <w n="54.6">son</w> <w n="54.7">habileté</w> ;</l>
						<l n="55" num="14.3"><w n="55.1">Au</w> <w n="55.2">dur</w> <w n="55.3">labeur</w> <w n="55.4">jamais</w> <w n="55.5">il</w> <w n="55.6">n</w>’<w n="55.7">était</w> <w n="55.8">infidèle</w>,</l>
						<l n="56" num="14.4"><w n="56.1">Et</w> <w n="56.2">c</w>’<w n="56.3">est</w> <w n="56.4">là</w> <w n="56.5">qu</w>’<w n="56.6">il</w> <w n="56.7">puisait</w> <w n="56.8">la</w> <w n="56.9">force</w> <w n="56.10">et</w> <w n="56.11">la</w> <w n="56.12">santé</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Mais</w> <w n="57.2">quelle</w> <w n="57.3">affreuse</w> <w n="57.4">chute</w> ! <w n="57.5">En</w> <w n="57.6">moins</w> <w n="57.7">de</w> <w n="57.8">trois</w> <w n="57.9">années</w>,</l>
						<l n="58" num="15.2"><w n="58.1">Il</w> <w n="58.2">a</w> <w n="58.3">perdu</w> <w n="58.4">la</w> <w n="58.5">foi</w>, <w n="58.6">l</w>’<w n="58.7">énergie</w> <w n="58.8">et</w> <w n="58.9">l</w>’<w n="58.10">amour</w> !</l>
						<l n="59" num="15.3"><w n="59.1">Il</w> <w n="59.2">donne</w> <w n="59.3">au</w> <w n="59.4">cabaret</w> <w n="59.5">le</w> <w n="59.6">fruit</w> <w n="59.7">de</w> <w n="59.8">ses</w> <w n="59.9">journées</w>,</l>
						<l n="60" num="15.4"><w n="60.1">Pendant</w> <w n="60.2">qu</w>’<w n="60.3">à</w> <w n="60.4">sa</w> <w n="60.5">demeure</w> <w n="60.6">on</w> <w n="60.7">souffre</w> <w n="60.8">nuit</w> <w n="60.9">et</w> <w n="60.10">jour</w>…</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Le</w> <w n="61.2">monde</w> <w n="61.3">quelquefois</w> <w n="61.4">repousse</w> <w n="61.5">avec</w> <w n="61.6">malice</w></l>
						<l n="62" num="16.2"><w n="62.1">L</w>’<w n="62.2">enfant</w> <w n="62.3">qui</w>, <w n="62.4">tout</w> <w n="62.5">en</w> <w n="62.6">pleurs</w>, <w n="62.7">lui</w> <w n="62.8">tend</w> <w n="62.9">sa</w> <w n="62.10">maigre</w> <w n="62.11">main</w> ;</l>
						<l n="63" num="16.3">« <w n="63.1">Quoi</w> ! <w n="63.2">te</w> <w n="63.3">faire</w> <w n="63.4">l</w>’<w n="63.5">aumône</w> ? <w n="63.6">encourager</w> <w n="63.7">le</w> <w n="63.8">vice</w></l>
						<l n="64" num="16.4">« <w n="64.1">De</w> <w n="64.2">ton</w> <w n="64.3">père</w>, <w n="64.4">un</w> <w n="64.5">ivrogne</w> ?…. <w n="64.6">Éloigne</w>-<w n="64.7">toi</w>, <w n="64.8">gamin</w>… »</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Ce</w> <w n="65.2">langage</w> <w n="65.3">est</w> <w n="65.4">cruel</w>, <w n="65.5">déraisonnable</w>, <w n="65.6">impie</w> ‒</l>
						<l n="66" num="17.2"><w n="66.1">Faire</w> <w n="66.2">expier</w> <w n="66.3">au</w> <w n="66.4">fils</w> <w n="66.5">le</w> <w n="66.6">crime</w> <w n="66.7">des</w> <w n="66.8">parents</w> ! ‒</l>
						<l n="67" num="17.3"><w n="67.1">Rappelons</w>-<w n="67.2">nous</w> <w n="67.3">ces</w> <w n="67.4">mots</w> <w n="67.5">du</w> <w n="67.6">maître</w> <w n="67.7">de</w> <w n="67.8">la</w> <w n="67.9">vie</w> :</l>
						<l n="68" num="17.4">« <w n="68.1">Laissez</w> <w n="68.2">venir</w> <w n="68.3">à</w> <w n="68.4">moi</w> <w n="68.5">tous</w> <w n="68.6">les</w> <w n="68.7">petits</w> <w n="68.8">enfants</w> ! »</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">Ah</w> ! <w n="69.2">ne</w> <w n="69.3">laissons</w> <w n="69.4">jamais</w> <w n="69.5">à</w> <w n="69.6">leur</w> <w n="69.7">sort</w> <w n="69.8">misérable</w>,</l>
						<l n="70" num="18.2"><w n="70.1">Ces</w> <w n="70.2">enfants</w> <w n="70.3">dont</w> <w n="70.4">le</w> <w n="70.5">père</w> <w n="70.6">est</w> <w n="70.7">parfois</w> <w n="70.8">un</w> <w n="70.9">bandit</w> ;</l>
						<l n="71" num="18.3"><w n="71.1">Mais</w> <w n="71.2">faisons</w>-<w n="71.3">les</w> <w n="71.4">plutôt</w> <w n="71.5">asseoir</w> <w n="71.6">à</w> <w n="71.7">notre</w> <w n="71.8">table</w></l>
						<l n="72" num="18.4"><w n="72.1">En</w> <w n="72.2">leur</w> <w n="72.3">donnant</w> <w n="72.4">le</w> <w n="72.5">pain</w> <w n="72.6">du</w> <w n="72.7">corps</w> <w n="72.8">et</w> <w n="72.9">de</w> <w n="72.10">l</w>’<w n="72.11">esprit</w>.</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><w n="73.1">Nos</w> <w n="73.2">bienfaits</w> <w n="73.3">trouveront</w> <w n="73.4">mille</w> <w n="73.5">échos</w> <w n="73.6">dans</w> <w n="73.7">leur</w> <w n="73.8">âme</w> ‒</l>
						<l n="74" num="19.2"><w n="74.1">Leur</w> <w n="74.2">âme</w> <w n="74.3">si</w> <w n="74.4">sensible</w> <w n="74.5">aux</w> <w n="74.6">élans</w> <w n="74.7">généreux</w> ‒</l>
						<l n="75" num="19.3"><w n="75.1">Et</w>, <w n="75.2">plus</w> <w n="75.3">tard</w>, <w n="75.4">la</w> <w n="75.5">vertu</w> ‒ <w n="75.6">cette</w> <w n="75.7">céleste</w> <w n="75.8">flamme</w> ‒</l>
						<l n="76" num="19.4"><w n="76.1">Réchauffera</w> <w n="76.2">leurs</w> <w n="76.3">cœurs</w> <w n="76.4">en</w> <w n="76.5">les</w> <w n="76.6">rendant</w> <w n="76.7">heureux</w>.</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1"><w n="77.1">Du</w> <w n="77.2">mauvais</w> <w n="77.3">artisan</w> <w n="77.4">et</w> <w n="77.5">de</w> <w n="77.6">ses</w> <w n="77.7">habitudes</w></l>
						<l n="78" num="20.2"><w n="78.1">Il</w> <w n="78.2">ne</w> <w n="78.3">leur</w> <w n="78.4">restera</w> <w n="78.5">qu</w>’<w n="78.6">un</w> <w n="78.7">pâle</w> <w n="78.8">souvenir</w>.</l>
						<l n="79" num="20.3"><w n="79.1">Joyeux</w>, <w n="79.2">ils</w> <w n="79.3">rempliront</w> <w n="79.4">les</w> <w n="79.5">tâches</w> <w n="79.6">les</w> <w n="79.7">plus</w> <w n="79.8">rudes</w>,</l>
						<l n="80" num="20.4"><w n="80.1">Sous</w> <w n="80.2">le</w> <w n="80.3">regard</w> <w n="80.4">de</w> <w n="80.5">Dieu</w>, <w n="80.6">sans</w> <w n="80.7">craindre</w> <w n="80.8">l</w>’<w n="80.9">avenir</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1889">1er octobre 1889</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>