<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS</head><div type="poem" key="CAO41">
					<head type="main">QUÉBEC</head>
					<opener>
						<salute>A M. NAPOLÉON LEGENDRE</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Assise</w> <w n="1.2">sur</w> <w n="1.3">le</w> <w n="1.4">haut</w> <w n="1.5">d</w>’<w n="1.6">un</w> <w n="1.7">vaste</w> <w n="1.8">promontoire</w></l>
						<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2">où</w> <w n="2.3">le</w> <w n="2.4">regard</w> <w n="2.5">embrasse</w> <w n="2.6">un</w> <w n="2.7">féerique</w> <w n="2.8">tableau</w>,</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">ville</w> <w n="3.3">de</w> <w n="3.4">Québec</w> <w n="3.5">semble</w> <w n="3.6">du</w> <w n="3.7">territoire</w></l>
						<l n="4" num="1.4"><w n="4.1">Être</w> <w n="4.2">la</w> <w n="4.3">sentinelle</w> <w n="4.4">ou</w> <w n="4.5">le</w> <w n="4.6">porte</w>-<w n="4.7">drapeau</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ses</w> <w n="5.2">vieux</w> <w n="5.3">murs</w> <w n="5.4">délabrés</w>, <w n="5.5">qui</w> <w n="5.6">faisaient</w> <w n="5.7">notre</w> <w n="5.8">gloire</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Tombent</w> <w n="6.2">de</w> <w n="6.3">jour</w> <w n="6.4">en</w> <w n="6.5">jour</w> <w n="6.6">sous</w> <w n="6.7">les</w> <w n="6.8">coups</w> <w n="6.9">du</w> <w n="6.10">marteau</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">N</w>’<w n="7.2">importe</w> ! <w n="7.3">elle</w> <w n="7.4">progresse</w>, <w n="7.5">et</w> <w n="7.6">son</w> <w n="7.7">nom</w> <w n="7.8">dans</w> <w n="7.9">l</w>’<w n="7.10">histoire</w></l>
						<l n="8" num="2.4"><w n="8.1">N</w>’<w n="8.2">en</w> <w n="8.3">brillera</w> <w n="8.4">pas</w> <w n="8.5">moins</w> <w n="8.6">d</w>’<w n="8.7">un</w> <w n="8.8">éclat</w> <w n="8.9">pur</w> <w n="8.10">et</w> <w n="8.11">beau</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Elle</w> <w n="9.2">a</w> <w n="9.3">dormi</w> <w n="9.4">longtemps</w> ; <w n="9.5">la</w> <w n="9.6">voilà</w> <w n="9.7">qui</w> <w n="9.8">se</w> <w n="9.9">lève</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Un</w> <w n="10.2">pont</w> <w n="10.3">traversera</w>, <w n="10.4">de</w> <w n="10.5">l</w>’<w n="10.6">une</w> <w n="10.7">à</w> <w n="10.8">l</w>’<w n="10.9">autre</w> <w n="10.10">grève</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">cours</w> <w n="11.3">majestueux</w> <w n="11.4">du</w> <w n="11.5">large</w> <w n="11.6">Saint</w>-<w n="11.7">Laurent</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">De</w> <w n="12.2">superbes</w> <w n="12.3">palais</w> <w n="12.4">embelliront</w> <w n="12.5">ses</w> <w n="12.6">rues</w> ;</l>
						<l n="13" num="4.2"><w n="13.1">Des</w> <w n="13.2">hôtels</w> <w n="13.3">dresseront</w> <w n="13.4">leurs</w> <w n="13.5">dômes</w> <w n="13.6">dans</w> <w n="13.7">les</w> <w n="13.8">nues</w> ;</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">l</w>’<w n="14.3">immortel</w> <w n="14.4">Champlain</w> <w n="14.5">aura</w> <w n="14.6">son</w> <w n="14.7">monument</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1889">1er mars 1889</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>