<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO2">
					<head type="main">RENOUVEAU</head>
					<opener>
						<salute>A M. BENJAMIN SULTE</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">doux</w> <w n="1.3">printemps</w> <w n="1.4">vient</w> <w n="1.5">de</w> <w n="1.6">paraître</w></l>
						<l n="2" num="1.2"><w n="2.1">Sous</w> <w n="2.2">son</w> <w n="2.3">manteau</w> <w n="2.4">de</w> <w n="2.5">velours</w> <w n="2.6">vert</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">déjà</w> <w n="3.3">l</w>’<w n="3.4">on</w> <w n="3.5">voit</w> <w n="3.6">disparaître</w></l>
						<l n="4" num="1.4"><w n="4.1">Tous</w> <w n="4.2">les</w> <w n="4.3">vestiges</w> <w n="4.4">de</w> <w n="4.5">l</w>’<w n="4.6">hiver</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Son</w> <w n="5.2">œil</w> <w n="5.3">à</w> <w n="5.4">l</w>’<w n="5.5">éclat</w> <w n="5.6">de</w> <w n="5.7">la</w> <w n="5.8">braise</w> :</l>
						<l n="6" num="2.2"><w n="6.1">A</w> <w n="6.2">la</w> <w n="6.3">chaleur</w> <w n="6.4">de</w> <w n="6.5">ses</w> <w n="6.6">rayons</w></l>
						<l n="7" num="2.3"><w n="7.1">Naissent</w> <w n="7.2">lilas</w>, <w n="7.3">fleur</w>, <w n="7.4">rose</w> <w n="7.5">et</w> <w n="7.6">fraise</w>.</l>
						<l n="8" num="2.4"><w n="8.1">Abeilles</w> <w n="8.2">d</w>’<w n="8.3">or</w> <w n="8.4">et</w> <w n="8.5">papillons</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">arbres</w> <w n="9.3">engourdis</w> <w n="9.4">naguère</w></l>
						<l n="10" num="3.2"><w n="10.1">Semblent</w> <w n="10.2">dresser</w> <w n="10.3">plus</w> <w n="10.4">haut</w> <w n="10.5">le</w> <w n="10.6">front</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Car</w> <w n="11.2">la</w> <w n="11.3">nature</w>, <w n="11.4">en</w> <w n="11.5">bonne</w> <w n="11.6">mère</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Verse</w> <w n="12.2">la</w> <w n="12.3">sève</w> <w n="12.4">dans</w> <w n="12.5">leur</w> <w n="12.6">tronc</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Au</w> <w n="13.2">plus</w> <w n="13.3">épais</w> <w n="13.4">de</w> <w n="13.5">la</w> <w n="13.6">ramure</w></l>
						<l n="14" num="4.2"><w n="14.1">Les</w> <w n="14.2">oiseaux</w> <w n="14.3">préparent</w> <w n="14.4">leurs</w> <w n="14.5">nids</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Sans</w> <w n="15.2">s</w>’<w n="15.3">occuper</w> <w n="15.4">si</w> <w n="15.5">la</w> <w n="15.6">pâture</w></l>
						<l n="16" num="4.4"><w n="16.1">Ou</w> <w n="16.2">le</w> <w n="16.3">lin</w> <w n="16.4">leur</w> <w n="16.5">seront</w> <w n="16.6">fournis</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Du</w> <w n="17.2">sol</w> <w n="17.3">jaillit</w> <w n="17.4">plus</w> <w n="17.5">d</w>’<w n="17.6">une</w> <w n="17.7">source</w></l>
						<l n="18" num="5.2"><w n="18.1">Que</w> <w n="18.2">la</w> <w n="18.3">froidure</w> <w n="18.4">emprisonnait</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">le</w> <w n="19.3">ruisseau</w> <w n="19.4">reprend</w> <w n="19.5">sa</w> <w n="19.6">course</w></l>
						<l n="20" num="5.4"><w n="20.1">A</w> <w n="20.2">travers</w> <w n="20.3">clos</w> <w n="20.4">et</w> <w n="20.5">jardinet</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Sur</w> <w n="21.2">le</w> <w n="21.3">bord</w> <w n="21.4">de</w> <w n="21.5">maintes</w> <w n="21.6">rivières</w></l>
						<l n="22" num="6.2"><w n="22.1">L</w>’<w n="22.2">on</w> <w n="22.3">voit</w> <w n="22.4">le</w> <w n="22.5">castor</w> <w n="22.6">vigilant</w></l>
						<l n="23" num="6.3"><w n="23.1">Transporter</w> <w n="23.2">le</w> <w n="23.3">bois</w> <w n="23.4">et</w> <w n="23.5">les</w> <w n="23.6">pierres</w></l>
						<l n="24" num="6.4"><w n="24.1">Pour</w> <w n="24.2">bâtir</w> <w n="24.3">son</w> <w n="24.4">gîte</w> <w n="24.5">étonnant</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">La</w> <w n="25.2">brise</w>, <w n="25.3">sylphide</w> <w n="25.4">légère</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Fait</w> <w n="26.2">la</w> <w n="26.3">cour</w> <w n="26.4">à</w> <w n="26.5">toutes</w> <w n="26.6">les</w> <w n="26.7">fleurs</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Puis</w> <w n="27.2">vole</w> <w n="27.3">embaumer</w> <w n="27.4">l</w>’<w n="27.5">atmosphère</w></l>
						<l n="28" num="7.4"><w n="28.1">Des</w> <w n="28.2">plus</w> <w n="28.3">enivrantes</w> <w n="28.4">senteurs</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">De</w> <w n="29.2">la</w> <w n="29.3">cime</w> <w n="29.4">de</w> <w n="29.5">nos</w> <w n="29.6">montagnes</w></l>
						<l n="30" num="8.2"><w n="30.1">Se</w> <w n="30.2">précipite</w> <w n="30.3">le</w> <w n="30.4">torrent</w></l>
						<l n="31" num="8.3"><w n="31.1">Qui</w> <w n="31.2">fertilise</w> <w n="31.3">nos</w> <w n="31.4">campagnes</w></l>
						<l n="32" num="8.4"><w n="32.1">Avec</w> <w n="32.2">les</w> <w n="32.3">eaux</w> <w n="32.4">du</w> <w n="32.5">Saint</w>-<w n="32.6">Laurent</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">A</w> <w n="33.2">nos</w> <w n="33.3">fenêtres</w>, <w n="33.4">l</w>’<w n="33.5">hirondelle</w></l>
						<l n="34" num="9.2"><w n="34.1">S</w>’<w n="34.2">annonce</w> <w n="34.3">par</w> <w n="34.4">des</w> <w n="34.5">cris</w> <w n="34.6">joyeux</w> ;</l>
						<l n="35" num="9.3"><w n="35.1">Elle</w> <w n="35.2">revient</w> <w n="35.3">à</w> <w n="35.4">tire</w>-<w n="35.5">d</w>’<w n="35.6">aile</w></l>
						<l n="36" num="9.4"><w n="36.1">Charmer</w> <w n="36.2">les</w> <w n="36.3">jeunes</w> <w n="36.4">et</w> <w n="36.5">les</w> <w n="36.6">vieux</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Au</w> <w n="37.2">palais</w> <w n="37.3">comme</w> <w n="37.4">à</w> <w n="37.5">la</w> <w n="37.6">chaumière</w>,</l>
						<l n="38" num="10.2"><w n="38.1">La</w> <w n="38.2">porte</w> <w n="38.3">s</w>’<w n="38.4">ouvre</w> <w n="38.5">à</w> <w n="38.6">deux</w> <w n="38.7">battants</w> :</l>
						<l n="39" num="10.3"><w n="39.1">Riche</w> <w n="39.2">et</w> <w n="39.3">pauvre</w> <w n="39.4">ont</w> <w n="39.5">soif</w> <w n="39.6">de</w> <w n="39.7">lumière</w></l>
						<l n="40" num="10.4"><w n="40.1">D</w>’<w n="40.2">air</w> <w n="40.3">pur</w>, <w n="40.4">de</w> <w n="40.5">parfums</w> <w n="40.6">odorants</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Parfois</w> <w n="41.2">l</w>’<w n="41.3">on</w> <w n="41.4">quitte</w> <w n="41.5">sa</w> <w n="41.6">demeure</w></l>
						<l n="42" num="11.2"><w n="42.1">Pour</w> <w n="42.2">aller</w> <w n="42.3">prendre</w> <w n="42.4">un</w> <w n="42.5">gai</w> <w n="42.6">repas</w></l>
						<l n="43" num="11.3"><w n="43.1">Sur</w> <w n="43.2">la</w> <w n="43.3">pelouse</w> <w n="43.4">où</w> <w n="43.5">toute</w> <w n="43.6">à</w> <w n="43.7">l</w>’<w n="43.8">heure</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Bébé</w> <w n="44.2">fera</w> <w n="44.3">ses</w> <w n="44.4">premiers</w> <w n="44.5">pas</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Plus</w> <w n="45.2">loin</w> <w n="45.3">les</w> <w n="45.4">colons</w> <w n="45.5">sur</w> <w n="45.6">leur</w> <w n="45.7">terre</w></l>
						<l n="46" num="12.2"><w n="46.1">Travaillent</w> <w n="46.2">courageusement</w></l>
						<l n="47" num="12.3"><w n="47.1">A</w> <w n="47.2">l</w>’<w n="47.3">œuvre</w> <w n="47.4">utile</w> <w n="47.5">et</w> <w n="47.6">salutaire</w></l>
						<l n="48" num="12.4"><w n="48.1">Qu</w>’<w n="48.2">on</w> <w n="48.3">nomme</w> <w n="48.4">le</w> <w n="48.5">défrichement</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Les</w> <w n="49.2">uns</w> <w n="49.3">creusent</w>, <w n="49.4">les</w> <w n="49.5">autres</w> <w n="49.6">sèment</w></l>
						<l n="50" num="13.2"><w n="50.1">Ou</w> <w n="50.2">bien</w> <w n="50.3">coupent</w> <w n="50.4">les</w> <w n="50.5">arbres</w> <w n="50.6">morts</w> ;</l>
						<l n="51" num="13.3"><w n="51.1">Ces</w> <w n="51.2">braves</w> <w n="51.3">bûchent</w>, <w n="51.4">chantent</w>, <w n="51.5">s</w>’<w n="51.6">aiment</w></l>
						<l n="52" num="13.4"><w n="52.1">Et</w> <w n="52.2">dorment</w> <w n="52.3">la</w> <w n="52.4">nuit</w> <w n="52.5">sans</w> <w n="52.6">remords</w> !</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">La</w> <w n="53.2">fillette</w> <w n="53.3">en</w> <w n="53.4">robe</w> <w n="53.5">de</w> <w n="53.6">bure</w></l>
						<l n="54" num="14.2"><w n="54.1">Chante</w> <w n="54.2">et</w> <w n="54.3">cultive</w> <w n="54.4">tout</w> <w n="54.5">le</w> <w n="54.6">jour</w> ;</l>
						<l n="55" num="14.3"><w n="55.1">Le</w> <w n="55.2">soir</w> <w n="55.3">venu</w>, <w n="55.4">sa</w> <w n="55.5">lèvre</w> <w n="55.6">pure</w></l>
						<l n="56" num="14.4"><w n="56.1">Dira</w> <w n="56.2">peut</w>-<w n="56.3">être</w> <w n="56.4">un</w> <w n="56.5">mot</w> <w n="56.6">d</w>’<w n="56.7">amour</w> !…</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Oui</w>, <w n="57.2">l</w>’<w n="57.3">homme</w>, <w n="57.4">les</w> <w n="57.5">oiseaux</w>, <w n="57.6">les</w> <w n="57.7">plantes</w></l>
						<l n="58" num="15.2"><w n="58.1">Et</w> <w n="58.2">l</w>’<w n="58.3">onde</w> <w n="58.4">aux</w> <w n="58.5">bruits</w> <w n="58.6">mystérieux</w></l>
						<l n="59" num="15.3"><w n="59.1">Mêlent</w> <w n="59.2">leurs</w> <w n="59.3">voix</w> <w n="59.4">reconnaissantes</w></l>
						<l n="60" num="15.4"><w n="60.1">Pour</w> <w n="60.2">célébrer</w> <w n="60.3">le</w> <w n="60.4">Roi</w> <w n="60.5">des</w> <w n="60.6">cieux</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Car</w> <w n="61.2">tout</w> <w n="61.3">ce</w> <w n="61.4">qui</w> <w n="61.5">vit</w> <w n="61.6">et</w> <w n="61.7">respire</w>,</l>
						<l n="62" num="16.2"><w n="62.1">Tout</w> <w n="62.2">ce</w> <w n="62.3">qui</w> <w n="62.4">chante</w>, <w n="62.5">pleure</w> <w n="62.6">ou</w> <w n="62.7">croit</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Reconnaît</w> <w n="63.2">qu</w>’<w n="63.3">il</w> <w n="63.4">est</w> <w n="63.5">sous</w> <w n="63.6">l</w>’<w n="63.7">empire</w></l>
						<l n="64" num="16.4"><w n="64.1">D</w>’<w n="64.2">un</w> <w n="64.3">esprit</w> <w n="64.4">souverain</w> <w n="64.5">et</w> <w n="64.6">droit</w> !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Printemps</w>, <w n="65.2">réveil</w> <w n="65.3">de</w> <w n="65.4">la</w> <w n="65.5">nature</w>,</l>
						<l n="66" num="17.2"><w n="66.1">Oh</w> ! <w n="66.2">sois</w> <w n="66.3">le</w> <w n="66.4">bienvenu</w> <w n="66.5">toujours</w> !</l>
						<l n="67" num="17.3"><w n="67.1">Quand</w> <w n="67.2">tu</w> <w n="67.3">parais</w>, <w n="67.4">la</w> <w n="67.5">créature</w></l>
						<l n="68" num="17.4"><w n="68.1">Espère</w> <w n="68.2">encore</w> <w n="68.3">des</w> <w n="68.4">beaux</w> <w n="68.5">jours</w> !</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">C</w>’<w n="69.2">est</w> <w n="69.3">toi</w> <w n="69.4">qui</w> <w n="69.5">donnes</w> <w n="69.6">à</w> <w n="69.7">la</w> <w n="69.8">plaine</w></l>
						<l n="70" num="18.2"><w n="70.1">Son</w> <w n="70.2">riche</w> <w n="70.3">et</w> <w n="70.4">moelleux</w> <w n="70.5">vêtement</w> ;</l>
						<l n="71" num="18.3"><w n="71.1">C</w>’<w n="71.2">est</w> <w n="71.3">toi</w> <w n="71.4">qui</w> <w n="71.5">fais</w> <w n="71.6">germer</w> <w n="71.7">la</w> <w n="71.8">graine</w></l>
						<l n="72" num="18.4"><w n="72.1">D</w>’<w n="72.2">où</w> <w n="72.3">sortira</w> <w n="72.4">notre</w> <w n="72.5">aliment</w> !</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><w n="73.1">C</w>’<w n="73.2">est</w> <w n="73.3">toi</w> <w n="73.4">qui</w> <w n="73.5">rends</w> <w n="73.6">au</w> <w n="73.7">pulmonaire</w></l>
						<l n="74" num="19.2"><w n="74.1">La</w> <w n="74.2">force</w> <w n="74.3">et</w> <w n="74.4">souvent</w> <w n="74.5">la</w> <w n="74.6">santé</w> ;</l>
						<l n="75" num="19.3"><w n="75.1">C</w>’<w n="75.2">est</w> <w n="75.3">toi</w> <w n="75.4">que</w> <w n="75.5">l</w>’<w n="75.6">Indien</w> <w n="75.7">vénère</w></l>
						<l n="76" num="19.4"><w n="76.1">En</w> <w n="76.2">recouvrant</w> <w n="76.3">la</w> <w n="76.4">liberté</w> !</l>
					</lg>
					<ab type="dot">⁂</ab>
					<lg n="20">
						<l n="77" num="20.1"><w n="77.1">O</w> <w n="77.2">printemps</w>, <w n="77.3">messager</w> <w n="77.4">Céleste</w>,</l>
						<l n="78" num="20.2"><w n="78.1">Admirable</w> <w n="78.2">consolateur</w></l>
						<l n="79" num="20.3"><w n="79.1">Ton</w> <w n="79.2">éclat</w> <w n="79.3">seul</w> <w n="79.4">nous</w> <w n="79.5">manifeste</w></l>
						<l n="80" num="20.4"><w n="80.1">La</w> <w n="80.2">puissance</w> <w n="80.3">du</w> <w n="80.4">Créateur</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1887">4 juin 1887</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>