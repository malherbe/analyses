<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO26">
					<head type="main">ELLE EST MORTE !</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Rose</w> <w n="1.2">avait</w> <w n="1.3">dix</w> <w n="1.4">sept</w> <w n="1.5">ans</w> ; <w n="1.6">elle</w> <w n="1.7">était</w> <w n="1.8">belle</w> <w n="1.9">et</w> <w n="1.10">blonde</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Sur</w> <w n="2.2">son</w> <w n="2.3">front</w> <w n="2.4">les</w> <w n="2.5">rayons</w> <w n="2.6">de</w> <w n="2.7">la</w> <w n="2.8">candeur</w> <w n="2.9">brillaient</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">perles</w> <w n="3.3">de</w> <w n="3.4">sa</w> <w n="3.5">bouche</w> <w n="3.6">enchantaient</w> <w n="3.7">tout</w> <w n="3.8">le</w> <w n="3.9">monde</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Ses</w> <w n="4.2">cheveux</w> <w n="4.3">en</w> <w n="4.4">flots</w> <w n="4.5">d</w>’<w n="4.6">or</w> <w n="4.7">jusqu</w>’<w n="4.8">à</w> <w n="4.9">ses</w> <w n="4.10">pieds</w> <w n="4.11">roulaient</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ses</w> <w n="5.2">lèvres</w> <w n="5.3">souriaient</w> <w n="5.4">comme</w> <w n="5.5">celles</w> <w n="5.6">d</w>’<w n="5.7">un</w> <w n="5.8">ange</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Son</w> <w n="6.2">œil</w> <w n="6.3">d</w>’<w n="6.4">azur</w> <w n="6.5">jetant</w> <w n="6.6">un</w> <w n="6.7">vif</w> <w n="6.8">rayonnement</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Sa</w> <w n="7.2">voix</w> <w n="7.3">avait</w> <w n="7.4">parfois</w> <w n="7.5">une</w> <w n="7.6">harmonie</w> <w n="7.7">étrange</w></l>
						<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">me</w> <w n="8.3">plongeant</w> <w n="8.4">soudain</w> <w n="8.5">dans</w> <w n="8.6">le</w> <w n="8.7">ravissement</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Quand</w> <w n="9.2">venait</w> <w n="9.3">le</w> <w n="9.4">printemps</w> <w n="9.5">avec</w> <w n="9.6">ses</w> <w n="9.7">nids</w> <w n="9.8">de</w> <w n="9.9">mousse</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Ses</w> <w n="10.2">brises</w>, <w n="10.3">ses</w> <w n="10.4">parfums</w>, <w n="10.5">son</w> <w n="10.6">soleil</w> <w n="10.7">radieux</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Nous</w> <w n="11.2">allions</w>, <w n="11.3">elle</w> <w n="11.4">et</w> <w n="11.5">moi</w>, ‒ <w n="11.6">réminiscence</w> <w n="11.7">douce</w> ‒</l>
						<l n="12" num="3.4"><w n="12.1">Tout</w> <w n="12.2">pensifs</w>, <w n="12.3">nous</w> <w n="12.4">asseoir</w> <w n="12.5">sur</w> <w n="12.6">le</w> <w n="12.7">gazon</w> <w n="12.8">soyeux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">là</w> <w n="13.3">nous</w> <w n="13.4">admirions</w> <w n="13.5">le</w> <w n="13.6">couchant</w> <w n="13.7">et</w> <w n="13.8">l</w>’<w n="13.9">aurore</w></l>
						<l n="14" num="4.2"><w n="14.1">Déployant</w> <w n="14.2">à</w> <w n="14.3">notre</w> <w n="14.4">œil</w> <w n="14.5">leurs</w> <w n="14.6">tableaux</w> <w n="14.7">gracieux</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">nos</w> <w n="15.3">cœurs</w> <w n="15.4">bénissaient</w> <w n="15.5">l</w>’<w n="15.6">Artiste</w> <w n="15.7">que</w> <w n="15.8">décore</w></l>
						<l n="16" num="4.4"><w n="16.1">Toute</w> <w n="16.2">l</w>’<w n="16.3">immensité</w> <w n="16.4">de</w> <w n="16.5">la</w> <w n="16.6">terre</w> <w n="16.7">et</w> <w n="16.8">des</w> <w n="16.9">cieux</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Aux</w> <w n="17.2">coupes</w> <w n="17.3">de</w> <w n="17.4">l</w>’<w n="17.5">espoir</w> <w n="17.6">nous</w> <w n="17.7">abreuvions</w> <w n="17.8">notre</w> <w n="17.9">âme</w> ;</l>
						<l n="18" num="5.2"><w n="18.1">Un</w> <w n="18.2">heureux</w> <w n="18.3">avenir</w> <w n="18.4">brillait</w> <w n="18.5">dans</w> <w n="18.6">le</w> <w n="18.7">lointain</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">L</w>’<w n="19.2">Hymen</w> <w n="19.3">allait</w> <w n="19.4">bientôt</w> <w n="19.5">nous</w> <w n="19.6">verser</w> <w n="19.7">son</w> <w n="19.8">dictame</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Mais</w>, <w n="20.2">hélas</w> ! <w n="20.3">nous</w> <w n="20.4">comptions</w> <w n="20.5">sans</w> <w n="20.6">le</w> <w n="20.7">cruel</w> <w n="20.8">destin</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">maintenant</w>, <w n="21.3">voyez</w> : <w n="21.4">elle</w> <w n="21.5">est</w> <w n="21.6">là</w> <w n="21.7">qui</w> <w n="21.8">repose</w></l>
						<l n="22" num="6.2"><w n="22.1">Sous</w> <w n="22.2">la</w> <w n="22.3">terre</w> <w n="22.4">où</w> <w n="22.5">chacun</w> <w n="22.6">tôt</w> <w n="22.7">ou</w> <w n="22.8">tard</w> <w n="22.9">doit</w> <w n="22.10">dormir</w> !</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">tout</w> <w n="23.3">ce</w> <w n="23.4">qui</w> <w n="23.5">me</w> <w n="23.6">reste</w> <w n="23.7">aujourd</w>’<w n="23.8">hui</w> <w n="23.9">de</w> <w n="23.10">ma</w> <hi rend="ital"><w n="23.11">Rose</w></hi>,</l>
						<l n="24" num="6.4"><w n="24.1">C</w>’<w n="24.2">est</w> <w n="24.3">le</w> <w n="24.4">parfum</w> <w n="24.5">que</w> <w n="24.6">m</w>’<w n="24.7">a</w> <w n="24.8">laissé</w> <w n="24.9">son</w> <w n="24.10">souvenir</w>…</l>
					</lg>
					<closer>
						<dateline>
							<date when="1879">Avril 1879</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>