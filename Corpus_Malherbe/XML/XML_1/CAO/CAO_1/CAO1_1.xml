<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO1">
					<head type="main">LE BONHEUR</head>
					<opener>
						<salute>A MA FEMME</salute>
						<epigraph>
							<cit>
								<quote>
									Où donc est le bonheur ? disais-je. ‒ Infortuné !<lb></lb>
									Le bonheur, ô mon Dieu, vous me l’avez donné.
								</quote>
								<bibl>
									<name>VICTOR HUGO</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">cherché</w> <w n="1.4">vainement</w> <w n="1.5">dans</w> <w n="1.6">les</w> <w n="1.7">bruyantes</w> <w n="1.8">fêtes</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">l</w>’<w n="2.3">éclat</w> <w n="2.4">des</w> <w n="2.5">plaisirs</w> <w n="2.6">éblouit</w> <w n="2.7">tant</w> <w n="2.8">de</w> <w n="2.9">têtes</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ce</w> <w n="3.2">trésor</w> <w n="3.3">précieux</w> <w n="3.4">qu</w>’<w n="3.5">on</w> <w n="3.6">nomme</w> <w n="3.7">le</w> <w n="3.8">bonheur</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">l</w>’<w n="4.3">ai</w> <w n="4.4">cherché</w> <w n="4.5">d</w>’<w n="4.6">abord</w> <w n="4.7">sur</w> <w n="4.8">le</w> <w n="4.9">sol</w> <w n="4.10">que</w> <w n="4.11">je</w> <w n="4.12">foule</w></l>
						<l n="5" num="1.5"><w n="5.1">En</w> <w n="5.2">voulant</w> <w n="5.3">soulever</w> <w n="5.4">les</w> <w n="5.5">bravos</w> <w n="5.6">de</w> <w n="5.7">la</w> <w n="5.8">foule</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">je</w> <w n="6.3">n</w>’<w n="6.4">ai</w> <w n="6.5">recueilli</w> <w n="6.6">qu</w>’<w n="6.7">un</w> <w n="6.8">éphémère</w> <w n="6.9">honneur</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Pour</w> <w n="7.2">le</w> <w n="7.3">trouver</w>, <w n="7.4">j</w>’<w n="7.5">ai</w> <w n="7.6">fait</w> <w n="7.7">de</w> <w n="7.8">pénibles</w> <w n="7.9">voyages</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Franchi</w> <w n="8.2">les</w> <w n="8.3">flots</w> <w n="8.4">amers</w>, <w n="8.5">parcouru</w> <w n="8.6">maints</w> <w n="8.7">villages</w></l>
						<l n="9" num="2.3"><w n="9.1">Où</w> <w n="9.2">la</w> <w n="9.3">vive</w> <w n="9.4">gaîté</w> <w n="9.5">faisait</w> <w n="9.6">battre</w> <w n="9.7">les</w> <w n="9.8">cœurs</w> ;</l>
						<l n="10" num="2.4"><w n="10.1">Mais</w>, <w n="10.2">ô</w> <w n="10.3">fatalité</w> ! <w n="10.4">la</w> <w n="10.5">sombre</w> <w n="10.6">nostalgie</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Ce</w> <w n="11.2">désir</w> <w n="11.3">violent</w> <w n="11.4">de</w> <w n="11.5">revoir</w> <w n="11.6">la</w> <w n="11.7">patrie</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Aggravait</w> <w n="12.2">chaque</w> <w n="12.3">jour</w> <w n="12.4">le</w> <w n="12.5">poids</w> <w n="12.6">de</w> <w n="12.7">mes</w> <w n="12.8">malheurs</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Après</w> <w n="13.2">avoir</w> <w n="13.3">vécu</w> <w n="13.4">sur</w> <w n="13.5">la</w> <w n="13.6">plage</w> <w n="13.7">étrangère</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Sans</w> <w n="14.2">ressource</w> <w n="14.3">et</w> <w n="14.4">craignant</w> <w n="14.5">la</w> <w n="14.6">main</w> <w n="14.7">de</w> <w n="14.8">la</w> <w n="14.9">misère</w>,</l>
						<l n="15" num="3.3"><w n="15.1">Je</w> <w n="15.2">revins</w> <w n="15.3">au</w> <w n="15.4">pays</w> <w n="15.5">avec</w> <w n="15.6">le</w> <w n="15.7">fol</w> <w n="15.8">espoir</w></l>
						<l n="16" num="3.4"><w n="16.1">De</w> <w n="16.2">trouver</w> <w n="16.3">le</w> <w n="16.4">bonheur</w> <w n="16.5">en</w> <w n="16.6">l</w>’<w n="16.7">amitié</w> <w n="16.8">sincère</w></l>
						<l n="17" num="3.5"><w n="17.1">D</w>’<w n="17.2">hommes</w> <w n="17.3">que</w> <w n="17.4">mainte</w> <w n="17.5">fois</w> <w n="17.6">j</w>’<w n="17.7">avais</w> <w n="17.8">aidés</w> <w n="17.9">naguère</w>.</l>
						<l n="18" num="3.6"><w n="18.1">Mais</w> <w n="18.2">les</w> <w n="18.3">cruels</w> <w n="18.4">ingrats</w> <w n="18.5">rougirent</w> <w n="18.6">de</w> <w n="18.7">me</w> <w n="18.8">voir</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Le</w> <w n="19.2">bonheur</w> !… <w n="19.3">pour</w> <w n="19.4">l</w>’<w n="19.5">avoir</w> <w n="19.6">j</w>’<w n="19.7">ai</w> <w n="19.8">gravi</w> <w n="19.9">le</w> <w n="19.10">Parnasse</w></l>
						<l n="20" num="4.2"><w n="20.1">Sur</w> <w n="20.2">la</w> <w n="20.3">cime</w> <w n="20.4">duquel</w> <w n="20.5">les</w> <w n="20.6">disciples</w> <w n="20.7">d</w>’<w n="20.8">Horace</w></l>
						<l n="21" num="4.3"><w n="21.1">Buvaient</w> <w n="21.2">le</w> <w n="21.3">doux</w> <w n="21.4">nectar</w> <w n="21.5">que</w> <w n="21.6">leur</w> <w n="21.7">versaient</w> <w n="21.8">les</w> <w n="21.9">dieux</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">J</w>’<w n="22.2">allais</w> <w n="22.3">toucher</w> <w n="22.4">au</w> <w n="22.5">but</w>, <w n="22.6">quand</w> <w n="22.7">mon</w> <w n="22.8">lâche</w> <w n="22.9">Pégase</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Prenant</w> <w n="23.2">un</w> <w n="23.3">ton</w> <w n="23.4">railleur</w>, <w n="23.5">me</w> <w n="23.6">lança</w> <w n="23.7">cette</w> <w n="23.8">phrase</w> :</l>
						<l n="24" num="4.6">« <w n="24.1">Halte</w>-<w n="24.2">là</w> ! <w n="24.3">car</w> <w n="24.4">tu</w> <w n="24.5">n</w>’<w n="24.6">es</w> <w n="24.7">qu</w>’<w n="24.8">un</w> <w n="24.9">intrus</w> <w n="24.10">en</w> <w n="24.11">ces</w> <w n="24.12">lieux</w>… »</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Alors</w> <w n="25.2">je</w> <w n="25.3">m</w>’<w n="25.4">écriai</w>, <w n="25.5">dans</w> <w n="25.6">ma</w> <w n="25.7">douleur</w> <w n="25.8">amère</w>.</l>
						<l n="26" num="5.2"><hi rend="ital"><w n="26.1">Où</w> <w n="26.2">donc</w> <w n="26.3">est</w> <w n="26.4">le</w> <w n="26.5">bonheur</w> ?</hi> <w n="26.6">Serait</w>-<w n="26.7">ce</w> <w n="26.8">une</w> <w n="26.9">chimère</w></l>
						<l n="27" num="5.3"><w n="27.1">Qui</w> <w n="27.2">redonne</w> <w n="27.3">l</w>’<w n="27.4">espoir</w> <w n="27.5">à</w> <w n="27.6">tout</w> <w n="27.7">être</w> <w n="27.8">souffrant</w> ?</l>
						<l n="28" num="5.4"><w n="28.1">Hélas</w> ! <w n="28.2">je</w> <w n="28.3">le</w> <w n="28.4">croyais</w>… <w n="28.5">Mais</w> <w n="28.6">dès</w> <w n="28.7">le</w> <w n="28.8">jour</w>, <w n="28.9">ô</w> <w n="28.10">femme</w>,</l>
						<l n="29" num="5.5"><w n="29.1">Où</w> <w n="29.2">les</w> <w n="29.3">sons</w> <w n="29.4">de</w> <w n="29.5">ta</w> <w n="29.6">voix</w> <w n="29.7">firent</w> <w n="29.8">vibrer</w> <w n="29.9">mon</w> <w n="29.10">âme</w>,</l>
						<l n="30" num="5.6"><w n="30.1">Je</w> <w n="30.2">goûtai</w> <w n="30.3">du</w> <w n="30.4">bonheur</w> <w n="30.5">le</w> <w n="30.6">délice</w> <w n="30.7">enivrant</w> !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Et</w> <w n="31.2">depuis</w> <w n="31.3">qu</w>’<w n="31.4">à</w> <w n="31.5">nos</w> <w n="31.6">yeux</w> ‒ <w n="31.7">aurore</w> <w n="31.8">fortunée</w> ‒</l>
						<l n="32" num="6.2"><w n="32.1">S</w>’<w n="32.2">alluma</w> <w n="32.3">le</w> <w n="32.4">divin</w> <w n="32.5">flambeau</w> <w n="32.6">de</w> <w n="32.7">l</w>’<w n="32.8">hyménée</w>,</l>
						<l n="33" num="6.3"><w n="33.1">Le</w> <w n="33.2">bonheur</w>, <w n="33.3">tu</w> <w n="33.4">le</w> <w n="33.5">sais</w>, <w n="33.6">nous</w> <w n="33.7">souria</w> <w n="33.8">toujours</w>.</l>
						<l n="34" num="6.4"><w n="34.1">Il</w> <w n="34.2">nous</w> <w n="34.3">sourira</w> <w n="34.4">même</w> <w n="34.5">au</w> <w n="34.6">sein</w> <w n="34.7">de</w> <w n="34.8">la</w> <w n="34.9">souffrance</w>,</l>
						<l n="35" num="6.5"><w n="35.1">Parce</w> <w n="35.2">que</w> <w n="35.3">nous</w> <w n="35.4">plaçons</w> <w n="35.5">toute</w> <w n="35.6">notre</w> <w n="35.7">espérance</w></l>
						<l n="36" num="6.6"><w n="36.1">Dans</w> <w n="36.2">le</w> <w n="36.3">Dieu</w> <w n="36.4">qui</w> <w n="36.5">bénit</w> <w n="36.6">et</w> <w n="36.7">féconde</w> <w n="36.8">les</w> <w n="36.9">jours</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1886">Septembre 1886</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>