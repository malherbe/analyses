<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS</head><div type="poem" key="CAO48">
					<head type="main">RÉPONSE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">autre</w> <w n="1.3">jour</w>, <w n="1.4">en</w> <w n="1.5">passant</w>, <w n="1.6">je</w> <w n="1.7">vis</w> <w n="1.8">dans</w> <w n="1.9">le</w> <w n="1.10">vallon</w></l>
						<l n="2" num="1.2"><w n="2.1">Une</w> <w n="2.2">harpe</w> <w n="2.3">au</w> <w n="2.4">rameau</w> <w n="2.5">d</w>’<w n="2.6">un</w> <w n="2.7">arbre</w> <w n="2.8">suspendue</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">soleil</w> <w n="3.3">lui</w> <w n="3.4">versait</w> <w n="3.5">comme</w> <w n="3.6">des</w> <w n="3.7">jets</w> <w n="3.8">de</w> <w n="3.9">plomb</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">nul</w> <w n="4.3">vent</w> <w n="4.4">ne</w> <w n="4.5">touchait</w> <w n="4.6">sa</w> <w n="4.7">corde</w> <w n="4.8">détendue</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">silence</w> <w n="5.3">de</w> <w n="5.4">mort</w> <w n="5.5">pesait</w> <w n="5.6">sur</w> <w n="5.7">l</w>’<w n="5.8">étendue</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Mais</w> <w n="6.2">soudain</w> <w n="6.3">un</w> <w n="6.4">zéphyr</w>, <w n="6.5">caché</w> <w n="6.6">dans</w> <w n="6.7">un</w> <w n="6.8">buisson</w>,</l>
						<l n="7" num="2.3"><w n="7.1">S</w>’<w n="7.2">en</w> <w n="7.3">vint</w> <w n="7.4">tourbillonner</w> <w n="7.5">sur</w> <w n="7.6">la</w> <w n="7.7">harpe</w> <w n="7.8">éperdue</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">l</w>’<w n="8.3">instrument</w> <w n="8.4">divin</w> <w n="8.5">rendit</w> <w n="8.6">encore</w> <w n="8.7">un</w> <w n="8.8">son</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ami</w>, <w n="9.2">mon</w> <w n="9.3">luth</w> <w n="9.4">gisait</w>, <w n="9.5">frappé</w> <w n="9.6">par</w> <w n="9.7">la</w> <w n="9.8">souffrance</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Dans</w> <w n="10.2">son</w> <w n="10.3">désert</w> <w n="10.4">brûlant</w> <w n="10.5">nul</w> <w n="10.6">souffle</w> <w n="10.7">d</w>’<w n="10.8">espérance</w></l>
						<l n="11" num="3.3"><w n="11.1">Ne</w> <w n="11.2">caressait</w> <w n="11.3">mon</w> <w n="11.4">cœur</w> <w n="11.5">navré</w> <w n="11.6">par</w> <w n="11.7">les</w> <w n="11.8">chagrins</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Mais</w> <w n="12.2">hier</w> <w n="12.3">votre</w> <w n="12.4">muse</w>, <w n="12.5">harmonieuse</w> <w n="12.6">brise</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Effleura</w> <w n="13.2">de</w> <w n="13.3">son</w> <w n="13.4">vol</w> <w n="13.5">ma</w> <w n="13.6">lyre</w> <w n="13.7">qui</w> <w n="13.8">se</w> <w n="13.9">brise</w>.</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">je</w> <w n="14.3">fredonne</w> <w n="14.4">encor</w> <w n="14.5">mes</w> <w n="14.6">modestes</w> <w n="14.7">refrains</w> !</l>
					</lg>
					<closer>
						<signed>C…</signed>
						<dateline>
							<date when="1885">15 septembre 1885</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>