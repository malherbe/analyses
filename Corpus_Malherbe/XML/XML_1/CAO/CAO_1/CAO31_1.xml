<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CAO31">
					<head type="main">L’AUTOMNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">ciel</w> <w n="1.3">n</w>’<w n="1.4">a</w> <w n="1.5">plus</w> <w n="1.6">d</w>’<w n="1.7">azur</w> ; <w n="1.8">l</w>’<w n="1.9">atmosphère</w> <w n="1.10">est</w> <w n="1.11">de</w> <w n="1.12">glace</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">splendeur</w> <w n="2.3">du</w> <w n="2.4">soleil</w> <w n="2.5">pâlit</w> <w n="2.6">de</w> <w n="2.7">jour</w> <w n="2.8">en</w> <w n="2.9">jour</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Sur</w> <w n="3.2">l</w>’<w n="3.3">arbre</w> <w n="3.4">dépouillé</w> <w n="3.5">que</w> <w n="3.6">le</w> <w n="3.7">frimas</w> <w n="3.8">enlace</w>,</l>
						<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">oiseau</w> <w n="4.3">ne</w> <w n="4.4">redit</w> <w n="4.5">plus</w> <w n="4.6">sa</w> <w n="4.7">romance</w> <w n="4.8">d</w>’<w n="4.9">amour</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">nature</w> <w n="5.3">a</w> <w n="5.4">souillé</w> <w n="5.5">la</w> <w n="5.6">robe</w> <w n="5.7">éblouissante</w></l>
						<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">parait</w> <w n="6.3">les</w> <w n="6.4">coteaux</w> <w n="6.5">de</w> <w n="6.6">ses</w> <w n="6.7">replis</w> <w n="6.8">soyeux</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Les</w> <w n="7.2">fleurs</w> <w n="7.3">ont</w> <w n="7.4">disparu</w> ; <w n="7.5">l</w>’<w n="7.6">abeille</w> <w n="7.7">vigilante</w></l>
						<l n="8" num="2.4"><w n="8.1">Ne</w> <w n="8.2">dore</w> <w n="8.3">plus</w> <w n="8.4">nos</w> <w n="8.5">bois</w> <w n="8.6">de</w> <w n="8.7">son</w> <w n="8.8">miel</w> <w n="8.9">savoureux</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">torrents</w> <w n="9.3">écumeux</w>, <w n="9.4">grandis</w> <w n="9.5">par</w> <w n="9.6">les</w> <w n="9.7">orages</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Font</w> <w n="10.2">retentir</w> <w n="10.3">les</w> <w n="10.4">airs</w> <w n="10.5">de</w> <w n="10.6">lugubres</w> <w n="10.7">sanglots</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">bondissant</w> <w n="11.3">soudain</w> <w n="11.4">par</w> <w n="11.5">dessus</w> <w n="11.6">les</w> <w n="11.7">rivages</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Dévastent</w> <w n="12.2">les</w> <w n="12.3">moissons</w> <w n="12.4">de</w> <w n="12.5">leurs</w> <w n="12.6">terribles</w> <w n="12.7">flots</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Quand</w> <w n="13.2">tu</w> <w n="13.3">parais</w>, <w n="13.4">automne</w>, <w n="13.5">aussitôt</w> <w n="13.6">la</w> <w n="13.7">tristesse</w></l>
						<l n="14" num="4.2"><w n="14.1">Sur</w> <w n="14.2">notre</w> <w n="14.3">front</w> <w n="14.4">serein</w> <w n="14.5">pose</w> <w n="14.6">son</w> <w n="14.7">noir</w> <w n="14.8">bandeau</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Tu</w> <w n="15.2">viens</w> <w n="15.3">ravir</w> <w n="15.4">aux</w> <w n="15.5">champs</w> <w n="15.6">leur</w> <w n="15.7">brillante</w> <w n="15.8">jeunesse</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Tu</w> <w n="16.2">nous</w> <w n="16.3">donnes</w> <w n="16.4">des</w> <w n="16.5">jours</w> <w n="16.6">sombres</w> <w n="16.7">comme</w> <w n="16.8">un</w> <w n="16.9">tombeau</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Au</w> <w n="17.2">vieillard</w> <w n="17.3">que</w> <w n="17.4">les</w> <w n="17.5">ans</w> <w n="17.6">inclinent</w> <w n="17.7">vers</w> <w n="17.8">la</w> <w n="17.9">tombe</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">qui</w> <w n="18.3">plonge</w> <w n="18.4">son</w> <w n="18.5">cœur</w> <w n="18.6">aux</w> <w n="18.7">sources</w> <w n="18.8">des</w> <w n="18.9">plaisirs</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Tu</w> <w n="19.2">dis</w> : « <w n="19.3">Lève</w> <w n="19.4">la</w> <w n="19.5">tête</w>, <w n="19.6">et</w> <w n="19.7">vois</w> <w n="19.8">ce</w> <w n="19.9">fruit</w> <w n="19.10">qui</w> <w n="19.11">tombe</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Ainsi</w> <w n="20.2">tu</w> <w n="20.3">tomberas</w> <w n="20.4">avec</w> <w n="20.5">tes</w> <w n="20.6">vains</w> <w n="20.7">désirs</w>… »</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">L</w>’<w n="21.2">automne</w>, <w n="21.3">de</w> <w n="21.4">la</w> <w n="21.5">vie</w> <w n="21.6">est</w> <w n="21.7">la</w> <w n="21.8">fidèle</w> <w n="21.9">image</w> :</l>
						<l n="22" num="6.2"><w n="22.1">Les</w> <w n="22.2">jours</w> <w n="22.3">calmes</w> <w n="22.4">et</w> <w n="22.5">doux</w> <w n="22.6">sont</w> <w n="22.7">nos</w> <w n="22.8">jours</w> <w n="22.9">sans</w> <w n="22.10">remords</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Les</w> <w n="23.2">bosquets</w> <w n="23.3">dénudés</w> <w n="23.4">rappellent</w> <w n="23.5">le</w> <w n="23.6">vieil</w> <w n="23.7">âge</w> ;</l>
						<l n="24" num="6.4"><w n="24.1">La</w> <w n="24.2">neige</w> <w n="24.3">et</w> <w n="24.4">les</w> <w n="24.5">frimas</w>, <w n="24.6">le</w> <w n="24.7">blanc</w> <w n="24.8">linceul</w> <w n="24.9">des</w> <w n="24.10">morts</w> !…</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Eh</w> <w n="25.2">bien</w> ! <w n="25.3">puisque</w> <w n="25.4">l</w>’<w n="25.5">automne</w> <w n="25.6">en</w> <w n="25.7">souverain</w> <w n="25.8">commande</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Inclinons</w> <w n="26.2">tous</w> <w n="26.3">nos</w> <w n="26.4">fronts</w> <w n="26.5">devant</w> <w n="26.6">sa</w> <w n="26.7">majesté</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">Car</w> <w n="27.2">sa</w> <w n="27.3">voix</w> <w n="27.4">est</w> <w n="27.5">l</w>’<w n="27.6">écho</w> <w n="27.7">de</w> <w n="27.8">Dieu</w> <w n="27.9">qui</w> <w n="27.10">réprimande</w></l>
						<l n="28" num="7.4"><w n="28.1">Ceux</w> <w n="28.2">qui</w> <w n="28.3">ne</w> <w n="28.4">pensent</w> <w n="28.5">pas</w> <w n="28.6">à</w> <w n="28.7">leur</w> <w n="28.8">éternité</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1883">Novembre 1883</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>