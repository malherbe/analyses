<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES VOIX INTIMES</title>
				<title type="medium">Édition électronique</title>
				<author key="CAO">
					<name>
						<forename>Jean Baptiste</forename>
						<surname>CAOUETTE</surname>
					</name>
					<date from="1854" to="1922">1854-1922</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3645 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CAO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>LES VOIX INTIMES</title>
						<author>Jean Baptiste Caouette</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/19689</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>PREMIÈRES POÉSIES</title>
								<title>LES VOIX INTIMES</title>
								<author>Jean Baptiste Caouette</author>
								<idno type="URI">https://archive.org/details/lesvoixintimes19689gut</idno>
								<imprint>
									<pubPlace></pubPlace>
									<publisher></publisher>
									<date when="1892">1892</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-07-31" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-07-31" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">HYMNES, ROMANCES ET CHANSONNETTES</head><div type="poem" key="CAO55">
					<head type="main">LA CANADIENNE</head>
					<head type="tune">Sur l’air de : « La Huronne »</head>
					<div type="section" n="1">
						<head type="main">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Ravissante</w> <w n="1.2">est</w> <w n="1.3">la</w> <w n="1.4">Canadienne</w></l>
							<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">ses</w> <w n="2.3">yeux</w> <w n="2.4">pleins</w> <w n="2.5">de</w> <w n="2.6">douceur</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Son</w> <w n="3.2">teint</w> <w n="3.3">rosé</w>, <w n="3.4">son</w> <w n="3.5">port</w> <w n="3.6">de</w> <w n="3.7">reine</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">admire</w> <w n="4.3">le</w> <w n="4.4">fin</w> <w n="4.5">connaisseur</w>.</l>
							<l n="5" num="1.5"><w n="5.1">En</w> <w n="5.2">robe</w> <w n="5.3">de</w> <w n="5.4">soie</w> <w n="5.5">ou</w> <w n="5.6">d</w>’<w n="5.7">indienne</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Elle</w> <w n="6.2">plaît</w> <w n="6.3">toujours</w> <w n="6.4">au</w> <w n="6.5">galant</w> !</l>
							<l n="7" num="1.7"><w n="7.1">Chantons</w> <w n="7.2">l</w>’<w n="7.3">aimable</w> <w n="7.4">Canadienne</w>, <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="8" num="1.8"><w n="8.1">Amis</w>, <w n="8.2">dans</w> <w n="8.3">un</w> <w n="8.4">joyeux</w> <w n="8.5">élan</w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1">
							<l n="9" num="1.1"><w n="9.1">Jadis</w>, <w n="9.2">sur</w> <w n="9.3">le</w> <w n="9.4">champ</w> <w n="9.5">de</w> <w n="9.6">bataille</w>,</l>
							<l n="10" num="1.2"><w n="10.1">Elle</w> <w n="10.2">cueillit</w> <w n="10.3">plus</w> <w n="10.4">d</w>’<w n="10.5">un</w> <w n="10.6">laurier</w>,</l>
							<l n="11" num="1.3"><w n="11.1">Et</w> <w n="11.2">de</w> <w n="11.3">nos</w> <w n="11.4">jours</w> <w n="11.5">elle</w> <w n="11.6">travaille</w></l>
							<l n="12" num="1.4"><w n="12.1">A</w> <w n="12.2">maintenir</w> <w n="12.3">l</w>’<w n="12.4">ordre</w> <w n="12.5">au</w> <w n="12.6">foyer</w> ;</l>
							<l n="13" num="1.5"><w n="13.1">De</w> <w n="13.2">notre</w> <w n="13.3">foi</w> <w n="13.4">c</w>’<w n="13.5">est</w> <w n="13.6">la</w> <w n="13.7">gardienne</w>,</l>
							<l n="14" num="1.6"><w n="14.1">Le</w> <w n="14.2">champion</w> <w n="14.3">ferme</w> <w n="14.4">et</w> <w n="14.5">vaillant</w>.</l>
							<l n="15" num="1.7"><w n="15.1">Chantons</w> <w n="15.2">l</w>’<w n="15.3">aimable</w> <w n="15.4">Canadienne</w>, <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="16" num="1.8"><w n="16.1">Amis</w>, <w n="16.2">dans</w> <w n="16.3">un</w> <w n="16.4">joyeux</w> <w n="16.5">élan</w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="main">III</head>
						<lg n="1">
							<l n="17" num="1.1"><w n="17.1">Regardez</w>-<w n="17.2">là</w> <w n="17.3">dans</w> <w n="17.4">une</w> <w n="17.5">fête</w></l>
							<l n="18" num="1.2"><w n="18.1">Rire</w> <w n="18.2">et</w> <w n="18.3">parler</w> <w n="18.4">avec</w> <w n="18.5">chaleur</w>,</l>
							<l n="19" num="1.3"><w n="19.1">Puis</w> <w n="19.2">souvent</w> <w n="19.3">faire</w> <w n="19.4">la</w> <w n="19.5">conquête</w></l>
							<l n="20" num="1.4"><w n="20.1">De</w> <w n="20.2">celui</w> <w n="20.3">qu</w>’<w n="20.4">elle</w> <w n="20.5">a</w> <w n="20.6">pour</w> <w n="20.7">causeur</w> !</l>
							<l n="21" num="1.5"><w n="21.1">On</w> <w n="21.2">la</w> <w n="21.3">proclame</w> <hi rend="ital"><w n="21.4">magicienne</w></hi>,</l>
							<l n="22" num="1.6"><w n="22.1">Certes</w>, <w n="22.2">c</w>’<w n="22.3">est</w> <w n="22.4">bien</w> <w n="22.5">l</w>’<w n="22.6">équivalent</w>…</l>
							<l n="23" num="1.7"><w n="23.1">Chantons</w> <w n="23.2">l</w>’<w n="23.3">aimable</w> <w n="23.4">Canadienne</w>, <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="24" num="1.8"><w n="24.1">Amis</w>, <w n="24.2">dans</w> <w n="24.3">un</w> <w n="24.4">joyeux</w> <w n="24.5">élan</w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="main">IV</head>
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">Charitable</w> <w n="25.2">autant</w> <w n="25.3">que</w> <w n="25.4">gentille</w>,</l>
							<l n="26" num="1.2"><w n="26.1">Elle</w> <w n="26.2">visite</w> <w n="26.3">le</w> <w n="26.4">réduit</w></l>
							<l n="27" num="1.3"><w n="27.1">Où</w> <w n="27.2">le</w> <w n="27.3">feu</w> <w n="27.4">rarement</w> <w n="27.5">pétille</w>,</l>
							<l n="28" num="1.4"><w n="28.1">Où</w> <w n="28.2">le</w> <w n="28.3">bonheur</w> <w n="28.4">jamais</w> <w n="28.5">ne</w> <w n="28.6">luit</w> !</l>
							<l n="29" num="1.5"><w n="29.1">Et</w> <w n="29.2">l</w>’<w n="29.3">or</w> <w n="29.4">de</w> <w n="29.5">cette</w> <w n="29.6">humble</w> <w n="29.7">chrétienne</w></l>
							<l n="30" num="1.6"><w n="30.1">Sèche</w> <w n="30.2">les</w> <w n="30.3">pleurs</w> <w n="30.4">de</w> <w n="30.5">l</w>’<w n="30.6">artisan</w>…</l>
							<l n="31" num="1.7"><w n="31.1">Ah</w> ! <w n="31.2">oui</w>, <w n="31.3">Chantons</w> <w n="31.4">la</w> <w n="31.5">Canadienne</w>, <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
							<l n="32" num="1.8"><w n="32.1">Amis</w>, <w n="32.2">dans</w> <w n="32.3">un</w> <w n="32.4">joyeux</w> <w n="32.5">élan</w> ! <del reason="analysis" hand="RR" type="repetition">(bis)</del></l>
						</lg>
					</div>
					<closer>
						<dateline>
							<date when="1881">Janvier 1881</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>