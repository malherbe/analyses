<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="sub_2">Compléments</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>80 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k5748094b</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<author>Jean-Pierre Claris de Florian</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DES BIBLIOPHILES</publisher>
									<date when="1894">1894</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1802">1802</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition ne contient que les fables non incluses dans le premier corpus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="FLO113">
				<head type="number">XII</head>
				<head type="main">L’AIGLE ET LA FOURMI</head>
				<head type="sub_1">PAR M. DE FLORIAN</head>
				<head type="sub_1">En envoyant ses Fables à M. Hérivaux.</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Du</w> <w n="1.2">dieu</w> <w n="1.3">qui</w> <w n="1.4">lance</w> <w n="1.5">le</w> <w n="1.6">tonnerre</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Un</w> <w n="2.2">beau</w> <w n="2.3">jour</w> <w n="2.4">l</w>’<w n="2.5">oiseau</w> <w n="2.6">favori</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Dirigeant</w> <w n="3.2">son</w> <w n="3.3">vol</w> <w n="3.4">sur</w> <w n="3.5">la</w> <w n="3.6">terre</w>,</l>
					<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">abattit</w> <w n="4.3">justement</w> <w n="4.4">tout</w> <w n="4.5">près</w> <w n="4.6">d</w>’<w n="4.7">une</w> <w n="4.8">fourmi</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="12"></space><w n="5.1">Qui</w> <w n="5.2">parmi</w> <w n="5.3">la</w> <w n="5.4">fougère</w>,</l>
					<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Non</w> <w n="6.2">loin</w> <w n="6.3">de</w> <w n="6.4">ses</w> <w n="6.5">foyers</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="12"></space><w n="7.1">En</w> <w n="7.2">bonne</w> <w n="7.3">ménagère</w></l>
					<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">Alloit</w>, <w n="8.2">venoit</w>, <w n="8.3">pour</w> <w n="8.4">remplir</w> <w n="8.5">ses</w> <w n="8.6">greniers</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Jugez</w> <w n="9.2">de</w> <w n="9.3">sa</w> <w n="9.4">surprise</w> <w n="9.5">en</w> <w n="9.6">voyant</w> <w n="9.7">si</w> <w n="9.8">près</w> <w n="9.9">d</w>’<w n="9.10">elle</w></l>
					<l n="10" num="1.10"><w n="10.1">Le</w> <w n="10.2">superbe</w> <w n="10.3">habitant</w> <w n="10.4">du</w> <w n="10.5">céleste</w> <w n="10.6">séjour</w>.</l>
					<l n="11" num="1.11"><w n="11.1">L</w>’<w n="11.2">aigle</w> <w n="11.3">ne</w> <w n="11.4">la</w> <w n="11.5">vit</w> <w n="11.6">point</w> ; <w n="11.7">sa</w> <w n="11.8">brillante</w> <w n="11.9">prunelle</w></l>
					<l n="12" num="1.12"><w n="12.1">Ne</w> <w n="12.2">sut</w> <w n="12.3">jamais</w> <w n="12.4">fixer</w> <w n="12.5">que</w> <w n="12.6">le</w> <w n="12.7">flambeau</w> <w n="12.8">du</w> <w n="12.9">jour</w>.</l>
					<l n="13" num="1.13"><w n="13.1">L</w>’<w n="13.2">insecte</w> <w n="13.3">veut</w> <w n="13.4">d</w>’<w n="13.5">abord</w> <w n="13.6">regagner</w> <w n="13.7">sa</w> <w n="13.8">cellule</w> ;</l>
					<l n="14" num="1.14"><w n="14.1">Il</w> <w n="14.2">s</w>’<w n="14.3">arrête</w>, <w n="14.4">il</w> <w n="14.5">hésite</w>, <w n="14.6">il</w> <w n="14.7">avance</w>, <w n="14.8">il</w> <w n="14.9">recule</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Un</w> <w n="15.2">désir</w> <w n="15.3">curieux</w> <w n="15.4">s</w>’<w n="15.5">oppose</w> <w n="15.6">à</w> <w n="15.7">son</w> <w n="15.8">retour</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">bientôt</w>, <w n="16.3">bannissant</w> <w n="16.4">un</w> <w n="16.5">frivole</w> <w n="16.6">scrupule</w>,</l>
					<l n="17" num="1.17"><space unit="char" quantity="4"></space><w n="17.1">Au</w> <w n="17.2">roi</w> <w n="17.3">des</w> <w n="17.4">airs</w> <w n="17.5">il</w> <w n="17.6">veut</w> <w n="17.7">faire</w> <w n="17.8">sa</w> <w n="17.9">cour</w>.</l>
					<l n="18" num="1.18"><w n="18.1">Méditant</w> <w n="18.2">sa</w> <w n="18.3">harangue</w> <w n="18.4">et</w> <w n="18.5">composant</w> <w n="18.6">sa</w> <w n="18.7">mine</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Vers</w> <w n="19.2">l</w>’<w n="19.3">aigle</w> <w n="19.4">sur</w>-<w n="19.5">le</w>-<w n="19.6">champ</w> <w n="19.7">la</w> <w n="19.8">fourmi</w> <w n="19.9">s</w>’<w n="19.10">achemine</w>.</l>
					<l n="20" num="1.20">« <w n="20.1">O</w> <w n="20.2">vous</w>, <w n="20.3">dit</w>-<w n="20.4">elle</w>, <w n="20.5">ô</w> <w n="20.6">vous</w> <w n="20.7">qu</w>’<w n="20.8">en</w> <w n="20.9">ces</w> <w n="20.10">champêtres</w> <w n="20.11">lieux</w></l>
					<l n="21" num="1.21"><w n="21.1">Pour</w> <w n="21.2">la</w> <w n="21.3">première</w> <w n="21.4">fois</w> <w n="21.5">aperçoivent</w> <w n="21.6">mes</w> <w n="21.7">yeux</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Excusez</w>-<w n="22.2">moi</w>, <w n="22.3">Seigneur</w>, <w n="22.4">si</w> <w n="22.5">je</w> <w n="22.6">vous</w> <w n="22.7">importune</w>,</l>
					<l n="23" num="1.23"><space unit="char" quantity="12"></space><w n="23.1">Et</w> <w n="23.2">souffrez</w> <w n="23.3">qu</w>’<w n="23.4">un</w> <w n="23.5">moment</w></l>
					<l n="24" num="1.24"><w n="24.1">Je</w> <w n="24.2">goûte</w> <w n="24.3">auprès</w> <w n="24.4">de</w> <w n="24.5">vous</w> <w n="24.6">le</w> <w n="24.7">doux</w> <w n="24.8">contentement</w></l>
					<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Que</w> <w n="25.2">m</w>’<w n="25.3">offre</w> <w n="25.4">ma</w> <w n="25.5">bonne</w> <w n="25.6">fortune</w>.</l>
					<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Par</w> <w n="26.2">quelques</w> <w n="26.3">mets</w> <w n="26.4">dignes</w> <w n="26.5">de</w> <w n="26.6">vous</w></l>
					<l n="27" num="1.27"><w n="27.1">Je</w> <w n="27.2">voudrais</w> <w n="27.3">vous</w> <w n="27.4">prouver</w> <w n="27.5">mon</w> <w n="27.6">respect</w> <w n="27.7">et</w> <w n="27.8">mon</w> <w n="27.9">zèle</w> ;</l>
					<l n="28" num="1.28"><w n="28.1">Mais</w> <w n="28.2">une</w> <w n="28.3">humble</w> <w n="28.4">fourmi</w> <w n="28.5">n</w>’<w n="28.6">a</w> <w n="28.7">que</w> <w n="28.8">ses</w> <w n="28.9">vœux</w> <w n="28.10">pour</w> <w n="28.11">elle</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Et</w> <w n="29.2">le</w> <w n="29.3">riche</w> <w n="29.4">Plutus</w>, <w n="29.5">de</w> <w n="29.6">ses</w> <w n="29.7">trésors</w> <w n="29.8">jaloux</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Ne</w> <w n="30.2">m</w>’<w n="30.3">en</w> <w n="30.4">donna</w> <w n="30.5">jamais</w> <w n="30.6">la</w> <w n="30.7">plus</w> <w n="30.8">simple</w> <w n="30.9">parcelle</w>.</l>
					<l n="31" num="1.31"><space unit="char" quantity="8"></space><w n="31.1">Pour</w> <w n="31.2">moi</w> <w n="31.3">daignez</w> <w n="31.4">être</w> <w n="31.5">indulgent</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Et</w> <w n="32.2">des</w> <w n="32.3">grains</w> <w n="32.4">qu</w>’<w n="32.5">amassa</w> <w n="32.6">ma</w> <w n="32.7">pénible</w> <w n="32.8">industrie</w></l>
					<l n="33" num="1.33"><space unit="char" quantity="12"></space><w n="33.1">Que</w> <w n="33.2">Votre</w> <w n="33.3">Seigneurie</w></l>
					<l n="34" num="1.34"><space unit="char" quantity="12"></space><w n="34.1">Accepte</w> <w n="34.2">le</w> <w n="34.3">présent</w>.</l>
					<l n="35" num="1.35"><space unit="char" quantity="12"></space><w n="35.1">Si</w> <w n="35.2">de</w> <w n="35.3">ma</w> <w n="35.4">foible</w> <w n="35.5">offrande</w></l>
					<l n="36" num="1.36"><space unit="char" quantity="12"></space><w n="36.1">Vous</w> <w n="36.2">faites</w> <w n="36.3">quelque</w> <w n="36.4">cas</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Du</w> <w n="37.2">sort</w> <w n="37.3">j</w>’<w n="37.4">aurai</w> <w n="37.5">reçu</w> <w n="37.6">la</w> <w n="37.7">faveur</w> <w n="37.8">la</w> <w n="37.9">plus</w> <w n="37.10">grande</w> ;</l>
					<l n="38" num="1.38"><w n="38.1">De</w> <w n="38.2">ses</w> <w n="38.3">longues</w> <w n="38.4">ligueurs</w> <w n="38.5">je</w> <w n="38.6">ne</w> <w n="38.7">me</w> <w n="38.8">plaindrai</w> <w n="38.9">pas</w>. »</l>
					<l n="39" num="1.39"><space unit="char" quantity="4"></space><w n="39.1">L</w>’<w n="39.2">aigle</w> <w n="39.3">sourit</w> <w n="39.4">à</w> <w n="39.5">notre</w> <w n="39.6">discoureuse</w>,</l>
					<l n="40" num="1.40"><space unit="char" quantity="4"></space><w n="40.1">Et</w>, <w n="40.2">déployant</w> <w n="40.3">son</w> <w n="40.4">aile</w> <w n="40.5">vigoureuse</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Il</w> <w n="41.2">l</w>’<w n="41.3">aide</w> <w n="41.4">à</w> <w n="41.5">s</w>’<w n="41.6">y</w> <w n="41.7">placer</w> ; <w n="41.8">puis</w>, <w n="41.9">dans</w> <w n="41.10">l</w>’<w n="41.11">air</w> <w n="41.12">s</w>’<w n="41.13">élançant</w>,</l>
					<l n="42" num="1.42"><space unit="char" quantity="16"></space><w n="42.1">En</w> <w n="42.2">un</w> <w n="42.3">instant</w></l>
					<l n="43" num="1.43"><w n="43.1">Il</w> <w n="43.2">l</w>’<w n="43.3">emporte</w> <w n="43.4">au</w>-<w n="43.5">dessus</w> <w n="43.6">de</w> <w n="43.7">la</w> <w n="43.8">voûte</w> <w n="43.9">azurée</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Interdite</w>, <w n="44.2">confuse</w>, <w n="44.3">à</w> <w n="44.4">peine</w> <w n="44.5">rassurée</w>.</l>
					<l n="45" num="1.45"><space unit="char" quantity="8"></space><w n="45.1">Là</w>, <w n="45.2">dans</w> <w n="45.3">un</w> <w n="45.4">palais</w> <w n="45.5">enchanté</w></l>
					<l n="46" num="1.46"><w n="46.1">Où</w> <w n="46.2">de</w> <w n="46.3">tableaux</w> <w n="46.4">charmants</w> <w n="46.5">une</w> <w n="46.6">suite</w> <w n="46.7">choisie</w></l>
					<l n="47" num="1.47"><w n="47.1">Flatte</w> <w n="47.2">l</w>’<w n="47.3">esprit</w>, <w n="47.4">le</w> <w n="47.5">cœur</w>, <w n="47.6">par</w> <w n="47.7">sa</w> <w n="47.8">variété</w>,</l>
					<l n="48" num="1.48"><w n="48.1">Il</w> <w n="48.2">l</w>’<w n="48.3">accueille</w>, <w n="48.4">et</w>, <w n="48.5">d</w>’<w n="48.6">un</w> <w n="48.7">air</w> <w n="48.8">rempli</w> <w n="48.9">de</w> <w n="48.10">courtoisie</w>,</l>
					<l n="49" num="1.49"><w n="49.1">Pour</w> <w n="49.2">un</w> <w n="49.3">peu</w> <w n="49.4">de</w> <w n="49.5">millet</w> <w n="49.6">par</w> <w n="49.7">elle</w> <w n="49.8">présenté</w>,</l>
					<l n="50" num="1.50"><space unit="char" quantity="8"></space><w n="50.1">Il</w> <w n="50.2">lui</w> <w n="50.3">prodigue</w> <w n="50.4">avec</w> <w n="50.5">bonté</w></l>
					<l n="51" num="1.51"><space unit="char" quantity="8"></space><w n="51.1">Et</w> <w n="51.2">le</w> <w n="51.3">nectar</w> <w n="51.4">et</w> <w n="51.5">l</w>’<w n="51.6">ambroisie</w>.</l>
				</lg>
			</div></body></text></TEI>