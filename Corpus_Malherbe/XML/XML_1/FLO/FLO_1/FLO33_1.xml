<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SECOND</head><div type="poem" key="FLO33">
					<head type="number">FABLE XI</head>
					<head type="main">Le Grillon</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">Un</w> <w n="1.2">pauvre</w> <w n="1.3">petit</w> <w n="1.4">grillon</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1">Caché</w> <w n="2.2">dans</w> <w n="2.3">l</w>’<w n="2.4">herbe</w> <w n="2.5">fleurie</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">Regardait</w> <w n="3.2">un</w> <w n="3.3">papillon</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">Voltigeant</w> <w n="4.2">dans</w> <w n="4.3">la</w> <w n="4.4">prairie</w>.</l>
						<l n="5" num="1.5"><w n="5.1">L</w>’<w n="5.2">insecte</w> <w n="5.3">ailé</w> <w n="5.4">brillait</w> <w n="5.5">des</w> <w n="5.6">plus</w> <w n="5.7">vives</w> <w n="5.8">couleurs</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">L</w>’<w n="6.2">azur</w>, <w n="6.3">le</w> <w n="6.4">pourpre</w> <w n="6.5">et</w> <w n="6.6">l</w>’<w n="6.7">or</w> <w n="6.8">éclataient</w> <w n="6.9">sur</w> <w n="6.10">ses</w> <w n="6.11">ailes</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Jeune</w>, <w n="7.2">beau</w>, <w n="7.3">petit</w>-<w n="7.4">maître</w>, <w n="7.5">il</w> <w n="7.6">court</w> <w n="7.7">de</w> <w n="7.8">fleurs</w> <w n="7.9">en</w> <w n="7.10">fleurs</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Prenant</w> <w n="8.2">et</w> <w n="8.3">quittant</w> <w n="8.4">les</w> <w n="8.5">plus</w> <w n="8.6">belles</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Ah</w> ! <w n="9.2">disait</w> <w n="9.3">le</w> <w n="9.4">grillon</w>, <w n="9.5">que</w> <w n="9.6">son</w> <w n="9.7">sort</w> <w n="9.8">et</w> <w n="9.9">le</w> <w n="9.10">mien</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Sont</w> <w n="10.2">différens</w> ! <w n="10.3">Dame</w> <w n="10.4">nature</w></l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Pour</w> <w n="11.2">lui</w> <w n="11.3">fit</w> <w n="11.4">tout</w> <w n="11.5">et</w> <w n="11.6">pour</w> <w n="11.7">moi</w> <w n="11.8">rien</w>.</l>
						<l n="12" num="1.12"><w n="12.1">Je</w> <w n="12.2">n</w>’<w n="12.3">ai</w> <w n="12.4">point</w> <w n="12.5">de</w> <w n="12.6">talent</w>, <w n="12.7">encor</w> <w n="12.8">moins</w> <w n="12.9">de</w> <w n="12.10">figure</w> ;</l>
						<l n="13" num="1.13"><w n="13.1">Nul</w> <w n="13.2">ne</w> <w n="13.3">prend</w> <w n="13.4">garde</w> <w n="13.5">à</w> <w n="13.6">moi</w>, <w n="13.7">l</w>’<w n="13.8">on</w> <w n="13.9">m</w>’<w n="13.10">ignore</w> <w n="13.11">ici</w> <w n="13.12">bas</w> :</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">Autant</w> <w n="14.2">vaudrait</w> <w n="14.3">n</w>’<w n="14.4">exister</w> <w n="14.5">pas</w>.</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">Comme</w> <w n="15.2">il</w> <w n="15.3">parlait</w>, <w n="15.4">dans</w> <w n="15.5">la</w> <w n="15.6">prairie</w></l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">Arrive</w> <w n="16.2">une</w> <w n="16.3">troupe</w> <w n="16.4">d</w>’<w n="16.5">enfans</w> :</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1">Aussitôt</w> <w n="17.2">les</w> <w n="17.3">voilà</w> <w n="17.4">courans</w></l>
						<l n="18" num="1.18"><w n="18.1">Après</w> <w n="18.2">ce</w> <w n="18.3">papillon</w> <w n="18.4">dont</w> <w n="18.5">ils</w> <w n="18.6">ont</w> <w n="18.7">tous</w> <w n="18.8">envie</w>.</l>
						<l n="19" num="1.19"><w n="19.1">Chapeaux</w>, <w n="19.2">mouchoirs</w>, <w n="19.3">bonnets</w>, <w n="19.4">servent</w> <w n="19.5">à</w> <w n="19.6">l</w>’<w n="19.7">attraper</w>.</l>
						<l n="20" num="1.20"><w n="20.1">L</w>’<w n="20.2">insecte</w> <w n="20.3">vainement</w> <w n="20.4">cherche</w> <w n="20.5">à</w> <w n="20.6">leur</w> <w n="20.7">échapper</w>,</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">Il</w> <w n="21.2">devient</w> <w n="21.3">bientôt</w> <w n="21.4">leur</w> <w n="21.5">conquête</w>.</l>
						<l n="22" num="1.22"><w n="22.1">L</w>’<w n="22.2">un</w> <w n="22.3">le</w> <w n="22.4">saisit</w> <w n="22.5">par</w> <w n="22.6">l</w>’<w n="22.7">aile</w>, <w n="22.8">un</w> <w n="22.9">autre</w> <w n="22.10">par</w> <w n="22.11">le</w> <w n="22.12">corps</w> ;</l>
						<l n="23" num="1.23"><w n="23.1">Un</w> <w n="23.2">troisième</w> <w n="23.3">survient</w>, <w n="23.4">et</w> <w n="23.5">le</w> <w n="23.6">prend</w> <w n="23.7">par</w> <w n="23.8">la</w> <w n="23.9">tête</w>.</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Il</w> <w n="24.2">ne</w> <w n="24.3">fallait</w> <w n="24.4">pas</w> <w n="24.5">tant</w> <w n="24.6">d</w>’<w n="24.7">efforts</w></l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Pour</w> <w n="25.2">déchirer</w> <w n="25.3">la</w> <w n="25.4">pauvre</w> <w n="25.5">bête</w>.</l>
						<l n="26" num="1.26"><w n="26.1">Oh</w> ! <w n="26.2">oh</w> ! <w n="26.3">dit</w> <w n="26.4">le</w> <w n="26.5">grillon</w>, <w n="26.6">je</w> <w n="26.7">ne</w> <w n="26.8">suis</w> <w n="26.9">plus</w> <w n="26.10">fâché</w> ;</l>
						<l n="27" num="1.27"><w n="27.1">Il</w> <w n="27.2">en</w> <w n="27.3">coûte</w> <w n="27.4">trop</w> <w n="27.5">cher</w> <w n="27.6">pour</w> <w n="27.7">briller</w> <w n="27.8">dans</w> <w n="27.9">le</w> <w n="27.10">monde</w>.</l>
						<l n="28" num="1.28"><w n="28.1">Combien</w> <w n="28.2">je</w> <w n="28.3">vais</w> <w n="28.4">aimer</w> <w n="28.5">ma</w> <w n="28.6">retraite</w> <w n="28.7">profonde</w> !</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">Pour</w> <w n="29.2">vivre</w> <w n="29.3">heureux</w> <w n="29.4">vivons</w> <w n="29.5">caché</w>.</l>
					</lg>
				</div></body></text></TEI>