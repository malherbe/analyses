<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO52">
					<head type="number">FABLE VIII</head>
					<head type="main">Les deux Bacheliers</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Deux</w> <w n="1.2">jeunes</w> <w n="1.3">bacheliers</w> <w n="1.4">logés</w> <w n="1.5">chez</w> <w n="1.6">un</w> <w n="1.7">docteur</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Y</w> <w n="2.2">travaillaient</w> <w n="2.3">avec</w> <w n="2.4">ardeur</w></l>
						<l n="3" num="1.3"><w n="3.1">A</w> <w n="3.2">se</w> <w n="3.3">mettre</w> <w n="3.4">en</w> <w n="3.5">état</w> <w n="3.6">de</w> <w n="3.7">prendre</w> <w n="3.8">leurs</w> <w n="3.9">licences</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Là</w>, <w n="4.2">du</w> <w n="4.3">matin</w> <w n="4.4">au</w> <w n="4.5">soir</w>, <w n="4.6">en</w> <w n="4.7">public</w> <w n="4.8">disputant</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Prouvant</w>, <w n="5.2">divisant</w>, <w n="5.3">ergotant</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Sur</w> <w n="6.2">la</w> <w n="6.3">nature</w> <w n="6.4">et</w> <w n="6.5">ses</w> <w n="6.6">substances</w>,</l>
						<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">infini</w>, <w n="7.3">le</w> <w n="7.4">fini</w>, <w n="7.5">l</w>’<w n="7.6">âme</w>, <w n="7.7">la</w> <w n="7.8">volonté</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Les</w> <w n="8.2">sens</w>, <w n="8.3">le</w> <w n="8.4">libre</w> <w n="8.5">arbitre</w> <w n="8.6">et</w> <w n="8.7">la</w> <w n="8.8">nécessité</w> ;</l>
						<l n="9" num="1.9"><w n="9.1">Ils</w> <w n="9.2">en</w> <w n="9.3">étaient</w> <w n="9.4">bientôt</w> <w n="9.5">à</w> <w n="9.6">ne</w> <w n="9.7">plus</w> <w n="9.8">se</w> <w n="9.9">comprendre</w> :</l>
						<l n="10" num="1.10"><w n="10.1">Même</w> <w n="10.2">par</w> <w n="10.3">là</w> <w n="10.4">souvent</w> <w n="10.5">l</w>’<w n="10.6">on</w> <w n="10.7">dit</w> <w n="10.8">qu</w>’<w n="10.9">ils</w> <w n="10.10">commençaient</w> ;</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Mais</w> <w n="11.2">c</w>’<w n="11.3">est</w> <w n="11.4">alors</w> <w n="11.5">qu</w>’<w n="11.6">ils</w> <w n="11.7">se</w> <w n="11.8">poussaient</w></l>
						<l n="12" num="1.12"><w n="12.1">Les</w> <w n="12.2">plus</w> <w n="12.3">beaux</w> <w n="12.4">argumens</w> ; <w n="12.5">qui</w> <w n="12.6">venait</w> <w n="12.7">les</w> <w n="12.8">entendre</w></l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">Bouche</w> <w n="13.2">béante</w> <w n="13.3">demeurait</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">leur</w> <w n="14.3">professeur</w> <w n="14.4">même</w> <w n="14.5">en</w> <w n="14.6">extase</w> <w n="14.7">admirait</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Une</w> <w n="15.2">nuit</w> <w n="15.3">qu</w>’<w n="15.4">ils</w> <w n="15.5">dormaient</w> <w n="15.6">dans</w> <w n="15.7">le</w> <w n="15.8">grenier</w> <w n="15.9">du</w> <w n="15.10">maître</w></l>
						<l n="16" num="1.16"><w n="16.1">Sur</w> <w n="16.2">un</w> <w n="16.3">grabat</w> <w n="16.4">commun</w>, <w n="16.5">voilà</w> <w n="16.6">mes</w> <w n="16.7">jeunes</w> <w n="16.8">gens</w></l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1">Qui</w>, <w n="17.2">dans</w> <w n="17.3">un</w> <w n="17.4">rêve</w>, <w n="17.5">pensent</w> <w n="17.6">être</w></l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">A</w> <w n="18.2">se</w> <w n="18.3">disputer</w> <w n="18.4">sur</w> <w n="18.5">les</w> <w n="18.6">bancs</w>.</l>
						<l n="19" num="1.19"><w n="19.1">Je</w> <w n="19.2">démontre</w>, <w n="19.3">dit</w> <w n="19.4">l</w>’<w n="19.5">un</w>. <w n="19.6">Je</w> <w n="19.7">distingue</w>, <w n="19.8">dit</w> <w n="19.9">l</w>’<w n="19.10">autre</w>.</l>
						<l n="20" num="1.20"><w n="20.1">Or</w>, <w n="20.2">voici</w> <w n="20.3">mon</w> <w n="20.4">dilemme</w>. <w n="20.5">Ergo</w>, <w n="20.6">voici</w> <w n="20.7">le</w> <w n="20.8">nôtre</w>…</l>
						<l n="21" num="1.21"><w n="21.1">A</w> <w n="21.2">ces</w> <w n="21.3">mots</w>, <w n="21.4">nos</w> <w n="21.5">rêveurs</w>, <w n="21.6">crians</w>, <w n="21.7">gesticulans</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Au</w> <w n="22.2">lieu</w> <w n="22.3">de</w> <w n="22.4">s</w>’<w n="22.5">en</w> <w n="22.6">tenir</w> <w n="22.7">aux</w> <w n="22.8">simples</w> <w n="22.9">argumens</w></l>
						<l n="23" num="1.23"><w n="23.1">D</w>’<w n="23.2">Aristote</w> <w n="23.3">ou</w> <w n="23.4">de</w> <w n="23.5">Scot</w>, <w n="23.6">soutiennent</w> <w n="23.7">leur</w> <w n="23.8">dilemme</w></l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">De</w> <w n="24.2">coups</w> <w n="24.3">de</w> <w n="24.4">poing</w> <w n="24.5">bien</w> <w n="24.6">assenés</w></l>
						<l n="25" num="1.25"><space unit="char" quantity="18"></space><w n="25.1">Sur</w> <w n="25.2">le</w> <w n="25.3">nez</w>.</l>
						<l n="26" num="1.26"><w n="26.1">Tous</w> <w n="26.2">deux</w> <w n="26.3">sautent</w> <w n="26.4">du</w> <w n="26.5">lit</w> <w n="26.6">dans</w> <w n="26.7">une</w> <w n="26.8">rage</w> <w n="26.9">extrême</w>,</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">Se</w> <w n="27.2">saisissent</w> <w n="27.3">par</w> <w n="27.4">les</w> <w n="27.5">cheveux</w>,</l>
						<l n="28" num="1.28"><w n="28.1">Tombent</w> <w n="28.2">et</w> <w n="28.3">font</w> <w n="28.4">tomber</w> <w n="28.5">pêle</w>-<w n="28.6">mêle</w> <w n="28.7">avec</w> <w n="28.8">eux</w></l>
						<l n="29" num="1.29"><w n="29.1">Tous</w> <w n="29.2">les</w> <w n="29.3">meubles</w> <w n="29.4">qu</w>’<w n="29.5">ils</w> <w n="29.6">ont</w>, <w n="29.7">deux</w> <w n="29.8">chaises</w>, <w n="29.9">une</w> <w n="29.10">table</w>,</l>
						<l n="30" num="1.30"><w n="30.1">Et</w> <w n="30.2">quatre</w> <w n="30.3">in</w>-<w n="30.4">folios</w> <w n="30.5">écrits</w> <w n="30.6">sur</w> <w n="30.7">parchemin</w>.</l>
						<l n="31" num="1.31"><w n="31.1">Le</w> <w n="31.2">professeur</w> <w n="31.3">arrive</w>, <w n="31.4">une</w> <w n="31.5">chandelle</w> <w n="31.6">en</w> <w n="31.7">main</w>,</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space><w n="32.1">A</w> <w n="32.2">ce</w> <w n="32.3">tintamarre</w> <w n="32.4">effroyable</w> :</l>
						<l n="33" num="1.33"><w n="33.1">Le</w> <w n="33.2">diable</w> <w n="33.3">est</w> <w n="33.4">donc</w> <w n="33.5">ici</w> ! <w n="33.6">dit</w>-<w n="33.7">il</w> <w n="33.8">tout</w> <w n="33.9">hors</w> <w n="33.10">de</w> <w n="33.11">soi</w> :</l>
						<l n="34" num="1.34"><w n="34.1">Comment</w> ! <w n="34.2">sans</w> <w n="34.3">y</w> <w n="34.4">voir</w> <w n="34.5">clair</w> <w n="34.6">et</w> <w n="34.7">sans</w> <w n="34.8">savoir</w> <w n="34.9">pourquoi</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Vous</w> <w n="35.2">vous</w> <w n="35.3">battez</w> <w n="35.4">ainsi</w> ! <w n="35.5">Quelle</w> <w n="35.6">mouche</w> <w n="35.7">vous</w> <w n="35.8">pique</w> ?</l>
						<l n="36" num="1.36"><w n="36.1">Nous</w> <w n="36.2">ne</w> <w n="36.3">nous</w> <w n="36.4">battons</w> <w n="36.5">point</w>, <w n="36.6">disent</w>-<w n="36.7">ils</w> ; <w n="36.8">jugez</w> <w n="36.9">mieux</w> :</l>
						<l n="37" num="1.37"><space unit="char" quantity="8"></space><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">que</w> <w n="37.4">nous</w> <w n="37.5">repassons</w> <w n="37.6">tous</w> <w n="37.7">deux</w></l>
						<l n="38" num="1.38"><space unit="char" quantity="8"></space><w n="38.1">Nos</w> <w n="38.2">leçons</w> <w n="38.3">de</w> <w n="38.4">métaphysique</w>.</l>
					</lg>
				</div></body></text></TEI>