<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO57">
					<head type="number">FABLE XIII</head>
					<head type="main">L’Hermine, le Castor et le Sanglier</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Une</w> <w n="1.2">hermine</w>, <w n="1.3">un</w> <w n="1.4">castor</w>, <w n="1.5">un</w> <w n="1.6">jeune</w> <w n="1.7">sanglier</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Cadets</w> <w n="2.2">de</w> <w n="2.3">leur</w> <w n="2.4">famille</w>, <w n="2.5">et</w> <w n="2.6">partant</w> <w n="2.7">sans</w> <w n="2.8">fortune</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Dans</w> <w n="3.2">l</w>’<w n="3.3">espoir</w> <w n="3.4">d</w>’<w n="3.5">en</w> <w n="3.6">acquérir</w> <w n="3.7">une</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Quittèrent</w> <w n="4.2">leur</w> <w n="4.3">forêt</w>, <w n="4.4">leur</w> <w n="4.5">étang</w>, <w n="4.6">leur</w> <w n="4.7">hallier</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Après</w> <w n="5.2">un</w> <w n="5.3">long</w> <w n="5.4">voyage</w>, <w n="5.5">après</w> <w n="5.6">mainte</w> <w n="5.7">aventure</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Ils</w> <w n="6.2">arrivent</w> <w n="6.3">dans</w> <w n="6.4">un</w> <w n="6.5">pays</w></l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Où</w> <w n="7.2">s</w>’<w n="7.3">offrent</w> <w n="7.4">à</w> <w n="7.5">leurs</w> <w n="7.6">yeux</w> <w n="7.7">ravis</w></l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Tous</w> <w n="8.2">les</w> <w n="8.3">trésors</w> <w n="8.4">de</w> <w n="8.5">la</w> <w n="8.6">nature</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Des</w> <w n="9.2">prés</w>, <w n="9.3">des</w> <w n="9.4">eaux</w>, <w n="9.5">des</w> <w n="9.6">bois</w>, <w n="9.7">des</w> <w n="9.8">vergers</w> <w n="9.9">pleins</w> <w n="9.10">de</w> <w n="9.11">fruits</w>.</l>
						<l n="10" num="1.10"><w n="10.1">Nos</w> <w n="10.2">pélerins</w>, <w n="10.3">voyant</w> <w n="10.4">cette</w> <w n="10.5">terre</w> <w n="10.6">chérie</w>,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Éprouvent</w> <w n="11.2">les</w> <w n="11.3">mêmes</w> <w n="11.4">transports</w></l>
						<l n="12" num="1.12"><w n="12.1">Qu</w>’<w n="12.2">Énée</w> <w n="12.3">et</w> <w n="12.4">ses</w> <w n="12.5">Troyens</w> <w n="12.6">en</w> <w n="12.7">découvrant</w> <w n="12.8">les</w> <w n="12.9">bords</w></l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">Du</w> <w n="13.2">royaume</w> <w n="13.3">de</w> <w n="13.4">Lavinie</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Mais</w> <w n="14.2">ce</w> <w n="14.3">riche</w> <w n="14.4">pays</w> <w n="14.5">était</w> <w n="14.6">de</w> <w n="14.7">toutes</w> <w n="14.8">parts</w></l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">Entouré</w> <w n="15.2">d</w>’<w n="15.3">un</w> <w n="15.4">marais</w> <w n="15.5">de</w> <w n="15.6">bourbe</w></l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">Où</w> <w n="16.2">des</w> <w n="16.3">serpens</w> <w n="16.4">et</w> <w n="16.5">des</w> <w n="16.6">lésards</w></l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1">Se</w> <w n="17.2">jouait</w> <w n="17.3">l</w>’<w n="17.4">effroyable</w> <w n="17.5">tourbe</w>.</l>
						<l n="18" num="1.18"><w n="18.1">Il</w> <w n="18.2">fallait</w> <w n="18.3">le</w> <w n="18.4">passer</w>, <w n="18.5">et</w> <w n="18.6">nos</w> <w n="18.7">trois</w> <w n="18.8">voyageurs</w></l>
						<l n="19" num="1.19"><w n="19.1">S</w>’<w n="19.2">arrêtent</w> <w n="19.3">sur</w> <w n="19.4">le</w> <w n="19.5">bord</w>, <w n="19.6">étonnés</w> <w n="19.7">et</w> <w n="19.8">rêveurs</w>.</l>
						<l n="20" num="1.20"><w n="20.1">L</w>’<w n="20.2">hermine</w> <w n="20.3">la</w> <w n="20.4">première</w> <w n="20.5">avance</w> <w n="20.6">un</w> <w n="20.7">peu</w> <w n="20.8">la</w> <w n="20.9">patte</w> ;</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">Elle</w> <w n="21.2">la</w> <w n="21.3">retire</w> <w n="21.4">aussitôt</w>,</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1">En</w> <w n="22.2">arrière</w> <w n="22.3">elle</w> <w n="22.4">fait</w> <w n="22.5">un</w> <w n="22.6">saut</w>,</l>
						<l n="23" num="1.23"><w n="23.1">En</w> <w n="23.2">disant</w> : <w n="23.3">mes</w> <w n="23.4">amis</w>, <w n="23.5">fuyons</w> <w n="23.6">en</w> <w n="23.7">grande</w> <w n="23.8">hâte</w> ;</l>
						<l n="24" num="1.24"><w n="24.1">Ce</w> <w n="24.2">lieu</w>, <w n="24.3">tout</w> <w n="24.4">beau</w> <w n="24.5">qu</w>’<w n="24.6">il</w> <w n="24.7">est</w>, <w n="24.8">ne</w> <w n="24.9">peut</w> <w n="24.10">nous</w> <w n="24.11">convenir</w> :</l>
						<l n="25" num="1.25"><w n="25.1">Pour</w> <w n="25.2">arriver</w> <w n="25.3">là</w> <w n="25.4">bas</w> <w n="25.5">il</w> <w n="25.6">faudrait</w> <w n="25.7">se</w> <w n="25.8">salir</w> ;</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Et</w> <w n="26.2">moi</w> <w n="26.3">je</w> <w n="26.4">suis</w> <w n="26.5">si</w> <w n="26.6">délicate</w>,</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">Qu</w>’<w n="27.2">une</w> <w n="27.3">tache</w> <w n="27.4">me</w> <w n="27.5">fait</w> <w n="27.6">mourir</w>.</l>
						<l n="28" num="1.28"><w n="28.1">Ma</w> <w n="28.2">sœur</w>, <w n="28.3">dit</w> <w n="28.4">le</w> <w n="28.5">castor</w>, <w n="28.6">un</w> <w n="28.7">peu</w> <w n="28.8">de</w> <w n="28.9">patience</w> ;</l>
						<l n="29" num="1.29"><w n="29.1">On</w> <w n="29.2">peut</w>, <w n="29.3">sans</w> <w n="29.4">se</w> <w n="29.5">tacher</w>, <w n="29.6">quelquefois</w> <w n="29.7">réussir</w> ;</l>
						<l n="30" num="1.30"><w n="30.1">Il</w> <w n="30.2">faut</w> <w n="30.3">alors</w> <w n="30.4">du</w> <w n="30.5">temps</w> <w n="30.6">et</w> <w n="30.7">de</w> <w n="30.8">l</w>’<w n="30.9">intelligence</w> :</l>
						<l n="31" num="1.31"><w n="31.1">Nous</w> <w n="31.2">avons</w> <w n="31.3">tout</w> <w n="31.4">cela</w> : <w n="31.5">pour</w> <w n="31.6">moi</w>, <w n="31.7">qui</w> <w n="31.8">suis</w> <w n="31.9">maçon</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Je</w> <w n="32.2">vais</w> <w n="32.3">en</w> <w n="32.4">quinze</w> <w n="32.5">jours</w> <w n="32.6">vous</w> <w n="32.7">bâtir</w> <w n="32.8">un</w> <w n="32.9">beau</w> <w n="32.10">pont</w></l>
						<l n="33" num="1.33"><w n="33.1">Sur</w> <w n="33.2">lequel</w> <w n="33.3">nous</w> <w n="33.4">pourrons</w>, <w n="33.5">sans</w> <w n="33.6">craindre</w> <w n="33.7">les</w> <w n="33.8">morsures</w></l>
						<l n="34" num="1.34"><w n="34.1">De</w> <w n="34.2">ces</w> <w n="34.3">vilains</w> <w n="34.4">serpens</w>, <w n="34.5">sans</w> <w n="34.6">gâter</w> <w n="34.7">nos</w> <w n="34.8">fourrures</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Arriver</w> <w n="35.2">au</w> <w n="35.3">milieu</w> <w n="35.4">de</w> <w n="35.5">ce</w> <w n="35.6">charmant</w> <w n="35.7">vallon</w>.</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space><w n="36.1">Quinze</w> <w n="36.2">jours</w> ! <w n="36.3">ce</w> <w n="36.4">terme</w> <w n="36.5">est</w> <w n="36.6">bien</w> <w n="36.7">long</w>,</l>
						<l n="37" num="1.37"><w n="37.1">Répond</w> <w n="37.2">le</w> <w n="37.3">sanglier</w> : <w n="37.4">moi</w>, <w n="37.5">j</w>’<w n="37.6">y</w> <w n="37.7">serai</w> <w n="37.8">plus</w> <w n="37.9">vîte</w> :</l>
						<l n="38" num="1.38"><w n="38.1">Vous</w> <w n="38.2">allez</w> <w n="38.3">voir</w> <w n="38.4">comment</w>. <w n="38.5">En</w> <w n="38.6">prononçant</w> <w n="38.7">ces</w> <w n="38.8">mots</w>,</l>
						<l n="39" num="1.39"><space unit="char" quantity="8"></space><w n="39.1">Le</w> <w n="39.2">voilà</w> <w n="39.3">qui</w> <w n="39.4">se</w> <w n="39.5">précipite</w></l>
						<l n="40" num="1.40"><w n="40.1">Au</w> <w n="40.2">plus</w> <w n="40.3">fort</w> <w n="40.4">du</w> <w n="40.5">bourbier</w>, <w n="40.6">s</w>’<w n="40.7">y</w> <w n="40.8">plonge</w> <w n="40.9">jusqu</w>’<w n="40.10">au</w> <w n="40.11">dos</w>,</l>
						<l n="41" num="1.41"><w n="41.1">A</w> <w n="41.2">travers</w> <w n="41.3">les</w> <w n="41.4">serpens</w>, <w n="41.5">les</w> <w n="41.6">lésards</w>, <w n="41.7">les</w> <w n="41.8">crapauds</w>,</l>
						<l n="42" num="1.42"><w n="42.1">Marche</w>, <w n="42.2">pousse</w> <w n="42.3">à</w> <w n="42.4">son</w> <w n="42.5">but</w>, <w n="42.6">arrive</w> <w n="42.7">plein</w> <w n="42.8">de</w> <w n="42.9">boue</w> ;</l>
						<l n="43" num="1.43"><space unit="char" quantity="8"></space><w n="43.1">Et</w> <w n="43.2">là</w>, <w n="43.3">tandis</w> <w n="43.4">qu</w>’<w n="43.5">il</w> <w n="43.6">se</w> <w n="43.7">secoue</w>,</l>
						<l n="44" num="1.44"><w n="44.1">Jetant</w> <w n="44.2">à</w> <w n="44.3">ses</w> <w n="44.4">amis</w> <w n="44.5">un</w> <w n="44.6">regard</w> <w n="44.7">de</w> <w n="44.8">dédain</w>,</l>
						<l n="45" num="1.45"><w n="45.1">Apprenez</w>, <w n="45.2">leur</w> <w n="45.3">dit</w>-<w n="45.4">il</w>, <w n="45.5">comme</w> <w n="45.6">on</w> <w n="45.7">fait</w> <w n="45.8">son</w> <w n="45.9">chemin</w>.</l>
					</lg>
				</div></body></text></TEI>