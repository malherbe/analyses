<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO54">
					<head type="number">FABLE X</head>
					<head type="main">Le Renard déguisé</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">renard</w> <w n="1.3">plein</w> <w n="1.4">d</w>’<w n="1.5">esprit</w>, <w n="1.6">d</w>’<w n="1.7">adresse</w>, <w n="1.8">de</w> <w n="1.9">prudence</w>,</l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">la</w> <w n="2.3">cour</w> <w n="2.4">d</w>’<w n="2.5">un</w> <w n="2.6">lion</w> <w n="2.7">servait</w> <w n="2.8">depuis</w> <w n="2.9">long</w>-<w n="2.10">temps</w> ;</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Les</w> <w n="3.2">succès</w> <w n="3.3">les</w> <w n="3.4">plus</w> <w n="3.5">éclatans</w></l>
						<l n="4" num="1.4"><w n="4.1">Avaient</w> <w n="4.2">prouvé</w> <w n="4.3">son</w> <w n="4.4">zèle</w> <w n="4.5">et</w> <w n="4.6">son</w> <w n="4.7">intelligence</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Pour</w> <w n="5.2">peu</w> <w n="5.3">qu</w>’<w n="5.4">on</w> <w n="5.5">l</w>’<w n="5.6">employât</w>, <w n="5.7">toute</w> <w n="5.8">affaire</w> <w n="5.9">allait</w> <w n="5.10">bien</w>.</l>
						<l n="6" num="1.6"><w n="6.1">On</w> <w n="6.2">le</w> <w n="6.3">louait</w> <w n="6.4">beaucoup</w>, <w n="6.5">mais</w> <w n="6.6">sans</w> <w n="6.7">lui</w> <w n="6.8">donner</w> <w n="6.9">rien</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">habile</w> <w n="7.4">renard</w> <w n="7.5">était</w> <w n="7.6">dans</w> <w n="7.7">l</w>’<w n="7.8">indigence</w>.</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Lassé</w> <w n="8.2">de</w> <w n="8.3">servir</w> <w n="8.4">des</w> <w n="8.5">ingrats</w>,</l>
						<l n="9" num="1.9"><w n="9.1">De</w> <w n="9.2">réussir</w> <w n="9.3">toujours</w> <w n="9.4">sans</w> <w n="9.5">en</w> <w n="9.6">être</w> <w n="9.7">plus</w> <w n="9.8">gras</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Il</w> <w n="10.2">s</w>’<w n="10.3">enfuit</w> <w n="10.4">de</w> <w n="10.5">la</w> <w n="10.6">cour</w> ; <w n="10.7">dans</w> <w n="10.8">un</w> <w n="10.9">bois</w> <w n="10.10">solitaire</w></l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Il</w> <w n="11.2">s</w>’<w n="11.3">en</w> <w n="11.4">va</w> <w n="11.5">trouver</w> <w n="11.6">son</w> <w n="11.7">grand</w>-<w n="11.8">père</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Vieux</w> <w n="12.2">renard</w> <w n="12.3">retiré</w>, <w n="12.4">qui</w> <w n="12.5">jadis</w> <w n="12.6">fut</w> <w n="12.7">visir</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Là</w>, <w n="13.2">contant</w> <w n="13.3">ses</w> <w n="13.4">exploits</w>, <w n="13.5">et</w> <w n="13.6">puis</w> <w n="13.7">les</w> <w n="13.8">injustices</w>,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">Les</w> <w n="14.2">dégoûts</w>, <w n="14.3">qu</w>’<w n="14.4">il</w> <w n="14.5">eut</w> <w n="14.6">à</w> <w n="14.7">souffrir</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Il</w> <w n="15.2">demande</w> <w n="15.3">pourquoi</w> <w n="15.4">de</w> <w n="15.5">si</w> <w n="15.6">nombreux</w> <w n="15.7">services</w></l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">N</w>’<w n="16.2">ont</w> <w n="16.3">jamais</w> <w n="16.4">pu</w> <w n="16.5">rien</w> <w n="16.6">obtenir</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Le</w> <w n="17.2">bon</w> <w n="17.3">homme</w> <w n="17.4">renard</w>, <w n="17.5">avec</w> <w n="17.6">sa</w> <w n="17.7">voix</w> <w n="17.8">cassée</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Lui</w> <w n="18.2">dit</w> : <w n="18.3">Mon</w> <w n="18.4">cher</w> <w n="18.5">enfant</w>, <w n="18.6">la</w> <w n="18.7">semaine</w> <w n="18.8">passée</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Un</w> <w n="19.2">blaireau</w>, <w n="19.3">mon</w> <w n="19.4">cousin</w>, <w n="19.5">est</w> <w n="19.6">mort</w> <w n="19.7">dans</w> <w n="19.8">ce</w> <w n="19.9">terrier</w> :</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">C</w>’<w n="20.2">est</w> <w n="20.3">moi</w> <w n="20.4">qui</w> <w n="20.5">suis</w> <w n="20.6">son</w> <w n="20.7">héritier</w>,</l>
						<l n="21" num="1.21"><w n="21.1">J</w>’<w n="21.2">ai</w> <w n="21.3">conservé</w> <w n="21.4">sa</w> <w n="21.5">peau</w> : <w n="21.6">mets</w>-<w n="21.7">la</w> <w n="21.8">dessus</w> <w n="21.9">la</w> <w n="21.10">tienne</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Et</w> <w n="22.2">retourne</w> <w n="22.3">à</w> <w n="22.4">la</w> <w n="22.5">cour</w>. <w n="22.6">Le</w> <w n="22.7">renard</w> <w n="22.8">avec</w> <w n="22.9">peine</w></l>
						<l n="23" num="1.23"><w n="23.1">Se</w> <w n="23.2">soumit</w> <w n="23.3">au</w> <w n="23.4">conseil</w> ; <w n="23.5">affublé</w> <w n="23.6">de</w> <w n="23.7">la</w> <w n="23.8">peau</w></l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">De</w> <w n="24.2">feu</w> <w n="24.3">son</w> <w n="24.4">cousin</w> <w n="24.5">le</w> <w n="24.6">blaireau</w>,</l>
						<l n="25" num="1.25"><w n="25.1">Il</w> <w n="25.2">va</w> <w n="25.3">se</w> <w n="25.4">regarder</w> <w n="25.5">dans</w> <w n="25.6">l</w>’<w n="25.7">eau</w> <w n="25.8">d</w>’<w n="25.9">une</w> <w n="25.10">fontaine</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Se</w> <w n="26.2">trouve</w> <w n="26.3">l</w>’<w n="26.4">air</w> <w n="26.5">d</w>’<w n="26.6">un</w> <w n="26.7">sot</w>, <w n="26.8">tel</w> <w n="26.9">qu</w>’<w n="26.10">était</w> <w n="26.11">le</w> <w n="26.12">cousin</w>.</l>
						<l n="27" num="1.27"><w n="27.1">Tout</w> <w n="27.2">honteux</w>, <w n="27.3">de</w> <w n="27.4">la</w> <w n="27.5">cour</w> <w n="27.6">il</w> <w n="27.7">reprend</w> <w n="27.8">le</w> <w n="27.9">chemin</w>.</l>
						<l n="28" num="1.28"><w n="28.1">Mais</w>, <w n="28.2">quelques</w> <w n="28.3">mois</w> <w n="28.4">après</w>, <w n="28.5">dans</w> <w n="28.6">un</w> <w n="28.7">riche</w> <w n="28.8">équipage</w>,</l>
						<l n="29" num="1.29"><w n="29.1">Entouré</w> <w n="29.2">de</w> <w n="29.3">valets</w>, <w n="29.4">d</w>’<w n="29.5">esclaves</w>, <w n="29.6">de</w> <w n="29.7">flatteurs</w>,</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">Comblé</w> <w n="30.2">de</w> <w n="30.3">dons</w> <w n="30.4">et</w> <w n="30.5">de</w> <w n="30.6">faveurs</w>,</l>
						<l n="31" num="1.31"><w n="31.1">Il</w> <w n="31.2">vient</w> <w n="31.3">de</w> <w n="31.4">sa</w> <w n="31.5">fortune</w> <w n="31.6">au</w> <w n="31.7">vieillard</w> <w n="31.8">faire</w> <w n="31.9">hommage</w> :</l>
						<l n="32" num="1.32"><w n="32.1">Il</w> <w n="32.2">était</w> <w n="32.3">grand</w> <w n="32.4">visir</w>. <w n="32.5">Je</w> <w n="32.6">te</w> <w n="32.7">l</w>’<w n="32.8">avais</w> <w n="32.9">bien</w> <w n="32.10">dit</w>,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space><w n="33.1">S</w>’<w n="33.2">écrie</w> <w n="33.3">alors</w> <w n="33.4">le</w> <w n="33.5">vieux</w> <w n="33.6">grand</w>-<w n="33.7">père</w> ;</l>
						<l n="34" num="1.34"><w n="34.1">Mon</w> <w n="34.2">ami</w>, <w n="34.3">chez</w> <w n="34.4">les</w> <w n="34.5">grands</w> <w n="34.6">quiconque</w> <w n="34.7">voudra</w> <w n="34.8">plaire</w></l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space><w n="35.1">Doit</w> <w n="35.2">d</w>’<w n="35.3">abord</w> <w n="35.4">cacher</w> <w n="35.5">son</w> <w n="35.6">esprit</w>.</l>
					</lg>
				</div></body></text></TEI>