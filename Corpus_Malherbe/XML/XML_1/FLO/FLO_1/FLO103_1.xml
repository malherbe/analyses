<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><div type="poem" key="FLO103">
					<head type="number">FABLE XV</head>
					<head type="main">La Sauterelle</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">C</w>’<w n="1.2">en</w> <w n="1.3">est</w> <w n="1.4">fait</w>, <w n="1.5">je</w> <w n="1.6">quitte</w> <w n="1.7">le</w> <w n="1.8">monde</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">veux</w> <w n="2.3">fuir</w> <w n="2.4">pour</w> <w n="2.5">jamais</w> <w n="2.6">le</w> <w n="2.7">spectacle</w> <w n="2.8">odieux</w></l>
						<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">crimes</w>, <w n="3.3">des</w> <w n="3.4">horreurs</w>, <w n="3.5">dont</w> <w n="3.6">sont</w> <w n="3.7">blessés</w> <w n="3.8">mes</w> <w n="3.9">yeux</w>.</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Dans</w> <w n="4.2">une</w> <w n="4.3">retraite</w> <w n="4.4">profonde</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Loin</w> <w n="5.2">des</w> <w n="5.3">vices</w>, <w n="5.4">loin</w> <w n="5.5">des</w> <w n="5.6">abus</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Je</w> <w n="6.2">passerai</w> <w n="6.3">mes</w> <w n="6.4">jours</w> <w n="6.5">doucement</w> <w n="6.6">à</w> <w n="6.7">maudire</w></l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Les</w> <w n="7.2">méchans</w> <w n="7.3">de</w> <w n="7.4">moi</w> <w n="7.5">trop</w> <w n="7.6">connus</w>.</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Seule</w> <w n="8.2">ici</w> <w n="8.3">bas</w> <w n="8.4">j</w>’<w n="8.5">ai</w> <w n="8.6">des</w> <w n="8.7">vertus</w> :</l>
						<l n="9" num="1.9"><w n="9.1">Aussi</w> <w n="9.2">pour</w> <w n="9.3">ennemi</w> <w n="9.4">j</w>’<w n="9.5">ai</w> <w n="9.6">tout</w> <w n="9.7">ce</w> <w n="9.8">qui</w> <w n="9.9">respire</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Tout</w> <w n="10.2">l</w>’<w n="10.3">univers</w> <w n="10.4">m</w>’<w n="10.5">en</w> <w n="10.6">veut</w> ; <w n="10.7">homme</w>, <w n="10.8">enfans</w>, <w n="10.9">animaux</w>,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Jusqu</w>’<w n="11.2">au</w> <w n="11.3">plus</w> <w n="11.4">petit</w> <w n="11.5">des</w> <w n="11.6">oiseaux</w>,</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">Tous</w> <w n="12.2">sont</w> <w n="12.3">occupés</w> <w n="12.4">de</w> <w n="12.5">me</w> <w n="12.6">nuire</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Eh</w> ! <w n="13.2">qu</w>’<w n="13.3">ai</w>-<w n="13.4">je</w> <w n="13.5">fait</w> <w n="13.6">pourtant</w> ?… <w n="13.7">Que</w> <w n="13.8">du</w> <w n="13.9">bien</w>. <w n="13.10">Les</w> <w n="13.11">ingrats</w> !</l>
						<l n="14" num="1.14"><w n="14.1">Ils</w> <w n="14.2">me</w> <w n="14.3">regretteront</w>, <w n="14.4">mais</w> <w n="14.5">après</w> <w n="14.6">mon</w> <w n="14.7">trépas</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Ainsi</w> <w n="15.2">se</w> <w n="15.3">lamentait</w> <w n="15.4">certaine</w> <w n="15.5">sauterelle</w>,</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">Hypocondre</w> <w n="16.2">et</w> <w n="16.3">n</w>’<w n="16.4">estimant</w> <w n="16.5">qu</w>’<w n="16.6">elle</w>.</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1">Où</w> <w n="17.2">prenez</w>-<w n="17.3">vous</w> <w n="17.4">cela</w>, <w n="17.5">ma</w> <w n="17.6">sœur</w> ?</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Lui</w> <w n="18.2">dit</w> <w n="18.3">une</w> <w n="18.4">de</w> <w n="18.5">ses</w> <w n="18.6">compagnes</w> :</l>
						<l n="19" num="1.19"><w n="19.1">Quoi</w> ! <w n="19.2">vous</w> <w n="19.3">ne</w> <w n="19.4">pouvez</w> <w n="19.5">pas</w> <w n="19.6">vivre</w> <w n="19.7">dans</w> <w n="19.8">ces</w> <w n="19.9">campagnes</w></l>
						<l n="20" num="1.20"><w n="20.1">En</w> <w n="20.2">broutant</w> <w n="20.3">de</w> <w n="20.4">ces</w> <w n="20.5">prés</w> <w n="20.6">la</w> <w n="20.7">douce</w> <w n="20.8">et</w> <w n="20.9">tendre</w> <w n="20.10">fleur</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Sans</w> <w n="21.2">vous</w> <w n="21.3">embarrasser</w> <w n="21.4">des</w> <w n="21.5">affaires</w> <w n="21.6">du</w> <w n="21.7">monde</w> ?</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1">Je</w> <w n="22.2">sais</w> <w n="22.3">qu</w>’<w n="22.4">en</w> <w n="22.5">travers</w> <w n="22.6">il</w> <w n="22.7">abonde</w> :</l>
						<l n="23" num="1.23"><w n="23.1">Il</w> <w n="23.2">fut</w> <w n="23.3">ainsi</w> <w n="23.4">toujours</w>, <w n="23.5">et</w> <w n="23.6">toujours</w> <w n="23.7">il</w> <w n="23.8">sera</w> ;</l>
						<l n="24" num="1.24"><w n="24.1">Ce</w> <w n="24.2">que</w> <w n="24.3">vous</w> <w n="24.4">en</w> <w n="24.5">direz</w> <w n="24.6">grand</w>’<w n="24.7">chose</w> <w n="24.8">n</w>’<w n="24.9">y</w> <w n="24.10">fera</w>.</l>
						<l n="25" num="1.25"><w n="25.1">D</w>’<w n="25.2">ailleurs</w> <w n="25.3">où</w> <w n="25.4">vit</w>-<w n="25.5">on</w> <w n="25.6">mieux</w> ? <w n="25.7">Quant</w> <w n="25.8">à</w> <w n="25.9">votre</w> <w n="25.10">colère</w></l>
						<l n="26" num="1.26"><w n="26.1">Contre</w> <w n="26.2">ces</w> <w n="26.3">ennemis</w> <w n="26.4">qui</w> <w n="26.5">n</w>’<w n="26.6">en</w> <w n="26.7">veulent</w> <w n="26.8">qu</w>’<w n="26.9">à</w> <w n="26.10">vous</w>,</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">Je</w> <w n="27.2">pense</w>, <w n="27.3">ma</w> <w n="27.4">sœur</w>, <w n="27.5">entre</w> <w n="27.6">nous</w>,</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space><w n="28.1">Que</w> <w n="28.2">c</w>’<w n="28.3">est</w> <w n="28.4">peut</w>-<w n="28.5">être</w> <w n="28.6">une</w> <w n="28.7">chimère</w>,</l>
						<l n="29" num="1.29"><w n="29.1">Et</w> <w n="29.2">que</w> <w n="29.3">l</w>’<w n="29.4">orgueil</w> <w n="29.5">souvent</w> <w n="29.6">donne</w> <w n="29.7">ces</w> <w n="29.8">visions</w>.</l>
						<l n="30" num="1.30"><w n="30.1">Dédaignant</w> <w n="30.2">de</w> <w n="30.3">répondre</w> <w n="30.4">à</w> <w n="30.5">ces</w> <w n="30.6">sottes</w> <w n="30.7">raisons</w> ;</l>
						<l n="31" num="1.31"><w n="31.1">La</w> <w n="31.2">sauterelle</w> <w n="31.3">part</w>, <w n="31.4">et</w> <w n="31.5">sort</w> <w n="31.6">de</w> <w n="31.7">la</w> <w n="31.8">prairie</w>,</l>
						<l n="32" num="1.32"><space unit="char" quantity="18"></space><w n="32.1">Sa</w> <w n="32.2">patrie</w>.</l>
						<l n="33" num="1.33"><w n="33.1">Elle</w> <w n="33.2">sauta</w> <w n="33.3">deux</w> <w n="33.4">jours</w> <w n="33.5">pour</w> <w n="33.6">faire</w> <w n="33.7">deux</w> <w n="33.8">cents</w> <w n="33.9">pas</w>.</l>
						<l n="34" num="1.34"><w n="34.1">Alors</w> <w n="34.2">elle</w> <w n="34.3">se</w> <w n="34.4">croit</w> <w n="34.5">au</w> <w n="34.6">bout</w> <w n="34.7">de</w> <w n="34.8">l</w>’<w n="34.9">hémisphère</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Chez</w> <w n="35.2">un</w> <w n="35.3">peuple</w> <w n="35.4">inconnu</w>, <w n="35.5">dans</w> <w n="35.6">de</w> <w n="35.7">nouveaux</w> <w n="35.8">états</w> ;</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space><w n="36.1">Elle</w> <w n="36.2">admire</w> <w n="36.3">ces</w> <w n="36.4">beaux</w> <w n="36.5">climats</w>,</l>
						<l n="37" num="1.37"><w n="37.1">Salue</w> <w n="37.2">avec</w> <w n="37.3">respect</w> <w n="37.4">cette</w> <w n="37.5">rive</w> <w n="37.6">étrangère</w>.</l>
						<l n="38" num="1.38"><space unit="char" quantity="8"></space><w n="38.1">Près</w> <w n="38.2">de</w> <w n="38.3">là</w>, <w n="38.4">des</w> <w n="38.5">épis</w> <w n="38.6">nombreux</w></l>
						<l n="39" num="1.39"><w n="39.1">Sur</w> <w n="39.2">de</w> <w n="39.3">longs</w> <w n="39.4">chalumeaux</w>, <w n="39.5">à</w> <w n="39.6">six</w> <w n="39.7">pieds</w> <w n="39.8">de</w> <w n="39.9">la</w> <w n="39.10">terre</w>,</l>
						<l n="40" num="1.40"><w n="40.1">Ondoyans</w> <w n="40.2">et</w> <w n="40.3">pressés</w> <w n="40.4">se</w> <w n="40.5">balançaient</w> <w n="40.6">entre</w> <w n="40.7">eux</w>.</l>
						<l n="41" num="1.41"><space unit="char" quantity="8"></space><w n="41.1">Ah</w> ! <w n="41.2">que</w> <w n="41.3">voilà</w> <w n="41.4">bien</w> <w n="41.5">mon</w> <w n="41.6">affaire</w> !</l>
						<l n="42" num="1.42"><w n="42.1">Dit</w>-<w n="42.2">elle</w> <w n="42.3">avec</w> <w n="42.4">transport</w>, <w n="42.5">dans</w> <w n="42.6">ces</w> <w n="42.7">sombres</w> <w n="42.8">taillis</w></l>
						<l n="43" num="1.43"><w n="43.1">Je</w> <w n="43.2">trouverai</w> <w n="43.3">sans</w> <w n="43.4">doute</w> <w n="43.5">un</w> <w n="43.6">désert</w> <w n="43.7">solitaire</w> ;</l>
						<l n="44" num="1.44"><w n="44.1">C</w>’<w n="44.2">est</w> <w n="44.3">un</w> <w n="44.4">asile</w> <w n="44.5">sûr</w> <w n="44.6">contre</w> <w n="44.7">mes</w> <w n="44.8">ennemis</w>.</l>
						<l n="45" num="1.45"><w n="45.1">La</w> <w n="45.2">voilà</w> <w n="45.3">dans</w> <w n="45.4">le</w> <w n="45.5">blé</w>. <w n="45.6">Mais</w> <w n="45.7">dès</w> <w n="45.8">l</w>’<w n="45.9">aube</w> <w n="45.10">suivante</w>,</l>
						<l n="46" num="1.46"><space unit="char" quantity="8"></space><w n="46.1">Voici</w> <w n="46.2">venir</w> <w n="46.3">les</w> <w n="46.4">moissonneurs</w>.</l>
						<l n="47" num="1.47"><space unit="char" quantity="8"></space><w n="47.1">Leur</w> <w n="47.2">troupe</w> <w n="47.3">nombreuse</w> <w n="47.4">et</w> <w n="47.5">bruyante</w></l>
						<l n="48" num="1.48"><w n="48.1">S</w>’<w n="48.2">étend</w> <w n="48.3">en</w> <w n="48.4">demi</w>-<w n="48.5">cercle</w> ; <w n="48.6">et</w>, <w n="48.7">parmi</w> <w n="48.8">les</w> <w n="48.9">clameurs</w>,</l>
						<l n="49" num="1.49"><space unit="char" quantity="8"></space><w n="49.1">Les</w> <w n="49.2">ris</w>, <w n="49.3">les</w> <w n="49.4">chants</w> <w n="49.5">des</w> <w n="49.6">jeunes</w> <w n="49.7">filles</w>,</l>
						<l n="50" num="1.50"><w n="50.1">Les</w> <w n="50.2">épis</w> <w n="50.3">entassés</w> <w n="50.4">tombent</w> <w n="50.5">sous</w> <w n="50.6">les</w> <w n="50.7">faucilles</w>,</l>
						<l n="51" num="1.51"><w n="51.1">La</w> <w n="51.2">terre</w> <w n="51.3">se</w> <w n="51.4">découvre</w>, <w n="51.5">et</w> <w n="51.6">les</w> <w n="51.7">bleds</w> <w n="51.8">abattus</w></l>
						<l n="52" num="1.52"><space unit="char" quantity="8"></space><w n="52.1">Laissent</w> <w n="52.2">voir</w> <w n="52.3">les</w> <w n="52.4">sillons</w> <w n="52.5">tout</w> <w n="52.6">nus</w>.</l>
						<l n="53" num="1.53"><w n="53.1">Pour</w> <w n="53.2">le</w> <w n="53.3">coup</w>, <w n="53.4">s</w>’<w n="53.5">écriait</w> <w n="53.6">la</w> <w n="53.7">triste</w> <w n="53.8">sauterelle</w>,</l>
						<l n="54" num="1.54"><w n="54.1">Voilà</w> <w n="54.2">qui</w> <w n="54.3">prouve</w> <w n="54.4">bien</w> <w n="54.5">la</w> <w n="54.6">haine</w> <w n="54.7">universelle</w></l>
						<l n="55" num="1.55"><w n="55.1">Qui</w> <w n="55.2">par</w>-<w n="55.3">tout</w> <w n="55.4">me</w> <w n="55.5">poursuit</w> : <w n="55.6">à</w> <w n="55.7">peine</w> <w n="55.8">en</w> <w n="55.9">ce</w> <w n="55.10">pays</w></l>
						<l n="56" num="1.56"><w n="56.1">A</w>-<w n="56.2">t</w>-<w n="56.3">on</w> <w n="56.4">su</w> <w n="56.5">que</w> <w n="56.6">j</w>’<w n="56.7">étais</w>, <w n="56.8">qu</w>’<w n="56.9">un</w> <w n="56.10">peuple</w> <w n="56.11">d</w>’<w n="56.12">ennemis</w></l>
						<l n="57" num="1.57"><space unit="char" quantity="8"></space><w n="57.1">S</w>’<w n="57.2">en</w> <w n="57.3">vient</w> <w n="57.4">pour</w> <w n="57.5">chercher</w> <w n="57.6">sa</w> <w n="57.7">victime</w>.</l>
						<l n="58" num="1.58"><space unit="char" quantity="8"></space><w n="58.1">Dans</w> <w n="58.2">la</w> <w n="58.3">fureur</w> <w n="58.4">qui</w> <w n="58.5">les</w> <w n="58.6">anime</w>,</l>
						<l n="59" num="1.59"><w n="59.1">Employant</w> <w n="59.2">contre</w> <w n="59.3">moi</w> <w n="59.4">les</w> <w n="59.5">plus</w> <w n="59.6">affreux</w> <w n="59.7">moyens</w>,</l>
						<l n="60" num="1.60"><w n="60.1">De</w> <w n="60.2">peur</w> <w n="60.3">que</w> <w n="60.4">je</w> <w n="60.5">n</w>’<w n="60.6">échappe</w>, <w n="60.7">ils</w> <w n="60.8">ravagent</w> <w n="60.9">leurs</w> <w n="60.10">biens</w> :</l>
						<l n="61" num="1.61"><w n="61.1">Ils</w> <w n="61.2">y</w> <w n="61.3">mettraient</w> <w n="61.4">le</w> <w n="61.5">feu</w>, <w n="61.6">s</w>’<w n="61.7">il</w> <w n="61.8">était</w> <w n="61.9">nécessaire</w>.</l>
						<l n="62" num="1.62"><w n="62.1">Eh</w> ! <w n="62.2">messieurs</w>, <w n="62.3">me</w> <w n="62.4">voilà</w>, <w n="62.5">dit</w>-<w n="62.6">elle</w> <w n="62.7">en</w> <w n="62.8">se</w> <w n="62.9">montrant</w> ;</l>
						<l n="63" num="1.63"><space unit="char" quantity="8"></space><w n="63.1">Finissez</w> <w n="63.2">un</w> <w n="63.3">travail</w> <w n="63.4">si</w> <w n="63.5">grand</w>,</l>
						<l n="64" num="1.64"><space unit="char" quantity="8"></space><w n="64.1">Je</w> <w n="64.2">me</w> <w n="64.3">livre</w> <w n="64.4">à</w> <w n="64.5">votre</w> <w n="64.6">colère</w>.</l>
						<l n="65" num="1.65"><space unit="char" quantity="8"></space><w n="65.1">Un</w> <w n="65.2">moissonneur</w>, <w n="65.3">dans</w> <w n="65.4">ce</w> <w n="65.5">moment</w>,</l>
						<l n="66" num="1.66"><w n="66.1">Par</w> <w n="66.2">hasard</w> <w n="66.3">la</w> <w n="66.4">distingue</w> ; <w n="66.5">il</w> <w n="66.6">se</w> <w n="66.7">baisse</w>, <w n="66.8">la</w> <w n="66.9">prend</w>,</l>
						<l n="67" num="1.67"><w n="67.1">Et</w> <w n="67.2">dit</w>, <w n="67.3">en</w> <w n="67.4">la</w> <w n="67.5">jetant</w> <w n="67.6">dans</w> <w n="67.7">une</w> <w n="67.8">herbe</w> <w n="67.9">fleurie</w> :</l>
						<l n="68" num="1.68"><space unit="char" quantity="8"></space><w n="68.1">Va</w> <w n="68.2">manger</w>, <w n="68.3">ma</w> <w n="68.4">petite</w> <w n="68.5">amie</w>.</l>
					</lg>
				</div></body></text></TEI>