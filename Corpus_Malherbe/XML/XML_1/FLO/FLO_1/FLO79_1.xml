<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="FLO79">
					<head type="number">FABLE XIII</head>
					<head type="main">Le Lapin et la Sarcelle</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="10"></space><w n="1.1">Unis</w> <w n="1.2">dès</w> <w n="1.3">leurs</w> <w n="1.4">jeunes</w> <w n="1.5">ans</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="10"></space><w n="2.1">D</w>’<w n="2.2">une</w> <w n="2.3">amitié</w> <w n="2.4">fraternelle</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="10"></space><w n="3.1">Un</w> <w n="3.2">lapin</w>, <w n="3.3">une</w> <w n="3.4">sarcelle</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">Vivaient</w> <w n="4.2">heureux</w> <w n="4.3">et</w> <w n="4.4">contens</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">terrier</w> <w n="5.3">du</w> <w n="5.4">lapin</w> <w n="5.5">était</w> <w n="5.6">sur</w> <w n="5.7">la</w> <w n="5.8">lisière</w></l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">D</w>’<w n="6.2">un</w> <w n="6.3">parc</w> <w n="6.4">bordé</w> <w n="6.5">d</w>’<w n="6.6">une</w> <w n="6.7">rivière</w>.</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Soir</w> <w n="7.2">et</w> <w n="7.3">matin</w> <w n="7.4">nos</w> <w n="7.5">bons</w> <w n="7.6">amis</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Profitant</w> <w n="8.2">de</w> <w n="8.3">ce</w> <w n="8.4">voisinage</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Tantôt</w> <w n="9.2">au</w> <w n="9.3">bord</w> <w n="9.4">de</w> <w n="9.5">l</w>’<w n="9.6">eau</w>, <w n="9.7">tantôt</w> <w n="9.8">sous</w> <w n="9.9">le</w> <w n="9.10">feuillage</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">L</w>’<w n="10.2">un</w> <w n="10.3">chez</w> <w n="10.4">l</w>’<w n="10.5">autre</w> <w n="10.6">étaient</w> <w n="10.7">réunis</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Là</w>, <w n="11.2">prenant</w> <w n="11.3">leurs</w> <w n="11.4">repas</w>, <w n="11.5">se</w> <w n="11.6">contant</w> <w n="11.7">des</w> <w n="11.8">nouvelles</w>,</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">Ils</w> <w n="12.2">n</w>’<w n="12.3">en</w> <w n="12.4">trouvaient</w> <w n="12.5">point</w> <w n="12.6">de</w> <w n="12.7">si</w> <w n="12.8">belles</w></l>
						<l n="13" num="1.13"><w n="13.1">Que</w> <w n="13.2">de</w> <w n="13.3">se</w> <w n="13.4">répéter</w> <w n="13.5">qu</w>’<w n="13.6">ils</w> <w n="13.7">s</w>’<w n="13.8">aimeraient</w> <w n="13.9">toujours</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Ce</w> <w n="14.2">sujet</w> <w n="14.3">revenait</w> <w n="14.4">sans</w> <w n="14.5">cesse</w> <w n="14.6">en</w> <w n="14.7">leurs</w> <w n="14.8">discours</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Tout</w> <w n="15.2">était</w> <w n="15.3">en</w> <w n="15.4">commun</w>, <w n="15.5">plaisir</w>, <w n="15.6">chagrin</w>, <w n="15.7">souffrance</w> ;</l>
						<l n="16" num="1.16"><w n="16.1">Ce</w> <w n="16.2">qui</w> <w n="16.3">manquait</w> <w n="16.4">à</w> <w n="16.5">l</w>’<w n="16.6">un</w>, <w n="16.7">l</w>’<w n="16.8">autre</w> <w n="16.9">le</w> <w n="16.10">regrettait</w> ;</l>
						<l n="17" num="1.17"><w n="17.1">Si</w> <w n="17.2">l</w>’<w n="17.3">un</w> <w n="17.4">avait</w> <w n="17.5">du</w> <w n="17.6">mal</w>, <w n="17.7">son</w> <w n="17.8">ami</w> <w n="17.9">le</w> <w n="17.10">sentait</w> ;</l>
						<l n="18" num="1.18"><w n="18.1">Si</w> <w n="18.2">d</w>’<w n="18.3">un</w> <w n="18.4">bien</w> <w n="18.5">au</w> <w n="18.6">contraire</w> <w n="18.7">il</w> <w n="18.8">goûtait</w> <w n="18.9">l</w>’<w n="18.10">espérance</w>,</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Tous</w> <w n="19.2">deux</w> <w n="19.3">en</w> <w n="19.4">jouissaient</w> <w n="19.5">d</w>’<w n="19.6">avance</w>.</l>
						<l n="20" num="1.20"><w n="20.1">Tel</w> <w n="20.2">était</w> <w n="20.3">leur</w> <w n="20.4">destin</w>, <w n="20.5">lorsqu</w>’<w n="20.6">un</w> <w n="20.7">jour</w>, <w n="20.8">jour</w> <w n="20.9">affreux</w> !</l>
						<l n="21" num="1.21"><w n="21.1">Le</w> <w n="21.2">lapin</w>, <w n="21.3">pour</w> <w n="21.4">dîner</w> <w n="21.5">venant</w> <w n="21.6">chez</w> <w n="21.7">la</w> <w n="21.8">sarcelle</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Ne</w> <w n="22.2">la</w> <w n="22.3">retrouve</w> <w n="22.4">plus</w> : <w n="22.5">inquiet</w>, <w n="22.6">il</w> <w n="22.7">l</w>’<w n="22.8">appelle</w> ;</l>
						<l n="23" num="1.23"><w n="23.1">Personne</w> <w n="23.2">ne</w> <w n="23.3">répond</w> <w n="23.4">à</w> <w n="23.5">ses</w> <w n="23.6">cris</w> <w n="23.7">douloureux</w>.</l>
						<l n="24" num="1.24"><w n="24.1">Le</w> <w n="24.2">lapin</w>, <w n="24.3">de</w> <w n="24.4">frayeur</w> <w n="24.5">l</w>’<w n="24.6">âme</w> <w n="24.7">toute</w> <w n="24.8">saisie</w>,</l>
						<l n="25" num="1.25"><w n="25.1">Va</w>, <w n="25.2">vient</w>, <w n="25.3">fait</w> <w n="25.4">mille</w> <w n="25.5">tours</w>, <w n="25.6">cherche</w> <w n="25.7">dans</w> <w n="25.8">les</w> <w n="25.9">roseaux</w>,</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">S</w>’<w n="26.2">incline</w> <w n="26.3">par</w>-<w n="26.4">dessus</w> <w n="26.5">les</w> <w n="26.6">flots</w>,</l>
						<l n="27" num="1.27"><w n="27.1">Et</w> <w n="27.2">voudrait</w> <w n="27.3">s</w>’<w n="27.4">y</w> <w n="27.5">plonger</w> <w n="27.6">pour</w> <w n="27.7">trouver</w> <w n="27.8">son</w> <w n="27.9">amie</w>.</l>
						<l n="28" num="1.28"><w n="28.1">Hélas</w> ! <w n="28.2">s</w>’<w n="28.3">écriait</w>-<w n="28.4">il</w>, <w n="28.5">m</w>’<w n="28.6">entends</w>-<w n="28.7">tu</w> ? <w n="28.8">Réponds</w>-<w n="28.9">moi</w>,</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">Ma</w> <w n="29.2">sœur</w>, <w n="29.3">ma</w> <w n="29.4">compagne</w> <w n="29.5">chérie</w> ;</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">Ne</w> <w n="30.2">prolonge</w> <w n="30.3">pas</w> <w n="30.4">mon</w> <w n="30.5">effroi</w> :</l>
						<l n="31" num="1.31"><w n="31.1">Encor</w> <w n="31.2">quelques</w> <w n="31.3">momens</w>, <w n="31.4">c</w>’<w n="31.5">en</w> <w n="31.6">est</w> <w n="31.7">fait</w> <w n="31.8">de</w> <w n="31.9">ma</w> <w n="31.10">vie</w> ;</l>
						<l n="32" num="1.32"><w n="32.1">J</w>’<w n="32.2">aime</w> <w n="32.3">mieux</w> <w n="32.4">expirer</w> <w n="32.5">que</w> <w n="32.6">de</w> <w n="32.7">trembler</w> <w n="32.8">pour</w> <w n="32.9">toi</w>.</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space><w n="33.1">Disant</w> <w n="33.2">ces</w> <w n="33.3">mots</w>, <w n="33.4">il</w> <w n="33.5">court</w>, <w n="33.6">il</w> <w n="33.7">pleure</w>,</l>
						<l n="34" num="1.34"><space unit="char" quantity="8"></space><w n="34.1">Et</w>, <w n="34.2">s</w>’<w n="34.3">avançant</w> <w n="34.4">le</w> <w n="34.5">long</w> <w n="34.6">de</w> <w n="34.7">l</w>’<w n="34.8">eau</w>,</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space><w n="35.1">Arrive</w> <w n="35.2">enfin</w> <w n="35.3">près</w> <w n="35.4">du</w> <w n="35.5">château</w></l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space><w n="36.1">Où</w> <w n="36.2">le</w> <w n="36.3">seigneur</w> <w n="36.4">du</w> <w n="36.5">lieu</w> <w n="36.6">demeure</w>.</l>
						<l n="37" num="1.37"><space unit="char" quantity="8"></space><w n="37.1">Là</w>, <w n="37.2">notre</w> <w n="37.3">désolé</w> <w n="37.4">lapin</w></l>
						<l n="38" num="1.38"><space unit="char" quantity="8"></space><w n="38.1">Se</w> <w n="38.2">trouve</w> <w n="38.3">au</w> <w n="38.4">milieu</w> <w n="38.5">d</w>’<w n="38.6">un</w> <w n="38.7">parterre</w>,</l>
						<l n="39" num="1.39"><space unit="char" quantity="8"></space><w n="39.1">Et</w> <w n="39.2">voit</w> <w n="39.3">une</w> <w n="39.4">grande</w> <w n="39.5">volière</w></l>
						<l n="40" num="1.40"><w n="40.1">Où</w> <w n="40.2">mille</w> <w n="40.3">oiseaux</w> <w n="40.4">divers</w> <w n="40.5">volaient</w> <w n="40.6">sur</w> <w n="40.7">un</w> <w n="40.8">bassin</w>.</l>
						<l n="41" num="1.41"><space unit="char" quantity="8"></space><w n="41.1">L</w>’<w n="41.2">amitié</w> <w n="41.3">donne</w> <w n="41.4">du</w> <w n="41.5">courage</w>.</l>
						<l n="42" num="1.42"><w n="42.1">Notre</w> <w n="42.2">ami</w>, <w n="42.3">sans</w> <w n="42.4">rien</w> <w n="42.5">craindre</w>, <w n="42.6">approche</w> <w n="42.7">du</w> <w n="42.8">grillage</w>,</l>
						<l n="43" num="1.43"><w n="43.1">Regarde</w> <w n="43.2">et</w> <w n="43.3">reconnaît</w>… <w n="43.4">ô</w> <w n="43.5">tendresse</w> ! <w n="43.6">ô</w> <w n="43.7">bonheur</w> !</l>
						<l n="44" num="1.44"><w n="44.1">La</w> <w n="44.2">sarcelle</w> : <w n="44.3">aussitôt</w> <w n="44.4">il</w> <w n="44.5">pousse</w> <w n="44.6">un</w> <w n="44.7">cri</w> <w n="44.8">de</w> <w n="44.9">joie</w>,</l>
						<l n="45" num="1.45"><w n="45.1">Et</w>, <w n="45.2">sans</w> <w n="45.3">perdre</w> <w n="45.4">de</w> <w n="45.5">temps</w> <w n="45.6">à</w> <w n="45.7">consoler</w> <w n="45.8">sa</w> <w n="45.9">sœur</w>,</l>
						<l n="46" num="1.46"><space unit="char" quantity="8"></space><w n="46.1">De</w> <w n="46.2">ses</w> <w n="46.3">quatre</w> <w n="46.4">pieds</w> <w n="46.5">il</w> <w n="46.6">s</w>’<w n="46.7">emploie</w></l>
						<l n="47" num="1.47"><space unit="char" quantity="8"></space><w n="47.1">A</w> <w n="47.2">creuser</w> <w n="47.3">un</w> <w n="47.4">secret</w> <w n="47.5">chemin</w></l>
						<l n="48" num="1.48"><w n="48.1">Pour</w> <w n="48.2">joindre</w> <w n="48.3">son</w> <w n="48.4">amie</w>, <w n="48.5">et</w>, <w n="48.6">par</w> <w n="48.7">ce</w> <w n="48.8">souterrain</w>,</l>
						<l n="49" num="1.49"><w n="49.1">Le</w> <w n="49.2">lapin</w> <w n="49.3">tout</w>-<w n="49.4">à</w>-<w n="49.5">coup</w> <w n="49.6">entre</w> <w n="49.7">dans</w> <w n="49.8">la</w> <w n="49.9">volière</w>,</l>
						<l n="50" num="1.50"><w n="50.1">Comme</w> <w n="50.2">un</w> <w n="50.3">mineur</w> <w n="50.4">qui</w> <w n="50.5">prend</w> <w n="50.6">une</w> <w n="50.7">place</w> <w n="50.8">de</w> <w n="50.9">guerre</w>.</l>
						<l n="51" num="1.51"><w n="51.1">Les</w> <w n="51.2">oiseaux</w> <w n="51.3">effrayés</w> <w n="51.4">se</w> <w n="51.5">pressent</w> <w n="51.6">en</w> <w n="51.7">fuyant</w>.</l>
						<l n="52" num="1.52"><w n="52.1">Lui</w> <w n="52.2">court</w> <w n="52.3">à</w> <w n="52.4">la</w> <w n="52.5">sarcelle</w>, <w n="52.6">il</w> <w n="52.7">l</w>’<w n="52.8">entraîne</w> <w n="52.9">à</w> <w n="52.10">l</w>’<w n="52.11">instant</w></l>
						<l n="53" num="1.53"><w n="53.1">Dans</w> <w n="53.2">son</w> <w n="53.3">obscur</w> <w n="53.4">sentier</w>, <w n="53.5">la</w> <w n="53.6">conduit</w> <w n="53.7">sous</w> <w n="53.8">la</w> <w n="53.9">terre</w>,</l>
						<l n="54" num="1.54"><w n="54.1">Et</w>, <w n="54.2">la</w> <w n="54.3">rendant</w> <w n="54.4">au</w> <w n="54.5">jour</w>, <w n="54.6">il</w> <w n="54.7">est</w> <w n="54.8">prêt</w> <w n="54.9">à</w> <w n="54.10">mourir</w></l>
						<l n="55" num="1.55"><space unit="char" quantity="18"></space><w n="55.1">De</w> <w n="55.2">plaisir</w>.</l>
						<l n="56" num="1.56"><w n="56.1">Quel</w> <w n="56.2">moment</w> <w n="56.3">pour</w> <w n="56.4">tous</w> <w n="56.5">deux</w> ! <w n="56.6">Que</w> <w n="56.7">ne</w> <w n="56.8">sais</w>-<w n="56.9">je</w> <w n="56.10">le</w> <w n="56.11">peindre</w></l>
						<l n="57" num="1.57"><space unit="char" quantity="8"></space><w n="57.1">Comme</w> <w n="57.2">je</w> <w n="57.3">saurais</w> <w n="57.4">le</w> <w n="57.5">sentir</w> !</l>
						<l n="58" num="1.58"><w n="58.1">Nos</w> <w n="58.2">bons</w> <w n="58.3">amis</w> <w n="58.4">croyaient</w> <w n="58.5">n</w>’<w n="58.6">avoir</w> <w n="58.7">plus</w> <w n="58.8">rien</w> <w n="58.9">à</w> <w n="58.10">craindre</w> ;</l>
						<l n="59" num="1.59"><w n="59.1">Ils</w> <w n="59.2">n</w>’<w n="59.3">étaient</w> <w n="59.4">pas</w> <w n="59.5">au</w> <w n="59.6">bout</w>. <w n="59.7">Le</w> <w n="59.8">maître</w> <w n="59.9">du</w> <w n="59.10">jardin</w>,</l>
						<l n="60" num="1.60"><w n="60.1">En</w> <w n="60.2">voyant</w> <w n="60.3">le</w> <w n="60.4">dégât</w> <w n="60.5">commis</w> <w n="60.6">dans</w> <w n="60.7">sa</w> <w n="60.8">volière</w>,</l>
						<l n="61" num="1.61"><w n="61.1">Jure</w> <w n="61.2">d</w>’<w n="61.3">exterminer</w> <w n="61.4">jusqu</w>’<w n="61.5">au</w> <w n="61.6">dernier</w> <w n="61.7">lapin</w> :</l>
						<l n="62" num="1.62"><w n="62.1">Mes</w> <w n="62.2">fusils</w> ! <w n="62.3">mes</w> <w n="62.4">furets</w> ! <w n="62.5">criait</w>-<w n="62.6">il</w> <w n="62.7">en</w> <w n="62.8">colère</w>.</l>
						<l n="63" num="1.63"><space unit="char" quantity="8"></space><w n="63.1">Aussitôt</w> <w n="63.2">fusils</w> <w n="63.3">et</w> <w n="63.4">furets</w></l>
						<l n="64" num="1.64"><space unit="char" quantity="18"></space><w n="64.1">Sont</w> <w n="64.2">tout</w> <w n="64.3">prêts</w>.</l>
						<l n="65" num="1.65"><w n="65.1">Les</w> <w n="65.2">gardes</w> <w n="65.3">et</w> <w n="65.4">les</w> <w n="65.5">chiens</w> <w n="65.6">vont</w> <w n="65.7">dans</w> <w n="65.8">les</w> <w n="65.9">jeunes</w> <w n="65.10">tailles</w>,</l>
						<l n="66" num="1.66"><space unit="char" quantity="8"></space><w n="66.1">Fouillant</w> <w n="66.2">les</w> <w n="66.3">terriers</w>, <w n="66.4">les</w> <w n="66.5">broussailles</w> ;</l>
						<l n="67" num="1.67"><w n="67.1">Tout</w> <w n="67.2">lapin</w> <w n="67.3">qui</w> <w n="67.4">paraît</w> <w n="67.5">trouve</w> <w n="67.6">un</w> <w n="67.7">affreux</w> <w n="67.8">trépas</w> :</l>
						<l n="68" num="1.68"><w n="68.1">Les</w> <w n="68.2">rivages</w> <w n="68.3">du</w> <w n="68.4">Styx</w> <w n="68.5">sont</w> <w n="68.6">bordés</w> <w n="68.7">de</w> <w n="68.8">leurs</w> <w n="68.9">manes</w> ;</l>
						<l n="69" num="1.69"><space unit="char" quantity="8"></space><w n="69.1">Dans</w> <w n="69.2">le</w> <w n="69.3">funeste</w> <w n="69.4">jour</w> <w n="69.5">de</w> <w n="69.6">Cannes</w></l>
						<l n="70" num="1.70"><space unit="char" quantity="8"></space><w n="70.1">On</w> <w n="70.2">mit</w> <w n="70.3">moins</w> <w n="70.4">de</w> <w n="70.5">romains</w> <w n="70.6">à</w> <w n="70.7">bas</w>.</l>
						<l n="71" num="1.71"><w n="71.1">La</w> <w n="71.2">nuit</w> <w n="71.3">vient</w> ; <w n="71.4">tant</w> <w n="71.5">de</w> <w n="71.6">sang</w> <w n="71.7">n</w>’<w n="71.8">a</w> <w n="71.9">point</w> <w n="71.10">éteint</w> <w n="71.11">la</w> <w n="71.12">rage</w></l>
						<l n="72" num="1.72"><w n="72.1">Du</w> <w n="72.2">seigneur</w>, <w n="72.3">qui</w> <w n="72.4">remet</w> <w n="72.5">au</w> <w n="72.6">lendemain</w> <w n="72.7">matin</w></l>
						<l n="73" num="1.73"><space unit="char" quantity="8"></space><w n="73.1">La</w> <w n="73.2">fin</w> <w n="73.3">de</w> <w n="73.4">l</w>’<w n="73.5">horrible</w> <w n="73.6">carnage</w>.</l>
						<l n="74" num="1.74"><space unit="char" quantity="8"></space><w n="74.1">Pendant</w> <w n="74.2">ce</w> <w n="74.3">temps</w>, <w n="74.4">notre</w> <w n="74.5">lapin</w>,</l>
						<l n="75" num="1.75"><w n="75.1">Tapi</w> <w n="75.2">sous</w> <w n="75.3">des</w> <w n="75.4">roseaux</w> <w n="75.5">auprès</w> <w n="75.6">de</w> <w n="75.7">la</w> <w n="75.8">sarcelle</w>,</l>
						<l n="76" num="1.76"><space unit="char" quantity="8"></space><w n="76.1">Attendait</w>, <w n="76.2">en</w> <w n="76.3">tremblant</w>, <w n="76.4">la</w> <w n="76.5">mort</w>,</l>
						<l n="77" num="1.77"><w n="77.1">Mais</w> <w n="77.2">conjurait</w> <w n="77.3">sa</w> <w n="77.4">sœur</w> <w n="77.5">de</w> <w n="77.6">fuir</w> <w n="77.7">à</w> <w n="77.8">l</w>’<w n="77.9">autre</w> <w n="77.10">bord</w></l>
						<l n="78" num="1.78"><space unit="char" quantity="8"></space><w n="78.1">Pour</w> <w n="78.2">ne</w> <w n="78.3">pas</w> <w n="78.4">mourir</w> <w n="78.5">devant</w> <w n="78.6">elle</w>.</l>
						<l n="79" num="1.79"><w n="79.1">Je</w> <w n="79.2">ne</w> <w n="79.3">te</w> <w n="79.4">quitte</w> <w n="79.5">point</w>, <w n="79.6">lui</w> <w n="79.7">répondait</w> <w n="79.8">l</w>’<w n="79.9">oiseau</w> ;</l>
						<l n="80" num="1.80"><w n="80.1">Nous</w> <w n="80.2">séparer</w> <w n="80.3">serait</w> <w n="80.4">la</w> <w n="80.5">mort</w> <w n="80.6">la</w> <w n="80.7">plus</w> <w n="80.8">cruelle</w>.</l>
						<l n="81" num="1.81"><space unit="char" quantity="8"></space><w n="81.1">Ah</w> ! <w n="81.2">si</w> <w n="81.3">tu</w> <w n="81.4">pouvais</w> <w n="81.5">passer</w> <w n="81.6">l</w>’<w n="81.7">eau</w> !</l>
						<l n="82" num="1.82"><w n="82.1">Pourquoi</w> <w n="82.2">pas</w> ? <w n="82.3">Attends</w>-<w n="82.4">moi</w>… <w n="82.5">La</w> <w n="82.6">sarcelle</w> <w n="82.7">le</w> <w n="82.8">quitte</w>,</l>
						<l n="83" num="1.83"><space unit="char" quantity="8"></space><w n="83.1">Et</w> <w n="83.2">revient</w> <w n="83.3">traînant</w> <w n="83.4">un</w> <w n="83.5">vieux</w> <w n="83.6">nid</w></l>
						<l n="84" num="1.84"><w n="84.1">Laissé</w> <w n="84.2">par</w> <w n="84.3">des</w> <w n="84.4">canards</w> : <w n="84.5">elle</w> <w n="84.6">l</w>’<w n="84.7">emplit</w> <w n="84.8">bien</w> <w n="84.9">vîte</w></l>
						<l n="85" num="1.85"><w n="85.1">De</w> <w n="85.2">feuilles</w> <w n="85.3">de</w> <w n="85.4">roseau</w>, <w n="85.5">les</w> <w n="85.6">presse</w>, <w n="85.7">les</w> <w n="85.8">unit</w></l>
						<l n="86" num="1.86"><w n="86.1">Des</w> <w n="86.2">pieds</w>, <w n="86.3">du</w> <w n="86.4">bec</w> ; <w n="86.5">en</w> <w n="86.6">forme</w> <w n="86.7">un</w> <w n="86.8">batelet</w> <w n="86.9">capable</w></l>
						<l n="87" num="1.87"><space unit="char" quantity="8"></space><w n="87.1">De</w> <w n="87.2">supporter</w> <w n="87.3">un</w> <w n="87.4">lourd</w> <w n="87.5">fardeau</w> ;</l>
						<l n="88" num="1.88"><space unit="char" quantity="8"></space><w n="88.1">Puis</w> <w n="88.2">elle</w> <w n="88.3">attache</w> <w n="88.4">à</w> <w n="88.5">ce</w> <w n="88.6">vaisseau</w></l>
						<l n="89" num="1.89"><space unit="char" quantity="4"></space><w n="89.1">Un</w> <w n="89.2">brin</w> <w n="89.3">de</w> <w n="89.4">jonc</w> <w n="89.5">qui</w> <w n="89.6">servira</w> <w n="89.7">de</w> <w n="89.8">cable</w>.</l>
						<l n="90" num="1.90"><space unit="char" quantity="8"></space><w n="90.1">Cela</w> <w n="90.2">fait</w>, <w n="90.3">et</w> <w n="90.4">le</w> <w n="90.5">bâtiment</w></l>
						<l n="91" num="1.91"><w n="91.1">Mis</w> <w n="91.2">à</w> <w n="91.3">l</w>’<w n="91.4">eau</w>, <w n="91.5">le</w> <w n="91.6">lapin</w> <w n="91.7">entre</w> <w n="91.8">tout</w> <w n="91.9">doucement</w></l>
						<l n="92" num="1.92"><w n="92.1">Dans</w> <w n="92.2">le</w> <w n="92.3">léger</w> <w n="92.4">esquif</w>, <w n="92.5">s</w>’<w n="92.6">assied</w> <w n="92.7">sur</w> <w n="92.8">son</w> <w n="92.9">derrière</w>,</l>
						<l n="93" num="1.93"><w n="93.1">Tandis</w> <w n="93.2">que</w> <w n="93.3">devant</w> <w n="93.4">lui</w> <w n="93.5">la</w> <w n="93.6">sarcelle</w> <w n="93.7">nageant</w></l>
						<l n="94" num="1.94"><w n="94.1">Tire</w> <w n="94.2">le</w> <w n="94.3">brin</w> <w n="94.4">de</w> <w n="94.5">jonc</w>, <w n="94.6">et</w> <w n="94.7">s</w>’<w n="94.8">en</w> <w n="94.9">va</w> <w n="94.10">dirigeant</w></l>
						<l n="95" num="1.95"><space unit="char" quantity="8"></space><w n="95.1">Cette</w> <w n="95.2">nef</w> <w n="95.3">à</w> <w n="95.4">son</w> <w n="95.5">cœur</w> <w n="95.6">si</w> <w n="95.7">chère</w>.</l>
						<l n="96" num="1.96"><w n="96.1">On</w> <w n="96.2">aborde</w>, <w n="96.3">on</w> <w n="96.4">débarque</w>, <w n="96.5">et</w> <w n="96.6">jugez</w> <w n="96.7">du</w> <w n="96.8">plaisir</w> !</l>
						<l n="97" num="1.97"><space unit="char" quantity="8"></space><w n="97.1">Non</w> <w n="97.2">loin</w> <w n="97.3">du</w> <w n="97.4">port</w> <w n="97.5">on</w> <w n="97.6">va</w> <w n="97.7">choisir</w></l>
						<l n="98" num="1.98"><w n="98.1">Un</w> <w n="98.2">asile</w> <w n="98.3">où</w>, <w n="98.4">coulant</w> <w n="98.5">des</w> <w n="98.6">jours</w> <w n="98.7">dignes</w> <w n="98.8">d</w>’<w n="98.9">envie</w>,</l>
						<l n="99" num="1.99"><space unit="char" quantity="8"></space><w n="99.1">Nos</w> <w n="99.2">bons</w> <w n="99.3">amis</w>, <w n="99.4">libres</w>, <w n="99.5">heureux</w>,</l>
						<l n="100" num="1.100"><space unit="char" quantity="8"></space><w n="100.1">Aimèrent</w> <w n="100.2">d</w>’<w n="100.3">autant</w> <w n="100.4">plus</w> <w n="100.5">la</w> <w n="100.6">vie</w></l>
						<l n="101" num="1.101"><space unit="char" quantity="8"></space><w n="101.1">Qu</w>’<w n="101.2">ils</w> <w n="101.3">se</w> <w n="101.4">la</w> <w n="101.5">devaient</w> <w n="101.6">tous</w> <w n="101.7">les</w> <w n="101.8">deux</w>.</l>
					</lg>
				</div></body></text></TEI>