<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="FLO85">
					<head type="number">FABLE XIX</head>
					<head type="main">Les deux Paysans et le Nuage</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Guillot</w>, <w n="1.2">disait</w> <w n="1.3">un</w> <w n="1.4">jour</w> <w n="1.5">Lucas</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">D</w>’<w n="2.2">une</w> <w n="2.3">voix</w> <w n="2.4">triste</w> <w n="2.5">et</w> <w n="2.6">lamentable</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Ne</w> <w n="3.2">vois</w>-<w n="3.3">tu</w> <w n="3.4">pas</w> <w n="3.5">venir</w> <w n="3.6">là</w> <w n="3.7">bas</w></l>
						<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">gros</w> <w n="4.3">nuage</w> <w n="4.4">noir</w> ? <w n="4.5">C</w>’<w n="4.6">est</w> <w n="4.7">la</w> <w n="4.8">marque</w> <w n="4.9">effroyable</w></l>
						<l n="5" num="1.5"><w n="5.1">Du</w> <w n="5.2">plus</w> <w n="5.3">grand</w> <w n="5.4">des</w> <w n="5.5">malheurs</w>. <w n="5.6">Pourquoi</w> ? <w n="5.7">répond</w> <w n="5.8">Guillot</w>.</l>
						<l n="6" num="1.6">— <w n="6.1">Pourquoi</w> ? <w n="6.2">Regarde</w> <w n="6.3">donc</w> : <w n="6.4">ou</w> <w n="6.5">je</w> <w n="6.6">ne</w> <w n="6.7">suis</w> <w n="6.8">qu</w>’<w n="6.9">un</w> <w n="6.10">sot</w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Ou</w> <w n="7.2">ce</w> <w n="7.3">nuage</w> <w n="7.4">est</w> <w n="7.5">de</w> <w n="7.6">la</w> <w n="7.7">grêle</w></l>
						<l n="8" num="1.8"><w n="8.1">Qui</w> <w n="8.2">va</w> <w n="8.3">tout</w> <w n="8.4">abymer</w> ; <w n="8.5">vigne</w>, <w n="8.6">avoine</w>, <w n="8.7">froment</w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Toute</w> <w n="9.2">la</w> <w n="9.3">récolte</w> <w n="9.4">nouvelle</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Sera</w> <w n="10.2">détruite</w> <w n="10.3">en</w> <w n="10.4">un</w> <w n="10.5">moment</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Il</w> <w n="11.2">ne</w> <w n="11.3">restera</w> <w n="11.4">rien</w>, <w n="11.5">le</w> <w n="11.6">village</w> <w n="11.7">en</w> <w n="11.8">ruine</w></l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">Dans</w> <w n="12.2">trois</w> <w n="12.3">mois</w> <w n="12.4">aura</w> <w n="12.5">la</w> <w n="12.6">famine</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Puis</w> <w n="13.2">la</w> <w n="13.3">peste</w> <w n="13.4">viendra</w>, <w n="13.5">puis</w> <w n="13.6">nous</w> <w n="13.7">périrons</w> <w n="13.8">tous</w>.</l>
						<l n="14" num="1.14"><w n="14.1">La</w> <w n="14.2">peste</w> ! <w n="14.3">dit</w> <w n="14.4">Guillot</w> : <w n="14.5">doucement</w>, <w n="14.6">calmez</w>-<w n="14.7">vous</w> ;</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">Je</w> <w n="15.2">ne</w> <w n="15.3">vois</w> <w n="15.4">point</w> <w n="15.5">cela</w>, <w n="15.6">compère</w> :</l>
						<l n="16" num="1.16"><w n="16.1">Et</w>, <w n="16.2">s</w>’<w n="16.3">il</w> <w n="16.4">faut</w> <w n="16.5">vous</w> <w n="16.6">parler</w> <w n="16.7">selon</w> <w n="16.8">mon</w> <w n="16.9">sentiment</w>,</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1">C</w>’<w n="17.2">est</w> <w n="17.3">que</w> <w n="17.4">je</w> <w n="17.5">vois</w> <w n="17.6">tout</w> <w n="17.7">le</w> <w n="17.8">contraire</w> ;</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Car</w> <w n="18.2">ce</w> <w n="18.3">nuage</w> <w n="18.4">assurément</w></l>
						<l n="19" num="1.19"><w n="19.1">Ne</w> <w n="19.2">porte</w> <w n="19.3">point</w> <w n="19.4">de</w> <w n="19.5">grêle</w>, <w n="19.6">il</w> <w n="19.7">porte</w> <w n="19.8">de</w> <w n="19.9">la</w> <w n="19.10">pluie</w>.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">La</w> <w n="20.2">terre</w> <w n="20.3">est</w> <w n="20.4">sèche</w> <w n="20.5">dès</w> <w n="20.6">long</w>-<w n="20.7">temps</w>,</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">Il</w> <w n="21.2">va</w> <w n="21.3">bien</w> <w n="21.4">arroser</w> <w n="21.5">nos</w> <w n="21.6">champs</w> ;</l>
						<l n="22" num="1.22"><w n="22.1">Toute</w> <w n="22.2">notre</w> <w n="22.3">récolte</w> <w n="22.4">en</w> <w n="22.5">doit</w> <w n="22.6">être</w> <w n="22.7">embellie</w>.</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">Nous</w> <w n="23.2">aurons</w> <w n="23.3">le</w> <w n="23.4">double</w> <w n="23.5">de</w> <w n="23.6">foin</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Moitié</w> <w n="24.2">plus</w> <w n="24.3">de</w> <w n="24.4">froment</w>, <w n="24.5">de</w> <w n="24.6">raisins</w> <w n="24.7">abondance</w> ;</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Nous</w> <w n="25.2">serons</w> <w n="25.3">tous</w> <w n="25.4">dans</w> <w n="25.5">l</w>’<w n="25.6">opulence</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Et</w> <w n="26.2">rien</w>, <w n="26.3">hors</w> <w n="26.4">les</w> <w n="26.5">tonneaux</w>, <w n="26.6">ne</w> <w n="26.7">nous</w> <w n="26.8">fera</w> <w n="26.9">besoin</w>.</l>
						<l n="27" num="1.27"><w n="27.1">C</w>’<w n="27.2">est</w> <w n="27.3">bien</w> <w n="27.4">voir</w> <w n="27.5">que</w> <w n="27.6">cela</w> ! <w n="27.7">dit</w> <w n="27.8">Lucas</w> <w n="27.9">en</w> <w n="27.10">colère</w>.</l>
						<l n="28" num="1.28"><w n="28.1">Mais</w> <w n="28.2">chacun</w> <w n="28.3">a</w> <w n="28.4">ses</w> <w n="28.5">yeux</w>, <w n="28.6">lui</w> <w n="28.7">répondit</w> <w n="28.8">Guillot</w>.</l>
						<l n="29" num="1.29">— <w n="29.1">Oh</w> ! <w n="29.2">puisqu</w>’<w n="29.3">il</w> <w n="29.4">est</w> <w n="29.5">ainsi</w>, <w n="29.6">je</w> <w n="29.7">ne</w> <w n="29.8">dirai</w> <w n="29.9">plus</w> <w n="29.10">mot</w> ;</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">Attendons</w> <w n="30.2">la</w> <w n="30.3">fin</w> <w n="30.4">de</w> <w n="30.5">l</w>’<w n="30.6">affaire</w> :</l>
						<l n="31" num="1.31"><w n="31.1">Rira</w> <w n="31.2">bien</w> <w n="31.3">qui</w> <w n="31.4">rira</w> <w n="31.5">le</w> <w n="31.6">dernier</w>. — <w n="31.7">Dieu</w> <w n="31.8">merci</w>,</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space><w n="32.1">Ce</w> <w n="32.2">n</w>’<w n="32.3">est</w> <w n="32.4">pas</w> <w n="32.5">moi</w> <w n="32.6">qui</w> <w n="32.7">pleure</w> <w n="32.8">ici</w>.</l>
						<l n="33" num="1.33"><w n="33.1">Ils</w> <w n="33.2">s</w>’<w n="33.3">échauffaient</w> <w n="33.4">tous</w> <w n="33.5">deux</w> ; <w n="33.6">déjà</w>, <w n="33.7">dans</w> <w n="33.8">leur</w> <w n="33.9">furie</w>,</l>
						<l n="34" num="1.34"><w n="34.1">Ils</w> <w n="34.2">allaient</w> <w n="34.3">se</w> <w n="34.4">gourmer</w>, <w n="34.5">lorsqu</w>’<w n="34.6">un</w> <w n="34.7">souffle</w> <w n="34.8">de</w> <w n="34.9">vent</w></l>
						<l n="35" num="1.35"><w n="35.1">Emporta</w> <w n="35.2">loin</w> <w n="35.3">de</w> <w n="35.4">là</w> <w n="35.5">le</w> <w n="35.6">nuage</w> <w n="35.7">effrayant</w> :</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space><w n="36.1">Ils</w> <w n="36.2">n</w>’<w n="36.3">eurent</w> <w n="36.4">ni</w> <w n="36.5">grêle</w> <w n="36.6">ni</w> <w n="36.7">pluie</w>.</l>
					</lg>
				</div></body></text></TEI>