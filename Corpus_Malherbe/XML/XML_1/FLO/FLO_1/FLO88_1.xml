<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="FLO88">
					<head type="number">FABLE XXII</head>
					<head type="main">Le Coq Fanfaron *</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Il</w> <w n="1.2">fait</w> <w n="1.3">bon</w> <w n="1.4">battre</w> <w n="1.5">un</w> <w n="1.6">glorieux</w> :</l>
						<l n="2" num="1.2"><w n="2.1">Des</w> <w n="2.2">revers</w> <w n="2.3">qu</w>’<w n="2.4">il</w> <w n="2.5">éprouve</w> <w n="2.6">il</w> <w n="2.7">est</w> <w n="2.8">toujours</w> <w n="2.9">joyeux</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Toujours</w> <w n="3.2">sa</w> <w n="3.3">vanité</w> <w n="3.4">trouve</w> <w n="3.5">dans</w> <w n="3.6">sa</w> <w n="3.7">défaite</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Un</w> <w n="4.2">moyen</w> <w n="4.3">d</w>’<w n="4.4">être</w> <w n="4.5">satisfaite</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Un</w> <w n="5.2">coq</w>, <w n="5.3">sans</w> <w n="5.4">force</w> <w n="5.5">et</w> <w n="5.6">sans</w> <w n="5.7">talent</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Jouissait</w>, <w n="6.2">on</w> <w n="6.3">ne</w> <w n="6.4">sait</w> <w n="6.5">comment</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">D</w>’<w n="7.2">une</w> <w n="7.3">certaine</w> <w n="7.4">renommée</w>.</l>
						<l n="8" num="2.4"><w n="8.1">Cela</w> <w n="8.2">se</w> <w n="8.3">voit</w>, <w n="8.4">dit</w>-<w n="8.5">on</w>, <w n="8.6">chez</w> <w n="8.7">la</w> <w n="8.8">gent</w> <w n="8.9">emplumée</w></l>
						<l n="9" num="2.5"><w n="9.1">Et</w> <w n="9.2">chez</w> <w n="9.3">d</w>’<w n="9.4">autres</w> <w n="9.5">encore</w>. <w n="9.6">Insolent</w> <w n="9.7">comme</w> <w n="9.8">un</w> <w n="9.9">sot</w>,</l>
						<l n="10" num="2.6"><w n="10.1">Notre</w> <w n="10.2">coq</w> <w n="10.3">traita</w> <w n="10.4">mal</w> <w n="10.5">un</w> <w n="10.6">poulet</w> <w n="10.7">de</w> <w n="10.8">mérite</w>.</l>
						<l n="11" num="2.7"><space unit="char" quantity="8"></space><w n="11.1">La</w> <w n="11.2">jeunesse</w> <w n="11.3">aisément</w> <w n="11.4">s</w>’<w n="11.5">irrite</w> ;</l>
						<l n="12" num="2.8"><w n="12.1">Le</w> <w n="12.2">poulet</w> <w n="12.3">offensé</w> <w n="12.4">le</w> <w n="12.5">provoque</w> <w n="12.6">aussitôt</w>,</l>
						<l n="13" num="2.9"><w n="13.1">Et</w> <w n="13.2">le</w> <w n="13.3">cou</w> <w n="13.4">tout</w> <w n="13.5">gonflé</w> <w n="13.6">sur</w> <w n="13.7">lui</w> <w n="13.8">se</w> <w n="13.9">précipite</w>.</l>
						<l n="14" num="2.10"><space unit="char" quantity="8"></space><w n="14.1">Dans</w> <w n="14.2">l</w>’<w n="14.3">instant</w> <w n="14.4">le</w> <w n="14.5">coq</w> <w n="14.6">orgueilleux</w></l>
						<l n="15" num="2.11"><w n="15.1">Est</w> <w n="15.2">battu</w>, <w n="15.3">déplumé</w>, <w n="15.4">reçoit</w> <w n="15.5">mainte</w> <w n="15.6">blessure</w> ;</l>
						<l n="16" num="2.12"><w n="16.1">Et</w>, <w n="16.2">si</w> <w n="16.3">l</w>’<w n="16.4">on</w> <w n="16.5">n</w>’<w n="16.6">eût</w> <w n="16.7">fini</w> <w n="16.8">ce</w> <w n="16.9">combat</w> <w n="16.10">dangereux</w>,</l>
						<l n="17" num="2.13"><space unit="char" quantity="8"></space><w n="17.1">Sa</w> <w n="17.2">mort</w> <w n="17.3">terminait</w> <w n="17.4">l</w>’<w n="17.5">aventure</w>.</l>
						<l n="18" num="2.14"><w n="18.1">Quand</w> <w n="18.2">le</w> <w n="18.3">poulet</w> <w n="18.4">fut</w> <w n="18.5">loin</w>, <w n="18.6">le</w> <w n="18.7">coq</w>, <w n="18.8">en</w> <w n="18.9">s</w>’<w n="18.10">épluchant</w>,</l>
						<l n="19" num="2.15"><w n="19.1">Disait</w> : <w n="19.2">Cet</w> <w n="19.3">enfant</w>-<w n="19.4">là</w> <w n="19.5">m</w>’<w n="19.6">a</w> <w n="19.7">montré</w> <w n="19.8">du</w> <w n="19.9">courage</w> ;</l>
						<l n="20" num="2.16"><space unit="char" quantity="8"></space><w n="20.1">J</w>’<w n="20.2">ai</w> <w n="20.3">beaucoup</w> <w n="20.4">ménagé</w> <w n="20.5">son</w> <w n="20.6">âge</w>,</l>
						<l n="21" num="2.17"><space unit="char" quantity="8"></space><w n="21.1">Mais</w> <w n="21.2">de</w> <w n="21.3">lui</w> <w n="21.4">je</w> <w n="21.5">suis</w> <w n="21.6">fort</w> <w n="21.7">content</w>.</l>
						<l n="22" num="2.18"><w n="22.1">Un</w> <w n="22.2">coq</w>, <w n="22.3">vieux</w> <w n="22.4">et</w> <w n="22.5">cassé</w>, <w n="22.6">témoin</w> <w n="22.7">de</w> <w n="22.8">cette</w> <w n="22.9">histoire</w>,</l>
						<l n="23" num="2.19"><space unit="char" quantity="8"></space><w n="23.1">La</w> <w n="23.2">répandit</w> <w n="23.3">et</w> <w n="23.4">s</w>’<w n="23.5">en</w> <w n="23.6">moqua</w>.</l>
						<l n="24" num="2.20"><space unit="char" quantity="8"></space><w n="24.1">Notre</w> <w n="24.2">fanfaron</w> <w n="24.3">l</w>’<w n="24.4">attaqua</w>,</l>
						<l n="25" num="2.21"><w n="25.1">Croyant</w> <w n="25.2">facilement</w> <w n="25.3">remporter</w> <w n="25.4">la</w> <w n="25.5">victoire</w>.</l>
						<l n="26" num="2.22"><w n="26.1">Le</w> <w n="26.2">brave</w> <w n="26.3">vétéran</w>, <w n="26.4">de</w> <w n="26.5">lui</w> <w n="26.6">trop</w> <w n="26.7">mal</w> <w n="26.8">connu</w>,</l>
						<l n="27" num="2.23"><w n="27.1">En</w> <w n="27.2">quatre</w> <w n="27.3">coups</w> <w n="27.4">de</w> <w n="27.5">bec</w> <w n="27.6">lui</w> <w n="27.7">partage</w> <w n="27.8">la</w> <w n="27.9">crête</w>,</l>
						<l n="28" num="2.24"><w n="28.1">Le</w> <w n="28.2">dépouille</w> <w n="28.3">en</w> <w n="28.4">entier</w> <w n="28.5">des</w> <w n="28.6">pieds</w> <w n="28.7">jusqu</w>’<w n="28.8">à</w> <w n="28.9">la</w> <w n="28.10">tête</w>,</l>
						<l n="29" num="2.25"><space unit="char" quantity="8"></space><w n="29.1">Et</w> <w n="29.2">le</w> <w n="29.3">laisse</w> <w n="29.4">là</w> <w n="29.5">presque</w> <w n="29.6">nu</w>.</l>
						<l n="30" num="2.26"><space unit="char" quantity="8"></space><w n="30.1">Alors</w> <w n="30.2">notre</w> <w n="30.3">coq</w>, <w n="30.4">sans</w> <w n="30.5">se</w> <w n="30.6">plaindre</w>,</l>
						<l n="31" num="2.27"><w n="31.1">Dit</w> : <w n="31.2">C</w>’<w n="31.3">est</w> <w n="31.4">un</w> <w n="31.5">bon</w> <w n="31.6">vieillard</w>, <w n="31.7">j</w>’<w n="31.8">en</w> <w n="31.9">ai</w> <w n="31.10">bien</w> <w n="31.11">peu</w> <w n="31.12">souffert</w> :</l>
						<l n="32" num="2.28"><space unit="char" quantity="8"></space><w n="32.1">Mais</w> <w n="32.2">je</w> <w n="32.3">le</w> <w n="32.4">trouve</w> <w n="32.5">encore</w> <w n="32.6">vert</w> ;</l>
						<l n="33" num="2.29"><w n="33.1">Et</w>, <w n="33.2">dans</w> <w n="33.3">son</w> <w n="33.4">jeune</w> <w n="33.5">temps</w>, <w n="33.6">il</w> <w n="33.7">devait</w> <w n="33.8">être</w> <w n="33.9">à</w> <w n="33.10">craindre</w>.</l>
					</lg>
				</div></body></text></TEI>