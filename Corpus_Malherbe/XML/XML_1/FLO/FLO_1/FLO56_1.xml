<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO56">
					<head type="number">FABLE XII</head>
					<head type="main">Les Enfans et les Perdreaux</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Deux</w> <w n="1.2">enfans</w> <w n="1.3">d</w>’<w n="1.4">un</w> <w n="1.5">fermier</w>, <w n="1.6">gentils</w>, <w n="1.7">espiègles</w>, <w n="1.8">beaux</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Mais</w> <w n="2.2">un</w> <w n="2.3">peu</w> <w n="2.4">gâtés</w> <w n="2.5">par</w> <w n="2.6">leur</w> <w n="2.7">père</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Cherchant</w> <w n="3.2">des</w> <w n="3.3">nids</w> <w n="3.4">dans</w> <w n="3.5">leur</w> <w n="3.6">enclos</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Trouvèrent</w> <w n="4.2">de</w> <w n="4.3">petits</w> <w n="4.4">perdreaux</w></l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Qui</w> <w n="5.2">voletaient</w> <w n="5.3">après</w> <w n="5.4">leur</w> <w n="5.5">mère</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Vous</w> <w n="6.2">jugez</w> <w n="6.3">de</w> <w n="6.4">leur</w> <w n="6.5">joie</w>, <w n="6.6">et</w> <w n="6.7">comment</w> <w n="6.8">mes</w> <w n="6.9">bambins</w></l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">A</w> <w n="7.2">la</w> <w n="7.3">troupe</w> <w n="7.4">qui</w> <w n="7.5">s</w>’<w n="7.6">éparpille</w></l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Vont</w> <w n="8.2">par</w>-<w n="8.3">tout</w> <w n="8.4">couper</w> <w n="8.5">les</w> <w n="8.6">chemins</w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Et</w> <w n="9.2">n</w>’<w n="9.3">ont</w> <w n="9.4">pas</w> <w n="9.5">assez</w> <w n="9.6">de</w> <w n="9.7">leurs</w> <w n="9.8">mains</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Pour</w> <w n="10.2">prendre</w> <w n="10.3">la</w> <w n="10.4">pauvre</w> <w n="10.5">famille</w> !</l>
						<l n="11" num="1.11"><w n="11.1">La</w> <w n="11.2">perdrix</w>, <w n="11.3">traînant</w> <w n="11.4">l</w>’<w n="11.5">aile</w>, <w n="11.6">appelant</w> <w n="11.7">ses</w> <w n="11.8">petits</w>,</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">Tourne</w> <w n="12.2">en</w> <w n="12.3">vain</w>, <w n="12.4">voltige</w>, <w n="12.5">s</w>’<w n="12.6">approche</w> ;</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">Déjà</w> <w n="13.2">mes</w> <w n="13.3">jeunes</w> <w n="13.4">étourdis</w></l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">Ont</w> <w n="14.2">toute</w> <w n="14.3">sa</w> <w n="14.4">couvée</w> <w n="14.5">en</w> <w n="14.6">poche</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Ils</w> <w n="15.2">veulent</w> <w n="15.3">partager</w> <w n="15.4">comme</w> <w n="15.5">de</w> <w n="15.6">bons</w> <w n="15.7">amis</w> ;</l>
						<l n="16" num="1.16"><w n="16.1">Chacun</w> <w n="16.2">en</w> <w n="16.3">garde</w> <w n="16.4">six</w>, <w n="16.5">il</w> <w n="16.6">en</w> <w n="16.7">reste</w> <w n="16.8">un</w> <w n="16.9">treizième</w> :</l>
						<l n="17" num="1.17"><space unit="char" quantity="4"></space><w n="17.1">L</w>’<w n="17.2">aîné</w> <w n="17.3">le</w> <w n="17.4">veut</w>, <w n="17.5">l</w>’<w n="17.6">autre</w> <w n="17.7">le</w> <w n="17.8">veut</w> <w n="17.9">aussi</w>.</l>
						<l n="18" num="1.18">— <w n="18.1">Tirons</w> <w n="18.2">au</w> <w n="18.3">doigt</w> <w n="18.4">mouillé</w>. — <w n="18.5">Parbleu</w> <w n="18.6">non</w>. — <w n="18.7">Parbleu</w> <w n="18.8">si</w>.</l>
						<l n="19" num="1.19">— <w n="19.1">Cède</w>, <w n="19.2">ou</w> <w n="19.3">bien</w> <w n="19.4">tu</w> <w n="19.5">verras</w>. — <w n="19.6">Mais</w> <w n="19.7">tu</w> <w n="19.8">verras</w> <w n="19.9">toi</w>-<w n="19.10">même</w>.</l>
						<l n="20" num="1.20"><w n="20.1">De</w> <w n="20.2">propos</w> <w n="20.3">en</w> <w n="20.4">propos</w>, <w n="20.5">l</w>’<w n="20.6">aîné</w>, <w n="20.7">peu</w> <w n="20.8">patient</w>,</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">Jette</w> <w n="21.2">à</w> <w n="21.3">la</w> <w n="21.4">tête</w> <w n="21.5">de</w> <w n="21.6">son</w> <w n="21.7">frère</w></l>
						<l n="22" num="1.22"><w n="22.1">Le</w> <w n="22.2">perdreau</w> <w n="22.3">disputé</w>. <w n="22.4">Le</w> <w n="22.5">cadet</w>, <w n="22.6">en</w> <w n="22.7">colère</w>,</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">D</w>’<w n="23.2">un</w> <w n="23.3">des</w> <w n="23.4">siens</w> <w n="23.5">riposte</w> <w n="23.6">à</w> <w n="23.7">l</w>’<w n="23.8">instant</w>.</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">L</w>’<w n="24.2">aîné</w> <w n="24.3">recommence</w> <w n="24.4">d</w>’<w n="24.5">autant</w> ;</l>
						<l n="25" num="1.25"><w n="25.1">Et</w> <w n="25.2">ce</w> <w n="25.3">jeu</w> <w n="25.4">qui</w> <w n="25.5">leur</w> <w n="25.6">plaît</w> <w n="25.7">couvre</w> <w n="25.8">autour</w> <w n="25.9">d</w>’<w n="25.10">eux</w> <w n="25.11">la</w> <w n="25.12">terre</w></l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">De</w> <w n="26.2">pauvres</w> <w n="26.3">perdreaux</w> <w n="26.4">palpitans</w>.</l>
						<l n="27" num="1.27"><w n="27.1">Le</w> <w n="27.2">fermier</w>, <w n="27.3">qui</w> <w n="27.4">passait</w> <w n="27.5">en</w> <w n="27.6">revenant</w> <w n="27.7">des</w> <w n="27.8">champs</w>,</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space><w n="28.1">Voit</w> <w n="28.2">ce</w> <w n="28.3">spectacle</w> <w n="28.4">sanguinaire</w>,</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">Accourt</w>, <w n="29.2">et</w> <w n="29.3">dit</w> <w n="29.4">à</w> <w n="29.5">ses</w> <w n="29.6">enfans</w> :</l>
						<l n="30" num="1.30"><w n="30.1">Comment</w> <w n="30.2">donc</w> ! <w n="30.3">petits</w> <w n="30.4">rois</w>, <w n="30.5">vos</w> <w n="30.6">discordes</w> <w n="30.7">cruelles</w></l>
						<l n="31" num="1.31"><w n="31.1">Font</w> <w n="31.2">que</w> <w n="31.3">tant</w> <w n="31.4">d</w>’<w n="31.5">innocens</w> <w n="31.6">expirent</w> <w n="31.7">par</w> <w n="31.8">vos</w> <w n="31.9">coups</w> !</l>
						<l n="32" num="1.32"><w n="32.1">De</w> <w n="32.2">quel</w> <w n="32.3">droit</w>, <w n="32.4">s</w>’<w n="32.5">il</w> <w n="32.6">vous</w> <w n="32.7">plaît</w>, <w n="32.8">dans</w> <w n="32.9">vos</w> <w n="32.10">tristes</w> <w n="32.11">querelles</w>,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space><w n="33.1">Faut</w>-<w n="33.2">il</w> <w n="33.3">que</w> <w n="33.4">l</w>’<w n="33.5">on</w> <w n="33.6">meure</w> <w n="33.7">pour</w> <w n="33.8">vous</w> ?</l>
					</lg>
				</div></body></text></TEI>