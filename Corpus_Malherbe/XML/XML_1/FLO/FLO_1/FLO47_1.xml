<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO47">
					<head type="number">FABLE III</head>
					<head type="main">Le Sanglier et les Rossignols</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Un</w> <w n="1.2">homme</w> <w n="1.3">riche</w>, <w n="1.4">sot</w> <w n="1.5">et</w> <w n="1.6">vain</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qualités</w> <w n="2.2">qui</w> <w n="2.3">parfois</w> <w n="2.4">marchent</w> <w n="2.5">de</w> <w n="2.6">compagnie</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Croyait</w> <w n="3.2">pour</w> <w n="3.3">tous</w> <w n="3.4">les</w> <w n="3.5">arts</w> <w n="3.6">avoir</w> <w n="3.7">un</w> <w n="3.8">goût</w> <w n="3.9">divin</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">pensait</w> <w n="4.3">que</w> <w n="4.4">son</w> <w n="4.5">or</w> <w n="4.6">lui</w> <w n="4.7">donnait</w> <w n="4.8">du</w> <w n="4.9">génie</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Chaque</w> <w n="5.2">jour</w> <w n="5.3">à</w> <w n="5.4">sa</w> <w n="5.5">table</w> <w n="5.6">on</w> <w n="5.7">voyait</w> <w n="5.8">réunis</w></l>
						<l n="6" num="1.6"><w n="6.1">Peintres</w>, <w n="6.2">sculpteurs</w>, <w n="6.3">savans</w>, <w n="6.4">artistes</w>, <w n="6.5">beaux</w> <w n="6.6">esprits</w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Qui</w> <w n="7.2">lui</w> <w n="7.3">prodiguaient</w> <w n="7.4">les</w> <w n="7.5">hommages</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Lui</w> <w n="8.2">montraient</w> <w n="8.3">des</w> <w n="8.4">dessins</w>, <w n="8.5">lui</w> <w n="8.6">lisaient</w> <w n="8.7">des</w> <w n="8.8">ouvrages</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Écoutaient</w> <w n="9.2">les</w> <w n="9.3">conseils</w> <w n="9.4">qu</w>’<w n="9.5">il</w> <w n="9.6">daignait</w> <w n="9.7">leur</w> <w n="9.8">donner</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">l</w>’<w n="10.3">appelaient</w> <w n="10.4">Mécène</w> <w n="10.5">en</w> <w n="10.6">mangeant</w> <w n="10.7">son</w> <w n="10.8">dîner</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Se</w> <w n="11.2">promenant</w> <w n="11.3">un</w> <w n="11.4">soir</w> <w n="11.5">dans</w> <w n="11.6">son</w> <w n="11.7">parc</w> <w n="11.8">solitaire</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Suivi</w> <w n="12.2">d</w>’<w n="12.3">un</w> <w n="12.4">jardinier</w>, <w n="12.5">homme</w> <w n="12.6">instruit</w> <w n="12.7">et</w> <w n="12.8">de</w> <w n="12.9">sens</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Il</w> <w n="13.2">vit</w> <w n="13.3">un</w> <w n="13.4">sanglier</w> <w n="13.5">qui</w> <w n="13.6">labourait</w> <w n="13.7">la</w> <w n="13.8">terre</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Comme</w> <w n="14.2">ils</w> <w n="14.3">font</w> <w n="14.4">quelquefois</w> <w n="14.5">pour</w> <w n="14.6">aiguiser</w> <w n="14.7">leurs</w> <w n="14.8">dents</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Autour</w> <w n="15.2">du</w> <w n="15.3">sanglier</w>, <w n="15.4">les</w> <w n="15.5">merles</w>, <w n="15.6">les</w> <w n="15.7">fauvettes</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Sur</w>-<w n="16.2">tout</w> <w n="16.3">les</w> <w n="16.4">rossignols</w>, <w n="16.5">voltigeant</w>, <w n="16.6">s</w>’<w n="16.7">arrêtant</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Répétaient</w> <w n="17.2">à</w> <w n="17.3">l</w>’<w n="17.4">envi</w> <w n="17.5">leurs</w> <w n="17.6">douces</w> <w n="17.7">chansonnettes</w>,</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Et</w> <w n="18.2">le</w> <w n="18.3">suivaient</w> <w n="18.4">toujours</w> <w n="18.5">chantant</w>.</l>
						<l n="19" num="1.19"><w n="19.1">L</w>’<w n="19.2">animal</w> <w n="19.3">écoutait</w> <w n="19.4">l</w>’<w n="19.5">harmonieux</w> <w n="19.6">ramage</w></l>
						<l n="20" num="1.20"><w n="20.1">Avec</w> <w n="20.2">la</w> <w n="20.3">gravité</w> <w n="20.4">d</w>’<w n="20.5">un</w> <w n="20.6">docte</w> <w n="20.7">connaisseur</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Baissait</w> <w n="21.2">parfois</w> <w n="21.3">la</w> <w n="21.4">hure</w> <w n="21.5">en</w> <w n="21.6">signe</w> <w n="21.7">de</w> <w n="21.8">faveur</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Ou</w> <w n="22.2">bien</w>, <w n="22.3">la</w> <w n="22.4">secouant</w>, <w n="22.5">refusait</w> <w n="22.6">son</w> <w n="22.7">suffrage</w>.</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">Qu</w>’<w n="23.2">est</w> <w n="23.3">ceci</w> ? <w n="23.4">dit</w> <w n="23.5">le</w> <w n="23.6">financier</w> :</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Comment</w> ! <w n="24.2">les</w> <w n="24.3">chantres</w> <w n="24.4">du</w> <w n="24.5">bocage</w></l>
						<l n="25" num="1.25"><w n="25.1">Pour</w> <w n="25.2">leur</w> <w n="25.3">juge</w> <w n="25.4">ont</w> <w n="25.5">choisi</w> <w n="25.6">cet</w> <w n="25.7">animal</w> <w n="25.8">sauvage</w> !</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Nenni</w>, <w n="26.2">répond</w> <w n="26.3">le</w> <w n="26.4">jardinier</w> :</l>
						<l n="27" num="1.27"><w n="27.1">De</w> <w n="27.2">la</w> <w n="27.3">terre</w> <w n="27.4">par</w> <w n="27.5">lui</w> <w n="27.6">fraîchement</w> <w n="27.7">labourée</w>,</l>
						<l n="28" num="1.28"><w n="28.1">Sont</w> <w n="28.2">sortis</w> <w n="28.3">plusieurs</w> <w n="28.4">vers</w>, <w n="28.5">excellente</w> <w n="28.6">curée</w></l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">Qui</w> <w n="29.2">seule</w> <w n="29.3">attire</w> <w n="29.4">ces</w> <w n="29.5">oiseaux</w> :</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">Ils</w> <w n="30.2">ne</w> <w n="30.3">se</w> <w n="30.4">tiennent</w> <w n="30.5">à</w> <w n="30.6">sa</w> <w n="30.7">suite</w></l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space><w n="31.1">Que</w> <w n="31.2">pour</w> <w n="31.3">manger</w> <w n="31.4">ces</w> <w n="31.5">vermisseaux</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Et</w> <w n="32.2">l</w>’<w n="32.3">imbécille</w> <w n="32.4">croit</w> <w n="32.5">que</w> <w n="32.6">c</w>’<w n="32.7">est</w> <w n="32.8">pour</w> <w n="32.9">son</w> <w n="32.10">mérite</w>.</l>
					</lg>
				</div></body></text></TEI>