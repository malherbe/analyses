<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="FLO17">
					<head type="number">FABLE XVII</head>
					<head type="main">Le jeune Homme et le Vieillard</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">grace</w>, <w n="1.3">apprenez</w>-<w n="1.4">moi</w> <w n="1.5">comment</w> <w n="1.6">l</w>’<w n="1.7">on</w> <w n="1.8">fait</w> <w n="1.9">fortune</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Demandait</w> <w n="2.2">à</w> <w n="2.3">son</w> <w n="2.4">père</w> <w n="2.5">un</w> <w n="2.6">jeune</w> <w n="2.7">ambitieux</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">est</w>, <w n="3.3">dit</w> <w n="3.4">le</w> <w n="3.5">vieillard</w>, <w n="3.6">un</w> <w n="3.7">chemin</w> <w n="3.8">glorieux</w>,</l>
						<l n="4" num="1.4"><w n="4.1">C</w>’<w n="4.2">est</w> <w n="4.3">de</w> <w n="4.4">se</w> <w n="4.5">rendre</w> <w n="4.6">utile</w> <w n="4.7">à</w> <w n="4.8">la</w> <w n="4.9">cause</w> <w n="4.10">commune</w>,</l>
						<l n="5" num="1.5"><w n="5.1">De</w> <w n="5.2">prodiguer</w> <w n="5.3">ses</w> <w n="5.4">jours</w>, <w n="5.5">ses</w> <w n="5.6">veilles</w>, <w n="5.7">ses</w> <w n="5.8">talens</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Au</w> <w n="6.2">service</w> <w n="6.3">de</w> <w n="6.4">la</w> <w n="6.5">patrie</w>.</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space>— <w n="7.1">Oh</w> ! <w n="7.2">trop</w> <w n="7.3">pénible</w> <w n="7.4">est</w> <w n="7.5">cette</w> <w n="7.6">vie</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Je</w> <w n="8.2">veux</w> <w n="8.3">des</w> <w n="8.4">moyens</w> <w n="8.5">moins</w> <w n="8.6">brillans</w>.</l>
						<l n="9" num="1.9">— <w n="9.1">Il</w> <w n="9.2">en</w> <w n="9.3">est</w> <w n="9.4">de</w> <w n="9.5">plus</w> <w n="9.6">sûrs</w>, <w n="9.7">l</w>’<w n="9.8">intrigue</w>… — <w n="9.9">Elle</w> <w n="9.10">est</w> <w n="9.11">trop</w> <w n="9.12">vile</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Sans</w> <w n="10.2">vice</w> <w n="10.3">et</w> <w n="10.4">sans</w> <w n="10.5">travail</w> <w n="10.6">je</w> <w n="10.7">voudrais</w> <w n="10.8">m</w>’<w n="10.9">enrichir</w>.</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space>— <w n="11.1">Eh</w> <w n="11.2">bien</w> ! <w n="11.3">sois</w> <w n="11.4">un</w> <w n="11.5">simple</w> <w n="11.6">imbécille</w>,</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">J</w>’<w n="12.2">en</w> <w n="12.3">ai</w> <w n="12.4">vu</w> <w n="12.5">beaucoup</w> <w n="12.6">réussir</w>.</l>
					</lg>
				</div></body></text></TEI>