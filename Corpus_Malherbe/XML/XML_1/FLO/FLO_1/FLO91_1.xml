<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE CINQUIÈME</head><div type="poem" key="FLO91">
					<head type="number">FABLE III</head>
					<head type="main">Le Procès des deux Renards</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Que</w> <w n="1.2">je</w> <w n="1.3">hais</w> <w n="1.4">cet</w> <w n="1.5">art</w> <w n="1.6">de</w> <w n="1.7">pédant</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Cette</w> <w n="2.2">logique</w> <w n="2.3">captieuse</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">d</w>’<w n="3.3">une</w> <w n="3.4">chose</w> <w n="3.5">claire</w> <w n="3.6">en</w> <w n="3.7">fait</w> <w n="3.8">une</w> <w n="3.9">douteuse</w>,</l>
						<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2">un</w> <w n="4.3">principe</w> <w n="4.4">erroné</w> <w n="4.5">tire</w> <w n="4.6">subtilement</w></l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Une</w> <w n="5.2">conséquence</w> <w n="5.3">trompeuse</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Et</w> <w n="6.2">raisonne</w> <w n="6.3">en</w> <w n="6.4">déraisonnant</w> !</l>
						<l n="7" num="1.7"><w n="7.1">Les</w> <w n="7.2">Grecs</w> <w n="7.3">ont</w> <w n="7.4">inventé</w> <w n="7.5">cette</w> <w n="7.6">belle</w> <w n="7.7">manière</w> :</l>
						<l n="8" num="1.8"><w n="8.1">Ils</w> <w n="8.2">ont</w> <w n="8.3">fait</w> <w n="8.4">plus</w> <w n="8.5">de</w> <w n="8.6">mal</w> <w n="8.7">qu</w>’<w n="8.8">ils</w> <w n="8.9">ne</w> <w n="8.10">croyaient</w> <w n="8.11">en</w> <w n="8.12">faire</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Que</w> <w n="9.2">Dieu</w> <w n="9.3">leur</w> <w n="9.4">donne</w> <w n="9.5">paix</w> ! <w n="9.6">Il</w> <w n="9.7">s</w>’<w n="9.8">agit</w> <w n="9.9">d</w>’<w n="9.10">un</w> <w n="9.11">renard</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Grand</w> <w n="10.2">argumentateur</w>, <w n="10.3">célèbre</w> <w n="10.4">babillard</w>,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Et</w> <w n="11.2">qui</w> <w n="11.3">montrait</w> <w n="11.4">la</w> <w n="11.5">rhétorique</w>.</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">Il</w> <w n="12.2">tenait</w> <w n="12.3">école</w> <w n="12.4">publique</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Avait</w> <w n="13.2">des</w> <w n="13.3">écoliers</w> <w n="13.4">qui</w> <w n="13.5">payaient</w> <w n="13.6">en</w> <w n="13.7">poulets</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Un</w> <w n="14.2">d</w>’<w n="14.3">eux</w>, <w n="14.4">qu</w>’<w n="14.5">on</w> <w n="14.6">destinait</w> <w n="14.7">à</w> <w n="14.8">plaider</w> <w n="14.9">au</w> <w n="14.10">palais</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Devait</w> <w n="15.2">payer</w> <w n="15.3">son</w> <w n="15.4">maître</w> <w n="15.5">à</w> <w n="15.6">la</w> <w n="15.7">première</w> <w n="15.8">cause</w></l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">Qu</w>’<w n="16.2">il</w> <w n="16.3">gagnerait</w> : <w n="16.4">ainsi</w> <w n="16.5">la</w> <w n="16.6">chose</w></l>
						<l n="17" num="1.17"><w n="17.1">Avait</w> <w n="17.2">été</w> <w n="17.3">réglée</w> <w n="17.4">et</w> <w n="17.5">d</w>’<w n="17.6">une</w> <w n="17.7">et</w> <w n="17.8">d</w>’<w n="17.9">autre</w> <w n="17.10">part</w>.</l>
						<l n="18" num="1.18"><w n="18.1">Son</w> <w n="18.2">cours</w> <w n="18.3">étant</w> <w n="18.4">fini</w>, <w n="18.5">mon</w> <w n="18.6">écolier</w> <w n="18.7">renard</w></l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Intente</w> <w n="19.2">un</w> <w n="19.3">procès</w> <w n="19.4">à</w> <w n="19.5">son</w> <w n="19.6">maître</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Disant</w> <w n="20.2">qu</w>’<w n="20.3">il</w> <w n="20.4">ne</w> <w n="20.5">doit</w> <w n="20.6">rien</w>. <w n="20.7">Devant</w> <w n="20.8">le</w> <w n="20.9">léopard</w></l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">Tous</w> <w n="21.2">les</w> <w n="21.3">deux</w> <w n="21.4">s</w>’<w n="21.5">en</w> <w n="21.6">vont</w> <w n="21.7">comparaître</w>.</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1">Monseigneur</w>, <w n="22.2">disait</w> <w n="22.3">l</w>’<w n="22.4">écolier</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Si</w> <w n="23.2">je</w> <w n="23.3">gagne</w>, <w n="23.4">c</w>’<w n="23.5">est</w> <w n="23.6">clair</w>, <w n="23.7">je</w> <w n="23.8">ne</w> <w n="23.9">dois</w> <w n="23.10">rien</w> <w n="23.11">payer</w> ;</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Et</w> <w n="24.2">cela</w> <w n="24.3">par</w> <w n="24.4">votre</w> <w n="24.5">sentence</w>,</l>
						<l n="25" num="1.25"><space unit="char" quantity="12"></space><w n="25.1">Puisque</w> <w n="25.2">par</w> <w n="25.3">la</w> <w n="25.4">sentence</w></l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">J</w>’<w n="26.2">aurai</w> <w n="26.3">droit</w> <w n="26.4">de</w> <w n="26.5">ne</w> <w n="26.6">pas</w> <w n="26.7">payer</w>.</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">Si</w> <w n="27.2">je</w> <w n="27.3">perds</w>, <w n="27.4">nulle</w> <w n="27.5">est</w> <w n="27.6">sa</w> <w n="27.7">créance</w> ;</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space><w n="28.1">Car</w> <w n="28.2">il</w> <w n="28.3">convient</w> <w n="28.4">que</w> <w n="28.5">l</w>’<w n="28.6">échéance</w></l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">N</w>’<w n="29.2">en</w> <w n="29.3">devait</w> <w n="29.4">arriver</w> <w n="29.5">qu</w>’<w n="29.6">après</w></l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">Le</w> <w n="30.2">gain</w> <w n="30.3">de</w> <w n="30.4">mon</w> <w n="30.5">premier</w> <w n="30.6">procès</w> :</l>
						<l n="31" num="1.31"><w n="31.1">Or</w>, <w n="31.2">ce</w> <w n="31.3">procès</w> <w n="31.4">perdu</w>, <w n="31.5">je</w> <w n="31.6">suis</w> <w n="31.7">quitte</w>, <w n="31.8">je</w> <w n="31.9">pense</w> :</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space><w n="32.1">Mon</w> <w n="32.2">dilemme</w> <w n="32.3">est</w> <w n="32.4">certain</w>. <w n="32.5">Nenni</w>,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space><w n="33.1">Répondait</w> <w n="33.2">aussitôt</w> <w n="33.3">le</w> <w n="33.4">maître</w> :</l>
						<l n="34" num="1.34"><w n="34.1">Si</w> <w n="34.2">vous</w> <w n="34.3">perdez</w>, <w n="34.4">payez</w> ; <w n="34.5">la</w> <w n="34.6">loi</w> <w n="34.7">l</w>’<w n="34.8">ordonne</w> <w n="34.9">ainsi</w>.</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space><w n="35.1">Si</w> <w n="35.2">vous</w> <w n="35.3">gagnez</w>, <w n="35.4">sans</w> <w n="35.5">plus</w> <w n="35.6">remettre</w>,</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space><w n="36.1">Payez</w> ; <w n="36.2">car</w> <w n="36.3">vous</w> <w n="36.4">avez</w> <w n="36.5">signé</w></l>
						<l n="37" num="1.37"><w n="37.1">Promesse</w> <w n="37.2">de</w> <w n="37.3">payer</w> <w n="37.4">au</w> <w n="37.5">premier</w> <w n="37.6">plaids</w> <w n="37.7">gagné</w> :</l>
						<l n="38" num="1.38"><w n="38.1">Vous</w> <w n="38.2">y</w> <w n="38.3">voilà</w>. <w n="38.4">Je</w> <w n="38.5">crois</w> <w n="38.6">l</w>’<w n="38.7">argument</w> <w n="38.8">sans</w> <w n="38.9">réponse</w>.</l>
						<l n="39" num="1.39"><w n="39.1">Chacun</w> <w n="39.2">attend</w> <w n="39.3">alors</w> <w n="39.4">que</w> <w n="39.5">le</w> <w n="39.6">juge</w> <w n="39.7">prononce</w>,</l>
						<l n="40" num="1.40"><space unit="char" quantity="8"></space><w n="40.1">Et</w> <w n="40.2">l</w>’<w n="40.3">auditoire</w> <w n="40.4">s</w>’<w n="40.5">étonnait</w></l>
						<l n="41" num="1.41"><space unit="char" quantity="8"></space><w n="41.1">Qu</w>’<w n="41.2">il</w> <w n="41.3">n</w>’<w n="41.4">y</w> <w n="41.5">jetât</w> <w n="41.6">pas</w> <w n="41.7">son</w> <w n="41.8">bonnet</w>.</l>
						<l n="42" num="1.42"><w n="42.1">Le</w> <w n="42.2">léopard</w> <w n="42.3">rêveur</w> <w n="42.4">prit</w> <w n="42.5">enfin</w> <w n="42.6">la</w> <w n="42.7">parole</w> :</l>
						<l n="43" num="1.43"><w n="43.1">Hors</w> <w n="43.2">de</w> <w n="43.3">cour</w>, <w n="43.4">leur</w> <w n="43.5">dit</w>-<w n="43.6">il</w> : <w n="43.7">défense</w> <w n="43.8">à</w> <w n="43.9">l</w>’<w n="43.10">écolier</w></l>
						<l n="44" num="1.44"><space unit="char" quantity="8"></space><w n="44.1">De</w> <w n="44.2">continuer</w> <w n="44.3">son</w> <w n="44.4">métier</w>,</l>
						<l n="45" num="1.45"><space unit="char" quantity="8"></space><w n="45.1">Au</w> <w n="45.2">maître</w> <w n="45.3">de</w> <w n="45.4">tenir</w> <w n="45.5">école</w>.</l>
					</lg>
				</div></body></text></TEI>