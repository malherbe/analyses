<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="FLO13">
					<head type="number">FABLE XIII</head>
					<head type="main">La Coquette et l’Abeille</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Chloé</w>, <w n="1.2">jeune</w>, <w n="1.3">jolie</w>, <w n="1.4">et</w> <w n="1.5">sur</w>-<w n="1.6">tout</w> <w n="1.7">fort</w> <w n="1.8">coquette</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Tous</w> <w n="2.2">les</w> <w n="2.3">matins</w>, <w n="2.4">en</w> <w n="2.5">se</w> <w n="2.6">levant</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">mettait</w> <w n="3.3">au</w> <w n="3.4">travail</w>, <w n="3.5">j</w>’<w n="3.6">entends</w> <w n="3.7">à</w> <w n="3.8">sa</w> <w n="3.9">toilette</w> ;</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Et</w> <w n="4.2">là</w>, <w n="4.3">souriant</w>, <w n="4.4">minaudant</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">Elle</w> <w n="5.2">disait</w> <w n="5.3">à</w> <w n="5.4">son</w> <w n="5.5">cher</w> <w n="5.6">confident</w></l>
						<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">peines</w>, <w n="6.3">les</w> <w n="6.4">plaisirs</w>, <w n="6.5">les</w> <w n="6.6">projets</w> <w n="6.7">de</w> <w n="6.8">son</w> <w n="6.9">âme</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Une</w> <w n="7.2">abeille</w> <w n="7.3">étourdie</w> <w n="7.4">arrive</w> <w n="7.5">en</w> <w n="7.6">bourdonnant</w>.</l>
						<l n="8" num="1.8"><w n="8.1">Au</w> <w n="8.2">secours</w> ! <w n="8.3">au</w> <w n="8.4">secours</w> ! <w n="8.5">crie</w> <w n="8.6">aussitôt</w> <w n="8.7">la</w> <w n="8.8">dame</w> :</l>
						<l n="9" num="1.9"><w n="9.1">Venez</w>, <w n="9.2">Lise</w>, <w n="9.3">Marton</w>, <w n="9.4">accourez</w> <w n="9.5">promptement</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">Chassez</w> <w n="10.2">ce</w> <w n="10.3">monstre</w> <w n="10.4">ailé</w>. <w n="10.5">Le</w> <w n="10.6">monstre</w> <w n="10.7">insolemment</w></l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Aux</w> <w n="11.2">lèvres</w> <w n="11.3">de</w> <w n="11.4">Chloé</w> <w n="11.5">se</w> <w n="11.6">pose</w>.</l>
						<l n="12" num="1.12"><w n="12.1">Chloé</w> <w n="12.2">s</w>’<w n="12.3">évanouit</w>, <w n="12.4">et</w> <w n="12.5">Marton</w> <w n="12.6">en</w> <w n="12.7">fureur</w></l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">Saisit</w> <w n="13.2">l</w>’<w n="13.3">abeille</w> <w n="13.4">et</w> <w n="13.5">se</w> <w n="13.6">dispose</w></l>
						<l n="14" num="1.14"><w n="14.1">A</w> <w n="14.2">l</w>’<w n="14.3">écraser</w>. <w n="14.4">Hélas</w> ! <w n="14.5">lui</w> <w n="14.6">dit</w> <w n="14.7">avec</w> <w n="14.8">douceur</w></l>
						<l n="15" num="1.15"><w n="15.1">L</w>’<w n="15.2">insecte</w> <w n="15.3">malheureux</w>, <w n="15.4">pardonnez</w> <w n="15.5">mon</w> <w n="15.6">erreur</w> ;</l>
						<l n="16" num="1.16"><w n="16.1">La</w> <w n="16.2">bouche</w> <w n="16.3">de</w> <w n="16.4">Chloé</w> <w n="16.5">me</w> <w n="16.6">semblait</w> <w n="16.7">une</w> <w n="16.8">rose</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">j</w>’<w n="17.3">ai</w> <w n="17.4">cru</w>… <w n="17.5">Ce</w> <w n="17.6">seul</w> <w n="17.7">mot</w> <w n="17.8">à</w> <w n="17.9">Chloé</w> <w n="17.10">rend</w> <w n="17.11">ses</w> <w n="17.12">sens</w>.</l>
						<l n="18" num="1.18"><w n="18.1">Faisons</w> <w n="18.2">grace</w>, <w n="18.3">dit</w>-<w n="18.4">elle</w>, <w n="18.5">à</w> <w n="18.6">son</w> <w n="18.7">aveu</w> <w n="18.8">sincère</w> :</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">D</w>’<w n="19.2">ailleurs</w> <w n="19.3">sa</w> <w n="19.4">piqûre</w> <w n="19.5">est</w> <w n="19.6">légère</w> ;</l>
						<l n="20" num="1.20"><w n="20.1">Depuis</w> <w n="20.2">qu</w>’<w n="20.3">elle</w> <w n="20.4">te</w> <w n="20.5">parle</w>, <w n="20.6">à</w> <w n="20.7">peine</w> <w n="20.8">je</w> <w n="20.9">la</w> <w n="20.10">sens</w>.</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1"><w n="21.1">Que</w> <w n="21.2">ne</w> <w n="21.3">fait</w>-<w n="21.4">on</w> <w n="21.5">passer</w> <w n="21.6">avec</w> <w n="21.7">un</w> <w n="21.8">peu</w> <w n="21.9">d</w>’<w n="21.10">encens</w> !</l>
					</lg>
				</div></body></text></TEI>