<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="FLO67">
					<head type="number">FABLE PREMIÈRE</head>
					<head type="main">Le Savant et le Fermier</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">j</w>’<w n="1.3">aime</w> <w n="1.4">les</w> <w n="1.5">héros</w> <w n="1.6">dont</w> <w n="1.7">je</w> <w n="1.8">conte</w> <w n="1.9">l</w>’<w n="1.10">histoire</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">qu</w>’<w n="2.3">à</w> <w n="2.4">m</w>’<w n="2.5">occuper</w> <w n="2.6">d</w>’<w n="2.7">eux</w> <w n="2.8">je</w> <w n="2.9">trouve</w> <w n="2.10">de</w> <w n="2.11">douceur</w> !</l>
						<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">ignore</w> <w n="3.3">s</w>’<w n="3.4">ils</w> <w n="3.5">pourront</w> <w n="3.6">m</w>’<w n="3.7">acquérir</w> <w n="3.8">de</w> <w n="3.9">la</w> <w n="3.10">gloire</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Mais</w> <w n="4.2">je</w> <w n="4.3">sais</w> <w n="4.4">qu</w>’<w n="4.5">ils</w> <w n="4.6">font</w> <w n="4.7">mon</w> <w n="4.8">bonheur</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Avec</w> <w n="5.2">les</w> <w n="5.3">animaux</w> <w n="5.4">je</w> <w n="5.5">veux</w> <w n="5.6">passer</w> <w n="5.7">ma</w> <w n="5.8">vie</w> ;</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Ils</w> <w n="6.2">sont</w> <w n="6.3">si</w> <w n="6.4">bonne</w> <w n="6.5">compagnie</w> !</l>
						<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">conviens</w> <w n="7.3">cependant</w>, <w n="7.4">et</w> <w n="7.5">c</w>’<w n="7.6">est</w> <w n="7.7">avec</w> <w n="7.8">douleur</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Que</w> <w n="8.2">tous</w> <w n="8.3">n</w>’<w n="8.4">ont</w> <w n="8.5">pas</w> <w n="8.6">le</w> <w n="8.7">même</w> <w n="8.8">cœur</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Plusieurs</w> <w n="9.2">que</w> <w n="9.3">l</w>’<w n="9.4">on</w> <w n="9.5">connaît</w>, <w n="9.6">sans</w> <w n="9.7">qu</w>’<w n="9.8">ici</w> <w n="9.9">je</w> <w n="9.10">les</w> <w n="9.11">nomme</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">De</w> <w n="10.2">nos</w> <w n="10.3">vices</w> <w n="10.4">ont</w> <w n="10.5">bonne</w> <w n="10.6">part</w> :</l>
						<l n="11" num="1.11"><w n="11.1">Mais</w> <w n="11.2">je</w> <w n="11.3">les</w> <w n="11.4">trouve</w> <w n="11.5">encor</w> <w n="11.6">moins</w> <w n="11.7">dangereux</w> <w n="11.8">que</w> <w n="11.9">l</w>’<w n="11.10">homme</w> ;</l>
						<l n="12" num="1.12"><w n="12.1">Et</w>, <w n="12.2">fripon</w> <w n="12.3">pour</w> <w n="12.4">fripon</w>, <w n="12.5">je</w> <w n="12.6">préfère</w> <w n="12.7">un</w> <w n="12.8">renard</w>.</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">ainsi</w> <w n="13.4">que</w> <w n="13.5">pensait</w> <w n="13.6">un</w> <w n="13.7">sage</w>,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">Un</w> <w n="14.2">bon</w> <w n="14.3">fermier</w> <w n="14.4">de</w> <w n="14.5">mon</w> <w n="14.6">pays</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Depuis</w> <w n="15.2">quatre</w>-<w n="15.3">vingts</w> <w n="15.4">ans</w>, <w n="15.5">de</w> <w n="15.6">tout</w> <w n="15.7">le</w> <w n="15.8">voisinage</w></l>
						<l n="16" num="1.16"><w n="16.1">On</w> <w n="16.2">venait</w> <w n="16.3">écouter</w> <w n="16.4">et</w> <w n="16.5">suivre</w> <w n="16.6">ses</w> <w n="16.7">avis</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Chaque</w> <w n="17.2">mot</w> <w n="17.3">qu</w>’<w n="17.4">il</w> <w n="17.5">disait</w> <w n="17.6">était</w> <w n="17.7">une</w> <w n="17.8">sentence</w>.</l>
						<l n="18" num="1.18"><w n="18.1">Son</w> <w n="18.2">exemple</w> <w n="18.3">sur</w>-<w n="18.4">tout</w> <w n="18.5">aidait</w> <w n="18.6">son</w> <w n="18.7">éloquence</w> ;</l>
						<l n="19" num="1.19"><w n="19.1">Et</w>, <w n="19.2">lorsqu</w>’<w n="19.3">environné</w> <w n="19.4">de</w> <w n="19.5">ses</w> <w n="19.6">quarante</w> <w n="19.7">enfans</w>,</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">Fils</w>, <w n="20.2">petit</w>-<w n="20.3">fils</w>, <w n="20.4">brus</w>, <w n="20.5">gendres</w>, <w n="20.6">filles</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Il</w> <w n="21.2">jugeait</w> <w n="21.3">les</w> <w n="21.4">procès</w> <w n="21.5">ou</w> <w n="21.6">réglait</w> <w n="21.7">les</w> <w n="21.8">familles</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Nul</w> <w n="22.2">n</w>’<w n="22.3">eût</w> <w n="22.4">osé</w> <w n="22.5">mentir</w> <w n="22.6">devant</w> <w n="22.7">ses</w> <w n="22.8">cheveux</w> <w n="22.9">blancs</w>.</l>
						<l n="23" num="1.23"><w n="23.1">Je</w> <w n="23.2">me</w> <w n="23.3">souviens</w> <w n="23.4">qu</w>’<w n="23.5">un</w> <w n="23.6">jour</w> <w n="23.7">dans</w> <w n="23.8">son</w> <w n="23.9">champêtre</w> <w n="23.10">asile</w></l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Il</w> <w n="24.2">vint</w> <w n="24.3">un</w> <w n="24.4">savant</w> <w n="24.5">de</w> <w n="24.6">la</w> <w n="24.7">ville</w></l>
						<l n="25" num="1.25"><w n="25.1">Qui</w> <w n="25.2">dit</w> <w n="25.3">au</w> <w n="25.4">bon</w> <w n="25.5">vieillard</w> : <w n="25.6">Mon</w> <w n="25.7">père</w>, <w n="25.8">enseignez</w>-<w n="25.9">moi</w></l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Dans</w> <w n="26.2">quel</w> <w n="26.3">auteur</w>, <w n="26.4">dans</w> <w n="26.5">quel</w> <w n="26.6">ouvrage</w>,</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">Vous</w> <w n="27.2">apprîtes</w> <w n="27.3">l</w>’<w n="27.4">art</w> <w n="27.5">d</w>’<w n="27.6">être</w> <w n="27.7">sage</w>.</l>
						<l n="28" num="1.28"><w n="28.1">Chez</w> <w n="28.2">quelle</w> <w n="28.3">nation</w>, <w n="28.4">à</w> <w n="28.5">la</w> <w n="28.6">cour</w> <w n="28.7">de</w> <w n="28.8">quel</w> <w n="28.9">roi</w>,</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">Avez</w>-<w n="29.2">vous</w> <w n="29.3">été</w>, <w n="29.4">comme</w> <w n="29.5">Ulysse</w>,</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">Prendre</w> <w n="30.2">des</w> <w n="30.3">leçons</w> <w n="30.4">de</w> <w n="30.5">justice</w> ?</l>
						<l n="31" num="1.31"><w n="31.1">Suivez</w>-<w n="31.2">vous</w> <w n="31.3">de</w> <w n="31.4">Zénon</w> <w n="31.5">la</w> <w n="31.6">rigoureuse</w> <w n="31.7">loi</w> ?</l>
						<l n="32" num="1.32"><w n="32.1">Avez</w>-<w n="32.2">vous</w> <w n="32.3">embrassé</w> <w n="32.4">la</w> <w n="32.5">secte</w> <w n="32.6">d</w>’<w n="32.7">Épicure</w>,</l>
						<l n="33" num="1.33"><w n="33.1">Celle</w> <w n="33.2">de</w> <w n="33.3">Pythagore</w> <w n="33.4">ou</w> <w n="33.5">du</w> <w n="33.6">divin</w> <w n="33.7">Platon</w> ?</l>
						<l n="34" num="1.34"><w n="34.1">De</w> <w n="34.2">tous</w> <w n="34.3">ces</w> <w n="34.4">messieurs</w>-<w n="34.5">là</w> <w n="34.6">je</w> <w n="34.7">ne</w> <w n="34.8">sais</w> <w n="34.9">pas</w> <w n="34.10">le</w> <w n="34.11">nom</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Répondit</w> <w n="35.2">le</w> <w n="35.3">vieillard</w> : <w n="35.4">mon</w> <w n="35.5">livre</w> <w n="35.6">est</w> <w n="35.7">la</w> <w n="35.8">nature</w> ;</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space><w n="36.1">Et</w> <w n="36.2">mon</w> <w n="36.3">unique</w> <w n="36.4">précepteur</w>,</l>
						<l n="37" num="1.37"><space unit="char" quantity="18"></space><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">mon</w> <w n="37.4">cœur</w>.</l>
						<l n="38" num="1.38"><w n="38.1">Je</w> <w n="38.2">vois</w> <w n="38.3">les</w> <w n="38.4">animaux</w>, <w n="38.5">j</w>’<w n="38.6">y</w> <w n="38.7">trouve</w> <w n="38.8">le</w> <w n="38.9">modèle</w></l>
						<l n="39" num="1.39"><space unit="char" quantity="8"></space><w n="39.1">Des</w> <w n="39.2">vertus</w> <w n="39.3">que</w> <w n="39.4">je</w> <w n="39.5">dois</w> <w n="39.6">chérir</w> :</l>
						<l n="40" num="1.40"><w n="40.1">La</w> <w n="40.2">colombe</w> <w n="40.3">m</w>’<w n="40.4">apprit</w> <w n="40.5">à</w> <w n="40.6">devenir</w> <w n="40.7">fidèle</w> ;</l>
						<l n="41" num="1.41"><w n="41.1">En</w> <w n="41.2">voyant</w> <w n="41.3">la</w> <w n="41.4">fourmi</w>, <w n="41.5">j</w>’<w n="41.6">amassai</w> <w n="41.7">pour</w> <w n="41.8">jouir</w> ;</l>
						<l n="42" num="1.42"><space unit="char" quantity="8"></space><w n="42.1">Mes</w> <w n="42.2">bœufs</w> <w n="42.3">m</w>’<w n="42.4">enseignent</w> <w n="42.5">la</w> <w n="42.6">constance</w>,</l>
						<l n="43" num="1.43"><w n="43.1">Mes</w> <w n="43.2">brebis</w> <w n="43.3">la</w> <w n="43.4">douceur</w>, <w n="43.5">mes</w> <w n="43.6">chiens</w> <w n="43.7">la</w> <w n="43.8">vigilance</w> ;</l>
						<l n="44" num="1.44"><space unit="char" quantity="8"></space><w n="44.1">Et</w> <w n="44.2">si</w> <w n="44.3">j</w>’<w n="44.4">avais</w> <w n="44.5">besoin</w> <w n="44.6">d</w>’<w n="44.7">avis</w></l>
						<l n="45" num="1.45"><space unit="char" quantity="8"></space><w n="45.1">Pour</w> <w n="45.2">aimer</w> <w n="45.3">mes</w> <w n="45.4">filles</w>, <w n="45.5">mes</w> <w n="45.6">fils</w>,</l>
						<l n="46" num="1.46"><w n="46.1">La</w> <w n="46.2">poule</w> <w n="46.3">et</w> <w n="46.4">ses</w> <w n="46.5">poussins</w> <w n="46.6">me</w> <w n="46.7">serviraient</w> <w n="46.8">d</w>’<w n="46.9">exemple</w>.</l>
						<l n="47" num="1.47"><w n="47.1">Ainsi</w> <w n="47.2">dans</w> <w n="47.3">l</w>’<w n="47.4">univers</w> <w n="47.5">tout</w> <w n="47.6">ce</w> <w n="47.7">que</w> <w n="47.8">je</w> <w n="47.9">contemple</w></l>
						<l n="48" num="1.48"><w n="48.1">M</w>’<w n="48.2">avertit</w> <w n="48.3">d</w>’<w n="48.4">un</w> <w n="48.5">devoir</w> <w n="48.6">qu</w>’<w n="48.7">il</w> <w n="48.8">m</w>’<w n="48.9">est</w> <w n="48.10">doux</w> <w n="48.11">de</w> <w n="48.12">remplir</w>.</l>
						<l n="49" num="1.49"><w n="49.1">Je</w> <w n="49.2">fais</w> <w n="49.3">souvent</w> <w n="49.4">du</w> <w n="49.5">bien</w> <w n="49.6">pour</w> <w n="49.7">avoir</w> <w n="49.8">du</w> <w n="49.9">plaisir</w>,</l>
						<l n="50" num="1.50"><w n="50.1">J</w>’<w n="50.2">aime</w> <w n="50.3">et</w> <w n="50.4">je</w> <w n="50.5">suis</w> <w n="50.6">aimé</w>, <w n="50.7">mon</w> <w n="50.8">âme</w> <w n="50.9">est</w> <w n="50.10">tendre</w> <w n="50.11">et</w> <w n="50.12">pure</w>,</l>
						<l n="51" num="1.51"><space unit="char" quantity="8"></space><w n="51.1">Et</w>, <w n="51.2">toujours</w> <w n="51.3">selon</w> <w n="51.4">ma</w> <w n="51.5">mesure</w>,</l>
						<l n="52" num="1.52"><space unit="char" quantity="8"></space><w n="52.1">Ma</w> <w n="52.2">raison</w> <w n="52.3">sait</w> <w n="52.4">régler</w> <w n="52.5">mes</w> <w n="52.6">vœux</w> :</l>
						<l n="53" num="1.53"><space unit="char" quantity="8"></space><w n="53.1">J</w>’<w n="53.2">observe</w> <w n="53.3">et</w> <w n="53.4">je</w> <w n="53.5">suis</w> <w n="53.6">la</w> <w n="53.7">nature</w>,</l>
						<l n="54" num="1.54"><space unit="char" quantity="8"></space><w n="54.1">C</w>’<w n="54.2">est</w> <w n="54.3">mon</w> <w n="54.4">secret</w> <w n="54.5">pour</w> <w n="54.6">être</w> <w n="54.7">heureux</w>.</l>
					</lg>
				</div></body></text></TEI>