<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SECOND</head><div type="poem" key="FLO26">
					<head type="number">FABLE IV</head>
					<head type="main">Le bon Homme et le Trésor</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Un</w> <w n="1.2">bon</w> <w n="1.3">homme</w> <w n="1.4">de</w> <w n="1.5">mes</w> <w n="1.6">parens</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Que</w> <w n="2.2">j</w>’<w n="2.3">ai</w> <w n="2.4">connu</w> <w n="2.5">dans</w> <w n="2.6">mon</w> <w n="2.7">jeune</w> <w n="2.8">âge</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">faisait</w> <w n="3.3">adorer</w> <w n="3.4">de</w> <w n="3.5">tout</w> <w n="3.6">son</w> <w n="3.7">voisinage</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Consulté</w>, <w n="4.2">vénéré</w> <w n="4.3">des</w> <w n="4.4">petits</w> <w n="4.5">et</w> <w n="4.6">des</w> <w n="4.7">grands</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">vivait</w> <w n="5.3">dans</w> <w n="5.4">sa</w> <w n="5.5">terre</w> <w n="5.6">en</w> <w n="5.7">véritable</w> <w n="5.8">sage</w>.</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Il</w> <w n="6.2">n</w>’<w n="6.3">avait</w> <w n="6.4">pas</w> <w n="6.5">beaucoup</w> <w n="6.6">d</w>’<w n="6.7">écus</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Mais</w> <w n="7.2">cependant</w> <w n="7.3">assez</w> <w n="7.4">pour</w> <w n="7.5">vivre</w> <w n="7.6">dans</w> <w n="7.7">l</w>’<w n="7.8">aisance</w> ;</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">En</w> <w n="8.2">revanche</w>, <w n="8.3">force</w> <w n="8.4">vertus</w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Du</w> <w n="9.2">sens</w>, <w n="9.3">de</w> <w n="9.4">l</w>’<w n="9.5">esprit</w> <w n="9.6">par</w>-<w n="9.7">dessus</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">cette</w> <w n="10.3">aménité</w> <w n="10.4">que</w> <w n="10.5">donne</w> <w n="10.6">l</w>’<w n="10.7">innocence</w>.</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Quand</w> <w n="11.2">un</w> <w n="11.3">pauvre</w> <w n="11.4">venait</w> <w n="11.5">le</w> <w n="11.6">voir</w>,</l>
						<l n="12" num="1.12"><w n="12.1">S</w>’<w n="12.2">il</w> <w n="12.3">avait</w> <w n="12.4">de</w> <w n="12.5">l</w>’<w n="12.6">argent</w>, <w n="12.7">il</w> <w n="12.8">donnait</w> <w n="12.9">des</w> <w n="12.10">pistoles</w> ;</l>
						<l n="13" num="1.13"><w n="13.1">Et</w>, <w n="13.2">s</w>’<w n="13.3">il</w> <w n="13.4">n</w>’<w n="13.5">en</w> <w n="13.6">avait</w> <w n="13.7">point</w>, <w n="13.8">du</w> <w n="13.9">moins</w> <w n="13.10">par</w> <w n="13.11">ses</w> <w n="13.12">paroles</w></l>
						<l n="14" num="1.14"><w n="14.1">Il</w> <w n="14.2">lui</w> <w n="14.3">rendait</w> <w n="14.4">un</w> <w n="14.5">peu</w> <w n="14.6">de</w> <w n="14.7">courage</w> <w n="14.8">et</w> <w n="14.9">d</w>’<w n="14.10">espoir</w>.</l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">Il</w> <w n="15.2">raccommodait</w> <w n="15.3">les</w> <w n="15.4">familles</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Corrigeait</w> <w n="16.2">doucement</w> <w n="16.3">les</w> <w n="16.4">jeunes</w> <w n="16.5">étourdis</w>,</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1">Riait</w> <w n="17.2">avec</w> <w n="17.3">les</w> <w n="17.4">jeunes</w> <w n="17.5">filles</w>,</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Et</w> <w n="18.2">leur</w> <w n="18.3">trouvait</w> <w n="18.4">de</w> <w n="18.5">bons</w> <w n="18.6">maris</w>.</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Indulgent</w> <w n="19.2">aux</w> <w n="19.3">défauts</w> <w n="19.4">des</w> <w n="19.5">autres</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Il</w> <w n="20.2">répétait</w> <w n="20.3">souvent</w> : <w n="20.4">N</w>’<w n="20.5">avons</w>-<w n="20.6">nous</w> <w n="20.7">pas</w> <w n="20.8">les</w> <w n="20.9">nôtres</w> ?</l>
						<l n="21" num="1.21"><w n="21.1">Ceux</w>-<w n="21.2">ci</w> <w n="21.3">sont</w> <w n="21.4">nés</w> <w n="21.5">boiteux</w>, <w n="21.6">ceux</w>-<w n="21.7">là</w> <w n="21.8">sont</w> <w n="21.9">nés</w> <w n="21.10">bossus</w>,</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1">L</w>’<w n="22.2">un</w> <w n="22.3">un</w> <w n="22.4">peu</w> <w n="22.5">moins</w> ; <w n="22.6">l</w>’<w n="22.7">autre</w> <w n="22.8">un</w> <w n="22.9">peu</w> <w n="22.10">plus</w> :</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">La</w> <w n="23.2">nature</w> <w n="23.3">de</w> <w n="23.4">cent</w> <w n="23.5">manières</w></l>
						<l n="24" num="1.24"><w n="24.1">Voulut</w> <w n="24.2">nous</w> <w n="24.3">affliger</w> : <w n="24.4">marchons</w> <w n="24.5">ensemble</w> <w n="24.6">en</w> <w n="24.7">paix</w> ;</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Le</w> <w n="25.2">chemin</w> <w n="25.3">est</w> <w n="25.4">assez</w> <w n="25.5">mauvais</w></l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Sans</w> <w n="26.2">nous</w> <w n="26.3">jeter</w> <w n="26.4">encor</w> <w n="26.5">des</w> <w n="26.6">pierres</w>.</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">Or</w> <w n="27.2">il</w> <w n="27.3">arriva</w> <w n="27.4">certain</w> <w n="27.5">jour</w></l>
						<l n="28" num="1.28"><w n="28.1">Que</w> <w n="28.2">notre</w> <w n="28.3">bon</w> <w n="28.4">vieillard</w> <w n="28.5">trouva</w> <w n="28.6">dans</w> <w n="28.7">une</w> <w n="28.8">tour</w></l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">Un</w> <w n="29.2">trésor</w> <w n="29.3">caché</w> <w n="29.4">sous</w> <w n="29.5">la</w> <w n="29.6">terre</w>.</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">D</w>’<w n="30.2">abord</w> <w n="30.3">il</w> <w n="30.4">n</w>’<w n="30.5">y</w> <w n="30.6">voit</w> <w n="30.7">qu</w>’<w n="30.8">un</w> <w n="30.9">moyen</w></l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space><w n="31.1">De</w> <w n="31.2">pouvoir</w> <w n="31.3">faire</w> <w n="31.4">plus</w> <w n="31.5">de</w> <w n="31.6">bien</w> ;</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space><w n="32.1">Il</w> <w n="32.2">le</w> <w n="32.3">prend</w>, <w n="32.4">l</w>’<w n="32.5">emporte</w> <w n="32.6">et</w> <w n="32.7">le</w> <w n="32.8">serre</w>.</l>
						<l n="33" num="1.33"><w n="33.1">Puis</w>, <w n="33.2">en</w> <w n="33.3">réfléchissant</w>, <w n="33.4">le</w> <w n="33.5">voilà</w> <w n="33.6">qui</w> <w n="33.7">se</w> <w n="33.8">dit</w> :</l>
						<l n="34" num="1.34"><w n="34.1">Cet</w> <w n="34.2">or</w> <w n="34.3">que</w> <w n="34.4">j</w>’<w n="34.5">ai</w> <w n="34.6">trouvé</w> <w n="34.7">ferait</w> <w n="34.8">plus</w> <w n="34.9">de</w> <w n="34.10">profit</w></l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space><w n="35.1">Si</w> <w n="35.2">j</w>’<w n="35.3">en</w> <w n="35.4">augmentais</w> <w n="35.5">mon</w> <w n="35.6">domaine</w> ;</l>
						<l n="36" num="1.36"><w n="36.1">J</w>’<w n="36.2">aurais</w> <w n="36.3">plus</w> <w n="36.4">de</w> <w n="36.5">vassaux</w>, <w n="36.6">je</w> <w n="36.7">serais</w> <w n="36.8">plus</w> <w n="36.9">puissant</w>.</l>
						<l n="37" num="1.37"><w n="37.1">Je</w> <w n="37.2">peux</w> <w n="37.3">mieux</w> <w n="37.4">faire</w> <w n="37.5">encor</w> : <w n="37.6">dans</w> <w n="37.7">la</w> <w n="37.8">ville</w> <w n="37.9">prochaine</w></l>
						<l n="38" num="1.38"><w n="38.1">Achetons</w> <w n="38.2">une</w> <w n="38.3">charge</w>, <w n="38.4">et</w> <w n="38.5">soyons</w> <w n="38.6">président</w>.</l>
						<l n="39" num="1.39"><space unit="char" quantity="8"></space><w n="39.1">Président</w> ! <w n="39.2">cela</w> <w n="39.3">vaut</w> <w n="39.4">la</w> <w n="39.5">peine</w>.</l>
						<l n="40" num="1.40"><w n="40.1">Je</w> <w n="40.2">n</w>’<w n="40.3">ai</w> <w n="40.4">pas</w> <w n="40.5">fait</w> <w n="40.6">mon</w> <w n="40.7">droit</w>, <w n="40.8">mais</w>, <w n="40.9">avec</w> <w n="40.10">mon</w> <w n="40.11">argent</w>,</l>
						<l n="41" num="1.41"><w n="41.1">On</w> <w n="41.2">m</w>’<w n="41.3">en</w> <w n="41.4">dispensera</w>, <w n="41.5">puisque</w> <w n="41.6">cela</w> <w n="41.7">s</w>’<w n="41.8">achète</w>.</l>
						<l n="42" num="1.42"><space unit="char" quantity="8"></space><w n="42.1">Tandis</w> <w n="42.2">qu</w>’<w n="42.3">il</w> <w n="42.4">rêve</w> <w n="42.5">et</w> <w n="42.6">qu</w>’<w n="42.7">il</w> <w n="42.8">projette</w>,</l>
						<l n="43" num="1.43"><space unit="char" quantity="8"></space><w n="43.1">Sa</w> <w n="43.2">servante</w> <w n="43.3">vient</w> <w n="43.4">l</w>’<w n="43.5">avertir</w></l>
						<l n="44" num="1.44"><space unit="char" quantity="8"></space><w n="44.1">Que</w> <w n="44.2">les</w> <w n="44.3">jeunes</w> <w n="44.4">gens</w> <w n="44.5">du</w> <w n="44.6">village</w></l>
						<l n="45" num="1.45"><w n="45.1">Dans</w> <w n="45.2">la</w> <w n="45.3">cour</w> <w n="45.4">du</w> <w n="45.5">château</w> <w n="45.6">sont</w> <w n="45.7">à</w> <w n="45.8">se</w> <w n="45.9">divertir</w>.</l>
						<l n="46" num="1.46"><space unit="char" quantity="8"></space><w n="46.1">Le</w> <w n="46.2">dimanche</w>, <w n="46.3">c</w>’<w n="46.4">était</w> <w n="46.5">l</w>’<w n="46.6">usage</w>,</l>
						<l n="47" num="1.47"><w n="47.1">Le</w> <w n="47.2">seigneur</w> <w n="47.3">se</w> <w n="47.4">plaisait</w> <w n="47.5">à</w> <w n="47.6">danser</w> <w n="47.7">avec</w> <w n="47.8">eux</w>.</l>
						<l n="48" num="1.48"><w n="48.1">Oh</w> ! <w n="48.2">ma</w> <w n="48.3">foi</w>, <w n="48.4">répond</w>-<w n="48.5">il</w>, <w n="48.6">j</w>’<w n="48.7">ai</w> <w n="48.8">bien</w> <w n="48.9">d</w>’<w n="48.10">autres</w> <w n="48.11">affaires</w>,</l>
						<l n="49" num="1.49"><w n="49.1">Que</w> <w n="49.2">l</w>’<w n="49.3">on</w> <w n="49.4">danse</w> <w n="49.5">sans</w> <w n="49.6">moi</w>. <w n="49.7">L</w>’<w n="49.8">esprit</w> <w n="49.9">plein</w> <w n="49.10">de</w> <w n="49.11">chimères</w>,</l>
						<l n="50" num="1.50"><w n="50.1">Il</w> <w n="50.2">s</w>’<w n="50.3">enferme</w> <w n="50.4">tout</w> <w n="50.5">seul</w> <w n="50.6">pour</w> <w n="50.7">se</w> <w n="50.8">tourmenter</w> <w n="50.9">mieux</w>.</l>
						<l n="51" num="1.51"><space unit="char" quantity="8"></space><w n="51.1">Ensuite</w> <w n="51.2">il</w> <w n="51.3">va</w> <w n="51.4">joindre</w> <w n="51.5">à</w> <w n="51.6">sa</w> <w n="51.7">somme</w></l>
						<l n="52" num="1.52"><w n="52.1">Un</w> <w n="52.2">petit</w> <w n="52.3">sac</w> <w n="52.4">d</w>’<w n="52.5">argent</w>, <w n="52.6">reste</w> <w n="52.7">du</w> <w n="52.8">mois</w> <w n="52.9">dernier</w>.</l>
						<l n="53" num="1.53"><space unit="char" quantity="8"></space><w n="53.1">Dans</w> <w n="53.2">l</w>’<w n="53.3">instant</w> <w n="53.4">arrive</w> <w n="53.5">un</w> <w n="53.6">pauvre</w> <w n="53.7">homme</w></l>
						<l n="54" num="1.54"><space unit="char" quantity="8"></space><w n="54.1">Qui</w>, <w n="54.2">tout</w> <w n="54.3">en</w> <w n="54.4">pleurs</w>, <w n="54.5">vient</w> <w n="54.6">le</w> <w n="54.7">prier</w></l>
						<l n="55" num="1.55"><w n="55.1">De</w> <w n="55.2">vouloir</w> <w n="55.3">lui</w> <w n="55.4">prêter</w> <w n="55.5">vingt</w> <w n="55.6">écus</w> <w n="55.7">pour</w> <w n="55.8">sa</w> <w n="55.9">taille</w> :</l>
						<l n="56" num="1.56"><w n="56.1">Le</w> <w n="56.2">collecteur</w>, <w n="56.3">dit</w>-<w n="56.4">il</w>, <w n="56.5">va</w> <w n="56.6">me</w> <w n="56.7">mettre</w> <w n="56.8">en</w> <w n="56.9">prison</w>,</l>
						<l n="57" num="1.57"><space unit="char" quantity="8"></space><w n="57.1">Et</w> <w n="57.2">n</w>’<w n="57.3">a</w> <w n="57.4">laissé</w> <w n="57.5">dans</w> <w n="57.6">ma</w> <w n="57.7">maison</w></l>
						<l n="58" num="1.58"><space unit="char" quantity="8"></space><w n="58.1">Que</w> <w n="58.2">six</w> <w n="58.3">enfans</w> <w n="58.4">sur</w> <w n="58.5">de</w> <w n="58.6">la</w> <w n="58.7">paille</w>.</l>
						<l n="59" num="1.59"><w n="59.1">Notre</w> <w n="59.2">nouveau</w> <w n="59.3">Crésus</w> <w n="59.4">lui</w> <w n="59.5">répond</w> <w n="59.6">durement</w></l>
						<l n="60" num="1.60"><space unit="char" quantity="8"></space><w n="60.1">Qu</w>’<w n="60.2">il</w> <w n="60.3">n</w>’<w n="60.4">est</w> <w n="60.5">point</w> <w n="60.6">en</w> <w n="60.7">argent</w> <w n="60.8">comptant</w>.</l>
						<l n="61" num="1.61"><w n="61.1">Le</w> <w n="61.2">pauvre</w> <w n="61.3">malheureux</w> <w n="61.4">le</w> <w n="61.5">regarde</w>, <w n="61.6">soupire</w>,</l>
						<l n="62" num="1.62"><space unit="char" quantity="8"></space><w n="62.1">Et</w> <w n="62.2">s</w>’<w n="62.3">en</w> <w n="62.4">retourne</w> <w n="62.5">sans</w> <w n="62.6">mot</w> <w n="62.7">dire</w>.</l>
						<l n="63" num="1.63"><w n="63.1">Mais</w> <w n="63.2">il</w> <w n="63.3">n</w>’<w n="63.4">était</w> <w n="63.5">pas</w> <w n="63.6">loin</w>, <w n="63.7">que</w> <w n="63.8">notre</w> <w n="63.9">bon</w> <w n="63.10">seigneur</w></l>
						<l n="64" num="1.64"><space unit="char" quantity="8"></space><w n="64.1">Retrouve</w> <w n="64.2">tout</w>-<w n="64.3">à</w>-<w n="64.4">coup</w> <w n="64.5">son</w> <w n="64.6">cœur</w> :</l>
						<l n="65" num="1.65"><space unit="char" quantity="8"></space><w n="65.1">Il</w> <w n="65.2">court</w> <w n="65.3">au</w> <w n="65.4">paysan</w>, <w n="65.5">l</w>’<w n="65.6">embrasse</w>,</l>
						<l n="66" num="1.66"><space unit="char" quantity="8"></space><w n="66.1">De</w> <w n="66.2">cent</w> <w n="66.3">écus</w> <w n="66.4">lui</w> <w n="66.5">fait</w> <w n="66.6">le</w> <w n="66.7">don</w>,</l>
						<l n="67" num="1.67"><space unit="char" quantity="8"></space><w n="67.1">Et</w> <w n="67.2">lui</w> <w n="67.3">demande</w> <w n="67.4">encor</w> <w n="67.5">pardon</w>.</l>
						<l n="68" num="1.68"><w n="68.1">Ensuite</w> <w n="68.2">il</w> <w n="68.3">fait</w> <w n="68.4">crier</w> <w n="68.5">que</w> <w n="68.6">sur</w> <w n="68.7">la</w> <w n="68.8">grande</w> <w n="68.9">place</w></l>
						<l n="69" num="1.69"><w n="69.1">Le</w> <w n="69.2">village</w> <w n="69.3">assemblé</w> <w n="69.4">se</w> <w n="69.5">rende</w> <w n="69.6">dans</w> <w n="69.7">l</w>’<w n="69.8">instant</w>.</l>
						<l n="70" num="1.70"><space unit="char" quantity="8"></space><w n="70.1">On</w> <w n="70.2">obéit</w> ; <w n="70.3">notre</w> <w n="70.4">bon</w> <w n="70.5">homme</w></l>
						<l n="71" num="1.71"><space unit="char" quantity="8"></space><w n="71.1">Arrive</w> <w n="71.2">avec</w> <w n="71.3">toute</w> <w n="71.4">sa</w> <w n="71.5">somme</w>,</l>
						<l n="72" num="1.72"><space unit="char" quantity="8"></space><w n="72.1">En</w> <w n="72.2">un</w> <w n="72.3">seul</w> <w n="72.4">monceau</w> <w n="72.5">la</w> <w n="72.6">répand</w>.</l>
						<l n="73" num="1.73"><w n="73.1">Mes</w> <w n="73.2">amis</w>, <w n="73.3">leur</w> <w n="73.4">dit</w>-<w n="73.5">il</w>, <w n="73.6">vous</w> <w n="73.7">voyez</w> <w n="73.8">cet</w> <w n="73.9">argent</w> :</l>
						<l n="74" num="1.74"><w n="74.1">Depuis</w> <w n="74.2">qu</w>’<w n="74.3">il</w> <w n="74.4">m</w>’<w n="74.5">appartient</w> <w n="74.6">je</w> <w n="74.7">ne</w> <w n="74.8">suis</w> <w n="74.9">plus</w> <w n="74.10">le</w> <w n="74.11">même</w>,</l>
						<l n="75" num="1.75"><w n="75.1">Mon</w> <w n="75.2">âme</w> <w n="75.3">est</w> <w n="75.4">endurcie</w>, <w n="75.5">et</w> <w n="75.6">la</w> <w n="75.7">voix</w> <w n="75.8">du</w> <w n="75.9">malheur</w></l>
						<l n="76" num="1.76"><space unit="char" quantity="8"></space><w n="76.1">N</w>’<w n="76.2">arrive</w> <w n="76.3">plus</w> <w n="76.4">jusqu</w>’<w n="76.5">à</w> <w n="76.6">mon</w> <w n="76.7">cœur</w>.</l>
						<l n="77" num="1.77"><w n="77.1">Mes</w> <w n="77.2">enfans</w>, <w n="77.3">sauvez</w>-<w n="77.4">moi</w> <w n="77.5">de</w> <w n="77.6">ce</w> <w n="77.7">péril</w> <w n="77.8">extrême</w> ;</l>
						<l n="78" num="1.78"><w n="78.1">Prenez</w> <w n="78.2">et</w> <w n="78.3">partagez</w> <w n="78.4">ce</w> <w n="78.5">dangereux</w> <w n="78.6">métal</w> ;</l>
						<l n="79" num="1.79"><w n="79.1">Emportez</w> <w n="79.2">votre</w> <w n="79.3">part</w> <w n="79.4">chacun</w> <w n="79.5">dans</w> <w n="79.6">votre</w> <w n="79.7">asile</w> :</l>
						<l n="80" num="1.80"><w n="80.1">Entre</w> <w n="80.2">tous</w> <w n="80.3">divisé</w>, <w n="80.4">cet</w> <w n="80.5">or</w> <w n="80.6">peut</w> <w n="80.7">être</w> <w n="80.8">utile</w> ;</l>
						<l n="81" num="1.81"><w n="81.1">Réuni</w> <w n="81.2">chez</w> <w n="81.3">un</w> <w n="81.4">seul</w>, <w n="81.5">il</w> <w n="81.6">ne</w> <w n="81.7">fait</w> <w n="81.8">que</w> <w n="81.9">du</w> <w n="81.10">mal</w>.</l>
						<l n="82" num="1.82"><space unit="char" quantity="8"></space><w n="82.1">Soyons</w> <w n="82.2">contens</w> <w n="82.3">du</w> <w n="82.4">nécessaire</w>,</l>
						<l n="83" num="1.83"><w n="83.1">Sans</w> <w n="83.2">jamais</w> <w n="83.3">souhaiter</w> <w n="83.4">de</w> <w n="83.5">trésors</w> <w n="83.6">superflus</w> :</l>
						<l n="84" num="1.84"><w n="84.1">Il</w> <w n="84.2">faut</w> <w n="84.3">les</w> <w n="84.4">redouter</w> <w n="84.5">autant</w> <w n="84.6">que</w> <w n="84.7">la</w> <w n="84.8">misère</w>,</l>
						<l n="85" num="1.85"><space unit="char" quantity="8"></space><w n="85.1">Comme</w> <w n="85.2">elle</w> <w n="85.3">ils</w> <w n="85.4">chassent</w> <w n="85.5">les</w> <w n="85.6">vertus</w>.</l>
					</lg>
				</div></body></text></TEI>