<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE SECOND</head><div type="poem" key="FLO44">
					<head type="number">FABLE XXII</head>
					<head type="main">Le Linot *</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Une</w> <w n="1.2">linotte</w> <w n="1.3">avait</w> <w n="1.4">un</w> <w n="1.5">fils</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qu</w>’<w n="2.2">elle</w> <w n="2.3">adorait</w> <w n="2.4">selon</w> <w n="2.5">l</w>’<w n="2.6">usage</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">était</w> <w n="3.3">l</w>’<w n="3.4">unique</w> <w n="3.5">fruit</w> <w n="3.6">du</w> <w n="3.7">plus</w> <w n="3.8">doux</w> <w n="3.9">mariage</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">le</w> <w n="4.3">plus</w> <w n="4.4">beau</w> <w n="4.5">linot</w> <w n="4.6">qui</w> <w n="4.7">fût</w> <w n="4.8">dans</w> <w n="4.9">le</w> <w n="4.10">pays</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Sa</w> <w n="5.2">mère</w> <w n="5.3">en</w> <w n="5.4">était</w> <w n="5.5">folle</w>, <w n="5.6">et</w> <w n="5.7">tous</w> <w n="5.8">les</w> <w n="5.9">témoignages</w></l>
						<l n="6" num="1.6"><w n="6.1">Que</w> <w n="6.2">peuvent</w> <w n="6.3">inventer</w> <w n="6.4">la</w> <w n="6.5">tendresse</w> <w n="6.6">et</w> <w n="6.7">l</w>’<w n="6.8">amour</w></l>
						<l n="7" num="1.7"><w n="7.1">Étaient</w> <w n="7.2">pour</w> <w n="7.3">cet</w> <w n="7.4">enfant</w> <w n="7.5">épuisés</w> <w n="7.6">chaque</w> <w n="7.7">jour</w>.</l>
						<l n="8" num="1.8"><w n="8.1">Notre</w> <w n="8.2">jeune</w> <w n="8.3">linot</w>, <w n="8.4">fier</w> <w n="8.5">de</w> <w n="8.6">ces</w> <w n="8.7">avantages</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Se</w> <w n="9.2">croyait</w> <w n="9.3">un</w> <w n="9.4">phénix</w>, <w n="9.5">prenait</w> <w n="9.6">l</w>’<w n="9.7">air</w> <w n="9.8">suffisant</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Tranchait</w> <w n="10.2">du</w> <w n="10.3">petit</w> <w n="10.4">important</w></l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Avec</w> <w n="11.2">les</w> <w n="11.3">oiseaux</w> <w n="11.4">de</w> <w n="11.5">son</w> <w n="11.6">âge</w> :</l>
						<l n="12" num="1.12"><w n="12.1">Persiflait</w> <w n="12.2">la</w> <w n="12.3">mésange</w> <w n="12.4">ou</w> <w n="12.5">bien</w> <w n="12.6">le</w> <w n="12.7">roitelet</w>,</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">Donnait</w> <w n="13.2">à</w> <w n="13.3">chacun</w> <w n="13.4">son</w> <w n="13.5">paquet</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">se</w> <w n="14.3">faisait</w> <w n="14.4">haïr</w> <w n="14.5">de</w> <w n="14.6">tout</w> <w n="14.7">le</w> <w n="14.8">voisinage</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Sa</w> <w n="15.2">mère</w> <w n="15.3">lui</w> <w n="15.4">disait</w> : <w n="15.5">Mon</w> <w n="15.6">cher</w> <w n="15.7">fils</w>, <w n="15.8">soit</w> <w n="15.9">plus</w> <w n="15.10">sage</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Plus</w> <w n="16.2">modeste</w> <w n="16.3">sur</w>-<w n="16.4">tout</w>. <w n="16.5">Hélas</w> ! <w n="16.6">je</w> <w n="16.7">conçois</w> <w n="16.8">bien</w></l>
						<l n="17" num="1.17"><w n="17.1">Les</w> <w n="17.2">dons</w>, <w n="17.3">les</w> <w n="17.4">qualités</w>, <w n="17.5">qui</w> <w n="17.6">furent</w> <w n="17.7">ton</w> <w n="17.8">partage</w> ;</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Mais</w> <w n="18.2">feignons</w> <w n="18.3">de</w> <w n="18.4">n</w>’<w n="18.5">en</w> <w n="18.6">savoir</w> <w n="18.7">rien</w>,</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Pour</w> <w n="19.2">qu</w>’<w n="19.3">on</w> <w n="19.4">les</w> <w n="19.5">aime</w> <w n="19.6">davantage</w>.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">A</w> <w n="20.2">tout</w> <w n="20.3">cela</w> <w n="20.4">notre</w> <w n="20.5">linot</w></l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">Répondait</w> <w n="21.2">par</w> <w n="21.3">quelque</w> <w n="21.4">bon</w> <w n="21.5">mot</w> ;</l>
						<l n="22" num="1.22"><w n="22.1">La</w> <w n="22.2">mère</w> <w n="22.3">en</w> <w n="22.4">gémissait</w> <w n="22.5">dans</w> <w n="22.6">le</w> <w n="22.7">fond</w> <w n="22.8">de</w> <w n="22.9">son</w> <w n="22.10">âme</w>.</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">Un</w> <w n="23.2">vieux</w> <w n="23.3">merle</w>, <w n="23.4">ami</w> <w n="23.5">de</w> <w n="23.6">la</w> <w n="23.7">dame</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Lui</w> <w n="24.2">dit</w> : <w n="24.3">Laissez</w> <w n="24.4">aller</w> <w n="24.5">votre</w> <w n="24.6">fils</w> <w n="24.7">au</w> <w n="24.8">grand</w> <w n="24.9">bois</w>,</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Je</w> <w n="25.2">vous</w> <w n="25.3">réponds</w> <w n="25.4">qu</w>’<w n="25.5">avant</w> <w n="25.6">un</w> <w n="25.7">mois</w></l>
						<l n="26" num="1.26"><w n="26.1">Il</w> <w n="26.2">sera</w> <w n="26.3">sans</w> <w n="26.4">défauts</w>. <w n="26.5">Vous</w> <w n="26.6">jugez</w> <w n="26.7">des</w> <w n="26.8">alarmes</w></l>
						<l n="27" num="1.27"><w n="27.1">De</w> <w n="27.2">la</w> <w n="27.3">mère</w>, <w n="27.4">qui</w> <w n="27.5">pleure</w> <w n="27.6">et</w> <w n="27.7">frémit</w> <w n="27.8">du</w> <w n="27.9">danger</w> ;</l>
						<l n="28" num="1.28"><w n="28.1">Mais</w> <w n="28.2">le</w> <w n="28.3">jeune</w> <w n="28.4">linot</w> <w n="28.5">brûlait</w> <w n="28.6">de</w> <w n="28.7">voyager</w>,</l>
						<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">Il</w> <w n="29.2">partit</w> <w n="29.3">donc</w> <w n="29.4">malgré</w> <w n="29.5">les</w> <w n="29.6">larmes</w>.</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">A</w> <w n="30.2">peine</w> <w n="30.3">est</w>-<w n="30.4">il</w> <w n="30.5">dans</w> <w n="30.6">la</w> <w n="30.7">forêt</w>,</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space><w n="31.1">Que</w> <w n="31.2">notre</w> <w n="31.3">petit</w> <w n="31.4">personnage</w></l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space><w n="32.1">Du</w> <w n="32.2">pivert</w> <w n="32.3">entend</w> <w n="32.4">le</w> <w n="32.5">ramage</w>,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space><w n="33.1">Et</w> <w n="33.2">se</w> <w n="33.3">moque</w> <w n="33.4">de</w> <w n="33.5">son</w> <w n="33.6">fausset</w>.</l>
						<l n="34" num="1.34"><w n="34.1">Le</w> <w n="34.2">pivert</w>, <w n="34.3">qui</w> <w n="34.4">prit</w> <w n="34.5">mal</w> <w n="34.6">cette</w> <w n="34.7">plaisanterie</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Vient</w> <w n="35.2">à</w> <w n="35.3">bons</w> <w n="35.4">coups</w> <w n="35.5">de</w> <w n="35.6">bec</w> <w n="35.7">plumer</w> <w n="35.8">le</w> <w n="35.9">persifleur</w> ;</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space><w n="36.1">Et</w>, <w n="36.2">deux</w> <w n="36.3">jours</w> <w n="36.4">après</w>, <w n="36.5">une</w> <w n="36.6">pie</w></l>
						<l n="37" num="1.37"><w n="37.1">Le</w> <w n="37.2">dégoûte</w> <w n="37.3">à</w> <w n="37.4">jamais</w> <w n="37.5">du</w> <w n="37.6">métier</w> <w n="37.7">de</w> <w n="37.8">railleur</w>.</l>
						<l n="38" num="1.38"><w n="38.1">Il</w> <w n="38.2">lui</w> <w n="38.3">restait</w> <w n="38.4">encor</w> <w n="38.5">la</w> <w n="38.6">vanité</w> <w n="38.7">secrète</w></l>
						<l n="39" num="1.39"><space unit="char" quantity="8"></space><w n="39.1">De</w> <w n="39.2">se</w> <w n="39.3">croire</w> <w n="39.4">excellent</w> <w n="39.5">chanteur</w> ;</l>
						<l n="40" num="1.40"><space unit="char" quantity="8"></space><w n="40.1">Le</w> <w n="40.2">rossignol</w> <w n="40.3">et</w> <w n="40.4">la</w> <w n="40.5">fauvette</w></l>
						<l n="41" num="1.41"><space unit="char" quantity="8"></space><w n="41.1">Le</w> <w n="41.2">guérirent</w> <w n="41.3">de</w> <w n="41.4">son</w> <w n="41.5">erreur</w>.</l>
						<l n="42" num="1.42"><space unit="char" quantity="8"></space><w n="42.1">Bref</w>, <w n="42.2">il</w> <w n="42.3">retourna</w>, <w n="42.4">chez</w> <w n="42.5">sa</w> <w n="42.6">mère</w>,</l>
						<l n="43" num="1.43"><space unit="char" quantity="8"></space><w n="43.1">Doux</w>, <w n="43.2">poli</w>, <w n="43.3">modeste</w> <w n="43.4">et</w> <w n="43.5">charmant</w>.</l>
					</lg>
					<lg n="2">
						<l n="44" num="2.1"><w n="44.1">Ainsi</w> <w n="44.2">l</w>’<w n="44.3">adversité</w> <w n="44.4">fit</w>, <w n="44.5">dans</w> <w n="44.6">un</w> <w n="44.7">seul</w> <w n="44.8">moment</w>,</l>
						<l n="45" num="2.2"><w n="45.1">Ce</w> <w n="45.2">que</w> <w n="45.3">tant</w> <w n="45.4">de</w> <w n="45.5">leçons</w> <w n="45.6">n</w>’<w n="45.7">avaient</w> <w n="45.8">jamais</w> <w n="45.9">pu</w> <w n="45.10">faire</w>.</l>
					</lg>
				</div></body></text></TEI>