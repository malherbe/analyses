<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="FLO69">
					<head type="number">FABLE III</head>
					<head type="main">Le Perroquet</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">gros</w> <w n="1.3">perroquet</w> <w n="1.4">gris</w>, <w n="1.5">échappé</w> <w n="1.6">de</w> <w n="1.7">sa</w> <w n="1.8">cage</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Vint</w> <w n="2.2">s</w>’<w n="2.3">établir</w> <w n="2.4">dans</w> <w n="2.5">un</w> <w n="2.6">bocage</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">là</w>, <w n="3.3">prenant</w> <w n="3.4">le</w> <w n="3.5">ton</w> <w n="3.6">de</w> <w n="3.7">nos</w> <w n="3.8">faux</w> <w n="3.9">connaisseurs</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Jugeant</w> <w n="4.2">tout</w>, <w n="4.3">blâmant</w> <w n="4.4">tout</w> <w n="4.5">d</w>’<w n="4.6">un</w> <w n="4.7">air</w> <w n="4.8">de</w> <w n="4.9">suffisance</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Au</w> <w n="5.2">chant</w> <w n="5.3">du</w> <w n="5.4">rossignol</w> <w n="5.5">il</w> <w n="5.6">trouvait</w> <w n="5.7">des</w> <w n="5.8">longueurs</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Critiquait</w> <w n="6.2">sur</w>-<w n="6.3">tout</w> <w n="6.4">sa</w> <w n="6.5">cadence</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">linot</w>, <w n="7.3">selon</w> <w n="7.4">lui</w>, <w n="7.5">ne</w> <w n="7.6">savait</w> <w n="7.7">pas</w> <w n="7.8">chanter</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">fauvette</w> <w n="8.3">aurait</w> <w n="8.4">fait</w> <w n="8.5">quelque</w> <w n="8.6">chose</w> <w n="8.7">peut</w>-<w n="8.8">être</w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="4"></space><w n="9.1">Si</w> <w n="9.2">de</w> <w n="9.3">bonne</w> <w n="9.4">heure</w> <w n="9.5">il</w> <w n="9.6">eût</w> <w n="9.7">été</w> <w n="9.8">son</w> <w n="9.9">maître</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Et</w> <w n="10.2">qu</w>’<w n="10.3">elle</w> <w n="10.4">eût</w> <w n="10.5">voulu</w> <w n="10.6">profiter</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Enfin</w> <w n="11.2">aucun</w> <w n="11.3">oiseau</w> <w n="11.4">n</w>’<w n="11.5">avait</w> <w n="11.6">l</w>’<w n="11.7">art</w> <w n="11.8">de</w> <w n="11.9">lui</w> <w n="11.10">plaire</w> ;</l>
						<l n="12" num="1.12"><w n="12.1">Et</w>, <w n="12.2">dès</w> <w n="12.3">qu</w>’<w n="12.4">ils</w> <w n="12.5">commençaient</w> <w n="12.6">leurs</w> <w n="12.7">joyeuses</w> <w n="12.8">chansons</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Par</w> <w n="13.2">des</w> <w n="13.3">coups</w> <w n="13.4">de</w> <w n="13.5">sifflet</w> <w n="13.6">répondant</w> <w n="13.7">à</w> <w n="13.8">leurs</w> <w n="13.9">sons</w>,</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">Le</w> <w n="14.2">perroquet</w> <w n="14.3">les</w> <w n="14.4">faisait</w> <w n="14.5">taire</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Lassés</w> <w n="15.2">de</w> <w n="15.3">tant</w> <w n="15.4">d</w>’<w n="15.5">affronts</w>, <w n="15.6">tous</w> <w n="15.7">les</w> <w n="15.8">oiseaux</w> <w n="15.9">du</w> <w n="15.10">bois</w></l>
						<l n="16" num="1.16"><w n="16.1">Viennent</w> <w n="16.2">lui</w> <w n="16.3">dire</w> <w n="16.4">un</w> <w n="16.5">jour</w> : <w n="16.6">Mais</w> <w n="16.7">parlez</w> <w n="16.8">donc</w>, <w n="16.9">beau</w> <w n="16.10">sire</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Vous</w> <w n="17.2">qui</w> <w n="17.3">sifflez</w> <w n="17.4">toujours</w>, <w n="17.5">faites</w> <w n="17.6">qu</w>’<w n="17.7">on</w> <w n="17.8">vous</w> <w n="17.9">admire</w> ;</l>
						<l n="18" num="1.18"><w n="18.1">Sans</w> <w n="18.2">doute</w> <w n="18.3">vous</w> <w n="18.4">avez</w> <w n="18.5">une</w> <w n="18.6">brillante</w> <w n="18.7">voix</w>,</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Daignez</w> <w n="19.2">chanter</w> <w n="19.3">pour</w> <w n="19.4">nous</w> <w n="19.5">instruire</w>.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">Le</w> <w n="20.2">perroquet</w>, <w n="20.3">dans</w> <w n="20.4">l</w>’<w n="20.5">embarras</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Se</w> <w n="21.2">gratte</w> <w n="21.3">un</w> <w n="21.4">peu</w> <w n="21.5">la</w> <w n="21.6">tête</w>, <w n="21.7">et</w> <w n="21.8">finit</w> <w n="21.9">par</w> <w n="21.10">leur</w> <w n="21.11">dire</w> :</l>
						<l n="22" num="1.22"><w n="22.1">Messieurs</w>, <w n="22.2">je</w> <w n="22.3">siffle</w> <w n="22.4">bien</w>, <w n="22.5">mais</w> <w n="22.6">je</w> <w n="22.7">ne</w> <w n="22.8">chante</w> <w n="22.9">pas</w>.</l>
					</lg>
				</div></body></text></TEI>