<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="FLO61">
					<head type="number">FABLE XVII</head>
					<head type="main">Le Hibou, le Chat, <lb></lb>l’Oison et le Rat</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">jeunes</w> <w n="1.3">écoliers</w> <w n="1.4">avaient</w> <w n="1.5">pris</w> <w n="1.6">dans</w> <w n="1.7">un</w> <w n="1.8">trou</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="18"></space><w n="2.1">Un</w> <w n="2.2">hibou</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">l</w>’<w n="3.3">avaient</w> <w n="3.4">élevé</w> <w n="3.5">dans</w> <w n="3.6">la</w> <w n="3.7">cour</w> <w n="3.8">du</w> <w n="3.9">collège</w>.</l>
						<l n="4" num="1.4"><space unit="char" quantity="10"></space><w n="4.1">Un</w> <w n="4.2">vieux</w> <w n="4.3">chat</w>, <w n="4.4">un</w> <w n="4.5">jeune</w> <w n="4.6">oison</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Nourris</w> <w n="5.2">par</w> <w n="5.3">le</w> <w n="5.4">portier</w>, <w n="5.5">étaient</w> <w n="5.6">en</w> <w n="5.7">liaison</w></l>
						<l n="6" num="1.6"><w n="6.1">Avec</w> <w n="6.2">l</w>’<w n="6.3">oiseau</w> ; <w n="6.4">tous</w> <w n="6.5">trois</w> <w n="6.6">avaient</w> <w n="6.7">le</w> <w n="6.8">privilège</w></l>
						<l n="7" num="1.7"><w n="7.1">D</w>’<w n="7.2">aller</w> <w n="7.3">et</w> <w n="7.4">de</w> <w n="7.5">venir</w> <w n="7.6">par</w> <w n="7.7">toute</w> <w n="7.8">la</w> <w n="7.9">maison</w>.</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">A</w> <w n="8.2">force</w> <w n="8.3">d</w>’<w n="8.4">être</w> <w n="8.5">dans</w> <w n="8.6">la</w> <w n="8.7">classe</w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Ils</w> <w n="9.2">avaient</w> <w n="9.3">orné</w> <w n="9.4">leur</w> <w n="9.5">esprit</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="4"></space><w n="10.1">Savaient</w> <w n="10.2">par</w> <w n="10.3">cœur</w> <w n="10.4">Denys</w> <w n="10.5">d</w>’<w n="10.6">Halicarnasse</w></l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">tout</w> <w n="11.3">ce</w> <w n="11.4">qu</w>’<w n="11.5">Hérodote</w> <w n="11.6">et</w> <w n="11.7">Tite</w>-<w n="11.8">Live</w> <w n="11.9">ont</w> <w n="11.10">dit</w>.</l>
						<l n="12" num="1.12"><w n="12.1">Un</w> <w n="12.2">soir</w>, <w n="12.3">en</w> <w n="12.4">disputant</w>, (<w n="12.5">des</w> <w n="12.6">docteurs</w> <w n="12.7">c</w>’<w n="12.8">est</w> <w n="12.9">l</w>’<w n="12.10">usage</w>)</l>
						<l n="13" num="1.13"><w n="13.1">Ils</w> <w n="13.2">comparaient</w> <w n="13.3">entre</w> <w n="13.4">eux</w> <w n="13.5">les</w> <w n="13.6">peuples</w> <w n="13.7">anciens</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Ma</w> <w n="14.2">foi</w>, <w n="14.3">disait</w> <w n="14.4">le</w> <w n="14.5">chat</w>, <w n="14.6">c</w>’<w n="14.7">est</w> <w n="14.8">aux</w> <w n="14.9">Égyptiens</w></l>
						<l n="15" num="1.15"><w n="15.1">Que</w> <w n="15.2">je</w> <w n="15.3">donne</w> <w n="15.4">le</w> <w n="15.5">prix</w> : <w n="15.6">c</w>’<w n="15.7">était</w> <w n="15.8">un</w> <w n="15.9">peuple</w> <w n="15.10">sage</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Un</w> <w n="16.2">peuple</w> <w n="16.3">ami</w> <w n="16.4">des</w> <w n="16.5">lois</w>, <w n="16.6">instruit</w>, <w n="16.7">discret</w>, <w n="16.8">pieux</w>,</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1">Rempli</w> <w n="17.2">de</w> <w n="17.3">respect</w> <w n="17.4">pour</w> <w n="17.5">ses</w> <w n="17.6">dieux</w> ;</l>
						<l n="18" num="1.18"><w n="18.1">Cela</w> <w n="18.2">seul</w> <w n="18.3">à</w> <w n="18.4">mon</w> <w n="18.5">gré</w> <w n="18.6">lui</w> <w n="18.7">donne</w> <w n="18.8">l</w>’<w n="18.9">avantage</w>.</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">J</w>’<w n="19.2">aime</w> <w n="19.3">mieux</w> <w n="19.4">les</w> <w n="19.5">Athéniens</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Répondait</w> <w n="20.2">le</w> <w n="20.3">hibou</w> : <w n="20.4">que</w> <w n="20.5">d</w>’<w n="20.6">esprit</w> ! <w n="20.7">que</w> <w n="20.8">de</w> <w n="20.9">grace</w> !</l>
						<l n="21" num="1.21"><space unit="char" quantity="8"></space><w n="21.1">Et</w> <w n="21.2">dans</w> <w n="21.3">les</w> <w n="21.4">combats</w> <w n="21.5">quelle</w> <w n="21.6">audace</w> !</l>
						<l n="22" num="1.22"><w n="22.1">Que</w> <w n="22.2">d</w>’<w n="22.3">aimables</w> <w n="22.4">héros</w> <w n="22.5">parmi</w> <w n="22.6">leurs</w> <w n="22.7">citoyens</w> !</l>
						<l n="23" num="1.23"><w n="23.1">A</w>-<w n="23.2">t</w>-<w n="23.3">on</w> <w n="23.4">jamais</w> <w n="23.5">plus</w> <w n="23.6">fait</w> <w n="23.7">avec</w> <w n="23.8">moins</w> <w n="23.9">de</w> <w n="23.10">moyens</w> ?</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Des</w> <w n="24.2">nations</w> <w n="24.3">c</w>’<w n="24.4">est</w> <w n="24.5">la</w> <w n="24.6">première</w>.</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Parbleu</w> ! <w n="25.2">dit</w> <w n="25.3">l</w>’<w n="25.4">oison</w> <w n="25.5">en</w> <w n="25.6">colère</w>,</l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Messieurs</w>, <w n="26.2">je</w> <w n="26.3">vous</w> <w n="26.4">trouve</w> <w n="26.5">plaisans</w> :</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">Et</w> <w n="27.2">les</w> <w n="27.3">Romains</w>, <w n="27.4">que</w> <w n="27.5">vous</w> <w n="27.6">en</w> <w n="27.7">semble</w> ?</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space><w n="28.1">Est</w>-<w n="28.2">il</w> <w n="28.3">un</w> <w n="28.4">peuple</w> <w n="28.5">qui</w> <w n="28.6">rassemble</w></l>
						<l n="29" num="1.29"><w n="29.1">Plus</w> <w n="29.2">de</w> <w n="29.3">grandeur</w>, <w n="29.4">de</w> <w n="29.5">gloire</w>, <w n="29.6">et</w> <w n="29.7">de</w> <w n="29.8">faits</w> <w n="29.9">éclatans</w> ?</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">Dans</w> <w n="30.2">les</w> <w n="30.3">arts</w>, <w n="30.4">comme</w> <w n="30.5">dans</w> <w n="30.6">la</w> <w n="30.7">guerre</w>,</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space><w n="31.1">Ils</w> <w n="31.2">ont</w> <w n="31.3">surpassé</w> <w n="31.4">vos</w> <w n="31.5">amis</w>.</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space><w n="32.1">Pour</w> <w n="32.2">moi</w>, <w n="32.3">ce</w> <w n="32.4">sont</w> <w n="32.5">mes</w> <w n="32.6">favoris</w> :</l>
						<l n="33" num="1.33"><w n="33.1">Tout</w> <w n="33.2">doit</w> <w n="33.3">céder</w> <w n="33.4">le</w> <w n="33.5">pas</w> <w n="33.6">aux</w> <w n="33.7">vainqueurs</w> <w n="33.8">de</w> <w n="33.9">la</w> <w n="33.10">terre</w>.</l>
						<l n="34" num="1.34"><w n="34.1">Chacun</w> <w n="34.2">des</w> <w n="34.3">trois</w> <w n="34.4">pédans</w> <w n="34.5">s</w>’<w n="34.6">obstine</w> <w n="34.7">en</w> <w n="34.8">son</w> <w n="34.9">avis</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Quand</w> <w n="35.2">un</w> <w n="35.3">rat</w>, <w n="35.4">qui</w> <w n="35.5">de</w> <w n="35.6">loin</w> <w n="35.7">entendait</w> <w n="35.8">la</w> <w n="35.9">dispute</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Rat</w> <w n="36.2">savant</w>, <w n="36.3">qui</w> <w n="36.4">mangeait</w> <w n="36.5">des</w> <w n="36.6">thêmes</w> <w n="36.7">dans</w> <w n="36.8">sa</w> <w n="36.9">hutte</w>,</l>
						<l n="37" num="1.37"><w n="37.1">Leur</w> <w n="37.2">cria</w> : <w n="37.3">Je</w> <w n="37.4">vois</w> <w n="37.5">bien</w> <w n="37.6">d</w>’<w n="37.7">où</w> <w n="37.8">viennent</w> <w n="37.9">vos</w> <w n="37.10">débats</w> :</l>
						<l n="38" num="1.38"><space unit="char" quantity="8"></space><w n="38.1">L</w>’<w n="38.2">Égypte</w> <w n="38.3">vénérait</w> <w n="38.4">les</w> <w n="38.5">chats</w>,</l>
						<l n="39" num="1.39"><w n="39.1">Athènes</w> <w n="39.2">les</w> <w n="39.3">hibous</w>, <w n="39.4">et</w> <w n="39.5">Rome</w>, <w n="39.6">au</w> <w n="39.7">capitole</w>,</l>
						<l n="40" num="1.40"><w n="40.1">Aux</w> <w n="40.2">dépens</w> <w n="40.3">de</w> <w n="40.4">l</w>’<w n="40.5">état</w>, <w n="40.6">nourrissait</w> <w n="40.7">des</w> <w n="40.8">oisons</w> :</l>
						<l n="41" num="1.41"><w n="41.1">Ainsi</w> <w n="41.2">notre</w> <w n="41.3">intérêt</w> <w n="41.4">est</w> <w n="41.5">toujours</w> <w n="41.6">la</w> <w n="41.7">boussole</w></l>
						<l n="42" num="1.42"><space unit="char" quantity="8"></space><w n="42.1">Que</w> <w n="42.2">suivent</w> <w n="42.3">nos</w> <w n="42.4">opinions</w>.</l>
					</lg>
				</div></body></text></TEI>