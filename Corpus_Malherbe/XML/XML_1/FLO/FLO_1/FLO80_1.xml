<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Fables de Florian</title>
				<title type="medium">Édition électronique</title>
				<author key="FLO">
					<name>
						<forename>Jean-Pierre Claris</forename>
						<nameLink>de</nameLink>
						<surname>FLORIAN</surname>
					</name>
					<date from="1755" to="1794">1755-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Version initiale du texte électronique (Project Gutenberg)</resp>
					<name id="LV">
						<forename>Laurent</forename>
						<surname>Vogel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3910 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">FLO_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Fables de Florian</title>
						<author>Jean-Pierre Claris de Florian</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">https://www.gutenberg.org/ebooks/58251</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Fables de Florian</title>
								<title>MISES DANS UN NOUVEL ORDRE</title>
								<author>Jean-Pierre Claris de Florian</author>
								<edition>Louis-François Jauffret</edition>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k571677</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LA LIBRAIRIE ÉCONOMIQUE</publisher>
									<date when="1801">1800-1801</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1793">1793</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’est pas reprise dans cette édition électronique.</p>
				<p>L’astérisque qui précède le titre des fables ajoutées par l’éditeur a été placé en fin de titre.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les notes de fin de page ont été reportées en fin de poème.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="FLO80">
					<head type="number">FABLE XIV</head>
					<head type="main">Pan et la Fortune</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">jeune</w> <w n="1.3">grand</w> <w n="1.4">seigneur</w> <w n="1.5">à</w> <w n="1.6">des</w> <w n="1.7">jeux</w> <w n="1.8">de</w> <w n="1.9">hasard</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Avait</w> <w n="2.2">perdu</w> <w n="2.3">sa</w> <w n="2.4">dernière</w> <w n="2.5">pistole</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Et</w> <w n="3.2">puis</w> <w n="3.3">joué</w> <w n="3.4">sur</w> <w n="3.5">sa</w> <w n="3.6">parole</w> ;</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Il</w> <w n="4.2">fallait</w> <w n="4.3">payer</w> <w n="4.4">sans</w> <w n="4.5">retard</w> :</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Les</w> <w n="5.2">dettes</w> <w n="5.3">du</w> <w n="5.4">jeu</w> <w n="5.5">sont</w> <w n="5.6">sacrées</w>.</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">On</w> <w n="6.2">peut</w> <w n="6.3">faire</w> <w n="6.4">attendre</w> <w n="6.5">un</w> <w n="6.6">marchand</w>,</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Un</w> <w n="7.2">ouvrier</w>, <w n="7.3">un</w> <w n="7.4">indigent</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Qui</w> <w n="8.2">nous</w> <w n="8.3">a</w> <w n="8.4">fourni</w> <w n="8.5">ses</w> <w n="8.6">denrées</w> ;</l>
						<l n="9" num="1.9"><w n="9.1">Mais</w> <w n="9.2">un</w> <w n="9.3">escroc</w> ? <w n="9.4">l</w>’<w n="9.5">honneur</w> <w n="9.6">veut</w> <w n="9.7">qu</w>’<w n="9.8">au</w> <w n="9.9">même</w> <w n="9.10">moment</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">On</w> <w n="10.2">le</w> <w n="10.3">paye</w>, <w n="10.4">et</w> <w n="10.5">très</w> <w n="10.6">poliment</w>.</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">La</w> <w n="11.2">loi</w> <w n="11.3">par</w> <w n="11.4">eux</w> <w n="11.5">fut</w> <w n="11.6">ainsi</w> <w n="11.7">faite</w>.</l>
						<l n="12" num="1.12"><w n="12.1">Notre</w> <w n="12.2">jeune</w> <w n="12.3">seigneur</w>, <w n="12.4">pour</w> <w n="12.5">acquitter</w> <w n="12.6">sa</w> <w n="12.7">dette</w>,</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">Ordonne</w> <w n="13.2">une</w> <w n="13.3">coupe</w> <w n="13.4">de</w> <w n="13.5">bois</w>.</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">Aussitôt</w> <w n="14.2">les</w> <w n="14.3">ormes</w>, <w n="14.4">les</w> <w n="14.5">frênes</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">les</w> <w n="15.3">hêtres</w> <w n="15.4">touffus</w>, <w n="15.5">et</w> <w n="15.6">les</w> <w n="15.7">antiques</w> <w n="15.8">chênes</w>,</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">Tombent</w> <w n="16.2">l</w>’<w n="16.3">un</w> <w n="16.4">sur</w> <w n="16.5">l</w>’<w n="16.6">autre</w> <w n="16.7">à</w> <w n="16.8">la</w> <w n="16.9">fois</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Les</w> <w n="17.2">faunes</w>, <w n="17.3">les</w> <w n="17.4">sylvains</w>, <w n="17.5">désertent</w> <w n="17.6">les</w> <w n="17.7">bocages</w> ;</l>
						<l n="18" num="1.18"><w n="18.1">Les</w> <w n="18.2">dryades</w> <w n="18.3">en</w> <w n="18.4">pleurs</w> <w n="18.5">regrettent</w> <w n="18.6">leurs</w> <w n="18.7">ombrages</w> ;</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Et</w> <w n="19.2">le</w> <w n="19.3">dieu</w> <w n="19.4">Pan</w>, <w n="19.5">dans</w> <w n="19.6">sa</w> <w n="19.7">fureur</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Instruit</w> <w n="20.2">que</w> <w n="20.3">le</w> <w n="20.4">jeu</w> <w n="20.5">seul</w> <w n="20.6">a</w> <w n="20.7">causé</w> <w n="20.8">ces</w> <w n="20.9">ravages</w>,</l>
						<l n="21" num="1.21"><w n="21.1">S</w>’<w n="21.2">en</w> <w n="21.3">prend</w> <w n="21.4">à</w> <w n="21.5">la</w> <w n="21.6">Fortune</w> : <w n="21.7">O</w> <w n="21.8">mère</w> <w n="21.9">du</w> <w n="21.10">malheur</w>,</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1">Dit</w>-<w n="22.2">il</w>, <w n="22.3">infernale</w> <w n="22.4">furie</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Tu</w> <w n="23.2">troubles</w> <w n="23.3">à</w> <w n="23.4">la</w> <w n="23.5">fois</w> <w n="23.6">les</w> <w n="23.7">mortels</w> <w n="23.8">et</w> <w n="23.9">les</w> <w n="23.10">dieux</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Tu</w> <w n="24.2">te</w> <w n="24.3">plais</w> <w n="24.4">dans</w> <w n="24.5">le</w> <w n="24.6">mal</w>, <w n="24.7">et</w> <w n="24.8">ta</w> <w n="24.9">rage</w> <w n="24.10">ennemie</w>…</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Il</w> <w n="25.2">parlait</w>, <w n="25.3">lorsque</w> <w n="25.4">dans</w> <w n="25.5">ces</w> <w n="25.6">lieux</w></l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Tout</w>-<w n="26.2">à</w>-<w n="26.3">coup</w> <w n="26.4">paraît</w> <w n="26.5">la</w> <w n="26.6">déesse</w>.</l>
						<l n="27" num="1.27"><w n="27.1">Calme</w>, <w n="27.2">dit</w>-<w n="27.3">elle</w> <w n="27.4">à</w> <w n="27.5">Pan</w>, <w n="27.6">le</w> <w n="27.7">chagrin</w> <w n="27.8">qui</w> <w n="27.9">te</w> <w n="27.10">presse</w> ;</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space><w n="28.1">Je</w> <w n="28.2">n</w>’<w n="28.3">ai</w> <w n="28.4">point</w> <w n="28.5">causé</w> <w n="28.6">tes</w> <w n="28.7">malheurs</w> :</l>
						<l n="29" num="1.29"><w n="29.1">Même</w> <w n="29.2">aux</w> <w n="29.3">jeux</w> <w n="29.4">de</w> <w n="29.5">hasard</w>, <w n="29.6">avec</w> <w n="29.7">certains</w> <w n="29.8">joueurs</w>,</l>
						<l n="30" num="1.30"><space unit="char" quantity="4"></space><w n="30.1">Je</w> <w n="30.2">ne</w> <w n="30.3">fais</w> <w n="30.4">rien</w>. — <w n="30.5">Qui</w> <w n="30.6">donc</w> <w n="30.7">fait</w> <w n="30.8">tout</w> ? — <w n="30.9">L</w>’<w n="30.10">adresse</w>.</l>
					</lg>
				</div></body></text></TEI>