<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉGLOGUES</head><div type="poem" key="CHE59">
					<head type="number">XXV</head>
					<head type="main">Le Bouvier</head>
					<lg n="1">
						<l ana="incomplete" where="M" n="1" num="1.1"><w n="1.1">Reste</w> <w n="1.2">ici</w>, <w n="1.3">Pardalis</w> ; ............... <w n="1.4">vagabonde</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">il</w> <w n="2.3">ne</w> <w n="2.4">me</w> <w n="2.5">faille</w> <w n="2.6">encor</w>, <w n="2.7">dans</w> <w n="2.8">la</w> <w n="2.9">forêt</w> <w n="2.10">profonde</w>,</l>
						<l rhyme="none" ana="incomplete" where="M" n="3" num="1.3"><w n="3.1">Suivre</w> <w n="3.2">pour</w> <w n="3.3">te</w> <w n="3.4">chercher</w> … <w n="3.5">la</w> <w n="3.6">cloche</w> <w n="3.7">d</w>’<w n="3.8">argent</w></l>
						<l ana="incomplete" where="F" n="4" num="1.4"><w n="4.1">Dont</w> <w n="4.2">j</w>’<w n="4.3">ai</w> <w n="4.4">su</w> <w n="4.5">te</w> <w n="4.6">parer</w> ......................................................</l>
						<l n="5" num="1.5"><w n="5.1">Reste</w>, <w n="5.2">ma</w> <w n="5.3">Pardalis</w>. <w n="5.4">Viens</w>, <w n="5.5">ma</w> <w n="5.6">belle</w> <w n="5.7">génisse</w>.</l>
						<l ana="incomplete" where="M" n="6" num="1.6"><w n="6.1">Ici</w> <w n="6.2">croît</w> ........................................... <w n="6.3">le</w> <w n="6.4">narcisse</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Reste</w> ; <w n="7.2">si</w> <w n="7.3">tu</w> <w n="7.4">me</w> <w n="7.5">fuis</w>, <w n="7.6">tu</w> <w n="7.7">n</w>’<w n="7.8">auras</w> <w n="7.9">plus</w> <w n="7.10">ma</w> <w n="7.11">main</w></l>
						<l n="8" num="1.8"><w n="8.1">Pour</w> <w n="8.2">y</w> <w n="8.3">venir</w> <w n="8.4">trouver</w> <w n="8.5">ou</w> <w n="8.6">du</w> <w n="8.7">sel</w> <w n="8.8">ou</w> <w n="8.9">du</w> <w n="8.10">pain</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Tu</w> <w n="9.2">ne</w> <w n="9.3">bondiras</w> <w n="9.4">plus</w> <w n="9.5">aux</w> <w n="9.6">chants</w> <w n="9.7">de</w> <w n="9.8">ma</w> <w n="9.9">musette</w>.</l>
						<l n="10" num="1.10"><w n="10.1">Un</w> <w n="10.2">ivoire</w> <w n="10.3">élégant</w> <w n="10.4">se</w> <w n="10.5">courbe</w> <w n="10.6">sur</w> <w n="10.7">ta</w> <w n="10.8">tête</w>.</l>
						<l rhyme="none" n="11" num="1.11"><w n="11.1">Ton</w> <w n="11.2">regard</w> <w n="11.3">est</w> <w n="11.4">serein</w>, <w n="11.5">tu</w> <w n="11.6">mugis</w> <w n="11.7">doucement</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Ton</w> <w n="12.2">lait</w> <w n="12.3">est</w> <w n="12.4">le</w> <w n="12.5">plus</w> <w n="12.6">doux</w> <w n="12.7">qu</w>’<w n="12.8">un</w> <w n="12.9">sein</w> <w n="12.10">pur</w> <w n="12.11">et</w> <w n="12.12">fertile</w></l>
						<l n="13" num="1.13"><w n="13.1">Ait</w> <w n="13.2">jamais</w> <w n="13.3">fait</w> <w n="13.4">couler</w> <w n="13.5">dans</w> <w n="13.6">mon</w> <w n="13.7">vase</w> <w n="13.8">d</w>’<w n="13.9">argile</w>.</l>
						<l n="14" num="1.14"><w n="14.1">La</w> <w n="14.2">fille</w> <w n="14.3">d</w>’<w n="14.4">Inachus</w>, <w n="14.5">quand</w> <w n="14.6">le</w> <w n="14.7">maître</w> <w n="14.8">des</w> <w n="14.9">dieux</w></l>
						<l n="15" num="1.15"><w n="15.1">La</w> <w n="15.2">fit</w> <w n="15.3">mugir</w> <w n="15.4">aussi</w> <w n="15.5">près</w> <w n="15.6">du</w> <w n="15.7">pâtre</w> <w n="15.8">aux</w> <w n="15.9">cent</w> <w n="15.10">yeux</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Était</w> <w n="16.2">moins</w> <w n="16.3">que</w> <w n="16.4">toi</w> <w n="16.5">belle</w> <w n="16.6">et</w> <w n="16.7">de</w> <w n="16.8">grâces</w> <w n="16.9">ornée</w> ;</l>
						<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">pourtant</w>, <w n="17.3">près</w> <w n="17.4">du</w> <w n="17.5">Nil</w>, <w n="17.6">de</w> <w n="17.7">lotos</w> <w n="17.8">couronnée</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Elle</w> <w n="18.2">voit</w> <w n="18.3">aujourd</w>’<w n="18.4">hui</w>, <w n="18.5">dans</w> <w n="18.6">son</w> <w n="18.7">temple</w> <w n="18.8">divin</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Ses</w> <w n="19.2">prêtres</w> <w n="19.3">revêtir</w> <w n="19.4">des</w> <w n="19.5">tuniques</w> <w n="19.6">de</w> <w n="19.7">lin</w>.</l>
					</lg>
				</div></body></text></TEI>