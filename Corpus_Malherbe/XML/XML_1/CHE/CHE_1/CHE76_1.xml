<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS</head><div type="poem" key="CHE76">
					<head type="number">XI</head>
					<head type="main">Traduction d’Horace</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">jour</w> <w n="1.3">le</w> <w n="1.4">rat</w> <w n="1.5">des</w> <w n="1.6">champs</w>, <w n="1.7">ami</w> <w n="1.8">du</w> <w n="1.9">rat</w> <w n="1.10">de</w> <w n="1.11">ville</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Invita</w> <w n="2.2">son</w> <w n="2.3">ami</w> <w n="2.4">dans</w> <w n="2.5">son</w> <w n="2.6">rustique</w> <w n="2.7">asile</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">était</w> <w n="3.3">économe</w> <w n="3.4">et</w> <w n="3.5">soigneux</w> <w n="3.6">de</w> <w n="3.7">son</w> <w n="3.8">bien</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">l</w>’<w n="4.3">hospitalité</w>, <w n="4.4">leur</w> <w n="4.5">antique</w> <w n="4.6">lien</w></l>
						<l n="5" num="1.5"><w n="5.1">Fit</w> <w n="5.2">les</w> <w n="5.3">frais</w> <w n="5.4">de</w> <w n="5.5">ce</w> <w n="5.6">jour</w> <w n="5.7">comme</w> <w n="5.8">d</w>’<w n="5.9">un</w> <w n="5.10">jour</w> <w n="5.11">de</w> <w n="5.12">fête</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Tout</w> <w n="6.2">fut</w> <w n="6.3">prêt</w> : <w n="6.4">lard</w>, <w n="6.5">raisin</w>, <w n="6.6">et</w> <w n="6.7">fromage</w> <w n="6.8">et</w> <w n="6.9">noisette</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Il</w> <w n="7.2">cherchait</w> <w n="7.3">par</w> <w n="7.4">le</w> <w n="7.5">luxe</w> <w n="7.6">et</w> <w n="7.7">la</w> <w n="7.8">variété</w>,</l>
						<l n="8" num="1.8"><w n="8.1">A</w> <w n="8.2">vaincre</w> <w n="8.3">les</w> <w n="8.4">dégoûts</w> <w n="8.5">d</w>’<w n="8.6">un</w> <w n="8.7">hôte</w> <w n="8.8">rebuté</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Qui</w>, <w n="9.2">parcourant</w> <w n="9.3">de</w> <w n="9.4">l</w>’<w n="9.5">œil</w> <w n="9.6">sa</w> <w n="9.7">table</w> <w n="9.8">officieuse</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Jetait</w> <w n="10.2">sur</w> <w n="10.3">tout</w> <w n="10.4">à</w> <w n="10.5">peine</w> <w n="10.6">une</w> <w n="10.7">dent</w> <w n="10.8">dédaigneuse</w></l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">lui</w>, <w n="11.3">d</w>’<w n="11.4">orge</w> <w n="11.5">et</w> <w n="11.6">de</w> <w n="11.7">blé</w> <w n="11.8">faisant</w> <w n="11.9">tout</w> <w n="11.10">son</w> <w n="11.11">repas</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Laissait</w> <w n="12.2">au</w> <w n="12.3">citadin</w> <w n="12.4">les</w> <w n="12.5">mets</w> <w n="12.6">plus</w> <w n="12.7">délicats</w>.</l>
						<l n="13" num="1.13">« <w n="13.1">Ami</w>, <w n="13.2">dit</w> <w n="13.3">celui</w>-<w n="13.4">ci</w>, <w n="13.5">veux</w>-<w n="13.6">tu</w> <w n="13.7">dans</w> <w n="13.8">la</w> <w n="13.9">misère</w></l>
						<l n="14" num="1.14"><w n="14.1">Vivre</w> <w n="14.2">au</w> <w n="14.3">dos</w> <w n="14.4">escarpé</w> <w n="14.5">de</w> <w n="14.6">ce</w> <w n="14.7">mont</w> <w n="14.8">solitaire</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Ou</w> <w n="15.2">préférer</w> <w n="15.3">le</w> <w n="15.4">monde</w> <w n="15.5">à</w> <w n="15.6">tes</w> <w n="15.7">tristes</w> <w n="15.8">forêts</w> ?</l>
						<l n="16" num="1.16"><w n="16.1">Viens</w> ; <w n="16.2">crois</w>-<w n="16.3">moi</w>, <w n="16.4">suis</w> <w n="16.5">mes</w> <w n="16.6">pas</w> ; <w n="16.7">la</w> <w n="16.8">ville</w> <w n="16.9">est</w> <w n="16.10">ici</w> <w n="16.11">près</w> :</l>
						<l n="17" num="1.17"><w n="17.1">Festins</w>, <w n="17.2">fêtes</w>, <w n="17.3">plaisirs</w> <w n="17.4">y</w> <w n="17.5">sont</w> <w n="17.6">en</w> <w n="17.7">abondance</w>.</l>
						<l n="18" num="1.18"><w n="18.1">L</w>’<w n="18.2">heure</w> <w n="18.3">s</w>’<w n="18.4">écoule</w>, <w n="18.5">ami</w> : <w n="18.6">tout</w> <w n="18.7">fuit</w>, <w n="18.8">la</w> <w n="18.9">mort</w> <w n="18.10">s</w>’<w n="18.11">avance</w> :</l>
						<l n="19" num="1.19"><w n="19.1">Les</w> <w n="19.2">grands</w> <w n="19.3">ni</w> <w n="19.4">les</w> <w n="19.5">petits</w> <w n="19.6">n</w>’<w n="19.7">échappent</w> <w n="19.8">à</w> <w n="19.9">ses</w> <w n="19.10">lois</w> ;</l>
						<l n="20" num="1.20"><w n="20.1">Jouis</w>, <w n="20.2">et</w> <w n="20.3">te</w> <w n="20.4">souviens</w> <w n="20.5">qu</w>’<w n="20.6">on</w> <w n="20.7">ne</w> <w n="20.8">vit</w> <w n="20.9">qu</w>’<w n="20.10">une</w> <w n="20.11">fois</w>. »</l>
						<l n="21" num="1.21"><w n="21.1">Le</w> <w n="21.2">villageois</w> <w n="21.3">écoute</w>, <w n="21.4">accepte</w> <w n="21.5">la</w> <w n="21.6">partie</w>.</l>
						<l n="22" num="1.22"><w n="22.1">On</w> <w n="22.2">se</w> <w n="22.3">lève</w>, <w n="22.4">et</w> <w n="22.5">d</w>’<w n="22.6">aller</w>. <w n="22.7">Tous</w> <w n="22.8">deux</w> <w n="22.9">de</w> <w n="22.10">compagnie</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Nocturnes</w> <w n="23.2">voyageurs</w>, <w n="23.3">dans</w> <w n="23.4">des</w> <w n="23.5">sentiers</w> <w n="23.6">obscurs</w></l>
						<l n="24" num="1.24"><w n="24.1">Se</w> <w n="24.2">glissent</w> <w n="24.3">vers</w> <w n="24.4">la</w> <w n="24.5">ville</w> <w n="24.6">et</w> <w n="24.7">rampent</w> <w n="24.8">sous</w> <w n="24.9">les</w> <w n="24.10">murs</w>.</l>
						<l n="25" num="1.25"><w n="25.1">La</w> <w n="25.2">nuit</w> <w n="25.3">quittait</w> <w n="25.4">les</w> <w n="25.5">cieux</w> <w n="25.6">quand</w> <w n="25.7">notre</w> <w n="25.8">couple</w> <w n="25.9">avide</w></l>
						<l n="26" num="1.26"><w n="26.1">Arrive</w> <w n="26.2">en</w> <w n="26.3">un</w> <w n="26.4">palais</w> <w n="26.5">opulent</w> <w n="26.6">et</w> <w n="26.7">splendide</w></l>
						<l n="27" num="1.27"><w n="27.1">Et</w> <w n="27.2">voit</w> <w n="27.3">fumer</w> <w n="27.4">encor</w> <w n="27.5">dans</w> <w n="27.6">des</w> <w n="27.7">plats</w> <w n="27.8">de</w> <w n="27.9">vermeil</w></l>
						<l n="28" num="1.28"><w n="28.1">Des</w> <w n="28.2">restes</w> <w n="28.3">d</w>’<w n="28.4">un</w> <w n="28.5">souper</w> <w n="28.6">le</w> <w n="28.7">brillant</w> <w n="28.8">appareil</w>.</l>
						<l n="29" num="1.29"><w n="29.1">L</w>’<w n="29.2">un</w> <w n="29.3">s</w>’<w n="29.4">écrie</w>, <w n="29.5">et</w>, <w n="29.6">riant</w> <w n="29.7">de</w> <w n="29.8">sa</w> <w n="29.9">frayeur</w> <w n="29.10">naïve</w>,</l>
						<l n="30" num="1.30"><w n="30.1">L</w>’<w n="30.2">autre</w> <w n="30.3">sur</w> <w n="30.4">le</w> <w n="30.5">duvet</w> <w n="30.6">fait</w> <w n="30.7">placer</w> <w n="30.8">son</w> <w n="30.9">convive</w>,</l>
						<l n="31" num="1.31"><w n="31.1">S</w>’<w n="31.2">empresser</w> <w n="31.3">de</w> <w n="31.4">servir</w>, <w n="31.5">ordonner</w>, <w n="31.6">disposer</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Va</w>, <w n="32.2">vient</w>, <w n="32.3">fait</w> <w n="32.4">les</w> <w n="32.5">honneurs</w>, <w n="32.6">le</w> <w n="32.7">priant</w> <w n="32.8">d</w>’<w n="32.9">excuser</w>.</l>
						<l n="33" num="1.33"><w n="33.1">Le</w> <w n="33.2">campagnard</w> <w n="33.3">bénit</w> <w n="33.4">sa</w> <w n="33.5">nouvelle</w> <w n="33.6">fortune</w> ;</l>
						<l n="34" num="1.34"><w n="34.1">Sa</w> <w n="34.2">vie</w> <w n="34.3">en</w> <w n="34.4">ses</w> <w n="34.5">déserts</w> <w n="34.6">était</w> <w n="34.7">âpre</w>, <w n="34.8">importune</w> :</l>
						<l n="35" num="1.35"><w n="35.1">La</w> <w n="35.2">tristesse</w>, <w n="35.3">l</w>’<w n="35.4">ennui</w>, <w n="35.5">le</w> <w n="35.6">travail</w> <w n="35.7">et</w> <w n="35.8">la</w> <w n="35.9">faim</w>.</l>
						<l n="36" num="1.36"><w n="36.1">Ici</w>, <w n="36.2">l</w>’<w n="36.3">on</w> <w n="36.4">y</w> <w n="36.5">peut</w> <w n="36.6">vivre</w> ; <w n="36.7">et</w> <w n="36.8">de</w> <w n="36.9">rire</w>. <w n="36.10">Et</w> <w n="36.11">soudain</w></l>
						<l n="37" num="1.37"><w n="37.1">Des</w> <w n="37.2">valets</w> <w n="37.3">à</w> <w n="37.4">grand</w> <w n="37.5">bruit</w> <w n="37.6">interrompent</w> <w n="37.7">la</w> <w n="37.8">fête</w>.</l>
						<l n="38" num="1.38"><w n="38.1">On</w> <w n="38.2">court</w>, <w n="38.3">on</w> <w n="38.4">vole</w>, <w n="38.5">on</w> <w n="38.6">fuit</w> ; <w n="38.7">nul</w> <w n="38.8">coin</w>, <w n="38.9">nulle</w> <w n="38.10">retraite</w>.</l>
						<l n="39" num="1.39"><w n="39.1">Les</w> <w n="39.2">dogues</w> <w n="39.3">réveillés</w> <w n="39.4">les</w> <w n="39.5">glacent</w> <w n="39.6">par</w> <w n="39.7">leur</w> <w n="39.8">voix</w> ;</l>
						<l n="40" num="1.40"><w n="40.1">Toute</w> <w n="40.2">la</w> <w n="40.3">maison</w> <w n="40.4">tremble</w> <w n="40.5">au</w> <w n="40.6">bruit</w> <w n="40.7">de</w> <w n="40.8">leurs</w> <w n="40.9">abois</w>,</l>
						<l n="41" num="1.41"><w n="41.1">Alors</w> <w n="41.2">le</w> <w n="41.3">campagnard</w>, <w n="41.4">honteux</w> <w n="41.5">de</w> <w n="41.6">son</w> <w n="41.7">délire</w> :</l>
						<l n="42" num="1.42">« <w n="42.1">Soyez</w> <w n="42.2">heureux</w>, <w n="42.3">dit</w>-<w n="42.4">il</w> ; <w n="42.5">adieu</w>, <w n="42.6">je</w> <w n="42.7">me</w> <w n="42.8">retire</w>,</l>
						<l n="43" num="1.43"><w n="43.1">Et</w> <w n="43.2">je</w> <w n="43.3">vais</w> <w n="43.4">dans</w> <w n="43.5">mon</w> <w n="43.6">trou</w> <w n="43.7">rejoindre</w> <w n="43.8">en</w> <w n="43.9">sûreté</w></l>
						<l n="44" num="1.44"><w n="44.1">Le</w> <w n="44.2">sommeil</w>, <w n="44.3">un</w> <w n="44.4">peu</w> <w n="44.5">d</w>’<w n="44.6">orge</w> <w n="44.7">et</w> <w n="44.8">la</w> <w n="44.9">tranquillité</w>,</l>
					</lg>
				</div></body></text></TEI>