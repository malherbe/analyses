<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉGLOGUES</head><div type="poem" key="CHE38">
					<head type="number">IV</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ah</w> ! <w n="1.2">ce</w> <w n="1.3">n</w>’<w n="1.4">est</w> <w n="1.5">point</w> <w n="1.6">à</w> <w n="1.7">moi</w> <w n="1.8">qu</w>’<w n="1.9">on</w> <w n="1.10">s</w>’<w n="1.11">occupe</w> <w n="1.12">de</w> <w n="1.13">plaire</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Ma</w> <w n="2.2">sœur</w> <w n="2.3">plus</w> <w n="2.4">tôt</w> <w n="2.5">que</w> <w n="2.6">moi</w> <w n="2.7">dût</w> <w n="2.8">le</w> <w n="2.9">jour</w> <w n="2.10">à</w> <w n="2.11">ma</w> <w n="2.12">mère</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Si</w> <w n="3.2">quelques</w> <w n="3.3">beaux</w> <w n="3.4">bergers</w> <w n="3.5">apportent</w> <w n="3.6">une</w> <w n="3.7">fleur</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">vois</w> <w n="4.3">qu</w>’<w n="4.4">en</w> <w n="4.5">me</w> <w n="4.6">l</w>’<w n="4.7">offrant</w> <w n="4.8">ils</w> <w n="4.9">regardent</w> <w n="4.10">ma</w> <w n="4.11">sœur</w>.</l>
						<l n="5" num="1.5"><w n="5.1">S</w>’<w n="5.2">ils</w> <w n="5.3">vantent</w> <w n="5.4">les</w> <w n="5.5">attraits</w> <w n="5.6">dont</w> <w n="5.7">brille</w> <w n="5.8">mon</w> <w n="5.9">visage</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Ils</w> <w n="6.2">disent</w> <w n="6.3">à</w> <w n="6.4">ma</w> <w n="6.5">sœur</w> : « <w n="6.6">C</w>’<w n="6.7">est</w> <w n="6.8">ta</w> <w n="6.9">vivante</w> <w n="6.10">image</w>. »</l>
						<l n="7" num="1.7"><w n="7.1">Ah</w> ! <w n="7.2">pourquoi</w> <w n="7.3">n</w>’<w n="7.4">ai</w>-<w n="7.5">je</w> <w n="7.6">encor</w> <w n="7.7">vu</w> <w n="7.8">que</w> <w n="7.9">douze</w> <w n="7.10">moissons</w> ?</l>
						<l n="8" num="1.8"><w n="8.1">Nul</w> <w n="8.2">amant</w> <w n="8.3">ne</w> <w n="8.4">me</w> <w n="8.5">flatte</w> <w n="8.6">en</w> <w n="8.7">ses</w> <w n="8.8">douces</w> <w n="8.9">chansons</w> ;</l>
						<l n="9" num="1.9"><w n="9.1">Nul</w> <w n="9.2">ne</w> <w n="9.3">dit</w> <w n="9.4">qu</w>’<w n="9.5">il</w> <w n="9.6">mourra</w> <w n="9.7">si</w> <w n="9.8">je</w> <w n="9.9">suis</w> <w n="9.10">infidèle</w>.</l>
						<l n="10" num="1.10"><w n="10.1">Mais</w> <w n="10.2">j</w>’<w n="10.3">attends</w>. <w n="10.4">L</w>’<w n="10.5">âge</w> <w n="10.6">vient</w>. <w n="10.7">Je</w> <w n="10.8">sais</w> <w n="10.9">que</w> <w n="10.10">je</w> <w n="10.11">suis</w> <w n="10.12">belle</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Je</w> <w n="11.2">sais</w> <w n="11.3">qu</w>’<w n="11.4">on</w> <w n="11.5">ne</w> <w n="11.6">voit</w> <w n="11.7">point</w> <w n="11.8">d</w>’<w n="11.9">attraits</w> <w n="11.10">plus</w> <w n="11.11">désirés</w></l>
						<l n="12" num="1.12"><w n="12.1">Qu</w>’<w n="12.2">un</w> <w n="12.3">visage</w> <w n="12.4">arrondi</w>, <w n="12.5">de</w> <w n="12.6">longs</w> <w n="12.7">cheveux</w> <w n="12.8">dorés</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Dans</w> <w n="13.2">une</w> <w n="13.3">bouche</w> <w n="13.4">étroite</w> <w n="13.5">un</w> <w n="13.6">double</w> <w n="13.7">rang</w> <w n="13.8">d</w>’<w n="13.9">ivoire</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">sur</w> <w n="14.3">de</w> <w n="14.4">beaux</w> <w n="14.5">yeux</w> <w n="14.6">bleus</w> <w n="14.7">une</w> <w n="14.8">paupière</w> <w n="14.9">noire</w>.</l>
					</lg>
				</div></body></text></TEI>