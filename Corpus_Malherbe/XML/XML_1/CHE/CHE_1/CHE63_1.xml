<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉGLOGUES</head><div type="poem" key="CHE63">
					<head type="number">XXIX</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Docte</w> <w n="1.2">et</w> <w n="1.3">jeune</w> <w n="1.4">Cosway</w>, <w n="1.5">des</w> <w n="1.6">neuf</w> <w n="1.7">sœurs</w> <w n="1.8">honorée</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Au</w> <w n="2.2">Pinde</w>, <w n="2.3">à</w> <w n="2.4">tous</w> <w n="2.5">les</w> <w n="2.6">arts</w> <w n="2.7">par</w> <w n="2.8">elles</w> <w n="2.9">consacrée</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Mes</w> <w n="3.2">bergers</w> <w n="3.3">en</w> <w n="3.4">dansant</w> <w n="3.5">t</w>’<w n="3.6">appellent</w> <w n="3.7">à</w> <w n="3.8">leurs</w> <w n="3.9">jeux</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Donne</w>-<w n="4.2">leur</w> <w n="4.3">un</w> <w n="4.4">regard</w>. <w n="4.5">Tu</w> <w n="4.6">trouveras</w> <w n="4.7">chez</w> <w n="4.8">eux</w></l>
						<l n="5" num="1.5"><w n="5.1">Ce</w> <w n="5.2">qu</w>’<w n="5.3">en</w> <w n="5.4">toi</w> <w n="5.5">chaque</w> <w n="5.6">jour</w> <w n="5.7">tu</w> <w n="5.8">trouves</w> <w n="5.9">dès</w> <w n="5.10">l</w>’<w n="5.11">enfance</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">calme</w> <w n="6.3">et</w> <w n="6.4">les</w> <w n="6.5">plaisirs</w> <w n="6.6">qui</w> <w n="6.7">suivent</w> <w n="6.8">l</w>’<w n="6.9">innocence</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Accueille</w> <w n="7.2">mes</w> <w n="7.3">hameaux</w>. <w n="7.4">Leurs</w> <w n="7.5">chansons</w>, <w n="7.6">leur</w> <w n="7.7">bonheur</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Sont</w> <w n="8.2">doux</w> <w n="8.3">comme</w> <w n="8.4">tes</w> <w n="8.5">yeux</w> <w n="8.6">et</w> <w n="8.7">purs</w> <w n="8.8">comme</w> <w n="8.9">ton</w> <w n="8.10">cœur</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Mes</w> <w n="9.2">chants</w>, <w n="9.3">aimés</w> <w n="9.4">de</w> <w n="9.5">Flore</w> <w n="9.6">et</w> <w n="9.7">de</w> <w n="9.8">ses</w> <w n="9.9">sœurs</w> <w n="9.10">divines</w>,</l>
						<l n="10" num="1.10"><w n="10.1">N</w>’<w n="10.2">ont</w> <w n="10.3">point</w> <w n="10.4">l</w>’<w n="10.5">ambre</w> <w n="10.6">et</w> <w n="10.7">le</w> <w n="10.8">fard</w> <w n="10.9">des</w> <w n="10.10">muses</w> <w n="10.11">citadines</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Je</w> <w n="11.2">ne</w> <w n="11.3">viens</w> <w n="11.4">point</w> <w n="11.5">t</w>’<w n="11.6">offrir</w>, <w n="11.7">dans</w> <w n="11.8">mes</w> <w n="11.9">vers</w> <w n="11.10">ingénus</w>,</l>
						<l n="12" num="1.12"><w n="12.1">De</w> <w n="12.2">ces</w> <w n="12.3">bergers</w> <w n="12.4">français</w> <w n="12.5">à</w> <w n="12.6">Palès</w> <w n="12.7">inconnus</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Ma</w> <w n="13.2">muse</w> <w n="13.3">grecque</w> <w n="13.4">et</w> <w n="13.5">simple</w> <w n="13.6">et</w> <w n="13.7">de</w> <w n="13.8">fleurs</w> <w n="13.9">embellie</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Visitant</w> <w n="14.2">son</w> <w n="14.3">Alphée</w> <w n="14.4">et</w> <w n="14.5">ta</w> <w n="14.6">noble</w> <w n="14.7">Italie</w>,</l>
						<l n="15" num="1.15"><w n="15.1">A</w> <w n="15.2">retenu</w> <w n="15.3">les</w> <w n="15.4">airs</w> <w n="15.5">qu</w>’<w n="15.6">en</w> <w n="15.7">ces</w> <w n="15.8">lieux</w> <w n="15.9">séducteurs</w></l>
						<l n="16" num="1.16"><w n="16.1">Souvent</w> <w n="16.2">à</w> <w n="16.3">son</w> <w n="16.4">oreille</w> <w n="16.5">ont</w> <w n="16.6">chanté</w> <w n="16.7">les</w> <w n="16.8">pasteurs</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Souvent</w> <w n="17.2">près</w> <w n="17.3">d</w>’<w n="17.4">une</w> <w n="17.5">grotte</w>, <w n="17.6">au</w> <w n="17.7">bord</w> <w n="17.8">d</w>’<w n="17.9">une</w> <w n="17.10">fontaine</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Elle</w> <w n="18.2">va</w> <w n="18.3">se</w> <w n="18.4">cacher</w> <w n="18.5">dans</w> <w n="18.6">l</w>’<w n="18.7">écorce</w> <w n="18.8">d</w>’<w n="18.9">un</w> <w n="18.10">chêne</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">sans</w> <w n="19.3">bruit</w> <w n="19.4">elle</w> <w n="19.5">écoute</w>, <w n="19.6">elle</w> <w n="19.7">apprend</w> <w n="19.8">à</w> <w n="19.9">chanter</w></l>
						<l n="20" num="1.20"><w n="20.1">Ce</w> <w n="20.2">qu</w>’<w n="20.3">aux</w> <w n="20.4">dieux</w> <w n="20.5">des</w> <w n="20.6">forêts</w> <w n="20.7">elle</w> <w n="20.8">entend</w> <w n="20.9">répéter</w>.</l>
					</lg>
				</div></body></text></TEI>