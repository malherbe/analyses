<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES ANTIQUES</head><div type="poem" key="CHE20">
					<head type="number">IV</head>
					<head type="main">Chrysé</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pourquoi</w>, <w n="1.2">belle</w> <w n="1.3">Chrysé</w>, <w n="1.4">t</w>’<w n="1.5">abandonnant</w> <w n="1.6">aux</w> <w n="1.7">voiles</w>,</l>
						<l n="2" num="1.2"><w n="2.1">T</w>’<w n="2.2">éloigner</w> <w n="2.3">de</w> <w n="2.4">nos</w> <w n="2.5">bords</w> <w n="2.6">sur</w> <w n="2.7">la</w> <w n="2.8">foi</w> <w n="2.9">des</w> <w n="2.10">étoiles</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Dieux</w> ! <w n="3.2">je</w> <w n="3.3">t</w>’<w n="3.4">ai</w> <w n="3.5">vue</w> <w n="3.6">en</w> <w n="3.7">songe</w>, <w n="3.8">et</w>, <w n="3.9">de</w> <w n="3.10">terreur</w> <w n="3.11">glacé</w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">ai</w> <w n="4.3">vu</w> <w n="4.4">sur</w> <w n="4.5">des</w> <w n="4.6">écueils</w> <w n="4.7">ton</w> <w n="4.8">vaisseau</w> <w n="4.9">fracassé</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Ton</w> <w n="5.2">corps</w> <w n="5.3">flottant</w> <w n="5.4">sur</w> <w n="5.5">l</w>’<w n="5.6">onde</w>, <w n="5.7">et</w> <w n="5.8">tes</w> <w n="5.9">bras</w> <w n="5.10">avec</w> <w n="5.11">peine</w></l>
						<l n="6" num="1.6"><w n="6.1">Cherchant</w> <w n="6.2">à</w> <w n="6.3">repousser</w> <w n="6.4">la</w> <w n="6.5">vague</w> <w n="6.6">ionienne</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Les</w> <w n="7.2">filles</w> <w n="7.3">de</w> <w n="7.4">Nérée</w> <w n="7.5">ont</w> <w n="7.6">volé</w> <w n="7.7">près</w> <w n="7.8">de</w> <w n="7.9">toi</w>.</l>
						<l n="8" num="1.8"><w n="8.1">Leur</w> <w n="8.2">sein</w> <w n="8.3">fut</w> <w n="8.4">moins</w> <w n="8.5">troublé</w> <w n="8.6">de</w> <w n="8.7">douleur</w> <w n="8.8">et</w> <w n="8.9">d</w>’<w n="8.10">effroi</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Quand</w> <w n="9.2">du</w> <w n="9.3">bélier</w> <w n="9.4">doré</w>, <w n="9.5">qui</w> <w n="9.6">traversait</w> <w n="9.7">leurs</w> <w n="9.8">ondes</w>,</l>
						<l n="10" num="1.10"><w n="10.1">La</w> <w n="10.2">jeune</w> <w n="10.3">Hellé</w> <w n="10.4">tomba</w> <w n="10.5">dans</w> <w n="10.6">leurs</w> <w n="10.7">grottes</w> <w n="10.8">profondes</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Oh</w> ! <w n="11.2">que</w> <w n="11.3">j</w>’<w n="11.4">ai</w> <w n="11.5">craint</w> <w n="11.6">de</w> <w n="11.7">voir</w> <w n="11.8">à</w> <w n="11.9">cette</w> <w n="11.10">mer</w>, <w n="11.11">un</w> <w n="11.12">jour</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Tiphys</w> <w n="12.2">donner</w> <w n="12.3">ton</w> <w n="12.4">nom</w> <w n="12.5">et</w> <w n="12.6">plaindre</w> <w n="12.7">mon</w> <w n="12.8">amour</w> !</l>
						<l n="13" num="1.13"><w n="13.1">Que</w> <w n="13.2">j</w>’<w n="13.3">adressai</w> <w n="13.4">de</w> <w n="13.5">vœux</w> <w n="13.6">aux</w> <w n="13.7">dieux</w> <w n="13.8">de</w> <w n="13.9">l</w>’<w n="13.10">onde</w> <w n="13.11">amère</w> !</l>
						<l n="14" num="1.14"><w n="14.1">Que</w> <w n="14.2">de</w> <w n="14.3">vœux</w> <w n="14.4">à</w> <w n="14.5">Neptune</w>, <w n="14.6">à</w> <w n="14.7">Castor</w>, <w n="14.8">à</w> <w n="14.9">son</w> <w n="14.10">frère</w> !</l>
						<l n="15" num="1.15"><w n="15.1">Glaucus</w> <w n="15.2">ne</w> <w n="15.3">te</w> <w n="15.4">vit</w> <w n="15.5">point</w> ; <w n="15.6">car</w> <w n="15.7">sans</w> <w n="15.8">doute</w> <w n="15.9">avec</w> <w n="15.10">lui</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Déesse</w>, <w n="16.2">au</w> <w n="16.3">sein</w> <w n="16.4">des</w> <w n="16.5">mers</w> <w n="16.6">tu</w> <w n="16.7">vivrais</w> <w n="16.8">aujourd</w>’<w n="16.9">hui</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Déjà</w> <w n="17.2">tu</w> <w n="17.3">n</w>’<w n="17.4">élevais</w> <w n="17.5">que</w> <w n="17.6">des</w> <w n="17.7">mains</w> <w n="17.8">défaillantes</w> ;</l>
						<l n="18" num="1.18"><w n="18.1">Tu</w> <w n="18.2">me</w> <w n="18.3">nommais</w> <w n="18.4">déjà</w> <w n="18.5">de</w> <w n="18.6">tes</w> <w n="18.7">lèvres</w> <w n="18.8">mourantes</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Quand</w>, <w n="19.2">pour</w> <w n="19.3">te</w> <w n="19.4">secourir</w>, <w n="19.5">j</w>’<w n="19.6">ai</w> <w n="19.7">vu</w> <w n="19.8">fendre</w> <w n="19.9">les</w> <w n="19.10">flots</w></l>
						<l n="20" num="1.20"><w n="20.1">Au</w> <w n="20.2">dauphin</w> <w n="20.3">qui</w> <w n="20.4">sauva</w> <w n="20.5">le</w> <w n="20.6">chanteur</w> <w n="20.7">de</w> <w n="20.8">Lesbos</w>.</l>
					</lg>
				</div></body></text></TEI>