<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="CHE30">
					<head type="number">IV</head>
					<head type="main">Fragment</head>
					<p>Un jeune homme dira :</p>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">étais</w> <w n="1.3">un</w> <w n="1.4">faible</w> <w n="1.5">enfant</w> <w n="1.6">qu</w>’<w n="1.7">elle</w> <w n="1.8">était</w> <w n="1.9">grande</w> <w n="1.10">et</w> <w n="1.11">belle</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Elle</w> <w n="2.2">me</w> <w n="2.3">souriait</w> <w n="2.4">et</w> <w n="2.5">m</w>’<w n="2.6">appelait</w> <w n="2.7">près</w> <w n="2.8">d</w>’<w n="2.9">elle</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Debout</w> <w n="3.2">sur</w> <w n="3.3">ses</w> <w n="3.4">genoux</w>, <w n="3.5">mon</w> <w n="3.6">innocente</w> <w n="3.7">main</w></l>
						<l n="4" num="1.4"><w n="4.1">Parcourait</w> <w n="4.2">ses</w> <w n="4.3">cheveux</w>, <w n="4.4">son</w> <w n="4.5">visage</w>, <w n="4.6">son</w> <w n="4.7">sein</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">sa</w> <w n="5.3">main</w> <w n="5.4">quelquefois</w>, <w n="5.5">aimable</w> <w n="5.6">et</w> <w n="5.7">caressante</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Feignait</w> <w n="6.2">de</w> <w n="6.3">châtier</w> <w n="6.4">mon</w> <w n="6.5">enfance</w> <w n="6.6">imprudente</w>.</l>
						<l n="7" num="1.7"><w n="7.1">C</w>’<w n="7.2">est</w> <w n="7.3">devant</w> <w n="7.4">ses</w> <w n="7.5">amants</w>, <w n="7.6">auprès</w> <w n="7.7">d</w>’<w n="7.8">elle</w> <w n="7.9">confus</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Que</w> <w n="8.2">la</w> <w n="8.3">fière</w> <w n="8.4">beauté</w> <w n="8.5">me</w> <w n="8.6">caressait</w> <w n="8.7">le</w> <w n="8.8">plus</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Que</w> <w n="9.2">de</w> <w n="9.3">fois</w> (<w n="9.4">mais</w>, <w n="9.5">hélas</w> ! <w n="9.6">que</w> <w n="9.7">sent</w>-<w n="9.8">on</w> <w n="9.9">à</w> <w n="9.10">cet</w> <w n="9.11">âge</w> ?)</l>
						<l n="10" num="1.10"><w n="10.1">Les</w> <w n="10.2">baisers</w> <w n="10.3">de</w> <w n="10.4">sa</w> <w n="10.5">bouche</w> <w n="10.6">ont</w> <w n="10.7">pressé</w> <w n="10.8">mon</w> <w n="10.9">visage</w> !</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">les</w> <w n="11.3">bergers</w> <w n="11.4">disaient</w>, <w n="11.5">me</w> <w n="11.6">voyant</w> <w n="11.7">triomphant</w> :</l>
						<l n="12" num="1.12">« <w n="12.1">Oh</w> ! <w n="12.2">que</w> <w n="12.3">de</w> <w n="12.4">biens</w> <w n="12.5">perdus</w> ! <w n="12.6">O</w> <w n="12.7">trop</w> <w n="12.8">heureux</w> <w n="12.9">enfant</w> ! »</l>
					</lg>
					<p>Plusieurs jeunes filles entourent un petit enfant… le cares-
					sent…
					— On dit que tu as fait une chanson pour Pannychis, ta cou-
					sine ?…
					— Oui, je l’aime, Pannychis… elle est belle. Elle a cinq
					ans comme moi… Nous avons arrondi en berceau ces buissons de
					roses… nous nous promenons sous cet ombrage… on ne peut
					nous y troubler, car il est trop bas pour qu’on y puisse entrer. Je
					lui ai donné une statue de Vénus que mon père m’a faite avec
					du buis. Elle l’appelle sa fille, elle la couche sur des feuilles de
					rose dans une écorce de grenade… Tous les amants font toujours
					des chansons pour leur bergère… Et moi aussi, j’en ai fait une
					pour elle…
					— Eh bien, chante-nous ta chanson et nous te donnerons
					des… et des figues mielleuses…
					— Donnez-les-moi d’abord et puis je vais chanter… Il tend
					ses deux mains… on lui donne… et puis, d’une voix claire et
					douce, il se mit à chanter :</p>
					<lg n="2">
						<l n="13" num="2.1">« <w n="13.1">Ma</w> <w n="13.2">belle</w> <w n="13.3">Pannychis</w>, <w n="13.4">il</w> <w n="13.5">faut</w> <w n="13.6">bien</w> <w n="13.7">que</w> <w n="13.8">tu</w> <w n="13.9">m</w>’<w n="13.10">aimes</w> :</l>
						<l n="14" num="2.2"><w n="14.1">Nous</w> <w n="14.2">avons</w> <w n="14.3">même</w> <w n="14.4">toit</w>, <w n="14.5">nos</w> <w n="14.6">âges</w> <w n="14.7">sont</w> <w n="14.8">les</w> <w n="14.9">mêmes</w>.</l>
						<l n="15" num="2.3"><w n="15.1">Vois</w> <w n="15.2">comme</w> <w n="15.3">je</w> <w n="15.4">suis</w> <w n="15.5">grand</w>, <w n="15.6">vois</w> <w n="15.7">comme</w> <w n="15.8">je</w> <w n="15.9">suis</w> <w n="15.10">beau</w>.</l>
						<l n="16" num="2.4"><w n="16.1">Hier</w> <w n="16.2">je</w> <w n="16.3">me</w> <w n="16.4">suis</w> <w n="16.5">mis</w> <w n="16.6">auprès</w> <w n="16.7">de</w> <w n="16.8">mon</w> <w n="16.9">chevreau</w> :</l>
						<l n="17" num="2.5"><w n="17.1">Par</w> <w n="17.2">Pollux</w> <w n="17.3">et</w> <w n="17.4">Minerve</w> ! <w n="17.5">il</w> <w n="17.6">ne</w> <w n="17.7">pouvait</w> <w n="17.8">qu</w>’<w n="17.9">à</w> <w n="17.10">peine</w></l>
						<l n="18" num="2.6"><w n="18.1">Faire</w> <w n="18.2">arriver</w> <w n="18.3">sa</w> <w n="18.4">tête</w> <w n="18.5">au</w> <w n="18.6">niveau</w> <w n="18.7">de</w> <w n="18.8">la</w> <w n="18.9">mienne</w>.</l>
						<l n="19" num="2.7"><w n="19.1">D</w>’<w n="19.2">une</w> <w n="19.3">coque</w> <w n="19.4">de</w> <w n="19.5">noix</w>, <w n="19.6">j</w>’<w n="19.7">ai</w> <w n="19.8">fait</w> <w n="19.9">un</w> <w n="19.10">abri</w> <w n="19.11">sûr</w></l>
						<l n="20" num="2.8"><w n="20.1">Pour</w> <w n="20.2">un</w> <w n="20.3">beau</w> <w n="20.4">scarabée</w> <w n="20.5">étincelant</w> <w n="20.6">d</w>’<w n="20.7">azur</w> ;</l>
						<l n="21" num="2.9"><w n="21.1">Il</w> <w n="21.2">couche</w> <w n="21.3">sur</w> <w n="21.4">la</w> <w n="21.5">laine</w>, <w n="21.6">et</w> <w n="21.7">je</w> <w n="21.8">te</w> <w n="21.9">le</w> <w n="21.10">destine</w>.</l>
						<l n="22" num="2.10"><w n="22.1">Ce</w> <w n="22.2">matin</w>, <w n="22.3">j</w>’<w n="22.4">ai</w> <w n="22.5">trouvé</w> <w n="22.6">parmi</w> <w n="22.7">l</w>’<w n="22.8">algue</w> <w n="22.9">marine</w></l>
						<l n="23" num="2.11"><w n="23.1">Une</w> <w n="23.2">vaste</w> <w n="23.3">coquille</w> <w n="23.4">aux</w> <w n="23.5">brillantes</w> <w n="23.6">couleurs</w> :</l>
						<l n="24" num="2.12"><w n="24.1">Nous</w> <w n="24.2">l</w>’<w n="24.3">emplirons</w> <w n="24.4">de</w> <w n="24.5">terre</w>, <w n="24.6">il</w> <w n="24.7">y</w> <w n="24.8">viendra</w> <w n="24.9">des</w> <w n="24.10">fleurs</w>.</l>
						<l n="25" num="2.13"><w n="25.1">Je</w> <w n="25.2">veux</w>, <w n="25.3">pour</w> <w n="25.4">te</w> <w n="25.5">montrer</w> <w n="25.6">une</w> <w n="25.7">flotte</w> <w n="25.8">nombreuse</w>,</l>
						<l n="26" num="2.14"><w n="26.1">Lancer</w> <w n="26.2">sur</w> <w n="26.3">notre</w> <w n="26.4">étang</w> <w n="26.5">des</w> <w n="26.6">écorces</w> <w n="26.7">d</w>’<w n="26.8">yeuse</w>.</l>
						<l n="27" num="2.15"><w n="27.1">Le</w> <w n="27.2">chien</w> <w n="27.3">de</w> <w n="27.4">la</w> <w n="27.5">maison</w> <w n="27.6">est</w> <w n="27.7">si</w> <w n="27.8">doux</w> ! <w n="27.9">chaque</w> <w n="27.10">soir</w>,</l>
						<l n="28" num="2.16"><w n="28.1">Mollement</w> <w n="28.2">sur</w> <w n="28.3">son</w> <w n="28.4">dos</w> <w n="28.5">je</w> <w n="28.6">veux</w> <w n="28.7">te</w> <w n="28.8">faire</w> <w n="28.9">asseoir</w> ;</l>
						<l n="29" num="2.17"><w n="29.1">Et</w>, <w n="29.2">marchant</w> <w n="29.3">devant</w> <w n="29.4">toi</w> <w n="29.5">jusques</w> <w n="29.6">à</w> <w n="29.7">notre</w> <w n="29.8">asile</w>,</l>
						<l n="30" num="2.18"><w n="30.1">Je</w> <w n="30.2">guiderai</w> <w n="30.3">les</w> <w n="30.4">pas</w> <w n="30.5">de</w> <w n="30.6">ce</w> <w n="30.7">coursier</w> <w n="30.8">docile</w>. »</l>
					</lg>
					<p>
						Il s’en va bien baisé, bien caressé… Les jeunes beautés le
						suivent de loin. Arrivées aux rosiers, elles regardent par-dessus
						le berceau, sous lequel elles les voient occupés à former avec des
						buissons de myrtes et de roses un temple de verdure autour d’un
						petit autel pour leur statue de Vénus, elles rient. Ils lèvent la
						tète, les votent et leur disent de s’en aller. On les embrasse… Et
						s’en allant, la jeune Myro dit : … ô heureux âge… mes com-
						pagnes, venez voir aussi chez moi les monuments de notre enfance…
						j’ai entouré d’une haie, pour le conserver, le jardin que j’avais
						alors… une chèvre l’aurait brouté tout entier en une heure… C’est
						là que je vivais avec… là, il m’appelait déjà sa femme et je
						l’appelais mon époux… nous n’étions pas plus hauts que telle
						plante… nous nous serions perdus dans une forêt de Urym… vous
						y verrez encore le romarin, et… s’élever en berceau comme des
						cyprès autour du tombeau de marbre où sont écrits les vers
						d’Anyté… Mon bien-aimé m’avait donné une cigale et une sau-
						terelle. Elles moururent, je leur élevai ce tombeau parmi le
						romarin et… j’étais en pleurs… La belle Anyté passa, sa lyre
						à la main…
						— Qu’as-tu ? me demanda-t-elle.
						— Ma cigale et ma sauterelle sont mortes…
						— Ah ! me dit-elle, nous devons tous mourir (cinq ou six vers
						de morale)…
						Puis elle écrivit sur la pierre (l’épigramme d’Anyté).
					</p>
					<lg n="3">
						<l n="31" num="3.1">« <w n="31.1">O</w> <w n="31.2">sauterelle</w>, <w n="31.3">à</w> <w n="31.4">toi</w>, <w n="31.5">rossignol</w> <w n="31.6">des</w> <w n="31.7">fougères</w>,</l>
						<l n="32" num="3.2"><w n="32.1">A</w> <w n="32.2">toi</w>, <w n="32.3">verte</w> <w n="32.4">cigale</w>, <w n="32.5">amante</w> <w n="32.6">des</w> <w n="32.7">bruyères</w>,</l>
						<l n="33" num="3.3"><w n="33.1">Myro</w> <w n="33.2">de</w> <w n="33.3">cette</w> <w n="33.4">tombe</w> <w n="33.5">élève</w> <w n="33.6">les</w> <w n="33.7">honneurs</w>,</l>
						<l n="34" num="3.4"><w n="34.1">Et</w> <w n="34.2">sa</w> <w n="34.3">joue</w> <w n="34.4">enfantine</w> <w n="34.5">est</w> <w n="34.6">humide</w> <w n="34.7">de</w> <w n="34.8">pleurs</w> ;</l>
						<l n="35" num="3.5"><w n="35.1">Car</w> <w n="35.2">l</w>’<w n="35.3">avare</w> <w n="35.4">Achéron</w>, <w n="35.5">les</w> <w n="35.6">sœurs</w> <w n="35.7">impitoyables</w>,</l>
						<l n="36" num="3.6"><w n="36.1">Ont</w> <w n="36.2">ravi</w> <w n="36.3">de</w> <w n="36.4">ses</w> <w n="36.5">jeux</w> <w n="36.6">ces</w> <w n="36.7">compagnes</w> <w n="36.8">aimables</w>. »</l>
					</lg>
				</div></body></text></TEI>