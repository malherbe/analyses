<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉGLOGUES</head><div type="poem" key="CHE35">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">LOIN</w> <w n="1.2">des</w> <w n="1.3">bords</w> <w n="1.4">trop</w> <w n="1.5">fleuris</w> <w n="1.6">de</w> <w n="1.7">Gnide</w> <w n="1.8">et</w> <w n="1.9">de</w> <w n="1.10">Paphos</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Effrayé</w> <w n="2.2">d</w>’<w n="2.3">un</w> <w n="2.4">bonheur</w> <w n="2.5">ennemi</w> <w n="2.6">du</w> <w n="2.7">repos</w>,</l>
						<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">allais</w>, <w n="3.3">nouveau</w> <w n="3.4">pasteur</w>, <w n="3.5">aux</w> <w n="3.6">champs</w> <w n="3.7">de</w> <w n="3.8">Syracuse</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Invoquer</w> <w n="4.2">dans</w> <w n="4.3">mes</w> <w n="4.4">vers</w> <w n="4.5">la</w> <w n="4.6">nymphe</w> <w n="4.7">d</w>’<w n="4.8">Aréthuse</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Lorsque</w> <w n="5.2">Vénus</w>, <w n="5.3">du</w> <w n="5.4">haut</w> <w n="5.5">des</w> <w n="5.6">célestes</w> <w n="5.7">lambris</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Sans</w> <w n="6.2">armes</w>, <w n="6.3">sans</w> <w n="6.4">carquois</w>, <w n="6.5">vint</w> <w n="6.6">m</w>’<w n="6.7">amener</w> <w n="6.8">son</w> <w n="6.9">fils</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Tous</w> <w n="7.2">deux</w> <w n="7.3">ils</w> <w n="7.4">souriaient</w> : « <w n="7.5">Tiens</w>, <w n="7.6">berger</w>, <w n="7.7">me</w> <w n="7.8">dit</w>-<w n="7.9">elle</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Je</w> <w n="8.2">te</w> <w n="8.3">laisse</w> <w n="8.4">mon</w> <w n="8.5">fils</w>, <w n="8.6">sois</w> <w n="8.7">son</w> <w n="8.8">guide</w> <w n="8.9">fidèle</w> ;</l>
						<l n="9" num="1.9"><w n="9.1">Des</w> <w n="9.2">champêtres</w> <w n="9.3">douceurs</w> <w n="9.4">instruis</w> <w n="9.5">ses</w> <w n="9.6">jeunes</w> <w n="9.7">ans</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">Montre</w>-<w n="10.2">lui</w> <w n="10.3">la</w> <w n="10.4">sagesse</w>, <w n="10.5">elle</w> <w n="10.6">habite</w> <w n="10.7">les</w> <w n="10.8">champs</w>. »</l>
						<l n="11" num="1.11"><w n="11.1">Elle</w> <w n="11.2">fuit</w>. <w n="11.3">Moi</w>, <w n="11.4">crédule</w>, <w n="11.5">à</w> <w n="11.6">cette</w> <w n="11.7">voix</w> <w n="11.8">perfide</w>,</l>
						<l n="12" num="1.12"><w n="12.1">J</w>’<w n="12.2">appelle</w> <w n="12.3">près</w> <w n="12.4">de</w> <w n="12.5">moi</w> <w n="12.6">l</w>’<w n="12.7">enfant</w> <w n="12.8">doux</w> <w n="12.9">et</w> <w n="12.10">timide</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Je</w> <w n="13.2">lui</w> <w n="13.3">dis</w> <w n="13.4">nos</w> <w n="13.5">plaisirs</w> <w n="13.6">et</w> <w n="13.7">la</w> <w n="13.8">paix</w> <w n="13.9">des</w> <w n="13.10">hameaux</w> ;</l>
						<l n="14" num="1.14"><w n="14.1">Un</w> <w n="14.2">dieu</w> <w n="14.3">même</w> <w n="14.4">au</w> <w n="14.5">Pénée</w> <w n="14.6">abreuvant</w> <w n="14.7">des</w> <w n="14.8">troupeaux</w> ;</l>
						<l n="15" num="1.15"><w n="15.1">Bacchus</w> <w n="15.2">et</w> <w n="15.3">les</w> <w n="15.4">moissons</w> ; <w n="15.5">quel</w> <w n="15.6">dieu</w>, <w n="15.7">sur</w> <w n="15.8">le</w> <w n="15.9">Ménale</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Forma</w> <w n="16.2">de</w> <w n="16.3">neuf</w> <w n="16.4">roseaux</w> <w n="16.5">une</w> <w n="16.6">flûte</w> <w n="16.7">inégale</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Mais</w> <w n="17.2">lui</w>, <w n="17.3">sans</w> <w n="17.4">écouter</w> <w n="17.5">mes</w> <w n="17.6">rustiques</w> <w n="17.7">leçons</w>,</l>
						<l n="18" num="1.18"><w n="18.1">M</w>’<w n="18.2">apprenait</w> <w n="18.3">à</w> <w n="18.4">son</w> <w n="18.5">tour</w> <w n="18.6">d</w>’<w n="18.7">amoureuses</w> <w n="18.8">chansons</w> ;</l>
						<l n="19" num="1.19"><w n="19.1">La</w> <w n="19.2">douceur</w> <w n="19.3">d</w>’<w n="19.4">un</w> <w n="19.5">baiser</w> <w n="19.6">et</w> <w n="19.7">l</w>’<w n="19.8">empire</w> <w n="19.9">des</w> <w n="19.10">belles</w> ;</l>
						<l n="20" num="1.20"><w n="20.1">Tout</w> <w n="20.2">l</w>’<w n="20.3">Olympe</w> <w n="20.4">soumis</w> <w n="20.5">à</w> <w n="20.6">des</w> <w n="20.7">beautés</w> <w n="20.8">mortelles</w>.</l>
						<l n="21" num="1.21"><w n="21.1">Des</w> <w n="21.2">flammes</w> <w n="21.3">de</w> <w n="21.4">Vénus</w> <w n="21.5">Pluton</w> <w n="21.6">même</w> <w n="21.7">animé</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Et</w> <w n="22.2">le</w> <w n="22.3">plaisir</w> <w n="22.4">divin</w> <w n="22.5">d</w>’<w n="22.6">aimer</w> <w n="22.7">et</w> <w n="22.8">d</w>’<w n="22.9">être</w> <w n="22.10">aimé</w>.</l>
						<l n="23" num="1.23"><w n="23.1">Que</w> <w n="23.2">ces</w> <w n="23.3">chants</w> <w n="23.4">étaient</w> <w n="23.5">doux</w> ! <w n="23.6">Je</w> <w n="23.7">m</w>’<w n="23.8">y</w> <w n="23.9">laissai</w> <w n="23.10">surprendre</w>.</l>
						<l n="24" num="1.24"><w n="24.1">Mon</w> <w n="24.2">âme</w> <w n="24.3">ne</w> <w n="24.4">pouvait</w> <w n="24.5">se</w> <w n="24.6">lasser</w> <w n="24.7">de</w> <w n="24.8">l</w>’<w n="24.9">entendre</w>.</l>
						<l n="25" num="1.25"><w n="25.1">Tous</w> <w n="25.2">mes</w> <w n="25.3">préceptes</w> <w n="25.4">vains</w>, <w n="25.5">bannis</w> <w n="25.6">de</w> <w n="25.7">mon</w> <w n="25.8">esprit</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Pour</w> <w n="26.2">jamais</w> <w n="26.3">firent</w> <w n="26.4">place</w> <w n="26.5">à</w> <w n="26.6">tout</w> <w n="26.7">ce</w> <w n="26.8">qu</w>’<w n="26.9">il</w> <w n="26.10">m</w>’<w n="26.11">apprit</w>.</l>
						<l n="27" num="1.27"><w n="27.1">Il</w> <w n="27.2">connaît</w> <w n="27.3">sa</w> <w n="27.4">victoire</w>, <w n="27.5">et</w> <w n="27.6">sa</w> <w n="27.7">bouche</w> <w n="27.8">embaumée</w></l>
						<l n="28" num="1.28"><w n="28.1">Verse</w> <w n="28.2">un</w> <w n="28.3">miel</w> <w n="28.4">amoureux</w> <w n="28.5">sur</w> <w n="28.6">ma</w> <w n="28.7">bouche</w> <w n="28.8">pâmée</w>.</l>
						<l n="29" num="1.29"><w n="29.1">Il</w> <w n="29.2">coula</w> <w n="29.3">dans</w> <w n="29.4">mon</w> <w n="29.5">cœur</w>, <w n="29.6">et</w>, <w n="29.7">de</w> <w n="29.8">cet</w> <w n="29.9">heureux</w> <w n="29.10">jour</w>,</l>
						<l n="30" num="1.30"><w n="30.1">Et</w> <w n="30.2">ma</w> <w n="30.3">bouche</w> <w n="30.4">et</w> <w n="30.5">mon</w> <w n="30.6">cœur</w> <w n="30.7">n</w>’<w n="30.8">ont</w> <w n="30.9">respiré</w> <w n="30.10">qu</w>’<w n="30.11">amour</w> !</l>
					</lg>
				</div></body></text></TEI>