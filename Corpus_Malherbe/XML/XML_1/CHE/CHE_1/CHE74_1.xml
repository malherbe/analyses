<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES POÉTIQUES</title>
				<title type="sub_2">tome I</title>
				<title type="medium">Édition électronique</title>
				<author key="CHE">
					<name>
						<forename>André</forename>
						<nameLink>de</nameLink>
						<surname>CHÉNIER</surname>
					</name>
					<date from="1762" to="1794">1762-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5334 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2011">2011</date>
				<idno type="local">CHE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ŒUVRES POÉTIQUES</title>
						<title type="sub">tome I</title>
						<author>André de Chénier</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k5457953r.r=Oeuvres%20po%C3%A9tiques%20de%20Andr%C3%A9%20de%20Ch%C3%A9nier</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ŒUVRES POÉTIQUES</title>
								<author>André de Chénier</author>
								<editor>Avec une notice et des notes de Raoul Guillard</editor>
								<imprint>
									<publisher>Alphonse Lemerre, éditeur</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1790">1790</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Tomaison et mise en forme conformes à l’édition de 1856. La préface n’est pas reprise</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRADUCTIONS</head><div type="poem" key="CHE74">
					<head type="number">IX</head>
					<head type="main">Traduction de Mnaïs</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Bergers</w>, <w n="1.2">vous</w> <w n="1.3">dont</w> <w n="1.4">ici</w> <w n="1.5">la</w> <w n="1.6">chèvre</w> <w n="1.7">vagabonde</w>,</l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">brebis</w> <w n="2.3">se</w> <w n="2.4">traînant</w> <w n="2.5">sous</w> <w n="2.6">sa</w> <w n="2.7">laine</w> <w n="2.8">féconde</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Au</w> <w n="3.2">front</w> <w n="3.3">de</w> <w n="3.4">la</w> <w n="3.5">colline</w> <w n="3.6">accompagnent</w> <w n="3.7">les</w> <w n="3.8">pas</w>,</l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">la</w> <w n="4.3">jeune</w> <w n="4.4">Mnaïs</w>, <w n="4.5">rendez</w>, <w n="4.6">rendez</w>, <w n="4.7">hélas</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Par</w> <w n="5.2">Cybèle</w> <w n="5.3">et</w> <w n="5.4">Cérès</w> <w n="5.5">et</w> <w n="5.6">sa</w> <w n="5.7">fille</w> <w n="5.8">adorée</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Une</w> <w n="6.2">grâce</w> <w n="6.3">légère</w>, <w n="6.4">une</w> <w n="6.5">grâce</w> <w n="6.6">sacrée</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Naguère</w> <w n="7.2">auprès</w> <w n="7.3">de</w> <w n="7.4">vous</w> <w n="7.5">elle</w> <w n="7.6">avait</w> <w n="7.7">son</w> <w n="7.8">berceau</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">sa</w> <w n="8.3">vingtième</w> <w n="8.4">année</w> <w n="8.5">a</w> <w n="8.6">trouvé</w> <w n="8.7">le</w> <w n="8.8">tombeau</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Que</w> <w n="9.2">vos</w> <w n="9.3">agneaux</w> <w n="9.4">au</w> <w n="9.5">moins</w> <w n="9.6">viennent</w> <w n="9.7">près</w> <w n="9.8">de</w> <w n="9.9">ma</w> <w n="9.10">cendre</w></l>
						<l n="10" num="1.10"><w n="10.1">Me</w> <w n="10.2">bêler</w> <w n="10.3">les</w> <w n="10.4">accents</w> <w n="10.5">de</w> <w n="10.6">leur</w> <w n="10.7">voix</w> <w n="10.8">douce</w> <w n="10.9">et</w> <w n="10.10">tendre</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">paître</w> <w n="11.3">au</w> <w n="11.4">pied</w> <w n="11.5">d</w>’<w n="11.6">un</w> <w n="11.7">roc</w> <w n="11.8">où</w>, <w n="11.9">d</w>’<w n="11.10">un</w> <w n="11.11">ton</w> <w n="11.12">enchanteur</w>,</l>
						<l n="12" num="1.12"><w n="12.1">La</w> <w n="12.2">flûte</w> <w n="12.3">parlera</w> <w n="12.4">sous</w> <w n="12.5">les</w> <w n="12.6">doigts</w> <w n="12.7">du</w> <w n="12.8">pasteur</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Qu</w>’<w n="13.2">au</w> <w n="13.3">retour</w> <w n="13.4">du</w> <w n="13.5">printemps</w>, <w n="13.6">dépouillant</w> <w n="13.7">la</w> <w n="13.8">prairie</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Des</w> <w n="14.2">dons</w> <w n="14.3">du</w> <w n="14.4">villageois</w> <w n="14.5">ma</w> <w n="14.6">tombe</w> <w n="14.7">soit</w> <w n="14.8">fleurie</w> ;</l>
						<l n="15" num="1.15"><w n="15.1">Puis</w>, <w n="15.2">d</w>’<w n="15.3">une</w> <w n="15.4">brebis</w> <w n="15.5">mère</w> <w n="15.6">et</w> <w n="15.7">docile</w> <w n="15.8">à</w> <w n="15.9">sa</w> <w n="15.10">main</w></l>
						<l n="16" num="1.16"><w n="16.1">En</w> <w n="16.2">un</w> <w n="16.3">vase</w> <w n="16.4">d</w>’<w n="16.5">argile</w> <w n="16.6">il</w> <w n="16.7">pressera</w> <w n="16.8">le</w> <w n="16.9">sein</w> ;</l>
						<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">sera</w> <w n="17.3">chaque</w> <w n="17.4">jour</w> <w n="17.5">d</w>’<w n="17.6">un</w> <w n="17.7">lait</w> <w n="17.8">pur</w> <w n="17.9">arrosée</w></l>
						<l n="18" num="1.18"><w n="18.1">La</w> <w n="18.2">pierre</w> <w n="18.3">en</w> <w n="18.4">ce</w> <w n="18.5">tombeau</w> <w n="18.6">sur</w> <w n="18.7">mes</w> <w n="18.8">mânes</w> <w n="18.9">posée</w>.</l>
						<l n="19" num="1.19"><w n="19.1">Morts</w> <w n="19.2">et</w> <w n="19.3">vivants</w>, <w n="19.4">il</w> <w n="19.5">est</w> <w n="19.6">encor</w> <w n="19.7">pour</w> <w n="19.8">nous</w> <w n="19.9">unir</w></l>
						<l n="20" num="1.20"><w n="20.1">Un</w> <w n="20.2">commerce</w> <w n="20.3">d</w>’<w n="20.4">amour</w> <w n="20.5">et</w> <w n="20.6">de</w> <w n="20.7">doux</w> <w n="20.8">souvenir</w>.</l>
					</lg>
				</div></body></text></TEI>