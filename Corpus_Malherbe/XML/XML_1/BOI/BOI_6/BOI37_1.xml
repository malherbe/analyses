<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI37">
				<head type="main">FABLE D’ÉSOPE</head>
				<head type="sub">Le Bûcheron et la Mort</head>
				<opener>
					<dateline>
						<date when="1668">(1668)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">dos</w> <w n="1.3">chargé</w> <w n="1.4">de</w> <w n="1.5">bois</w>, <w n="1.6">et</w> <w n="1.7">le</w> <w n="1.8">corps</w> <w n="1.9">tout</w> <w n="1.10">en</w> <w n="1.11">eau</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">pauvre</w> <w n="2.3">bûcheron</w>, <w n="2.4">dans</w> <w n="2.5">l</w>’<w n="2.6">extrême</w> <w n="2.7">vieillesse</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Marchait</w>, <w n="3.2">en</w> <w n="3.3">haletant</w> <w n="3.4">de</w> <w n="3.5">peine</w> <w n="3.6">et</w> <w n="3.7">de</w> <w n="3.8">détresse</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Enfin</w>, <w n="4.2">las</w> <w n="4.3">de</w> <w n="4.4">souffrir</w>, <w n="4.5">jetant</w> <w n="4.6">là</w> <w n="4.7">son</w> <w n="4.8">fardeau</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Plutôt</w> <w n="5.2">que</w> <w n="5.3">de</w> <w n="5.4">s</w>’<w n="5.5">en</w> <w n="5.6">voir</w> <w n="5.7">accablé</w> <w n="5.8">de</w> <w n="5.9">nouveau</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Il</w> <w n="6.2">souhaite</w> <w n="6.3">la</w> <w n="6.4">Mort</w>, <w n="6.5">et</w> <w n="6.6">cent</w> <w n="6.7">fois</w> <w n="6.8">il</w> <w n="6.9">l</w>’<w n="6.10">appelle</w>.</l>
					<l n="7" num="1.7"><w n="7.1">La</w> <w n="7.2">Mort</w> <w n="7.3">vint</w> <w n="7.4">à</w> <w n="7.5">la</w> <w n="7.6">fin</w>. « <w n="7.7">Que</w> <w n="7.8">veux</w>-<w n="7.9">tu</w> ? <w n="7.10">cria</w>-<w n="7.11">t</w>-<w n="7.12">elle</w>.</l>
					<l n="8" num="1.8">— <w n="8.1">Qui</w> ? <w n="8.2">moi</w> ! <w n="8.3">dit</w>-<w n="8.4">il</w> <w n="8.5">alors</w>, <w n="8.6">prompt</w> <w n="8.7">à</w> <w n="8.8">se</w> <w n="8.9">corriger</w> ;</l>
					<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Que</w> <w n="9.2">tu</w> <w n="9.3">m</w>’<w n="9.4">aides</w> <w n="9.5">à</w> <w n="9.6">me</w> <w n="9.7">charger</w>. »</l>
				</lg>
			</div></body></text></TEI>