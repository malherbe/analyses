<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODES, ÉPIGRAMMES ET AUTRES POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>633 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1664" to="1704">1664-1704</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI40">
				<head type="main">Vers pour mettre sous le buste du Roi,</head>
				<head type="sub_1">fait par M. Girardon, l’année que <lb></lb>les Allemands prirent Belgrade</head>
				<opener>
					<dateline>
						<date when="1687">(1687)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">ce</w> <w n="1.4">Roi</w>, <w n="1.5">si</w> <w n="1.6">fameux</w> <w n="1.7">dans</w> <w n="1.8">la</w> <w n="1.9">paix</w>, <w n="1.10">dans</w> <w n="1.11">la</w> <w n="1.12">guerre</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">seul</w> <w n="2.3">fait</w> <w n="2.4">à</w> <w n="2.5">son</w> <w n="2.6">gré</w> <w n="2.7">le</w> <w n="2.8">destin</w> <w n="2.9">de</w> <w n="2.10">la</w> <w n="2.11">terre</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Tout</w> <w n="3.2">reconnaît</w> <w n="3.3">ses</w> <w n="3.4">lois</w> ; <w n="3.5">on</w> <w n="3.6">brigue</w> <w n="3.7">son</w> <w n="3.8">appui</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">ses</w> <w n="4.3">nombreux</w> <w n="4.4">combats</w> <w n="4.5">le</w> <w n="4.6">Rhin</w> <w n="4.7">frémit</w> <w n="4.8">encore</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">l</w>’<w n="5.3">Europe</w>, <w n="5.4">en</w> <w n="5.5">cent</w> <w n="5.6">lieux</w>, <w n="5.7">a</w> <w n="5.8">vu</w> <w n="5.9">fuir</w> <w n="5.10">devant</w> <w n="5.11">lui</w></l>
					<l n="6" num="1.6"><w n="6.1">Tous</w> <w n="6.2">ces</w> <w n="6.3">héros</w> <w n="6.4">si</w> <w n="6.5">fiers</w>, <w n="6.6">que</w> <w n="6.7">l</w>’<w n="6.8">on</w> <w n="6.9">voit</w> <w n="6.10">aujourd</w>’<w n="6.11">hui</w></l>
					<l n="7" num="1.7"><w n="7.1">Faire</w> <w n="7.2">fuir</w> <w n="7.3">l</w>’<w n="7.4">Ottoman</w> <w n="7.5">au</w> <w n="7.6">delà</w> <w n="7.7">du</w> <w n="7.8">Bosphore</w>.</l>
				</lg>
			</div></body></text></TEI>