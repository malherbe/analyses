<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">APPENDICE</title>
				<title type="sub_2">À L’ÉDITION DES ŒUVRES POÉTIQUES (édition Hachette, 1889)</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>125 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1654" to="1705">1654-1705</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI97">
				<head type="number">XII</head>
				<head type="main">Épigramme sur la réconciliation <lb></lb>de l’auteur et de Perrault</head>
				<opener>
					<dateline>
						<date when="1695">(1695)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Tout</w> <w n="1.2">le</w> <w n="1.3">trouble</w> <w n="1.4">poétique</w>,</l>
					<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">Paris</w> <w n="2.3">s</w>’<w n="2.4">en</w> <w n="2.5">va</w> <w n="2.6">cesser</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Perrault</w> <w n="3.2">l</w>’<w n="3.3">antipindarique</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">Despréaux</w> <w n="4.3">l</w>’<w n="4.4">homérique</w></l>
					<l n="5" num="1.5"><w n="5.1">Consentent</w> <w n="5.2">de</w> <w n="5.3">s</w>’<w n="5.4">embrasser</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Quelque</w> <w n="6.2">aigreur</w> <w n="6.3">qui</w> <w n="6.4">les</w> <w n="6.5">anime</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Quand</w>, <w n="7.2">malgré</w> <w n="7.3">l</w>’<w n="7.4">emportement</w></l>
					<l n="8" num="1.8"><w n="8.1">Comme</w> <w n="8.2">eux</w> <w n="8.3">l</w>’<w n="8.4">un</w> <w n="8.5">l</w>’<w n="8.6">autre</w> <w n="8.7">on</w> <w n="8.8">s</w>’<w n="8.9">estime</w>,</l>
					<l n="9" num="1.9"><w n="9.1">L</w>’<w n="9.2">accord</w> <w n="9.3">se</w> <w n="9.4">fait</w> <w n="9.5">aisément</w>.</l>
					<l n="10" num="1.10"><w n="10.1">Mon</w> <w n="10.2">embarras</w> <w n="10.3">est</w> <w n="10.4">comment</w></l>
					<l n="11" num="1.11"><w n="11.1">On</w> <w n="11.2">pourra</w> <w n="11.3">finir</w> <w n="11.4">la</w> <w n="11.5">guerre</w></l>
					<l n="12" num="1.12"><w n="12.1">De</w> <w n="12.2">Pradon</w> <w n="12.3">et</w> <w n="12.4">du</w> <w n="12.5">parterre</w>.</l>
				</lg>
			</div></body></text></TEI>