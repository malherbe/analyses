<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">APPENDICE</title>
				<title type="sub_2">À L’ÉDITION DES ŒUVRES POÉTIQUES (édition Hachette, 1889)</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>125 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1654" to="1705">1654-1705</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-06-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-06-29" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI87">
				<head type="number">II</head>
				<head type="main">Sonnet sur la mort d’une de mes parentes</head>
				<opener>
					<dateline>
						<date when="1663">(1663)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Parmi</w> <w n="1.2">les</w> <w n="1.3">doux</w> <w n="1.4">transports</w> <w n="1.5">d</w>’<w n="1.6">une</w> <w n="1.7">amitié</w> <w n="1.8">fidèle</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">voyais</w> <w n="2.3">près</w> <w n="2.4">d</w>’<w n="2.5">Iris</w> <w n="2.6">couler</w> <w n="2.7">mes</w> <w n="2.8">heureux</w> <w n="2.9">jours</w> :</l>
					<l n="3" num="1.3"><w n="3.1">Iris</w>, <w n="3.2">que</w> <w n="3.3">j</w>’<w n="3.4">aime</w> <w n="3.5">encore</w>, <w n="3.6">et</w> <w n="3.7">que</w> <w n="3.8">j</w>’<w n="3.9">aimai</w> <w n="3.10">toujours</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Brûlait</w> <w n="4.2">des</w> <w n="4.3">mêmes</w> <w n="4.4">feux</w> <w n="4.5">dont</w> <w n="4.6">je</w> <w n="4.7">brûlais</w> <w n="4.8">pour</w> <w n="4.9">elle</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Quand</w>, <w n="5.2">par</w> <w n="5.3">l</w>’<w n="5.4">ordre</w> <w n="5.5">du</w> <w n="5.6">ciel</w>, <w n="5.7">une</w> <w n="5.8">fièvre</w> <w n="5.9">cruelle</w></l>
					<l n="6" num="2.2"><w n="6.1">M</w>’<w n="6.2">enleva</w> <w n="6.3">cet</w> <w n="6.4">objet</w> <w n="6.5">de</w> <w n="6.6">mes</w> <w n="6.7">tendres</w> <w n="6.8">amours</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Et</w>, <w n="7.2">de</w> <w n="7.3">tous</w> <w n="7.4">mes</w> <w n="7.5">plaisirs</w> <w n="7.6">interrompant</w> <w n="7.7">le</w> <w n="7.8">cours</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Me</w> <w n="8.2">laissa</w> <w n="8.3">de</w> <w n="8.4">regrets</w> <w n="8.5">une</w> <w n="8.6">suite</w> <w n="8.7">éternelle</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ah</w> ! <w n="9.2">qu</w>’<w n="9.3">un</w> <w n="9.4">si</w> <w n="9.5">rude</w> <w n="9.6">coup</w> <w n="9.7">étonna</w> <w n="9.8">mes</w> <w n="9.9">esprits</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Que</w> <w n="10.2">je</w> <w n="10.3">versai</w> <w n="10.4">de</w> <w n="10.5">pleurs</w> ! <w n="10.6">que</w> <w n="10.7">je</w> <w n="10.8">poussai</w> <w n="10.9">de</w> <w n="10.10">cris</w> !</l>
					<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">combien</w> <w n="11.3">de</w> <w n="11.4">douleurs</w> <w n="11.5">ma</w> <w n="11.6">douleur</w> <w n="11.7">fut</w> <w n="11.8">suivie</w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Iris</w>, <w n="12.2">tu</w> <w n="12.3">fus</w> <w n="12.4">alors</w> <w n="12.5">moins</w> <w n="12.6">à</w> <w n="12.7">plaindre</w> <w n="12.8">que</w> <w n="12.9">moi</w> ;</l>
					<l n="13" num="4.2"><w n="13.1">Et</w>, <w n="13.2">bien</w> <w n="13.3">qu</w>’<w n="13.4">un</w> <w n="13.5">triste</w> <w n="13.6">sort</w> <w n="13.7">t</w>’<w n="13.8">ait</w> <w n="13.9">fait</w> <w n="13.10">perdre</w> <w n="13.11">la</w> <w n="13.12">vie</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Hélas</w> ! <w n="14.2">en</w> <w n="14.3">te</w> <w n="14.4">perdant</w> <w n="14.5">j</w>’<w n="14.6">ai</w> <w n="14.7">perdu</w> <w n="14.8">plus</w> <w n="14.9">que</w> <w n="14.10">toi</w>.</l>
				</lg>
			</div></body></text></TEI>