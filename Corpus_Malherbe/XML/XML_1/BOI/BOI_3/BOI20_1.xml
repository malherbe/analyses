<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ÉPITRES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1708 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1670" to="1698">1670-1698</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI20">
				<head type="main">ÉPÎTRE VII</head>
				<head type="sub">A M. RACINE</head>
				<opener>
					<dateline>
						<date when="1677">(1677)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space><w n="1.1">Que</w> <w n="1.2">tu</w> <w n="1.3">sais</w> <w n="1.4">bien</w>, <w n="1.5">Racine</w>, <w n="1.6">à</w> <w n="1.7">l</w>’<w n="1.8">aide</w> <w n="1.9">d</w>’<w n="1.10">un</w> <w n="1.11">acteur</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Émouvoir</w>, <w n="2.2">étonner</w>, <w n="2.3">ravir</w> <w n="2.4">un</w> <w n="2.5">spectateur</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Jamais</w> <w n="3.2">Iphigénie</w> <w n="3.3">en</w> <w n="3.4">Aulide</w> <w n="3.5">immolée</w></l>
					<l n="4" num="1.4"><w n="4.1">N</w>’<w n="4.2">a</w> <w n="4.3">coûté</w> <w n="4.4">tant</w> <w n="4.5">de</w> <w n="4.6">pleurs</w> <w n="4.7">à</w> <w n="4.8">la</w> <w n="4.9">Grèce</w> <w n="4.10">assemblée</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Que</w>, <w n="5.2">dans</w> <w n="5.3">l</w>’<w n="5.4">heureux</w> <w n="5.5">spectacle</w> <w n="5.6">à</w> <w n="5.7">nos</w> <w n="5.8">yeux</w> <w n="5.9">étalé</w>,</l>
					<l n="6" num="1.6"><w n="6.1">En</w> <w n="6.2">a</w> <w n="6.3">fait</w> <w n="6.4">sous</w> <w n="6.5">son</w> <w n="6.6">nom</w> <w n="6.7">verser</w> <w n="6.8">la</w> <w n="6.9">Champmeslé</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Ne</w> <w n="7.2">crois</w> <w n="7.3">pas</w> <w n="7.4">toutefois</w>, <w n="7.5">par</w> <w n="7.6">tes</w> <w n="7.7">savants</w> <w n="7.8">ouvrages</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Entraînant</w> <w n="8.2">tous</w> <w n="8.3">les</w> <w n="8.4">cœurs</w>, <w n="8.5">gagner</w> <w n="8.6">tous</w> <w n="8.7">les</w> <w n="8.8">suffrages</w> :</l>
					<l n="9" num="1.9"><w n="9.1">Sitôt</w> <w n="9.2">que</w> <w n="9.3">d</w>’<w n="9.4">Apollon</w> <w n="9.5">un</w> <w n="9.6">génie</w> <w n="9.7">inspiré</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Trouve</w> <w n="10.2">loin</w> <w n="10.3">du</w> <w n="10.4">vulgaire</w> <w n="10.5">un</w> <w n="10.6">chemin</w> <w n="10.7">ignoré</w>,</l>
					<l n="11" num="1.11"><w n="11.1">En</w> <w n="11.2">cent</w> <w n="11.3">lieux</w> <w n="11.4">contre</w> <w n="11.5">lui</w> <w n="11.6">les</w> <w n="11.7">cabales</w> <w n="11.8">s</w>’<w n="11.9">amassent</w> ;</l>
					<l n="12" num="1.12"><w n="12.1">Ses</w> <w n="12.2">rivaux</w> <w n="12.3">obscurcis</w> <w n="12.4">autour</w> <w n="12.5">de</w> <w n="12.6">lui</w> <w n="12.7">croassent</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">son</w> <w n="13.3">trop</w> <w n="13.4">de</w> <w n="13.5">lumière</w>, <w n="13.6">importunant</w> <w n="13.7">les</w> <w n="13.8">yeux</w>,</l>
					<l n="14" num="1.14"><w n="14.1">De</w> <w n="14.2">ses</w> <w n="14.3">propres</w> <w n="14.4">amis</w> <w n="14.5">lui</w> <w n="14.6">fait</w> <w n="14.7">des</w> <w n="14.8">envieux</w>.</l>
					<l n="15" num="1.15"><w n="15.1">La</w> <w n="15.2">mort</w> <w n="15.3">seule</w> <w n="15.4">ici</w>-<w n="15.5">bas</w>, <w n="15.6">en</w> <w n="15.7">terminant</w> <w n="15.8">sa</w> <w n="15.9">vie</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Peut</w> <w n="16.2">calmer</w> <w n="16.3">sur</w> <w n="16.4">son</w> <w n="16.5">nom</w> <w n="16.6">l</w>’<w n="16.7">injustice</w> <w n="16.8">et</w> <w n="16.9">l</w>’<w n="16.10">envie</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">Faire</w> <w n="17.2">au</w> <w n="17.3">poids</w> <w n="17.4">du</w> <w n="17.5">bon</w> <w n="17.6">sens</w> <w n="17.7">peser</w> <w n="17.8">tous</w> <w n="17.9">ses</w> <w n="17.10">écrits</w> ;</l>
					<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">donner</w> <w n="18.3">à</w> <w n="18.4">ses</w> <w n="18.5">vers</w> <w n="18.6">leur</w> <w n="18.7">légitime</w> <w n="18.8">prix</w>.</l>
					<l n="19" num="1.19"><space quantity="2" unit="char"></space><w n="19.1">Avant</w> <w n="19.2">qu</w>’<w n="19.3">un</w> <w n="19.4">peu</w> <w n="19.5">de</w> <w n="19.6">terre</w>, <w n="19.7">obtenu</w> <w n="19.8">par</w> <w n="19.9">prière</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Pour</w> <w n="20.2">jamais</w> <w n="20.3">sous</w>. <w n="20.4">la</w> <w n="20.5">tombe</w> <w n="20.6">eût</w> <w n="20.7">enfermé</w> <w n="20.8">Molière</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Mille</w> <w n="21.2">de</w> <w n="21.3">ces</w> <w n="21.4">beaux</w> <w n="21.5">traits</w>, <w n="21.6">aujourd</w>’<w n="21.7">hui</w> <w n="21.8">si</w> <w n="21.9">vantés</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Furent</w> <w n="22.2">des</w> <w n="22.3">sots</w> <w n="22.4">esprits</w> <w n="22.5">à</w> <w n="22.6">nos</w> <w n="22.7">yeux</w> <w n="22.8">rebutés</w>.</l>
					<l n="23" num="1.23"><w n="23.1">L</w>’<w n="23.2">ignorance</w> <w n="23.3">et</w> <w n="23.4">l</w>’<w n="23.5">erreur</w>, <w n="23.6">à</w> <w n="23.7">ses</w> <w n="23.8">naissantes</w> <w n="23.9">pièces</w>,</l>
					<l n="24" num="1.24"><w n="24.1">En</w> <w n="24.2">habits</w> <w n="24.3">de</w> <w n="24.4">marquis</w>, <w n="24.5">en</w> <w n="24.6">robes</w> <w n="24.7">de</w> <w n="24.8">comtesses</w>,</l>
					<l n="25" num="1.25"><w n="25.1">Venaient</w> <w n="25.2">pour</w> <w n="25.3">diffamer</w> <w n="25.4">son</w> <w n="25.5">chef</w>-<w n="25.6">d</w>’<w n="25.7">œuvre</w> <w n="25.8">nouveau</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Et</w> <w n="26.2">secouaient</w> <w n="26.3">la</w> <w n="26.4">tête</w> <w n="26.5">à</w> <w n="26.6">l</w>’<w n="26.7">endroit</w> <w n="26.8">le</w> <w n="26.9">plus</w> <w n="26.10">beau</w> :</l>
					<l n="27" num="1.27"><w n="27.1">Le</w> <w n="27.2">commandeur</w> <w n="27.3">voulait</w> <w n="27.4">la</w> <w n="27.5">scène</w> <w n="27.6">plus</w> <w n="27.7">exacte</w> ;</l>
					<l n="28" num="1.28"><w n="28.1">Le</w> <w n="28.2">vicomte</w>, <w n="28.3">indigné</w>, <w n="28.4">sortait</w> <w n="28.5">au</w> <w n="28.6">second</w> <w n="28.7">acte</w> ;</l>
					<l n="29" num="1.29"><w n="29.1">L</w>’<w n="29.2">un</w>, <w n="29.3">défenseur</w> <w n="29.4">zélé</w> <w n="29.5">des</w> <w n="29.6">bigots</w> <w n="29.7">mis</w> <w n="29.8">en</w> <w n="29.9">jeu</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Pour</w> <w n="30.2">prix</w> <w n="30.3">de</w> <w n="30.4">ses</w> <w n="30.5">bons</w> <w n="30.6">mots</w> <w n="30.7">le</w> <w n="30.8">condamnait</w> <w n="30.9">au</w> <w n="30.10">feu</w> ;</l>
					<l n="31" num="1.31"><w n="31.1">L</w>’<w n="31.2">autre</w>, <w n="31.3">fougueux</w> <w n="31.4">marquis</w>, <w n="31.5">lui</w> <w n="31.6">déclarant</w> <w n="31.7">la</w> <w n="31.8">guerre</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Voulait</w> <w n="32.2">venger</w> <w n="32.3">la</w> <w n="32.4">cour</w> <w n="32.5">immolée</w> <w n="32.6">au</w> <w n="32.7">parterre</w>.</l>
					<l n="33" num="1.33"><w n="33.1">Mais</w>, <w n="33.2">sitôt</w> <w n="33.3">que</w> <w n="33.4">d</w>’<w n="33.5">un</w> <w n="33.6">trait</w> <w n="33.7">de</w> <w n="33.8">ses</w> <w n="33.9">fatales</w> <w n="33.10">mains</w></l>
					<l n="34" num="1.34"><w n="34.1">La</w> <w n="34.2">Parque</w> <w n="34.3">l</w>’<w n="34.4">eut</w> <w n="34.5">rayé</w> <w n="34.6">du</w> <w n="34.7">nombre</w> <w n="34.8">des</w> <w n="34.9">humains</w>,</l>
					<l n="35" num="1.35"><w n="35.1">On</w> <w n="35.2">reconnut</w> <w n="35.3">le</w> <w n="35.4">prix</w> <w n="35.5">de</w> <w n="35.6">sa</w> <w n="35.7">Muse</w> <w n="35.8">éclipsée</w>.</l>
					<l n="36" num="1.36"><w n="36.1">L</w>’<w n="36.2">aimable</w> <w n="36.3">Comédie</w>, <w n="36.4">avec</w> <w n="36.5">lui</w> <w n="36.6">terrassée</w>,</l>
					<l n="37" num="1.37"><w n="37.1">En</w> <w n="37.2">vain</w> <w n="37.3">d</w>’<w n="37.4">un</w> <w n="37.5">coup</w> <w n="37.6">si</w> <w n="37.7">rude</w> <w n="37.8">espéra</w> <w n="37.9">revenir</w>,</l>
					<l n="38" num="1.38"><w n="38.1">Et</w> <w n="38.2">sur</w> <w n="38.3">ses</w> <w n="38.4">brodequins</w> <w n="38.5">ne</w> <w n="38.6">put</w> <w n="38.7">plus</w> <w n="38.8">se</w> <w n="38.9">tenir</w>.</l>
					<l n="39" num="1.39"><w n="39.1">Tel</w> <w n="39.2">fut</w> <w n="39.3">chez</w> <w n="39.4">nous</w> <w n="39.5">le</w> <w n="39.6">sort</w> <w n="39.7">du</w> <w n="39.8">théâtre</w> <w n="39.9">comique</w>.</l>
					<l n="40" num="1.40"><space quantity="2" unit="char"></space><w n="40.1">Toi</w> <w n="40.2">donc</w>, <w n="40.3">qui</w> <w n="40.4">t</w>’<w n="40.5">élevant</w> <w n="40.6">sur</w> <w n="40.7">la</w> <w n="40.8">scène</w> <w n="40.9">tragique</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Suis</w> <w n="41.2">les</w> <w n="41.3">pas</w> <w n="41.4">de</w> <w n="41.5">Sophocle</w>, <w n="41.6">et</w>, <w n="41.7">seul</w> <w n="41.8">de</w> <w n="41.9">tant</w> <w n="41.10">d</w>’<w n="41.11">esprits</w>,</l>
					<l n="42" num="1.42"><w n="42.1">De</w> <w n="42.2">Corneille</w> <w n="42.3">vieilli</w> <w n="42.4">sais</w> <w n="42.5">consoler</w> <w n="42.6">Paris</w>,</l>
					<l n="43" num="1.43"><w n="43.1">Cesse</w> <w n="43.2">de</w> <w n="43.3">t</w>’<w n="43.4">étonner</w>, <w n="43.5">si</w> <w n="43.6">l</w>’<w n="43.7">envie</w> <w n="43.8">animée</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Attachant</w> <w n="44.2">à</w> <w n="44.3">ton</w> <w n="44.4">nom</w> <w n="44.5">sa</w> <w n="44.6">rouille</w> <w n="44.7">envenimée</w>,</l>
					<l n="45" num="1.45"><w n="45.1">La</w> <w n="45.2">calomnie</w> <w n="45.3">en</w> <w n="45.4">main</w>, <w n="45.5">quelquefois</w> <w n="45.6">te</w> <w n="45.7">poursuit</w>.</l>
					<l n="46" num="1.46"><w n="46.1">En</w> <w n="46.2">cela</w>, <w n="46.3">comme</w> <w n="46.4">en</w> <w n="46.5">tout</w>, <w n="46.6">le</w> <w n="46.7">Ciel</w> <w n="46.8">qui</w> <w n="46.9">nous</w> <w n="46.10">conduit</w>,</l>
					<l n="47" num="1.47"><w n="47.1">Racine</w>, <w n="47.2">fait</w> <w n="47.3">briller</w> <w n="47.4">sa</w> <w n="47.5">profonde</w> <w n="47.6">sagesse</w>.</l>
					<l n="48" num="1.48"><w n="48.1">Le</w> <w n="48.2">mérite</w> <w n="48.3">en</w> <w n="48.4">repos</w>, <w n="48.5">s</w>’<w n="48.6">endort</w> <w n="48.7">dans</w> <w n="48.8">la</w> <w n="48.9">paresse</w> ;</l>
					<l n="49" num="1.49"><w n="49.1">Mais</w>, <w n="49.2">par</w> <w n="49.3">les</w> <w n="49.4">envieux</w> <w n="49.5">un</w> <w n="49.6">génie</w> <w n="49.7">excité</w>,</l>
					<l n="50" num="1.50"><w n="50.1">Au</w> <w n="50.2">comble</w> <w n="50.3">de</w> <w n="50.4">son</w> <w n="50.5">art</w> <w n="50.6">est</w> <w n="50.7">mille</w> <w n="50.8">fois</w> <w n="50.9">monté</w> ;</l>
					<l n="51" num="1.51"><w n="51.1">Plus</w> <w n="51.2">on</w> <w n="51.3">veut</w> <w n="51.4">l</w>’<w n="51.5">affaiblir</w>, <w n="51.6">plus</w> <w n="51.7">il</w> <w n="51.8">croît</w> <w n="51.9">et</w> <w n="51.10">s</w>’<w n="51.11">élance</w> ;</l>
					<l n="52" num="1.52"><w n="52.1">Au</w> <hi rend="ital"><w n="52.2">Cid</w></hi> <w n="52.3">persécuté</w> <hi rend="ital"><w n="52.4">Cinna</w></hi> <w n="52.5">doit</w> <w n="52.6">sa</w> <w n="52.7">naissance</w> ;</l>
					<l n="53" num="1.53"><w n="53.1">Et</w>, <w n="53.2">peut</w>-<w n="53.3">être</w>, <w n="53.4">ta</w> <w n="53.5">plume</w>, <w n="53.6">aux</w> <w n="53.7">censeurs</w> <w n="53.8">de</w> <w n="53.9">Pyrrhus</w></l>
					<l n="54" num="1.54"><w n="54.1">Doit</w> <w n="54.2">les</w> <w n="54.3">plus</w> <w n="54.4">nobles</w> <w n="54.5">traits</w> <w n="54.6">dont</w> <w n="54.7">tu</w> <w n="54.8">peignis</w> <w n="54.9">Burrhus</w>.</l>
					<l n="55" num="1.55"><space quantity="2" unit="char"></space><w n="55.1">Moi</w>-<w n="55.2">même</w>, <w n="55.3">dont</w> <w n="55.4">la</w> <w n="55.5">gloire</w> <w n="55.6">ici</w> <w n="55.7">moins</w> <w n="55.8">répandue</w></l>
					<l n="56" num="1.56"><w n="56.1">Des</w> <w n="56.2">pâles</w> <w n="56.3">envieux</w> <w n="56.4">ne</w> <w n="56.5">blesse</w> <w n="56.6">point</w> <w n="56.7">la</w> <w n="56.8">vue</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Mais</w> <w n="57.2">qu</w>’<w n="57.3">une</w> <w n="57.4">humeur</w> <w n="57.5">trop</w> <w n="57.6">libre</w>, <w n="57.7">un</w> <w n="57.8">esprit</w> <w n="57.9">peu</w> <w n="57.10">soumis</w>,</l>
					<l n="58" num="1.58"><w n="58.1">De</w> <w n="58.2">bonne</w> <w n="58.3">heure</w> <w n="58.4">a</w> <w n="58.5">pourvu</w> <w n="58.6">d</w>’<w n="58.7">utiles</w> <w n="58.8">ennemis</w>,</l>
					<l n="59" num="1.59"><w n="59.1">Je</w> <w n="59.2">dois</w> <w n="59.3">plus</w> <w n="59.4">à</w> <w n="59.5">leur</w> <w n="59.6">haine</w>, <w n="59.7">il</w> <w n="59.8">faut</w> <w n="59.9">que</w> <w n="59.10">je</w> <w n="59.11">l</w>’<w n="59.12">avoue</w>,</l>
					<l n="60" num="1.60"><w n="60.1">Qu</w>’<w n="60.2">au</w> <w n="60.3">faible</w> <w n="60.4">et</w> <w n="60.5">vain</w> <w n="60.6">talent</w> <w n="60.7">dont</w> <w n="60.8">la</w> <w n="60.9">France</w> <w n="60.10">me</w> <w n="60.11">loue</w>.</l>
					<l n="61" num="1.61"><w n="61.1">Leur</w> <w n="61.2">venin</w>, <w n="61.3">qui</w> <w n="61.4">sur</w> <w n="61.5">moi</w> <w n="61.6">brûle</w> <w n="61.7">de</w> <w n="61.8">s</w>’<w n="61.9">épancher</w>,</l>
					<l n="62" num="1.62"><w n="62.1">Tous</w> <w n="62.2">les</w> <w n="62.3">jours</w>, <w n="62.4">en</w> <w n="62.5">marchant</w>, <w n="62.6">m</w>’<w n="62.7">empêche</w> <w n="62.8">de</w> <w n="62.9">broncher</w> ;</l>
					<l n="63" num="1.63"><w n="63.1">Je</w> <w n="63.2">songe</w>, <w n="63.3">à</w> <w n="63.4">chaque</w> <w n="63.5">trait</w> <w n="63.6">que</w> <w n="63.7">ma</w> <w n="63.8">plume</w> <w n="63.9">hasarde</w>,</l>
					<l n="64" num="1.64"><w n="64.1">Que</w> <w n="64.2">d</w>’<w n="64.3">un</w> <w n="64.4">œil</w> <w n="64.5">dangereux</w> <w n="64.6">leur</w> <w n="64.7">troupe</w> <w n="64.8">me</w> <w n="64.9">regarde</w> ;</l>
					<l n="65" num="1.65"><w n="65.1">Je</w> <w n="65.2">sais</w> <w n="65.3">sur</w> <w n="65.4">leurs</w> <w n="65.5">avis</w> <w n="65.6">corriger</w> <w n="65.7">mes</w> <w n="65.8">erreurs</w>,</l>
					<l n="66" num="1.66"><w n="66.1">Et</w> <w n="66.2">je</w> <w n="66.3">mets</w> <w n="66.4">à</w> <w n="66.5">profit</w> <w n="66.6">leurs</w> <w n="66.7">malignes</w> <w n="66.8">fureurs</w>.</l>
					<l n="67" num="1.67"><w n="67.1">Sitôt</w> <w n="67.2">que</w> <w n="67.3">sur</w> <w n="67.4">un</w> <w n="67.5">vice</w> <w n="67.6">ils</w> <w n="67.7">pensent</w> <w n="67.8">me</w> <w n="67.9">confondre</w>,</l>
					<l n="68" num="1.68"><w n="68.1">C</w>’<w n="68.2">est</w> <w n="68.3">en</w> <w n="68.4">me</w> <w n="68.5">guérissant</w> <w n="68.6">que</w> <w n="68.7">je</w> <w n="68.8">sais</w> <w n="68.9">leur</w> <w n="68.10">répondre</w> ;</l>
					<l n="69" num="1.69"><w n="69.1">Et</w>, <w n="69.2">plus</w> <w n="69.3">en</w> <w n="69.4">criminel</w> <w n="69.5">ils</w> <w n="69.6">pensent</w> <w n="69.7">m</w>’<w n="69.8">ériger</w>,</l>
					<l n="70" num="1.70"><w n="70.1">Plus</w>, <w n="70.2">croissant</w> <w n="70.3">en</w> <w n="70.4">vertu</w>, <w n="70.5">je</w> <w n="70.6">songe</w> <w n="70.7">à</w> <w n="70.8">me</w> <w n="70.9">venger</w>.</l>
					<l n="71" num="1.71"><w n="71.1">Imite</w> <w n="71.2">mon</w> <w n="71.3">exemple</w> ; <w n="71.4">et</w>, <w n="71.5">lorsqu</w>’<w n="71.6">une</w> <w n="71.7">cabale</w>,</l>
					<l n="72" num="1.72"><w n="72.1">Un</w> <w n="72.2">flot</w> <w n="72.3">de</w> <w n="72.4">vains</w> <w n="72.5">auteurs</w> <w n="72.6">follement</w> <w n="72.7">te</w> <w n="72.8">ravale</w>,</l>
					<l n="73" num="1.73"><w n="73.1">Profite</w> <w n="73.2">de</w> <w n="73.3">leur</w> <w n="73.4">haine</w> <w n="73.5">et</w> <w n="73.6">de</w> <w n="73.7">leur</w> <w n="73.8">mauvais</w> <w n="73.9">sens</w> ;</l>
					<l n="74" num="1.74"><w n="74.1">Ris</w> <w n="74.2">du</w> <w n="74.3">bruit</w> <w n="74.4">passager</w> <w n="74.5">de</w> <w n="74.6">leurs</w> <w n="74.7">cris</w> <w n="74.8">impuissants</w>.</l>
					<l n="75" num="1.75"><w n="75.1">Que</w> <w n="75.2">peut</w> <w n="75.3">contre</w> <w n="75.4">tes</w> <w n="75.5">vers</w> <w n="75.6">une</w> <w n="75.7">ignorance</w> <w n="75.8">vaine</w> ?</l>
					<l n="76" num="1.76"><w n="76.1">Le</w> <w n="76.2">Parnasse</w> <w n="76.3">français</w>, <w n="76.4">ennobli</w> <w n="76.5">par</w> <w n="76.6">ta</w> <w n="76.7">veine</w>,</l>
					<l n="77" num="1.77"><w n="77.1">Contre</w> <w n="77.2">tous</w> <w n="77.3">ces</w> <w n="77.4">complots</w> <w n="77.5">saura</w> <w n="77.6">te</w> <w n="77.7">maintenir</w>,</l>
					<l n="78" num="1.78"><w n="78.1">Et</w> <w n="78.2">soulever</w> <w n="78.3">pour</w> <w n="78.4">toi</w> <w n="78.5">l</w>’<w n="78.6">équitable</w> <w n="78.7">avenir</w>.</l>
					<l n="79" num="1.79"><w n="79.1">Et</w> <w n="79.2">qui</w>, <w n="79.3">voyant</w> <w n="79.4">un</w> <w n="79.5">jour</w> <w n="79.6">la</w> <w n="79.7">douleur</w> <w n="79.8">vertueuse</w></l>
					<l n="80" num="1.80"><w n="80.1">De</w> <w n="80.2">Phèdre</w>, <w n="80.3">malgré</w> <w n="80.4">soi</w> <w n="80.5">perfide</w>, <w n="80.6">incestueuse</w>,</l>
					<l n="81" num="1.81"><w n="81.1">D</w>’<w n="81.2">un</w> <w n="81.3">si</w> <w n="81.4">noble</w> <w n="81.5">travail</w> <w n="81.6">justement</w> <w n="81.7">étonné</w>,</l>
					<l n="82" num="1.82"><w n="82.1">Ne</w> <w n="82.2">bénira</w> <w n="82.3">d</w>’<w n="82.4">abord</w> <w n="82.5">le</w> <w n="82.6">siècle</w> <w n="82.7">fortuné</w></l>
					<l n="83" num="1.83"><w n="83.1">Qui</w>, <w n="83.2">rendu</w> <w n="83.3">plus</w> <w n="83.4">fameux</w> <w n="83.5">par</w> <w n="83.6">tes</w> <w n="83.7">illustres</w> <w n="83.8">veilles</w>,</l>
					<l n="84" num="1.84"><w n="84.1">Vit</w> <w n="84.2">naître</w> <w n="84.3">sous</w> <w n="84.4">ta</w> <w n="84.5">main</w> <w n="84.6">ces</w> <w n="84.7">pompeuses</w> <w n="84.8">merveilles</w> ?</l>
					<l n="85" num="1.85"><space quantity="2" unit="char"></space><w n="85.1">Cependant</w>, <w n="85.2">laisse</w> <w n="85.3">ici</w> <w n="85.4">gronder</w> <w n="85.5">quelques</w> <w n="85.6">censeurs</w></l>
					<l n="86" num="1.86"><w n="86.1">Qu</w>’<w n="86.2">aigrissent</w> <w n="86.3">de</w> <w n="86.4">tes</w> <w n="86.5">vers</w> <w n="86.6">les</w> <w n="86.7">charmantes</w> <w n="86.8">douceurs</w>.</l>
					<l n="87" num="1.87"><w n="87.1">Et</w> <w n="87.2">qu</w>’<w n="87.3">importe</w> <w n="87.4">à</w> <w n="87.5">nos</w> <w n="87.6">vers</w> <w n="87.7">que</w> <w n="87.8">Perrin</w> <w n="87.9">les</w> <w n="87.10">admire</w> ;</l>
					<l n="88" num="1.88"><w n="88.1">Que</w> <w n="88.2">l</w>’<w n="88.3">auteur</w> <w n="88.4">du</w> <hi rend="ital"><w n="88.5">Jonas</w></hi> <w n="88.6">s</w>’<w n="88.7">empresse</w> <w n="88.8">pour</w> <w n="88.9">les</w> <w n="88.10">lire</w> ;</l>
					<l n="89" num="1.89"><w n="89.1">Qu</w>’<w n="89.2">ils</w> <w n="89.3">charment</w> <w n="89.4">de</w> <w n="89.5">Senlis</w> <w n="89.6">le</w> <w n="89.7">poète</w> <w n="89.8">idiot</w>,</l>
					<l n="90" num="1.90"><w n="90.1">Ou</w> <w n="90.2">le</w> <w n="90.3">sec</w> <w n="90.4">traducteur</w> <w n="90.5">du</w> <w n="90.6">français</w> <w n="90.7">d</w>’<w n="90.8">Amyot</w> ;</l>
					<l n="91" num="1.91"><w n="91.1">Pourvu</w> <w n="91.2">qu</w>’<w n="91.3">avec</w> <w n="91.4">éclat</w> <w n="91.5">leurs</w> <w n="91.6">rimes</w> <w n="91.7">débitées</w></l>
					<l n="92" num="1.92"><w n="92.1">Soient</w> <w n="92.2">du</w> <w n="92.3">peuple</w>, <w n="92.4">des</w> <w n="92.5">grands</w>, <w n="92.6">des</w> <w n="92.7">provinces</w> <w n="92.8">goûtées</w> ;</l>
					<l n="93" num="1.93"><w n="93.1">Pourvu</w> <w n="93.2">qu</w>’<w n="93.3">ils</w> <w n="93.4">puissent</w> <w n="93.5">plaire</w> <w n="93.6">au</w> <w n="93.7">plus</w> <w n="93.8">puissant</w> <w n="93.9">des</w> <w n="93.10">rois</w> ;</l>
					<l n="94" num="1.94"><w n="94.1">Qu</w>’<w n="94.2">à</w> <w n="94.3">Chantilly</w> <w n="94.4">Condé</w> <w n="94.5">les</w> <w n="94.6">souffre</w> <w n="94.7">quelquefois</w>,</l>
					<l n="95" num="1.95"><w n="95.1">Qu</w>’<w n="95.2">Enghien</w> <w n="95.3">en</w> <w n="95.4">soit</w> <w n="95.5">touché</w> ; <w n="95.6">que</w> <w n="95.7">Colbert</w> <w n="95.8">et</w> <w n="95.9">Vivonne</w>,</l>
					<l n="96" num="1.96"><w n="96.1">Que</w> <w n="96.2">La</w> <w n="96.3">Rochefoucauld</w>, <w n="96.4">Marsillac</w>, <w n="96.5">et</w> <w n="96.6">Pomponne</w>,</l>
					<l n="97" num="1.97"><w n="97.1">Et</w> <w n="97.2">mille</w> <w n="97.3">autres</w> <w n="97.4">qu</w>’<w n="97.5">ici</w> <w n="97.6">je</w> <w n="97.7">ne</w> <w n="97.8">puis</w> <w n="97.9">faire</w> <w n="97.10">entrer</w>,</l>
					<l n="98" num="1.98"><w n="98.1">A</w> <w n="98.2">leurs</w> <w n="98.3">traits</w> <w n="98.4">délicats</w> <w n="98.5">se</w> <w n="98.6">laissent</w> <w n="98.7">pénétrer</w> ?</l>
					<l n="99" num="1.99"><w n="99.1">Et</w>, <w n="99.2">plût</w> <w n="99.3">au</w> <w n="99.4">ciel</w> <w n="99.5">encor</w>, <w n="99.6">pour</w> <w n="99.7">couronner</w> <w n="99.8">l</w>’<w n="99.9">ouvrage</w>,</l>
					<l n="100" num="1.100"><w n="100.1">Que</w> <w n="100.2">Montausier</w> <w n="100.3">voulût</w> <w n="100.4">leur</w> <w n="100.5">donner</w> <w n="100.6">son</w> <w n="100.7">suffrage</w> !</l>
					<l n="101" num="1.101"><space quantity="2" unit="char"></space><w n="101.1">C</w>’<w n="101.2">est</w> <w n="101.3">à</w> <w n="101.4">de</w> <w n="101.5">tels</w> <w n="101.6">lecteurs</w> <w n="101.7">que</w> <w n="101.8">j</w>’<w n="101.9">offre</w> <w n="101.10">mes</w> <w n="101.11">écrits</w> ;</l>
					<l n="102" num="1.102"><w n="102.1">Mais</w>, <w n="102.2">pour</w> <w n="102.3">un</w> <w n="102.4">tas</w> <w n="102.5">grossier</w> <w n="102.6">de</w> <w n="102.7">frivoles</w> <w n="102.8">esprits</w>,</l>
					<l n="103" num="1.103"><w n="103.1">Admirateurs</w> <w n="103.2">zélés</w> <w n="103.3">de</w> <w n="103.4">toute</w> <w n="103.5">œuvre</w> <w n="103.6">insipide</w>,</l>
					<l n="104" num="1.104"><w n="104.1">Que</w>, <w n="104.2">non</w> <w n="104.3">loin</w> <w n="104.4">de</w> <w n="104.5">la</w> <w n="104.6">place</w> <w n="104.7">où</w> <w n="104.8">Brioché</w> <w n="104.9">préside</w></l>
					<l n="105" num="1.105"><w n="105.1">Sans</w> <w n="105.2">chercher</w> <w n="105.3">dans</w> <w n="105.4">les</w> <w n="105.5">vers</w> <w n="105.6">ni</w> <w n="105.7">cadence</w> <w n="105.8">ni</w> <w n="105.9">son</w>,</l>
					<l n="106" num="1.106"><w n="106.1">Il</w> <w n="106.2">s</w>’<w n="106.3">en</w> <w n="106.4">aille</w> <w n="106.5">admirer</w> <w n="106.6">le</w> <w n="106.7">savoir</w> <w n="106.8">de</w> <w n="106.9">Pradon</w> !</l>
				</lg>
			</div></body></text></TEI>