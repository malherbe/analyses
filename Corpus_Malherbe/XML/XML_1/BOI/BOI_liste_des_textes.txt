
	▪ Nicolas BOILEAU-DESPRÉAUX [BOI]

 		Liste des recueils de poésies
 		─────────────────────────────
		▫ DISCOURS AU ROI, 1665 [BOI_1]
		▫ SATIRES, 1666-1716 [BOI_2]
		▫ ÉPITRES, 1670-1698 [BOI_3]
		▫ L’ART POÉTIQUE, 1674 [BOI_4]
		▫ LE LUTRIN - POÈME HÉROÏ-COMIQUE, 1674-1683 [BOI_5]
		▫ ODES, ÉPIGRAMMES ET AUTRES POÉSIES, 1664-1704 [BOI_6]
		▫ APPENDICE - À L’ÉDITION DES ŒUVRES POÉTIQUES (édition Hachette, 1889), 1654-1705 [BOI_7]
