<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SATIRES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOI">
					<name>
						<forename>Nicolas</forename>
						<surname>BOILEAU-DESPRÉAUX</surname>
					</name>
					<date from="1636" to="1711">1636-1711</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2918 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BOI_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres poétiques de Boileau-Despréaux avec une introduction et des notes, par F. Brunetière</title>
						<author>Nicolas Boileau-Despréaux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k97348967</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Œuvres poétiques</title>
								<author>Nicolas Boileau-Despréaux</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Hachette</publisher>
									<date when="1889">1889</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1666" to="1716">1666-1716</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La présente édition correspond à une partie du texte d’origine.</p>
				<p>L’ensemble des textes versifiés est réparti en 7 fichiers correspondant chacun à un chapitre.</p>
				<p>La préface ainsi que les autres parties liminaires ne sont pas encodées.</p>
				<p>Les gravures et références des gravures sont pas incluses.</p>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BOI8">
				<head type="main">SATIRE VII</head>
				<opener>
					<dateline>
						<date when="1663">(1663)</date>
					</dateline>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="2" unit="char"></space><w n="1.1">Muse</w>, <w n="1.2">changeons</w> <w n="1.3">de</w> <w n="1.4">style</w>, <w n="1.5">et</w> <w n="1.6">quittons</w> <w n="1.7">la</w> <w n="1.8">satire</w> :</l>
					<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2">est</w> <w n="2.3">un</w> <w n="2.4">méchant</w> <w n="2.5">métier</w> <w n="2.6">que</w> <w n="2.7">celui</w> <w n="2.8">de</w> <w n="2.9">médire</w>,</l>
					<l n="3" num="1.3"><w n="3.1">A</w> <w n="3.2">l</w>’<w n="3.3">auteur</w> <w n="3.4">qui</w> <w n="3.5">l</w>’<w n="3.6">embrasse</w> <w n="3.7">il</w> <w n="3.8">est</w> <w n="3.9">toujours</w> <w n="3.10">fatal</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">mal</w> <w n="4.3">qu</w>’<w n="4.4">on</w> <w n="4.5">dit</w> <w n="4.6">d</w>’<w n="4.7">autrui</w> <w n="4.8">ne</w> <w n="4.9">produit</w> <w n="4.10">que</w> <w n="4.11">du</w> <w n="4.12">mal</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Maint</w> <w n="5.2">poète</w>, <w n="5.3">aveuglé</w> <w n="5.4">d</w>’<w n="5.5">une</w> <w n="5.6">telle</w> <w n="5.7">manie</w>,</l>
					<l n="6" num="1.6"><w n="6.1">En</w> <w n="6.2">courant</w> <w n="6.3">à</w> <w n="6.4">l</w>’<w n="6.5">honneur</w>, <w n="6.6">trouve</w> <w n="6.7">l</w>’<w n="6.8">ignominie</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">tel</w> <w n="7.3">mot</w>, <w n="7.4">pour</w> <w n="7.5">avoir</w> <w n="7.6">réjoui</w> <w n="7.7">le</w> <w n="7.8">lecteur</w>,</l>
					<l n="8" num="1.8"><w n="8.1">A</w> <w n="8.2">coûté</w> <w n="8.3">bien</w> <w n="8.4">souvent</w> <w n="8.5">des</w> <w n="8.6">larmes</w> <w n="8.7">à</w> <w n="8.8">l</w>’<w n="8.9">auteur</w>.</l>
					<l n="9" num="1.9"><space quantity="2" unit="char"></space><w n="9.1">Un</w> <w n="9.2">éloge</w> <w n="9.3">ennuyeux</w>, <w n="9.4">un</w> <w n="9.5">froid</w> <w n="9.6">panégyrique</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Peut</w> <w n="10.2">pourrir</w> <w n="10.3">à</w> <w n="10.4">son</w> <w n="10.5">aise</w> <w n="10.6">au</w> <w n="10.7">fond</w> <w n="10.8">d</w>’<w n="10.9">une</w> <w n="10.10">boutique</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Ne</w> <w n="11.2">craint</w> <w n="11.3">point</w> <w n="11.4">du</w> <w n="11.5">public</w> <w n="11.6">les</w> <w n="11.7">jugements</w> <w n="11.8">divers</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">n</w>’<w n="12.3">a</w> <w n="12.4">pour</w> <w n="12.5">ennemis</w> <w n="12.6">que</w> <w n="12.7">la</w> <w n="12.8">poudre</w> <w n="12.9">et</w> <w n="12.10">les</w> <w n="12.11">vers</w> :</l>
					<l n="13" num="1.13"><w n="13.1">Mais</w>, <w n="13.2">un</w> <w n="13.3">auteur</w> <w n="13.4">malin</w>, <w n="13.5">qui</w> <w n="13.6">rit</w> <w n="13.7">et</w> <w n="13.8">qui</w> <w n="13.9">fait</w> <w n="13.10">rire</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Qu</w>’<w n="14.2">on</w> <w n="14.3">blâme</w> <w n="14.4">en</w> <w n="14.5">le</w> <w n="14.6">lisant</w>, <w n="14.7">et</w> <w n="14.8">pourtant</w> <w n="14.9">qu</w>’<w n="14.10">on</w> <w n="14.11">veut</w> <w n="14.12">lire</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Dans</w> <w n="15.2">ses</w> <w n="15.3">plaisants</w> <w n="15.4">accès</w> <w n="15.5">qui</w> <w n="15.6">se</w> <w n="15.7">croit</w> <w n="15.8">tout</w> <w n="15.9">permis</w>,</l>
					<l n="16" num="1.16"><w n="16.1">De</w> <w n="16.2">ses</w> <w n="16.3">propres</w> <w n="16.4">rieurs</w> <w n="16.5">se</w> <w n="16.6">fait</w> <w n="16.7">des</w> <w n="16.8">ennemis</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Un</w> <w n="17.2">discours</w> <w n="17.3">trop</w> <w n="17.4">sincère</w> <w n="17.5">aisément</w> <w n="17.6">nous</w> <w n="17.7">outrage</w> :</l>
					<l n="18" num="1.18"><w n="18.1">Chacun</w> <w n="18.2">dans</w> <w n="18.3">ce</w> <w n="18.4">miroir</w> <w n="18.5">pense</w> <w n="18.6">voir</w> <w n="18.7">son</w> <w n="18.8">visage</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">tel</w>, <w n="19.3">en</w> <w n="19.4">vous</w> <w n="19.5">lisant</w>, <w n="19.6">admire</w> <w n="19.7">chaque</w> <w n="19.8">trait</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Qui</w>, <w n="20.2">dans</w> <w n="20.3">le</w> <w n="20.4">fond</w> <w n="20.5">de</w> <w n="20.6">l</w>’<w n="20.7">âme</w>, <w n="20.8">et</w> <w n="20.9">vous</w> <w n="20.10">craint</w> <w n="20.11">et</w> <w n="20.12">vous</w> <w n="20.13">hait</w>.</l>
					<l n="21" num="1.21"><space quantity="2" unit="char"></space><w n="21.1">Muse</w>, <w n="21.2">c</w>’<w n="21.3">est</w> <w n="21.4">donc</w> <w n="21.5">en</w> <w n="21.6">vain</w> <w n="21.7">que</w> <w n="21.8">la</w> <w n="21.9">main</w> <w n="21.10">vous</w> <w n="21.11">démange</w>,</l>
					<l n="22" num="1.22"><w n="22.1">S</w>’<w n="22.2">il</w> <w n="22.3">faut</w> <w n="22.4">rimer</w> <w n="22.5">ici</w>, <w n="22.6">rimons</w> <w n="22.7">quelque</w> <w n="22.8">louange</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Et</w> <w n="23.2">cherchons</w> <w n="23.3">un</w> <w n="23.4">héros</w>, <w n="23.5">parmi</w> <w n="23.6">cet</w> <w n="23.7">univers</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Digne</w> <w n="24.2">de</w> <w n="24.3">notre</w> <w n="24.4">encens</w> <w n="24.5">et</w> <w n="24.6">digne</w> <w n="24.7">de</w> <w n="24.8">nos</w> <w n="24.9">vers</w>.</l>
					<l n="25" num="1.25"><w n="25.1">Mais</w>, <w n="25.2">à</w> <w n="25.3">ce</w> <w n="25.4">grand</w> <w n="25.5">effort</w> <w n="25.6">en</w> <w n="25.7">vain</w> <w n="25.8">je</w> <w n="25.9">vous</w> <w n="25.10">anime</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Je</w> <w n="26.2">ne</w> <w n="26.3">puis</w> <w n="26.4">pour</w> <w n="26.5">louer</w> <w n="26.6">rencontrer</w> <w n="26.7">une</w> <w n="26.8">rime</w>.</l>
					<l n="27" num="1.27"><w n="27.1">Dès</w> <w n="27.2">que</w> <w n="27.3">j</w>’<w n="27.4">y</w> <w n="27.5">veux</w> <w n="27.6">rêver</w>, <w n="27.7">ma</w> <w n="27.8">veine</w> <w n="27.9">est</w> <w n="27.10">aux</w> <w n="27.11">abois</w>.</l>
					<l n="28" num="1.28"><w n="28.1">J</w>’<w n="28.2">ai</w> <w n="28.3">beau</w> <w n="28.4">frotter</w> <w n="28.5">mon</w> <w n="28.6">front</w>, <w n="28.7">j</w>’<w n="28.8">ai</w> <w n="28.9">beau</w> <w n="28.10">mordre</w> <w n="28.11">mes</w> <w n="28.12">doigts</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Je</w> <w n="29.2">ne</w> <w n="29.3">puis</w> <w n="29.4">arracher</w> <w n="29.5">du</w> <w n="29.6">creux</w> <w n="29.7">de</w> <w n="29.8">ma</w> <w n="29.9">cervelle</w></l>
					<l n="30" num="1.30"><w n="30.1">Que</w> <w n="30.2">des</w> <w n="30.3">vers</w> <w n="30.4">plus</w> <w n="30.5">forcés</w> <w n="30.6">que</w> <w n="30.7">ceux</w> <w n="30.8">de</w> <hi rend="ital"><w n="30.9">la</w> <w n="30.10">Pucelle</w></hi> ;</l>
					<l n="31" num="1.31"><w n="31.1">Je</w> <w n="31.2">pense</w> <w n="31.3">être</w> <w n="31.4">à</w> <w n="31.5">la</w> <w n="31.6">gêne</w> ; <w n="31.7">et</w>, <w n="31.8">pour</w> <w n="31.9">un</w> <w n="31.10">tel</w> <w n="31.11">dessein</w>,</l>
					<l n="32" num="1.32"><w n="32.1">La</w> <w n="32.2">plume</w> <w n="32.3">et</w> <w n="32.4">le</w> <w n="32.5">papier</w> <w n="32.6">résistent</w> <w n="32.7">à</w> <w n="32.8">ma</w> <w n="32.9">main</w>.</l>
					<l n="33" num="1.33"><w n="33.1">Mais</w>, <w n="33.2">quand</w> <w n="33.3">il</w> <w n="33.4">faut</w> <w n="33.5">railler</w>, <w n="33.6">j</w>’<w n="33.7">ai</w> <w n="33.8">ce</w> <w n="33.9">que</w> <w n="33.10">je</w> <w n="33.11">souhaite</w>.</l>
					<l n="34" num="1.34"><w n="34.1">Alors</w>, <w n="34.2">certes</w>, <w n="34.3">alors</w> <w n="34.4">je</w> <w n="34.5">me</w> <w n="34.6">connais</w> <w n="34.7">poète</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Phébus</w>, <w n="35.2">dès</w> <w n="35.3">que</w> <w n="35.4">je</w> <w n="35.5">parle</w>, <w n="35.6">est</w> <w n="35.7">prêt</w> <w n="35.8">à</w> <w n="35.9">m</w>’<w n="35.10">exaucer</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Mes</w> <w n="36.2">mots</w> <w n="36.3">viennent</w> <w n="36.4">sans</w> <w n="36.5">peine</w>, <w n="36.6">et</w> <w n="36.7">courent</w> <w n="36.8">se</w> <w n="36.9">placer</w>.</l>
					<l n="37" num="1.37"><w n="37.1">Faut</w>-<w n="37.2">il</w> <w n="37.3">peindre</w> <w n="37.4">un</w> <w n="37.5">fripon</w> <w n="37.6">fameux</w> <w n="37.7">dans</w> <w n="37.8">cette</w> <w n="37.9">ville</w> ?</l>
					<l n="38" num="1.38"><w n="38.1">Ma</w> <w n="38.2">main</w>, <w n="38.3">sans</w> <w n="38.4">que</w> <w n="38.5">j</w>’<w n="38.6">y</w> <w n="38.7">rêve</w>, <w n="38.8">écrira</w> <w n="38.9">Raumaville</w>.</l>
					<l n="39" num="1.39"><w n="39.1">Faut</w>-<w n="39.2">il</w> <w n="39.3">d</w>’<w n="39.4">un</w> <w n="39.5">sot</w> <w n="39.6">parfait</w> <w n="39.7">montrer</w> <w n="39.8">l</w>’<w n="39.9">original</w> ?</l>
					<l n="40" num="1.40"><w n="40.1">Ma</w> <w n="40.2">plume</w> <w n="40.3">au</w> <w n="40.4">bout</w> <w n="40.5">du</w> <w n="40.6">vers</w> <w n="40.7">d</w>’<w n="40.8">abord</w> <w n="40.9">trouve</w> <w n="40.10">Sofal</w>.</l>
					<l n="41" num="1.41"><w n="41.1">Je</w> <w n="41.2">sens</w> <w n="41.3">que</w> <w n="41.4">mon</w> <w n="41.5">esprit</w> <w n="41.6">travaille</w> <w n="41.7">de</w> <w n="41.8">génie</w>.</l>
					<l n="42" num="1.42"><w n="42.1">Faut</w>-<w n="42.2">il</w> <w n="42.3">d</w>’<w n="42.4">un</w> <w n="42.5">froid</w> <w n="42.6">rimeur</w> <w n="42.7">dépeindre</w> <w n="42.8">la</w> <w n="42.9">manie</w> ?</l>
					<l n="43" num="1.43"><w n="43.1">Mes</w> <w n="43.2">vers</w>, <w n="43.3">comme</w> <w n="43.4">un</w> <w n="43.5">torrent</w>, <w n="43.6">coulent</w> <w n="43.7">sur</w> <w n="43.8">le</w> <w n="43.9">papier</w> :</l>
					<l n="44" num="1.44"><w n="44.1">Je</w> <w n="44.2">rencontre</w> <w n="44.3">à</w> <w n="44.4">la</w> <w n="44.5">fois</w> <w n="44.6">Perrin</w> <w n="44.7">et</w> <w n="44.8">Pelletier</w>,</l>
					<l n="45" num="1.45"><w n="45.1">Bonnecorse</w>, <w n="45.2">Pradon</w>, <w n="45.3">Colletet</w>, <w n="45.4">Titreville</w> ;</l>
					<l n="46" num="1.46"><w n="46.1">Et</w>, <w n="46.2">pour</w> <w n="46.3">un</w> <w n="46.4">que</w> <w n="46.5">je</w> <w n="46.6">veux</w>, <w n="46.7">j</w>’<w n="46.8">en</w> <w n="46.9">trouve</w> <w n="46.10">plus</w> <w n="46.11">de</w> <w n="46.12">mille</w>.</l>
					<l n="47" num="1.47"><w n="47.1">Aussitôt</w> <w n="47.2">je</w> <w n="47.3">triomphe</w> ; <w n="47.4">et</w> <w n="47.5">ma</w> <w n="47.6">Muse</w> <w n="47.7">en</w> <w n="47.8">secret</w></l>
					<l n="48" num="1.48"><w n="48.1">S</w>’<w n="48.2">estime</w> <w n="48.3">et</w> <w n="48.4">s</w>’<w n="48.5">applaudit</w> <w n="48.6">du</w> <w n="48.7">beau</w> <w n="48.8">coup</w> <w n="48.9">qu</w>’<w n="48.10">elle</w> <w n="48.11">a</w> <w n="48.12">fait</w>.</l>
					<l n="49" num="1.49"><w n="49.1">C</w>’<w n="49.2">est</w> <w n="49.3">en</w> <w n="49.4">vain</w> <w n="49.5">qu</w>’<w n="49.6">au</w> <w n="49.7">milieu</w> <w n="49.8">de</w> <w n="49.9">ma</w> <w n="49.10">fureur</w> <w n="49.11">extrême</w></l>
					<l n="50" num="1.50"><w n="50.1">Je</w> <w n="50.2">me</w> <w n="50.3">fais</w> <w n="50.4">quelquefois</w> <w n="50.5">des</w> <w n="50.6">leçons</w> <w n="50.7">à</w> <w n="50.8">moi</w>-<w n="50.9">même</w> ;</l>
					<l n="51" num="1.51"><w n="51.1">En</w> <w n="51.2">vain</w> <w n="51.3">je</w> <w n="51.4">veux</w> <w n="51.5">au</w> <w n="51.6">moins</w> <w n="51.7">faire</w> <w n="51.8">grâce</w> <w n="51.9">à</w> <w n="51.10">quelqu</w>’<w n="51.11">un</w> ;</l>
					<l n="52" num="1.52"><w n="52.1">Ma</w> <w n="52.2">plume</w> <w n="52.3">aurait</w> <w n="52.4">regret</w> <w n="52.5">d</w>’<w n="52.6">en</w> <w n="52.7">épargner</w> <w n="52.8">aucun</w>,</l>
					<l n="53" num="1.53"><w n="53.1">Et</w>, <w n="53.2">sitôt</w> <w n="53.3">qu</w>’<w n="53.4">une</w> <w n="53.5">fois</w> <w n="53.6">la</w> <w n="53.7">verve</w> <w n="53.8">me</w> <w n="53.9">domine</w>,</l>
					<l n="54" num="1.54"><w n="54.1">Tout</w> <w n="54.2">ce</w> <w n="54.3">qui</w> <w n="54.4">s</w>’<w n="54.5">offre</w> <w n="54.6">à</w> <w n="54.7">moi</w> <w n="54.8">passe</w> <w n="54.9">par</w> <w n="54.10">l</w>’<w n="54.11">étamine</w>.</l>
					<l n="55" num="1.55"><w n="55.1">Le</w> <w n="55.2">mérite</w> <w n="55.3">pourtant</w> <w n="55.4">m</w>’<w n="55.5">est</w> <w n="55.6">toujours</w> <w n="55.7">précieux</w>.</l>
					<l n="56" num="1.56"><w n="56.1">Mais</w>, <w n="56.2">tout</w> <w n="56.3">fat</w> <w n="56.4">me</w> <w n="56.5">déplaît</w>, <w n="56.6">et</w> <w n="56.7">me</w> <w n="56.8">blesse</w> <w n="56.9">les</w> <w n="56.10">yeux</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Je</w> <w n="57.2">le</w> <w n="57.3">poursuis</w> <w n="57.4">partout</w>, <w n="57.5">comme</w> <w n="57.6">un</w> <w n="57.7">chien</w> <w n="57.8">fait</w> <w n="57.9">sa</w> <w n="57.10">proie</w>,</l>
					<l n="58" num="1.58"><w n="58.1">Et</w> <w n="58.2">ne</w> <w n="58.3">le</w> <w n="58.4">sens</w> <w n="58.5">jamais</w> <w n="58.6">qu</w>’<w n="58.7">aussitôt</w> <w n="58.8">je</w> <w n="58.9">n</w>’<w n="58.10">aboie</w>.</l>
					<l n="59" num="1.59"><w n="59.1">Enfin</w>, <w n="59.2">sans</w> <w n="59.3">perdre</w> <w n="59.4">temps</w> <w n="59.5">en</w> <w n="59.6">de</w> <w n="59.7">si</w> <w n="59.8">vains</w> <w n="59.9">propos</w>,</l>
					<l n="60" num="1.60"><w n="60.1">Je</w> <w n="60.2">sais</w> <w n="60.3">coudre</w> <w n="60.4">une</w> <w n="60.5">rime</w> <w n="60.6">au</w> <w n="60.7">bout</w> <w n="60.8">de</w> <w n="60.9">quelques</w> <w n="60.10">mots</w>.</l>
					<l n="61" num="1.61"><w n="61.1">Souvent</w> <w n="61.2">j</w>’<w n="61.3">habille</w> <w n="61.4">en</w> <w n="61.5">vers</w> <w n="61.6">une</w> <w n="61.7">maligne</w> <w n="61.8">prose</w> :</l>
					<l n="62" num="1.62"><w n="62.1">C</w>’<w n="62.2">est</w> <w n="62.3">par</w> <w n="62.4">là</w> <w n="62.5">que</w> <w n="62.6">je</w> <w n="62.7">vaux</w>, <w n="62.8">si</w> <w n="62.9">je</w> <w n="62.10">vaux</w> <w n="62.11">quelque</w> <w n="62.12">chose</w>.</l>
					<l n="63" num="1.63"><w n="63.1">Ainsi</w>, <w n="63.2">soit</w> <w n="63.3">que</w> <w n="63.4">bientôt</w>, <w n="63.5">par</w> <w n="63.6">une</w> <w n="63.7">dure</w> <w n="63.8">loi</w>,</l>
					<l n="64" num="1.64"><w n="64.1">La</w> <w n="64.2">mort</w>, <w n="64.3">d</w>’<w n="64.4">un</w> <w n="64.5">vol</w> <w n="64.6">affreux</w>, <w n="64.7">vienne</w> <w n="64.8">fondre</w> <w n="64.9">sur</w> <w n="64.10">moi</w>,</l>
					<l n="65" num="1.65"><w n="65.1">Soit</w> <w n="65.2">que</w> <w n="65.3">le</w> <w n="65.4">ciel</w> <w n="65.5">me</w> <w n="65.6">garde</w> <w n="65.7">un</w> <w n="65.8">cours</w> <w n="65.9">long</w> <w n="65.10">et</w> <w n="65.11">tranquille</w>,</l>
					<l n="66" num="1.66"><w n="66.1">A</w> <w n="66.2">Rome</w> <w n="66.3">ou</w> <w n="66.4">dans</w> <w n="66.5">Paris</w>, <w n="66.6">aux</w> <w n="66.7">champs</w> <w n="66.8">ou</w> <w n="66.9">dans</w> <w n="66.10">la</w> <w n="66.11">ville</w>,</l>
					<l n="67" num="1.67"><w n="67.1">Dût</w> <w n="67.2">ma</w> <w n="67.3">Muse</w> <w n="67.4">par</w> <w n="67.5">là</w> <w n="67.6">choquer</w> <w n="67.7">tout</w> <w n="67.8">l</w>’<w n="67.9">univers</w>,</l>
					<l n="68" num="1.68"><w n="68.1">Riche</w>, <w n="68.2">gueux</w>, <w n="68.3">triste</w>, <w n="68.4">ou</w> <w n="68.5">gai</w>, <w n="68.6">je</w> <w n="68.7">veux</w> <w n="68.8">faire</w> <w n="68.9">des</w> <w n="68.10">vers</w>.</l>
					<l n="69" num="1.69"><space quantity="2" unit="char"></space>« <w n="69.1">Pauvre</w> <w n="69.2">esprit</w>, <w n="69.3">dira</w>-<w n="69.4">t</w>-<w n="69.5">on</w>, <w n="69.6">que</w> <w n="69.7">je</w> <w n="69.8">plains</w> <w n="69.9">ta</w> <w n="69.10">folie</w> !</l>
					<l n="70" num="1.70"><w n="70.1">Modère</w> <w n="70.2">ces</w> <w n="70.3">bouillons</w> <w n="70.4">de</w> <w n="70.5">ta</w> <w n="70.6">mélancolie</w>,</l>
					<l n="71" num="1.71"><w n="71.1">Et</w> <w n="71.2">garde</w>, <w n="71.3">qu</w>’<w n="71.4">un</w> <w n="71.5">de</w> <w n="71.6">ceux</w> <w n="71.7">que</w> <w n="71.8">tu</w> <w n="71.9">penses</w> <w n="71.10">blâmer</w></l>
					<l n="72" num="1.72"><w n="72.1">N</w>’<w n="72.2">éteigne</w> <w n="72.3">dans</w> <w n="72.4">ton</w> <w n="72.5">sang</w> <w n="72.6">cette</w> <w n="72.7">ardeur</w> <w n="72.8">de</w> <w n="72.9">rimer</w>. »</l>
					<l n="73" num="1.73"><w n="73.1">Eh</w> <w n="73.2">quoi</w> ! <w n="73.3">lorsqu</w>’<w n="73.4">autrefois</w> <w n="73.5">Horace</w>, <w n="73.6">après</w> <w n="73.7">Lucile</w>,</l>
					<l n="74" num="1.74"><w n="74.1">Exhalait</w> <w n="74.2">en</w> <w n="74.3">bons</w> <w n="74.4">mots</w> <w n="74.5">les</w> <w n="74.6">vapeurs</w> <w n="74.7">de</w> <w n="74.8">sa</w> <w n="74.9">bile</w>,</l>
					<l n="75" num="1.75"><w n="75.1">Et</w>, <w n="75.2">vengeant</w> <w n="75.3">la</w> <w n="75.4">vertu</w> <w n="75.5">par</w> <w n="75.6">des</w> <w n="75.7">traits</w> <w n="75.8">éclatants</w>,</l>
					<l n="76" num="1.76"><w n="76.1">Allait</w> <w n="76.2">ôter</w> <w n="76.3">le</w> <w n="76.4">masque</w> <w n="76.5">aux</w> <w n="76.6">vices</w> <w n="76.7">de</w> <w n="76.8">son</w> <w n="76.9">temps</w> ;</l>
					<l n="77" num="1.77"><w n="77.1">Ou</w> <w n="77.2">bien</w>, <w n="77.3">quand</w> <w n="77.4">Juvénal</w>, <w n="77.5">de</w> <w n="77.6">sa</w> <w n="77.7">mordante</w> <w n="77.8">plume</w></l>
					<l n="78" num="1.78"><w n="78.1">Faisant</w> <w n="78.2">couler</w> <w n="78.3">des</w> <w n="78.4">flots</w> <w n="78.5">de</w> <w n="78.6">fiel</w> <w n="78.7">et</w> <w n="78.8">d</w>’<w n="78.9">amertume</w>,</l>
					<l n="79" num="1.79"><w n="79.1">Gourmandait</w> <w n="79.2">en</w> <w n="79.3">courroux</w> <w n="79.4">tout</w> <w n="79.5">le</w> <w n="79.6">peuple</w> <w n="79.7">latin</w>,</l>
					<l n="80" num="1.80"><w n="80.1">L</w>’<w n="80.2">un</w> <w n="80.3">ou</w> <w n="80.4">l</w>’<w n="80.5">autre</w> <w n="80.6">fit</w>-<w n="80.7">il</w> <w n="80.8">une</w> <w n="80.9">tragique</w> <w n="80.10">fin</w> ?</l>
					<l n="81" num="1.81"><w n="81.1">Et</w> <w n="81.2">que</w> <w n="81.3">craindre</w>, <w n="81.4">après</w> <w n="81.5">tout</w>, <w n="81.6">d</w>’<w n="81.7">une</w> <w n="81.8">fureur</w> <w n="81.9">si</w> <w n="81.10">vaine</w> ?</l>
					<l n="82" num="1.82"><w n="82.1">Personne</w> <w n="82.2">ne</w> <w n="82.3">connaît</w> <w n="82.4">ni</w> <w n="82.5">mon</w> <w n="82.6">nom</w> <w n="82.7">ni</w> <w n="82.8">ma</w> <w n="82.9">veine</w> ;</l>
					<l n="83" num="1.83"><w n="83.1">On</w> <w n="83.2">ne</w> <w n="83.3">voit</w> <w n="83.4">point</w> <w n="83.5">mes</w> <w n="83.6">vers</w>, <w n="83.7">à</w> <w n="83.8">l</w>’<w n="83.9">envi</w> <w n="83.10">de</w> <w n="83.11">Montreuil</w>,</l>
					<l n="84" num="1.84"><w n="84.1">Grossir</w> <w n="84.2">impunément</w> <w n="84.3">les</w> <w n="84.4">feuillets</w> <w n="84.5">d</w>’<w n="84.6">un</w> <w n="84.7">recueil</w> ;</l>
					<l n="85" num="1.85"><w n="85.1">A</w> <w n="85.2">peine</w>, <w n="85.3">quelquefois</w>, <w n="85.4">je</w> <w n="85.5">me</w> <w n="85.6">force</w> <w n="85.7">à</w> <w n="85.8">les</w> <w n="85.9">lire</w>,</l>
					<l n="86" num="1.86"><w n="86.1">Pour</w> <w n="86.2">plaire</w> <w n="86.3">à</w> <w n="86.4">quelque</w> <w n="86.5">ami</w> <w n="86.6">que</w> <w n="86.7">charme</w> <w n="86.8">la</w> <w n="86.9">satire</w>,</l>
					<l n="87" num="1.87"><w n="87.1">Qui</w> <w n="87.2">me</w> <w n="87.3">flatte</w> <w n="87.4">peut</w>-<w n="87.5">être</w>, <w n="87.6">et</w>, <w n="87.7">d</w>’<w n="87.8">un</w> <w n="87.9">air</w> <w n="87.10">imposteur</w>,</l>
					<l n="88" num="1.88"><w n="88.1">Rit</w> <w n="88.2">tout</w> <w n="88.3">haut</w> <w n="88.4">de</w> <w n="88.5">l</w>’<w n="88.6">ouvrage</w>, <w n="88.7">et</w> <w n="88.8">tout</w> <w n="88.9">bas</w> <w n="88.10">de</w> <w n="88.11">l</w>’<w n="88.12">auteur</w>.</l>
					<l n="89" num="1.89"><w n="89.1">Enfin</w> <w n="89.2">c</w>’<w n="89.3">est</w> <w n="89.4">mon</w> <w n="89.5">plaisir</w>, <w n="89.6">je</w> <w n="89.7">veux</w> <w n="89.8">me</w> <w n="89.9">satisfaire</w>.</l>
					<l n="90" num="1.90"><w n="90.1">Je</w> <w n="90.2">ne</w> <w n="90.3">puis</w> <w n="90.4">bien</w> <w n="90.5">parler</w>, <w n="90.6">et</w> <w n="90.7">ne</w> <w n="90.8">saurais</w> <w n="90.9">me</w> <w n="90.10">taire</w>,</l>
					<l n="91" num="1.91"><w n="91.1">Et</w>, <w n="91.2">dès</w> <w n="91.3">qu</w>’<w n="91.4">un</w> <w n="91.5">mot</w> <w n="91.6">plaisant</w> <w n="91.7">vient</w> <w n="91.8">luire</w> <w n="91.9">à</w> <w n="91.10">mon</w> <w n="91.11">esprit</w>,</l>
					<l n="92" num="1.92"><w n="92.1">Je</w> <w n="92.2">n</w>’<w n="92.3">ai</w> <w n="92.4">point</w> <w n="92.5">de</w> <w n="92.6">repos</w> <w n="92.7">qu</w>’<w n="92.8">il</w> <w n="92.9">ne</w> <w n="92.10">soit</w> <w n="92.11">en</w> <w n="92.12">écrit</w>.</l>
					<l n="93" num="1.93"><w n="93.1">Je</w> <w n="93.2">ne</w> <w n="93.3">résiste</w> <w n="93.4">point</w> <w n="93.5">au</w> <w n="93.6">torrent</w> <w n="93.7">qui</w> <w n="93.8">m</w>’<w n="93.9">entraîne</w></l>
					<l n="94" num="1.94"><space quantity="2" unit="char"></space><w n="94.1">Mais</w>, <w n="94.2">c</w>’<w n="94.3">est</w> <w n="94.4">assez</w> <w n="94.5">parlé</w>, <w n="94.6">prenons</w> <w n="94.7">un</w> <w n="94.8">peu</w> <w n="94.9">d</w>’<w n="94.10">haleine</w> ;</l>
					<l n="95" num="1.95"><w n="95.1">Ma</w> <w n="95.2">main</w>, <w n="95.3">pour</w> <w n="95.4">cette</w> <w n="95.5">fois</w>, <w n="95.6">commence</w> <w n="95.7">à</w> <w n="95.8">se</w> <w n="95.9">lasser</w> ;</l>
					<l n="96" num="1.96"><w n="96.1">Finissons</w>. <w n="96.2">Mais</w>, <w n="96.3">demain</w>, <w n="96.4">Muse</w>, <w n="96.5">à</w> <w n="96.6">recommencer</w>.</l>
				</lg>
			</div></body></text></TEI>