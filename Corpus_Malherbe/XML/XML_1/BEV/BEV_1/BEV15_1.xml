<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES DÉLIQUESCENCES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEA" sort="1">
					<name>
						<forename>Henri</forename>
						<surname>BEAUCLAIR</surname>
					</name>
					<date from="1860" to="1919">1860-1919</date>
				</author>
				<author key="VIC" sort="2">
					<name>
						<forename>Gabriel</forename>
						<surname>VICAIRE</surname>
					</name>
					<date from="1848" to="1900">1848-1900</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Saisie du texte</resp>
					<name id="SP">
						<forename>S.</forename>
						<surname>Pestel</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Relecture du texte</resp>
					<name id="AG">
						<forename>A.</forename>
						<surname>Guézou</surname>
					</name>
				</respStmt>				
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>297 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BEV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Déliquescences</title>
						<author>Henri Beauclair et Gabriel Vicaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>LA BIBLIOTHÈQUE ÉLECTRONIQUE DE LISIEUX</publisher>
						<pubPlace>Lisieux</pubPlace>
						<address>
							<addrLine>Place de la République</addrLine>
							<addrLine>B.P. 27216 - 14107 Lisieux cedex</addrLine>
							<addrLine>FRANCE</addrLine>
						</address>
						<idno type="URL">http://www.bmlisieux.com/archives/deliqu01.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Déliquescences</title>
								<author>Henri Beauclair et Gabriel Vicaire</author>
								<imprint>
									<publisher>Les Editions Henri Jonquières et Cie, Paris</publisher>
									<date when="1923">1923</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les balises de pagination ont été supprimées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BEV15">
				<head type="main">Décadents</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Nos</w> <w n="1.2">pères</w> <w n="1.3">étaient</w> <w n="1.4">forts</w>, <w n="1.5">et</w> <w n="1.6">leurs</w> <w n="1.7">rêves</w> <w n="1.8">ardents</w></l>
					<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">envolaient</w> <w n="2.3">d</w>’<w n="2.4">un</w> <w n="2.5">coup</w> <w n="2.6">d</w>’<w n="2.7">aile</w> <w n="2.8">au</w> <w n="2.9">pays</w> <w n="2.10">de</w> <w n="2.11">Lumière</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Nous</w> <w n="3.2">dont</w> <w n="3.3">la</w> <w n="3.4">fleur</w> <w n="3.5">dolente</w> <w n="3.6">est</w> <w n="3.7">la</w> <w n="3.8">Rose</w> <w n="3.9">Trémière</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">n</w>’<w n="4.3">avons</w> <w n="4.4">plus</w> <w n="4.5">de</w> <w n="4.6">cœur</w>, <w n="4.7">nous</w> <w n="4.8">n</w>’<w n="4.9">avons</w> <w n="4.10">plus</w> <w n="4.11">de</w> <w n="4.12">dents</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pauvres</w> <w n="5.2">pantins</w> <w n="5.3">avec</w> <w n="5.4">un</w> <w n="5.5">peu</w> <w n="5.6">de</w> <w n="5.7">son</w> <w n="5.8">dedans</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Nous</w> <w n="6.2">regardons</w>, <w n="6.3">sans</w> <w n="6.4">voir</w>, <w n="6.5">la</w> <w n="6.6">ferme</w> <w n="6.7">et</w> <w n="6.8">la</w> <w n="6.9">fermière</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Nous</w> <w n="7.2">renâclons</w> <w n="7.3">devant</w> <w n="7.4">la</w> <w n="7.5">tâche</w> <w n="7.6">coutumière</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Charlots</w> <w n="8.2">trop</w> <w n="8.3">amusés</w>, <w n="8.4">ultimes</w> <w n="8.5">Décadents</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mais</w>, <w n="9.2">ô</w> <w n="9.3">Mort</w> <w n="9.4">du</w> <w n="9.5">Désir</w> ! <w n="9.6">Inappétence</w> <w n="9.7">exquise</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Nous</w> <w n="10.2">gardons</w> <w n="10.3">le</w> <w n="10.4">fumet</w> <w n="10.5">d</w>’<w n="10.6">une</w> <w n="10.7">antique</w> <w n="10.8">Marquise</w></l>
					<l n="11" num="3.3"><w n="11.1">Dont</w> <w n="11.2">un</w> <w n="11.3">Vase</w> <w n="11.4">de</w> <w n="11.5">Nuit</w> <w n="11.6">parfume</w> <w n="11.7">les</w> <w n="11.8">Dessous</w> !</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Être</w> <w n="12.2">Gâteux</w>, <w n="12.3">c</w>’<w n="12.4">est</w> <w n="12.5">toute</w> <w n="12.6">une</w> <w n="12.7">philosophie</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Nos</w> <w n="13.2">nerfs</w> <w n="13.3">et</w> <w n="13.4">notre</w> <w n="13.5">sang</w> <w n="13.6">ne</w> <w n="13.7">valent</w> <w n="13.8">pas</w> <w n="13.9">deux</w> <w n="13.10">sous</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Notre</w> <w n="14.2">cervelle</w>, <w n="14.3">au</w> <w n="14.4">vent</w> <w n="14.5">d</w>’<w n="14.6">Été</w>, <w n="14.7">se</w> <w n="14.8">liquéfie</w> !</l>
				</lg>
			</div></body></text></TEI>