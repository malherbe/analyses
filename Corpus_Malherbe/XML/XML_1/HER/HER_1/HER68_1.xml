<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><div type="poem" key="HER68">
					<head type="main">Sur le Livre des Amours</head>
					<head type="sub">de Pierre de Ronsard</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Jadis</w> <w n="1.2">plus</w> <w n="1.3">d</w>’<w n="1.4">un</w> <w n="1.5">amant</w>, <w n="1.6">aux</w> <w n="1.7">jardins</w> <w n="1.8">de</w> <w n="1.9">Bourgueil</w>,</l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">gravé</w> <w n="2.3">plus</w> <w n="2.4">d</w>’<w n="2.5">un</w> <w n="2.6">nom</w> <w n="2.7">dans</w> <w n="2.8">l</w>’<w n="2.9">écorce</w> <w n="2.10">qu</w>’<w n="2.11">il</w> <w n="2.12">ouvre</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">plus</w> <w n="3.3">d</w>’<w n="3.4">un</w> <w n="3.5">cœur</w>, <w n="3.6">sous</w> <w n="3.7">l</w>’<w n="3.8">or</w> <w n="3.9">des</w> <w n="3.10">hauts</w> <w n="3.11">plafonds</w> <w n="3.12">du</w> <w n="3.13">Louvre</w>,</l>
						<l n="4" num="1.4"><w n="4.1">À</w> <w n="4.2">l</w>’<w n="4.3">éclair</w> <w n="4.4">d</w>’<w n="4.5">un</w> <w n="4.6">sourire</w> <w n="4.7">a</w> <w n="4.8">tressailli</w> <w n="4.9">d</w>’<w n="4.10">orgueil</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Qu</w>’<w n="5.2">importe</w> ? <w n="5.3">Rien</w> <w n="5.4">n</w>’<w n="5.5">a</w> <w n="5.6">dit</w> <w n="5.7">leur</w> <w n="5.8">ivresse</w> <w n="5.9">ou</w> <w n="5.10">leur</w> <w n="5.11">deuil</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Ils</w> <w n="6.2">gisent</w> <w n="6.3">tout</w> <w n="6.4">entiers</w> <w n="6.5">entre</w> <w n="6.6">quatre</w> <w n="6.7">ais</w> <w n="6.8">de</w> <w n="6.9">rouvre</w></l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">nul</w> <w n="7.3">n</w>’<w n="7.4">a</w> <w n="7.5">disputé</w>, <w n="7.6">sous</w> <w n="7.7">l</w>’<w n="7.8">herbe</w> <w n="7.9">qui</w> <w n="7.10">les</w> <w n="7.11">couvre</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Leur</w> <w n="8.2">inerte</w> <w n="8.3">poussière</w> <w n="8.4">à</w> <w n="8.5">l</w>’<w n="8.6">oubli</w> <w n="8.7">du</w> <w n="8.8">cercueil</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Tout</w> <w n="9.2">meurt</w>. <w n="9.3">Marie</w>, <w n="9.4">Hélène</w> <w n="9.5">et</w> <w n="9.6">toi</w>, <w n="9.7">fière</w> <w n="9.8">Cassandre</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Vos</w> <w n="10.2">beaux</w> <w n="10.3">corps</w> <w n="10.4">ne</w> <w n="10.5">seraient</w> <w n="10.6">qu</w>’<w n="10.7">une</w> <w n="10.8">insensible</w> <w n="10.9">cendre</w>,</l>
						<l n="11" num="3.3">— <w n="11.1">Les</w> <w n="11.2">roses</w> <w n="11.3">et</w> <w n="11.4">les</w> <w n="11.5">lys</w> <w n="11.6">n</w>’<w n="11.7">ont</w> <w n="11.8">pas</w> <w n="11.9">de</w> <w n="11.10">lendemain</w> —</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Si</w> <w n="12.2">Ronsard</w>, <w n="12.3">sur</w> <w n="12.4">la</w> <w n="12.5">Seine</w> <w n="12.6">ou</w> <w n="12.7">sur</w> <w n="12.8">la</w> <w n="12.9">blonde</w> <w n="12.10">Loire</w>,</l>
						<l n="13" num="4.2"><w n="13.1">N</w>’<w n="13.2">eût</w> <w n="13.3">tressé</w> <w n="13.4">pour</w> <w n="13.5">vos</w> <w n="13.6">fronts</w>, <w n="13.7">d</w>’<w n="13.8">une</w> <w n="13.9">immortelle</w> <w n="13.10">main</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Aux</w> <w n="14.2">myrtes</w> <w n="14.3">de</w> <w n="14.4">l</w>’<w n="14.5">Amour</w> <w n="14.6">le</w> <w n="14.7">laurier</w> <w n="14.8">de</w> <w n="14.9">la</w> <w n="14.10">Gloire</w>.</l>
					</lg>
				</div></body></text></TEI>