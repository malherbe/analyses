<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><head type="main_subpart">Artémis et les Nymphes</head><div type="poem" key="HER13">
						<head type="main">Pan</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">À</w> <w n="1.2">travers</w> <w n="1.3">les</w> <w n="1.4">halliers</w>, <w n="1.5">par</w> <w n="1.6">les</w> <w n="1.7">chemins</w> <w n="1.8">secrets</w></l>
							<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">se</w> <w n="2.3">perdent</w> <w n="2.4">au</w> <w n="2.5">fond</w> <w n="2.6">des</w> <w n="2.7">vertes</w> <w n="2.8">avenues</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">Chèvre</w>-<w n="3.3">pied</w>, <w n="3.4">divin</w> <w n="3.5">chasseur</w> <w n="3.6">de</w> <w n="3.7">Nymphes</w> <w n="3.8">nues</w></l>
							<l n="4" num="1.4"><w n="4.1">Se</w> <w n="4.2">glisse</w>, <w n="4.3">l</w>’<w n="4.4">œil</w> <w n="4.5">ardent</w>, <w n="4.6">sous</w> <w n="4.7">les</w> <w n="4.8">hautes</w> <w n="4.9">forêts</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">est</w> <w n="5.3">doux</w> <w n="5.4">d</w>’<w n="5.5">écouter</w> <w n="5.6">les</w> <w n="5.7">soupirs</w>, <w n="5.8">les</w> <w n="5.9">bruits</w> <w n="5.10">frais</w></l>
							<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">montent</w> <w n="6.3">à</w> <w n="6.4">midi</w> <w n="6.5">des</w> <w n="6.6">sources</w> <w n="6.7">inconnues</w></l>
							<l n="7" num="2.3"><w n="7.1">Quand</w> <w n="7.2">le</w> <w n="7.3">Soleil</w>, <w n="7.4">vainqueur</w> <w n="7.5">étincelant</w> <w n="7.6">des</w> <w n="7.7">nues</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Dans</w> <w n="8.2">la</w> <w n="8.3">mouvante</w> <w n="8.4">nuit</w> <w n="8.5">darde</w> <w n="8.6">l</w>’<w n="8.7">or</w> <w n="8.8">de</w> <w n="8.9">ses</w> <w n="8.10">traits</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Une</w> <w n="9.2">Nymphe</w> <w n="9.3">s</w>’<w n="9.4">égare</w> <w n="9.5">et</w> <w n="9.6">s</w>’<w n="9.7">arrête</w>. <w n="9.8">Elle</w> <w n="9.9">écoute</w></l>
							<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">larmes</w> <w n="10.3">du</w> <w n="10.4">matin</w> <w n="10.5">qui</w> <w n="10.6">pleuvent</w> <w n="10.7">goutte</w> <w n="10.8">à</w> <w n="10.9">goutte</w></l>
							<l n="11" num="3.3"><w n="11.1">Sur</w> <w n="11.2">la</w> <w n="11.3">mousse</w>. <w n="11.4">L</w>’<w n="11.5">ivresse</w> <w n="11.6">emplit</w> <w n="11.7">son</w> <w n="11.8">jeune</w> <w n="11.9">cœur</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Mais</w>, <w n="12.2">d</w>’<w n="12.3">un</w> <w n="12.4">seul</w> <w n="12.5">bond</w>, <w n="12.6">le</w> <w n="12.7">Dieu</w> <w n="12.8">du</w> <w n="12.9">noir</w> <w n="12.10">taillis</w> <w n="12.11">s</w>’<w n="12.12">élance</w>,</l>
							<l n="13" num="4.2"><w n="13.1">La</w> <w n="13.2">saisit</w>, <w n="13.3">frappe</w> <w n="13.4">l</w>’<w n="13.5">air</w> <w n="13.6">de</w> <w n="13.7">son</w> <w n="13.8">rire</w> <w n="13.9">moqueur</w>,</l>
							<l n="14" num="4.3"><w n="14.1">Disparaît</w>… <w n="14.2">Et</w> <w n="14.3">les</w> <w n="14.4">bois</w> <w n="14.5">retombent</w> <w n="14.6">au</w> <w n="14.7">silence</w>.</l>
						</lg>
					</div></body></text></TEI>