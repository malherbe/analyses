<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><head type="main_subpart">Persée et Andromède</head><div type="poem" key="HER22">
						<head type="main">Andromède au Monstre</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">Vierge</w> <w n="1.3">Céphéenne</w>, <w n="1.4">hélas</w> ! <w n="1.5">encor</w> <w n="1.6">vivante</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Liée</w>, <w n="2.2">échevelée</w>, <w n="2.3">au</w> <w n="2.4">roc</w> <w n="2.5">des</w> <w n="2.6">noirs</w> <w n="2.7">îlots</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">lamente</w> <w n="3.3">en</w> <w n="3.4">tordant</w> <w n="3.5">avec</w> <w n="3.6">de</w> <w n="3.7">vains</w> <w n="3.8">sanglots</w></l>
							<l n="4" num="1.4"><w n="4.1">Sa</w> <w n="4.2">chair</w> <w n="4.3">royale</w> <w n="4.4">où</w> <w n="4.5">court</w> <w n="4.6">un</w> <w n="4.7">frisson</w> <w n="4.8">d</w>’<w n="4.9">épouvante</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">océan</w> <w n="5.3">monstrueux</w> <w n="5.4">que</w> <w n="5.5">la</w> <w n="5.6">tempête</w> <w n="5.7">évente</w></l>
							<l n="6" num="2.2"><w n="6.1">Crache</w> <w n="6.2">à</w> <w n="6.3">ses</w> <w n="6.4">pieds</w> <w n="6.5">glacés</w> <w n="6.6">l</w>’<w n="6.7">âcre</w> <w n="6.8">bave</w> <w n="6.9">des</w> <w n="6.10">flots</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">partout</w> <w n="7.3">elle</w> <w n="7.4">voit</w>, <w n="7.5">à</w> <w n="7.6">travers</w> <w n="7.7">ses</w> <w n="7.8">cils</w> <w n="7.9">clos</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Bâiller</w> <w n="8.2">la</w> <w n="8.3">gueule</w> <w n="8.4">glauque</w>, <w n="8.5">innombrable</w> <w n="8.6">et</w> <w n="8.7">mouvante</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Tel</w> <w n="9.2">qu</w>’<w n="9.3">un</w> <w n="9.4">éclat</w> <w n="9.5">de</w> <w n="9.6">foudre</w> <w n="9.7">en</w> <w n="9.8">un</w> <w n="9.9">ciel</w> <w n="9.10">sans</w> <w n="9.11">éclair</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Tout</w> <w n="10.2">à</w> <w n="10.3">coup</w>, <w n="10.4">retentit</w> <w n="10.5">un</w> <w n="10.6">hennissement</w> <w n="10.7">clair</w></l>
							<l n="11" num="3.3"><w n="11.1">Ses</w> <w n="11.2">yeux</w> <w n="11.3">s</w>’<w n="11.4">ouvrent</w>. <w n="11.5">L</w>’<w n="11.6">horreur</w> <w n="11.7">les</w> <w n="11.8">emplit</w>, <w n="11.9">et</w> <w n="11.10">l</w>’<w n="11.11">extase</w> ;</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Car</w> <w n="12.2">elle</w> <w n="12.3">a</w> <w n="12.4">vu</w>, <w n="12.5">d</w>’<w n="12.6">un</w> <w n="12.7">vol</w> <w n="12.8">vertigineux</w> <w n="12.9">et</w> <w n="12.10">sûr</w>,</l>
							<l n="13" num="4.2"><w n="13.1">Se</w> <w n="13.2">cabrant</w> <w n="13.3">sous</w> <w n="13.4">le</w> <w n="13.5">poids</w> <w n="13.6">du</w> <w n="13.7">fils</w> <w n="13.8">de</w> <w n="13.9">Zeus</w>, <w n="13.10">Pégase</w></l>
							<l n="14" num="4.3"><w n="14.1">Allonger</w> <w n="14.2">sur</w> <w n="14.3">la</w> <w n="14.4">mer</w> <w n="14.5">sa</w> <w n="14.6">grande</w> <w n="14.7">ombre</w> <w n="14.8">d</w>’<w n="14.9">azur</w>.</l>
						</lg>
					</div></body></text></TEI>