<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Rome et les Barbares</head><div type="poem" key="HER53">
				<head type="main">À un Triomphateur</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Fais</w> <w n="1.2">sculpter</w> <w n="1.3">sur</w> <w n="1.4">ton</w> <w n="1.5">arc</w>, <w n="1.6">Imperator</w> <w n="1.7">illustre</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Des</w> <w n="2.2">files</w> <w n="2.3">de</w> <w n="2.4">guerriers</w> <w n="2.5">barbares</w>, <w n="2.6">de</w> <w n="2.7">vieux</w> <w n="2.8">chefs</w></l>
						<l n="3" num="1.3"><w n="3.1">Sous</w> <w n="3.2">le</w> <w n="3.3">joug</w>, <w n="3.4">des</w> <w n="3.5">tronçons</w> <w n="3.6">d</w>’<w n="3.7">armures</w> <w n="3.8">et</w> <w n="3.9">de</w> <w n="3.10">nefs</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">la</w> <w n="4.3">flotte</w> <w n="4.4">captive</w> <w n="4.5">et</w> <w n="4.6">le</w> <w n="4.7">rostre</w> <w n="4.8">et</w> <w n="4.9">l</w>’<w n="4.10">aplustre</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Quel</w> <w n="5.2">que</w> <w n="5.3">tu</w> <w n="5.4">sois</w>, <w n="5.5">issu</w> <w n="5.6">d</w>’<w n="5.7">Ancus</w> <w n="5.8">ou</w> <w n="5.9">né</w> <w n="5.10">d</w>’<w n="5.11">un</w> <w n="5.12">rustre</w></l>
						<l n="6" num="2.2"><w n="6.1">Tes</w> <w n="6.2">noms</w>, <w n="6.3">famille</w>, <w n="6.4">honneurs</w> <w n="6.5">et</w> <w n="6.6">titres</w>, <w n="6.7">longs</w> <w n="6.8">ou</w> <w n="6.9">brefs</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Grave</w>-<w n="7.2">les</w> <w n="7.3">dans</w> <w n="7.4">la</w> <w n="7.5">frise</w> <w n="7.6">et</w> <w n="7.7">dans</w> <w n="7.8">les</w> <w n="7.9">bas</w>-<w n="7.10">reliefs</w></l>
						<l n="8" num="2.4"><w n="8.1">Profondément</w>, <w n="8.2">de</w> <w n="8.3">peur</w> <w n="8.4">que</w> <w n="8.5">l</w>’<w n="8.6">avenir</w> <w n="8.7">te</w> <w n="8.8">frustre</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Déjà</w> <w n="9.2">le</w> <w n="9.3">Temps</w> <w n="9.4">brandit</w> <w n="9.5">l</w>’<w n="9.6">arme</w> <w n="9.7">fatale</w>. <w n="9.8">As</w>-<w n="9.9">tu</w></l>
						<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2">espoir</w> <w n="10.3">d</w>’<w n="10.4">éterniser</w> <w n="10.5">le</w> <w n="10.6">bruit</w> <w n="10.7">de</w> <w n="10.8">ta</w> <w n="10.9">vertu</w> ?</l>
						<l n="11" num="3.3"><w n="11.1">Un</w> <w n="11.2">vil</w> <w n="11.3">lierre</w> <w n="11.4">suffit</w> <w n="11.5">à</w> <w n="11.6">disjoindre</w> <w n="11.7">un</w> <w n="11.8">trophée</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">seul</w>, <w n="12.3">aux</w> <w n="12.4">blocs</w> <w n="12.5">épars</w> <w n="12.6">des</w> <w n="12.7">marbres</w> <w n="12.8">triomphaux</w></l>
						<l n="13" num="4.2"><w n="13.1">Où</w> <w n="13.2">ta</w> <w n="13.3">gloire</w> <w n="13.4">en</w> <w n="13.5">ruine</w> <w n="13.6">est</w> <w n="13.7">par</w> <w n="13.8">l</w>’<w n="13.9">herbe</w> <w n="13.10">étouffée</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Quelque</w> <w n="14.2">faucheur</w> <w n="14.3">Samnite</w> <w n="14.4">ébréchera</w> <w n="14.5">sa</w> <w n="14.6">faulx</w>.</l>
					</lg>
				</div></body></text></TEI>