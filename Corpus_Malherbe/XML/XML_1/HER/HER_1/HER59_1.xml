<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Rome et les Barbares</head><head type="main_subpart">Sonnets épigraphique</head><div type="poem" key="HER59">
						<head type="main">Le Dieu Hêtre</head>
						<opener>
							<epigraph>
								<cit>
									<quote>FAGO DEO</quote>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">Garumne</w> <w n="1.3">a</w> <w n="1.4">bâti</w> <w n="1.5">sa</w> <w n="1.6">rustique</w> <w n="1.7">maison</w></l>
							<l n="2" num="1.2"><w n="2.1">Sous</w> <w n="2.2">un</w> <w n="2.3">grand</w> <w n="2.4">hêtre</w> <w n="2.5">au</w> <w n="2.6">tronc</w> <w n="2.7">musculeux</w> <w n="2.8">comme</w> <w n="2.9">un</w> <w n="2.10">torse</w></l>
							<l n="3" num="1.3"><w n="3.1">Dont</w> <w n="3.2">la</w> <w n="3.3">sève</w> <w n="3.4">d</w>’<w n="3.5">un</w> <w n="3.6">Dieu</w> <w n="3.7">gonfle</w> <w n="3.8">la</w> <w n="3.9">blanche</w> <w n="3.10">écorce</w>.</l>
							<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">forêt</w> <w n="4.3">maternelle</w> <w n="4.4">est</w> <w n="4.5">tout</w> <w n="4.6">son</w> <w n="4.7">horizon</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Car</w> <w n="5.2">l</w>’<w n="5.3">homme</w> <w n="5.4">libre</w> <w n="5.5">y</w> <w n="5.6">trouve</w>, <w n="5.7">au</w> <w n="5.8">gré</w> <w n="5.9">de</w> <w n="5.10">la</w> <w n="5.11">saison</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">faînes</w>, <w n="6.3">le</w> <w n="6.4">bois</w>, <w n="6.5">l</w>’<w n="6.6">ombre</w> <w n="6.7">et</w> <w n="6.8">les</w> <w n="6.9">bêtes</w> <w n="6.10">qu</w>’<w n="6.11">il</w> <w n="6.12">force</w></l>
							<l n="7" num="2.3"><w n="7.1">Avec</w> <w n="7.2">l</w>’<w n="7.3">arc</w> <w n="7.4">ou</w> <w n="7.5">l</w>’<w n="7.6">épieu</w>, <w n="7.7">le</w> <w n="7.8">filet</w> <w n="7.9">ou</w> <w n="7.10">l</w>’<w n="7.11">amorce</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Pour</w> <w n="8.2">en</w> <w n="8.3">manger</w> <w n="8.4">la</w> <w n="8.5">chair</w> <w n="8.6">et</w> <w n="8.7">vêtir</w> <w n="8.8">leur</w> <w n="8.9">toison</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Longtemps</w> <w n="9.2">il</w> <w n="9.3">a</w> <w n="9.4">vécu</w> <w n="9.5">riche</w>, <w n="9.6">heureux</w> <w n="9.7">et</w> <w n="9.8">sans</w> <w n="9.9">maître</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">le</w> <w n="10.3">soir</w>, <w n="10.4">lorsqu</w>’<w n="10.5">il</w> <w n="10.6">rentre</w> <w n="10.7">au</w> <w n="10.8">logis</w>, <w n="10.9">le</w> <w n="10.10">vieux</w> <w n="10.11">Hêtre</w></l>
							<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">ses</w> <w n="11.3">bras</w> <w n="11.4">familiers</w> <w n="11.5">semble</w> <w n="11.6">lui</w> <w n="11.7">faire</w> <w n="11.8">accueil</w> ;</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">quand</w> <w n="12.3">la</w> <w n="12.4">Mort</w> <w n="12.5">viendra</w> <w n="12.6">courber</w> <w n="12.7">sa</w> <w n="12.8">tête</w> <w n="12.9">franche</w>,</l>
							<l n="13" num="4.2"><w n="13.1">Ses</w> <w n="13.2">petits</w>-<w n="13.3">fils</w> <w n="13.4">auront</w> <w n="13.5">pour</w> <w n="13.6">tailler</w> <w n="13.7">son</w> <w n="13.8">cercueil</w></l>
							<l n="14" num="4.3"><w n="14.1">L</w>’<w n="14.2">incorruptible</w> <w n="14.3">cœur</w> <w n="14.4">de</w> <w n="14.5">la</w> <w n="14.6">maîtresse</w> <w n="14.7">branche</w>.</l>
						</lg>
					</div></body></text></TEI>