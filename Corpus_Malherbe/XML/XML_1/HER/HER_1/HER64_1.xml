<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><div type="poem" key="HER64">
					<head type="main">Le Huchier de Nazareth</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">bon</w> <w n="1.3">maître</w> <w n="1.4">huchier</w>, <w n="1.5">pour</w> <w n="1.6">finir</w> <w n="1.7">un</w> <w n="1.8">dressoir</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Courbé</w> <w n="2.2">sur</w> <w n="2.3">l</w>’<w n="2.4">établi</w> <w n="2.5">depuis</w> <w n="2.6">l</w>’<w n="2.7">aurore</w> <w n="2.8">ahane</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Maniant</w> <w n="3.2">tour</w> <w n="3.3">à</w> <w n="3.4">tour</w> <w n="3.5">le</w> <w n="3.6">rabot</w>, <w n="3.7">le</w> <w n="3.8">bédane</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">la</w> <w n="4.3">râpe</w> <w n="4.4">grinçante</w> <w n="4.5">ou</w> <w n="4.6">le</w> <w n="4.7">dur</w> <w n="4.8">polissoir</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Aussi</w>, <w n="5.2">non</w> <w n="5.3">sans</w> <w n="5.4">plaisir</w>, <w n="5.5">a</w>-<w n="5.6">t</w>-<w n="5.7">il</w> <w n="5.8">vu</w>, <w n="5.9">vers</w> <w n="5.10">le</w> <w n="5.11">soir</w>,</l>
						<l n="6" num="2.2"><w n="6.1">S</w>’<w n="6.2">allonger</w> <w n="6.3">jusqu</w>’<w n="6.4">au</w> <w n="6.5">seuil</w> <w n="6.6">l</w>’<w n="6.7">ombre</w> <w n="6.8">du</w> <w n="6.9">grand</w> <w n="6.10">platane</w></l>
						<l n="7" num="2.3"><w n="7.1">Où</w> <w n="7.2">madame</w> <w n="7.3">la</w> <w n="7.4">Vierge</w> <w n="7.5">et</w> <w n="7.6">sa</w> <w n="7.7">mère</w> <w n="7.8">sainte</w> <w n="7.9">Anne</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">Monseigneur</w> <w n="8.3">Jésus</w> <w n="8.4">près</w> <w n="8.5">de</w> <w n="8.6">lui</w> <w n="8.7">vont</w> <w n="8.8">s</w>’<w n="8.9">asseoir</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2">air</w> <w n="9.3">est</w> <w n="9.4">brûlant</w> <w n="9.5">et</w> <w n="9.6">pas</w> <w n="9.7">une</w> <w n="9.8">feuille</w> <w n="9.9">ne</w> <w n="9.10">bouge</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">saint</w> <w n="10.3">Joseph</w>, <w n="10.4">très</w> <w n="10.5">las</w>, <w n="10.6">a</w> <w n="10.7">laissé</w> <w n="10.8">choir</w> <w n="10.9">la</w> <w n="10.10">gouge</w></l>
						<l n="11" num="3.3"><w n="11.1">En</w> <w n="11.2">s</w>’<w n="11.3">essuyant</w> <w n="11.4">le</w> <w n="11.5">front</w> <w n="11.6">au</w> <w n="11.7">coin</w> <w n="11.8">du</w> <w n="11.9">tablier</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Mais</w> <w n="12.2">l</w>’<w n="12.3">Apprenti</w> <w n="12.4">divin</w> <w n="12.5">qu</w>’<w n="12.6">une</w> <w n="12.7">gloire</w> <w n="12.8">enveloppe</w></l>
						<l n="13" num="4.2"><w n="13.1">Fait</w> <w n="13.2">toujours</w>, <w n="13.3">dans</w> <w n="13.4">le</w> <w n="13.5">fond</w> <w n="13.6">obscur</w> <w n="13.7">de</w> <w n="13.8">l</w>’<w n="13.9">atelier</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Voler</w> <w n="14.2">les</w> <w n="14.3">copeaux</w> <w n="14.4">d</w>’<w n="14.5">or</w> <w n="14.6">au</w> <w n="14.7">fil</w> <w n="14.8">de</w> <w n="14.9">sa</w> <w n="14.10">varlope</w>.</l>
					</lg>
				</div></body></text></TEI>