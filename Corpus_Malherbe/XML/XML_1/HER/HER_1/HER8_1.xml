<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><div type="poem" key="HER8">
					<head type="main">La Naissance d’Aphrodité</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Avant</w> <w n="1.2">tout</w>, <w n="1.3">le</w> <w n="1.4">Chaos</w> <w n="1.5">enveloppait</w> <w n="1.6">les</w> <w n="1.7">mondes</w></l>
						<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">roulaient</w> <w n="2.3">sans</w> <w n="2.4">mesure</w> <w n="2.5">et</w> <w n="2.6">l</w>’<w n="2.7">Espace</w> <w n="2.8">et</w> <w n="2.9">le</w> <w n="2.10">Temps</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Puis</w> <w n="3.2">Gaia</w>, <w n="3.3">favorable</w> <w n="3.4">à</w> <w n="3.5">ses</w> <w n="3.6">fils</w> <w n="3.7">les</w> <w n="3.8">Titans</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Leur</w> <w n="4.2">prêta</w> <w n="4.3">son</w> <w n="4.4">grand</w> <w n="4.5">sein</w> <w n="4.6">aux</w> <w n="4.7">mamelles</w> <w n="4.8">fécondes</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ils</w> <w n="5.2">tombèrent</w>. <w n="5.3">Le</w> <w n="5.4">Styx</w> <w n="5.5">les</w> <w n="5.6">couvrit</w> <w n="5.7">de</w> <w n="5.8">ses</w> <w n="5.9">ondes</w>.</l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">jamais</w>, <w n="6.3">sous</w> <w n="6.4">l</w>’<w n="6.5">éther</w> <w n="6.6">foudroyé</w>, <w n="6.7">le</w> <w n="6.8">Printemps</w></l>
						<l n="7" num="2.3"><w n="7.1">N</w>’<w n="7.2">avait</w> <w n="7.3">fait</w> <w n="7.4">resplendir</w> <w n="7.5">les</w> <w n="7.6">soleils</w> <w n="7.7">éclatants</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Ni</w> <w n="8.2">l</w>’<w n="8.3">Été</w> <w n="8.4">généreux</w> <w n="8.5">mûri</w> <w n="8.6">les</w> <w n="8.7">moissons</w> <w n="8.8">blondes</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Farouches</w>, <w n="9.2">ignorants</w> <w n="9.3">des</w> <w n="9.4">rires</w> <w n="9.5">et</w> <w n="9.6">des</w> <w n="9.7">jeux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">Immortels</w> <w n="10.3">siégeaient</w> <w n="10.4">sur</w> <w n="10.5">l</w>’<w n="10.6">Olympe</w> <w n="10.7">neigeux</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Mais</w> <w n="11.2">le</w> <w n="11.3">ciel</w> <w n="11.4">fit</w> <w n="11.5">pleuvoir</w> <w n="11.6">la</w> <w n="11.7">virile</w> <w n="11.8">rosée</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">L</w>’<w n="12.2">Océan</w> <w n="12.3">s</w>’<w n="12.4">entr</w>’<w n="12.5">ouvrit</w>, <w n="12.6">et</w> <w n="12.7">dans</w> <w n="12.8">sa</w> <w n="12.9">nudité</w></l>
						<l n="13" num="4.2"><w n="13.1">Radieuse</w>, <w n="13.2">émergeant</w> <w n="13.3">de</w> <w n="13.4">l</w>’<w n="13.5">écume</w> <w n="13.6">embrasée</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Dans</w> <w n="14.2">le</w> <w n="14.3">sang</w> <w n="14.4">d</w>’<w n="14.5">Ouranos</w> <w n="14.6">fleurit</w> <w n="14.7">Aphrodité</w>.</l>
					</lg>
				</div></body></text></TEI>