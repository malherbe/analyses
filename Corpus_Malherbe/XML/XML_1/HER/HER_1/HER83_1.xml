<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Le Moyen âge et la Renaissance</head><head type="main_subpart">Les conquérants</head><div type="poem" key="HER83">
						<head type="main">L’Ancêtre</head>
						<opener>
							<salute>À Claudius Popelin.</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">gloire</w> <w n="1.3">a</w> <w n="1.4">sillonné</w> <w n="1.5">de</w> <w n="1.6">ses</w> <w n="1.7">illustres</w> <w n="1.8">rides</w></l>
							<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">visage</w> <w n="2.3">hardi</w> <w n="2.4">de</w> <w n="2.5">ce</w> <w n="2.6">grand</w> <w n="2.7">Cavalier</w></l>
							<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">porte</w> <w n="3.3">sur</w> <w n="3.4">son</w> <w n="3.5">front</w> <w n="3.6">que</w> <w n="3.7">nul</w> <w n="3.8">n</w>’<w n="3.9">a</w> <w n="3.10">fait</w> <w n="3.11">plier</w></l>
							<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">hâle</w> <w n="4.3">de</w> <w n="4.4">la</w> <w n="4.5">guerre</w> <w n="4.6">et</w> <w n="4.7">des</w> <w n="4.8">soleils</w> <w n="4.9">torrides</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">En</w> <w n="5.2">tous</w> <w n="5.3">lieux</w>, <w n="5.4">Côte</w>-<w n="5.5">Ferme</w>, <w n="5.6">îles</w>, <w n="5.7">sierras</w> <w n="5.8">arides</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Il</w> <w n="6.2">a</w> <w n="6.3">planté</w> <w n="6.4">la</w> <w n="6.5">croix</w>, <w n="6.6">et</w>, <w n="6.7">depuis</w> <w n="6.8">l</w>’<w n="6.9">escalier</w></l>
							<l n="7" num="2.3"><w n="7.1">Des</w> <w n="7.2">Andes</w>, <w n="7.3">promené</w> <w n="7.4">son</w> <w n="7.5">pennon</w> <w n="7.6">familier</w></l>
							<l n="8" num="2.4"><w n="8.1">Jusqu</w>’<w n="8.2">au</w> <w n="8.3">golfe</w> <w n="8.4">orageux</w> <w n="8.5">qui</w> <w n="8.6">blanchit</w> <w n="8.7">les</w> <w n="8.8">Florides</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Pour</w> <w n="9.2">ses</w> <w n="9.3">derniers</w> <w n="9.4">neveux</w>, <w n="9.5">Claudius</w>, <w n="9.6">tes</w> <w n="9.7">pinceaux</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Sous</w> <w n="10.2">l</w>’<w n="10.3">armure</w> <w n="10.4">de</w> <w n="10.5">bronze</w> <w n="10.6">aux</w> <w n="10.7">splendides</w> <w n="10.8">rinceaux</w></l>
							<l n="11" num="3.3"><w n="11.1">Font</w> <w n="11.2">revivre</w> <w n="11.3">l</w>’<w n="11.4">aïeul</w> <w n="11.5">fier</w> <w n="11.6">et</w> <w n="11.7">mélancolique</w> ;</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">ses</w> <w n="12.3">yeux</w> <w n="12.4">assombris</w> <w n="12.5">semblent</w> <w n="12.6">chercher</w> <w n="12.7">encor</w></l>
							<l n="13" num="4.2"><w n="13.1">Dans</w> <w n="13.2">le</w> <w n="13.3">ciel</w> <w n="13.4">de</w> <w n="13.5">l</w>’<w n="13.6">émail</w> <w n="13.7">ardent</w> <w n="13.8">et</w> <w n="13.9">métallique</w></l>
							<l n="14" num="4.3"><w n="14.1">Les</w> <w n="14.2">éblouissements</w> <w n="14.3">de</w> <w n="14.4">la</w> <w n="14.5">Castille</w> <w n="14.6">d</w>’<w n="14.7">or</w>.</l>
						</lg>
					</div></body></text></TEI>