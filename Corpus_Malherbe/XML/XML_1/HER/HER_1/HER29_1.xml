<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Trophées</title>
				<title type="medium">Une édition électronique</title>
				<author key="HER">
					<name>
						<forename>José-Maria</forename>
						<nameLink>de</nameLink>
						<surname>HEREDIA</surname>
					</name>
					<date from="1842" to="1905">1842-1905</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2519 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">HER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Trophées</title>
						<author>José-Maria de Heredia</author>
					</titleStmt>
					<publicationStmt>
						<publisher>lyres.chez.com</publisher>
						<idno type="URL">http://lyres.chez.com/telecharg/telecharger.htm</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Trophées</title>
						<author>José-Maria de Heredia</author>
						<edition>Édition d’Anny Detalle</edition>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Poésie/Gallimard</publisher>
							<date when="1981">1981</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1893">1893</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Cette édition électronique ne comprend pas la préface de l’auteur.</p>
				<p>L’édition qui est à l’origine de la version électronique n’est pas indiquée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La Grèce et la Sicile</head><head type="main_subpart">Épigrammes bucoliques</head><div type="poem" key="HER29">
						<head type="main">Le Naufragé</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Avec</w> <w n="1.2">la</w> <w n="1.3">brise</w> <w n="1.4">en</w> <w n="1.5">poupe</w> <w n="1.6">et</w> <w n="1.7">par</w> <w n="1.8">un</w> <w n="1.9">ciel</w> <w n="1.10">serein</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Voyant</w> <w n="2.2">le</w> <w n="2.3">Phare</w> <w n="2.4">fuir</w> <w n="2.5">à</w> <w n="2.6">travers</w> <w n="2.7">la</w> <w n="2.8">mâture</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">est</w> <w n="3.3">parti</w> <w n="3.4">d</w>’<w n="3.5">Égypte</w> <w n="3.6">au</w> <w n="3.7">lever</w> <w n="3.8">de</w> <w n="3.9">l</w>’<w n="3.10">Arcture</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Fier</w> <w n="4.2">de</w> <w n="4.3">sa</w> <w n="4.4">nef</w> <w n="4.5">rapide</w> <w n="4.6">aux</w> <w n="4.7">flancs</w> <w n="4.8">doublés</w> <w n="4.9">d</w>’<w n="4.10">airain</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">ne</w> <w n="5.3">reverra</w> <w n="5.4">plus</w> <w n="5.5">le</w> <w n="5.6">môle</w> <w n="5.7">Alexandrin</w>.</l>
							<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">le</w> <w n="6.3">sable</w> <w n="6.4">où</w> <w n="6.5">pas</w> <w n="6.6">même</w> <w n="6.7">un</w> <w n="6.8">chevreau</w> <w n="6.9">ne</w> <w n="6.10">pâture</w></l>
							<l n="7" num="2.3"><w n="7.1">La</w> <w n="7.2">tempête</w> <w n="7.3">a</w> <w n="7.4">creusé</w> <w n="7.5">sa</w> <w n="7.6">triste</w> <w n="7.7">sépulture</w> ;</l>
							<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">vent</w> <w n="8.3">du</w> <w n="8.4">large</w> <w n="8.5">y</w> <w n="8.6">tord</w> <w n="8.7">quelque</w> <w n="8.8">arbuste</w> <w n="8.9">marin</w></l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Au</w> <w n="9.2">pli</w> <w n="9.3">le</w> <w n="9.4">plus</w> <w n="9.5">profond</w> <w n="9.6">de</w> <w n="9.7">la</w> <w n="9.8">mouvante</w> <w n="9.9">dune</w>,</l>
							<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">la</w> <w n="10.3">nuit</w> <w n="10.4">sans</w> <w n="10.5">aurore</w> <w n="10.6">et</w> <w n="10.7">sans</w> <w n="10.8">astre</w> <w n="10.9">et</w> <w n="10.10">sans</w> <w n="10.11">lune</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">le</w> <w n="11.3">navigateur</w> <w n="11.4">trouve</w> <w n="11.5">enfin</w> <w n="11.6">le</w> <w n="11.7">repos</w> !</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Ô</w> <w n="12.2">Terre</w>, <w n="12.3">ô</w> <w n="12.4">Mer</w>, <w n="12.5">pitié</w> <w n="12.6">pour</w> <w n="12.7">son</w> <w n="12.8">Ombre</w> <w n="12.9">anxieuse</w> !</l>
							<l n="13" num="4.2"><w n="13.1">Et</w> <w n="13.2">sur</w> <w n="13.3">la</w> <w n="13.4">rive</w> <w n="13.5">hellène</w> <w n="13.6">où</w> <w n="13.7">sont</w> <w n="13.8">venus</w> <w n="13.9">ses</w> <w n="13.10">os</w>,</l>
							<l n="14" num="4.3"><w n="14.1">Soyez</w>-<w n="14.2">lui</w>, <w n="14.3">toi</w>, <w n="14.4">légère</w>, <w n="14.5">et</w> <w n="14.6">toi</w>, <w n="14.7">silencieuse</w>.</l>
						</lg>
					</div></body></text></TEI>