<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SÉRÉNADE DES SÉRÉNADES</head><div type="poem" key="CRB39">
					<head type="main">TOIT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tiens</w> <w n="1.2">non</w> ! <w n="1.3">J</w>’<w n="1.4">attendrai</w> <w n="1.5">tranquille</w>,</l>
						<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">Planté</w> <w n="2.2">sous</w> <w n="2.3">le</w> <w n="2.4">toit</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">il</w> <w n="3.3">me</w> <w n="3.4">tombe</w> <w n="3.5">quelque</w> <w n="3.6">tuile</w>,</l>
						<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">Souvenir</w> <w n="4.2">de</w> <w n="4.3">Toi</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2">ai</w> <w n="5.3">tondu</w> <w n="5.4">l</w>’<w n="5.5">herbe</w>, <w n="5.6">je</w> <w n="5.7">lèche</w></l>
						<l n="6" num="2.2"><space quantity="4" unit="char"></space><w n="6.1">La</w> <w n="6.2">pierre</w>, — <w n="6.3">altéré</w></l>
						<l n="7" num="2.3"><w n="7.1">Comme</w> <hi rend="ital"><w n="7.2">la</w> <w n="7.3">Colique</w>-<w n="7.4">sèche</w></hi></l>
						<l n="8" num="2.4"><space quantity="4" unit="char"></space><hi rend="ital"><w n="8.1">De</w> <w n="8.2">Miserere</w></hi> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">crèverai</w> — <w n="9.3">Dieu</w> <w n="9.4">me</w> <w n="9.5">damne</w> ! —</l>
						<l n="10" num="3.2"><w n="10.1">Ton</w> <w n="10.2">tympan</w> <w n="10.3">ou</w> <w n="10.4">la</w> <w n="10.5">peau</w> <w n="10.6">d</w>’<w n="10.7">âne</w></l>
						<l n="11" num="3.3"><space quantity="4" unit="char"></space><w n="11.1">De</w> <w n="11.2">mon</w> <w n="11.3">bon</w> <w n="11.4">tambour</w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Dans</w> <w n="12.2">ton</w> <w n="12.3">boîtier</w>, <w n="12.4">ô</w> <w n="12.5">Fenêtre</w> !</l>
						<l n="13" num="4.2"><w n="13.1">Calme</w> <w n="13.2">et</w> <w n="13.3">pure</w>, <w n="13.4">gît</w> <w n="13.5">peut</w>-<w n="13.6">être</w>…</l>
						<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="14" num="4.3"><space quantity="4" unit="char"></space><w n="14.1">Un</w> <w n="14.2">vieux</w> <w n="14.3">monsieur</w> <w n="14.4">sourd</w> !</l>
					</lg>
				</div></body></text></TEI>