<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB26">
					<head type="main">INSOMNIE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Insomnie</w>, <w n="1.2">impalpable</w> <w n="1.3">Bête</w> !</l>
						<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2">as</w>-<w n="2.3">tu</w> <w n="2.4">d</w>’<w n="2.5">amour</w> <w n="2.6">que</w> <w n="2.7">dans</w> <w n="2.8">la</w> <w n="2.9">tête</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">venir</w> <w n="3.3">te</w> <w n="3.4">pâmer</w> <w n="3.5">à</w> <w n="3.6">voir</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Sous</w> <w n="4.2">ton</w> <w n="4.3">mauvais</w> <w n="4.4">œil</w>, <w n="4.5">l</w>’<w n="4.6">homme</w> <w n="4.7">mordre</w></l>
						<l n="5" num="1.5"><w n="5.1">Ses</w> <w n="5.2">draps</w>, <w n="5.3">et</w> <w n="5.4">dans</w> <w n="5.5">l</w>’<w n="5.6">ennui</w> <w n="5.7">se</w> <w n="5.8">tordre</w> !…</l>
						<l n="6" num="1.6"><w n="6.1">Sous</w> <w n="6.2">ton</w> <w n="6.3">œil</w> <w n="6.4">de</w> <w n="6.5">diamant</w> <w n="6.6">noir</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Dis</w> : <w n="7.2">pourquoi</w>, <w n="7.3">durant</w> <w n="7.4">la</w> <w n="7.5">nuit</w> <w n="7.6">blanche</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Pluvieuse</w> <w n="8.2">comme</w> <w n="8.3">un</w> <w n="8.4">dimanche</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Venir</w> <w n="9.2">nous</w> <w n="9.3">lécher</w> <w n="9.4">comme</w> <w n="9.5">un</w> <w n="9.6">chien</w> :</l>
						<l n="10" num="2.4"><w n="10.1">Espérance</w> <w n="10.2">ou</w> <w n="10.3">Regret</w> <w n="10.4">qui</w> <w n="10.5">veille</w>,</l>
						<l n="11" num="2.5"><w n="11.1">A</w> <w n="11.2">notre</w> <w n="11.3">palpitante</w> <w n="11.4">oreille</w></l>
						<l n="12" num="2.6"><w n="12.1">Parler</w> <w n="12.2">bas</w> … <w n="12.3">et</w> <w n="12.4">ne</w> <w n="12.5">dire</w> <w n="12.6">rien</w> ?</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Pourquoi</w>, <w n="13.2">sur</w> <w n="13.3">notre</w> <w n="13.4">gorge</w> <w n="13.5">aride</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Toujours</w> <w n="14.2">pencher</w> <w n="14.3">ta</w> <w n="14.4">coupe</w> <w n="14.5">vide</w></l>
						<l n="15" num="3.3"><w n="15.1">Et</w> <w n="15.2">nous</w> <w n="15.3">laisser</w> <w n="15.4">le</w> <w n="15.5">cou</w> <w n="15.6">tendu</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Tantales</w>, <w n="16.2">soiffeurs</w> <w n="16.3">de</w> <w n="16.4">chimère</w> :</l>
						<l n="17" num="3.5">— <w n="17.1">Philtre</w> <w n="17.2">amoureux</w> <w n="17.3">ou</w> <w n="17.4">lie</w> <w n="17.5">amère</w></l>
						<l n="18" num="3.6"><w n="18.1">Fraîche</w> <w n="18.2">rosée</w> <w n="18.3">ou</w> <w n="18.4">plomb</w> <w n="18.5">fondu</w> ! —</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Insomnie</w>, <w n="19.2">es</w>-<w n="19.3">tu</w> <w n="19.4">donc</w> <w n="19.5">pas</w> <w n="19.6">belle</w> ?…</l>
						<l n="20" num="4.2"><w n="20.1">Eh</w> <w n="20.2">pourquoi</w>, <w n="20.3">lubrique</w> <w n="20.4">pucelle</w>,</l>
						<l n="21" num="4.3"><w n="21.1">Nous</w> <w n="21.2">étreindre</w> <w n="21.3">entre</w> <w n="21.4">tes</w> <w n="21.5">genoux</w> ?</l>
						<l n="22" num="4.4"><w n="22.1">Pourquoi</w> <w n="22.2">râler</w> <w n="22.3">sur</w> <w n="22.4">notre</w> <w n="22.5">bouche</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Pourquoi</w> <w n="23.2">défaire</w> <w n="23.3">notre</w> <w n="23.4">couche</w>,</l>
						<l n="24" num="4.6"><w n="24.1">Et</w> … <w n="24.2">ne</w> <w n="24.3">pas</w> <w n="24.4">coucher</w> <w n="24.5">avec</w> <w n="24.6">nous</w></l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Pourquoi</w>, <w n="25.2">Belle</w>-<w n="25.3">de</w>-<w n="25.4">nuit</w> <w n="25.5">impure</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Ce</w> <w n="26.2">masque</w> <w n="26.3">noir</w> <w n="26.4">sur</w> <w n="26.5">ta</w> <w n="26.6">figure</w> ?…</l>
						<l n="27" num="5.3">— <w n="27.1">Pour</w> <w n="27.2">intriguer</w> <w n="27.3">les</w> <w n="27.4">songes</w> <w n="27.5">d</w>’<w n="27.6">or</w> ?…</l>
						<l n="28" num="5.4"><w n="28.1">N</w>’<w n="28.2">es</w>-<w n="28.3">tu</w> <w n="28.4">pas</w> <w n="28.5">l</w>’<w n="28.6">amour</w> <w n="28.7">dans</w> <w n="28.8">l</w>’<w n="28.9">espace</w>,</l>
						<l n="29" num="5.5"><w n="29.1">Souffle</w> <w n="29.2">de</w> <w n="29.3">Messaline</w> <w n="29.4">lasse</w>,</l>
						<l n="30" num="5.6"><w n="30.1">Mais</w> <w n="30.2">pas</w> <w n="30.3">rassasiée</w> <w n="30.4">encor</w> !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Insomnie</w>, <w n="31.2">est</w>-<w n="31.3">tu</w> <w n="31.4">l</w>’<w n="31.5">Hystérie</w>…</l>
						<l n="32" num="6.2"><w n="32.1">Es</w>-<w n="32.2">tu</w> <w n="32.3">l</w>’<w n="32.4">orgue</w> <w n="32.5">de</w> <w n="32.6">barbarie</w></l>
						<l n="33" num="6.3"><w n="33.1">Qui</w> <w n="33.2">moud</w> <w n="33.3">l</w>’<hi rend="ital"><w n="33.4">Hosannah</w></hi> <w n="33.5">des</w> <w n="33.6">élus</w> ?…</l>
						<l n="34" num="6.4">— <w n="34.1">Ou</w> <w n="34.2">n</w>’<w n="34.3">es</w>-<w n="34.4">tu</w> <w n="34.5">pas</w> <w n="34.6">l</w>’<w n="34.7">éternel</w> <w n="34.8">plectre</w>,</l>
						<l n="35" num="6.5"><w n="35.1">Sur</w> <w n="35.2">les</w> <w n="35.3">nerfs</w> <w n="35.4">des</w> <w n="35.5">damnés</w>-<w n="35.6">de</w>-<w n="35.7">lettre</w>,</l>
						<l n="36" num="6.6"><w n="36.1">Raclant</w> <w n="36.2">leurs</w> <w n="36.3">vers</w> — <w n="36.4">qu</w>’<w n="36.5">eux</w> <w n="36.6">seuls</w> <w n="36.7">ont</w> <w n="36.8">lus</w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Insomnie</w>, <w n="37.2">es</w>-<w n="37.3">tu</w> <w n="37.4">l</w>’<w n="37.5">âne</w> <w n="37.6">en</w> <w n="37.7">peine</w></l>
						<l n="38" num="7.2"><w n="38.1">De</w> <w n="38.2">Buridan</w> — <w n="38.3">ou</w> <w n="38.4">le</w> <w n="38.5">phalène</w></l>
						<l n="39" num="7.3"><w n="39.1">De</w> <w n="39.2">l</w>’<w n="39.3">enfer</w> ? — <w n="39.4">Ton</w> <w n="39.5">baiser</w> <w n="39.6">de</w> <w n="39.7">feu</w></l>
						<l n="40" num="7.4"><w n="40.1">Laisse</w> <w n="40.2">un</w> <w n="40.3">goût</w> <w n="40.4">froidi</w> <w n="40.5">de</w> <w n="40.6">fer</w> <w n="40.7">rouge</w>…</l>
						<l n="41" num="7.5"><w n="41.1">Oh</w> ! <w n="41.2">viens</w> <w n="41.3">te</w> <w n="41.4">poser</w> <w n="41.5">dans</w> <w n="41.6">mon</w> <w n="41.7">bouge</w> !…</l>
						<l n="42" num="7.6"><w n="42.1">Nous</w> <w n="42.2">dormirons</w> <w n="42.3">ensemble</w> <w n="42.4">un</w> <w n="42.5">peu</w>.</l>
					</lg>
				</div></body></text></TEI>