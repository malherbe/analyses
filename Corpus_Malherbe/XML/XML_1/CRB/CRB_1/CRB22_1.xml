<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB22">
					<head type="main">À LA MÉMOIRE DE ZULMA</head>
					<head type="sub_1">Vierge-folle hors barrière</head>
					<head type="sub_1">ET D’UN LOUIS</head>
					<opener>
						<dateline>
							<placeName>Bougival</placeName> 8 mai.
						</dateline>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Elle</w> <w n="1.2">était</w> <w n="1.3">riche</w> <w n="1.4">de</w> <w n="1.5">vingt</w> <w n="1.6">ans</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Moi</w> <w n="2.2">j</w>’<w n="2.3">étais</w> <w n="2.4">jeune</w> <w n="2.5">de</w> <w n="2.6">vingt</w> <w n="2.7">francs</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">nous</w> <w n="3.3">fîmes</w> <w n="3.4">bourse</w> <w n="3.5">commune</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Placée</w>, <w n="4.2">à</w> <w n="4.3">fond</w>-<w n="4.4">perdu</w>, <w n="4.5">dans</w> <w n="4.6">une</w></l>
						<l n="5" num="1.5"><w n="5.1">Infidèle</w> <w n="5.2">nuit</w> <w n="5.3">de</w> <w n="5.4">printemps</w>…</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">La</w> <w n="6.2">lune</w> <w n="6.3">a</w> <w n="6.4">fait</w> <w n="6.5">un</w> <w n="6.6">trou</w> <w n="6.7">dedans</w>,</l>
						<l n="7" num="2.2"><w n="7.1">Rond</w> <w n="7.2">comme</w> <w n="7.3">un</w> <w n="7.4">écu</w> <w n="7.5">de</w> <w n="7.6">cinq</w> <w n="7.7">francs</w>,</l>
						<l n="8" num="2.3"><w n="8.1">Par</w> <w n="8.2">où</w> <w n="8.3">passa</w> <w n="8.4">notre</w> <w n="8.5">fortune</w> :</l>
						<l n="9" num="2.4"><w n="9.1">Vingt</w> <w n="9.2">ans</w> ! <w n="9.3">vingt</w> <w n="9.4">francs</w> !… <w n="9.5">et</w> <w n="9.6">puis</w> <w n="9.7">la</w> <w n="9.8">lune</w> !</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1">— <w n="10.1">En</w> <w n="10.2">monnaie</w> — <w n="10.3">hélas</w> — <w n="10.4">les</w> <w n="10.5">vingt</w> <w n="10.6">francs</w> !</l>
						<l n="11" num="3.2"><w n="11.1">En</w> <w n="11.2">monnaie</w> <w n="11.3">aussi</w> <w n="11.4">les</w> <w n="11.5">vingt</w> <w n="11.6">ans</w> !</l>
						<l n="12" num="3.3"><w n="12.1">Toujours</w> <w n="12.2">de</w> <w n="12.3">trous</w> <w n="12.4">en</w> <w n="12.5">trous</w> <w n="12.6">de</w> <w n="12.7">lune</w>,</l>
						<l n="13" num="3.4"><w n="13.1">Et</w> <w n="13.2">de</w> <w n="13.3">bourse</w> <w n="13.4">en</w> <w n="13.5">bourse</w> <w n="13.6">commune</w>…</l>
						<l n="14" num="3.5">— <w n="14.1">C</w>’<w n="14.2">est</w> <w n="14.3">à</w> <w n="14.4">peu</w> <w n="14.5">près</w> <w n="14.6">même</w> <w n="14.7">fortune</w> !</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . .</ab>
					<lg n="4">
						<l n="15" num="4.1">— <w n="15.1">Je</w> <w n="15.2">la</w> <w n="15.3">trouvai</w> — <w n="15.4">bien</w> <w n="15.5">des</w> <w n="15.6">printemps</w>,</l>
						<l n="16" num="4.2"><w n="16.1">Bien</w> <w n="16.2">des</w> <w n="16.3">vingt</w> <w n="16.4">ans</w>, <w n="16.5">bien</w> <w n="16.6">des</w> <w n="16.7">vingt</w> <w n="16.8">francs</w>,</l>
						<l n="17" num="4.3"><w n="17.1">Bien</w> <w n="17.2">des</w> <w n="17.3">trous</w> <w n="17.4">et</w> <w n="17.5">bien</w> <w n="17.6">de</w> <w n="17.7">la</w> <w n="17.8">lune</w></l>
						<l n="18" num="4.4"><w n="18.1">Après</w> — <w n="18.2">Toujours</w> <w n="18.3">vierge</w> <w n="18.4">et</w> <w n="18.5">vingt</w> <w n="18.6">ans</w>,</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . .. . . . . . . .</ab>
					<lg n="5">
						<l n="19" num="5.1">— <w n="19.1">Puis</w> <w n="19.2">après</w> : <w n="19.3">la</w> <w n="19.4">chasse</w> <w n="19.5">aux</w> <w n="19.6">passants</w>,</l>
						<l n="20" num="5.2"><w n="20.1">Aux</w> <w n="20.2">vingt</w> <w n="20.3">sols</w>, <w n="20.4">et</w> <w n="20.5">plus</w> <w n="20.6">aux</w> <w n="20.7">vingt</w> <w n="20.8">francs</w>…</l>
						<l n="21" num="5.3"><w n="21.1">Puis</w> <w n="21.2">après</w> : <w n="21.3">la</w> <w n="21.4">fosse</w> <w n="21.5">commune</w>,</l>
						<l n="22" num="5.4"><w n="22.1">Nuit</w> <w n="22.2">gratuite</w> <w n="22.3">sans</w> <w n="22.4">trou</w> <w n="22.5">de</w> <w n="22.6">lune</w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Saint-Cloud</placeName> — Novembre.
						</dateline>
					</closer>
				</div></body></text></TEI>