<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB19">
					<head type="main">PUDENTIANE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Attouchez</w>, <w n="1.2">sans</w> <w n="1.3">toucher</w>. <w n="1.4">On</w> <w n="1.5">est</w> <w n="1.6">dévotieuse</w>,</l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><hi rend="ital"><w n="2.1">Ni</w> <w n="2.2">ne</w> <w n="2.3">retient</w> <w n="2.4">à</w> <w n="2.5">son</w> <w n="2.6">escient</w></hi>.</l>
						<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">On</w> <w n="3.3">pâme</w> <w n="3.4">d</w>’<w n="3.5">horreur</w> <w n="3.6">d</w>’<w n="3.7">être</w> : <hi rend="ital"><w n="3.8">luxurieuse</w></hi></l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><hi rend="ital"><w n="4.1">De</w> <w n="4.2">corps</w> <w n="4.3">et</w> <w n="4.4">de</w> <w n="4.5">consentement</w></hi> !…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><hi rend="ital"><w n="5.1">Et</w> <w n="5.2">de</w> <w n="5.3">chair</w></hi> … <w n="5.4">de</w> <w n="5.5">cette</w> <w n="5.6">œuvre</w> <w n="5.7">On</w> <w n="5.8">est</w> <w n="5.9">fort</w> <w n="5.10">curieuse</w>.</l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space><hi rend="ital"><w n="6.1">Sauf</w> <w n="6.2">le</w> <w n="6.3">vendredi</w> — <w n="6.4">seulement</w></hi> :</l>
						<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">confesseur</w> <w n="7.3">est</w> <w n="7.4">maigre</w> … <w n="7.5">et</w> <w n="7.6">l</w>’<w n="7.7">extase</w> <w n="7.8">pieuse</w></l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">En</w> <w n="8.2">fait</w> : <hi rend="ital"><w n="8.3">carême</w> <w n="8.4">entièrement</w></hi>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space quantity="4" unit="char"></space>… <w n="9.1">Une</w> <w n="9.2">autre</w> <w n="9.3">se</w> <w n="9.4">donne</w>. — <w n="9.5">Ici</w> <w n="9.6">l</w>’<w n="9.7">On</w> <w n="9.8">se</w> <w n="9.9">damne</w> —</l>
						<l n="10" num="3.2"><space quantity="4" unit="char"></space><w n="10.1">C</w>’<w n="10.2">est</w> <w n="10.3">un</w> <w n="10.4">tabernacle</w> — <w n="10.5">ouvert</w> — <w n="10.6">qu</w>’<w n="10.7">on</w> <w n="10.8">profane</w>.</l>
						<l n="11" num="3.3"><space quantity="4" unit="char"></space><w n="11.1">Bénitier</w> <w n="11.2">où</w> <w n="11.3">le</w> <w n="11.4">serpent</w> <w n="11.5">est</w> <w n="11.6">caché</w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><space quantity="4" unit="char"></space><w n="12.1">Que</w> <w n="12.2">l</w>’<w n="12.3">Amour</w>, <w n="12.4">ailleurs</w>, <w n="12.5">comme</w> <w n="12.6">un</w> <w n="12.7">coq</w> <w n="12.8">se</w> <w n="12.9">chante</w>…</l>
						<l n="13" num="4.2"><space quantity="4" unit="char"></space><w n="13.1">CI</w>-<w n="13.2">GIT</w> ! <w n="13.3">La</w> <hi rend="ital"><w n="13.4">pudeur</w>-<w n="13.5">d</w>’-<w n="13.6">attentat</w></hi> <w n="13.7">le</w> <w n="13.8">hante</w>…</l>
						<l n="14" num="4.3"><space quantity="4" unit="char"></space><w n="14.1">C</w>’<w n="14.2">est</w> <w n="14.3">la</w> <w n="14.4">Pomme</w> (<w n="14.5">cuite</w>) <w n="14.6">en</w> <w n="14.7">fleur</w> <w n="14.8">de</w> <w n="14.9">pêché</w>.</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Rome</placeName> — 40 ans. — 16 août.
						</dateline>
					</closer>
				</div></body></text></TEI>