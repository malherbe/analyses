<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RACCROCS</head><div type="poem" key="CRB50">
					<head type="main">LAISSER-COURRE</head>
					<head type="sub_1"><hi rend="ital">Musique de</hi> : ISAAC LAQUEDEM.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">laissé</w> <w n="1.4">la</w> <w n="1.5">potence</w></l>
						<l n="2" num="1.2"><w n="2.1">Après</w> <w n="2.2">tous</w> <w n="2.3">les</w> <w n="2.4">pendus</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Andouilles</w> <w n="3.2">de</w> <w n="3.3">naissance</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Maigres</w> <w n="4.2">fruits</w> <w n="4.3">défendus</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Les</w> <w n="5.2">plumes</w> <w n="5.3">aux</w> <w n="5.4">canards</w></l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">la</w> <w n="6.3">queue</w> <w n="6.4">aux</w> <w n="6.5">renards</w>…</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Au</w> <w n="7.2">Diable</w> <w n="7.3">aussi</w> <w n="7.4">sa</w> <w n="7.5">queue</w></l>
						<l n="8" num="2.2"><w n="8.1">Et</w> <w n="8.2">ses</w> <w n="8.3">cornes</w> <w n="8.4">aussi</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Au</w> <w n="9.2">ciel</w> <w n="9.3">sa</w> <w n="9.4">chose</w> <w n="9.5">bleue</w></l>
						<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">la</w> <w n="10.3">Planète</w> — <w n="10.4">ici</w> —</l>
						<l n="11" num="2.5"><w n="11.1">Et</w> <w n="11.2">puis</w> <w n="11.3">tout</w> : <w n="11.4">n</w>’<w n="11.5">importe</w> <w n="11.6">où</w></l>
						<l n="12" num="2.6"><w n="12.1">Dans</w> <w n="12.2">le</w> <w n="12.3">désert</w> <w n="12.4">au</w> <w n="12.5">clou</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">laissé</w> <w n="13.4">dans</w> <w n="13.5">l</w>’<w n="13.6">Espagne</w></l>
						<l n="14" num="3.2"><w n="14.1">Le</w> <w n="14.2">reste</w> <w n="14.3">et</w> <w n="14.4">mon</w> <w n="14.5">château</w> ;</l>
						<l n="15" num="3.3"><w n="15.1">Ailleurs</w>, <w n="15.2">à</w> <w n="15.3">la</w> <w n="15.4">campagne</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Ma</w> <w n="16.2">tête</w> <w n="16.3">et</w> <w n="16.4">son</w> <w n="16.5">chapeau</w> ;</l>
						<l n="17" num="3.5"><w n="17.1">J</w>’<w n="17.2">ai</w> <w n="17.3">laissé</w> <w n="17.4">mes</w> <w n="17.5">souliers</w></l>
						<l n="18" num="3.6"><w n="18.1">Sirènes</w>, <w n="18.2">à</w> <w n="18.3">vos</w> <w n="18.4">pieds</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">J</w>’<w n="19.2">ai</w> <w n="19.3">laissé</w> <w n="19.4">par</w> <w n="19.5">les</w> <w n="19.6">mondes</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Parmi</w> <w n="20.2">tous</w> <w n="20.3">les</w> <w n="20.4">frisons</w></l>
						<l n="21" num="4.3"><w n="21.1">Des</w> <w n="21.2">chauves</w>, <w n="21.3">brunes</w>, <w n="21.4">blondes</w></l>
						<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">rousses</w> … <w n="22.3">mes</w> <w n="22.4">toisons</w>.</l>
						<l n="23" num="4.5"><w n="23.1">Mon</w> <w n="23.2">épée</w> <w n="23.3">aux</w> <w n="23.4">vaincus</w>,</l>
						<l n="24" num="4.6"><w n="24.1">Ma</w> <w n="24.2">maîtresse</w> <w n="24.3">aux</w> <w n="24.4">cocus</w>…</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Aux</w> <w n="25.2">portes</w> <w n="25.3">les</w> <w n="25.4">portières</w>,</l>
						<l n="26" num="5.2"><w n="26.1">La</w> <w n="26.2">portière</w> <w n="26.3">au</w> <w n="26.4">portier</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Le</w> <w n="27.2">bouton</w> <w n="27.3">aux</w> <w n="27.4">rosières</w>,</l>
						<l n="28" num="5.4"><w n="28.1">Les</w> <w n="28.2">roses</w> <w n="28.3">au</w> <w n="28.4">rosier</w>,</l>
						<l n="29" num="5.5"><w n="29.1">A</w> <w n="29.2">l</w>’<w n="29.3">huys</w> <w n="29.4">les</w> <w n="29.5">huissiers</w>,</l>
						<l n="30" num="5.6"><w n="30.1">Créance</w> <w n="30.2">aux</w> <w n="30.3">créanciers</w>…</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Dans</w> <w n="31.2">mes</w> <w n="31.3">veines</w> <w n="31.4">ma</w> <w n="31.5">veine</w>,</l>
						<l n="32" num="6.2"><w n="32.1">Mon</w> <w n="32.2">rayon</w> <w n="32.3">au</w> <w n="32.4">soleil</w>,</l>
						<l n="33" num="6.3"><w n="33.1">Ma</w> <w n="33.2">dégaîne</w> <w n="33.3">en</w> <w n="33.4">sa</w> <w n="33.5">gaîne</w>,</l>
						<l n="34" num="6.4"><w n="34.1">Mon</w> <w n="34.2">lézard</w> <w n="34.3">au</w> <w n="34.4">sommeil</w> ;</l>
						<l n="35" num="6.5"><w n="35.1">J</w>’<w n="35.2">ai</w> <w n="35.3">laissé</w> <w n="35.4">mes</w> <w n="35.5">amours</w></l>
						<l n="36" num="6.6"><w n="36.1">Dans</w> <w n="36.2">les</w> <w n="36.3">tours</w>, <w n="36.4">dans</w> <w n="36.5">les</w> <w n="36.6">fours</w>…</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Et</w> <w n="37.2">ma</w> <w n="37.3">cotte</w> <w n="37.4">de</w> <w n="37.5">maille</w></l>
						<l n="38" num="7.2"><w n="38.1">Aux</w> <w n="38.2">artichauts</w> <w n="38.3">de</w> <w n="38.4">fer</w></l>
						<l n="39" num="7.3"><w n="39.1">Qui</w> <w n="39.2">sont</w> <w n="39.3">à</w> <w n="39.4">la</w> <w n="39.5">muraille</w></l>
						<l n="40" num="7.4"><w n="40.1">Des</w> <w n="40.2">Jardins</w> <w n="40.3">de</w> <w n="40.4">l</w>’<w n="40.5">Enfer</w> ;</l>
						<l n="41" num="7.5"><w n="41.1">Après</w> <w n="41.2">chaque</w> <w n="41.3">oripeau</w></l>
						<l n="42" num="7.6"><w n="42.1">J</w>’<w n="42.2">ai</w> <w n="42.3">laissé</w> <w n="42.4">de</w> <w n="42.5">ma</w> <w n="42.6">peau</w>.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><w n="43.1">J</w>’<w n="43.2">ai</w> <w n="43.3">laissé</w> <w n="43.4">toute</w> <w n="43.5">chose</w></l>
						<l n="44" num="8.2"><w n="44.1">Me</w> <w n="44.2">retirer</w> <w n="44.3">du</w> <w n="44.4">nez</w></l>
						<l n="45" num="8.3"><w n="45.1">Des</w> <w n="45.2">vers</w>, <w n="45.3">en</w> <w n="45.4">vers</w>, <w n="45.5">en</w> <w n="45.6">prose</w>…</l>
						<l n="46" num="8.4"><w n="46.1">Aux</w> <w n="46.2">bornes</w>, <w n="46.3">les</w> <w n="46.4">bornés</w> ;</l>
						<l n="47" num="8.5"><w n="47.1">A</w> <w n="47.2">tous</w> <w n="47.3">les</w> <w n="47.4">jeux</w> <w n="47.5">partout</w>,</l>
						<l n="48" num="8.6"><w n="48.1">Des</w> <w n="48.2">rois</w> <w n="48.3">et</w> <w n="48.4">de</w> <w n="48.5">l</w>’<w n="48.6">atout</w>.</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1"><w n="49.1">J</w>’<w n="49.2">ai</w> <w n="49.3">laissé</w> <w n="49.4">la</w> <w n="49.5">police</w></l>
						<l n="50" num="9.2"><w n="50.1">Captive</w> <w n="50.2">en</w> <w n="50.3">liberté</w>,</l>
						<l n="51" num="9.3"><w n="51.1">J</w>’<w n="51.2">ai</w> <w n="51.3">laissé</w> <w n="51.4">La</w> <w n="51.5">Palisse</w></l>
						<l n="52" num="9.4"><w n="52.1">Dire</w> <w n="52.2">la</w> <w n="52.3">vérité</w>…</l>
						<l n="53" num="9.5"><w n="53.1">Laissé</w> <w n="53.2">courre</w> <w n="53.3">le</w> <w n="53.4">sort</w></l>
						<l n="54" num="9.6"><w n="54.1">Et</w> <w n="54.2">ce</w> <w n="54.3">qui</w> <w n="54.4">court</w> <w n="54.5">encor</w>.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1"><w n="55.1">J</w>’<w n="55.2">ai</w> <w n="55.3">laissé</w> <w n="55.4">l</w>’<w n="55.5">Espérance</w>,</l>
						<l n="56" num="10.2"><w n="56.1">Vieillissant</w> <w n="56.2">doucement</w>,</l>
						<l n="57" num="10.3"><w n="57.1">Retomber</w> <w n="57.2">en</w> <w n="57.3">enfance</w>,</l>
						<l n="58" num="10.4"><w n="58.1">Vierge</w> <w n="58.2">folle</w> <w n="58.3">sans</w> <w n="58.4">dent</w>.</l>
						<l n="59" num="10.5"><w n="59.1">J</w>’<w n="59.2">ai</w> <w n="59.3">laissé</w> <w n="59.4">tous</w> <w n="59.5">les</w> <w n="59.6">Dieux</w>,</l>
						<l n="60" num="10.6"><w n="60.1">J</w>’<w n="60.2">ai</w> <w n="60.3">laissé</w> <w n="60.4">pire</w> <w n="60.5">et</w> <w n="60.6">mieux</w>.</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1"><w n="61.1">J</w>’<w n="61.2">ai</w> <w n="61.3">laissé</w> <w n="61.4">bien</w> <w n="61.5">tranquilles</w></l>
						<l n="62" num="11.2"><w n="62.1">Ceux</w> <w n="62.2">qui</w> <w n="62.3">ne</w> <w n="62.4">l</w>’<w n="62.5">étaient</w> <w n="62.6">pas</w>,</l>
						<l n="63" num="11.3"><w n="63.1">Aux</w> <w n="63.2">pattes</w> <w n="63.3">imbéciles</w></l>
						<l n="64" num="11.4"><w n="64.1">J</w>’<w n="64.2">ai</w> <w n="64.3">laissé</w> <w n="64.4">tous</w> <w n="64.5">les</w> <w n="64.6">plats</w> ;</l>
						<l n="65" num="11.5"><w n="65.1">Aux</w> <w n="65.2">poètes</w> <w n="65.3">la</w> <w n="65.4">foi</w>…</l>
						<l n="66" num="11.6"><w n="66.1">Puis</w> <w n="66.2">me</w> <w n="66.3">suis</w> <w n="66.4">laissé</w> <w n="66.5">moi</w>.</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1"><w n="67.1">Sous</w> <w n="67.2">le</w> <w n="67.3">temps</w>, <w n="67.4">sans</w> <w n="67.5">égides</w></l>
						<l n="68" num="12.2"><w n="68.1">M</w>’<w n="68.2">a</w> <w n="68.3">mal</w> <w n="68.4">mené</w> <w n="68.5">fort</w> <w n="68.6">bien</w></l>
						<l n="69" num="12.3"><w n="69.1">La</w> <w n="69.2">vie</w> <w n="69.3">à</w> <w n="69.4">grandes</w> <w n="69.5">guides</w>…</l>
						<l n="70" num="12.4"><w n="70.1">Au</w> <w n="70.2">bout</w> <w n="70.3">des</w> <w n="70.4">guides</w> — <w n="70.5">rien</w> —</l>
						<l n="71" num="12.5">… <w n="71.1">Laissé</w>, <w n="71.2">blasé</w>, <w n="71.3">passé</w>,</l>
						<l n="72" num="12.6"><w n="72.1">Rien</w> <w n="72.2">ne</w> <w n="72.3">m</w>’<w n="72.4">a</w> <w n="72.5">rien</w> <w n="72.6">laissé</w>…</l>
					</lg>
				</div></body></text></TEI>