<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GENS DE MER</head><div type="poem" key="CRB84">
					<head type="main">LA GOUTTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Sous</w> <w n="1.2">un</w> <w n="1.3">seul</w> <w n="1.4">hunier</w> — <w n="1.5">le</w> <w n="1.6">dernier</w> — <w n="1.7">à</w> <w n="1.8">la</w> <w n="1.9">cape</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">navire</w> <w n="2.3">était</w> <w n="2.4">soûl</w> ; <w n="2.5">l</w>’<w n="2.6">eau</w> <w n="2.7">sur</w> <w n="2.8">nous</w> <w n="2.9">faisait</w> <w n="2.10">nappe</w>.</l>
						<l n="3" num="1.3">— <w n="3.1">Aux</w> <w n="3.2">pompes</w>, <w n="3.3">faillis</w> <w n="3.4">chiens</w> ! — <w n="3.5">L</w>’<w n="3.6">équipage</w> <w n="3.7">fit</w> — <w n="3.8">non</w>. —</l>
					</lg>
					<lg n="2">
						<l part="I" n="4" num="2.1">— <w n="4.1">Le</w> <w n="4.2">hunier</w> ! <w n="4.3">le</w> <w n="4.4">hunier</w> !… </l>
						<l part="F" n="4" num="2.1"><w n="4.5">C</w>’<w n="4.6">est</w> <w n="4.7">un</w> <w n="4.8">coup</w> <w n="4.9">de</w> <w n="4.10">canon</w>.</l>
						<l n="5" num="2.2"><w n="5.1">Un</w> <w n="5.2">grand</w> <w n="5.3">froufrou</w> <w n="5.4">de</w> <w n="5.5">soie</w> <w n="5.6">à</w> <w n="5.7">travers</w> <w n="5.8">la</w> <w n="5.9">tourmente</w>.</l>
					</lg>
					<lg n="3">
						<l n="6" num="3.1">— <w n="6.1">Le</w> <w n="6.2">hunier</w> <w n="6.3">emporté</w> ! — <w n="6.4">C</w>’<w n="6.5">est</w> <w n="6.6">la</w> <w n="6.7">fin</w>. <w n="6.8">Quelqu</w>’<w n="6.9">un</w> <w n="6.10">chante</w>. —</l>
						<l n="7" num="3.2">— <w n="7.1">Tais</w>-<w n="7.2">toi</w>, <w n="7.3">Lascar</w> ! — <w n="7.4">Tantôt</w>. — <w n="7.5">Le</w> <w n="7.6">hunier</w> <w n="7.7">emporté</w> !…</l>
						<l n="8" num="3.3">— <w n="8.1">Pare</w> <w n="8.2">le</w> <w n="8.3">foc</w>, <w n="8.4">quelqu</w>’<w n="8.5">un</w> <w n="8.6">de</w> <w n="8.7">bonne</w> <w n="8.8">volonté</w> !…</l>
						<l n="9" num="3.4">— <w n="9.1">Moi</w>. — <w n="9.2">Toi</w>, <w n="9.3">lascar</w> ? — <w n="9.4">Je</w> <w n="9.5">chantais</w> <w n="9.6">ça</w>, <w n="9.7">moi</w>, <w n="9.8">capitaine</w>.</l>
						<l n="10" num="3.5">— <w n="10.1">Va</w>. — <w n="10.2">Non</w> : <w n="10.3">la</w> <w n="10.4">goutte</w> <w n="10.5">avant</w> ? — <w n="10.6">Non</w>, <w n="10.7">après</w>. — <w n="10.8">Pas</w> <w n="10.9">la</w> <w n="10.10">peine</w> :</l>
						<l part="I" n="11" num="3.6"><w n="11.1">La</w> <w n="11.2">grande</w> <w n="11.3">tasse</w> <w n="11.4">est</w> <w n="11.5">là</w> <w n="11.6">pour</w> <w n="11.7">un</w> <w n="11.8">coup</w>… — </l>
						<l part="F" n="11" num="3.6"><w n="11.9">Pour</w> <w n="11.10">braver</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Quoi</w> ! <w n="12.2">mourir</w> <w n="12.3">pour</w> <w n="12.4">mourir</w> <w n="12.5">et</w> <w n="12.6">ne</w> <w n="12.7">rien</w> <w n="12.8">sauver</w>…</l>
						<l n="13" num="4.2">— <w n="13.1">Fais</w> <w n="13.2">comme</w> <w n="13.3">tu</w> <w n="13.4">pourras</w> : <w n="13.5">Coupe</w>. <w n="13.6">Et</w> <w n="13.7">gare</w> <w n="13.8">à</w> <w n="13.9">ta</w> <w n="13.10">drisse</w>.</l>
						<l part="I" n="14" num="4.3">— <w n="14.1">Merci</w> — </l>
						<l part="F" n="14" num="4.3"><w n="14.2">D</w>’<w n="14.3">un</w> <w n="14.4">bond</w> <w n="14.5">du</w> <w n="14.6">singe</w> <w n="14.7">il</w> <w n="14.8">saute</w>, <w n="14.9">de</w> <w n="14.10">la</w> <w n="14.11">lisse</w>,</l>
						<l n="15" num="4.4"><w n="15.1">Sur</w> <w n="15.2">le</w> <w n="15.3">beaupré</w> <w n="15.4">noyé</w>, <w n="15.5">dans</w> <w n="15.6">les</w> <w n="15.7">agrès</w> <w n="15.8">pendants</w>.</l>
						<l part="I" n="16" num="4.5">— <w n="16.1">Bravo</w> ! — </l>
						<l part="F" n="16" num="4.5"><w n="16.2">Nous</w> <w n="16.3">regardions</w>, <w n="16.4">la</w> <w n="16.5">mort</w> <w n="16.6">entre</w> <w n="16.7">les</w> <w n="16.8">dents</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">— <w n="17.1">Garçons</w>, <w n="17.2">tous</w> <w n="17.3">à</w> <w n="17.4">la</w> <w n="17.5">drisse</w> ! <w n="17.6">à</w> <w n="17.7">nous</w> ! <w n="17.8">pare</w> <w n="17.9">l</w>’<w n="17.10">écoute</w> !…</l>
						<l n="18" num="5.2">(<w n="18.1">Le</w> <w n="18.2">coup</w> <w n="18.3">de</w> <w n="18.4">grâce</w> <w n="18.5">enfin</w>… ) — <w n="18.6">Hisse</w> ! <w n="18.7">barre</w> <w n="18.8">au</w> <w n="18.9">vent</w> <w n="18.10">toute</w> !</l>
						<l part="I" n="19" num="5.3"><w n="19.1">Hurrah</w> ! <w n="19.2">nous</w> <w n="19.3">abattons</w> !… — </l>
						<l part="F" n="19" num="5.3"><w n="19.4">Et</w> <w n="19.5">le</w> <w n="19.6">foc</w> <w n="19.7">déferlé</w></l>
						<l n="20" num="5.4"><w n="20.1">Redresse</w> <w n="20.2">en</w> <w n="20.3">un</w> <w n="20.4">clin</w> <w n="20.5">d</w>’<w n="20.6">œil</w> <w n="20.7">le</w> <w n="20.8">navire</w> <w n="20.9">acculé</w>.</l>
						<l n="21" num="5.5"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">le</w> <w n="21.4">salut</w> <w n="21.5">à</w> <w n="21.6">nous</w> <w n="21.7">qui</w> <w n="21.8">bat</w> <w n="21.9">dans</w> <w n="21.10">cette</w> <w n="21.11">loque</w></l>
						<l n="22" num="5.6"><w n="22.1">Fuyant</w> <w n="22.2">devant</w> <w n="22.3">le</w> <w n="22.4">temps</w> ! <w n="22.5">Encor</w> <w n="22.6">paré</w> <w n="22.7">la</w> <w n="22.8">coque</w> !</l>
						<l part="I" n="23" num="5.7">— <w n="23.1">Hurrah</w> <w n="23.2">pour</w> <w n="23.3">le</w> <w n="23.4">lascar</w> ! — <w n="23.5">Le</w> <w n="23.6">lascar</w> ?… </l>
						<l part="F" n="23" num="5.7"> — <w n="23.7">A</w> <w n="23.8">la</w> <w n="23.9">mer</w> !</l>
						<l n="24" num="5.8">— <w n="24.1">Disparu</w> ? — <w n="24.2">Disparu</w> — <w n="24.3">Bon</w>, <w n="24.4">ce</w> <w n="24.5">n</w>’<w n="24.6">est</w> <w n="24.7">pas</w> <w n="24.8">trop</w> <w n="24.9">cher</w>.</l>
			</lg>
			<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
			<lg n="6">
						<l n="25" num="6.1">— <w n="25.1">Ouf</w> ! <w n="25.2">c</w>’<w n="25.3">est</w> <w n="25.4">fait</w> — <w n="25.5">Toi</w>, <w n="25.6">Lascar</w> ! — <w n="25.7">moi</w>, <w n="25.8">Lascar</w>, <w n="25.9">capitaine</w>,</l>
						<l n="26" num="6.2"><w n="26.1">La</w> <w n="26.2">lame</w> <w n="26.3">m</w>’<w n="26.4">a</w> <w n="26.5">rincé</w> <w n="26.6">de</w> <w n="26.7">dessus</w> <w n="26.8">la</w> <w n="26.9">poulaine</w>,</l>
						<l n="27" num="6.3"><w n="27.1">Le</w> <w n="27.2">même</w> <w n="27.3">coup</w> <w n="27.4">de</w> <w n="27.5">mer</w> <w n="27.6">m</w>’<w n="27.7">a</w> <w n="27.8">ramené</w> <w n="27.9">gratis</w>…</l>
						<l n="28" num="6.4"><w n="28.1">Allons</w>, <w n="28.2">mes</w> <w n="28.3">poux</w> <w n="28.4">n</w>’<w n="28.5">auront</w> <w n="28.6">pas</w> <w n="28.7">besoin</w> <w n="28.8">d</w>’<w n="28.9">onguent</w>-<w n="28.10">gris</w>.</l>
					</lg>
					<lg n="7">
						<l n="29" num="7.1">— <w n="29.1">Accoste</w>, <w n="29.2">tout</w> <w n="29.3">le</w> <w n="29.4">monde</w> ! <w n="29.5">Et</w> <w n="29.6">toi</w>, <w n="29.7">Lascar</w>, <w n="29.8">écoute</w> :</l>
						<l n="30" num="7.2"><w n="30.1">Nous</w> <w n="30.2">te</w> <w n="30.3">devons</w> <w n="30.4">la</w> <w n="30.5">vie</w>… — <w n="30.6">Après</w> ? — <w n="30.7">Pour</w> <w n="30.8">ça</w> ?… — <w n="30.9">La</w> <w n="30.10">goutte</w> !</l>
						<l n="31" num="7.3"><w n="31.1">Mais</w> <w n="31.2">c</w>’<w n="31.3">était</w> <w n="31.4">pas</w> <w n="31.5">pour</w> <w n="31.6">ça</w>, <w n="31.7">n</w>’<w n="31.8">allez</w> <w n="31.9">pas</w> <w n="31.10">croire</w>, <w n="31.11">au</w> <w n="31.12">moins</w>…</l>
						<l n="32" num="7.4">— <w n="32.1">Viens</w> <w n="32.2">m</w>’<w n="32.3">embrasser</w> ! — <w n="32.4">Attrape</w> <w n="32.5">à</w> <w n="32.6">torcher</w> <w n="32.7">les</w> <w n="32.8">grouins</w>.</l>
						<l n="33" num="7.5"><w n="33.1">J</w>’<w n="33.2">suis</w> <w n="33.3">pas</w> <w n="33.4">beau</w>, <w n="33.5">capitain</w>’, <w n="33.6">mais</w>, <w n="33.7">soit</w> <w n="33.8">dit</w> <w n="33.9">en</w> <w n="33.10">famille</w>,</l>
						<l n="34" num="7.6"><w n="34.1">Je</w> <w n="34.2">vous</w> <w n="34.3">ai</w> <w n="34.4">fait</w> <w n="34.5">plaisir</w> <w n="34.6">plus</w> <w n="34.7">qu</w>’<w n="34.8">une</w> <w n="34.9">belle</w> <w n="34.10">fille</w> ?…</l>
			</lg>
			<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
			<lg n="8">
						<l n="35" num="8.1"><w n="35.1">Le</w> <w n="35.2">capitaine</w> <w n="35.3">mit</w>, <w n="35.4">ce</w> <w n="35.5">jour</w>, <w n="35.6">sur</w> <w n="35.7">son</w> <w n="35.8">rapport</w> :</l>
						<l n="36" num="8.2">— <hi rend="ital"><w n="36.1">Gros</w> <w n="36.2">temps</w>. <w n="36.3">Laissé</w> <w n="36.4">porter</w>. <w n="36.5">Rien</w> <w n="36.6">de</w> <w n="36.7">neuf</w> <w n="36.8">à</w> <w n="36.9">bord</w></hi>. —</l>
					</lg>
					<closer>
						<placeName>À bord</placeName>.
					</closer>
				</div></body></text></TEI>