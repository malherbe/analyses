<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES AMOURS JAUNES</head><div type="poem" key="CRB31">
					<head type="main">FLEUR D’ART</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Oui</w> — <w n="1.2">Quel</w> <w n="1.3">art</w> <w n="1.4">jaloux</w> <w n="1.5">dans</w> <w n="1.6">Ta</w> <w n="1.7">fine</w> <w n="1.8">histoire</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Quels</w> <w n="2.2">bibelots</w> <w n="2.3">chers</w> ! — <w n="2.4">Un</w> <w n="2.5">bout</w> <w n="2.6">de</w> <w n="2.7">sonnet</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">cœur</w> <w n="3.3">gravé</w> <w n="3.4">dans</w> <w n="3.5">ta</w> <w n="3.6">manière</w> <w n="3.7">noire</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">traits</w> <w n="4.3">de</w> <w n="4.4">canif</w> <w n="4.5">à</w> <w n="4.6">coups</w> <w n="4.7">de</w> <w n="4.8">stylet</w>. —</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tout</w> <w n="5.2">fier</w> <w n="5.3">mon</w> <w n="5.4">cœur</w> <w n="5.5">porte</w> <w n="5.6">à</w> <w n="5.7">la</w> <w n="5.8">boutonnière</w></l>
						<l n="6" num="2.2"><w n="6.1">Que</w> <w n="6.2">tu</w> <w n="6.3">lui</w> <w n="6.4">taillas</w>, <w n="6.5">un</w> <w n="6.6">petit</w> <w n="6.7">bouquet</w></l>
						<l n="7" num="2.3"><w n="7.1">D</w>’<w n="7.2">immortelle</w> <w n="7.3">rouge</w> — <w n="7.4">Encor</w> <w n="7.5">ta</w> <w n="7.6">manière</w> —</l>
						<l n="8" num="2.4"><w n="8.1">C</w>’<w n="8.2">est</w> <w n="8.3">du</w> <w n="8.4">sang</w> <w n="8.5">en</w> <w n="8.6">fleur</w>. <w n="8.7">Souvenir</w> <w n="8.8">coquet</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Allons</w>, <w n="9.2">pas</w> <w n="9.3">de</w> <w n="9.4">pleurs</w> <w n="9.5">à</w> <w n="9.6">notre</w> <w n="9.7">mémoire</w> !</l>
						<l n="10" num="3.2">— <w n="10.1">C</w>’<w n="10.2">est</w> <w n="10.3">la</w> <w n="10.4">mâle</w>-<w n="10.5">mort</w> <w n="10.6">de</w> <w n="10.7">l</w>’<w n="10.8">amour</w> <w n="10.9">ici</w> —</l>
						<l n="11" num="3.3"><w n="11.1">Foin</w> <w n="11.2">du</w> <w n="11.3">myosotis</w>, <w n="11.4">vieux</w> <w n="11.5">sachet</w> <w n="11.6">d</w>’<w n="11.7">armoire</w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Double</w> <w n="12.2">femme</w>, <w n="12.3">va</w> !… <w n="12.4">Qu</w>’<w n="12.5">un</w> <w n="12.6">âne</w> <w n="12.7">te</w> <w n="12.8">braie</w> !</l>
						<l n="13" num="4.2"><w n="13.1">Si</w> <w n="13.2">tu</w> <w n="13.3">n</w>’<w n="13.4">étais</w> <w n="13.5">fausse</w>, <w n="13.6">eh</w> <w n="13.7">serais</w>-<w n="13.8">tu</w> <w n="13.9">vraie</w> ?…</l>
						<l n="14" num="4.3"><w n="14.1">L</w>’<w n="14.2">amour</w> <w n="14.3">est</w> <w n="14.4">un</w> <w n="14.5">duel</w> : — <w n="14.6">Bien</w> <w n="14.7">touché</w> ! <w n="14.8">Merci</w>.</l>
					</lg>
				</div></body></text></TEI>