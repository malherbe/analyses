<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES AMOURS JAUNES</title>
				<title type="medium">Édition électronique</title>
				<author key="CRB">
					<name>
						<forename>Tristan</forename>
						<surname>CORBIÈRE</surname>
					</name>
					<date from="1845" to="1875">1845-1875</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4094 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">CRB_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Amours jaunes</title>
						<author>Tristan Corbière</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URL">http://www.gutenberg.org/ebooks/16883</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Amours jaunes</title>
								<author>Tristan Corbière</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k70668p.r=tristan+corbi%C3%A8re.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>LIBRAIRIE DU XIXe SIÈCLE ‒ GLADY FRÈRES, ÉDITEURS</publisher>
									<date when="1873">1873</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">GENS DE MER</head><div type="poem" key="CRB81">
					<head type="main">LE RENÉGAT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ça</w> <w n="1.2">c</w>’<w n="1.3">est</w> <w n="1.4">un</w> <w n="1.5">renégat</w>. <w n="1.6">Contumace</w> <w n="1.7">partout</w> :</l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">Pour</w> <w n="2.2">ne</w> <w n="2.3">rien</w> <w n="2.4">faire</w>, <w n="2.5">ça</w> <w n="2.6">fait</w> <w n="2.7">tout</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Écumé</w> <w n="3.2">de</w> <w n="3.3">partout</w> <w n="3.4">et</w> <w n="3.5">d</w>’<w n="3.6">ailleurs</w> ; <w n="3.7">crâne</w> <w n="3.8">et</w> <w n="3.9">lâche</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Écumeur</w> <w n="4.2">amphibie</w>, <w n="4.3">à</w> <w n="4.4">la</w> <w n="4.5">course</w>, <w n="4.6">à</w> <w n="4.7">la</w> <w n="4.8">tâche</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Esclave</w>, <w n="5.2">flibustier</w>, <w n="5.3">nègre</w>, <w n="5.4">blanc</w>, <w n="5.5">ou</w> <w n="5.6">soldat</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Bravo</w> : <w n="6.2">fait</w> <w n="6.3">tout</w> <w n="6.4">ce</w> <w n="6.5">qui</w> <w n="6.6">concerne</w> <w n="6.7">tout</w> <w n="6.8">état</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Singe</w>, <w n="7.2">limier</w> <w n="7.3">de</w> <w n="7.4">femme</w> … <w n="7.5">ou</w> <w n="7.6">même</w>, <w n="7.7">au</w> <w n="7.8">besoin</w>, <w n="7.9">femme</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">Prophète</w> <hi rend="ital"><w n="8.2">in</w> <w n="8.3">partibus</w></hi>, <w n="8.4">à</w> <w n="8.5">tant</w> <w n="8.6">par</w> <w n="8.7">kilo</w> <w n="8.8">d</w>’<w n="8.9">âme</w> ;</l>
						<l n="9" num="1.9"><w n="9.1">Pendu</w>, <w n="9.2">bourreau</w>, <w n="9.3">poison</w>, <w n="9.4">flûtiste</w>, <w n="9.5">médecin</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Eunuque</w> ; <w n="10.2">ou</w> <w n="10.3">mendiant</w>, <w n="10.4">un</w> <w n="10.5">coutelas</w> <w n="10.6">en</w> <w n="10.7">main</w>…</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">La</w> <w n="11.2">mort</w> <w n="11.3">le</w> <w n="11.4">connaît</w> <w n="11.5">bien</w>, <w n="11.6">mais</w> <w n="11.7">n</w>’<w n="11.8">en</w> <w n="11.9">a</w> <w n="11.10">plus</w> <w n="11.11">envie</w>…</l>
						<l n="12" num="2.2"><w n="12.1">Recraché</w> <w n="12.2">par</w> <w n="12.3">la</w> <w n="12.4">mort</w>, <w n="12.5">recraché</w> <w n="12.6">par</w> <w n="12.7">la</w> <w n="12.8">vie</w>,</l>
						<l n="13" num="2.3"><w n="13.1">Ça</w> <w n="13.2">mange</w> <w n="13.3">de</w> <w n="13.4">l</w>’<w n="13.5">humain</w>, <w n="13.6">de</w> <w n="13.7">l</w>’<w n="13.8">or</w>, <w n="13.9">de</w> <w n="13.10">l</w>’<w n="13.11">excrément</w>,</l>
						<l n="14" num="2.4"><w n="14.1">Du</w> <w n="14.2">plomb</w>, <w n="14.3">de</w> <w n="14.4">l</w>’<w n="14.5">ambroisie</w> … <w n="14.6">ou</w> <w n="14.7">rien</w> — <w n="14.8">Ce</w> <w n="14.9">que</w> <w n="14.10">ça</w> <w n="14.11">sent</w>. —</l>
					</lg>
					<lg n="3">
						<l n="15" num="3.1"><w n="15.1">Son</w> <w n="15.2">nom</w> ? — <w n="15.3">Il</w> <w n="15.4">a</w> <w n="15.5">changé</w> <w n="15.6">de</w> <w n="15.7">peau</w>, <w n="15.8">comme</w> <w n="15.9">chemise</w>…</l>
						<l n="16" num="3.2"><w n="16.1">Dans</w> <w n="16.2">toutes</w> <w n="16.3">langues</w> <w n="16.4">c</w>’<w n="16.5">est</w> : <w n="16.6">Ignace</w> <w n="16.7">ou</w> <w n="16.8">Cydalyse</w>,</l>
						<l n="17" num="3.3"><hi rend="ital"><w n="17.1">Todos</w> <w n="17.2">los</w> <w n="17.3">santos</w></hi>… <w n="17.4">Mais</w> <w n="17.5">il</w> <w n="17.6">ne</w> <w n="17.7">porte</w> <w n="17.8">plus</w> <w n="17.9">ça</w> ;</l>
						<l n="18" num="3.4"><w n="18.1">Il</w> <w n="18.2">a</w> <w n="18.3">bien</w> <w n="18.4">effacé</w> <w n="18.5">son</w> <hi rend="ital"><subst reason="analysis" type="phonemization" hand="RR"><del>T</del><add rend="hidden"><w n="18.6">é</w></add></subst>. <subst reason="analysis" type="phonemization" hand="RR"><add rend="hidden"><w n="18.7">èf</w></add><del>F</del></subst>.</hi>. <w n="18.8">de</w> <w n="18.9">forçat</w> !…</l>
						<l n="19" num="3.5">— <w n="19.1">Qui</w> <w n="19.2">l</w>’<w n="19.3">a</w> <w n="19.4">poussé</w> … <w n="19.5">l</w>’<w n="19.6">amour</w> ? — <w n="19.7">Il</w> <w n="19.8">a</w> <w n="19.9">jeté</w> <w n="19.10">sa</w> <w n="19.11">gourme</w> !</l>
						<l n="20" num="3.6"><w n="20.1">Il</w> <w n="20.2">a</w> <w n="20.3">tout</w> <w n="20.4">violé</w> : <w n="20.5">potence</w> <w n="20.6">et</w> <w n="20.7">garde</w>-<w n="20.8">chiourme</w>.</l>
						<l n="21" num="3.7">— <w n="21.1">La</w> <w n="21.2">haine</w> ? — <w n="21.3">Non</w>. — <w n="21.4">Le</w> <w n="21.5">vol</w> ? — <w n="21.6">Il</w> <w n="21.7">a</w> <w n="21.8">refusé</w> <w n="21.9">mieux</w>.</l>
						<l n="22" num="3.8">— <w n="22.1">Coup</w> <w n="22.2">de</w> <w n="22.3">barre</w> <w n="22.4">du</w> <w n="22.5">vice</w> ? — <w n="22.6">Il</w> <w n="22.7">n</w>’<w n="22.8">est</w> <w n="22.9">pas</w> <w n="22.10">vicieux</w> ;</l>
						<l n="23" num="3.9"><w n="23.1">Non</w> … <w n="23.2">dans</w> <w n="23.3">le</w> <w n="23.4">ventre</w> <w n="23.5">il</w> <w n="23.6">a</w> <w n="23.7">de</w> <w n="23.8">la</w> <w n="23.9">fille</w>-<w n="23.10">de</w>-<w n="23.11">joie</w>,</l>
						<l n="24" num="3.10"><w n="24.1">C</w>’<w n="24.2">est</w> <w n="24.3">un</w> <w n="24.4">tempérament</w> … <w n="24.5">un</w> <w n="24.6">artiste</w> <w n="24.7">de</w> <w n="24.8">proie</w>.</l>
					</lg>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Au</w> <w n="25.2">diable</w> <w n="25.3">même</w> <w n="25.4">il</w> <w n="25.5">n</w>’<w n="25.6">a</w> <w n="25.7">pas</w> <w n="25.8">fait</w> <w n="25.9">miséricorde</w>.</l>
						<l n="26" num="4.2">— <w n="26.1">Hâle</w> <w n="26.2">encore</w> ! — <w n="26.3">Il</w> <w n="26.4">a</w> <w n="26.5">tout</w> <w n="26.6">pourri</w> <w n="26.7">jusqu</w>’<w n="26.8">à</w> <w n="26.9">la</w> <w n="26.10">corde</w>,</l>
						<l n="27" num="4.3"><w n="27.1">Il</w> <w n="27.2">a</w> <w n="27.3">tué</w> <w n="27.4">toute</w> <hi rend="ital"><w n="27.5">bête</w></hi>, <w n="27.6">éreinté</w> <w n="27.7">tous</w> <w n="27.8">les</w> <w n="27.9">coups</w>…</l>
					</lg>
					<lg n="5">
						<l n="28" num="5.1"><w n="28.1">Pur</w>, <w n="28.2">à</w> <w n="28.3">force</w> <w n="28.4">d</w>’<w n="28.5">avoir</w> <w n="28.6">purgé</w> <w n="28.7">tous</w> <w n="28.8">les</w> <w n="28.9">dégoûts</w>.</l>
					</lg>
					<closer>
						<placeName>Baléares</placeName>.
					</closer>
				</div></body></text></TEI>