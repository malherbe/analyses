<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">MADRIGAUX</head><div type="poem" key="DHL49">
					<head type="main">Au Roi</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">LOUIS</w>, <w n="1.2">que</w> <w n="1.3">vous</w> <w n="1.4">imitez</w> <w n="1.5">bien</w></l>
						<l n="2" num="1.2"><w n="2.1">Cet</w> <w n="2.2">Être</w> <w n="2.3">indépendant</w> <w n="2.4">dont</w> <w n="2.5">vous</w> <w n="2.6">êtes</w> <w n="2.7">l</w>’<w n="2.8">image</w> !</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Comme</w> <w n="3.2">lui</w>, <w n="3.3">des</w> <w n="3.4">rois</w> <w n="3.5">qu</w>’<w n="3.6">on</w> <w n="3.7">outrage</w></l>
						<l n="4" num="1.4"><w n="4.1">Vous</w> <w n="4.2">êtes</w> <w n="4.3">le</w> <w n="4.4">vengeur</w> <w n="4.5">et</w> <w n="4.6">l</w>’<w n="4.7">unique</w> <w n="4.8">soutien</w> ;</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Comme</w> <w n="5.2">lui</w>, <w n="5.3">votre</w> <w n="5.4">main</w> <w n="5.5">foudroie</w></l>
						<l n="6" num="1.6"><w n="6.1">Ces</w> <w n="6.2">coupables</w> <w n="6.3">mortels</w> <w n="6.4">dont</w> <w n="6.5">les</w> <w n="6.6">noires</w> <w n="6.7">fureurs</w></l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Ont</w> <w n="7.2">mis</w> <w n="7.3">toute</w> <w n="7.4">l</w>’<w n="7.5">Europe</w> <w n="7.6">en</w> <w n="7.7">proie</w></l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">À</w> <w n="8.2">ce</w> <w n="8.3">que</w> <w n="8.4">la</w> <w n="8.5">guerre</w> <w n="8.6">a</w> <w n="8.7">d</w>’<w n="8.8">horreurs</w> :</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Comme</w> <w n="9.2">lui</w>, <w n="9.3">rempli</w> <w n="9.4">de</w> <w n="9.5">clémence</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Quelque</w> <w n="10.2">douceur</w> <w n="10.3">qu</w>’<w n="10.4">ait</w> <w n="10.5">la</w> <w n="10.6">vengeance</w>,</l>
						<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Vous</w> <w n="11.2">êtes</w> <w n="11.3">prêt</w> <w n="11.4">à</w> <w n="11.5">pardonner</w> ;</l>
						<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">sur</w> <w n="12.3">les</w> <w n="12.4">bords</w> <w n="12.5">du</w> <w n="12.6">Pô</w>, <w n="12.7">du</w> <w n="12.8">Rhin</w> <w n="12.9">et</w> <w n="12.10">de</w> <w n="12.11">la</w> <w n="12.12">Meuse</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Vous</w> <w n="13.2">ne</w> <w n="13.3">les</w> <w n="13.4">accablez</w> <w n="13.5">que</w> <w n="13.6">pour</w> <w n="13.7">les</w> <w n="13.8">amener</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Par</w> <w n="14.2">un</w> <w n="14.3">prompt</w> <w n="14.4">repentir</w>, <w n="14.5">à</w> <w n="14.6">cette</w> <w n="14.7">paix</w> <w n="14.8">heureuse</w></l>
						<l n="15" num="1.15"><space unit="char" quantity="8"></space><w n="15.1">Que</w> <w n="15.2">vous</w> <w n="15.3">seul</w> <w n="15.4">pouvez</w> <w n="15.5">leur</w> <w n="15.6">donner</w>.</l>
					</lg>
					<ab type="dot">▻◅</ab>
					<lg n="2">
						<l n="16" num="2.1"><w n="16.1">DE</w> <w n="16.2">lauriers</w> <w n="16.3">immortels</w> <w n="16.4">mon</w> <w n="16.5">front</w> <w n="16.6">est</w> <w n="16.7">couronné</w>,</l>
						<l n="17" num="2.2"><w n="17.1">Sur</w> <w n="17.2">d</w>’<w n="17.3">illustres</w> <w n="17.4">rivaux</w> <w n="17.5">j</w>’<w n="17.6">emporte</w> <w n="17.7">la</w> <w n="17.8">victoire</w> ;</l>
						<l n="18" num="2.3"><space unit="char" quantity="8"></space><w n="18.1">Rien</w> <w n="18.2">ne</w> <w n="18.3">manquerait</w> <w n="18.4">à</w> <w n="18.5">ma</w> <w n="18.6">gloire</w>,</l>
						<l n="19" num="2.4"><w n="19.1">Si</w> <w n="19.2">Louis</w>, <w n="19.3">ce</w> <w n="19.4">héros</w> <w n="19.5">si</w> <w n="19.6">grand</w>, <w n="19.7">si</w> <w n="19.8">fortuné</w>,</l>
						<l n="20" num="2.5"><w n="20.1">Applaudissait</w> <w n="20.2">aux</w> <w n="20.3">prix</w> <w n="20.4">qu</w>’<w n="20.5">Apollon</w> <w n="20.6">m</w>’<w n="20.7">a</w> <w n="20.8">donné</w>.</l>
					</lg>
					<ab type="dot">▻◅</ab>
				</div></body></text></TEI>