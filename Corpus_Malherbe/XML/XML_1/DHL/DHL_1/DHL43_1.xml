<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">RONDEAUX</head><div type="poem" key="DHL43">
					<head type="main">Rondeau redoublé</head>
					<head type="sub_1">À M. LE DUC DE SAINT-AIGNAN,</head>
					<head type="sub_2">SUR LA GUÉRISON DE LA FIÈVRE QUARTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">SANS</w> <w n="1.2">dégaîner</w> <w n="1.3">et</w> <w n="1.4">sans</w> <w n="1.5">monter</w> <w n="1.6">Moreau</w></l>
						<l n="2" num="1.2"><w n="2.1">Mettez</w> <w n="2.2">à</w> <w n="2.3">fin</w> <w n="2.4">périlleuse</w> <w n="2.5">aventure</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Onc</w> <w n="3.2">chevalier</w> <w n="3.3">ne</w> <w n="3.4">fit</w> <w n="3.5">exploit</w> <w n="3.6">plus</w> <w n="3.7">beau</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Contre</w> <w n="4.2">vous</w>-<w n="4.3">même</w> <w n="4.4">en</w> <w n="4.5">ferais</w> <w n="4.6">la</w> <w n="4.7">gageure</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Quoi</w> ! <w n="5.2">de</w> <w n="5.3">félonne</w> <w n="5.4">et</w> <w n="5.5">laide</w> <w n="5.6">créature</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Fièvre</w> <w n="6.2">qui</w> <w n="6.3">sait</w> <w n="6.4">ouvrir</w> <w n="6.5">l</w>’<w n="6.6">huis</w> <w n="6.7">du</w> <w n="6.8">tombeau</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Savez</w> <w n="7.2">en</w> <w n="7.3">bref</w> <w n="7.4">faire</w> <w n="7.5">déconfiture</w></l>
						<l n="8" num="2.4"><w n="8.1">Sans</w> <w n="8.2">dégaîner</w> <w n="8.3">et</w> <w n="8.4">sans</w> <w n="8.5">monter</w> <w n="8.6">Moreau</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Vaincre</w> <w n="9.2">pour</w> <w n="9.3">vous</w> <w n="9.4">n</w>’<w n="9.5">est</w> <w n="9.6">pas</w> <w n="9.7">un</w> <w n="9.8">fait</w> <w n="9.9">nouveau</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Ne</w> <w n="10.2">gît</w>, <w n="10.3">beau</w> <w n="10.4">sire</w>, <w n="10.5">en</w> <w n="10.6">ce</w> <w n="10.7">point</w> <w n="10.8">l</w>’<w n="10.9">enclouure</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Dès</w> <w n="11.2">votre</w> <w n="11.3">avril</w>, <w n="11.4">comme</w> <w n="11.5">Hercule</w> <w n="11.6">au</w> <w n="11.7">berceau</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Mettez</w> <w n="12.2">à</w> <w n="12.3">fin</w> <w n="12.4">périlleuse</w> <w n="12.5">aventure</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">qu</w>’<w n="13.3">en</w> <w n="13.4">combat</w> <w n="13.5">où</w> <w n="13.6">rien</w> <w n="13.7">ne</w> <w n="13.8">sert</w> <w n="13.9">armure</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Où</w> <w n="14.2">rien</w> <w n="14.3">ne</w> <w n="14.4">sert</w> <w n="14.5">qu</w>’<w n="14.6">on</w> <w n="14.7">ait</w> <w n="14.8">féé</w> <w n="14.9">la</w> <w n="14.10">peau</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Ayez</w> <w n="15.2">dompté</w> <w n="15.3">qui</w> <w n="15.4">dompte</w> <w n="15.5">la</w> <w n="15.6">nature</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Onc</w> <w n="16.2">chevalier</w> <w n="16.3">ne</w> <w n="16.4">fit</w> <w n="16.5">exploit</w> <w n="16.6">si</w> <w n="16.7">beau</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ci</w> <w n="17.2">vous</w> <w n="17.3">verrons</w> <w n="17.4">encor</w> <w n="17.5">faire</w> <w n="17.6">rondeau</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Fendre</w> <w n="18.2">géans</w> <w n="18.3">du</w> <w n="18.4">chef</w> <w n="18.5">à</w> <w n="18.6">la</w> <w n="18.7">ceinture</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Faire</w> <w n="19.2">de</w> <w n="19.3">vous</w> <w n="19.4">plus</w> <w n="19.5">d</w>’<w n="19.6">un</w> <w n="19.7">vivant</w> <w n="19.8">tableau</w> :</l>
						<l n="20" num="5.4"><w n="20.1">Contre</w> <w n="20.2">vous</w>-<w n="20.3">même</w> <w n="20.4">en</w> <w n="20.5">ferais</w> <w n="20.6">la</w> <w n="20.7">gageure</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Or</w> <w n="21.2">de</w> <w n="21.3">mes</w> <w n="21.4">vœux</w>, <w n="21.5">si</w> <w n="21.6">le</w> <w n="21.7">destin</w> <w n="21.8">a</w> <w n="21.9">cure</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Point</w> <w n="22.2">n</w>’<w n="22.3">entrerez</w> <w n="22.4">dans</w> <w n="22.5">le</w> <w n="22.6">fatal</w> <w n="22.7">bateau</w></l>
						<l n="23" num="6.3"><w n="23.1">Qu</w>’<w n="23.2">un</w> <w n="23.3">siècle</w> <w n="23.4">n</w>’<w n="23.5">ait</w> <w n="23.6">accompli</w> <w n="23.7">sa</w> <w n="23.8">mesure</w> ;</l>
						<l n="24" num="6.4"><w n="24.1">Point</w> <w n="24.2">ne</w> <w n="24.3">serez</w> <w n="24.4">sans</w> <w n="24.5">amours</w>, <w n="24.6">sans</w> <w n="24.7">pipeau</w>,</l>
						<l rhyme="none" n="25" num="6.5"><space unit="char" quantity="12"></space><w n="25.1">Sans</w> <w n="25.2">dégainer</w>.</l>
					</lg>
				</div></body></text></TEI>