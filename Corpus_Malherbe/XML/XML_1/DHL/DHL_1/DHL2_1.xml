<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DHL2">
					<head type="main">Les Moutons</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">HÉLAS</w> ! <w n="1.2">petits</w> <w n="1.3">moutons</w>, <w n="1.4">que</w> <w n="1.5">vous</w> <w n="1.6">êtes</w> <w n="1.7">heureux</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Vous</w> <w n="2.2">paissez</w> <w n="2.3">dans</w> <w n="2.4">nos</w> <w n="2.5">champs</w>, <w n="2.6">sans</w> <w n="2.7">souci</w>, <w n="2.8">sans</w> <w n="2.9">alarmes</w>.</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Aussitôt</w> <w n="3.2">aimés</w> <w n="3.3">qu</w>’<w n="3.4">amoureux</w>,</l>
						<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">ne</w> <w n="4.3">vous</w> <w n="4.4">force</w> <w n="4.5">point</w> <w n="4.6">à</w> <w n="4.7">répandre</w> <w n="4.8">des</w> <w n="4.9">larmes</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Vous</w> <w n="5.2">ne</w> <w n="5.3">formez</w> <w n="5.4">jamais</w> <w n="5.5">d</w>’<w n="5.6">inutiles</w> <w n="5.7">désirs</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Dans</w> <w n="6.2">vos</w> <w n="6.3">tranquilles</w> <w n="6.4">cœurs</w> <w n="6.5">l</w>’<w n="6.6">amour</w> <w n="6.7">suit</w> <w n="6.8">la</w> <w n="6.9">nature</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Sans</w> <w n="7.2">ressentir</w> <w n="7.3">ses</w> <w n="7.4">maux</w>, <w n="7.5">vous</w> <w n="7.6">avez</w> <w n="7.7">ses</w> <w n="7.8">plaisirs</w>.</l>
						<l n="8" num="1.8"><w n="8.1">L</w>’<w n="8.2">ambition</w>, <w n="8.3">l</w>’<w n="8.4">honneur</w>, <w n="8.5">l</w>’<w n="8.6">intérêt</w>, <w n="8.7">l</w>’<w n="8.8">imposture</w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Qui</w> <w n="9.2">font</w> <w n="9.3">tant</w> <w n="9.4">de</w> <w n="9.5">maux</w> <w n="9.6">parmi</w> <w n="9.7">nous</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Ne</w> <w n="10.2">se</w> <w n="10.3">rencontrent</w> <w n="10.4">point</w> <w n="10.5">chez</w> <w n="10.6">vous</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Cependant</w> <w n="11.2">nous</w> <w n="11.3">avons</w> <w n="11.4">la</w> <w n="11.5">raison</w> <w n="11.6">pour</w> <w n="11.7">partage</w>,</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">Et</w> <w n="12.2">vous</w> <w n="12.3">en</w> <w n="12.4">ignorez</w> <w n="12.5">l</w>’<w n="12.6">usage</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Innocens</w> <w n="13.2">animaux</w>, <w n="13.3">n</w>’<w n="13.4">en</w> <w n="13.5">soyez</w> <w n="13.6">point</w> <w n="13.7">jaloux</w> ;</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">Ce</w> <w n="14.2">n</w>’<w n="14.3">est</w> <w n="14.4">pas</w> <w n="14.5">un</w> <w n="14.6">grand</w> <w n="14.7">avantage</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Cette</w> <w n="15.2">fière</w> <w n="15.3">raison</w> <w n="15.4">dont</w> <w n="15.5">on</w> <w n="15.6">fait</w> <w n="15.7">tant</w> <w n="15.8">de</w> <w n="15.9">bruit</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Contre</w> <w n="16.2">les</w> <w n="16.3">passions</w> <w n="16.4">n</w>’<w n="16.5">est</w> <w n="16.6">pas</w> <w n="16.7">un</w> <w n="16.8">sûr</w> <w n="16.9">remède</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Un</w> <w n="17.2">peu</w> <w n="17.3">de</w> <w n="17.4">vin</w> <w n="17.5">la</w> <w n="17.6">trouble</w>, <w n="17.7">un</w> <w n="17.8">enfant</w> <w n="17.9">la</w> <w n="17.10">séduit</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">déchirer</w> <w n="18.3">un</w> <w n="18.4">cœur</w> <w n="18.5">qui</w> <w n="18.6">l</w>’<w n="18.7">appelle</w> <w n="18.8">à</w> <w n="18.9">son</w> <w n="18.10">aide</w></l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Est</w> <w n="19.2">tout</w> <w n="19.3">l</w>’<w n="19.4">effet</w> <w n="19.5">qu</w>’<w n="19.6">elle</w> <w n="19.7">produit</w>.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">Toujours</w> <w n="20.2">impuissante</w> <w n="20.3">et</w> <w n="20.4">sévère</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Elle</w> <w n="21.2">s</w>’<w n="21.3">oppose</w> <w n="21.4">à</w> <w n="21.5">tout</w>, <w n="21.6">et</w> <w n="21.7">ne</w> <w n="21.8">surmonte</w> <w n="21.9">rien</w>.</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1">Sous</w> <w n="22.2">la</w> <w n="22.3">garde</w> <w n="22.4">de</w> <w n="22.5">votre</w> <w n="22.6">chien</w></l>
						<l n="23" num="1.23"><w n="23.1">Vous</w> <w n="23.2">devez</w> <w n="23.3">beaucoup</w> <w n="23.4">moins</w> <w n="23.5">redouter</w> <w n="23.6">la</w> <w n="23.7">colère</w></l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Des</w> <w n="24.2">loups</w> <w n="24.3">cruels</w> <w n="24.4">et</w> <w n="24.5">ravissans</w></l>
						<l n="25" num="1.25"><w n="25.1">Que</w> <w n="25.2">sous</w> <w n="25.3">l</w>’<w n="25.4">autorité</w> <w n="25.5">d</w>’<w n="25.6">une</w> <w n="25.7">telle</w> <w n="25.8">chimère</w></l>
						<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Nous</w> <w n="26.2">ne</w> <w n="26.3">devons</w> <w n="26.4">craindre</w> <w n="26.5">nos</w> <w n="26.6">sens</w>.</l>
						<l n="27" num="1.27"><w n="27.1">Ne</w> <w n="27.2">vaudrait</w>-<w n="27.3">il</w> <w n="27.4">pas</w> <w n="27.5">mieux</w> <w n="27.6">vivre</w> <w n="27.7">comme</w> <w n="27.8">vous</w> <w n="27.9">faites</w>,</l>
						<l n="28" num="1.28"><space unit="char" quantity="8"></space><w n="28.1">Dans</w> <w n="28.2">une</w> <w n="28.3">douce</w> <w n="28.4">oisiveté</w> ?</l>
						<l n="29" num="1.29"><w n="29.1">Ne</w> <w n="29.2">vaudrait</w>-<w n="29.3">il</w> <w n="29.4">pas</w> <w n="29.5">mieux</w> <w n="29.6">être</w> <w n="29.7">comme</w> <w n="29.8">vous</w> <w n="29.9">êtes</w>,</l>
						<l n="30" num="1.30"><space unit="char" quantity="8"></space><w n="30.1">Dans</w> <w n="30.2">une</w> <w n="30.3">heureuse</w> <w n="30.4">obscurité</w>,</l>
						<l n="31" num="1.31"><space unit="char" quantity="8"></space><w n="31.1">Que</w> <w n="31.2">d</w>’<w n="31.3">avoir</w> <w n="31.4">sans</w> <w n="31.5">tranquillité</w></l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space><w n="32.1">Des</w> <w n="32.2">richesses</w>, <w n="32.3">de</w> <w n="32.4">la</w> <w n="32.5">naissance</w>,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space><w n="33.1">De</w> <w n="33.2">l</w>’<w n="33.3">esprit</w> <w n="33.4">et</w> <w n="33.5">de</w> <w n="33.6">la</w> <w n="33.7">beauté</w> ?</l>
						<l n="34" num="1.34"><w n="34.1">Ces</w> <w n="34.2">prétendus</w> <w n="34.3">trésors</w>, <w n="34.4">dont</w> <w n="34.5">on</w> <w n="34.6">fait</w> <w n="34.7">vanité</w>,</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space><w n="35.1">Valent</w> <w n="35.2">moins</w> <w n="35.3">que</w> <w n="35.4">votre</w> <w n="35.5">indolence</w>.</l>
						<l n="36" num="1.36"><w n="36.1">Ils</w> <w n="36.2">nous</w> <w n="36.3">livrent</w> <w n="36.4">sans</w> <w n="36.5">cesse</w> <w n="36.6">à</w> <w n="36.7">des</w> <w n="36.8">soins</w> <w n="36.9">criminels</w> :</l>
						<l n="37" num="1.37"><space unit="char" quantity="8"></space><w n="37.1">Par</w> <w n="37.2">eux</w> <w n="37.3">plus</w> <w n="37.4">d</w>’<w n="37.5">un</w> <w n="37.6">remords</w> <w n="37.7">nous</w> <w n="37.8">ronge</w>.</l>
						<l n="38" num="1.38"><space unit="char" quantity="8"></space><w n="38.1">Nous</w> <w n="38.2">voulons</w> <w n="38.3">les</w> <w n="38.4">rendre</w> <w n="38.5">éternels</w>,</l>
						<l n="39" num="1.39"><w n="39.1">Sans</w> <w n="39.2">songer</w> <w n="39.3">qu</w>’<w n="39.4">eux</w> <w n="39.5">et</w> <w n="39.6">nous</w> <w n="39.7">passerons</w> <w n="39.8">comme</w> <w n="39.9">un</w> <w n="39.10">songe</w>.</l>
						<l n="40" num="1.40"><space unit="char" quantity="8"></space><w n="40.1">Il</w> <w n="40.2">n</w>’<w n="40.3">est</w> <w n="40.4">dans</w> <w n="40.5">ce</w> <w n="40.6">vaste</w> <w n="40.7">univers</w></l>
						<l n="41" num="1.41"><space unit="char" quantity="8"></space><w n="41.1">Rien</w> <w n="41.2">d</w>’<w n="41.3">assuré</w>, <w n="41.4">rien</w> <w n="41.5">de</w> <w n="41.6">solide</w> ;</l>
						<l n="42" num="1.42"><w n="42.1">Des</w> <w n="42.2">choses</w> <w n="42.3">ici</w>-<w n="42.4">bas</w> <w n="42.5">la</w> <w n="42.6">fortune</w> <w n="42.7">décide</w></l>
						<l n="43" num="1.43"><space unit="char" quantity="8"></space><w n="43.1">Selon</w> <w n="43.2">ses</w> <w n="43.3">caprices</w> <w n="43.4">divers</w> :</l>
						<l n="44" num="1.44"><space unit="char" quantity="8"></space><w n="44.1">Tout</w> <w n="44.2">l</w>’<w n="44.3">effort</w> <w n="44.4">de</w> <w n="44.5">notre</w> <w n="44.6">prudence</w></l>
						<l n="45" num="1.45"><w n="45.1">Ne</w> <w n="45.2">peut</w> <w n="45.3">nous</w> <w n="45.4">dérober</w> <w n="45.5">au</w> <w n="45.6">moindre</w> <w n="45.7">de</w> <w n="45.8">ses</w> <w n="45.9">coups</w>.</l>
						<l n="46" num="1.46"><w n="46.1">Paissez</w>, <w n="46.2">moutons</w>, <w n="46.3">paissez</w> <w n="46.4">sans</w> <w n="46.5">règle</w> <w n="46.6">et</w> <w n="46.7">sans</w> <w n="46.8">science</w> ;</l>
						<l n="47" num="1.47"><space unit="char" quantity="8"></space><w n="47.1">Malgré</w> <w n="47.2">la</w> <w n="47.3">trompeuse</w> <w n="47.4">apparence</w>,</l>
						<l n="48" num="1.48"><w n="48.1">Vous</w> <w n="48.2">êtes</w> <w n="48.3">plus</w> <w n="48.4">heureux</w> <w n="48.5">et</w> <w n="48.6">plus</w> <w n="48.7">sages</w> <w n="48.8">que</w> <w n="48.9">nous</w>.</l>
					</lg>
				</div></body></text></TEI>