<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><div type="poem" key="DHL63">
				<head type="main">RÉPONSE</head>
				<head type="sub">DE GRISETTE À TATA</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">COMMENT</w> <w n="1.2">osez</w>-<w n="1.3">vous</w> <w n="1.4">me</w> <w n="1.5">conter</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Les</w> <w n="2.2">pertes</w> <w n="2.3">que</w> <w n="2.4">vous</w> <w n="2.5">avez</w> <w n="2.6">faites</w> ?</l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">En</w> <w n="3.2">amour</w> <w n="3.3">c</w>’<w n="3.4">est</w> <w n="3.5">mal</w> <w n="3.6">débuter</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">je</w> <w n="4.3">ne</w> <w n="4.4">sais</w> <w n="4.5">que</w> <w n="4.6">moi</w> <w n="4.7">qui</w> <w n="4.8">voulût</w> <w n="4.9">écouter</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Un</w> <w n="5.2">pareil</w> <w n="5.3">conteur</w> <w n="5.4">de</w> <w n="5.5">fleurettes</w>.</l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Ah</w> <w n="6.2">fi</w> ! <w n="6.3">diraient</w> <w n="6.4">nonchalamment</w></l>
					<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Un</w> <w n="7.2">tas</w> <w n="7.3">de</w> <w n="7.4">chattes</w> <w n="7.5">précieuses</w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Fi</w> ! <w n="8.2">mes</w> <w n="8.3">chères</w>, <w n="8.4">d</w>’<w n="8.5">un</w> <w n="8.6">tel</w> <w n="8.7">amant</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Car</w>, <w n="9.2">si</w> <w n="9.3">j</w>’<w n="9.4">ose</w>, <w n="9.5">Tata</w>, <w n="9.6">vous</w> <w n="9.7">parler</w> <w n="9.8">librement</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Chattes</w> <w n="10.2">aux</w> <w n="10.3">airs</w> <w n="10.4">penchés</w> <w n="10.5">sont</w> <w n="10.6">les</w> <w n="10.7">plus</w> <w n="10.8">amoureuses</w> ;</l>
					<l n="11" num="1.11"><space unit="char" quantity="8"></space><w n="11.1">Malheur</w> <w n="11.2">chez</w> <w n="11.3">elles</w> <w n="11.4">aux</w> <w n="11.5">matous</w></l>
					<l n="12" num="1.12"><space unit="char" quantity="8"></space><w n="12.1">Aussi</w> <w n="12.2">disgraciés</w> <w n="12.3">que</w> <w n="12.4">vous</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Pour</w> <w n="13.2">moi</w> <w n="13.3">qu</w>’<w n="13.4">un</w> <w n="13.5">heureux</w> <w n="13.6">sort</w> <w n="13.7">fit</w> <w n="13.8">naître</w> <w n="13.9">tendre</w> <w n="13.10">et</w> <w n="13.11">sage</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Je</w> <w n="14.2">vous</w> <w n="14.3">quitte</w> <w n="14.4">aisément</w> <w n="14.5">des</w> <w n="14.6">solides</w> <w n="14.7">plaisirs</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Faisons</w> <w n="15.2">de</w> <w n="15.3">notre</w> <w n="15.4">amour</w> <w n="15.5">un</w> <w n="15.6">plus</w> <w n="15.7">galant</w> <w n="15.8">usage</w>,</l>
					<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">Il</w> <w n="16.2">est</w> <w n="16.3">un</w> <w n="16.4">charmant</w> <w n="16.5">badinage</w></l>
					<l n="17" num="1.17"><w n="17.1">Qui</w> <w n="17.2">ne</w> <w n="17.3">tarit</w> <w n="17.4">jamais</w> <w n="17.5">la</w> <w n="17.6">source</w> <w n="17.7">des</w> <w n="17.8">désirs</w>.</l>
					<l n="18" num="1.18"><w n="18.1">Je</w> <w n="18.2">renonce</w> <w n="18.3">pour</w> <w n="18.4">vous</w> <w n="18.5">à</w> <w n="18.6">toutes</w> <w n="18.7">les</w> <w n="18.8">gouttières</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Où</w>, <w n="19.2">soit</w> <w n="19.3">dit</w> <w n="19.4">en</w> <w n="19.5">passant</w>, <w n="19.6">je</w> <w n="19.7">n</w>’<w n="19.8">ai</w> <w n="19.9">jamais</w> <w n="19.10">été</w> ;</l>
					<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">Je</w> <w n="20.2">suis</w> <w n="20.3">de</w> <w n="20.4">ces</w> <w n="20.5">minettes</w> <w n="20.6">fières</w></l>
					<l n="21" num="1.21"><w n="21.1">Qui</w> <w n="21.2">donnent</w> <w n="21.3">aux</w> <w n="21.4">grands</w> <w n="21.5">airs</w>, <w n="21.6">aux</w> <w n="21.7">galantes</w> <w n="21.8">manières</w>.</l>
					<l n="22" num="1.22"><w n="22.1">Hélas</w> ! <w n="22.2">ce</w> <w n="22.3">fut</w> <w n="22.4">par</w>-<w n="22.5">là</w> <w n="22.6">que</w> <w n="22.7">mon</w> <w n="22.8">cœur</w> <w n="22.9">fut</w> <w n="22.10">tenté</w></l>
					<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">Quand</w> <w n="23.2">j</w>’<w n="23.3">appris</w> <w n="23.4">ce</w> <w n="23.5">qu</w>’<w n="23.6">avait</w> <w n="23.7">conté</w>,</l>
					<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">De</w> <w n="24.2">vos</w> <w n="24.3">appas</w>, <w n="24.4">de</w> <w n="24.5">votre</w> <w n="24.6">adresse</w>,</l>
					<l n="25" num="1.25"><space unit="char" quantity="8"></space><w n="25.1">Votre</w> <w n="25.2">incomparable</w> <w n="25.3">maîtresse</w>.</l>
					<l n="26" num="1.26"><space unit="char" quantity="8"></space><w n="26.1">Depuis</w> <w n="26.2">ce</w> <w n="26.3">dangereux</w> <w n="26.4">moment</w>,</l>
					<l n="27" num="1.27"><space unit="char" quantity="4"></space><w n="27.1">Pleine</w> <w n="27.2">de</w> <w n="27.3">vous</w> <w n="27.4">autant</w> <w n="27.5">qu</w>’<w n="27.6">on</w> <w n="27.7">le</w> <w n="27.8">peut</w> <w n="27.9">être</w>,</l>
					<l n="28" num="1.28"><space unit="char" quantity="4"></space><w n="28.1">Je</w> <w n="28.2">fis</w> <w n="28.3">dessein</w> <w n="28.4">de</w> <w n="28.5">vous</w> <w n="28.6">faire</w> <w n="28.7">connaître</w></l>
					<l n="29" num="1.29"><space unit="char" quantity="8"></space><w n="29.1">Par</w> <w n="29.2">un</w> <w n="29.3">doucereux</w> <w n="29.4">compliment</w></l>
					<l n="30" num="1.30"><w n="30.1">L</w>’<w n="30.2">amour</w> <w n="30.3">que</w> <w n="30.4">dans</w> <w n="30.5">mon</w> <w n="30.6">cœur</w> <w n="30.7">ce</w> <w n="30.8">récit</w> <w n="30.9">a</w> <w n="30.10">fait</w> <w n="30.11">naître</w>.</l>
					<l n="31" num="1.31"><w n="31.1">Vous</w> <w n="31.2">m</w>’<w n="31.3">avez</w> <w n="31.4">confirmé</w> <w n="31.5">par</w> <w n="31.6">d</w>’<w n="31.7">agréables</w> <w n="31.8">vers</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Tout</w> <w n="32.2">ce</w> <w n="32.3">qu</w>’<w n="32.4">on</w> <w n="32.5">m</w>’<w n="32.6">avait</w> <w n="32.7">dit</w> <w n="32.8">de</w> <w n="32.9">vos</w> <w n="32.10">talens</w> <w n="32.11">divers</w> ;</l>
					<l n="33" num="1.33"><space unit="char" quantity="8"></space><w n="33.1">Malgré</w> <w n="33.2">votre</w> <w n="33.3">juste</w> <w n="33.4">tristesse</w>,</l>
					<l n="34" num="1.34"><w n="34.1">On</w> <w n="34.2">y</w> <w n="34.3">voit</w>, <w n="34.4">cher</w> <w n="34.5">Tata</w>, <w n="34.6">briller</w> <w n="34.7">un</w> <w n="34.8">air</w> <w n="34.9">galant</w> ;</l>
					<l n="35" num="1.35"><w n="35.1">Les</w> <w n="35.2">miens</w> <w n="35.3">répondront</w> <w n="35.4">mal</w> <w n="35.5">à</w> <w n="35.6">leur</w> <w n="35.7">délicatesse</w>,</l>
					<l n="36" num="1.36"><space unit="char" quantity="4"></space><w n="36.1">Écrire</w> <w n="36.2">bien</w> <w n="36.3">n</w>’<w n="36.4">est</w> <w n="36.5">pas</w> <w n="36.6">notre</w> <w n="36.7">talent</w> ;</l>
					<l n="37" num="1.37"><w n="37.1">Il</w> <w n="37.2">est</w> <w n="37.3">rare</w>, <w n="37.4">dit</w>-<w n="37.5">on</w>, <w n="37.6">parmi</w> <w n="37.7">les</w> <w n="37.8">hommes</w> <w n="37.9">même</w>.</l>
					<l n="38" num="1.38"><space unit="char" quantity="8"></space><w n="38.1">Mais</w> <w n="38.2">de</w> <w n="38.3">quoi</w> <w n="38.4">vais</w>-<w n="38.5">je</w> <w n="38.6">m</w>’<w n="38.7">alarmer</w> ?</l>
					<l n="39" num="1.39"><space unit="char" quantity="8"></space><w n="39.1">Vous</w> <w n="39.2">y</w> <w n="39.3">verrez</w> <w n="39.4">que</w> <w n="39.5">je</w> <w n="39.6">vous</w> <w n="39.7">aime</w>,</l>
					<l n="40" num="1.40"><space unit="char" quantity="8"></space><w n="40.1">C</w>’<w n="40.2">est</w> <w n="40.3">assez</w> <w n="40.4">pour</w> <w n="40.5">qui</w> <w n="40.6">sait</w> <w n="40.7">aimer</w>.</l>
				</lg>
			</div></body></text></TEI>