<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">BALLADES</head><div type="poem" key="DHL46">
					<head type="main">Ballade</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">À</w> <w n="1.2">CAUTION</w> <w n="1.3">tous</w> <w n="1.4">amans</w> <w n="1.5">sont</w> <w n="1.6">sujets</w> :</l>
						<l n="2" num="1.2"><w n="2.1">Cette</w> <w n="2.2">maxime</w> <w n="2.3">en</w> <w n="2.4">ma</w> <w n="2.5">tête</w> <w n="2.6">est</w> <w n="2.7">écrite</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Point</w> <w n="3.2">n</w>’<w n="3.3">ai</w> <w n="3.4">de</w> <w n="3.5">foi</w> <w n="3.6">pour</w> <w n="3.7">leurs</w> <w n="3.8">tourmens</w> <w n="3.9">secrets</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Point</w> <w n="4.2">auprès</w> <w n="4.3">d</w>’<w n="4.4">eux</w> <w n="4.5">n</w>’<w n="4.6">ai</w> <w n="4.7">besoin</w> <w n="4.8">d</w>’<w n="4.9">eau</w> <w n="4.10">bénite</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Dans</w> <w n="5.2">cœur</w> <w n="5.3">humain</w> <w n="5.4">probité</w> <w n="5.5">plus</w> <w n="5.6">n</w>’<w n="5.7">habite</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Trop</w> <w n="6.2">bien</w> <w n="6.3">encore</w> <w n="6.4">a</w>-<w n="6.5">t</w>-<w n="6.6">on</w> <w n="6.7">les</w> <w n="6.8">mêmes</w> <w n="6.9">dits</w></l>
						<l n="7" num="1.7"><w n="7.1">Qu</w>’<w n="7.2">avant</w> <w n="7.3">qu</w>’<w n="7.4">Astuce</w> <w n="7.5">au</w> <w n="7.6">monde</w> <w n="7.7">fût</w> <w n="7.8">venue</w> :</l>
						<l n="8" num="1.8"><w n="8.1">Mais</w> <w n="8.2">pour</w> <w n="8.3">d</w>’<w n="8.4">effets</w>, <w n="8.5">la</w> <w n="8.6">mode</w> <w n="8.7">en</w> <w n="8.8">est</w> <w n="8.9">perdue</w>.</l>
						<l n="9" num="1.9"><w n="9.1">On</w> <w n="9.2">n</w>’<w n="9.3">aime</w> <w n="9.4">plus</w> <w n="9.5">comme</w> <w n="9.6">on</w> <w n="9.7">aimait</w> <w n="9.8">jadis</w>.</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1"><w n="10.1">Riches</w> <w n="10.2">atours</w>, <w n="10.3">tables</w>, <w n="10.4">nombreux</w> <w n="10.5">valets</w>,</l>
						<l n="11" num="2.2"><w n="11.1">Font</w> <w n="11.2">aujourd</w>’<w n="11.3">hui</w> <w n="11.4">les</w> <w n="11.5">trois</w> <w n="11.6">quarts</w> <w n="11.7">du</w> <w n="11.8">mérite</w>.</l>
						<l n="12" num="2.3"><w n="12.1">Si</w> <w n="12.2">des</w> <w n="12.3">amans</w> <w n="12.4">soumis</w>, <w n="12.5">constans</w>, <w n="12.6">discrets</w></l>
						<l n="13" num="2.4"><w n="13.1">Il</w> <w n="13.2">est</w> <choice hand="RR" type="false verse" reason="analysis"><sic>encore</sic><corr source="édition de 1768"><w n="13.3">encor</w></corr></choice>, <w n="13.4">la</w> <w n="13.5">troupe</w> <w n="13.6">en</w> <w n="13.7">est</w> <w n="13.8">petite</w>.</l>
						<l n="14" num="2.5"><w n="14.1">Amour</w> <w n="14.2">d</w>’<w n="14.3">un</w> <w n="14.4">mois</w> <w n="14.5">est</w> <w n="14.6">amour</w> <w n="14.7">décrépite</w>.</l>
						<l n="15" num="2.6"><w n="15.1">Amans</w> <w n="15.2">brutaux</w> <w n="15.3">sont</w> <w n="15.4">les</w> <w n="15.5">plus</w> <w n="15.6">applaudis</w>.</l>
						<l n="16" num="2.7"><w n="16.1">Soupirs</w> <w n="16.2">et</w> <w n="16.3">pleurs</w> <w n="16.4">feraient</w> <w n="16.5">passer</w> <w n="16.6">pour</w> <w n="16.7">grue</w>.</l>
						<l n="17" num="2.8"><w n="17.1">Faveur</w> <w n="17.2">est</w> <w n="17.3">dite</w> <w n="17.4">aussitôt</w> <w n="17.5">qu</w>’<w n="17.6">obtenue</w>.</l>
						<l n="18" num="2.9"><w n="18.1">On</w> <w n="18.2">n</w>’<w n="18.3">aime</w> <w n="18.4">plus</w> <w n="18.5">comme</w> <w n="18.6">on</w> <w n="18.7">aimait</w> <w n="18.8">jadis</w>.</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1"><w n="19.1">Jeunes</w> <w n="19.2">beautés</w> <w n="19.3">en</w> <w n="19.4">vain</w> <w n="19.5">tendent</w> <w n="19.6">filets</w> ;</l>
						<l n="20" num="3.2"><w n="20.1">Les</w> <w n="20.2">jouvenceaux</w>, <w n="20.3">cette</w> <w n="20.4">engeance</w> <w n="20.5">maudite</w>,</l>
						<l n="21" num="3.3"><w n="21.1">Fait</w> <w n="21.2">bande</w> <w n="21.3">à</w> <w n="21.4">part</w> ; <w n="21.5">près</w> <w n="21.6">des</w> <w n="21.7">plus</w> <w n="21.8">doux</w> <w n="21.9">objets</w></l>
						<l n="22" num="3.4"><w n="22.1">D</w>’<w n="22.2">être</w> <w n="22.3">indolent</w> <w n="22.4">chacun</w> <w n="22.5">se</w> <w n="22.6">félicite</w>.</l>
						<l n="23" num="3.5"><w n="23.1">Nul</w> <w n="23.2">en</w> <w n="23.3">amour</w> <w n="23.4">ne</w> <w n="23.5">daigne</w> <w n="23.6">être</w> <w n="23.7">hypocrite</w> ;</l>
						<l n="24" num="3.6"><w n="24.1">Ou</w> <w n="24.2">si</w> <w n="24.3">parfois</w> <w n="24.4">un</w> <w n="24.5">de</w> <w n="24.6">ces</w> <w n="24.7">étourdis</w></l>
						<l n="25" num="3.7"><w n="25.1">À</w> <w n="25.2">quelques</w> <w n="25.3">soins</w> <w n="25.4">s</w>’<w n="25.5">abaisse</w> <w n="25.6">et</w> <w n="25.7">s</w>’<w n="25.8">habitue</w>,</l>
						<l n="26" num="3.8"><w n="26.1">Don</w> <w n="26.2">de</w> <w n="26.3">merci</w> <w n="26.4">seul</w> <w n="26.5">il</w> <w n="26.6">n</w>’<w n="26.7">a</w> <w n="26.8">pas</w> <w n="26.9">en</w> <w n="26.10">vue</w>.</l>
						<l n="27" num="3.9"><w n="27.1">On</w> <w n="27.2">n</w>’<w n="27.3">aime</w> <w n="27.4">plus</w> <w n="27.5">comme</w> <w n="27.6">on</w> <w n="27.7">aimait</w> <w n="27.8">jadis</w>.</l>
					</lg>
					<lg n="4">
						<l n="28" num="4.1"><w n="28.1">Tous</w> <w n="28.2">jeunes</w> <w n="28.3">cœurs</w> <w n="28.4">se</w> <w n="28.5">trouvent</w> <w n="28.6">ainsi</w> <w n="28.7">faits</w>.</l>
						<l n="29" num="4.2"><w n="29.1">Telle</w> <w n="29.2">denrée</w> <w n="29.3">aux</w> <w n="29.4">folles</w> <w n="29.5">se</w> <w n="29.6">débite</w>.</l>
						<l n="30" num="4.3"><w n="30.1">Cœurs</w> <w n="30.2">de</w> <w n="30.3">barbons</w> <w n="30.4">sont</w> <w n="30.5">un</w> <w n="30.6">peu</w> <w n="30.7">moins</w> <w n="30.8">coquets</w> ;</l>
						<l n="31" num="4.4"><w n="31.1">Quand</w> <w n="31.2">il</w> <w n="31.3">fut</w> <w n="31.4">vieux</w> <w n="31.5">le</w> <w n="31.6">diable</w> <w n="31.7">fut</w> <w n="31.8">ermite</w> ;</l>
						<l n="32" num="4.5"><w n="32.1">Mais</w> <w n="32.2">rien</w> <w n="32.3">chez</w> <w n="32.4">eux</w> <w n="32.5">à</w> <w n="32.6">tendresse</w> <w n="32.7">n</w>’<w n="32.8">invite</w>.</l>
						<l n="33" num="4.6"><w n="33.1">Par</w> <w n="33.2">maints</w> <w n="33.3">hivers</w> <w n="33.4">désirs</w> <w n="33.5">sont</w> <w n="33.6">refroidis</w>.</l>
						<l n="34" num="4.7"><w n="34.1">Par</w> <w n="34.2">maux</w> <w n="34.3">fréquens</w> <w n="34.4">humeur</w> <w n="34.5">devient</w> <w n="34.6">bourrue</w></l>
						<l n="35" num="4.8"><w n="35.1">Quand</w> <w n="35.2">une</w> <w n="35.3">fois</w> <w n="35.4">on</w> <w n="35.5">a</w> <w n="35.6">tête</w> <w n="35.7">chenue</w>.</l>
						<l n="36" num="4.9"><w n="36.1">On</w> <w n="36.2">n</w>’<w n="36.3">aime</w> <w n="36.4">plus</w> <w n="36.5">comme</w> <w n="36.6">on</w> <w n="36.7">aimait</w> <w n="36.8">jadis</w>.</l>
					</lg>
					<lg n="5">
						<head type="main">ENVOI</head>
						<l n="37" num="5.1"><w n="37.1">Fils</w> <w n="37.2">de</w> <w n="37.3">Vénus</w>, <w n="37.4">songe</w> <w n="37.5">à</w> <w n="37.6">tes</w> <w n="37.7">intérêts</w> ;</l>
						<l n="38" num="5.2"><w n="38.1">Je</w> <w n="38.2">vois</w> <w n="38.3">changer</w> <w n="38.4">l</w>’<w n="38.5">encens</w> <w n="38.6">en</w> <w n="38.7">camouflets</w> :</l>
						<l n="39" num="5.3"><w n="39.1">Tout</w> <w n="39.2">est</w> <w n="39.3">perdu</w> <w n="39.4">si</w> <w n="39.5">ce</w> <w n="39.6">train</w> <w n="39.7">continue</w>.</l>
						<l n="40" num="5.4"><w n="40.1">Ramène</w>-<w n="40.2">nous</w> <w n="40.3">le</w> <w n="40.4">siècle</w> <w n="40.5">d</w>’<w n="40.6">Amadis</w>.</l>
						<l n="41" num="5.5"><w n="41.1">Il</w> <w n="41.2">t</w>’<w n="41.3">est</w> <w n="41.4">honteux</w> <w n="41.5">qu</w>’<w n="41.6">en</w> <w n="41.7">cour</w> <w n="41.8">d</w>’<w n="41.9">attraits</w> <w n="41.10">pourvue</w>,</l>
						<l n="42" num="5.6"><w n="42.1">Où</w> <w n="42.2">politesse</w> <w n="42.3">au</w> <w n="42.4">comble</w> <w n="42.5">est</w> <w n="42.6">parvenue</w>,</l>
						<l n="43" num="5.7"><w n="43.1">On</w> <w n="43.2">n</w>’<w n="43.3">aime</w> <w n="43.4">plus</w> <w n="43.5">comme</w> <w n="43.6">on</w> <w n="43.7">aimait</w> <w n="43.8">jadis</w>.</l>
					</lg>
				</div></body></text></TEI>