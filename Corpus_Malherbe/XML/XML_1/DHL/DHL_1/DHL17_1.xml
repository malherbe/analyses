<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">STANCES</head><div type="poem" key="DHL17">
					<head type="main">Stances</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">HÉ</w> ! <w n="1.2">que</w> <w n="1.3">te</w> <w n="1.4">sert</w>, <w n="1.5">Amour</w>, <w n="1.6">de</w> <w n="1.7">me</w> <w n="1.8">lancer</w> <w n="1.9">des</w> <w n="1.10">traits</w>,</l>
						<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2">ai</w>-<w n="2.3">je</w> <w n="2.4">pas</w> <w n="2.5">reconnu</w> <w n="2.6">ta</w> <w n="2.7">fatale</w> <w n="2.8">puissance</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Ne</w> <w n="3.2">te</w> <w n="3.3">souvient</w>-<w n="3.4">il</w> <w n="3.5">plus</w> <w n="3.6">des</w> <w n="3.7">maux</w> <w n="3.8">que</w> <w n="3.9">tu</w> <w n="3.10">m</w>’<w n="3.11">as</w> <w n="3.12">faits</w> ?</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Laisse</w>-<w n="4.2">moi</w> <w n="4.3">dans</w> <w n="4.4">l</w>’<w n="4.5">indifférence</w>,</l>
						<l n="5" num="1.5"><w n="5.1">À</w> <w n="5.2">l</w>’<w n="5.3">ombre</w> <w n="5.4">des</w> <w n="5.5">ormeaux</w>, <w n="5.6">vivre</w> <w n="5.7">et</w> <w n="5.8">mourir</w> <w n="5.9">en</w> <w n="5.10">paix</w>.</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Souvent</w> <w n="6.2">dans</w> <w n="6.3">nos</w> <w n="6.4">plaines</w> <w n="6.5">fleuries</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">mêle</w> <w n="7.3">avec</w> <w n="7.4">plaisir</w> <w n="7.5">mes</w> <w n="7.6">soupirs</w> <w n="7.7">à</w> <w n="7.8">mes</w> <w n="7.9">pleurs</w>.</l>
						<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">chant</w> <w n="8.3">des</w> <w n="8.4">rossignols</w>, <w n="8.5">les</w> <w n="8.6">déserts</w> <w n="8.7">enchanteurs</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Le</w> <w n="9.2">murmure</w> <w n="9.3">des</w> <w n="9.4">eaux</w> <w n="9.5">et</w> <w n="9.6">l</w>’<w n="9.7">émail</w> <w n="9.8">des</w> <w n="9.9">prairies</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Mon</w> <w n="10.2">chien</w> <w n="10.3">sensible</w> <w n="10.4">à</w> <w n="10.5">mes</w> <w n="10.6">douleurs</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Mes</w> <w n="11.2">troupeaux</w> <w n="11.3">languissans</w>, <w n="11.4">ces</w> <w n="11.5">guirlandes</w> <w n="11.6">de</w> <w n="11.7">fleurs</w></l>
						<l n="12" num="1.12"><w n="12.1">Que</w> <w n="12.2">le</w> <w n="12.3">temps</w>, <w n="12.4">mes</w> <w n="12.5">soupirs</w> <w n="12.6">et</w> <w n="12.7">mes</w> <w n="12.8">pleurs</w> <w n="12.9">ont</w> <w n="12.10">flétries</w></l>
						<l n="13" num="1.13"><w n="13.1">Don</w> <w n="13.2">cher</w> <w n="13.3">et</w> <w n="13.4">précieux</w> <w n="13.5">du</w> <w n="13.6">plus</w> <w n="13.7">beau</w> <w n="13.8">des</w> <w n="13.9">pasteurs</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Tout</w> <w n="14.2">nourrit</w> <w n="14.3">avec</w> <w n="14.4">soin</w> <w n="14.5">mes</w> <w n="14.6">tendres</w> <w n="14.7">rêveries</w>.</l>
					</lg>
					<lg n="2">
						<l n="15" num="2.1"><w n="15.1">Éloigne</w>-<w n="15.2">toi</w>, <w n="15.3">cruel</w>, <w n="15.4">de</w> <w n="15.5">ces</w> <w n="15.6">lieux</w> <w n="15.7">fortunés</w>,</l>
						<l n="16" num="2.2"><space unit="char" quantity="8"></space><w n="16.1">La</w> <w n="16.2">paix</w> <w n="16.3">y</w> <w n="16.4">règne</w> <w n="16.5">en</w> <w n="16.6">ton</w> <w n="16.7">absence</w> :</l>
						<l n="17" num="2.3"><space unit="char" quantity="8"></space><w n="17.1">Ne</w> <w n="17.2">trouble</w> <w n="17.3">plus</w> <w n="17.4">par</w> <w n="17.5">ta</w> <w n="17.6">présence</w></l>
						<l n="18" num="2.4"><w n="18.1">Les</w> <w n="18.2">funestes</w> <w n="18.3">plaisirs</w> <w n="18.4">qui</w> <w n="18.5">me</w> <w n="18.6">sont</w> <w n="18.7">destinés</w>.</l>
						<l n="19" num="2.5"><w n="19.1">Rassemble</w> <w n="19.2">en</w> <w n="19.3">d</w>’<w n="19.4">autres</w> <w n="19.5">lieux</w> <w n="19.6">tes</w> <w n="19.7">attraits</w> <w n="19.8">et</w> <w n="19.9">tes</w> <w n="19.10">charmes</w> ;</l>
						<l n="20" num="2.6"><space unit="char" quantity="8"></space><w n="20.1">Mon</w> <w n="20.2">cœur</w> <w n="20.3">n</w>’<w n="20.4">en</w> <w n="20.5">sera</w> <w n="20.6">point</w> <w n="20.7">jaloux</w>.</l>
						<l n="21" num="2.7"><w n="21.1">Non</w>, <w n="21.2">je</w> <w n="21.3">n</w>’<w n="21.4">enivrai</w> <w n="21.5">point</w> <w n="21.6">ces</w> <w n="21.7">secrètes</w> <w n="21.8">alarmes</w></l>
						<l n="22" num="2.8"><w n="22.1">Dont</w> <w n="22.2">tu</w> <w n="22.3">rends</w> <w n="22.4">quand</w> <w n="22.5">tu</w> <w n="22.6">veux</w> <w n="22.7">le</w> <w n="22.8">souvenir</w> <w n="22.9">si</w> <w n="22.10">doux</w>.</l>
						<l n="23" num="2.9"><w n="23.1">Mon</w> <w n="23.2">chien</w> <w n="23.3">et</w> <w n="23.4">mes</w> <w n="23.5">moutons</w>, <w n="23.6">chers</w> <w n="23.7">témoins</w> <w n="23.8">de</w> <w n="23.9">mes</w> <w n="23.10">larmes</w></l>
						<l n="24" num="2.10"><w n="24.1">J</w>’<w n="24.2">en</w> <w n="24.3">atteste</w> <w n="24.4">les</w> <w n="24.5">dieux</w>, <w n="24.6">je</w> <w n="24.7">n</w>’<w n="24.8">aimerai</w> <w n="24.9">que</w> <w n="24.10">vous</w>.</l>
					</lg>
				</div></body></text></TEI>