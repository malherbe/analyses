<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">MADRIGAUX</head><div type="poem" key="DHL51">
					<head type="main">Pour Monsieur Doujat,</head>
					<head type="main">DOYEN DU PARLEMENT</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">D</w>’<w n="1.2">UN</w> <w n="1.3">madrigal</w> <w n="1.4">on</w> <w n="1.5">veut</w> <w n="1.6">que</w> <w n="1.7">je</w> <w n="1.8">régale</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Un</w> <w n="2.2">magistrat</w> <w n="2.3">favori</w> <w n="2.4">de</w> <w n="2.5">Thémis</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">pour</w> <w n="3.3">le</w> <w n="3.4">bien</w> <w n="3.5">louer</w> <w n="3.6">ma</w> <w n="3.7">peine</w> <w n="3.8">est</w> <w n="3.9">sans</w> <w n="3.10">égale</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">magistrat</w> <w n="4.3">pourtant</w> <w n="4.4">est</w> <w n="4.5">fort</w> <w n="4.6">de</w> <w n="4.7">mes</w> <w n="4.8">amis</w> ;</l>
						<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">De</w> <w n="5.2">tous</w> <w n="5.3">les</w> <w n="5.4">temps</w> <w n="5.5">je</w> <w n="5.6">l</w>’<w n="5.7">appelle</w> <w n="5.8">mon</w> <w n="5.9">père</w>.</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">S</w>’<w n="6.2">il</w> <w n="6.3">l</w>’<w n="6.4">est</w> <w n="6.5">au</w> <w n="6.6">vrai</w> <w n="6.7">je</w> <w n="6.8">n</w>’<w n="6.9">en</w> <w n="6.10">sais</w> <w n="6.11">rien</w>.</l>
						<l n="7" num="1.7"><space unit="char" quantity="4"></space><w n="7.1">Ce</w> <w n="7.2">que</w> <w n="7.3">je</w> <w n="7.4">sais</w> <w n="7.5">c</w>’<w n="7.6">est</w> <w n="7.7">qu</w>’<w n="7.8">il</w> <w n="7.9">aimait</w> <w n="7.10">ma</w> <w n="7.11">mère</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="4"></space><w n="8.1">Et</w> <w n="8.2">que</w> <w n="8.3">ma</w> <w n="8.4">mère</w> <w n="8.5">était</w> <w n="8.6">femme</w> <w n="8.7">de</w> <w n="8.8">bien</w>.</l>
					</lg>
				</div></body></text></TEI>