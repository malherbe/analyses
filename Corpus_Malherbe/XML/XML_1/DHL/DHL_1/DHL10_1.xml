<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Madame Deshoulières</title>
				<title type="medium">Édition électronique</title>
				<author key="DHL">
					<name>
						<forename>Antoinette</forename>
						<surname>DESHOULIÈRES</surname>
					</name>
					<date from="1638" to="1694">1638-1694</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Maherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4026 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2022">2022</date>
				<idno type="local">DHL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Madame Deshoulières</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Madame_Deshouli%C3%A8res</idno>
						<p>Exporté de Wikisource le 24 juillet 2022</p>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Antoinette Deshoulières</author>
								<idno type="URL">https://gallica.bnf.fr/ark:/12148/bpt6k886829s</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>THÉOPHILE BERQUET, LIBRAIRE</publisher>
									<date when="1824">1824</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee01deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">I</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de madame et de mademoiselle Deshoulieres</title>
						<author>Antoinette Deshoulières</author>
						<idno type="URL">https://archive.org/details/oeuvresdemadamee02deshuoft/page/n5/mode/2up</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher> Libraires associés</publisher>
							<date when="1768">1768</date>
						</imprint>
						<biblScope unit="tome">II</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1688">1688</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>

			<samplingDecl>
				<p>La présente édition électronique est issue d’une exportation à partir de Wikisource.org (Exporté de Wikisource le 26 juillet 2022).</p>
				<p>La partie "Autobiographie" n’est pas incluse dans la présente édition électronique.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DHL10">
					<head type="main">À ses Enfans</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">DANS</w> <w n="1.2">ces</w> <w n="1.3">prés</w> <w n="1.4">fleuris</w></l>
						<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">arrose</w> <w n="2.3">la</w> <w n="2.4">Seine</w></l>
						<l n="3" num="1.3"><w n="3.1">Cherchez</w> <w n="3.2">qui</w> <w n="3.3">vous</w> <w n="3.4">mène</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Mes</w> <w n="4.2">chères</w> <w n="4.3">brebis</w>.</l>
						<l n="5" num="1.5"><w n="5.1">J</w>’<w n="5.2">ai</w> <w n="5.3">fait</w>, <w n="5.4">pour</w> <w n="5.5">vous</w> <w n="5.6">rendre</w></l>
						<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">destin</w> <w n="6.3">plus</w> <w n="6.4">doux</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Ce</w> <w n="7.2">qu</w>’<w n="7.3">on</w> <w n="7.4">peut</w> <w n="7.5">attendre</w></l>
						<l n="8" num="1.8"><w n="8.1">D</w>’<w n="8.2">une</w> <w n="8.3">amitié</w> <w n="8.4">tendre</w> ;</l>
						<l n="9" num="1.9"><w n="9.1">Mais</w> <w n="9.2">son</w> <w n="9.3">long</w> <w n="9.4">courroux</w></l>
						<l n="10" num="1.10"><w n="10.1">Détruit</w>, <w n="10.2">empoisonne</w></l>
						<l n="11" num="1.11"><w n="11.1">Tous</w> <w n="11.2">mes</w> <w n="11.3">soins</w> <w n="11.4">pour</w> <w n="11.5">vous</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">vous</w> <w n="12.3">abandonne</w></l>
						<l n="13" num="1.13"><w n="13.1">Aux</w> <w n="13.2">fureurs</w> <w n="13.3">des</w> <w n="13.4">loups</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Seriez</w>-<w n="14.2">vous</w> <w n="14.3">leur</w> <w n="14.4">proie</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Aimable</w> <w n="15.2">troupeau</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Vous</w>, <w n="16.2">de</w> <w n="16.3">ce</w> <w n="16.4">hameau</w></l>
						<l n="17" num="1.17"><w n="17.1">L</w>’<w n="17.2">honneur</w> <w n="17.3">et</w> <w n="17.4">la</w> <w n="17.5">joie</w> ;</l>
						<l n="18" num="1.18"><w n="18.1">Vous</w> <w n="18.2">qui</w>, <w n="18.3">gras</w> <w n="18.4">et</w> <w n="18.5">beau</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Me</w> <w n="19.2">donniez</w> <w n="19.3">sans</w> <w n="19.4">cesse</w></l>
						<l n="20" num="1.20"><w n="20.1">Sur</w> <w n="20.2">l</w>’<w n="20.3">herbette</w> <w n="20.4">épaisse</w></l>
						<l n="21" num="1.21"><w n="21.1">Un</w> <w n="21.2">plaisir</w> <w n="21.3">nouveau</w> ?</l>
						<l n="22" num="1.22"><w n="22.1">Que</w> <w n="22.2">je</w> <w n="22.3">vous</w> <w n="22.4">regrette</w> !</l>
						<l n="23" num="1.23"><w n="23.1">Mais</w> <w n="23.2">il</w> <w n="23.3">faut</w> <w n="23.4">céder</w> :</l>
						<l n="24" num="1.24"><w n="24.1">Sans</w> <w n="24.2">chien</w>, <w n="24.3">sans</w> <w n="24.4">houlette</w>,</l>
						<l n="25" num="1.25"><w n="25.1">Puis</w>-<w n="25.2">je</w> <w n="25.3">vous</w> <w n="25.4">garder</w> ?</l>
						<l n="26" num="1.26"><w n="26.1">L</w>’<w n="26.2">injuste</w> <w n="26.3">fortune</w></l>
						<l n="27" num="1.27"><w n="27.1">Me</w> <w n="27.2">les</w> <w n="27.3">a</w> <w n="27.4">ravis</w>.</l>
						<l n="28" num="1.28"><w n="28.1">En</w> <w n="28.2">vain</w> <w n="28.3">j</w>’<w n="28.4">importune</w></l>
						<l n="29" num="1.29"><w n="29.1">Le</w> <w n="29.2">ciel</w> <w n="29.3">par</w> <w n="29.4">mes</w> <w n="29.5">cris</w> ;</l>
						<l n="30" num="1.30"><w n="30.1">Il</w> <w n="30.2">rit</w> <w n="30.3">de</w> <w n="30.4">mes</w> <w n="30.5">craintes</w>,</l>
						<l n="31" num="1.31"><w n="31.1">Et</w>, <w n="31.2">sourd</w> <w n="31.3">à</w> <w n="31.4">mes</w> <w n="31.5">plaintes</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Houlette</w> <w n="32.2">ni</w> <w n="32.3">chien</w>,</l>
						<l n="33" num="1.33"><w n="33.1">Il</w> <w n="33.2">ne</w> <w n="33.3">me</w> <w n="33.4">rend</w> <w n="33.5">rien</w>.</l>
						<l n="34" num="1.34"><w n="34.1">Puissiez</w>-<w n="34.2">vous</w>, <w n="34.3">contentes</w></l>
						<l n="35" num="1.35"><w n="35.1">Et</w> <w n="35.2">sans</w> <w n="35.3">mon</w> <w n="35.4">secours</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Passer</w> <w n="36.2">d</w>’<w n="36.3">heureux</w> <w n="36.4">jours</w>,</l>
						<l n="37" num="1.37"><w n="37.1">Brebis</w> <w n="37.2">innocentes</w>,</l>
						<l n="38" num="1.38"><w n="38.1">Brebis</w>, <w n="38.2">mes</w> <w n="38.3">amours</w>.</l>
						<l n="39" num="1.39"><w n="39.1">Que</w> <w n="39.2">Pan</w> <w n="39.3">vous</w> <w n="39.4">défende</w> ;</l>
						<l n="40" num="1.40"><w n="40.1">Hélas</w> ! <w n="40.2">il</w> <w n="40.3">le</w> <w n="40.4">sait</w>,</l>
						<l n="41" num="1.41"><w n="41.1">Je</w> <w n="41.2">ne</w> <w n="41.3">lui</w> <w n="41.4">demande</w></l>
						<l n="42" num="1.42"><w n="42.1">Que</w> <w n="42.2">ce</w> <w n="42.3">seul</w> <w n="42.4">bienfait</w>.</l>
						<l n="43" num="1.43"><w n="43.1">Oui</w>, <w n="43.2">brebis</w> <w n="43.3">chéries</w>,</l>
						<l n="44" num="1.44"><w n="44.1">Qu</w>’<w n="44.2">avec</w> <w n="44.3">tant</w> <w n="44.4">de</w> <w n="44.5">soin</w></l>
						<l n="45" num="1.45"><w n="45.1">J</w>’<w n="45.2">ai</w> <w n="45.3">toujours</w> <w n="45.4">nourries</w>,</l>
						<l n="46" num="1.46"><w n="46.1">Je</w> <w n="46.2">prends</w> <w n="46.3">à</w> <w n="46.4">témoin</w></l>
						<l n="47" num="1.47"><w n="47.1">Ces</w> <w n="47.2">bois</w>, <w n="47.3">ces</w> <w n="47.4">prairies</w>,</l>
						<l n="48" num="1.48"><w n="48.1">Que</w>, <w n="48.2">si</w> <w n="48.3">les</w> <w n="48.4">faveurs</w></l>
						<l n="49" num="1.49"><w n="49.1">Du</w> <w n="49.2">dieu</w> <w n="49.3">des</w> <w n="49.4">pasteurs</w></l>
						<l n="50" num="1.50"><w n="50.1">Vous</w> <w n="50.2">gardent</w> <w n="50.3">d</w>’<w n="50.4">outrages</w>,</l>
						<l n="51" num="1.51"><w n="51.1">Et</w> <w n="51.2">vous</w> <w n="51.3">font</w> <w n="51.4">avoir</w></l>
						<l n="52" num="1.52"><w n="52.1">Du</w> <w n="52.2">matin</w> <w n="52.3">au</w> <w n="52.4">soir</w></l>
						<l n="53" num="1.53"><w n="53.1">De</w> <w n="53.2">gras</w> <w n="53.3">pâturages</w>,</l>
						<l n="54" num="1.54"><w n="54.1">J</w>’<w n="54.2">en</w> <w n="54.3">conserverai</w></l>
						<l n="55" num="1.55"><w n="55.1">Tant</w> <w n="55.2">que</w> <w n="55.3">je</w> <w n="55.4">vivrai</w></l>
						<l n="56" num="1.56"><w n="56.1">La</w> <w n="56.2">douce</w> <w n="56.3">mémoire</w> ;</l>
						<l n="57" num="1.57"><w n="57.1">Et</w> <w n="57.2">que</w> <w n="57.3">mes</w> <w n="57.4">chansons</w></l>
						<l n="58" num="1.58"><w n="58.1">En</w> <w n="58.2">mille</w> <w n="58.3">façons</w></l>
						<l n="59" num="1.59"><w n="59.1">Porteront</w> <w n="59.2">sa</w> <w n="59.3">gloire</w>,</l>
						<l n="60" num="1.60"><w n="60.1">Du</w> <w n="60.2">rivage</w> <w n="60.3">heureux</w></l>
						<l n="61" num="1.61"><w n="61.1">Où</w>, <w n="61.2">vif</w> <w n="61.3">et</w> <w n="61.4">pompeux</w>,</l>
						<l n="62" num="1.62"><w n="62.1">L</w>’<w n="62.2">astre</w> <w n="62.3">qui</w> <w n="62.4">mesure</w></l>
						<l n="63" num="1.63"><w n="63.1">Les</w> <w n="63.2">nuits</w> <w n="63.3">et</w> <w n="63.4">les</w> <w n="63.5">jours</w>,</l>
						<l n="64" num="1.64"><w n="64.1">Commençant</w> <w n="64.2">son</w> <w n="64.3">cours</w>,</l>
						<l n="65" num="1.65"><w n="65.1">Rend</w> <w n="65.2">à</w> <w n="65.3">la</w> <w n="65.4">nature</w></l>
						<l n="66" num="1.66"><w n="66.1">Toute</w> <w n="66.2">sa</w> <w n="66.3">parure</w>,</l>
						<l n="67" num="1.67"><w n="67.1">Jusqu</w>’<w n="67.2">en</w> <w n="67.3">ces</w> <w n="67.4">climats</w></l>
						<l n="68" num="1.68"><w n="68.1">Où</w>, <w n="68.2">sans</w> <w n="68.3">doute</w> <w n="68.4">las</w></l>
						<l n="69" num="1.69"><w n="69.1">D</w>’<w n="69.2">éclairer</w> <w n="69.3">le</w> <w n="69.4">monde</w>,</l>
						<l n="70" num="1.70"><w n="70.1">Il</w> <w n="70.2">va</w> <w n="70.3">chez</w> <w n="70.4">Téthys</w></l>
						<l n="71" num="1.71"><w n="71.1">Rallumer</w> <w n="71.2">dans</w> <w n="71.3">l</w>’<w n="71.4">onde</w></l>
						<l n="72" num="1.72"><w n="72.1">Ses</w> <w n="72.2">feux</w> <w n="72.3">amortis</w>.</l>
					</lg>
				</div></body></text></TEI>