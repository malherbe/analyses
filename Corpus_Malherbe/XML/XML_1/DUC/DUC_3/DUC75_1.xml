<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC75">
					<head type="main">Assassinat de Victor Noir</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">bien</w> ! — <w n="1.4">Murat</w> <w n="1.5">bâtonne</w> <w n="1.6">et</w> <w n="1.7">Bonaparte</w> <w n="1.8">tue</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">donc</w> <w n="2.3">ose</w> <w n="2.4">parler</w> <w n="2.5">encor</w> <w n="2.6">de</w> <w n="2.7">liberté</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">l</w>’<w n="3.3">étouffer</w> <w n="3.4">vivante</w> <w n="3.5">avant</w> <w n="3.6">sa</w> <w n="3.7">puberté</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Aux</w> <w n="4.2">genoux</w> <w n="4.3">de</w> <w n="4.4">César</w>, <w n="4.5">Brutus</w> <w n="4.6">se</w> <w n="4.7">prostitue</w> !</l>
						<l n="5" num="1.5"><w n="5.1">France</w>, <w n="5.2">courbe</w> <w n="5.3">le</w> <w n="5.4">front</w>, <w n="5.5">plus</w> <w n="5.6">de</w> <w n="5.7">vaine</w> <w n="5.8">fierté</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">Murat</w>, <w n="6.2">Murât</w> <w n="6.3">bâtonne</w> <w n="6.4">et</w> <w n="6.5">Bonaparte</w> <w n="6.6">tue</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Mais</w> <w n="7.2">où</w> <w n="7.3">donc</w> <w n="7.4">marchons</w>-<w n="7.5">nous</w>, <w n="7.6">que</w> <w n="7.7">va</w>-<w n="7.8">t</w>-<w n="7.9">il</w> <w n="7.10">advenir</w> ?</l>
						<l n="8" num="2.2"><w n="8.1">Que</w> <w n="8.2">sommes</w>-<w n="8.3">nous</w>, <w n="8.4">enfin</w> ? <w n="8.5">peuple</w> <w n="8.6">ou</w> <w n="8.7">bêtes</w> <w n="8.8">de</w> <w n="8.9">sommes</w> ?</l>
						<l n="9" num="2.3"><w n="9.1">Est</w>-<w n="9.2">ce</w> <w n="9.3">un</w> <w n="9.4">cœur</w> <w n="9.5">qui</w> <w n="9.6">palpite</w> <w n="9.7">en</w> <w n="9.8">nos</w> <w n="9.9">poitrines</w> <w n="9.10">d</w>’<w n="9.11">hommes</w>,</l>
						<l n="10" num="2.4"><w n="10.1">Ou</w> <w n="10.2">d</w>’<w n="10.3">un</w> <w n="10.4">cœur</w> <w n="10.5">n</w>’<w n="10.6">avons</w>-<w n="10.7">nous</w> <w n="10.8">rien</w> <w n="10.9">que</w> <w n="10.10">le</w> <w n="10.11">souvenir</w> ?</l>
						<l n="11" num="2.5"><w n="11.1">Non</w> ! <w n="11.2">la</w> <w n="11.3">peur</w> <w n="11.4">nous</w> <w n="11.5">terrasse</w> ! <w n="11.6">Ah</w> ! <w n="11.7">lâches</w> <w n="11.8">que</w> <w n="11.9">nous</w> <w n="11.10">sommes</w> !</l>
						<l n="12" num="2.6"><w n="12.1">Mais</w> <w n="12.2">où</w> <w n="12.3">donc</w> <w n="12.4">marchons</w>-<w n="12.5">nous</w>, <w n="12.6">que</w> <w n="12.7">va</w>-<w n="12.8">t</w>-<w n="12.9">il</w> <w n="12.10">advenir</w> ?</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Oh</w> ! <w n="13.2">ces</w> <w n="13.3">Napoléon</w> ! <w n="13.4">il</w> <w n="13.5">leur</w> <w n="13.6">faut</w> <w n="13.7">les</w> <w n="13.8">mains</w> <w n="13.9">rouges</w> !</l>
						<l n="14" num="3.2"><w n="14.1">A</w> <w n="14.2">Vincennes</w>, <w n="14.3">Boulogne</w> <w n="14.4">et</w> <w n="14.5">jusqu</w>’<w n="14.6">en</w> <w n="14.7">leur</w> <w n="14.8">hôtel</w> !</l>
						<l n="15" num="3.3">— « <w n="15.1">A</w> <w n="15.2">moi</w>, <w n="15.3">prince</w> ; <w n="15.4">tu</w> <w n="15.5">viens</w> <w n="15.6">présenter</w> <w n="15.7">un</w> <w n="15.8">cartel</w> ?</l>
						<l n="16" num="3.4"><w n="16.1">Voici</w> <w n="16.2">mon</w> <w n="16.3">revolver</w>, <w n="16.4">je</w> <w n="16.5">fais</w> <w n="16.6">feu</w> <w n="16.7">si</w> <w n="16.8">tu</w> <w n="16.9">bouges</w>. »</l>
						<l n="17" num="3.5"><w n="17.1">Et</w> <w n="17.2">Noir</w> <w n="17.3">tombe</w> <w n="17.4">aussitôt</w> <w n="17.5">frappé</w> <w n="17.6">d</w>’<w n="17.7">un</w> <w n="17.8">coup</w> <w n="17.9">mortel</w> !</l>
						<l n="18" num="3.6"><w n="18.1">Oh</w> ! <w n="18.2">ces</w> <w n="18.3">Napoléon</w>…, <w n="18.4">il</w> <w n="18.5">leur</w> <w n="18.6">faut</w> <w n="18.7">les</w> <w n="18.8">mains</w> <w n="18.9">rouges</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Avais</w>-<w n="19.2">tu</w> <w n="19.3">bien</w> <w n="19.4">le</w> <w n="19.5">droit</w> <w n="19.6">de</w> <w n="19.7">tuer</w> <w n="19.8">cet</w> <w n="19.9">enfant</w> !</l>
						<l n="20" num="4.2"><w n="20.1">Venait</w>-<w n="20.2">il</w> <w n="20.3">en</w> <w n="20.4">son</w> <w n="20.5">nom</w> <w n="20.6">relever</w> <w n="20.7">un</w> <w n="20.8">outrage</w> ?</l>
						<l n="21" num="4.3"><w n="21.1">Il</w> <w n="21.2">venait</w> <w n="21.3">simplement</w> <w n="21.4">s</w>’<w n="21.5">acquitter</w> <w n="21.6">d</w>’<w n="21.7">un</w> <w n="21.8">message</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">Il</w> <w n="22.2">venait</w>, <w n="22.3">comme</w> <w n="22.4">ceux</w> <w n="22.5">que</w> <w n="22.6">leur</w> <w n="22.7">mandat</w> <w n="22.8">défend</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Sommer</w> <w n="23.2">ta</w> <w n="23.3">loyauté</w>, <w n="23.4">parler</w> <w n="23.5">à</w> <w n="23.6">ton</w> <w n="23.7">courage</w>…</l>
						<l n="24" num="4.6"><w n="24.1">Avais</w>-<w n="24.2">tu</w> <w n="24.3">bien</w> <w n="24.4">le</w> <w n="24.5">droit</w> <w n="24.6">de</w> <w n="24.7">tuer</w> <w n="24.8">cet</w> <w n="24.9">enfant</w> ?</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Les</w> <w n="25.2">princes</w> <w n="25.3">aujourd</w>’<w n="25.4">hui</w> <w n="25.5">se</w> <w n="25.6">font</w> <w n="25.7">tueurs</w> <w n="25.8">en</w> <w n="25.9">chambre</w>.</l>
						<l n="26" num="5.2"><w n="26.1">Oh</w> ! <w n="26.2">l</w>’<w n="26.3">indignation</w> <w n="26.4">nous</w> <w n="26.5">saisit</w>, <w n="26.6">nous</w> <w n="26.7">étreint</w>.</l>
						<l n="27" num="5.3"><w n="27.1">Le</w> <w n="27.2">grand</w> <w n="27.3">jour</w> <w n="27.4">leur</w> <w n="27.5">fait</w> <w n="27.6">peur</w> ; <w n="27.7">on</w> <w n="27.8">le</w> <w n="27.9">fuit</w>, <w n="27.10">on</w> <w n="27.11">le</w> <w n="27.12">craint</w>,</l>
						<l n="28" num="5.4"><w n="28.1">On</w> <w n="28.2">ne</w> <w n="28.3">massacre</w> <w n="28.4">plus</w> <w n="28.5">ainsi</w> <w n="28.6">qu</w>’<w n="28.7">au</w> <hi rend="ital"><w n="28.8">Deux</w> <w n="28.9">Décembre</w></hi>,</l>
						<l n="29" num="5.5"><w n="29.1">Par</w> <w n="29.2">la</w> <w n="29.3">rue</w> <w n="29.4">et</w> <w n="29.5">les</w> <w n="29.6">quais</w> <w n="29.7">le</w> <w n="29.8">Peuple</w> <w n="29.9">souverain</w>…</l>
						<l n="30" num="5.6"><w n="30.1">Les</w> <w n="30.2">Princes</w> <w n="30.3">aujourd</w>’<w n="30.4">hui</w> <w n="30.5">se</w> <w n="30.6">font</w> <w n="30.7">tueurs</w> <w n="30.8">en</w> <w n="30.9">chambre</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">10 Janvier 1870 — 5 h. du soir</date>.
							Écrit <placeName>au café des Variétés</placeName>
								en apprenant <lb></lb>
								la nouvelle de l’assassinat de notre jeune ami Victor Noir.
						</dateline>
					</closer>
				</div></body></text></TEI>