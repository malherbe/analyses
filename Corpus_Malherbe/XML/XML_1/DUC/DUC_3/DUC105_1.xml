<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Troisième partie</head><head type="sub_part">(1871-1885)</head><div type="poem" key="DUC105">
					<head type="main">Les morts protestent</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">âpre</w> <w n="1.3">bise</w> <w n="1.4">soufflait</w> <w n="1.5">à</w> <w n="1.6">travers</w> <w n="1.7">les</w> <w n="1.8">bois</w> <w n="1.9">nus</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Et</w>, <w n="2.2">des</w> <w n="2.3">entrailles</w> <w n="2.4">de</w> <w n="2.5">la</w> <w n="2.6">terre</w>,</l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">longs</w> <w n="3.3">gémissements</w>, <w n="3.4">effroyable</w> <w n="3.5">mystère</w>,</l>
						<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">élevaient</w> <w n="4.3">lentement</w> <w n="4.4">mornes</w> <w n="4.5">et</w> <w n="4.6">continus</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Des</w> <w n="5.2">morts</w> <w n="5.3">parlaient</w> <w n="5.4">entre</w> <w n="5.5">eux</w> <w n="5.6">sous</w> <w n="5.7">la</w> <w n="5.8">neige</w> <w n="5.9">durcie</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">de</w> <w n="6.3">leur</w> <w n="6.4">bouche</w> <w n="6.5">blême</w> <w n="6.6">où</w> <w n="6.7">le</w> <w n="6.8">ver</w> <w n="6.9">est</w> <w n="6.10">entré</w></l>
						<l n="7" num="1.7"><w n="7.1">Il</w> <w n="7.2">s</w>’<w n="7.3">élevait</w> <w n="7.4">un</w> <w n="7.5">chant</w> <w n="7.6">comme</w> <w n="7.7">une</w> <w n="7.8">litanie</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Un</w> <w n="8.2">lugubre</w> <foreign lang="LAT"><w n="8.3">Miserere</w></foreign> !</l>
					</lg>
					<lg n="2">
						<head type="speaker">VOIX DE DESSOUS TERRE</head>
						<l n="9" num="2.1"><space unit="char" quantity="8"></space>« <w n="9.1">Je</w> <w n="9.2">n</w>’<w n="9.3">entends</w> <w n="9.4">plus</w> <w n="9.5">le</w> <w n="9.6">bruit</w> <w n="9.7">des</w> <w n="9.8">armes</w>,</l>
						<l n="10" num="2.2"><space unit="char" quantity="8"></space><w n="10.1">Et</w> <w n="10.2">cependant</w> <w n="10.3">tous</w> <w n="10.4">les</w> <w n="10.5">chemins</w>,</l>
						<l n="11" num="2.3"><space unit="char" quantity="8"></space><w n="11.1">Sont</w>-<w n="11.2">couverts</w> <w n="11.3">de</w> <w n="11.4">soldats</w> <w n="11.5">germains</w></l>
						<l n="12" num="2.4"><space unit="char" quantity="8"></space><w n="12.1">O</w> <w n="12.2">désespoir</w> ! <w n="12.3">ô</w> <w n="12.4">deuil</w> ! <w n="12.5">ô</w> <w n="12.6">larmes</w> !</l>
						<l n="13" num="2.5"><space unit="char" quantity="4"></space><w n="13.1">Par</w> <w n="13.2">l</w>’<w n="13.3">ennemi</w> <w n="13.4">nos</w> <w n="13.5">chemins</w> <w n="13.6">sont</w> <w n="13.7">souillés</w> ;</l>
						<l n="14" num="2.6"><space unit="char" quantity="8"></space><w n="14.1">De</w> <w n="14.2">notre</w> <w n="14.3">sang</w> <w n="14.4">la</w> <w n="14.5">terre</w> <w n="14.6">est</w> <w n="14.7">rouge</w> !</l>
						<l n="15" num="2.7"><w n="15.1">On</w> <w n="15.2">peut</w> <w n="15.3">combattre</w> <w n="15.4">encore</w> <w n="15.5">et</w> <w n="15.6">personne</w> <w n="15.7">ne</w> <w n="15.8">bouge</w>…</l>
						<l n="16" num="2.8"><space unit="char" quantity="8"></space><w n="16.1">Déjà</w> <w n="16.2">les</w> <w n="16.3">morts</w> <w n="16.4">sont</w> <w n="16.5">oubliés</w> ! »</l>
					</lg>
					<lg n="3">
						<head type="speaker">UNE VOIX</head>
						<l n="17" num="3.1"><space unit="char" quantity="8"></space><w n="17.1">J</w>’<w n="17.2">avais</w> <w n="17.3">une</w> <w n="17.4">femme</w>, <w n="17.5">un</w> <w n="17.6">enfant</w>,</l>
						<l n="18" num="3.2"><space unit="char" quantity="8"></space><w n="18.1">Mais</w> <w n="18.2">pour</w> <w n="18.3">défendre</w> <w n="18.4">la</w> <w n="18.5">Patrie</w>,</l>
						<l n="19" num="3.3"><space unit="char" quantity="8"></space><w n="19.1">Je</w> <w n="19.2">pris</w> <w n="19.3">les</w> <w n="19.4">armes</w> <w n="19.5">et</w> <w n="19.6">mon</w> <w n="19.7">sang</w></l>
						<l n="20" num="3.4"><space unit="char" quantity="8"></space><w n="20.1">Coula</w> <w n="20.2">pour</w> <w n="20.3">toi</w>, <w n="20.4">France</w> <w n="20.5">chérie</w> !</l>
						<l n="21" num="3.5"><space unit="char" quantity="8"></space><w n="21.1">A</w> <w n="21.2">mon</w> <w n="21.3">fils</w> <w n="21.4">si</w> <w n="21.5">doux</w> <w n="21.6">et</w> <w n="21.7">si</w> <w n="21.8">beau</w>,</l>
						<l n="22" num="3.6"><space unit="char" quantity="8"></space><w n="22.1">Le</w> <w n="22.2">deuil</w> <w n="22.3">met</w> <w n="22.4">une</w> <w n="22.5">robe</w> <w n="22.6">neuve</w> !…</l>
						<l n="23" num="3.7"><space unit="char" quantity="8"></space><w n="23.1">Entre</w> <w n="23.2">ma</w> <w n="23.3">fosse</w> <w n="23.4">et</w> <w n="23.5">son</w> <w n="23.6">berceau</w>,</l>
						<l n="24" num="3.8"><space unit="char" quantity="8"></space><w n="24.1">Ma</w> <w n="24.2">jeune</w> <w n="24.3">femme</w> <w n="24.4">reste</w> <w n="24.5">veuve</w> !</l>
					</lg>
					<lg n="4">
						<head type="speaker">TOUTES LES VOIX</head>
						<l n="25" num="4.1"><space unit="char" quantity="4"></space><w n="25.1">Par</w> <w n="25.2">l</w>’<w n="25.3">ennemi</w> <w n="25.4">nos</w> <w n="25.5">chemins</w> <w n="25.6">sont</w> <w n="25.7">souillés</w> ;</l>
						<l n="26" num="4.2"><space unit="char" quantity="8"></space><w n="26.1">De</w> <w n="26.2">notre</w> <w n="26.3">sang</w> <w n="26.4">la</w> <w n="26.5">terre</w> <w n="26.6">est</w> <w n="26.7">rouge</w> !</l>
						<l n="27" num="4.3"><w n="27.1">On</w> <w n="27.2">peut</w> <w n="27.3">combattre</w> <w n="27.4">encore</w> <w n="27.5">et</w> <w n="27.6">personne</w> <w n="27.7">ne</w> <w n="27.8">bouge</w>…</l>
						<l n="28" num="4.4"><space unit="char" quantity="8"></space><w n="28.1">Déjà</w> <w n="28.2">les</w> <w n="28.3">morts</w> <w n="28.4">sont</w> <w n="28.5">oubliés</w> !</l>
					</lg>
					<lg n="5">
						<head type="speaker">VOIX D’ENFANTS</head>
						<l n="29" num="5.1"><space unit="char" quantity="8"></space><w n="29.1">Nos</w> <w n="29.2">pères</w>, <w n="29.3">là</w>-<w n="29.4">bas</w>, <w n="29.5">se</w> <w n="29.6">battaient</w> ;</l>
						<l n="30" num="5.2"><space unit="char" quantity="8"></space><w n="30.1">Ils</w> <w n="30.2">se</w> <w n="30.3">battaient</w> <w n="30.4">pour</w> <w n="30.5">la</w> <w n="30.6">Patrie</w> !</l>
						<l n="31" num="5.3"><space unit="char" quantity="8"></space><w n="31.1">Nos</w> <w n="31.2">pauvres</w> <w n="31.3">mères</w> <w n="31.4">attendaient</w></l>
						<l n="32" num="5.4"><space unit="char" quantity="8"></space><w n="32.1">L</w>’<w n="32.2">horrible</w> <w n="32.3">pain</w> <w n="32.4">de</w> <w n="32.5">la</w> <w n="32.6">Mairie</w> ! <ref type="noteAnchor" target="(1)">(1)</ref></l>
						<l n="33" num="5.5"><space unit="char" quantity="8"></space><w n="33.1">Dans</w> <w n="33.2">l</w>’<w n="33.3">école</w> <w n="33.4">où</w> <w n="33.5">nous</w> <w n="33.6">allions</w> <w n="33.7">tous</w>,</l>
						<l n="34" num="5.6"><space unit="char" quantity="8"></space><w n="34.1">Nous</w> <w n="34.2">entendions</w> <w n="34.3">l</w>’<w n="34.4">éclat</w> <w n="34.5">des</w> <w n="34.6">bombes</w>…</l>
						<l n="35" num="5.7"><space unit="char" quantity="8"></space><w n="35.1">L</w>’<w n="35.2">une</w> <w n="35.3">frappe</w> <w n="35.4">cinq</w> <w n="35.5">d</w>’<w n="35.6">entre</w> <w n="35.7">nous</w> ;</l>
						<l n="36" num="5.8"><space unit="char" quantity="8"></space><w n="36.1">On</w> <w n="36.2">creusa</w> <w n="36.3">cinq</w> <w n="36.4">petites</w> <w n="36.5">tombes</w> ! <ref type="noteAnchor" target="2">(2)</ref></l>
					</lg>
					<lg n="6">
						<head type="speaker">TOUTES LES VOIX</head>
						<l n="37" num="6.1"><space unit="char" quantity="4"></space><w n="37.1">Par</w> <w n="37.2">l</w>’<w n="37.3">ennemi</w> <w n="37.4">nos</w> <w n="37.5">chemins</w> <w n="37.6">sont</w> <w n="37.7">souillés</w> ;</l>
						<l n="38" num="6.2"><space unit="char" quantity="8"></space><w n="38.1">De</w> <w n="38.2">notre</w> <w n="38.3">sang</w> <w n="38.4">la</w> <w n="38.5">terre</w> <w n="38.6">est</w> <w n="38.7">rouge</w> !</l>
						<l n="39" num="6.3"><w n="39.1">On</w> <w n="39.2">peut</w> <w n="39.3">combattre</w> <w n="39.4">encore</w> <w n="39.5">et</w> <w n="39.6">personne</w> <w n="39.7">ne</w> <w n="39.8">bouge</w> !</l>
						<l n="40" num="6.4"><space unit="char" quantity="8"></space><w n="40.1">Déjà</w> <w n="40.2">les</w> <w n="40.3">morts</w> <w n="40.4">sont</w> <w n="40.5">oubliés</w> !</l>
					</lg>
					<lg n="7">
						<head type="speaker">UNE AUTRE VOIX</head>
						<l n="41" num="7.1"><space unit="char" quantity="8"></space>— « <w n="41.1">Malgré</w> <w n="41.2">le</w> <w n="41.3">poids</w> <w n="41.4">glacé</w> <w n="41.5">des</w> <w n="41.6">ans</w>,</l>
						<l n="42" num="7.2"><space unit="char" quantity="8"></space><w n="42.1">J</w>’<w n="42.2">avais</w>, <w n="42.3">à</w> <w n="42.4">l</w>’<w n="42.5">appel</w> <w n="42.6">de</w> <w n="42.7">la</w> <w n="42.8">France</w>,</l>
						<l n="43" num="7.3"><space unit="char" quantity="8"></space><w n="43.1">Retrouvé</w> <w n="43.2">mes</w> <w n="43.3">jeunes</w> <w n="43.4">élans</w>,</l>
						<l n="44" num="7.4"><space unit="char" quantity="8"></space><w n="44.1">De</w> <w n="44.2">liberté</w>, <w n="44.3">de</w> <w n="44.4">délivrance</w> !</l>
						<l n="45" num="7.5"><space unit="char" quantity="8"></space><w n="45.1">Je</w> <w n="45.2">pouvais</w> <w n="45.3">goûter</w> <w n="45.4">le</w> <w n="45.5">repos</w>…</l>
						<l n="46" num="7.6"><space unit="char" quantity="8"></space><w n="46.1">Mais</w>, <w n="46.2">honte</w> <w n="46.3">à</w> <w n="46.4">qui</w> <w n="46.5">tremble</w> <w n="46.6">ou</w> <w n="46.7">se</w> <w n="46.8">sauve</w> !</l>
						<l n="47" num="7.7"><space unit="char" quantity="8"></space><w n="47.1">Les</w> <w n="47.2">vautours</w> <w n="47.3">ont</w> <w n="47.4">rongé</w> <w n="47.5">mes</w> <w n="47.6">os</w> ;</l>
						<l n="48" num="7.8"><space unit="char" quantity="8"></space><w n="48.1">Les</w> <w n="48.2">vents</w> <w n="48.3">roulent</w> <w n="48.4">ma</w> <w n="48.5">tête</w> <w n="48.6">chauve</w> ! »</l>
					</lg>
					<lg n="8">
						<head type="speaker">TOUTES LES VOIX</head>
						<l n="49" num="8.1"><space unit="char" quantity="4"></space><w n="49.1">Par</w> <w n="49.2">l</w>’<w n="49.3">ennemi</w> <w n="49.4">nos</w> <w n="49.5">chemins</w> <w n="49.6">sont</w> <w n="49.7">souillés</w> !</l>
						<l n="50" num="8.2"><space unit="char" quantity="8"></space><w n="50.1">De</w> <w n="50.2">notre</w> <w n="50.3">sang</w> <w n="50.4">la</w> <w n="50.5">terre</w> <w n="50.6">est</w> <w n="50.7">rouge</w> !…</l>
						<l n="51" num="8.3"><w n="51.1">On</w> <w n="51.2">peut</w> <w n="51.3">combattre</w> <w n="51.4">encore</w> <w n="51.5">et</w> <w n="51.6">personne</w> <w n="51.7">ne</w> <w n="51.8">bouge</w></l>
						<l n="52" num="8.4"><space unit="char" quantity="8"></space><w n="52.1">Déjà</w> <w n="52.2">les</w> <w n="52.3">morts</w> <w n="52.4">sont</w> <w n="52.5">oubliés</w> !…</l>
					</lg>
					<lg n="9">
						<head type="speaker">LA VOIX DE LA PATRIE</head>
						<l n="53" num="9.1"><space unit="char" quantity="8"></space><w n="53.1">Dormez</w>, <w n="53.2">martyrs</w> ! <w n="53.3">dormez</w>, <w n="53.4">héros</w> !</l>
						<l n="54" num="9.2"><space unit="char" quantity="8"></space><w n="54.1">Vous</w> <w n="54.2">m</w>’<w n="54.3">avez</w> <w n="54.4">donné</w> <w n="54.5">votre</w> <w n="54.6">vie</w> ;</l>
						<l n="55" num="9.3"><space unit="char" quantity="8"></space><w n="55.1">De</w> <w n="55.2">la</w> <w n="55.3">pierre</w> <w n="55.4">de</w> <w n="55.5">vos</w> <w n="55.6">tombeaux</w>,</l>
						<l n="56" num="9.4"><space unit="char" quantity="8"></space><w n="56.1">Je</w> <w n="56.2">fais</w> <w n="56.3">l</w>’<w n="56.4">autel</w> <w n="56.5">de</w> <w n="56.6">la</w> <w n="56.7">Patrie</w> !</l>
						<l n="57" num="9.5"><space unit="char" quantity="8"></space><w n="57.1">O</w> <w n="57.2">morts</w> ! <w n="57.3">C</w>’<w n="57.4">est</w> <w n="57.5">là</w> <w n="57.6">que</w> <w n="57.7">vos</w> <w n="57.8">enfants</w>,</l>
						<l n="58" num="9.6"><space unit="char" quantity="8"></space><w n="58.1">Quand</w> <w n="58.2">notre</w> <w n="58.3">aube</w> <w n="58.4">luira</w> <w n="58.5">plus</w> <w n="58.6">blanche</w>,</l>
						<l n="59" num="9.7"><space unit="char" quantity="8"></space><w n="59.1">Jureront</w> <w n="59.2">d</w>’<w n="59.3">être</w> <w n="59.4">triomphants</w>,</l>
						<l n="60" num="9.8"><space unit="char" quantity="8"></space><w n="60.1">Au</w> <w n="60.2">jour</w> <w n="60.3">de</w> <w n="60.4">la</w> <w n="60.5">Sainte</w> <w n="60.6">Revanche</w> !</l>
						<l n="61" num="9.9"><space unit="char" quantity="4"></space><w n="61.1">Dormez</w> <w n="61.2">en</w> <w n="61.3">paix</w> ! <w n="61.4">vous</w> <w n="61.5">serez</w> <w n="61.6">réveillés</w>,</l>
						<l n="62" num="9.10"><space unit="char" quantity="8"></space><w n="62.1">Le</w> <w n="62.2">jour</w> <w n="62.3">où</w> <w n="62.4">nos</w> <w n="62.5">mains</w> <w n="62.6">seront</w> <w n="62.7">rouges</w>,</l>
						<l n="63" num="9.11"><w n="63.1">Du</w> <w n="63.2">sang</w> <w n="63.3">de</w> <w n="63.4">l</w>’<w n="63.5">Allemand</w> <w n="63.6">éperdu</w> <w n="63.7">dans</w> <w n="63.8">ses</w> <w n="63.9">bouges</w> !…</l>
						<l n="64" num="9.12"><space unit="char" quantity="8"></space><w n="64.1">Les</w> <w n="64.2">morts</w> <w n="64.3">ne</w> <w n="64.4">sont</w> <w n="64.5">pas</w> <w n="64.6">oubliés</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1871">février 1871</date>.
						</dateline>
						<note type="footnote" id="1">
							On distribuait, avec des bons des Mairies, 300 grammes par jour <lb></lb>
							et par tête d’un affreux mélange de choses innommées que l’on <lb></lb>
							disait être du pain ! nos ménagères devaient, pour en avoir, <lb></lb>
							faire la queue des heures entières devant les boulangeries, <lb></lb>
							sous les obus et les pieds, dans la neige.
						</note>
						<note type="footnote" id="2">
							Une bombe prussienne, tombant sur une école du quartier <lb></lb>
							Montparnasse, tua du même coup cinq garçonnets, — Malgré <lb></lb>
							le bombardement, les écoles étalent restées ouvertes et les <lb></lb>
							théâtres jouaient le soir. — O peuple héroïque !
						</note>
					</closer>
				</div></body></text></TEI>