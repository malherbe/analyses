<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC67">
					<head type="main">Les Morts Expropriés<ref type="noteAnchor" target="(1)">(1)</ref></head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ainsi</w> <w n="1.2">donc</w> <w n="1.3">le</w> <w n="1.4">cordeau</w> <w n="1.5">tiré</w> <w n="1.6">par</w> <w n="1.7">l</w>’<w n="1.8">architecte</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Va</w> <w n="2.2">déranger</w> <w n="2.3">les</w> <w n="2.4">morts</w> ? — <w n="2.5">Leur</w> <w n="2.6">sommeil</w> <w n="2.7">que</w> <w n="2.8">respecte</w></l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">passant</w> <w n="3.3">incliné</w> <w n="3.4">sur</w> <w n="3.5">le</w> <w n="3.6">froid</w> <w n="3.7">monument</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">épeler</w> <w n="4.3">un</w> <w n="4.4">nom</w> <w n="4.5">avec</w> <w n="4.6">recueillement</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Doit</w> <w n="5.2">être</w> <w n="5.3">interrompu</w> ? — <w n="5.4">Donc</w>, <w n="5.5">la</w> <w n="5.6">pioche</w> <w n="5.7">sonore</w></l>
						<l n="6" num="1.6"><w n="6.1">Du</w> <w n="6.2">sombre</w> <w n="6.3">fossoyeur</w> <w n="6.4">va</w> <w n="6.5">retentir</w> <w n="6.6">encore</w></l>
						<l n="7" num="1.7"><w n="7.1">Sur</w> <w n="7.2">la</w> <w n="7.3">fosse</w> <w n="7.4">comblée</w>, <w n="7.5">et</w>, <w n="7.6">les</w> <w n="7.7">morts</w> <w n="7.8">réveillés</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Par</w> <w n="8.2">les</w> <w n="8.3">trous</w> <w n="8.4">du</w> <w n="8.5">cercueil</w> <w n="8.6">aux</w> <w n="8.7">ais</w> <w n="8.8">entre</w>-<w n="8.9">bâillés</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Surpris</w>, <w n="9.2">regarderont</w> <w n="9.3">si</w> <w n="9.4">l</w>’<w n="9.5">Archange</w> <w n="9.6">lui</w>-<w n="9.7">même</w>,</l>
						<l n="10" num="1.10"><w n="10.1">A</w> <w n="10.2">sonné</w> <w n="10.3">les</w> <w n="10.4">appels</w> <w n="10.5">du</w> <w n="10.6">jugement</w> <w n="10.7">suprême</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">se</w> <w n="11.3">demanderont</w> : — « <w n="11.4">Est</w>-<w n="11.5">ce</w> <w n="11.6">donc</w> <w n="11.7">aujourd</w>’<w n="11.8">hui</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Que</w> <w n="12.2">le</w> <w n="12.3">Seigneur</w> <w n="12.4">nous</w> <w n="12.5">fait</w> <w n="12.6">paraître</w> <w n="12.7">devant</w> <w n="12.8">Lui</w> ? »</l>
						<l n="13" num="1.13"><w n="13.1">Et</w>, <w n="13.2">secouant</w> <w n="13.3">les</w> <w n="13.4">plis</w> <w n="13.5">d</w>’<w n="13.6">un</w> <w n="13.7">lambeau</w> <w n="13.8">de</w> <w n="13.9">suaire</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Agitant</w>, <w n="14.2">effarés</w>, <w n="14.3">dans</w> <w n="14.4">l</w>’<w n="14.5">étroit</w> <w n="14.6">ossuaire</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Leurs</w> <w n="15.2">membres</w> <w n="15.3">décharnés</w> <w n="15.4">que</w> <w n="15.5">la</w> <w n="15.6">mort</w> <w n="15.7">a</w> <w n="15.8">roidis</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Eux</w>-<w n="16.2">mêmes</w> <w n="16.3">chanteront</w> <w n="16.4">un</w> <w n="16.5">long</w> <foreign lang="LAT"> <w n="16.6">De</w> <w n="16.7">Profundis</w></foreign> !</l>
					</lg>
					<lg n="2">
						<l n="17" num="2.1"><w n="17.1">Non</w> ! <w n="17.2">Ce</w> <w n="17.3">n</w>’<w n="17.4">est</w> <w n="17.5">pas</w> <w n="17.6">le</w> <w n="17.7">jour</w> <w n="17.8">où</w> <w n="17.9">le</w> <w n="17.10">cercueil</w> <w n="17.11">doit</w> <w n="17.12">rendre</w></l>
						<l n="18" num="2.2"><w n="18.1">Le</w> <w n="18.2">corps</w> <w n="18.3">des</w> <w n="18.4">trépassés</w> <w n="18.5">dont</w> <w n="18.6">il</w> <w n="18.7">gardait</w> <w n="18.8">la</w> <w n="18.9">cendre</w> ;</l>
						<l n="19" num="2.3"><w n="19.1">O</w> <w n="19.2">morts</w> ! <w n="19.3">rassurez</w>-<w n="19.4">vous</w>, <w n="19.5">ce</w> <w n="19.6">n</w>’<w n="19.7">est</w> <w n="19.8">pas</w> <w n="19.9">aujourd</w>’<w n="19.10">hui</w>,</l>
						<l n="20" num="2.4"><w n="20.1">Que</w> <w n="20.2">l</w>’<w n="20.3">Archange</w> <w n="20.4">a</w> <w n="20.5">crié</w> ; — <foreign lang="LAT"><w n="20.6">Surgite</w> <w n="20.7">Mortui</w></foreign> !</l>
						<l n="21" num="2.5"><w n="21.1">Levez</w>-<w n="21.2">vous</w>, <w n="21.3">cependant</w> ! <w n="21.4">levez</w>-<w n="21.5">vous</w> ! <w n="21.6">nos</w> <w n="21.7">édiles</w>,</l>
						<l n="22" num="2.6"><w n="22.1">Hier</w>, <w n="22.2">ont</w> <w n="22.3">décrété</w> <w n="22.4">que</w> <w n="22.5">les</w> <w n="22.6">morts</w> <w n="22.7">inutiles</w>,</l>
						<l n="23" num="2.7"><w n="23.1">Malgré</w> <w n="23.2">le</w> <w n="23.3">saint</w> <w n="23.4">regret</w>, <w n="23.5">le</w> <w n="23.6">pieux</w> <w n="23.7">souvenir</w>,</l>
						<l n="24" num="2.8"><w n="24.1">Devant</w> <w n="24.2">l</w>’<w n="24.3">utilité</w> <w n="24.4">publique</w>, <w n="24.5">à</w> <w n="24.6">l</w>’<w n="24.7">avenir</w></l>
						<l n="25" num="2.9"><w n="25.1">Seraient</w> <w n="25.2">expropriés</w> ! <w n="25.3">Oui</w>, <w n="25.4">les</w> <w n="25.5">morts</w> ! — <w n="25.6">On</w> <w n="25.7">les</w> <w n="25.8">chasse</w>,</l>
						<l n="26" num="2.10"><w n="26.1">Du</w> <w n="26.2">sol</w> <w n="26.3">où</w>, <w n="26.4">pour</w> <w n="26.5">dormir</w>, <w n="26.6">ils</w> <w n="26.7">ont</w> <w n="26.8">payé</w> <w n="26.9">la</w> <w n="26.10">place</w>.</l>
						<l n="27" num="2.11"><w n="27.1">On</w> <w n="27.2">va</w> <w n="27.3">rouvrir</w> <w n="27.4">leur</w> <w n="27.5">tombe</w>, <w n="27.6">et</w> <w n="27.7">les</w> <w n="27.8">cieux</w> <w n="27.9">étonnés</w>,</l>
						<l n="28" num="2.12"><w n="28.1">Verront</w> <w n="28.2">leurs</w> <w n="28.3">ossements</w> <w n="28.4">froidement</w> <w n="28.5">profanés</w>,</l>
						<l n="29" num="2.13"><w n="29.1">Par</w> <w n="29.2">les</w> <w n="29.3">démolisseurs</w> <w n="29.4">aux</w> <w n="29.5">gages</w> <w n="29.6">de</w> <w n="29.7">la</w> <w n="29.8">Ville</w>.</l>
						<l n="30" num="2.14"><w n="30.1">La</w> <w n="30.2">réclamation</w>, <w n="30.3">hélas</w> ! <w n="30.4">est</w> <w n="30.5">inutile</w> :</l>
						<l n="31" num="2.15"><w n="31.1">Il</w> <w n="31.2">faut</w> <w n="31.3">un</w> <w n="31.4">boulevard</w>, <w n="31.5">sur</w> <w n="31.6">lequel</w> <w n="31.7">on</w> <w n="31.8">pourra</w></l>
						<l n="32" num="2.16"><w n="32.1">Bâtir</w> <w n="32.2">le</w> <w n="32.3">riche</w> <w n="32.4">immeuble</w> <w n="32.5">et</w> <w n="32.6">qui</w> <w n="32.7">rapportera</w>.</l>
						<l n="33" num="2.17"><w n="33.1">On</w> <w n="33.2">a</w> <w n="33.3">pris</w> <w n="33.4">les</w> <w n="33.5">jardins</w> ? <w n="33.6">Les</w> <w n="33.7">cités</w> <w n="33.8">ouvrières</w> ?</l>
						<l n="34" num="2.18"><w n="34.1">Votez</w>, <w n="34.2">pour</w> <w n="34.3">que</w> <w n="34.4">l</w>’<w n="34.5">on</w> <w n="34.6">prenne</w> <w n="34.7">aussi</w> <w n="34.8">les</w> <w n="34.9">cimetières</w> !</l>
						<l n="35" num="2.19"><w n="35.1">Vous</w> <w n="35.2">n</w>’<w n="35.3">aviez</w> <w n="35.4">pas</w> <w n="35.5">songé</w> <w n="35.6">jusqu</w>’<w n="35.7">ici</w> <w n="35.8">que</w> <w n="35.9">le</w> <w n="35.10">mort</w></l>
						<l n="36" num="2.20"><w n="36.1">Pouvait</w> <w n="36.2">être</w> <w n="36.3">au</w> <w n="36.4">vivant</w> <w n="36.5">d</w>’<w n="36.6">un</w> <w n="36.7">utile</w> <w n="36.8">rapport</w> ?</l>
						<l n="37" num="2.21"><w n="37.1">Votez</w> ! <w n="37.2">pour</w> <w n="37.3">que</w> <w n="37.4">la</w> <w n="37.5">tombe</w> <w n="37.6">où</w> <w n="37.7">repose</w> <w n="37.8">l</w>’<w n="37.9">ancêtre</w>,</l>
						<l n="38" num="2.22"><w n="38.1">Se</w> <w n="38.2">transforme</w> <w n="38.3">en</w> <w n="38.4">terrain</w> <w n="38.5">à</w> <w n="38.6">huit</w> <w n="38.7">cents</w> <w n="38.8">francs</w> <w n="38.9">le</w> <w n="38.10">mètre</w> !</l>
					</lg>
					<lg n="3">
						<l n="39" num="3.1"><w n="39.1">O</w> <w n="39.2">morts</w> ! <w n="39.3">en</w> <w n="39.4">allez</w>-<w n="39.5">vous</w> <w n="39.6">devant</w> <w n="39.7">l</w>’<w n="39.8">alignement</w> !</l>
						<l n="40" num="3.2"><w n="40.1">On</w> <w n="40.2">vous</w> <w n="40.3">paiera</w> <w n="40.4">les</w> <w n="40.5">frais</w> <w n="40.6">de</w> <w n="40.7">déménagement</w> !</l>
						<l n="41" num="3.3"><w n="41.1">Dans</w> <w n="41.2">un</w> <w n="41.3">coin</w> <w n="41.4">du</w> <w n="41.5">cercueil</w> <w n="41.6">roulez</w> <w n="41.7">vos</w> <w n="41.8">longs</w> <w n="41.9">suaires</w>,</l>
						<l n="42" num="3.4"><w n="42.1">La</w> <w n="42.2">croix</w>, <w n="42.3">le</w> <w n="42.4">buis</w> <w n="42.5">bénit</w>, <w n="42.6">le</w> <w n="42.7">livre</w> <w n="42.8">de</w> <w n="42.9">prières</w>,</l>
						<l n="43" num="3.5"><w n="43.1">Que</w> <w n="43.2">la</w> <w n="43.3">religion</w>, <w n="43.4">la</w> <w n="43.5">tendre</w> <w n="43.6">piété</w></l>
						<l n="44" num="3.6"><w n="44.1">Mirent</w> <w n="44.2">entre</w> <w n="44.3">vos</w> <w n="44.4">mains</w> <w n="44.5">et</w> <w n="44.6">pour</w> <w n="44.7">l</w>’<w n="44.8">Éternité</w> ;</l>
						<l n="45" num="3.7"><w n="45.1">Un</w> <w n="45.2">camion</w> <w n="45.3">viendra</w> <w n="45.4">vous</w> <w n="45.5">charger</w> <w n="45.6">tout</w> <w n="45.7">à</w> <w n="45.8">l</w>’<w n="45.9">heure</w> !</l>
						<l n="46" num="3.8"><w n="46.1">N</w>’<w n="46.2">oubliez</w> <w n="46.3">rien</w> <w n="46.4">surtout</w> <w n="46.5">dans</w> <w n="46.6">l</w>’<w n="46.7">obscure</w> <w n="46.8">demeure</w> ;</l>
						<l n="47" num="3.9"><w n="47.1">Ni</w> <w n="47.2">la</w> <w n="47.3">croix</w>, <w n="47.4">ni</w> <w n="47.5">le</w> <w n="47.6">buis</w>, <w n="47.7">ni</w> <w n="47.8">le</w> <w n="47.9">livre</w> ; — <w n="47.10">demain</w></l>
						<l n="48" num="3.10"><w n="48.1">Quelque</w> <w n="48.2">maçon</w> <w n="48.3">pourra</w> <w n="48.4">sur</w> <w n="48.5">le</w> <w n="48.6">bord</w> <w n="48.7">du</w> <w n="48.8">chemin</w>,</l>
						<l n="49" num="3.11"><w n="49.1">Les</w> <w n="49.2">jeter</w> <w n="49.3">en</w> <w n="49.4">chantant</w>, <w n="49.5">ou</w> <w n="49.6">bien</w> <w n="49.7">s</w>’<w n="49.8">il</w> <w n="49.9">les</w> <w n="49.10">ramasse</w>,</l>
						<l n="50" num="3.12"><w n="50.1">Il</w> <w n="50.2">les</w> <w n="50.3">vendra</w> <w n="50.4">pour</w> <w n="50.5">boire</w> <w n="50.6">au</w> <w n="50.7">chiffonnier</w> <w n="50.8">qui</w> <w n="50.9">passe</w> !</l>
						<l n="51" num="3.13"><w n="51.1">O</w> <w n="51.2">morts</w> ! <w n="51.3">vous</w> <w n="51.4">aviez</w> <w n="51.5">cru</w>, <w n="51.6">quand</w> <w n="51.7">se</w> <w n="51.8">ferma</w> <w n="51.9">votre</w> <w n="51.10">œil</w>,</l>
						<l n="52" num="3.14"><w n="52.1">Que</w> <w n="52.2">vous</w> <w n="52.3">alliez</w> <w n="52.4">avoir</w> <w n="52.5">le</w> <w n="52.6">repos</w> <w n="52.7">du</w> <w n="52.8">cercueil</w> ?</l>
						<l n="53" num="3.15"><w n="53.1">Que</w> <w n="53.2">vous</w> <w n="53.3">croisant</w> <w n="53.4">les</w> <w n="53.5">bras</w> <w n="53.6">sous</w> <w n="53.7">les</w> <w n="53.8">bandes</w> <w n="53.9">funèbres</w>,</l>
						<l n="54" num="3.16"><w n="54.1">Vous</w> <w n="54.2">alliez</w>, <w n="54.3">dans</w> <w n="54.4">la</w> <w n="54.5">mort</w> <w n="54.6">aux</w> <w n="54.7">épaisses</w> <w n="54.8">ténèbres</w>,</l>
						<l n="55" num="3.17"><w n="55.1">Connaître</w> <w n="55.2">enfin</w> <w n="55.3">la</w> <w n="55.4">paix</w> <w n="55.5">inconnue</w> <w n="55.6">aux</w> <w n="55.7">vivants</w>,</l>
						<l n="56" num="3.18"><w n="56.1">Et</w> <w n="56.2">que</w>, <w n="56.3">dans</w> <w n="56.4">vos</w> <w n="56.5">cercueils</w>, <w n="56.6">seuls</w> <w n="56.7">et</w> <w n="56.8">sans</w> <w n="56.9">mouvements</w>,</l>
						<l n="57" num="3.19"><w n="57.1">Vous</w> <w n="57.2">goûteriez</w> <w n="57.3">ce</w> <w n="57.4">calme</w>, <w n="57.5">énigme</w> <w n="57.6">poursuivie</w>,</l>
						<l n="58" num="3.20"><w n="58.1">Que</w> <w n="58.2">la</w> <w n="58.3">mort</w> <w n="58.4">ne</w> <w n="58.5">veut</w> <w n="58.6">pas</w> <w n="58.7">expliquer</w> <w n="58.8">à</w> <w n="58.9">la</w> <w n="58.10">vie</w> ?</l>
						<l n="59" num="3.21"><w n="59.1">Eh</w> <w n="59.2">bien</w>, <w n="59.3">détrompez</w>-<w n="59.4">vous</w> <w n="59.5">et</w> <w n="59.6">sortez</w> <w n="59.7">du</w> <w n="59.8">repos</w> !</l>
						<l n="60" num="3.22"><w n="60.1">Réveillez</w>-<w n="60.2">vous</w>, <w n="60.3">ô</w> <w n="60.4">morts</w> ! <w n="60.5">et</w> <w n="60.6">rassemblez</w> <w n="60.7">vos</w> <w n="60.8">os</w> !</l>
						<l n="61" num="3.23"><w n="61.1">Le</w> <w n="61.2">tracas</w> <w n="61.3">vous</w> <w n="61.4">poursuit</w> <w n="61.5">jusque</w> <w n="61.6">dans</w> <w n="61.7">votre</w> <w n="61.8">tombe</w> ;</l>
						<l n="62" num="3.24"><w n="62.1">Il</w> <w n="62.2">faut</w> <w n="62.3">qu</w>’<w n="62.4">une</w> <w n="62.5">autre</w> <w n="62.6">fois</w>, <w n="62.7">sur</w> <w n="62.8">vous</w> <w n="62.9">la</w> <w n="62.10">terre</w> <w n="62.11">tombe</w> !</l>
						<l n="63" num="3.25"><w n="63.1">Allons</w> ! <w n="63.2">interrompez</w>, <w n="63.3">ô</w> <w n="63.4">morts</w>, <w n="63.5">votre</w> <w n="63.6">sommeil</w>,</l>
						<l n="64" num="3.26"><w n="64.1">Sortez</w> <w n="64.2">de</w> <w n="64.3">votre</w> <w n="64.4">nuit</w>, <w n="64.5">paraissez</w> <w n="64.6">au</w> <w n="64.7">soleil</w>,</l>
						<l n="65" num="3.27"><w n="65.1">Rouvrez</w> <w n="65.2">à</w> <w n="65.3">ses</w> <w n="65.4">clartés</w> <w n="65.5">vos</w> <w n="65.6">grands</w> <w n="65.7">yeux</w> <w n="65.8">sans</w> <w n="65.9">prunelles</w>.</l>
						<l n="66" num="3.28"><w n="66.1">Remontez</w> <w n="66.2">au</w> <w n="66.3">séjour</w> <w n="66.4">des</w> <w n="66.5">haines</w> <w n="66.6">éternelles</w>,</l>
						<l n="67" num="3.29"><w n="67.1">Vous</w> <w n="67.2">étiez</w> <w n="67.3">bien</w>, <w n="67.4">couchés</w> ? <w n="67.5">mais</w> <w n="67.6">c</w>’<w n="67.7">est</w> <w n="67.8">assez</w> <w n="67.9">dormir</w> ;</l>
						<l n="68" num="3.30"><w n="68.1">O</w> <w n="68.2">morts</w> ! <w n="68.3">réveillez</w>-<w n="68.4">vous</w> ! <w n="68.5">les</w> <w n="68.6">maçons</w> <w n="68.7">vont</w> <w n="68.8">venir</w> !</l>
						<l n="69" num="3.31"><w n="69.1">Il</w> <w n="69.2">faut</w>, <w n="69.3">entendez</w>-<w n="69.4">vous</w> ? — <w n="69.5">il</w> <w n="69.6">faut</w> <w n="69.7">leur</w> <w n="69.8">faire</w> <w n="69.9">place</w> ;</l>
						<l n="70" num="3.32"><w n="70.1">Vous</w> <w n="70.2">les</w> <w n="70.3">gênez</w>, <w n="70.4">enfin</w> ! — <w n="70.5">ils</w> <w n="70.6">vont</w> <w n="70.7">venir</w> <w n="70.8">en</w> <w n="70.9">masse</w>,</l>
						<l n="71" num="3.33"><w n="71.1">Le</w> <w n="71.2">plan</w> <w n="71.3">dans</w> <w n="71.4">une</w> <w n="71.5">main</w> <w n="71.6">et</w> <w n="71.7">dans</w> <w n="71.8">l</w>’<w n="71.9">autre</w> <w n="71.10">un</w> <w n="71.11">marteau</w>,</l>
						<l n="72" num="3.34"><w n="72.1">Pour</w> <w n="72.2">ouvrir</w> <w n="72.3">une</w> <w n="72.4">voie</w>, <w n="72.5">effondrer</w> <w n="72.6">un</w> <w n="72.7">tombeau</w> !</l>
						<l n="73" num="3.35"><w n="73.1">Avec</w> <w n="73.2">un</w> <w n="73.3">mot</w> <w n="73.4">en</w> <w n="73.5">vain</w> <w n="73.6">vous</w> <w n="73.7">croyez</w> <w n="73.8">les</w> <w n="73.9">confondre</w> ;</l>
						<l n="74" num="3.36">— « <w n="74.1">Le</w> <w n="74.2">respect</w> ! » <w n="74.3">dites</w>-<w n="74.4">vous</w> ? — « <w n="74.5">Le</w> <w n="74.6">plan</w> ! » <w n="74.7">vont</w>-<w n="74.8">ils</w> <w n="74.9">répondre</w>.</l>
						<l n="75" num="3.37"><w n="75.1">Aussi</w> <w n="75.2">gardez</w>-<w n="75.3">vous</w> <w n="75.4">bien</w> <w n="75.5">d</w>’<w n="75.6">invoquer</w> <w n="75.7">le</w> <w n="75.8">respect</w>,</l>
						<l n="76" num="3.38"><w n="76.1">En</w> <w n="76.2">face</w> <w n="76.3">de</w> <w n="76.4">la</w> <w n="76.5">loi</w> <w n="76.6">le</w> <w n="76.7">mort</w> <w n="76.8">serait</w> <w n="76.9">suspect</w> !</l>
						<l n="77" num="3.39"><w n="77.1">Le</w> <w n="77.2">vote</w> <w n="77.3">du</w> <w n="77.4">Sénat</w> <w n="77.5">déclare</w>, <w n="77.6">sans</w> <w n="77.7">réplique</w>,</l>
						<l n="78" num="3.40"><w n="78.1">La</w> <w n="78.2">profanation</w> <w n="78.3">d</w>’<w n="78.4">utilité</w> <w n="78.5">publique</w> !</l>
					</lg>
					<lg n="4">
						<l n="79" num="4.1"><w n="79.1">Ce</w> <w n="79.2">que</w> <w n="79.3">n</w>’<w n="79.4">ont</w> <w n="79.5">jamais</w> <w n="79.6">vu</w> <w n="79.7">tous</w> <w n="79.8">les</w> <w n="79.9">siècles</w> <w n="79.10">passés</w>,</l>
						<l n="80" num="4.2"><w n="80.1">Le</w> <w n="80.2">nôtre</w> <w n="80.3">l</w>’<w n="80.4">aura</w> <w n="80.5">vu</w> ; <w n="80.6">chasser</w> <w n="80.7">les</w> <w n="80.8">trépassés</w> !</l>
						<l n="81" num="4.3"><w n="81.1">Jadis</w>, <w n="81.2">les</w> <w n="81.3">citoyens</w> <w n="81.4">de</w> <w n="81.5">la</w> <w n="81.6">Rome</w> <w n="81.7">païenne</w>,</l>
						<l n="82" num="4.4"><w n="82.1">A</w> <w n="82.2">manteau</w> <w n="82.3">laticlave</w> <w n="82.4">ou</w> <w n="82.5">robe</w> <w n="82.6">plébéienne</w> ;</l>
						<l n="83" num="4.5"><w n="83.1">Noblesse</w> <w n="83.2">ou</w> <w n="83.3">populace</w>, <w n="83.4">honoraient</w> <w n="83.5">saintement</w></l>
						<l n="84" num="4.6"><w n="84.1">La</w> <w n="84.2">dépouille</w> <w n="84.3">des</w> <w n="84.4">morts</w> ; — <w n="84.5">leurs</w> <w n="84.6">mains</w> <w n="84.7">pieusement</w>,</l>
						<l n="85" num="4.7"><w n="85.1">La</w> <w n="85.2">recueillaient</w> <w n="85.3">dans</w> <w n="85.4">l</w>’<w n="85.5">urne</w> <w n="85.6">avec</w> <w n="85.7">soin</w> <w n="85.8">parfumée</w>,</l>
						<l n="86" num="4.8"><w n="86.1">Et</w> <w n="86.2">la</w> <w n="86.3">maison</w> <w n="86.4">gardait</w> <w n="86.5">la</w> <w n="86.6">cendre</w> <w n="86.7">bien</w>-<w n="86.8">aimée</w>.</l>
						<l n="87" num="4.9"><w n="87.1">Le</w> <w n="87.2">mort</w> <w n="87.3">ne</w> <w n="87.4">quittait</w> <w n="87.5">point</w> <w n="87.6">le</w> <w n="87.7">parent</w> <w n="87.8">qui</w> <w n="87.9">l</w>’<w n="87.10">avait</w></l>
						<l n="88" num="4.10"><w n="88.1">Tendrement</w> <w n="88.2">consolé</w>, <w n="88.3">chéri</w>, <w n="88.4">lorsqu</w>’<w n="88.5">il</w> <w n="88.6">vivait</w>.</l>
						<l n="89" num="4.11"><w n="89.1">Son</w> <w n="89.2">souvenir</w> <w n="89.3">peuplait</w> <w n="89.4">la</w> <w n="89.5">maison</w> <w n="89.6">mortuaire</w>,</l>
						<l n="90" num="4.12"><w n="90.1">Et</w> <w n="90.2">défiait</w> <w n="90.3">l</w>’<w n="90.4">oubli</w>, — <w n="90.5">ce</w> <w n="90.6">deuxième</w> <w n="90.7">suaire</w> !</l>
						<l n="91" num="4.13"><w n="91.1">Chacun</w> <w n="91.2">gardait</w> <w n="91.3">ses</w> <w n="91.4">morts</w>, <w n="91.5">craignant</w> <w n="91.6">qu</w>’<w n="91.7">en</w> <w n="91.8">d</w>’<w n="91.9">autres</w> <w n="91.10">lieux</w>,</l>
						<l n="92" num="4.14"><w n="92.1">On</w> <w n="92.2">ne</w> <w n="92.3">sut</w> <w n="92.4">protéger</w> <w n="92.5">leurs</w> <w n="92.6">restes</w> <w n="92.7">précieux</w>,</l>
						<l n="93" num="4.15"><w n="93.1">Et</w> <w n="93.2">non</w> <w n="93.3">pas</w> <w n="93.4">seulement</w> <w n="93.5">du</w> <w n="93.6">temps</w> <w n="93.7">où</w> <w n="93.8">Rome</w> <w n="93.9">libre</w>,</l>
						<l n="94" num="4.16"><w n="94.1">Du</w> <w n="94.2">Gange</w> <w n="94.3">à</w> <w n="94.4">l</w>’<w n="94.5">Hellespont</w> <w n="94.6">faisait</w> <w n="94.7">régner</w> <w n="94.8">le</w> <w n="94.9">Tibre</w> ?</l>
						<l n="95" num="4.17"><w n="95.1">Non</w>, <w n="95.2">je</w> <w n="95.3">n</w>’<w n="95.4">en</w> <w n="95.5">parle</w> <w n="95.6">pas</w>, <w n="95.7">mais</w> <w n="95.8">du</w> <w n="95.9">temps</w> <w n="95.10">détesté</w>,</l>
						<l n="96" num="4.18"><w n="96.1">Où</w> <w n="96.2">Rome</w> <w n="96.3">dans</w> <w n="96.4">les</w> <w n="96.5">fers</w> <w n="96.6">pleurait</w> <w n="96.7">sa</w> <w n="96.8">liberté</w>.</l>
						<l n="97" num="4.19"><w n="97.1">On</w> <w n="97.2">prenait</w> <w n="97.3">à</w> <w n="97.4">ses</w> <w n="97.5">fils</w> <w n="97.6">et</w> <w n="97.7">leurs</w> <w n="97.8">biens</w> <w n="97.9">et</w> <w n="97.10">leur</w> <w n="97.11">vie</w>,</l>
						<l n="98" num="4.20"><w n="98.1">On</w> <w n="98.2">menait</w> <w n="98.3">aux</w> <w n="98.4">égouts</w> <w n="98.5">cette</w> <w n="98.6">race</w> <w n="98.7">avilie</w>,</l>
						<l n="99" num="4.21"><w n="99.1">Et</w> <w n="99.2">les</w> <w n="99.3">plus</w> <w n="99.4">durs</w> <w n="99.5">travaux</w> <w n="99.6">et</w> <w n="99.7">les</w> <w n="99.8">plus</w> <w n="99.9">dures</w> <w n="99.10">lois</w>,</l>
						<l n="100" num="4.22"><w n="100.1">Étaient</w> <w n="100.2">pour</w> <w n="100.3">les</w> <w n="100.4">Romains</w> <w n="100.5">le</w> <w n="100.6">prix</w> <w n="100.7">des</w> <w n="100.8">vieux</w> <w n="100.9">exploits</w> !</l>
						<l n="101" num="4.23"><w n="101.1">Cependant</w> <w n="101.2">les</w> <w n="101.3">tyrans</w> <w n="101.4">de</w> <w n="101.5">ces</w> <w n="101.6">vainqueurs</w> <w n="101.7">du</w> <w n="101.8">monde</w>,</l>
						<l n="102" num="4.24"><w n="102.1">Ne</w> <w n="102.2">heurtèrent</w> <w n="102.3">jamais</w> <w n="102.4">leur</w> <w n="102.5">piété</w> <w n="102.6">profonde</w>,</l>
						<l n="103" num="4.25"><w n="103.1">Car</w> <w n="103.2">ils</w> <w n="103.3">laissaient</w> <w n="103.4">en</w> <w n="103.5">paix</w> <w n="103.6">la</w> <w n="103.7">cendre</w> <w n="103.8">des</w> <w n="103.9">aïeux</w> ;</l>
						<l n="104" num="4.26"><w n="104.1">Ils</w> <w n="104.2">avaient</w> <w n="104.3">un</w> <w n="104.4">mérite</w> ; <w n="104.5">ils</w> <w n="104.6">redoutaient</w> <w n="104.7">les</w> <w n="104.8">Dieux</w> !</l>
					</lg>
					<lg n="5">
						<l n="105" num="5.1"><w n="105.1">Vous</w> <w n="105.2">ne</w> <w n="105.3">redoutez</w> <w n="105.4">rien</w>, <w n="105.5">ni</w> <w n="105.6">les</w> <w n="105.7">Dieux</w>, <w n="105.8">ni</w> <w n="105.9">les</w> <w n="105.10">hommes</w>,</l>
						<l n="106" num="5.2"><w n="106.1">O</w> <w n="106.2">vous</w>, <w n="106.3">démolisseurs</w> ! <w n="106.4">Pas</w> <w n="106.5">même</w> <w n="106.6">les</w> <w n="106.7">fantômes</w></l>
						<l n="107" num="5.3"><w n="107.1">De</w> <w n="107.2">ceux</w> <w n="107.3">que</w> <w n="107.4">vous</w> <w n="107.5">aurez</w> <w n="107.6">chassés</w> <w n="107.7">de</w> <w n="107.8">leurs</w> <w n="107.9">tombeaux</w> !</l>
						<l n="108" num="5.4"><w n="108.1">S</w>’<w n="108.2">ils</w> <w n="108.3">allaient</w> <w n="108.4">revenir</w>, <w n="108.5">drapés</w> <w n="108.6">dans</w> <w n="108.7">les</w> <w n="108.8">lambeaux</w></l>
						<l n="109" num="5.5"><w n="109.1">Du</w> <w n="109.2">suaire</w>, <w n="109.3">tracer</w> <w n="109.4">de</w> <w n="109.5">leur</w> <w n="109.6">doigt</w> <w n="109.7">de</w> <w n="109.8">squelettes</w> :</l>
						<l n="110" num="5.6"><hi rend="ital"><w n="110.1">Mane</w></hi> ! <hi rend="ital"><w n="110.2">Thecel</w></hi> ! <hi rend="ital"><w n="110.3">Pharès</w></hi> ! <w n="110.4">dans</w> <w n="110.5">vos</w> <w n="110.6">palais</w> <w n="110.7">en</w> <w n="110.8">fêtes</w> ?</l>
					</lg>
					<lg n="6">
						<l n="111" num="6.1">— « <w n="111.1">Non</w> ! <w n="111.2">les</w> <w n="111.3">morts</w> <w n="111.4">sont</w> <w n="111.5">bien</w> <w n="111.6">morts</w> ! — <w n="111.7">dites</w>-<w n="111.8">vous</w>. <w n="111.9">Et</w> <w n="111.10">d</w>’<w n="111.11">abord</w>,</l>
						<l n="112" num="6.2"><w n="112.1">Si</w> <w n="112.2">nous</w> <w n="112.3">allons</w> <w n="112.4">fouiller</w> <w n="112.5">dans</w> <w n="112.6">les</w> <w n="112.7">champs</w> <w n="112.8">de</w> <w n="112.9">la</w> <w n="112.10">mort</w>,</l>
						<l n="113" num="6.3"><w n="113.1">C</w>’<w n="113.2">est</w> <w n="113.3">pour</w> <w n="113.4">vous</w>, <w n="113.5">pour</w> <w n="113.6">vous</w> <w n="113.7">seuls</w>. <w n="113.8">Notre</w> <w n="113.9">sollicitude</w>,</l>
						<l n="114" num="6.4"><w n="114.1">Fait</w> <w n="114.2">de</w> <w n="114.3">votre</w> <w n="114.4">bien</w>-<w n="114.5">être</w> <w n="114.6">une</w> <w n="114.7">constante</w> <w n="114.8">étude</w>.</l>
						<l n="115" num="6.5"><w n="115.1">Ingrats</w> ! <w n="115.2">vous</w> <w n="115.3">nous</w> <w n="115.4">blâmez</w> ? <w n="115.5">vos</w> <w n="115.6">neveux</w> <w n="115.7">nous</w> <w n="115.8">loueront</w> ;</l>
						<l n="116" num="6.6"><w n="116.1">C</w>’<w n="116.2">est</w> <w n="116.3">un</w> <w n="116.4">El</w> <w n="116.5">Dorado</w> <w n="116.6">dans</w> <w n="116.7">lequel</w> <w n="116.8">ils</w> <w n="116.9">vivront</w>,</l>
						<l n="117" num="6.7"><w n="117.1">Voyez</w> <w n="117.2">comme</w> <w n="117.3">Paris</w> <w n="117.4">par</w> <w n="117.5">nos</w> <w n="117.6">soins</w> <w n="117.7">se</w> <w n="117.8">transforme</w> !</l>
						<l n="118" num="6.8"><w n="118.1">Nous</w> <w n="118.2">sommes</w> <w n="118.3">des</w> <w n="118.4">géants</w> <w n="118.5">et</w> <w n="118.6">notre</w> <w n="118.7">œuvre</w> <w n="118.8">est</w> <w n="118.9">énorme</w> !</l>
						<l n="119" num="6.9"><w n="119.1">L</w>’<w n="119.2">Europe</w> <w n="119.3">émerveillée</w> <w n="119.4">admire</w> <w n="119.5">nos</w> <w n="119.6">travaux</w>.</l>
						<l n="120" num="6.10"><w n="120.1">Nous</w> <w n="120.2">vous</w> <w n="120.3">faisons</w> <w n="120.4">un</w> <w n="120.5">peuple</w>, <w n="120.6">un</w> <w n="120.7">peuple</w> <w n="120.8">sans</w> <w n="120.9">rivaux</w>.</l>
						<l n="121" num="6.11"><w n="121.1">Cessez</w> <w n="121.2">de</w> <w n="121.3">murmurer</w> <w n="121.4">et</w> <w n="121.5">sachez</w> <w n="121.6">reconnaître</w>,</l>
						<l n="122" num="6.12"><w n="122.1">Que</w> <w n="122.2">nous</w> <w n="122.3">n</w>’<w n="122.4">avons</w> <w n="122.5">qu</w>’<w n="122.6">un</w> <w n="122.7">but</w>, <w n="122.8">un</w> <w n="122.9">seul</w>, <w n="122.10">votre</w> <w n="122.11">bien</w>-<w n="122.12">être</w>. »</l>
					</lg>
					<lg n="7">
						<l n="123" num="7.1"><w n="123.1">Merci</w> ! <w n="123.2">mais</w> <w n="123.3">s</w>’<w n="123.4">il</w> <w n="123.5">vous</w> <w n="123.6">faut</w> <w n="123.7">pour</w> <w n="123.8">cela</w>, <w n="123.9">pour</w> <w n="123.10">vos</w> <w n="123.11">plans</w></l>
						<l n="124" num="7.2"><w n="124.1">Fouiller</w> <w n="124.2">le</w> <w n="124.3">sol</w> <w n="124.4">béni</w> <w n="124.5">de</w> <w n="124.6">ces</w> <w n="124.7">pauvres</w> <w n="124.8">os</w> <w n="124.9">blancs</w>,</l>
						<l n="125" num="7.3"><w n="125.1">S</w>’<w n="125.2">il</w> <w n="125.3">vous</w> <w n="125.4">faut</w> <w n="125.5">morceler</w> <w n="125.6">la</w> <w n="125.7">terre</w> <w n="125.8">des</w> <w n="125.9">reliques</w>,</l>
						<l n="126" num="7.4"><w n="126.1">Dirigez</w> <w n="126.2">vos</w> <w n="126.3">cordeaux</w> <w n="126.4">sur</w> <w n="126.5">des</w> <w n="126.6">lignes</w> <w n="126.7">obliques</w></l>
						<l n="127" num="7.5"><w n="127.1">Et</w> <w n="127.2">laissez</w> <w n="127.3">les</w> <w n="127.4">tombeaux</w>, <w n="127.5">nous</w> <w n="127.6">vous</w> <w n="127.7">applaudirons</w>,</l>
						<l n="128" num="7.6"><w n="128.1">Et</w> <w n="128.2">s</w>’<w n="128.3">il</w> <w n="128.4">vous</w> <w n="128.5">faut</w> <w n="128.6">des</w> <w n="128.7">bras</w> <w n="128.8">nous</w> <w n="128.9">vous</w> <w n="128.10">les</w> <w n="128.11">fournirons</w>.</l>
						<l n="129" num="7.7"><w n="129.1">Mais</w> <w n="129.2">tant</w> <w n="129.3">que</w> <w n="129.4">vous</w> <w n="129.5">voudrez</w> <w n="129.6">profaner</w> <w n="129.7">les</w> <w n="129.8">enceintes</w>,</l>
						<l n="130" num="7.8"><w n="130.1">Où</w> <w n="130.2">dorment</w> <w n="130.3">les</w> <w n="130.4">débris</w> <w n="130.5">des</w> <w n="130.6">affections</w> <w n="130.7">saintes</w>,</l>
						<l n="131" num="7.9"><w n="131.1">Comment</w> <w n="131.2">vous</w> <w n="131.3">applaudir</w>, <w n="131.4">ô</w> <w n="131.5">vous</w>, <w n="131.6">qui</w> <w n="131.7">sans</w> <w n="131.8">remords</w>,</l>
						<l n="132" num="7.10"><w n="132.1">Gâchez</w> <w n="132.2">votre</w> <w n="132.3">mortier</w> <w n="132.4">des</w> <w n="132.5">cendres</w> <w n="132.6">de</w> <w n="132.7">nos</w> <w n="132.8">morts</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1868">21 Janvier 1868</date>.
						</dateline>
						<note type="footnote" id="1">Il s’agissait de la création d’un boulevard qui aurait traversé le cimetière Montmartre.</note>
					</closer>
				</div></body></text></TEI>