<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC72">
					<head type="main">La Nostalgie de Joko</head>
					<head type="sub">Le Singe de l’Impératrice</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">étais</w> <w n="1.3">le</w> <w n="1.4">plus</w> <w n="1.5">gai</w> <w n="1.6">des</w> <w n="1.7">Jokos</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Là</w>-<w n="2.2">bas</w>, <w n="2.3">vers</w> <w n="2.4">nos</w> <w n="2.5">riantes</w> <w n="2.6">côtes</w>,</l>
						<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">m</w>’<w n="3.3">enlève</w> <w n="3.4">aux</w> <w n="3.5">bois</w> <w n="3.6">de</w> <w n="3.7">cocos</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">la</w> <w n="4.3">grand</w>’<w n="4.4">ville</w> <w n="4.5">des</w> <w n="4.6">cocotes</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Malgré</w> <w n="5.2">le</w> <w n="5.3">pompeux</w> <w n="5.4">appareil</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Dont</w> <w n="6.2">m</w>’<w n="6.3">entourent</w> <w n="6.4">des</w> <w n="6.5">mains</w> <w n="6.6">illustres</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">regrette</w> <w n="7.3">mon</w> <w n="7.4">beau</w> <w n="7.5">soleil</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Que</w> <w n="8.2">ne</w> <w n="8.3">remplacent</w> <w n="8.4">point</w> <w n="8.5">les</w> <w n="8.6">lustres</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">je</w> <w n="9.3">rêve</w>, <w n="9.4">et</w> <w n="9.5">je</w> <w n="9.6">pleure</w> <w n="9.7">encor</w>,</l>
						<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">songeant</w> <w n="10.3">aux</w> <w n="10.4">rives</w> <w n="10.5">lointaines</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="4"></space><w n="11.1">Car</w> <w n="11.2">mes</w> <w n="11.3">chaînes</w> <w n="11.4">pour</w> <w n="11.5">d</w>’<w n="11.6">or</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Hélas</w> ! <w n="12.2">n</w>’<w n="12.3">en</w> <w n="12.4">sont</w> <w n="12.5">pas</w> <w n="12.6">moins</w> <w n="12.7">des</w> <w n="12.8">chaînes</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Oh</w> ! <w n="13.2">le</w> <w n="13.3">grand</w> <w n="13.4">bois</w> <w n="13.5">tout</w> <w n="13.6">parfumé</w> !</l>
						<l n="14" num="4.2"><w n="14.1">Quand</w> <w n="14.2">la</w> <w n="14.3">guenon</w> <w n="14.4">dans</w> <w n="14.5">la</w> <w n="14.6">nuit</w> <w n="14.7">bleue</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Guette</w> <w n="15.2">l</w>’<w n="15.3">approche</w> <w n="15.4">de</w> <w n="15.5">l</w>’<w n="15.6">aimé</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Pendue</w> <w n="16.2">aux</w> <w n="16.3">branches</w>… <w n="16.4">par</w> <w n="16.5">la</w> <w n="16.6">queue</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Si</w> <w n="17.2">pour</w> <w n="17.3">distraire</w> <w n="17.4">mes</w> <w n="17.5">ennuis</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Dans</w> <w n="18.2">le</w> <w n="18.3">palais</w>, <w n="18.4">où</w> <w n="18.5">je</w> <w n="18.6">soupire</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Si</w> <w n="19.2">pour</w> <w n="19.3">oublier</w> <w n="19.4">où</w> <w n="19.5">je</w> <w n="19.6">suis</w>.</l>
						<l n="20" num="5.4"><w n="20.1">La</w> <w n="20.2">nouveauté</w> <w n="20.3">venait</w> <w n="20.4">m</w>’<w n="20.5">instruîre</w> ?</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Mais</w>, <w n="21.2">non</w> ! <w n="21.3">ici</w> <w n="21.4">comme</w> <w n="21.5">là</w>-<w n="21.6">bas</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Vers</w> <w n="22.2">les</w> <w n="22.3">palmiers</w> <w n="22.4">qui</w> <w n="22.5">m</w>’<w n="22.6">ont</w> <w n="22.7">vu</w> <w n="22.8">naître</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Ce</w> <w n="23.2">sont</w> <w n="23.3">des</w> <w n="23.4">bonds</w> <w n="23.5">et</w> <w n="23.6">des</w> <w n="23.7">ébats</w></l>
						<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">courtisans</w> <w n="24.3">sous</w> <w n="24.4">l</w>’<w n="24.5">œil</w> <w n="24.6">du</w> <w n="24.7">maître</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Comme</w> <w n="25.2">ils</w> <w n="25.3">sautent</w> ! <w n="25.4">qu</w>’<w n="25.5">ils</w> <w n="25.6">sont</w> <w n="25.7">adroits</w> !</l>
						<l n="26" num="7.2"><w n="26.1">Oh</w> ! <w n="26.2">que</w> <w n="26.3">ces</w> <w n="26.4">hommes</w> <w n="26.5">sont</w> <w n="26.6">habiles</w> !</l>
						<l n="27" num="7.3"><w n="27.1">On</w> <w n="27.2">ne</w> <w n="27.3">saurait</w> <w n="27.4">trouver</w>, <w n="27.5">je</w> <w n="27.6">crois</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Des</w> <w n="28.2">plus</w> <w n="28.3">souples</w>, <w n="28.4">mains</w> <w n="28.5">plus</w> <w n="28.6">agiles</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Je</w> <w n="29.2">disais</w> : — « <w n="29.3">Oublions</w> <w n="29.4">nos</w> <w n="29.5">maux</w>,</l>
						<l n="30" num="8.2"><w n="30.1">En</w> <w n="30.2">étudiant</w> <w n="30.3">où</w> <w n="30.4">nous</w> <w n="30.5">sommes</w>. »</l>
						<l n="31" num="8.3"><w n="31.1">Mais</w> <w n="31.2">je</w> <w n="31.3">trouve</w> <w n="31.4">encor</w> <w n="31.5">des</w> <w n="31.6">Jokos</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Où</w> <w n="32.2">je</w> <w n="32.3">croyais</w> <w n="32.4">trouver</w> <w n="32.5">des</w> <w n="32.6">hommes</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">tout</w> <w n="33.3">honteux</w> <w n="33.4">je</w> <w n="33.5">me</w> <w n="33.6">tiens</w> <w n="33.7">coi</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Et</w> <w n="34.2">je</w> <w n="34.3">rougis</w> <w n="34.4">et</w> <w n="34.5">je</w> <w n="34.6">sanglote</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">pour</w> <w n="35.3">avoir</w> <w n="35.4">pitié</w> <w n="35.5">de</w> <w n="35.6">moi</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Je</w> <w n="36.2">n</w>’<w n="36.3">ai</w> <w n="36.4">pas</w> <w n="36.5">la</w> <w n="36.6">moindre</w> <w n="36.7">Jokote</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Courtisans</w>, <w n="37.2">sautez</w> <w n="37.3">tour</w> <w n="37.4">à</w> <w n="37.5">tour</w>.</l>
						<l n="38" num="10.2"><w n="38.1">Sous</w> <w n="38.2">l</w>’<w n="38.3">habit</w> <w n="38.4">brodé</w>, <w n="38.5">le</w> <w n="38.6">beau</w> <w n="38.7">linge</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Mais</w> <w n="39.2">craignez</w>, <w n="39.3">que</w> <w n="39.4">le</w> <w n="39.5">maître</w>, <w n="39.6">un</w> <w n="39.7">jour</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Ne</w> <w n="40.2">vous</w> <w n="40.3">paie</w> <w n="40.4">en</w> <w n="40.5">monnai</w><ref type="noteAnchor" target="(e)">(e)</ref> <w n="40.6">de</w> <w n="40.7">singe</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">J</w>’<w n="41.2">étais</w> <w n="41.3">le</w> <w n="41.4">plus</w> <w n="41.5">gai</w> <w n="41.6">des</w> <w n="41.7">Jokos</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Là</w>-<w n="42.2">bas</w>, <w n="42.3">vers</w> <w n="42.4">nos</w> <w n="42.5">riantes</w> <w n="42.6">côtes</w>,</l>
						<l n="43" num="11.3"><w n="43.1">On</w> <w n="43.2">m</w>’<w n="43.3">enlève</w> <w n="43.4">aux</w> <w n="43.5">bois</w> <w n="43.6">de</w> <w n="43.7">cocos</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Pour</w> <w n="44.2">la</w> <w n="44.3">grand</w>’<w n="44.4">ville</w> <w n="44.5">des</w> <w n="44.6">cocotes</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1869">21 Décembre 1869</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>