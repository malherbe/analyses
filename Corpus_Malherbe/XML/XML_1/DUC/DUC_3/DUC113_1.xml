<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">Troisième partie</head><head type="sub_part">(1871-1885)</head><div type="poem" key="DUC113">
					<head type="main">A propos de la " Marseillaise "</head>
					<div type="section" n="1">
						<head type="main">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">cet</w> <w n="1.3">hymne</w> <w n="1.4">comme</w> <w n="1.5">un</w> <w n="1.6">tonnerre</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Gronda</w> <w n="2.2">dans</w> <w n="2.3">l</w>’<w n="2.4">âme</w> <w n="2.5">de</w> <hi rend="ital"><w n="2.6">Rouget</w></hi>,</l>
							<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">tyranie</w> <w n="3.3">encor</w> <w n="3.4">forgeait</w></l>
							<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">armes</w> <w n="4.3">pour</w> <w n="4.4">le</w> <w n="4.5">mercenaire</w> ;</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Bien</w> <w n="5.2">des</w> <w n="5.3">siècles</w> <w n="5.4">d</w>’<w n="5.5">oppression</w></l>
							<l n="6" num="2.2"><w n="6.1">Tenaient</w> <w n="6.2">les</w> <w n="6.3">peuples</w> <w n="6.4">en</w> <w n="6.5">servage</w>.</l>
							<l n="7" num="2.3"><w n="7.1">Partout</w> <w n="7.2">le</w> <w n="7.3">stupide</w> <w n="7.4">esclavage</w></l>
							<l n="8" num="2.4"><w n="8.1">Déshonorait</w> <w n="8.2">les</w> <w n="8.3">nations</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Comme</w> <w n="9.2">la</w> <w n="9.3">cavale</w> <w n="9.4">farouche</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Qui</w> <w n="10.2">sous</w> <w n="10.3">l</w>’<w n="10.4">éperon</w> <w n="10.5">d</w>’<w n="10.6">or</w> <w n="10.7">bondit</w>,</l>
							<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">France</w>, <w n="11.3">en</w> <w n="11.4">se</w> <w n="11.5">cabrant</w>, <w n="11.6">mordit</w></l>
							<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">frein</w> <w n="12.3">qui</w> <w n="12.4">lui</w> <w n="12.5">tordait</w> <w n="12.6">la</w> <w n="12.7">bouche</w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Elle</w> <w n="13.2">eût</w> <w n="13.3">des</w> <w n="13.4">appels</w> <w n="13.5">de</w> <w n="13.6">lion</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">convia</w>, <w n="14.3">superbe</w>, <w n="14.4">altière</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Les</w> <w n="15.2">peuples</w> <w n="15.3">de</w> <w n="15.4">l</w>’<w n="15.5">Europe</w> <w n="15.6">entière</w>,</l>
							<l n="16" num="4.4"><w n="16.1">A</w> <w n="16.2">sa</w> <w n="16.3">sainte</w> <w n="16.4">rébellion</w> !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Mais</w> <w n="17.2">les</w> <w n="17.3">peuples</w>, <w n="17.4">dans</w> <w n="17.5">l</w>’<w n="17.6">ignorance</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Ne</w> <w n="18.2">comprirent</w> <w n="18.3">pas</w> <w n="18.4">ses</w> <w n="18.5">accents</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">sous</w> <w n="19.3">le</w> <w n="19.4">bâton</w> <w n="19.5">des</w> <w n="19.6">puissants</w>,</l>
							<l n="20" num="5.4"><w n="20.1">Us</w> <w n="20.2">marchèrent</w> <w n="20.3">contre</w> <w n="20.4">la</w> <w n="20.5">France</w> !</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Ils</w> <w n="21.2">voulaient</w> <w n="21.3">renverser</w> <w n="21.4">l</w>’<w n="21.5">autel</w></l>
							<l n="22" num="6.2"><w n="22.1">De</w> <w n="22.2">notre</w> <w n="22.3">liberté</w> <w n="22.4">naissante</w> !…</l>
							<l n="23" num="6.3"><w n="23.1">Mais</w> <w n="23.2">soudain</w> <w n="23.3">de</w> <w n="23.4">sa</w> <w n="23.5">voix</w> <w n="23.6">puissante</w>,</l>
							<l n="24" num="6.4"><hi rend="ital"><w n="24.1">Rouget</w></hi> <w n="24.2">jette</w> <w n="24.3">un</w> <w n="24.4">chant</w> <w n="24.5">immortel</w> ;</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">L</w>’<w n="25.2">hymne</w> <w n="25.3">sacré</w> <w n="25.4">de</w> <w n="25.5">la</w> <w n="25.6">Patrie</w> ;</l>
							<l n="26" num="7.2"><w n="26.1">La</w> <hi rend="ital"><w n="26.2">Marseillaise</w></hi> ! — <w n="26.3">Son</w> <w n="26.4">refrain</w></l>
							<l n="27" num="7.3"><w n="27.1">Dominant</w> <w n="27.2">les</w> <w n="27.3">canons</w> <w n="27.4">d</w>’<w n="27.5">airain</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Des</w> <w n="28.2">rois</w> <w n="28.3">va</w> <w n="28.4">braver</w> <w n="28.5">la</w> <w n="28.6">furie</w> !</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">les</w> <w n="29.3">outrages</w> <w n="29.4">essuyés</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Les</w> <w n="30.2">hontes</w> <w n="30.3">vingt</w> <w n="30.4">fois</w> <w n="30.5">séculaires</w>,</l>
							<l n="31" num="8.3"><w n="31.1">Par</w> <w n="31.2">l</w>’<w n="31.3">hymne</w> <w n="31.4">des</w> <w n="31.5">saintes</w> <w n="31.6">colères</w>,</l>
							<l n="32" num="8.4"><w n="32.1">Avec</w> <w n="32.2">les</w> <w n="32.3">rois</w> <w n="32.4">sont</w> <w n="32.5">balayés</w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="main">II</head>
						<lg n="1">
							<l n="33" num="1.1"><w n="33.1">Ce</w> <w n="33.2">noble</w> <w n="33.3">chant</w> <w n="33.4">qu</w>’<w n="33.5">on</w> <w n="33.6">nous</w> <w n="33.7">jalouse</w>,</l>
							<l n="34" num="1.2"><w n="34.1">Peuple</w>, <w n="34.2">ne</w> <w n="34.3">le</w> <w n="34.4">profane</w> <w n="34.5">pas</w> !</l>
							<l n="35" num="1.3"><w n="35.1">Il</w> <w n="35.2">fit</w> <w n="35.3">affronter</w> <w n="35.4">le</w> <w n="35.5">trépas</w>,</l>
							<l n="36" num="1.4"><w n="36.1">Aux</w> <w n="36.2">hommes</w> <w n="36.3">de</w> <hi rend="ital"><w n="36.4">Quatre</w>-<w n="36.5">vingt</w>-<w n="36.6">douze</w> </hi></l>
						</lg>
						<lg n="2">
							<l n="37" num="2.1"><w n="37.1">A</w> <w n="37.2">vaincre</w> <w n="37.3">ou</w> <w n="37.4">mourir</w> <w n="37.5">toujours</w> <w n="37.6">prêts</w>,</l>
							<l n="38" num="2.2"><w n="38.1">Quand</w> <w n="38.2">leur</w> <w n="38.3">sang</w> <w n="38.4">coulait</w> <w n="38.5">aux</w> <w n="38.6">mitrailles</w>,</l>
							<l n="39" num="2.3"><w n="39.1">Ils</w> <w n="39.2">l</w>’<w n="39.3">entonnaient</w> ! — <w n="39.4">Et</w> <w n="39.5">tu</w> <w n="39.6">le</w> <w n="39.7">brailles</w>,</l>
							<l n="40" num="2.4"><w n="40.1">Quand</w> <w n="40.2">le</w> <w n="40.3">vin</w> <w n="40.4">coule</w> <w n="40.5">aux</w> <w n="40.6">cabarets</w> !</l>
						</lg>
						<lg n="3">
							<l n="41" num="3.1"><w n="41.1">Tu</w> <w n="41.2">profanes</w> <w n="41.3">ses</w> <w n="41.4">origines</w>,</l>
							<l n="42" num="3.2"><w n="42.1">En</w> <w n="42.2">le</w> <hi rend="ital"><w n="42.3">hurlant</w></hi> <w n="42.4">à</w> <w n="42.5">tous</w> <w n="42.6">propos</w>.</l>
							<l n="43" num="3.3"><w n="43.1">C</w>’<w n="43.2">est</w> <w n="43.3">lorsque</w> <w n="43.4">flottent</w> <w n="43.5">nos</w> <w n="43.6">drapeaux</w>,</l>
							<l n="44" num="3.4"><w n="44.1">Qu</w>’<w n="44.2">il</w> <w n="44.3">doit</w> <w n="44.4">sortir</w> <w n="44.5">de</w> <w n="44.6">nos</w> <w n="44.7">poitrines</w> ;</l>
						</lg>
						<lg n="4">
							<l n="45" num="4.1"><w n="45.1">Ce</w> <w n="45.2">n</w>’<w n="45.3">est</w> <w n="45.4">qu</w>’<w n="45.5">à</w> <w n="45.6">l</w>’<w n="45.7">heure</w> <w n="45.8">du</w> <w n="45.9">danger</w>,</l>
							<l n="46" num="4.2"><w n="46.1">Que</w>, <w n="46.2">suprême</w> <w n="46.3">cri</w> <w n="46.4">d</w>’<w n="46.5">espérance</w>,</l>
							<l n="47" num="4.3"><w n="47.1">Il</w> <w n="47.2">doit</w> — <hi rend="ital"><w n="47.3">entonné</w></hi> <w n="47.4">par</w> <w n="47.5">la</w> <w n="47.6">France</w>,</l>
							<l n="48" num="4.4"><w n="48.1">Faire</w> <w n="48.2">reculer</w> <w n="48.3">l</w>’<w n="48.4">étranger</w> !</l>
						</lg>
						<closer>
							<placeName>Paris</placeName>
						</closer>
					</div>
				</div></body></text></TEI>