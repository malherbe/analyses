<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Étrivières</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2017</date>
				<idno type="local">DUC_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Étrivières</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
						<publicationStmt>
							<publisher>gallica.bnf.fr</publisher>
							<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k373777b.r=alexandre%20Ducros%20les%20%C3%A9trivi%C3%A8res?rk=21459;2</idno>
						</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Étrivières</title>
								<title>(1867-1885)</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>A. GAUTHERIN</publisher>
									<date when="1898">1898</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>IMPRIMERIE ALCAN-LEVY</publisher>
							<date when="1870">1870</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les Étrivières</title>
						<title>(1862-1872)</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>A LECHEVALIER, LIBRAIRE-ÉDITEUR</publisher>
							<date when="1875">1875</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date from="1867" to="1885">1867-1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface ainsi que les autres parties liminaires ne sont pas reprises dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2018-09-05" who="RR">Correction d’un vers dans le poème "Le Maréchal Lebœuf" à partir de l’édition de 1870.</change>
				<change when="2019-04-02" who="RR">Dans l’édition de 1875, le dernier vers de "La Siffétéide" n’est pas traité comme un vers clausule.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Le Roi boit" à partir de l’édition de 1875.</change>
				<change when="2019-04-02" who="RR">Correction d’un vers faux dans "Nous avons la guerre" à partir de l’édition de 1875.</change>

			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">PREMIÈRE PARTIE</head><head type="sub_part">(1867-1870)</head><div type="poem" key="DUC78">
					<head type="main">A "hue ! à dia !"</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Bon</w> ! <w n="1.2">voilà</w> <w n="1.3">que</w> <w n="1.4">le</w> <w n="1.5">Char</w> <w n="1.6">accroche</w>…</l>
						<l n="2" num="1.2"><w n="2.1">Oh</w> ! <w n="2.2">mes</w> <w n="2.3">amis</w>, <w n="2.4">quel</w> <w n="2.5">aria</w> !</l>
						<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">tire</w> <w n="3.3">à</w> <w n="3.4">droite</w>, <w n="3.5">on</w> <w n="3.6">tire</w> <w n="3.7">à</w> <w n="3.8">gauche</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Fouette</w> <w n="4.2">cocher</w> ! « <w n="4.3">à</w> <w n="4.4">hue</w> ! <w n="4.5">à</w> <w n="4.6">dia</w> ! »</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">le</w> <w n="5.3">pauvre</w> <w n="5.4">diable</w> <w n="5.5">qu</w>’<w n="5.6">on</w> <w n="5.7">mène</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Par</w> <w n="6.2">les</w> <w n="6.3">cahots</w> <w n="6.4">est</w> <w n="6.5">ballotté</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">sur</w> <w n="7.3">le</w> <w n="7.4">mont</w> <w n="7.5">ou</w> <w n="7.6">dans</w> <w n="7.7">la</w> <w n="7.8">plaine</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Il</w> <w n="8.2">penche</w> <w n="8.3">toujours</w> <w n="8.4">d</w>’<w n="8.5">un</w> <w n="8.6">côté</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Soit</w> <w n="9.2">qu</w>’<w n="9.3">il</w> <w n="9.4">suive</w> <w n="9.5">la</w> <w n="9.6">ligne</w> <w n="9.7">courbe</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Ou</w> <w n="10.2">la</w> <w n="10.3">ligne</w> <w n="10.4">droite</w>, — <w n="10.5">ma</w> <w n="10.6">foi</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">Char</w> <w n="11.3">de</w> <w n="11.4">plus</w> <w n="11.5">en</w> <w n="11.6">plus</w> <w n="11.7">s</w>’<w n="11.8">embourbe</w>,</l>
						<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2">équipage</w> <w n="12.3">est</w> <w n="12.4">en</w> <w n="12.5">désarroi</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Lorsque</w> <w n="13.2">l</w>’<w n="13.3">un</w> <w n="13.4">veut</w> <w n="13.5">pousser</w> <w n="13.6">la</w> <w n="13.7">roue</w>,</l>
						<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2">autre</w> <w n="14.3">devant</w> <w n="14.4">met</w> <w n="14.5">un</w> <w n="14.6">caillou</w>,</l>
						<l n="15" num="4.3"><w n="15.1">On</w> <w n="15.2">s</w>’<w n="15.3">agite</w>, <w n="15.4">on</w> <w n="15.5">crie</w>, <w n="15.6">on</w> <w n="15.7">s</w>’<w n="15.8">enroue</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">l</w>’<w n="16.3">on</w> <w n="16.4">va</w>… <w n="16.5">se</w> <w n="16.6">rompre</w> <w n="16.7">le</w> <w n="16.8">cou</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Tandis</w> <w n="17.2">qu</w>’<w n="17.3">ils</w> <w n="17.4">s</w>’<w n="17.5">échauffent</w> <w n="17.6">la</w> <w n="17.7">bile</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Tandis</w> <w n="18.2">que</w> <w n="18.3">leur</w> <w n="18.4">colère</w> <w n="18.5">bout</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">Char</w>, <w n="19.3">hélas</w> ! <w n="19.4">reste</w> <w n="19.5">immobile</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Le</w> <w n="20.2">Char</w> <w n="20.3">n</w>’<w n="20.4">avance</w> <w n="20.5">pas</w> <w n="20.6">du</w> <w n="20.7">tout</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">trois</w> <w n="21.3">voyageurs</w> <w n="21.4">qui</w> <w n="21.5">le</w> <w n="21.6">suivent</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Trois</w> <w n="22.2">voyageurs</w> <w n="22.3">allant</w> <w n="22.4">à</w> <w n="22.5">pied</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Plus</w> <w n="23.2">vite</w> <w n="23.3">que</w> <w n="23.4">le</w> <w n="23.5">Char</w> <w n="23.6">arrivent</w></l>
						<l n="24" num="6.4"><w n="24.1">Au</w> <w n="24.2">but</w>, <w n="24.3">où</w> <w n="24.4">chacun</w> <w n="24.5">d</w>’<w n="24.6">eux</w> <w n="24.7">s</w>’<w n="24.8">assied</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Est</w>-<w n="25.2">il</w> <w n="25.3">besoin</w> <w n="25.4">que</w> <w n="25.5">je</w> <w n="25.6">le</w> <w n="25.7">nomme</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Ce</w> <w n="26.2">Char</w>, <w n="26.3">orgueil</w> <w n="26.4">d</w>’<w n="26.5">un</w> <w n="26.6">potentat</w> ?</l>
						<l n="27" num="7.3"><w n="27.1">C</w>’<w n="27.2">est</w> <w n="27.3">celui</w> <w n="27.4">que</w> <w n="27.5">Joseph</w> <w n="27.6">Prud</w>’<w n="27.7">homme</w></l>
						<l n="28" num="7.4"><w n="28.1">Appelait</w> <w n="28.2">le</w> <w n="28.3">Char</w> <w n="28.4">de</w> <w n="28.5">l</w>’<w n="28.6">État</w>,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ses</w> <w n="29.2">conducteurs</w> ? <w n="29.3">le</w> <w n="29.4">Ministère</w>,</l>
						<l n="30" num="8.2"><w n="30.1">L</w>’<w n="30.2">imprudente</w> <w n="30.3">majorité</w>,</l>
						<l n="31" num="8.3"><w n="31.1">L</w>’<w n="31.2">un</w> <w n="31.3">veut</w> <w n="31.4">faire</w> <w n="31.5">trop</w> <w n="31.6">de</w> <w n="31.7">manière</w>,</l>
						<l n="32" num="8.4"><w n="32.1">L</w>’<w n="32.2">autre</w> <w n="32.3">a</w> <w n="32.4">trop</w> <w n="32.5">de</w> <w n="32.6">brutalité</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Pour</w> <w n="33.2">finir</w> <w n="33.3">faut</w>-<w n="33.4">il</w> <w n="33.5">que</w> <w n="33.6">j</w>’<w n="33.7">explique</w></l>
						<l n="34" num="9.2"><w n="34.1">Les</w> <w n="34.2">trois</w> <w n="34.3">piétons</w> ? <w n="34.4">en</w> <w n="34.5">vérité</w>,</l>
						<l n="35" num="9.3"><w n="35.1">C</w>’<w n="35.2">est</w> <w n="35.3">d</w>’<hi rend="ital"><w n="35.4">Orléans</w></hi>, <w n="35.5">la</w> <hi rend="ital"><w n="35.6">République</w></hi>,</l>
						<l n="36" num="9.4"><w n="36.1">Avec</w> <w n="36.2">la</w> <hi rend="ital"><w n="36.3">Légitimité</w></hi>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Sans</w> <w n="37.2">être</w> <w n="37.3">devin</w> <w n="37.4">ni</w> <w n="37.5">prophète</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Que</w> <w n="38.2">ça</w> <w n="38.3">cahote</w> <w n="38.4">encore</w> <w n="38.5">un</w> <w n="38.6">peu</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Une</w> <w n="39.2">fois</w> <w n="39.3">arrivés</w> <w n="39.4">au</w> <w n="39.5">faîte</w>,</l>
						<l n="40" num="10.4"><w n="40.1">La</w> <w n="40.2">République</w> <w n="40.3">aura</w> <w n="40.4">beau</w> <w n="40.5">jeu</w> ! <ref type="noteAnchor" target="(1)">(1)</ref></l>
					</lg>
					<closer>
						<dateline>
							<placeName>Paris</placeName>,
							<date when="1870">7 Mars 1870</date>.
						</dateline>
						<note type="footnote" id="1">
							Certes, quelque chose me disait que je serais <lb></lb>
							bon prophète, mais j’étais loin de penser que <lb></lb>
							le char de l’Empire verserait dans la boue et <lb></lb>
							la honte de Sedan, et que la République <lb></lb>
							surgirait du sein de nos désastres ! — Nous <lb></lb>
							l’attendions radieuse, mais non vêtue de deuil. <lb></lb>
						</note>
					</closer>
				</div></body></text></TEI>