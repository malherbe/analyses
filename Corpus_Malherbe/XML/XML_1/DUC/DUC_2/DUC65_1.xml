<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC65">
				<head type="main">Antoinette G. B.</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">vous</w>, <w n="1.3">lorsque</w> <w n="1.4">Jupin</w> <w n="1.5">voulut</w> <w n="1.6">faire</w> <w n="1.7">une</w> <w n="1.8">femme</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Exprès</w> <w n="2.2">il</w> <w n="2.3">convoqua</w> <w n="2.4">les</w> <w n="2.5">Déesses</w>, <w n="2.6">les</w> <w n="2.7">Dieux</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Minerve</w> <w n="3.2">vous</w> <w n="3.3">donna</w> <w n="3.4">la</w> <w n="3.5">sagesse</w>, <w n="3.6">Madame</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">Mercure</w> <w n="4.3">l</w>’<w n="4.4">esprit</w> <w n="4.5">qui</w> <w n="4.6">brille</w> <w n="4.7">dans</w> <w n="4.8">vos</w> <w n="4.9">yeux</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">la</w> <w n="5.3">belle</w> <w n="5.4">Vénus</w> <w n="5.5">que</w> <w n="5.6">partout</w> <w n="5.7">on</w> <w n="5.8">acclame</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Dictait</w> <w n="6.2">chez</w> <w n="6.3">les</w> <w n="6.4">mortels</w> <w n="6.5">ses</w> <w n="6.6">décrets</w> <w n="6.7">amoureux</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Apollon</w> <w n="7.2">loin</w> <w n="7.3">de</w> <w n="7.4">là</w> <w n="7.5">solfiait</w> <w n="7.6">une</w> <w n="7.7">gamme</w>…</l>
					<l n="8" num="2.4"><w n="8.1">Bref</w>, <w n="8.2">ils</w> <w n="8.3">étaient</w> <w n="8.4">absents</w> <w n="8.5">de</w> <w n="8.6">l</w>’<w n="8.7">Olympe</w> <w n="8.8">tous</w> <w n="8.9">deux</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Sans</w> <w n="9.2">la</w> <w n="9.3">voix</w>, <w n="9.4">la</w> <w n="9.5">beauté</w>, <w n="9.6">vous</w> <w n="9.7">n</w>’<w n="9.8">étiez</w> <w n="9.9">point</w> <w n="9.10">parfaite</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">Jupin</w> <w n="10.3">maugréait</w> <w n="10.4">et</w> <w n="10.5">se</w> <w n="10.6">creusait</w> <w n="10.7">la</w> <w n="10.8">tête</w></l>
					<l n="11" num="3.3"><w n="11.1">Pour</w> <w n="11.2">vous</w> <w n="11.3">parachever</w>. — <w n="11.4">Lorsque</w> <w n="11.5">sur</w> <w n="11.6">un</w> <w n="11.7">rosier</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Le</w> <w n="12.2">rossignol</w> <w n="12.3">chanta</w> ! — <w n="12.4">Jupin</w> <w n="12.5">reprit</w> <w n="12.6">courage</w> ;</l>
					<l n="13" num="4.2"><space unit="char" quantity="8"></space><w n="13.1">La</w> <w n="13.2">rose</w> <w n="13.3">fit</w> <w n="13.4">votre</w> <w n="13.5">visage</w>,</l>
					<l n="14" num="4.3"><space unit="char" quantity="8"></space><w n="14.1">Le</w> <w n="14.2">rossignol</w> <w n="14.3">votre</w> <w n="14.4">gosier</w> !</l>
				</lg>
			</div></body></text></TEI>