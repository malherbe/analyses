<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC36">
				<head type="main">Blanche</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Du</w> <w n="1.2">jardin</w> <w n="1.3">odorant</w> <w n="1.4">la</w> <w n="1.5">pénétrante</w> <w n="1.6">haleine</w></l>
					<l n="2" num="1.2"><w n="2.1">Embaumait</w> <w n="2.2">l</w>’<w n="2.3">air</w> <w n="2.4">du</w> <w n="2.5">soir</w>, <w n="2.6">et</w> <w n="2.7">le</w> <w n="2.8">chant</w> <w n="2.9">des</w> <w n="2.10">oiseaux</w></l>
					<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">regagnaient</w> <w n="3.3">leur</w> <w n="3.4">nid</w> <w n="3.5">se</w> <w n="3.6">taisait</w> <w n="3.7">dans</w> <w n="3.8">la</w> <w n="3.9">plaine</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">les</w> <w n="4.3">zéphirs</w> <w n="4.4">couraient</w> <w n="4.5">à</w> <w n="4.6">travers</w> <w n="4.7">les</w> <w n="4.8">roseaux</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">soleil</w> <w n="5.3">avait</w> <w n="5.4">fui</w> <w n="5.5">derrière</w> <w n="5.6">la</w> <w n="5.7">colline</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">ruisseau</w> <w n="6.3">caressait</w> <w n="6.4">les</w> <w n="6.5">berges</w> <w n="6.6">du</w> <w n="6.7">sentier</w>.</l>
					<l n="7" num="2.3"><w n="7.1">C</w>’<w n="7.2">était</w> <w n="7.3">l</w>’<w n="7.4">heure</w> <w n="7.5">où</w> <w n="7.6">l</w>’<w n="7.7">étoile</w> <w n="7.8">au</w> <w n="7.9">ciel</w> <w n="7.10">bleu</w> <w n="7.11">s</w>’<w n="7.12">illumine</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">le</w> <w n="8.3">recueillement</w> <w n="8.4">couvre</w> <w n="8.5">le</w> <w n="8.6">monde</w> <w n="8.7">entier</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2">était</w> <w n="9.3">l</w>’<w n="9.4">heure</w> <w n="9.5">d</w>’<w n="9.6">amour</w> <w n="9.7">pour</w> <w n="9.8">le</w> <w n="9.9">rêve</w> <w n="9.10">choisie</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">Le</w> <w n="10.2">moment</w> <w n="10.3">solennel</w> <w n="10.4">qu</w>’<w n="10.5">on</w> <w n="10.6">ne</w> <w n="10.7">peut</w> <w n="10.8">définir</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">tout</w> <w n="11.3">n</w>’<w n="11.4">est</w> <w n="11.5">que</w> <w n="11.6">rayons</w>, <w n="11.7">extase</w>, <w n="11.8">poésie</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Où</w> <w n="12.2">le</w> <w n="12.3">cœur</w> <w n="12.4">veut</w> <w n="12.5">aimer</w>, <w n="12.6">où</w> <w n="12.7">l</w>’<w n="12.8">âme</w> <w n="12.9">veut</w> <w n="12.10">bénir</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Nous</w> <w n="13.2">marchions</w>… <w n="13.3">et</w> <w n="13.4">nos</w> <w n="13.5">voix</w> <w n="13.6">étaient</w> <w n="13.7">pourtant</w> <w n="13.8">muettes</w></l>
					<l n="14" num="4.2"><w n="14.1">Nous</w> <w n="14.2">tenant</w> <w n="14.3">par</w> <w n="14.4">la</w> <w n="14.5">main</w> <w n="14.6">nous</w> <w n="14.7">allions</w> <w n="14.8">devant</w> <w n="14.9">nous</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Écoutant</w> <w n="15.2">la</w> <w n="15.3">nature</w> <w n="15.4">en</w> <w n="15.5">ses</w> <w n="15.6">hymnes</w> <w n="15.7">secrètes</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Concert</w> <w n="16.2">mystérieux</w>, <w n="16.3">insaisissable</w> <w n="16.4">et</w> <w n="16.5">doux</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">mon</w> <w n="17.3">cœur</w> <w n="17.4">inondé</w> <w n="17.5">d</w>’<w n="17.6">une</w> <w n="17.7">ivresse</w> <w n="17.8">suprême</w></l>
					<l n="18" num="5.2"><w n="18.1">Tressaillit</w> <w n="18.2">tout</w> <w n="18.3">à</w> <w n="18.4">coup</w>, <w n="18.5">à</w> <w n="18.6">moitié</w> <w n="18.7">du</w> <w n="18.8">chemin</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Un</w> <w n="19.2">cri</w> <w n="19.3">s</w>’<w n="19.4">en</w> <w n="19.5">échappa</w> : — « <w n="19.6">Blanche</w> ! <w n="19.7">Blanche</w> ! <w n="19.8">je</w> <w n="19.9">t</w>’<w n="19.10">aime</w> ! »</l>
					<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">je</w> <w n="20.3">sentis</w> <w n="20.4">ta</w> <w n="20.5">main</w> <w n="20.6">qui</w> <w n="20.7">tremblait</w> <w n="20.8">dans</w> <w n="20.9">ma</w> <w n="20.10">main</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">O</w> <w n="21.2">douces</w> <w n="21.3">voluptés</w> ! <w n="21.4">ineffables</w> <w n="21.5">ivresses</w> !</l>
					<l n="22" num="6.2"><w n="22.1">A</w> <w n="22.2">cet</w> <w n="22.3">aveu</w> <w n="22.4">brûlant</w> <w n="22.5">un</w> <w n="22.6">autre</w> <w n="22.7">répondit</w> !</l>
					<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">le</w> <w n="23.3">bruit</w> <w n="23.4">d</w>’<w n="23.5">un</w> <w n="23.6">baiser</w> <w n="23.7">qui</w> <w n="23.8">scellait</w> <w n="23.9">nos</w> <w n="23.10">promesses</w></l>
					<l n="24" num="6.4"><w n="24.1">Éveilla</w> <w n="24.2">le</w> <w n="24.3">bosquet</w>… <w n="24.4">Mais</w> <w n="24.5">nul</w> <w n="24.6">ne</w> <w n="24.7">l</w>’<w n="24.8">entendit</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">moi</w>, <w n="25.3">de</w> <w n="25.4">cet</w> <w n="25.5">aveu</w> <w n="25.6">j</w>’<w n="25.7">ai</w> <w n="25.8">fait</w> <w n="25.9">toute</w> <w n="25.10">ma</w> <w n="25.11">vie</w> ;</l>
					<l n="26" num="7.2"><w n="26.1">J</w>’<w n="26.2">ai</w> <w n="26.3">pris</w> <w n="26.4">dans</w> <w n="26.5">ton</w> <w n="26.6">baiser</w> <w n="26.7">ma</w> <w n="26.8">force</w> <w n="26.9">et</w> <w n="26.10">mon</w> <w n="26.11">espoir</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">j</w>’<w n="27.3">ai</w> <w n="27.4">senti</w> <w n="27.5">descendre</w> <w n="27.6">en</w> <w n="27.7">moi</w> <w n="27.8">la</w> <w n="27.9">poésie</w></l>
					<l n="28" num="7.4"><w n="28.1">Qui</w> <w n="28.2">chantait</w> <w n="28.3">par</w> <w n="28.4">ta</w> <w n="28.5">voix</w> <w n="28.6">dans</w> <w n="28.7">les</w> <w n="28.8">splendeurs</w> <w n="28.9">du</w> <w n="28.10">soir</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1860">1860</date>
					</dateline>
				</closer>
			</div></body></text></TEI>