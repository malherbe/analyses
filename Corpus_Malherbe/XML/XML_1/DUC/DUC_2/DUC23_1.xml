<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES RUBANS DE MARIE</head><head type="sub_part">Simple Histoire</head><div type="poem" key="DUC23">
					<head type="number">II</head>
					<head type="main">Ruban bleu</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ton</w> <w n="1.2">front</w> <w n="1.3">est</w> <w n="1.4">inquiet</w>, <w n="1.5">ô</w> <w n="1.6">Marie</w> ! <w n="1.7">et</w> <w n="1.8">ta</w> <w n="1.9">mère</w></l>
						<l n="2" num="1.2"><w n="2.1">Ne</w> <w n="2.2">t</w>’<w n="2.3">a</w> <w n="2.4">pas</w> <w n="2.5">entendu</w> <w n="2.6">réciter</w> <w n="2.7">la</w> <w n="2.8">prière</w></l>
						<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">ensemble</w> <w n="3.3">à</w> <w n="3.4">ton</w> <w n="3.5">chevet</w> <w n="3.6">vous</w> <w n="3.7">faisiez</w> <w n="3.8">le</w> <w n="3.9">matin</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Bien</w> <w n="4.2">des</w> <w n="4.3">fois</w> <w n="4.4">de</w> <w n="4.5">tes</w> <w n="4.6">doigts</w> <w n="4.7">ton</w> <w n="4.8">aiguille</w> <w n="4.9">est</w> <w n="4.10">tombée</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Vis</w>-<w n="5.2">à</w>-<w n="5.3">vis</w>, <w n="5.4">ton</w> <w n="5.5">regard</w> <w n="5.6">erre</w> <w n="5.7">à</w> <w n="5.8">la</w> <w n="5.9">dérobée</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">ton</w> <w n="6.3">oiseau</w> <w n="6.4">tout</w> <w n="6.5">seul</w> <w n="6.6">a</w> <w n="6.7">chanté</w> <w n="6.8">son</w> <w n="6.9">refrain</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Qu</w>’<w n="7.2">as</w>-<w n="7.3">tu</w> ? <w n="7.4">quelle</w> <w n="7.5">langueur</w> <w n="7.6">décolore</w> <w n="7.7">ta</w> <w n="7.8">joue</w></l>
						<l n="8" num="2.2"><w n="8.1">Et</w> <w n="8.2">quel</w> <w n="8.3">esprit</w> <w n="8.4">malin</w> <w n="8.5">de</w> <w n="8.6">ton</w> <w n="8.7">repos</w> <w n="8.8">se</w> <w n="8.9">joue</w> ?</l>
						<l n="9" num="2.3"><w n="9.1">Hier</w> <w n="9.2">encor</w> <w n="9.3">tu</w> <w n="9.4">riais</w>, <w n="9.5">libre</w> <w n="9.6">ainsi</w> <w n="9.7">qu</w>’<w n="9.8">à</w> <w n="9.9">seize</w> <w n="9.10">ans</w>.</l>
						<l n="10" num="2.4"><w n="10.1">Mais</w>, <w n="10.2">ta</w> <w n="10.3">mère</w> <w n="10.4">va</w> <w n="10.5">mieux</w>, <w n="10.6">tu</w> <w n="10.7">dois</w> <w n="10.8">être</w> <w n="10.9">joyeuse</w> ?</l>
						<l n="11" num="2.5"><w n="11.1">Es</w>-<w n="11.2">tu</w> <w n="11.3">malade</w> ? <w n="11.4">non</w>. — <w n="11.5">Ma</w> <w n="11.6">jeune</w> <w n="11.7">soucieuse</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Pourquoi</w> <w n="12.2">ce</w> <w n="12.3">front</w> <w n="12.4">rêveur</w> <w n="12.5">et</w> <w n="12.6">ces</w> <w n="12.7">yeux</w> <w n="12.8">languissants</w> ?</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">D</w>’<w n="13.2">où</w> <w n="13.3">naît</w> <w n="13.4">ce</w> <w n="13.5">changement</w> ? — <w n="13.6">Regarde</w> <w n="13.7">en</w> <w n="13.8">ta</w> <w n="13.9">demeure</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Gaie</w> <w n="14.2">et</w> <w n="14.3">contente</w> <w n="14.4">hier</w>, <w n="14.5">maintenant</w> <w n="14.6">tout</w> <w n="14.7">y</w> <w n="14.8">pleure</w>.</l>
						<l n="15" num="3.3"><w n="15.1">Pourquoi</w>, <w n="15.2">mon</w> <w n="15.3">Dieu</w>, <w n="15.4">pourquoi</w> <w n="15.5">ce</w> <w n="15.6">subit</w> <w n="15.7">abandon</w> ?…</l>
						<l n="16" num="3.4"><w n="16.1">Quel</w> <w n="16.2">secret</w> <w n="16.3">caches</w>-<w n="16.4">tu</w> ? <w n="16.5">quel</w> <w n="16.6">trouble</w> ? <w n="16.7">quel</w> <w n="16.8">mystère</w> ?…</l>
						<l n="17" num="3.5"><w n="17.1">Tu</w> <w n="17.2">détournes</w> <w n="17.3">les</w> <w n="17.4">yeux</w>, <w n="17.5">enfant</w>, <w n="17.6">pourquoi</w> <w n="17.7">nous</w> <w n="17.8">taire</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Vers</w> <w n="18.2">quel</w> <w n="18.3">bonheur</w> <w n="18.4">perdu</w> <w n="18.5">ces</w> <w n="18.6">longs</w> <w n="18.7">soupirs</w> <w n="18.8">s</w>’<w n="18.9">en</w> <w n="18.10">vont</w> ?</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Tout</w> <w n="19.2">auprès</w>, <w n="19.3">vis</w>-<w n="19.4">à</w>-<w n="19.5">vis</w>, <w n="19.6">dans</w> <w n="19.7">une</w> <w n="19.8">chambre</w> <w n="19.9">étroite</w></l>
						<l n="20" num="4.2"><w n="20.1">Que</w> <w n="20.2">l</w>’<w n="20.3">été</w> <w n="20.4">rend</w> <w n="20.5">brûlante</w> <w n="20.6">et</w> <w n="20.7">l</w>’<w n="20.8">hiver</w> <w n="20.9">toute</w> <w n="20.10">moite</w></l>
						<l n="21" num="4.3"><w n="21.1">D</w>’<w n="21.2">humidité</w>, — <w n="21.3">depuis</w> <w n="21.4">quinze</w> <w n="21.5">jours</w> <w n="21.6">environ</w>,</l>
						<l n="22" num="4.4"><w n="22.1">Habitait</w> <w n="22.2">un</w> <w n="22.3">jeune</w> <w n="22.4">homme</w>. — <w n="22.5">Orphelin</w> <w n="22.6">dès</w> <w n="22.7">l</w>’<w n="22.8">enfance</w></l>
						<l n="23" num="4.5"><w n="23.1">Il</w> <w n="23.2">n</w>’<w n="23.3">avait</w> <w n="23.4">pas</w> <w n="23.5">connu</w> <w n="23.6">sa</w> <w n="23.7">mère</w> ; — <w n="23.8">à</w> <w n="23.9">sa</w> <w n="23.10">naissance</w></l>
						<l n="24" num="4.6"><w n="24.1">Le</w> <w n="24.2">signe</w> <w n="24.3">du</w> <w n="24.4">mépris</w> <w n="24.5">avait</w> <w n="24.6">meurtri</w> <w n="24.7">son</w> <w n="24.8">front</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Un</w> <w n="25.2">soir</w>, <w n="25.3">de</w> <w n="25.4">bonnes</w> <w n="25.5">gens</w> <w n="25.6">l</w>’<w n="25.7">avaient</w> <w n="25.8">sur</w> <w n="25.9">une</w> <w n="25.10">pierre</w></l>
						<l n="26" num="5.2"><w n="26.1">Ramassé</w> <w n="26.2">tout</w> <w n="26.3">enfant</w>, <w n="26.4">presque</w> <w n="26.5">nu</w>, <w n="26.6">car</w> <w n="26.7">sa</w> <w n="26.8">mère</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Que</w> <w n="27.2">nul</w> <w n="27.3">ne</w> <w n="27.4">vit</w> <w n="27.5">jamais</w>, <w n="27.6">l</w>’<w n="27.7">avait</w> <w n="27.8">abandonné</w>.</l>
						<l n="28" num="5.4"><w n="28.1">Ils</w> <w n="28.2">en</w> <w n="28.3">eurent</w> <w n="28.4">pitié</w>, <w n="28.5">ses</w> <w n="28.6">pleurs</w> <w n="28.7">les</w> <w n="28.8">attendrirent</w> ;</l>
						<l n="29" num="5.5"><w n="29.1">Ce</w> <w n="29.2">que</w> <w n="29.3">n</w>’<w n="29.4">avait</w> <w n="29.5">point</w> <w n="29.6">fait</w> <w n="29.7">une</w> <w n="29.8">mère</w>, <w n="29.9">ils</w> <w n="29.10">le</w> <w n="29.11">firent</w> ;</l>
						<l n="30" num="5.6"><w n="30.1">Ils</w> <w n="30.2">donnèrent</w> <w n="30.3">leur</w> <w n="30.4">pain</w> <w n="30.5">à</w> <w n="30.6">l</w>’<w n="30.7">enfant</w> <w n="30.8">nouveau</w>-<w n="30.9">né</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Plus</w> <w n="31.2">tard</w>, <w n="31.3">lorsqu</w>’<w n="31.4">il</w> <w n="31.5">grandit</w>, <w n="31.6">il</w> <w n="31.7">dut</w> <w n="31.8">gagner</w> <w n="31.9">sa</w> <w n="31.10">vie</w>,</l>
						<l n="32" num="6.2"><w n="32.1">Et</w> <w n="32.2">souvent</w> <w n="32.3">il</w> <w n="32.4">jetait</w> <w n="32.5">un</w> <w n="32.6">long</w> <w n="32.7">regard</w> <w n="32.8">d</w>’<w n="32.9">envie</w></l>
						<l n="33" num="6.3"><w n="33.1">Sur</w> <w n="33.2">les</w> <w n="33.3">autres</w> <w n="33.4">enfants</w> <w n="33.5">dont</w> <w n="33.6">il</w> <w n="33.7">voyait</w> <w n="33.8">les</w> <w n="33.9">jeux</w> ;</l>
						<l n="34" num="6.4"><w n="34.1">Mais</w> <w n="34.2">lorsqu</w>’<w n="34.3">il</w> <w n="34.4">recherchait</w> <w n="34.5">leur</w> <w n="34.6">troupe</w> <w n="34.7">fortunée</w>,</l>
						<l n="35" num="6.5"><w n="35.1">L</w>’<w n="35.2">ouvrage</w> <w n="35.3">l</w>’<w n="35.4">appelait</w>, <w n="35.5">son</w> <w n="35.6">pain</w> <w n="35.7">de</w> <w n="35.8">la</w> <w n="35.9">journée</w> !</l>
						<l n="36" num="6.6"><w n="36.1">Et</w> <w n="36.2">l</w>’<w n="36.3">enfant</w> <w n="36.4">retournait</w> <w n="36.5">au</w> <w n="36.6">chantier</w> <w n="36.7">soucieux</w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">— « <w n="37.1">Toujours</w> <w n="37.2">seul</w> ! <w n="37.3">disait</w>-<w n="37.4">il</w>, <w n="37.5">jamais</w> <w n="37.6">une</w> <w n="37.7">voix</w> <w n="37.8">douce</w>.</l>
						<l n="38" num="7.2"><w n="38.1">Celui</w> <w n="38.2">que</w> <w n="38.3">je</w> <w n="38.4">voudrais</w> <w n="38.5">pour</w> <w n="38.6">ami</w> <w n="38.7">me</w> <w n="38.8">repousse</w>,</l>
						<l n="39" num="7.3"><w n="39.1">Et</w> <w n="39.2">je</w> <w n="39.3">vais</w> <w n="39.4">dévorer</w> <w n="39.5">mes</w> <w n="39.6">larmes</w> <w n="39.7">à</w> <w n="39.8">l</w>’<w n="39.9">écart</w>.</l>
						<l n="40" num="7.4"><w n="40.1">Je</w> <w n="40.2">n</w>’<w n="40.3">ai</w> <w n="40.4">pas</w> <w n="40.5">demandé</w> <w n="40.6">pourtant</w>, <w n="40.7">Seigneur</w>, <w n="40.8">à</w> <w n="40.9">naître</w> ?</l>
						<l n="41" num="7.5"><w n="41.1">N</w>’<w n="41.2">aurais</w>-<w n="41.3">tu</w> <w n="41.4">pas</w> <w n="41.5">mieux</w> <w n="41.6">fait</w> <w n="41.7">de</w> <w n="41.8">dérober</w> <w n="41.9">à</w> <w n="41.10">l</w>’<w n="41.11">être</w></l>
						<l n="42" num="7.6"><w n="42.1">Le</w> <w n="42.2">pauvre</w> <w n="42.3">paria</w> <w n="42.4">qu</w>’<w n="42.5">ils</w> <w n="42.6">appellent</w> <w n="42.7">bâtard</w>. »</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><w n="43.1">Louis</w>, — <w n="43.2">c</w>’<w n="43.3">était</w> <w n="43.4">son</w> <w n="43.5">nom</w>, — <w n="43.6">voyait</w> <w n="43.7">passer</w> <w n="43.8">Marie</w> ;</l>
						<l n="44" num="8.2"><w n="44.1">Il</w> <w n="44.2">l</w>’<w n="44.3">attendait</w> <w n="44.4">le</w> <w n="44.5">soir</w>. — <w n="44.6">C</w>’<w n="44.7">était</w> <w n="44.8">là</w> <w n="44.9">de</w> <w n="44.10">sa</w> <w n="44.11">vie</w></l>
						<l n="45" num="8.3"><w n="45.1">Le</w> <w n="45.2">seul</w> <w n="45.3">bonheur</w>, <w n="45.4">hélas</w> ! — <w n="45.5">Marie</w>, <w n="45.6">en</w> <w n="45.7">souriant</w>,</l>
						<l n="46" num="8.4"><w n="46.1">Lui</w> <w n="46.2">donnait</w> <w n="46.3">un</w> « <w n="46.4">Bonsoir</w> ! » <w n="46.5">lorsqu</w>’<w n="46.6">elle</w> <w n="46.7">entrait</w> <w n="46.8">chez</w> <w n="46.9">elle</w>,</l>
						<l n="47" num="8.5"><w n="47.1">Et</w> <w n="47.2">lui</w> <w n="47.3">la</w> <w n="47.4">contemplait</w>, <w n="47.5">il</w> <w n="47.6">la</w> <w n="47.7">trouvait</w> <w n="47.8">si</w> <w n="47.9">belle</w></l>
						<l n="48" num="8.6"><w n="48.1">Qu</w>’<w n="48.2">il</w> <w n="48.3">n</w>’<w n="48.4">osait</w> <w n="48.5">lui</w> <w n="48.6">parler</w> <w n="48.7">dans</w> <w n="48.8">son</w> <w n="48.9">ravissement</w>.</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1"><w n="49.1">Mais</w> <w n="49.2">il</w> <w n="49.3">était</w> <w n="49.4">toujours</w> <w n="49.5">placé</w> <w n="49.6">sur</w> <w n="49.7">son</w> <w n="49.8">passage</w>,</l>
						<l n="50" num="9.2"><w n="50.1">Un</w> <w n="50.2">regard</w> <w n="50.3">bienveillant</w> <w n="50.4">lui</w> <w n="50.5">donnait</w> <w n="50.6">du</w> <w n="50.7">courage</w>.</l>
						<l n="51" num="9.3">— « <w n="51.1">Si</w> <w n="51.2">tu</w> <w n="51.3">voulais</w> <w n="51.4">m</w>’<w n="51.5">aimer</w>, <w n="51.6">ange</w> ! <w n="51.7">murmurait</w>-<w n="51.8">il</w>,</l>
						<l n="52" num="9.4"><w n="52.1">Mais</w>, <w n="52.2">si</w> <w n="52.3">bas</w> <w n="52.4">que</w> <w n="52.5">lui</w> <w n="52.6">seul</w> <w n="52.7">l</w>’<w n="52.8">entendait</w> <w n="52.9">dans</w> <w n="52.10">son</w> <w n="52.11">âme</w> ;</l>
						<l n="53" num="9.5"><w n="53.1">Si</w> <w n="53.2">tu</w> <w n="53.3">voulais</w> <w n="53.4">m</w>’<w n="53.5">aimer</w>, — <w n="53.6">de</w> <w n="53.7">cette</w> <w n="53.8">foule</w> <w n="53.9">infâme</w></l>
						<l n="54" num="9.6"><w n="54.1">Je</w> <w n="54.2">braverais</w> <w n="54.3">l</w>’<w n="54.4">affront</w> ! — <w n="54.5">Comme</w> <w n="54.6">la</w> <w n="54.7">fleur</w> <w n="54.8">d</w>’<w n="54.9">avril</w></l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1"><w n="55.1">S</w>’<w n="55.2">échappe</w> <w n="55.3">du</w> <w n="55.4">bouton</w> <w n="55.5">qui</w> <w n="55.6">parfume</w> <w n="55.7">sa</w> <w n="55.8">tige</w>,</l>
						<l n="56" num="10.2"><w n="56.1">Et</w> <w n="56.2">vient</w> <w n="56.3">ouvrir</w> <w n="56.4">son</w> <w n="56.5">sein</w> <w n="56.6">à</w> <w n="56.7">l</w>’<w n="56.8">oiseau</w> <w n="56.9">qui</w> <w n="56.10">voltige</w>,</l>
						<l n="57" num="10.3"><w n="57.1">Comme</w> <w n="57.2">elle</w>, <w n="57.3">douce</w> <w n="57.4">enfant</w>, <w n="57.5">je</w> <w n="57.6">t</w>’<w n="57.7">ouvrirais</w> <w n="57.8">mon</w> <w n="57.9">cœur</w></l>
						<l n="58" num="10.4"><w n="58.1">Fermé</w> <w n="58.2">jusqu</w>’<w n="58.3">aujourd</w>’<w n="58.4">hui</w>. — <w n="58.5">De</w> <w n="58.6">ton</w> <w n="58.7">amour</w> <w n="58.8">la</w> <w n="58.9">force</w></l>
						<l n="59" num="10.5"><w n="59.1">Saurait</w> <w n="59.2">briser</w>, <w n="59.3">crois</w>-<w n="59.4">moi</w>, <w n="59.5">sa</w> <w n="59.6">trop</w> <w n="59.7">rugueuse</w> <w n="59.8">écorce</w>,</l>
						<l n="60" num="10.6"><w n="60.1">Et</w> <w n="60.2">serait</w> <w n="60.3">le</w> <w n="60.4">soleil</w> <w n="60.5">qui</w> <w n="60.6">féconde</w> <w n="60.7">la</w> <w n="60.8">fleur</w> ! »</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1"><w n="61.1">Un</w> <w n="61.2">jour</w>, <w n="61.3">elle</w> <w n="61.4">venait</w> <w n="61.5">de</w> <w n="61.6">reporter</w> <w n="61.7">l</w>’<w n="61.8">ouvrage</w> ;</l>
						<l n="62" num="11.2"><w n="62.1">Marie</w>, <w n="62.2">en</w> <w n="62.3">s</w>’<w n="62.4">approchant</w>, <w n="62.5">aperçut</w> <w n="62.6">dans</w> <w n="62.7">la</w> <w n="62.8">cage</w></l>
						<l n="63" num="11.3"><w n="63.1">Un</w> <w n="63.2">joyeux</w> <w n="63.3">compagnon</w> <w n="63.4">pour</w> <w n="63.5">son</w> <w n="63.6">oiseau</w> <w n="63.7">chéri</w>.</l>
						<l n="64" num="11.4"><w n="64.1">D</w>’<w n="64.2">un</w> <w n="64.3">nouvel</w> <w n="64.4">oiselet</w> <w n="64.5">ayant</w> <w n="64.6">fait</w> <w n="64.7">la</w> <w n="64.8">demande</w>,</l>
						<l n="65" num="11.5"><w n="65.1">Elle</w> <w n="65.2">crut</w> <w n="65.3">deviner</w> <w n="65.4">de</w> <w n="65.5">qui</w> <w n="65.6">venait</w> <w n="65.7">l</w>’<w n="65.8">offrande</w>,</l>
						<l n="66" num="11.6"><w n="66.1">Car</w> <w n="66.2">celui</w>-<w n="66.3">ci</w> <w n="66.4">portait</w> <w n="66.5">son</w> <w n="66.6">ruban</w> <w n="66.7">favori</w>.</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1">— « <w n="67.1">C</w>’<w n="67.2">est</w> <w n="67.3">toi</w>, <w n="67.4">mère</w> ? <w n="67.5">fit</w>-<w n="67.6">elle</w>. — <w n="67.7">Ah</w> ! <w n="67.8">je</w> <w n="67.9">te</w> <w n="67.10">remercie</w>,</l>
						<l n="68" num="12.2">— « <w n="68.1">Mais</w> <w n="68.2">non</w>, <w n="68.3">c</w>’<w n="68.4">est</w> <w n="68.5">le</w> <w n="68.6">voisin</w> <w n="68.7">qui</w> <w n="68.8">tantôt</w>, <w n="68.9">ma</w> <w n="68.10">chérie</w>,</l>
						<l n="69" num="12.3"><w n="69.1">M</w>’<w n="69.2">a</w> <w n="69.3">dit</w> : — <w n="69.4">Votre</w> <w n="69.5">chanteur</w>, <w n="69.6">tout</w> <w n="69.7">seul</w>, <w n="69.8">doit</w> <w n="69.9">s</w>’<w n="69.10">ennuyer</w>,</l>
						<l n="70" num="12.4"><w n="70.1">Car</w> <w n="70.2">vivre</w> <w n="70.3">seul</w>, <w n="70.4">allez</w> ! <w n="70.5">c</w>’<w n="70.6">est</w> <w n="70.7">bien</w> <w n="70.8">triste</w>, <w n="70.9">madame</w> !…</l>
						<l n="71" num="12.5"><w n="71.1">Mais</w>, <w n="71.2">à</w> <w n="71.3">deux</w>, <w n="71.4">ces</w> <w n="71.5">oiseaux</w> <w n="71.6">égrèneront</w> <w n="71.7">leur</w> <w n="71.8">gamme</w></l>
						<l n="72" num="12.6"><w n="72.1">Comme</w> <w n="72.2">des</w> <w n="72.3">perles</w> <w n="72.4">d</w>’<w n="72.5">or</w> <w n="72.6">au</w> <w n="72.7">paisible</w> <w n="72.8">foyer</w>. »</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1"><w n="73.1">Le</w> <w n="73.2">soir</w>, <w n="73.3">lorsque</w> <w n="73.4">Louis</w> <w n="73.5">eut</w> <w n="73.6">fini</w> <w n="73.7">sa</w> <w n="73.8">journée</w>,</l>
						<l n="74" num="13.2"><w n="74.1">Marie</w>, <w n="74.2">en</w> <w n="74.3">rougissant</w>, — <w n="74.4">elle</w> <w n="74.5">en</w> <w n="74.6">fut</w> <w n="74.7">étonnée</w>,</l>
						<l n="75" num="13.3"><w n="75.1">Alla</w> <w n="75.2">remercier</w> <w n="75.3">son</w> <w n="75.4">généreux</w> <w n="75.5">voisin</w>.</l>
						<l n="76" num="13.4"><w n="76.1">Louis</w>, <w n="76.2">en</w> <w n="76.3">l</w>’<w n="76.4">écoutant</w>, <w n="76.5">avait</w> <w n="76.6">comme</w> <w n="76.7">la</w> <w n="76.8">fièvre</w> ;</l>
						<l n="77" num="13.5"><w n="77.1">Un</w> <w n="77.2">mot</w> : — « <w n="77.3">Oh</w> ! <w n="77.4">je</w> <w n="77.5">vous</w> <w n="77.6">aime</w> ! » <w n="77.7">échappa</w> <w n="77.8">de</w> <w n="77.9">sa</w> <w n="77.10">lèvre</w>,</l>
						<l n="78" num="13.6"><w n="78.1">Qui</w> <w n="78.2">de</w> <w n="78.3">la</w> <w n="78.4">pauvre</w> <w n="78.5">enfant</w> <w n="78.6">vint</w> <w n="78.7">effleurer</w> <w n="78.8">la</w> <w n="78.9">main</w> !</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1"><w n="79.1">Elle</w> <w n="79.2">rêva</w> <w n="79.3">la</w> <w n="79.4">nuit</w>… <w n="79.5">Mais</w> <w n="79.6">non</w> <w n="79.7">plus</w> <w n="79.8">l</w>’<w n="79.9">heureux</w> <w n="79.10">songe</w></l>
						<l n="80" num="14.2"><w n="80.1">Dans</w> <w n="80.2">lequel</w> <w n="80.3">chaque</w> <w n="80.4">soir</w>, <w n="80.5">le</w> <w n="80.6">cœur</w> <w n="80.7">en</w> <w n="80.8">paix</w> <w n="80.9">se</w> <w n="80.10">plonge</w>,</l>
						<l n="81" num="14.3"><w n="81.1">Car</w> <w n="81.2">elle</w> <w n="81.3">ne</w> <w n="81.4">vit</w> <w n="81.5">point</w> <w n="81.6">le</w> <w n="81.7">paradis</w> <w n="81.8">et</w> <w n="81.9">Dieu</w>.</l>
						<l n="82" num="14.4"><w n="82.1">Cet</w> <w n="82.2">aveu</w> <w n="82.3">de</w> <w n="82.4">Louis</w>, <w n="82.5">le</w> <w n="82.6">songe</w> <w n="82.7">le</w> <w n="82.8">répète</w> !…</l>
						<l n="83" num="14.5"><w n="83.1">Pensive</w>, <w n="83.2">le</w> <w n="83.3">matin</w>, <w n="83.4">elle</w> <w n="83.5">mit</w> <w n="83.6">sur</w> <w n="83.7">sa</w> <w n="83.8">tête</w>,</l>
						<l n="84" num="14.6"><w n="84.1">Au</w> <w n="84.2">lieu</w> <w n="84.3">du</w> <w n="84.4">ruban</w> <w n="84.5">blanc</w>, <w n="84.6">un</w> <w n="84.7">autre</w> <w n="84.8">ruban</w> <w n="84.9">bleu</w> !</l>
					</lg>
				</div></body></text></TEI>