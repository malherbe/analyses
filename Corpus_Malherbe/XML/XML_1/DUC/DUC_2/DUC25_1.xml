<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES RUBANS DE MARIE</head><head type="sub_part">Simple Histoire</head><div type="poem" key="DUC25">
					<head type="number">IV</head>
					<head type="main">Ruban noir</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">clairon</w> <w n="1.3">a</w> <w n="1.4">sonné</w>, <w n="1.5">tout</w> <w n="1.6">s</w>’<w n="1.7">émeut</w>, <w n="1.8">le</w> <w n="1.9">sol</w> <w n="1.10">tremble</w>,</l>
						<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">dirait</w> <w n="2.3">un</w> <w n="2.4">seul</w> <w n="2.5">corps</w> <w n="2.6">en</w> <w n="2.7">voyant</w> <w n="2.8">cet</w> <w n="2.9">ensemble</w></l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">mille</w> <w n="3.3">bataillons</w> <w n="3.4">marchant</w> <w n="3.5">à</w> <w n="3.6">rangs</w> <w n="3.7">serrés</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">silence</w> <w n="4.3">est</w> <w n="4.4">partout</w> ; <w n="4.5">l</w>’<w n="4.6">heure</w> <w n="4.7">de</w> <w n="4.8">la</w> <w n="4.9">bataille</w></l>
						<l n="5" num="1.5"><w n="5.1">Produit</w> <w n="5.2">une</w> <w n="5.3">stupeur</w> <w n="5.4">que</w> <w n="5.5">bientôt</w> <w n="5.6">la</w> <w n="5.7">mitraille</w></l>
						<l n="6" num="1.6"><w n="6.1">Va</w> <w n="6.2">chasser</w>, <w n="6.3">en</w> <w n="6.4">passant</w>, <w n="6.5">sur</w> <w n="6.6">ces</w> <w n="6.7">fronts</w> <w n="6.8">assurés</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Ils</w> <w n="7.2">vaincront</w> <w n="7.3">ou</w> <w n="7.4">mourront</w> ! — <w n="7.5">en</w> <w n="7.6">avant</w> ! <w n="7.7">la</w> <w n="7.8">victoire</w></l>
						<l n="8" num="2.2"><w n="8.1">Leur</w> <w n="8.2">est</w> <w n="8.3">promise</w> <w n="8.4">à</w> <w n="8.5">tous</w>, <w n="8.6">ils</w> <w n="8.7">couvriront</w> <w n="8.8">de</w> <w n="8.9">gloire</w></l>
						<l n="9" num="2.3"><w n="9.1">Et</w> <w n="9.2">d</w>’<w n="9.3">immortalité</w> <w n="9.4">leurs</w> <w n="9.5">drapeaux</w> <w n="9.6">triomphants</w> !</l>
						<l n="10" num="2.4"><w n="10.1">D</w>’<w n="10.2">où</w> <w n="10.3">leur</w> <w n="10.4">vient</w> <w n="10.5">donc</w> <w n="10.6">ainsi</w> <w n="10.7">cette</w> <w n="10.8">mâle</w> <w n="10.9">assurance</w> ?</l>
						<l n="11" num="2.5"><w n="11.1">Qui</w> <w n="11.2">les</w> <w n="11.3">guide</w> ? — <w n="11.4">Un</w> <w n="11.5">génie</w> <w n="11.6">a</w> <w n="11.7">fait</w> <w n="11.8">par</w> <w n="11.9">sa</w> <w n="11.10">présence</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Passer</w> <w n="12.2">d</w>’<w n="12.3">un</w> <w n="12.4">seul</w> <w n="12.5">coup</w> <w n="12.6">d</w>’<w n="12.7">œil</w> <w n="12.8">la</w> <w n="12.9">victoire</w> <w n="12.10">en</w> <w n="12.11">leurs</w> <w n="12.12">rangs</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Un</w> <w n="13.2">seul</w> <w n="13.3">coup</w> <w n="13.4">d</w>’<w n="13.5">œil</w>, <w n="13.6">un</w> <w n="13.7">geste</w>, <w n="13.8">un</w> <w n="13.9">signe</w>, <w n="13.10">une</w> <w n="13.11">parole</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Celle</w> <w n="14.2">qui</w> <w n="14.3">fit</w> <w n="14.4">franchir</w> <w n="14.5">d</w>’<w n="14.6">un</w> <w n="14.7">bond</w> <w n="14.8">le</w> <w n="14.9">pont</w> <w n="14.10">d</w>’<w n="14.11">Arcole</w> !</l>
						<l n="15" num="3.3"><w n="15.1">Car</w> <w n="15.2">ce</w> <w n="15.3">génie</w> <w n="15.4">était</w> <w n="15.5">le</w> <w n="15.6">vainqueur</w> <w n="15.7">d</w>’<w n="15.8">Austerlitz</w> ;</l>
						<l n="16" num="3.4"><w n="16.1">C</w>’<w n="16.2">était</w> <w n="16.3">Napoléon</w>, <w n="16.4">qui</w>, <w n="16.5">ravageant</w> <w n="16.6">la</w> <w n="16.7">terre</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Dans</w> <w n="17.2">son</w> <w n="17.3">immense</w> <w n="17.4">orgueil</w> <w n="17.5">avait</w> <w n="17.6">rêvé</w> <w n="17.7">de</w> <w n="17.8">faire</w></l>
						<l n="18" num="3.6"><w n="18.1">Des</w> <w n="18.2">couronnes</w> <w n="18.3">des</w> <w n="18.4">rois</w> <w n="18.5">des</w> <w n="18.6">jouets</w> <w n="18.7">pour</w> <w n="18.8">son</w> <w n="18.9">fils</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">En</w> <w n="19.2">avant</w> ! <w n="19.3">en</w> <w n="19.4">avant</w> ! <w n="19.5">la</w> <w n="19.6">fanfare</w> <w n="19.7">résonne</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Par</w> <w n="20.2">cent</w> <w n="20.3">bouches</w> <w n="20.4">d</w>’<w n="20.5">airain</w> <w n="20.6">la</w> <w n="20.7">mort</w> <w n="20.8">s</w>’<w n="20.9">élance</w> <w n="20.10">et</w> <w n="20.11">tonne</w>,</l>
						<l n="21" num="4.3"><w n="21.1">Et</w> <w n="21.2">le</w> <w n="21.3">champ</w> <w n="21.4">de</w> <w n="21.5">bataille</w> <w n="21.6">est</w> <w n="21.7">jonché</w> <w n="21.8">de</w> <w n="21.9">mourants</w> !</l>
						<l n="22" num="4.4"><w n="22.1">En</w> <w n="22.2">avant</w>, <w n="22.3">vieux</w> <w n="22.4">soldat</w> ! <w n="22.5">quelles</w> <w n="22.6">sont</w> <w n="22.7">donc</w> <w n="22.8">tes</w> <w n="22.9">craintes</w> ?</l>
						<l n="23" num="4.5"><w n="23.1">N</w>’<w n="23.2">entends</w>-<w n="23.3">tu</w> <w n="23.4">pas</w> <w n="23.5">ces</w> <w n="23.6">cris</w> <w n="23.7">de</w> <w n="23.8">victoire</w> ? — <w n="23.9">Ces</w> <w n="23.10">plaintes</w>,</l>
						<l n="24" num="4.6"><w n="24.1">Ce</w> <w n="24.2">monstrueux</w> <w n="24.3">concert</w> <w n="24.4">que</w> <w n="24.5">font</w> <w n="24.6">des</w> <w n="24.7">combattants</w> ?</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Napoléon</w> <w n="25.2">est</w> <w n="25.3">là</w> ! <w n="25.4">son</w> <w n="25.5">regard</w>, <w n="25.6">regard</w> <w n="25.7">d</w>’<w n="25.8">aigle</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Mesure</w> <w n="26.2">tous</w> <w n="26.3">les</w> <w n="26.4">plans</w> ; <w n="26.5">il</w> <w n="26.6">court</w>, <w n="26.7">il</w> <w n="26.8">vient</w>, <w n="26.9">il</w> <w n="26.10">règle</w></l>
						<l n="27" num="5.3"><w n="27.1">Les</w> <w n="27.2">chances</w> <w n="27.3">du</w> <w n="27.4">succès</w> ; <w n="27.5">il</w> <w n="27.6">a</w> <w n="27.7">vu</w> <w n="27.8">ta</w> <w n="27.9">valeur</w> ;</l>
						<l n="28" num="5.4"><w n="28.1">Il</w> <w n="28.2">te</w> <w n="28.3">fait</w> <w n="28.4">signe</w>, <w n="28.5">approche</w> <w n="28.6">et</w> <w n="28.7">que</w> <w n="28.8">ton</w> <w n="28.9">front</w> <w n="28.10">s</w>’<w n="28.11">incline</w>,</l>
						<l n="29" num="5.5"><w n="29.1">Pour</w> <w n="29.2">étancher</w> <w n="29.3">ton</w> <w n="29.4">sang</w> <w n="29.5">il</w> <w n="29.6">va</w> <w n="29.7">sur</w> <w n="29.8">ta</w> <w n="29.9">poitrine</w></l>
						<l n="30" num="5.6"><w n="30.1">Poser</w>, <w n="30.2">ô</w> <w n="30.3">vieux</w> <w n="30.4">soldat</w>, <w n="30.5">l</w>’<w n="30.6">étoile</w> <w n="30.7">de</w> <w n="30.8">l</w>’<w n="30.9">honneur</w> !</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Va</w> <w n="31.2">te</w> <w n="31.3">faire</w> <w n="31.4">tuer</w> <w n="31.5">maintenant</w> ; — <w n="31.6">que</w> <w n="31.7">t</w>’<w n="31.8">importe</w> ?</l>
						<l n="32" num="6.2"><w n="32.1">Tu</w> <w n="32.2">jetteras</w> <w n="32.3">encor</w> <w n="32.4">d</w>’<w n="32.5">une</w> <w n="32.6">voix</w> <w n="32.7">assez</w> <w n="32.8">forte</w>,</l>
						<l n="33" num="6.3"><w n="33.1">Un</w> <w n="33.2">cri</w> <w n="33.3">d</w>’<w n="33.4">enthousiasme</w> <w n="33.5">et</w> « <w n="33.6">Vive</w> <w n="33.7">l</w>’<w n="33.8">Empereur</w> !… »</l>
						<l n="34" num="6.4"><w n="34.1">Mais</w> <w n="34.2">les</w> <w n="34.3">rangs</w> <w n="34.4">ennemis</w> <w n="34.5">faiblissent</w> <w n="34.6">et</w> <w n="34.7">s</w>’<w n="34.8">affaissent</w>.</l>
						<l n="35" num="6.5"><w n="35.1">Leurs</w> <w n="35.2">derniers</w> <w n="35.3">bataillons</w> <w n="35.4">devant</w> <w n="35.5">vous</w> <w n="35.6">disparaissent</w>,</l>
						<l n="36" num="6.6"><w n="36.1">Napoléon</w> <w n="36.2">encor</w> <w n="36.3">se</w> <w n="36.4">promène</w> <w n="36.5">vainqueur</w> !</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Mais</w> <w n="37.2">que</w> <w n="37.3">de</w> <w n="37.4">morts</w>, <w n="37.5">grand</w> <w n="37.6">Dieu</w> ! <w n="37.7">dorment</w> <w n="37.8">dans</w> <w n="37.9">la</w> <w n="37.10">poussière</w></l>
						<l n="38" num="7.2"><w n="38.1">Qui</w> <w n="38.2">pourrait</w> <w n="38.3">les</w> <w n="38.4">compter</w> ? <w n="38.5">leurs</w> <w n="38.6">corps</w> <w n="38.7">couvrent</w> <w n="38.8">la</w> <w n="38.9">terre</w> !</l>
						<l n="39" num="7.3"><w n="39.1">A</w> <w n="39.2">l</w>’<w n="39.3">appel</w> <w n="39.4">du</w> <w n="39.5">clairon</w> <w n="39.6">ils</w> <w n="39.7">ne</w> <w n="39.8">répondront</w> <w n="39.9">plus</w> !</l>
						<l n="40" num="7.4"><w n="40.1">Un</w> <w n="40.2">lourd</w> <w n="40.3">sommeil</w> <w n="40.4">de</w> <w n="40.5">plomb</w> <w n="40.6">pèse</w> <w n="40.7">sur</w> <w n="40.8">leur</w> <w n="40.9">paupière</w> ;</l>
						<l n="41" num="7.5"><w n="41.1">Ils</w> <w n="41.2">ne</w> <w n="41.3">reverront</w> <w n="41.4">plus</w> <w n="41.5">leurs</w> <w n="41.6">parents</w>, <w n="41.7">leur</w> <w n="41.8">chaumière</w>,</l>
						<l n="42" num="7.6"><w n="42.1">Où</w> <w n="42.2">depuis</w> <w n="42.3">si</w> <w n="42.4">longtemps</w> <w n="42.5">ils</w> <w n="42.6">étaient</w> <w n="42.7">attendus</w> !</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1"><w n="43.1">Retournons</w> <w n="43.2">maintenant</w> <w n="43.3">à</w> <w n="43.4">la</w> <w n="43.5">pauvre</w> <w n="43.6">Marie</w>.</l>
						<l n="44" num="8.2"><w n="44.1">Que</w> <w n="44.2">fait</w>-<w n="44.3">elle</w> ? <w n="44.4">elle</w> <w n="44.5">espère</w>… <w n="44.6">elle</w> <w n="44.7">doute</w>… <w n="44.8">elle</w> <w n="44.9">prie</w> !</l>
						<l n="45" num="8.3"><w n="45.1">Un</w> <w n="45.2">noir</w> <w n="45.3">pressentiment</w> <w n="45.4">attriste</w> <w n="45.5">son</w> <w n="45.6">amour</w>.</l>
						<l n="46" num="8.4">— « <w n="46.1">Oh</w> ! <w n="46.2">s</w>’<w n="46.3">il</w> <w n="46.4">était</w> <w n="46.5">tué</w> ! » <w n="46.6">se</w> <w n="46.7">disait</w>-<w n="46.8">elle</w>, <w n="46.9">émue</w>.</l>
						<l n="47" num="8.5"><w n="47.1">Un</w> <w n="47.2">jour</w>, <w n="47.3">elle</w> <w n="47.4">descend</w> <w n="47.5">en</w> <w n="47.6">courant</w> ; <w n="47.7">dans</w> <w n="47.8">la</w> <w n="47.9">rue</w></l>
						<l n="48" num="8.6"><w n="48.1">Elle</w> <w n="48.2">avait</w> <w n="48.3">entendu</w> <w n="48.4">comme</w> <w n="48.5">un</w> <w n="48.6">bruit</w> <w n="48.7">de</w> <w n="48.8">tambour</w> ;</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1"><w n="49.1">Un</w> <w n="49.2">régiment</w> <w n="49.3">passait</w>. — « <w n="49.4">C</w>’<w n="49.5">est</w> <w n="49.6">le</w> <w n="49.7">sien</w>, <w n="49.8">cria</w>-<w n="49.9">t</w>-<w n="49.10">elle</w>,</l>
						<l n="50" num="9.2"><w n="50.1">Il</w> <w n="50.2">revient</w> <w n="50.3">donc</w>, <w n="50.4">enfin</w> ! » <w n="50.5">Et</w> <w n="50.6">puis</w>, <w n="50.7">elle</w> <w n="50.8">chancelle</w>,</l>
						<l n="51" num="9.3"><w n="51.1">Car</w> <w n="51.2">Louis</w> <w n="51.3">n</w>’<w n="51.4">était</w> <w n="51.5">pas</w> <w n="51.6">parmi</w> <w n="51.7">tous</w> <w n="51.8">ces</w> <w n="51.9">soldats</w>.</l>
						<l n="52" num="9.4"><w n="52.1">Elle</w> <w n="52.2">s</w>’<w n="52.3">informe</w> <w n="52.4">alors</w>, <w n="52.5">elle</w> <w n="52.6">demande</w> <w n="52.7">et</w> <w n="52.8">pleure</w> ;</l>
						<l n="53" num="9.5">— « <w n="53.1">Avez</w>-<w n="53.2">vous</w> <w n="53.3">vu</w> <w n="53.4">Louis</w> ? <w n="53.5">pourquoi</w> <w n="53.6">donc</w> <w n="53.7">à</w> <w n="53.8">cette</w> <w n="53.9">heure</w></l>
						<l n="54" num="9.6"><w n="54.1">N</w>’<w n="54.2">est</w>-<w n="54.3">il</w> <w n="54.4">pas</w> <w n="54.5">avec</w> <w n="54.6">vous</w> ? — <w n="54.7">On</w> <w n="54.8">ne</w> <w n="54.9">répondait</w> <w n="54.10">pas</w>.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1">— « <w n="55.1">Parlez</w> ; <w n="55.2">dites</w> <w n="55.3">un</w> <w n="55.4">mot</w>. <w n="55.5">J</w>’<w n="55.6">étais</w> <w n="55.7">sa</w> <w n="55.8">sœur</w> <w n="55.9">chérie</w>,</l>
						<l n="56" num="10.2"><w n="56.1">Sa</w> <w n="56.2">fiancée</w>, <w n="56.3">enfin</w> <w n="56.4">le</w> <w n="56.5">bonheur</w> <w n="56.6">de</w> <w n="56.7">sa</w> <w n="56.8">vie</w>…</l>
						<l n="57" num="10.3"><w n="57.1">Vous</w> <w n="57.2">voulez</w> <w n="57.3">m</w>’<w n="57.4">effrayer</w>, <w n="57.5">Messieurs</w> ? <w n="57.6">vous</w> <w n="57.7">avez</w> <w n="57.8">tort</w> ;</l>
						<l n="58" num="10.4"><w n="58.1">Tenez</w>, <w n="58.2">je</w> <w n="58.3">ris</w>…, <w n="58.4">parlez</w>… <w n="58.5">déjà</w> <w n="58.6">l</w>’<w n="58.7">heure</w> <w n="58.8">s</w>’<w n="58.9">écoule</w>,</l>
						<l n="59" num="10.5"><w n="59.1">Pourquoi</w> <w n="59.2">retardez</w>-<w n="59.3">vous</w> <w n="59.4">mon</w> <w n="59.5">bonheur</w> ? » — <w n="59.6">De</w> <w n="59.7">la</w> <w n="59.8">foule</w></l>
						<l n="60" num="10.6"><w n="60.1">Une</w> <w n="60.2">voix</w> <w n="60.3">s</w>’<w n="60.4">éleva</w> <w n="60.5">disant</w> : — « <w n="60.6">Louis</w> <w n="60.7">est</w> <w n="60.8">mort</w> ! »</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1">— « <w n="61.1">Mort</w> ! » — <w n="61.2">Ce</w> <w n="61.3">cri</w> <w n="61.4">de</w> <w n="61.5">l</w>’<w n="61.6">enfant</w> <w n="61.7">fut</w> <w n="61.8">la</w> <w n="61.9">seule</w> <w n="61.10">parole</w>,</l>
						<l n="62" num="11.2"><w n="62.1">Et</w> <w n="62.2">puis</w> <w n="62.3">elle</w> <w n="62.4">tomba</w> <w n="62.5">pour</w> <w n="62.6">se</w> <w n="62.7">relever</w> <w n="62.8">folle</w> !</l>
						<l n="63" num="11.3"><w n="63.1">Lorsque</w> <w n="63.2">de</w> <w n="63.3">sa</w> <w n="63.4">mansarde</w> <w n="63.5">elle</w> <w n="63.6">prit</w> <w n="63.7">le</w> <w n="63.8">chemin</w>,</l>
						<l n="64" num="11.4"><w n="64.1">Ses</w> <w n="64.2">yeux</w> <w n="64.3">étaient</w> <w n="64.4">hagards</w> ; <w n="64.5">pas</w> <w n="64.6">une</w> <w n="64.7">plainte</w> <w n="64.8">amère</w></l>
						<l n="65" num="11.5"><w n="65.1">Ne</w> <w n="65.2">sortait</w> <w n="65.3">de</w> <w n="65.4">sa</w> <w n="65.5">bouche</w>. — <w n="65.6">Elle</w> <w n="65.7">embrassa</w> <w n="65.8">sa</w> <w n="65.9">mère</w></l>
						<l n="66" num="11.6"><w n="66.1">Qui</w> <w n="66.2">quelques</w> <w n="66.3">jours</w> <w n="66.4">après</w> <w n="66.5">expirait</w> <w n="66.6">de</w> <w n="66.7">chagrin</w>.</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1"><w n="67.1">Oh</w> ! <w n="67.2">comme</w> <w n="67.3">tout</w> <w n="67.4">était</w> <w n="67.5">changé</w> <w n="67.6">dans</w> <w n="67.7">la</w> <w n="67.8">mansarde</w> !</l>
						<l n="68" num="12.2"><w n="68.1">Plus</w> <w n="68.2">de</w> <w n="68.3">chants</w>, <w n="68.4">plus</w> <w n="68.5">d</w>’<w n="68.6">ouvrage</w>, <w n="68.7">et</w> <w n="68.8">la</w> <w n="68.9">lueur</w> <w n="68.10">blafarde</w></l>
						<l n="69" num="12.3"><w n="69.1">D</w>’<w n="69.2">une</w> <w n="69.3">lampe</w> <w n="69.4">éclairait</w> <w n="69.5">tout</w> <w n="69.6">ce</w> <w n="69.7">morne</w> <w n="69.8">abandon</w>.</l>
						<l n="70" num="12.4"><w n="70.1">Les</w> <w n="70.2">voisins</w> <w n="70.3">par</w> <w n="70.4">pitié</w> <w n="70.5">secouraient</w> <w n="70.6">la</w> <w n="70.7">misère</w></l>
						<l n="71" num="12.5"><w n="71.1">De</w> <w n="71.2">Marie</w> <w n="71.3">accroupie</w> <w n="71.4">au</w> <w n="71.5">foyer</w> <w n="71.6">solitaire</w>,</l>
						<l n="72" num="12.6"><w n="72.1">Et</w> <w n="72.2">qui</w> <w n="72.3">semblait</w> <w n="72.4">n</w>’<w n="72.5">avoir</w> <w n="72.6">retenu</w> <w n="72.7">qu</w>’<w n="72.8">un</w> <w n="72.9">seul</w> <w n="72.10">nom</w> !</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1"><w n="73.1">Parfois</w>, <w n="73.2">on</w> <w n="73.3">l</w>’<w n="73.4">entendait</w>, <w n="73.5">debout</w> <w n="73.6">à</w> <w n="73.7">la</w> <w n="73.8">fenêtre</w>,</l>
						<l n="74" num="13.2"><w n="74.1">Pousser</w> <w n="74.2">un</w> <w n="74.3">long</w> <w n="74.4">éclat</w> <w n="74.5">de</w> <w n="74.6">rire</w> ; — « <w n="74.7">Il</w> <w n="74.8">va</w> <w n="74.9">paraître</w>,</l>
						<l n="75" num="13.3"><w n="75.1">Criait</w>-<w n="75.2">elle</w> <w n="75.3">aux</w> <w n="75.4">passants</w>, — <w n="75.5">il</w> <w n="75.6">revient</w> <w n="75.7">aujourd</w>’<w n="75.8">hui</w> !</l>
						<l n="76" num="13.4"><w n="76.1">Ou</w> <w n="76.2">bien</w>, <w n="76.3">elle</w> <w n="76.4">arrêtait</w> <w n="76.5">un</w> <w n="76.6">soldat</w> <w n="76.7">au</w> <w n="76.8">passage</w>,</l>
						<l n="77" num="13.5"><w n="77.1">Elle</w> <w n="77.2">le</w> <w n="77.3">regardait</w> <w n="77.4">fixement</w> <w n="77.5">au</w> <w n="77.6">visage</w>,</l>
						<l n="78" num="13.6"><w n="78.1">Et</w> <w n="78.2">le</w> <w n="78.3">laissait</w> <w n="78.4">aller</w>, <w n="78.5">disant</w> : — « <w n="78.6">Ce</w> <w n="78.7">n</w>’<w n="78.8">est</w> <w n="78.9">pas</w> <w n="78.10">lui</w> ! »</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1"><w n="79.1">Elle</w> <w n="79.2">avait</w> <w n="79.3">enlevé</w> <w n="79.4">dans</w> <w n="79.5">un</w> <w n="79.6">moment</w> <w n="79.7">lucide</w></l>
						<l n="80" num="14.2"><w n="80.1">Son</w> <w n="80.2">ruban</w> <w n="80.3">vert</w>, <w n="80.4">hélas</w>, <w n="80.5">de</w> <w n="80.6">tant</w> <w n="80.7">de</w> <w n="80.8">pleurs</w> <w n="80.9">humide</w> !</l>
						<l n="81" num="14.3"><w n="81.1">A</w> <w n="81.2">quoi</w> <w n="81.3">bon</w> <w n="81.4">désormais</w> <w n="81.5">l</w>’<w n="81.6">emblème</w> <w n="81.7">de</w> <w n="81.8">l</w>’<w n="81.9">espoir</w> ?</l>
						<l n="82" num="14.4"><w n="82.1">Seulement</w>, <w n="82.2">et</w> <w n="82.3">parfois</w>, <w n="82.4">aux</w> <w n="82.5">belles</w> <w n="82.6">amoureuses</w>,</l>
						<l n="83" num="14.5"><w n="83.1">De</w> <w n="83.2">leur</w> <w n="83.3">bonheur</w> <w n="83.4">présent</w>, <w n="83.5">si</w> <w n="83.6">fières</w>, <w n="83.7">si</w> <w n="83.8">joyeuses</w>,</l>
						<l n="84" num="14.6"><space unit="char" quantity="8"></space><w n="84.1">Elle</w> <w n="84.2">montrait</w> <w n="84.3">un</w> <w n="84.4">ruban</w> <w n="84.5">noir</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1852">1852</date>
						</dateline>
					</closer>
				</div></body></text></TEI>