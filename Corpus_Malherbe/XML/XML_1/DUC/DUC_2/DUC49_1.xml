<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLISE</head><div type="poem" key="DUC49">
					<head type="main">Élise</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">avais</w> <w n="1.3">vingt</w> <w n="1.4">ans</w> ; <w n="1.5">elle</w>, <w n="1.6">dix</w>-<w n="1.7">huit</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Nous</w> <w n="2.2">étions</w> <w n="2.3">dans</w> <w n="2.4">le</w> <w n="2.5">mois</w> <w n="2.6">des</w> <w n="2.7">roses</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">l</w>’<w n="3.3">oiseau</w>, <w n="3.4">par</w> <w n="3.5">Dieu</w> <w n="3.6">conduit</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">allions</w> <w n="4.3">loin</w> <w n="4.4">des</w> <w n="4.5">fronts</w> <w n="4.6">moroses</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Que</w> <w n="5.2">nous</w> <w n="5.3">importait</w> <w n="5.4">les</w> <w n="5.5">moqueurs</w> ?</l>
						<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2">espoir</w> <w n="6.3">était</w> <w n="6.4">notre</w> <w n="6.5">richesse</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">amour</w> <w n="7.4">chantait</w> <w n="7.5">dans</w> <w n="7.6">nos</w> <w n="7.7">cœurs</w></l>
						<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">hymne</w> <w n="8.3">de</w> <w n="8.4">la</w> <w n="8.5">blonde</w> <w n="8.6">jeunesse</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Alors</w> <w n="9.2">notre</w> <w n="9.3">seul</w> <w n="9.4">gagne</w>-<w n="9.5">pain</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Était</w> <w n="10.2">une</w> <w n="10.3">vieille</w> <w n="10.4">guitare</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Nous</w> <w n="11.2">ne</w> <w n="11.3">chantions</w> <w n="11.4">jamais</w> <w n="11.5">en</w> <w n="11.6">vain</w> ;</l>
						<l n="12" num="3.4"><w n="12.1">Pour</w> <w n="12.2">nous</w> <w n="12.3">on</w> <w n="12.4">n</w>’<w n="12.5">était</w> <w n="12.6">point</w> <w n="12.7">avare</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Dans</w> <w n="13.2">le</w> <w n="13.3">bois</w>, <w n="13.4">quand</w> <w n="13.5">le</w> <w n="13.6">firmament</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Déployait</w> <w n="14.2">ses</w> <w n="14.3">nocturnes</w> <w n="14.4">voiles</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Nous</w> <w n="15.2">nous</w> <w n="15.3">endormions</w> <w n="15.4">doucement</w>.</l>
						<l n="16" num="4.4"><w n="16.1">En</w> <w n="16.2">causant</w> <w n="16.3">avec</w> <w n="16.4">les</w> <w n="16.5">étoiles</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">En</w> <w n="17.2">route</w> <w n="17.3">dès</w> <w n="17.4">le</w> <w n="17.5">point</w> <w n="17.6">du</w> <w n="17.7">jour</w>,</l>
						<l n="18" num="5.2"><w n="18.1">A</w> <w n="18.2">midi</w> <w n="18.3">nous</w> <w n="18.4">cherchions</w> <w n="18.5">l</w>’<w n="18.6">ombrage</w></l>
						<l n="19" num="5.3"><w n="19.1">Où</w>, <w n="19.2">pour</w> <w n="19.3">écouter</w> <w n="19.4">notre</w> <w n="19.5">amour</w>,</l>
						<l n="20" num="5.4"><w n="20.1">L</w>’<w n="20.2">oiseau</w> <w n="20.3">suspendait</w> <w n="20.4">son</w> <w n="20.5">ramage</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">L</w>’<w n="21.2">amour</w> ! <w n="21.3">C</w>’<w n="21.4">est</w> <w n="21.5">le</w> <w n="21.6">premier</w> <w n="21.7">besoin</w></l>
						<l n="22" num="6.2"><w n="22.1">Pour</w> <w n="22.2">le</w> <w n="22.3">poète</w> <w n="22.4">et</w> <w n="22.5">pour</w> <w n="22.6">la</w> <w n="22.7">femme</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">nous</w> <w n="23.3">n</w>’<w n="23.4">avions</w> <w n="23.5">pour</w> <w n="23.6">seul</w> <w n="23.7">témoin</w></l>
						<l n="24" num="6.4"><w n="24.1">Que</w> <w n="24.2">Dieu</w>… <w n="24.3">qui</w> <w n="24.4">souriait</w>, <w n="24.5">Madame</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Puis</w>, <w n="25.2">le</w> <w n="25.3">soir</w>, <w n="25.4">dans</w> <w n="25.5">l</w>’<w n="25.6">humble</w> <w n="25.7">hameau</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Qu</w>’<w n="26.2">abrite</w> <w n="26.3">le</w> <w n="26.4">bois</w> <w n="26.5">sur</w> <w n="26.6">sa</w> <w n="26.7">berge</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Nous</w> <w n="27.2">chantions</w> <w n="27.3">un</w> <w n="27.4">refrain</w> <w n="27.5">nouveau</w></l>
						<l n="28" num="7.4"><w n="28.1">Devant</w> <w n="28.2">la</w> <w n="28.3">porte</w> <w n="28.4">d</w>’<w n="28.5">une</w> <w n="28.6">auberge</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">On</w> <w n="29.2">nous</w> <w n="29.3">fêtait</w> !… <w n="29.4">il</w> <w n="29.5">fallait</w> <w n="29.6">voir</w> !</l>
						<l n="30" num="8.2"><w n="30.1">On</w> <w n="30.2">nous</w> <w n="30.3">appelait</w> <w n="30.4">des</w> <hi rend="ital"><w n="30.5">artistes</w></hi> !</l>
						<l n="31" num="8.3"><w n="31.1">On</w> <w n="31.2">enviait</w> <w n="31.3">le</w> <w n="31.4">gai</w> <w n="31.5">savoir</w></l>
						<l n="32" num="8.4"><w n="32.1">Des</w> <w n="32.2">deux</w> <w n="32.3">nomades</w> <w n="32.4">guitaristes</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">les</w> <w n="33.3">filles</w> <w n="33.4">et</w> <w n="33.5">les</w> <w n="33.6">garçons</w></l>
						<l n="34" num="9.2"><w n="34.1">Retenaient</w> <w n="34.2">l</w>’<w n="34.3">air</w> <w n="34.4">et</w> <w n="34.5">les</w> <w n="34.6">paroles</w></l>
						<l n="35" num="9.3"><w n="35.1">De</w> <w n="35.2">mes</w> <w n="35.3">incorrectes</w> <w n="35.4">chansons</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Pour</w> <w n="36.2">les</w> <w n="36.3">chanter</w> <w n="36.4">sous</w> <w n="36.5">les</w> <w n="36.6">vieux</w> <w n="36.7">saules</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">puis</w>, <w n="37.3">nous</w> <w n="37.4">tenant</w> <w n="37.5">par</w> <w n="37.6">la</w> <w n="37.7">main</w>,</l>
						<l n="38" num="10.2"><w n="38.1">A</w> <w n="38.2">pied</w>, <w n="38.3">sans</w> <w n="38.4">regrets</w> <w n="38.5">inutiles</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Nous</w> <w n="39.2">poursuivions</w> <w n="39.3">notre</w> <w n="39.4">chemin</w>,</l>
						<l n="40" num="10.4"><w n="40.1">En</w> <w n="40.2">évitant</w> <w n="40.3">les</w> <w n="40.4">grandes</w> <w n="40.5">villes</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Car</w> <w n="41.2">il</w> <w n="41.3">nous</w> <w n="41.4">fallait</w> <w n="41.5">le</w> <w n="41.6">ciel</w> <w n="41.7">bleu</w>,</l>
						<l n="42" num="11.2"><w n="42.1">La</w> <w n="42.2">brise</w> <w n="42.3">à</w> <w n="42.4">la</w> <w n="42.5">suave</w> <w n="42.6">haleine</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Et</w> <w n="43.2">toutes</w> <w n="43.3">ces</w> <w n="43.4">voix</w> <w n="43.5">du</w> <w n="43.6">Bon</w> <w n="43.7">Dieu</w></l>
						<l n="44" num="11.4"><w n="44.1">Qui</w> <w n="44.2">passent</w> <w n="44.3">du</w> <w n="44.4">val</w> <w n="44.5">dans</w> <w n="44.6">la</w> <w n="44.7">plaine</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Pauvre</w> <w n="45.2">Élise</w> ! <w n="45.3">Comme</w> <w n="45.4">elle</w> <w n="45.5">était</w></l>
						<l n="46" num="12.2"><w n="46.1">Dans</w> <w n="46.2">ce</w> <w n="46.3">temps</w>-<w n="46.4">là</w> <w n="46.5">folle</w> <w n="46.6">et</w> <w n="46.7">rieuse</w> !</l>
						<l n="47" num="12.3"><w n="47.1">A</w> <w n="47.2">tous</w> <w n="47.3">les</w> <w n="47.4">vents</w> <w n="47.5">elle</w> <w n="47.6">jetait</w></l>
						<l n="48" num="12.4"><w n="48.1">Le</w> <w n="48.2">cri</w> <w n="48.3">de</w> <w n="48.4">son</w> <w n="48.5">âme</w> <w n="48.6">joyeuse</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Il</w> <w n="49.2">me</w> <w n="49.3">semble</w> <w n="49.4">la</w> <w n="49.5">voir</w> <w n="49.6">toujours</w> :</l>
						<l n="50" num="13.2"><w n="50.1">Elle</w> <w n="50.2">avait</w> <w n="50.3">une</w> <w n="50.4">robe</w> <w n="50.5">grise</w></l>
						<l n="51" num="13.3"><w n="51.1">Qui</w> <w n="51.2">dessinait</w> <w n="51.3">les</w> <w n="51.4">purs</w> <w n="51.5">contours</w></l>
						<l n="52" num="13.4"><w n="52.1">D</w>’<w n="52.2">une</w> <w n="52.3">taille</w> <w n="52.4">fine</w> <w n="52.5">et</w> <w n="52.6">bien</w> <w n="52.7">prise</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Elle</w> <w n="53.2">eût</w> <w n="53.3">tenu</w> <w n="53.4">dans</w> <w n="53.5">mes</w> <w n="53.6">dix</w> <w n="53.7">doigts</w>,</l>
						<l n="54" num="14.2"><w n="54.1">Sa</w> <w n="54.2">taille</w> <w n="54.3">à</w> <w n="54.4">nulle</w> <w n="54.5">autre</w> <w n="54.6">pareille</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Flexible</w> <w n="55.2">à</w> <w n="55.3">faire</w>, <w n="55.4">mille</w> <w n="55.5">fois</w>,</l>
						<l n="56" num="14.4"><w n="56.1">De</w> <w n="56.2">dépit</w> <w n="56.3">mourir</w> <w n="56.4">une</w> <w n="56.5">abeille</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Un</w> <w n="57.2">charmant</w> <w n="57.3">sourire</w> <w n="57.4">moqueur</w></l>
						<l n="58" num="15.2"><w n="58.1">Courait</w> <w n="58.2">sur</w> <w n="58.3">ses</w> <w n="58.4">lèvres</w> <w n="58.5">humides</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Doux</w> <w n="59.2">nid</w> ! <w n="59.3">où</w>, <w n="59.4">chers</w> <w n="59.5">oiseaux</w> <w n="59.6">du</w> <w n="59.7">cœur</w>,</l>
						<l n="60" num="15.4"><w n="60.1">S</w>’<w n="60.2">envolaient</w> <w n="60.3">mes</w> <w n="60.4">baisers</w> <w n="60.5">timides</w> !</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Nous</w> <w n="61.2">nous</w> <w n="61.3">étions</w> <w n="61.4">connus</w> <w n="61.5">un</w> <w n="61.6">soir</w></l>
						<l n="62" num="16.2"><w n="62.1">Que</w> <w n="62.2">le</w> <w n="62.3">printemps</w> <w n="62.4">était</w> <w n="62.5">en</w> <w n="62.6">fête</w>.</l>
						<l n="63" num="16.3"><w n="63.1">Nous</w> <w n="63.2">confondîmes</w> <w n="63.3">notre</w> <w n="63.4">avoir</w> :</l>
						<l n="64" num="16.4"><w n="64.1">Guitare</w> <w n="64.2">et</w> <w n="64.3">gosier</w> <w n="64.4">de</w> <w n="64.5">fauvette</w>.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Tout</w> <w n="65.2">fut</w> <w n="65.3">commun</w> <w n="65.4">dès</w> <w n="65.5">ce</w> <w n="65.6">soir</w>-<w n="65.7">là</w> :</l>
						<l n="66" num="17.2"><w n="66.1">Heure</w> <w n="66.2">triste</w>, <w n="66.3">heure</w> <w n="66.4">fortunée</w>,</l>
						<l n="67" num="17.3"><w n="67.1">Et</w> <w n="67.2">nous</w> <w n="67.3">baptisâmes</w> <w n="67.4">cela</w> :</l>
						<l n="68" num="17.4"><w n="68.1">Une</w> <w n="68.2">misère</w> <w n="68.3">couronnée</w> !</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">L</w>’<w n="69.2">avenir</w> <w n="69.3">sombre</w> <w n="69.4">menaçait</w> !</l>
						<l n="70" num="18.2"><w n="70.1">Mais</w> <w n="70.2">nous</w> <w n="70.3">étions</w> <w n="70.4">si</w> <w n="70.5">bien</w> <w n="70.6">ensemble</w>,</l>
						<l n="71" num="18.3"><w n="71.1">Comme</w> <w n="71.2">ces</w> <w n="71.3">oiseaux</w> <w n="71.4">de</w> <w n="71.5">Musset</w>,</l>
						<l n="72" num="18.4"><hi rend="ital"><w n="72.1">Qu</w>’<w n="72.2">un</w> <w n="72.3">même</w> <w n="72.4">coup</w> <w n="72.5">de</w> <w n="72.6">vent</w> <w n="72.7">rassemble</w></hi>.</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><w n="73.1">Mais</w> <w n="73.2">le</w> <w n="73.3">printemps</w> <w n="73.4">vint</w> <w n="73.5">à</w> <w n="73.6">finir</w>,</l>
						<l n="74" num="19.2"><w n="74.1">Puis</w> <w n="74.2">l</w>’<w n="74.3">été</w> <w n="74.4">perdit</w> <w n="74.5">sa</w> <w n="74.6">couronne</w>,</l>
						<l n="75" num="19.3"><w n="75.1">Et</w> <w n="75.2">je</w> <w n="75.3">vis</w> <w n="75.4">Élise</w> <w n="75.5">pâlir</w>,</l>
						<l n="76" num="19.4"><w n="76.1">Aux</w> <w n="76.2">premiers</w> <w n="76.3">souffles</w> <w n="76.4">de</w> <w n="76.5">l</w>’<w n="76.6">automne</w> !</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1"><w n="77.1">Une</w> <w n="77.2">tache</w> <w n="77.3">rouge</w> <w n="77.4">naissait</w></l>
						<l n="78" num="20.2"><w n="78.1">Sur</w> <w n="78.2">sa</w> <w n="78.3">joue</w> <w n="78.4">aux</w> <w n="78.5">teintes</w> <w n="78.6">d</w>’<w n="78.7">opale</w>,</l>
						<l n="79" num="20.3"><w n="79.1">Et</w> <w n="79.2">plus</w> <w n="79.3">la</w> <w n="79.4">tache</w> <w n="79.5">rougissait</w>,</l>
						<l n="80" num="20.4"><w n="80.1">Plus</w> <w n="80.2">la</w> <w n="80.3">joue</w> <w n="80.4">encore</w> <w n="80.5">était</w> <w n="80.6">pâle</w> !</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1"><w n="81.1">Une</w> <w n="81.2">toux</w> <w n="81.3">sèche</w> <w n="81.4">déchirait</w></l>
						<l n="82" num="21.2"><w n="82.1">Sa</w> <w n="82.2">poitrine</w>, <w n="82.3">et</w> — <w n="82.4">muet</w> <w n="82.5">martyre</w> !</l>
						<l n="83" num="21.3">— <w n="83.1">En</w> <w n="83.2">souriant</w>, <w n="83.3">elle</w> <w n="83.4">souffrait</w>…</l>
						<l n="84" num="21.4"><w n="84.1">Qu</w>’<w n="84.2">il</w> <w n="84.3">me</w> <w n="84.4">faisait</w> <w n="84.5">mal</w>, <w n="84.6">son</w> <w n="84.7">sourire</w> !</l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1"><w n="85.1">Et</w> <w n="85.2">je</w> <w n="85.3">me</w> <w n="85.4">cachais</w> <w n="85.5">pour</w> <w n="85.6">pleurer</w>,</l>
						<l n="86" num="22.2"><w n="86.1">En</w> <w n="86.2">priant</w> <w n="86.3">Dieu</w> <w n="86.4">du</w> <w n="86.5">fond</w> <w n="86.6">de</w> <w n="86.7">l</w>’<w n="86.8">âme</w> !…</l>
						<l n="87" num="22.3">— « <w n="87.1">A</w> <w n="87.2">quoi</w> <w n="87.3">bon</w>, <w n="87.4">ami</w>, <w n="87.5">nous</w> <w n="87.6">leurrer</w>,</l>
						<l n="88" num="22.4"><w n="88.1">Disait</w>-<w n="88.2">elle</w>, <w n="88.3">Dieu</w> <w n="88.4">me</w> <w n="88.5">réclame</w>.</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1">« <w n="89.1">Ta</w> <w n="89.2">fauvette</w> <w n="89.3">va</w> <w n="89.4">s</w>’<w n="89.5">envoler</w> :</l>
						<l n="90" num="23.2"><w n="90.1">La</w> <w n="90.2">tempête</w> <w n="90.3">a</w> <w n="90.4">brisé</w> <w n="90.5">la</w> <w n="90.6">cage</w> !</l>
						<l n="91" num="23.3"><w n="91.1">Ami</w>, <w n="91.2">pourquoi</w> <w n="91.3">te</w> <w n="91.4">désoler</w> ?</l>
						<l n="92" num="23.4"><w n="92.1">Il</w> <w n="92.2">est</w>, <w n="92.3">là</w>-<w n="92.4">haut</w>, <w n="92.5">un</w> <w n="92.6">doux</w> <w n="92.7">bocage</w></l>
					</lg>
					<lg n="24">
						<l n="93" num="24.1"><w n="93.1">Où</w> <w n="93.2">les</w> <w n="93.3">fauvettes</w>, <w n="93.4">comme</w> <w n="93.5">moi</w>,</l>
						<l n="94" num="24.2"><w n="94.1">Quand</w> <w n="94.2">sonnent</w> <w n="94.3">les</w> <w n="94.4">heures</w> <w n="94.5">amères</w>,</l>
						<l n="95" num="24.3"><w n="95.1">Quand</w> <w n="95.2">vient</w> <w n="95.3">la</w> <w n="95.4">saison</w> <w n="95.5">du</w> <w n="95.6">grand</w> <w n="95.7">froid</w>,</l>
						<l n="96" num="24.4"><w n="96.1">Pour</w> <w n="96.2">chanter</w>, <w n="96.3">retrouvent</w> <w n="96.4">leurs</w> <w n="96.5">mères</w> ! »</l>
					</lg>
					<lg n="25">
						<l n="97" num="25.1"><w n="97.1">Et</w> <w n="97.2">tandis</w> <w n="97.3">qu</w>’<w n="97.4">elle</w> <w n="97.5">me</w> <w n="97.6">parlait</w>,</l>
						<l n="98" num="25.2"><w n="98.1">A</w> <w n="98.2">ses</w> <w n="98.3">lèvres</w> <w n="98.4">presque</w> <w n="98.5">mi</w>-<w n="98.6">closes</w>,</l>
						<l n="99" num="25.3">— <w n="99.1">O</w> <w n="99.2">chères</w> <w n="99.3">lèvres</w> ! — <w n="99.4">il</w> <w n="99.5">perlait</w></l>
						<l n="100" num="25.4"><w n="100.1">Comme</w> <w n="100.2">des</w> <w n="100.3">gouttelettes</w> <w n="100.4">roses</w> !</l>
					</lg>
					<lg n="26">
						<l n="101" num="26.1"><w n="101.1">Pauvre</w> <w n="101.2">fille</w>, <w n="101.3">ton</w> <w n="101.4">cœur</w> <w n="101.5">rêvait</w></l>
						<l n="102" num="26.2"><w n="102.1">La</w> <w n="102.2">vie</w> <w n="102.3">au</w> <w n="102.4">grand</w> <w n="102.5">soleil</w> <w n="102.6">qui</w> <w n="102.7">brille</w>…</l>
						<l n="103" num="26.3"><w n="103.1">Et</w> <w n="103.2">puis</w>, <w n="103.3">un</w> <w n="103.4">matin</w> <w n="103.5">qu</w>’<w n="103.6">il</w> <w n="103.7">pleuvait</w>,</l>
						<l n="104" num="26.4"><w n="104.1">La</w> <w n="104.2">mort</w> <w n="104.3">te</w> <w n="104.4">prit</w> ! <w n="104.5">O</w> <w n="104.6">pauvre</w> <w n="104.7">fille</w> !</l>
					</lg>
					<lg n="27">
						<l n="105" num="27.1">— « <hi rend="ital"><w n="105.1">Adieu</w> !… <w n="105.2">je</w> <w n="105.3">m</w>’<w n="105.4">en</w> <w n="105.5">vais</w> !… <w n="105.6">souviens</w>-<w n="105.7">toi</w></hi> !…</l>
						<l n="106" num="27.2"><w n="106.1">Ce</w> <w n="106.2">fût</w> <w n="106.3">sa</w> <w n="106.4">parole</w> <w n="106.5">dernière</w> !</l>
						<l n="107" num="27.3"><w n="107.1">Mon</w> <w n="107.2">cœur</w>, <w n="107.3">qui</w> <w n="107.4">suivit</w> <w n="107.5">son</w> <w n="107.6">convoi</w>,</l>
						<l n="108" num="27.4"><w n="108.1">Ne</w> <w n="108.2">revint</w> <w n="108.3">pas</w> <w n="108.4">du</w> <w n="108.5">cimetière</w> !</l>
					</lg>
					<lg n="28">
						<l n="109" num="28.1"><w n="109.1">Si</w> <w n="109.2">notre</w> <w n="109.3">amour</w> <w n="109.4">n</w>’<w n="109.5">eut</w> <w n="109.6">qu</w>’<w n="109.7">un</w> <w n="109.8">printemps</w>,</l>
						<l n="110" num="28.2"><w n="110.1">Chère</w> <w n="110.2">Élise</w>, <w n="110.3">il</w> <w n="110.4">fut</w> <w n="110.5">plein</w> <w n="110.6">de</w> <w n="110.7">charmes</w>,</l>
						<l n="111" num="28.3"><w n="111.1">Il</w> <w n="111.2">met</w> <w n="111.3">encore</w>, <w n="111.4">après</w> <w n="111.5">vingt</w> <w n="111.6">ans</w>,</l>
						<l n="112" num="28.4"><w n="112.1">Ton</w> <w n="112.2">sourire</w> <w n="112.3">à</w> <w n="112.4">travers</w> <w n="112.5">mes</w> <w n="112.6">larmes</w>.</l>
					</lg>
					<lg n="29">
						<l n="113" num="29.1"><w n="113.1">Un</w> <w n="113.2">jour</w>, <w n="113.3">quand</w> <w n="113.4">Dieu</w> <w n="113.5">m</w>’<w n="113.6">appellera</w>,</l>
						<l n="114" num="29.2"><w n="114.1">Pour</w> <w n="114.2">nous</w> <w n="114.3">revoir</w>, <w n="114.4">ô</w> <w n="114.5">chère</w> <w n="114.6">morte</w> !</l>
						<l n="115" num="29.3"><w n="115.1">C</w>’<w n="115.2">est</w> <w n="115.3">ta</w> <w n="115.4">main</w>, <w n="115.5">ta</w> <w n="115.6">main</w> <w n="115.7">qui</w> <w n="115.8">viendra</w></l>
						<l n="116" num="29.4"><w n="116.1">Du</w> <w n="116.2">Paradis</w> <w n="116.3">m</w>’<w n="116.4">ouvrir</w> <w n="116.5">la</w> <w n="116.6">porte</w> !</l>
					</lg>
					<lg n="30">
						<l n="117" num="30.1"><w n="117.1">De</w> <w n="117.2">mes</w> <w n="117.3">fiers</w> <w n="117.4">vingt</w> <w n="117.5">ans</w> <w n="117.6">disparus</w>,</l>
						<l n="118" num="30.2"><w n="118.1">Il</w> <w n="118.2">fut</w> <w n="118.3">court</w>, <w n="118.4">le</w> <w n="118.5">joyeux</w> <w n="118.6">poème</w> !</l>
						<l n="119" num="30.3"><w n="119.1">Trop</w> <w n="119.2">courts</w>, <w n="119.3">les</w> <w n="119.4">chemins</w> <w n="119.5">parcourus</w></l>
						<l n="120" num="30.4"><w n="120.1">Au</w> <w n="120.2">lointain</w> <w n="120.3">pays</w> <w n="120.4">de</w> <w n="120.5">Bohème</w> !</l>
					</lg>
					<lg n="31">
						<l n="121" num="31.1"><w n="121.1">Mais</w> <w n="121.2">chaque</w> <w n="121.3">automne</w> <w n="121.4">qui</w> <w n="121.5">revient</w></l>
						<l n="122" num="31.2"><w n="122.1">M</w>’<w n="122.2">apporte</w> <w n="122.3">une</w> <w n="122.4">chanson</w> <w n="122.5">touchante</w> ;</l>
						<l n="123" num="31.3"><w n="123.1">C</w>’<w n="123.2">est</w>, <w n="123.3">dans</w> <w n="123.4">mon</w> <w n="123.5">cœur</w> <w n="123.6">qui</w> <w n="123.7">se</w> <w n="123.8">souvient</w>,</l>
						<l n="124" num="31.4"><w n="124.1">Ma</w> <w n="124.2">pauvre</w> <w n="124.3">fauvette</w> <w n="124.4">qui</w> <w n="124.5">chante</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1870">1870</date>
						</dateline>
					</closer>
				</div></body></text></TEI>