<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC52">
				<head type="main">Maud</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Elle</w> <w n="1.2">a</w> <w n="1.3">des</w> <w n="1.4">poses</w> <w n="1.5">de</w> <w n="1.6">félin</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">d</w>’<w n="2.3">une</w> <w n="2.4">tunique</w> <w n="2.5">de</w> <w n="2.6">lin</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Elle</w> <w n="3.2">habille</w> <w n="3.3">ses</w> <w n="3.4">formes</w> <w n="3.5">souples</w> ;</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">Du</w> <w n="4.2">félin</w> <w n="4.3">elle</w> <w n="4.4">a</w> <w n="4.5">les</w> <w n="4.6">yeux</w> <w n="4.7">clairs</w>,</l>
					<l n="5" num="2.2"><w n="5.1">Où</w> <w n="5.2">parfois</w> <w n="5.3">brillent</w> <w n="5.4">des</w> <w n="5.5">éclairs</w>,</l>
					<l n="6" num="2.3"><w n="6.1">Des</w> <w n="6.2">éclairs</w> <w n="6.3">qui</w> <w n="6.4">passent</w> <w n="6.5">par</w> <w n="6.6">couples</w>.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">Pleine</w> <w n="7.2">d</w>’<w n="7.3">ineffables</w> <w n="7.4">douceurs</w>,</l>
					<l n="8" num="3.2"><w n="8.1">Sa</w> <w n="8.2">voix</w> <w n="8.3">musicale</w> <w n="8.4">a</w> <w n="8.5">pour</w> <w n="8.6">sœurs</w></l>
					<l n="9" num="3.3"><w n="9.1">Celle</w> <w n="9.2">des</w> <w n="9.3">oiseaux</w> <w n="9.4">et</w> <w n="9.5">des</w> <w n="9.6">anges</w> ;</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1"><w n="10.1">Elle</w> <w n="10.2">est</w> <w n="10.3">comme</w> <w n="10.4">un</w> <w n="10.5">concert</w> <w n="10.6">des</w> <w n="10.7">cieux</w>,</l>
					<l n="11" num="4.2"><w n="11.1">Ou</w> <w n="11.2">des</w> <w n="11.3">couchants</w> <w n="11.4">délicieux</w>,</l>
					<l n="12" num="4.3"><w n="12.1">Un</w> <w n="12.2">gazouillement</w> <w n="12.3">de</w> <w n="12.4">mésanges</w>.</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1"><w n="13.1">La</w> <w n="13.2">charmeresse</w> ! — <w n="13.3">Elle</w> <w n="13.4">nous</w> <w n="13.5">prend</w></l>
					<l n="14" num="5.2"><w n="14.1">Tout</w> <w n="14.2">notre</w> <w n="14.3">cœur</w> <w n="14.4">et</w> <w n="14.5">nous</w> <w n="14.6">le</w> <w n="14.7">rend</w>,</l>
					<l n="15" num="5.3"><w n="15.1">Saignant</w> <w n="15.2">d</w>’<w n="15.3">une</w> <w n="15.4">chère</w> <w n="15.5">morsure</w> !</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1"><w n="16.1">Et</w> <w n="16.2">l</w>’<w n="16.3">on</w> <w n="16.4">ne</w> <w n="16.5">veut</w> <w n="16.6">pas</w> <w n="16.7">en</w> <w n="16.8">guérir</w>,</l>
					<l n="17" num="6.2"><w n="17.1">Et</w> <w n="17.2">l</w>’<w n="17.3">on</w> <w n="17.4">préférerait</w> <w n="17.5">mourir</w></l>
					<l n="18" num="6.3"><w n="18.1">Que</w> <w n="18.2">de</w> <w n="18.3">vivre</w> <w n="18.4">sans</w> <w n="18.5">sa</w> <w n="18.6">blessure</w> !</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1"><w n="19.1">Et</w> <w n="19.2">l</w>’<w n="19.3">on</w> <w n="19.4">brûlerait</w> <w n="19.5">à</w> <w n="19.6">ses</w> <w n="19.7">pieds</w></l>
					<l n="20" num="7.2"><w n="20.1">Les</w> <w n="20.2">parfums</w> <w n="20.3">des</w> <w n="20.4">riches</w> <w n="20.5">trépieds</w></l>
					<l n="21" num="7.3"><w n="21.1">Qui</w> <w n="21.2">fument</w> <w n="21.3">aux</w> <w n="21.4">sérails</w> <w n="21.5">d</w>’<w n="21.6">Asie</w> ;</l>
				</lg>
				<lg n="8">
					<l n="22" num="8.1"><w n="22.1">Et</w> <w n="22.2">se</w> <w n="22.3">prosternant</w>, <w n="22.4">on</w> <w n="22.5">voudrait</w></l>
					<l n="23" num="8.2"><w n="23.1">Baiser</w> <w n="23.2">la</w> <w n="23.3">poudre</w> <w n="23.4">ou</w> <w n="23.5">marcherait</w></l>
					<l n="24" num="8.3"><w n="24.1">Cette</w> <w n="24.2">vertueuse</w> <w n="24.3">Aspasie</w> !</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1"><w n="25.1">Car</w> <w n="25.2">malgré</w> <w n="25.3">son</w> <w n="25.4">regard</w> <w n="25.5">lascif</w>,</l>
					<l n="26" num="9.2"><w n="26.1">Et</w> <w n="26.2">parfois</w> <w n="26.3">chastement</w> <w n="26.4">pensif</w>,</l>
					<l n="27" num="9.3"><w n="27.1">Qui</w> <w n="27.2">prend</w> <w n="27.3">tout</w> <w n="27.4">l</w>’<w n="27.5">être</w> <w n="27.6">et</w> <w n="27.7">le</w> <w n="27.8">dévore</w>,</l>
				</lg>
				<lg n="10">
					<l n="28" num="10.1"><w n="28.1">Il</w> <w n="28.2">faut</w> <w n="28.3">la</w> <w n="28.4">fuir</w> <w n="28.5">et</w> <w n="28.6">se</w> <w n="28.7">garder</w></l>
					<l n="29" num="10.2"><w n="29.1">De</w> <w n="29.2">loin</w>, <w n="29.3">bien</w> <w n="29.4">loin</w>, <w n="29.5">la</w> <w n="29.6">regarder</w>…</l>
					<l n="30" num="10.3"><w n="30.1">Et</w> <w n="30.2">vers</w> <w n="30.3">elle</w> <w n="30.4">venir</w> <w n="30.5">encore</w> !</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1"><w n="31.1">Mais</w> <w n="31.2">à</w> <w n="31.3">quoi</w> <w n="31.4">bon</w> <w n="31.5">fuir</w> ?… <w n="31.6">la</w> <w n="31.7">voilà</w> !</l>
					<l n="32" num="11.2"><w n="32.1">Le</w> <w n="32.2">désir</w> <w n="32.3">vous</w> <w n="32.4">emporte</w> !… <w n="32.5">Elle</w> <w n="32.6">a</w></l>
					<l n="33" num="11.3"><w n="33.1">L</w>’<w n="33.2">œil</w> <w n="33.3">de</w> <w n="33.4">la</w> <w n="33.5">colombe</w> <w n="33.6">et</w> <w n="33.7">du</w> <w n="33.8">fauve</w> ;</l>
				</lg>
				<lg n="12">
					<l n="34" num="12.1"><w n="34.1">Le</w> <w n="34.2">désir</w> <w n="34.3">se</w> <w n="34.4">change</w> <w n="34.5">en</w> <w n="34.6">respect</w>,</l>
					<l n="35" num="12.2"><w n="35.1">Et</w> <w n="35.2">nous</w> <w n="35.3">rêvons</w> <w n="35.4">à</w> <w n="35.5">son</w> <w n="35.6">aspect</w>,</l>
					<l n="36" num="12.3"><w n="36.1">Du</w> <w n="36.2">sanctuaire</w>… <w n="36.3">et</w> <w n="36.4">de</w> <w n="36.5">l</w>’<w n="36.6">alcôve</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1874">1874</date>
					</dateline>
				</closer>
			</div></body></text></TEI>