<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC16">
				<head type="main">Le Ménestrel</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">Pour</w> <w n="1.2">seul</w> <w n="1.3">bien</w> <w n="1.4">sur</w> <w n="1.5">la</w> <w n="1.6">terre</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">J</w>’<w n="2.2">ai</w> <w n="2.3">Mandore</w> <w n="2.4">légère</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Et</w> <w n="3.2">la</w> <w n="3.3">plume</w> <w n="3.4">au</w> <w n="3.5">Chapel</w>.</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Mais</w> <w n="4.2">toujours</w> <w n="4.3">gai</w>, <w n="4.4">je</w> <w n="4.5">chante</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">Lai</w>, <w n="5.2">complainte</w>, <w n="5.3">sirvente</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Chanson</w> <w n="6.2">d</w>’<w n="6.3">amour</w> <w n="6.4">du</w> <w n="6.5">Ménestrel</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><space unit="char" quantity="4"></space><w n="7.1">En</w> <w n="7.2">parcourant</w> <w n="7.3">le</w> <w n="7.4">monde</w>,</l>
					<l n="8" num="2.2"><space unit="char" quantity="4"></space><w n="8.1">Tout</w> <w n="8.2">seigneur</w>, <w n="8.3">à</w> <w n="8.4">la</w> <w n="8.5">ronde</w>,</l>
					<l n="9" num="2.3"><space unit="char" quantity="4"></space><w n="9.1">Me</w> <w n="9.2">reçoit</w> <w n="9.3">au</w> <w n="9.4">Castel</w> ;</l>
					<l n="10" num="2.4"><space unit="char" quantity="4"></space><w n="10.1">Puis</w> <w n="10.2">à</w> <w n="10.3">la</w> <w n="10.4">Châtelaine</w></l>
					<l n="11" num="2.5"><space unit="char" quantity="4"></space><w n="11.1">Je</w> <w n="11.2">vais</w> <w n="11.3">dire</w> <w n="11.4">ma</w> <w n="11.5">peine</w> ;</l>
					<l n="12" num="2.6"><w n="12.1">Chanson</w> <w n="12.2">d</w>’<w n="12.3">amour</w> <w n="12.4">du</w> <w n="12.5">Ménestrel</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><space unit="char" quantity="4"></space><w n="13.1">Du</w> <w n="13.2">haut</w> <w n="13.3">de</w> <w n="13.4">la</w> <w n="13.5">tourelle</w>,</l>
					<l n="14" num="3.2"><space unit="char" quantity="4"></space><w n="14.1">La</w> <w n="14.2">gentille</w> <w n="14.3">pucelle</w>,</l>
					<l n="15" num="3.3"><space unit="char" quantity="4"></space><w n="15.1">Songeant</w> <w n="15.2">au</w> <w n="15.3">Damoisel</w>,</l>
					<l n="16" num="3.4"><space unit="char" quantity="4"></space><w n="16.1">A</w> <w n="16.2">l</w>’<w n="16.3">espoir</w> <w n="16.4">s</w>’<w n="16.5">abandonne</w>,</l>
					<l n="17" num="3.5"><space unit="char" quantity="4"></space><w n="17.1">Lorsque</w> <w n="17.2">ma</w> <w n="17.3">voix</w> <w n="17.4">entonne</w>,</l>
					<l n="18" num="3.6"><w n="18.1">Chanson</w> <w n="18.2">d</w>’<w n="18.3">amour</w> <w n="18.4">du</w> <w n="18.5">Ménestrel</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><space unit="char" quantity="4"></space><w n="19.1">Des</w> <w n="19.2">preux</w> <w n="19.3">de</w> <w n="19.4">la</w> <w n="19.5">patrie</w>,</l>
					<l n="20" num="4.2"><space unit="char" quantity="4"></space><w n="20.1">Je</w> <w n="20.2">célèbre</w> <w n="20.3">la</w> <w n="20.4">vie</w> ;</l>
					<l n="21" num="4.3"><space unit="char" quantity="4"></space><w n="21.1">Et</w> <w n="21.2">comme</w> <w n="21.3">en</w> <w n="21.4">un</w> <w n="21.5">cartel</w></l>
					<l n="22" num="4.4"><space unit="char" quantity="4"></space><w n="22.1">Je</w> <w n="22.2">grave</w> <w n="22.3">la</w> <w n="22.4">victoire</w>,</l>
					<l n="23" num="4.5"><space unit="char" quantity="4"></space><w n="23.1">Et</w> <w n="23.2">joins</w> <w n="23.3">au</w> <w n="23.4">chant</w> <w n="23.5">de</w> <w n="23.6">gloire</w>,</l>
					<l n="24" num="4.6"><w n="24.1">Chanson</w> <w n="24.2">d</w>’<w n="24.3">amour</w> <w n="24.4">du</w> <w n="24.5">Ménestrel</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><space unit="char" quantity="4"></space><w n="25.1">Puisqu</w>’<w n="25.2">il</w> <w n="25.3">faut</w> <w n="25.4">que</w> <w n="25.5">l</w>’<w n="25.6">on</w> <w n="25.7">meure</w> ;</l>
					<l n="26" num="5.2"><space unit="char" quantity="4"></space><w n="26.1">Quand</w> <w n="26.2">sonnera</w> <w n="26.3">mon</w> <w n="26.4">heure</w>,</l>
					<l n="27" num="5.3"><space unit="char" quantity="4"></space><w n="27.1">Que</w> <w n="27.2">le</w> <w n="27.3">trépas</w> <w n="27.4">cruel</w>,</l>
					<l n="28" num="5.4"><space unit="char" quantity="4"></space><w n="28.1">Ici</w>, <w n="28.2">viendra</w> <w n="28.3">me</w> <w n="28.4">prendre</w>,</l>
					<l n="29" num="5.5"><space unit="char" quantity="4"></space><w n="29.1">Encor</w> <w n="29.2">veux</w> <w n="29.3">faire</w> <w n="29.4">entendre</w>,</l>
					<l n="30" num="5.6"><w n="30.1">Chanson</w> <w n="30.2">d</w>’<w n="30.3">amour</w> <w n="30.4">du</w> <w n="30.5">Ménestrel</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><space unit="char" quantity="4"></space><w n="31.1">Et</w> <w n="31.2">sur</w> <w n="31.3">la</w> <w n="31.4">tombe</w> <w n="31.5">froide</w>,</l>
					<l n="32" num="6.2"><space unit="char" quantity="4"></space><w n="32.1">Où</w> <w n="32.2">je</w> <w n="32.3">dormirai</w> <w n="32.4">roide</w>,</l>
					<l n="33" num="6.3"><space unit="char" quantity="4"></space><w n="33.1">Du</w> <w n="33.2">sommeil</w> <w n="33.3">éternel</w>,</l>
					<l n="34" num="6.4"><space unit="char" quantity="4"></space><w n="34.1">Que</w> <w n="34.2">le</w> <w n="34.3">rossignol</w> <w n="34.4">chante</w>,</l>
					<l n="35" num="6.5"><space unit="char" quantity="4"></space><w n="35.1">Lai</w>, <w n="35.2">complainte</w>, <w n="35.3">sirvente</w> ;</l>
					<l n="36" num="6.6"><w n="36.1">Chanson</w> <w n="36.2">d</w>’<w n="36.3">amour</w> <w n="36.4">du</w> <w n="36.5">Ménestrel</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1847">1847</date>
					</dateline>
				</closer>
			</div></body></text></TEI>