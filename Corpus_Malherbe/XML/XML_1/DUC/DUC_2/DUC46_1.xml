<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">NI JAMAIS NI TOUJOURS</head><div type="drama" key="DUC46">
					<head type="main">Ni Jamais ni Toujours !</head>
					<div type="body">
						<castList>
							<castItem>
								<role>CLÉON</role>
								<roleDesc>berger</roleDesc>
							</castItem>
							<castItem>
								<role>MYRTIL</role>
								<roleDesc>berger</roleDesc>
							</castItem>
							<castItem>
								<role>IANTHÉ</role>
								<roleDesc>bergère</roleDesc>
							</castItem>
							<castItem>
								<role>THIMAS</role>
								<roleDesc>bergère</roleDesc>
							</castItem>
							<castItem>
								<role>LYCENION</role>
								<roleDesc>mariée</roleDesc>
							</castItem>
						</castList>
						<set>Un site pittoresque.</set>
						<div type="scene" n="1">
							<sp n="1">
								<speaker>CLÉON à Ianthé</speaker>
								<l n="1"><w n="1.1">Veux</w>-<w n="1.2">tu</w>, <w n="1.3">belle</w> <w n="1.4">Ianthé</w>, <w n="1.5">qu</w>’<w n="1.6">avec</w> <w n="1.7">toi</w> <w n="1.8">dans</w> <w n="1.9">la</w> <w n="1.10">plaine</w></l>
								<l n="2"><w n="2.1">Je</w> <w n="2.2">guide</w> <w n="2.3">mes</w> <w n="2.4">troupeaux</w> <w n="2.5">à</w> <w n="2.6">l</w>’<w n="2.7">abondante</w> <w n="2.8">laine</w> ?</l>
							</sp>
							<sp n="2">
								<speaker>MYRTIL à Thimas</speaker>
								<l n="3"><w n="3.1">Veux</w>-<w n="3.2">tu</w>, <w n="3.3">jeune</w> <w n="3.4">Thimas</w>, <w n="3.5">mener</w> <w n="3.6">dans</w> <w n="3.7">les</w> <w n="3.8">halliers</w></l>
								<l n="4"><w n="4.1">Tes</w> <w n="4.2">timides</w> <w n="4.3">agneaux</w> <w n="4.4">avec</w> <w n="4.5">mes</w> <w n="4.6">grands</w> <w n="4.7">béliers</w> ?</l>
							</sp>
							<sp n="3">
								<speaker>CLÉON à Ianthé</speaker>
								<l n="5"><w n="5.1">Je</w> <w n="5.2">ferai</w> <w n="5.3">des</w> <w n="5.4">colliers</w> <w n="5.5">d</w>’<w n="5.6">osier</w> <w n="5.7">franc</w> <w n="5.8">pour</w> <w n="5.9">tes</w> <w n="5.10">chèvres</w>.</l>
							</sp>
							<sp n="4">
								<speaker>MYRTIL à Thimas</speaker>
								<l n="6"><w n="6.1">Je</w> <w n="6.2">sais</w> <w n="6.3">une</w> <w n="6.4">chanson</w> <w n="6.5">faite</w> <w n="6.6">exprès</w> <w n="6.7">pour</w> <w n="6.8">tes</w> <w n="6.9">lèvres</w>.</l>
							</sp>
							<sp n="5">
								<speaker>IANTHÉ à Cléon</speaker>
								<l n="7"><w n="7.1">Mes</w> <w n="7.2">chèvres</w> <w n="7.3">ont</w> <w n="7.4">déjà</w> <w n="7.5">des</w> <w n="7.6">colliers</w>, <w n="7.7">ô</w> <w n="7.8">Cléon</w> !</l>
							</sp>
							<sp n="6">
								<speaker>THIMAS à Myrtil</speaker>
								<l n="8"><w n="8.1">Mes</w> <w n="8.2">lèvres</w> <w n="8.3">ont</w> <w n="8.4">déjà</w> <w n="8.5">murmuré</w> <w n="8.6">ta</w> <w n="8.7">chanson</w> !</l>
							</sp>
							<sp n="7">
								<speaker>CLÉON à Ianthé</speaker>
								<l part="I" n="9"><w n="9.1">Seule</w>, <w n="9.2">n</w>’<w n="9.3">as</w>-<w n="9.4">tu</w> <w n="9.5">point</w> <w n="9.6">peur</w> ? </l>
							</sp>
							<sp n="8">
								<speaker>IANTHÉ</speaker>
								<l part="F" n="9"><w n="9.7">Non</w>, <w n="9.8">mon</w> <w n="9.9">chien</w> <w n="9.10">m</w>’<w n="9.11">accompagne</w>.</l>
							</sp>
							<sp n="9">
								<speaker>MYRTIL à Thimas</speaker>
								<l n="10"><w n="10.1">Et</w> <w n="10.2">toi</w>, <w n="10.3">Thimas</w> ? <w n="10.4">on</w> <w n="10.5">dit</w> <w n="10.6">que</w> <w n="10.7">Pan</w> <w n="10.8">dans</w> <w n="10.9">la</w> <w n="10.10">campagne</w></l>
								<l n="11"><w n="11.1">Suit</w> <w n="11.2">la</w> <w n="11.3">bergère</w> <w n="11.4">seule</w> <w n="11.5">à</w> <w n="11.6">travers</w> <w n="11.7">les</w> <w n="11.8">roseaux</w> ?</l>
							</sp>
							<sp n="10">
								<speaker>THIMAS</speaker>
								<l n="12"><w n="12.1">Tant</w> <w n="12.2">mieux</w> ! — <w n="12.3">J</w>’<w n="12.4">écouterai</w> <w n="12.5">sa</w> <w n="12.6">flûte</w> <w n="12.7">aux</w> <w n="12.8">sept</w> <w n="12.9">tuyaux</w>.</l>
							</sp>
							<sp n="11">
								<speaker>CLÉON à Ianthé</speaker>
								<l part="I" n="13"><w n="13.1">O</w> <w n="13.2">méchante</w> ! </l>
							</sp>
							<sp n="12">
								<speaker>MYRTIL à Thimas</speaker>
								<l part="M" n="13"><w n="13.3">O</w> <w n="13.4">cruelle</w> ! </l>
							</sp>
							<sp n="13">
								<speaker>LYCENION, <hi rend="ital">arrivant</hi></speaker>
								<l part="F" n="13"><w n="13.5">Eh</w> ! <w n="13.6">bien</w>, <w n="13.7">que</w> <w n="13.8">signifie</w>…</l>
								<l part="I" n="14"><w n="14.1">Et</w> <w n="14.2">pourquoi</w> <w n="14.3">ces</w> <w n="14.4">soupirs</w> ? </l>
							</sp>
							<sp n="14">
								<speaker>MYRTIL</speaker>
								<l part="F" n="14"><w n="14.5">Thimas</w> <w n="14.6">me</w> <w n="14.7">sacrifie</w></l>
								<l n="15"><w n="15.1">A</w> <w n="15.2">quelque</w> <w n="15.3">autre</w> <w n="15.4">berger</w> <w n="15.5">plus</w> <w n="15.6">fortuné</w> <w n="15.7">que</w> <w n="15.8">moi</w> !</l>
							</sp>
							<sp n="15">
								<speaker>CLÉON</speaker>
								<l n="16"><w n="16.1">L</w>’<w n="16.2">insensible</w> <w n="16.3">Ianthé</w> <w n="16.4">se</w> <w n="16.5">moque</w> <w n="16.6">de</w> <w n="16.7">ma</w> <w n="16.8">foi</w> !</l>
							</sp>
							<sp n="16">
								<speaker>LYCENION aux deux bergers</speaker>
								<l n="17"><w n="17.1">N</w>’<w n="17.2">est</w>-<w n="17.3">il</w> <w n="17.4">plus</w> <w n="17.5">en</w> <w n="17.6">ces</w> <w n="17.7">lieux</w> <w n="17.8">d</w>’<w n="17.9">autres</w> <w n="17.10">bergères</w> ? — <w n="17.11">Certes</w>,</l>
								<l n="18"><w n="18.1">Lorsque</w> <w n="18.2">Helios</w> <w n="18.3">nous</w> <w n="18.4">fait</w>, <w n="18.5">sous</w> <w n="18.6">les</w> <w n="18.7">ramures</w> <w n="18.8">vertes</w>,</l>
								<l n="19"><w n="19.1">Chercher</w> <w n="19.2">une</w> <w n="19.3">retraite</w>, <w n="19.4">on</w> <w n="19.5">entend</w> <w n="19.6">des</w> <w n="19.7">chansons</w>,</l>
								<l n="20"><w n="20.1">Des</w> <w n="20.2">rires</w> <w n="20.3">frais</w> <w n="20.4">et</w> <w n="20.5">purs</w> <w n="20.6">derrière</w> <w n="20.7">les</w> <w n="20.8">buissons</w>,</l>
								<l n="21"><w n="21.1">Dont</w> <w n="21.2">chacun</w> <w n="21.3">pourrait</w> <w n="21.4">dire</w> <w n="21.5">une</w> <w n="21.6">amoureuse</w> <w n="21.7">histoire</w> !</l>
								<l n="22"><w n="22.1">Et</w>, <w n="22.2">le</w> <w n="22.3">soir</w>, <w n="22.4">près</w> <w n="22.5">des</w> <w n="22.6">puits</w> <w n="22.7">où</w> <w n="22.8">les</w> <w n="22.9">troupeaux</w> <w n="22.10">vont</w> <w n="22.11">boire</w>,</l>
								<l n="23"><w n="23.1">On</w> <w n="23.2">entend</w> <w n="23.3">rire</w> <w n="23.4">encor</w>, <w n="23.5">quand</w> <w n="23.6">Diane</w>, <w n="23.7">ô</w> <w n="23.8">bergers</w> !</l>
								<l n="24"><w n="24.1">De</w> <w n="24.2">son</w> <w n="24.3">rayon</w> <w n="24.4">limpide</w> <w n="24.5">argenté</w> <w n="24.6">les</w> <w n="24.7">vergers</w>.</l>
								<l n="25"><w n="25.1">Et</w> <w n="25.2">c</w>’<w n="25.3">est</w> <w n="25.4">l</w>’<w n="25.5">essaim</w> <w n="25.6">joyeux</w> <w n="25.7">des</w> <w n="25.8">belles</w> <w n="25.9">jeunes</w> <w n="25.10">filles</w>,</l>
								<l n="26"><w n="26.1">Causant</w> <w n="26.2">tout</w> <w n="26.3">bas</w> <w n="26.4">d</w>’<w n="26.5">amour</w> <w n="26.6">loin</w> <w n="26.7">du</w> <w n="26.8">seuil</w> <w n="26.9">des</w> <w n="26.10">familles</w>,</l>
								<l n="27"><w n="27.1">Qui</w> <w n="27.2">rit</w> <w n="27.3">et</w> <w n="27.4">chante</w> <w n="27.5">ainsi</w>, <w n="27.6">groupe</w> <w n="27.7">aimable</w> <w n="27.8">et</w> <w n="27.9">charmant</w>,</l>
								<l n="28"><w n="28.1">Où</w> <w n="28.2">la</w> <w n="28.3">vierge</w> <w n="28.4">pour</w> <w n="28.5">être</w> <w n="28.6">amante</w> <w n="28.7">attend</w> <w n="28.8">l</w>’<w n="28.9">amant</w> !</l>
								<l n="29"><w n="29.1">Et</w> <w n="29.2">vous</w> <w n="29.3">désespérez</w> ? <w n="29.4">et</w> <w n="29.5">votre</w> <w n="29.6">cœur</w> <w n="29.7">soupire</w> ?…</l>
							</sp>
							<sp n="17">
								<speaker>CLÉON</speaker>
								<l part="I" n="30"><w n="30.1">Je</w> <w n="30.2">vis</w> <w n="30.3">pour</w> <w n="30.4">Ianthé</w> ! </l>
							</sp>
							<sp n="18">
								<speaker>MYRTIL</speaker>
								<l part="F" n="30"><w n="30.5">Pour</w> <w n="30.6">Thimas</w> <w n="30.7">je</w> <w n="30.8">respire</w> !</l>
							</sp>
							<sp n="19">
								<speaker>LYCENION</speaker>
								<l n="31"><w n="31.1">Ne</w> <w n="31.2">vous</w> <w n="31.3">plaignez</w> <w n="31.4">donc</w> <w n="31.5">pas</w> <w n="31.6">puisque</w> <w n="31.7">votre</w> <w n="31.8">tourment</w></l>
								<l n="32"><w n="32.1">Fait</w> <w n="32.2">aussi</w> <w n="32.3">votre</w> <w n="32.4">joie</w>. — <w n="32.5">Et</w> <w n="32.6">c</w>’<w n="32.7">est</w> <w n="32.8">heureux</w>, <w n="32.9">vraiment</w>,</l>
								<l n="33"><w n="33.1">Que</w> <w n="33.2">les</w> <w n="33.3">dieux</w> <w n="33.4">aient</w> <w n="33.5">placé</w> <w n="33.6">dans</w> <w n="33.7">cette</w> <w n="33.8">douleur</w> <w n="33.9">même</w>,</l>
								<l n="34"><w n="34.1">Dans</w> <w n="34.2">la</w> <w n="34.3">douleur</w> <w n="34.4">d</w>’<w n="34.5">aimer</w>, <w n="34.6">une</w> <w n="34.7">ivresse</w> <w n="34.8">suprême</w> !</l>
								<l n="35"><w n="35.1">Car</w> <w n="35.2">l</w>’<w n="35.3">amour</w> <w n="35.4">satisfait</w> <w n="35.5">est</w> <w n="35.6">bien</w> <w n="35.7">près</w> <w n="35.8">de</w> <w n="35.9">l</w>’<w n="35.10">oubli</w> ;</l>
								<l n="36"><w n="36.1">Il</w> <w n="36.2">survit</w> <w n="36.3">rarement</w> <w n="36.4">au</w> <w n="36.5">désir</w> <w n="36.6">accompli</w> !</l>
								<stage>(Avec un soupir :)</stage>
								<l part="I" n="37"> <w n="37.1">Je</w> <w n="37.2">le</w> <w n="37.3">sais</w>, <w n="37.4">moi</w> ! </l>
							</sp>
							<sp n="20">
								<speaker>CLÉON et MYRTIL</speaker>
								<l part="M" n="37"><w n="37.5">Comment</w> ? </l>
							</sp>
							<sp n="21">
								<speaker>IANTHÉ et THIMAS</speaker>
								<l part="F" n="37"><w n="37.6">Lycenion</w>, <w n="37.7">achève</w>.</l>
							</sp>
							<sp n="22">
								<speaker>LYCENION</speaker>
								<l n="38"><w n="38.1">Après</w> <w n="38.2">trois</w> <w n="38.3">mois</w> <w n="38.4">d</w>’<w n="38.5">hymen</w> ; <w n="38.6">envolés</w> <w n="38.7">comme</w> <w n="38.8">un</w> <w n="38.9">rêve</w> !</l>
								<l n="39"><w n="39.1">Seule</w>, <w n="39.2">un</w> <w n="39.3">soir</w>, <w n="39.4">au</w> <w n="39.5">logis</w>, <w n="39.6">j</w>’<w n="39.7">attendais</w> <w n="39.8">mon</w> <w n="39.9">époux</w>.</l>
								<l n="40"><w n="40.1">Mille</w> <w n="40.2">parfums</w> <w n="40.3">montaient</w> <w n="40.4">dans</w> <w n="40.5">l</w>’<w n="40.6">air</w> <w n="40.7">tiède</w> <w n="40.8">et</w> <w n="40.9">doux</w>,</l>
								<l n="41"><w n="41.1">Et</w> <w n="41.2">le</w> <w n="41.3">soleil</w> <w n="41.4">couchant</w>, <w n="41.5">dans</w> <w n="41.6">ses</w> <w n="41.7">métamorphoses</w>,</l>
								<l n="42"><w n="42.1">Sur</w> <w n="42.2">les</w> <w n="42.3">grands</w> <w n="42.4">arbres</w> <w n="42.5">verts</w> <w n="42.6">jetait</w> <w n="42.7">des</w> <w n="42.8">teintes</w> <w n="42.9">roses</w>.</l>
								<l n="43"><w n="43.1">Les</w> <w n="43.2">oiseaux</w> <w n="43.3">des</w> <w n="43.4">bosquets</w> <w n="43.5">chantaient</w> <w n="43.6">à</w> <w n="43.7">qui</w> <w n="43.8">mieux</w> <w n="43.9">mieux</w>.</l>
								<l n="44"><w n="44.1">Puis</w> <w n="44.2">les</w> <w n="44.3">sources</w> <w n="44.4">avaient</w> <w n="44.5">des</w> <w n="44.6">murmures</w> <w n="44.7">joyeux</w>.</l>
								<l n="45"><w n="45.1">J</w>’<w n="45.2">écoutais</w> <w n="45.3">ces</w> <w n="45.4">doux</w> <w n="45.5">bruits</w>, <w n="45.6">j</w>’<w n="45.7">aspirais</w> <w n="45.8">cette</w> <w n="45.9">haleine</w></l>
								<l n="46"><w n="46.1">Qui</w> <w n="46.2">me</w> <w n="46.3">venaient</w> <w n="46.4">des</w> <w n="46.5">bois</w>, <w n="46.6">des</w> <w n="46.7">monts</w> <w n="46.8">et</w> <w n="46.9">de</w> <w n="46.10">la</w> <w n="46.11">plaine</w>,</l>
								<l n="47"><w n="47.1">Et</w> <w n="47.2">je</w> <w n="47.3">ne</w> <w n="47.4">sais</w> <w n="47.5">alors</w> <w n="47.6">quelle</w> <w n="47.7">étrange</w> <w n="47.8">langueur</w>,</l>
								<l n="48"><w n="48.1">Pour</w> <w n="48.2">enivrer</w> <w n="48.3">mes</w> <w n="48.4">sens</w> <w n="48.5">vint</w> <w n="48.6">captiver</w> <w n="48.7">mon</w> <w n="48.8">cœur</w> !…</l>
								<l n="49"><w n="49.1">C</w>’<w n="49.2">était</w> <w n="49.3">comme</w> <w n="49.4">une</w> <w n="49.5">extase</w> ! — <w n="49.6">Oh</w> ! <w n="49.7">la</w> <w n="49.8">belle</w> <w n="49.9">soirée</w> !</l>
								<l n="50"><w n="50.1">Les</w> <w n="50.2">étoiles</w> <w n="50.3">déjà</w> <w n="50.4">sur</w> <w n="50.5">la</w> <w n="50.6">voûte</w> <w n="50.7">azurée</w></l>
								<l n="51"><w n="51.1">S</w>’<w n="51.2">allumaient</w> ; — <w n="51.3">diamants</w> <w n="51.4">dont</w> <w n="51.5">la</w> <w n="51.6">blanche</w> <w n="51.7">lueur</w>,</l>
								<l n="52"><w n="52.1">Éclaire</w> <w n="52.2">dans</w> <w n="52.3">la</w> <w n="52.4">nuit</w> <w n="52.5">les</w> <w n="52.6">pas</w> <w n="52.7">du</w> <w n="52.8">voyageur</w>…</l>
								<l part="I" n="53"><w n="53.1">Mon</w> <w n="53.2">être</w> <w n="53.3">frissonnait</w> ! </l>
							</sp>
							<sp n="23">
								<speaker>LES AUTRES PERSONNAGES, <hi rend="ital">curieusement</hi></speaker>
								<l part="M" n="53"><w n="53.4">Et</w> <w n="53.5">pourquoi</w> ? </l>
							</sp>
							<sp n="24">
								<speaker>LYCENION, <hi rend="ital">baissant les yeux</hi></speaker>
								<l part="F" n="53"><w n="53.6">Je</w> <w n="53.7">l</w>’<w n="53.8">ignore</w> !</l>
								<stage>(Reprenant son récit avec plus de force)</stage>
								<l n="54"><w n="54.1">J</w>’<w n="54.2">appelais</w> <w n="54.3">mon</w> <w n="54.4">époux</w>, <w n="54.5">je</w> <w n="54.6">l</w>’<w n="54.7">appelais</w> <w n="54.8">encore</w> !</l>
								<l n="55"><w n="55.1">Vingt</w> <w n="55.2">fois</w> <w n="55.3">j</w>’<w n="55.4">interrogeais</w>, <w n="55.5">du</w> <w n="55.6">seuil</w> <w n="55.7">de</w> <w n="55.8">la</w> <w n="55.9">maison</w>,</l>
								<l n="56"><w n="56.1">Et</w> <w n="56.2">le</w> <w n="56.3">sentier</w> <w n="56.4">désert</w> <w n="56.5">et</w> <w n="56.6">jusqu</w>’<w n="56.7">à</w> <w n="56.8">l</w>’<w n="56.9">horizon</w>.</l>
								<l n="57"><w n="57.1">Mon</w> <w n="57.2">regard</w> <w n="57.3">épiait</w> <w n="57.4">les</w> <w n="57.5">ombres</w> <w n="57.6">fugitives</w>,</l>
								<l n="58"><w n="58.1">Que</w> <w n="58.2">la</w> <w n="58.3">lune</w> <w n="58.4">en</w> <w n="58.5">courant</w> <w n="58.6">fait</w> <w n="58.7">glisser</w> <w n="58.8">sur</w> <w n="58.9">les</w> <w n="58.10">rives</w> ;</l>
								<l n="59"><w n="59.1">Mais</w> <w n="59.2">il</w> <w n="59.3">n</w>’<w n="59.4">aperçut</w> <w n="59.5">rien</w> !… <w n="59.6">Si</w> <w n="59.7">ce</w> <w n="59.8">n</w>’<w n="59.9">est</w> <w n="59.10">au</w> <w n="59.11">repos</w>,</l>
								<l n="60"><w n="60.1">Un</w> <w n="60.2">berger</w> <w n="60.3">indolent</w> <w n="60.4">qui</w> <w n="60.5">sifflait</w> <w n="60.6">ses</w> <w n="60.7">troupeaux</w>.</l>
								<l n="61"><w n="61.1">Et</w> <w n="61.2">j</w>’<w n="61.3">appelais</w> <w n="61.4">toujours</w>, <w n="61.5">mon</w> <w n="61.6">époux</w>, <w n="61.7">mon</w> <w n="61.8">doux</w> <w n="61.9">maître</w>…</l>
								<l n="62"><w n="62.1">Trois</w> <w n="62.2">ans</w> <w n="62.3">sont</w> <w n="62.4">écoulés</w> ; <w n="62.5">j</w>’<w n="62.6">attends</w> <choice reason="analysis" type="false_verse" hand="RR"><sic>encore</sic><corr source="édition_1885"><w n="62.7">encor</w></corr></choice> <w n="62.8">le</w> <w n="62.9">traître</w> !</l>
							</sp>
							<sp n="25">
								<speaker>CLÉON</speaker>
								<l part="I" n="63"><w n="63.1">Traître</w> ? <w n="63.2">Et</w> <w n="63.3">s</w>’<w n="63.4">il</w> <w n="63.5">était</w> <w n="63.6">mort</w> ? </l>
							</sp>
							<sp n="26">
								<speaker>LYCENION</speaker>
								<l part="F" n="63"><w n="63.7">Non</w> <w n="63.8">pas</w> ! <w n="63.9">j</w>’<w n="63.10">ai</w> <w n="63.11">su</w> <w n="63.12">depuis</w></l>
								<l n="64"><w n="64.1">Qu</w>’<w n="64.2">il</w> <w n="64.3">est</w> <w n="64.4">vivant</w>, <w n="64.5">Cléon</w>, <w n="64.6">et</w> <w n="64.7">bon</w> <w n="64.8">vivant</w> ! — <w n="64.9">Je</w> <w n="64.10">puis</w></l>
								<l n="65"><w n="65.1">L</w>’<w n="65.2">affirmer</w> ; — <w n="65.3">un</w> <w n="65.4">marchand</w> <w n="65.5">de</w> <w n="65.6">Métymne</w>, <w n="65.7">naguère</w>,</l>
								<l n="66"><w n="66.1">M</w>’<w n="66.2">assura</w> <w n="66.3">l</w>’<w n="66.4">avoir</w> <w n="66.5">vu</w>, <w n="66.6">ne</w> <w n="66.7">s</w>’<w n="66.8">inquiétant</w> <w n="66.9">guère</w></l>
								<l n="67"><w n="67.1">De</w> <w n="67.2">sa</w> <w n="67.3">Lycenion</w> <w n="67.4">qu</w>’<w n="67.5">il</w> <w n="67.6">croit</w> <w n="67.7">bien</w> <w n="67.8">morte</w>, <w n="67.9">lui</w> !</l>
								<l n="68"><w n="68.1">Aussi</w>, <w n="68.2">voilà</w> <w n="68.3">pourquoi</w> <w n="68.4">je</w> <w n="68.5">vous</w> <w n="68.6">dis</w> <w n="68.7">aujourd</w>’<w n="68.8">hui</w></l>
								<l n="69"><w n="69.1">Qu</w>’<w n="69.2">il</w> <w n="69.3">n</w>’<w n="69.4">est</w> <w n="69.5">pas</w> <w n="69.6">de</w> <w n="69.7">serment</w>, <w n="69.8">de</w> <w n="69.9">promesse</w> <w n="69.10">éternelle</w>,</l>
								<l n="70"><w n="70.1">Que</w> <w n="70.2">l</w>’<w n="70.3">amour</w> <w n="70.4">satisfait</w> <w n="70.5">s</w>’<w n="70.6">enfuit</w> <w n="70.7">à</w> <w n="70.8">tire</w>-<w n="70.9">d</w>’<w n="70.10">aile</w>,</l>
								<l n="71"><w n="71.1">Papillon</w> <w n="71.2">inconstant</w> <w n="71.3">que</w> <w n="71.4">mène</w> <w n="71.5">le</w> <w n="71.6">désir</w> ;</l>
								<l n="72"><w n="72.1">Que</w> <w n="72.2">le</w> <w n="72.3">plaisir</w> <w n="72.4">fait</w> <w n="72.5">vivre</w> <w n="72.6">et</w> <w n="72.7">qui</w> <w n="72.8">meurt</w> <w n="72.9">du</w> <w n="72.10">plaisir</w> !</l>
								<l n="73"><w n="73.1">Et</w> <w n="73.2">maintenant</w>, <w n="73.3">allez</w>, <w n="73.4">garçons</w> <w n="73.5">et</w> <w n="73.6">jeunes</w> <w n="73.7">filles</w>,</l>
								<l n="74"><w n="74.1">Par</w> <w n="74.2">les</w> <w n="74.3">sentiers</w> <w n="74.4">ombreux</w>, <w n="74.5">sous</w> <w n="74.6">les</w> <w n="74.7">vertes</w> <w n="74.8">charmilles</w>,</l>
								<l n="75"><w n="75.1">Vous</w> <w n="75.2">tenant</w> <w n="75.3">par</w> <w n="75.4">la</w> <w n="75.5">main</w>, <w n="75.6">riches</w> <w n="75.7">de</w> <w n="75.8">vos</w> <w n="75.9">vingt</w> <w n="75.10">ans</w>,</l>
								<l n="76"><w n="76.1">Ensemble</w> <w n="76.2">allez</w>… <w n="76.3">cueillir</w> <w n="76.4">les</w> <w n="76.5">roses</w> <w n="76.6">du</w> <w n="76.7">printemps</w>.</l>
							</sp>
							<sp n="27">
								<speaker>CLÉON et MYRTIL</speaker>
								<l part="I" n="77"><w n="77.1">Nous</w> <w n="77.2">aimerons</w> <w n="77.3">toujours</w> ! </l>
							</sp>
							<sp n="28">
								<speaker>IANTHÉ et THIMAS</speaker>
								<l part="M" n="77"><w n="77.4">Nous</w>, <w n="77.5">jamais</w> ! </l>
							</sp>
							<sp n="29">
								<speaker>LYCENION</speaker>
								<l part="F" n="77"><w n="77.6">Têtes</w> <w n="77.7">folles</w> !</l>
								<l n="78"><w n="78.1">L</w>’<w n="78.2">amour</w> <w n="78.3">se</w> <w n="78.4">rit</w> <w n="78.5">tout</w> <w n="78.6">bas</w> <w n="78.7">de</w> <w n="78.8">ces</w> <w n="78.9">vaines</w> <w n="78.10">paroles</w>.</l>
								<l n="79"><w n="79.1">Pour</w> <w n="79.2">le</w> <w n="79.3">rendre</w> <w n="79.4">propice</w> <w n="79.5">à</w> <w n="79.6">vos</w> <w n="79.7">cœurs</w>, <w n="79.8">désormais</w>,</l>
								<stage>(Aux bergers)</stage>
								<l part="I" n="80"><w n="80.1">Ne</w> <w n="80.2">dites</w> <w n="80.3">point</w> : <w n="80.4">Toujours</w> ! </l>
								<stage>(Aux bergères)</stage>
								<l part="F" n="80"><w n="80.5">Ne</w> <w n="80.6">dites</w> <w n="80.7">point</w> : <w n="80.8">Jamais</w> !</l>
							</sp>
						</div>
						<closer>
							<dateline>
								<date when="1865">1865</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>