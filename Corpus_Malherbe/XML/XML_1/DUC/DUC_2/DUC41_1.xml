<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC41">
				<head type="main">Nita</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ses</w> <w n="1.2">cheveux</w> <w n="1.3">étaient</w> <w n="1.4">blonds</w>, <w n="1.5">ses</w> <w n="1.6">lèvres</w> <w n="1.7">étaient</w> <w n="1.8">roses</w>,</l>
					<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">innocence</w> <w n="2.3">brillait</w> <w n="2.4">dans</w> <w n="2.5">ses</w> <w n="2.6">grands</w> <w n="2.7">yeux</w> <w n="2.8">d</w>’<w n="2.9">azur</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Son</w> <w n="3.2">corps</w> <w n="3.3">souple</w> <w n="3.4">et</w> <w n="3.5">mignon</w> <w n="3.6">avait</w> <w n="3.7">de</w> <w n="3.8">chastes</w> <w n="3.9">poses</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">dans</w> <w n="4.3">les</w> <w n="4.4">tableaux</w> <w n="4.5">les</w> <w n="4.6">vierges</w> <w n="4.7">au</w> <w n="4.8">front</w> <w n="4.9">pur</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">sa</w> <w n="5.3">voix</w> <w n="5.4">était</w> <w n="5.5">douce</w> ! <w n="5.6">et</w> <w n="5.7">tremblant</w> <w n="5.8">devant</w> <w n="5.9">elle</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">voulus</w> <w n="6.3">lui</w> <w n="6.4">parler</w>, <w n="6.5">murmurer</w> <w n="6.6">un</w> <w n="6.7">aveu</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">je</w> <w n="7.3">craignis</w> <w n="7.4">que</w> <w n="7.5">l</w>’<w n="7.6">ange</w> <w n="7.7">en</w> <w n="7.8">déployant</w> <w n="7.9">son</w> <w n="7.10">aile</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Comme</w> <w n="8.2">une</w> <w n="8.3">vision</w> <w n="8.4">ne</w> <w n="8.5">remontât</w> <w n="8.6">vers</w> <w n="8.7">Dieu</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">pourtant</w>, <w n="9.3">je</w> <w n="9.4">lui</w> <w n="9.5">dis</w> : — « <w n="9.6">Je</w> <w n="9.7">vous</w> <w n="9.8">aime</w>, <w n="9.9">ô</w> <w n="9.10">doux</w> <w n="9.11">ange</w> !</l>
					<l n="10" num="3.2"><w n="10.1">O</w> <w n="10.2">doux</w> <w n="10.3">ange</w> <w n="10.4">du</w> <w n="10.5">ciel</w>, <w n="10.6">je</w> <w n="10.7">vous</w> <w n="10.8">aime</w> ! — <w n="10.9">Croyez</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Croyez</w> <w n="11.2">à</w> <w n="11.3">mon</w> <w n="11.4">amour</w> <w n="11.5">qui</w> <w n="11.6">ne</w> <w n="11.7">veut</w> <w n="11.8">en</w> <w n="11.9">échange</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Comme</w> <w n="12.2">un</w> <w n="12.3">pieux</w> <w n="12.4">encens</w> <w n="12.5">que</w> <w n="12.6">brûler</w> <w n="12.7">à</w> <w n="12.8">vos</w> <w n="12.9">pieds</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Que</w> <w n="13.2">votre</w> <w n="13.3">doux</w> <w n="13.4">regard</w> <w n="13.5">tombe</w> <w n="13.6">sur</w> <w n="13.7">le</w> <w n="13.8">poète</w> !</l>
					<l n="14" num="4.2"><w n="14.1">Il</w> <w n="14.2">dira</w> <w n="14.3">vos</w> <w n="14.4">vertus</w> <w n="14.5">dans</w> <w n="14.6">un</w> <w n="14.7">hymne</w> <w n="14.8">sans</w> <w n="14.9">fin</w> ! »</l>
					<l n="15" num="4.3"><w n="15.1">Elle</w> <w n="15.2">me</w> <w n="15.3">répondit</w> : — « <w n="15.4">Ah</w> ! <w n="15.5">mon</w> <w n="15.6">cher</w>, <w n="15.7">que</w> <w n="15.8">t</w>’<w n="15.9">es</w> <w n="15.10">bête</w> !</l>
					<l n="16" num="4.4"><w n="16.1">Allons</w> <w n="16.2">plutôt</w> <w n="16.3">souper</w>, <w n="16.4">car</w> <w n="16.5">je</w> <w n="16.6">crève</w> <w n="16.7">de</w> <w n="16.8">faim</w> ! »</l>
				</lg>
				<closer>
					<dateline>
						<date when="1862">1862</date>
					</dateline>
				</closer>
			</div></body></text></TEI>