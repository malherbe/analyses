<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Caresses d’Antan</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2199 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Caresses d’Antan</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54577888.r=alexandre%20ducros?rk=236052;4</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Caresses d’Antan</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALEXANDRE GAUTHERIN, ÉDITEUR</publisher>
									<date when="1896">1847 ‒ 1896</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>POÉSIES NOUVELLES</title>
						<title>1852 ‒ 1885</title>
						<author>Alexandre Ducros</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>E. DENTU, ÉDITEUR DE LA SOCIÉTÉ DES GENS DE LETTRES</publisher>
							<date when="1885">1885</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La partie introductive du recueil n’est pas reprise dans cette édition.</p>
				<p>Le texte liminaire de la partie "La Légende du vers à soie" n’a pas été reprise.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits introduits automatiquement ont été supprimés afin d’être conforme à l’édition de référence</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratin</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-26" who="RR">Une correction à partir de la version imprimée de 1885</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA LÉGENDE DE LA POMME</head><div type="poem" key="DUC56">
					<head type="main">La Légende de la pomme<ref type="noteAnchor" target="1">(1)</ref></head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ève</w> <w n="1.2">perdit</w> <w n="1.3">le</w> <w n="1.4">premier</w> <w n="1.5">homme</w>.</l>
						<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2">est</w> <w n="2.3">la</w> <hi rend="ital"><w n="2.4">Genèse</w></hi> <w n="2.5">qui</w> <w n="2.6">le</w> <w n="2.7">dit</w>,</l>
						<l n="3" num="1.3"><w n="3.1">A</w> <w n="3.2">propos</w> <w n="3.3">de</w> <w n="3.4">certaine</w> <w n="3.5">pomme</w></l>
						<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">laquelle</w> <w n="4.3">l</w>’<w n="4.4">homme</w> <w n="4.5">mordit</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">aventure</w> <w n="5.3">est</w>-<w n="5.4">elle</w> <w n="5.5">notoire</w> ?</l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">devons</w>-<w n="6.3">nous</w>, <w n="6.4">sans</w> <w n="6.5">examen</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Donner</w> <w n="7.2">créance</w> <w n="7.3">à</w> <w n="7.4">cette</w> <w n="7.5">histoire</w></l>
						<l n="8" num="2.4"><w n="8.1">D</w>’<w n="8.2">où</w> <w n="8.3">date</w> <w n="8.4">le</w> <w n="8.5">premier</w> <w n="8.6">hymen</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Sans</w> <w n="9.2">être</w> <w n="9.3">taxé</w> <w n="9.4">d</w>’<w n="9.5">hérésie</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Je</w> <w n="10.2">dois</w> <w n="10.3">en</w> <w n="10.4">faire</w> <w n="10.5">ici</w> <w n="10.6">l</w>’<w n="10.7">aveu</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Malgré</w> <w n="11.2">toute</w> <w n="11.3">sa</w> <w n="11.4">poésie</w>,</l>
						<l n="12" num="3.4"><w n="12.1">La</w> <hi rend="ital"><w n="12.2">Genèse</w></hi> <w n="12.3">s</w>’<w n="12.4">égare</w> <w n="12.5">un</w> <w n="12.6">peu</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Non</w>, <w n="13.2">la</w> <w n="13.3">femme</w>, <w n="13.4">l</w>’<w n="13.5">épouse</w> <w n="13.6">blonde</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Qu</w>’<w n="14.2">Adam</w> <w n="14.3">trouvait</w> <w n="14.4">à</w> <w n="14.5">son</w> <w n="14.6">réveil</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Surprise</w>, <w n="15.2">se</w> <w n="15.3">mirait</w> <w n="15.4">dans</w> <w n="15.5">l</w>’<w n="15.6">onde</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Aux</w> <w n="16.2">premiers</w> <w n="16.3">rayons</w> <w n="16.4">du</w> <w n="16.5">soleil</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Voyant</w> <w n="17.2">le</w> <w n="17.3">trésor</w> <w n="17.4">adorable</w></l>
						<l n="18" num="5.2"><w n="18.1">De</w> <w n="18.2">sa</w> <w n="18.3">virginale</w> <w n="18.4">beauté</w>,</l>
						<l n="19" num="5.3"><w n="19.1">De</w> <w n="19.2">l</w>’<w n="19.3">homme</w>, <w n="19.4">son</w> <w n="19.5">inséparable</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Elle</w> <w n="20.2">plaignait</w> <w n="20.3">l</w>’<w n="20.4">oisiveté</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Car</w> <w n="21.2">à</w> <w n="21.3">l</w>’<w n="21.4">ombre</w> <w n="21.5">des</w> <w n="21.6">vertes</w> <w n="21.7">pousses</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Dont</w> <w n="22.2">le</w> <w n="22.3">jardin</w> <w n="22.4">se</w> <w n="22.5">parfumait</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Tranquille</w>, <w n="23.2">Adam</w> <w n="23.3">tournait</w> <w n="23.4">ses</w> <w n="23.5">pouces</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Ou</w> <w n="24.2">du</w> <w n="24.3">matin</w> <w n="24.4">au</w> <w n="24.5">soir</w> <w n="24.6">dormait</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">— « <w n="25.1">Que</w> <w n="25.2">faire</w>, <w n="25.3">hélas</w> ! <w n="25.4">murmurait</w> <w n="25.5">Ève</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Pour</w> <w n="26.2">l</w>’<w n="26.3">occuper</w>, <w n="26.4">ce</w> <w n="26.5">paresseux</w> ?</l>
						<l n="27" num="7.3"><w n="27.1">Pour</w> <w n="27.2">que</w> <w n="27.3">ce</w> <w n="27.4">long</w> <w n="27.5">sommeil</w> <w n="27.6">s</w>’<w n="27.7">achève</w> ?</l>
						<l n="28" num="7.4"><w n="28.1">Les</w> <w n="28.2">oiseaux</w> <w n="28.3">ne</w> <w n="28.4">dorment</w> <w n="28.5">pas</w>, <w n="28.6">eux</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ensemble</w> <w n="29.2">ils</w> <w n="29.3">vont</w> <w n="29.4">à</w> <w n="29.5">la</w> <w n="29.6">cueillée</w>.</l>
						<l n="30" num="8.2"><w n="30.1">Que</w> <w n="30.2">leur</w> <w n="30.3">chant</w> <w n="30.4">est</w> <w n="30.5">doux</w> <w n="30.6">et</w> <w n="30.7">profond</w> !…</l>
						<l n="31" num="8.3"><w n="31.1">Ils</w> <w n="31.2">se</w> <w n="31.3">cachent</w> <w n="31.4">sous</w> <w n="31.5">la</w> <w n="31.6">feuillée</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">je</w> <w n="32.3">ne</w> <w n="32.4">vois</w> <w n="32.5">plus</w> <w n="32.6">ce</w> <w n="32.7">qu</w>’<w n="32.8">ils</w> <w n="32.9">font</w> ! »</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">L</w>’<w n="33.2">était</w> <w n="33.3">sous</w> <w n="33.4">un</w> <w n="33.5">pommier</w> <w n="33.6">superbe</w></l>
						<l n="34" num="9.2"><w n="34.1">Que</w> <w n="34.2">ces</w> <w n="34.3">longs</w> <w n="34.4">soupirs</w> <w n="34.5">s</w>’<w n="34.6">exhalaient</w>.</l>
						<l n="35" num="9.3"><w n="35.1">Le</w> <w n="35.2">soleil</w> <w n="35.3">ruisselait</w> <w n="35.4">sur</w> <w n="35.5">l</w>’<w n="35.6">herbe</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">les</w> <w n="36.3">pommes</w> <w n="36.4">étincelaient</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Qu</w>’<w n="37.2">elles</w> <w n="37.3">étaient</w> <w n="37.4">appétissantes</w> !</l>
						<l n="38" num="10.2"><w n="38.1">Ève</w> <w n="38.2">voulut</w> <w n="38.3">s</w>’<w n="38.4">en</w> <w n="38.5">approcher</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Dents</w> <w n="39.2">prêtes</w>, <w n="39.3">lèvres</w> <w n="39.4">frémissantes</w>…</l>
						<l n="40" num="10.4"><w n="40.1">Mais</w> <w n="40.2">défense</w> <w n="40.3">était</w> <w n="40.4">d</w>’<w n="40.5">y</w> <w n="40.6">toucher</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Le</w> <w n="41.2">Créateur</w> <w n="41.3">de</w> <w n="41.4">toutes</w> <w n="41.5">choses</w></l>
						<l n="42" num="11.2"><w n="42.1">Leur</w> <w n="42.2">avait</w> <w n="42.3">dit</w> <w n="42.4">au</w> <w n="42.5">jour</w> <w n="42.6">premier</w> :</l>
						<l n="43" num="11.3">— « <w n="43.1">Mangez</w> <w n="43.2">des</w> <w n="43.3">fruits</w>, <w n="43.4">cueillez</w> <w n="43.5">des</w> <w n="43.6">roses</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Mais</w> <w n="44.2">ne</w> <w n="44.3">touchez</w> <w n="44.4">pas</w> <w n="44.5">au</w> <w n="44.6">pommier</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Ma</w> <w n="45.2">Droite</w> <w n="45.3">frappe</w> <w n="45.4">qui</w> <w n="45.5">m</w>’<w n="45.6">offense</w>.</l>
						<l n="46" num="12.2"><w n="46.1">Épargnez</w>-<w n="46.2">vous</w> <w n="46.3">un</w> <w n="46.4">grand</w> <w n="46.5">remord</w> ;</l>
						<l n="47" num="12.3"><w n="47.1">Si</w> <w n="47.2">vous</w> <w n="47.3">enfreigniez</w> <w n="47.4">ma</w> <w n="47.5">défense</w>,</l>
						<l n="48" num="12.4"><w n="48.1">Tous</w> <w n="48.2">les</w> <w n="48.3">deux</w> <w n="48.4">vous</w> <w n="48.5">mourriez</w> <w n="48.6">de</w> <w n="48.7">mort</w> ! »</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Ah</w> ! <w n="49.2">que</w> <w n="49.3">les</w> <w n="49.4">pommes</w> <w n="49.5">semblaient</w> <w n="49.6">douces</w> !…</l>
						<l n="50" num="13.2"><w n="50.1">La</w> <w n="50.2">défense</w> <w n="50.3">en</w> <w n="50.4">doublait</w> <w n="50.5">le</w> <w n="50.6">prix</w>…</l>
						<l n="51" num="13.3"><w n="51.1">Adam</w>, <w n="51.2">ne</w> <w n="51.3">tournant</w> <w n="51.4">plus</w> <w n="51.5">ses</w> <w n="51.6">pouces</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Contemplait</w> <w n="52.2">Ève</w> <w n="52.3">tout</w> <w n="52.4">surpris</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1">— « <w n="53.1">Que</w> <w n="53.2">fais</w>-<w n="53.3">tu</w>, <w n="53.4">là</w> ? » — « <w n="53.5">Moi</w> ? <w n="53.6">je</w> <w n="53.7">regarde</w>. »</l>
						<l n="54" num="14.2">— « <w n="54.1">Et</w> <w n="54.2">que</w> <w n="54.3">regardes</w>-<w n="54.4">tu</w> ? » — « <w n="54.5">Ces</w> <w n="54.6">fruits</w>.</l>
						<l n="55" num="14.3"><w n="55.1">Qu</w>’<w n="55.2">ils</w> <w n="55.3">doivent</w> <w n="55.4">être</w> <w n="55.5">bons</w> ! » — « <w n="55.6">Prends</w> <w n="55.7">garde</w> !</l>
						<l n="56" num="14.4"><w n="56.1">Tu</w> <w n="56.2">sais</w> <w n="56.3">quels</w> <w n="56.4">maux</w> <w n="56.5">nous</w> <w n="56.6">sont</w> <w n="56.7">prédits</w> ?</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1">« <w n="57.1">N</w>’<w n="57.2">y</w> <w n="57.3">touche</w> <w n="57.4">pas</w> ! » — « <w n="57.5">J</w>’<w n="57.6">ai</w> <w n="57.7">soif</w> ! » — « <w n="57.8">La</w> <w n="57.9">source</w></l>
						<l n="58" num="15.2"><w n="58.1">Coule</w> <w n="58.2">sur</w> <w n="58.3">les</w> <w n="58.4">cailloux</w> <w n="58.5">dorés</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Tantôt</w> <w n="59.2">le</w> <w n="59.3">lion</w> <w n="59.4">avec</w> <w n="59.5">l</w>’<w n="59.6">ourse</w>,</l>
						<l n="60" num="15.4"><w n="60.1">Gaîment</w> <w n="60.2">s</w>’<w n="60.3">y</w> <w n="60.4">sont</w> <w n="60.5">désaltérés</w>. »</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1">— « <w n="61.1">Non</w> ! <w n="61.2">j</w>’<w n="61.3">aimerais</w> <w n="61.4">mieux</w> <w n="61.5">une</w> <w n="61.6">pomme</w></l>
						<l n="62" num="16.2"><w n="62.1">Que</w> <w n="62.2">cette</w> <w n="62.3">eau</w> <w n="62.4">baignant</w> <w n="62.5">les</w> <w n="62.6">glaïeuls</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Cueille</w>-<w n="63.2">m</w>’<w n="63.3">en</w> <w n="63.4">une</w>, <w n="63.5">ô</w> <w n="63.6">mon</w> <w n="63.7">cher</w> <w n="63.8">homme</w>,</l>
						<l n="64" num="16.4"><w n="64.1">J</w>’<w n="64.2">ai</w> <w n="64.3">bien</w> <w n="64.4">soif</w>… <w n="64.5">et</w> <w n="64.6">nous</w> <w n="64.7">sommes</w> <w n="64.8">seuls</w> ! »</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Ève</w> <w n="65.2">prit</w> <w n="65.3">le</w> <w n="65.4">fruit</w>, <w n="65.5">et</w> <w n="65.6">ses</w> <w n="65.7">lèvres</w></l>
						<l n="66" num="17.2"><w n="66.1">S</w>’<w n="66.2">ouvrirent</w> <w n="66.3">pour</w> <w n="66.4">croquer</w>, <w n="66.5">et</w>, <w n="66.6">dam</w> !</l>
						<l n="67" num="17.3"><w n="67.1">De</w> <w n="67.2">ses</w> <w n="67.3">gastronomiques</w> <w n="67.4">fièvres</w>,</l>
						<l n="68" num="17.4"><w n="68.1">Elle</w> <w n="68.2">rendit</w> <w n="68.3">complice</w> <w n="68.4">Adam</w> !</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">Il</w> <w n="69.2">mordit</w>, <w n="69.3">bravant</w> <w n="69.4">l</w>’<w n="69.5">anathème</w>,</l>
						<l n="70" num="18.2"><w n="70.1">Sur</w> <w n="70.2">la</w> <w n="70.3">moitié</w> <w n="70.4">qu</w>’<w n="70.5">elle</w> <w n="70.6">laissa</w>…</l>
						<l n="71" num="18.3"><w n="71.1">Ce</w> <w n="71.2">dut</w> <w n="71.3">être</w> <w n="71.4">bon</w> <w n="71.5">tout</w> <w n="71.6">de</w> <w n="71.7">même</w></l>
						<l n="72" num="18.4"><w n="72.1">Car</w> <w n="72.2">la</w> <w n="72.3">pomme</w> <w n="72.4">entière</w> <w n="72.5">y</w> <w n="72.6">passa</w> !</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><w n="73.1">Soudain</w>, <w n="73.2">les</w> <w n="73.3">zéphyrs</w> <w n="73.4">emportèrent</w></l>
						<l n="74" num="19.2"><w n="74.1">Des</w> <w n="74.2">époux</w> <w n="74.3">les</w> <w n="74.4">ennuis</w> <w n="74.5">défunts</w>.</l>
						<l n="75" num="19.3"><w n="75.1">Comme</w> <w n="75.2">l</w>’<w n="75.3">oiseau</w>, <w n="75.4">les</w> <w n="75.5">fleurs</w> <w n="75.6">chantèrent</w></l>
						<l n="76" num="19.4"><w n="76.1">D</w>’<w n="76.2">ardentes</w> <w n="76.3">strophes</w> <w n="76.4">de</w> <w n="76.5">parfums</w></l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1"><w n="77.1">Et</w> <w n="77.2">sortant</w> <w n="77.3">des</w> <w n="77.4">roches</w> <w n="77.5">profondes</w>,</l>
						<l n="78" num="20.2"><w n="78.1">Les</w> <w n="78.2">sources</w> <w n="78.3">firent</w> <w n="78.4">un</w> <w n="78.5">moment</w></l>
						<l n="79" num="20.3"><w n="79.1">Rire</w> <w n="79.2">d</w>’<w n="79.3">aise</w> <w n="79.4">leurs</w> <w n="79.5">fraîches</w> <w n="79.6">ondes</w></l>
						<l n="80" num="20.4"><w n="80.1">Dans</w> <w n="80.2">leur</w> <w n="80.3">joyeux</w> <w n="80.4">clapotement</w>.</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1"><w n="81.1">Des</w> <w n="81.2">cieux</w> <w n="81.3">la</w> <w n="81.4">voûte</w> <w n="81.5">ensoleillée</w>,</l>
						<l n="82" num="21.2"><w n="82.1">D</w>’<w n="82.2">un</w> <w n="82.3">éclat</w> <w n="82.4">plus</w> <w n="82.5">pur</w> <w n="82.6">resplendit</w>,</l>
						<l n="83" num="21.3"><w n="83.1">Et</w>, <w n="83.2">clairement</w> <w n="83.3">émerveillée</w>,</l>
						<l n="84" num="21.4"><w n="84.1">La</w> <w n="84.2">nature</w> <w n="84.3">entière</w> <w n="84.4">applaudit</w>.</l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1"><w n="85.1">Mais</w> <w n="85.2">tout</w> <w n="85.3">à</w> <w n="85.4">coup</w> <w n="85.5">une</w> <w n="85.6">voix</w>, <w n="85.7">celle</w></l>
						<l n="86" num="22.2"><w n="86.1">D</w>’<w n="86.2">Adonaï</w>, <w n="86.3">de</w> <w n="86.4">Jéhova</w>,</l>
						<l n="87" num="22.3"><w n="87.1">Troublant</w> <w n="87.2">l</w>’<w n="87.3">extase</w> <w n="87.4">universelle</w>,</l>
						<l n="88" num="22.4"><w n="88.1">Comme</w> <w n="88.2">un</w> <w n="88.3">vent</w> <w n="88.4">de</w> <w n="88.5">feu</w> <w n="88.6">s</w>’<w n="88.7">éleva</w> !</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1"><w n="89.1">C</w>’<w n="89.2">est</w> <w n="89.3">ici</w>, — <w n="89.4">mais</w> <w n="89.5">à</w> <w n="89.6">Dieu</w> <w n="89.7">ne</w> <w n="89.8">plaise</w></l>
						<l n="90" num="23.2"><w n="90.1">Que</w> <w n="90.2">ma</w> <w n="90.3">plume</w> <w n="90.4">soit</w> <w n="90.5">sans</w> <w n="90.6">respect</w>,</l>
						<l n="91" num="23.3"><w n="91.1">Oui</w>, <w n="91.2">c</w>’<w n="91.3">est</w> <w n="91.4">ici</w> <w n="91.5">que</w> <w n="91.6">la</w> <hi rend="ital"><w n="91.7">Genèse</w> </hi></l>
						<l n="92" num="23.4"><w n="92.1">Place</w> <w n="92.2">un</w> <w n="92.3">point</w> <w n="92.4">que</w> <w n="92.5">je</w> <w n="92.6">crois</w> <w n="92.7">suspect</w> :</l>
					</lg>
					<lg n="24">
						<l n="93" num="24.1"><w n="93.1">Gratuitement</w> <w n="93.2">elle</w> <w n="93.3">diffame</w></l>
						<l n="94" num="24.2"><w n="94.1">L</w>’<w n="94.2">instigatrice</w> <w n="94.3">du</w> <w n="94.4">péché</w> ;</l>
						<l n="95" num="24.3"><w n="95.1">Ce</w> <w n="95.2">premier</w> <w n="95.3">péché</w> <w n="95.4">de</w> <w n="95.5">la</w> <w n="95.6">femme</w>,</l>
						<l n="96" num="24.4"><w n="96.1">Depuis</w> <w n="96.2">six</w> <w n="96.3">mille</w> <w n="96.4">ans</w> <w n="96.5">reproché</w>.</l>
					</lg>
					<lg n="25">
						<l n="97" num="25.1"><w n="97.1">L</w>’<w n="97.2">est</w> <w n="97.3">son</w> <w n="97.4">triomphe</w> ; <w n="97.5">c</w>’<w n="97.6">est</w> <w n="97.7">sa</w> <w n="97.8">gloire</w> !</l>
						<l n="98" num="25.2"><w n="98.1">La</w> <w n="98.2">Genèse</w> <w n="98.3">même</w> <w n="98.4">le</w> <w n="98.5">dit</w>,</l>
						<l n="99" num="25.3"><w n="99.1">Dans</w> <w n="99.2">son</w> <w n="99.3">texte</w> <w n="99.4">contradictoire</w> ;</l>
						<l n="100" num="25.4"><w n="100.1">C</w>’<w n="100.2">est</w> <w n="100.3">la</w> <w n="100.4">chute</w> <w n="100.5">qui</w> <w n="100.6">la</w> <w n="100.7">grandit</w> !</l>
					</lg>
					<lg n="26">
						<l n="101" num="26.1"><w n="101.1">Car</w> <w n="101.2">pour</w> <w n="101.3">suprême</w> <w n="101.4">pénitence</w>,</l>
						<l n="102" num="26.2"><w n="102.1">Sur</w> <w n="102.2">les</w> <w n="102.3">deux</w> <w n="102.4">coupables</w> <w n="102.5">époux</w>,</l>
						<l n="103" num="26.3"><w n="103.1">Dieu</w> <w n="103.2">fit</w> <w n="103.3">tomber</w> <w n="103.4">une</w> <w n="103.5">sentence</w></l>
						<l n="104" num="26.4"><w n="104.1">Qu</w>’<w n="104.2">ils</w> <w n="104.3">durent</w> <w n="104.4">bénir</w> <w n="104.5">à</w> <w n="104.6">genoux</w> ;</l>
					</lg>
					<lg n="27">
						<l n="105" num="27.1"><w n="105.1">Adam</w> <w n="105.2">dut</w> <w n="105.3">féconder</w> <w n="105.4">la</w> <w n="105.5">terre</w>,</l>
						<l n="106" num="27.2"><w n="106.1">Le</w> <w n="106.2">sol</w>, <w n="106.3">de</w> <w n="106.4">sa</w> <w n="106.5">sueur</w> <w n="106.6">baigné</w>,</l>
						<l n="107" num="27.3"><w n="107.1">Et</w> <w n="107.2">par</w> <w n="107.3">le</w> <w n="107.4">travail</w> <w n="107.5">noble</w>, <w n="107.6">austère</w>,</l>
						<l n="108" num="27.4"><w n="108.1">Manger</w> <w n="108.2">son</w> <w n="108.3">pain</w> <w n="108.4">ainsi</w> <w n="108.5">gagné</w>.</l>
					</lg>
					<lg n="28">
						<l n="109" num="28.1"><w n="109.1">Ève</w> ? — <w n="109.2">Dieu</w> <w n="109.3">voulut</w> <w n="109.4">dans</w> <w n="109.5">ses</w> <w n="109.6">voies</w>,</l>
						<l n="110" num="28.2"><w n="110.1">Qui</w> <w n="110.2">sont</w> <w n="110.3">de</w> <w n="110.4">toute</w> <w n="110.5">éternité</w>,</l>
						<l n="111" num="28.3"><w n="111.1">Qu</w>’<w n="111.2">elle</w> <w n="111.3">connut</w> <w n="111.4">les</w> <w n="111.5">chastes</w> <w n="111.6">joies</w>,</l>
						<l n="112" num="28.4"><w n="112.1">L</w>’<w n="112.2">orgueil</w> <w n="112.3">de</w> <w n="112.4">la</w> <w n="112.5">maternité</w> !</l>
					</lg>
					<lg n="29">
						<l n="113" num="29.1"><w n="113.1">De</w> <w n="113.2">bonne</w> <w n="113.3">foi</w>, <w n="113.4">je</w> <w n="113.5">le</w> <w n="113.6">demande</w>,</l>
						<l n="114" num="29.2"><w n="114.1">Mesdames</w>, <w n="114.2">qui</w> <w n="114.3">de</w> <w n="114.4">vous</w>, <w n="114.5">vraiment</w>,</l>
						<l n="115" num="29.3"><w n="115.1">N</w>’<w n="115.2">eût</w> <w n="115.3">pas</w> <w n="115.4">même</w> <w n="115.5">ajouté</w> <w n="115.6">l</w>’<w n="115.7">amende</w>,</l>
						<l n="116" num="29.4"><w n="116.1">A</w> <w n="116.2">ce</w> <w n="116.3">rédempteur</w> <w n="116.4">châtiment</w> ?</l>
					</lg>
					<lg n="30">
						<l n="117" num="30.1"><w n="117.1">Croyez</w>-<w n="117.2">le</w> <w n="117.3">bien</w>, <w n="117.4">si</w> <w n="117.5">jamais</w> <w n="117.6">l</w>’<w n="117.7">homme</w>,</l>
						<l n="118" num="30.2"><w n="118.1">Si</w> <w n="118.2">jamais</w> <w n="118.3">Adam</w> <w n="118.4">fut</w> <w n="118.5">tenté</w></l>
						<l n="119" num="30.3"><w n="119.1">De</w> <w n="119.2">déplorer</w>… <w n="119.3">ce</w> <w n="119.4">vol</w> <w n="119.5">de</w> <w n="119.6">pomme</w>,</l>
						<l n="120" num="30.4"><w n="120.1">De</w> <w n="120.2">regretter</w> <w n="120.3">son</w> <hi rend="ital"><w n="120.4">far</w> <w n="120.5">niente</w></hi>,</l>
					</lg>
					<lg n="31">
						<l n="121" num="31.1"><w n="121.1">Sans</w> <w n="121.2">grand</w> <w n="121.3">effort</w> <w n="121.4">pour</w> <w n="121.5">le</w> <w n="121.6">confondre</w>,</l>
						<l n="122" num="31.2"><w n="122.1">Et</w> <w n="122.2">le</w> <w n="122.3">rendre</w> <w n="122.4">à</w> <w n="122.5">jamais</w> <w n="122.6">joyeux</w>,</l>
						<l n="123" num="31.3"><w n="123.1">Ève</w> <w n="123.2">à</w> <w n="123.3">coup</w> <w n="123.4">sûr</w> <w n="123.5">dut</w> <w n="123.6">lui</w> <w n="123.7">répondre</w> :</l>
						<l n="124" num="31.4">— « <w n="124.1">Tu</w> <w n="124.2">te</w> <w n="124.3">plains</w> ? <w n="124.4">ouvre</w> <w n="124.5">donc</w> <w n="124.6">les</w> <w n="124.7">yeux</w>.</l>
					</lg>
					<lg n="32">
						<l n="125" num="32.1">« <w n="125.1">Des</w> <w n="125.2">Œuvres</w> <w n="125.3">du</w> <w n="125.4">Seigneur</w>, <w n="125.5">rivales</w>,</l>
						<l n="126" num="32.2"><w n="126.1">Les</w> <w n="126.2">tiennes</w>, <w n="126.3">précieux</w> <w n="126.4">trésor</w> !</l>
						<l n="127" num="32.3"><w n="127.1">Ondoient</w> <w n="127.2">aux</w> <w n="127.3">brises</w> <w n="127.4">estivales</w>,</l>
						<l n="128" num="32.4"><w n="128.1">Comme</w> <w n="128.2">un</w> <w n="128.3">superbe</w> <w n="128.4">océan</w> <w n="128.5">d</w>’<w n="128.6">or</w> !</l>
					</lg>
					<lg n="33">
						<l n="129" num="33.1">« <w n="129.1">C</w>’<w n="129.2">est</w> <w n="129.3">ton</w> <w n="129.4">travail</w> <w n="129.5">opiniâtre</w>,</l>
						<l n="130" num="33.2"><w n="130.1">C</w>’<w n="130.2">est</w> <w n="130.3">ta</w> <w n="130.4">conquête</w> ! <w n="130.5">et</w>, <w n="130.6">sort</w> <w n="130.7">plus</w> <w n="130.8">doux</w> :</l>
						<l n="131" num="33.3"><w n="131.1">Nous</w> <w n="131.2">étions</w> <w n="131.3">deux</w>… <w n="131.4">Nous</w> <w n="131.5">sommes</w> <w n="131.6">quatre</w> !</l>
						<l n="132" num="33.4"><w n="132.1">Bénis</w> <w n="132.2">ma</w> <w n="132.3">faute</w>, <w n="132.4">ô</w> <w n="132.5">mon</w> <w n="132.6">époux</w> ! »</l>
					</lg>
					<lg n="34">
						<l n="133" num="34.1"><w n="133.1">Et</w> <w n="133.2">sur</w> <w n="133.3">ce</w>, <w n="133.4">le</w> <w n="133.5">père</w> <w n="133.6">des</w> <w n="133.7">hommes</w>, —</l>
						<l n="134" num="34.2"><w n="134.1">C</w>’<w n="134.2">est</w> <w n="134.3">clair</w>, <w n="134.4">puisqu</w>’<w n="134.5">il</w> <w n="134.6">fut</w> <w n="134.7">le</w> <w n="134.8">premier</w> —</l>
						<l n="135" num="34.3"><w n="135.1">Pour</w> <w n="135.2">voir</w> <w n="135.3">s</w>’<w n="135.4">il</w> <w n="135.5">restait</w> <w n="135.6">quelques</w> <w n="135.7">pommes</w>…</l>
						<l n="136" num="34.4"><w n="136.1">Alla</w> <w n="136.2">secouer</w> <w n="136.3">le</w> <w n="136.4">pommier</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1880">1880</date>
						</dateline>
						<note type="footnote" id="1">C’est après avoir lu un très spirituel récit <lb></lb>
						dans le <hi rend="ital">Talmud</hi> d’ALEXANDRE WEILL, que je composai <lb></lb>
						cette légende.</note>
					</closer>
				</div></body></text></TEI>