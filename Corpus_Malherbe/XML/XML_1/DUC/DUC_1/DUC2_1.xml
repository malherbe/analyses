<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Capricieuses</title>
				<title type="medium">Une édition électronique</title>
				<author key="DUC">
					<name>
						<forename>Alexandre</forename>
						<surname>DUCROS</surname>
					</name>
					<date from="1823" to="1906">1823-1906</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>830 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">DUC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Capricieuse</title>
						<author>Alexandre Ducros</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k6101950r.r=alexandre%20ducros?rk=193134;0</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Capricieuses</title>
								<author>Alexandre Ducros</author>
								<imprint>
									<pubPlace>Nîmes</pubPlace>
									<publisher>IMPRIMERIE TYPOGRAPHIQUE BALDY ET ROGER</publisher>
									<date when="1854">1854</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La dédicace en début d’ouvrage n’est pas reprise dans cette édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>Certains retraits de vers ont été supprimés conformément à l’édition de référence.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DUC2">
				<head type="main">SOUVENIRS</head>
				<opener>
					<salute>A ma Mère.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Revenez</w> <w n="1.2">tous</w> <w n="1.3">dans</w> <w n="1.4">ma</w> <w n="1.5">demeure</w>,</l>
					<l n="2" num="1.2"><w n="2.1">O</w> <w n="2.2">vous</w>, <w n="2.3">mes</w> <w n="2.4">jeunes</w> <w n="2.5">souvenirs</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Retracez</w> <w n="3.2">mes</w> <w n="3.3">jeunes</w> <w n="3.4">plaisirs</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Lorsque</w> <w n="4.2">mon</w> <w n="4.3">âme</w> <w n="4.4">est</w> <w n="4.5">triste</w> <w n="4.6">et</w> <w n="4.7">pleure</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Venez</w> <w n="5.2">apaiser</w> <w n="5.3">ses</w> <w n="5.4">soupirs</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Rapportez</w> <w n="6.2">ma</w> <w n="6.3">douce</w> <w n="6.4">croyance</w></l>
					<l n="7" num="2.2"><w n="7.1">Avec</w> <w n="7.2">ma</w> <w n="7.3">naïve</w> <w n="7.4">ignorance</w></l>
					<l n="8" num="2.3"><w n="8.1">Et</w> <w n="8.2">même</w> <w n="8.3">mes</w> <w n="8.4">chagrins</w> <w n="8.5">d</w>’<w n="8.6">enfant</w> ;</l>
					<l n="9" num="2.4"><w n="9.1">Alors</w> <w n="9.2">qu</w>’<w n="9.3">un</w> <w n="9.4">baiser</w> <w n="9.5">de</w> <w n="9.6">ma</w> <w n="9.7">mère</w>,</l>
					<l n="10" num="2.5"><w n="10.1">Effaçant</w> <w n="10.2">leur</w> <w n="10.3">ombre</w> <w n="10.4">éphémère</w>,</l>
					<l n="11" num="2.6"><w n="11.1">Rendait</w> <w n="11.2">mon</w> <w n="11.3">front</w> <w n="11.4">plus</w> <w n="11.5">souriant</w>.</l>
				</lg>
				<lg n="3">
					<l n="12" num="3.1"><w n="12.1">Oh</w> ! <w n="12.2">dans</w> <w n="12.3">ma</w> <w n="12.4">chambre</w> <w n="12.5">vaste</w> <w n="12.6">et</w> <w n="12.7">nue</w>,</l>
					<l n="13" num="3.2"><w n="13.1">Où</w> <w n="13.2">j</w>’<w n="13.3">entends</w> <w n="13.4">mon</w> <w n="13.5">cœur</w> <w n="13.6">sangloter</w>,</l>
					<l n="14" num="3.3"><w n="14.1">Revenez</w> <w n="14.2">tous</w> <w n="14.3">vous</w> <w n="14.4">abriter</w></l>
					<l n="15" num="3.4"><w n="15.1">Avec</w> <w n="15.2">votre</w> <w n="15.3">joie</w> <w n="15.4">inconnue</w>…</l>
					<l n="16" num="3.5"><w n="16.1">Pourquoi</w> <w n="16.2">m</w>’<w n="16.3">avez</w>-<w n="16.4">vous</w> <w n="16.5">pu</w> <w n="16.6">quitter</w> ?…</l>
				</lg>
				<lg n="4">
					<l n="17" num="4.1"><w n="17.1">Vous</w> <w n="17.2">auriez</w> <w n="17.3">embelli</w> <w n="17.4">ma</w> <w n="17.5">route</w> ;</l>
					<l n="18" num="4.2"><w n="18.1">Votre</w> <w n="18.2">voix</w>, <w n="18.3">que</w> <w n="18.4">toujours</w> <w n="18.5">j</w>’<w n="18.6">écoute</w></l>
					<l n="19" num="4.3"><w n="19.1">Comme</w> <w n="19.2">un</w> <w n="19.3">murmure</w>, <w n="19.4">un</w> <w n="19.5">bruit</w> <w n="19.6">lointain</w>,</l>
					<l n="20" num="4.4"><w n="20.1">Aurait</w> <w n="20.2">conseillé</w> <w n="20.3">ma</w> <w n="20.4">pensée</w>,</l>
					<l n="21" num="4.5"><w n="21.1">Guidé</w> <w n="21.2">mes</w> <w n="21.3">pas</w> <w n="21.4">dans</w> <w n="21.5">la</w> <w n="21.6">vallée</w></l>
					<l n="22" num="4.6"><w n="22.1">Où</w> <w n="22.2">le</w> <w n="22.3">doute</w> <w n="22.4">croît</w> <w n="22.5">au</w> <w n="22.6">chemin</w>.</l>
				</lg>
				<lg n="5">
					<l n="23" num="5.1"><w n="23.1">Vous</w> <w n="23.2">m</w>’<w n="23.3">auriez</w> <w n="23.4">dit</w> : — « <w n="23.5">Enfant</w>, <w n="23.6">ta</w> <w n="23.7">mère</w> »</l>
					<l n="24" num="5.2"><w n="24.1">Te</w> <w n="24.2">fit</w> <w n="24.3">une</w> <w n="24.4">bien</w> <w n="24.5">douce</w> <w n="24.6">loi</w>,</l>
					<l n="25" num="5.3">« <w n="25.1">Lorsque</w>, <w n="25.2">son</w> <w n="25.3">front</w> <w n="25.4">penché</w> <w n="25.5">sur</w> <w n="25.6">toi</w>,</l>
					<l n="26" num="5.4">« <w n="26.1">Elle</w> <w n="26.2">t</w>’<w n="26.3">apprit</w> <w n="26.4">cette</w> <w n="26.5">prière</w></l>
					<l n="27" num="5.5">« <w n="27.1">Que</w> <w n="27.2">tu</w> <w n="27.3">récitais</w> <w n="27.4">avec</w> <w n="27.5">foi</w>… »</l>
				</lg>
				<lg n="6">
					<l n="28" num="6.1"><w n="28.1">De</w> <w n="28.2">la</w> <w n="28.3">foi</w> <w n="28.4">naquit</w> <w n="28.5">l</w>’<w n="28.6">espérance</w>,</l>
					<l n="29" num="6.2"><w n="29.1">Qui</w> <w n="29.2">sait</w> <w n="29.3">calmer</w> <w n="29.4">toute</w> <w n="29.5">souffrance</w></l>
					<l n="30" num="6.3"><w n="30.1">Et</w> <w n="30.2">qui</w> <w n="30.3">fait</w> <w n="30.4">croire</w> <w n="30.5">à</w> <w n="30.6">tout</w> <w n="30.7">bonheur</w>.</l>
					<l n="31" num="6.4"><w n="31.1">Vous</w> <w n="31.2">m</w>’<w n="31.3">auriez</w> <w n="31.4">montré</w> <w n="31.5">celte</w> <w n="31.6">voie</w>,</l>
					<l n="32" num="6.5"><w n="32.1">Et</w> <w n="32.2">vous</w> <w n="32.3">auriez</w>, <w n="32.4">par</w> <w n="32.5">tant</w> <w n="32.6">de</w> <w n="32.7">joie</w>,</l>
					<l n="33" num="6.6"><w n="33.1">Tari</w> <w n="33.2">les</w> <w n="33.3">larmes</w> <w n="33.4">de</w> <w n="33.5">mon</w> <w n="33.6">cœur</w> !</l>
				</lg>
				<lg n="7">
					<l n="34" num="7.1"><w n="34.1">Vous</w> <w n="34.2">auriez</w> <w n="34.3">grandi</w> <w n="34.4">mon</w> <w n="34.5">courage</w></l>
					<l n="35" num="7.2"><w n="35.1">Dans</w> <w n="35.2">les</w> <w n="35.3">jours</w> <w n="35.4">de</w> <w n="35.5">l</w>’<w n="35.6">adversité</w> ;</l>
					<l n="36" num="7.3"><w n="36.1">Chacun</w> <w n="36.2">de</w> <w n="36.3">vous</w> <w n="36.4">m</w>’<w n="36.5">eût</w> <w n="36.6">répété</w></l>
					<l n="37" num="7.4"><w n="37.1">Que</w> <w n="37.2">jadis</w> <w n="37.3">je</w> <w n="37.4">bravais</w> <w n="37.5">l</w>’<w n="37.6">orage</w></l>
					<l n="38" num="7.5"><w n="38.1">Qu</w>’<w n="38.2">ignorait</w> <w n="38.3">ma</w> <w n="38.4">simplicité</w>.</l>
				</lg>
				<lg n="8">
					<l n="39" num="8.1"><w n="39.1">Vous</w> <w n="39.2">m</w>’<w n="39.3">auriez</w> <w n="39.4">dit</w> <w n="39.5">ces</w> <w n="39.6">douces</w> <w n="39.7">choses</w>,</l>
					<l n="40" num="8.2"><w n="40.1">Mots</w> <w n="40.2">suspendus</w> <w n="40.3">aux</w> <w n="40.4">lèvres</w> <w n="40.5">roses</w></l>
					<l n="41" num="8.3"><w n="41.1">De</w> <w n="41.2">l</w>’<w n="41.3">enfant</w> <w n="41.4">qui</w> <w n="41.5">bégaie</w> <w n="41.6">et</w> <w n="41.7">rit</w> ;</l>
					<l n="42" num="8.4"><w n="42.1">Mots</w> <w n="42.2">qu</w>’<w n="42.3">il</w> <w n="42.4">ne</w> <w n="42.5">comprend</w> <w n="42.6">pas</w> <w n="42.7">encore</w>,</l>
					<l n="43" num="8.5"><w n="43.1">Qu</w>’<w n="43.2">il</w> <w n="43.3">adresse</w> <w n="43.4">au</w> <w n="43.5">Dieu</w> <w n="43.6">qu</w>’<w n="43.7">il</w> <w n="43.8">ignore</w>,</l>
					<l n="44" num="8.6"><w n="44.1">Mais</w> <w n="44.2">qu</w>’<w n="44.3">il</w> <w n="44.4">voit</w> <w n="44.5">pourtant</w> <w n="44.6">en</w> <w n="44.7">esprit</w>.</l>
				</lg>
				<lg n="9">
					<l n="45" num="9.1"><w n="45.1">Vous</w> <w n="45.2">m</w>’<w n="45.3">auriez</w> <w n="45.4">dit</w> <w n="45.5">mon</w> <w n="45.6">jeune</w>, <w n="45.7">rêve</w>,</l>
					<l n="46" num="9.2"><w n="46.1">Mes</w> <w n="46.2">naïves</w> <w n="46.3">illusions</w>,</l>
					<l n="47" num="9.3"><w n="47.1">Mes</w> <w n="47.2">jours</w> <w n="47.3">exempts</w> <w n="47.4">de</w> <w n="47.5">passions</w>,</l>
					<l n="48" num="9.4"><w n="48.1">Pareils</w> <w n="48.2">au</w> <w n="48.3">soleil</w> <w n="48.4">qui</w> <w n="48.5">se</w> <w n="48.6">lève</w></l>
					<l n="49" num="9.5"><w n="49.1">Et</w> <w n="49.2">qui</w> <w n="49.3">promet</w> <w n="49.4">tant</w> <w n="49.5">de</w> <w n="49.6">rayons</w>.</l>
				</lg>
				<lg n="10">
					<l n="50" num="10.1"><w n="50.1">Vous</w> <w n="50.2">auriez</w> <w n="50.3">dévié</w> <w n="50.4">ma</w> <w n="50.5">course</w></l>
					<l n="51" num="10.2"><w n="51.1">De</w> <w n="51.2">cet</w> <w n="51.3">abîme</w>, <w n="51.4">vaste</w> <w n="51.5">source</w></l>
					<l n="52" num="10.3"><w n="52.1">De</w> <w n="52.2">maux</w> <w n="52.3">et</w> <w n="52.4">de</w> <w n="52.5">corruptions</w>,</l>
					<l n="53" num="10.4"><w n="53.1">De</w> <w n="53.2">dégoût</w>, <w n="53.3">d</w>’<w n="53.4">implacable</w> <w n="53.5">haine</w>,</l>
					<l n="54" num="10.5"><w n="54.1">Fosse</w> <w n="54.2">béante</w> <w n="54.3">et</w> <w n="54.4">toujours</w> <w n="54.5">pleine</w></l>
					<l n="55" num="10.6"><w n="55.1">De</w> <w n="55.2">cris</w> <w n="55.3">de</w> <w n="55.4">malédictions</w>.</l>
				</lg>
				<lg n="11">
					<l n="56" num="11.1"><w n="56.1">Souvenirs</w>… <w n="56.2">image</w> <w n="56.3">si</w> <w n="56.4">chère</w></l>
					<l n="57" num="11.2"><w n="57.1">De</w> <w n="57.2">tout</w> <w n="57.3">mon</w> <w n="57.4">bonheur</w> <w n="57.5">d</w>’<w n="57.6">autrefois</w>,</l>
					<l n="58" num="11.3"><w n="58.1">Revenez</w> <w n="58.2">encore</w> <w n="58.3">une</w> <w n="58.4">fois</w>,</l>
					<l n="59" num="11.4"><w n="59.1">Mêlez</w> <w n="59.2">à</w> <w n="59.3">la</w> <w n="59.4">voix</w> <w n="59.5">de</w> <w n="59.6">ma</w> <w n="59.7">mère</w></l>
					<l n="60" num="11.5"><w n="60.1">Le</w> <w n="60.2">bruit</w> <w n="60.3">sacré</w> <w n="60.4">de</w> <w n="60.5">votre</w> <w n="60.6">voix</w>.</l>
				</lg>
				<lg n="12">
					<l n="61" num="12.1"><w n="61.1">Oh</w> ! <w n="61.2">revenez</w>, <w n="61.3">candide</w> <w n="61.4">troupe</w>,</l>
					<l n="62" num="12.2"><w n="62.1">Ma</w> <w n="62.2">présenter</w> <w n="62.3">encor</w> <w n="62.4">la</w> <w n="62.5">coupe</w></l>
					<l n="63" num="12.3"><w n="63.1">Où</w> <w n="63.2">sont</w> <w n="63.3">tant</w> <w n="63.4">de</w> <w n="63.5">précieux</w> <w n="63.6">dons</w>…</l>
					<l n="64" num="12.4"><w n="64.1">Car</w> <w n="64.2">j</w>’<w n="64.3">ai</w> <w n="64.4">bu</w> <w n="64.5">l</w>’<w n="64.6">absinthe</w> <w n="64.7">fatale</w>,</l>
					<l n="65" num="12.5"><w n="65.1">Laissé</w> <w n="65.2">ma</w> <w n="65.3">robe</w> <w n="65.4">virginale</w></l>
					<l n="66" num="12.6"><w n="66.1">Sur</w> <w n="66.2">les</w> <w n="66.3">ronces</w> <w n="66.4">et</w> <w n="66.5">les</w> <w n="66.6">chardons</w>…</l>
				</lg>
				<closer>
					<dateline>
						<placeName>Montpellier</placeName>,
						<date when="1852">juillet 1852</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>