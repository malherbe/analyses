<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre puéril</head><head type="main_subpart">VI</head><head type="main_subpart">L’Initiation du cœur</head><head type="sub_1">… et des mots étouffés fait ses seules caresses…</head><div type="poem" key="HEU14">
						<head type="sub_1">L’AMOUR BALBUTIÉ</head>
						<head type="main">SOLEILS COUCHÉS</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Toute</w> <w n="1.2">ma</w> <w n="1.3">vie</w>, <w n="1.4">enfant</w>, <w n="1.5">de</w> <w n="1.6">ton</w> <w n="1.7">charme</w> <w n="1.8">est</w> <w n="1.9">hantée</w>…</l>
							<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">jour</w> <w n="2.3">où</w> <w n="2.4">tu</w> <w n="2.5">n</w>’<w n="2.6">es</w> <w n="2.7">plus</w> <w n="2.8">n</w>’<w n="2.9">est</w> <w n="2.10">qu</w>’<w n="2.11">une</w> <w n="2.12">ombre</w> <w n="2.13">enchantée</w></l>
							<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">ton</w> <w n="3.3">prestige</w> <w n="3.4">épars</w> <w n="3.5">se</w> <w n="3.6">prolonge</w> <w n="3.7">en</w> <w n="3.8">vainqueur</w>…</l>
							<l n="4" num="1.4"><w n="4.1">Vois</w> ! <w n="4.2">mes</w> <w n="4.3">yeux</w> <w n="4.4">éblouis</w> <w n="4.5">sous</w> <w n="4.6">leur</w> <w n="4.7">vaine</w> <w n="4.8">paupière</w>,</l>
							<l n="5" num="1.5"><space quantity="16" unit="char"></space><w n="5.1">Retiennent</w> <w n="5.2">ta</w> <w n="5.3">lumière</w></l>
							<l n="6" num="1.6"><space quantity="16" unit="char"></space><w n="6.1">En</w> <w n="6.2">un</w> <w n="6.3">défi</w> <w n="6.4">du</w> <w n="6.5">cœur</w> !</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><w n="7.1">Ainsi</w>, <w n="7.2">lorsque</w> <w n="7.3">le</w> <w n="7.4">soir</w>, <w n="7.5">tels</w> <w n="7.6">que</w> <w n="7.7">de</w> <w n="7.8">vagues</w> <w n="7.9">tulles</w>,</l>
							<l n="8" num="2.2"><w n="8.1">Fait</w> <w n="8.2">s</w>’<w n="8.3">abattre</w> <w n="8.4">en</w> <w n="8.5">flottant</w> <w n="8.6">de</w> <w n="8.7">graves</w> <w n="8.8">crépuscules</w>,</l>
							<l n="9" num="2.3"><w n="9.1">Quelque</w> <w n="9.2">rayon</w> <w n="9.3">parfois</w> <w n="9.4">y</w> <w n="9.5">creuse</w> <w n="9.6">un</w> <w n="9.7">trou</w> <w n="9.8">vermeil</w>,</l>
							<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">brodant</w> <w n="10.3">de</w> <w n="10.4">ses</w> <w n="10.5">ors</w> <w n="10.6">la</w> <w n="10.7">gaze</w> <w n="10.8">des</w> <w n="10.9">nuits</w> <w n="10.10">sombres</w>,</l>
							<l n="11" num="2.5"><space quantity="16" unit="char"></space><w n="11.1">Irise</w> <w n="11.2">encor</w> <w n="11.3">les</w> <w n="11.4">ombres</w></l>
							<l n="12" num="2.6"><space quantity="16" unit="char"></space><w n="12.1">D</w>’<w n="12.2">un</w> <w n="12.3">reste</w> <w n="12.4">de</w> <w n="12.5">soleil</w> !</l>
						</lg>
					</div></body></text></TEI>