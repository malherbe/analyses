<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">VI</head><head type="main_subpart">Le Fumet du Quotidien</head><div type="poem" key="HEU44">
						<opener>
							<epigraph>
								<cit>
									<quote>… « <hi rend="ital">Ce sont de grandes dames</hi>… »</quote>
									<bibl>
										<name>(<hi rend="ital">La Tour de Nesles.</hi> Dumas père.)</name>
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<head type="main">CANTINES DE GUERRE</head>
						<div type="section" n="1">
							<head type="number">I</head>
							<head type="main">En rupture de Samothrace</head>
							<lg n="1">
								<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">auberge</w> <w n="1.3">de</w> <w n="1.4">mon</w> <w n="1.5">choix</w> <w n="1.6">m</w>’<w n="1.7">excède</w></l>
								<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2">un</w> <w n="2.3">rétif</w> <w n="2.4">et</w> <w n="2.5">long</w> <w n="2.6">contre</w>-<w n="2.7">sens</w>…</l>
								<l n="3" num="1.3"><w n="3.1">Non</w> <w n="3.2">qu</w>’<w n="3.3">au</w> <w n="3.4">rabais</w> <w n="3.5">elle</w> <w n="3.6">nous</w> <w n="3.7">cède</w></l>
								<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">chapon</w> <w n="4.3">que</w> <w n="4.4">gave</w> <w n="4.5">le</w> <w n="4.6">Mans</w>…</l>
							</lg>
							<lg n="2">
								<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">table</w> <w n="5.3">claire</w> <w n="5.4">offre</w> <w n="5.5">une</w> <w n="5.6">eau</w> <w n="5.7">fraîche</w></l>
								<l n="6" num="2.2"><w n="6.1">A</w> <w n="6.2">des</w> <w n="6.3">clients</w> <w n="6.4">de</w> <w n="6.5">quatre</w> <w n="6.6">sous</w>…</l>
								<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">ivresse</w>, <w n="7.3">même</w> <w n="7.4">de</w> <w n="7.5">Campêche</w>,</l>
								<l n="8" num="2.4"><w n="8.1">Ne</w> <w n="8.2">les</w> <w n="8.3">roule</w> <w n="8.4">jamais</w> <w n="8.5">dessous</w>.</l>
							</lg>
							<lg n="3">
								<l n="9" num="3.1"><w n="9.1">Ma</w> <w n="9.2">mémoire</w>, <w n="9.3">quel</w> <w n="9.4">est</w> <w n="9.5">l</w>’<w n="9.6">artiste</w>,</l>
								<l n="10" num="3.2">— <w n="10.1">Est</w>-<w n="10.2">ce</w> <w n="10.3">Van</w> <w n="10.4">Dyck</w>, <w n="10.5">est</w>-<w n="10.6">ce</w> <w n="10.7">Callot</w> ? —</l>
								<l n="11" num="3.3"><w n="11.1">Qui</w> <w n="11.2">de</w> <w n="11.3">l</w>’<w n="11.4">agape</w> <w n="11.5">fantaisiste</w></l>
								<l n="12" num="3.4"><w n="12.1">Sortait</w> <w n="12.2">en</w> <w n="12.3">peignant</w> <w n="12.4">son</w> <w n="12.5">écot</w> ?</l>
							</lg>
							<lg n="4">
								<l n="13" num="4.1"> ? —<w n="13.1">Mais</w> <w n="13.2">foin</w> <w n="13.3">d</w>’<w n="13.4">une</w> <w n="13.5">absurde</w> <w n="13.6">ressource</w>…</l>
								<l n="14" num="4.2"><w n="14.1">Plus</w> <w n="14.2">de</w> <w n="14.3">festins</w> <w n="14.4">qui</w> <w n="14.5">soient</w> <w n="14.6">trop</w> <w n="14.7">verts</w> !</l>
								<l n="15" num="4.3"><w n="15.1">Les</w> <w n="15.2">poètes</w>, <w n="15.3">mal</w> <w n="15.4">vus</w> <w n="15.5">en</w> <w n="15.6">bourse</w>,</l>
								<l n="16" num="4.4"><w n="16.1">N</w>’<w n="16.2">auront</w> <w n="16.3">point</w> <w n="16.4">à</w> <w n="16.5">solder</w> <w n="16.6">en</w> <w n="16.7">vers</w>.</l>
							</lg>
							<lg n="5">
								<l n="17" num="5.1"><w n="17.1">Nul</w> <w n="17.2">n</w>’<w n="17.3">est</w> <w n="17.4">si</w> <w n="17.5">pauvre</w> <w n="17.6">que</w> <w n="17.7">Tantale</w></l>
								<l n="18" num="5.2"><w n="18.1">Tournant</w> <w n="18.2">au</w> <w n="18.3">gueux</w> <w n="18.4">dans</w> <w n="18.5">ce</w> <w n="18.6">pays</w>,</l>
								<l n="19" num="5.3"><w n="19.1">Doive</w>, <w n="19.2">exilé</w> <w n="19.3">de</w> <w n="19.4">notre</w> <w n="19.5">salle</w>,</l>
								<l n="20" num="5.4"><w n="20.1">Humer</w> <w n="20.2">de</w> <w n="20.3">loin</w> <w n="20.4">ce</w> <w n="20.5">paradis</w>…</l>
							</lg>
							<lg n="6">
								<l n="21" num="6.1"><w n="21.1">Par</w> <w n="21.2">quelque</w> <w n="21.3">échelle</w> <w n="21.4">qu</w>’<w n="21.5">on</w> <w n="21.6">y</w> <w n="21.7">grimpe</w></l>
								<l n="22" num="6.2"><w n="22.1">Le</w> <w n="22.2">rêve</w> <w n="22.3">agile</w> <w n="22.4">y</w> <w n="22.5">voit</w> <w n="22.6">les</w> <w n="22.7">cieux</w></l>
								<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">savoure</w> <w n="23.3">une</w> <w n="23.4">heure</w> <w n="23.5">d</w>’<w n="23.6">Olympe</w></l>
								<l n="24" num="6.4"><w n="24.1">A</w> <w n="24.2">la</w> <w n="24.3">table</w> <w n="24.4">où</w> <w n="24.5">servent</w> <w n="24.6">les</w> <w n="24.7">dieux</w>.</l>
							</lg>
							<lg n="7">
								<l n="25" num="7.1"><w n="25.1">Si</w> <w n="25.2">Watteau</w> <w n="25.3">changeait</w> <w n="25.4">en</w> <w n="25.5">bergères</w></l>
								<l n="26" num="7.2"><w n="26.1">Les</w> <w n="26.2">marquises</w> <w n="26.3">à</w> <w n="26.4">falbalas</w>…</l>
								<l n="27" num="7.3"><w n="27.1">Nos</w> <w n="27.2">déesses</w>, <w n="27.3">plus</w> <w n="27.4">ménagères</w>,</l>
								<l n="28" num="7.4"><w n="28.1">Perdent</w> <w n="28.2">leurs</w> <w n="28.3">roses</w> <w n="28.4">dans</w> <w n="28.5">nos</w> <w n="28.6">plats</w></l>
							</lg>
							<lg n="8">
								<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">j</w>’<w n="29.3">épie</w> <w n="29.4">et</w> <w n="29.5">suis</w> <w n="29.6">à</w> <w n="29.7">la</w> <w n="29.8">trace</w>,</l>
								<l n="30" num="8.2"><w n="30.1">Aux</w> <w n="30.2">bols</w> <w n="30.3">fumants</w> <w n="30.4">chauffant</w> <w n="30.5">leurs</w> <w n="30.6">doigts</w></l>
								<l n="31" num="8.3"><w n="31.1">Des</w> <w n="31.2">serveuses</w> <w n="31.3">de</w> <w n="31.4">Samothrace</w></l>
								<l n="32" num="8.4"><w n="32.1">Cachant</w> <w n="32.2">des</w> <w n="32.3">ailes</w>… <w n="32.4">que</w> <w n="32.5">je</w> <w n="32.6">vois</w> !</l>
							</lg>
							<lg n="9">
								<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">je</w> <w n="33.3">sens</w>, <w n="33.4">qui</w> <w n="33.5">lutte</w> <w n="33.6">et</w> <w n="33.7">s</w>’<w n="33.8">ébroue</w>,</l>
								<l n="34" num="9.2"><w n="34.1">L</w>’<w n="34.2">impatience</w> <w n="34.3">d</w>’<w n="34.4">un</w> <w n="34.5">vol</w> <w n="34.6">d</w>’<w n="34.7">or</w>,</l>
								<l n="35" num="9.3"><w n="35.1">Quand</w> <w n="35.2">la</w> <w n="35.3">table</w>, <w n="35.4">ainsi</w> <w n="35.5">qu</w>’<w n="35.6">une</w> <w n="35.7">proue</w>,</l>
								<l n="36" num="9.4"><w n="36.1">Invite</w> <w n="36.2">à</w> <w n="36.3">l</w>’<w n="36.4">appui</w> <w n="36.5">leur</w> <w n="36.6">essor</w> !</l>
							</lg>
						</div>
						<div type="section" n="2">
							<head type="number">II</head>
							<head type="main">Et d’autres</head>
							<lg n="1">
								<l n="37" num="1.1"><w n="37.1">Et</w> <w n="37.2">d</w>’<w n="37.3">autres</w>, <w n="37.4">mamans</w> <w n="37.5">qu</w>’<w n="37.6">improvise</w></l>
								<l n="38" num="1.2"><w n="38.1">L</w>’<w n="38.2">instinct</w> <w n="38.3">de</w> <w n="38.4">leurs</w> <w n="38.5">cœurs</w> <w n="38.6">avertis</w>,</l>
								<l n="39" num="1.3"><w n="39.1">Adoptent</w> <w n="39.2">la</w> <w n="39.3">langue</w> <w n="39.4">indécise</w>,</l>
								<l n="40" num="1.4"><w n="40.1">Le</w> <w n="40.2">balbutiement</w> <w n="40.3">des</w> <w n="40.4">petits</w>…</l>
							</lg>
							<lg n="2">
								<l n="41" num="2.1"><w n="41.1">Riens</w> <w n="41.2">profonds</w> <w n="41.3">experts</w> <w n="41.4">à</w> <w n="41.5">tout</w> <w n="41.6">dire</w></l>
								<l n="42" num="2.2"><w n="42.1">Murmures</w> <w n="42.2">voilant</w> <w n="42.3">leurs</w> <w n="42.4">propos</w> !…</l>
								<l n="43" num="2.3"><w n="43.1">Sens</w> <w n="43.2">précis</w> <w n="43.3">d</w>’<w n="43.4">un</w> <w n="43.5">vague</w> <w n="43.6">sourire</w>,</l>
								<l n="44" num="2.4"><w n="44.1">Sourdine</w> <w n="44.2">dans</w> <w n="44.3">la</w> <w n="44.4">paix</w> <w n="44.5">des</w> <w n="44.6">mots</w> !</l>
							</lg>
							<lg n="3">
								<l n="45" num="3.1"><w n="45.1">Au</w> <w n="45.2">seuil</w> <w n="45.3">encor</w> <w n="45.4">de</w> <w n="45.5">la</w> <w n="45.6">réserve</w>,</l>
								<l n="46" num="3.2"><w n="46.1">Leur</w> <w n="46.2">voix</w> <w n="46.3">s</w>’<w n="46.4">enhardit</w> <w n="46.5">pour</w> <w n="46.6">ce</w> <w n="46.7">chant</w>,</l>
								<l n="47" num="3.3"><w n="47.1">Et</w>, <w n="47.2">timide</w> <w n="47.3">en</w> <w n="47.4">veine</w> <w n="47.5">de</w> <w n="47.6">verve</w>,</l>
								<l n="48" num="3.4"><w n="48.1">Le</w> <w n="48.2">babil</w> <w n="48.3">en</w> <w n="48.4">devient</w> <w n="48.5">touchant</w>.</l>
							</lg>
							<lg n="4">
								<l n="49" num="4.1"><w n="49.1">Ah</w> ! <w n="49.2">que</w> <w n="49.3">fait</w> <w n="49.4">d</w>’<w n="49.5">elles</w> <w n="49.6">l</w>’<w n="49.7">existence</w>,</l>
								<l n="50" num="4.2"><w n="50.1">De</w> <w n="50.2">ces</w> <w n="50.3">mamans</w> <w n="50.4">des</w> <w n="50.5">fils</w> <w n="50.6">d</w>’<w n="50.7">autrui</w> ?</l>
								<l n="51" num="4.3"><w n="51.1">Que</w> <w n="51.2">sont</w>-<w n="51.3">elles</w> <w n="51.4">dans</w> <w n="51.5">leur</w> <w n="51.6">silence</w>,</l>
								<l n="52" num="4.4"><w n="52.1">Ces</w> <w n="52.2">éloquentes</w> <w n="52.3">d</w>’<w n="52.4">aujourd</w>’<w n="52.5">hui</w>,</l>
							</lg>
							<lg n="5">
								<l n="53" num="5.1"><w n="53.1">D</w>’<w n="53.2">un</w> <w n="53.3">excès</w> <w n="53.4">de</w> <w n="53.5">joie</w> <w n="53.6">oppressées</w>,</l>
								<l n="54" num="5.2"><w n="54.1">Elles</w> <w n="54.2">n</w>’<w n="54.3">assistent</w> <w n="54.4">sans</w> <w n="54.5">dédain</w>.</l>
								<l n="55" num="5.3"><w n="55.1">Des</w> <w n="55.2">pénombres</w> <w n="55.3">de</w> <w n="55.4">leurs</w> <w n="55.5">pensées</w>,</l>
								<l n="56" num="5.4"><w n="56.1">Qu</w>’<w n="56.2">à</w> <w n="56.3">des</w> <w n="56.4">fêtes</w> <w n="56.5">dans</w> <w n="56.6">du</w> <w n="56.7">lointain</w>.</l>
							</lg>
							<lg n="6">
								<l n="57" num="6.1"><w n="57.1">Et</w> <w n="57.2">cet</w> <w n="57.3">effroi</w> <w n="57.4">des</w> <w n="57.5">clartés</w> <w n="57.6">vives</w> !</l>
								<l n="58" num="6.2"><w n="58.1">Pudeur</w> <w n="58.2">qui</w> <w n="58.3">détourne</w> <w n="58.4">du</w> <w n="58.5">bal</w></l>
								<l n="59" num="6.3"><w n="59.1">Ces</w> <w n="59.2">âmes</w> <w n="59.3">si</w> <w n="59.4">vraiment</w> <w n="59.5">pensives</w></l>
								<l n="60" num="6.4"><w n="60.1">Que</w> <w n="60.2">l</w>’<w n="60.3">allégresse</w> <w n="60.4">leur</w> <w n="60.5">sied</w> <w n="60.6">mal</w>.</l>
							</lg>
							<lg n="7">
								<l n="61" num="7.1"><w n="61.1">La</w> <w n="61.2">seule</w> <w n="61.3">force</w> <w n="61.4">du</w> <w n="61.5">sourire</w>,</l>
								<l n="62" num="7.2"><w n="62.1">Le</w> <w n="62.2">charme</w> <w n="62.3">enlisant</w> <w n="62.4">de</w> <w n="62.5">leur</w> <w n="62.6">voix</w></l>
								<l n="63" num="7.3"><w n="63.1">Sur</w> <w n="63.2">l</w>’<w n="63.3">enfance</w> <w n="63.4">étend</w> <w n="63.5">un</w> <w n="63.6">empire</w></l>
								<l n="64" num="7.4"><w n="64.1">Dont</w> <w n="64.2">le</w> <w n="64.3">sceptre</w> <w n="64.4">étonne</w> <w n="64.5">leurs</w> <w n="64.6">doigts</w></l>
							</lg>
							<lg n="8">
								<l n="65" num="8.1"><w n="65.1">Et</w> <w n="65.2">les</w> <w n="65.3">petits</w> <w n="65.4">se</w> <w n="65.5">reconnaissent</w></l>
								<l n="66" num="8.2"><w n="66.1">Dans</w> <w n="66.2">ces</w> <w n="66.3">humbles</w> <w n="66.4">et</w> <w n="66.5">grandes</w> <w n="66.6">sœurs</w></l>
								<l n="67" num="8.3"><w n="67.1">Sœurs</w> <w n="67.2">très</w> <w n="67.3">grandes</w>, <w n="67.4">mais</w> <w n="67.5">qui</w> <w n="67.6">se</w> <w n="67.7">baissent</w></l>
								<l n="68" num="8.4"><w n="68.1">Sœurs</w> <w n="68.2">très</w> <w n="68.3">humbles</w>, <w n="68.4">jusqu</w>’<w n="68.5">à</w> <w n="68.6">leurs</w> <w n="68.7">cœurs</w> !</l>
							</lg>
							<lg n="9">
								<l n="69" num="9.1"><w n="69.1">Dérobant</w> <w n="69.2">leur</w> <w n="69.3">tendresse</w> <w n="69.4">en</w> <w n="69.5">elle</w></l>
								<l n="70" num="9.2"><w n="70.1">Elles</w> <w n="70.2">grondent</w> <w n="70.3">les</w> <w n="70.4">chers</w> <w n="70.5">méchants</w> ;</l>
								<l n="71" num="9.3"><w n="71.1">Mais</w>, <w n="71.2">justicières</w> <w n="71.3">maternelles</w>,</l>
								<l n="72" num="9.4"><w n="72.1">Sont</w> <w n="72.2">douces</w> <w n="72.3">aux</w> <w n="72.4">remords</w> <w n="72.5">d</w>’<w n="72.6">enfants</w>.</l>
							</lg>
							<lg n="10">
								<l n="73" num="10.1"><w n="73.1">Comme</w> <w n="73.2">une</w> <w n="73.3">eau</w> <w n="73.4">bleue</w> <w n="73.5">où</w> <w n="73.6">vont</w> <w n="73.7">descendre</w></l>
								<l n="74" num="10.2"><w n="74.1">Les</w> <w n="74.2">ombres</w> <w n="74.3">précoces</w> <w n="74.4">des</w> <w n="74.5">soirs</w>,</l>
								<l n="75" num="10.3"><w n="75.1">Tels</w>, <w n="75.2">à</w> <w n="75.3">force</w> <w n="75.4">de</w> <w n="75.5">feinte</w> <w n="75.6">tendre</w>,</l>
								<l n="76" num="10.4"><w n="76.1">Les</w> <w n="76.2">yeux</w> <w n="76.3">clairs</w> <w n="76.4">se</w> <w n="76.5">font</w> <w n="76.6">presque</w> <w n="76.7">noirs</w> !…</l>
							</lg>
						</div>
					</div></body></text></TEI>