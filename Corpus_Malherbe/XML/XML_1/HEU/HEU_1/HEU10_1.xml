<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre puéril</head><head type="main_subpart">V</head><head type="main_subpart">Le vague de l’âme</head><div type="poem" key="HEU10">
						<head type="main">CRÉPUSCULE DANS LES BOIS</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">lueurs</w> <w n="1.3">s</w>’<w n="1.4">éteignaient</w> <w n="1.5">dans</w> <w n="1.6">les</w> <w n="1.7">ternes</w> <w n="1.8">rosées</w>…</l>
							<l n="2" num="1.2">« <w n="2.1">Chers</w> <w n="2.2">désirs</w>, <w n="2.3">vœux</w> <w n="2.4">secrets</w>, <w n="2.5">larmes</w> <w n="2.6">cristallisées</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Voici</w> <w n="3.2">discrètement</w>, <w n="3.3">autour</w> <w n="3.4">de</w> <w n="3.5">vous</w>, <w n="3.6">les</w> <w n="3.7">bois</w>…</l>
							<l n="4" num="1.4"><w n="4.1">Leur</w> <w n="4.2">nocturne</w> <w n="4.3">douceur</w> <w n="4.4">vous</w> <w n="4.5">parle</w> <w n="4.6">d</w>’<w n="4.7">autrefois</w> ;</l>
							<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">plane</w> <w n="5.3">autour</w> <w n="5.4">de</w> <w n="5.5">vous</w>, <w n="5.6">l</w>’<w n="5.7">adorable</w> <w n="5.8">présage</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">le</w> <w n="6.3">silence</w> <w n="6.4">affine</w> <w n="6.5">en</w> <w n="6.6">son</w> <w n="6.7">muet</w> <w n="6.8">langage</w></l>
							<l n="7" num="1.7"><w n="7.1">Dont</w> <w n="7.2">le</w> <w n="7.3">sens</w> <w n="7.4">ineffable</w> <w n="7.5">au</w> <w n="7.6">cœur</w> <w n="7.7">seul</w> <w n="7.8">se</w> <w n="7.9">traduit</w>,</l>
							<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">consolation</w> <w n="8.3">pensive</w> <w n="8.4">de</w> <w n="8.5">la</w> <w n="8.6">nuit</w>. »</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">La</w> <w n="9.2">jeunesse</w> <w n="9.3">de</w> <w n="9.4">l</w>’<w n="9.5">ombre</w> <w n="9.6">émerveillait</w> <w n="9.7">les</w> <w n="9.8">sentes</w>…</l>
							<l n="10" num="2.2"><w n="10.1">Quel</w> <w n="10.2">souvenir</w> <w n="10.3">épars</w> <w n="10.4">des</w> <w n="10.5">clartés</w> <w n="10.6">presque</w> <w n="10.7">absentes</w></l>
							<l n="11" num="2.3"><w n="11.1">Sur</w> <w n="11.2">les</w> <w n="11.3">lèvres</w> <w n="11.4">du</w> <w n="11.5">soir</w> <w n="11.6">faisait</w> <w n="11.7">errer</w> <w n="11.8">encor</w>,</l>
							<l n="12" num="2.4"><w n="12.1">Lumières</w> <w n="12.2">en</w> <w n="12.3">exil</w>, <w n="12.4">votre</w> <w n="12.5">sourire</w> <w n="12.6">d</w>’<w n="12.7">or</w> !</l>
							<l n="13" num="2.5"><w n="13.1">Un</w> <w n="13.2">clair</w>-<w n="13.3">obscur</w> <w n="13.4">divin</w> <w n="13.5">argentait</w> <w n="13.6">les</w> <w n="13.7">érables</w>,</l>
							<l n="14" num="2.6"><w n="14.1">Et</w> <w n="14.2">vous</w>, <w n="14.3">bruissement</w> <w n="14.4">des</w> <w n="14.5">forêts</w> <w n="14.6">vénérables</w>,</l>
							<l n="15" num="2.7"><w n="15.1">Voluptueux</w> <w n="15.2">appels</w> <w n="15.3">tombés</w> <w n="15.4">au</w> <w n="15.5">loin</w> <w n="15.6">d</w>’<w n="15.7">un</w> <w n="15.8">nid</w>,</l>
							<l n="16" num="2.8"><w n="16.1">Aile</w> <w n="16.2">immense</w> <w n="16.3">du</w> <w n="16.4">vent</w> <w n="16.5">flagellant</w> <w n="16.6">l</w>’<w n="16.7">infini</w>,</l>
							<l n="17" num="2.9"><w n="17.1">Des</w> <w n="17.2">fleuves</w> <w n="17.3">de</w> <w n="17.4">la</w> <w n="17.5">vie</w> <w n="17.6">enveloppant</w> <w n="17.7">murmure</w>,</l>
							<l n="18" num="2.10"><w n="18.1">Vos</w> <w n="18.2">rires</w> <w n="18.3">s</w>’<w n="18.4">exaltaient</w> <w n="18.5">dans</w> <w n="18.6">la</w> <w n="18.7">jeune</w> <w n="18.8">nature</w>,</l>
							<l n="19" num="2.11"><w n="19.1">Comme</w> <w n="19.2">frémit</w> <w n="19.3">sous</w> <w n="19.4">bois</w>, <w n="19.5">en</w> <w n="19.6">milliers</w> <w n="19.7">de</w> <w n="19.8">rumeurs</w>,</l>
							<l n="20" num="2.12"><w n="20.1">L</w>’<w n="20.2">avril</w> <w n="20.3">des</w> <w n="20.4">sources</w> <w n="20.5">d</w>’<w n="20.6">or</w> <w n="20.7">sur</w> <w n="20.8">les</w> <w n="20.9">cailloux</w> <w n="20.10">chanteurs</w> !</l>
						</lg>
					</div></body></text></TEI>