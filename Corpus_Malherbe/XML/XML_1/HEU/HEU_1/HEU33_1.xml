<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">III</head><head type="main_subpart">Prières à d’autres dieux</head><div type="poem" key="HEU33">
						<head type="sub_1">Dieux et symboles</head>
						<head type="sub_1">RÉSURRECTIONS</head>
						<head type="main">Les Chiennes d’Enfer</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Complice</w> <w n="1.2">à</w> <w n="1.3">demi</w> <w n="1.4">contre</w> <w n="1.5">l</w>’<w n="1.6">innocent</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Aux</w> <w n="2.2">placides</w> <w n="2.3">yeux</w> <w n="2.4">que</w> <w n="2.5">ferme</w> <w n="2.6">le</w> <w n="2.7">juge</w>,</l>
							<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2">ouvre</w> <w n="3.3">en</w> <w n="3.4">vain</w> <w n="3.5">partout</w> <w n="3.6">un</w> <w n="3.7">lâche</w> <w n="3.8">refuge</w></l>
							<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">soustraire</w> <w n="4.3">aux</w> <w n="4.4">dieux</w> <w n="4.5">le</w> <w n="4.6">crime</w> <w n="4.7">puissant</w>…</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">En</w> <w n="5.2">vain</w> <w n="5.3">le</w> <w n="5.4">méfait</w>, <w n="5.5">trop</w> <w n="5.6">sûr</w> <w n="5.7">d</w>’<w n="5.8">un</w> <w n="5.9">asile</w>.</l>
							<l n="6" num="2.2"><w n="6.1">Prévoyait</w> <w n="6.2">sans</w> <w n="6.3">peur</w> <w n="6.4">l</w>’<w n="6.5">éclair</w> <w n="6.6">irrité</w></l>
							<l n="7" num="2.3"><w n="7.1">Aboyant</w> <w n="7.2">au</w> <w n="7.3">loin</w> <w n="7.4">vers</w> <w n="7.5">l</w>’<w n="7.6">impunité</w> !…</l>
							<l n="8" num="2.4"><w n="8.1">Va</w>, <w n="8.2">musèle</w>, <w n="8.3">ô</w> <w n="8.4">Zeus</w>, <w n="8.5">ta</w> <w n="8.6">Meute</w> <w n="8.7">inutile</w></l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Notre</w> <w n="9.2">seule</w> <w n="9.3">odeur</w> <w n="9.4">chargerait</w> <w n="9.5">son</w> <w n="9.6">flair</w>…</l>
							<l n="10" num="3.2"><w n="10.1">Des</w> <w n="10.2">chiennes</w> <w n="10.3">d</w>’<w n="10.4">Hadès</w> <w n="10.5">la</w> <w n="10.6">mâchoire</w> <w n="10.7">est</w> <w n="10.8">bonne</w>…</l>
							<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">dents</w> <w n="11.3">du</w> <w n="11.4">remords</w>, <w n="11.5">triplant</w> <w n="11.6">Tisiphone</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Déjà</w> <w n="12.2">par</w> <w n="12.3">dedans</w> <w n="12.4">dévorent</w> <w n="12.5">la</w> <w n="12.6">chair</w> !</l>
						</lg>
					</div></body></text></TEI>