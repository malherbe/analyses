<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">I</head><head type="main_subpart">Défi</head><div type="poem" key="HEU21">
						<opener>
							<salute>A M. et Mme L. Rosy.</salute>
						</opener>
						<head type="main">SOIR DE TEMPÊTE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">paix</w> <w n="1.3">définitive</w> <w n="1.4">est</w> <w n="1.5">en</w> <w n="1.6">toi</w>… <w n="1.7">tu</w> <w n="1.8">le</w> <w n="1.9">sais</w>…</l>
							<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">la</w> <w n="2.3">démence</w> <w n="2.4">d</w>’<w n="2.5">or</w> <w n="2.6">des</w> <w n="2.7">éclairs</w> <w n="2.8">convulsés</w></l>
							<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">rompt</w> <w n="3.3">avec</w> <w n="3.4">des</w> <w n="3.5">poings</w> <w n="3.6">furieux</w>, <w n="3.7">sur</w> <w n="3.8">les</w> <w n="3.9">cimes</w>,</l>
							<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">Esprit</w> <w n="4.3">rôdeur</w> <w n="4.4">qui</w> <w n="4.5">bat</w> <w n="4.6">des</w> <w n="4.7">talons</w> <w n="4.8">les</w> <w n="4.9">abîmes</w> ;</l>
							<l n="5" num="1.5"><w n="5.1">Tout</w> <w n="5.2">ce</w> <w n="5.3">tumulte</w> <w n="5.4">où</w> <w n="5.5">ton</w> <w n="5.6">vieux</w> <w n="5.7">cœur</w> <w n="5.8">est</w> <w n="5.9">contenu</w>,</l>
							<l n="6" num="1.6"><w n="6.1">C</w>’<w n="6.2">est</w> <w n="6.3">loin</w> <w n="6.4">de</w> <w n="6.5">toi</w>, <w n="6.6">ce</w> <w n="6.7">soir</w>, <w n="6.8">là</w>-<w n="6.9">bas</w>, <w n="6.10">dans</w> <w n="6.11">l</w>’<w n="6.12">inconnu</w>,</l>
							<l n="7" num="1.7"><w n="7.1">Si</w> <w n="7.2">loin</w> <w n="7.3">de</w> <w n="7.4">toi</w> <w n="7.5">qui</w> <w n="7.6">t</w>’<w n="7.7">es</w> <w n="7.8">enfin</w> <w n="7.9">prouvé</w> <w n="7.10">ton</w> <w n="7.11">maître</w>,</l>
							<l n="8" num="1.8"><w n="8.1">Qu</w>’<w n="8.2">il</w> <w n="8.3">frappe</w> <w n="8.4">sa</w> <w n="8.5">victime</w> <w n="8.6">et</w> <w n="8.7">t</w>’<w n="8.8">épargne</w> <w n="8.9">de</w> <w n="8.10">l</w>’<w n="8.11">être</w>.</l>
							<l n="9" num="1.9"><w n="9.1">Non</w> ! <w n="9.2">tu</w> <w n="9.3">n</w>’<w n="9.4">es</w> <w n="9.5">plus</w> <w n="9.6">le</w> <w n="9.7">mont</w> <w n="9.8">livide</w> <w n="9.9">et</w> <w n="9.10">foudroyé</w></l>
							<l n="10" num="1.10"><w n="10.1">Qui</w> <w n="10.2">jette</w> <w n="10.3">à</w> <w n="10.4">la</w> <w n="10.5">dérive</w>, <w n="10.6">avec</w> <w n="10.7">l</w>’<w n="10.8">aigle</w> <w n="10.9">noyé</w>,</l>
							<l n="11" num="1.11"><w n="11.1">En</w> <w n="11.2">larges</w> <w n="11.3">pans</w>, <w n="11.4">dans</w> <w n="11.5">les</w> <w n="11.6">écumes</w> <w n="11.7">palpitantes</w>,</l>
							<l n="12" num="1.12"><w n="12.1">Ses</w> <w n="12.2">forêts</w> <w n="12.3">qu</w>’<w n="12.4">un</w> <w n="12.5">torrent</w> <w n="12.6">roule</w> <w n="12.7">encore</w> <w n="12.8">chantantes</w> !</l>
							<l n="13" num="1.13"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">loin</w>, <w n="13.4">là</w>-<w n="13.5">bas</w>, <w n="13.6">au</w> <w n="13.7">ras</w> <w n="13.8">d</w>’<w n="13.9">horizons</w> <w n="13.10">convulsifs</w>,</l>
							<l n="14" num="1.14"><w n="14.1">Que</w> <w n="14.2">des</w> <w n="14.3">bêtes</w> <w n="14.4">de</w> <w n="14.5">flamme</w> <w n="14.6">et</w> <w n="14.7">des</w> <w n="14.8">monstres</w> <w n="14.9">lascifs</w></l>
							<l n="15" num="1.15"><w n="15.1">Lappent</w> <w n="15.2">à</w> <w n="15.3">pleine</w> <w n="15.4">gueule</w> <w n="15.5">et</w> <w n="15.6">par</w> <w n="15.7">fauve</w> <w n="15.8">lampée</w>,</l>
							<l n="16" num="1.16"><w n="16.1">Le</w> <w n="16.2">sang</w> <w n="16.3">qui</w> <w n="16.4">teint</w> <w n="16.5">l</w>’<w n="16.6">éclair</w> <w n="16.7">comme</w> <w n="16.8">un</w> <w n="16.9">tranchant</w> <w n="16.10">d</w>’<w n="16.11">épée</w>.</l>
							<l n="17" num="1.17"><w n="17.1">L</w>’<w n="17.2">ombre</w> <w n="17.3">est</w> <w n="17.4">seule</w> <w n="17.5">à</w> <w n="17.6">saigner</w>, <w n="17.7">ton</w> <w n="17.8">âme</w> <w n="17.9">est</w> <w n="17.10">claire</w>… <w n="17.11">dors</w></l>
							<l n="18" num="1.18">— <w n="18.1">Sache</w>-<w n="18.2">les</w> <w n="18.3">bien</w> <w n="18.4">vivants</w>, <w n="18.5">ceux</w> <w n="18.6">que</w> <w n="18.7">tu</w> <w n="18.8">croyais</w> <w n="18.9">morts</w> :</l>
							<l n="19" num="1.19"><w n="19.1">Tes</w> <w n="19.2">rêves</w> <w n="19.3">qu</w>’<w n="19.4">un</w> <w n="19.5">réveil</w> <w n="19.6">parfumé</w> <w n="19.7">ressuscite</w>,</l>
							<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">là</w>, <w n="20.3">dans</w> <w n="20.4">la</w> <w n="20.5">lumière</w> <w n="20.6">en</w> <w n="20.7">fête</w> <w n="20.8">d</w>’<w n="20.9">un</w> <w n="20.10">beau</w> <w n="20.11">site</w>,</l>
							<l n="21" num="1.21"><w n="21.1">Comme</w> <w n="21.2">un</w> <w n="21.3">groupe</w> <w n="21.4">de</w> <w n="21.5">dieux</w> <w n="21.6">d</w>’<w n="21.7">un</w> <w n="21.8">exil</w> <w n="21.9">ressurgis</w>,</l>
							<l n="22" num="1.22"><w n="22.1">Les</w> <w n="22.2">graves</w> <w n="22.3">voluptés</w> <w n="22.4">de</w> <w n="22.5">tes</w> <w n="22.6">jours</w> <w n="22.7">assagis</w>.</l>
							<l n="23" num="1.23"><w n="23.1">Une</w> <w n="23.2">candeur</w> <w n="23.3">rit</w> <w n="23.4">dans</w> <w n="23.5">vos</w> <w n="23.6">yeux</w>, <w n="23.7">âme</w> <w n="23.8">souillée</w> !…</l>
							<l n="24" num="1.24">… <w n="24.1">Quand</w> <w n="24.2">sa</w> <w n="24.3">robe</w> <w n="24.4">de</w> <w n="24.5">stupre</w> <w n="24.6">est</w> <w n="24.7">enfin</w> <w n="24.8">dépouillée</w>,</l>
							<l n="25" num="1.25"><w n="25.1">Une</w> <w n="25.2">enfant</w> <w n="25.3">courtisane</w>, <w n="25.4">en</w> <w n="25.5">fleur</w> <w n="25.6">dans</w> <w n="25.7">son</w> <w n="25.8">printemps</w>,</l>
							<l n="26" num="1.26"><w n="26.1">Rayonne</w> <w n="26.2">encor</w> <w n="26.3">sous</w> <w n="26.4">la</w> <w n="26.5">chair</w> <w n="26.6">pâle</w> <w n="26.7">des</w> <w n="26.8">quinze</w> <w n="26.9">ans</w> ;</l>
							<l n="27" num="1.27"><w n="27.1">Elle</w> <w n="27.2">rentre</w> <w n="27.3">divine</w> <w n="27.4">en</w> <w n="27.5">cette</w> <w n="27.6">autre</w> <w n="27.7">innocence</w>…</l>
							<l n="28" num="1.28"><w n="28.1">Rejetez</w> <w n="28.2">quelque</w> <w n="28.3">jour</w> <w n="28.4">d</w>’<w n="28.5">un</w> <w n="28.6">clair</w> <w n="28.7">geste</w> <w n="28.8">d</w>’<w n="28.9">enfance</w>,</l>
							<l n="29" num="1.29"><w n="29.1">Les</w> <w n="29.2">vêtements</w> <w n="29.3">du</w> <w n="29.4">vice</w> <w n="29.5">obscur</w> <w n="29.6">que</w> <w n="29.7">vous</w> <w n="29.8">portez</w>,</l>
							<l n="30" num="1.30"><w n="30.1">Et</w> <w n="30.2">soyez</w>, <w n="30.3">âme</w> <w n="30.4">et</w> <w n="30.5">corps</w>, <w n="30.6">traversés</w> <w n="30.7">de</w> <w n="30.8">clartés</w> !</l>
						</lg>
						<lg n="2">
							<l n="31" num="2.1"><w n="31.1">C</w>’<w n="31.2">est</w> <w n="31.3">fait</w> !… <w n="31.4">Endormez</w>-<w n="31.5">vous</w> <w n="31.6">dans</w> <w n="31.7">une</w> <w n="31.8">eau</w> <w n="31.9">translucide</w>,</l>
							<l n="32" num="2.2"><w n="32.1">O</w> <w n="32.2">perle</w> <w n="32.3">où</w> <w n="32.4">des</w> <w n="32.5">éclairs</w> <w n="32.6">s</w>’<w n="32.7">ouvrent</w> <w n="32.8">un</w> <w n="32.9">ciel</w> <w n="32.10">limpide</w> !</l>
							<l n="33" num="2.3"><w n="33.1">Et</w> <w n="33.2">ne</w> <w n="33.3">redoutez</w> <w n="33.4">plus</w> <w n="33.5">que</w> <w n="33.6">ce</w> <w n="33.7">calme</w> <w n="33.8">pervers</w></l>
							<l n="34" num="2.4"><w n="34.1">Soit</w> <w n="34.2">l</w>’<w n="34.3">image</w> <w n="34.4">féline</w> <w n="34.5">et</w> <w n="34.6">traîtresse</w> <w n="34.7">des</w> <w n="34.8">mers</w>,</l>
							<l n="35" num="2.5"><w n="35.1">Où</w> <w n="35.2">derrière</w> <w n="35.3">la</w> <w n="35.4">flore</w> <w n="35.5">écumante</w> <w n="35.6">des</w> <w n="35.7">vagues</w>,</l>
							<l n="36" num="2.6"><w n="36.1">Joue</w> <w n="36.2">avant</w> <w n="36.3">de</w> <w n="36.4">rugir</w> <w n="36.5">la</w> <w n="36.6">houle</w>, <w n="36.7">Bêtes</w> <w n="36.8">vagues</w> !</l>
							<l n="37" num="2.7"><w n="37.1">La</w> <w n="37.2">paix</w> <w n="37.3">définitive</w> <w n="37.4">est</w> <w n="37.5">en</w> <w n="37.6">toi</w>…</l>
						</lg>
					</div></body></text></TEI>