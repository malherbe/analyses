<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">III</head><head type="main_subpart">Prières à d’autres dieux</head><div type="poem" key="HEU30">
						<head type="sub_1">Dieux et symboles</head>
						<head type="sub_1">RÉSURRECTIONS</head>
						<head type="main">Sirènes</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Sœurs</w> <w n="1.2">captives</w> <w n="1.3">dans</w> <w n="1.4">vos</w> <w n="1.5">élans</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Esclaves</w> <w n="2.2">en</w> <w n="2.3">proue</w> <w n="2.4">aux</w> <w n="2.5">carènes</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Loin</w> <w n="3.2">de</w> <w n="3.3">vous</w> <w n="3.4">de</w> <w n="3.5">libres</w> <w n="3.6">sirènes</w></l>
							<l n="4" num="1.4"><w n="4.1">Tressent</w> <w n="4.2">des</w> <w n="4.3">algues</w> <w n="4.4">à</w> <w n="4.5">leurs</w> <w n="4.6">flancs</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Craignez</w> <w n="5.2">le</w> <w n="5.3">destin</w> <w n="5.4">d</w>’<w n="5.5">être</w> <w n="5.6">femmes</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Vous</w> <w n="6.2">dont</w> <w n="6.3">l</w>’<w n="6.4">homme</w> <w n="6.5">d</w>’<w n="6.6">un</w> <w n="6.7">geste</w> <w n="6.8">dur</w></l>
							<l n="7" num="2.3"><w n="7.1">Émeut</w> <w n="7.2">d</w>’<w n="7.3">avance</w>, <w n="7.4">sous</w> <w n="7.5">l</w>’<w n="7.6">azur</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">sillage</w> <w n="8.3">à</w> <w n="8.4">travers</w> <w n="8.5">les</w> <w n="8.6">lames</w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">chant</w> <w n="9.3">des</w> <w n="9.4">flots</w> <w n="9.5">reste</w> <w n="9.6">en</w> <w n="9.7">ma</w> <w n="9.8">chair</w>…</l>
							<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2">onde</w> <w n="10.3">est</w> <w n="10.4">encor</w> <w n="10.5">toute</w> <w n="10.6">l</w>’<w n="10.7">Ondine</w> !</l>
							<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">si</w> <w n="11.3">ma</w> <w n="11.4">force</w> <w n="11.5">est</w> <w n="11.6">féminine</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Ma</w> <w n="12.2">grâce</w> <w n="12.3">est</w> <w n="12.4">d</w>’<w n="12.5">incarner</w> <w n="12.6">la</w> <w n="12.7">mer</w>…</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">suis</w> <w n="13.3">la</w> <w n="13.4">souplesse</w> <w n="13.5">des</w> <w n="13.6">vagues</w>…</l>
							<l n="14" num="4.2"><w n="14.1">Vénus</w>, <w n="14.2">Béthelgeuse</w> <w n="14.3">ou</w> <w n="14.4">Rigel</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Reflets</w> <w n="15.2">d</w>’<w n="15.3">astres</w> <w n="15.4">tombés</w> <w n="15.5">du</w> <w n="15.6">ciel</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Soyez</w> <w n="16.2">l</w>’<w n="16.3">opale</w> <w n="16.4">de</w> <w n="16.5">mes</w> <w n="16.6">bagues</w> !</l>
						</lg>
					</div></body></text></TEI>