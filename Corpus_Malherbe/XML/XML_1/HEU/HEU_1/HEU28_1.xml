<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’INITIATION DOULOUREUSE</title>
				<title type="sub">1er cahier</title>
				<title type="medium">Édition électronique</title>
				<author key="HEU">
					<name>
						<forename>Gaston</forename>
						<surname>HEUX</surname>
					</name>
					<date from="1879" to="1950">1879-1950</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2512 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">HEU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Initiation douloureuse — 1er cahier</title>
						<author>Gaston Heux</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k141655w/f1n269.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Initiation douloureuse — 1er cahier</title>
								<author>Gaston Heux</author>
								<idno type="URL">http://gallica.bnf.fr/ark :/12148/bpt6k10575703.r=%22max%20elskamp%22 ?rk=42918 ;4</idno>
								<imprint>
									<pubPlace>Paris — Bruxelles</pubPlace>
									<publisher>Éditions Gauloises</publisher>
									<date when="1924">1924</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1924">1924</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">Le livre viril</head><head type="main_subpart">III</head><head type="main_subpart">Prières à d’autres dieux</head><div type="poem" key="HEU28">
						<head type="sub_1">Dieux et symboles</head>
						<head type="sub_1">RÉSURRECTIONS</head>
						<head type="main">Endymion</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Reviens</w> <w n="1.2">vers</w> <w n="1.3">ton</w> <w n="1.4">pâtre</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Déesse</w> <w n="2.2">oublieuse</w>…</l>
							<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">lune</w> <w n="3.3">bleuâtre</w></l>
							<l n="4" num="1.4"><w n="4.1">Moire</w>, <w n="4.2">sous</w> <w n="4.3">l</w>’<w n="4.4">yeuse</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Les</w> <w n="5.2">rides</w> <w n="5.3">des</w> <w n="5.4">eaux</w>…</l>
						</lg>
						<lg n="2">
							<l n="6" num="2.1"><w n="6.1">J</w>’<w n="6.2">entends</w> <w n="6.3">les</w> <w n="6.4">roseaux</w>…</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1"><w n="7.1">Parfume</w> <w n="7.2">ma</w> <w n="7.3">couche</w></l>
							<l n="8" num="3.2"><w n="8.1">Des</w> <w n="8.2">chères</w> <w n="8.3">étreintes</w>…</l>
							<l n="9" num="3.3"><w n="9.1">Bientôt</w> <w n="9.2">cette</w> <w n="9.3">bouche</w></l>
							<l n="10" num="3.4"><w n="10.1">Qui</w> <w n="10.2">fut</w> <w n="10.3">toute</w> <w n="10.4">plaintes</w></l>
							<l n="11" num="3.5"><w n="11.1">Sera</w> <w n="11.2">toute</w> <w n="11.3">oiseaux</w> !</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">J</w>’<w n="12.2">entends</w> <w n="12.3">les</w> <w n="12.4">roseaux</w>…</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1"><w n="13.1">Du</w> <w n="13.2">fond</w> <w n="13.3">de</w> <w n="13.4">l</w>’<w n="13.5">attente</w>,</l>
							<l n="14" num="5.2"><w n="14.1">Espoir</w> <w n="14.2">de</w> <w n="14.3">ma</w> <w n="14.4">nuit</w>,</l>
							<l n="15" num="5.3"><w n="15.1">S</w>’<w n="15.2">élève</w> <w n="15.3">et</w> <w n="15.4">s</w>’<w n="15.5">argente</w></l>
							<l n="16" num="5.4"><w n="16.1">Ta</w> <w n="16.2">forme</w> <w n="16.3">qui</w> <w n="16.4">luit</w>…</l>
						</lg>
						<lg n="6">
							<l n="17" num="6.1"><w n="17.1">J</w>’<w n="17.2">entends</w> <w n="17.3">sur</w> <w n="17.4">les</w> <w n="17.5">eaux</w></l>
							<l n="18" num="6.2"><w n="18.1">Pleurer</w> <w n="18.2">les</w> <w n="18.3">roseaux</w>.</l>
						</lg>
					</div></body></text></TEI>