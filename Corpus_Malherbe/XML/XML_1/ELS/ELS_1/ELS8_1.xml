<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">PAYSAGES</head><div type="poem" key="ELS8">
					<head type="number">VII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Et</w> <w n="1.2">lors</w> <w n="1.3">en</w> <w n="1.4">gris</w>, <w n="1.5">et</w> <w n="1.6">lors</w> <w n="1.7">en</w> <w n="1.8">noir</w>,</l>
						<l n="2" num="1.2">— <w n="2.1">araignée</w> <w n="2.2">du</w> <w n="2.3">soir</w>, <w n="2.4">bon</w> <w n="2.5">espoir</w>, —</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1"><w n="3.1">fumez</w> <w n="3.2">les</w> <w n="3.3">toits</w> <w n="3.4">et</w>, <w n="3.5">sur</w> <w n="3.6">les</w> <w n="3.7">tables</w>,</l>
						<l n="4" num="2.2"><w n="4.1">les</w> <w n="4.2">mets</w>, <w n="4.3">aux</w> <w n="4.4">bouches</w>, <w n="4.5">délectables</w> ;</l>
					</lg>
					<lg n="3">
						<l n="5" num="3.1"><w n="5.1">et</w> <w n="5.2">lors</w> <w n="5.3">à</w> <w n="5.4">tous</w>, <w n="5.5">hommes</w> <w n="5.6">et</w> <w n="5.7">villes</w>,</l>
						<l n="6" num="3.2"><w n="6.1">baisers</w> <w n="6.2">donnés</w>, <w n="6.3">garçons</w> <w n="6.4">et</w> <w n="6.5">filles</w>,</l>
					</lg>
					<lg n="4">
						<l n="7" num="4.1"><w n="7.1">bonne</w> <w n="7.2">nuit</w> ! <w n="7.3">car</w>, <w n="7.4">à</w> <w n="7.5">tricots</w> <w n="7.6">chus</w>,</l>
						<l n="8" num="4.2"><w n="8.1">voici</w> <w n="8.2">déjà</w> <w n="8.3">qu</w>’<w n="8.4">on</w> <w n="8.5">n</w>’<w n="8.6">y</w> <w n="8.7">voit</w> <w n="8.8">plus</w></l>
					</lg>
					<lg n="5">
						<l n="9" num="5.1"><w n="9.1">et</w> <w n="9.2">que</w>, <w n="9.3">fil</w> <w n="9.4">aux</w> <w n="9.5">doigts</w> <w n="9.6">qui</w> <w n="9.7">se</w> <w n="9.8">lie</w>,</l>
						<l n="10" num="5.2"><w n="10.1">c</w>’<w n="10.2">est</w> <w n="10.3">sommeil</w> <w n="10.4">et</w> <w n="10.5">tâche</w> <w n="10.6">accomplie</w>.</l>
					</lg>
					<lg n="6">
						<l n="11" num="6.1"><w n="11.1">Or</w> <w n="11.2">baume</w> <w n="11.3">alors</w> <w n="11.4">comme</w> <w n="11.5">à</w> <w n="11.6">mains</w> <w n="11.7">pies</w>,</l>
						<l n="12" num="6.2"><w n="12.1">ceux</w> <w n="12.2">qui</w> <w n="12.3">pleurent</w> <w n="12.4">et</w> <w n="12.5">ceux</w> <w n="12.6">qui</w> <w n="12.7">prient</w>,</l>
					</lg>
					<lg n="7">
						<l n="13" num="7.1"><w n="13.1">et</w> <w n="13.2">paille</w> <w n="13.3">aux</w> <w n="13.4">bêtes</w>, <w n="13.5">lits</w> <w n="13.6">aux</w> <w n="13.7">gens</w></l>
						<l n="14" num="7.2"><w n="14.1">de</w> <w n="14.2">douceur</w> <w n="14.3">et</w> <w n="14.4">de</w> <w n="14.5">pansement</w>,</l>
					</lg>
					<lg n="8">
						<l n="15" num="8.1"><w n="15.1">bonne</w> <w n="15.2">nuit</w> ! <w n="15.3">les</w> <w n="15.4">hommes</w>, <w n="15.5">les</w> <w n="15.6">femmes</w>,</l>
						<l n="16" num="8.2"><w n="16.1">bras</w> <w n="16.2">en</w> <w n="16.3">croix</w> <w n="16.4">sur</w> <w n="16.5">le</w> <w n="16.6">cœur</w> <w n="16.7">ou</w> <w n="16.8">l</w>’<w n="16.9">âme</w>,</l>
					</lg>
					<lg n="9">
						<l n="17" num="9.1"><w n="17.1">et</w> <w n="17.2">rêve</w> <w n="17.3">aux</w> <w n="17.4">doigts</w> <w n="17.5">en</w> <w n="17.6">bleu</w> <w n="17.7">et</w> <w n="17.8">blanc</w></l>
						<l n="18" num="9.2"><w n="18.1">les</w> <w n="18.2">servantes</w> <w n="18.3">près</w> <w n="18.4">des</w> <w n="18.5">enfants</w> ;</l>
					</lg>
					<lg n="10">
						<l n="19" num="10.1"><w n="19.1">et</w> <w n="19.2">paix</w> <w n="19.3">alors</w> <w n="19.4">toute</w> <w n="19.5">la</w> <w n="19.6">vie</w> :</l>
						<l n="20" num="10.2"><w n="20.1">arbres</w>, <w n="20.2">moulins</w>, <w n="20.3">toits</w> <w n="20.4">et</w> <w n="20.5">prairies</w>,</l>
					</lg>
					<lg n="11">
						<l n="21" num="11.1"><w n="21.1">et</w> <w n="21.2">repos</w> <w n="21.3">alors</w> <w n="21.4">ceux</w> <w n="21.5">qui</w> <w n="21.6">peinent</w></l>
						<l n="22" num="11.2"><w n="22.1">au</w> <w n="22.2">doux</w> <w n="22.3">des</w> <w n="22.4">draps</w>, <w n="22.5">au</w> <w n="22.6">chaud</w> <w n="22.7">des</w> <w n="22.8">laines</w>,</l>
					</lg>
					<lg n="12">
						<l n="23" num="12.1"><w n="23.1">et</w> <w n="23.2">Christs</w> <w n="23.3">au</w> <w n="23.4">froid</w> <w n="23.5">que</w> <w n="23.6">l</w>’<w n="23.7">on</w> <w n="23.8">oublie</w>,</l>
						<l n="24" num="12.2"><w n="24.1">et</w> <w n="24.2">Madeleines</w> <w n="24.3">repenties</w>,</l>
					</lg>
					<lg n="13">
						<l n="25" num="13.1"><w n="25.1">et</w> <w n="25.2">Ciel</w> <w n="25.3">aussi</w> <w n="25.4">de</w> <w n="25.5">large</w> <w n="25.6">en</w> <w n="25.7">long</w></l>
						<l n="26" num="13.2"><w n="26.1">aux</w> <w n="26.2">quatre</w> <w n="26.3">coins</w> <w n="26.4">des</w> <w n="26.5">horizons</w>.</l>
					</lg>
				</div></body></text></TEI>