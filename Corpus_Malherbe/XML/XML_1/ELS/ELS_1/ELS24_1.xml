<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Enluminures</title>
				<title type="medium">Édition électronique</title>
				<author key="ELS">
					<name>
						<forename>Max</forename>
						<surname>ELSKAMP</surname>
					</name>
					<date from="1862" to="1931">1862-1931</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>718 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">ELS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Enluminures</title>
						<author>Max Elskamp</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/maxelskampenluminures.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Enluminures / paysages, heures, vies, chansons, grotesques</title>
						<author>Max Elskamp</author>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k10575703.r=%22max%20elskamp%22?rk=42918;4</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>Paul Lacomblez, Éditeur</publisher>
							<date when="1898">1898</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1898">1898</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les tirets simples ont été remplacés par des tirets demi-cadratins</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Il reste probablement de nombreuses erreurs de numérisation indétectables par le correcteur orthographique ; exemple : lu pour tu.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">CHANSONS</head><div type="poem" key="ELS24">
					<head type="number">VI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mais</w> <w n="1.2">lors</w> <w n="1.3">ma</w> <w n="1.4">joie</w> <w n="1.5">étant</w> <w n="1.6">Hollande</w>,</l>
						<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2">ai</w> <w n="2.3">bâti</w> <w n="2.4">du</w> <w n="2.5">côté</w> <w n="2.6">du</w> <w n="2.7">jour</w>,</l>
						<l n="3" num="1.3"><w n="3.1">et</w> <w n="3.2">dans</w> <w n="3.3">des</w> <w n="3.4">arbres</w> <w n="3.5">tout</w> <w n="3.6">d</w>’<w n="3.7">atours</w>,</l>
						<l n="4" num="1.4"><w n="4.1">ma</w> <w n="4.2">maison</w> <w n="4.3">qui</w> <w n="4.4">est</w> <w n="4.5">en</w> <w n="4.6">Hollande</w></l>
						<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">avec</w> <w n="5.2">la</w> <w n="5.3">mer</w> <w n="5.4">autour</w>,</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">et</w> <w n="6.2">mon</w> <w n="6.3">cœur</w> <w n="6.4">y</w> <w n="6.5">vit</w> <w n="6.6">sa</w> <w n="6.7">semaine</w></l>
						<l n="7" num="2.2"><w n="7.1">avec</w> <w n="7.2">sa</w> <w n="7.3">joie</w>, <w n="7.4">avec</w> <w n="7.5">sa</w> <w n="7.6">peine</w>,</l>
						<l n="8" num="2.3"><w n="8.1">et</w> <w n="8.2">Jean</w> <w n="8.3">qui</w> <w n="8.4">rit</w>, <w n="8.5">ou</w> <w n="8.6">Madeleine</w></l>
						<l n="9" num="2.4"><w n="9.1">mon</w> <w n="9.2">cœur</w> <w n="9.3">y</w> <w n="9.4">passe</w> <w n="9.5">la</w> <w n="9.6">semaine</w></l>
						<l n="10" num="2.5"><space unit="char" quantity="4"></space><w n="10.1">avec</w> <w n="10.2">la</w> <w n="10.3">mer</w> <w n="10.4">autour</w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">Or</w>, <w n="11.2">en</w> <w n="11.3">attendant</w> <w n="11.4">son</w> <w n="11.5">dimanche</w>,</l>
						<l n="12" num="3.2"><w n="12.1">mon</w> <w n="12.2">âme</w> <w n="12.3">est</w> <w n="12.4">là</w> <w n="12.5">comme</w> <w n="12.6">un</w> <w n="12.7">pêcheur</w></l>
						<l n="13" num="3.3"><w n="13.1">au</w> <w n="13.2">bord</w> <w n="13.3">de</w> <w n="13.4">l</w>’<w n="13.5">eau</w> <w n="13.6">et</w> <w n="13.7">sous</w> <w n="13.8">les</w> <w n="13.9">branches</w></l>
						<l n="14" num="3.4"><w n="14.1">à</w> <w n="14.2">causer</w> <w n="14.3">bas</w> <w n="14.4">avec</w> <w n="14.5">mon</w> <w n="14.6">cœur</w></l>
						<l n="15" num="3.5"><space unit="char" quantity="4"></space><w n="15.1">Près</w> <w n="15.2">de</w> <w n="15.3">la</w> <w n="15.4">mer</w> <w n="15.5">autour</w>,</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">d</w>’<w n="16.2">une</w> <w n="16.3">paix</w> <w n="16.4">dont</w> <w n="16.5">la</w> <w n="16.6">bonté</w> <w n="16.7">franche</w></l>
						<l n="17" num="4.2"><w n="17.1">serait</w> <w n="17.2">de</w> <w n="17.3">partager</w> <w n="17.4">d</w>’<w n="17.5">amour</w></l>
						<l n="18" num="4.3"><w n="18.1">toute</w> <w n="18.2">ma</w> <w n="18.3">vie</w> <w n="18.4">dont</w> <w n="18.5">c</w>’<w n="18.6">est</w> <w n="18.7">le</w> <w n="18.8">tour</w></l>
						<l n="19" num="4.4"><w n="19.1">de</w> <w n="19.2">mettre</w> <w n="19.3">enfin</w> <w n="19.4">sa</w> <w n="19.5">robe</w> <w n="19.6">blanche</w></l>
						<l n="20" num="4.5"><space unit="char" quantity="4"></space><w n="20.1">avec</w> <w n="20.2">la</w> <w n="20.3">mer</w> <w n="20.4">autour</w>,</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">car</w> <w n="21.2">tout</w> <w n="21.3">est</w> <w n="21.4">prêt</w>, <w n="21.5">jusqu</w>’<w n="21.6">à</w> <w n="21.7">moi</w>-<w n="21.8">même</w>,</l>
						<l n="22" num="5.2"><w n="22.1">dans</w> <w n="22.2">la</w> <w n="22.3">maison</w> <w n="22.4">de</w> <w n="22.5">bon</w> <w n="22.6">séjour</w></l>
						<l n="23" num="5.3"><w n="23.1">pour</w> <w n="23.2">le</w> <w n="23.3">bonheur</w> <w n="23.4">qui</w> <w n="23.5">vient</w> <w n="23.6">quand</w> <w n="23.7">même</w></l>
						<l n="24" num="5.4"><w n="24.1">quand</w> <w n="24.2">on</w> <w n="24.3">l</w>’<w n="24.4">attend</w>, <w n="24.5">celle</w> <w n="24.6">qu</w>’<w n="24.7">on</w> <w n="24.8">aime</w>,</l>
						<l n="25" num="5.5"><space unit="char" quantity="4"></space><w n="25.1">avec</w> <w n="25.2">la</w> <w n="25.3">mer</w> <w n="25.4">autour</w>.</l>
					</lg>
				</div></body></text></TEI>