<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RONDELS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>312 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_7</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RONDELS</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillerondels.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN317">
				<head type="number">II</head>
				<head type="main">La Nuit</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">bénissons</w> <w n="1.3">la</w> <w n="1.4">douce</w> <w n="1.5">Nuit</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">le</w> <w n="2.3">frais</w> <w n="2.4">baiser</w> <w n="2.5">nous</w> <w n="2.6">délivre</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Sous</w> <w n="3.2">ses</w> <w n="3.3">voiles</w> <w n="3.4">on</w> <w n="3.5">se</w> <w n="3.6">sent</w> <w n="3.7">vivre</w></l>
					<l n="4" num="1.4"><w n="4.1">Sans</w> <w n="4.2">inquiétude</w> <w n="4.3">et</w> <w n="4.4">sans</w> <w n="4.5">bruit</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">souci</w> <w n="5.3">dévorant</w> <w n="5.4">s</w>’<w n="5.5">enfuit</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">parfum</w> <w n="6.3">de</w> <w n="6.4">l</w>’<w n="6.5">air</w> <w n="6.6">nous</w> <w n="6.7">enivre</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Nous</w> <w n="7.2">bénissons</w> <w n="7.3">la</w> <w n="7.4">douce</w> <w n="7.5">Nuit</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Dont</w> <w n="8.2">le</w> <w n="8.3">frais</w> <w n="8.4">baiser</w> <w n="8.5">nous</w> <w n="8.6">délivre</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Pâle</w> <w n="9.2">songeur</w> <w n="9.3">qu</w>’<w n="9.4">un</w> <w n="9.5">Dieu</w> <w n="9.6">poursuit</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Repose</w>-<w n="10.2">toi</w>, <w n="10.3">ferme</w> <w n="10.4">ton</w> <w n="10.5">livre</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Dans</w> <w n="11.2">les</w> <w n="11.3">cieux</w> <w n="11.4">blancs</w> <w n="11.5">comme</w> <w n="11.6">du</w> <w n="11.7">givre</w></l>
					<l n="12" num="3.4"><w n="12.1">Un</w> <w n="12.2">flot</w> <w n="12.3">d</w>’<w n="12.4">astres</w> <w n="12.5">frissonne</w> <w n="12.6">et</w> <w n="12.7">luit</w>,</l>
					<l n="13" num="3.5"><w n="13.1">Nous</w> <w n="13.2">bénissons</w> <w n="13.3">la</w> <w n="13.4">douce</w> <w n="13.5">Nuit</w>.</l>
				</lg>
		</div></body></text></TEI>