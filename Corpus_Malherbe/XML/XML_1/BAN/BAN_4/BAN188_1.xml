<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">AMÉTHYSTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>319 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">AMÉTHYSTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleamethystes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1860-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN188">
				<head type="main">Printemps d’Avril</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ma</w> <w n="1.2">mie</w>, <w n="1.3">à</w> <w n="1.4">son</w> <w n="1.5">toit</w> <w n="1.6">fidèle</w>,</l>
					<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">frétillante</w> <w n="2.3">hirondelle</w></l>
					<l n="3" num="1.3"><w n="3.1">Revient</w> <w n="3.2">du</w> <w n="3.3">lointain</w> <w n="3.4">exil</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Déjà</w> <w n="4.2">le</w> <w n="4.3">long</w> <w n="4.4">des</w> <w n="4.5">rivages</w></l>
					<l n="5" num="1.5"><w n="5.1">S</w>’<w n="5.2">égaie</w> <w n="5.3">un</w> <w n="5.4">sylphe</w> <w n="5.5">subtil</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">baise</w> <w n="6.3">les</w> <w n="6.4">fleurs</w> <w n="6.5">sauvages</w> :</l>
					<l n="7" num="1.7"><w n="7.1">Voici</w> <w n="7.2">le</w> <w n="7.3">printemps</w> <w n="7.4">d</w>’<w n="7.5">Avril</w> !</l>
				</lg>
				<lg n="2">
					<l n="8" num="2.1"><w n="8.1">C</w>’<w n="8.2">est</w> <w n="8.3">le</w> <w n="8.4">moment</w> <w n="8.5">où</w> <w n="8.6">les</w> <w n="8.7">fées</w>,</l>
					<l n="9" num="2.2"><w n="9.1">De</w> <w n="9.2">volubilis</w> <w n="9.3">coiffées</w>,</l>
					<l n="10" num="2.3"><w n="10.1">Viennent</w>, <w n="10.2">au</w> <w n="10.3">matin</w> <w n="10.4">changeant</w>,</l>
					<l n="11" num="2.4"><w n="11.1">Sur</w> <w n="11.2">le</w> <w n="11.3">bord</w> <w n="11.4">vert</w> <w n="11.5">des</w> <w n="11.6">fontaines</w>,</l>
					<l n="12" num="2.5"><w n="12.1">Où</w> <w n="12.2">court</w> <w n="12.3">le</w> <w n="12.4">flot</w> <w n="12.5">diligent</w>,</l>
					<l n="13" num="2.6"><w n="13.1">Charmer</w> <w n="13.2">les</w> <w n="13.3">biches</w> <w n="13.4">hautaines</w></l>
					<l n="14" num="2.7"><w n="14.1">De</w> <w n="14.2">leurs</w> <w n="14.3">baguettes</w> <w n="14.4">d</w>’<w n="14.5">argent</w>.</l>
				</lg>
				<lg n="3">
					<l n="15" num="3.1"><w n="15.1">Elles</w> <w n="15.2">dansent</w> <w n="15.3">à</w> <w n="15.4">l</w>’<w n="15.5">aurore</w></l>
					<l n="16" num="3.2"><w n="16.1">Sur</w> <w n="16.2">l</w>’<w n="16.3">herbe</w>, <w n="16.4">où</w> <w n="16.5">les</w> <w n="16.6">suit</w> <w n="16.7">encore</w></l>
					<l n="17" num="3.3"><w n="17.1">Un</w> <w n="17.2">troupeau</w> <w n="17.3">de</w> <w n="17.4">nains</w> <w n="17.5">velus</w>.</l>
					<l n="18" num="3.4"><w n="18.1">Ne</w> <w n="18.2">va</w> <w n="18.3">pas</w>, <w n="18.4">enfant</w> <w n="18.5">sereine</w>,</l>
					<l n="19" num="3.5"><w n="19.1">Au</w> <w n="19.2">fond</w> <w n="19.3">des</w> <w n="19.4">bois</w> <w n="19.5">chevelus</w> ;</l>
					<l n="20" num="3.6"><w n="20.1">Elles</w> <w n="20.2">te</w> <w n="20.3">prendraient</w> <w n="20.4">pour</w> <w n="20.5">reine</w>,</l>
					<l n="21" num="3.7"><w n="21.1">Et</w> <w n="21.2">je</w> <w n="21.3">ne</w> <w n="21.4">te</w> <w n="21.5">verrais</w> <w n="21.6">plus</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1861">Avril 1860.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>