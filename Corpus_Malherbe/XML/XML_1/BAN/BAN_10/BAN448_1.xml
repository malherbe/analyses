<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN448">
				<head type="main">A Meaux, en Brie</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Avec</w> <w n="1.2">ses</w> <w n="1.3">cohortes</w> <w n="1.4">guerrières</w></l>
					<l n="2" num="1.2"><w n="2.1">Ayant</w> <w n="2.2">traversé</w> <w n="2.3">les</w> <w n="2.4">hameaux</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Après</w> <w n="3.2">avoir</w> <w n="3.3">quitté</w> <w n="3.4">Ferrières</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">bon</w> <w n="4.3">roi</w> <w n="4.4">Guillaume</w> <w n="4.5">est</w> <w n="4.6">à</w> <w n="4.7">Meaux</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Comme</w> <w n="5.2">il</w> <w n="5.3">chemine</w> <w n="5.4">vers</w> <w n="5.5">les</w> <w n="5.6">banques</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">le</w> <w n="6.3">but</w> <w n="6.4">de</w> <w n="6.5">les</w> <w n="6.6">prendre</w> <w n="6.7">en</w> <w n="6.8">flanc</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Sur</w> <w n="7.2">la</w> <w n="7.3">place</w>, <w n="7.4">des</w> <w n="7.5">saltimbanques</w></l>
					<l n="8" num="2.4"><w n="8.1">Regardent</w> <w n="8.2">le</w> <w n="8.3">monarque</w> <w n="8.4">blanc</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ces</w> <w n="9.2">gais</w> <w n="9.3">comédiens</w> <w n="9.4">en</w> <w n="9.5">fête</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Ces</w> <w n="10.2">Rachels</w> <w n="10.3">et</w> <w n="10.4">ces</w> <w n="10.5">Frédéricks</w></l>
					<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">rencontre</w>, <w n="11.3">dont</w> <w n="11.4">la</w> <w n="11.5">tempête</w></l>
					<l n="12" num="3.4"><w n="12.1">A</w> <w n="12.2">léché</w> <w n="12.3">les</w> <w n="12.4">pâles</w> <w n="12.5">carricks</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">Atala</w>, <w n="13.4">c</w>’<w n="13.5">est</w> <w n="13.6">Zéphirine</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Fleur</w> <w n="14.2">que</w> <w n="14.3">Sosthènes</w> <w n="14.4">invoquait</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">Gringalet</w>, <w n="15.3">que</w> <w n="15.4">tout</w> <w n="15.5">chagrine</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">leur</w> <w n="16.3">maître</w> <w n="16.4">à</w> <w n="16.5">tous</w>, <w n="16.6">Bilboquet</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Or</w>, <w n="17.2">dans</w> <w n="17.3">la</w> <w n="17.4">ville</w> <w n="17.5">de</w> <w n="17.6">province</w></l>
					<l n="18" num="5.2"><w n="18.1">Toute</w> <w n="18.2">noire</w> <w n="18.3">de</w> <w n="18.4">Bavarois</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Ils</w> <w n="19.2">se</w> <w n="19.3">dévisagent</w>, <w n="19.4">le</w> <w n="19.5">prince</w></l>
					<l n="20" num="5.4"><w n="20.1">Des</w> <w n="20.2">bouffons</w> <w n="20.3">et</w> <w n="20.4">le</w> <w n="20.5">roi</w> <w n="20.6">des</w> <w n="20.7">rois</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Tous</w> <w n="21.2">deux</w> <w n="21.3">sont</w> <w n="21.4">grands</w> <w n="21.5">et</w> <w n="21.6">font</w> <w n="21.7">campagne</w>.</l>
					<l n="22" num="6.2"><w n="22.1">Si</w> <w n="22.2">Guillaume</w>, <w n="22.3">le</w> <w n="22.4">pourfendeur</w>,</l>
					<l n="23" num="6.3"><w n="23.1">A</w> <w n="23.2">la</w> <w n="23.3">fureur</w> <w n="23.4">de</w> <w n="23.5">Charlemagne</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Bilboquet</w> <w n="24.2">en</w> <w n="24.3">a</w> <w n="24.4">la</w> <w n="24.5">splendeur</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Car</w> <w n="25.2">sur</w> <w n="25.3">son</w> <w n="25.4">dos</w> <w n="25.5">le</w> <w n="25.6">carrick</w> <w n="25.7">flotte</w> ;</l>
					<l n="26" num="7.2"><w n="26.1">Et</w>, <w n="26.2">flamboyant</w> <w n="26.3">devant</w> <w n="26.4">ses</w> <w n="26.5">pas</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Comme</w> <w n="27.2">il</w> <w n="27.3">s</w>’<w n="27.4">en</w> <w n="27.5">fit</w> <w n="27.6">une</w> <w n="27.7">culotte</w>,</l>
					<l n="28" num="7.4"><w n="28.1">La</w> <w n="28.2">pourpre</w> <w n="28.3">ne</w> <w n="28.4">l</w>’<w n="28.5">étonne</w> <w n="28.6">pas</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Le</w> <w n="29.2">grand</w> <w n="29.3">saltimbanque</w> <w n="29.4">fantasque</w></l>
					<l n="30" num="8.2"><w n="30.1">Voit</w> <w n="30.2">l</w>’<w n="30.3">aigle</w> <w n="30.4">de</w> <w n="30.5">cuivre</w> <w n="30.6">écrasé</w></l>
					<l n="31" num="8.3"><w n="31.1">Sur</w> <w n="31.2">le</w> <w n="31.3">cuir</w> <w n="31.4">miroitant</w> <w n="31.5">du</w> <w n="31.6">casque</w></l>
					<l n="32" num="8.4"><w n="32.1">Dont</w> <w n="32.2">se</w> <w n="32.3">coiffe</w> <w n="32.4">le</w> <w n="32.5">roi</w> <w n="32.6">rusé</w> ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Alors</w>, <w n="33.2">ôtant</w> <w n="33.3">son</w> <w n="33.4">feutre</w> <w n="33.5">glabre</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Que</w> <w n="34.2">chaque</w> <w n="34.3">ouragan</w> <w n="34.4">bossuait</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">qui</w> <w n="35.3">fut</w> <w n="35.4">fait</w> <w n="35.5">à</w> <w n="35.6">coups</w> <w n="35.7">de</w> <w n="35.8">sabre</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Il</w> <w n="36.2">dit</w> <w n="36.3">ces</w> <w n="36.4">mots</w> : <w n="36.5">O</w> <w n="36.6">Bossuet</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Chacun</w> <w n="37.2">à</w> <w n="37.3">sa</w> <w n="37.4">manière</w> <w n="37.5">dîne</w>.</l>
					<l n="38" num="10.2"><w n="38.1">Qu</w>’<w n="38.2">un</w> <w n="38.3">aiglon</w> <w n="38.4">soit</w> <w n="38.5">un</w> <w n="38.6">bon</w> <w n="38.7">régal</w></l>
					<l n="39" num="10.3"><w n="39.1">Étant</w> <w n="39.2">mis</w> <w n="39.3">à</w> <w n="39.4">la</w> <w n="39.5">crapaudine</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Je</w> <w n="40.2">le</w> <w n="40.3">veux</w> <w n="40.4">bien</w>. <w n="40.5">Mais</w> <w n="40.6">c</w>’<w n="40.7">est</w> <w n="40.8">égal</w>,</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">J</w>’<w n="41.2">admire</w>, <w n="41.3">en</w> <w n="41.4">riant</w> <w n="41.5">comme</w> <w n="41.6">un</w> <w n="41.7">faune</w></l>
					<l n="42" num="11.2"><w n="42.1">En</w> <w n="42.2">ce</w> <w n="42.3">monde</w> <w n="42.4">rempli</w> <w n="42.5">de</w> <w n="42.6">maux</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Qu</w>’<w n="43.2">un</w> <w n="43.3">tel</w> <w n="43.4">oiseau</w> <w n="43.5">de</w> <w n="43.6">cuivre</w> <w n="43.7">jaune</w></l>
					<l n="44" num="11.4"><w n="44.1">Soit</w> <w n="44.2">aujourd</w>’<w n="44.3">hui</w>… <w n="44.4">l</w>’<w n="44.5">aigle</w> <w n="44.6">de</w> <w n="44.7">Meaux</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Décembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>