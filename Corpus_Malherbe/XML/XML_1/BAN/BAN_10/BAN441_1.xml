<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN441">
				<head type="main">L’Âne</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">Âne</w>, <w n="1.3">aimé</w> <w n="1.4">de</w> <w n="1.5">Titania</w>,</l>
					<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2">a</w> <w n="2.3">qu</w>’<w n="2.4">un</w> <w n="2.5">seul</w> <w n="2.6">défaut</w>, <w n="2.7">tout</w> <w n="2.8">physique</w> :</l>
					<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">que</w> <w n="3.4">de</w> <w n="3.5">tout</w> <w n="3.6">temps</w> <w n="3.7">il</w> <w n="3.8">nia</w></l>
					<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">délices</w> <w n="4.3">de</w> <w n="4.4">la</w> <w n="4.5">musique</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">mange</w> <w n="5.3">les</w> <w n="5.4">chardons</w> <w n="5.5">qu</w>’<w n="5.6">il</w> <w n="5.7">voit</w>,</l>
					<l n="6" num="2.2"><w n="6.1">O</w> <w n="6.2">la</w> <w n="6.3">précieuse</w> <w n="6.4">nature</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Méprise</w> <w n="7.2">la</w> <w n="7.3">boue</w>, <w n="7.4">et</w> <w n="7.5">ne</w> <w n="7.6">boit</w></l>
					<l n="8" num="2.4"><w n="8.1">Que</w> <w n="8.2">dans</w> <w n="8.3">une</w> <w n="8.4">eau</w> <w n="8.5">splendide</w> <w n="8.6">et</w> <w n="8.7">pure</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Douce</w> <w n="9.2">monture</w> <w n="9.3">de</w> <w n="9.4">Jésus</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">est</w> <w n="10.3">tout</w> <w n="10.4">joyeux</w> <w n="10.5">le</w> <w n="10.6">dimanche</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Ses</w> <w n="11.2">chants</w> <w n="11.3">sont</w> <w n="11.4">un</w> <w n="11.5">peu</w> <w n="11.6">décousus</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Mais</w> <w n="12.2">il</w> <w n="12.3">porte</w> <w n="12.4">au</w> <w n="12.5">dos</w> <w n="12.6">la</w> <w n="12.7">croix</w> <w n="12.8">blanche</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Il</w> <w n="13.2">aime</w> <w n="13.3">ce</w> <w n="13.4">qui</w> <w n="13.5">nous</w> <w n="13.6">est</w> <w n="13.7">cher</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">ne</w> <w n="14.3">commet</w> <w n="14.4">point</w> <w n="14.5">de</w> <w n="14.6">rapines</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">Cependant</w> <w n="15.2">nous</w> <w n="15.3">trouons</w> <w n="15.4">sa</w> <w n="15.5">chair</w></l>
					<l n="16" num="4.4"><w n="16.1">Avec</w> <w n="16.2">les</w> <w n="16.3">durs</w> <w n="16.4">bâtons</w> <w n="16.5">d</w>’<w n="16.6">épines</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">quand</w> <w n="17.3">il</w> <w n="17.4">est</w> <w n="17.5">mort</w>, <w n="17.6">tous</w> <w n="17.7">les</w> <w n="17.8">jours</w></l>
					<l n="18" num="5.2"><w n="18.1">Pour</w> <w n="18.2">nos</w> <w n="18.3">concerts</w> <w n="18.4">et</w> <w n="18.5">pour</w> <w n="18.6">nos</w> <w n="18.7">luttes</w>,</l>
					<l n="19" num="5.3"><w n="19.1">On</w> <w n="19.2">fait</w> <w n="19.3">de</w> <w n="19.4">sa</w> <w n="19.5">peau</w> <w n="19.6">des</w> <w n="19.7">tambours</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">de</w> <w n="20.3">ses</w> <w n="20.4">tibias</w> <w n="20.5">des</w> <w n="20.6">flûtes</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Il</w> <w n="21.2">nous</w> <w n="21.3">croit</w> <w n="21.4">bons</w>, <w n="21.5">rêveur</w> <w n="21.6">charmant</w> !</l>
					<l n="22" num="6.2"><w n="22.1">Nous</w> <w n="22.2">flatte</w> <w n="22.3">de</w> <w n="22.4">sa</w> <w n="22.5">longue</w> <w n="22.6">queue</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">nous</w> <w n="23.3">regarde</w> <w n="23.4">tendrement</w></l>
					<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">sa</w> <w n="24.3">vague</w> <w n="24.4">prunelle</w> <w n="24.5">bleue</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Tant</w> <w n="25.2">de</w> <w n="25.3">haine</w> <w n="25.4">et</w> <w n="25.5">tant</w> <w n="25.6">de</w> <w n="25.7">fureur</w></l>
					<l n="26" num="7.2"><w n="26.1">N</w>’<w n="26.2">ont</w> <w n="26.3">pas</w> <w n="26.4">troublé</w> <w n="26.5">sa</w> <w n="26.6">douceur</w> <w n="26.7">d</w>’<w n="26.8">ange</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">le</w> <w n="27.3">laissaient</w> <w n="27.4">dans</w> <w n="27.5">son</w> <w n="27.6">erreur</w> :</l>
					<l n="28" num="7.4"><w n="28.1">A</w> <w n="28.2">présent</w> <w n="28.3">voici</w> <w n="28.4">qu</w>’<w n="28.5">on</w> <w n="28.6">le</w> <w n="28.7">mange</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">C</w>’<w n="29.2">est</w> <w n="29.3">que</w> <w n="29.4">Bismarck</w> <w n="29.5">et</w> <w n="29.6">les</w> <w n="29.7">destins</w></l>
					<l n="30" num="8.2"><w n="30.1">Sont</w> <w n="30.2">d</w>’<w n="30.3">une</w> <w n="30.4">humeur</w> <w n="30.5">capricieuse</w> !</l>
					<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">le</w> <w n="31.3">pauvre</w> <w n="31.4">être</w> <w n="31.5">à</w> <w n="31.6">nos</w> <w n="31.7">festins</w></l>
					<l n="32" num="8.4"><w n="32.1">Offre</w> <w n="32.2">une</w> <w n="32.3">chair</w> <w n="32.4">délicieuse</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Elle</w> <w n="33.2">a</w> <w n="33.3">conservé</w> <w n="33.4">le</w> <w n="33.5">parfum</w></l>
					<l n="34" num="9.2"><w n="34.1">Du</w> <w n="34.2">pré</w> <w n="34.3">fleurissant</w> <w n="34.4">qui</w> <w n="34.5">verdoie</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Et</w>, <w n="35.2">malgré</w> <w n="35.3">son</w> <w n="35.4">léger</w> <w n="35.5">ton</w> <w n="35.6">brun</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Sa</w> <w n="36.2">graisse</w> <w n="36.3">vaut</w> <w n="36.4">la</w> <w n="36.5">graisse</w> <w n="36.6">d</w>’<w n="36.7">oie</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Comme</w> <w n="37.2">lorsqu</w>’<w n="37.3">on</w> <w n="37.4">prend</w> <w n="37.5">des</w> <w n="37.6">galons</w></l>
					<l n="38" num="10.2"><w n="38.1">On</w> <w n="38.2">n</w>’<w n="38.3">en</w> <w n="38.4">saurait</w> <w n="38.5">jamais</w> <w n="38.6">trop</w> <w n="38.7">prendre</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Nous</w>, <w n="39.2">ingrats</w>, <w n="39.3">nous</w> <w n="39.4">nous</w> <w n="39.5">régalons</w></l>
					<l n="40" num="10.4"><w n="40.1">De</w> <w n="40.2">ce</w> <w n="40.3">manger</w> <w n="40.4">bizarre</w> <w n="40.5">et</w> <w n="40.6">tendre</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Âne</w>, <w n="41.2">qui</w> <w n="41.3">te</w> <w n="41.4">protégera</w> ?</l>
					<l n="42" num="11.2"><w n="42.1">Car</w>, <w n="42.2">je</w> <w n="42.3">le</w> <w n="42.4">dis</w>, <w n="42.5">quoiqu</w>’<w n="42.6">il</w> <w n="42.7">m</w>’<w n="42.8">en</w> <w n="42.9">coûte</w>,</l>
					<l n="43" num="11.3"><w n="43.1">A</w> <w n="43.2">l</w>’<w n="43.3">avenir</w> <w n="43.4">on</w> <w n="43.5">mangera</w></l>
					<l n="44" num="11.4"><w n="44.1">Toujours</w> <w n="44.2">des</w> <w n="44.3">ânes</w>, <w n="44.4">sans</w> <w n="44.5">nul</w> <w n="44.6">doute</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Pourtant</w> <w n="45.2">rassurez</w>-<w n="45.3">vous</w>, <w n="45.4">pédants</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Barnums</w>, <w n="46.2">cuistres</w>, <w n="46.3">faiseurs</w> <w n="46.4">de</w> <w n="46.5">banques</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Spadassins</w>, <w n="47.2">arracheurs</w> <w n="47.3">de</w> <w n="47.4">dents</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Pitres</w>, <w n="48.2">charlatans</w>, <w n="48.3">saltimbanques</w> !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Rassurez</w>-<w n="49.2">vous</w>, <w n="49.3">faux</w> <w n="49.4">avocats</w></l>
					<l n="50" num="13.2"><w n="50.1">Instruits</w> <w n="50.2">au</w> <w n="50.3">seul</w> <w n="50.4">talent</w> <w n="50.5">de</w> <w n="50.6">braire</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Et</w> <w n="51.2">toi</w>, <w n="51.3">rimeur</w>, <w n="51.4">qui</w> <w n="51.5">provoquas</w></l>
					<l n="52" num="13.4"><w n="52.1">Au</w> <w n="52.2">suicide</w> <w n="52.3">ton</w> <w n="52.4">libraire</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Vous</w> <w n="53.2">qu</w>’<w n="53.3">on</w> <w n="53.4">vit</w>, <w n="53.5">troupeau</w> <w n="53.6">révolté</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Prendre</w> <w n="54.2">pour</w> <w n="54.3">des</w> <w n="54.4">accords</w> <w n="54.5">de</w> <w n="54.6">lyre</w></l>
					<l n="55" num="14.3"><w n="55.1">Des</w> <w n="55.2">chants</w> <w n="55.3">de</w> <w n="55.4">Jocrisse</w> <w n="55.5">exalté</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Rassurez</w>-<w n="56.2">vous</w>, <w n="56.3">cols</w> <w n="56.4">en</w> <w n="56.5">délire</w> !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Oui</w>, <w n="57.2">rassurez</w>-<w n="57.3">vous</w>, <w n="57.4">manitous</w> !</l>
					<l n="58" num="15.2"><w n="58.1">Fabricants</w> <w n="58.2">de</w> <w n="58.3">vieux</w> <w n="58.4">vers</w> <w n="58.5">classiques</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Rassurez</w>-<w n="59.2">vous</w> ! <w n="59.3">Rassurez</w>-<w n="59.4">vous</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Paillasses</w>, <w n="60.2">Pierrots</w> <w n="60.3">et</w> <w n="60.4">Caciques</w> !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Et</w> <w n="61.2">toi</w>, <w n="61.3">vendeur</w> <w n="61.4">d</w>’<w n="61.5">orviétan</w></l>
					<l n="62" num="16.2"><w n="62.1">Qui</w> <w n="62.2">séduis</w> <w n="62.3">la</w> <w n="62.4">rouge</w> <w n="62.5">et</w> <w n="62.6">la</w> <w n="62.7">noire</w> !</l>
					<l n="63" num="16.3"><w n="63.1">Rassure</w>-<w n="63.2">toi</w>, <w n="63.3">beau</w> <w n="63.4">capitan</w></l>
					<l n="64" num="16.4"><w n="64.1">Que</w> <w n="64.2">l</w>’<w n="64.3">on</w> <w n="64.4">admirait</w> <w n="64.5">à</w> <w n="64.6">la</w> <w n="64.7">foire</w>,</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Et</w> <w n="65.2">tâchez</w> <w n="65.3">de</w> <w n="65.4">faire</w> <w n="65.5">tenir</w></l>
					<l n="66" num="17.2"><w n="66.1">Vos</w> <w n="66.2">anciens</w> <w n="66.3">plumets</w> <w n="66.4">sur</w> <w n="66.5">vos</w> <w n="66.6">crânes</w> ;</l>
					<l n="67" num="17.3"><w n="67.1">Jamais</w> <w n="67.2">nous</w> <w n="67.3">ne</w> <w n="67.4">pourrons</w> <w n="67.5">venir</w></l>
					<l n="68" num="17.4"><w n="68.1">A</w> <w n="68.2">bout</w> <w n="68.3">de</w> <w n="68.4">manger</w> <w n="68.5">tous</w> <w n="68.6">les</w> <w n="68.7">ânes</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Novembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>