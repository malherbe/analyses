<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN463">
				<head type="main">Un vieux Monarque</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">monarque</w> <w n="1.3">aux</w> <w n="1.4">favoris</w> <w n="1.5">blancs</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Turbulent</w>, <w n="2.2">ivrogne</w> <w n="2.3">et</w> <w n="2.4">féroce</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Affronte</w> <w n="3.2">les</w> <w n="3.3">passants</w> <w n="3.4">tremblants</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">gonfle</w> <w n="4.3">sa</w> <w n="4.4">poitrine</w> <w n="4.5">en</w> <w n="4.6">bosse</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">est</w> <w n="5.3">rouge</w> <w n="5.4">comme</w> <w n="5.5">du</w> <w n="5.6">vin</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Par</w> <w n="6.2">Bacchus</w> ! <w n="6.3">dit</w>-<w n="6.4">il</w>, <w n="6.5">on</w> <w n="6.6">me</w> <w n="6.7">brave</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Moi</w> <w n="7.2">le</w> <w n="7.3">héros</w>, <w n="7.4">l</w>’<w n="7.5">homme</w> <w n="7.6">divin</w> !</l>
					<l n="8" num="2.4"><w n="8.1">Moi</w> <w n="8.2">le</w> <w n="8.3">vainqueur</w> ! <w n="8.4">moi</w>, <w n="8.5">le</w> <w n="8.6">burgrave</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Moi</w> <w n="9.2">le</w> <w n="9.3">vieux</w> <w n="9.4">qui</w>, <w n="9.5">depuis</w> <w n="9.6">longtemps</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Ai</w> <w n="10.2">conquis</w>, <w n="10.3">montrant</w> <w n="10.4">ma</w> <w n="10.5">semelle</w>,</l>
					<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2">Europe</w> <w n="11.3">et</w> <w n="11.4">tous</w> <w n="11.5">ses</w> <w n="11.6">habitants</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">les</w> <w n="12.3">enfants</w> <w n="12.4">à</w> <w n="12.5">la</w> <w n="12.6">mamelle</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Moi</w> <w n="13.2">qui</w> <w n="13.3">puis</w> <w n="13.4">à</w> <w n="13.5">mon</w> <w n="13.6">gré</w> <w n="13.7">vêtir</w></l>
					<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">bleu</w> <w n="14.3">riant</w> <w n="14.4">que</w> <w n="14.5">chacun</w> <w n="14.6">flatte</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Ou</w> <w n="15.2">la</w> <w n="15.3">vieille</w> <w n="15.4">pourpre</w> <w n="15.5">de</w> <w n="15.6">Tyr</w>,</l>
					<l n="16" num="4.4"><w n="16.1">L</w>’<w n="16.2">azur</w> <w n="16.3">céleste</w> <w n="16.4">ou</w> <w n="16.5">l</w>’<w n="16.6">écarlate</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Voyez</w>, <w n="17.2">j</w>’<w n="17.3">ouvre</w> <w n="17.4">mon</w> <w n="17.5">calepin</w></l>
					<l n="18" num="5.2"><w n="18.1">Enjolivé</w> <w n="18.2">d</w>’<w n="18.3">or</w> <w n="18.4">et</w> <w n="18.5">de</w> <w n="18.6">nacre</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">Qui</w> <w n="19.2">veut</w> <w n="19.3">perdre</w> <w n="19.4">le</w> <w n="19.5">goût</w> <w n="19.6">du</w> <w n="19.7">pain</w> ?</l>
					<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">faudra</w>-<w n="20.3">t</w>-<w n="20.4">il</w> <w n="20.5">que</w> <w n="20.6">je</w> <w n="20.7">massacre</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Qui</w> <w n="21.2">donc</w> <w n="21.3">m</w>’<w n="21.4">a</w> <w n="21.5">causé</w> <w n="21.6">cet</w> <w n="21.7">ennui</w> ?</l>
					<l n="22" num="6.2"><w n="22.1">Son</w> <w n="22.2">destin</w> <w n="22.3">irrémédiable</w></l>
					<l n="23" num="6.3"><w n="23.1">Est</w> <w n="23.2">de</w> <w n="23.3">périr</w> <w n="23.4">dès</w> <w n="23.5">aujourd</w>’<w n="23.6">hui</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Je</w> <w n="24.2">le</w> <w n="24.3">tuerai</w>, <w n="24.4">fût</w>-<w n="24.5">ce</w> <w n="24.6">le</w> <w n="24.7">diable</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Or</w> <w n="25.2">savez</w>-<w n="25.3">vous</w> <w n="25.4">qui</w> <w n="25.5">parle</w> <w n="25.6">ainsi</w></l>
					<l n="26" num="7.2"><w n="26.1">D</w>’<w n="26.2">une</w> <w n="26.3">voix</w> <w n="26.4">rauque</w> <w n="26.5">et</w> <w n="26.6">solennelle</w></l>
					<l n="27" num="7.3"><w n="27.1">Qui</w> <w n="27.2">monte</w> <w n="27.3">parfois</w> <w n="27.4">jusqu</w>’<w n="27.5">au</w> <w n="27.6">si</w> ?</l>
					<l n="28" num="7.4"><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">le</w> <w n="28.4">seigneur</w> <w n="28.5">Polichinelle</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">S</w>’<w n="29.2">il</w> <w n="29.3">a</w> <w n="29.4">pris</w> <w n="29.5">cet</w> <w n="29.6">air</w> <w n="29.7">espagnol</w></l>
					<l n="30" num="8.2"><w n="30.1">De</w> <w n="30.2">fou</w> <w n="30.3">décrochant</w> <w n="30.4">une</w> <w n="30.5">étoile</w>,</l>
					<l n="31" num="8.3"><w n="31.1">C</w>’<w n="31.2">est</w> <w n="31.3">qu</w>’<w n="31.4">il</w> <w n="31.5">regrette</w> <w n="31.6">son</w> <w n="31.7">Guignol</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Son</w> <w n="32.2">palais</w>, <w n="32.3">sa</w> <w n="32.4">maison</w> <w n="32.5">de</w> <w n="32.6">toile</w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Dont</w> <w n="33.2">un</w> <w n="33.3">large</w> <w n="33.4">obus</w> <w n="33.5">éperdu</w></l>
					<l n="34" num="9.2"><w n="34.1">A</w> <w n="34.2">massacré</w> <w n="34.3">la</w> <w n="34.4">vieille</w> <w n="34.5">gloire</w>,</l>
					<l n="35" num="9.3"><w n="35.1">L</w>’<w n="35.2">autre</w> <w n="35.3">jour</w>, <w n="35.4">au</w> <w n="35.5">beau</w> <w n="35.6">milieu</w> <w n="35.7">du</w></l>
					<l n="36" num="9.4"><w n="36.1">Carrefour</w> <w n="36.2">de</w> <w n="36.3">l</w>’<w n="36.4">Observatoire</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>