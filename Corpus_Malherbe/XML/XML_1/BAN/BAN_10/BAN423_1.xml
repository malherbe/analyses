<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN423">
				<head type="main">La Lune</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Cependant, dans l’expansion de sa joie la <lb></lb>
								Lune remplissait toute la chambre comme une <lb></lb>
								atmosphère phosphorique…
							</quote>
							<bibl>
								<name>Charles Baudelaire</name>,<hi rend="ital">Poëmes en prose</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">les</w> <w n="1.3">sœurs</w> <w n="1.4">aux</w> <w n="1.5">fronts</w> <w n="1.6">étroits</w></l>
					<l n="2" num="1.2"><w n="2.1">Hurlant</w> <w n="2.2">leurs</w> <w n="2.3">chansons</w> <w n="2.4">meurtrières</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">on</w> <w n="3.3">voit</w> <w n="3.4">dans</w> <w n="3.5">Macbeth</w>, <w n="3.6">ils</w> <w n="3.7">sont</w> <w n="3.8">trois</w></l>
					<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">une</w> <w n="4.3">chambre</w> <w n="4.4">de</w> <w n="4.5">Ferrières</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Plus</w> <w n="5.2">ridé</w> <w n="5.3">que</w> <w n="5.4">la</w> <w n="5.5">vaste</w> <w n="5.6">mer</w>,</l>
					<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">Moltke</w> <w n="6.3">a</w> <w n="6.4">le</w> <w n="6.5">visage</w> <w n="6.6">glabre</w></l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">plisse</w> <w n="7.3">en</w> <w n="7.4">un</w> <w n="7.5">rictus</w> <w n="7.6">amer</w></l>
					<l n="8" num="2.4"><w n="8.1">Sa</w> <w n="8.2">bouche</w> <w n="8.3">ouverte</w> <w n="8.4">en</w> <w n="8.5">coup</w> <w n="8.6">de</w> <w n="8.7">sabre</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">ne</w> <w n="9.3">dit</w> <w n="9.4">rien</w>, <w n="9.5">mais</w> <w n="9.6">son</w> <w n="9.7">compas</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Qu</w>’<w n="10.2">il</w> <w n="10.3">rétrécit</w> <w n="10.4">ou</w> <w n="10.5">qu</w>’<w n="10.6">il</w> <w n="10.7">écarte</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Prend</w> <w n="11.2">des</w> <w n="11.3">villes</w>, <w n="11.4">et</w> <w n="11.5">pas</w> <w n="11.6">à</w> <w n="11.7">pas</w></l>
					<l n="12" num="3.4"><w n="12.1">Refait</w> <w n="12.2">l</w>’<w n="12.3">univers</w>, <w n="12.4">sur</w> <w n="12.5">la</w> <w n="12.6">carte</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">deux</w> <w n="13.3">autres</w> <w n="13.4">causent</w> ; <w n="13.5">Bismarck</w></l>
					<l n="14" num="4.2"><w n="14.1">Parle</w> <w n="14.2">avec</w> <w n="14.3">un</w> <w n="14.4">geste</w> <w n="14.5">d</w>’<w n="14.6">athlète</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">le</w> <w n="15.3">paysage</w> <w n="15.4">du</w> <w n="15.5">parc</w></l>
					<l n="16" num="4.4"><w n="16.1">Dans</w> <w n="16.2">son</w> <w n="16.3">crâne</w> <w n="16.4">blanc</w> <w n="16.5">se</w> <w n="16.6">reflète</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Guillaume</w> <w n="17.2">écoute</w>, <w n="17.3">sabre</w> <w n="17.4">au</w> <w n="17.5">flanc</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Pliant</w> <w n="18.2">d</w>’<w n="18.3">une</w> <w n="18.4">main</w> <w n="18.5">fantaisiste</w></l>
					<l n="19" num="5.3"><w n="19.1">Sa</w> <w n="19.2">moustache</w> <w n="19.3">de</w> <w n="19.4">tigre</w> <w n="19.5">blanc</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">se</w> <w n="20.3">hérisse</w> — <w n="20.4">et</w> <w n="20.5">lui</w> <w n="20.6">résiste</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Sire</w>, <w n="21.2">dit</w> <w n="21.3">Bismarck</w>, <w n="21.4">je</w> <w n="21.5">conquiers</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Après</w> <w n="22.2">la</w> <w n="22.3">France</w>, <w n="22.4">l</w>’<w n="22.5">Angleterre</w> ;</l>
					<l n="23" num="6.3"><w n="23.1">Puis</w> <w n="23.2">après</w>, <w n="23.3">je</w> <w n="23.4">vous</w> <w n="23.5">en</w> <w n="23.6">requiers</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Songeons</w> <w n="24.2">au</w> <w n="24.3">reste</w> <w n="24.4">de</w> <w n="24.5">la</w> <w n="24.6">terre</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">L</w>’<w n="25.2">Espagne</w>, <w n="25.3">l</w>’<w n="25.4">Italie</w> <w n="25.5">en</w> <w n="25.6">deuil</w></l>
					<l n="26" num="7.2"><w n="26.1">Et</w> <w n="26.2">la</w> <w n="26.3">Turquie</w> <w n="26.4">effarouchée</w></l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">la</w> <w n="27.3">Russie</w> <w n="27.4">ivre</w> <w n="27.5">d</w>’<w n="27.6">orgueil</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Nous</w> <w n="28.2">n</w>’<w n="28.3">en</w> <w n="28.4">ferons</w> <w n="28.5">qu</w>’<w n="28.6">une</w> <w n="28.7">bouchée</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Nous</w> <w n="29.2">les</w> <w n="29.3">aurons</w>, <w n="29.4">foi</w> <w n="29.5">de</w> <w n="29.6">Bismarck</w> !</l>
					<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">quant</w> <w n="30.3">à</w> <w n="30.4">vos</w> <w n="30.5">brumes</w>, <w n="30.6">Hollande</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Suède</w>, <w n="31.2">Norwège</w> <w n="31.3">et</w> <w n="31.4">Danemark</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Je</w> <w n="32.2">m</w>’<w n="32.3">en</w> <w n="32.4">fais</w> <w n="32.5">une</w> <w n="32.6">houppelande</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Grèce</w>, <w n="33.2">Afrique</w>, <w n="33.3">Hongrie</w> <w n="33.4">encor</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Nous</w> <w n="34.2">empochons</w> <w n="34.3">tout</w> ; <w n="34.4">quant</w> <w n="34.5">aux</w> <w n="34.6">Indes</w></l>
					<l n="35" num="9.3"><w n="35.1">Fleurissantes</w> <w n="35.2">sous</w> <w n="35.3">leur</w> <w n="35.4">ciel</w> <w n="35.5">d</w>’<w n="35.6">or</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Nous</w> <w n="36.2">en</w> <w n="36.3">ferons</w> <w n="36.4">nos</w> <w n="36.5">Rosalindes</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">parlons</w> <w n="37.3">net</w>, <w n="37.4">je</w> <w n="37.5">ne</w> <w n="37.6">crois</w> <w n="37.7">pas</w></l>
					<l n="38" num="10.2"><w n="38.1">Former</w> <w n="38.2">des</w> <w n="38.3">espoirs</w> <w n="38.4">chimériques</w></l>
					<l n="39" num="10.3"><w n="39.1">Si</w> <w n="39.2">je</w> <w n="39.3">compte</w> <w n="39.4">réduire</w> <w n="39.5">au</w> <w n="39.6">pas</w></l>
					<l n="40" num="10.4"><w n="40.1">L</w>’<w n="40.2">Asie</w> <w n="40.3">et</w> <w n="40.4">les</w> <w n="40.5">deux</w> <w n="40.6">Amériques</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">L</w>’<w n="41.2">univers</w> <w n="41.3">ainsi</w> <w n="41.4">dévasté</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Sur</w> <w n="42.2">son</w> <w n="42.3">cheval</w> <w n="42.4">d</w>’<w n="42.5">apothéose</w></l>
					<l n="43" num="11.3"><w n="43.1">Que</w> <w n="43.2">fera</w> <w n="43.3">Votre</w> <w n="43.4">Majesté</w> ?</l>
					<l n="44" num="11.4"><w n="44.1">Je</w> <w n="44.2">crois</w> <w n="44.3">bon</w> <w n="44.4">qu</w>’<w n="44.5">Elle</w> <w n="44.6">se</w> <w n="44.7">repose</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Oui</w>, <w n="45.2">nous</w> <w n="45.3">rentrerons</w>, <w n="45.4">Vous</w> <w n="45.5">et</w> <w n="45.6">moi</w>.</l>
					<l n="46" num="12.2"><w n="46.1">Faire</w> <w n="46.2">un</w> <w n="46.3">Prussien</w> <w n="46.4">de</w> <w n="46.5">chaque</w> <w n="46.6">homme</w></l>
					<l n="47" num="12.3"><w n="47.1">Vivant</w>, <w n="47.2">cela</w> <w n="47.3">suffit</w>, <w n="47.4">ô</w> <w n="47.5">Roi</w> !</l>
					<l n="48" num="12.4"><w n="48.1">Quelles</w> <w n="48.2">que</w> <w n="48.3">puissent</w> <w n="48.4">être</w>, <w n="48.5">en</w> <w n="48.6">somme</w>,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">notre</w> <w n="49.3">soif</w> <w n="49.4">et</w> <w n="49.5">notre</w> <w n="49.6">faim</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Tout</w> <w n="50.2">boire</w> <w n="50.3">et</w> <w n="50.4">manger</w> <w n="50.5">guérit</w> <w n="50.6">l</w>’<w n="50.7">une</w></l>
					<l n="51" num="13.3"><w n="51.1">Ainsi</w> <w n="51.2">que</w> <w n="51.3">l</w>’<w n="51.4">autre</w> ; <w n="51.5">puis</w> <w n="51.6">enfin</w></l>
					<l n="52" num="13.4"><w n="52.1">On</w> <w n="52.2">ne</w> <w n="52.3">peut</w> <w n="52.4">pas</w> <w n="52.5">prendre</w> <w n="52.6">la</w> <w n="52.7">lune</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Quiconque</w> <w n="53.2">s</w>’<w n="53.3">en</w> <w n="53.4">serait</w> <w n="53.5">chargé</w></l>
					<l n="54" num="14.2"><w n="54.1">Risquerait</w> <w n="54.2">fort</w> <w n="54.3">à</w> <w n="54.4">l</w>’<w n="54.5">entreprendre</w>.</l>
					<l n="55" num="14.3"><w n="55.1">Si</w>, <w n="55.2">dit</w> <w n="55.3">alors</w> <w n="55.4">de</w> <w n="55.5">Moltke</w>, <w n="55.6">j</w>’<w n="55.7">ai</w></l>
					<l n="56" num="14.4"><w n="56.1">Fait</w> <w n="56.2">mes</w> <w n="56.3">calculs</w> : <w n="56.4">on</w> <w n="56.5">peut</w> <w n="56.6">la</w> <w n="56.7">prendre</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Octobre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>