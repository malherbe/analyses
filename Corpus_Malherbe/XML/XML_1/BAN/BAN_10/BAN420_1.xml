<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN420">
				<head type="main">Un Prussien mort</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Couché</w> <w n="1.2">par</w> <w n="1.3">terre</w> <w n="1.4">dans</w> <w n="1.5">la</w> <w n="1.6">plaine</w></l>
					<l n="2" num="1.2"><w n="2.1">Sous</w> <w n="2.2">une</w> <w n="2.3">aigre</w> <w n="2.4">bise</w> <w n="2.5">du</w> <w n="2.6">nord</w></l>
					<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">le</w> <w n="3.3">fouettait</w> <w n="3.4">de</w> <w n="3.5">son</w> <w n="3.6">haleine</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">vîmes</w> <w n="4.3">un</w> <w n="4.4">Prussien</w> <w n="4.5">mort</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2">était</w> <w n="5.3">un</w> <w n="5.4">bel</w> <w n="5.5">enfant</w> <w n="5.6">imberbe</w>,</l>
					<l n="6" num="2.2"><w n="6.1">N</w>’<w n="6.2">ayant</w> <w n="6.3">pas</w> <w n="6.4">dix</w>-<w n="6.5">huit</w> <w n="6.6">ans</w> <w n="6.7">encor</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Une</w> <w n="7.2">chevelure</w> <w n="7.3">superbe</w></l>
					<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">parait</w> <w n="8.3">de</w> <w n="8.4">ses</w> <w n="8.5">anneaux</w> <w n="8.6">d</w>’<w n="8.7">or</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">sur</w> <w n="9.3">son</w> <w n="9.4">cou</w>, <w n="9.5">séchée</w> <w n="9.6">et</w> <w n="9.7">mate</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Faisant</w> <w n="10.2">ressortir</w> <w n="10.3">sa</w> <w n="10.4">pâleur</w>,</l>
					<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">large</w> <w n="11.3">blessure</w> <w n="11.4">écarlate</w></l>
					<l n="12" num="3.4"><w n="12.1">S</w>’<w n="12.2">ouvrait</w> <w n="12.3">comme</w> <w n="12.4">une</w> <w n="12.5">rouge</w> <w n="12.6">fleur</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Il</w> <w n="13.2">montrait</w> <w n="13.3">son</w> <w n="13.4">regard</w> <w n="13.5">sans</w> <w n="13.6">flamme</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Étendant</w> <w n="14.2">ses</w> <w n="14.3">bras</w> <w n="14.4">onduleux</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">l</w>’<w n="15.3">on</w> <w n="15.4">eût</w> <w n="15.5">dit</w> <w n="15.6">que</w> <w n="15.7">sa</w> <w n="15.8">jeune</w> <w n="15.9">âme</w></l>
					<l n="16" num="4.4"><w n="16.1">Errait</w> <w n="16.2">encor</w> <w n="16.3">dans</w> <w n="16.4">ses</w> <w n="16.5">yeux</w> <w n="16.6">bleus</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Il</w> <w n="17.2">dormait</w>, <w n="17.3">le</w> <w n="17.4">jeune</w> <w n="17.5">barbare</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Avec</w> <w n="18.2">un</w> <w n="18.3">doux</w> <w n="18.4">regard</w> <w n="18.5">ami</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">Un</w> <w n="19.2">volume</w> <w n="19.3">grec</w> <w n="19.4">de</w> <w n="19.5">Pindare</w></l>
					<l n="20" num="5.4"><w n="20.1">Sortait</w> <w n="20.2">de</w> <w n="20.3">sa</w> <w n="20.4">poche</w> <w n="20.5">à</w> <w n="20.6">demi</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">C</w>’<w n="21.2">était</w> <w n="21.3">un</w> <w n="21.4">poëte</w> <w n="21.5">peut</w>-<w n="21.6">être</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Divin</w> <w n="22.2">Orphée</w>, <w n="22.3">un</w> <w n="22.4">de</w> <w n="22.5">tes</w> <w n="22.6">fils</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Qui</w> <w n="23.2">pour</w> <w n="23.3">un</w> <w n="23.4">caprice</w> <w n="23.5">du</w> <w n="23.6">maître</w></l>
					<l n="24" num="6.4"><w n="24.1">Est</w> <w n="24.2">mort</w> <w n="24.3">là</w>, <w n="24.4">brisé</w> <w n="24.5">comme</w> <w n="24.6">un</w> <w n="24.7">lys</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Ah</w> ! <w n="25.2">sans</w> <w n="25.3">doute</w>, <w n="25.4">au</w> <w n="25.5">bord</w> <w n="25.6">de</w> <w n="25.7">la</w> <w n="25.8">Sprée</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Une</w> <w n="26.2">belle</w> <w n="26.3">enfant</w> <w n="26.4">de</w> <w n="26.5">seize</w> <w n="26.6">ans</w></l>
					<l n="27" num="7.3"><w n="27.1">A</w> <w n="27.2">la</w> <w n="27.3">chevelure</w> <w n="27.4">dorée</w></l>
					<l n="28" num="7.4"><w n="28.1">En</w> <w n="28.2">versera</w> <w n="28.3">des</w> <w n="28.4">pleurs</w> <w n="28.5">cuisants</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">toujours</w> <w n="29.3">parcourant</w> <w n="29.4">la</w> <w n="29.5">route</w></l>
					<l n="30" num="8.2"><w n="30.1">Qu</w>’<w n="30.2">il</w> <w n="30.3">suivait</w> <w n="30.4">en</w> <w n="30.5">venant</w> <w n="30.6">les</w> <w n="30.7">soirs</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Une</w> <w n="31.2">mère</w> <w n="31.3">de</w> <w n="31.4">plus</w> <w n="31.5">sans</w> <w n="31.6">doute</w></l>
					<l n="32" num="8.4"><w n="32.1">Portera</w> <w n="32.2">de</w> <w n="32.3">longs</w> <w n="32.4">voiles</w> <w n="32.5">noirs</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Il</w> <w n="33.2">est</w> <w n="33.3">parti</w> <w n="33.4">bien</w> <w n="33.5">avant</w> <w n="33.6">l</w>’<w n="33.7">heure</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Jeune</w> <w n="34.2">et</w> <w n="34.3">pur</w>, <w n="34.4">sans</w> <w n="34.5">avoir</w> <w n="34.6">pleuré</w>.</l>
					<l n="35" num="9.3"><w n="35.1">Pour</w> <w n="35.2">quel</w> <w n="35.3">crime</w> <w n="35.4">faut</w>-<w n="35.5">il</w> <w n="35.6">qu</w>’<w n="35.7">il</w> <w n="35.8">meure</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Cet</w> <w n="36.2">enfant</w> <w n="36.3">à</w> <w n="36.4">l</w>’<w n="36.5">œil</w> <w n="36.6">inspiré</w> ?</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Peut</w>-<w n="37.2">être</w> <w n="37.3">que</w> <w n="37.4">sa</w> <w n="37.5">mort</w> <w n="37.6">est</w> <w n="37.7">juste</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Et</w> <w n="38.2">ne</w> <w n="38.3">sera</w> <w n="38.4">qu</w>’<w n="38.5">un</w> <w n="38.6">accident</w></l>
					<l n="39" num="10.3"><w n="39.1">S</w>’<w n="39.2">il</w> <w n="39.3">se</w> <w n="39.4">peut</w> <w n="39.5">que</w> <w n="39.6">son</w> <w n="39.7">maître</w> <w n="39.8">auguste</w></l>
					<l n="40" num="10.4"><w n="40.1">Devienne</w> <w n="40.2">empereur</w> <w n="40.3">d</w>’<w n="40.4">Occident</w>,</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Et</w> <w n="41.2">qu</w>’<w n="41.3">en</w> <w n="41.4">sa</w> <w n="41.5">tragique</w> <w n="41.6">folie</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Monsieur</w> <w n="42.2">le</w> <w n="42.3">chancelier</w> <w n="42.4">Bismarck</w></l>
					<l n="43" num="11.3"><w n="43.1">Prenne</w> <w n="43.2">d</w>’<w n="43.3">une</w> <w n="43.4">main</w> <w n="43.5">l</w>’<w n="43.6">Italie</w></l>
					<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">de</w> <w n="44.3">l</w>’<w n="44.4">autre</w> <w n="44.5">le</w> <w n="44.6">Danemark</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Ah</w> ! <w n="45.2">Bismarck</w>, <w n="45.3">si</w> <w n="45.4">tu</w> <w n="45.5">continues</w>,</l>
					<l n="46" num="12.2"><w n="46.1">De</w> <w n="46.2">ces</w> <w n="46.3">beaux</w> <w n="46.4">enfants</w> <w n="46.5">chevelus</w></l>
					<l n="47" num="12.3"><w n="47.1">Aux</w> <w n="47.2">douces</w> <w n="47.3">lèvres</w> <w n="47.4">ingénues</w></l>
					<l n="48" num="12.4"><w n="48.1">Bientôt</w> <w n="48.2">il</w> <w n="48.3">n</w>’<w n="48.4">en</w> <w n="48.5">restera</w> <w n="48.6">plus</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Octobre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>