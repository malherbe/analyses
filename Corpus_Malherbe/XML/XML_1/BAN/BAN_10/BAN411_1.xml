<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN411">
				<head type="main">La Besace</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">air</w> <w n="1.3">est</w> <w n="1.4">lourd</w> <w n="1.5">et</w> <w n="1.6">le</w> <w n="1.7">soleil</w> <w n="1.8">fauve</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Dis</w>, <w n="2.2">que</w> <w n="2.3">veux</w>-<w n="2.4">tu</w>, <w n="2.5">bon</w> <w n="2.6">Allemand</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Pauvre</w> <w n="3.2">vieillard</w> <w n="3.3">au</w> <w n="3.4">crâne</w> <w n="3.5">chauve</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">t</w>’<w n="4.3">en</w> <w n="4.4">aller</w> <w n="4.5">tranquillement</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Que</w> <w n="5.2">faut</w>-<w n="5.3">il</w> <w n="5.4">mettre</w> <w n="5.5">en</w> <w n="5.6">ta</w> <w n="5.7">besace</w> ?</l>
					<l n="6" num="2.2"><w n="6.1">Âmes</w> <w n="6.2">secourables</w>, <w n="6.3">merci</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Mettez</w>-<w n="7.2">y</w>, <w n="7.3">s</w>’<w n="7.4">il</w> <w n="7.5">vous</w> <w n="7.6">plaît</w>, <w n="7.7">l</w>’<w n="7.8">Alsace</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">Mettez</w>-<w n="8.2">y</w> <w n="8.3">la</w> <w n="8.4">Lorraine</w> <w n="8.5">aussi</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Sur</w> <w n="9.2">votre</w> <w n="9.3">bonté</w> <w n="9.4">souveraine</w></l>
					<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">l</w>’<w n="10.3">amour</w> <w n="10.4">de</w> <w n="10.5">Dieu</w> <w n="10.6">j</w>’<w n="10.7">ai</w> <w n="10.8">compté</w> :</l>
					<l n="11" num="3.3"><w n="11.1">Dans</w> <w n="11.2">mon</w> <w n="11.3">sac</w> <w n="11.4">avec</w> <w n="11.5">la</w> <w n="11.6">Lorraine</w></l>
					<l n="12" num="3.4"><w n="12.1">Mettez</w>-<w n="12.2">moi</w> <w n="12.3">la</w> <w n="12.4">Franche</w>-<w n="12.5">Comté</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Messieurs</w>, <w n="13.2">à</w> <w n="13.3">Dieu</w> <w n="13.4">je</w> <w n="13.5">recommande</w></l>
					<l n="14" num="4.2"><w n="14.1">Votre</w> <w n="14.2">vendange</w> <w n="14.3">et</w> <w n="14.4">vos</w> <w n="14.5">moissons</w> !</l>
					<l n="15" num="4.3"><w n="15.1">Ma</w> <w n="15.2">besace</w> <w n="15.3">a</w> <w n="15.4">la</w> <w n="15.5">bouche</w> <w n="15.6">grande</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Mettez</w>-<w n="16.2">y</w>, <w n="16.3">s</w>’<w n="16.4">il</w> <w n="16.5">vous</w> <w n="16.6">plaît</w>, <w n="16.7">Soissons</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">vous</w> <w n="17.3">bénis</w>, <w n="17.4">que</w> <w n="17.5">Dieu</w> <w n="17.6">m</w>’<w n="17.7">entende</w> !</l>
					<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">je</w> <w n="18.3">ne</w> <w n="18.4">réclame</w> <w n="18.5">plus</w> <w n="18.6">rien</w></l>
					<l n="19" num="5.3">(<w n="19.1">Ma</w> <w n="19.2">besace</w> <w n="19.3">a</w> <w n="19.4">la</w> <w n="19.5">bouche</w> <w n="19.6">grande</w>)</l>
					<l n="20" num="5.4"><w n="20.1">Sinon</w> <w n="20.2">le</w> <w n="20.3">mont</w> <w n="20.4">Valérien</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Bon</w> <w n="21.2">vieillard</w> <w n="21.3">au</w> <w n="21.4">crâne</w> <w n="21.5">d</w>’<w n="21.6">ivoire</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Dont</w> <w n="22.2">les</w> <w n="22.3">jours</w> <w n="22.4">heureux</w> <w n="22.5">sont</w> <w n="22.6">passés</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Reste</w> <w n="23.2">ici</w> <w n="23.3">jusqu</w>’<w n="23.4">à</w> <w n="23.5">la</w> <w n="23.6">nuit</w> <w n="23.7">noire</w> :</l>
					<l n="24" num="6.4"><w n="24.1">Tu</w> <w n="24.2">ne</w> <w n="24.3">demandes</w> <w n="24.4">pas</w> <w n="24.5">assez</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Pour</w> <w n="25.2">apaiser</w> <w n="25.3">ta</w> <w n="25.4">faim</w> <w n="25.5">qui</w> <w n="25.6">raille</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Vieillard</w> <w n="26.2">chauve</w>, <w n="26.3">nous</w> <w n="26.4">te</w> <w n="26.5">donnons</w></l>
					<l n="27" num="7.3"><w n="27.1">Les</w> <w n="27.2">éclats</w> <w n="27.3">de</w> <w n="27.4">notre</w> <w n="27.5">mitraille</w></l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">les</w> <w n="28.3">boulets</w> <w n="28.4">de</w> <w n="28.5">nos</w> <w n="28.6">canons</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">le</w> <w n="29.3">sang</w> <w n="29.4">que</w> <w n="29.5">ton</w> <w n="29.6">cœur</w> <w n="29.7">préfère</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Vieillard</w>, <w n="30.2">et</w> <w n="30.3">nous</w> <w n="30.4">allons</w> <w n="30.5">t</w>’<w n="30.6">offrir</w></l>
					<l n="31" num="8.3"><w n="31.1">Les</w> <w n="31.2">prodiges</w> <w n="31.3">que</w> <w n="31.4">peuvent</w> <w n="31.5">faire</w></l>
					<l n="32" num="8.4"><w n="32.1">Tous</w> <w n="32.2">ceux</w> <w n="32.3">qui</w> <w n="32.4">veulent</w> <w n="32.5">bien</w> <w n="32.6">mourir</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Nous</w> <w n="33.2">t</w>’<w n="33.3">offrons</w> <w n="33.4">un</w> <w n="33.5">festin</w> <w n="33.6">sur</w> <w n="33.7">l</w>’<w n="33.8">herbe</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Où</w> <w n="34.2">devant</w> <w n="34.3">toi</w> <w n="34.4">dans</w> <w n="34.5">le</w> <w n="34.6">ravin</w></l>
					<l n="35" num="9.3"><w n="35.1">Le</w> <w n="35.2">sang</w> <w n="35.3">généreux</w> <w n="35.4">et</w> <w n="35.5">superbe</w></l>
					<l n="36" num="9.4"><w n="36.1">Ruissellera</w> <w n="36.2">comme</w> <w n="36.3">du</w> <w n="36.4">vin</w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Où</w> <w n="37.2">la</w> <w n="37.3">Mort</w>, <w n="37.4">ta</w> <w n="37.5">fidèle</w> <w n="37.6">amante</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Blanche</w> <w n="38.2">sous</w> <w n="38.3">le</w> <w n="38.4">casque</w> <w n="38.5">allemand</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Peut</w> <w n="39.2">remplir</w> <w n="39.3">sa</w> <w n="39.4">coupe</w> <w n="39.5">fumante</w></l>
					<l n="40" num="10.4"><w n="40.1">Et</w> <w n="40.2">se</w> <w n="40.3">soûler</w> <w n="40.4">hideusement</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Oui</w>, <w n="41.2">vous</w> <w n="41.3">pourrez</w> <w n="41.4">manger</w> <w n="41.5">et</w> <w n="41.6">boire</w></l>
					<l n="42" num="11.2"><w n="42.1">Et</w> <w n="42.2">laver</w> <w n="42.3">vous</w> <w n="42.4">bras</w> <w n="42.5">rafraîchis</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Toi</w>, <w n="43.2">vieillard</w> <w n="43.3">au</w> <w n="43.4">crâne</w> <w n="43.5">d</w>’<w n="43.6">ivoire</w></l>
					<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">ton</w> <w n="44.3">amante</w> <w n="44.4">aux</w> <w n="44.5">os</w> <w n="44.6">blanchis</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Devant</w> <w n="45.2">les</w> <w n="45.3">paroles</w> <w n="45.4">railleuses</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Paris</w> <w n="46.2">est</w> <w n="46.3">lent</w> <w n="46.4">à</w> <w n="46.5">s</w>’<w n="46.6">étonner</w> :</l>
					<l n="47" num="12.3"><w n="47.1">Écoute</w> <w n="47.2">un</w> <w n="47.3">peu</w> <w n="47.4">nos</w> <w n="47.5">mitrailleuses</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Ce</w> <w n="48.2">sont</w> <w n="48.3">elles</w> <w n="48.4">qui</w> <w n="48.5">vont</w> <w n="48.6">tonner</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Donc</w>, <w n="49.2">mange</w> <w n="49.3">à</w> <w n="49.4">ta</w> <w n="49.5">faim</w> ! <w n="49.6">Continue</w>.</l>
					<l n="50" num="13.2"><w n="50.1">Les</w> <w n="50.2">noirs</w> <w n="50.3">corbeaux</w> <w n="50.4">au</w> <w n="50.5">bec</w> <w n="50.6">durci</w></l>
					<l n="51" num="13.3"><w n="51.1">Qui</w> <w n="51.2">volent</w> <w n="51.3">en</w> <w n="51.4">haut</w> <w n="51.5">dans</w> <w n="51.6">la</w> <w n="51.7">nue</w></l>
					<l n="52" num="13.4"><w n="52.1">Prétendent</w> <w n="52.2">qu</w>’<w n="52.3">ils</w> <w n="52.4">ont</w> <w n="52.5">faim</w> <w n="52.6">aussi</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Octobre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>