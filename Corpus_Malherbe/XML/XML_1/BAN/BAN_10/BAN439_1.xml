<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN439">
				<head type="main">Orléans</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Blessé</w>, <w n="1.2">mourant</w>, <w n="1.3">traînant</w> <w n="1.4">son</w> <w n="1.5">aile</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">pauvre</w> <w n="2.3">pigeon</w> <w n="2.4">gris</w> <w n="2.5">et</w> <w n="2.6">blanc</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Apportant</w> <w n="3.2">la</w> <w n="3.3">bonne</w> <w n="3.4">nouvelle</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Est</w> <w n="4.2">arrivé</w>, <w n="4.3">taché</w> <w n="4.4">de</w> <w n="4.5">sang</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Donc</w>, <w n="5.2">ô</w> <w n="5.3">Victoire</w>, <w n="5.4">tu</w> <w n="5.5">te</w> <w n="5.6">lasses</w></l>
					<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">suivre</w> <w n="6.3">machinalement</w>,</l>
					<l n="7" num="2.3"><w n="7.1">En</w> <w n="7.2">rampant</w> <w n="7.3">dans</w> <w n="7.4">les</w> <w n="7.5">routes</w> <w n="7.6">basses</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">porte</w>-<w n="8.3">cuirasse</w> <w n="8.4">allemand</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Tu</w> <w n="9.2">ne</w> <w n="9.3">veux</w> <w n="9.4">plus</w>, <w n="9.5">sous</w> <w n="9.6">nos</w> <w n="9.7">huées</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Car</w>, <w n="10.2">même</w> <w n="10.3">en</w> <w n="10.4">tombant</w>, <w n="10.5">nous</w> <w n="10.6">raillons</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Ainsi</w> <w n="11.2">que</w> <w n="11.3">les</w> <w n="11.4">prostituées</w></l>
					<l n="12" num="3.4"><w n="12.1">Marcher</w> <w n="12.2">vers</w> <w n="12.3">les</w> <w n="12.4">gros</w> <w n="12.5">bataillons</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Tu</w> <w n="13.2">reviens</w> ! <w n="13.3">sois</w> <w n="13.4">la</w> <w n="13.5">bienvenue</w> !</l>
					<l n="14" num="4.2"><w n="14.1">Dans</w> <w n="14.2">les</w> <w n="14.3">rangs</w> <w n="14.4">d</w>’<w n="14.5">où</w> <w n="14.6">l</w>’<w n="14.7">on</w> <w n="14.8">t</w>’<w n="14.9">exila</w></l>
					<l n="15" num="4.3"><w n="15.1">Tu</w> <w n="15.2">ne</w> <w n="15.3">pouvais</w> <w n="15.4">être</w> <w n="15.5">inconnue</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Puisque</w> <w n="16.2">tes</w> <w n="16.3">amants</w> <w n="16.4">étaient</w> <w n="16.5">là</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Ce</w> <w n="17.2">vin</w> <w n="17.3">d</w>’<w n="17.4">espérance</w> <w n="17.5">et</w> <w n="17.6">de</w> <w n="17.7">fièvre</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Ce</w> <w n="18.2">noble</w>, <w n="18.3">ce</w> <w n="18.4">généreux</w> <w n="18.5">vin</w></l>
					<l n="19" num="5.3"><w n="19.1">Dans</w> <w n="19.2">lequel</w> <w n="19.3">tu</w> <w n="19.4">trompes</w> <w n="19.5">ta</w> <w n="19.6">lèvre</w></l>
					<l n="20" num="5.4"><w n="20.1">En</w> <w n="20.2">nous</w> <w n="20.3">offrant</w> <w n="20.4">son</w> <w n="20.5">flot</w> <w n="20.6">divin</w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Oui</w>, <w n="21.2">c</w>’<w n="21.3">est</w> <w n="21.4">chez</w> <w n="21.5">nous</w> <w n="21.6">qu</w>’<w n="21.7">on</w> <w n="21.8">le</w> <w n="21.9">savoure</w> !</l>
					<l n="22" num="6.2"><w n="22.1">Nos</w> <w n="22.2">fils</w>, <w n="22.3">dont</w> <w n="22.4">rien</w> <w n="22.5">ne</w> <w n="22.6">peut</w> <w n="22.7">briser</w></l>
					<l n="23" num="6.3"><w n="23.1">La</w> <w n="23.2">stoïque</w> <w n="23.3">et</w> <w n="23.4">mâle</w> <w n="23.5">bravoure</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Connaissent</w> <w n="24.2">ton</w> <w n="24.3">rouge</w> <w n="24.4">baiser</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Dans</w> <w n="25.2">leurs</w> <w n="25.3">maisons</w>, <w n="25.4">au</w> <w n="25.5">vent</w> <w n="25.6">flottantes</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Ils</w> <w n="26.2">savent</w> <w n="26.3">te</w> <w n="26.4">garder</w> <w n="26.5">pour</w> <w n="26.6">eux</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w>, <w n="27.2">lorsque</w> <w n="27.3">tu</w> <w n="27.4">quittes</w> <w n="27.5">leurs</w> <w n="27.6">tentes</w>,</l>
					<l n="28" num="7.4"><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">une</w> <w n="28.4">brouille</w> <w n="28.5">d</w>’<w n="28.6">amoureux</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Mais</w> <w n="29.2">te</w> <w n="29.3">voilà</w> ! <w n="29.4">C</w>’<w n="29.5">était</w> <w n="29.6">un</w> <w n="29.7">rêve</w>.</l>
					<l n="30" num="8.2"><w n="30.1">Regarde</w>-<w n="30.2">nous</w> <w n="30.3">de</w> <w n="30.4">tes</w> <w n="30.5">yeux</w> <w n="30.6">clairs</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Chère</w> <w n="31.2">infidèle</w>, <w n="31.3">dont</w> <w n="31.4">le</w> <w n="31.5">glaive</w></l>
					<l n="32" num="8.4"><w n="32.1">Met</w> <w n="32.2">sur</w> <w n="32.3">nos</w> <w n="32.4">têtes</w> <w n="32.5">des</w> <w n="32.6">éclairs</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">brisons</w> <w n="33.3">leurs</w> <w n="33.4">fourches</w> <w n="33.5">caudines</w> !</l>
					<l n="34" num="9.2"><w n="34.1">Là</w>-<w n="34.2">bas</w>, <w n="34.3">son</w> <w n="34.4">épée</w> <w n="34.5">à</w> <w n="34.6">la</w> <w n="34.7">main</w>,</l>
					<l n="35" num="9.3"><w n="35.1">C</w>’<w n="35.2">est</w> <w n="35.3">Aurelles</w> <w n="35.4">de</w> <w n="35.5">Paladines</w></l>
					<l n="36" num="9.4"><w n="36.1">Qui</w> <w n="36.2">t</w>’<w n="36.3">emportait</w> <w n="36.4">dans</w> <w n="36.5">son</w> <w n="36.6">chemin</w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Et</w>, <w n="37.2">recommençant</w> <w n="37.3">notre</w> <w n="37.4">histoire</w></l>
					<l n="38" num="10.2"><w n="38.1">Dans</w> <w n="38.2">un</w> <w n="38.3">long</w> <w n="38.4">combat</w> <w n="38.5">de</w> <w n="38.6">géants</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Hurrah</w> ! <w n="39.2">nos</w> <w n="39.3">soldats</w> <w n="39.4">de</w> <w n="39.5">la</w> <w n="39.6">Loire</w></l>
					<l n="40" num="10.4"><w n="40.1">Ont</w>, <w n="40.2">en</w> <w n="40.3">deux</w> <w n="40.4">jours</w>, <w n="40.5">pris</w> <w n="40.6">Orléans</w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Orléans</w> ! <w n="41.2">c</w>’<w n="41.3">est</w> <w n="41.4">là</w> <w n="41.5">que</w> <w n="41.6">la</w> <w n="41.7">France</w></l>
					<l n="42" num="11.2"><w n="42.1">Trouve</w> <w n="42.2">son</w> <w n="42.3">plus</w> <w n="42.4">cher</w> <w n="42.5">souvenir</w> ;</l>
					<l n="43" num="11.3"><w n="43.1">C</w>’<w n="43.2">est</w> <w n="43.3">de</w> <w n="43.4">là</w> <w n="43.5">que</w> <w n="43.6">la</w> <w n="43.7">délivrance</w></l>
					<l n="44" num="11.4"><w n="44.1">Vers</w> <w n="44.2">nous</w> <w n="44.3">encor</w> <w n="44.4">devait</w> <w n="44.5">venir</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Là</w>, <w n="45.2">ce</w> <w n="45.3">flot</w> <w n="45.4">d</w>’<w n="45.5">azur</w> <w n="45.6">qui</w> <w n="45.7">s</w>’<w n="45.8">allume</w></l>
					<l n="46" num="12.2"><w n="46.1">Au</w> <w n="46.2">soleil</w>, <w n="46.3">ces</w> <w n="46.4">bois</w> <w n="46.5">et</w> <w n="46.6">le</w> <w n="46.7">val</w></l>
					<l n="47" num="12.3"><w n="47.1">Qui</w> <w n="47.2">la</w> <w n="47.3">vit</w> <w n="47.4">passer</w> <w n="47.5">dans</w> <w n="47.6">la</w> <w n="47.7">brume</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Flattant</w> <w n="48.2">de</w> <w n="48.3">la</w> <w n="48.4">main</w> <w n="48.5">son</w> <w n="48.6">cheval</w>,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Tout</w> <w n="49.2">nous</w> <w n="49.3">parle</w> <w n="49.4">de</w> <w n="49.5">la</w> <w n="49.6">guerrière</w> !</l>
					<l n="50" num="13.2"><w n="50.1">O</w> <w n="50.2">Prussien</w>, <w n="50.3">qui</w> <w n="50.4">t</w>’<w n="50.5">aveuglais</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Orléans</w> <w n="51.2">est</w> <w n="51.3">la</w> <w n="51.4">ville</w> <w n="51.5">fière</w></l>
					<l n="52" num="13.4"><w n="52.1">D</w>’<w n="52.2">où</w> <w n="52.3">Jeanne</w> <w n="52.4">a</w> <w n="52.5">chassé</w> <w n="52.6">les</w> <w n="52.7">Anglais</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Ah</w> ! <w n="53.2">sans</w> <w n="53.3">doute</w>, <w n="53.4">forte</w> <w n="53.5">et</w> <w n="53.6">sereine</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Dans</w> <w n="54.2">la</w> <w n="54.3">rue</w>, <w n="54.4">en</w> <w n="54.5">armure</w> <w n="54.6">d</w>’<w n="54.7">or</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Avec</w> <w n="55.2">nous</w> <w n="55.3">la</w> <w n="55.4">bonne</w> <w n="55.5">Lorraine</w></l>
					<l n="56" num="14.4"><w n="56.1">Combattait</w> <w n="56.2">cette</w> <w n="56.3">fois</w> <w n="56.4">encor</w> !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Elle</w> <w n="57.2">veille</w> <w n="57.3">sur</w> <w n="57.4">la</w> <w n="57.5">chaumière</w> !</l>
					<l n="58" num="15.2"><w n="58.1">Et</w> <w n="58.2">nos</w> <w n="58.3">ennemis</w>, <w n="58.4">en</w> <w n="58.5">fuyant</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Durent</w> <w n="59.2">entrevoir</w> <w n="59.3">la</w> <w n="59.4">lumière</w></l>
					<l n="60" num="15.4"><w n="60.1">De</w> <w n="60.2">son</w> <w n="60.3">doux</w> <w n="60.4">sourire</w> <w n="60.5">effrayant</w> !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Oui</w> ! <w n="61.2">sans</w> <w n="61.3">cesse</w>, <w n="61.4">ô</w> <w n="61.5">fatal</w> <w n="61.6">présage</w> !</l>
					<l n="62" num="16.2"><w n="62.1">Le</w> <w n="62.2">troupeau</w> <w n="62.3">mené</w> <w n="62.4">par</w> <w n="62.5">Bismarck</w></l>
					<l n="63" num="16.3"><w n="63.1">Rencontrera</w> <w n="63.2">sur</w> <w n="63.3">son</w> <w n="63.4">passage</w></l>
					<l n="64" num="16.4"><w n="64.1">La</w> <w n="64.2">figure</w> <w n="64.3">de</w> <w n="64.4">Jeanne</w> <w n="64.5">d</w>’<w n="64.6">Arc</w> !</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Et</w> <w n="65.2">si</w> <w n="65.3">son</w> <w n="65.4">nom</w> <w n="65.5">vient</w> <w n="65.6">sur</w> <w n="65.7">ma</w> <w n="65.8">bouche</w></l>
					<l n="66" num="17.2"><w n="66.1">Au</w> <w n="66.2">jour</w> <w n="66.3">éclatant</w> <w n="66.4">du</w> <w n="66.5">réveil</w>,</l>
					<l n="67" num="17.3"><w n="67.1">Lorsque</w> <w n="67.2">enfin</w> <w n="67.3">notre</w> <w n="67.4">honneur</w> <w n="67.5">farouche</w></l>
					<l n="68" num="17.4"><w n="68.1">Prend</w> <w n="68.2">sa</w> <w n="68.3">revanche</w> <w n="68.4">au</w> <w n="68.5">grand</w> <w n="68.6">soleil</w>,</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">C</w>’<w n="69.2">est</w> <w n="69.3">parce</w> <w n="69.4">que</w> <w n="69.5">jadis</w>, <w n="69.6">haïe</w></l>
					<l n="70" num="18.2"><w n="70.1">Des</w> <w n="70.2">traîtres</w> <w n="70.3">qui</w> <w n="70.4">sèment</w> <w n="70.5">l</w>’<w n="70.6">effroi</w>,</l>
					<l n="71" num="18.3"><w n="71.1">Elle</w> <w n="71.2">ne</w> <w n="71.3">tomba</w> <w n="71.4">que</w> <w n="71.5">trahie</w></l>
					<l n="72" num="18.4"><w n="72.1">Par</w> <w n="72.2">la</w> <w n="72.3">lâcheté</w> <w n="72.4">de</w> <w n="72.5">son</w> <w n="72.6">roi</w>.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Et</w> <w n="73.2">ce</w> <w n="73.3">qui</w> <w n="73.4">de</w> <w n="73.5">tout</w> <w n="73.6">temps</w> <w n="73.7">vers</w> <w n="73.8">elle</w></l>
					<l n="74" num="19.2"><w n="74.1">Ramène</w> <w n="74.2">mon</w> <w n="74.3">esprit</w> <w n="74.4">charmé</w>,</l>
					<l n="75" num="19.3"><w n="75.1">C</w>’<w n="75.2">est</w> <w n="75.3">que</w> <w n="75.4">pour</w> <w n="75.5">nous</w> <w n="75.6">cette</w> <w n="75.7">pucelle</w></l>
					<l n="76" num="19.4"><w n="76.1">Reste</w> <w n="76.2">la</w> <w n="76.3">foi</w> <w n="76.4">du</w> <w n="76.5">peuple</w> <w n="76.6">armé</w>,</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">Et</w> <w n="77.2">que</w> <w n="77.3">sa</w> <w n="77.4">vertu</w> <w n="77.5">d</w>’<w n="77.6">héroïne</w></l>
					<l n="78" num="20.2"><w n="78.1">Brûle</w> <w n="78.2">toujours</w>, <w n="78.3">malgré</w> <w n="78.4">les</w> <w n="78.5">ans</w>,</l>
					<l n="79" num="20.3"><w n="79.1">Flamme</w> <w n="79.2">inextinguible</w> <w n="79.3">et</w> <w n="79.4">divine</w></l>
					<l n="80" num="20.4"><w n="80.1">Dans</w> <w n="80.2">l</w>’<w n="80.3">âme</w> <w n="80.4">de</w> <w n="80.5">nos</w> <w n="80.6">paysans</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Novembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>