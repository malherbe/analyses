<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN459">
				<head type="main">Alsace</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Toute</w> <w n="1.2">désolée</w> <w n="1.3">et</w> <w n="1.4">meurtrie</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Notre</w> <w n="2.2">Alsace</w>, <w n="2.3">en</w> <w n="2.4">proie</w> <w n="2.5">aux</w> <w n="2.6">horreurs</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">son</w> <w n="3.3">sein</w> <w n="3.4">de</w> <w n="3.5">mère</w> <w n="3.6">patrie</w></l>
					<l n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">trouve</w> <w n="4.3">encor</w> <w n="4.4">des</w> <w n="4.5">francs</w>-<w n="4.6">tireurs</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Où</w> <w n="5.2">se</w> <w n="5.3">forment</w>-<w n="5.4">ils</w> ? <w n="5.5">On</w> <w n="5.6">l</w>’<w n="5.7">ignore</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Calmes</w> <w n="6.2">et</w> <w n="6.3">le</w> <w n="6.4">fusil</w> <w n="6.5">aux</w> <w n="6.6">doigts</w>,</l>
					<l n="7" num="2.3"><w n="7.1">On</w> <w n="7.2">les</w> <w n="7.3">voit</w> <w n="7.4">paraître</w> <w n="7.5">à</w> <w n="7.6">l</w>’<w n="7.7">aurore</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Devant</w> <w n="8.2">quelque</w> <w n="8.3">bouquet</w> <w n="8.4">de</w> <w n="8.5">bois</w></l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">D</w>’<w n="9.2">où</w> <w n="9.3">leur</w> <w n="9.4">troupe</w> <w n="9.5">au</w> <w n="9.6">combat</w> <w n="9.7">s</w>’<w n="9.8">élance</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Ou</w> <w n="10.2">bien</w> <w n="10.3">émerger</w> <w n="10.4">d</w>’<w n="10.5">un</w> <w n="10.6">rideau</w></l>
					<l n="11" num="3.3"><w n="11.1">D</w>’<w n="11.2">arbres</w> <w n="11.3">noirs</w>, <w n="11.4">ou</w> <w n="11.5">bien</w> <w n="11.6">en</w> <w n="11.7">silence</w></l>
					<l n="12" num="3.4"><w n="12.1">Suivre</w> <w n="12.2">quelque</w> <w n="12.3">petit</w> <w n="12.4">cours</w> <w n="12.5">d</w>’<w n="12.6">eau</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Leur</w> <w n="13.2">flot</w> <w n="13.3">se</w> <w n="13.4">masse</w> <w n="13.5">ou</w> <w n="13.6">s</w>’<w n="13.7">éparpille</w> ;</l>
					<l n="14" num="4.2"><w n="14.1">Harcelant</w>, <w n="14.2">pillant</w> <w n="14.3">les</w> <w n="14.4">convois</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Ils</w> <w n="15.2">fusillent</w>, <w n="15.3">on</w> <w n="15.4">les</w> <w n="15.5">fusille</w> ;</l>
					<l n="16" num="4.4"><w n="16.1">Ils</w> <w n="16.2">vont</w>, <w n="16.3">par</w> <w n="16.4">les</w> <w n="16.5">temps</w> <w n="16.6">les</w> <w n="16.7">plus</w> <w n="16.8">froids</w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Affrontant</w> <w n="17.2">la</w> <w n="17.3">neige</w> <w n="17.4">brûlante</w></l>
					<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">le</w> <w n="18.3">plomb</w> <w n="18.4">qui</w> <w n="18.5">siffle</w> <w n="18.6">à</w> <w n="18.7">l</w>’<w n="18.8">entour</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Embrasser</w> <w n="19.2">une</w> <w n="19.3">Mort</w> <w n="19.4">sanglante</w></l>
					<l n="20" num="5.4"><w n="20.1">Avec</w> <w n="20.2">de</w> <w n="20.3">grands</w> <w n="20.4">transports</w> <w n="20.5">d</w>’<w n="20.6">amour</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">en</w> <w n="21.3">vain</w> <w n="21.4">le</w> <w n="21.5">plomb</w> <w n="21.6">les</w> <w n="21.7">dévore</w> :</l>
					<l n="22" num="6.2"><w n="22.1">Exterminés</w>, <w n="22.2">ils</w> <w n="22.3">sont</w> <w n="22.4">vivants</w> ;</l>
					<l n="23" num="6.3"><w n="23.1">On</w> <w n="23.2">les</w> <w n="23.3">entend</w> <w n="23.4">crier</w> <w n="23.5">encore</w></l>
					<l n="24" num="6.4"><w n="24.1">Le</w> <w n="24.2">nom</w> <w n="24.3">de</w> <w n="24.4">France</w> <w n="24.5">aux</w> <w n="24.6">quatre</w> <w n="24.7">vents</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">l</w>’<w n="25.3">Alsace</w> <w n="25.4">française</w> <w n="25.5">admire</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Sur</w> <w n="26.2">son</w> <w n="26.3">vieux</w> <w n="26.4">sol</w> <w n="26.5">bouleversé</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Ces</w> <w n="27.2">enfants</w> <w n="27.3">au</w> <w n="27.4">hardi</w> <w n="27.5">sourire</w></l>
					<l n="28" num="7.4"><w n="28.1">Qui</w> <w n="28.2">renaissent</w> <w n="28.3">du</w> <w n="28.4">sang</w> <w n="28.5">versé</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1871">Janvier 1871.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>