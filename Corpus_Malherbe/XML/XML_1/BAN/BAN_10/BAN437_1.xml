<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN437">
				<head type="main">Le Cuisinier</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Bismarck</w> <w n="1.2">a</w> <w n="1.3">dit</w> : <w n="1.4">Pour</w> <w n="1.5">les</w> <w n="1.6">réduire</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Tous</w> <w n="2.2">ces</w> <w n="2.3">Parisiens</w> <w n="2.4">que</w> <w n="2.5">j</w>’<w n="2.6">eus</w></l>
					<l n="3" num="1.3"><w n="3.1">En</w> <w n="3.2">haine</w>, <w n="3.3">il</w> <w n="3.4">faut</w> <w n="3.5">les</w> <w n="3.6">laisser</w> <w n="3.7">cuire</w></l>
					<l n="4" num="1.4"><w n="4.1">Jusqu</w>’<w n="4.2">au</w> <w n="4.3">bon</w> <w n="4.4">moment</w>, <w n="4.5">dans</w> <w n="4.6">leur</w> <w n="4.7">jus</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">En</w> <w n="5.2">attendant</w> <w n="5.3">qu</w>’<w n="5.4">il</w> <w n="5.5">nous</w> <w n="5.6">perfore</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Notre</w> <w n="6.2">ennemi</w> <w n="6.3">pille</w> <w n="6.4">Varin</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Joue</w>, <w n="7.2">emprunte</w> <w n="7.3">sa</w> <w n="7.4">métaphore</w></l>
					<l n="8" num="2.4"><w n="8.1">A</w> <w n="8.2">l</w>’<w n="8.3">art</w> <w n="8.4">de</w> <w n="8.5">Brillat</w>-<w n="8.6">Savarin</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Se</w> <w n="9.2">fait</w> <w n="9.3">blanc</w> <w n="9.4">comme</w> <w n="9.5">une</w> <w n="9.6">avalanche</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">même</w>, <w n="10.3">d</w>’<w n="10.4">un</w> <w n="10.5">air</w> <w n="10.6">ingénu</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Décore</w> <w n="11.2">de</w> <w n="11.3">la</w> <w n="11.4">toque</w> <w n="11.5">blanche</w></l>
					<l n="12" num="3.4"><w n="12.1">Son</w> <w n="12.2">crâne</w>, <w n="12.3">ce</w> <w n="12.4">blanc</w> <w n="12.5">rocher</w> <w n="12.6">nu</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Donc</w> <w n="13.2">il</w> <w n="13.3">se</w> <w n="13.4">fait</w>, <w n="13.5">d</w>’<w n="13.6">un</w> <w n="13.7">cœur</w> <w n="13.8">tranquille</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Cuisinier</w>. <w n="14.2">Oui</w>. <w n="14.3">Pas</w> <w n="14.4">de</w> <w n="14.5">mot</w> <w n="14.6">vain</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Il</w> <w n="15.2">est</w> <w n="15.3">cuisinier</w>, — <w n="15.4">comme</w> <w n="15.5">Achille</w> !</l>
					<l n="16" num="4.4"><w n="16.1">Et</w>, <w n="16.2">comme</w> <w n="16.3">ce</w> <w n="16.4">boucher</w> <w n="16.5">divin</w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">S</w>’<w n="17.2">il</w> <w n="17.3">le</w> <w n="17.4">peut</w>, <w n="17.5">guerrier</w> <w n="17.6">magnanime</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Jetant</w> <w n="18.2">loin</w> <w n="18.3">de</w> <w n="18.4">lui</w> <w n="18.5">son</w> <w n="18.6">manteau</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Dans</w> <w n="19.2">la</w> <w n="19.3">gorge</w> <w n="19.4">de</w> <w n="19.5">la</w> <w n="19.6">victime</w></l>
					<l n="20" num="5.4"><w n="20.1">Il</w> <w n="20.2">enfoncera</w> <w n="20.3">le</w> <w n="20.4">couteau</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Il</w> <w n="21.2">veut</w>, <w n="21.3">ce</w> <w n="21.4">nouveau</w> <w n="21.5">Péliade</w></l>
					<l n="22" num="6.2"><w n="22.1">Choisi</w> <w n="22.2">pour</w> <w n="22.3">forger</w> <w n="22.4">les</w> <w n="22.5">destins</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Que</w> <w n="23.2">les</w> <w n="23.3">chants</w> <w n="23.4">de</w> <w n="23.5">son</w> <w n="23.6">Iliade</w></l>
					<l n="24" num="6.4"><w n="24.1">Soient</w> <w n="24.2">coupés</w> <w n="24.3">de</w> <w n="24.4">larges</w> <w n="24.5">festins</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Lorsque</w> <w n="25.2">sera</w> <w n="25.3">venu</w> <w n="25.4">le</w> <w n="25.5">terme</w></l>
					<l n="26" num="7.2"><w n="26.1">Déjà</w> <w n="26.2">fixé</w>, <w n="26.3">la</w> <w n="26.4">hache</w> <w n="26.5">au</w> <w n="26.6">flanc</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Il</w> <w n="27.2">portera</w> <w n="27.3">d</w>’<w n="27.4">une</w> <w n="27.5">main</w> <w n="27.6">ferme</w></l>
					<l n="28" num="7.4"><w n="28.1">Le</w> <w n="28.2">vase</w> <w n="28.3">où</w> <w n="28.4">doit</w> <w n="28.5">tomber</w> <w n="28.6">le</w> <w n="28.7">sang</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Il</w> <w n="29.2">veut</w>, <w n="29.3">comme</w> <w n="29.4">on</w> <w n="29.5">faisait</w> <w n="29.6">en</w> <w n="29.7">Grèce</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Brûlant</w> <w n="30.2">sous</w> <w n="30.3">le</w> <w n="30.4">ciel</w> <w n="30.5">radieux</w></l>
					<l n="31" num="8.3"><w n="31.1">Les</w> <w n="31.2">entrailles</w> <w n="31.3">avec</w> <w n="31.4">la</w> <w n="31.5">graisse</w>,</l>
					<l n="32" num="8.4"><w n="32.1">En</w> <w n="32.2">offrir</w> <w n="32.3">la</w> <w n="32.4">fumée</w> <w n="32.5">aux</w> <w n="32.6">Dieux</w> ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Il</w> <w n="33.2">veut</w>, <w n="33.3">lui</w> <w n="33.4">soldat</w> <w n="33.5">qu</w>’<w n="33.6">on</w> <w n="33.7">redoute</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Cuirassier</w>, <w n="34.2">général</w> <w n="34.3">en</w> <w n="34.4">chef</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Savoir</w> <w n="35.2">quel</w> <w n="35.3">goût</w>, <w n="35.4">quand</w> <w n="35.5">on</w> <w n="35.6">les</w> <w n="35.7">goûte</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Ont</w> <w n="36.2">les</w> <w n="36.3">vrais</w> <w n="36.4">Parisiens</w> ; — <w n="36.5">bref</w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Il</w> <w n="37.2">veut</w> — <w n="37.3">c</w>’<w n="37.4">est</w> <w n="37.5">le</w> <w n="37.6">désir</w> <w n="37.7">en</w> <w n="37.8">somme</w></l>
					<l n="38" num="10.2"><w n="38.1">Dont</w> <w n="38.2">il</w> <w n="38.3">fut</w> <w n="38.4">toujours</w> <w n="38.5">démangé</w></l>
					<l n="39" num="10.3"><w n="39.1">Dire</w> <w n="39.2">un</w> <w n="39.3">jour</w> <w n="39.4">de</w> <w n="39.5">nous</w>, <w n="39.6">le</w> <w n="39.7">pauvre</w> <w n="39.8">homme</w> :</l>
					<l n="40" num="10.4"><w n="40.1">Ils</w> <w n="40.2">étaient</w> <w n="40.3">bons</w>, <w n="40.4">j</w>’<w n="40.5">en</w> <w n="40.6">ai</w> <w n="40.7">mangé</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Novembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>