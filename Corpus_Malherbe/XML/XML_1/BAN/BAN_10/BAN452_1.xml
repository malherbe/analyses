<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN452">
				<head type="main">Sabbat</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
									Ah ! au milieu du chant, une souris <lb></lb>
									rouge lui a jailli de la bouche.
							</quote>
							<bibl>
								<name>Goethe</name>,<hi rend="ital">Faust</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">le</w> <w n="1.4">sabbat</w>. <w n="1.5">Des</w> <w n="1.6">femmes</w> <w n="1.7">nues</w></l>
					<l n="2" num="1.2"><w n="2.1">Aux</w> <w n="2.2">ailes</w> <w n="2.3">de</w> <w n="2.4">chauve</w>-<w n="2.5">souris</w></l>
					<l n="3" num="1.3"><w n="3.1">Volent</w> <w n="3.2">prestement</w> <w n="3.3">dans</w> <w n="3.4">les</w> <w n="3.5">nues</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Au</w>-<w n="4.2">dessus</w> <w n="4.3">des</w> <w n="4.4">toits</w> <w n="4.5">de</w> <w n="4.6">Paris</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Germania</w> <w n="5.2">mène</w> <w n="5.3">la</w> <w n="5.4">danse</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Plus</w> <w n="6.2">folle</w> <w n="6.3">qu</w>’<w n="6.4">un</w> <w n="6.5">cheval</w> <w n="6.6">sans</w> <w n="6.7">mors</w></l>
					<l n="7" num="2.3"><w n="7.1">Ou</w> <w n="7.2">qu</w>’<w n="7.3">une</w> <w n="7.4">urne</w> <w n="7.5">qui</w> <w n="7.6">n</w>’<w n="7.7">a</w> <w n="7.8">plus</w> <w n="7.9">d</w>’<w n="7.10">anse</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Sur</w> <w n="8.2">la</w> <w n="8.3">colline</w> <w n="8.4">où</w> <w n="8.5">sont</w> <w n="8.6">les</w> <w n="8.7">morts</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Cette</w> <w n="9.2">Gretchen</w> <w n="9.3">dorée</w> <w n="9.4">et</w> <w n="9.5">blanche</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Dans</w> <w n="10.2">ses</w> <w n="10.3">prunelles</w> <w n="10.4">de</w> <w n="10.5">saphir</w></l>
					<l n="11" num="3.3"><w n="11.1">Montre</w> <w n="11.2">des</w> <w n="11.3">reflets</w> <w n="11.4">de</w> <w n="11.5">pervenche</w>.</l>
					<l n="12" num="3.4"><w n="12.1">Elle</w> <w n="12.2">frémit</w> <w n="12.3">pour</w> <w n="12.4">un</w> <w n="12.5">zéphyr</w></l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Ou</w> <w n="13.2">pour</w> <w n="13.3">un</w> <w n="13.4">brin</w> <w n="13.5">d</w>’<w n="13.6">herbe</w> <w n="13.7">qui</w> <w n="13.8">bouge</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Comme</w> <w n="14.2">une</w> <w n="14.3">Agnès</w> <w n="14.4">au</w> <w n="14.5">temps</w> <w n="14.6">jadis</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">Mais</w> <w n="15.2">parfois</w> <w n="15.3">une</w> <w n="15.4">souris</w> <w n="15.5">rouge</w></l>
					<l n="16" num="4.4"><w n="16.1">Sort</w> <w n="16.2">de</w> <w n="16.3">sa</w> <w n="16.4">bouche</w> <w n="16.5">aux</w> <w n="16.6">dents</w> <w n="16.7">de</w> <w n="16.8">lys</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">En</w> <w n="17.2">face</w> <w n="17.3">d</w>’<w n="17.4">elle</w> <w n="17.5">se</w> <w n="17.6">trémousse</w></l>
					<l n="18" num="5.2"><w n="18.1">Un</w> <w n="18.2">cuirassier</w>, <w n="18.3">brillant</w> <w n="18.4">Myrtil</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Qui</w> <w n="19.2">fait</w> <w n="19.3">merveille</w> <w n="19.4">sur</w> <w n="19.5">la</w> <w n="19.6">mousse</w>.</l>
					<l n="20" num="5.4"><w n="20.1">Oh</w> ! <w n="20.2">le</w> <w n="20.3">beau</w> <w n="20.4">sabbat</w> ! <w n="20.5">lui</w> <w n="20.6">dit</w>-<w n="20.7">il</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Sous</w> <w n="21.2">ce</w> <w n="21.3">brillant</w> <w n="21.4">habit</w> <w n="21.5">de</w> <w n="21.6">reître</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Sans</w> <w n="22.2">plume</w> <w n="22.3">de</w> <w n="22.4">coq</w> <w n="22.5">ni</w> <w n="22.6">manteau</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Qui</w> <w n="23.2">diable</w> <w n="23.3">pourrait</w> <w n="23.4">reconnaître</w></l>
					<l n="24" num="6.4"><w n="24.1">Le</w> <w n="24.2">vieux</w> <w n="24.3">compère</w> <w n="24.4">Méphisto</w> ?</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">D</w>’<w n="25.2">où</w> <w n="25.3">je</w> <w n="25.4">viens</w> <w n="25.5">avec</w> <w n="25.6">mon</w> <w n="25.7">amante</w>,</l>
					<l n="26" num="7.2"><w n="26.1">On</w> <w n="26.2">ne</w> <w n="26.3">s</w>’<w n="26.4">en</w> <w n="26.5">doutera</w> <w n="26.6">jamais</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">je</w> <w n="27.3">veux</w>, <w n="27.4">ô</w> <w n="27.5">ma</w> <w n="27.6">Bradamante</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Vous</w> <w n="28.2">faire</w> <w n="28.3">impératrice</w> ! — <w n="28.4">Mais</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Comme</w> <w n="29.2">il</w> <w n="29.3">la</w> <w n="29.4">berce</w> <w n="29.5">d</w>’<w n="29.6">un</w> <w n="29.7">tel</w> <w n="29.8">conte</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Embéguiné</w> <w n="30.2">dans</w> <w n="30.3">ses</w> <w n="30.4">amours</w>,</l>
					<l n="31" num="8.3"><w n="31.1">De</w> <w n="31.2">Moltke</w> <w n="31.3">dit</w> : <w n="31.4">Pardon</w>, <w n="31.5">cher</w> <w n="31.6">comte</w> !</l>
					<l n="32" num="8.4"><w n="32.1">On</w> <w n="32.2">vous</w> <w n="32.3">reconnaîtra</w> <w n="32.4">toujours</w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Tant</w> <w n="33.2">votre</w> <w n="33.3">valeur</w> <w n="33.4">a</w> <w n="33.5">de</w> <w n="33.6">lustre</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Fussiez</w>-<w n="34.2">vous</w> <w n="34.3">même</w> <w n="34.4">à</w> <w n="34.5">Fernambouc</w> ;</l>
					<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">là</w>, <w n="35.3">dans</w> <w n="35.4">votre</w> <w n="35.5">botte</w> <w n="35.6">illustre</w></l>
					<l n="36" num="9.4"><w n="36.1">On</w> <w n="36.2">voit</w> <w n="36.3">très</w>-<w n="36.4">bien</w> <w n="36.5">le</w> <w n="36.6">pied</w> <w n="36.7">de</w> <w n="36.8">bouc</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Décembre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>