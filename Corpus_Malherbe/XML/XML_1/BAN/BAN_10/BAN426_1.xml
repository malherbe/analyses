<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES PRUSSIENNES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2876 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_10</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvilleidyllesprussienes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Idylles prussiennes</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">IV</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1871">1871</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN426">
				<head type="main">Châteaudun</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Châteaudun</w> ! <w n="1.2">qui</w> <w n="1.3">vois</w> <w n="1.4">des</w> <w n="1.5">bourreaux</w></l>
					<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">furent</w> <w n="2.3">des</w> <w n="2.4">cœurs</w> <w n="2.5">de</w> <w n="2.6">lion</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Tu</w> <w n="3.2">nous</w> <w n="3.3">parais</w>, <w n="3.4">nid</w> <w n="3.5">de</w> <w n="3.6">héros</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Plus</w> <w n="4.2">sublime</w> <w n="4.3">qu</w>’<w n="4.4">un</w> <w n="4.5">Ilion</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Comme</w> <w n="5.2">on</w> <w n="5.3">fauche</w> <w n="5.4">des</w> <w n="5.5">épis</w> <w n="5.6">mûrs</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">boulets</w> <w n="6.3">rougis</w> <w n="6.4">et</w> <w n="6.5">fumants</w></l>
					<l n="7" num="2.3"><w n="7.1">Ont</w> <w n="7.2">dans</w> <w n="7.3">les</w> <w n="7.4">débris</w> <w n="7.5">de</w> <w n="7.6">tes</w> <w n="7.7">murs</w></l>
					<l n="8" num="2.4"><w n="8.1">Dispersé</w> <w n="8.2">tes</w> <w n="8.3">abris</w> <w n="8.4">charmants</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">tes</w> <w n="9.3">fils</w>, <w n="9.4">les</w> <w n="9.5">chasseurs</w> <w n="9.6">de</w> <w n="9.7">loups</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Sont</w> <w n="10.2">tombés</w> <w n="10.3">purs</w> <w n="10.4">et</w> <w n="10.5">sans</w> <w n="10.6">remords</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Ils</w> <w n="11.2">étaient</w> <w n="11.3">mille</w>, <w n="11.4">et</w> <w n="11.5">sous</w> <w n="11.6">leurs</w> <w n="11.7">coups</w></l>
					<l n="12" num="3.4"><w n="12.1">Dix</w>-<w n="12.2">huit</w> <w n="12.3">cents</w> <w n="12.4">Prussiens</w> <w n="12.5">sont</w> <w n="12.6">morts</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Illustre</w> <w n="13.2">cité</w> (<w n="13.3">les</w> <w n="13.4">Romains</w></l>
					<l n="14" num="4.2"><w n="14.1">Te</w> <w n="14.2">nommaient</w> <w n="14.3">ainsi</w>) <w n="14.4">pour</w> <w n="14.5">tes</w> <w n="14.6">fils</w></l>
					<l n="15" num="4.3"><w n="15.1">Tu</w> <w n="15.2">renaîtras</w> ! <w n="15.3">par</w> <w n="15.4">tes</w> <w n="15.5">chemins</w></l>
					<l n="16" num="4.4"><w n="16.1">On</w> <w n="16.2">entendra</w>, <w n="16.3">comme</w> <w n="16.4">jadis</w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Dans</w> <w n="17.2">tes</w> <w n="17.3">arbres</w> <w n="17.4">en</w> <w n="17.5">floraison</w></l>
					<l n="18" num="5.2"><w n="18.1">L</w>’<w n="18.2">alouette</w> <w n="18.3">éveiller</w> <w n="18.4">l</w>’<w n="18.5">écho</w>.</l>
					<l n="19" num="5.3"><w n="19.1">La</w> <w n="19.2">devise</w> <w n="19.3">de</w> <w n="19.4">ton</w> <w n="19.5">blason</w></l>
					<l n="20" num="5.4"><w n="20.1">Dit</w> : <w n="20.2">Extincta</w> <w n="20.3">revivisco</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Mais</w>, <w n="21.2">froid</w> <w n="21.3">cadavre</w> <w n="21.4">au</w> <w n="21.5">pied</w> <w n="21.6">des</w> <w n="21.7">tours</w></l>
					<l n="22" num="6.2"><w n="22.1">Parmi</w> <w n="22.2">les</w> <w n="22.3">décombres</w> <w n="22.4">mouvants</w></l>
					<l n="23" num="6.3"><w n="23.1">Fouillé</w> <w n="23.2">par</w> <w n="23.3">le</w> <w n="23.4">bec</w> <w n="23.5">des</w> <w n="23.6">vautours</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">cendre</w> <w n="24.3">abandonnée</w> <w n="24.4">aux</w> <w n="24.5">vents</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Tu</w> <w n="25.2">resplendis</w> ! <w n="25.3">patrie</w> <w n="25.4">en</w> <w n="25.5">deuil</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Qui</w>, <w n="26.2">devant</w> <w n="26.3">le</w> <w n="26.4">destin</w> <w n="26.5">moqueur</w></l>
					<l n="27" num="7.3"><w n="27.1">Moins</w> <w n="27.2">obstiné</w> <w n="27.3">que</w> <w n="27.4">ton</w> <w n="27.5">orgueil</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Portas</w> <w n="28.2">la</w> <w n="28.3">France</w> <w n="28.4">dans</w> <w n="28.5">ton</w> <w n="28.6">cœur</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Car</w> <w n="29.2">tes</w> <w n="29.3">défenseurs</w> <w n="29.4">belliqueux</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Frémissant</w> <w n="30.2">d</w>’<w n="30.3">indignation</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Laissaient</w> <w n="31.2">à</w> <w n="31.3">de</w> <w n="31.4">plus</w> <w n="31.5">lâches</w> <w n="31.6">qu</w>’<w n="31.7">eux</w></l>
					<l n="32" num="8.4"><w n="32.1">L</w>’<w n="32.2">ignoble</w> <w n="32.3">résignation</w> ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Voulant</w> <w n="33.2">tous</w>, <w n="33.3">d</w>’<w n="33.4">un</w> <w n="33.5">esprit</w> <w n="33.6">têtu</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Que</w> <w n="34.2">ton</w> <w n="34.3">beau</w> <w n="34.4">renom</w> <w n="34.5">pût</w> <w n="34.6">fleurir</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Ils</w> <w n="35.2">eurent</w> <w n="35.3">la</w> <w n="35.4">mâle</w> <w n="35.5">vertu</w></l>
					<l n="36" num="9.4"><w n="36.1">De</w> <w n="36.2">tuer</w> <w n="36.3">avant</w> <w n="36.4">de</w> <w n="36.5">mourir</w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">rien</w> <w n="37.3">ne</w> <w n="37.4">vaut</w> <w n="37.5">le</w> <w n="37.6">fier</w> <w n="37.7">sommeil</w></l>
					<l n="38" num="10.2"><w n="38.1">De</w> <w n="38.2">ces</w> <w n="38.3">soldats</w> <w n="38.4">couchés</w> <w n="38.5">en</w> <w n="38.6">rang</w></l>
					<l n="39" num="10.3"><w n="39.1">Sur</w> <w n="39.2">la</w> <w n="39.3">terre</w> <w n="39.4">nue</w>, <w n="39.5">au</w> <w n="39.6">soleil</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Qui</w> <w n="40.2">dorment</w>, <w n="40.3">couchés</w> <w n="40.4">dans</w> <w n="40.5">leur</w> <w n="40.6">sang</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">Octobre 1870.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>