<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="BAN34">
					<head type="main">LA RENAISSANCE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Ameine avecques toy la Cyprienne sainte…</quote>
								<bibl>
									<name>RONSARD</name>, <hi rend="ital">Églogue</hi>II.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">On</w> <w n="1.2">a</w> <w n="1.3">dit</w> <w n="1.4">qu</w>’<w n="1.5">une</w> <w n="1.6">vierge</w> <w n="1.7">à</w> <w n="1.8">la</w> <w n="1.9">parure</w> <w n="1.10">d</w>’<w n="1.11">or</w></l>
						<l n="2" num="1.2"><w n="2.1">Sur</w> <w n="2.2">l</w>’<w n="2.3">épaule</w> <w n="2.4">des</w> <w n="2.5">flots</w> <w n="2.6">vint</w> <w n="2.7">de</w> <w n="2.8">Cypre</w> <w n="2.9">à</w> <w n="2.10">Cythère</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">que</w> <w n="3.3">ses</w> <w n="3.4">pieds</w> <w n="3.5">polis</w>, <w n="3.6">en</w> <w n="3.7">caressant</w> <w n="3.8">la</w> <w n="3.9">terre</w>,</l>
						<l n="4" num="1.4"><w n="4.1">À</w> <w n="4.2">chacun</w> <w n="4.3">de</w> <w n="4.4">ses</w> <w n="4.5">pas</w> <w n="4.6">laissèrent</w> <w n="4.7">un</w> <w n="4.8">trésor</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">oiseau</w> <w n="5.3">vermeil</w>, <w n="5.4">qui</w> <w n="5.5">chante</w> <w n="5.6">en</w> <w n="5.7">prenant</w> <w n="5.8">son</w> <w n="5.9">essor</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Emplit</w> <w n="6.2">d</w>’<w n="6.3">enchantements</w> <w n="6.4">la</w> <w n="6.5">forêt</w> <w n="6.6">solitaire</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">les</w> <w n="7.3">ruisseaux</w> <w n="7.4">glacés</w> <w n="7.5">où</w> <w n="7.6">l</w>’<w n="7.7">on</w> <w n="7.8">se</w> <w n="7.9">désaltère</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Sentirent</w> <w n="8.2">dans</w> <w n="8.3">leurs</w> <w n="8.4">flots</w> <w n="8.5">plus</w> <w n="8.6">de</w> <w n="8.7">fraîcheur</w> <w n="8.8">encor</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">fleur</w> <w n="9.3">s</w>’<w n="9.4">ouvrit</w> <w n="9.5">plus</w> <w n="9.6">pure</w> <w n="9.7">aux</w> <w n="9.8">baisers</w> <w n="9.9">de</w> <w n="9.10">la</w> <w n="9.11">brise</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">sous</w> <w n="10.3">les</w> <w n="10.4">myrtes</w> <w n="10.5">verts</w>, <w n="10.6">la</w> <w n="10.7">vierge</w> <w n="10.8">plus</w> <w n="10.9">éprise</w></l>
						<l n="11" num="3.3"><w n="11.1">Releva</w> <w n="11.2">dans</w> <w n="11.3">ses</w> <w n="11.4">bras</w> <w n="11.5">son</w> <w n="11.6">amant</w> <w n="11.7">à</w> <w n="11.8">genoux</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">De</w> <w n="12.2">même</w> <w n="12.3">quand</w> <w n="12.4">plus</w> <w n="12.5">tard</w>, <w n="12.6">autre</w> <w n="12.7">Anadyomène</w>,</l>
						<l n="13" num="4.2"><w n="13.1">La</w> <w n="13.2">renaissance</w> <w n="13.3">vint</w>, <w n="13.4">et</w> <w n="13.5">rayonna</w> <w n="13.6">sur</w> <w n="13.7">nous</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Toute</w> <w n="14.2">chose</w> <w n="14.3">fleurit</w> <w n="14.4">au</w> <w n="14.5">fond</w> <w n="14.6">de</w> <w n="14.7">l</w>’<w n="14.8">âme</w> <w n="14.9">humaine</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">juin 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>