<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="BAN1">
					<head type="main">À MA MÈRE</head>
					<head type="sub_1">Madame Élisabeth Zélie de Banville</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ô</w> <w n="1.2">ma</w> <w n="1.3">mère</w>, <w n="1.4">ce</w> <w n="1.5">sont</w> <w n="1.6">nos</w> <w n="1.7">mères</w></l>
						<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">les</w> <w n="2.3">sourires</w> <w n="2.4">triomphants</w></l>
						<l n="3" num="1.3"><w n="3.1">Bercent</w> <w n="3.2">nos</w> <w n="3.3">premières</w> <w n="3.4">chimères</w></l>
						<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">nos</w> <w n="4.3">premiers</w> <w n="4.4">berceaux</w> <w n="4.5">d</w>’<w n="4.6">enfants</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Donc</w> <w n="5.2">reçois</w>, <w n="5.3">comme</w> <w n="5.4">une</w> <w n="5.5">promesse</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Ce</w> <w n="6.2">livre</w> <w n="6.3">où</w> <w n="6.4">coulent</w> <w n="6.5">de</w> <w n="6.6">mes</w> <w n="6.7">vers</w></l>
						<l n="7" num="2.3"><w n="7.1">Tous</w> <w n="7.2">les</w> <w n="7.3">espoirs</w> <w n="7.4">de</w> <w n="7.5">ma</w> <w n="7.6">jeunesse</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Comme</w> <w n="8.2">l</w>’<w n="8.3">eau</w> <w n="8.4">des</w> <w n="8.5">lys</w> <w n="8.6">entr</w>’<w n="8.7">ouverts</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Reçois</w> <w n="9.2">ce</w> <w n="9.3">livre</w>, <w n="9.4">qui</w> <w n="9.5">peut</w>-<w n="9.6">être</w></l>
						<l n="10" num="3.2"><w n="10.1">Sera</w> <w n="10.2">muet</w> <w n="10.3">pour</w> <w n="10.4">l</w>’<w n="10.5">avenir</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Mais</w> <w n="11.2">où</w> <w n="11.3">tu</w> <w n="11.4">verras</w> <w n="11.5">apparaître</w></l>
						<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">vague</w> <w n="12.3">et</w> <w n="12.4">lointain</w> <w n="12.5">souvenir</w></l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">De</w> <w n="13.2">mon</w> <w n="13.3">enfance</w> <w n="13.4">dépensée</w></l>
						<l n="14" num="4.2"><w n="14.1">Dans</w> <w n="14.2">un</w> <w n="14.3">rêve</w> <w n="14.4">triste</w> <w n="14.5">ou</w> <w n="14.6">moqueur</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Fou</w>, <w n="15.2">car</w> <w n="15.3">il</w> <w n="15.4">contient</w> <w n="15.5">ma</w> <w n="15.6">pensée</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Chaste</w>, <w n="16.2">car</w> <w n="16.3">il</w> <w n="16.4">contient</w> <w n="16.5">mon</w> <w n="16.6">cœur</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">juillet 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>