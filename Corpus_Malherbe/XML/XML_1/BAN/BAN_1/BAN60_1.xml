<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="BAN60">
					<head type="main">SIESTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">sombre</w> <w n="1.3">forêt</w>, <w n="1.4">où</w> <w n="1.5">la</w> <w n="1.6">roche</w></l>
						<l n="2" num="1.2"><w n="2.1">Est</w> <w n="2.2">pleine</w> <w n="2.3">d</w>’<w n="2.4">éblouissements</w></l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">qui</w> <w n="3.3">tressaille</w> <w n="3.4">à</w> <w n="3.5">mon</w> <w n="3.6">approche</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Murmure</w> <w n="4.2">avec</w> <w n="4.3">des</w> <w n="4.4">bruits</w> <w n="4.5">charmants</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">fauvettes</w> <w n="5.3">font</w> <w n="5.4">leur</w> <w n="5.5">prière</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">La</w> <w n="6.2">terre</w> <w n="6.3">noire</w> <w n="6.4">après</w> <w n="6.5">ses</w> <w n="6.6">deuils</w></l>
						<l n="7" num="2.3"><w n="7.1">Refleurit</w>, <w n="7.2">et</w> <w n="7.3">dans</w> <w n="7.4">la</w> <w n="7.5">clairière</w></l>
						<l n="8" num="2.4"><w n="8.1">Je</w> <w n="8.2">vois</w> <w n="8.3">passer</w> <w n="8.4">les</w> <w n="8.5">doux</w> <w n="8.6">chevreuils</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Voici</w> <w n="9.2">la</w> <w n="9.3">caverne</w> <w n="9.4">des</w> <w n="9.5">fées</w></l>
						<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2">où</w> <w n="10.3">fuyant</w> <w n="10.4">vers</w> <w n="10.5">le</w> <w n="10.6">bleu</w> <w n="10.7">des</w> <w n="10.8">cieux</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Montent</w> <w n="11.2">des</w> <w n="11.3">chansons</w> <w n="11.4">étouffées</w></l>
						<l n="12" num="3.4"><w n="12.1">Sous</w> <w n="12.2">les</w> <w n="12.3">rosiers</w> <w n="12.4">délicieux</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">veux</w> <w n="13.3">dormir</w> <w n="13.4">là</w> <w n="13.5">toute</w> <w n="13.6">une</w> <w n="13.7">heure</w></l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">goûter</w> <w n="14.3">un</w> <w n="14.4">calme</w> <w n="14.5">sommeil</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Bercé</w> <w n="15.2">par</w> <w n="15.3">le</w> <w n="15.4">ruisseau</w> <w n="15.5">qui</w> <w n="15.6">pleure</w></l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">caressé</w> <w n="16.3">par</w> <w n="16.4">l</w>’<w n="16.5">air</w> <w n="16.6">vermeil</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">tandis</w> <w n="17.3">que</w> <w n="17.4">dans</w> <w n="17.5">ma</w> <w n="17.6">pensée</w></l>
						<l n="18" num="5.2"><w n="18.1">Je</w> <w n="18.2">verrai</w>, <w n="18.3">ne</w> <w n="18.4">songeant</w> <w n="18.5">à</w> <w n="18.6">rien</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Une</w> <w n="19.2">riche</w> <w n="19.3">étoffe</w> <w n="19.4">tissée</w></l>
						<l n="20" num="5.4"><w n="20.1">Par</w> <w n="20.2">quelque</w> <w n="20.3">rêve</w> <w n="20.4">aérien</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Peut</w>-<w n="21.2">être</w> <w n="21.3">que</w> <w n="21.4">sous</w> <w n="21.5">la</w> <w n="21.6">ramure</w></l>
						<l n="22" num="6.2"><w n="22.1">Une</w> <w n="22.2">blanche</w> <w n="22.3">fée</w> <w n="22.4">en</w> <w n="22.5">plein</w> <w n="22.6">jour</w></l>
						<l n="23" num="6.3"><w n="23.1">Viendra</w> <w n="23.2">baiser</w> <w n="23.3">ma</w> <w n="23.4">chevelure</w></l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">ma</w> <w n="24.3">bouche</w> <w n="24.4">folle</w> <w n="24.5">d</w>’<w n="24.6">amour</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">avril 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>