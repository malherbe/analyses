<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">SONGE D’HIVER</head><div type="poem" key="BAN25">
						<head type="number">XII</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Chose</w> <w n="1.2">horrible</w> ! <w n="1.3">Ils</w> <w n="1.4">n</w>’<w n="1.5">étaient</w> <w n="1.6">d</w>’<w n="1.7">abord</w> <w n="1.8">que</w> <w n="1.9">quelques</w>-<w n="1.10">uns</w></l>
							<l n="2" num="1.2"><w n="2.1">Noyant</w> <w n="2.2">leur</w> <w n="2.3">âme</w> <w n="2.4">vierge</w> <w n="2.5">à</w> <w n="2.6">ces</w> <w n="2.7">âcres</w> <w n="2.8">parfums</w> ;</l>
							<l n="3" num="1.3"><space quantity="12" unit="char"></space><w n="3.1">Mais</w> <w n="3.2">bientôt</w> <w n="3.3">une</w> <w n="3.4">foule</w></l>
							<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">festin</w> <w n="4.3">monstrueux</w> <w n="4.4">s</w>’<w n="4.5">amassa</w> <w n="4.6">follement</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">je</w> <w n="5.3">les</w> <w n="5.4">vis</w> <w n="5.5">tomber</w>, <w n="5.6">privés</w> <w n="5.7">de</w> <w n="5.8">sentiment</w>,</l>
							<l n="6" num="1.6"><space quantity="12" unit="char"></space><w n="6.1">Comme</w> <w n="6.2">un</w> <w n="6.3">mur</w> <w n="6.4">qui</w> <w n="6.5">s</w>’<w n="6.6">écroule</w>.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><w n="7.1">Ils</w> <w n="7.2">allaient</w> ! <w n="7.3">Déchirés</w> <w n="7.4">par</w> <w n="7.5">quelque</w> <w n="7.6">étrange</w> <w n="7.7">faim</w>,</l>
							<l n="8" num="2.2"><w n="8.1">Sans</w> <w n="8.2">entrevoir</w> <w n="8.3">le</w> <w n="8.4">but</w>, <w n="8.5">sans</w> <w n="8.6">regarder</w> <w n="8.7">la</w> <w n="8.8">fin</w>,</l>
							<l n="9" num="2.3"><space quantity="12" unit="char"></space><w n="9.1">Pris</w> <w n="9.2">dans</w> <w n="9.3">un</w> <w n="9.4">noir</w> <w n="9.5">vertige</w> ;</l>
							<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">chacun</w>, <w n="10.3">l</w>’<w n="10.4">œil</w> <w n="10.5">éteint</w> <w n="10.6">et</w> <w n="10.7">le</w> <w n="10.8">front</w> <w n="10.9">dans</w> <w n="10.10">les</w> <w n="10.11">cieux</w>,</l>
							<l n="11" num="2.5"><w n="11.1">Tombait</w>, <w n="11.2">en</w> <w n="11.3">murmurant</w> <w n="11.4">des</w> <w n="11.5">mots</w> <w n="11.6">harmonieux</w>,</l>
							<l n="12" num="2.6"><space quantity="12" unit="char"></space><w n="12.1">Lys</w> <w n="12.2">inclinant</w> <w n="12.3">sa</w> <w n="12.4">tige</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">Et</w> <w n="13.2">l</w>’<w n="13.3">ivresse</w> <w n="13.4">augmenta</w>. <w n="13.5">Par</w> <w n="13.6">degrés</w>, <w n="13.7">éperdus</w></l>
							<l n="14" num="3.2"><w n="14.1">Tous</w> <w n="14.2">chancelaient</w>. <w n="14.3">À</w> <w n="14.4">voir</w> <w n="14.5">tous</w> <w n="14.6">leurs</w> <w n="14.7">corps</w> <w n="14.8">étendus</w></l>
							<l n="15" num="3.3"><space quantity="12" unit="char"></space><w n="15.1">Près</w> <w n="15.2">du</w> <w n="15.3">marbre</w> <w n="15.4">des</w> <w n="15.5">portes</w>,</l>
							<l n="16" num="3.4"><w n="16.1">On</w> <w n="16.2">eût</w> <w n="16.3">dit</w>, <w n="16.4">aux</w> <w n="16.5">glaçons</w>, <w n="16.6">à</w> <w n="16.7">la</w> <w n="16.8">blancheur</w> <w n="16.9">de</w> <w n="16.10">lys</w></l>
							<l n="17" num="3.5"><w n="17.1">De</w> <w n="17.2">ces</w> <w n="17.3">rêveurs</w> <w n="17.4">couchés</w>, <w n="17.5">une</w> <w n="17.6">nécropolis</w></l>
							<l n="18" num="3.6"><space quantity="12" unit="char"></space><w n="18.1">Pleine</w> <w n="18.2">de</w> <w n="18.3">choses</w> <w n="18.4">mortes</w>.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1"><w n="19.1">Alors</w>, <w n="19.2">plus</w> <w n="19.3">j</w>’<w n="19.4">en</w> <w n="19.5">voyais</w> <w n="19.6">tomber</w> <w n="19.7">autour</w> <w n="19.8">de</w> <w n="19.9">moi</w>,</l>
							<l n="20" num="4.2"><w n="20.1">Hasard</w> <w n="20.2">étrange</w> ! <w n="20.3">Et</w> <w n="20.4">plus</w> <w n="20.5">dans</w> <w n="20.6">un</w> <w n="20.7">divin</w> <w n="20.8">émoi</w></l>
							<l n="21" num="4.3"><space quantity="12" unit="char"></space><w n="21.1">Je</w> <w n="21.2">me</w> <w n="21.3">sentais</w> <w n="21.4">revivre</w>.</l>
							<l n="22" num="4.4"><w n="22.1">Enfin</w>, <w n="22.2">glacé</w> <w n="22.3">d</w>’<w n="22.4">attente</w> <w n="22.5">et</w> <w n="22.6">chaud</w> <w n="22.7">de</w> <w n="22.8">leurs</w> <w n="22.9">baisers</w>,</l>
							<l n="23" num="4.5"><w n="23.1">Je</w> <w n="23.2">sentis</w> <w n="23.3">tressaillir</w> <w n="23.4">mes</w> <w n="23.5">membres</w> <w n="23.6">embrasés</w></l>
							<l n="24" num="4.6"><space quantity="12" unit="char"></space><w n="24.1">Et</w> <w n="24.2">je</w> <w n="24.3">voulus</w> <w n="24.4">les</w> <w n="24.5">suivre</w>.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1"><w n="25.1">Mais</w> <w n="25.2">la</w> <w n="25.3">vierge</w> <w n="25.4">à</w> <w n="25.5">la</w> <w n="25.6">lyre</w> <w n="25.7">eut</w> <w n="25.8">un</w> <w n="25.9">air</w> <w n="25.10">abattu</w></l>
							<l n="26" num="5.2"><w n="26.1">Et</w> <w n="26.2">me</w> <w n="26.3">prit</w> <w n="26.4">par</w> <w n="26.5">la</w> <w n="26.6">main</w> <w n="26.7">en</w> <w n="26.8">disant</w> : <w n="26.9">connais</w>-<w n="26.10">tu</w></l>
							<l n="27" num="5.3"><space quantity="12" unit="char"></space><w n="27.1">Ces</w> <w n="27.2">deux</w> <w n="27.3">beautés</w> <w n="27.4">de</w> <w n="27.5">neige</w> ?</l>
							<l n="28" num="5.4"><w n="28.1">Moi</w> <w n="28.2">je</w> <w n="28.3">voulus</w> <w n="28.4">partir</w> <w n="28.5">et</w> <w n="28.6">je</w> <w n="28.7">répondis</w> : <w n="28.8">non</w> !</l>
							<l n="29" num="5.5">—<w n="29.1">L</w>’<w n="29.2">une</w> <w n="29.3">est</w> <w n="29.4">la</w> <w n="29.5">volupté</w>, <w n="29.6">dit</w>-<w n="29.7">elle</w>, <w n="29.8">c</w>’<w n="29.9">est</w> <w n="29.10">son</w> <w n="29.11">nom</w>.</l>
							<l n="30" num="5.6"><space quantity="12" unit="char"></space>—<w n="30.1">et</w> <w n="30.2">l</w>’<w n="30.3">autre</w> ? <w n="30.4">Demandai</w>-<w n="30.5">je</w>.</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1">—<w n="31.1">Cette</w> <w n="31.2">fille</w> <w n="31.3">si</w> <w n="31.4">pâle</w>, <w n="31.5">aux</w> <w n="31.6">baisers</w> <w n="31.7">si</w> <w n="31.8">nerveux</w>,</l>
							<l n="32" num="6.2"><w n="32.1">Qui</w> <w n="32.2">se</w> <w n="32.3">laisse</w> <w n="32.4">oublier</w> <w n="32.5">et</w> <w n="32.6">dort</w> <w n="32.7">dans</w> <w n="32.8">ses</w> <w n="32.9">cheveux</w> ?</l>
							<l n="33" num="6.3"><space quantity="12" unit="char"></space><w n="33.1">C</w>’<w n="33.2">est</w> <w n="33.3">la</w> <w n="33.4">mort</w> <w n="33.5">qu</w>’<w n="33.6">on</w> <w n="33.7">la</w> <w n="33.8">nomme</w>.</l>
							<l n="34" num="6.4"><w n="34.1">Et</w> <w n="34.2">malgré</w> <w n="34.3">ces</w> <w n="34.4">deux</w> <w n="34.5">noms</w> <w n="34.6">effrayants</w>, <w n="34.7">j</w>’<w n="34.8">allai</w> <w n="34.9">pour</w></l>
							<l n="35" num="6.5"><w n="35.1">Baiser</w> <w n="35.2">aussi</w> <w n="35.3">les</w> <w n="35.4">seins</w> <w n="35.5">des</w> <w n="35.6">vénus</w>, <w n="35.7">fou</w> <w n="35.8">d</w>’<w n="35.9">amour</w></l>
							<l n="36" num="6.6"><space quantity="12" unit="char"></space><w n="36.1">N</w>’<w n="36.2">ayant</w> <w n="36.3">plus</w> <w n="36.4">rien</w> <w n="36.5">d</w>’<w n="36.6">un</w> <w n="36.7">homme</w>.</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1"><w n="37.1">Dès</w> <w n="37.2">le</w> <w n="37.3">premier</w> <w n="37.4">baiser</w> <w n="37.5">je</w> <w n="37.6">ne</w> <w n="37.7">sais</w> <w n="37.8">quelle</w> <w n="37.9">peur</w></l>
							<l n="38" num="7.2"><w n="38.1">Me</w> <w n="38.2">vint</w>, <w n="38.3">et</w> <w n="38.4">je</w> <w n="38.5">fléchis</w>, <w n="38.6">livide</w> <w n="38.7">de</w> <w n="38.8">stupeur</w>,</l>
							<l n="39" num="7.3"><space quantity="12" unit="char"></space><w n="39.1">Comme</w> <w n="39.2">en</w> <w n="39.3">paralysie</w>.</l>
							<l n="40" num="7.4"><w n="40.1">À</w> <w n="40.2">mon</w> <w n="40.3">réveil</w>, <w n="40.4">autour</w> <w n="40.5">du</w> <w n="40.6">lustre</w> <w n="40.7">qui</w> <w n="40.8">pâlit</w>,</l>
							<l n="41" num="7.5"><w n="41.1">Ces</w> <w n="41.2">visions</w> <w n="41.3">fuyaient</w>. <w n="41.4">Seule</w> <w n="41.5">auprès</w> <w n="41.6">de</w> <w n="41.7">mon</w> <w n="41.8">lit</w></l>
							<l n="42" num="7.6"><space quantity="12" unit="char"></space><w n="42.1">Restait</w> <w n="42.2">la</w> <w n="42.3">poésie</w>.</l>
						</lg>
						<lg n="8">
							<l n="43" num="8.1"><w n="43.1">C</w>’<w n="43.2">est</w> <w n="43.3">l</w>’<w n="43.4">enfant</w> <w n="43.5">à</w> <w n="43.6">la</w> <w n="43.7">lyre</w>, <w n="43.8">aux</w> <w n="43.9">célestes</w> <w n="43.10">amours</w>,</l>
							<l n="44" num="8.2"><w n="44.1">Que</w> <w n="44.2">depuis</w> <w n="44.3">j</w>’<w n="44.4">ai</w> <w n="44.5">suivie</w>, <w n="44.6">et</w> <w n="44.7">que</w> <w n="44.8">je</w> <w n="44.9">suis</w> <w n="44.10">toujours</w></l>
							<l n="45" num="8.3"><space quantity="12" unit="char"></space><w n="45.1">Dans</w> <w n="45.2">son</w> <w n="45.3">chemin</w> <w n="45.4">aride</w>.</l>
							<l n="46" num="8.4"><w n="46.1">Voilà</w> <w n="46.2">pourquoi</w>, <w n="46.3">souvent</w> <w n="46.4">sur</w> <w n="46.5">mon</w> <w n="46.6">front</w> <w n="46.7">fatigué</w>,</l>
							<l n="47" num="8.5"><w n="47.1">On</w> <w n="47.2">voit</w>, <w n="47.3">dans</w> <w n="47.4">les</w> <w n="47.5">éclats</w> <w n="47.6">du</w> <w n="47.7">rire</w> <w n="47.8">le</w> <w n="47.9">plus</w> <w n="47.10">gai</w>,</l>
							<l n="48" num="8.6"><space quantity="12" unit="char"></space><w n="48.1">Grimacer</w> <w n="48.2">une</w> <w n="48.3">ride</w>.</l>
						</lg>
					</div></body></text></TEI>