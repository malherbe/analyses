<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">CEUX QUI MEURENT ET CEUX QUI COMBATTENT</head><div type="poem" key="BAN31">
						<head type="number">IV</head>
						<head type="main">UNE NUIT BLANCHE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">ville</w>, <w n="1.3">mer</w> <w n="1.4">immense</w>, <w n="1.5">avec</w> <w n="1.6">ses</w> <w n="1.7">bruits</w> <w n="1.8">sans</w> <w n="1.9">nombre</w>,</l>
							<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">sur</w> <w n="2.3">les</w> <w n="2.4">flots</w> <w n="2.5">du</w> <w n="2.6">jour</w> <w n="2.7">replié</w> <w n="2.8">ses</w> <w n="2.9">flots</w> <w n="2.10">d</w>’<w n="2.11">ombre</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">la</w> <w n="3.3">nuit</w> <w n="3.4">secouant</w> <w n="3.5">son</w> <w n="3.6">front</w> <w n="3.7">plein</w> <w n="3.8">de</w> <w n="3.9">parfums</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Inonde</w> <w n="4.2">le</w> <w n="4.3">ciel</w> <w n="4.4">pur</w> <w n="4.5">de</w> <w n="4.6">ses</w> <w n="4.7">longs</w> <w n="4.8">cheveux</w> <w n="4.9">bruns</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Moi</w>, <w n="5.2">pensif</w>, <w n="5.3">accoudé</w> <w n="5.4">sur</w> <w n="5.5">la</w> <w n="5.6">table</w>, <w n="5.7">j</w>’<w n="5.8">écoute</w></l>
							<l n="6" num="1.6"><w n="6.1">Cette</w> <w n="6.2">haleine</w> <w n="6.3">du</w> <w n="6.4">soir</w> <w n="6.5">que</w> <w n="6.6">je</w> <w n="6.7">recueille</w> <w n="6.8">toute</w>.</l>
							<l n="7" num="1.7"><space quantity="2" unit="char"></space><w n="7.1">Plus</w> <w n="7.2">rien</w> ! <w n="7.3">Ma</w> <w n="7.4">lampe</w> <w n="7.5">seule</w>, <w n="7.6">en</w> <w n="7.7">mon</w> <w n="7.8">réduit</w> <w n="7.9">obscur</w></l>
							<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">son</w> <w n="8.3">pâle</w> <w n="8.4">reflet</w> <w n="8.5">inondant</w> <w n="8.6">le</w> <w n="8.7">vieux</w> <w n="8.8">mur</w>,</l>
							<l n="9" num="1.9"><w n="9.1">Dit</w> <w n="9.2">tout</w> <w n="9.3">bas</w> <w n="9.4">qu</w>’<w n="9.5">au</w> <w n="9.6">milieu</w> <w n="9.7">du</w> <w n="9.8">sommeil</w> <w n="9.9">de</w> <w n="9.10">la</w> <w n="9.11">terre</w></l>
							<l n="10" num="1.10"><w n="10.1">Travaille</w> <w n="10.2">une</w> <w n="10.3">pensée</w> <w n="10.4">étrange</w> <w n="10.5">et</w> <w n="10.6">solitaire</w>.</l>
							<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">cependant</w> <w n="11.3">en</w> <w n="11.4">proie</w> <w n="11.5">à</w> <w n="11.6">mille</w> <w n="11.7">visions</w>,</l>
							<l n="12" num="1.12"><w n="12.1">Mon</w> <w n="12.2">esprit</w> <w n="12.3">hésitant</w> <w n="12.4">s</w>’<w n="12.5">emplit</w> <w n="12.6">d</w>’<w n="12.7">illusions</w>,</l>
							<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">mes</w> <w n="13.3">doigts</w> <w n="13.4">engourdis</w> <w n="13.5">laissent</w> <w n="13.6">tomber</w> <w n="13.7">ma</w> <w n="13.8">plume</w>.</l>
							<l n="14" num="1.14"><w n="14.1">C</w>’<w n="14.2">est</w> <w n="14.3">le</w> <w n="14.4">sommeil</w> <w n="14.5">qui</w> <w n="14.6">vient</w>. <w n="14.7">Non</w>, <w n="14.8">mon</w> <w n="14.9">regard</w> <w n="14.10">s</w>’<w n="14.11">allume</w>,</l>
							<l n="15" num="1.15"><w n="15.1">Et</w>, <w n="15.2">comme</w> <w n="15.3">avec</w> <w n="15.4">terreur</w>, <w n="15.5">ma</w> <w n="15.6">chair</w> <w n="15.7">a</w> <w n="15.8">frissonné</w>.</l>
							<l n="16" num="1.16"><w n="16.1">Quel</w> <w n="16.2">est</w> <w n="16.3">ce</w> <w n="16.4">bruit</w> <w n="16.5">lointain</w> ? <w n="16.6">Ah</w> ! <w n="16.7">L</w>’<w n="16.8">horloge</w> <w n="16.9">a</w> <w n="16.10">sonné</w> !</l>
							<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">la</w> <w n="17.3">page</w> <w n="17.4">est</w> <w n="17.5">encor</w> <w n="17.6">vierge</w>. <w n="17.7">Mon</w> <w n="17.8">corps</w> <w n="17.9">débile</w></l>
							<l n="18" num="1.18"><w n="18.1">Se</w> <w n="18.2">débat</w> <w n="18.3">sous</w> <w n="18.4">le</w> <w n="18.5">feu</w> <w n="18.6">d</w>’<w n="18.7">une</w> <w n="18.8">fièvre</w> <w n="18.9">stérile</w>.</l>
							<l n="19" num="1.19"><w n="19.1">J</w>’<w n="19.2">attends</w> <w n="19.3">en</w> <w n="19.4">vain</w> <w n="19.5">l</w>’<w n="19.6">idée</w> <w n="19.7">et</w> <w n="19.8">l</w>’<w n="19.9">inspiration</w>.</l>
							<l n="20" num="1.20"><w n="20.1">Comme</w> <w n="20.2">tu</w> <w n="20.3">me</w> <w n="20.4">mentais</w>, <w n="20.5">splendide</w> <w n="20.6">vision</w></l>
							<l n="21" num="1.21"><w n="21.1">Qui</w> <w n="21.2">venais</w> <w n="21.3">me</w> <w n="21.4">bercer</w> <w n="21.5">d</w>’<w n="21.6">une</w> <w n="21.7">espérance</w> <w n="21.8">vaine</w> !</l>
							<l n="22" num="1.22"><w n="22.1">Être</w> <w n="22.2">impuissant</w> ! <w n="22.3">N</w>’<w n="22.4">avoir</w> <w n="22.5">que</w> <w n="22.6">du</w> <w n="22.7">sang</w> <w n="22.8">dans</w> <w n="22.9">la</w> <w n="22.10">veine</w> !</l>
							<l n="23" num="1.23"><w n="23.1">Avoir</w> <w n="23.2">voulu</w> <w n="23.3">d</w>’<w n="23.4">un</w> <w n="23.5">mot</w> <w n="23.6">définir</w> <w n="23.7">l</w>’<w n="23.8">univers</w>,</l>
							<l n="24" num="1.24"><w n="24.1">Et</w> <w n="24.2">ne</w> <w n="24.3">pouvoir</w> <w n="24.4">trouver</w> <w n="24.5">l</w>’<w n="24.6">arrangement</w> <w n="24.7">d</w>’<w n="24.8">un</w> <w n="24.9">vers</w> !</l>
							<l n="25" num="1.25"><w n="25.1">Me</w> <w n="25.2">suis</w>-<w n="25.3">je</w> <w n="25.4">donc</w> <w n="25.5">mépris</w> ? <w n="25.6">Dans</w> <w n="25.7">mon</w> <w n="25.8">cœur</w> <w n="25.9">qui</w> <w n="25.10">ruisselle</w></l>
							<l n="26" num="1.26"><w n="26.1">Dieu</w> <w n="26.2">n</w>’<w n="26.3">avait</w>-<w n="26.4">il</w> <w n="26.5">pas</w> <w n="26.6">mis</w> <w n="26.7">la</w> <w n="26.8">sublime</w> <w n="26.9">étincelle</w> ?</l>
							<l n="27" num="1.27"><space quantity="2" unit="char"></space><w n="27.1">Oh</w> ! <w n="27.2">Si</w>, <w n="27.3">je</w> <w n="27.4">me</w> <w n="27.5">souviens</w>. <w n="27.6">En</w> <w n="27.7">mes</w> <w n="27.8">désirs</w> <w n="27.9">sans</w> <w n="27.10">frein</w>,</l>
							<l n="28" num="1.28"><w n="28.1">Enfant</w>, <w n="28.2">j</w>’<w n="28.3">ai</w> <w n="28.4">vu</w> <w n="28.5">de</w> <w n="28.6">près</w> <w n="28.7">les</w> <w n="28.8">colosses</w> <w n="28.9">d</w>’<w n="28.10">airain</w> ;</l>
							<l n="29" num="1.29"><w n="29.1">Je</w> <w n="29.2">cherchais</w> <w n="29.3">dans</w> <w n="29.4">la</w> <w n="29.5">forme</w> <w n="29.6">ardemment</w> <w n="29.7">fécondée</w></l>
							<l n="30" num="1.30"><w n="30.1">Le</w> <w n="30.2">moule</w> <w n="30.3">harmonieux</w> <w n="30.4">de</w> <w n="30.5">toute</w> <w n="30.6">large</w> <w n="30.7">idée</w> ;</l>
							<l n="31" num="1.31"><w n="31.1">J</w>’<w n="31.2">allais</w> <w n="31.3">aux</w> <w n="31.4">géants</w> <w n="31.5">grecs</w> <w n="31.6">demander</w> <w n="31.7">tour</w> <w n="31.8">à</w> <w n="31.9">tour</w></l>
							<l n="32" num="1.32"><w n="32.1">Quelle</w> <w n="32.2">grâce</w> <w n="32.3">polie</w> <w n="32.4">ou</w> <w n="32.5">quel</w> <w n="32.6">rude</w> <w n="32.7">contour</w></l>
							<l n="33" num="1.33"><w n="33.1">Fait</w> <w n="33.2">vivre</w> <w n="33.3">pour</w> <w n="33.4">les</w> <w n="33.5">yeux</w> <w n="33.6">la</w> <w n="33.7">synthèse</w> <w n="33.8">éternelle</w>.</l>
							<l n="34" num="1.34"><w n="34.1">Esprit</w> <w n="34.2">épouvanté</w>, <w n="34.3">je</w> <w n="34.4">me</w> <w n="34.5">perdais</w> <w n="34.6">en</w> <w n="34.7">elle</w>,</l>
							<l n="35" num="1.35"><w n="35.1">Tâchant</w> <w n="35.2">de</w> <w n="35.3">distinguer</w> <w n="35.4">dans</w> <w n="35.5">quels</w> <w n="35.6">vastes</w> <w n="35.7">accords</w></l>
							<l n="36" num="1.36"><w n="36.1">Se</w> <w n="36.2">fondent</w> <w n="36.3">les</w> <w n="36.4">splendeurs</w> <w n="36.5">des</w> <w n="36.6">âmes</w> <w n="36.7">et</w> <w n="36.8">des</w> <w n="36.9">corps</w>,</l>
							<l n="37" num="1.37"><w n="37.1">Et</w> <w n="37.2">méditant</w> <w n="37.3">déjà</w> <w n="37.4">comment</w> <w n="37.5">notre</w> <w n="37.6">génie</w></l>
							<l n="38" num="1.38"><w n="38.1">Impose</w> <w n="38.2">une</w> <w n="38.3">enveloppe</w> <w n="38.4">à</w> <w n="38.5">la</w> <w n="38.6">chose</w> <w n="38.7">infinie</w>.</l>
							<l n="39" num="1.39"><w n="39.1">Hélas</w> ! <w n="39.2">Amants</w> <w n="39.3">d</w>’<w n="39.4">un</w> <w n="39.5">soir</w>, <w n="39.6">en</w> <w n="39.7">vain</w> <w n="39.8">nous</w> <w n="39.9">enlaçons</w></l>
							<l n="40" num="1.40"><w n="40.1">La</w> <w n="40.2">morne</w> <w n="40.3">Galatée</w> <w n="40.4">et</w> <w n="40.5">ses</w> <w n="40.6">divins</w> <w n="40.7">glaçons</w>.</l>
							<l n="41" num="1.41"><w n="41.1">Pourquoi</w> <w n="41.2">m</w>’<w n="41.3">as</w>-<w n="41.4">tu</w> <w n="41.5">quitté</w>, <w n="41.6">muse</w> <w n="41.7">blanche</w> ? <w n="41.8">ô</w> <w n="41.9">ma</w> <w n="41.10">lyre</w> !</l>
							<l n="42" num="1.42"><w n="42.1">Quel</w> <w n="42.2">ouragan</w> <w n="42.3">t</w>’<w n="42.4">a</w> <w n="42.5">pris</w> <w n="42.6">ton</w> <w n="42.7">suave</w> <w n="42.8">délire</w> ?</l>
							<l n="43" num="1.43"><w n="43.1">Quelle</w> <w n="43.2">foudre</w> <w n="43.3">a</w> <w n="43.4">brisé</w> <w n="43.5">votre</w> <w n="43.6">prisme</w> <w n="43.7">éclatant</w>,</l>
							<l n="44" num="1.44"><w n="44.1">Ô</w> <w n="44.2">mes</w> <w n="44.3">illusions</w> <w n="44.4">de</w> <w n="44.5">jeunesse</w> ? <w n="44.6">Pourtant</w></l>
							<l n="45" num="1.45"><w n="45.1">J</w>’<w n="45.2">aime</w> <w n="45.3">encor</w> <w n="45.4">les</w> <w n="45.5">longs</w> <w n="45.6">bruits</w>, <w n="45.7">le</w> <w n="45.8">ciel</w> <w n="45.9">bleu</w>, <w n="45.10">le</w> <w n="45.11">vieil</w> <w n="45.12">arbre</w>,</l>
							<l n="46" num="1.46"><w n="46.1">Les</w> <w n="46.2">lointains</w> <w n="46.3">discordants</w>, <w n="46.4">et</w> <w n="46.5">ma</w> <w n="46.6">strophe</w> <w n="46.7">de</w> <w n="46.8">marbre</w></l>
							<l n="47" num="1.47"><w n="47.1">Sait</w> <w n="47.2">encor</w> <w n="47.3">rajeunir</w> <w n="47.4">la</w> <w n="47.5">grande</w> <w n="47.6">antiquité</w>.</l>
							<l n="48" num="1.48"><w n="48.1">Ô</w> <w n="48.2">muse</w> <w n="48.3">que</w> <w n="48.4">j</w>’<w n="48.5">aimais</w>, <w n="48.6">pourquoi</w> <w n="48.7">m</w>’<w n="48.8">as</w>-<w n="48.9">tu</w> <w n="48.10">quitté</w> ?</l>
							<l n="49" num="1.49"><w n="49.1">Pourquoi</w> <w n="49.2">ne</w> <w n="49.3">plus</w> <w n="49.4">venir</w> <w n="49.5">sur</w> <w n="49.6">ma</w> <w n="49.7">table</w> <w n="49.8">connue</w></l>
							<l n="50" num="1.50"><w n="50.1">Avec</w> <w n="50.2">tes</w> <w n="50.3">bras</w> <w n="50.4">nerveux</w> <w n="50.5">t</w>’<w n="50.6">accouder</w> <w n="50.7">chaste</w> <w n="50.8">et</w> <w n="50.9">nue</w> ?</l>
							<l n="51" num="1.51"><space quantity="2" unit="char"></space><w n="51.1">Jetons</w> <w n="51.2">les</w> <w n="51.3">yeux</w> <w n="51.4">sur</w> <w n="51.5">nous</w>, <w n="51.6">vieillards</w> <w n="51.7">anticipés</w>,</l>
							<l n="52" num="1.52"><w n="52.1">Cœurs</w> <w n="52.2">souillés</w> <w n="52.3">au</w> <w n="52.4">berceau</w>, <w n="52.5">parleurs</w> <w n="52.6">inoccupés</w> !</l>
							<l n="53" num="1.53"><w n="53.1">Ce</w> <w n="53.2">qui</w> <w n="53.3">nous</w> <w n="53.4">perdra</w> <w n="53.5">tous</w>, <w n="53.6">ce</w> <w n="53.7">qui</w> <w n="53.8">corrode</w> <w n="53.9">l</w>’<w n="53.10">âme</w>,</l>
							<l n="54" num="1.54"><w n="54.1">Ce</w> <w n="54.2">qui</w> <w n="54.3">dans</w> <w n="54.4">nos</w> <w n="54.5">cœurs</w> <w n="54.6">même</w> <w n="54.7">éteint</w> <w n="54.8">l</w>’<w n="54.9">ardente</w> <w n="54.10">flamme</w>,</l>
							<l n="55" num="1.55"><w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">notre</w> <w n="55.4">lâche</w> <w n="55.5">orgueil</w>, <w n="55.6">spectre</w> <w n="55.7">qui</w> <w n="55.8">devant</w> <w n="55.9">nous</w></l>
							<l n="56" num="1.56"><w n="56.1">Illumine</w> <w n="56.2">les</w> <w n="56.3">fronts</w> <w n="56.4">de</w> <w n="56.5">la</w> <w n="56.6">foule</w> <w n="56.7">à</w> <w n="56.8">genoux</w> ;</l>
							<l n="57" num="1.57"><w n="57.1">Le</w> <w n="57.2">poison</w> <w n="57.3">qui</w> <w n="57.4">décime</w> <w n="57.5">en</w> <w n="57.6">un</w> <w n="57.7">jour</w> <w n="57.8">nos</w> <w n="57.9">phalanges</w>,</l>
							<l n="58" num="1.58"><w n="58.1">C</w>’<w n="58.2">est</w> <w n="58.3">ce</w> <w n="58.4">désir</w> <w n="58.5">de</w> <w n="58.6">gloire</w> <w n="58.7">et</w> <w n="58.8">de</w> <w n="58.9">vaines</w> <w n="58.10">louanges</w></l>
							<l n="59" num="1.59"><w n="59.1">Qui</w> <w n="59.2">fait</w> <w n="59.3">bouillir</w> <w n="59.4">le</w> <w n="59.5">sang</w> <w n="59.6">vers</w> <w n="59.7">le</w> <w n="59.8">cœur</w> <w n="59.9">refoulé</w>.</l>
							<l n="60" num="1.60"><w n="60.1">Oh</w> ! <w n="60.2">Nous</w> <w n="60.3">avons</w> <w n="60.4">l</w>’<w n="60.5">orgueil</w> <w n="60.6">superbement</w> <w n="60.7">enflé</w>,</l>
							<l n="61" num="1.61"><w n="61.1">Nous</w> <w n="61.2">autres</w> ! <w n="61.3">Travailleurs</w> <w n="61.4">qui</w> <w n="61.5">voulons</w> <w n="61.6">le</w> <w n="61.7">salaire</w></l>
							<l n="62" num="1.62"><w n="62.1">Avant</w> <w n="62.2">l</w>’<w n="62.3">œuvre</w>, <w n="62.4">et</w> <w n="62.5">montrons</w> <w n="62.6">une</w> <w n="62.7">sainte</w> <w n="62.8">colère</w></l>
							<l n="63" num="1.63"><w n="63.1">Pour</w> <w n="63.2">saisir</w> <w n="63.3">les</w> <w n="63.4">lauriers</w> <w n="63.5">avant</w> <w n="63.6">la</w> <w n="63.7">lutte</w> ! <w n="63.8">Enfants</w></l>
							<l n="64" num="1.64"><w n="64.1">Qui</w>, <w n="64.2">le</w> <w n="64.3">cigare</w> <w n="64.4">en</w> <w n="64.5">main</w>, <w n="64.6">nous</w> <w n="64.7">rêvons</w> <w n="64.8">triomphants</w>,</l>
							<l n="65" num="1.65"><w n="65.1">Vierges</w> <w n="65.2">encor</w> <w n="65.3">du</w> <w n="65.4">glaive</w> <w n="65.5">et</w> <w n="65.6">du</w> <w n="65.7">champ</w> <w n="65.8">de</w> <w n="65.9">bataille</w> !</l>
							<l n="66" num="1.66"><w n="66.1">Nains</w> <w n="66.2">au</w> <w n="66.3">front</w> <w n="66.4">dédaigneux</w> <w n="66.5">qui</w> <w n="66.6">haussons</w> <w n="66.7">notre</w> <w n="66.8">taille</w></l>
							<l n="67" num="1.67"><w n="67.1">Sur</w> <w n="67.2">les</w> <w n="67.3">calculs</w> <w n="67.4">étroits</w> <w n="67.5">de</w> <w n="67.6">notre</w> <w n="67.7">ambition</w>,</l>
							<l n="68" num="1.68"><w n="68.1">Qui</w>, <w n="68.2">blasés</w> <w n="68.3">sans</w> <w n="68.4">avoir</w> <w n="68.5">connu</w> <w n="68.6">la</w> <w n="68.7">passion</w>,</l>
							<l n="69" num="1.69"><w n="69.1">Croyons</w> <w n="69.2">sentir</w> <w n="69.3">en</w> <w n="69.4">nous</w> <w n="69.5">cette</w> <w n="69.6">verve</w> <w n="69.7">stridente</w></l>
							<l n="70" num="1.70"><w n="70.1">Que</w> <w n="70.2">l</w>’<w n="70.3">enfer</w> <w n="70.4">avait</w> <w n="70.5">mis</w> <w n="70.6">dans</w> <w n="70.7">la</w> <w n="70.8">plume</w> <w n="70.9">du</w> <w n="70.10">Dante</w>,</l>
							<l n="71" num="1.71"><w n="71.1">Ou</w> <w n="71.2">le</w> <w n="71.3">doute</w> <w n="71.4">fatal</w> <w n="71.5">qui</w> <w n="71.6">réveillait</w> <w n="71.7">Byron</w>,</l>
							<l n="72" num="1.72"><w n="72.1">Comme</w> <w n="72.2">un</w> <w n="72.3">cheval</w> <w n="72.4">fouetté</w> <w n="72.5">par</w> <w n="72.6">le</w> <w n="72.7">vent</w> <w n="72.8">du</w> <w n="72.9">clairon</w> !</l>
							<l n="73" num="1.73"><space quantity="2" unit="char"></space><w n="73.1">Devant</w> <w n="73.2">nous</w> <w n="73.3">ont</w> <w n="73.4">passé</w> <w n="73.5">quelques</w> <w n="73.6">sombres</w> <w n="73.7">génies</w></l>
							<l n="74" num="1.74"><w n="74.1">Qui</w> <w n="74.2">vous</w> <w n="74.3">jetaient</w> <w n="74.4">aux</w> <w n="74.5">vents</w>, <w n="74.6">farouches</w> <w n="74.7">harmonies</w></l>
							<l n="75" num="1.75"><w n="75.1">Dont</w> <w n="75.2">nous</w> <w n="75.3">psalmodions</w> <w n="75.4">une</w> <w n="75.5">note</w> <w n="75.6">au</w> <w n="75.7">hasard</w> !</l>
							<l n="76" num="1.76"><w n="76.1">Tout</w> <w n="76.2">fiers</w> <w n="76.3">d</w>’<w n="76.4">avoir</w> <w n="76.5">produit</w> <w n="76.6">un</w> <w n="76.7">pastiche</w> <w n="76.8">bâtard</w>,</l>
							<l n="77" num="1.77"><w n="77.1">D</w>’<w n="77.2">avoir</w> <w n="77.3">éparpillé</w> <w n="77.4">quelques</w> <w n="77.5">syllabes</w> <w n="77.6">fortes</w>,</l>
							<l n="78" num="1.78"><w n="78.1">Fous</w>, <w n="78.2">ivres</w>, <w n="78.3">éperdus</w>, <w n="78.4">nous</w> <w n="78.5">assiégeons</w> <w n="78.6">les</w> <w n="78.7">portes</w></l>
							<l n="79" num="1.79"><w n="79.1">Des</w> <w n="79.2">panthéons</w> <w n="79.3">bâtis</w> <w n="79.4">pour</w> <w n="79.5">la</w> <w n="79.6">postérité</w> !</l>
							<l n="80" num="1.80"><w n="80.1">C</w>’<w n="80.2">est</w> <w n="80.3">un</w> <w n="80.4">aveuglement</w> <w n="80.5">risible</w> <w n="80.6">en</w> <w n="80.7">vérité</w> !</l>
							<l n="81" num="1.81"><space quantity="2" unit="char"></space><w n="81.1">Quand</w> <w n="81.2">nous</w> <w n="81.3">aurons</w> <w n="81.4">longtemps</w> <w n="81.5">sur</w> <w n="81.6">les</w> <w n="81.7">livres</w> <w n="81.8">antiques</w></l>
							<l n="82" num="1.82"><w n="82.1">Interrogé</w> <w n="82.2">le</w> <w n="82.3">sens</w> <w n="82.4">des</w> <w n="82.5">choses</w> <w n="82.6">prophétiques</w>,</l>
							<l n="83" num="1.83"><w n="83.1">Lu</w> <w n="83.2">sur</w> <w n="83.3">les</w> <w n="83.4">marbres</w> <w n="83.5">saints</w> <w n="83.6">d</w>’<w n="83.7">Égine</w> <w n="83.8">et</w> <w n="83.9">de</w> <w n="83.10">Paros</w></l>
							<l n="84" num="1.84"><w n="84.1">Le</w> <w n="84.2">sort</w> <w n="84.3">des</w> <w n="84.4">dieux</w>, <w n="84.5">jouet</w> <w n="84.6">mystérieux</w> <w n="84.7">d</w>’<w n="84.8">Éros</w> ;</l>
							<l n="85" num="1.85"><w n="85.1">Dans</w> <w n="85.2">le</w> <w n="85.3">livre</w> <w n="85.4">du</w> <w n="85.5">monde</w>, <w n="85.6">à</w> <w n="85.7">la</w> <w n="85.8">page</w> <w n="85.9">où</w> <w n="85.10">nous</w> <w n="85.11">sommes</w>,</l>
							<l n="86" num="1.86"><w n="86.1">Quand</w> <w n="86.2">nous</w> <w n="86.3">épellerons</w> <w n="86.4">le</w> <w n="86.5">noir</w> <w n="86.6">secret</w> <w n="86.7">des</w> <w n="86.8">hommes</w> ;</l>
							<l n="87" num="1.87"><w n="87.1">Quand</w> <w n="87.2">nous</w> <w n="87.3">aurons</w> <w n="87.4">usé</w> <w n="87.5">sans</w> <w n="87.6">relâche</w> <w n="87.7">nos</w> <w n="87.8">fronts</w></l>
							<l n="88" num="1.88"><w n="88.1">Sous</w> <w n="88.2">l</w>’<w n="88.3">étude</w>, <w n="88.4">et</w> <w n="88.5">non</w> <w n="88.6">pas</w> <w n="88.7">sous</w> <w n="88.8">de</w> <w n="88.9">justes</w> <w n="88.10">affronts</w>,</l>
							<l n="89" num="1.89"><w n="89.1">Ô</w> <w n="89.2">lutteurs</w>, <w n="89.3">nous</w> <w n="89.4">pourrons</w> <w n="89.5">de</w> <w n="89.6">notre</w> <w n="89.7">voix</w> <w n="89.8">profonde</w></l>
							<l n="90" num="1.90"><w n="90.1">Dire</w> <w n="90.2">au</w> <w n="90.3">monde</w> : <w n="90.4">c</w>’<w n="90.5">est</w> <w n="90.6">nous</w>, <w n="90.7">et</w> <w n="90.8">remuer</w> <w n="90.9">le</w> <w n="90.10">monde</w>.</l>
							<l n="91" num="1.91"><w n="91.1">Mais</w> <w n="91.2">jusque</w>-<w n="91.3">là</w>, <w n="91.4">sans</w> <w n="91.5">trêve</w>, <w n="91.6">aux</w> <w n="91.7">zoïles</w> <w n="91.8">méchants</w></l>
							<l n="92" num="1.92"><w n="92.1">Voilant</w> <w n="92.2">avec</w> <w n="92.3">amour</w> <w n="92.4">l</w>’<w n="92.5">ébauche</w> <w n="92.6">de</w> <w n="92.7">nos</w> <w n="92.8">chants</w>,</l>
							<l n="93" num="1.93"><w n="93.1">Étreignons</w> <w n="93.2">la</w> <w n="93.3">nature</w>, <w n="93.4">et</w> <w n="93.5">mesurons</w> <w n="93.6">sans</w> <w n="93.7">crainte</w></l>
							<l n="94" num="1.94"><w n="94.1">Ce</w> <w n="94.2">bas</w>-<w n="94.3">relief</w> <w n="94.4">géant</w> <w n="94.5">dont</w> <w n="94.6">nous</w> <w n="94.7">prenons</w> <w n="94.8">l</w>’<w n="94.9">empreinte</w> !</l>
						</lg>
					</div></body></text></TEI>