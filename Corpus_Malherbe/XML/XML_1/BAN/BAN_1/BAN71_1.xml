<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="BAN71">
					<head type="main">À UNE MUSE FOLLE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Allons</w>, <w n="1.2">insoucieuse</w>, <w n="1.3">ô</w> <w n="1.4">ma</w> <w n="1.5">folle</w> <w n="1.6">compagne</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Voici</w> <w n="2.2">que</w> <w n="2.3">l</w>’<w n="2.4">hiver</w> <w n="2.5">sombre</w> <w n="2.6">attriste</w> <w n="2.7">la</w> <w n="2.8">campagne</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Rentrons</w> <w n="3.2">fouler</w> <w n="3.3">tous</w> <w n="3.4">deux</w> <w n="3.5">les</w> <w n="3.6">splendides</w> <w n="3.7">coussins</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">C</w>’<w n="4.2">est</w> <w n="4.3">le</w> <w n="4.4">moment</w> <w n="4.5">de</w> <w n="4.6">voir</w> <w n="4.7">le</w> <w n="4.8">feu</w> <w n="4.9">briller</w> <w n="4.10">dans</w> <w n="4.11">l</w>’<w n="4.12">âtre</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">La</w> <w n="5.2">bise</w> <w n="5.3">vient</w> ; <w n="5.4">j</w>’<w n="5.5">ai</w> <w n="5.6">peur</w> <w n="5.7">de</w> <w n="5.8">son</w> <w n="5.9">baiser</w> <w n="5.10">bleuâtre</w></l>
						<l n="6" num="1.6"><space quantity="8" unit="char"></space><w n="6.1">Pour</w> <w n="6.2">la</w> <w n="6.3">peau</w> <w n="6.4">blanche</w> <w n="6.5">de</w> <w n="6.6">tes</w> <w n="6.7">seins</w>.</l>
						</lg>
						<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Allons</w> <w n="7.2">chercher</w> <w n="7.3">tous</w> <w n="7.4">deux</w> <w n="7.5">la</w> <w n="7.6">caresse</w> <w n="7.7">frileuse</w>.</l>
						<l n="8" num="2.2"><w n="8.1">Notre</w> <w n="8.2">lit</w> <w n="8.3">est</w> <w n="8.4">couvert</w> <w n="8.5">d</w>’<w n="8.6">une</w> <w n="8.7">étoffe</w> <w n="8.8">moelleuse</w> ;</l>
						<l n="9" num="2.3"><w n="9.1">Enroule</w> <w n="9.2">ma</w> <w n="9.3">pensée</w> <w n="9.4">à</w> <w n="9.5">tes</w> <w n="9.6">muscles</w> <w n="9.7">nerveux</w> ;</l>
						<l n="10" num="2.4"><w n="10.1">Ma</w> <w n="10.2">chère</w> <w n="10.3">âme</w> ! <w n="10.4">Trésor</w> <w n="10.5">de</w> <w n="10.6">la</w> <w n="10.7">race</w> <w n="10.8">d</w>’<w n="10.9">Hélène</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Verse</w> <w n="11.2">autour</w> <w n="11.3">de</w> <w n="11.4">mon</w> <w n="11.5">corps</w> <w n="11.6">l</w>’<w n="11.7">ambre</w> <w n="11.8">de</w> <w n="11.9">ton</w> <w n="11.10">haleine</w></l>
						<l n="12" num="2.6"><space quantity="8" unit="char"></space><w n="12.1">Et</w> <w n="12.2">le</w> <w n="12.3">manteau</w> <w n="12.4">de</w> <w n="12.5">tes</w> <w n="12.6">cheveux</w>.</l>
						</lg>
						<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Que</w> <w n="13.2">me</w> <w n="13.3">fait</w> <w n="13.4">cette</w> <w n="13.5">glace</w> <w n="13.6">aux</w> <w n="13.7">brillantes</w> <w n="13.8">arêtes</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Cette</w> <w n="14.2">neige</w> <w n="14.3">éternelle</w> <w n="14.4">utile</w> <w n="14.5">à</w> <w n="14.6">maints</w> <w n="14.7">poëtes</w></l>
						<l n="15" num="3.3"><w n="15.1">Et</w> <w n="15.2">ce</w> <w n="15.3">vieil</w> <w n="15.4">ouragan</w> <w n="15.5">au</w> <w n="15.6">blasphème</w> <w n="15.7">hagard</w> ?</l>
						<l n="16" num="3.4"><w n="16.1">Moi</w>, <w n="16.2">j</w>’<w n="16.3">aurai</w> <w n="16.4">l</w>’<w n="16.5">ouragan</w> <w n="16.6">dans</w> <w n="16.7">l</w>’<w n="16.8">onde</w> <w n="16.9">où</w> <w n="16.10">tu</w> <w n="16.11">te</w> <w n="16.12">joues</w>,</l>
						<l n="17" num="3.5"><w n="17.1">La</w> <w n="17.2">glace</w> <w n="17.3">dans</w> <w n="17.4">ton</w> <w n="17.5">cœur</w>, <w n="17.6">la</w> <w n="17.7">neige</w> <w n="17.8">sur</w> <w n="17.9">tes</w> <w n="17.10">joues</w>,</l>
						<l n="18" num="3.6"><space quantity="8" unit="char"></space><w n="18.1">Et</w> <w n="18.2">l</w>’<w n="18.3">arc</w>-<w n="18.4">en</w>-<w n="18.5">ciel</w> <w n="18.6">dans</w> <w n="18.7">ton</w> <w n="18.8">regard</w>.</l>
						</lg>
						<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Il</w> <w n="19.2">faudrait</w> <w n="19.3">n</w>’<w n="19.4">avoir</w> <w n="19.5">pas</w> <w n="19.6">de</w> <w n="19.7">bonnes</w> <w n="19.8">chambres</w> <w n="19.9">closes</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Pour</w> <w n="20.2">chercher</w> <w n="20.3">en</w> <w n="20.4">janvier</w> <w n="20.5">des</w> <w n="20.6">strophes</w> <w n="20.7">et</w> <w n="20.8">des</w> <w n="20.9">roses</w>.</l>
						<l n="21" num="4.3"><w n="21.1">Les</w> <w n="21.2">vers</w> <w n="21.3">en</w> <w n="21.4">ce</w> <w n="21.5">temps</w>-<w n="21.6">là</w> <w n="21.7">sont</w> <w n="21.8">de</w> <w n="21.9">méchants</w> <w n="21.10">fardeaux</w>.</l>
						<l n="22" num="4.4"><w n="22.1">Si</w> <w n="22.2">nous</w> <w n="22.3">ne</w> <w n="22.4">trouvons</w> <w n="22.5">plus</w> <w n="22.6">les</w> <w n="22.7">roses</w> <w n="22.8">que</w> <w n="22.9">tu</w> <w n="22.10">sèmes</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Au</w> <w n="23.2">lieu</w> <w n="23.3">d</w>’<w n="23.4">user</w> <w n="23.5">nos</w> <w n="23.6">voix</w> <w n="23.7">à</w> <w n="23.8">chanter</w> <w n="23.9">des</w> <w n="23.10">poëmes</w>,</l>
						<l n="24" num="4.6"><space quantity="8" unit="char"></space><w n="24.1">Nous</w> <w n="24.2">en</w> <w n="24.3">ferons</w> <w n="24.4">sous</w> <w n="24.5">les</w> <w n="24.6">rideaux</w>.</l>
						</lg>
						<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Tandis</w> <w n="25.2">que</w> <w n="25.3">la</w> <w n="25.4">Naïade</w> <w n="25.5">interrompt</w> <w n="25.6">son</w> <w n="25.7">murmure</w></l>
						<l n="26" num="5.2"><w n="26.1">Et</w> <w n="26.2">que</w> <w n="26.3">ses</w> <w n="26.4">tristes</w> <w n="26.5">flots</w> <w n="26.6">lui</w> <w n="26.7">prêtent</w> <w n="26.8">pour</w> <w n="26.9">armure</w></l>
						<l n="27" num="5.3"><w n="27.1">Leurs</w> <w n="27.2">glaçons</w> <w n="27.3">transparents</w> <w n="27.4">faits</w> <w n="27.5">de</w> <w n="27.6">cristal</w> <w n="27.7">ouvré</w>,</l>
						<l n="28" num="5.4"><w n="28.1">Échevelés</w> <w n="28.2">tous</w> <w n="28.3">deux</w> <w n="28.4">sur</w> <w n="28.5">la</w> <w n="28.6">couche</w> <w n="28.7">défaite</w>,</l>
						<l n="29" num="5.5"><w n="29.1">Nous</w> <w n="29.2">puiserons</w> <w n="29.3">les</w> <w n="29.4">vins</w>, <w n="29.5">pleurs</w> <w n="29.6">du</w> <w n="29.7">soleil</w> <w n="29.8">en</w> <w n="29.9">fête</w>,</l>
						<l n="30" num="5.6"><space quantity="8" unit="char"></space><w n="30.1">Dans</w> <w n="30.2">un</w> <w n="30.3">grand</w> <w n="30.4">cratère</w> <w n="30.5">doré</w>.</l>
						</lg>
						<lg n="6">
						<l n="31" num="6.1"><w n="31.1">À</w> <w n="31.2">nous</w> <w n="31.3">les</w> <w n="31.4">arbres</w> <w n="31.5">morts</w> <w n="31.6">luttant</w> <w n="31.7">avec</w> <w n="31.8">la</w> <w n="31.9">flamme</w></l>
						<l n="32" num="6.2"><w n="32.1">Les</w> <w n="32.2">tapis</w> <w n="32.3">variés</w> <w n="32.4">qui</w> <w n="32.5">réjouissent</w> <w n="32.6">l</w>’<w n="32.7">âme</w>,</l>
						<l n="33" num="6.3"><w n="33.1">Et</w> <w n="33.2">les</w> <w n="33.3">divans</w>, <w n="33.4">profonds</w> <w n="33.5">à</w> <w n="33.6">nous</w> <w n="33.7">anéantir</w> !</l>
						<l n="34" num="6.4"><w n="34.1">Nous</w> <w n="34.2">nous</w> <w n="34.3">préserverons</w> <w n="34.4">de</w> <w n="34.5">toute</w> <w n="34.6">rude</w> <w n="34.7">atteinte</w></l>
						<l n="35" num="6.5"><w n="35.1">Sous</w> <w n="35.2">des</w> <w n="35.3">voiles</w> <w n="35.4">épais</w> <w n="35.5">de</w> <w n="35.6">pourpre</w> <w n="35.7">trois</w> <w n="35.8">fois</w> <w n="35.9">teinte</w></l>
						<l n="36" num="6.6"><space quantity="8" unit="char"></space><w n="36.1">Que</w> <w n="36.2">signerait</w> <w n="36.3">l</w>’<w n="36.4">ancienne</w> <w n="36.5">Tyr</w>.</l>
						</lg>
						<lg n="7">
						<l n="37" num="7.1"><w n="37.1">À</w> <w n="37.2">nous</w> <w n="37.3">les</w> <w n="37.4">lambris</w> <w n="37.5">d</w>’<w n="37.6">or</w> <w n="37.7">illuminant</w> <w n="37.8">les</w> <w n="37.9">salles</w>,</l>
						<l n="38" num="7.2"><w n="38.1">À</w> <w n="38.2">nous</w> <w n="38.3">les</w> <w n="38.4">contes</w> <w n="38.5">bleus</w> <w n="38.6">des</w> <w n="38.7">nuits</w> <w n="38.8">orientales</w>,</l>
						<l n="39" num="7.3"><w n="39.1">Caprices</w> <w n="39.2">pailletés</w> <w n="39.3">que</w> <w n="39.4">l</w>’<w n="39.5">on</w> <w n="39.6">brode</w> <w n="39.7">en</w> <w n="39.8">fumant</w>,</l>
						<l n="40" num="7.4"><w n="40.1">Et</w> <w n="40.2">le</w> <w n="40.3">loisir</w> <w n="40.4">sans</w> <w n="40.5">fin</w> <w n="40.6">des</w> <w n="40.7">molles</w> <w n="40.8">cigarettes</w></l>
						<l n="41" num="7.5"><w n="41.1">Que</w> <w n="41.2">le</w> <w n="41.3">feu</w> <w n="41.4">caressant</w> <w n="41.5">pare</w> <w n="41.6">de</w> <w n="41.7">collerettes</w></l>
						<l n="42" num="7.6"><space quantity="8" unit="char"></space><w n="42.1">Où</w> <w n="42.2">brille</w> <w n="42.3">un</w> <w n="42.4">rouge</w> <w n="42.5">diamant</w> !</l>
						</lg>
						<lg n="8">
						<l n="43" num="8.1"><w n="43.1">Ainsi</w> <w n="43.2">pour</w> <w n="43.3">de</w> <w n="43.4">longs</w> <w n="43.5">jours</w> <w n="43.6">suspendus</w> <w n="43.7">notre</w> <w n="43.8">lyre</w> ;</l>
						<l n="44" num="8.2"><w n="44.1">Aimons</w>-<w n="44.2">nous</w> ; <w n="44.3">oublions</w> <w n="44.4">que</w> <w n="44.5">nous</w> <w n="44.6">avons</w> <w n="44.7">su</w> <w n="44.8">lire</w> !</l>
						<l n="45" num="8.3"><w n="45.1">Que</w> <w n="45.2">le</w> <w n="45.3">vieux</w> <w n="45.4">goût</w> <w n="45.5">romain</w> <w n="45.6">préside</w> <w n="45.7">à</w> <w n="45.8">nos</w> <w n="45.9">repas</w> !</l>
						<l n="46" num="8.4"><w n="46.1">Apprenons</w> <w n="46.2">à</w> <w n="46.3">nous</w> <w n="46.4">deux</w> <w n="46.5">comme</w> <w n="46.6">il</w> <w n="46.7">est</w> <w n="46.8">bon</w> <w n="46.9">de</w> <w n="46.10">vivre</w>,</l>
						<l n="47" num="8.5"><w n="47.1">Faisons</w> <w n="47.2">nos</w> <w n="47.3">plus</w> <w n="47.4">doux</w> <w n="47.5">chants</w> <w n="47.6">et</w> <w n="47.7">notre</w> <w n="47.8">plus</w> <w n="47.9">beau</w> <w n="47.10">livre</w>,</l>
						<l n="48" num="8.6"><space quantity="8" unit="char"></space><w n="48.1">Le</w> <w n="48.2">livre</w> <w n="48.3">que</w> <w n="48.4">l</w>’<w n="48.5">on</w> <w n="48.6">n</w>’<w n="48.7">écrit</w> <w n="48.8">pas</w>.</l>
						</lg>
						<lg n="9">
						<l n="49" num="9.1"><w n="49.1">Tressaille</w> <w n="49.2">mollement</w> <w n="49.3">sous</w> <w n="49.4">la</w> <w n="49.5">main</w> <w n="49.6">qui</w> <w n="49.7">te</w> <w n="49.8">flatte</w>.</l>
						<l n="50" num="9.2"><w n="50.1">Quand</w> <w n="50.2">le</w> <w n="50.3">tendre</w> <w n="50.4">lilas</w>, <w n="50.5">le</w> <w n="50.6">vert</w> <w n="50.7">et</w> <w n="50.8">l</w>’<w n="50.9">écarlate</w>,</l>
						<l n="51" num="9.3"><w n="51.1">L</w>’<w n="51.2">azur</w> <w n="51.3">délicieux</w>, <w n="51.4">l</w>’<w n="51.5">ivoire</w> <w n="51.6">aux</w> <w n="51.7">fiers</w> <w n="51.8">dédains</w>,</l>
						<l n="52" num="9.4"><w n="52.1">Le</w> <w n="52.2">jaune</w> <w n="52.3">fleur</w> <w n="52.4">de</w> <w n="52.5">soufre</w> <w n="52.6">aimé</w> <w n="52.7">de</w> <w n="52.8">Véronèse</w></l>
						<l n="53" num="9.5"><w n="53.1">Et</w> <w n="53.2">le</w> <w n="53.3">rose</w> <w n="53.4">du</w> <w n="53.5">feu</w> <w n="53.6">qui</w> <w n="53.7">rougit</w> <w n="53.8">la</w> <w n="53.9">fournaise</w></l>
						<l n="54" num="9.6"><space quantity="8" unit="char"></space><w n="54.1">Éclateront</w> <w n="54.2">sur</w> <w n="54.3">les</w> <w n="54.4">jardins</w>,</l>
						</lg>
						<lg n="10">
						<l n="55" num="10.1"><w n="55.1">Nous</w> <w n="55.2">irons</w> <w n="55.3">découvrir</w> <w n="55.4">aussi</w> <w n="55.5">notre</w> <w n="55.6">Amérique</w> !</l>
						<l n="56" num="10.2"><w n="56.1">L</w>’<w n="56.2">Eldorado</w> <w n="56.3">rêvé</w>, <w n="56.4">le</w> <w n="56.5">pays</w> <w n="56.6">chimérique</w></l>
						<l n="57" num="10.3"><w n="57.1">Où</w> <w n="57.2">l</w>’<w n="57.3">Ondine</w> <w n="57.4">aux</w> <w n="57.5">yeux</w> <w n="57.6">bleus</w> <w n="57.7">sort</w> <w n="57.8">du</w> <w n="57.9">lac</w> <w n="57.10">en</w> <w n="57.11">songeant</w>,</l>
						<l n="58" num="10.4"><w n="58.1">Où</w> <w n="58.2">pour</w> <w n="58.3">Titania</w> <w n="58.4">la</w> <w n="58.5">perle</w> <w n="58.6">noire</w> <w n="58.7">abonde</w>,</l>
						<l n="59" num="10.5"><w n="59.1">Où</w> <w n="59.2">près</w> <w n="59.3">d</w>’<w n="59.4">Hérodiade</w> <w n="59.5">avec</w> <w n="59.6">la</w> <w n="59.7">fée</w> <w n="59.8">Habonde</w></l>
						<l n="60" num="10.6"><space quantity="8" unit="char"></space><w n="60.1">Chasse</w> <w n="60.2">Diane</w> <w n="60.3">au</w> <w n="60.4">front</w> <w n="60.5">d</w>’<w n="60.6">argent</w> !</l>
						</lg>
						<lg n="11">
						<l n="61" num="11.1"><w n="61.1">Mais</w> <w n="61.2">pour</w> <w n="61.3">l</w>’<w n="61.4">heure</w> <w n="61.5">qu</w>’<w n="61.6">il</w> <w n="61.7">est</w>, <w n="61.8">sur</w> <w n="61.9">nos</w> <w n="61.10">vitres</w> <w n="61.11">gothiques</w></l>
						<l n="62" num="11.2"><w n="62.1">Brillent</w> <w n="62.2">des</w> <w n="62.3">fleurs</w> <w n="62.4">de</w> <w n="62.5">givre</w> <w n="62.6">et</w> <w n="62.7">des</w> <w n="62.8">lys</w> <w n="62.9">fantastiques</w> ;</l>
						<l n="63" num="11.3"><w n="63.1">Tu</w> <w n="63.2">soupires</w> <w n="63.3">des</w> <w n="63.4">mots</w> <w n="63.5">qui</w> <w n="63.6">ne</w> <w n="63.7">sont</w> <w n="63.8">pas</w> <w n="63.9">des</w> <w n="63.10">chants</w>,</l>
						<l n="64" num="11.4"><w n="64.1">Et</w> <w n="64.2">tes</w> <w n="64.3">beaux</w> <w n="64.4">seins</w> <w n="64.5">polis</w>, <w n="64.6">plus</w> <w n="64.7">blancs</w> <w n="64.8">que</w> <w n="64.9">deux</w> <w n="64.10">étoiles</w>,</l>
						<l n="65" num="11.5"><w n="65.1">Ont</w> <w n="65.2">l</w>’<w n="65.3">air</w>, <w n="65.4">à</w> <w n="65.5">la</w> <w n="65.6">façon</w> <w n="65.7">dont</w> <w n="65.8">ils</w> <w n="65.9">tordent</w> <w n="65.10">leurs</w> <w n="65.11">voiles</w>,</l>
						<l n="66" num="11.6"><space quantity="8" unit="char"></space><w n="66.1">De</w> <w n="66.2">vouloir</w> <w n="66.3">s</w>’<w n="66.4">en</w> <w n="66.5">aller</w> <w n="66.6">aux</w> <w n="66.7">champs</w>.</l>
						</lg>
						<lg n="12">
						<l n="67" num="12.1"><w n="67.1">Donc</w>, <w n="67.2">fais</w> <w n="67.3">la</w> <w n="67.4">révérence</w> <w n="67.5">au</w> <w n="67.6">lecteur</w> <w n="67.7">qui</w> <w n="67.8">savoure</w></l>
						<l n="68" num="12.2"><w n="68.1">Peut</w>-<w n="68.2">être</w> <w n="68.3">avec</w> <w n="68.4">plaisir</w>, <w n="68.5">mais</w> <w n="68.6">non</w> <w n="68.7">pas</w> <w n="68.8">sans</w> <w n="68.9">bravoure</w>,</l>
						<l n="69" num="12.3"><w n="69.1">Tes</w> <w n="69.2">délires</w> <w n="69.3">de</w> <w n="69.4">muse</w> <w n="69.5">et</w> <w n="69.6">mes</w> <w n="69.7">rêves</w> <w n="69.8">de</w> <w n="69.9">fou</w>,</l>
						<l n="70" num="12.4"><w n="70.1">Et</w>, <w n="70.2">comme</w> <w n="70.3">en</w> <w n="70.4">te</w> <w n="70.5">courbant</w> <w n="70.6">dans</w> <w n="70.7">un</w> <w n="70.8">adieu</w> <w n="70.9">suprême</w>,</l>
						<l n="71" num="12.5"><w n="71.1">Jette</w>-<w n="71.2">lui</w>, <w n="71.3">si</w> <w n="71.4">tu</w> <w n="71.5">veux</w>, <w n="71.6">pour</w> <w n="71.7">ton</w> <w n="71.8">meilleur</w> <w n="71.9">poëme</w>,</l>
						<l n="72" num="12.6"><space quantity="8" unit="char"></space><w n="72.1">Tes</w> <w n="72.2">bras</w> <w n="72.3">de</w> <w n="72.4">femme</w> <w n="72.5">autour</w> <w n="72.6">du</w> <w n="72.7">cou</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1842">janvier 1842</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>