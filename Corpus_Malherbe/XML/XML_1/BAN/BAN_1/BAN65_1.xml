<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><head type="main_subpart">EN HABIT ZINZOLIN</head><div type="poem" key="BAN65">
						<head type="number">III</head>
						<head type="main">RONDEAU, À ISMÈNE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Oui</w>, <w n="1.2">pour</w> <w n="1.3">le</w> <w n="1.4">moins</w>, <w n="1.5">laissez</w>-<w n="1.6">moi</w>, <w n="1.7">jeune</w> <w n="1.8">Ismène</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Pleurer</w> <w n="2.2">tout</w> <w n="2.3">bas</w> ; <w n="2.4">si</w> <w n="2.5">jamais</w>, <w n="2.6">inhumaine</w>,</l>
							<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">osais</w> <w n="3.3">vous</w> <w n="3.4">peindre</w> <w n="3.5">avec</w> <w n="3.6">de</w> <w n="3.7">vrais</w> <w n="3.8">accents</w></l>
							<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">feu</w> <w n="4.3">caché</w> <w n="4.4">qu</w>’<w n="4.5">en</w> <w n="4.6">mes</w> <w n="4.7">veines</w> <w n="4.8">je</w> <w n="4.9">sens</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Vous</w> <w n="5.2">gémiriez</w>, <w n="5.3">cruelle</w>, <w n="5.4">de</w> <w n="5.5">ma</w> <w n="5.6">peine</w>.</l>
						</lg>
						<lg n="2">
							<l n="6" num="2.1"><w n="6.1">Par</w> <w n="6.2">ce</w> <w n="6.3">récit</w>, <w n="6.4">l</w>’<w n="6.5">aventure</w> <w n="6.6">est</w> <w n="6.7">certaine</w>,</l>
							<l n="7" num="2.2"><w n="7.1">Je</w> <w n="7.2">changerais</w> <w n="7.3">en</w> <w n="7.4">amour</w> <w n="7.5">votre</w> <w n="7.6">haine</w>,</l>
							<l n="8" num="2.3"><w n="8.1">Votre</w> <w n="8.2">froideur</w> <w n="8.3">en</w> <w n="8.4">désirs</w> <w n="8.5">bien</w> <w n="8.6">pressants</w>,</l>
							<l n="9" num="2.4"><space quantity="12" unit="char"></space><w n="9.1">Oui</w>, <w n="9.2">pour</w> <w n="9.3">le</w> <w n="9.4">moins</w>.</l>
						</lg>
						<lg n="3">
							<l n="10" num="3.1"><w n="10.1">Échevelée</w> <w n="10.2">alors</w>, <w n="10.3">ma</w> <w n="10.4">blonde</w> <w n="10.5">reine</w>,</l>
							<l n="11" num="3.2"><w n="11.1">Vos</w> <w n="11.2">bras</w> <w n="11.3">de</w> <w n="11.4">lys</w> <w n="11.5">me</w> <w n="11.6">feraient</w> <w n="11.7">une</w> <w n="11.8">chaîne</w>,</l>
							<l n="12" num="3.3"><w n="12.1">Et</w> <w n="12.2">les</w> <w n="12.3">baisers</w> <w n="12.4">des</w> <w n="12.5">baisers</w> <w n="12.6">renaissants</w></l>
							<l n="13" num="3.4"><w n="13.1">M</w>’<w n="13.2">enivreraient</w> <w n="13.3">de</w> <w n="13.4">leurs</w> <w n="13.5">charmes</w> <w n="13.6">puissants</w> ;</l>
							<l n="14" num="3.5"><w n="14.1">Vous</w> <w n="14.2">veilleriez</w> <w n="14.3">avec</w> <w n="14.4">moi</w> <w n="14.5">la</w> <w n="14.6">nuit</w> <w n="14.7">pleine</w>,</l>
							<l n="15" num="3.6"><space quantity="12" unit="char"></space><w n="15.1">Oui</w>, <w n="15.2">pour</w> <w n="15.3">le</w> <w n="15.4">moins</w>.</l>
						</lg>
					</div></body></text></TEI>