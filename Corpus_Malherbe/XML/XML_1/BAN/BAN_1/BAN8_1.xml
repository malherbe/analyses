<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">AMOURS D’ÉLISE</head><div type="poem" key="BAN8">
					<head type="number">III</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Oui</w>, <w n="1.2">mon</w> <w n="1.3">cœur</w> <w n="1.4">et</w> <w n="1.5">ma</w> <w n="1.6">vie</w> !</l>
						<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">Et</w> <w n="2.2">je</w> <w n="2.3">sais</w> <w n="2.4">bien</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ô</w> <w n="3.2">chère</w> <w n="3.3">inassouvie</w>,</l>
						<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">Que</w> <w n="4.2">ce</w> <w n="4.3">n</w>’<w n="4.4">est</w> <w n="4.5">rien</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ah</w> ! <w n="5.2">Si</w> <w n="5.3">j</w>’<w n="5.4">étais</w> <w n="5.5">la</w> <w n="5.6">rose</w></l>
						<l n="6" num="2.2"><space quantity="4" unit="char"></space><w n="6.1">Que</w> <w n="6.2">le</w> <w n="6.3">soir</w> <w n="6.4">brun</w></l>
						<l n="7" num="2.3"><w n="7.1">En</w> <w n="7.2">souriant</w> <w n="7.3">arrose</w></l>
						<l n="8" num="2.4"><space quantity="4" unit="char"></space><w n="8.1">D</w>’<w n="8.2">un</w> <w n="8.3">doux</w> <w n="8.4">parfum</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Si</w> <w n="9.2">j</w>’<w n="9.3">étais</w> <w n="9.4">le</w> <w n="9.5">bois</w> <w n="9.6">sombre</w></l>
						<l n="10" num="3.2"><space quantity="4" unit="char"></space><w n="10.1">Qui</w> <w n="10.2">sur</w> <w n="10.3">les</w> <w n="10.4">champs</w></l>
						<l n="11" num="3.3"><w n="11.1">Jette</w> <w n="11.2">au</w> <w n="11.3">loin</w> <w n="11.4">sa</w> <w n="11.5">grande</w> <w n="11.6">ombre</w></l>
						<l n="12" num="3.4"><space quantity="4" unit="char"></space><w n="12.1">Et</w> <w n="12.2">ses</w> <w n="12.3">doux</w> <w n="12.4">chants</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ou</w> <w n="13.2">l</w>’<w n="13.3">onde</w> <w n="13.4">triomphale</w></l>
						<l n="14" num="4.2"><space quantity="4" unit="char"></space><w n="14.1">D</w>’<w n="14.2">où</w> <w n="14.3">le</w> <w n="14.4">soleil</w></l>
						<l n="15" num="4.3"><w n="15.1">Sur</w> <w n="15.2">son</w> <w n="15.3">beau</w> <w n="15.4">char</w> <w n="15.5">d</w>’<w n="15.6">opale</w></l>
						<l n="16" num="4.4"><space quantity="4" unit="char"></space><w n="16.1">S</w>’<w n="16.2">enfuit</w> <w n="16.3">vermeil</w> ;</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Si</w> <w n="17.2">j</w>’<w n="17.3">étais</w> <w n="17.4">la</w> <w n="17.5">pervenche</w></l>
						<l n="18" num="5.2"><space quantity="4" unit="char"></space><w n="18.1">Ou</w> <w n="18.2">les</w> <w n="18.3">roseaux</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Ou</w> <w n="19.2">le</w> <w n="19.3">lac</w>, <w n="19.4">ou</w> <w n="19.5">la</w> <w n="19.6">branche</w></l>
						<l n="20" num="5.4"><space quantity="4" unit="char"></space><w n="20.1">Pleine</w> <w n="20.2">d</w>’<w n="20.3">oiseaux</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Ou</w> <w n="21.2">l</w>’<w n="21.3">étoile</w> <w n="21.4">qui</w> <w n="21.5">marche</w></l>
						<l n="22" num="6.2"><space quantity="4" unit="char"></space><w n="22.1">Dans</w> <w n="22.2">un</w> <w n="22.3">ciel</w> <w n="22.4">pur</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Ou</w> <w n="23.2">le</w> <w n="23.3">vieux</w> <w n="23.4">pont</w> <w n="23.5">d</w>’<w n="23.6">une</w> <w n="23.7">arche</w></l>
						<l n="24" num="6.4"><space quantity="4" unit="char"></space><w n="24.1">Au</w> <w n="24.2">profil</w> <w n="24.3">dur</w> ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Si</w> <w n="25.2">j</w>’<w n="25.3">étais</w> <w n="25.4">la</w> <w n="25.5">voix</w> <w n="25.6">pleine</w>,</l>
						<l n="26" num="7.2"><space quantity="4" unit="char"></space><w n="26.1">La</w> <w n="26.2">voix</w> <w n="26.3">des</w> <w n="26.4">cors</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Qui</w> <w n="27.2">fait</w> <w n="27.3">bondir</w> <w n="27.4">la</w> <w n="27.5">plaine</w></l>
						<l n="28" num="7.4"><space quantity="4" unit="char"></space><w n="28.1">À</w> <w n="28.2">ses</w> <w n="28.3">accords</w>,</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Ou</w> <w n="29.2">la</w> <w n="29.3">nymphe</w> <w n="29.4">du</w> <w n="29.5">saule</w></l>
						<l n="30" num="8.2"><space quantity="4" unit="char"></space><w n="30.1">Au</w> <w n="30.2">sein</w> <w n="30.3">nerveux</w></l>
						<l n="31" num="8.3"><w n="31.1">Qui</w> <w n="31.2">met</w> <w n="31.3">sur</w> <w n="31.4">son</w> <w n="31.5">épaule</w></l>
						<l n="32" num="8.4"><space quantity="4" unit="char"></space><w n="32.1">Ses</w> <w n="32.2">longs</w> <w n="32.3">cheveux</w> ;</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">À</w> <w n="33.2">vous</w>, <w n="33.3">ô</w> <w n="33.4">charmeresse</w></l>
						<l n="34" num="9.2"><space quantity="4" unit="char"></space><w n="34.1">Pleine</w> <w n="34.2">d</w>’<w n="34.3">attraits</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Élise</w>, <w n="35.2">à</w> <w n="35.3">vous</w>, <w n="35.4">sans</w> <w n="35.5">cesse</w></l>
						<l n="36" num="9.4"><space quantity="4" unit="char"></space><w n="36.1">Je</w> <w n="36.2">donnerais</w></l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Ma</w> <w n="37.2">voix</w>, <w n="37.3">ma</w> <w n="37.4">fleur</w>, <w n="37.5">mon</w> <w n="37.6">ombre</w></l>
						<l n="38" num="10.2"><space quantity="4" unit="char"></space><w n="38.1">Douce</w> <w n="38.2">à</w> <w n="38.3">chacun</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Mes</w> <w n="39.2">chants</w>, <w n="39.3">mes</w> <w n="39.4">bruits</w> <w n="39.5">sans</w> <w n="39.6">nombre</w></l>
						<l n="40" num="10.4"><space quantity="4" unit="char"></space><w n="40.1">Et</w> <w n="40.2">mon</w> <w n="40.3">parfum</w>,</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Et</w> <w n="41.2">tout</w> <w n="41.3">ce</w> <w n="41.4">qui</w> <w n="41.5">vous</w> <w n="41.6">fête</w></l>
						<l n="42" num="11.2"><space quantity="4" unit="char"></space><w n="42.1">Comme</w> <w n="42.2">une</w> <w n="42.3">sœur</w>.</l>
						<l n="43" num="11.3"><w n="43.1">Mais</w> <w n="43.2">je</w> <w n="43.3">suis</w> <w n="43.4">un</w> <w n="43.5">poëte</w></l>
						<l n="44" num="11.4"><space quantity="4" unit="char"></space><w n="44.1">Plein</w> <w n="44.2">de</w> <w n="44.3">douceur</w>,</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Qui</w> <w n="45.2">ne</w> <w n="45.3">sait</w> <w n="45.4">que</w> <w n="45.5">bruire</w></l>
						<l n="46" num="12.2"><space quantity="4" unit="char"></space><w n="46.1">À</w> <w n="46.2">tous</w> <w n="46.3">les</w> <w n="46.4">bruits</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Faire</w> <w n="47.2">vibrer</w> <w n="47.3">sa</w> <w n="47.4">lyre</w></l>
						<l n="48" num="12.4"><space quantity="4" unit="char"></space><w n="48.1">Au</w> <w n="48.2">vent</w> <w n="48.3">des</w> <w n="48.4">nuits</w>,</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Ou</w>, <w n="49.2">quand</w> <w n="49.3">le</w> <w n="49.4">jour</w> <w n="49.5">se</w> <w n="49.6">lève</w></l>
						<l n="50" num="13.2"><space quantity="4" unit="char"></space><w n="50.1">Tout</w> <w n="50.2">azuré</w>,</l>
						<l n="51" num="13.3"><w n="51.1">S</w>’<w n="51.2">envoler</w> <w n="51.3">dans</w> <w n="51.4">un</w> <w n="51.5">rêve</w></l>
						<l n="52" num="13.4"><space quantity="4" unit="char"></space><w n="52.1">Démesuré</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Donc</w>, <w n="53.2">je</w> <w n="53.3">vous</w> <w n="53.4">ai</w> <w n="53.5">servie</w>,</l>
						<l n="54" num="14.2"><space quantity="4" unit="char"></space><w n="54.1">Heureux</w> <w n="54.2">encor</w></l>
						<l n="55" num="14.3"><w n="55.1">De</w> <w n="55.2">vous</w> <w n="55.3">donner</w> <w n="55.4">ma</w> <w n="55.5">vie</w>,</l>
						<l n="56" num="14.4"><space quantity="4" unit="char"></space><w n="56.1">Cette</w> <w n="56.2">fleur</w> <w n="56.3">d</w>’<w n="56.4">or</w></l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Que</w> <w n="57.2">tourmente</w> <w n="57.3">et</w> <w n="57.4">caresse</w></l>
						<l n="58" num="15.2"><space quantity="4" unit="char"></space><w n="58.1">Dans</w> <w n="58.2">un</w> <w n="58.3">rayon</w></l>
						<l n="59" num="15.3"><w n="59.1">La</w> <w n="59.2">frivole</w> <w n="59.3">déesse</w></l>
						<l n="60" num="15.4"><space quantity="4" unit="char"></space><w n="60.1">Illusion</w> ;</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Mon</w> <w n="61.2">esprit</w>, <w n="61.3">qui</w> <w n="61.4">s</w>’<w n="61.5">enivre</w></l>
						<l n="62" num="16.2"><space quantity="4" unit="char"></space><w n="62.1">De</w> <w n="62.2">vos</w> <w n="62.3">clartés</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Et</w> <w n="63.2">qui</w> <w n="63.3">ne</w> <w n="63.4">veut</w> <w n="63.5">plus</w> <w n="63.6">vivre</w></l>
						<l n="64" num="16.4"><space quantity="4" unit="char"></space><w n="64.1">Quand</w> <w n="64.2">vous</w> <w n="64.3">partez</w> ;</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Et</w> <w n="65.2">tout</w> <w n="65.3">ce</w> <w n="65.4">que</w> <w n="65.5">je</w> <w n="65.6">souffre</w></l>
						<l n="66" num="17.2"><space quantity="4" unit="char"></space><w n="66.1">Si</w> <w n="66.2">loin</w> <w n="66.3">du</w> <w n="66.4">jour</w>,</l>
						<l n="67" num="17.3"><w n="67.1">Et</w> <w n="67.2">mon</w> <w n="67.3">âme</w>, <w n="67.4">ce</w> <w n="67.5">gouffre</w></l>
						<l n="68" num="17.4"><space quantity="4" unit="char"></space><w n="68.1">Empli</w> <w n="68.2">d</w>’<w n="68.3">amour</w> !</l>
					</lg>
				</div></body></text></TEI>