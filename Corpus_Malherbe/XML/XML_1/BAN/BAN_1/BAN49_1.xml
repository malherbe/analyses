<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="BAN49">
					<head type="main">LES IMPRÉCATIONS D’UNE CARIATIDE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>Que la cariatide,en sa lente révolte, <lb></lb>
									Se refuse,enfin lasse, à porter l’archivolte <lb></lb>
									Et dise : C’est assez !</quote>
								<bibl>
									<name>VICTOR HUGO</name>, <hi rend="ital">Les voix intérieures</hi>.
								</bibl>
							</cit>
							<cit>
								<quote>C’est le réveil, le déchainement <lb></lb>
									et la vengeance des cariatides.</quote>
								<bibl>
									<name>VICTOR HUGO</name>, <hi rend="ital">Le Rhin</hi>, lettre XXIV.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Puisse</w> <w n="1.2">le</w> <w n="1.3">dieu</w> <w n="1.4">vivant</w> <w n="1.5">dessécher</w> <w n="1.6">la</w> <w n="1.7">paupière</w></l>
						<l n="2" num="1.2"><w n="2.1">À</w> <w n="2.2">qui</w> <w n="2.3">m</w>’<w n="2.4">a</w> <w n="2.5">mise</w> <w n="2.6">là</w> <w n="2.7">vivante</w> <w n="2.8">sous</w> <w n="2.9">la</w> <w n="2.10">pierre</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">comme</w> <w n="3.3">un</w> <w n="3.4">enfant</w> <w n="3.5">porte</w> <w n="3.6">un</w> <w n="3.7">manteau</w> <w n="3.8">de</w> <w n="3.9">velours</w>,</l>
						<l n="4" num="1.4"><w n="4.1">M</w>’<w n="4.2">a</w> <w n="4.3">forcée</w> <w n="4.4">à</w> <w n="4.5">porter</w> <w n="4.6">ces</w> <w n="4.7">édifices</w> <w n="4.8">lourds</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Ces</w> <w n="5.2">vieux</w> <w n="5.3">murs</w> <w n="5.4">en</w> <w n="5.5">haillons</w>, <w n="5.6">ces</w> <w n="5.7">maisons</w> <w n="5.8">condamnées</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Dont</w> <w n="6.2">le</w> <w n="6.3">gouffre</w> <w n="6.4">est</w> <w n="6.5">si</w> <w n="6.6">plein</w> <w n="6.7">de</w> <w n="6.8">choses</w> <w n="6.9">et</w> <w n="6.10">d</w>’<w n="6.11">années</w></l>
						<l n="7" num="1.7"><w n="7.1">Que</w> <w n="7.2">je</w> <w n="7.3">me</w> <w n="7.4">sentirais</w> <w n="7.5">moins</w> <w n="7.6">de</w> <w n="7.7">crispations</w></l>
						<l n="8" num="1.8"><w n="8.1">À</w> <w n="8.2">tenir</w> <w n="8.3">sur</w> <w n="8.4">mon</w> <w n="8.5">dos</w> <w n="8.6">les</w> <w n="8.7">Tyrs</w> <w n="8.8">et</w> <w n="8.9">les</w> <w n="8.10">Sions</w></l>
						<l n="9" num="1.9"><w n="9.1">Que</w> <w n="9.2">laissa</w> <w n="9.3">choir</w> <w n="9.4">le</w> <w n="9.5">monde</w> <w n="9.6">aux</w> <w n="9.7">deux</w> <w n="9.8">bras</w> <w n="9.9">atlastiques</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Ou</w> <w n="10.2">bien</w> <w n="10.3">à</w> <w n="10.4">soulever</w> <w n="10.5">les</w> <w n="10.6">vagues</w> <w n="10.7">élastiques</w></l>
						<l n="11" num="1.11"><w n="11.1">Sommeillant</w> <w n="11.2">à</w> <w n="11.3">demi</w> <w n="11.4">dans</w> <w n="11.5">les</w> <w n="11.6">noirs</w> <w n="11.7">océans</w></l>
						<l n="12" num="1.12"><w n="12.1">Comme</w> <w n="12.2">dans</w> <w n="12.3">son</w> <w n="12.4">désert</w> <w n="12.5">le</w> <w n="12.6">troupeau</w> <w n="12.7">des</w> <w n="12.8">géants</w> !</l>
						<l n="13" num="1.13"><w n="13.1">Si</w> <w n="13.2">bien</w> <w n="13.3">que</w> <w n="13.4">mieux</w> <w n="13.5">vaudrait</w> <w n="13.6">sous</w> <w n="13.7">la</w> <w n="13.8">blonde</w> <w n="13.9">phalange</w></l>
						<l n="14" num="1.14"><w n="14.1">Tomber</w>, <w n="14.2">comme</w> <w n="14.3">Jacob</w> <w n="14.4">dans</w> <w n="14.5">sa</w> <w n="14.6">lutte</w> <w n="14.7">avec</w> <w n="14.8">l</w>’<w n="14.9">ange</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Ou</w> <w n="15.2">soutenir</w> <w n="15.3">du</w> <w n="15.4">front</w> <w n="15.5">avec</w> <w n="15.6">les</w> <w n="15.7">yeux</w> <w n="15.8">ouverts</w></l>
						<l n="16" num="1.16"><w n="16.1">Gœthe</w>, <w n="16.2">dont</w> <w n="16.3">la</w> <w n="16.4">pensée</w> <w n="16.5">était</w> <w n="16.6">un</w> <w n="16.7">univers</w> !</l>
						<l n="17" num="1.17"><space quantity="2" unit="char"></space><w n="17.1">Oh</w> ! <w n="17.2">Si</w> <w n="17.3">le</w> <w n="17.4">feu</w> <w n="17.5">divin</w> <w n="17.6">qui</w> <w n="17.7">brûla</w> <w n="17.8">les</w> <w n="17.9">Sodomes</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Fait</w> <w n="18.2">palpiter</w> <w n="18.3">un</w> <w n="18.4">jour</w> <w n="18.5">ces</w> <w n="18.6">pierres</w> <w n="18.7">et</w> <w n="18.8">ces</w> <w n="18.9">dômes</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Ces</w> <w n="19.2">clochetons</w> <w n="19.3">à</w> <w n="19.4">dents</w>, <w n="19.5">ces</w> <w n="19.6">larges</w> <w n="19.7">escaliers</w></l>
						<l n="20" num="1.20"><w n="20.1">Que</w> <w n="20.2">dans</w> <w n="20.3">l</w>’<w n="20.4">ombre</w> <w n="20.5">une</w> <w n="20.6">main</w> <w n="20.7">gigantesque</w> <w n="20.8">a</w> <w n="20.9">liés</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Ces</w> <w n="21.2">monolithes</w> <w n="21.3">noirs</w> <w n="21.4">qui</w> <w n="21.5">n</w>’<w n="21.6">ont</w> <w n="21.7">fait</w> <w n="21.8">qu</w>’<w n="21.9">une</w> <w n="21.10">rampe</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Ces</w> <w n="22.2">monstres</w> <w n="22.3">vomissants</w> <w n="22.4">dont</w> <w n="22.5">la</w> <w n="22.6">cohorte</w> <w n="22.7">rampe</w></l>
						<l n="23" num="1.23"><w n="23.1">De</w> <w n="23.2">la</w> <w n="23.3">fondation</w> <w n="23.4">jusqu</w>’<w n="23.5">à</w> <w n="23.6">l</w>’<w n="23.7">entablement</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Ces</w> <w n="24.2">granits</w> <w n="24.3">attachés</w> <w n="24.4">impérissablement</w> ;</l>
						<l n="25" num="1.25"><w n="25.1">Si</w> <w n="25.2">ce</w> <w n="25.3">monde</w> <w n="25.4">sur</w> <w n="25.5">eux</w> <w n="25.6">se</w> <w n="25.7">déchire</w> <w n="25.8">et</w> <w n="25.9">s</w>’<w n="25.10">écroule</w></l>
						<l n="26" num="1.26"><w n="26.1">Sous</w> <w n="26.2">le</w> <w n="26.3">souffle</w> <w n="26.4">embrasé</w> <w n="26.5">de</w> <w n="26.6">ce</w> <w n="26.7">simoun</w> <w n="26.8">que</w> <w n="26.9">roule</w></l>
						<l n="27" num="1.27"><w n="27.1">Sans</w> <w n="27.2">pitié</w> <w n="27.3">l</w>’<w n="27.4">ouragan</w> <w n="27.5">des</w> <w n="27.6">révolutions</w></l>
						<l n="28" num="1.28"><w n="28.1">Sur</w> <w n="28.2">les</w> <w n="28.3">peuples</w> <w n="28.4">trop</w> <w n="28.5">pleins</w> <w n="28.6">de</w> <w n="28.7">leurs</w> <w n="28.8">pollutions</w> ;</l>
						<l n="29" num="1.29"><w n="29.1">Si</w>, <w n="29.2">dégageant</w> <w n="29.3">alors</w> <w n="29.4">son</w> <w n="29.5">bras</w> <w n="29.6">et</w> <w n="29.7">sa</w> <w n="29.8">mamelle</w></l>
						<l n="30" num="1.30"><w n="30.1">Du</w> <w n="30.2">vieux</w> <w n="30.3">mur</w> <w n="30.4">qui</w> <w n="30.5">gémit</w> <w n="30.6">et</w> <w n="30.7">qui</w> <w n="30.8">souffre</w> <w n="30.9">comme</w> <w n="30.10">elle</w>,</l>
						<l n="31" num="1.31"><w n="31.1">Ma</w> <w n="31.2">colère</w> <w n="31.3">à</w> <w n="31.4">son</w> <w n="31.5">tour</w> <w n="31.6">peut</w> <w n="31.7">jeter</w> <w n="31.8">sur</w> <w n="31.9">leur</w> <w n="31.10">dos</w></l>
						<l n="32" num="1.32"><w n="32.1">Une</w> <w n="32.2">expiation</w> <w n="32.3">et</w> <w n="32.4">choisir</w> <w n="32.5">les</w> <w n="32.6">fardeaux</w>,</l>
						<l n="33" num="1.33"><w n="33.1">Je</w> <w n="33.2">mettrai</w> <w n="33.3">ce</w> <w n="33.4">jour</w>-<w n="33.5">là</w> <w n="33.6">sur</w> <w n="33.7">l</w>’<w n="33.8">épaule</w> <w n="33.9">des</w> <w n="33.10">hommes</w>,</l>
						<l n="34" num="1.34"><w n="34.1">Au</w> <w n="34.2">lieu</w> <w n="34.3">des</w> <w n="34.4">monuments</w>, <w n="34.5">tombeaux</w> <w n="34.6">sous</w> <w n="34.7">qui</w> <w n="34.8">nous</w> <w n="34.9">sommes</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Au</w> <w n="35.2">lieu</w> <w n="35.3">des</w> <w n="35.4">clochetons</w> <w n="35.5">et</w> <w n="35.6">des</w> <w n="35.7">granits</w> <w n="35.8">quittés</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Le</w> <w n="36.2">poids</w> <w n="36.3">intérieur</w> <w n="36.4">de</w> <w n="36.5">leurs</w> <w n="36.6">iniquités</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1841">février 1841</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>