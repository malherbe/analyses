<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">AMOURS D’ÉLISE</head><div type="poem" key="BAN11">
					<head type="number">VI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tout</w> <w n="1.2">vous</w> <w n="1.3">adore</w>, <w n="1.4">ô</w> <w n="1.5">mon</w> <w n="1.6">Élise</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">quand</w> <w n="2.3">vous</w> <w n="2.4">priez</w> <w n="2.5">à</w> <w n="2.6">l</w>’<w n="2.7">église</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Votre</w> <w n="3.2">figure</w> <w n="3.3">idéalise</w></l>
						<l n="4" num="1.4"><w n="4.1">Jusqu</w>’<w n="4.2">à</w> <w n="4.3">la</w> <w n="4.4">maison</w> <w n="4.5">du</w> <w n="4.6">bon</w> <w n="4.7">Dieu</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Votre</w> <w n="5.2">corps</w> <w n="5.3">charmant</w> <w n="5.4">qui</w> <w n="5.5">se</w> <w n="5.6">ploie</w></l>
						<l n="6" num="1.6"><w n="6.1">Est</w> <w n="6.2">comme</w> <w n="6.3">un</w> <w n="6.4">cantique</w> <w n="6.5">de</w> <w n="6.6">joie</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Et</w>, <w n="7.2">frémissant</w> <w n="7.3">d</w>’<w n="7.4">amour</w>, <w n="7.5">envoie</w></l>
						<l n="8" num="1.8"><w n="8.1">Son</w> <w n="8.2">parfum</w> <w n="8.3">de</w> <w n="8.4">femme</w> <w n="8.5">au</w> <w n="8.6">saint</w> <w n="8.7">lieu</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Votre</w> <w n="9.2">missel</w> <w n="9.3">a</w> <w n="9.4">sur</w> <w n="9.5">ses</w> <w n="9.6">pages</w></l>
						<l n="10" num="2.2"><w n="10.1">Bien</w> <w n="10.2">des</w> <w n="10.3">gracieuses</w> <w n="10.4">images</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Bien</w> <w n="11.2">des</w> <w n="11.3">ornements</w> <w n="11.4">d</w>’<w n="11.5">or</w>, <w n="11.6">ouvrages</w></l>
						<l n="12" num="2.4"><w n="12.1">D</w>’<w n="12.2">un</w> <w n="12.3">grand</w> <w n="12.4">mosaïste</w> <w n="12.5">inconnu</w> ;</l>
						<l n="13" num="2.5"><w n="13.1">Et</w> <w n="13.2">fier</w> <w n="13.3">de</w> <w n="13.4">vous</w> <w n="13.5">faire</w> <w n="13.6">une</w> <w n="13.7">chaîne</w>,</l>
						<l n="14" num="2.6"><w n="14.1">Votre</w> <w n="14.2">chapelet</w> <w n="14.3">noir</w> <w n="14.4">qui</w> <w n="14.5">traîne</w></l>
						<l n="15" num="2.7"><w n="15.1">Redit</w> <w n="15.2">son</w> <w n="15.3">madrigal</w> <w n="15.4">d</w>’<w n="15.5">ébène</w></l>
						<l n="16" num="2.8"><w n="16.1">Aux</w> <w n="16.2">blancheurs</w> <w n="16.3">de</w> <w n="16.4">votre</w> <w n="16.5">bras</w> <w n="16.6">nu</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Comme</w> <w n="17.2">un</w> <w n="17.3">troupeau</w> <w n="17.4">leste</w> <w n="17.5">et</w> <w n="17.6">vorace</w>,</l>
						<l n="18" num="3.2"><w n="18.1">On</w> <w n="18.2">voit</w> <w n="18.3">s</w>’<w n="18.4">élancer</w> <w n="18.5">sur</w> <w n="18.6">la</w> <w n="18.7">trace</w></l>
						<l n="19" num="3.3"><w n="19.1">De</w> <w n="19.2">vos</w> <w n="19.3">chevaux</w> <w n="19.4">de</w> <w n="19.5">noble</w> <w n="19.6">race</w></l>
						<l n="20" num="3.4"><w n="20.1">Mille</w> <w n="20.2">amants</w>, <w n="20.3">le</w> <w n="20.4">cœur</w> <w n="20.5">aux</w> <w n="20.6">abois</w> ;</l>
						<l n="21" num="3.5"><w n="21.1">Derrière</w> <w n="21.2">vous</w> <w n="21.3">marche</w> <w n="21.4">la</w> <w n="21.5">foule</w>,</l>
						<l n="22" num="3.6"><w n="22.1">Mugissante</w> <w n="22.2">comme</w> <w n="22.3">la</w> <w n="22.4">houle</w>,</l>
						<l n="23" num="3.7"><w n="23.1">Et</w> <w n="23.2">dont</w> <w n="23.3">le</w> <w n="23.4">chuchotement</w> <w n="23.5">roule</w></l>
						<l n="24" num="3.8"><w n="24.1">À</w> <w n="24.2">travers</w> <w n="24.3">les</w> <w n="24.4">détours</w> <w n="24.5">du</w> <w n="24.6">bois</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Vous</w> <w n="25.2">avez</w> <w n="25.3">de</w> <w n="25.4">tremblantes</w> <w n="25.5">gazes</w>,</l>
						<l n="26" num="4.2"><w n="26.1">Des</w> <w n="26.2">diamants</w> <w n="26.3">et</w> <w n="26.4">des</w> <w n="26.5">topazes</w></l>
						<l n="27" num="4.3"><w n="27.1">À</w> <w n="27.2">replonger</w> <w n="27.3">dans</w> <w n="27.4">leurs</w> <w n="27.5">extases</w></l>
						<l n="28" num="4.4"><w n="28.1">Les</w> <w n="28.2">Aladins</w> <w n="28.3">expatriés</w>,</l>
						<l n="29" num="4.5"><w n="29.1">Et</w> <w n="29.2">des</w> <w n="29.3">cercles</w> <w n="29.4">de</w> <w n="29.5">blonds</w> <w n="29.6">Clitandres</w></l>
						<l n="30" num="4.6"><w n="30.1">Dont</w> <w n="30.2">le</w> <w n="30.3">cœur</w> <w n="30.4">brûlant</w> <w n="30.5">sous</w> <w n="30.6">les</w> <w n="30.7">cendres</w></l>
						<l n="31" num="4.7"><w n="31.1">Vous</w> <w n="31.2">redit</w> <w n="31.3">en</w> <w n="31.4">fadaises</w> <w n="31.5">tendres</w></l>
						<l n="32" num="4.8"><w n="32.1">Des</w> <w n="32.2">souffrances</w> <w n="32.3">dont</w> <w n="32.4">vous</w> <w n="32.5">riez</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Vous</w> <w n="33.2">avez</w> <w n="33.3">de</w> <w n="33.4">blondes</w> <w n="33.5">servantes</w></l>
						<l n="34" num="5.2"><w n="34.1">Aux</w> <w n="34.2">larges</w> <w n="34.3">prunelles</w> <w n="34.4">ardentes</w>,</l>
						<l n="35" num="5.3"><w n="35.1">Aux</w> <w n="35.2">chevelures</w> <w n="35.3">débordantes</w></l>
						<l n="36" num="5.4"><w n="36.1">Pour</w> <w n="36.2">essuyer</w> <w n="36.3">vos</w> <w n="36.4">blanches</w> <w n="36.5">mains</w> ;</l>
						<l n="37" num="5.5"><w n="37.1">Vous</w> <w n="37.2">portez</w> <w n="37.3">les</w> <w n="37.4">bonheurs</w> <w n="37.5">en</w> <w n="37.6">gerbe</w>,</l>
						<l n="38" num="5.6"><w n="38.1">Et</w> <w n="38.2">sous</w> <w n="38.3">votre</w> <w n="38.4">talon</w> <w n="38.5">superbe</w></l>
						<l n="39" num="5.7"><w n="39.1">Mille</w> <w n="39.2">fleurs</w> <w n="39.3">s</w>’<w n="39.4">éveillent</w> <w n="39.5">dans</w> <w n="39.6">l</w>’<w n="39.7">herbe</w></l>
						<l n="40" num="5.8"><w n="40.1">Afin</w> <w n="40.2">d</w>’<w n="40.3">embaumer</w> <w n="40.4">vos</w> <w n="40.5">chemins</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">Moi</w>, <w n="41.2">je</w> <w n="41.3">suis</w> <w n="41.4">un</w> <w n="41.5">jeune</w> <w n="41.6">poëte</w></l>
						<l n="42" num="6.2"><w n="42.1">Dont</w> <w n="42.2">la</w> <w n="42.3">rêverie</w> <w n="42.4">inquiète</w></l>
						<l n="43" num="6.3"><w n="43.1">N</w>’<w n="43.2">a</w> <w n="43.3">jamais</w> <w n="43.4">connu</w> <w n="43.5">d</w>’<w n="43.6">autre</w> <w n="43.7">fête</w></l>
						<l n="44" num="6.4"><w n="44.1">Que</w> <w n="44.2">l</w>’<w n="44.3">azur</w> <w n="44.4">et</w> <w n="44.5">le</w> <w n="44.6">lys</w> <w n="44.7">en</w> <w n="44.8">fleur</w>.</l>
						<l n="45" num="6.5"><w n="45.1">Je</w> <w n="45.2">n</w>’<w n="45.3">ai</w> <w n="45.4">pour</w> <w n="45.5">trésor</w> <w n="45.6">que</w> <w n="45.7">ma</w> <w n="45.8">plume</w></l>
						<l n="46" num="6.6"><w n="46.1">Et</w> <w n="46.2">ce</w> <w n="46.3">cœur</w> <w n="46.4">broyé</w>, <w n="46.5">qui</w> <w n="46.6">s</w>’<w n="46.7">allume</w>,</l>
						<l n="47" num="6.7"><w n="47.1">Comme</w> <w n="47.2">le</w> <w n="47.3">fer</w> <w n="47.4">rouge</w> <w n="47.5">à</w> <w n="47.6">l</w>’<w n="47.7">enclume</w>,</l>
						<l n="48" num="6.8"><w n="48.1">Sous</w> <w n="48.2">le</w> <w n="48.3">lourd</w> <w n="48.4">marteau</w> <w n="48.5">du</w> <w n="48.6">malheur</w>.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1"><w n="49.1">Mon</w> <w n="49.2">âme</w> <w n="49.3">était</w> <w n="49.4">comme</w> <w n="49.5">cette</w> <w n="49.6">onde</w></l>
						<l n="50" num="7.2"><w n="50.1">Pleine</w> <w n="50.2">d</w>’<w n="50.3">amertume</w>, <w n="50.4">qui</w> <w n="50.5">gronde</w></l>
						<l n="51" num="7.3"><w n="51.1">En</w> <w n="51.2">son</w> <w n="51.3">délire</w>, <w n="51.4">et</w> <w n="51.5">dont</w> <w n="51.6">la</w> <w n="51.7">sonde</w></l>
						<l n="52" num="7.4"><w n="52.1">N</w>’<w n="52.2">a</w> <w n="52.3">jamais</w> <w n="52.4">pu</w> <w n="52.5">trouver</w> <w n="52.6">le</w> <w n="52.7">fond</w> ;</l>
						<l n="53" num="7.5"><w n="53.1">Comme</w> <w n="53.2">ce</w> <w n="53.3">flot</w> <w n="53.4">qu</w>’<w n="53.5">un</w> <w n="53.6">sable</w> <w n="53.7">aride</w></l>
						<l n="54" num="7.6"><w n="54.1">Absorbe</w> <w n="54.2">de</w> <w n="54.3">sa</w> <w n="54.4">bouche</w> <w n="54.5">avide</w>,</l>
						<l n="55" num="7.7"><w n="55.1">Et</w> <w n="55.2">qui</w> <w n="55.3">cherche</w> <w n="55.4">à</w> <w n="55.5">combler</w> <w n="55.6">le</w> <w n="55.7">vide</w></l>
						<l n="56" num="7.8"><w n="56.1">D</w>’<w n="56.2">un</w> <w n="56.3">abîme</w> <w n="56.4">vaste</w> <w n="56.5">et</w> <w n="56.6">profond</w>.</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1"><w n="57.1">Et</w> <w n="57.2">pourtant</w> <w n="57.3">vous</w>, <w n="57.4">type</w> <w n="57.5">suprême</w>,</l>
						<l n="58" num="8.2"><w n="58.1">Vous</w> <w n="58.2">m</w>’<w n="58.3">avez</w> <w n="58.4">dit</w> <w n="58.5">tout</w> <w n="58.6">haut</w> : <w n="58.7">je</w> <w n="58.8">t</w>’<w n="58.9">aime</w></l>
						<l n="59" num="8.3"><w n="59.1">Vous</w> <w n="59.2">m</w>’<w n="59.3">avez</w> <w n="59.4">couché</w> <w n="59.5">morne</w> <w n="59.6">et</w> <w n="59.7">blême</w></l>
						<l n="60" num="8.4"><w n="60.1">Sur</w> <w n="60.2">un</w> <w n="60.3">beau</w> <w n="60.4">lit</w> <w n="60.5">de</w> <w n="60.6">volupté</w> ;</l>
						<l n="61" num="8.5"><w n="61.1">Vous</w> <w n="61.2">avez</w> <w n="61.3">rafraîchi</w> <w n="61.4">ma</w> <w n="61.5">lèvre</w>,</l>
						<l n="62" num="8.6"><w n="62.1">Encor</w> <w n="62.2">toute</w> <w n="62.3">chaude</w> <w n="62.4">de</w> <w n="62.5">fièvre</w>,</l>
						<l n="63" num="8.7"><w n="63.1">Dans</w> <w n="63.2">le</w> <w n="63.3">doux</w> <w n="63.4">vin</w> <w n="63.5">pour</w> <w n="63.6">qui</w> <w n="63.7">l</w>’<w n="63.8">orfèvre</w></l>
						<l n="64" num="8.8"><w n="64.1">Poétise</w> <w n="64.2">un</w> <w n="64.3">cachot</w> <w n="64.4">sculpté</w>.</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1"><w n="65.1">Dans</w> <w n="65.2">vos</w> <w n="65.3">colères</w> <w n="65.4">de</w> <w n="65.5">tigresse</w>,</l>
						<l n="66" num="9.2"><w n="66.1">Vous</w> <w n="66.2">m</w>’<w n="66.3">avez</w> <w n="66.4">fait</w> <w n="66.5">des</w> <w n="66.6">nuits</w> <w n="66.7">d</w>’<w n="66.8">ivresse</w></l>
						<l n="67" num="9.3"><w n="67.1">Où</w> <w n="67.2">le</w> <w n="67.3">plaisir</w>, <w n="67.4">sous</w> <w n="67.5">la</w> <w n="67.6">caresse</w>,</l>
						<l n="68" num="9.4"><w n="68.1">Pleure</w> <w n="68.2">le</w> <w n="68.3">râle</w> <w n="68.4">de</w> <w n="68.5">la</w> <w n="68.6">mort</w>,</l>
						<l n="69" num="9.5"><w n="69.1">Où</w> <w n="69.2">toute</w> <w n="69.3">pudeur</w> <w n="69.4">se</w> <w n="69.5">profane</w>,</l>
						<l n="70" num="9.6"><w n="70.1">Où</w> <w n="70.2">l</w>’<w n="70.3">ange</w> <w n="70.4">le</w> <w n="70.5">plus</w> <w n="70.6">diaphane</w></l>
						<l n="71" num="9.7"><w n="71.1">Se</w> <w n="71.2">fait</w> <w n="71.3">bacchante</w> <w n="71.4">et</w> <w n="71.5">courtisane</w></l>
						<l n="72" num="9.8"><w n="72.1">Et</w> <w n="72.2">grince</w> <w n="72.3">des</w> <w n="72.4">dents</w>, <w n="72.5">et</w> <w n="72.6">vous</w> <w n="72.7">mord</w> !</l>
					</lg>
					<lg n="10">
						<l n="73" num="10.1"><w n="73.1">Puis</w> <w n="73.2">vous</w> <w n="73.3">m</w>’<w n="73.4">avez</w> <w n="73.5">dit</w> <w n="73.6">à</w> <w n="73.7">l</w>’<w n="73.8">oreille</w></l>
						<l n="74" num="10.2"><w n="74.1">Quelque</w> <w n="74.2">étincelante</w> <w n="74.3">merveille</w></l>
						<l n="75" num="10.3"><w n="75.1">Dont</w> <w n="75.2">la</w> <w n="75.3">mélancolie</w> <w n="75.4">éveille</w></l>
						<l n="76" num="10.4"><w n="76.1">Les</w> <w n="76.2">fibres</w> <w n="76.3">de</w> <w n="76.4">l</w>’<w n="76.5">être</w> <w n="76.6">endormi</w> ;</l>
						<l n="77" num="10.5"><w n="77.1">Vous</w> <w n="77.2">aviez</w> <w n="77.3">la</w> <w n="77.4">pudeur</w> <w n="77.5">craintive</w></l>
						<l n="78" num="10.6"><w n="78.1">De</w> <w n="78.2">la</w> <w n="78.3">mourante</w> <w n="78.4">sensitive</w></l>
						<l n="79" num="10.7"><w n="79.1">Qui</w> <w n="79.2">renferme</w> <w n="79.3">son</w> <w n="79.4">cœur</w>, <w n="79.5">plaintive</w></l>
						<l n="80" num="10.8"><w n="80.1">De</w> <w n="80.2">n</w>’<w n="80.3">être</w> <w n="80.4">morte</w> <w n="80.5">qu</w>’<w n="80.6">à</w> <w n="80.7">demi</w>.</l>
					</lg>
					<lg n="11">
						<l n="81" num="11.1"><w n="81.1">Et</w> <w n="81.2">le</w> <w n="81.3">doute</w> <w n="81.4">railleur</w> <w n="81.5">m</w>’<w n="81.6">assiège</w></l>
						<l n="82" num="11.2"><w n="82.1">Lorsque</w>, <w n="82.2">pris</w> <w n="82.3">dans</w> <w n="82.4">un</w> <w n="82.5">divin</w> <w n="82.6">piège</w>,</l>
						<l n="83" num="11.3"><w n="83.1">Mon</w> <w n="83.2">cou</w> <w n="83.3">plus</w> <w n="83.4">pâle</w> <w n="83.5">que</w> <w n="83.6">la</w> <w n="83.7">neige</w></l>
						<l n="84" num="11.4"><w n="84.1">Est</w> <w n="84.2">par</w> <w n="84.3">vos</w> <w n="84.4">bras</w> <w n="84.5">blancs</w> <w n="84.6">enlacé</w>.</l>
						<l n="85" num="11.5"><w n="85.1">J</w>’<w n="85.2">ai</w> <w n="85.3">peur</w> <w n="85.4">que</w> <w n="85.5">le</w> <w n="85.6">riant</w> <w n="85.7">mensonge</w></l>
						<l n="86" num="11.6"><w n="86.1">Du</w> <w n="86.2">lac</w> <w n="86.3">d</w>’<w n="86.4">azur</w> <w n="86.5">où</w> <w n="86.6">je</w> <w n="86.7">me</w> <w n="86.8">plonge</w></l>
						<l n="87" num="11.7"><w n="87.1">Ne</w> <w n="87.2">soit</w> <w n="87.3">l</w>’<w n="87.4">illusion</w> <w n="87.5">d</w>’<w n="87.6">un</w> <w n="87.7">songe</w></l>
						<l n="88" num="11.8"><w n="88.1">Qui</w> <w n="88.2">tenaille</w> <w n="88.3">mon</w> <w n="88.4">front</w> <w n="88.5">glacé</w>.</l>
					</lg>
					<lg n="12">
						<l n="89" num="12.1"><w n="89.1">Or</w>, <w n="89.2">dites</w>-<w n="89.3">moi</w>, <w n="89.4">rêve</w> <w n="89.5">céleste</w>,</l>
						<l n="90" num="12.2"><w n="90.1">Pour</w> <w n="90.2">que</w> <w n="90.3">votre</w> <w n="90.4">belle</w> <w n="90.5">âme</w> <w n="90.6">reste</w></l>
						<l n="91" num="12.3"><w n="91.1">En</w> <w n="91.2">proie</w> <w n="91.3">à</w> <w n="91.4">mon</w> <w n="91.5">amour</w> <w n="91.6">funeste</w>,</l>
						<l n="92" num="12.4"><w n="92.1">Les</w> <w n="92.2">crimes</w> <w n="92.3">que</w> <w n="92.4">vous</w> <w n="92.5">expiez</w> ?</l>
						<l n="93" num="12.5"><w n="93.1">Parlez</w>-<w n="93.2">moi</w>, <w n="93.3">pour</w> <w n="93.4">que</w> <w n="93.5">je</w> <w n="93.6">devine</w></l>
						<l n="94" num="12.6"><w n="94.1">De</w> <w n="94.2">quel</w> <w n="94.3">feu</w> <w n="94.4">bout</w> <w n="94.5">votre</w> <w n="94.6">poitrine</w>,</l>
						<l n="95" num="12.7"><w n="95.1">Et</w> <w n="95.2">quelle</w> <w n="95.3">colère</w> <w n="95.4">divine</w></l>
						<l n="96" num="12.8"><w n="96.1">Vous</w> <w n="96.2">met</w> <w n="96.3">pantelante</w> <w n="96.4">à</w> <w n="96.5">mes</w> <w n="96.6">pieds</w> ?</l>
					</lg>
					<lg n="13">
						<l n="97" num="13.1"><w n="97.1">Avez</w>-<w n="97.2">vous</w> <w n="97.3">surpris</w> <w n="97.4">chez</w> <w n="97.5">les</w> <w n="97.6">anges</w></l>
						<l n="98" num="13.2"><w n="98.1">Le</w> <w n="98.2">secret</w> <w n="98.3">des</w> <w n="98.4">strophes</w> <w n="98.5">étranges</w></l>
						<l n="99" num="13.3"><w n="99.1">Qu</w>’<w n="99.2">ils</w> <w n="99.3">murmurent</w>, <w n="99.4">quand</w> <w n="99.5">leurs</w> <w n="99.6">phalanges</w></l>
						<l n="100" num="13.4"><w n="100.1">S</w>’<w n="100.2">envolent</w> <w n="100.3">dans</w> <w n="100.4">les</w> <w n="100.5">airs</w> <w n="100.6">subtils</w> ?</l>
						<l n="101" num="13.5"><w n="101.1">Au</w> <w n="101.2">Vatican</w>, <w n="101.3">sur</w> <w n="101.4">une</w> <w n="101.5">toile</w>,</l>
						<l n="102" num="13.6"><w n="102.1">Avez</w>-<w n="102.2">vous</w> <w n="102.3">dérobé</w> <w n="102.4">l</w>’<w n="102.5">étoile</w></l>
						<l n="103" num="13.7"><w n="103.1">Qu</w>’<w n="103.2">une</w> <w n="103.3">sainte</w> <w n="103.4">paupière</w> <w n="103.5">voile</w></l>
						<l n="104" num="13.8"><w n="104.1">Avec</w> <w n="104.2">un</w> <w n="104.3">réseau</w> <w n="104.4">de</w> <w n="104.5">longs</w> <w n="104.6">cils</w> ?</l>
					</lg>
					<lg n="14">
						<l n="105" num="14.1"><w n="105.1">Ô</w> <w n="105.2">vous</w> <w n="105.3">que</w> <w n="105.4">la</w> <w n="105.5">lumière</w> <w n="105.6">adore</w>,</l>
						<l n="106" num="14.2"><w n="106.1">De</w> <w n="106.2">quel</w> <w n="106.3">astre</w> <w n="106.4">et</w> <w n="106.5">de</w> <w n="106.6">quelle</w> <w n="106.7">aurore</w></l>
						<l n="107" num="14.3"><w n="107.1">Venez</w>-<w n="107.2">vous</w>, <w n="107.3">radieuse</w> <w n="107.4">encore</w> ?</l>
						<l n="108" num="14.4"><w n="108.1">Je</w> <w n="108.2">ne</w> <w n="108.3">sais</w> ; <w n="108.4">en</w> <w n="108.5">vain</w>, <w n="108.6">ce</w> <w n="108.7">trompeur</w>,</l>
						<l n="109" num="14.5"><w n="109.1">L</w>’<w n="109.2">espoir</w>, <w n="109.3">me</w> <w n="109.4">caresse</w> <w n="109.5">et</w> <w n="109.6">me</w> <w n="109.7">blâme</w> ;</l>
						<l n="110" num="14.6"><w n="110.1">Je</w> <w n="110.2">ne</w> <w n="110.3">sais</w> <w n="110.4">quel</w> <w n="110.5">souffle</w> <w n="110.6">en</w> <w n="110.7">votre</w> <w n="110.8">âme</w></l>
						<l n="111" num="14.7"><w n="111.1">Alluma</w> <w n="111.2">cette</w> <w n="111.3">mer</w> <w n="111.4">de</w> <w n="111.5">flamme</w>,</l>
						<l n="112" num="14.8"><w n="112.1">Ô</w> <w n="112.2">jeune</w> <w n="112.3">déesse</w>, <w n="112.4">et</w> <w n="112.5">j</w>’<w n="112.6">ai</w> <w n="112.7">peur</w>.</l>
					</lg>
				</div></body></text></TEI>