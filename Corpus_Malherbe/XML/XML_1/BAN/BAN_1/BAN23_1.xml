<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Cariatides</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5667 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Cariatides</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L899</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Cariatides</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Charpentier</publisher>
									<date when="1891">1891</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1842">1842</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>L’avant-propos n’est pas repris.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><head type="main_subpart">SONGE D’HIVER</head><div type="poem" key="BAN23">
						<head type="number">X</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="10" unit="char"></space><w n="1.1">Je</w> <w n="1.2">bois</w> <w n="1.3">à</w> <w n="1.4">toi</w>, <w n="1.5">jeune</w> <w n="1.6">reine</w> !</l>
							<l n="2" num="1.2"><space quantity="10" unit="char"></space><w n="2.1">Endormeuse</w> <w n="2.2">souveraine</w>,</l>
							<l n="3" num="1.3"><space quantity="10" unit="char"></space><w n="3.1">Oublieuse</w> <w n="3.2">des</w> <w n="3.3">soucis</w> !</l>
							<l n="4" num="1.4"><space quantity="10" unit="char"></space><w n="4.1">Car</w> <w n="4.2">c</w>’<w n="4.3">est</w> <w n="4.4">pour</w> <w n="4.5">bercer</w> <w n="4.6">ma</w> <w n="4.7">joie</w></l>
							<l n="5" num="1.5"><space quantity="10" unit="char"></space><w n="5.1">Que</w> <w n="5.2">ton</w> <w n="5.3">caprice</w> <w n="5.4">déploie</w></l>
							<l n="6" num="1.6"><space quantity="10" unit="char"></space><w n="6.1">Les</w> <w n="6.2">lits</w> <w n="6.3">de</w> <w n="6.4">pourpre</w> <w n="6.5">et</w> <w n="6.6">de</w> <w n="6.7">soie</w>,</l>
							<l n="7" num="1.7"><space quantity="10" unit="char"></space><w n="7.1">Charmeresse</w> <w n="7.2">aux</w> <w n="7.3">noirs</w> <w n="7.4">sourcils</w> !</l>
						</lg>
						<lg n="2">
							<l n="8" num="2.1"><space quantity="10" unit="char"></space><w n="8.1">Ta</w> <w n="8.2">folle</w> <w n="8.3">toison</w> <w n="8.4">hardie</w></l>
							<l n="9" num="2.2"><space quantity="10" unit="char"></space><w n="9.1">Brille</w> <w n="9.2">comme</w> <w n="9.3">l</w>’<w n="9.4">incendie</w></l>
							<l n="10" num="2.3"><space quantity="10" unit="char"></space><w n="10.1">Hôtesse</w> <w n="10.2">du</w> <w n="10.3">flot</w> <w n="10.4">amer</w>,</l>
							<l n="11" num="2.4"><space quantity="10" unit="char"></space><w n="11.1">Ta</w> <w n="11.2">gorge</w> <w n="11.3">aiguë</w> <w n="11.4">étincelle</w></l>
							<l n="12" num="2.5"><space quantity="10" unit="char"></space><w n="12.1">Dans</w> <w n="12.2">un</w> <w n="12.3">rayon</w> <w n="12.4">qui</w> <w n="12.5">ruisselle</w> ;</l>
							<l n="13" num="2.6"><space quantity="10" unit="char"></space><w n="13.1">Tu</w> <w n="13.2">gardes</w> <w n="13.3">sous</w> <w n="13.4">ton</w> <w n="13.5">aisselle</w></l>
							<l n="14" num="2.7"><space quantity="10" unit="char"></space><w n="14.1">Tous</w> <w n="14.2">les</w> <w n="14.3">parfums</w> <w n="14.4">de</w> <w n="14.5">la</w> <w n="14.6">mer</w>.</l>
						</lg>
						<lg n="3">
							<l n="15" num="3.1"><space quantity="10" unit="char"></space><w n="15.1">Ta</w> <w n="15.2">chevelure</w> <w n="15.3">est</w> <w n="15.4">vivante</w>.</l>
							<l n="16" num="3.2"><space quantity="10" unit="char"></space><w n="16.1">Elle</w> <w n="16.2">frappe</w> <w n="16.3">d</w>’<w n="16.4">épouvante</w></l>
							<l n="17" num="3.3"><space quantity="10" unit="char"></space><w n="17.1">Le</w> <w n="17.2">lion</w> <w n="17.3">et</w> <w n="17.4">le</w> <w n="17.5">vautour</w> :</l>
							<l n="18" num="3.4"><space quantity="10" unit="char"></space><w n="18.1">Sur</w> <w n="18.2">ton</w> <w n="18.3">beau</w> <w n="18.4">ventre</w> <w n="18.5">d</w>’<w n="18.6">ivoire</w></l>
							<l n="19" num="3.5"><space quantity="10" unit="char"></space><w n="19.1">S</w>’<w n="19.2">éparpille</w> <w n="19.3">une</w> <w n="19.4">ombre</w> <w n="19.5">noire</w>,</l>
							<l n="20" num="3.6"><space quantity="10" unit="char"></space><w n="20.1">Et</w> <w n="20.2">tu</w> <w n="20.3">marches</w> <w n="20.4">dans</w> <w n="20.5">ta</w> <w n="20.6">gloire</w>,</l>
							<l n="21" num="3.7"><space quantity="10" unit="char"></space><w n="21.1">Superbe</w> <w n="21.2">comme</w> <w n="21.3">une</w> <w n="21.4">tour</w>.</l>
						</lg>
						<lg n="4">
							<l n="22" num="4.1"><space quantity="10" unit="char"></space><w n="22.1">Ô</w> <w n="22.2">déesse</w> <w n="22.3">protectrice</w> !</l>
							<l n="23" num="4.2"><space quantity="10" unit="char"></space><w n="23.1">Heureux</w>, <w n="23.2">ô</w> <w n="23.3">sage</w> <w n="23.4">nourrice</w>,</l>
							<l n="24" num="4.3"><space quantity="10" unit="char"></space><w n="24.1">L</w>’<w n="24.2">athlète</w> <w n="24.3">aux</w> <w n="24.4">muscles</w> <w n="24.5">ardents</w></l>
							<l n="25" num="4.4"><space quantity="10" unit="char"></space><w n="25.1">Qui</w> <w n="25.2">tout</w> <w n="25.3">couvert</w> <w n="25.4">de</w> <w n="25.5">blessures</w>,</l>
							<l n="26" num="4.5"><space quantity="10" unit="char"></space><w n="26.1">D</w>’<w n="26.2">écume</w> <w n="26.3">et</w> <w n="26.4">de</w> <w n="26.5">meurtrissures</w>,</l>
							<l n="27" num="4.6"><space quantity="10" unit="char"></space><w n="27.1">Appelle</w> <w n="27.2">encor</w> <w n="27.3">les</w> <w n="27.4">morsures</w></l>
							<l n="28" num="4.7"><space quantity="10" unit="char"></space><w n="28.1">De</w> <w n="28.2">ta</w> <w n="28.3">lèvre</w> <w n="28.4">et</w> <w n="28.5">de</w> <w n="28.6">tes</w> <w n="28.7">dents</w> !</l>
						</lg>
						<lg n="5">
							<l n="29" num="5.1"><space quantity="10" unit="char"></space><w n="29.1">Toi</w> <w n="29.2">seule</w>, <w n="29.3">ô</w> <w n="29.4">bonne</w> <w n="29.5">déesse</w>,</l>
							<l n="30" num="5.2"><space quantity="10" unit="char"></space><w n="30.1">As</w> <w n="30.2">l</w>’<w n="30.3">incurable</w> <w n="30.4">tristesse</w></l>
							<l n="31" num="5.3"><space quantity="10" unit="char"></space><w n="31.1">De</w> <w n="31.2">l</w>’<w n="31.3">étoile</w> <w n="31.4">et</w> <w n="31.5">de</w> <w n="31.6">la</w> <w n="31.7">fleur</w></l>
							<l n="32" num="5.4"><space quantity="10" unit="char"></space><w n="32.1">Sous</w> <w n="32.2">l</w>’<w n="32.3">or</w> <w n="32.4">touffu</w> <w n="32.5">qui</w> <w n="32.6">te</w> <w n="32.7">baigne</w> ;</l>
							<l n="33" num="5.5"><space quantity="10" unit="char"></space><w n="33.1">Et</w> <w n="33.2">ton</w> <w n="33.3">désespoir</w> <w n="33.4">m</w>’<w n="33.5">enseigne</w></l>
							<l n="34" num="5.6"><space quantity="10" unit="char"></space><w n="34.1">Sur</w> <w n="34.2">ton</w> <w n="34.3">flanc</w> <w n="34.4">glacé</w> <w n="34.5">qui</w> <w n="34.6">saigne</w></l>
							<l n="35" num="5.7"><space quantity="10" unit="char"></space><w n="35.1">L</w>’<w n="35.2">extase</w> <w n="35.3">de</w> <w n="35.4">la</w> <w n="35.5">douleur</w>.</l>
						</lg>
						<lg n="6">
							<l n="36" num="6.1"><space quantity="10" unit="char"></space><w n="36.1">Honte</w> <w n="36.2">au</w> <w n="36.3">cœur</w> <w n="36.4">timide</w> ! <w n="36.5">Il</w> <w n="36.6">trouve</w></l>
							<l n="37" num="6.2"><space quantity="10" unit="char"></space><w n="37.1">Sous</w> <w n="37.2">ta</w> <w n="37.3">figure</w>, <w n="37.4">la</w> <w n="37.5">louve</w></l>
							<l n="38" num="6.3"><space quantity="10" unit="char"></space><w n="38.1">Qu</w>’<w n="38.2">il</w> <w n="38.3">nomme</w> <w n="38.4">réalité</w>.</l>
							<l n="39" num="6.4"><space quantity="10" unit="char"></space><w n="39.1">Mais</w> <w n="39.2">à</w> <w n="39.3">celui</w> <w n="39.4">qui</w> <w n="39.5">t</w>’<w n="39.6">adore</w></l>
							<l n="40" num="6.5"><space quantity="10" unit="char"></space><w n="40.1">Ta</w> <w n="40.2">main</w>, <w n="40.3">où</w> <w n="40.4">tout</w> <w n="40.5">flot</w> <w n="40.6">se</w> <w n="40.7">dore</w>,</l>
							<l n="41" num="6.6"><space quantity="10" unit="char"></space><w n="41.1">Verse</w>, <w n="41.2">ô</w> <w n="41.3">fille</w> <w n="41.4">de</w> <w n="41.5">Pandore</w>,</l>
							<l n="42" num="6.7"><space quantity="10" unit="char"></space><w n="42.1">Un</w> <w n="42.2">vin</w> <w n="42.3">d</w>’<w n="42.4">immortalité</w> !</l>
						</lg>
					</div></body></text></TEI>