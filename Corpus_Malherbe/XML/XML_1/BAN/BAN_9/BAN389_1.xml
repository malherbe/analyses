<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN389">
				<head type="main">La Fontaine de Jouvence</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Magnus ab integro saeclorum nascitur ordo.</quote>
							<bibl>
								<name>Virgile</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">est</w> <w n="1.3">une</w> <w n="1.4">fontaine</w> <w n="1.5">heureuse</w>, <w n="1.6">dont</w> <w n="1.7">l</w>’<w n="1.8">eau</w> <w n="1.9">tombe</w></l>
					<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">un</w> <w n="2.3">bassin</w> <w n="2.4">plus</w> <w n="2.5">blanc</w> <w n="2.6">qu</w>’<w n="2.7">une</w> <w n="2.8">aile</w> <w n="2.9">de</w> <w n="2.10">colombe</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Cette</w> <w n="3.2">eau</w> <w n="3.3">limpide</w>, <w n="3.4">avec</w> <w n="3.5">de</w> <w n="3.6">clairs</w> <w n="3.7">rayonnements</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">les</w> <w n="4.3">dauphins</w> <w n="4.4">de</w> <w n="4.5">marbre</w> <w n="4.6">éclate</w> <w n="4.7">en</w> <w n="4.8">diamants</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Elle</w> <w n="5.2">rend</w> <w n="5.3">aux</w> <w n="5.4">vieillards</w> <w n="5.5">la</w> <w n="5.6">jeunesse</w> <w n="5.7">et</w> <w n="5.8">la</w> <w n="5.9">force</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Mille</w> <w n="6.2">jeunes</w> <w n="6.3">Cypris</w>, <w n="6.4">fières</w> <w n="6.5">de</w> <w n="6.6">leur</w> <w n="6.7">beau</w> <w n="6.8">torse</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Sur</w> <w n="7.2">l</w>’<w n="7.3">azur</w> <w n="7.4">de</w> <w n="7.5">ses</w> <w n="7.6">flots</w> <w n="7.7">qui</w> <w n="7.8">ne</w> <w n="7.9">sont</w> <w n="7.10">point</w> <w n="7.11">amers</w></l>
					<l n="8" num="2.4"><w n="8.1">Lèvent</w> <w n="8.2">un</w> <w n="8.3">pied</w> <w n="8.4">plus</w> <w n="8.5">blanc</w> <w n="8.6">que</w> <w n="8.7">la</w> <w n="8.8">perle</w> <w n="8.9">des</w> <w n="8.10">mers</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Celles</w> <w n="9.2">qui</w> <w n="9.3">n</w>’<w n="9.4">aimaient</w> <w n="9.5">plus</w> <w n="9.6">les</w> <w n="9.7">tourterelles</w> <w n="9.8">blanches</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">ne</w> <w n="10.3">tressaillaient</w> <w n="10.4">pas</w> <w n="10.5">dans</w> <w n="10.6">le</w> <w n="10.7">mois</w> <w n="10.8">des</w> <w n="10.9">pervenches</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Ceux</w> <w n="11.2">que</w> <w n="11.3">laissaient</w> <w n="11.4">glacés</w> <w n="11.5">la</w> <w n="11.6">Lyre</w> <w n="11.7">et</w> <w n="11.8">le</w> <w n="11.9">bon</w> <w n="11.10">vin</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Sortent</w> <w n="12.2">joyeux</w> <w n="12.3">et</w> <w n="12.4">beaux</w> <w n="12.5">de</w> <w n="12.6">ce</w> <w n="12.7">Léthé</w> <w n="12.8">divin</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Non</w> <w n="13.2">beaux</w> <w n="13.3">comme</w> <w n="13.4">autrefois</w> <w n="13.5">d</w>’<w n="13.6">une</w> <w n="13.7">beauté</w> <w n="13.8">sévère</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Mais</w> <w n="14.2">semblables</w> <w n="14.3">aux</w> <w n="14.4">Dieux</w> <w n="14.5">qui</w> <w n="14.6">boivent</w> <w n="14.7">à</w> <w n="14.8">plein</w> <w n="14.9">verre</w></l>
					<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">feu</w> <w n="15.3">que</w> <w n="15.4">le</w> <w n="15.5">Titan</w> <w n="15.6">pour</w> <w n="15.7">nous</w> <w n="15.8">a</w> <w n="15.9">dérobé</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">qui</w> <w n="16.3">puisent</w> <w n="16.4">le</w> <w n="16.5">vin</w> <w n="16.6">dans</w> <w n="16.7">la</w> <w n="16.8">coupe</w> <w n="16.9">d</w>’<w n="16.10">Hébé</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">La</w> <w n="17.2">Naïde</w> <w n="17.3">aux</w> <w n="17.4">yeux</w> <w n="17.5">bleus</w>, <w n="17.6">qui</w> <w n="17.7">pleure</w> <w n="17.8">goutte</w> <w n="17.9">à</w> <w n="17.10">goutte</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Noie</w> <w n="18.2">au</w> <w n="18.3">fond</w> <w n="18.4">de</w> <w n="18.5">leur</w> <w n="18.6">cœur</w> <w n="18.7">la</w> <w n="18.8">tristesse</w> <w n="18.9">et</w> <w n="18.10">le</w> <w n="18.11">doute</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Et</w>, <w n="19.2">tournant</w> <w n="19.3">leur</w> <w n="19.4">esprit</w> <w n="19.5">vers</w> <w n="19.6">les</w> <w n="19.7">biens</w> <w n="19.8">éternels</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Leur</w> <w n="20.2">montre</w> <w n="20.3">l</w>’<w n="20.4">Idéal</w> <w n="20.5">dans</w> <w n="20.6">les</w> <w n="20.7">plaisirs</w> <w n="20.8">charnels</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Voyez</w>-<w n="21.2">les</w>, <w n="21.3">souriants</w>, <w n="21.4">fiers</w> <w n="21.5">de</w> <w n="21.6">leur</w> <w n="21.7">belle</w> <w n="21.8">taille</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Dans</w> <w n="22.2">ces</w> <w n="22.3">riches</w> <w n="22.4">habits</w> <w n="22.5">de</w> <w n="22.6">fête</w> <w n="22.7">et</w> <w n="22.8">de</w> <w n="22.9">bataille</w></l>
					<l n="23" num="6.3"><w n="23.1">Qui</w> <w n="23.2">relèvent</w> <w n="23.3">la</w> <w n="23.4">mine</w>, <w n="23.5">et</w> <w n="23.6">qu</w>’<w n="23.7">aux</w> <w n="23.8">siècles</w> <w n="23.9">anciens</w></l>
					<l n="24" num="6.4"><w n="24.1">Peignaient</w> <w n="24.2">avec</w> <w n="24.3">amour</w> <w n="24.4">les</w> <w n="24.5">grands</w> <w n="24.6">Vénitiens</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Les</w> <w n="25.2">couples</w> <w n="25.3">sont</w> <w n="25.4">épars</w> : <w n="25.5">de</w> <w n="25.6">jeunes</w> <w n="25.7">femmes</w> <w n="25.8">rousses</w></l>
					<l n="26" num="7.2"><w n="26.1">Dont</w> <w n="26.2">les</w> <w n="26.3">yeux</w> <w n="26.4">rallumés</w> <w n="26.5">sont</w> <w n="26.6">pleins</w> <w n="26.7">de</w> <w n="26.8">clartés</w> <w n="26.9">douces</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Avec</w> <w n="27.2">leurs</w> <w n="27.3">amoureux</w> <w n="27.4">assis</w> <w n="27.5">sur</w> <w n="27.6">le</w> <w n="27.7">gazon</w></l>
					<l n="28" num="7.4"><w n="28.1">Effeuillent</w> <w n="28.2">les</w> <w n="28.3">bouquets</w> <w n="28.4">de</w> <w n="28.5">leur</w> <w n="28.6">jeune</w> <w n="28.7">saison</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">L</w>’<w n="29.2">une</w> <w n="29.3">parle</w> <w n="29.4">à</w> <w n="29.5">mi</w>-<w n="29.6">voix</w>, <w n="29.7">et</w>, <w n="29.8">comme</w> <w n="29.9">en</w> <w n="29.10">un</w> <w n="29.11">méandre</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Erre</w> <w n="30.2">par</w> <w n="30.3">les</w> <w n="30.4">sentiers</w> <w n="30.5">de</w> <w n="30.6">la</w> <w n="30.7">carte</w> <w n="30.8">du</w> <w n="30.9">Tendre</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Celle</w>-<w n="31.2">là</w>, <w n="31.3">fière</w> <w n="31.4">enfin</w> <w n="31.5">de</w> <w n="31.6">vivre</w> <w n="31.7">et</w> <w n="31.8">de</w> <w n="31.9">se</w> <w n="31.10">voir</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Tantôt</w> <w n="32.2">joue</w>, <w n="32.3">et</w> <w n="32.4">ternit</w> <w n="32.5">l</w>’<w n="32.6">acier</w> <w n="32.7">de</w> <w n="32.8">son</w> <w n="32.9">miroir</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Tandis</w> <w n="33.2">qu</w>’<w n="33.3">à</w> <w n="33.4">ses</w> <w n="33.5">genoux</w> <w n="33.6">son</w> <w n="33.7">compagnon</w> <w n="33.8">étale</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Jeune</w> <w n="34.2">et</w> <w n="34.3">fort</w> <w n="34.4">comme</w> <w n="34.5">un</w> <w n="34.6">dieu</w>, <w n="34.7">la</w> <w n="34.8">grâce</w> <w n="34.9">orientale</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Une</w> <w n="35.2">verse</w> <w n="35.3">du</w> <w n="35.4">vin</w> <w n="35.5">dans</w> <w n="35.6">le</w> <w n="35.7">verre</w> <w n="35.8">incrusté</w></l>
					<l n="36" num="9.4"><w n="36.1">D</w>’<w n="36.2">un</w> <w n="36.3">jeune</w> <w n="36.4">cavalier</w> <w n="36.5">debout</w> <w n="36.6">à</w> <w n="36.7">son</w> <w n="36.8">côté</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Plus</w> <w n="37.2">loin</w>, <w n="37.3">deux</w> <w n="37.4">rajeunis</w>, <w n="37.5">sur</w> <w n="37.6">la</w> <w n="37.7">mousse</w> <w n="37.8">des</w> <w n="37.9">plaines</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Mêlent</w> <w n="38.2">dans</w> <w n="38.3">un</w> <w n="38.4">baiser</w> <w n="38.5">les</w> <w n="38.6">fleurs</w> <w n="38.7">de</w> <w n="38.8">leurs</w> <w n="38.9">haleines</w> ;</l>
					<l n="39" num="10.3"><w n="39.1">Et</w>, <w n="39.2">seins</w> <w n="39.3">nus</w>, <w n="39.4">une</w> <w n="39.5">vierge</w> <w n="39.6">en</w> <w n="39.7">fleur</w>, <w n="39.8">sans</w> <w n="39.9">embarras</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Tord</w> <w n="40.2">ses</w> <w n="40.3">cheveux</w> <w n="40.4">luisants</w> <w n="40.5">qui</w> <w n="40.6">pleurent</w> <w n="40.7">sur</w> <w n="40.8">ses</w> <w n="40.9">bras</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Dans</w> <w n="41.2">l</w>’<w n="41.3">humide</w> <w n="41.4">vapeur</w> <w n="41.5">de</w> <w n="41.6">sa</w> <w n="41.7">métamorphose</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Blanche</w> <w n="42.2">encore</w> <w n="42.3">à</w> <w n="42.4">demi</w> <w n="42.5">comme</w> <w n="42.6">une</w> <w n="42.7">jeune</w> <w n="42.8">rose</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Une</w> <w n="43.2">autre</w> <w n="43.3">naît</w> <w n="43.4">au</w> <w n="43.5">monde</w>, <w n="43.6">et</w> <w n="43.7">ses</w> <w n="43.8">beaux</w> <w n="43.9">yeux</w> <w n="43.10">voilés</w></l>
					<l n="44" num="11.4"><w n="44.1">Argentent</w> <w n="44.2">l</w>’<w n="44.3">eau</w> <w n="44.4">d</w>’<w n="44.5">azur</w> <w n="44.6">de</w> <w n="44.7">rayons</w> <w n="44.8">étoilés</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Dans</w> <w n="45.2">les</w> <w n="45.3">vagues</w> <w n="45.4">lointains</w> <w n="45.5">l</w>’<w n="45.6">une</w> <w n="45.7">l</w>’<w n="45.8">autre</w> <w n="45.9">s</w>’<w n="45.10">enchantent</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Agitant</w> <w n="46.2">leurs</w> <w n="46.3">tambours</w> <w n="46.4">dont</w> <w n="46.5">les</w> <w n="46.6">clochettes</w> <w n="46.7">chantent</w>,</l>
					<l n="47" num="12.3"><w n="47.1">De</w> <w n="47.2">galantes</w> <w n="47.3">beautés</w>, <w n="47.4">honneur</w> <w n="47.5">de</w> <w n="47.6">ces</w> <w n="47.7">pourpris</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Qui</w> <w n="48.2">teignent</w> <w n="48.3">l</w>’<w n="48.4">air</w> <w n="48.5">limpide</w> <w n="48.6">à</w> <w n="48.7">leur</w> <w n="48.8">rose</w> <w n="48.9">souris</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">tous</w> <w n="49.3">ces</w> <w n="49.4">nouveau</w>-<w n="49.5">nés</w> <w n="49.6">de</w> <w n="49.7">qui</w> <w n="49.8">l</w>’<w n="49.9">âme</w> <w n="49.10">ravie</w></l>
					<l n="50" num="13.2"><w n="50.1">Connaît</w> <w n="50.2">le</w> <w n="50.3">prix</w> <w n="50.4">des</w> <w n="50.5">biens</w> <w n="50.6">qui</w> <w n="50.7">font</w> <w n="50.8">aimer</w> <w n="50.9">la</w> <w n="50.10">vie</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Sans</w> <w n="51.2">trouble</w> <w n="51.3">et</w> <w n="51.4">sans</w> <w n="51.5">froideur</w> <w n="51.6">cèdent</w> <w n="51.7">à</w> <w n="51.8">leurs</w> <w n="51.9">désirs</w>,</l>
					<l n="52" num="13.4"><w n="52.1">Et</w> <w n="52.2">vident</w> <w n="52.3">lentement</w> <w n="52.4">la</w> <w n="52.5">coupe</w> <w n="52.6">des</w> <w n="52.7">plaisirs</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">O</w> <w n="53.2">doux</w> <w n="53.3">cygnes</w> <w n="53.4">chanteurs</w>, <w n="53.5">vous</w> <w n="53.6">que</w> <w n="53.7">la</w> <w n="53.8">Poésie</w></l>
					<l n="54" num="14.2"><w n="54.1">Retrempe</w> <w n="54.2">incessamment</w> <w n="54.3">dans</w> <w n="54.4">son</w> <w n="54.5">onde</w> <w n="54.6">choisie</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Amis</w>, <w n="55.2">soyons</w> <w n="55.3">pareils</w> <w n="55.4">à</w> <w n="55.5">ces</w> <w n="55.6">beaux</w> <w n="55.7">jeunes</w> <w n="55.8">gens</w> :</l>
					<l n="56" num="14.4"><w n="56.1">Créons</w> <w n="56.2">autour</w> <w n="56.3">de</w> <w n="56.4">nous</w> <w n="56.5">des</w> <w n="56.6">cieux</w> <w n="56.7">intelligents</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Cherchons</w> <w n="57.2">au</w> <w n="57.3">fond</w> <w n="57.4">du</w> <w n="57.5">vin</w> <w n="57.6">les</w> <w n="57.7">sciences</w> <w n="57.8">rebelles</w>,</l>
					<l n="58" num="15.2"><w n="58.1">Et</w> <w n="58.2">l</w>’<w n="58.3">amour</w> <w n="58.4">idéal</w> <w n="58.5">sur</w> <w n="58.6">les</w> <w n="58.7">lèvres</w> <w n="58.8">des</w> <w n="58.9">belles</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Et</w> <w n="59.2">dans</w> <w n="59.3">leurs</w> <w n="59.4">bras</w>, <w n="59.5">qu</w>’<w n="59.6">anime</w> <w n="59.7">une</w> <w n="59.8">calme</w> <w n="59.9">fierté</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Rêvons</w> <w n="60.2">la</w> <w n="60.3">Jouissance</w> <w n="60.4">et</w> <w n="60.5">l</w>’<w n="60.6">Immortalité</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Mai 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>