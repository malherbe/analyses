<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN394">
				<head type="main">A une petite Chanteuse des rues</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Mon père est oiseau, <lb></lb>
								Ma mère est oiselle, <lb></lb>
								Je passe l’eau sans nacelle, <lb></lb>
								Je passe l’eau sans bateau.
							</quote>
							<bibl>
								<name>Victor Hugo</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Enfant</w> <w n="1.2">au</w> <w n="1.3">hasard</w> <w n="1.4">vêtu</w>,</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">D</w>’<w n="2.2">où</w> <w n="2.3">viens</w>-<w n="2.4">tu</w></l>
					<l n="3" num="1.3"><w n="3.1">Avec</w> <w n="3.2">ta</w> <w n="3.3">chanson</w> <w n="3.4">bizarre</w> ?</l>
					<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2">où</w> <w n="4.3">viennent</w> <w n="4.4">à</w> <w n="4.5">l</w>’<w n="4.6">unisson</w></l>
					<l n="5" num="1.5"><space quantity="8" unit="char"></space><w n="5.1">Ta</w> <w n="5.2">chanson</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Ta</w> <w n="6.2">chanson</w> <w n="6.3">et</w> <w n="6.4">ta</w> <w n="6.5">guitare</w> ?</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Tu</w> <w n="7.2">livres</w> <w n="7.3">au</w> <w n="7.4">doigt</w> <w n="7.5">vermeil</w></l>
					<l n="8" num="2.2"><space quantity="8" unit="char"></space><w n="8.1">Du</w> <w n="8.2">soleil</w>,</l>
					<l n="9" num="2.3"><w n="9.1">Qui</w> <w n="9.2">les</w> <w n="9.3">dore</w> <w n="9.4">et</w> <w n="9.5">les</w> <w n="9.6">caresse</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Tes</w> <w n="10.2">longs</w> <w n="10.3">cheveux</w> <w n="10.4">emmêlés</w>,</l>
					<l n="11" num="2.5"><space quantity="8" unit="char"></space><w n="11.1">Crespelés</w></l>
					<l n="12" num="2.6"><w n="12.1">Comme</w> <w n="12.2">ceux</w> <w n="12.3">d</w>’<w n="12.4">une</w> <w n="12.5">Déesse</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">D</w>’<w n="13.2">où</w> <w n="13.3">vient</w> <w n="13.4">ce</w> <w n="13.5">front</w> <w n="13.6">soucieux</w>,</l>
					<l n="14" num="3.2"><space quantity="8" unit="char"></space><w n="14.1">Ces</w> <w n="14.2">grands</w> <w n="14.3">yeux</w>,</l>
					<l n="15" num="3.3"><w n="15.1">Ces</w> <w n="15.2">chairs</w> <w n="15.3">dont</w> <w n="15.4">la</w> <w n="15.5">transparence</w></l>
					<l n="16" num="3.4"><w n="16.1">Fait</w> <w n="16.2">voir</w> <w n="16.3">parmi</w> <w n="16.4">les</w> <w n="16.5">couleurs</w></l>
					<l n="17" num="3.5"><space quantity="8" unit="char"></space><w n="17.1">De</w> <w n="17.2">cent</w> <w n="17.3">fleurs</w></l>
					<l n="18" num="3.6"><w n="18.1">Des</w> <w n="18.2">tons</w> <w n="18.3">dignes</w> <w n="18.4">de</w> <w n="18.5">Lawrence</w> ?</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Viens</w>-<w n="19.2">tu</w> <w n="19.3">du</w> <w n="19.4">pays</w> <w n="19.5">serein</w></l>
					<l n="20" num="4.2"><space quantity="8" unit="char"></space><w n="20.1">Où</w> <w n="20.2">le</w> <w n="20.3">Rhin</w></l>
					<l n="21" num="4.3"><w n="21.1">Baise</w> <w n="21.2">les</w> <w n="21.3">coteaux</w> <w n="21.4">de</w> <w n="21.5">vignes</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Dont</w> <w n="22.2">le</w> <w n="22.3">feuillage</w> <w n="22.4">mouvant</w></l>
					<l n="23" num="4.5"><space quantity="8" unit="char"></space><w n="23.1">Tremble</w> <w n="23.2">au</w> <w n="23.3">vent</w>,</l>
					<l n="24" num="4.6"><w n="24.1">Et</w> <w n="24.2">serpente</w> <w n="24.3">en</w> <w n="24.4">longues</w> <w n="24.5">lignes</w> ?</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Viens</w>-<w n="25.2">tu</w> <w n="25.3">du</w> <w n="25.4">pays</w> <w n="25.5">riant</w></l>
					<l n="26" num="5.2"><space quantity="8" unit="char"></space><w n="26.1">D</w>’<w n="26.2">Orient</w>,</l>
					<l n="27" num="5.3"><w n="27.1">De</w> <w n="27.2">Sorrente</w> <w n="27.3">aux</w> <w n="27.4">blondes</w> <w n="27.5">grèves</w>,</l>
					<l n="28" num="5.4"><w n="28.1">Ou</w> <w n="28.2">de</w> <w n="28.3">Venise</w> <w n="28.4">au</w> <w n="28.5">ciel</w> <w n="28.6">bleu</w></l>
					<l n="29" num="5.5"><space quantity="8" unit="char"></space><w n="29.1">Tout</w> <w n="29.2">en</w> <w n="29.3">feu</w>,</l>
					<l n="30" num="5.6"><w n="30.1">Ou</w> <w n="30.2">du</w> <w n="30.3">blond</w> <w n="30.4">pays</w> <w n="30.5">des</w> <w n="30.6">rêves</w> ?</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Avec</w> <w n="31.2">son</w> <w n="31.3">hardi</w> <w n="31.4">carmin</w>,</l>
					<l n="32" num="6.2"><space quantity="8" unit="char"></space><w n="32.1">Quelle</w> <w n="32.2">main</w></l>
					<l n="33" num="6.3"><w n="33.1">A</w> <w n="33.2">pourpré</w> <w n="33.3">pour</w> <w n="33.4">les</w> <w n="33.5">féeries</w></l>
					<l n="34" num="6.4"><w n="34.1">Tes</w> <w n="34.2">lèvres</w>, <w n="34.3">ces</w> <w n="34.4">fruits</w> <w n="34.5">brûlants</w>,</l>
					<l n="35" num="6.5"><space quantity="8" unit="char"></space><w n="35.1">Plus</w> <w n="35.2">sanglants</w></l>
					<l n="36" num="6.6"><w n="36.1">Que</w> <w n="36.2">des</w> <w n="36.3">grenades</w> <w n="36.4">fleuries</w> ?</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Est</w>-<w n="37.2">ce</w> <w n="37.3">bien</w> <w n="37.4">toi</w>, <w n="37.5">cet</w> <w n="37.6">enfant</w></l>
					<l n="38" num="7.2"><space quantity="8" unit="char"></space><w n="38.1">Triomphant</w>,</l>
					<l n="39" num="7.3"><w n="39.1">Dont</w> <w n="39.2">le</w> <w n="39.3">père</w>, <w n="39.4">ouvrant</w> <w n="39.5">son</w> <w n="39.6">aile</w>,</l>
					<l n="40" num="7.4"><w n="40.1">Au</w> <w n="40.2">fond</w> <w n="40.3">d</w>’<w n="40.4">un</w> <w n="40.5">nid</w> <w n="40.6">de</w> <w n="40.7">roseau</w></l>
					<l n="41" num="7.5"><space quantity="8" unit="char"></space><w n="41.1">Fut</w> <w n="41.2">oiseau</w>,</l>
					<l n="42" num="7.6"><w n="42.1">Dont</w> <w n="42.2">la</w> <w n="42.3">mère</w> <w n="42.4">fut</w> <w n="42.5">oiselle</w> ?</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Belle</w> <w n="43.2">fille</w> <w n="43.3">aux</w> <w n="43.4">cheveux</w> <w n="43.5">d</w>’<w n="43.6">or</w>,</l>
					<l n="44" num="8.2"><space quantity="8" unit="char"></space><w n="44.1">Est</w>-<w n="44.2">ce</w> <w n="44.3">encor</w></l>
					<l n="45" num="8.3"><w n="45.1">Toi</w>, <w n="45.2">qui</w>, <w n="45.3">rieuse</w> <w n="45.4">et</w> <w n="45.5">fantasque</w>,</l>
					<l n="46" num="8.4"><w n="46.1">Faisais</w> <w n="46.2">voltiger</w> <w n="46.3">en</w> <w n="46.4">l</w>’<w n="46.5">air</w></l>
					<l n="47" num="8.5"><space quantity="8" unit="char"></space><w n="47.1">Un</w> <w n="47.2">éclair</w></l>
					<l n="48" num="8.6"><w n="48.1">Avec</w> <w n="48.2">ton</w> <w n="48.3">tambour</w> <w n="48.4">de</w> <w n="48.5">basque</w> ?</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">Toi</w>, <w n="49.2">la</w> <w n="49.3">Bohême</w> <w n="49.4">à</w> <w n="49.5">l</w>’<w n="49.6">œil</w> <w n="49.7">noir</w></l>
					<l n="50" num="9.2"><space quantity="8" unit="char"></space><w n="50.1">Qui</w>, <w n="50.2">le</w> <w n="50.3">soir</w>,</l>
					<l n="51" num="9.3"><w n="51.1">D</w>’<w n="51.2">une</w> <w n="51.3">dorure</w> <w n="51.4">fanée</w></l>
					<l n="52" num="9.4"><w n="52.1">Serrais</w> <w n="52.2">ton</w> <w n="52.3">ample</w> <w n="52.4">chignon</w>,</l>
					<l n="53" num="9.5"><space quantity="8" unit="char"></space><w n="53.1">Et</w> <w n="53.2">Mignon</w></l>
					<l n="54" num="9.6"><w n="54.1">Est</w>-<w n="54.2">elle</w> <w n="54.3">ta</w> <w n="54.4">sœur</w> <w n="54.5">aînée</w> ?</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1"><w n="55.1">Ou</w> <w n="55.2">plutôt</w>, <w n="55.3">courant</w> <w n="55.4">au</w> <w n="55.5">bois</w>,</l>
					<l n="56" num="10.2"><space quantity="8" unit="char"></space><w n="56.1">Et</w> <w n="56.2">sans</w> <w n="56.3">voix</w></l>
					<l n="57" num="10.3"><w n="57.1">Pour</w> <w n="57.2">un</w> <w n="57.3">brin</w> <w n="57.4">d</w>’<w n="57.5">herbe</w> <w n="57.6">qui</w> <w n="57.7">bouge</w>,</l>
					<l n="58" num="10.4"><w n="58.1">Interdite</w> <w n="58.2">à</w> <w n="58.3">chaque</w> <w n="58.4">pas</w>,</l>
					<l n="59" num="10.5"><space quantity="8" unit="char"></space><w n="59.1">N</w>’<w n="59.2">es</w>-<w n="59.3">tu</w> <w n="59.4">pas</w></l>
					<l n="60" num="10.6"><w n="60.1">Le</w> <w n="60.2">petit</w> <w n="60.3">Chaperon</w>-<w n="60.4">Rouge</w>,</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1"><w n="61.1">Qui</w> <w n="61.2">fit</w> <w n="61.3">même</w> <w n="61.4">des</w> <w n="61.5">jaloux</w></l>
					<l n="62" num="11.2"><space quantity="8" unit="char"></space><w n="62.1">Chez</w> <w n="62.2">les</w> <w n="62.3">loups</w>,</l>
					<l n="63" num="11.3"><w n="63.1">Et</w> <w n="63.2">qui</w>, <w n="63.3">portant</w> <w n="63.4">sa</w> <w n="63.5">galette</w></l>
					<l n="64" num="11.4"><w n="64.1">Chez</w> <w n="64.2">la</w> <w n="64.3">bonne</w> <w n="64.4">mère</w> <w n="64.5">grand</w>,</l>
					<l n="65" num="11.5"><space quantity="8" unit="char"></space><w n="65.1">En</w> <w n="65.2">entrant</w></l>
					<l n="66" num="11.6"><w n="66.1">Faisait</w> <w n="66.2">choir</w> <w n="66.3">la</w> <w n="66.4">bobinette</w> ?</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1"><w n="67.1">Mais</w> <w n="67.2">non</w>, <w n="67.3">aux</w> <w n="67.4">divins</w> <w n="67.5">attraits</w></l>
					<l n="68" num="12.2"><space quantity="8" unit="char"></space><w n="68.1">De</w> <w n="68.2">tes</w> <w n="68.3">traits</w></l>
					<l n="69" num="12.3"><w n="69.1">Et</w> <w n="69.2">de</w> <w n="69.3">ta</w> <w n="69.4">voix</w>, <w n="69.5">je</w> <w n="69.6">devine</w></l>
					<l n="70" num="12.4"><w n="70.1">L</w>’<w n="70.2">enfant</w> <w n="70.3">comblé</w> <w n="70.4">des</w> <w n="70.5">faveurs</w></l>
					<l n="71" num="12.5"><space quantity="8" unit="char"></space><w n="71.1">Des</w> <w n="71.2">rêveurs</w>,</l>
					<l n="72" num="12.6"><w n="72.1">La</w> <w n="72.2">folâtre</w> <w n="72.3">Colombine</w>.</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1"><w n="73.1">Mais</w> <w n="73.2">où</w> <w n="73.3">sont</w> <w n="73.4">tes</w> <w n="73.5">beaux</w> <w n="73.6">souliers</w>,</l>
					<l n="74" num="13.2"><space quantity="8" unit="char"></space><w n="74.1">Tes</w> <w n="74.2">colliers</w></l>
					<l n="75" num="13.3"><w n="75.1">Qui</w> <w n="75.2">font</w> <w n="75.3">rêver</w> <w n="75.4">les</w> <w n="75.5">fillettes</w> ?</l>
					<l n="76" num="13.4"><w n="76.1">Où</w> <w n="76.2">sont</w> <w n="76.3">le</w> <w n="76.4">bel</w> <w n="76.5">or</w> <w n="76.6">changeant</w></l>
					<l n="77" num="13.5"><space quantity="8" unit="char"></space><w n="77.1">Et</w> <w n="77.2">l</w>’<w n="77.3">argent</w></l>
					<l n="78" num="13.6"><w n="78.1">De</w> <w n="78.2">tes</w> <w n="78.3">jupes</w> <w n="78.4">à</w> <w n="78.5">paillettes</w> ?</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1"><w n="79.1">Et</w> <w n="79.2">le</w> <w n="79.3">souple</w> <w n="79.4">casaquin</w></l>
					<l n="80" num="14.2"><space quantity="8" unit="char"></space><w n="80.1">D</w>’<w n="80.2">Arlequin</w> ?</l>
					<l n="81" num="14.3"><w n="81.1">Et</w> <w n="81.2">Cassandre</w> <w n="81.3">et</w> <w n="81.4">sa</w> <w n="81.5">fortune</w> ?</l>
					<l n="82" num="14.4"><w n="82.1">Où</w> <w n="82.2">Pierrot</w>, <w n="82.3">l</w>’<w n="82.4">homme</w> <w n="82.5">subtil</w>,</l>
					<l n="83" num="14.5"><space quantity="8" unit="char"></space><w n="83.1">Cache</w>-<w n="83.2">t</w>-<w n="83.3">il</w></l>
					<l n="84" num="14.6"><w n="84.1">Sa</w> <w n="84.2">face</w> <w n="84.3">de</w> <w n="84.4">clair</w> <w n="84.5">de</w> <w n="84.6">lune</w> ?</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845">Mars 1845.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>