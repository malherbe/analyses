<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN392">
				<head type="main">Chanson de bateau</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Et vogue la nacelle <lb></lb>
								Qui porte mes amours.
							</quote>
							<bibl>
								<hi rend="ital">Chanson.</hi>
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">canal</w> <w n="1.3">endort</w> <w n="1.4">ses</w> <w n="1.5">flots</w>,</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">Ses</w> <w n="2.2">échos</w>,</l>
					<l n="3" num="1.3"><space quantity="2" unit="char"></space><w n="3.1">Et</w> <w n="3.2">le</w> <w n="3.3">zéphyr</w> <w n="3.4">nous</w> <w n="3.5">verse</w></l>
					<l n="4" num="1.4"><space quantity="2" unit="char"></space><w n="4.1">Des</w> <w n="4.2">parfums</w> <w n="4.3">purs</w> <w n="4.4">et</w> <w n="4.5">doux</w>.</l>
					<l n="5" num="1.5"><space quantity="4" unit="char"></space><w n="5.1">Le</w> <w n="5.2">flot</w> <w n="5.3">nous</w> <w n="5.4">berce</w>,</l>
					<l n="6" num="1.6"><space quantity="4" unit="char"></space><w n="6.1">Endormons</w>-<w n="6.2">nous</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Les</w> <w n="7.2">voix</w> <w n="7.3">emplissent</w> <w n="7.4">les</w> <w n="7.5">airs</w></l>
					<l n="8" num="2.2"><space quantity="8" unit="char"></space><w n="8.1">De</w> <w n="8.2">concerts</w>,</l>
					<l n="9" num="2.3"><space quantity="2" unit="char"></space><w n="9.1">Et</w> <w n="9.2">le</w> <w n="9.3">vent</w> <w n="9.4">les</w> <w n="9.5">disperse</w></l>
					<l n="10" num="2.4"><space quantity="2" unit="char"></space><w n="10.1">Avec</w> <w n="10.2">nos</w> <w n="10.3">baisers</w> <w n="10.4">fous</w>.</l>
					<l n="11" num="2.5"><space quantity="4" unit="char"></space><w n="11.1">Le</w> <w n="11.2">flot</w> <w n="11.3">nous</w> <w n="11.4">berce</w>,</l>
					<l n="12" num="2.6"><space quantity="4" unit="char"></space><w n="12.1">Endormons</w>-<w n="12.2">nous</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">En</w> <w n="13.2">vain</w> <w n="13.3">ton</w> <w n="13.4">époux</w> <w n="13.5">caduc</w>,</l>
					<l n="14" num="3.2"><space quantity="8" unit="char"></space><w n="14.1">Comte</w> <w n="14.2">ou</w> <w n="14.3">duc</w>,</l>
					<l n="15" num="3.3"><space quantity="2" unit="char"></space><w n="15.1">Se</w> <w n="15.2">jette</w> <w n="15.3">à</w> <w n="15.4">la</w> <w n="15.5">traverse</w></l>
					<l n="16" num="3.4"><space quantity="2" unit="char"></space><w n="16.1">De</w> <w n="16.2">nos</w> <w n="16.3">gais</w> <w n="16.4">rendez</w>-<w n="16.5">vous</w>.</l>
					<l n="17" num="3.5"><space quantity="4" unit="char"></space><w n="17.1">Le</w> <w n="17.2">flot</w> <w n="17.3">nous</w> <w n="17.4">berce</w>,</l>
					<l n="18" num="3.6"><space quantity="4" unit="char"></space><w n="18.1">Endormons</w>-<w n="18.2">nous</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Ah</w> ! <w n="19.2">que</w> <w n="19.3">les</w> <w n="19.4">cieux</w> <w n="19.5">étoilés</w></l>
					<l n="20" num="4.2"><space quantity="8" unit="char"></space><w n="20.1">Soient</w> <w n="20.2">voilés</w>,</l>
					<l n="21" num="4.3"><space quantity="2" unit="char"></space><w n="21.1">Tandis</w> <w n="21.2">que</w> <w n="21.3">je</w> <w n="21.4">renverse</w></l>
					<l n="22" num="4.4"><space quantity="2" unit="char"></space><w n="22.1">Ton</w> <w n="22.2">front</w> <w n="22.3">sur</w> <w n="22.4">mes</w> <w n="22.5">genoux</w> !</l>
					<l n="23" num="4.5"><space quantity="4" unit="char"></space><w n="23.1">Le</w> <w n="23.2">flot</w> <w n="23.3">nous</w> <w n="23.4">berce</w>,</l>
					<l n="24" num="4.6"><space quantity="4" unit="char"></space><w n="24.1">Endormons</w>-<w n="24.2">nous</w> !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Qu</w>’<w n="25.2">importe</w> <w n="25.3">si</w>, <w n="25.4">dans</w> <w n="25.5">la</w> <w n="25.6">nuit</w></l>
					<l n="26" num="5.2"><space quantity="8" unit="char"></space><w n="26.1">Qui</w> <w n="26.2">s</w>’<w n="26.3">enfuit</w>,</l>
					<l n="27" num="5.3"><space quantity="2" unit="char"></space><w n="27.1">L</w>’<w n="27.2">orage</w> <w n="27.3">bouleverse</w></l>
					<l n="28" num="5.4"><space quantity="2" unit="char"></space><w n="28.1">Les</w> <w n="28.2">éléments</w> <w n="28.3">jaloux</w> !</l>
					<l n="29" num="5.5"><space quantity="4" unit="char"></space><w n="29.1">Le</w> <w n="29.2">flot</w> <w n="29.3">nous</w> <w n="29.4">berce</w>,</l>
					<l n="30" num="5.6"><space quantity="4" unit="char"></space><w n="30.1">Endormons</w>-<w n="30.2">nous</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Juillet 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>