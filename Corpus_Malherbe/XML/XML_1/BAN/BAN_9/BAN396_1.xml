<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN396">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Toute</w> <w n="1.2">cette</w> <w n="1.3">nuit</w> <w n="1.4">nous</w> <w n="1.5">avons</w></l>
					<l n="2" num="1.2"><w n="2.1">Relu</w> <w n="2.2">le</w> <w n="2.3">vieil</w> <w n="2.4">ami</w> <w n="2.5">Shakspere</w></l>
					<l n="3" num="1.3"><w n="3.1">Aux</w> <w n="3.2">beaux</w> <w n="3.3">endroits</w> <w n="3.4">que</w> <w n="3.5">nous</w> <w n="3.6">savons</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">voici</w> <w n="4.3">que</w> <w n="4.4">la</w> <w n="4.5">nuit</w> <w n="4.6">expire</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Nous</w> <w n="5.2">avons</w> <w n="5.3">longtemps</w> <w n="5.4">veillé</w>, <w n="5.5">mais</w></l>
					<l n="6" num="2.2"><w n="6.1">Nous</w> <w n="6.2">lisions</w> <w n="6.3">le</w> <w n="6.4">poëte</w> <w n="6.5">unique</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">la</w> <w n="7.3">sombre</w> <w n="7.4">nuit</w> <w n="7.5">n</w>’<w n="7.6">eut</w> <w n="7.7">jamais</w></l>
					<l n="8" num="2.4"><w n="8.1">Plus</w> <w n="8.2">d</w>’<w n="8.3">étoiles</w> <w n="8.4">à</w> <w n="8.5">sa</w> <w n="8.6">tunique</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Phœbé</w>, <w n="9.2">qu</w>’<w n="9.3">en</w> <w n="9.4">riant</w> <w n="9.5">nous</w> <w n="9.6">troublons</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Va</w> <w n="10.2">s</w>’<w n="10.3">enfuir</w>, <w n="10.4">et</w> <w n="10.5">le</w> <w n="10.6">jour</w> <w n="10.7">va</w> <w n="10.8">naître</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">ma</w> <w n="11.3">voisine</w> <w n="11.4">aux</w> <w n="11.5">cheveux</w> <w n="11.6">blonds</w></l>
					<l n="12" num="3.4"><w n="12.1">Viendra</w> <w n="12.2">se</w> <w n="12.3">mettre</w> <w n="12.4">à</w> <w n="12.5">sa</w> <w n="12.6">fenêtre</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Ah</w> ! <w n="13.2">lorsque</w> <w n="13.3">vous</w> <w n="13.4">allez</w> <w n="13.5">venir</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Ma</w> <w n="14.2">voisine</w>, <w n="14.3">en</w> <w n="14.4">jupe</w> <w n="14.5">de</w> <w n="14.6">toile</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Nous</w> <w n="15.2">ne</w> <w n="15.3">suivrons</w> <w n="15.4">du</w> <w n="15.5">souvenir</w></l>
					<l n="16" num="4.4"><w n="16.1">Aucun</w> <w n="16.2">beau</w> <w n="16.3">vers</w>, <w n="16.4">aucune</w> <w n="16.5">étoile</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Vous</w> <w n="17.2">apparaîtrez</w> <w n="17.3">comme</w> <w n="17.4">un</w> <w n="17.5">lys</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Avec</w> <w n="18.2">votre</w> <w n="18.3">guimpe</w> <w n="18.4">croisée</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Au</w> <w n="19.2">milieu</w> <w n="19.3">des</w> <w n="19.4">volubilis</w></l>
					<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">couronnent</w> <w n="20.3">votre</w> <w n="20.4">croisée</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">nous</w>, <w n="21.3">nous</w> <w n="21.4">analyserons</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Sans</w> <w n="22.2">redouter</w> <w n="22.3">qu</w>’<w n="22.4">elle</w> <w n="22.5">nous</w> <w n="22.6">mente</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Sous</w> <w n="23.2">son</w> <w n="23.3">rideau</w> <w n="23.4">de</w> <w n="23.5">liserons</w></l>
					<l n="24" num="6.4"><w n="24.1">Votre</w> <w n="24.2">tête</w> <w n="24.3">simple</w> <w n="24.4">et</w> <w n="24.5">charmante</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1843">Avril 1843.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>