<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN379">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Viens</w>. <w n="1.2">Sur</w> <w n="1.3">tes</w> <w n="1.4">cheveux</w> <w n="1.5">noirs</w> <w n="1.6">jette</w> <w n="1.7">un</w> <w n="1.8">chapeau</w> <w n="1.9">de</w> <w n="1.10">paille</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Avant</w> <w n="2.2">l</w>’<w n="2.3">heure</w> <w n="2.4">du</w> <w n="2.5">bruit</w>, <w n="2.6">l</w>’<w n="2.7">heure</w> <w n="2.8">où</w> <w n="2.9">chacun</w> <w n="2.10">travaille</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Allons</w> <w n="3.2">voir</w> <w n="3.3">le</w> <w n="3.4">matin</w> <w n="3.5">se</w> <w n="3.6">lever</w> <w n="3.7">sur</w> <w n="3.8">les</w> <w n="3.9">monts</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">cueillir</w> <w n="4.3">par</w> <w n="4.4">les</w> <w n="4.5">prés</w> <w n="4.6">les</w> <w n="4.7">fleurs</w> <w n="4.8">que</w> <w n="4.9">nous</w> <w n="4.10">aimons</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">les</w> <w n="5.3">bords</w> <w n="5.4">de</w> <w n="5.5">la</w> <w n="5.6">source</w> <w n="5.7">aux</w> <w n="5.8">moires</w> <w n="5.9">assouplies</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">nénufars</w> <w n="6.3">dorés</w> <w n="6.4">penchent</w> <w n="6.5">des</w> <w n="6.6">fleurs</w> <w n="6.7">pâlies</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Il</w> <w n="7.2">reste</w> <w n="7.3">dans</w> <w n="7.4">les</w> <w n="7.5">champs</w> <w n="7.6">et</w> <w n="7.7">dans</w> <w n="7.8">les</w> <w n="7.9">grands</w> <w n="7.10">vergers</w></l>
					<l n="8" num="1.8"><w n="8.1">Comme</w> <w n="8.2">un</w> <w n="8.3">écho</w> <w n="8.4">lointain</w> <w n="8.5">des</w> <w n="8.6">chansons</w> <w n="8.7">des</w> <w n="8.8">bergers</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Et</w>, <w n="9.2">secouant</w> <w n="9.3">pour</w> <w n="9.4">nous</w> <w n="9.5">leurs</w> <w n="9.6">ailes</w> <w n="9.7">odorantes</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Les</w> <w n="10.2">brises</w> <w n="10.3">du</w> <w n="10.4">matin</w>, <w n="10.5">comme</w> <w n="10.6">des</w> <w n="10.7">sœurs</w> <w n="10.8">errantes</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Jettent</w> <w n="11.2">déjà</w> <w n="11.3">vers</w> <w n="11.4">toi</w>, <w n="11.5">tandis</w> <w n="11.6">que</w> <w n="11.7">tu</w> <w n="11.8">souris</w>,</l>
					<l n="12" num="1.12"><w n="12.1">L</w>’<w n="12.2">odeur</w> <w n="12.3">du</w> <w n="12.4">pêcher</w> <w n="12.5">rose</w> <w n="12.6">et</w> <w n="12.7">des</w> <w n="12.8">pommiers</w> <w n="12.9">fleuris</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845">Avril 1845.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>