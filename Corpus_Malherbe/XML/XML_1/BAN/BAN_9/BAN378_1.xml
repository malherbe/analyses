<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN378">
				<head type="main">Chanson à boire</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Allons en vendanges, <lb></lb>Les raisins sont bons !</quote>
							<bibl><hi rend="ital">Chanson</hi></bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="4" unit="char"></space><w n="1.1">De</w> <w n="1.2">ce</w> <w n="1.3">vieux</w> <w n="1.4">vin</w> <w n="1.5">que</w> <w n="1.6">je</w> <w n="1.7">révère</w></l>
					<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">Cherchez</w> <w n="2.2">un</w> <w n="2.3">flacon</w> <w n="2.4">dans</w> <w n="2.5">ce</w> <w n="2.6">coin</w>.</l>
					<l n="3" num="1.3"><space quantity="4" unit="char"></space><w n="3.1">Çà</w>, <w n="3.2">qu</w>’<w n="3.3">on</w> <w n="3.4">le</w> <w n="3.5">débouche</w> <w n="3.6">avec</w> <w n="3.7">soin</w>,</l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">Et</w> <w n="4.2">qu</w>’<w n="4.3">on</w> <w n="4.4">emplisse</w> <w n="4.5">mon</w> <w n="4.6">grand</w> <w n="4.7">verre</w>.</l>
				</lg>
				<lg n="2">
					<l rhyme="none" n="5" num="2.1"><space quantity="16" unit="char"></space><w n="5.1">Chantons</w> <w n="5.2">Io</w> <w n="5.3">Pæan</w> !</l>
				</lg>
				<lg n="3">
					<l n="6" num="3.1"><space quantity="4" unit="char"></space><w n="6.1">Le</w> <w n="6.2">Léthé</w> <w n="6.3">des</w> <w n="6.4">soucis</w> <w n="6.5">moroses</w></l>
					<l n="7" num="3.2"><space quantity="4" unit="char"></space><w n="7.1">Sous</w> <w n="7.2">son</w> <w n="7.3">beau</w> <w n="7.4">cristal</w> <w n="7.5">est</w> <w n="7.6">enclos</w>,</l>
					<l n="8" num="3.3"><space quantity="4" unit="char"></space><w n="8.1">Et</w> <w n="8.2">dans</w> <w n="8.3">son</w> <w n="8.4">cœur</w> <w n="8.5">je</w> <w n="8.6">veux</w> <w n="8.7">à</w> <w n="8.8">flots</w></l>
					<l n="9" num="3.4"><space quantity="4" unit="char"></space><w n="9.1">Boire</w> <w n="9.2">du</w> <w n="9.3">soleil</w> <w n="9.4">et</w> <w n="9.5">des</w> <w n="9.6">roses</w>.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1"><w n="10.1">La</w> <w n="10.2">treille</w> <w n="10.3">a</w> <w n="10.4">ployé</w> <w n="10.5">tout</w> <w n="10.6">le</w> <w n="10.7">long</w> <w n="10.8">des</w> <w n="10.9">murs</w>,</l>
					<l n="11" num="4.2"><w n="11.1">Allez</w>, <w n="11.2">vendangeurs</w>, <w n="11.3">les</w> <w n="11.4">raisins</w> <w n="11.5">sont</w> <w n="11.6">mûrs</w> !</l>
				</lg>
				<lg n="5">
					<l n="12" num="5.1"><space quantity="4" unit="char"></space><w n="12.1">Jusqu</w>’<w n="12.2">en</w> <w n="12.3">la</w> <w n="12.4">moindre</w> <w n="12.5">gouttelette</w>,</l>
					<l n="13" num="5.2"><space quantity="4" unit="char"></space><w n="13.1">La</w> <w n="13.2">fraîche</w> <w n="13.3">haleine</w> <w n="13.4">de</w> <w n="13.5">ce</w> <w n="13.6">vin</w></l>
					<l n="14" num="5.3"><space quantity="4" unit="char"></space><w n="14.1">Exhale</w> <w n="14.2">un</w> <w n="14.3">parfum</w> <w n="14.4">plus</w> <w n="14.5">divin</w></l>
					<l n="15" num="5.4"><space quantity="4" unit="char"></space><w n="15.1">Qu</w>’<w n="15.2">une</w> <w n="15.3">touffe</w> <w n="15.4">de</w> <w n="15.5">violette</w>,</l>
				</lg>
				<lg n="6">
					<l rhyme="none" n="16" num="6.1"><space quantity="16" unit="char"></space><w n="16.1">Chantons</w> <w n="16.2">Io</w> <w n="16.3">Pæan</w> !</l>
				</lg>
				<lg n="7">
					<l n="17" num="7.1"><space quantity="4" unit="char"></space><w n="17.1">Et</w>, <w n="17.2">dessus</w> <w n="17.3">la</w> <w n="17.4">lèvre</w> <w n="17.5">endormie</w></l>
					<l n="18" num="7.2"><space quantity="4" unit="char"></space><w n="18.1">Des</w> <w n="18.2">pâles</w> <w n="18.3">et</w> <w n="18.4">tristes</w> <w n="18.5">songeurs</w>,</l>
					<l n="19" num="7.3"><space quantity="4" unit="char"></space><w n="19.1">Met</w> <w n="19.2">de</w> <w n="19.3">plus</w> <w n="19.4">ardentes</w> <w n="19.5">rougeurs</w></l>
					<l n="20" num="7.4"><space quantity="4" unit="char"></space><w n="20.1">Que</w> <w n="20.2">n</w>’<w n="20.3">en</w> <w n="20.4">a</w> <w n="20.5">le</w> <w n="20.6">sein</w> <w n="20.7">de</w> <w n="20.8">ma</w> <w n="20.9">mie</w>.</l>
				</lg>
				<lg n="8">
					<l n="21" num="8.1"><w n="21.1">La</w> <w n="21.2">treille</w> <w n="21.3">a</w> <w n="21.4">ployé</w> <w n="21.5">tout</w> <w n="21.6">le</w> <w n="21.7">long</w> <w n="21.8">des</w> <w n="21.9">murs</w>,</l>
					<l n="22" num="8.2"><w n="22.1">Allez</w>, <w n="22.2">vendangeurs</w>, <w n="22.3">les</w> <w n="22.4">raisins</w> <w n="22.5">sont</w> <w n="22.6">mûrs</w> !</l>
				</lg>
				<lg n="9">
					<l n="23" num="9.1"><space quantity="4" unit="char"></space><w n="23.1">A</w> <w n="23.2">mes</w> <w n="23.3">yeux</w>, <w n="23.4">en</w> <w n="23.5">nappes</w> <w n="23.6">fleuries</w></l>
					<l n="24" num="9.2"><space quantity="4" unit="char"></space><w n="24.1">Dansantes</w> <w n="24.2">sous</w> <w n="24.3">le</w> <w n="24.4">ciel</w> <w n="24.5">en</w> <w n="24.6">feu</w>,</l>
					<l n="25" num="9.3"><space quantity="4" unit="char"></space><w n="25.1">L</w>’<w n="25.2">air</w> <w n="25.3">se</w> <w n="25.4">teint</w> <w n="25.5">de</w> <w n="25.6">rose</w> <w n="25.7">et</w> <w n="25.8">de</w> <w n="25.9">bleu</w></l>
					<l n="26" num="9.4"><space quantity="4" unit="char"></space><w n="26.1">Comme</w> <w n="26.2">au</w> <w n="26.3">théâtre</w> <w n="26.4">des</w> <w n="26.5">féeries</w> ;</l>
				</lg>
				<lg n="10">
					<l rhyme="none" n="27" num="10.1"><space quantity="16" unit="char"></space><w n="27.1">Chantons</w> <w n="27.2">Io</w> <w n="27.3">Pæan</w> !</l>
				</lg>
				<lg n="11">
					<l n="28" num="11.1"><space quantity="4" unit="char"></space><w n="28.1">Je</w> <w n="28.2">vois</w> <w n="28.3">un</w> <w n="28.4">cortège</w> <w n="28.5">fantasque</w>,</l>
					<l n="29" num="11.2"><space quantity="4" unit="char"></space><w n="29.1">Suivi</w> <w n="29.2">de</w> <w n="29.3">cors</w> <w n="29.4">et</w> <w n="29.5">de</w> <w n="29.6">hautbois</w>,</l>
					<l n="30" num="11.3"><space quantity="4" unit="char"></space><w n="30.1">Tourbillonner</w>, <w n="30.2">et</w> <w n="30.3">joindre</w> <w n="30.4">aux</w> <w n="30.5">voix</w></l>
					<l n="31" num="11.4"><space quantity="4" unit="char"></space><w n="31.1">La</w> <w n="31.2">flûte</w> <w n="31.3">et</w> <w n="31.4">les</w> <w n="31.5">tambours</w> <w n="31.6">de</w> <w n="31.7">basque</w> !</l>
				</lg>
				<lg n="12">
					<l n="32" num="12.1"><w n="32.1">La</w> <w n="32.2">treille</w> <w n="32.3">a</w> <w n="32.4">ployé</w> <w n="32.5">tout</w> <w n="32.6">le</w> <w n="32.7">long</w> <w n="32.8">des</w> <w n="32.9">murs</w>,</l>
					<l n="33" num="12.2"><w n="33.1">Allez</w>, <w n="33.2">vendangeurs</w>, <w n="33.3">les</w> <w n="33.4">raisins</w> <w n="33.5">sont</w> <w n="33.6">mûrs</w> !</l>
				</lg>
				<lg n="13">
					<l n="34" num="13.1"><space quantity="4" unit="char"></space><w n="34.1">C</w>’<w n="34.2">est</w> <w n="34.3">Galatée</w> <w n="34.4">ou</w> <w n="34.5">Vénus</w> <w n="34.6">même</w></l>
					<l n="35" num="13.2"><space quantity="4" unit="char"></space><w n="35.1">Qui</w>, <w n="35.2">dans</w> <w n="35.3">l</w>’<w n="35.4">éclat</w> <w n="35.5">du</w> <w n="35.6">flot</w> <w n="35.7">profond</w>,</l>
					<l n="36" num="13.3"><space quantity="4" unit="char"></space><w n="36.1">Se</w> <w n="36.2">joue</w> <w n="36.3">et</w> <w n="36.4">me</w> <w n="36.5">sourit</w> <w n="36.6">au</w> <w n="36.7">fond</w></l>
					<l n="37" num="13.4"><space quantity="4" unit="char"></space><w n="37.1">De</w> <w n="37.2">mon</w> <w n="37.3">grand</w> <w n="37.4">verre</w> <w n="37.5">de</w> <w n="37.6">Bohême</w>.</l>
				</lg>
				<lg n="14">
					<l rhyme="none" n="38" num="14.1"><space quantity="16" unit="char"></space><w n="38.1">Chantons</w> <w n="38.2">Io</w> <w n="38.3">Pæan</w> !</l>
				</lg>
				<lg n="15">
					<l n="39" num="15.1"><space quantity="4" unit="char"></space><w n="39.1">Cette</w> <w n="39.2">autre</w> <w n="39.3">Cypris</w>, <w n="39.4">plus</w> <w n="39.5">galante</w>,</l>
					<l n="40" num="15.2"><space quantity="4" unit="char"></space><w n="40.1">Naît</w> <w n="40.2">du</w> <w n="40.3">nectar</w> <w n="40.4">si</w> <w n="40.5">bien</w> <w n="40.6">chanté</w>,</l>
					<l n="41" num="15.3"><space quantity="4" unit="char"></space><w n="41.1">Et</w> <w n="41.2">laisse</w> <w n="41.3">voir</w> <w n="41.4">sa</w> <w n="41.5">nudité</w></l>
					<l n="42" num="15.4"><space quantity="4" unit="char"></space><w n="42.1">Sous</w> <w n="42.2">une</w> <w n="42.3">pourpre</w> <w n="42.4">étincelante</w>.</l>
				</lg>
				<lg n="16">
					<l n="43" num="16.1"><w n="43.1">La</w> <w n="43.2">treille</w> <w n="43.3">a</w> <w n="43.4">ployé</w> <w n="43.5">tout</w> <w n="43.6">le</w> <w n="43.7">long</w> <w n="43.8">des</w> <w n="43.9">murs</w>,</l>
					<l n="44" num="16.2"><w n="44.1">Allez</w>, <w n="44.2">vendangeurs</w>, <w n="44.3">les</w> <w n="44.4">raisins</w> <w n="44.5">sont</w> <w n="44.6">mûrs</w> !</l>
				</lg>
				<lg n="17">
					<l n="45" num="17.1"><space quantity="4" unit="char"></space><w n="45.1">Plus</w> <w n="45.2">d</w>’<w n="45.3">amante</w> <w n="45.4">froide</w> <w n="45.5">ou</w> <w n="45.6">traîtresse</w>,</l>
					<l n="46" num="17.2"><space quantity="4" unit="char"></space><w n="46.1">Plus</w> <w n="46.2">de</w> <w n="46.3">poëtes</w> <w n="46.4">envieux</w> !</l>
					<l n="47" num="17.3"><space quantity="4" unit="char"></space><w n="47.1">Dans</w> <w n="47.2">ce</w> <w n="47.3">grand</w> <w n="47.4">verre</w> <w n="47.5">de</w> <w n="47.6">vin</w> <w n="47.7">vieux</w></l>
					<l n="48" num="17.4"><space quantity="4" unit="char"></space><w n="48.1">Pleure</w> <w n="48.2">une</w> <w n="48.3">immortelle</w> <w n="48.4">maîtresse</w>,</l>
				</lg>
				<lg n="18">
					<l rhyme="none" n="49" num="18.1"><space quantity="16" unit="char"></space><w n="49.1">Chantons</w> <w n="49.2">Io</w> <w n="49.3">Pæan</w> !</l>
				</lg>
				<lg n="19">
					<l n="50" num="19.1"><space quantity="4" unit="char"></space><w n="50.1">Et</w>, <w n="50.2">comme</w> <w n="50.3">un</w> <w n="50.4">ballet</w> <w n="50.5">magnifique</w>,</l>
					<l n="51" num="19.2"><space quantity="4" unit="char"></space><w n="51.1">Je</w> <w n="51.2">vois</w>, <w n="51.3">dans</w> <w n="51.4">le</w> <w n="51.5">flacon</w> <w n="51.6">vermeil</w>,</l>
					<l n="52" num="19.3"><space quantity="4" unit="char"></space><w n="52.1">Couleur</w> <w n="52.2">de</w> <w n="52.3">lune</w> <w n="52.4">et</w> <w n="52.5">de</w> <w n="52.6">soleil</w>,</l>
					<l n="53" num="19.4"><space quantity="4" unit="char"></space><w n="53.1">Des</w> <w n="53.2">rhythmes</w> <w n="53.3">danser</w> <w n="53.4">en</w> <w n="53.5">musique</w> !</l>
				</lg>
				<lg n="20">
					<l n="54" num="20.1"><w n="54.1">La</w> <w n="54.2">treille</w> <w n="54.3">a</w> <w n="54.4">ployé</w> <w n="54.5">tout</w> <w n="54.6">le</w> <w n="54.7">long</w> <w n="54.8">des</w> <w n="54.9">murs</w>,</l>
					<l n="55" num="20.2"><w n="55.1">Allez</w>, <w n="55.2">vendangeurs</w>, <w n="55.3">les</w> <w n="55.4">raisins</w> <w n="55.5">sont</w> <w n="55.6">mûrs</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Septembre 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>