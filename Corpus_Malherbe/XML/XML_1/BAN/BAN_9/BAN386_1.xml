<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN386">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Chère</w>, <w n="1.2">voici</w> <w n="1.3">le</w> <w n="1.4">mois</w> <w n="1.5">de</w> <w n="1.6">mai</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">mois</w> <w n="2.3">du</w> <w n="2.4">printemps</w> <w n="2.5">parfumé</w></l>
					<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">Qui</w>, <w n="3.2">sous</w> <w n="3.3">les</w> <w n="3.4">branches</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Fait</w> <w n="4.2">vibrer</w> <w n="4.3">des</w> <w n="4.4">sons</w> <w n="4.5">inconnus</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">couvre</w> <w n="5.3">les</w> <w n="5.4">seins</w> <w n="5.5">demi</w>-<w n="5.6">nus</w></l>
					<l n="6" num="1.6"><space quantity="8" unit="char"></space><w n="6.1">De</w> <w n="6.2">robes</w> <w n="6.3">blanches</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Voici</w> <w n="7.2">la</w> <w n="7.3">saison</w> <w n="7.4">des</w> <w n="7.5">doux</w> <w n="7.6">nids</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Le</w> <w n="8.2">temps</w> <w n="8.3">où</w> <w n="8.4">les</w> <w n="8.5">cieux</w> <w n="8.6">rajeunis</w></l>
					<l n="9" num="2.3"><space quantity="8" unit="char"></space><w n="9.1">Sont</w> <w n="9.2">tout</w> <w n="9.3">en</w> <w n="9.4">flamme</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Où</w> <w n="10.2">déjà</w>, <w n="10.3">tout</w> <w n="10.4">le</w> <w n="10.5">long</w> <w n="10.6">du</w> <w n="10.7">jour</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Le</w> <w n="11.2">doux</w> <w n="11.3">rossignol</w> <w n="11.4">de</w> <w n="11.5">l</w>’<w n="11.6">amour</w></l>
					<l n="12" num="2.6"><space quantity="8" unit="char"></space><w n="12.1">Chante</w> <w n="12.2">dans</w> <w n="12.3">l</w>’<w n="12.4">âme</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Ah</w> ! <w n="13.2">de</w> <w n="13.3">quels</w> <w n="13.4">suaves</w> <w n="13.5">rayons</w></l>
					<l n="14" num="3.2"><w n="14.1">Se</w> <w n="14.2">dorent</w> <w n="14.3">nos</w> <w n="14.4">illusions</w></l>
					<l n="15" num="3.3"><space quantity="12" unit="char"></space><w n="15.1">Les</w> <w n="15.2">plus</w> <w n="15.3">chéries</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Et</w> <w n="16.2">combien</w> <w n="16.3">de</w> <w n="16.4">charmants</w> <w n="16.5">espoirs</w></l>
					<l n="17" num="3.5"><w n="17.1">Nous</w> <w n="17.2">jettent</w> <w n="17.3">dans</w> <w n="17.4">l</w>’<w n="17.5">ombre</w> <w n="17.6">des</w> <w n="17.7">soirs</w></l>
					<l n="18" num="3.6"><space quantity="8" unit="char"></space><w n="18.1">Leurs</w> <w n="18.2">rêveries</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Parmi</w> <w n="19.2">nos</w> <w n="19.3">rêves</w> <w n="19.4">à</w> <w n="19.5">tous</w> <w n="19.6">deux</w>,</l>
					<l n="20" num="4.2"><w n="20.1">Beaux</w> <w n="20.2">projets</w> <w n="20.3">souvent</w> <w n="20.4">hasardeux</w></l>
					<l n="21" num="4.3"><space quantity="8" unit="char"></space><w n="21.1">Qui</w> <w n="21.2">sont</w> <w n="21.3">les</w> <w n="21.4">mêmes</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Songes</w> <w n="22.2">pleins</w> <w n="22.3">d</w>’<w n="22.4">amour</w> <w n="22.5">et</w> <w n="22.6">de</w> <w n="22.7">foi</w></l>
					<l n="23" num="4.5"><w n="23.1">Que</w> <w n="23.2">tu</w> <w n="23.3">dois</w> <w n="23.4">avoir</w> <w n="23.5">comme</w> <w n="23.6">moi</w>,</l>
					<l n="24" num="4.6"><space quantity="8" unit="char"></space><w n="24.1">Puisque</w> <w n="24.2">tu</w> <w n="24.3">m</w>’<w n="24.4">aimes</w> ;</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Il</w> <w n="25.2">en</w> <w n="25.3">est</w> <w n="25.4">un</w> <w n="25.5">seul</w> <w n="25.6">plus</w> <w n="25.7">aimé</w>.</l>
					<l n="26" num="5.2"><w n="26.1">Tel</w> <w n="26.2">meurt</w> <w n="26.3">un</w> <w n="26.4">zéphyr</w> <w n="26.5">embaumé</w></l>
					<l n="27" num="5.3"><space quantity="8" unit="char"></space><w n="27.1">Sur</w> <w n="27.2">votre</w> <w n="27.3">bouche</w>,</l>
					<l n="28" num="5.4"><w n="28.1">Telle</w>, <w n="28.2">par</w> <w n="28.3">une</w> <w n="28.4">ardente</w> <w n="28.5">nuit</w>,</l>
					<l n="29" num="5.5"><w n="29.1">De</w> <w n="29.2">quelque</w> <w n="29.3">Séraphin</w>, <w n="29.4">sans</w> <w n="29.5">bruit</w>,</l>
					<l n="30" num="5.6"><space quantity="12" unit="char"></space><w n="30.1">L</w>’<w n="30.2">aile</w> <w n="30.3">vous</w> <w n="30.4">touche</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Camille</w>, <w n="31.2">as</w>-<w n="31.3">tu</w> <w n="31.4">rêvé</w> <w n="31.5">parfois</w></l>
					<l n="32" num="6.2"><w n="32.1">Qu</w>’<w n="32.2">à</w> <w n="32.3">l</w>’<w n="32.4">heure</w> <w n="32.5">où</w> <w n="32.6">s</w>’<w n="32.7">éveillent</w> <w n="32.8">les</w> <w n="32.9">bois</w></l>
					<l n="33" num="6.3"><space quantity="8" unit="char"></space><w n="33.1">Et</w> <w n="33.2">l</w>’<w n="33.3">alouette</w>,</l>
					<l n="34" num="6.4"><w n="34.1">Où</w> <w n="34.2">Roméo</w>, <w n="34.3">vingt</w> <w n="34.4">fois</w> <w n="34.5">baisé</w>,</l>
					<l n="35" num="6.5"><w n="35.1">Enjambe</w> <w n="35.2">le</w> <w n="35.3">balcon</w> <w n="35.4">brisé</w></l>
					<l n="36" num="6.6"><space quantity="8" unit="char"></space><w n="36.1">De</w> <w n="36.2">Juliette</w>,</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Nous</w> <w n="37.2">partons</w> <w n="37.3">tous</w> <w n="37.4">les</w> <w n="37.5">deux</w>, <w n="37.6">tout</w> <w n="37.7">seuls</w> ?</l>
					<l n="38" num="7.2"><w n="38.1">Hors</w> <w n="38.2">Paris</w>, <w n="38.3">dans</w> <w n="38.4">les</w> <w n="38.5">grands</w> <w n="38.6">tilleuls</w></l>
					<l n="39" num="7.3"><space quantity="8" unit="char"></space><w n="39.1">Un</w> <w n="39.2">rayon</w> <w n="39.3">joue</w> ;</l>
					<l n="40" num="7.4"><w n="40.1">L</w>’<w n="40.2">air</w> <w n="40.3">sent</w> <w n="40.4">les</w> <w n="40.5">lilas</w> <w n="40.6">et</w> <w n="40.7">le</w> <w n="40.8">thym</w>,</l>
					<l n="41" num="7.5"><w n="41.1">La</w> <w n="41.2">fraîche</w> <w n="41.3">brise</w> <w n="41.4">du</w> <w n="41.5">matin</w></l>
					<l n="42" num="7.6"><space quantity="8" unit="char"></space><w n="42.1">Baise</w> <w n="42.2">ta</w> <w n="42.3">joue</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Après</w> <w n="43.2">avoir</w> <w n="43.3">passé</w> <w n="43.4">tout</w> <w n="43.5">près</w></l>
					<l n="44" num="8.2"><w n="44.1">De</w> <w n="44.2">vastes</w> <w n="44.3">ombrages</w>, <w n="44.4">plus</w> <w n="44.5">frais</w></l>
					<l n="45" num="8.3"><space quantity="12" unit="char"></space><w n="45.1">Qu</w>’<w n="45.2">une</w> <w n="45.3">glacière</w></l>
					<l n="46" num="8.4"><w n="46.1">Et</w> <w n="46.2">tout</w> <w n="46.3">pleins</w> <w n="46.4">de</w> <w n="46.5">charmants</w> <w n="46.6">abords</w>,</l>
					<l n="47" num="8.5"><w n="47.1">Nous</w> <w n="47.2">allons</w> <w n="47.3">nous</w> <w n="47.4">asseoir</w> <w n="47.5">aux</w> <w n="47.6">bords</w></l>
					<l n="48" num="8.6"><space quantity="8" unit="char"></space><w n="48.1">De</w> <w n="48.2">la</w> <w n="48.3">rivière</w>.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">L</w>’<w n="49.2">eau</w> <w n="49.3">frémit</w>, <w n="49.4">le</w> <w n="49.5">poisson</w> <w n="49.6">changeant</w></l>
					<l n="50" num="9.2"><w n="50.1">Émaille</w> <w n="50.2">la</w> <w n="50.3">vague</w> <w n="50.4">d</w>’<w n="50.5">argent</w></l>
					<l n="51" num="9.3"><space quantity="8" unit="char"></space><w n="51.1">D</w>’<w n="51.2">écailles</w> <w n="51.3">blondes</w> ;</l>
					<l n="52" num="9.4"><w n="52.1">Le</w> <w n="52.2">saule</w>, <w n="52.3">arbre</w> <w n="52.4">des</w> <w n="52.5">tristes</w> <w n="52.6">vœux</w>,</l>
					<l n="53" num="9.5"><w n="53.1">Pleure</w>, <w n="53.2">et</w> <w n="53.3">baigne</w> <w n="53.4">ses</w> <w n="53.5">longs</w> <w n="53.6">cheveux</w></l>
					<l n="54" num="9.6"><space quantity="8" unit="char"></space><w n="54.1">Parmi</w> <w n="54.2">les</w> <w n="54.3">ondes</w>.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1"><w n="55.1">Tout</w> <w n="55.2">est</w> <w n="55.3">calme</w> <w n="55.4">et</w> <w n="55.5">silencieux</w>.</l>
					<l n="56" num="10.2"><w n="56.1">Étoiles</w> <w n="56.2">que</w> <w n="56.3">la</w> <w n="56.4">terre</w> <w n="56.5">aux</w> <w n="56.6">cieux</w></l>
					<l n="57" num="10.3"><space quantity="8" unit="char"></space><w n="57.1">A</w> <w n="57.2">dérobées</w>,</l>
					<l n="58" num="10.4"><w n="58.1">On</w> <w n="58.2">voit</w> <w n="58.3">briller</w> <w n="58.4">d</w>’<w n="58.5">un</w> <w n="58.6">éclat</w> <w n="58.7">pur</w></l>
					<l n="59" num="10.5"><w n="59.1">Les</w> <w n="59.2">corsages</w> <w n="59.3">d</w>’<w n="59.4">or</w> <w n="59.5">et</w> <w n="59.6">d</w>’<w n="59.7">azur</w></l>
					<l n="60" num="10.6"><space quantity="12" unit="char"></space><w n="60.1">Des</w> <w n="60.2">scarabées</w>.</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1"><w n="61.1">Nos</w> <w n="61.2">yeux</w> <w n="61.3">s</w>’<w n="61.4">enivrent</w>, <w n="61.5">assouplis</w>,</l>
					<l n="62" num="11.2"><w n="62.1">A</w> <w n="62.2">voir</w> <w n="62.3">l</w>’<w n="62.4">eau</w> <w n="62.5">dérouler</w> <w n="62.6">les</w> <w n="62.7">plis</w></l>
					<l n="63" num="11.3"><space quantity="8" unit="char"></space><w n="63.1">De</w> <w n="63.2">sa</w> <w n="63.3">ceinture</w>.</l>
					<l n="64" num="11.4"><w n="64.1">Je</w> <w n="64.2">baise</w> <w n="64.3">en</w> <w n="64.4">pleurant</w> <w n="64.5">tes</w> <w n="64.6">genoux</w>,</l>
					<l n="65" num="11.5"><w n="65.1">Et</w> <w n="65.2">nous</w> <w n="65.3">sommes</w> <w n="65.4">seuls</w>, <w n="65.5">rien</w> <w n="65.6">que</w> <w n="65.7">nous</w></l>
					<l n="66" num="11.6"><space quantity="8" unit="char"></space><w n="66.1">Et</w> <w n="66.2">la</w> <w n="66.3">nature</w> !</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1"><w n="67.1">Tout</w> <w n="67.2">alors</w>, <w n="67.3">les</w> <w n="67.4">flots</w> <w n="67.5">enchanteurs</w>,</l>
					<l n="68" num="12.2"><w n="68.1">L</w>’<w n="68.2">arbre</w> <w n="68.3">ému</w>, <w n="68.4">les</w> <w n="68.5">oiseaux</w> <w n="68.6">chanteurs</w></l>
					<l n="69" num="12.3"><space quantity="8" unit="char"></space><w n="69.1">Et</w> <w n="69.2">les</w> <w n="69.3">feuillées</w>,</l>
					<l n="70" num="12.4"><w n="70.1">Et</w> <w n="70.2">les</w> <w n="70.3">voix</w> <w n="70.4">aux</w> <w n="70.5">accords</w> <w n="70.6">touchants</w></l>
					<l n="71" num="12.5"><w n="71.1">Que</w> <w n="71.2">le</w> <w n="71.3">silence</w> <w n="71.4">dans</w> <w n="71.5">les</w> <w n="71.6">champs</w></l>
					<l n="72" num="12.6"><space quantity="8" unit="char"></space><w n="72.1">Tient</w> <w n="72.2">éveillées</w>,</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1"><w n="73.1">La</w> <w n="73.2">brise</w> <w n="73.3">aux</w> <w n="73.4">parfums</w> <w n="73.5">caressants</w>,</l>
					<l n="74" num="13.2"><w n="74.1">Les</w> <w n="74.2">horizons</w> <w n="74.3">éblouissants</w></l>
					<l n="75" num="13.3"><space quantity="12" unit="char"></space><w n="75.1">De</w> <w n="75.2">fantaisie</w>,</l>
					<l n="76" num="13.4"><w n="76.1">Les</w> <w n="76.2">serments</w> <w n="76.3">dans</w> <w n="76.4">nos</w> <w n="76.5">cœurs</w> <w n="76.6">écrits</w>,</l>
					<l n="77" num="13.5"><w n="77.1">Tout</w> <w n="77.2">en</w> <w n="77.3">nous</w> <w n="77.4">demande</w> <w n="77.5">à</w> <w n="77.6">grands</w> <w n="77.7">cris</w></l>
					<l n="78" num="13.6"><space quantity="8" unit="char"></space><w n="78.1">La</w> <w n="78.2">Poésie</w>.</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1"><w n="79.1">Nous</w> <w n="79.2">sommes</w> <w n="79.3">heureux</w> <w n="79.4">sans</w> <w n="79.5">froideur</w>.</l>
					<l n="80" num="14.2"><w n="80.1">Plus</w> <w n="80.2">de</w> <w n="80.3">bouderie</w> <w n="80.4">ou</w> <w n="80.5">d</w>’<w n="80.6">humeur</w></l>
					<l n="81" num="14.3"><space quantity="8" unit="char"></space><w n="81.1">Triste</w> <w n="81.2">ou</w> <w n="81.3">chagrine</w> ;</l>
					<l n="82" num="14.4"><w n="82.1">Tu</w> <w n="82.2">poses</w> <w n="82.3">d</w>’<w n="82.4">un</w> <w n="82.5">air</w> <w n="82.6">triomphant</w></l>
					<l n="83" num="14.5"><w n="83.1">Ta</w> <w n="83.2">petite</w> <w n="83.3">tête</w> <w n="83.4">d</w>’<w n="83.5">enfant</w></l>
					<l n="84" num="14.6"><space quantity="8" unit="char"></space><w n="84.1">Sur</w> <w n="84.2">ma</w> <w n="84.3">poitrine</w> ;</l>
				</lg>
				<lg n="15">
					<l n="85" num="15.1"><w n="85.1">Tu</w> <w n="85.2">m</w>’<w n="85.3">écoutes</w>, <w n="85.4">et</w> <w n="85.5">je</w> <w n="85.6">te</w> <w n="85.7">lis</w>,</l>
					<l n="86" num="15.2"><w n="86.1">Quoique</w> <w n="86.2">ta</w> <w n="86.3">bouche</w> <w n="86.4">aux</w> <w n="86.5">coins</w> <w n="86.6">pâlis</w></l>
					<l n="87" num="15.3"><space quantity="8" unit="char"></space><w n="87.1">S</w>’<w n="87.2">ouvre</w> <w n="87.3">et</w> <w n="87.4">soupire</w>,</l>
					<l n="88" num="15.4"><w n="88.1">Quelques</w> <w n="88.2">stances</w> <w n="88.3">d</w>’<w n="88.4">Alighieri</w>,</l>
					<l n="89" num="15.5"><w n="89.1">Ronsard</w>, <w n="89.2">le</w> <w n="89.3">poëte</w> <w n="89.4">chéri</w>,</l>
					<l n="90" num="15.6"><space quantity="12" unit="char"></space><w n="90.1">Ou</w> <w n="90.2">bien</w> <w n="90.3">Shakspere</w>.</l>
				</lg>
				<lg n="16">
					<l n="91" num="16.1"><w n="91.1">Mais</w> <w n="91.2">je</w> <w n="91.3">jette</w> <w n="91.4">le</w> <w n="91.5">livre</w> <w n="91.6">ouvert</w>,</l>
					<l n="92" num="16.2"><w n="92.1">Tandis</w> <w n="92.2">que</w> <w n="92.3">ton</w> <w n="92.4">regard</w> <w n="92.5">se</w> <w n="92.6">perd</w></l>
					<l n="93" num="16.3"><space quantity="8" unit="char"></space><w n="93.1">Parmi</w> <w n="93.2">les</w> <w n="93.3">mousses</w>,</l>
					<l n="94" num="16.4"><w n="94.1">Et</w> <w n="94.2">je</w> <w n="94.3">préfère</w>, <w n="94.4">en</w> <w n="94.5">vrai</w> <w n="94.6">jaloux</w>,</l>
					<l n="95" num="16.5"><w n="95.1">A</w> <w n="95.2">nos</w> <w n="95.3">poëtes</w> <w n="95.4">les</w> <w n="95.5">plus</w> <w n="95.6">doux</w></l>
					<l n="96" num="16.6"><space quantity="8" unit="char"></space><w n="96.1">Tes</w> <w n="96.2">lèvres</w> <w n="96.3">douces</w> !</l>
				</lg>
				<lg n="17">
					<l n="97" num="17.1"><w n="97.1">Tiens</w>, <w n="97.2">voici</w> <w n="97.3">qu</w>’<w n="97.4">un</w> <w n="97.5">couple</w> <w n="97.6">charmant</w>,</l>
					<l n="98" num="17.2"><w n="98.1">Comme</w> <w n="98.2">nous</w> <w n="98.3">jeune</w> <w n="98.4">et</w> <w n="98.5">bien</w> <w n="98.6">aimant</w>,</l>
					<l n="99" num="17.3"><space quantity="8" unit="char"></space><w n="99.1">Vient</w> <w n="99.2">et</w> <w n="99.3">regarde</w>.</l>
					<l n="100" num="17.4"><w n="100.1">Que</w> <w n="100.2">de</w> <w n="100.3">bonheur</w> <w n="100.4">rien</w> <w n="100.5">qu</w>’<w n="100.6">à</w> <w n="100.7">leurs</w> <w n="100.8">pas</w> !</l>
					<l n="101" num="17.5"><w n="101.1">Ils</w> <w n="101.2">passent</w> <w n="101.3">et</w> <w n="101.4">ne</w> <w n="101.5">nous</w> <w n="101.6">voient</w> <w n="101.7">pas</w> :</l>
					<l n="102" num="17.6"><space quantity="8" unit="char"></space><w n="102.1">Que</w> <w n="102.2">Dieu</w> <w n="102.3">les</w> <w n="102.4">garde</w> !</l>
				</lg>
				<lg n="18">
					<l n="103" num="18.1"><w n="103.1">Ce</w> <w n="103.2">sont</w> <w n="103.3">des</w> <w n="103.4">frères</w>, <w n="103.5">mon</w> <w n="103.6">cher</w> <w n="103.7">cœur</w>,</l>
					<l n="104" num="18.2"><w n="104.1">Que</w>, <w n="104.2">comme</w> <w n="104.3">nous</w>, <w n="104.4">l</w>’<w n="104.5">amour</w> <w n="104.6">vainqueur</w></l>
					<l n="105" num="18.3"><space quantity="10" unit="char"></space><w n="105.1">Fit</w> <w n="105.2">l</w>’<w n="105.3">un</w> <w n="105.4">pour</w> <w n="105.5">l</w>’<w n="105.6">autre</w>.</l>
					<l n="106" num="18.4"><w n="106.1">Ah</w> ! <w n="106.2">qu</w>’<w n="106.3">ils</w> <w n="106.4">soient</w> <w n="106.5">heureux</w> <w n="106.6">à</w> <w n="106.7">leur</w> <w n="106.8">tour</w> !</l>
					<l n="107" num="18.5"><w n="107.1">Embrassons</w>-<w n="107.2">nous</w> <w n="107.3">pour</w> <w n="107.4">leur</w> <w n="107.5">amour</w></l>
					<l n="108" num="18.6"><space quantity="8" unit="char"></space><w n="108.1">Et</w> <w n="108.2">pour</w> <w n="108.3">le</w> <w n="108.4">nôtre</w> !</l>
				</lg>
				<lg n="19">
					<l n="109" num="19.1"><w n="109.1">Chère</w>, <w n="109.2">quel</w> <w n="109.3">ineffable</w> <w n="109.4">émoi</w>,</l>
					<l n="110" num="19.2"><w n="110.1">Sur</w> <w n="110.2">ce</w> <w n="110.3">rivage</w> <w n="110.4">où</w> <w n="110.5">près</w> <w n="110.6">de</w> <w n="110.7">moi</w></l>
					<l n="111" num="19.3"><space quantity="8" unit="char"></space><w n="111.1">Tu</w> <w n="111.2">te</w> <w n="111.3">recueilles</w>,</l>
					<l n="112" num="19.4"><w n="112.1">De</w> <w n="112.2">mêler</w> <w n="112.3">d</w>’<w n="112.4">amoureux</w> <w n="112.5">sanglots</w></l>
					<l n="113" num="19.5"><w n="113.1">Aux</w> <w n="113.2">douces</w> <w n="113.3">plaintes</w> <w n="113.4">que</w> <w n="113.5">les</w> <w n="113.6">flots</w></l>
					<l n="114" num="19.6"><space quantity="8" unit="char"></space><w n="114.1">Disent</w> <w n="114.2">aux</w> <w n="114.3">feuilles</w> !</l>
				</lg>
				<lg n="20">
					<l n="115" num="20.1"><w n="115.1">Dis</w>, <w n="115.2">quel</w> <w n="115.3">bonheur</w> <w n="115.4">d</w>’<w n="115.5">être</w> <w n="115.6">enlacés</w></l>
					<l n="116" num="20.2"><w n="116.1">Par</w> <w n="116.2">des</w> <w n="116.3">bras</w> <w n="116.4">forts</w>, <w n="116.5">jamais</w> <w n="116.6">lassés</w> !</l>
					<l n="117" num="20.3"><space quantity="8" unit="char"></space><w n="117.1">Avec</w> <w n="117.2">quels</w> <w n="117.3">charmes</w>,</l>
					<l n="118" num="20.4"><w n="118.1">Après</w> <w n="118.2">tous</w> <w n="118.3">nos</w> <w n="118.4">mortels</w> <w n="118.5">exils</w>,</l>
					<l n="119" num="20.5"><w n="119.1">Je</w> <w n="119.2">savoure</w> <w n="119.3">au</w> <w n="119.4">bout</w> <w n="119.5">de</w> <w n="119.6">tes</w> <w n="119.7">cils</w></l>
					<l n="120" num="20.6"><space quantity="10" unit="char"></space><w n="120.1">De</w> <w n="120.2">fraîches</w> <w n="120.3">larmes</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1844">Avril 1844.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>