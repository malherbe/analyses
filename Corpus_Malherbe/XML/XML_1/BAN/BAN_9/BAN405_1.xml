<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES STALACTITES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1359 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_9</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LES STALACTITES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesstalactictes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Les stalactites, Odelettes, Améthystes, Le Forgeron.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1889">1889</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1846">1843-1846</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN405">
				<head type="main">L’Âme de la Lyre</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Fille des hommes, je suis une parcelle de <lb></lb>
								l’esprit de Dieu. Cette Lyre est mon corps.
							</quote>
							<bibl>
								<name>George Sand</name>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">le</w> <w n="1.3">premier</w> <w n="1.4">sculpteur</w> <w n="1.5">eut</w> <w n="1.6">achevé</w> <w n="1.7">la</w> <w n="1.8">Lyre</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">caché</w> <w n="2.3">dans</w> <w n="2.4">son</w> <w n="2.5">sein</w> <w n="2.6">les</w> <w n="2.7">chants</w> <w n="2.8">harmonieux</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Ouvrier</w> <w n="3.2">sans</w> <w n="3.3">défaut</w>, <w n="3.4">lorsqu</w>’<w n="3.5">il</w> <w n="3.6">eut</w> <w n="3.7">fait</w> <w n="3.8">sourire</w></l>
					<l n="4" num="1.4"><w n="4.1">Parmi</w> <w n="4.2">ses</w> <w n="4.3">ornements</w> <w n="4.4">les</w> <w n="4.5">figures</w> <w n="4.6">des</w> <w n="4.7">Dieux</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">qu</w>’<w n="5.3">il</w> <w n="5.4">eut</w> <w n="5.5">couronné</w> <w n="5.6">l</w>’<w n="5.7">instrument</w> <w n="5.8">de</w> <w n="5.9">martyre</w></l>
					<l n="6" num="1.6"><w n="6.1">Avec</w> <w n="6.2">le</w> <w n="6.3">vert</w> <w n="6.4">rameau</w> <w n="6.5">d</w>’<w n="6.6">un</w> <w n="6.7">laurier</w> <w n="6.8">radieux</w> ;</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">L</w>’<w n="7.2">indomptable</w> <w n="7.3">Titan</w>, <w n="7.4">à</w> <w n="7.5">son</w> <w n="7.6">désir</w> <w n="7.7">fidèle</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Qui</w>, <w n="8.2">tout</w> <w n="8.3">brûlant</w> <w n="8.4">encor</w>, <w n="8.5">vers</w> <w n="8.6">la</w> <w n="8.7">voûte</w> <w n="8.8">éternelle</w></l>
					<l n="9" num="2.3"><w n="9.1">Une</w> <w n="9.2">seconde</w> <w n="9.3">fois</w>, <w n="9.4">tentait</w> <w n="9.5">de</w> <w n="9.6">s</w>’<w n="9.7">envoler</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Fit</w>, <w n="10.2">pareil</w> <w n="10.3">au</w> <w n="10.4">vautour</w> <w n="10.5">qui</w> <w n="10.6">devait</w> <w n="10.7">l</w>’<w n="10.8">immoler</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Tomber</w> <w n="11.2">sur</w> <w n="11.3">le</w> <w n="11.4">chef</w>-<w n="11.5">d</w>’<w n="11.6">œuvre</w> <w n="11.7">une</w> <w n="11.8">blanche</w> <w n="11.9">étincelle</w></l>
					<l n="12" num="2.6"><w n="12.1">Du</w> <w n="12.2">feu</w> <w n="12.3">resplendissant</w> <w n="12.4">qu</w>’<w n="12.5">il</w> <w n="12.6">venait</w> <w n="12.7">de</w> <w n="12.8">voler</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">l</w>’<w n="13.4">âme</w> <w n="13.5">de</w> <w n="13.6">la</w> <w n="13.7">Lyre</w> ; <w n="13.8">à</w> <w n="13.9">notre</w> <w n="13.10">âme</w> <w n="13.11">invisible</w></l>
					<l n="14" num="3.2"><w n="14.1">Elle</w> <w n="14.2">se</w> <w n="14.3">plaint</w> <w n="14.4">souvent</w> <w n="14.5">loin</w> <w n="14.6">du</w> <w n="14.7">monde</w> <w n="14.8">réel</w>,</l>
					<l n="15" num="3.3"><w n="15.1">Souvent</w>, <w n="15.2">dans</w> <w n="15.3">une</w> <w n="15.4">étreinte</w> <w n="15.5">amoureuse</w> <w n="15.6">et</w> <w n="15.7">terrible</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Vient</w> <w n="16.2">la</w> <w n="16.3">brûler</w> <w n="16.4">aux</w> <w n="16.5">feux</w> <w n="16.6">de</w> <w n="16.7">son</w> <w n="16.8">œil</w> <w n="16.9">immortel</w> ;</l>
					<l n="17" num="3.5"><w n="17.1">Et</w>, <w n="17.2">captive</w> <w n="17.3">à</w> <w n="17.4">jamais</w> <w n="17.5">dans</w> <w n="17.6">le</w> <w n="17.7">rhythme</w> <w n="17.8">inflexible</w>,</l>
					<l n="18" num="3.6"><w n="18.1">Elle</w> <w n="18.2">aspire</w> <w n="18.3">sans</w> <w n="18.4">cesse</w> <w n="18.5">à</w> <w n="18.6">remonter</w> <w n="18.7">au</w> <w n="18.8">ciel</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Elle</w> <w n="19.2">meurt</w> <w n="19.3">du</w> <w n="19.4">désir</w> <w n="19.5">qui</w> <w n="19.6">toujours</w> <w n="19.7">la</w> <w n="19.8">dévore</w></l>
					<l n="20" num="4.2"><w n="20.1">Dans</w> <w n="20.2">la</w> <w n="20.3">froide</w> <w n="20.4">prison</w> <w n="20.5">des</w> <w n="20.6">mètres</w> <w n="20.7">et</w> <w n="20.8">des</w> <w n="20.9">vers</w>,</l>
					<l n="21" num="4.3"><w n="21.1">Et</w> <w n="21.2">tâche</w>, <w n="21.3">l</w>’<w n="21.4">œil</w> <w n="21.5">perdu</w> <w n="21.6">parmi</w> <w n="21.7">les</w> <w n="21.8">cieux</w> <w n="21.9">ouverts</w>,</l>
					<l n="22" num="4.4"><w n="22.1">D</w>’<w n="22.2">entendre</w> <w n="22.3">encor</w> <w n="22.4">la</w> <w n="22.5">voix</w> <w n="22.6">de</w> <w n="22.7">cet</w> <w n="22.8">archet</w> <w n="22.9">sonore</w></l>
					<l n="23" num="4.5"><w n="23.1">Qui</w>, <w n="23.2">si</w> <w n="23.3">loin</w> <w n="23.4">du</w> <w n="23.5">désert</w> <w n="23.6">où</w> <w n="23.7">ses</w> <w n="23.8">chants</w> <w n="23.9">vont</w> <w n="23.10">éclore</w>,</l>
					<l n="24" num="4.6"><w n="24.1">Mène</w> <w n="24.2">dans</w> <w n="24.3">l</w>’<w n="24.4">infini</w> <w n="24.5">le</w> <w n="24.6">chœur</w> <w n="24.7">de</w> <w n="24.8">l</w>’<w n="24.9">univers</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1845">Juin 1845.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>