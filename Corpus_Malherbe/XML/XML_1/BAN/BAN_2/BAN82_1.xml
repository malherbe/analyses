<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉVOHÉ</head><head type="sub_part">NÉMÉSIS INTÉRIMAIRE</head><div type="poem" key="BAN82">
					<head type="form">SATIRE CINQUIÈME</head>
					<head type="main">L’AMOUR À PARIS</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="2" unit="char"></space><w n="1.1">Fille</w> <w n="1.2">du</w> <w n="1.3">grand</w> <w n="1.4">Daumier</w> <w n="1.5">ou</w> <w n="1.6">du</w> <w n="1.7">sublime</w> <w n="1.8">Cham</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Toi</w> <w n="2.2">qui</w> <w n="2.3">portes</w> <w n="2.4">du</w> <w n="2.5">reps</w> <w n="2.6">et</w> <w n="2.7">du</w> <w n="2.8">madapolam</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ô</w> <w n="3.2">muse</w> <w n="3.3">de</w> <w n="3.4">Paris</w> ! <w n="3.5">Toi</w> <w n="3.6">par</w> <w n="3.7">qui</w> <w n="3.8">l</w>’<w n="3.9">on</w> <w n="3.10">admire</w></l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">peignoirs</w> <w n="4.3">érudits</w> <w n="4.4">qui</w> <w n="4.5">naissent</w> <w n="4.6">chez</w> <w n="4.7">Palmyre</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Toi</w> <w n="5.2">pour</w> <w n="5.3">qui</w> <w n="5.4">notre</w> <w n="5.5">siècle</w> <w n="5.6">inventa</w> <w n="5.7">les</w> <hi rend="ital"><w n="5.8">corsets</w> </hi></l>
						<l n="6" num="1.6"><hi rend="ital"><w n="6.1">à</w> <w n="6.2">la</w> <w n="6.3">minute</w>, </hi><w n="6.4">amour</w> <w n="6.5">du</w> <w n="6.6">puff</w> <w n="6.7">et</w> <w n="6.8">du</w> <w n="6.9">succès</w> !</l>
						<l n="7" num="1.7"><w n="7.1">Toi</w> <w n="7.2">qui</w> <w n="7.3">chez</w> <w n="7.4">la</w> <w n="7.5">comtesse</w> <w n="7.6">et</w> <w n="7.7">chez</w> <w n="7.8">la</w> <w n="7.9">chambrière</w></l>
						<l n="8" num="1.8"><w n="8.1">Colportes</w> <w n="8.2">Marivaux</w> <w n="8.3">retouché</w> <w n="8.4">par</w> <w n="8.5">Barrière</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Précieuse</w> <w n="9.2">Évohé</w> ! <w n="9.3">Chante</w>, <w n="9.4">après</w> <w n="9.5">Gavarni</w>,</l>
						<l n="10" num="1.10"><w n="10.1">L</w>’<w n="10.2">amour</w> <w n="10.3">et</w> <w n="10.4">la</w> <w n="10.5">constance</w> <w n="10.6">en</w> <w n="10.7">brodequin</w> <w n="10.8">verni</w>.</l>
						<l n="11" num="1.11"><space quantity="2" unit="char"></space><w n="11.1">Dans</w> <w n="11.2">ces</w> <w n="11.3">pays</w> <w n="11.4">lointains</w> <w n="11.5">situés</w> <w n="11.6">à</w> <w n="11.7">dix</w> <w n="11.8">lieues</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Où</w> <w n="12.2">l</w>’<w n="12.3">Oise</w> <w n="12.4">dans</w> <w n="12.5">la</w> <w n="12.6">Seine</w> <w n="12.7">épanche</w> <w n="12.8">ses</w> <w n="12.9">eaux</w> <w n="12.10">bleues</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Parmi</w> <w n="13.2">ces</w> <w n="13.3">saharas</w> <w n="13.4">récemment</w> <w n="13.5">découverts</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Quand</w> <w n="14.2">l</w>’<w n="14.3">indigène</w> <w n="14.4">ému</w> <w n="14.5">voit</w> <w n="14.6">passer</w> <w n="14.7">dans</w> <w n="14.8">nos</w> <w n="14.9">vers</w></l>
						<l n="15" num="1.15"><w n="15.1">Ces</w> <w n="15.2">mots</w> <w n="15.3">déjà</w> <w n="15.4">caducs</w> : <hi rend="ital"><w n="15.5">rat</w>, <w n="15.6">grisette</w> </hi><w n="15.7">ou</w> <hi rend="ital"><w n="15.8">lorette</w>,</hi></l>
						<l n="16" num="1.16"><w n="16.1">Il</w> <w n="16.2">se</w> <w n="16.3">sent</w> <w n="16.4">vivre</w>, <w n="16.5">un</w> <w n="16.6">charme</w> <w n="16.7">impérieux</w> <w n="16.8">l</w>’<w n="16.9">arrête</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Et</w>, <w n="17.2">l</w>’<w n="17.3">œil</w> <w n="17.4">dans</w> <w n="17.5">le</w> <w n="17.6">ciel</w> <w n="17.7">bleu</w>, <w n="17.8">ce</w> <w n="17.9">naturel</w> <w n="17.10">naïf</w></l>
						<l n="18" num="1.18"><w n="18.1">Évacue</w> <w n="18.2">un</w> <w n="18.3">sonnet</w> <w n="18.4">imité</w> <w n="18.5">de</w> <w n="18.6">Baïf</w>.</l>
						<l n="19" num="1.19"><w n="19.1">Il</w> <w n="19.2">voit</w> <w n="19.3">dans</w> <w n="19.4">le</w> <w n="19.5">verger</w> <w n="19.6">qu</w>’<w n="19.7">il</w> <w n="19.8">eut</w> <w n="19.9">en</w> <w n="19.10">patrimoine</w></l>
						<l n="20" num="1.20"><w n="20.1">Tourbillonner</w> <w n="20.2">en</w> <w n="20.3">chœur</w> <w n="20.4">les</w> <w n="20.5">cauchemars</w> <w n="20.6">d</w>’<w n="20.7">Antoine</w> ;</l>
						<l n="21" num="1.21"><w n="21.1">Le</w> <w n="21.2">voilà</w> <w n="21.3">frémissant</w> <w n="21.4">et</w> <w n="21.5">rouge</w> <w n="21.6">comme</w> <w n="21.7">un</w> <w n="21.8">coq</w> ;</l>
						<l n="22" num="1.22"><w n="22.1">Il</w> <w n="22.2">rêve</w>, <w n="22.3">il</w> <w n="22.4">doute</w>, <w n="22.5">il</w> <w n="22.6">songe</w>, <w n="22.7">et</w> <w n="22.8">tout</w> <w n="22.9">son</w> <w n="22.10">Paul</w> <w n="22.11">de</w> <w n="22.12">Kock</w></l>
						<l n="23" num="1.23"><w n="23.1">Lui</w> <w n="23.2">revient</w> <w n="23.3">en</w> <w n="23.4">mémoire</w>, <w n="23.5">et</w>, <w n="23.6">pendant</w> <w n="23.7">trois</w> <w n="23.8">semaines</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Fait</w> <w n="24.2">partir</w> <w n="24.3">à</w> <w n="24.4">ses</w> <w n="24.5">yeux</w> <w n="24.6">des</w> <w n="24.7">chandelles</w> <w n="24.8">romaines</w></l>
						<l n="25" num="1.25"><w n="25.1">Et</w> <w n="25.2">dans</w> <w n="25.3">son</w> <w n="25.4">cœur</w> <w n="25.5">troublé</w> <w n="25.6">met</w> <w n="25.7">tout</w> <w n="25.8">en</w> <w n="25.9">désarroi</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Comme</w> <w n="26.2">un</w> <w n="26.3">feu</w> <w n="26.4">d</w>’<w n="26.5">artifice</w> <w n="26.6">à</w> <w n="26.7">la</w> <w n="26.8">fête</w> <w n="26.9">du</w> <w n="26.10">roi</w>.</l>
						<l n="27" num="1.27"><space quantity="2" unit="char"></space><w n="27.1">La</w> <w n="27.2">grisette</w> ! <w n="27.3">Il</w> <w n="27.4">revoit</w> <w n="27.5">la</w> <w n="27.6">petite</w> <w n="27.7">fenêtre</w>.</l>
						<l n="28" num="1.28"><w n="28.1">Les</w> <w n="28.2">rayons</w> <w n="28.3">souriants</w> <w n="28.4">du</w> <w n="28.5">jour</w> <w n="28.6">qui</w> <w n="28.7">vient</w> <w n="28.8">de</w> <w n="28.9">naître</w>,</l>
						<l n="29" num="1.29"><w n="29.1">À</w> <w n="29.2">leur</w> <w n="29.3">premier</w> <w n="29.4">réveil</w>, <w n="29.5">comme</w> <w n="29.6">un</w> <w n="29.7">cadre</w> <w n="29.8">enchanteur</w>,</l>
						<l n="30" num="1.30"><w n="30.1">Dorent</w> <w n="30.2">les</w> <w n="30.3">liserons</w> <w n="30.4">et</w> <w n="30.5">les</w> <w n="30.6">pois</w> <w n="30.7">de</w> <w n="30.8">senteur</w>.</l>
						<l n="31" num="1.31"><w n="31.1">Une</w> <w n="31.2">tête</w> <w n="31.3">charmante</w>, <w n="31.4">un</w> <w n="31.5">ange</w>, <w n="31.6">une</w> <w n="31.7">vignette</w></l>
						<l n="32" num="1.32"><w n="32.1">De</w> <w n="32.2">ce</w> <w n="32.3">gai</w> <w n="32.4">reposoir</w> <w n="32.5">agace</w> <w n="32.6">la</w> <w n="32.7">lorgnette</w>.</l>
						<l n="33" num="1.33"><w n="33.1">En</w> <w n="33.2">voyant</w> <w n="33.3">de</w> <w n="33.4">la</w> <w n="33.5">rue</w> <w n="33.6">un</w> <w n="33.7">rire</w> <w n="33.8">triomphant</w></l>
						<l n="34" num="1.34"><w n="34.1">Ouvrir</w> <w n="34.2">des</w> <w n="34.3">dents</w> <w n="34.4">de</w> <w n="34.5">perle</w>, <w n="34.6">on</w> <w n="34.7">dirait</w> <w n="34.8">qu</w>’<w n="34.9">un</w> <w n="34.10">enfant</w></l>
						<l n="35" num="1.35"><w n="35.1">Ou</w> <w n="35.2">quelque</w> <w n="35.3">sylphe</w>, <w n="35.4">épris</w> <w n="35.5">de</w> <w n="35.6">leurs</w> <w n="35.7">touffes</w> <w n="35.8">écloses</w>,</l>
						<l n="36" num="1.36"><w n="36.1">A</w> <w n="36.2">fait</w> <w n="36.3">choir</w>, <w n="36.4">en</w> <w n="36.5">jouant</w>, <w n="36.6">du</w> <w n="36.7">lait</w> <w n="36.8">parmi</w> <w n="36.9">les</w> <w n="36.10">roses</w>.</l>
						<l n="37" num="1.37"><space quantity="2" unit="char"></space><w n="37.1">Elle</w> <w n="37.2">va</w> <w n="37.3">se</w> <w n="37.4">lacer</w> <w n="37.5">en</w> <w n="37.6">chantant</w> <w n="37.7">sa</w> <w n="37.8">chanson</w>,</l>
						<l n="38" num="1.38"><hi rend="ital"><w n="38.1">Lisette</w> </hi><w n="38.2">ou</w> <hi rend="ital"><w n="38.3">l</w>’<w n="38.4">andalouse</w> </hi><w n="38.5">ou</w> <w n="38.6">bien</w> <hi rend="ital"><w n="38.7">mimi</w> <w n="38.8">pinson</w>, </hi></l>
						<l n="39" num="1.39"><w n="39.1">Puis</w> <w n="39.2">tendre</w> <w n="39.3">son</w> <w n="39.4">bas</w> <w n="39.5">blanc</w> <w n="39.6">sur</w> <w n="39.7">sa</w> <w n="39.8">jambe</w> <w n="39.9">plus</w> <w n="39.10">blanche</w> ;</l>
						<l n="40" num="1.40"><w n="40.1">Les</w> <w n="40.2">plis</w> <w n="40.3">du</w> <w n="40.4">frais</w> <w n="40.5">jupon</w> <w n="40.6">vont</w> <w n="40.7">embrasser</w> <w n="40.8">sa</w> <w n="40.9">hanche</w></l>
						<l n="41" num="1.41"><w n="41.1">Et</w> <w n="41.2">cacher</w> <w n="41.3">cent</w> <w n="41.4">trésors</w>, <w n="41.5">et</w> <w n="41.6">du</w> <w n="41.7">cachot</w> <w n="41.8">de</w> <w n="41.9">grès</w></l>
						<l n="42" num="1.42"><w n="42.1">La</w> <w n="42.2">naïade</w> <w n="42.3">aux</w> <w n="42.4">yeux</w> <w n="42.5">bleus</w> <w n="42.6">glissera</w> <w n="42.7">sans</w> <w n="42.8">regrets</w></l>
						<l n="43" num="1.43"><w n="43.1">Sur</w> <w n="43.2">sa</w> <w n="43.3">folle</w> <w n="43.4">poitrine</w> <w n="43.5">et</w> <w n="43.6">sur</w> <w n="43.7">son</w> <w n="43.8">col</w>, <w n="43.9">que</w> <w n="43.10">baigne</w></l>
						<l n="44" num="1.44"><w n="44.1">Un</w> <w n="44.2">doux</w> <w n="44.3">or</w> <w n="44.4">délivré</w> <w n="44.5">des</w> <w n="44.6">morsures</w> <w n="44.7">du</w> <w n="44.8">peigne</w>.</l>
						<l n="45" num="1.45"><w n="45.1">Ce</w> <w n="45.2">poëme</w> <w n="45.3">fini</w>, <w n="45.4">dans</w> <w n="45.5">un</w> <w n="45.6">grossier</w> <w n="45.7">réseau</w></l>
						<l n="46" num="1.46"><w n="46.1">Elle</w> <w n="46.2">va</w> <w n="46.3">becqueter</w> <w n="46.4">son</w> <w n="46.5">déjeuner</w> <w n="46.6">d</w>’<w n="46.7">oiseau</w>,</l>
						<l n="47" num="1.47"><w n="47.1">Puis</w>, <w n="47.2">son</w> <w n="47.3">ouvrage</w> <w n="47.4">en</w> <w n="47.5">main</w>, <w n="47.6">sur</w> <w n="47.7">sa</w> <w n="47.8">chaise</w> <w n="47.9">de</w> <w n="47.10">paille</w>,</l>
						<l n="48" num="1.48"><w n="48.1">La</w> <w n="48.2">folle</w> <w n="48.3">va</w> <w n="48.4">laisser</w>, <w n="48.5">tandis</w> <w n="48.6">qu</w>’<w n="48.7">elle</w> <w n="48.8">travaille</w>,</l>
						<l n="49" num="1.49"><w n="49.1">L</w>’<w n="49.2">aiguille</w> <w n="49.3">aux</w> <w n="49.4">dents</w> <w n="49.5">d</w>’<w n="49.6">acier</w> <w n="49.7">mordre</w> <w n="49.8">ses</w> <w n="49.9">petits</w> <w n="49.10">doigts</w> ;</l>
						<l n="50" num="1.50"><w n="50.1">Et</w>, <w n="50.2">comme</w> <w n="50.3">un</w> <w n="50.4">frais</w> <w n="50.5">méandre</w> <w n="50.6">égaré</w> <w n="50.7">dans</w> <w n="50.8">les</w> <w n="50.9">bois</w>,</l>
						<l n="51" num="1.51"><w n="51.1">Elle</w> <w n="51.2">entrelacera</w>, <w n="51.3">modeste</w> <w n="51.4">poésie</w>,</l>
						<l n="52" num="1.52"><w n="52.1">Les</w> <w n="52.2">fleurs</w> <w n="52.3">de</w> <w n="52.4">son</w> <w n="52.5">caprice</w> <w n="52.6">et</w> <w n="52.7">de</w> <w n="52.8">sa</w> <w n="52.9">fantaisie</w>.</l>
						<l n="53" num="1.53"><space quantity="2" unit="char"></space><w n="53.1">C</w>’<w n="53.2">est</w> <w n="53.3">ce</w> <w n="53.4">que</w> <w n="53.5">l</w>’<w n="53.6">on</w> <w n="53.7">appelle</w> <w n="53.8">une</w> <w n="53.9">brodeuse</w>. <w n="53.10">Hélas</w> !</l>
						<l n="54" num="1.54"><w n="54.1">Depuis</w> <w n="54.2">qu</w>’<w n="54.3">en</w> <w n="54.4">retournant</w> <w n="54.5">le</w> <w n="54.6">sept</w> <w n="54.7">de</w> <w n="54.8">cœur</w> <w n="54.9">ou</w> <w n="54.10">l</w>’<w n="54.11">as</w></l>
						<l n="55" num="1.55"><w n="55.1">Dans</w> <w n="55.2">un</w> <w n="55.3">estaminet</w>, <w n="55.4">le</w> <w n="55.5">premier</w> <w n="55.6">journaliste</w></l>
						<l n="56" num="1.56"><w n="56.1">Contre</w> <w n="56.2">les</w> <w n="56.3">murs</w> <w n="56.4">du</w> <w n="56.5">beau</w> <w n="56.6">dressa</w> <w n="56.7">cette</w> <w n="56.8">baliste</w>,</l>
						<l n="57" num="1.57"><w n="57.1">Combien</w> <w n="57.2">ces</w> <w n="57.3">frais</w> <w n="57.4">croquis</w>, <w n="57.5">plus</w> <w n="57.6">faux</w> <w n="57.7">que</w> <w n="57.8">des</w> <w n="57.9">jetons</w>,</l>
						<l n="58" num="1.58"><w n="58.1">Ont</w> <w n="58.2">fait</w> <w n="58.3">dans</w> <w n="58.4">notre</w> <w n="58.5">ciel</w> <w n="58.6">errer</w> <w n="58.7">de</w> <w n="58.8">Phaétons</w> !</l>
						<l n="59" num="1.59"><w n="59.1">La</w> <w n="59.2">grisette</w>, <w n="59.3">doux</w> <w n="59.4">rêve</w> ! <w n="59.5">Elle</w> <w n="59.6">avait</w> <w n="59.7">ses</w> <w n="59.8">apôtres</w>,</l>
						<l n="60" num="1.60"><w n="60.1">Balzac</w> <w n="60.2">et</w> <w n="60.3">Gavarni</w> <w n="60.4">mentaient</w> <w n="60.5">comme</w> <w n="60.6">les</w> <w n="60.7">autres</w> ;</l>
						<l n="61" num="1.61"><w n="61.1">Mais</w> <w n="61.2">un</w> <w n="61.3">jour</w>, <w n="61.4">Roqueplan</w>, <w n="61.5">s</w>’<w n="61.6">étant</w> <w n="61.7">mis</w> <w n="61.8">à</w> <w n="61.9">l</w>’<w n="61.10">affût</w>,</l>
						<l n="62" num="1.62"><w n="62.1">Fit</w> <w n="62.2">un</w> <w n="62.3">mot</w> <w n="62.4">de</w> <w n="62.5">génie</w>, <w n="62.6">et</w> <w n="62.7">la</w> <hi rend="ital"><w n="62.8">lorette</w> </hi><w n="62.9">fut</w> !</l>
						<l n="63" num="1.63"><space quantity="2" unit="char"></space><w n="63.1">Hurrah</w> ! <w n="63.2">Les</w> <w n="63.3">Aglaé</w> ! <w n="63.4">Les</w> <w n="63.5">Ida</w>, <w n="63.6">les</w> <w n="63.7">charmantes</w>,</l>
						<l n="64" num="1.64"><w n="64.1">En</w> <w n="64.2">avant</w> ! <w n="64.3">Le</w> <w n="64.4">champagne</w> <w n="64.5">a</w> <w n="64.6">baptisé</w> <w n="64.7">les</w> <w n="64.8">mantes</w> !</l>
						<l n="65" num="1.65"><w n="65.1">Déchirons</w> <w n="65.2">nos</w> <w n="65.3">gants</w> <w n="65.4">blancs</w> <w n="65.5">au</w> <w n="65.6">seuil</w> <w n="65.7">de</w> <w n="65.8">l</w>’<w n="65.9">opéra</w> !</l>
						<l n="66" num="1.66"><w n="66.1">Après</w>, <w n="66.2">la</w> <w n="66.3">maison</w>-<w n="66.4">d</w>’<w n="66.5">or</w> ! <w n="66.6">Corinne</w> <w n="66.7">chantera</w>,</l>
						<l n="67" num="1.67"><w n="67.1">Et</w> <w n="67.2">puis</w>, <w n="67.3">nous</w> <w n="67.4">ferons</w> <w n="67.5">tous</w>, <w n="67.6">comme</w> <w n="67.7">c</w>’<w n="67.8">est</w> <w n="67.9">nécessaire</w>,</l>
						<l n="68" num="1.68"><w n="68.1">Des</w> <w n="68.2">mots</w> <w n="68.3">qui</w> <w n="68.4">paraîtront</w> <w n="68.5">demain</w> <w n="68.6">dans</w> <hi rend="ital"><w n="68.7">le</w> <w n="68.8">corsaire</w> ! </hi></l>
						<l n="69" num="1.69"><w n="69.1">Des</w> <w n="69.2">mots</w> <w n="69.3">tout</w> <w n="69.4">neufs</w>, <w n="69.5">si</w> <w n="69.6">bien</w> <w n="69.7">arrachés</w> <w n="69.8">au</w> <w n="69.9">trépas</w></l>
						<l n="70" num="1.70"><w n="70.1">Qu</w>’<w n="70.2">ils</w> <w n="70.3">se</w> <w n="70.4">rendent</w> <w n="70.5">parfois</w>, <w n="70.6">mais</w> <w n="70.7">qu</w>’<w n="70.8">ils</w> <w n="70.9">ne</w> <w n="70.10">meurent</w> <w n="70.11">pas</w> !</l>
						<l n="71" num="1.71"><w n="71.1">Écoutez</w> <w n="71.2">Célina</w>, <w n="71.3">reine</w> <w n="71.4">de</w> <w n="71.5">la</w> <w n="71.6">folie</w>,</l>
						<l n="72" num="1.72"><w n="72.1">Qui</w> <w n="72.2">chante</w> : <hi rend="ital"><w n="72.3">un</w> <w n="72.4">général</w> <w n="72.5">de</w> <w n="72.6">l</w>’<w n="72.7">armée</w> <w n="72.8">d</w>’<w n="72.9">Italie</w> ! </hi></l>
						<l n="73" num="1.73"><w n="73.1">Ah</w> ! <w n="73.2">Bravo</w> ! <w n="73.3">C</w>’<w n="73.4">est</w> <w n="73.5">épique</w>, <w n="73.6">on</w> <w n="73.7">ne</w> <w n="73.8">peut</w> <w n="73.9">le</w> <w n="73.10">nier</w>.</l>
						<l n="74" num="1.74"><w n="74.1">Quel</w> <w n="74.2">aplomb</w> ! <w n="74.3">Je</w> <w n="74.4">l</w>’<w n="74.5">avais</w> <w n="74.6">entendu</w> <w n="74.7">l</w>’<w n="74.8">an</w> <w n="74.9">dernier</w>.</l>
						<l n="75" num="1.75"><w n="75.1">Vive</w> <w n="75.2">Aspasie</w> ! <w n="75.3">Athène</w> <w n="75.4">existe</w> <w n="75.5">au</w> <w n="75.6">sein</w> <w n="75.7">des</w> <w n="75.8">gaules</w> !</l>
						<l n="76" num="1.76"><w n="76.1">Ah</w> ! <w n="76.2">Nous</w> <w n="76.3">avons</w> <w n="76.4">vraiment</w> <w n="76.5">les</w> <w n="76.6">femmes</w> <w n="76.7">les</w> <w n="76.8">plus</w> <w n="76.9">drôles</w></l>
						<l n="77" num="1.77"><w n="77.1">De</w> <w n="77.2">Paris</w> ! <w n="77.3">Périclès</w> <w n="77.4">vit</w> <w n="77.5">chez</w> <w n="77.6">nous</w> <w n="77.7">en</w> <w n="77.8">exil</w>,</l>
						<l n="78" num="1.78"><w n="78.1">Et</w> <w n="78.2">nous</w> <w n="78.3">nous</w> <w n="78.4">amusons</w> <w n="78.5">beaucoup</w>. <w n="78.6">Quelle</w> <w n="78.7">heure</w> <w n="78.8">est</w>-<w n="78.9">il</w> ?</l>
						<l n="79" num="1.79"><space quantity="2" unit="char"></space><w n="79.1">Évohé</w> ! <w n="79.2">Toi</w> <w n="79.3">qui</w> <w n="79.4">sais</w> <w n="79.5">le</w> <w n="79.6">fond</w> <w n="79.7">de</w> <w n="79.8">ces</w> <w n="79.9">arcanes</w>,</l>
						<l n="80" num="1.80"><w n="80.1">Depuis</w> <w n="80.2">la</w> <w n="80.3">maison</w>-<w n="80.4">d</w>’<w n="80.5">or</w> <w n="80.6">jusqu</w>’<w n="80.7">au</w> <w n="80.8">bureau</w> <w n="80.9">des</w> <w n="80.10">cannes</w>,</l>
						<l n="81" num="1.81"><w n="81.1">Toi</w> <w n="81.2">qui</w> <w n="81.3">portas</w> <w n="81.4">naguère</w> <w n="81.5">avec</w> <w n="81.6">assez</w> <w n="81.7">d</w>’<w n="81.8">ardeur</w></l>
						<l n="82" num="1.82"><w n="82.1">Le</w> <w n="82.2">claque</w> <w n="82.3">enrubanné</w> <w n="82.4">du</w> <w n="82.5">fameux</w> <w n="82.6">débardeur</w>,</l>
						<l n="83" num="1.83"><w n="83.1">Apparais</w> ! <w n="83.2">Montre</w>-<w n="83.3">nous</w>, <w n="83.4">ô</w> <w n="83.5">femme</w> <w n="83.6">sibylline</w>,</l>
						<l n="84" num="1.84"><w n="84.1">La</w> <w n="84.2">pâle</w> <w n="84.3">vérité</w> <w n="84.4">nue</w> <w n="84.5">et</w> <w n="84.6">sans</w> <w n="84.7">crinoline</w>,</l>
						<l n="85" num="1.85"><w n="85.1">Et</w> <w n="85.2">convaincs</w> <w n="85.3">une</w> <w n="85.4">fois</w> <w n="85.5">les</w> <w n="85.6">faiseurs</w> <w n="85.7">de</w> <w n="85.8">journaux</w></l>
						<l n="86" num="1.86"><w n="86.1">De</w> <w n="86.2">complicité</w> <w n="86.3">vile</w> <w n="86.4">avec</w> <w n="86.5">les</w> <w n="86.6">Oudinots</w>.</l>
						<l n="87" num="1.87"><space quantity="2" unit="char"></space><w n="87.1">Descends</w> <w n="87.2">jusques</w> <w n="87.3">au</w> <w n="87.4">fond</w> <w n="87.5">de</w> <w n="87.6">ces</w> <w n="87.7">hontes</w> <w n="87.8">immenses</w></l>
						<l n="88" num="1.88"><w n="88.1">Qui</w> <w n="88.2">sont</w> <w n="88.3">le</w> <w n="88.4">paradis</w> <w n="88.5">des</w> <w n="88.6">auteurs</w> <w n="88.7">de</w> <w n="88.8">romances</w>,</l>
						<l n="89" num="1.89"><w n="89.1">Dis</w>-<w n="89.2">nous</w> <w n="89.3">tous</w> <w n="89.4">les</w> <w n="89.5">détours</w> <w n="89.6">de</w> <w n="89.7">ces</w> <w n="89.8">gouffres</w> <w n="89.9">amers</w>,</l>
						<l n="90" num="1.90"><w n="90.1">Et</w> <w n="90.2">si</w> <w n="90.3">la</w> <w n="90.4">perle</w> <w n="90.5">en</w> <w n="90.6">feu</w> <w n="90.7">rayonne</w> <w n="90.8">au</w> <w n="90.9">fond</w> <w n="90.10">des</w> <w n="90.11">mers</w>,</l>
						<l n="91" num="1.91"><w n="91.1">Et</w> <w n="91.2">quels</w> <w n="91.3">monstres</w>, <w n="91.4">avec</w> <w n="91.5">leurs</w> <w n="91.6">cent</w> <w n="91.7">gueules</w> <w n="91.8">ouvertes</w>,</l>
						<l n="92" num="1.92"><w n="92.1">Attendent</w> <w n="92.2">le</w> <w n="92.3">nageur</w> <w n="92.4">tombé</w> <w n="92.5">dans</w> <w n="92.6">les</w> <w n="92.7">eaux</w> <w n="92.8">vertes</w>.</l>
						<l n="93" num="1.93"><w n="93.1">Mène</w>-<w n="93.2">nous</w> <w n="93.3">par</w> <w n="93.4">la</w> <w n="93.5">main</w> <w n="93.6">au</w> <w n="93.7">fond</w> <w n="93.8">de</w> <w n="93.9">ces</w> <w n="93.10">tombeaux</w> !</l>
						<l n="94" num="1.94"><w n="94.1">Montre</w> <w n="94.2">ces</w> <w n="94.3">jeunes</w> <w n="94.4">corps</w> <w n="94.5">si</w> <w n="94.6">pâles</w> <w n="94.7">et</w> <w n="94.8">si</w> <w n="94.9">beaux</w></l>
						<l n="95" num="1.95"><w n="95.1">D</w>’<w n="95.2">où</w> <w n="95.3">la</w> <w n="95.4">beauté</w> <w n="95.5">s</w>’<w n="95.6">enfuit</w> <w n="95.7">sans</w> <w n="95.8">y</w> <w n="95.9">laisser</w> <w n="95.10">de</w> <w n="95.11">trace</w> !</l>
						<l n="96" num="1.96"><w n="96.1">Fais</w>-<w n="96.2">nous</w> <w n="96.3">voir</w> <w n="96.4">la</w> <w n="96.5">misère</w> <w n="96.6">et</w> <w n="96.7">l</w>’<w n="96.8">impudeur</w> <w n="96.9">sans</w> <w n="96.10">grâce</w> !</l>
						<l n="97" num="1.97"><w n="97.1">Parcours</w>, <w n="97.2">en</w> <w n="97.3">exhalant</w> <w n="97.4">tes</w> <w n="97.5">regrets</w> <w n="97.6">superflus</w>,</l>
						<l n="98" num="1.98"><w n="98.1">Ces</w> <w n="98.2">beaux</w> <w n="98.3">temples</w> <w n="98.4">de</w> <w n="98.5">l</w>’<w n="98.6">âme</w> <w n="98.7">où</w> <w n="98.8">le</w> <w n="98.9">dieu</w> <w n="98.10">ne</w> <w n="98.11">vit</w> <w n="98.12">plus</w>,</l>
						<l n="99" num="1.99"><w n="99.1">Sans</w> <w n="99.2">craindre</w> <w n="99.3">d</w>’<w n="99.4">y</w> <w n="99.5">salir</w> <w n="99.6">ta</w> <w n="99.7">cheville</w> <w n="99.8">nacrée</w>.</l>
						<l n="100" num="1.100"><w n="100.1">Tu</w> <w n="100.2">peux</w> <w n="100.3">entrer</w> <w n="100.4">partout</w>, <w n="100.5">car</w> <w n="100.6">la</w> <w n="100.7">muse</w> <w n="100.8">est</w> <w n="100.9">sacrée</w>.</l>
						<l n="101" num="1.101"><space quantity="2" unit="char"></space><w n="101.1">Mais</w> <w n="101.2">du</w> <w n="101.3">moins</w>, <w n="101.4">Évohé</w>, <w n="101.5">si</w> <w n="101.6">la</w> <w n="101.7">jeune</w> <w n="101.8">Laïs</w>,</l>
						<l n="102" num="1.102"><w n="102.1">Avec</w> <w n="102.2">ses</w> <w n="102.3">cheveux</w> <w n="102.4">d</w>’<w n="102.5">or</w>, <w n="102.6">blonds</w> <w n="102.7">comme</w> <w n="102.8">le</w> <w n="102.9">maïs</w>,</l>
						<l n="103" num="1.103"><w n="103.1">N</w>’<w n="103.2">enchaîne</w> <w n="103.3">déjà</w> <w n="103.4">plus</w> <w n="103.5">son</w> <w n="103.6">amant</w> <w n="103.7">Diogène</w> ;</l>
						<l n="104" num="1.104"><w n="104.1">Dans</w> <w n="104.2">ces</w> <w n="104.3">murs</w>, <w n="104.4">d</w>’<w n="104.5">où</w> <w n="104.6">s</w>’<w n="104.7">enfuit</w> <w n="104.8">l</w>’<w n="104.9">esprit</w> <w n="104.10">avec</w> <w n="104.11">la</w> <w n="104.12">gêne</w>,</l>
						<l n="105" num="1.105"><w n="105.1">Si</w> <w n="105.2">leur</w> <w n="105.3">Alcibiade</w> <w n="105.4">et</w> <w n="105.5">leur</w> <w n="105.6">sage</w> <w n="105.7">Phryné</w></l>
						<l n="106" num="1.106"><w n="106.1">Abandonnent</w> <w n="106.2">déjà</w> <w n="106.3">ce</w> <w n="106.4">siècle</w> <w n="106.5">nouveau</w>-<w n="106.6">né</w>,</l>
						<l n="107" num="1.107"><w n="107.1">Si</w> <w n="107.2">dans</w> <w n="107.3">notre</w> <w n="107.4">Paris</w> <w n="107.5">leur</w> <w n="107.6">Athène</w> <w n="107.7">est</w> <w n="107.8">bien</w> <w n="107.9">morte</w>,</l>
						<l n="108" num="1.108"><w n="108.1">Dans</w> <w n="108.2">les</w> <w n="108.3">salons</w> <w n="108.4">dorés</w> <w n="108.5">où</w> <w n="108.6">se</w> <w n="108.7">tient</w> <w n="108.8">à</w> <w n="108.9">la</w> <w n="108.10">porte</w></l>
						<l n="109" num="1.109"><w n="109.1">La</w> <w n="109.2">noble</w> <w n="109.3">courtoisie</w>, <w n="109.4">il</w> <w n="109.5">est</w> <w n="109.6">plus</w> <w n="109.7">d</w>’<w n="109.8">un</w> <w n="109.9">grand</w> <w n="109.10">nom</w></l>
						<l n="110" num="1.110"><w n="110.1">Qui</w> <w n="110.2">dérobe</w> <w n="110.3">la</w> <w n="110.4">grâce</w> <w n="110.5">et</w> <w n="110.6">l</w>’<w n="110.7">esprit</w> <w n="110.8">de</w> <w n="110.9">Ninon</w>.</l>
						<l n="111" num="1.111"><w n="111.1">Là</w>, <w n="111.2">l</w>’<w n="111.3">amour</w> <w n="111.4">est</w> <w n="111.5">un</w> <w n="111.6">art</w> <w n="111.7">comme</w> <w n="111.8">la</w> <w n="111.9">poésie</w> :</l>
						<l n="112" num="1.112"><w n="112.1">Le</w> <w n="112.2">caprice</w> <w n="112.3">aux</w> <w n="112.4">yeux</w> <w n="112.5">verts</w>, <w n="112.6">la</w> <w n="112.7">rose</w> <w n="112.8">fantaisie</w></l>
						<l n="113" num="1.113"><w n="113.1">Poussent</w> <w n="113.2">la</w> <w n="113.3">blanche</w> <w n="113.4">nef</w> <w n="113.5">que</w> <w n="113.6">guident</w> <w n="113.7">sur</w> <w n="113.8">son</w> <w n="113.9">lac</w></l>
						<l n="114" num="1.114"><w n="114.1">Anacréon</w>, <w n="114.2">Ovide</w> <w n="114.3">et</w> <w n="114.4">le</w> <w n="114.5">divin</w> <w n="114.6">Balzac</w>,</l>
						<l n="115" num="1.115"><w n="115.1">Et</w> <w n="115.2">mènent</w> <w n="115.3">sur</w> <w n="115.4">ces</w> <w n="115.5">flots</w>, <w n="115.6">célébrés</w> <w n="115.7">par</w> <w n="115.8">Horace</w>,</l>
						<l n="116" num="1.116"><w n="116.1">La</w> <w n="116.2">volupté</w> <w n="116.3">plus</w> <w n="116.4">belle</w> <w n="116.5">encore</w> <w n="116.6">que</w> <w n="116.7">la</w> <w n="116.8">grâce</w> !</l>
						<l n="117" num="1.117"><space quantity="2" unit="char"></space><w n="117.1">Ô</w> <w n="117.2">doux</w> <w n="117.3">mensonge</w> ! <w n="117.4">Avec</w> <w n="117.5">tes</w> <w n="117.6">ongles</w> <w n="117.7">déjà</w> <w n="117.8">longs</w>,</l>
						<l n="118" num="1.118"><w n="118.1">Tâche</w> <w n="118.2">d</w>’<w n="118.3">égratigner</w> <w n="118.4">la</w> <w n="118.5">porte</w> <w n="118.6">des</w> <w n="118.7">salons</w>,</l>
						<l n="119" num="1.119"><w n="119.1">Et</w> <w n="119.2">peins</w>-<w n="119.3">nous</w>, <w n="119.4">s</w>’<w n="119.5">il</w> <w n="119.6">se</w> <w n="119.7">peut</w>, <w n="119.8">en</w> <w n="119.9">paroles</w> <w n="119.10">courtoises</w>,</l>
						<l n="120" num="1.120"><w n="120.1">Les</w> <w n="120.2">amours</w> <w n="120.3">de</w> <w n="120.4">duchesse</w> <w n="120.5">et</w> <w n="120.6">les</w> <w n="120.7">amours</w> <w n="120.8">bourgeoises</w> !</l>
						<l n="121" num="1.121"><w n="121.1">Dis</w> <w n="121.2">l</w>’<w n="121.3">enfant</w> <w n="121.4">chérubin</w> <w n="121.5">tenant</w> <w n="121.6">sur</w> <w n="121.7">ses</w> <w n="121.8">genoux</w></l>
						<l n="122" num="1.122"><w n="122.1">Sa</w> <w n="122.2">marraine</w> <w n="122.3">aujourd</w>’<w n="122.4">hui</w> <w n="122.5">moins</w> <w n="122.6">sévère</w> ; <w n="122.7">dis</w>-<w n="122.8">nous</w></l>
						<l n="123" num="1.123"><w n="123.1">La</w> <w n="123.2">nouvelle</w> <w n="123.3">Phryné</w>, <w n="123.4">lascive</w> <w n="123.5">et</w> <w n="123.6">dédaigneuse</w>,</l>
						<l n="124" num="1.124"><w n="124.1">Instruisant</w> <w n="124.2">les</w> <w n="124.3">d</w>’<w n="124.4">Espard</w> <w n="124.5">après</w> <w n="124.6">les</w> <w n="124.7">Maufrigneuse</w> ;</l>
						<l n="125" num="1.125"><w n="125.1">Dis</w>-<w n="125.2">nous</w> <w n="125.3">les</w> <w n="125.4">nobles</w> <w n="125.5">seins</w> <w n="125.6">que</w> <w n="125.7">froissent</w> <w n="125.8">les</w> <w n="125.9">talons</w></l>
						<l n="126" num="1.126"><w n="126.1">Des</w> <w n="126.2">superbes</w> <w n="126.3">chasseurs</w> <w n="126.4">choisis</w> <w n="126.5">pour</w> <w n="126.6">étalons</w> ;</l>
						<l n="127" num="1.127"><w n="127.1">Et</w> <w n="127.2">comment</w> <subst reason="analysis" type="completion" hand="RR"><del>Mess.....</del><add rend="hidden"><w n="127.3">aline</w></add></subst> <w n="127.4">encore</w> <w n="127.5">extasiée</w>,</l>
						<l n="128" num="1.128"><w n="128.1">Au</w> <w n="128.2">matin</w> <w n="128.3">rentre</w> <w n="128.4">lasse</w> <w n="128.5">et</w> <w n="128.6">non</w> <w n="128.7">rassasiée</w>,</l>
						<l n="129" num="1.129"><w n="129.1">Pâle</w>, <w n="129.2">essoufflée</w>, <w n="129.3">en</w> <w n="129.4">eau</w>, <w n="129.5">suivant</w> <w n="129.6">l</w>’<w n="129.7">ombre</w> <w n="129.8">du</w> <w n="129.9">mur</w>,</l>
						<l n="130" num="1.130"><w n="130.1">Tandis</w> <w n="130.2">que</w> <w n="130.3">son</w> <w n="130.4">époux</w>, <w n="130.5">orateur</w> <w n="130.6">déjà</w> <w n="130.7">mûr</w>,</l>
						<l n="131" num="1.131"><w n="131.1">Dans</w> <w n="131.2">son</w> <w n="131.3">boudoir</w> <w n="131.4">de</w> <w n="131.5">pair</w> <w n="131.6">désinfecté</w> <w n="131.7">par</w> <w n="131.8">l</w>’<w n="131.9">ambre</w>,</l>
						<l n="132" num="1.132"><w n="132.1">Interpelle</w> <w n="132.2">un</w> <w n="132.3">miroir</w> <w n="132.4">en</w> <w n="132.5">attendant</w> <w n="132.6">la</w> <w n="132.7">chambre</w> !</l>
						<l n="133" num="1.133"><space quantity="2" unit="char"></space><w n="133.1">Ah</w> ! <w n="133.2">Posons</w> <w n="133.3">nos</w> <w n="133.4">deux</w> <w n="133.5">mains</w> <w n="133.6">sur</w> <w n="133.7">notre</w> <w n="133.8">cœur</w> <w n="133.9">sanglant</w> !</l>
						<l n="134" num="1.134"><w n="134.1">Ce</w> <w n="134.2">n</w>’<w n="134.3">est</w> <w n="134.4">pas</w> <w n="134.5">sans</w> <w n="134.6">gémir</w> <w n="134.7">qu</w>’<w n="134.8">on</w> <w n="134.9">cherche</w>, <w n="134.10">en</w> <w n="134.11">se</w> <w n="134.12">troublant</w>,</l>
						<l n="135" num="1.135"><w n="135.1">Quelle</w> <w n="135.2">plaie</w> <w n="135.3">ouvre</w> <w n="135.4">encor</w>, <w n="135.5">dans</w> <w n="135.6">l</w>’<w n="135.7">éternelle</w> <w n="135.8">Troie</w></l>
						<l n="136" num="1.136"><w n="136.1">L</w>’<w n="136.2">implacable</w> <w n="136.3">Vénus</w> <w n="136.4">attachée</w> <w n="136.5">à</w> <w n="136.6">sa</w> <w n="136.7">proie</w> !</l>
						<l n="137" num="1.137"><w n="137.1">Quand</w> <w n="137.2">il</w> <w n="137.3">parle</w> <w n="137.4">d</w>’<w n="137.5">amour</w> <w n="137.6">sans</w> <w n="137.7">pleurer</w> <w n="137.8">et</w> <w n="137.9">crier</w>,</l>
						<l n="138" num="1.138"><w n="138.1">Le</w> <w n="138.2">plus</w> <w n="138.3">heureux</w> <w n="138.4">de</w> <w n="138.5">nous</w>, <w n="138.6">quel</w> <w n="138.7">que</w> <w n="138.8">soit</w> <w n="138.9">le</w> <w n="138.10">laurier</w></l>
						<l n="139" num="1.139"><w n="139.1">Ou</w> <w n="139.2">le</w> <w n="139.3">myrte</w> <w n="139.4">charmant</w> <w n="139.5">dont</w> <w n="139.6">sa</w> <w n="139.7">tête</w> <w n="139.8">se</w> <w n="139.9">ceigne</w>,</l>
						<l n="140" num="1.140"><w n="140.1">Sent</w> <w n="140.2">grincer</w> <w n="140.3">à</w> <w n="140.4">son</w> <w n="140.5">flanc</w> <w n="140.6">la</w> <w n="140.7">blessure</w> <w n="140.8">qui</w> <w n="140.9">saigne</w>,</l>
						<l n="141" num="1.141"><w n="141.1">Et</w> <w n="141.2">se</w> <w n="141.3">plaindre</w> <w n="141.4">et</w> <w n="141.5">frémir</w> <w n="141.6">avec</w> <w n="141.7">un</w> <w n="141.8">ris</w> <w n="141.9">moqueur</w>,</l>
						<l n="142" num="1.142"><w n="142.1">L</w>’<w n="142.2">ouragan</w> <w n="142.3">du</w> <w n="142.4">passé</w> <w n="142.5">dans</w> <w n="142.6">les</w> <w n="142.7">flots</w> <w n="142.8">de</w> <w n="142.9">son</w> <w n="142.10">cœur</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1846">février 1846</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>