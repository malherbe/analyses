<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN125">
				<head type="main">MONSIEUR COQUARDEAU</head>
				<head type="form">CHANT ROYAL</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Roi</w> <w n="1.2">des</w> <w n="1.3">crétins</w>, <w n="1.4">qu</w>’<w n="1.5">avec</w> <w n="1.6">terreur</w> <w n="1.7">on</w> <w n="1.8">nomme</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Grand</w> <w n="2.2">Coquardeau</w>, <w n="2.3">non</w>, <w n="2.4">tu</w> <w n="2.5">ne</w> <w n="2.6">mourras</w> <w n="2.7">pas</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Lépidoptère</w> <w n="3.2">en</w> <w n="3.3">habit</w> <w n="3.4">de</w> <w n="3.5">prudhomme</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Ta</w> <w n="4.2">majesté</w> <w n="4.3">t</w>’<w n="4.4">affranchit</w> <w n="4.5">du</w> <w n="4.6">trépas</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Car</w> <w n="5.2">tu</w> <w n="5.3">naquis</w> <w n="5.4">aux</w> <w n="5.5">premiers</w> <w n="5.6">jours</w> <w n="5.7">du</w> <w n="5.8">monde</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Avant</w> <w n="6.2">les</w> <w n="6.3">cieux</w> <w n="6.4">et</w> <w n="6.5">les</w> <w n="6.6">terres</w> <w n="6.7">et</w> <w n="6.8">l</w>’<w n="6.9">onde</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Quand</w> <w n="7.2">le</w> <w n="7.3">métal</w> <w n="7.4">entrait</w> <w n="7.5">en</w> <w n="7.6">fusion</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Titan</w>, <w n="8.2">instruit</w> <w n="8.3">par</w> <w n="8.4">une</w> <w n="8.5">vision</w></l>
					<l n="9" num="1.9"><w n="9.1">Que</w> <w n="9.2">son</w> <w n="9.3">travail</w> <w n="9.4">durerait</w> <w n="9.5">la</w> <w n="9.6">semaine</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Fondit</w> <w n="10.2">d</w>’<w n="10.3">abord</w>, <w n="10.4">et</w> <w n="10.5">par</w> <w n="10.6">provision</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">front</w> <w n="11.3">serein</w> <w n="11.4">de</w> <w n="11.5">la</w> <w n="11.6">bêtise</w> <w n="11.7">humaine</w>.</l>
				</lg>
				<lg n="2">
					<l n="12" num="2.1"><w n="12.1">On</w> <w n="12.2">t</w>’<w n="12.3">a</w> <w n="12.4">connu</w> <w n="12.5">dans</w> <w n="12.6">Athène</w> <w n="12.7">et</w> <w n="12.8">dans</w> <w n="12.9">Rome</w> :</l>
					<l n="13" num="2.2"><w n="13.1">Plus</w> <w n="13.2">tard</w> <w n="13.3">Colomb</w> <w n="13.4">t</w>’<w n="13.5">a</w> <w n="13.6">vu</w> <w n="13.7">sous</w> <w n="13.8">les</w> <w n="13.9">pampas</w>.</l>
					<l n="14" num="2.3"><w n="14.1">Mais</w> <w n="14.2">sur</w> <w n="14.3">tes</w> <w n="14.4">yeux</w> <w n="14.5">de</w> <w n="14.6">vautour</w> <w n="14.7">économe</w></l>
					<l n="15" num="2.4"><w n="15.1">Se</w> <w n="15.2">courbait</w> <w n="15.3">l</w>’<w n="15.4">arc</w> <w n="15.5">d</w>’<w n="15.6">un</w> <w n="15.7">sourcil</w> <w n="15.8">plein</w> <w n="15.9">d</w>’<w n="15.10">appas</w>,</l>
					<l n="16" num="2.5"><w n="16.1">Et</w> <w n="16.2">le</w> <w n="16.3">sommet</w> <w n="16.4">de</w> <w n="16.5">ta</w> <w n="16.6">tête</w> <w n="16.7">profonde</w></l>
					<l n="17" num="2.6"><w n="17.1">A</w> <w n="17.2">resplendi</w> <w n="17.3">sous</w> <w n="17.4">la</w> <w n="17.5">crinière</w> <w n="17.6">blonde</w>.</l>
					<l n="18" num="2.7"><w n="18.1">Que</w> <w n="18.2">Gavarni</w> <w n="18.3">tourne</w> <w n="18.4">en</w> <w n="18.5">dérision</w></l>
					<l n="19" num="2.8"><w n="19.1">Tes</w> <w n="19.2">six</w> <w n="19.3">cheveux</w> ! <w n="19.4">Avec</w> <w n="19.5">décision</w></l>
					<l n="20" num="2.9"><w n="20.1">Le</w> <w n="20.2">démêloir</w> <w n="20.3">en</w> <w n="20.4">toupet</w> <w n="20.5">les</w> <w n="20.6">ramène</w> :</l>
					<l n="21" num="2.10"><w n="21.1">Un</w> <w n="21.2">dieu</w> <w n="21.3">scalpa</w>, <w n="21.4">comme</w> <w n="21.5">l</w>’<w n="21.6">occasion</w>,</l>
					<l n="22" num="2.11"><w n="22.1">Le</w> <w n="22.2">front</w> <w n="22.3">serein</w> <w n="22.4">de</w> <w n="22.5">la</w> <w n="22.6">bêtise</w> <w n="22.7">humaine</w>.</l>
				</lg>
				<lg n="3">
					<l n="23" num="3.1"><w n="23.1">Tu</w> <w n="23.2">te</w> <w n="23.3">rêvais</w> <w n="23.4">député</w> <w n="23.5">de</w> <w n="23.6">la</w> <w n="23.7">Somme</w></l>
					<l n="24" num="3.2"><w n="24.1">Dans</w> <w n="24.2">les</w> <w n="24.3">discours</w> <w n="24.4">que</w> <w n="24.5">tu</w> <w n="24.6">développas</w>,</l>
					<l n="25" num="3.3"><w n="25.1">Et</w>, <w n="25.2">beau</w> <w n="25.3">parleur</w> <w n="25.4">grâce</w> <w n="25.5">à</w> <w n="25.6">ton</w> <w n="25.7">majordome</w>,</l>
					<l n="26" num="3.4"><w n="26.1">On</w> <w n="26.2">te</w> <w n="26.3">voit</w> <w n="26.4">fier</w> <w n="26.5">de</w> <w n="26.6">tes</w> <w n="26.7">quatre</w> <w n="26.8">repas</w>.</l>
					<l n="27" num="3.5"><w n="27.1">Lorsqu</w>’<w n="27.2">en</w> <w n="27.3">s</w>’<w n="27.4">ouvrant</w> <w n="27.5">ta</w> <w n="27.6">bouche</w> <w n="27.7">rubiconde</w></l>
					<l n="28" num="3.6"><w n="28.1">Verse</w> <w n="28.2">au</w> <w n="28.3">hasard</w> <w n="28.4">les</w> <w n="28.5">trésors</w> <w n="28.6">de</w> <w n="28.7">Golconde</w>,</l>
					<l n="29" num="3.7"><w n="29.1">On</w> <w n="29.2">cause</w> <w n="29.3">bas</w>, <w n="29.4">à</w> <w n="29.5">ton</w> <w n="29.6">exclusion</w>,</l>
					<l n="30" num="3.8"><w n="30.1">Ou</w> <w n="30.2">chacun</w> <w n="30.3">rêve</w> <w n="30.4">à</w> <w n="30.5">son</w> <w n="30.6">évasion</w>.</l>
					<l n="31" num="3.9"><w n="31.1">Tu</w> <w n="31.2">n</w>’<w n="31.3">as</w> <w n="31.4">jamais</w> <w n="31.5">connu</w> <w n="31.6">ce</w> <w n="31.7">phénomène</w> :</l>
					<l n="32" num="3.10"><w n="32.1">Mais</w> <w n="32.2">l</w>’<w n="32.3">ouvrier</w> <w n="32.4">doubla</w> <w n="32.5">l</w>’<w n="32.6">illusion</w></l>
					<l n="33" num="3.11"><w n="33.1">Le</w> <w n="33.2">front</w> <w n="33.3">serein</w> <w n="33.4">de</w> <w n="33.5">la</w> <w n="33.6">bêtise</w> <w n="33.7">humaine</w>.</l>
				</lg>
				<lg n="4">
					<l n="34" num="4.1"><w n="34.1">Comme</w> <w n="34.2">Pâris</w>, <w n="34.3">tu</w> <w n="34.4">tiens</w> <w n="34.5">toujours</w> <w n="34.6">la</w> <w n="34.7">pomme</w>.</l>
					<l n="35" num="4.2"><w n="35.1">Dans</w> <w n="35.2">ton</w> <w n="35.3">salon</w>, <w n="35.4">meublé</w> <w n="35.5">d</w>’<w n="35.6">un</w> <w n="35.7">fier</w> <w n="35.8">lampas</w>,</l>
					<l n="36" num="4.3"><w n="36.1">On</w> <w n="36.2">boit</w> <w n="36.3">du</w> <w n="36.4">lait</w> <w n="36.5">et</w> <w n="36.6">du</w> <w n="36.7">sirop</w> <w n="36.8">de</w> <w n="36.9">gomme</w>,</l>
					<l n="37" num="4.4"><w n="37.1">Et</w> <w n="37.2">tu</w> <w n="37.3">n</w>’<w n="37.4">y</w> <w n="37.5">peux</w>, <w n="37.6">selon</w> <w n="37.7">toi</w>, <w n="37.8">faire</w> <w n="37.9">un</w> <w n="37.10">pas</w></l>
					<l n="38" num="4.5"><w n="38.1">Sans</w> <w n="38.2">qu</w>’<w n="38.3">à</w> <w n="38.4">ta</w> <w n="38.5">flamme</w> <w n="38.6">une</w> <w n="38.7">flamme</w> <w n="38.8">réponde</w>.</l>
					<l n="39" num="4.6"><w n="39.1">Dans</w> <w n="39.2">tes</w> <w n="39.3">miroirs</w> <w n="39.4">tu</w> <w n="39.5">te</w> <w n="39.6">vois</w> <w n="39.7">en</w> <w n="39.8">Joconde</w>.</l>
					<l n="40" num="4.7"><w n="40.1">Jamais</w> <w n="40.2">pourtant</w>, <w n="40.3">cœur</w> <w n="40.4">plein</w> <w n="40.5">d</w>’<w n="40.6">effusion</w>,</l>
					<l n="41" num="4.8"><w n="41.1">Tu</w> <w n="41.2">n</w>’<w n="41.3">oublias</w> <w n="41.4">ta</w> <w n="41.5">chère</w> <w n="41.6">infusion</w></l>
					<l n="42" num="4.9"><w n="42.1">Pour</w> <w n="42.2">les</w> <w n="42.3">rigueurs</w> <w n="42.4">d</w>’<w n="42.5">Iris</w> <w n="42.6">ou</w> <w n="42.7">de</w> <w n="42.8">Climène</w>.</l>
					<l n="43" num="4.10"><w n="43.1">L</w>’<w n="43.2">espoir</w> <w n="43.3">fleurit</w> <w n="43.4">avec</w> <w n="43.5">profusion</w></l>
					<l n="44" num="4.11"><w n="44.1">Le</w> <w n="44.2">front</w> <w n="44.3">serein</w> <w n="44.4">de</w> <w n="44.5">la</w> <w n="44.6">bêtise</w> <w n="44.7">humaine</w>.</l>
				</lg>
				<lg n="5">
					<l n="45" num="5.1"><w n="45.1">À</w> <w n="45.2">ton</w> <w n="45.3">café</w>, <w n="45.4">tu</w> <w n="45.5">te</w> <w n="45.6">dis</w> <w n="45.7">brave</w> <w n="45.8">comme</w></l>
					<l n="46" num="5.2"><w n="46.1">Un</w> <w n="46.2">Perceval</w>, <w n="46.3">et</w> <w n="46.4">toi</w>-<w n="46.5">même</w> <w n="46.6">écharpas</w></l>
					<l n="47" num="5.3"><w n="47.1">Le</w> <w n="47.2">rude</w> <w n="47.3">Arpin</w> ; <w n="47.4">ta</w> <w n="47.5">chiquenaude</w> <w n="47.6">assomme</w>.</l>
					<l n="48" num="5.4"><w n="48.1">Lorsque</w> <w n="48.2">tu</w> <w n="48.3">vas</w>, <w n="48.4">les</w> <w n="48.5">jambes</w> <w n="48.6">en</w> <w n="48.7">compas</w>,</l>
					<l n="49" num="5.5"><w n="49.1">On</w> <w n="49.2">croirait</w> <w n="49.3">voir</w> <w n="49.4">un</w> <w n="49.5">héros</w> <w n="49.6">de</w> <w n="49.7">la</w> <w n="49.8">fronde</w>,</l>
					<l n="50" num="5.6"><w n="50.1">Ou</w> <w n="50.2">quelque</w> <w n="50.3">preux</w>, <w n="50.4">vainqueur</w> <w n="50.5">de</w> <w n="50.6">Trébizonde</w>.</l>
					<l n="51" num="5.7"><w n="51.1">Mais</w>, <w n="51.2">évitant</w>, <w n="51.3">avec</w> <w n="51.4">précision</w></l>
					<l n="52" num="5.8"><w n="52.1">L</w>’<w n="52.2">éclat</w> <w n="52.3">fatal</w> <w n="52.4">d</w>’<w n="52.5">une</w> <w n="52.6">collision</w>,</l>
					<l n="53" num="5.9"><w n="53.1">Tu</w> <w n="53.2">vis</w> <w n="53.3">dodu</w> <w n="53.4">comme</w> <w n="53.5">un</w> <w n="53.6">chapon</w> <w n="53.7">du</w> <w n="53.8">Maine</w>,</l>
					<l n="54" num="5.10"><w n="54.1">Pour</w> <w n="54.2">sauver</w> <w n="54.3">mieux</w> <w n="54.4">de</w> <w n="54.5">toute</w> <w n="54.6">lésion</w></l>
					<l n="55" num="5.11"><w n="55.1">Le</w> <w n="55.2">front</w> <w n="55.3">serein</w> <w n="55.4">de</w> <w n="55.5">la</w> <w n="55.6">bêtise</w> <w n="55.7">humaine</w>.</l>
				</lg>
				<lg n="6">
					<head type="form">ENVOI</head>
					<l n="56" num="6.1"><w n="56.1">Prince</w> <w n="56.2">des</w> <w n="56.3">sots</w>, <w n="56.4">un</w> <w n="56.5">système</w> <w n="56.6">qu</w>’<w n="56.7">on</w> <w n="56.8">fonde</w>,</l>
					<l n="57" num="6.2"><w n="57.1">À</w> <w n="57.2">son</w> <w n="57.3">aurore</w> <w n="57.4">a</w> <w n="57.5">soif</w> <w n="57.6">de</w> <w n="57.7">ta</w> <w n="57.8">faconde</w>.</l>
					<l n="58" num="6.3"><w n="58.1">Toi</w>, <w n="58.2">tu</w> <w n="58.3">vivais</w> <w n="58.4">dans</w> <w n="58.5">la</w> <w n="58.6">prévision</w></l>
					<l n="59" num="6.4"><w n="59.1">Et</w> <w n="59.2">dans</w> <w n="59.3">l</w>’<w n="59.4">espoir</w> <w n="59.5">de</w> <w n="59.6">cette</w> <w n="59.7">invasion</w> :</l>
					<l n="60" num="6.5"><w n="60.1">Le</w> <w n="60.2">réalisme</w> <w n="60.3">est</w> <w n="60.4">ton</w> <w n="60.5">meilleur</w> <w n="60.6">domaine</w>,</l>
					<l n="61" num="6.6"><w n="61.1">Car</w> <w n="61.2">il</w> <w n="61.3">charma</w> <w n="61.4">dès</w> <w n="61.5">son</w> <w n="61.6">éclosion</w></l>
					<l n="62" num="6.7"><w n="62.1">Le</w> <w n="62.2">front</w> <w n="62.3">serein</w> <w n="62.4">de</w> <w n="62.5">la</w> <w n="62.6">bêtise</w> <w n="62.7">humaine</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1856">novembre 1856</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>