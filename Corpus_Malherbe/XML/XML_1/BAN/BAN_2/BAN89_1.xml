<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">OCCIDENTALES</head><div type="poem" key="BAN89">
					<head type="main">OCCIDENTALE CINQUIÈME</head>
					<head type="sub">LE FLAN DANS L’ODÉON</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Avant</w> <w n="1.2">que</w> <w n="1.3">la</w> <w n="1.4">brise</w> <w n="1.5">adultère</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">fait</w> <w n="2.3">le</w> <w n="2.4">charme</w> <w n="2.5">des</w> <w n="2.6">hivers</w>,</l>
						<l n="3" num="1.3"><w n="3.1">N</w>’<w n="3.2">émaille</w> <w n="3.3">de</w> <w n="3.4">recueils</w> <w n="3.5">de</w> <w n="3.6">vers</w></l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">parapets</w> <w n="4.3">du</w> <w n="4.4">quai</w> <w n="4.5">Voltaire</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Avant</w> <w n="5.2">que</w> <w n="5.3">Chaumier</w> <w n="5.4">Siméon</w></l>
						<l n="6" num="1.6"><w n="6.1">N</w>’<w n="6.2">ait</w> <w n="6.3">publié</w> <w n="6.4">ses</w> <w n="6.5">hexamètres</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Allez</w>, <w n="7.2">allez</w>, <w n="7.3">ô</w> <w n="7.4">gendelettres</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Manger</w> <w n="8.2">du</w> <w n="8.3">flan</w> <w n="8.4">dans</w> <w n="8.5">l</w>’<w n="8.6">odéon</w> !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Des</w> <w n="9.2">journaux</w> <w n="9.3">qui</w> <w n="9.4">mettent</w> <w n="9.5">leur</w> <w n="9.6">liste</w></l>
						<l n="10" num="2.2"><w n="10.1">Dans</w> <w n="10.2">l</w>’<w n="10.3">annuaire</w> <w n="10.4">officiel</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Il</w> <w n="11.2">n</w>’<w n="11.3">en</w> <w n="11.4">est</w> <w n="11.5">pas</w> <w n="11.6">qui</w> <w n="11.7">sous</w> <w n="11.8">le</w> <w n="11.9">ciel</w></l>
						<l n="12" num="2.4"><w n="12.1">Soit</w> <w n="12.2">plus</w> <w n="12.3">mordoré</w> <w n="12.4">que</w> <hi rend="ital"><w n="12.5">l</w>’<w n="12.6">artiste</w>. </hi></l>
						<l n="13" num="2.5"><w n="13.1">Messieurs</w> <w n="13.2">Arthur</w>, <w n="13.3">Jule</w> <w n="13.4">et</w> <w n="13.5">Léon</w></l>
						<l n="14" num="2.6"><w n="14.1">En</w> <w n="14.2">sont</w> <w n="14.3">les</w> <w n="14.4">rédacteurs</w> <w n="14.5">champêtres</w>…</l>
						<l n="15" num="2.7"><w n="15.1">Allez</w>, <w n="15.2">allez</w>, <w n="15.3">ô</w> <w n="15.4">gendelettres</w>,</l>
						<l n="16" num="2.8"><w n="16.1">Manger</w> <w n="16.2">du</w> <w n="16.3">flan</w> <w n="16.4">dans</w> <w n="16.5">l</w>’<w n="16.6">odéon</w> !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Il</w> <w n="17.2">n</w>’<w n="17.3">est</w> <w n="17.4">pas</w> <w n="17.5">de</w> <w n="17.6">revue</w> <w n="17.7">alpestre</w>,</l>
						<l n="18" num="3.2"><w n="18.1">Pas</w> <w n="18.2">de</w> <w n="18.3">recueil</w> <w n="18.4">ni</w> <w n="18.5">de</w> <w n="18.6">journal</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Soit</w> <w n="19.2">chez</w> <w n="19.3">Bertin</w> <w n="19.4">ou</w> <w n="19.5">Jubinal</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Où</w> <w n="20.2">viennent</w>, <w n="20.3">vers</w> <w n="20.4">la</w> <w n="20.5">saint</w>-<w n="20.6">Sylvestre</w>,</l>
						<l n="21" num="3.5"><w n="21.1">Plus</w> <w n="21.2">de</w> <w n="21.3">ces</w> <w n="21.4">chevaliers</w> <w n="21.5">d</w>’<w n="21.6">Éon</w></l>
						<l n="22" num="3.6"><w n="22.1">Moitié</w> <w n="22.2">lorettes</w>, <w n="22.3">moitié</w> <w n="22.4">reîtres</w>…</l>
						<l n="23" num="3.7"><w n="23.1">Allez</w>, <w n="23.2">allez</w>, <w n="23.3">ô</w> <w n="23.4">gendelettres</w>,</l>
						<l n="24" num="3.8"><w n="24.1">Manger</w> <w n="24.2">du</w> <w n="24.3">flan</w> <w n="24.4">dans</w> <w n="24.5">l</w>’<w n="24.6">odéon</w> !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Nulle</w> <w n="25.2">part</w>, <w n="25.3">dans</w> <w n="25.4">le</w> <w n="25.5">ciel</w> <w n="25.6">sans</w> <w n="25.7">brise</w>,</l>
						<l n="26" num="4.2"><w n="26.1">Les</w> <w n="26.2">jeunes</w> <w n="26.3">gens</w> <w n="26.4">au</w> <w n="26.5">cœur</w> <w n="26.6">de</w> <w n="26.7">feu</w></l>
						<l n="27" num="4.3"><w n="27.1">Ne</w> <w n="27.2">regardent</w> <w n="27.3">d</w>’<w n="27.4">un</w> <w n="27.5">œil</w> <w n="27.6">plus</w> <w n="27.7">bleu</w></l>
						<l n="28" num="4.4"><w n="28.1">La</w> <w n="28.2">lune</w> <w n="28.3">changer</w> <w n="28.4">de</w> <w n="28.5">chemise</w>.</l>
						<l n="29" num="4.5"><w n="29.1">Ainsi</w> <w n="29.2">la</w> <w n="29.3">voyait</w> <w n="29.4">Actéon</w></l>
						<l n="30" num="4.6"><w n="30.1">Faire</w> <w n="30.2">la</w> <w n="30.3">planche</w> <w n="30.4">sous</w> <w n="30.5">les</w> <w n="30.6">hêtres</w>…</l>
						<l n="31" num="4.7"><w n="31.1">Allez</w>, <w n="31.2">allez</w>, <w n="31.3">ô</w> <w n="31.4">gendelettres</w>,</l>
						<l n="32" num="4.8"><w n="32.1">Manger</w> <w n="32.2">du</w> <w n="32.3">flan</w> <w n="32.4">dans</w> <w n="32.5">l</w>’<w n="32.6">odéon</w> !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">De</w> <hi rend="ital"><w n="33.2">l</w>’<w n="33.3">artiste</w> </hi><w n="33.4">la</w> <w n="33.5">grande</w> <w n="33.6">actrice</w></l>
						<l n="34" num="5.2"><w n="34.1">Fut</w> <w n="34.2">Asphodèle</w> <w n="34.3">Carabas</w>,</l>
						<l n="35" num="5.3"><w n="35.1">Carabas</w>, <w n="35.2">qu</w>’<w n="35.3">avec</w> <w n="35.4">son</w> <w n="35.5">cabas</w></l>
						<l n="36" num="5.4"><w n="36.1">Buloz</w> <w n="36.2">guignait</w> <w n="36.3">pour</w> <w n="36.4">rédactrice</w>.</l>
						<l n="37" num="5.5"><w n="37.1">Hélas</w> ! <w n="37.2">Changeant</w> <w n="37.3">caméléon</w>,</l>
						<l n="38" num="5.6"><hi rend="ital"><w n="38.1">l</w>’<w n="38.2">artiste</w> </hi><w n="38.3">lui</w> <w n="38.4">tourne</w> <w n="38.5">les</w> <w n="38.6">guêtres</w>…</l>
						<l n="39" num="5.7"><w n="39.1">Allez</w>, <w n="39.2">allez</w>, <w n="39.3">ô</w> <w n="39.4">gendelettres</w>,</l>
						<l n="40" num="5.8"><w n="40.1">Manger</w> <w n="40.2">du</w> <w n="40.3">flan</w> <w n="40.4">dans</w> <w n="40.5">l</w>’<w n="40.6">odéon</w> !</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">Un</w> <w n="41.2">étranger</w> <w n="41.3">vint</w> <w n="41.4">à</w> <hi rend="ital"><w n="41.5">l</w>’<w n="41.6">artiste</w>, </hi></l>
						<l n="42" num="6.2"><w n="42.1">Jeune</w>, <w n="42.2">avec</w> <w n="42.3">un</w> <w n="42.4">air</w> <w n="42.5">ahuri</w>.</l>
						<l n="43" num="6.3"><w n="43.1">Était</w>-<w n="43.2">ce</w> <w n="43.3">un</w> <w n="43.4">du</w> <hi rend="ital"><w n="43.5">charivari</w>, </hi></l>
						<l n="44" num="6.4"><w n="44.1">Du</w> <hi rend="ital"><w n="44.2">furet</w>, </hi><w n="44.3">du</w> <hi rend="ital"><w n="44.4">feuilletoniste</w> ? </hi></l>
						<l n="45" num="6.5"><w n="45.1">Était</w>-<w n="45.2">il</w> <w n="45.3">le</w> <w n="45.4">Timoléon</w></l>
						<l n="46" num="6.6"><w n="46.1">Des</w> <w n="46.2">saint</w>-<w n="46.3">Almes</w> <w n="46.4">et</w> <w n="46.5">des</w> <w n="46.6">Virmaîtres</w>… ?</l>
						<l n="47" num="6.7"><w n="47.1">Allez</w>, <w n="47.2">allez</w>, <w n="47.3">ô</w> <w n="47.4">gendelettres</w>,</l>
						<l n="48" num="6.8"><w n="48.1">Manger</w> <w n="48.2">du</w> <w n="48.3">flan</w> <w n="48.4">dans</w> <w n="48.5">l</w>’<w n="48.6">odéon</w> !</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1"><w n="49.1">On</w> <w n="49.2">ne</w> <w n="49.3">savait</w>. <w n="49.4">L</w>’<w n="49.5">ange</w> <w n="49.6">Asphodèle</w></l>
						<l n="50" num="7.2"><w n="50.1">Fit</w> <w n="50.2">avec</w> <w n="50.3">lui</w> <w n="50.4">deux</w> <w n="50.5">mille</w> <w n="50.6">vers</w>.</l>
						<l n="51" num="7.3"><w n="51.1">Les</w> <w n="51.2">Vermots</w> <w n="51.3">et</w> <w n="51.4">les</w> <w n="51.5">Mantz</w> <w n="51.6">divers</w></l>
						<l n="52" num="7.4"><w n="52.1">Derrière</w> <w n="52.2">eux</w> <w n="52.3">tenaient</w> <w n="52.4">la</w> <w n="52.5">chandelle</w>.</l>
						<l n="53" num="7.5"><w n="53.1">Ils</w> <w n="53.2">jouaient</w> <w n="53.3">de</w> <w n="53.4">l</w>’<w n="53.5">accordéon</w></l>
						<l n="54" num="7.6"><w n="54.1">Pour</w> <w n="54.2">mieux</w> <w n="54.3">accompagner</w> <w n="54.4">ces</w> <w n="54.5">mètres</w>…</l>
						<l n="55" num="7.7"><w n="55.1">Allez</w>, <w n="55.2">allez</w>, <w n="55.3">ô</w> <w n="55.4">gendelettres</w>,</l>
						<l n="56" num="7.8"><w n="56.1">Manger</w> <w n="56.2">du</w> <w n="56.3">flan</w> <w n="56.4">dans</w> <w n="56.5">l</w>’<w n="56.6">odéon</w> !</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1"><w n="57.1">La</w> <w n="57.2">lune</w> <w n="57.3">était</w> <w n="57.4">à</w> <w n="57.5">la</w> <w n="57.6">fin</w> <w n="57.7">nue</w>,</l>
						<l n="58" num="8.2"><w n="58.1">Et</w> <w n="58.2">ses</w> <w n="58.3">rayons</w>, <w n="58.4">doux</w> <w n="58.5">aux</w> <w n="58.6">rimeurs</w>,</l>
						<l n="59" num="8.3"><w n="59.1">Parmi</w> <w n="59.2">le</w> <w n="59.3">gaz</w> <w n="59.4">des</w> <w n="59.5">allumeurs</w></l>
						<l n="60" num="8.4"><w n="60.1">Découpaient</w> <w n="60.2">en</w> <w n="60.3">blanc</w> <w n="60.4">sur</w> <w n="60.5">la</w> <w n="60.6">nue</w></l>
						<l n="61" num="8.5"><w n="61.1">Les</w> <w n="61.2">chapiteaux</w> <w n="61.3">du</w> <w n="61.4">panthéon</w></l>
						<l n="62" num="8.6"><w n="62.1">Pareils</w> <w n="62.2">à</w> <w n="62.3">de</w> <w n="62.4">grands</w> <w n="62.5">baromètres</w>…</l>
						<l n="63" num="8.7"><w n="63.1">Allez</w>, <w n="63.2">allez</w>, <w n="63.3">ô</w> <w n="63.4">gendelettres</w>,</l>
						<l n="64" num="8.8"><w n="64.1">Manger</w> <w n="64.2">du</w> <w n="64.3">flan</w> <w n="64.4">dans</w> <w n="64.5">l</w>’<w n="64.6">odéon</w> !</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1"><w n="65.1">Mais</w> <w n="65.2">contre</w> <w n="65.3">Asphodèle</w> <w n="65.4">rageuses</w>,</l>
						<l n="66" num="9.2"><w n="66.1">Des</w> <w n="66.2">bas</w>-<w n="66.3">bleus</w>, <w n="66.4">confits</w> <w n="66.5">par</w> <w n="66.6">gannal</w>,</l>
						<l n="67" num="9.3"><w n="67.1">Dans</w> <w n="67.2">le</w> <w n="67.3">salon</w> <w n="67.4">vert</w> <w n="67.5">du</w> <w n="67.6">journal</w></l>
						<l n="68" num="9.4"><w n="68.1">Dansaient</w> <w n="68.2">des</w> <w n="68.3">polkas</w> <w n="68.4">orageuses</w>.</l>
						<l n="69" num="9.5"><w n="69.1">Les</w> <w n="69.2">élèves</w> <w n="69.3">de</w> <w n="69.4">l</w>’<w n="69.5">orphéon</w></l>
						<l n="70" num="9.6"><w n="70.1">Leur</w> <w n="70.2">chantaient</w> <hi rend="ital"><w n="70.3">les</w> <w n="70.4">bœufs</w> </hi><w n="70.5">aux</w> <w n="70.6">fenêtres</w>…</l>
						<l n="71" num="9.7"><w n="71.1">Allez</w>, <w n="71.2">allez</w>, <w n="71.3">ô</w> <w n="71.4">gendelettres</w>,</l>
						<l n="72" num="9.8"><w n="72.1">Manger</w> <w n="72.2">du</w> <w n="72.3">flan</w> <w n="72.4">dans</w> <w n="72.5">l</w>’<w n="72.6">odéon</w> !</l>
					</lg>
					<lg n="10">
						<l n="73" num="10.1"><w n="73.1">On</w> <w n="73.2">voit</w> <w n="73.3">dormir</w> <w n="73.4">au</w> <w n="73.5">nid</w> <w n="73.6">la</w> <w n="73.7">caille</w></l>
						<l n="74" num="10.2"><w n="74.1">Qu</w>’<w n="74.2">un</w> <w n="74.3">vautour</w> <w n="74.4">fauve</w> <w n="74.5">lorgne</w> <w n="74.6">en</w> <w n="74.7">bas</w> :</l>
						<l n="75" num="10.3"><w n="75.1">Telle</w> <w n="75.2">s</w>’<w n="75.3">endormait</w> <w n="75.4">Carabas</w>.</l>
						<l n="76" num="10.4"><w n="76.1">Le</w> <w n="76.2">jeune</w> <w n="76.3">homme</w> <w n="76.4">au</w> <w n="76.5">lorgnon</w> <w n="76.6">d</w>’<w n="76.7">écaille</w>,</l>
						<l n="77" num="10.5"><w n="77.1">C</w>’<w n="77.2">était</w> <w n="77.3">le</w> <w n="77.4">doux</w> <w n="77.5">Napoléon</w></l>
						<l n="78" num="10.6"><w n="78.1">Citrouillard</w>, <w n="78.2">l</w>’<w n="78.3">un</w> <w n="78.4">de</w> <w n="78.5">nos</w> <w n="78.6">vieux</w> <w n="78.7">maîtres</w>…</l>
						<l n="79" num="10.7"><w n="79.1">Allez</w>, <w n="79.2">allez</w>, <w n="79.3">ô</w> <w n="79.4">gendelettres</w>,</l>
						<l n="80" num="10.8"><w n="80.1">Manger</w> <w n="80.2">du</w> <w n="80.3">flan</w> <w n="80.4">dans</w> <w n="80.5">l</w>’<w n="80.6">odéon</w> !</l>
					</lg>
					<lg n="11">
						<l n="81" num="11.1"><w n="81.1">Voici</w> <w n="81.2">bien</w> <w n="81.3">une</w> <w n="81.4">autre</w> <w n="81.5">guitare</w> !</l>
						<l n="82" num="11.2"><w n="82.1">Citrouillard</w>, <w n="82.2">ce</w> <w n="82.3">dandy</w> <w n="82.4">sans</w> <w n="82.5">foi</w>,</l>
						<l n="83" num="11.3"><w n="83.1">La</w> <w n="83.2">fit</w> <w n="83.3">un</w> <w n="83.4">jour</w>, <w n="83.5">de</w> <w n="83.6">par</w> <w n="83.7">le</w> <w n="83.8">roi</w>,</l>
						<l n="84" num="11.4"><w n="84.1">Rédactrice</w> <w n="84.2">du</w> <hi rend="ital"><w n="84.3">tintamarre</w> ! </hi></l>
						<l n="85" num="11.5"><w n="85.1">Elle</w> <w n="85.2">y</w> <w n="85.3">traduit</w> <w n="85.4">Anacréon</w></l>
						<l n="86" num="11.6"><w n="86.1">En</w> <w n="86.2">vers</w> <w n="86.3">de</w> <w n="86.4">quatre</w> <w n="86.5">centimètres</w>…</l>
						<l n="87" num="11.7"><w n="87.1">Allez</w>, <w n="87.2">allez</w>, <w n="87.3">ô</w> <w n="87.4">gendelettres</w>,</l>
						<l n="88" num="11.8"><w n="88.1">Manger</w> <w n="88.2">du</w> <w n="88.3">flan</w> <w n="88.4">dans</w> <w n="88.5">l</w>’<w n="88.6">odéon</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1846">septembre 1846</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>