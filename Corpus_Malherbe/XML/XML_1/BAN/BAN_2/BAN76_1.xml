<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN76">
				<head type="main">PREMIER SOLEIL</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Italie</w>, <w n="1.2">Italie</w>, <w n="1.3">ô</w> <w n="1.4">terre</w> <w n="1.5">où</w> <w n="1.6">toutes</w> <w n="1.7">choses</w></l>
					<l n="2" num="1.2"><w n="2.1">Frissonnent</w> <w n="2.2">de</w> <w n="2.3">soleil</w>, <w n="2.4">hormis</w> <w n="2.5">tes</w> <w n="2.6">méchants</w> <w n="2.7">vins</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Paradis</w> <w n="3.2">où</w> <w n="3.3">l</w>’<w n="3.4">on</w> <w n="3.5">trouve</w> <w n="3.6">avec</w> <w n="3.7">les</w> <w n="3.8">lauriers</w>-<w n="3.9">roses</w></l>
					<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">sorbets</w> <w n="4.3">à</w> <w n="4.4">la</w> <w n="4.5">neige</w> <w n="4.6">et</w> <w n="4.7">des</w> <w n="4.8">ballets</w> <w n="4.9">divins</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Terre</w> <w n="5.2">où</w> <w n="5.3">le</w> <w n="5.4">doux</w> <w n="5.5">langage</w> <w n="5.6">est</w> <w n="5.7">rempli</w> <w n="5.8">de</w> <w n="5.9">diphthongues</w> !</l>
					<l n="6" num="2.2"><w n="6.1">Voici</w> <w n="6.2">qu</w>’<w n="6.3">on</w> <w n="6.4">pense</w> <w n="6.5">à</w> <w n="6.6">toi</w>, <w n="6.7">car</w> <w n="6.8">voici</w> <w n="6.9">venir</w> <w n="6.10">mai</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">nous</w> <w n="7.3">ne</w> <w n="7.4">verrons</w> <w n="7.5">plus</w> <w n="7.6">les</w> <w n="7.7">redingotes</w> <w n="7.8">longues</w></l>
					<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">tout</w> <w n="8.3">parfait</w> <w n="8.4">dandy</w> <w n="8.5">se</w> <w n="8.6">tenait</w> <w n="8.7">enfermé</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Sourire</w> <w n="9.2">du</w> <w n="9.3">printemps</w>, <w n="9.4">je</w> <w n="9.5">t</w>’<w n="9.6">offre</w> <w n="9.7">en</w> <w n="9.8">holocauste</w></l>
					<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">manchons</w>, <w n="10.3">les</w> <w n="10.4">albums</w> <w n="10.5">et</w> <w n="10.6">le</w> <w n="10.7">pesant</w> <w n="10.8">castor</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Hurrah</w> ! <w n="11.2">Gais</w> <w n="11.3">postillons</w>, <w n="11.4">que</w> <w n="11.5">les</w> <w n="11.6">chaises</w> <w n="11.7">de</w> <w n="11.8">poste</w></l>
					<l n="12" num="3.4"><w n="12.1">Volent</w>, <w n="12.2">en</w> <w n="12.3">agitant</w> <w n="12.4">une</w> <w n="12.5">poussière</w> <w n="12.6">d</w>’<w n="12.7">or</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">lilas</w> <w n="13.3">vont</w> <w n="13.4">fleurir</w>, <w n="13.5">et</w> <w n="13.6">Ninon</w> <w n="13.7">me</w> <w n="13.8">querelle</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">ce</w> <w n="14.3">matin</w> <w n="14.4">j</w>’<w n="14.5">ai</w> <w n="14.6">vu</w> <w n="14.7">Mademoiselle</w> <w n="14.8">Ozy</w></l>
					<l n="15" num="4.3"><w n="15.1">Près</w> <w n="15.2">des</w> <w n="15.3">panoramas</w> <w n="15.4">déployer</w> <w n="15.5">son</w> <w n="15.6">ombrelle</w> :</l>
					<l n="16" num="4.4"><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">que</w> <w n="16.4">le</w> <w n="16.5">triste</w> <w n="16.6">hiver</w> <w n="16.7">est</w> <w n="16.8">bien</w> <w n="16.9">mort</w>, <w n="16.10">songez</w>-<w n="16.11">y</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Voici</w> <w n="17.2">dans</w> <w n="17.3">le</w> <w n="17.4">gazon</w> <w n="17.5">les</w> <w n="17.6">corolles</w> <w n="17.7">ouvertes</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Le</w> <w n="18.2">parfum</w> <w n="18.3">de</w> <w n="18.4">la</w> <w n="18.5">sève</w> <w n="18.6">embaumera</w> <w n="18.7">les</w> <w n="18.8">soirs</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">devant</w> <w n="19.3">les</w> <w n="19.4">cafés</w>, <w n="19.5">des</w> <w n="19.6">rangs</w> <w n="19.7">de</w> <w n="19.8">tables</w> <w n="19.9">vertes</w></l>
					<l n="20" num="5.4"><w n="20.1">Ont</w> <w n="20.2">par</w> <w n="20.3">enchantement</w> <w n="20.4">poussé</w> <w n="20.5">sur</w> <w n="20.6">les</w> <w n="20.7">trottoirs</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Adieu</w> <w n="21.2">donc</w>, <w n="21.3">nuits</w> <w n="21.4">en</w> <w n="21.5">flamme</w> <w n="21.6">où</w> <w n="21.7">le</w> <w n="21.8">bal</w> <w n="21.9">s</w>’<w n="21.10">extasie</w> !</l>
					<l n="22" num="6.2"><w n="22.1">Adieu</w> <w n="22.2">concerts</w>, <w n="22.3">scotishs</w>, <w n="22.4">glaces</w> <w n="22.5">à</w> <w n="22.6">l</w>’<w n="22.7">ananas</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Fleurissez</w> <w n="23.2">maintenant</w>, <w n="23.3">fleurs</w> <w n="23.4">de</w> <w n="23.5">la</w> <w n="23.6">fantaisie</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Sur</w> <w n="24.2">la</w> <w n="24.3">toile</w> <w n="24.4">imprimée</w> <w n="24.5">et</w> <w n="24.6">sur</w> <w n="24.7">le</w> <w n="24.8">jaconas</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">vous</w>, <w n="25.3">pour</w> <w n="25.4">qui</w> <w n="25.5">naîtra</w> <w n="25.6">la</w> <w n="25.7">saison</w> <w n="25.8">des</w> <w n="25.9">pervenches</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Rendez</w> <w n="26.2">à</w> <w n="26.3">ces</w> <w n="26.4">zéphyrs</w> <w n="26.5">que</w> <w n="26.6">voilà</w> <w n="26.7">revenus</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Les</w> <w n="27.2">légers</w> <w n="27.3">mantelets</w> <w n="27.4">avec</w> <w n="27.5">les</w> <w n="27.6">robes</w> <w n="27.7">blanches</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">dans</w> <w n="28.3">un</w> <w n="28.4">mois</w> <w n="28.5">d</w>’<w n="28.6">ici</w> <w n="28.7">vous</w> <w n="28.8">sortirez</w> <w n="28.9">bras</w> <w n="28.10">nus</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Bientôt</w>, <w n="29.2">sous</w> <w n="29.3">les</w> <w n="29.4">forêts</w> <w n="29.5">qu</w>’<w n="29.6">argentera</w> <w n="29.7">la</w> <w n="29.8">lune</w>,</l>
					<l n="30" num="8.2"><w n="30.1">S</w>’<w n="30.2">envolera</w> <w n="30.3">gaîment</w> <w n="30.4">la</w> <w n="30.5">nouvelle</w> <w n="30.6">chanson</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Nous</w> <w n="31.2">y</w> <w n="31.3">verrons</w> <w n="31.4">courir</w> <w n="31.5">la</w> <w n="31.6">rousse</w> <w n="31.7">avec</w> <w n="31.8">la</w> <w n="31.9">brune</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">Musette</w> <w n="32.3">et</w> <w n="32.4">Nichette</w> <w n="32.5">avec</w> <w n="32.6">Mimi</w> <w n="32.7">Pinson</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Bientôt</w> <w n="33.2">tu</w> <w n="33.3">t</w>’<w n="33.4">enfuiras</w>, <w n="33.5">ange</w> <w n="33.6">mélancolie</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Et</w> <w n="34.2">dans</w> <w n="34.3">le</w> <w n="34.4">bas</w>-<w n="34.5">meudon</w> <w n="34.6">les</w> <w n="34.7">bosquets</w> <w n="34.8">seront</w> <w n="34.9">verts</w>.</l>
					<l n="35" num="9.3"><w n="35.1">Débouchez</w> <w n="35.2">de</w> <w n="35.3">ce</w> <w n="35.4">vin</w> <w n="35.5">que</w> <w n="35.6">j</w>’<w n="35.7">aime</w> <w n="35.8">à</w> <w n="35.9">la</w> <w n="35.10">folie</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">donnez</w>-<w n="36.3">moi</w> <w n="36.4">Ronsard</w>, <w n="36.5">je</w> <w n="36.6">veux</w> <w n="36.7">lire</w> <w n="36.8">des</w> <w n="36.9">vers</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Par</w> <w n="37.2">ces</w> <w n="37.3">premiers</w> <w n="37.4">beaux</w> <w n="37.5">jours</w> <w n="37.6">la</w> <w n="37.7">campagne</w> <w n="37.8">est</w> <w n="37.9">en</w> <w n="37.10">fête</w></l>
					<l n="38" num="10.2"><w n="38.1">Ainsi</w> <w n="38.2">qu</w>’<w n="38.3">une</w> <w n="38.4">épousée</w>, <w n="38.5">et</w> <w n="38.6">Paris</w> <w n="38.7">est</w> <w n="38.8">charmant</w>.</l>
					<l n="39" num="10.3"><w n="39.1">Chantez</w>, <w n="39.2">petits</w> <w n="39.3">oiseaux</w> <w n="39.4">du</w> <w n="39.5">ciel</w>, <w n="39.6">et</w> <w n="39.7">toi</w>, <w n="39.8">poëte</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Parle</w> ! <w n="40.2">Nous</w> <w n="40.3">t</w>’<w n="40.4">écoutons</w> <w n="40.5">avec</w> <w n="40.6">ravissement</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">C</w>’<w n="41.2">est</w> <w n="41.3">le</w> <w n="41.4">temps</w> <w n="41.5">où</w> <w n="41.6">l</w>’<w n="41.7">on</w> <w n="41.8">mène</w> <w n="41.9">une</w> <w n="41.10">jeune</w> <w n="41.11">maîtresse</w></l>
					<l n="42" num="11.2"><w n="42.1">Cueillir</w> <w n="42.2">la</w> <w n="42.3">violette</w> <w n="42.4">avec</w> <w n="42.5">ses</w> <w n="42.6">petits</w> <w n="42.7">doigts</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Et</w> <w n="43.2">toute</w> <w n="43.3">créature</w> <w n="43.4">a</w> <w n="43.5">le</w> <w n="43.6">cœur</w> <w n="43.7">plein</w> <w n="43.8">d</w>’<w n="43.9">ivresse</w></l>
					<l n="44" num="11.4"><w n="44.1">Excepté</w> <w n="44.2">les</w> <w n="44.3">pervers</w> <w n="44.4">et</w> <w n="44.5">les</w> <w n="44.6">marchands</w> <w n="44.7">de</w> <w n="44.8">bois</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1854">avril 1854</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>