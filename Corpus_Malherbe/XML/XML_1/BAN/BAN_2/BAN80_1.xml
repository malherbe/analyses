<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉVOHÉ</head><head type="sub_part">NÉMÉSIS INTÉRIMAIRE</head><div type="poem" key="BAN80">
					<head type="form">SATIRE TROISIÈME</head>
					<head type="main">L’OPÉRA TURC</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="2" unit="char"></space><w n="1.1">Chère</w> <w n="1.2">Évohé</w>, <w n="1.3">voici</w> <w n="1.4">le</w> <w n="1.5">carnaval</w> <w n="1.6">qui</w> <w n="1.7">vient</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">l</w>’<w n="2.3">on</w> <w n="2.4">danse</w> <w n="2.5">à</w> <w n="2.6">la</w> <w n="2.7">fin</w> <w n="2.8">du</w> <w n="2.9">mois</w>, <w n="2.10">s</w>’<w n="2.11">il</w> <w n="2.12">m</w>’<w n="2.13">en</w> <w n="2.14">souvient</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">voulais</w> <w n="3.3">vous</w> <w n="3.4">montrer</w> <w n="3.5">une</w> <w n="3.6">chose</w> <w n="3.7">divine</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">domino</w> <w n="4.3">charmant</w> <w n="4.4">que</w> <w n="4.5">Gavarni</w> <w n="4.6">dessine</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Une</w> <w n="5.2">surprise</w>, <w n="5.3">enfin</w> ! <w n="5.4">Pourquoi</w> <w n="5.5">venir</w> <w n="5.6">le</w> <w n="5.7">soir</w> ?</l>
						<l n="6" num="1.6"><w n="6.1">Nous</w> <w n="6.2">n</w>’<w n="6.3">avons</w> <w n="6.4">même</w> <w n="6.5">pas</w> <w n="6.6">le</w> <w n="6.7">temps</w> <w n="6.8">de</w> <w n="6.9">nous</w> <w n="6.10">asseoir</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Quand</w> <w n="7.2">j</w>’<w n="7.3">aurais</w>, <w n="7.4">pour</w> <w n="7.5">rester</w> <w n="7.6">sur</w> <w n="7.7">ces</w> <w n="7.8">divans</w> <w n="7.9">sublimes</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Encor</w> <w n="8.2">plus</w> <w n="8.3">de</w> <w n="8.4">raisons</w> <w n="8.5">que</w> <w n="8.6">vous</w> <w n="8.7">n</w>’<w n="8.8">avez</w> <w n="8.9">de</w> <w n="8.10">rimes</w> !</l>
						<l n="9" num="1.9"><w n="9.1">Il</w> <w n="9.2">faut</w> <w n="9.3">partir</w>. <w n="9.4">Prenez</w> <w n="9.5">votre</w> <w n="9.6">châle</w>, <w n="9.7">Évohé</w>.</l>
						<l n="10" num="1.10"><w n="10.1">Si</w> <w n="10.2">je</w> <w n="10.3">ne</w> <w n="10.4">vous</w> <w n="10.5">savais</w> <w n="10.6">un</w> <w n="10.7">cœur</w> <w n="10.8">très</w>-<w n="10.9">dévoué</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">de</w> <w n="11.3">l</w>’<w n="11.4">esprit</w> <w n="11.5">à</w> <w n="11.6">flots</w>, <w n="11.7">si</w> <w n="11.8">vous</w> <w n="11.9">étiez</w> <w n="11.10">bégueule</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Je</w> <w n="12.2">vous</w> <w n="12.3">engagerais</w> <w n="12.4">à</w> <w n="12.5">rester</w> <w n="12.6">toute</w> <w n="12.7">seule</w> ;</l>
						<l n="13" num="1.13"><w n="13.1">Car</w> <w n="13.2">je</w> <w n="13.3">crois</w> <w n="13.4">qu</w>’<w n="13.5">il</w> <w n="13.6">s</w>’<w n="13.7">agit</w> <w n="13.8">d</w>’<w n="13.9">aller</w> <w n="13.10">encore</w> <w n="13.11">un</w> <w n="13.12">coup</w></l>
						<l n="14" num="1.14"><w n="14.1">Attaquer</w> <w n="14.2">un</w> <w n="14.3">défaut</w> <w n="14.4">que</w> <w n="14.5">vous</w> <w n="14.6">avez</w> <w n="14.7">beaucoup</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Vous</w> <w n="15.2">voyez</w> <w n="15.3">trop</w> <w n="15.4">souvent</w> <w n="15.5">votre</w> <w n="15.6">amie</w> <w n="15.7">au</w> <w n="15.8">king</w>’<w n="15.9">s</w> <w n="15.10">Charle</w></l>
						<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">je</w> <w n="16.3">vous</w> <w n="16.4">vois</w> <w n="16.5">rougir</w> <w n="16.6">chaque</w> <w n="16.7">fois</w> <w n="16.8">que</w> <w n="16.9">j</w>’<w n="16.10">en</w> <w n="16.11">parle</w> !</l>
						<l n="17" num="1.17"><space quantity="2" unit="char"></space><w n="17.1">Tortille</w> <w n="17.2">tes</w> <w n="17.3">cheveux</w> <w n="17.4">avec</w> <w n="17.5">des</w> <w n="17.6">tresses</w> <w n="17.7">d</w>’<w n="17.8">or</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Ô</w> <w n="18.2">ma</w> <w n="18.3">muse</w>, <w n="18.4">et</w> <w n="18.5">volons</w> <w n="18.6">sur</w> <w n="18.7">l</w>’<w n="18.8">aile</w> <w n="18.9">d</w>’<w n="18.10">un</w> <w n="18.11">condor</w></l>
						<l n="19" num="1.19"><w n="19.1">Jusqu</w>’<w n="19.2">au</w> <w n="19.3">pays</w> <w n="19.4">féerique</w> <w n="19.5">où</w> <w n="19.6">les</w> <w n="19.7">blanches</w> <w n="19.8">sultanes</w></l>
						<l n="20" num="1.20"><w n="20.1">Baignent</w> <w n="20.2">leurs</w> <w n="20.3">corps</w> <w n="20.4">polis</w> <w n="20.5">à</w> <w n="20.6">l</w>’<w n="20.7">ombre</w> <w n="20.8">des</w> <w n="20.9">platanes</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">s</w>’<w n="21.3">enivrent</w> <w n="21.4">le</w> <w n="21.5">cœur</w> <w n="21.6">aux</w> <w n="21.7">chansons</w> <w n="21.8">du</w> <w n="21.9">harem</w></l>
						<l n="22" num="1.22"><w n="22.1">Sous</w> <w n="22.2">les</w> <w n="22.3">rosiers</w> <w n="22.4">de</w> <w n="22.5">Perse</w> <w n="22.6">et</w> <w n="22.7">de</w> <w n="22.8">Jérusalem</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Tandis</w> <w n="23.2">qu</w>’<w n="23.3">en</w> <w n="23.4">souriant</w>, <w n="23.5">les</w> <w n="23.6">esclaves</w> <w n="23.7">tartares</w></l>
						<l n="24" num="1.24"><w n="24.1">Arrachent</w> <w n="24.2">des</w> <w n="24.3">soupirs</w> <w n="24.4">à</w> <w n="24.5">l</w>’<w n="24.6">âme</w> <w n="24.7">des</w> <w n="24.8">guitares</w>.</l>
						<l n="25" num="1.25"><space quantity="2" unit="char"></space><w n="25.1">Il</w> <w n="25.2">était</w> <w n="25.3">à</w> <w n="25.4">Stamboul</w> <w n="25.5">un</w> <w n="25.6">théâtre</w> <w n="25.7">enchanteur</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Dont</w> <w n="26.2">le</w> <w n="26.3">sultan</w> <w n="26.4">lui</w>-<w n="26.5">même</w> <w n="26.6">était</w> <w n="26.7">le</w> <w n="26.8">directeur</w> :</l>
						<l n="27" num="1.27"><w n="27.1">La</w> <w n="27.2">musique</w> <w n="27.3">et</w> <w n="27.4">ses</w> <w n="27.5">voix</w>, <w n="27.6">l</w>’<w n="27.7">altière</w> <w n="27.8">poésie</w>,</l>
						<l n="28" num="1.28"><w n="28.1">Les</w> <w n="28.2">danses</w> <w n="28.3">de</w> <w n="28.4">l</w>’<w n="28.5">Espagne</w> <w n="28.6">et</w> <w n="28.7">de</w> <w n="28.8">la</w> <w n="28.9">molle</w> <w n="28.10">Asie</w></l>
						<l n="29" num="1.29"><w n="29.1">Enchantaient</w>, <w n="29.2">à</w> <w n="29.3">souhait</w> <w n="29.4">pour</w> <w n="29.5">l</w>’<w n="29.6">extase</w> <w n="29.7">des</w> <w n="29.8">sens</w>,</l>
						<l n="30" num="1.30"><w n="30.1">Ce</w> <w n="30.2">palais</w> <w n="30.3">ébloui</w> <w n="30.4">de</w> <w n="30.5">feux</w> <w n="30.6">resplendissants</w>.</l>
						<l n="31" num="1.31"><w n="31.1">Or</w>, <w n="31.2">le</w> <w n="31.3">sultan</w>, <w n="31.4">naguère</w>, <w n="31.5">en</w> <w n="31.6">ses</w> <w n="31.7">jours</w> <w n="31.8">d</w>’<w n="31.9">allégresse</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Avait</w> <w n="32.2">dormi</w> <w n="32.3">longtemps</w> <w n="32.4">chez</w> <w n="32.5">les</w> <w n="32.6">filles</w> <w n="32.7">de</w> <w n="32.8">Grèce</w>,</l>
						<l n="33" num="1.33"><w n="33.1">Et</w>, <w n="33.2">versant</w> <w n="33.3">des</w> <w n="33.4">parfums</w> <w n="33.5">sous</w> <w n="33.6">le</w> <w n="33.7">ciel</w> <w n="33.8">embaumé</w>,</l>
						<l n="34" num="1.34"><w n="34.1">Ainsi</w> <w n="34.2">que</w> <w n="34.3">Madeleine</w> <w n="34.4">avait</w> <w n="34.5">beaucoup</w> <w n="34.6">aimé</w>.</l>
						<l n="35" num="1.35"><w n="35.1">Mais</w> <w n="35.2">quand</w> <w n="35.3">l</w>’<w n="35.4">âge</w> <w n="35.5">de</w> <w n="35.6">glace</w> <w n="35.7">eut</w> <w n="35.8">fondu</w> <w n="35.9">cette</w> <w n="35.10">lave</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Il</w> <w n="36.2">fut</w>, <w n="36.3">à</w> <w n="36.4">son</w> <w n="36.5">hiver</w>, <w n="36.6">l</w>’<w n="36.7">esclave</w> <w n="36.8">d</w>’<w n="36.9">une</w> <w n="36.10">esclave</w></l>
						<l n="37" num="1.37"><w n="37.1">Qui</w> <w n="37.2">lui</w> <w n="37.3">chantait</w> <w n="37.4">le</w> <w n="37.5">soir</w> <w n="37.6">de</w> <w n="37.7">doux</w> <w n="37.8">airs</w> <w n="37.9">espagnols</w>,</l>
						<l n="38" num="1.38"><w n="38.1">D</w>’<w n="38.2">une</w> <w n="38.3">voix</w> <w n="38.4">douce</w> <w n="38.5">à</w> <w n="38.6">faire</w> <w n="38.7">envie</w> <w n="38.8">aux</w> <w n="38.9">rossignols</w>.</l>
						<l n="39" num="1.39"><w n="39.1">Elle</w> <w n="39.2">avait</w> <w n="39.3">les</w> <w n="39.4">langueurs</w> <w n="39.5">des</w> <w n="39.6">filles</w> <w n="39.7">de</w> <w n="39.8">la</w> <w n="39.9">Gaule</w>,</l>
						<l n="40" num="1.40"><w n="40.1">Soit</w> <w n="40.2">qu</w>’<w n="40.3">elle</w> <w n="40.4">soupirât</w> <w n="40.5">la</w> <w n="40.6">romance</w> <w n="40.7">du</w> <w n="40.8">saule</w>,</l>
						<l n="41" num="1.41"><w n="41.1">Ou</w> <w n="41.2">quelque</w> <w n="41.3">chant</w> <w n="41.4">d</w>’<w n="41.5">amour</w> <w n="41.6">plaintif</w> <w n="41.7">et</w> <w n="41.8">singulier</w>,</l>
						<l n="42" num="1.42"><w n="42.1">Sous</w> <w n="42.2">l</w>’<w n="42.3">habit</w> <w n="42.4">provoquant</w> <w n="42.5">d</w>’<w n="42.6">un</w> <w n="42.7">jeune</w> <w n="42.8">cavalier</w>.</l>
						<l n="43" num="1.43"><space quantity="2" unit="char"></space><w n="43.1">Mais</w> <w n="43.2">sa</w> <w n="43.3">pourpre</w>, <w n="43.4">fatale</w> <w n="43.5">aux</w> <w n="43.6">amours</w> <w n="43.7">des</w> <w n="43.8">captives</w>,</l>
						<l n="44" num="1.44"><w n="44.1">Buvait</w> <w n="44.2">le</w> <w n="44.3">sang</w> <w n="44.4">vermeil</w> <w n="44.5">des</w> <w n="44.6">blanches</w> <w n="44.7">et</w> <w n="44.8">des</w> <w n="44.9">juives</w>,</l>
						<l n="45" num="1.45"><w n="45.1">Et</w> <w n="45.2">ses</w> <w n="45.3">regards</w> <w n="45.4">emplis</w> <w n="45.5">de</w> <w n="45.6">force</w> <w n="45.7">et</w> <w n="45.8">de</w> <w n="45.9">douceur</w>,</l>
						<l n="46" num="1.46"><w n="46.1">Demandaient</w> <w n="46.2">chaque</w> <w n="46.3">mois</w> <w n="46.4">la</w> <w n="46.5">tête</w> <w n="46.6">d</w>’<w n="46.7">un</w> <w n="46.8">danseur</w>.</l>
						<l n="47" num="1.47"><w n="47.1">Lorsque</w> <w n="47.2">la</w> <w n="47.3">favorite</w>, <w n="47.4">avec</w> <w n="47.5">ses</w> <w n="47.6">airs</w> <w n="47.7">de</w> <w n="47.8">reine</w>,</l>
						<l n="48" num="1.48"><w n="48.1">Apparaissait</w>, <w n="48.2">portant</w> <w n="48.3">la</w> <w n="48.4">couronne</w> <w n="48.5">sereine</w></l>
						<l n="49" num="1.49"><w n="49.1">Dont</w> <w n="49.2">les</w> <w n="49.3">lys</w> <w n="49.4">enflammés</w> <w n="49.5">ruisselaient</w> <w n="49.6">en</w> <w n="49.7">marchant</w>,</l>
						<l n="50" num="1.50"><w n="50.1">Tout</w> <w n="50.2">le</w> <w n="50.3">peuple</w> <w n="50.4">ébloui</w> <w n="50.5">du</w> <w n="50.6">ballet</w> <w n="50.7">et</w> <w n="50.8">du</w> <w n="50.9">chant</w></l>
						<l n="51" num="1.51"><w n="51.1">Tremblait</w> <w n="51.2">devant</w> <w n="51.3">son</w> <w n="51.4">doigt</w> <w n="51.5">noyé</w> <w n="51.6">dans</w> <w n="51.7">la</w> <w n="51.8">dentelle</w>.</l>
						<l n="52" num="1.52"><w n="52.1">Un</w> <w n="52.2">seul</w> <w n="52.3">avait</w> <w n="52.4">trouvé</w> <w n="52.5">sa</w> <w n="52.6">grâce</w> <w n="52.7">devant</w> <w n="52.8">elle</w>,</l>
						<l n="53" num="1.53"><w n="53.1">Ardent</w> <w n="53.2">comme</w> <w n="53.3">un</w> <w n="53.4">lion</w> <w n="53.5">ou</w> <w n="53.6">comme</w> <w n="53.7">le</w> <w n="53.8">simoun</w>,</l>
						<l n="54" num="1.54"><w n="54.1">Un</w> <w n="54.2">habile</w> <w n="54.3">chanteur</w> <w n="54.4">qu</w>’<w n="54.5">on</w> <w n="54.6">appelait</w> <w n="54.7">Medjnoun</w>.</l>
						<l n="55" num="1.55"><w n="55.1">Or</w>, <w n="55.2">ce</w> <w n="55.3">jeune</w> <w n="55.4">homme</w> <w n="55.5">avait</w> <w n="55.6">la</w> <w n="55.7">perle</w> <w n="55.8">des</w> <w n="55.9">maîtresses</w>,</l>
						<l n="56" num="1.56"><w n="56.1">Une</w> <w n="56.2">blanche</w> <w n="56.3">houri</w> <w n="56.4">qui</w>, <w n="56.5">par</w> <w n="56.6">ses</w> <w n="56.7">longues</w> <w n="56.8">tresses</w>,</l>
						<l n="57" num="1.57"><w n="57.1">Jetait</w> <w n="57.2">aux</w> <w n="57.3">quatre</w> <w n="57.4">vents</w> <w n="57.5">tous</w> <w n="57.6">les</w> <w n="57.7">parfums</w> <w n="57.8">d</w>’<w n="57.9">Ophir</w>,</l>
						<l n="58" num="1.58"><w n="58.1">Paupière</w> <w n="58.2">aux</w> <w n="58.3">sourcils</w> <w n="58.4">noirs</w>, <w n="58.5">prunelles</w> <w n="58.6">de</w> <w n="58.7">saphir</w>,</l>
						<l n="59" num="1.59"><w n="59.1">Gazelle</w> <w n="59.2">pour</w> <w n="59.3">la</w> <w n="59.4">grâce</w> <w n="59.5">indolente</w> <w n="59.6">des</w> <w n="59.7">poses</w>,</l>
						<l n="60" num="1.60"><w n="60.1">Nourmahal</w>, <w n="60.2">dont</w> <w n="60.3">la</w> <w n="60.4">lèvre</w> <w n="60.5">énamourait</w> <w n="60.6">les</w> <w n="60.7">roses</w>.</l>
						<l n="61" num="1.61"><space quantity="2" unit="char"></space><w n="61.1">Medjnoun</w> <w n="61.2">se</w> <w n="61.3">demandait</w> <w n="61.4">quel</w> <w n="61.5">ange</w> <w n="61.6">au</w> <w n="61.7">firmament</w></l>
						<l n="62" num="1.62"><w n="62.1">Avait</w> <w n="62.2">fondu</w> <w n="62.3">pour</w> <w n="62.4">lui</w> <w n="62.5">des</w> <w n="62.6">cœurs</w> <w n="62.7">de</w> <w n="62.8">diamant</w>,</l>
						<l n="63" num="1.63"><w n="63.1">Lorsque</w>, <w n="63.2">par</w> <w n="63.3">une</w> <w n="63.4">nuit</w> <w n="63.5">claire</w> <w n="63.6">d</w>’<w n="63.7">astres</w> <w n="63.8">sans</w> <w n="63.9">nombre</w>,</l>
						<l n="64" num="1.64"><w n="64.1">Errant</w> <w n="64.2">par</w> <w n="64.3">les</w> <w n="64.4">sentiers</w> <w n="64.5">du</w> <w n="64.6">jardin</w> <w n="64.7">comme</w> <w n="64.8">une</w> <w n="64.9">ombre</w>,</l>
						<l n="65" num="1.65"><w n="65.1">Près</w> <w n="65.2">d</w>’<w n="65.3">un</w> <w n="65.4">kiosque</w> <w n="65.5">doré</w>, <w n="65.6">que</w> <w n="65.7">les</w> <w n="65.8">pâles</w> <w n="65.9">jasmins</w></l>
						<l n="66" num="1.66"><w n="66.1">Et</w> <w n="66.2">les</w> <w n="66.3">lys</w> <w n="66.4">aux</w> <w n="66.5">yeux</w> <w n="66.6">d</w>’<w n="66.7">or</w> <w n="66.8">entouraient</w> <w n="66.9">de</w> <w n="66.10">leurs</w> <w n="66.11">mains</w>,</l>
						<l n="67" num="1.67"><w n="67.1">Et</w> <w n="67.2">sur</w> <w n="67.3">lequel</w> <w n="67.4">aussi</w> <w n="67.5">dormaient</w> <w n="67.6">dans</w> <w n="67.7">la</w> <w n="67.8">nuit</w> <w n="67.9">brune</w></l>
						<l n="68" num="1.68"><w n="68.1">Les</w> <w n="68.2">blancs</w> <w n="68.3">rosiers</w> <w n="68.4">baignés</w> <w n="68.5">des</w> <w n="68.6">blancs</w> <w n="68.7">rayons</w> <w n="68.8">de</w> <w n="68.9">lune</w>,</l>
						<l n="69" num="1.69"><w n="69.1">Par</w> <w n="69.2">la</w> <w n="69.3">fenêtre</w> <w n="69.4">ouverte</w> <w n="69.5">il</w> <w n="69.6">entendit</w> <w n="69.7">deux</w> <w n="69.8">voix</w>.</l>
						<l n="70" num="1.70"><w n="70.1">L</w>’<w n="70.2">une</w> <w n="70.3">disait</w> (<w n="70.4">c</w>’<w n="70.5">était</w> <w n="70.6">la</w> <w n="70.7">favorite</w>) : « <w n="70.8">oh</w> ! <w n="70.9">Vois</w>,</l>
						<l n="71" num="1.71"><w n="71.1">Ma</w> <w n="71.2">Nourmahal</w> ! <w n="71.3">Jamais</w> <w n="71.4">le</w> <w n="71.5">cœur</w> <w n="71.6">des</w> <w n="71.7">jeunes</w> <w n="71.8">hommes</w></l>
						<l n="72" num="1.72"><w n="72.1">Ne</w> <w n="72.2">s</w>’<w n="72.3">attendrit</w> ; <w n="72.4">mais</w> <w n="72.5">nous</w>, <w n="72.6">ma</w> <w n="72.7">chère</w> <w n="72.8">âme</w>, <w n="72.9">nous</w> <w n="72.10">sommes</w></l>
						<l n="73" num="1.73"><w n="73.1">Douces</w> ; <w n="73.2">nos</w> <w n="73.3">longs</w> <w n="73.4">cheveux</w> <w n="73.5">sur</w> <w n="73.6">nos</w> <w n="73.7">seins</w> <w n="73.8">endormis</w></l>
						<l n="74" num="1.74"><w n="74.1">Ont</w> <w n="74.2">l</w>’<w n="74.3">air</w> <w n="74.4">en</w> <w n="74.5">se</w> <w n="74.6">mêlant</w> <w n="74.7">de</w> <w n="74.8">deux</w> <w n="74.9">fleuves</w> <w n="74.10">amis</w> ;</l>
						<l n="75" num="1.75"><w n="75.1">Les</w> <w n="75.2">rayons</w> <w n="75.3">de</w> <w n="75.4">la</w> <w n="75.5">nuit</w> <w n="75.6">argentent</w> <w n="75.7">nos</w> <w n="75.8">pensées</w>,</l>
						<l n="76" num="1.76"><w n="76.1">Lorsque</w> <w n="76.2">dans</w> <w n="76.3">un</w> <w n="76.4">hamac</w> <w n="76.5">mollement</w> <w n="76.6">balancées</w>,</l>
						<l n="77" num="1.77"><w n="77.1">Entrelaçant</w> <w n="77.2">nos</w> <w n="77.3">bras</w>, <w n="77.4">nous</w> <w n="77.5">chantons</w> <w n="77.6">deux</w> <w n="77.7">à</w> <w n="77.8">deux</w>,</l>
						<l n="78" num="1.78"><w n="78.1">Ou</w> <w n="78.2">que</w>, <w n="78.3">nous</w> <w n="78.4">confiant</w> <w n="78.5">à</w> <w n="78.6">des</w> <w n="78.7">flots</w> <w n="78.8">hasardeux</w>,</l>
						<l n="79" num="1.79"><w n="79.1">Et</w> <w n="79.2">laissant</w> <w n="79.3">l</w>’<w n="79.4">eau</w> <w n="79.5">d</w>’<w n="79.6">azur</w> <w n="79.7">baiser</w> <w n="79.8">nos</w> <w n="79.9">gorges</w> <w n="79.10">blondes</w>,</l>
						<l n="80" num="1.80"><w n="80.1">Nous</w> <w n="80.2">en</w> <w n="80.3">dérobons</w> <w n="80.4">l</w>’<w n="80.5">or</w> <w n="80.6">sous</w> <w n="80.7">la</w> <w n="80.8">moire</w> <w n="80.9">des</w> <w n="80.10">ondes</w>. »</l>
						<l n="81" num="1.81"><w n="81.1">La</w> <w n="81.2">favorite</w> <w n="81.3">alors</w>, <w n="81.4">les</w> <w n="81.5">yeux</w> <w n="81.6">noyés</w> <w n="81.7">de</w> <w n="81.8">pleurs</w>,</l>
						<l n="82" num="1.82"><w n="82.1">Voyait</w> <w n="82.2">à</w> <w n="82.3">chaque</w> <w n="82.4">mot</w> <w n="82.5">éclore</w> <w n="82.6">mille</w> <w n="82.7">fleurs</w></l>
						<l n="83" num="1.83"><w n="83.1">Sur</w> <w n="83.2">le</w> <w n="83.3">sein</w> <w n="83.4">de</w> <w n="83.5">l</w>’<w n="83.6">enfant</w> <w n="83.7">rougissante</w> <w n="83.8">et</w> <w n="83.9">sans</w> <w n="83.10">voiles</w>,</l>
						<l n="84" num="1.84"><w n="84.1">Et</w> <w n="84.2">le</w> <w n="84.3">regard</w> <w n="84.4">perdu</w> <w n="84.5">dans</w> <w n="84.6">ses</w> <w n="84.7">yeux</w> <w n="84.8">pleins</w> <w n="84.9">d</w>’<w n="84.10">étoiles</w></l>
						<l n="85" num="1.85"><w n="85.1">Comme</w> <w n="85.2">les</w> <w n="85.3">océans</w> <w n="85.4">du</w> <w n="85.5">ciel</w> <w n="85.6">oriental</w>,</l>
						<l n="86" num="1.86"><w n="86.1">Était</w> <w n="86.2">agenouillée</w> <w n="86.3">aux</w> <w n="86.4">pieds</w> <w n="86.5">de</w> <w n="86.6">Nourmahal</w>,</l>
						<l n="87" num="1.87"><w n="87.1">Et</w> <w n="87.2">Nourmahal</w> <w n="87.3">honteuse</w>, <w n="87.4">au</w> <w n="87.5">bout</w> <w n="87.6">de</w> <w n="87.7">chaque</w> <w n="87.8">phrase</w>,</l>
						<l n="88" num="1.88"><w n="88.1">Ramenait</w> <w n="88.2">sur</w> <w n="88.3">son</w> <w n="88.4">cou</w> <w n="88.5">sa</w> <w n="88.6">tunique</w> <w n="88.7">de</w> <w n="88.8">gaze</w>.</l>
						<l n="89" num="1.89"><space quantity="2" unit="char"></space>—« <w n="89.1">permettez</w>, <w n="89.2">dit</w> <w n="89.3">Medjnoun</w>, <w n="89.4">entrant</w> <w n="89.5">à</w> <w n="89.6">la</w> <w n="89.7">Talma</w>,</l>
						<l n="90" num="1.90">« <w n="90.1">Qu</w>’<w n="90.2">ici</w> <w n="90.3">je</w> <w n="90.4">vous</w> <w n="90.5">salue</w>, <w n="90.6">et</w> <w n="90.7">que</w> <w n="90.8">j</w>’<w n="90.9">emmène</w> <w n="90.10">ma</w></l>
						<l n="91" num="1.91">« <w n="91.1">Maîtresse</w> ; <w n="91.2">il</w> <w n="91.3">se</w> <w n="91.4">fait</w> <w n="91.5">tard</w> <w n="91.6">et</w> <w n="91.7">notre</w> <w n="91.8">chambre</w> <w n="91.9">est</w> <w n="91.10">prête</w>.</l>
						<l n="92" num="1.92"><w n="92.1">Medjnoun</w> <w n="92.2">fut</w> <w n="92.3">le</w> <w n="92.4">jour</w> <w n="92.5">même</w> <w n="92.6">admis</w> <w n="92.7">à</w> <w n="92.8">la</w> <w n="92.9">retraite</w>.</l>
						<l n="93" num="1.93"><space quantity="2" unit="char"></space><w n="93.1">Ô</w> <w n="93.2">frères</w> <w n="93.3">de</w> <w n="93.4">don</w> <w n="93.5">Juan</w> ! <w n="93.6">Dompteurs</w> <w n="93.7">des</w> <w n="93.8">flots</w> <w n="93.9">amers</w>,</l>
						<l n="94" num="1.94"><w n="94.1">Qui</w> <w n="94.2">déchirez</w> <w n="94.3">la</w> <w n="94.4">perle</w> <w n="94.5">au</w> <w n="94.6">sein</w> <w n="94.7">meurtri</w> <w n="94.8">des</w> <w n="94.9">mers</w>,</l>
						<l n="95" num="1.95"><w n="95.1">Vous</w> <w n="95.2">dont</w> <w n="95.3">l</w>’<w n="95.4">ardente</w> <w n="95.5">lèvre</w> <w n="95.6">eût</w> <w n="95.7">bu</w> <w n="95.8">jusqu</w>’<w n="95.9">à</w> <w n="95.10">la</w> <w n="95.11">lie</w></l>
						<l n="96" num="1.96"><w n="96.1">Les</w> <w n="96.2">mystères</w> <w n="96.3">sacrés</w> <w n="96.4">de</w> <w n="96.5">Gnide</w> <w n="96.6">et</w> <w n="96.7">d</w>’<w n="96.8">Idalie</w>,</l>
						<l n="97" num="1.97"><w n="97.1">Avec</w> <w n="97.2">vos</w> <w n="97.3">doigts</w> <w n="97.4">sanglants</w> <w n="97.5">fouillez</w> <w n="97.6">l</w>’<w n="97.7">œuvre</w> <w n="97.8">de</w> <w n="97.9">Dieu</w>,</l>
						<l n="98" num="1.98"><w n="98.1">Et</w> <w n="98.2">vous</w> <w n="98.3">ne</w> <w n="98.4">trouverez</w> <w n="98.5">jamais</w>, <w n="98.6">sous</w> <w n="98.7">le</w> <w n="98.8">ciel</w> <w n="98.9">bleu</w>,</l>
						<l n="99" num="1.99"><w n="99.1">Si</w> <w n="99.2">chaste</w> <w n="99.3">lèvre</w>, <w n="99.4">encor</w> <w n="99.5">pleine</w> <w n="99.6">de</w> <w n="99.7">fleurs</w> <w n="99.8">mi</w>-<w n="99.9">closes</w>,</l>
						<l n="100" num="1.100"><w n="100.1">Dont</w> <w n="100.2">la</w> <w n="100.3">pâle</w> <w n="100.4">amitié</w> <w n="100.5">n</w>’<w n="100.6">ait</w> <w n="100.7">effeuillé</w> <w n="100.8">les</w> <w n="100.9">roses</w> !</l>
						<l n="101" num="1.101"><space quantity="2" unit="char"></space><w n="101.1">Toi</w> <w n="101.2">qui</w>, <w n="101.3">depuis</w> <w n="101.4">longtemps</w> <w n="101.5">avec</w> <w n="101.6">ton</w> <w n="101.7">pied</w> <w n="101.8">vainqueur</w>,</l>
						<l n="102" num="1.102"><w n="102.1">As</w> <w n="102.2">foulé</w> <w n="102.3">pas</w> <w n="102.4">à</w> <w n="102.5">pas</w> <w n="102.6">les</w> <w n="102.7">replis</w> <w n="102.8">de</w> <w n="102.9">mon</w> <w n="102.10">cœur</w>,</l>
						<l n="103" num="1.103"><w n="103.1">Blonde</w> <w n="103.2">Évohé</w> ! <w n="103.3">Tu</w> <w n="103.4">sais</w> <w n="103.5">si</w> <w n="103.6">j</w>’<w n="103.7">aime</w> <w n="103.8">le</w> <w n="103.9">théâtre</w>.</l>
						<l n="104" num="1.104"><w n="104.1">Polichinelle</w> <w n="104.2">seul</w> <w n="104.3">peut</w> <w n="104.4">me</w> <w n="104.5">rendre</w> <w n="104.6">idolâtre</w>,</l>
						<l n="105" num="1.105"><w n="105.1">Et</w>, <w n="105.2">lorsque</w> <w n="105.3">nous</w> <w n="105.4">prenons</w> <w n="105.5">des</w> <w n="105.6">billets</w> <w n="105.7">au</w> <w n="105.8">bureau</w>,</l>
						<l n="106" num="1.106"><w n="106.1">C</w>’<w n="106.2">est</w> <w n="106.3">pour</w> <w n="106.4">voir</w> <w n="106.5">par</w> <w n="106.6">hasard</w>, <hi rend="ital"><w n="106.7">Giselle</w> </hi><w n="106.8">ou</w> <w n="106.9">Deburau</w>.</l>
						<l n="107" num="1.107"><w n="107.1">Pour</w> <w n="107.2">la</w> <w n="107.3">grande</w> <w n="107.4">musique</w>, <w n="107.5">elle</w> <w n="107.6">est</w> <w n="107.7">notre</w> <w n="107.8">ennemie</w> :</l>
						<l n="108" num="1.108"><hi rend="ital"><w n="108.1">les</w> <w n="108.2">lauriers</w> <w n="108.3">sont</w> <w n="108.4">coupés</w> </hi><w n="108.5">et</w> <hi rend="ital"><w n="108.6">j</w>’<w n="108.7">aime</w> <w n="108.8">mieux</w> <w n="108.9">ma</w> <w n="108.10">mie</w>,</hi></l>
						<l n="109" num="1.109"><w n="109.1">Avec</w> <w n="109.2">la</w> <hi rend="ital"><w n="109.3">Kradoudja</w>, </hi><w n="109.4">suffisent</w> <w n="109.5">à</w> <w n="109.6">nos</w> <w n="109.7">vœux</w>,</l>
						<l n="110" num="1.110"><w n="110.1">Et</w> <w n="110.2">le</w> <w n="110.3">moindre</w> <w n="110.4">trio</w> <w n="110.5">fait</w> <w n="110.6">dresser</w> <w n="110.7">nos</w> <w n="110.8">cheveux</w>.</l>
						<l n="111" num="1.111"><w n="111.1">Eh</w> <w n="111.2">bien</w> ! <w n="111.3">Ma</w> <w n="111.4">pauvre</w> <w n="111.5">fille</w>, <w n="111.6">il</w> <w n="111.7">faut</w> <w n="111.8">parler</w> <w n="111.9">musique</w> !</l>
						<l n="112" num="1.112"><w n="112.1">La</w> <w n="112.2">basse</w> <w n="112.3">foudroyante</w> <w n="112.4">et</w> <w n="112.5">le</w> <w n="112.6">ténor</w> <w n="112.7">phthisique</w></l>
						<l n="113" num="1.113"><w n="113.1">Nous</w> <w n="113.2">font</w> <w n="113.3">l</w>’<w n="113.4">œil</w> <w n="113.5">en</w> <w n="113.6">coulisse</w> <w n="113.7">et</w> <w n="113.8">demandent</w> <w n="113.9">nos</w> <w n="113.10">vers</w> ;</l>
						<l n="114" num="1.114"><w n="114.1">Duègne</w> <w n="114.2">au</w> <w n="114.3">nez</w> <w n="114.4">de</w> <w n="114.5">rubis</w>, <w n="114.6">ingénue</w> <w n="114.7">aux</w> <w n="114.8">bras</w> <w n="114.9">verts</w>,</l>
						<l n="115" num="1.115"><w n="115.1">Ciel</w> <w n="115.2">rouge</w>, <w n="115.3">galonné</w> <w n="115.4">de</w> <w n="115.5">quinquets</w> <w n="115.6">pour</w> <w n="115.7">la</w> <w n="115.8">frange</w>,</l>
						<l n="116" num="1.116"><w n="116.1">Il</w> <w n="116.2">faut</w> <w n="116.3">décrire</w> <w n="116.4">tout</w>, <w n="116.5">jusqu</w>’<w n="116.6">aux</w> <w n="116.7">arbres</w> <w n="116.8">orange</w>.</l>
						<l n="117" num="1.117"><w n="117.1">La</w> <w n="117.2">clarinette</w> <w n="117.3">aspire</w> <w n="117.4">à</w> <w n="117.5">des</w> <w n="117.6">canards</w> <w n="117.7">écrits</w>,</l>
						<l n="118" num="1.118"><w n="118.1">Et</w> <w n="118.2">le</w> <w n="118.3">bugle</w> <w n="118.4">naissant</w> <w n="118.5">nous</w> <w n="118.6">réclame</w> <w n="118.7">à</w> <w n="118.8">grands</w> <w n="118.9">cris</w>.</l>
						<l n="119" num="1.119"><space quantity="2" unit="char"></space><w n="119.1">Donc</w>, <w n="119.2">samedi</w> <w n="119.3">prochain</w>, <w n="119.4">nous</w> <w n="119.5">dirons</w> <w n="119.6">à</w> <w n="119.7">l</w>’<w n="119.8">Europe</w></l>
						<l n="120" num="1.120"><w n="120.1">Comment</w> <w n="120.2">tombe</w> <w n="120.3">le</w> <w n="120.4">cèdre</w> <w n="120.5">au</w> <w n="120.6">niveau</w> <w n="120.7">de</w> <w n="120.8">l</w>’<w n="120.9">hysope</w>,</l>
						<l n="121" num="1.121"><w n="121.1">Et</w> <w n="121.2">comment</w>, <w n="121.3">et</w> <w n="121.4">par</w> <w n="121.5">quels</w> <w n="121.6">joueurs</w> <w n="121.7">d</w>’<w n="121.8">accordéon</w>,</l>
						<l n="122" num="1.122"><w n="122.1">L</w>’<w n="122.2">opéra</w>, <w n="122.3">devenu</w> <w n="122.4">pareil</w> <w n="122.5">à</w> <w n="122.6">l</w>’<w n="122.7">odéon</w>,</l>
						<l n="123" num="1.123"><w n="123.1">A</w> <w n="123.2">vu</w>, <w n="123.3">depuis</w> <w n="123.4">trois</w> <w n="123.5">ans</w>, <w n="123.6">aux</w> <w n="123.7">stalles</w> <w n="123.8">dédaignées</w>,</l>
						<l n="124" num="1.124"><w n="124.1">S</w>’<w n="124.2">empiler</w> <w n="124.3">en</w> <w n="124.4">monceau</w> <w n="124.5">les</w> <w n="124.6">toiles</w> <w n="124.7">d</w>’<w n="124.8">araignées</w> ;</l>
						<l n="125" num="1.125"><w n="125.1">Et</w> <w n="125.2">comment</w> <w n="125.3">il</w> <w n="125.4">a</w> <w n="125.5">fait</w>, <w n="125.6">pour</w> <w n="125.7">trouver</w> <w n="125.8">un</w> <w n="125.9">ténor</w>,</l>
						<l n="126" num="1.126"><w n="126.1">Des</w> <w n="126.2">voyages</w> <w n="126.3">plus</w> <w n="126.4">longs</w> <w n="126.5">que</w> <w n="126.6">tous</w> <w n="126.7">ceux</w> <w n="126.8">d</w>’<w n="126.9">Anténor</w>.</l>
						<l n="127" num="1.127"><space quantity="2" unit="char"></space><w n="127.1">Après</w> <w n="127.2">tous</w> <w n="127.3">nos</w> <w n="127.4">malheurs</w> <w n="127.5">et</w> <w n="127.6">ton</w> <w n="127.7">frac</w> <w n="127.8">mis</w> <w n="127.9">en</w> <w n="127.10">loques</w>,</l>
						<l n="128" num="1.128"><w n="128.1">Tu</w> <w n="128.2">dois</w> <w n="128.3">haïr</w> <w n="128.4">Thalie</w> <w n="128.5">et</w> <w n="128.6">toutes</w> <w n="128.7">ses</w> <w n="128.8">breloques</w> ;</l>
						<l n="129" num="1.129"><w n="129.1">Mais</w> <w n="129.2">si</w> <w n="129.3">tu</w> <w n="129.4">peux</w> <w n="129.5">encor</w> <w n="129.6">me</w> <w n="129.7">suivre</w> <w n="129.8">sans</w> <w n="129.9">frémir</w>,</l>
						<l n="130" num="1.130"><w n="130.1">Je</w> <w n="130.2">te</w> <w n="130.3">promets</w> <w n="130.4">ce</w> <w n="130.5">soir</w> <w n="130.6">ce</w> <w n="130.7">bijou</w> <w n="130.8">de</w> <w n="130.9">Kashmir</w></l>
						<l n="131" num="1.131"><w n="131.1">Qu</w>’<w n="131.2">un</w> <w n="131.3">faible</w> <w n="131.4">vent</w> <w n="131.5">d</w>’<w n="131.6">été</w> <w n="131.7">ride</w> <w n="131.8">comme</w> <w n="131.9">les</w> <w n="131.10">vagues</w>,</l>
						<l n="132" num="1.132"><w n="132.1">Et</w> <w n="132.2">qui</w> <w n="132.3">passe</w> <w n="132.4">au</w> <w n="132.5">travers</w> <w n="132.6">des</w> <w n="132.7">plus</w> <w n="132.8">petites</w> <w n="132.9">bagues</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1845">décembre 1845</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>