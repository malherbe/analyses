<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Odes funambulesques</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3888 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L922</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Odes funambulesques</title>
								<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>M. Levy</publisher>
									<date when="1859">1859</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres de Théodore de Banville : Odes funambulesques</title>
						<author>Théodore de BANVILLE</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-23" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-14" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRIOLETS</head><div type="poem" key="BAN115">
					<head type="main">MONSIEUR JASPIN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Connaissez</w>-<w n="1.2">vous</w> <w n="1.3">Monsieur</w> <w n="1.4">Jaspin</w></l>
						<l n="2" num="1.2"><w n="2.1">De</w> <hi rend="ital"><w n="2.2">l</w>’<w n="2.3">estaminet</w> <w n="2.4">de</w> <w n="2.5">l</w>’<w n="2.6">Europe</w> ? </hi></l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">a</w> <w n="3.3">la</w> <w n="3.4">barbe</w> <w n="3.5">d</w>’<w n="3.6">un</w> <w n="3.7">rapin</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Connaissez</w>-<w n="4.2">vous</w> <w n="4.3">Monsieur</w> <w n="4.4">Jaspin</w> ?</l>
						<l n="5" num="1.5"><w n="5.1">Chevelu</w> <w n="5.2">comme</w> <w n="5.3">un</w> <w n="5.4">vieux</w> <w n="5.5">sapin</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Il</w> <w n="6.2">aime</w> <w n="6.3">la</w> <w n="6.4">brune</w> <w n="6.5">et</w> <w n="6.6">la</w> <w n="6.7">chope</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Connaissez</w>-<w n="7.2">vous</w> <w n="7.3">Monsieur</w> <w n="7.4">Jaspin</w></l>
						<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">l</w>’<hi rend="ital"><w n="8.3">estaminet</w> <w n="8.4">de</w> <w n="8.5">l</w>’<w n="8.6">Europe</w> ? </hi></l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Il</w> <w n="9.2">sait</w> <w n="9.3">hurler</w> <w n="9.4">avec</w> <w n="9.5">les</w> <w n="9.6">loups</w></l>
						<l n="10" num="2.2"><w n="10.1">À</w> <w n="10.2">l</w>’<hi rend="ital"><w n="10.3">estaminet</w> <w n="10.4">de</w> <w n="10.5">l</w>’<w n="10.6">Europe</w>. </hi></l>
						<l n="11" num="2.3"><w n="11.1">Son</w> <w n="11.2">esprit</w> <w n="11.3">pique</w> <w n="11.4">ainsi</w> <w n="11.5">qu</w>’<w n="11.6">un</w> <w n="11.7">houx</w>,</l>
						<l n="12" num="2.4"><w n="12.1">Il</w> <w n="12.2">sait</w> <w n="12.3">hurler</w> <w n="12.4">avec</w> <w n="12.5">les</w> <w n="12.6">loups</w>.</l>
						<l n="13" num="2.5"><w n="13.1">L</w>’<w n="13.2">ivoire</w> <w n="13.3">en</w> <w n="13.4">ses</w> <w n="13.5">longs</w> <w n="13.6">cheveux</w> <w n="13.7">roux</w></l>
						<l n="14" num="2.6"><w n="14.1">Fait</w> <w n="14.2">un</w> <w n="14.3">labeur</w> <w n="14.4">de</w> <w n="14.5">Pénélope</w>.</l>
						<l n="15" num="2.7"><w n="15.1">Il</w> <w n="15.2">sait</w> <w n="15.3">hurler</w> <w n="15.4">avec</w> <w n="15.5">les</w> <w n="15.6">loups</w></l>
						<l n="16" num="2.8"><w n="16.1">À</w> <w n="16.2">l</w>’<hi rend="ital"><w n="16.3">estaminet</w> <w n="16.4">de</w> <w n="16.5">l</w>’<w n="16.6">Europe</w>. </hi></l>
					</lg>
					<closer>
						<dateline>
							<date when="1845">décembre 1845</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>