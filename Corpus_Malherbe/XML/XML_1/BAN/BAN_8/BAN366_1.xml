<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN366">
				<head type="main">La Muse des vingt ans</head>
				<head type="sub_2">Prologue <lb></lb>
					écrit pour la première représentation de « Sappho » <lb></lb>
					Drame de Philoxène Boyer.
				</head>
				<div type="section" n="1">
					<head type="main">La Fantaisie</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Mesdames</w> <w n="1.2">et</w> <w n="1.3">Messieurs</w>, <w n="1.4">pardonnez</w>-<w n="1.5">moi</w> <w n="1.6">si</w> <w n="1.7">j</w>’<w n="1.8">ose</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Pauvre</w> <w n="2.2">Muse</w> <w n="2.3">troublée</w>, <w n="2.4">affronter</w> <w n="2.5">vos</w> <w n="2.6">regards</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">suis</w> <w n="3.3">la</w> <w n="3.4">Fantaisie</w> <w n="3.5">aux</w> <w n="3.6">doigts</w> <w n="3.7">couleur</w> <w n="3.8">de</w> <w n="3.9">rose</w>,</l>
							<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">Muse</w> <w n="4.3">des</w> <w n="4.4">vingt</w> <w n="4.5">ans</w>, <w n="4.6">chercheuse</w> <w n="4.7">de</w> <w n="4.8">hasards</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">tremble</w> <w n="5.3">devant</w> <w n="5.4">vous</w>, <w n="5.5">ô</w> <w n="5.6">foule</w> ! <w n="5.7">hôtes</w> <w n="5.8">illustres</w>,</l>
							<l n="6" num="2.2"><w n="6.1">O</w> <w n="6.2">lèvres</w> <w n="6.3">de</w> <w n="6.4">penseurs</w>, <w n="6.5">ô</w> <w n="6.6">corsages</w> <w n="6.7">fleuris</w> !</l>
							<l n="7" num="2.3"><w n="7.1">Moi</w> <w n="7.2">qui</w> <w n="7.3">vois</w> <w n="7.4">resplendir</w> <w n="7.5">sous</w> <w n="7.6">l</w>’<w n="7.7">éclat</w> <w n="7.8">de</w> <w n="7.9">ces</w> <w n="7.10">lustres</w></l>
							<l n="8" num="2.4"><w n="8.1">Toutes</w> <w n="8.2">les</w> <w n="8.3">majestés</w> <w n="8.4">dont</w> <w n="8.5">rayonne</w> <w n="8.6">Paris</w> ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Tout</w> <w n="9.2">ce</w> <w n="9.3">qui</w> <w n="9.4">brille</w> <w n="9.5">encor</w> <w n="9.6">dans</w> <w n="9.7">la</w> <w n="9.8">moderne</w> <w n="9.9">Athènes</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Toutes</w> <w n="10.2">les</w> <w n="10.3">mains</w> <w n="10.4">de</w> <w n="10.5">lys</w> <w n="10.6">et</w> <w n="10.7">tous</w> <w n="10.8">les</w> <w n="10.9">bras</w> <w n="10.10">charmants</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">grands</w> <w n="11.3">fronts</w> <w n="11.4">éblouis</w> <w n="11.5">et</w> <w n="11.6">les</w> <w n="11.7">beautés</w> <w n="11.8">hautaines</w></l>
							<l n="12" num="3.4"><w n="12.1">Dont</w> <w n="12.2">les</w> <w n="12.3">yeux</w> <w n="12.4">font</w> <w n="12.5">pâlir</w> <w n="12.6">l</w>’<w n="12.7">éclair</w> <w n="12.8">des</w> <w n="12.9">diamants</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">tremble</w>, <w n="13.3">moi</w> <w n="13.4">qui</w> <w n="13.5">sais</w> <w n="13.6">dans</w> <w n="13.7">un</w> <w n="13.8">jardin</w> <w n="13.9">féerique</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Mêlant</w> <w n="14.2">aux</w> <w n="14.3">doux</w> <w n="14.4">ruisseaux</w> <w n="14.5">la</w> <w n="14.6">chanson</w> <w n="14.7">de</w> <w n="14.8">mes</w> <w n="14.9">vers</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Tresser</w> <w n="15.2">en</w> <w n="15.3">souriant</w> <w n="15.4">la</w> <w n="15.5">guirlande</w> <w n="15.6">lyrique</w></l>
							<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">danser</w> <w n="16.3">au</w> <w n="16.4">soleil</w> <w n="16.5">parmi</w> <w n="16.6">les</w> <w n="16.7">gazons</w> <w n="16.8">verts</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">sais</w> <w n="17.3">épanouir</w> <w n="17.4">les</w> <w n="17.5">odes</w> <w n="17.6">amoureuses</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Charmant</w> <w n="18.2">avec</w> <w n="18.3">mes</w> <w n="18.4">sœurs</w> <w n="18.5">les</w> <w n="18.6">bois</w> <w n="18.7">extasiés</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">j</w>’<w n="19.3">accorde</w> <w n="19.4">ma</w> <w n="19.5">voix</w>, <w n="19.6">sous</w> <w n="19.7">les</w> <w n="19.8">forêts</w> <w n="19.9">ombreuses</w>,</l>
							<l n="20" num="5.4"><w n="20.1">Avec</w> <w n="20.2">les</w> <w n="20.3">rossignols</w> <w n="20.4">cachés</w> <w n="20.5">dans</w> <w n="20.6">les</w> <w n="20.7">rosiers</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">je</w> <w n="21.3">tremble</w> <w n="21.4">d</w>’<w n="21.5">oser</w> <w n="21.6">sur</w> <w n="21.7">la</w> <w n="21.8">scène</w> <w n="21.9">divine</w></l>
							<l n="22" num="6.2"><w n="22.1">Où</w> <w n="22.2">le</w> <w n="22.3">maître</w> <w n="22.4">Racine</w> <w n="22.5">a</w> <w n="22.6">fait</w> <w n="22.7">parler</w> <w n="22.8">les</w> <w n="22.9">Dieux</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Vous</w> <w n="23.2">montrer</w> <w n="23.3">après</w> <w n="23.4">lui</w> <w n="23.5">cette</w> <w n="23.6">double</w> <w n="23.7">colline</w></l>
							<l n="24" num="6.4"><w n="24.1">Que</w> <w n="24.2">Phoebos</w> <w n="24.3">emplissait</w> <w n="24.4">de</w> <w n="24.5">chants</w> <w n="24.6">mélodieux</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">J</w>’<w n="25.2">ai</w> <w n="25.3">voulu</w>, <w n="25.4">pauvre</w> <w n="25.5">enfant</w>, <w n="25.6">en</w> <w n="25.7">mes</w> <w n="25.8">jeunes</w> <w n="25.9">délires</w>,</l>
							<l n="26" num="7.2"><w n="26.1">Vous</w> <w n="26.2">faire</w> <w n="26.3">voir</w>, <w n="26.4">parmi</w> <w n="26.5">des</w> <w n="26.6">rayons</w> <w n="26.7">irisés</w>,</l>
							<l n="27" num="7.3"><w n="27.1">La</w> <w n="27.2">sereine</w> <w n="27.3">Lesbos</w> <w n="27.4">où</w> <w n="27.5">dans</w> <w n="27.6">la</w> <w n="27.7">voix</w> <w n="27.8">des</w> <w n="27.9">lyres</w></l>
							<l n="28" num="7.4"><w n="28.1">Se</w> <w n="28.2">confondait</w> <w n="28.3">le</w> <w n="28.4">bruit</w> <w n="28.5">des</w> <w n="28.6">chants</w> <w n="28.7">et</w> <w n="28.8">des</w> <w n="28.9">baisers</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Mais</w> <w n="29.2">je</w> <w n="29.3">tremble</w> <w n="29.4">à</w> <w n="29.5">présent</w>, <w n="29.6">moi</w> <w n="29.7">compagne</w> <w n="29.8">du</w> <w n="29.9">pâtre</w>,</l>
							<l n="30" num="8.2"><w n="30.1">En</w> <w n="30.2">voyant</w> <w n="30.3">mon</w> <w n="30.4">idylle</w> <w n="30.5">et</w> <w n="30.6">mon</w> <w n="30.7">rêve</w> <w n="30.8">enchanteur</w></l>
							<l n="31" num="8.3"><w n="31.1">Fouler</w> <w n="31.2">d</w>’<w n="31.3">un</w> <w n="31.4">pied</w> <w n="31.5">craintif</w> <w n="31.6">ces</w> <w n="31.7">planches</w> <w n="31.8">du</w> <w n="31.9">théâtre</w></l>
							<l n="32" num="8.4"><w n="32.1">Que</w> <w n="32.2">peut</w> <w n="32.3">seul</w> <w n="32.4">animer</w> <w n="32.5">le</w> <w n="32.6">génie</w>, <w n="32.7">et</w> <w n="32.8">j</w>’<w n="32.9">ai</w> <w n="32.10">peur</w>.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">Ah</w> ! <w n="33.2">soyez</w>-<w n="33.3">moi</w> <w n="33.4">cléments</w>, <w n="33.5">rois</w> <w n="33.6">élus</w> <w n="33.7">de</w> <w n="33.8">ces</w> <w n="33.9">fêtes</w>,</l>
							<l n="34" num="9.2"><w n="34.1">Qui</w> <w n="34.2">souriez</w> <w n="34.3">déjà</w> <w n="34.4">rien</w> <w n="34.5">qu</w>’<w n="34.6">en</w> <w n="34.7">me</w> <w n="34.8">regardant</w>,</l>
							<l n="35" num="9.3"><w n="35.1">O</w> <w n="35.2">fronts</w> <w n="35.3">que</w> <w n="35.4">le</w> <w n="35.5">laurier</w> <w n="35.6">couronne</w>, <w n="35.7">ô</w> <w n="35.8">vous</w>, <w n="35.9">poëtes</w></l>
							<l n="36" num="9.4"><w n="36.1">Qui</w> <w n="36.2">marchez</w> <w n="36.3">d</w>’<w n="36.4">un</w> <w n="36.5">pied</w> <w n="36.6">sûr</w> <w n="36.7">dans</w> <w n="36.8">le</w> <w n="36.9">buisson</w> <w n="36.10">ardent</w>.</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">vous</w>, <w n="37.3">reines</w> <w n="37.4">du</w> <w n="37.5">monde</w>, <w n="37.6">ô</w> <w n="37.7">femmes</w> <w n="37.8">adorées</w>,</l>
							<l n="38" num="10.2"><w n="38.1">Déesses</w> <w n="38.2">de</w> <w n="38.3">Paris</w>, <w n="38.4">ô</w> <w n="38.5">fiertés</w> <w n="38.6">et</w> <w n="38.7">douceurs</w>,</l>
							<l n="39" num="10.3"><w n="39.1">Beaux</w> <w n="39.2">yeux</w>, <w n="39.3">boucles</w> <w n="39.4">de</w> <w n="39.5">jais</w>, <w n="39.6">chevelures</w> <w n="39.7">dorées</w>,</l>
							<l n="40" num="10.4"><w n="40.1">Accueillez</w>-<w n="40.2">moi</w>, <w n="40.3">je</w> <w n="40.4">tremble</w>, <w n="40.5">ô</w> <w n="40.6">mes</w> <w n="40.7">divines</w> <w n="40.8">sœurs</w> !</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1"><w n="41.1">Rien</w> <w n="41.2">qu</w>’<w n="41.3">en</w> <w n="41.4">posant</w> <w n="41.5">au</w> <w n="41.6">bord</w> <w n="41.7">des</w> <w n="41.8">fontaines</w> <w n="41.9">limpides</w>,</l>
							<l n="42" num="11.2"><w n="42.1">O</w> <w n="42.2">sœurs</w> <w n="42.3">de</w> <w n="42.4">Galatée</w>, <w n="42.5">ô</w> <w n="42.6">sœurs</w> <w n="42.7">d</w>’<w n="42.8">Amaryllis</w>,</l>
							<l n="43" num="11.3"><w n="43.1">Vos</w> <w n="43.2">pieds</w>, <w n="43.3">vos</w> <w n="43.4">petits</w> <w n="43.5">pieds</w> <w n="43.6">sur</w> <w n="43.7">les</w> <w n="43.8">rochers</w> <w n="43.9">arides</w>,</l>
							<l n="44" num="11.4"><w n="44.1">Vous</w> <w n="44.2">y</w> <w n="44.3">faites</w> <w n="44.4">fleurir</w> <w n="44.5">des</w> <w n="44.6">roses</w> <w n="44.7">et</w> <w n="44.8">des</w> <w n="44.9">lys</w>.</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1"><w n="45.1">O</w> <w n="45.2">vous</w>, <w n="45.3">troupe</w> <w n="45.4">charmante</w> <w n="45.5">avec</w> <w n="45.6">amour</w> <w n="45.7">chantée</w>,</l>
							<l n="46" num="12.2"><w n="46.1">Si</w> <w n="46.2">vous</w> <w n="46.3">voulez</w>, <w n="46.4">orgueil</w> <w n="46.5">de</w> <w n="46.6">mes</w> <w n="46.7">vers</w> <w n="46.8">ciselés</w>,</l>
							<l n="47" num="12.3"><w n="47.1">L</w>’<w n="47.2">outremer</w> <w n="47.3">brillera</w> <w n="47.4">sur</w> <w n="47.5">ma</w> <w n="47.6">toile</w> <w n="47.7">enchantée</w></l>
							<l n="48" num="12.4"><w n="48.1">Et</w> <w n="48.2">ma</w> <w n="48.3">pauvre</w> <w n="48.4">Lesbos</w> <w n="48.5">vivra</w>, <w n="48.6">si</w> <w n="48.7">vous</w> <w n="48.8">voulez</w>.</l>
						</lg>
						<lg n="13">
							<l n="49" num="13.1"><w n="49.1">Si</w> <w n="49.2">vous</w> <w n="49.3">voulez</w>, <w n="49.4">mes</w> <w n="49.5">sœurs</w>, <w n="49.6">votre</w> <w n="49.7">fière</w> <w n="49.8">jeunesse</w></l>
							<l n="50" num="13.2"><w n="50.1">Fera</w> <w n="50.2">vivre</w> <w n="50.3">un</w> <w n="50.4">moment</w> <w n="50.5">dans</w> <w n="50.6">un</w> <w n="50.7">rêve</w> <w n="50.8">fleuri</w></l>
							<l n="51" num="13.3"><w n="51.1">Ma</w> <w n="51.2">jeunesse</w> <w n="51.3">impuissante</w>, <w n="51.4">et</w> <w n="51.5">j</w>’<w n="51.6">aurai</w> <w n="51.7">trop</w> <w n="51.8">d</w>’<w n="51.9">ivresse</w></l>
							<l n="52" num="13.4"><w n="52.1">Si</w> <w n="52.2">vous</w> <w n="52.3">avez</w> <w n="52.4">pleuré</w>, <w n="52.5">si</w> <w n="52.6">vous</w> <w n="52.7">avez</w> <w n="52.8">souri</w> !</l>
						</lg>
						<closer>
							<dateline>
								<placeName>Odéon</placeName>,
								<date when="1850">12 novembre 1850.</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>