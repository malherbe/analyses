<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN345">
				<head type="main">Amazone Nue</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Amazone</w> <w n="1.2">aux</w> <w n="1.3">reins</w> <w n="1.4">forts</w>, <w n="1.5">solide</w> <w n="1.6">centauresse</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">tiens</w> <w n="2.3">par</w> <w n="2.4">les</w> <w n="2.5">cheveux</w>, <w n="2.6">sans</w> <w n="2.7">mors</w> <w n="2.8">et</w> <w n="2.9">sans</w> <w n="2.10">lien</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ton</w> <w n="3.2">cheval</w> <w n="3.3">de</w> <w n="3.4">Titan</w>, <w n="3.5">monstre</w> <w n="3.6">thessalien</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Ta</w> <w n="4.2">cuisse</w> <w n="4.3">avec</w> <w n="4.4">fureur</w> <w n="4.5">le</w> <w n="4.6">dompte</w> <w n="4.7">et</w> <w n="4.8">le</w> <w n="4.9">caresse</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">On</w> <w n="5.2">voit</w> <w n="5.3">voler</w> <w n="5.4">au</w> <w n="5.5">vent</w> <w n="5.6">sa</w> <w n="5.7">crinière</w> <w n="5.8">et</w> <w n="5.9">ta</w> <w n="5.10">tresse</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">superbe</w> <w n="6.3">coursier</w> <w n="6.4">t</w>’<w n="6.5">obéit</w> <w n="6.6">comme</w> <w n="6.7">un</w> <w n="6.8">chien</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">rien</w> <w n="7.3">n</w>’<w n="7.4">arrêterait</w> <w n="7.5">dans</w> <w n="7.6">son</w> <w n="7.7">calme</w> <w n="7.8">païen</w></l>
					<l n="8" num="2.4"><w n="8.1">Ton</w> <w n="8.2">corps</w>, <w n="8.3">bâti</w> <w n="8.4">de</w> <w n="8.5">rocs</w> <w n="8.6">comme</w> <w n="8.7">une</w> <w n="8.8">forteresse</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Franchissant</w> <w n="9.2">d</w>’<w n="9.3">un</w> <w n="9.4">seul</w> <w n="9.5">bond</w> <w n="9.6">les</w> <w n="9.7">antres</w> <w n="9.8">effrayés</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Vous</w> <w n="10.2">frappez</w> <w n="10.3">du</w> <w n="10.4">sabot</w>, <w n="10.5">dans</w> <w n="10.6">les</w> <w n="10.7">bois</w> <w n="10.8">non</w> <w n="10.9">frayés</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">pâtres</w> <w n="11.3">chevelus</w> <w n="11.4">et</w> <w n="11.5">les</w> <w n="11.6">troupeaux</w> <w n="11.7">qui</w> <w n="11.8">bêlent</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Toi</w>, <w n="12.2">Nymphe</w>, <w n="12.3">sans</w> <w n="12.4">tunique</w>, <w n="12.5">et</w> <w n="12.6">ton</w> <w n="12.7">cheval</w> <w n="12.8">sans</w> <w n="12.9">mors</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Vos</w> <w n="13.2">flancs</w> <w n="13.3">restent</w> <w n="13.4">collés</w> <w n="13.5">et</w> <w n="13.6">vos</w> <w n="13.7">croupes</w> <w n="13.8">se</w> <w n="13.9">mêlent</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Solide</w> <w n="14.2">centauresse</w>, <w n="14.3">amazone</w> <w n="14.4">aux</w> <w n="14.5">reins</w> <w n="14.6">forts</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1847">Octobre 1847.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>