<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN343">
				<head type="main">Louanges d’Aurélie</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Toi</w> <w n="1.2">qui</w> <w n="1.3">rêvas</w> <w n="1.4">parmi</w> <w n="1.5">les</w> <w n="1.6">lys</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">le</w> <w n="2.3">sylphe</w> <w n="2.4">et</w> <w n="2.5">les</w> <w n="2.6">willis</w></l>
					<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">Pour</w> <w n="3.2">coryphées</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">la</w> <w n="4.3">rosée</w> <w n="4.4">en</w> <w n="4.5">diamants</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Un</w> <w n="5.2">théâtre</w> <w n="5.3">pour</w> <w n="5.4">les</w> <w n="5.5">amants</w></l>
					<l n="6" num="1.6"><space quantity="8" unit="char"></space><w n="6.1">Et</w> <w n="6.2">pour</w> <w n="6.3">les</w> <w n="6.4">fées</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Je</w> <w n="7.2">sais</w>, <w n="7.3">poëte</w> <w n="7.4">du</w> <w n="7.5">roi</w> <w n="7.6">Lear</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Une</w> <w n="8.2">femme</w> <w n="8.3">qui</w> <w n="8.4">fait</w> <w n="8.5">pâlir</w></l>
					<l n="9" num="2.3"><space quantity="8" unit="char"></space><w n="9.1">Toutes</w> <w n="9.2">les</w> <w n="9.3">flammes</w></l>
					<l n="10" num="2.4"><w n="10.1">Dont</w> <w n="10.2">ta</w> <w n="10.3">noble</w> <w n="10.4">main</w> <w n="10.5">couronna</w></l>
					<l n="11" num="2.5"><w n="11.1">Juliette</w> <w n="11.2">et</w> <w n="11.3">Desdémona</w>,</l>
					<l n="12" num="2.6"><space quantity="8" unit="char"></space><w n="12.1">Ces</w> <w n="12.2">blanches</w> <w n="12.3">âmes</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Elle</w> <w n="13.2">avait</w> <w n="13.3">au</w> <w n="13.4">front</w> <w n="13.5">moins</w> <w n="13.6">de</w> <w n="13.7">fleurs</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Celle</w> <w n="14.2">que</w>, <w n="14.3">d</w>’<w n="14.4">amour</w> <w n="14.5">et</w> <w n="14.6">de</w> <w n="14.7">pleurs</w></l>
					<l n="15" num="3.3"><space quantity="8" unit="char"></space><w n="15.1">Tout</w> <w n="15.2">arrosée</w>,</l>
					<l n="16" num="3.4"><w n="16.1">La</w> <w n="16.2">lune</w> <w n="16.3">rêveuse</w>, <w n="16.4">en</w> <w n="16.5">songeant</w>,</l>
					<l n="17" num="3.5"><w n="17.1">Couronnait</w> <w n="17.2">de</w> <w n="17.3">rayons</w> <w n="17.4">d</w>’<w n="17.5">argent</w></l>
					<l n="18" num="3.6"><space quantity="8" unit="char"></space><w n="18.1">Et</w> <w n="18.2">de</w> <w n="18.3">rosée</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Elle</w> <w n="19.2">avait</w> <w n="19.3">moins</w> <w n="19.4">de</w> <w n="19.5">doux</w> <w n="19.6">regards</w>,</l>
					<l n="20" num="4.2"><w n="20.1">Celle</w> <w n="20.2">qui</w>, <w n="20.3">les</w> <w n="20.4">cheveux</w> <w n="20.5">épars</w></l>
					<l n="21" num="4.3"><space quantity="8" unit="char"></space><w n="21.1">Sur</w> <w n="21.2">son</w> <w n="21.3">épaule</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Blanche</w> <w n="22.2">comme</w> <w n="22.3">un</w> <w n="22.4">camellia</w>,</l>
					<l n="23" num="4.5"><w n="23.1">A</w> <w n="23.2">sa</w> <w n="23.3">servante</w> <w n="23.4">Émilia</w></l>
					<l n="24" num="4.6"><space quantity="8" unit="char"></space><w n="24.1">Chantait</w> <w n="24.2">le</w> <w n="24.3">Saule</w> !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Il</w> <w n="25.2">est</w> <w n="25.3">moins</w> <w n="25.4">agréable</w> <w n="25.5">au</w> <w n="25.6">ciel</w>,</l>
					<l n="26" num="5.2"><w n="26.1">Cet</w> <w n="26.2">ange</w> <w n="26.3">qu</w>’<w n="26.4">un</w> <w n="26.5">chant</w> <w n="26.6">immortel</w></l>
					<l n="27" num="5.3"><space quantity="8" unit="char"></space><w n="27.1">Toujours</w> <w n="27.2">caresse</w>,</l>
					<l n="28" num="5.4"><w n="28.1">Cet</w> <w n="28.2">inestimable</w> <w n="28.3">joyau</w></l>
					<l n="29" num="5.5"><w n="29.1">Sur</w> <w n="29.2">lequel</w> <w n="29.3">pleure</w> <w n="29.4">Olympio</w></l>
					<l n="30" num="5.6"><space quantity="8" unit="char"></space><w n="30.1">Dans</w> <w n="30.2">sa</w> <w n="30.3">tristesse</w> !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Et</w> <w n="31.2">toi</w>, <w n="31.3">mon</w> <w n="31.4">maître</w>, <w n="31.5">ô</w> <w n="31.6">fier</w> <w n="31.7">Ronsard</w>,</l>
					<l n="32" num="6.2"><w n="32.1">Enthousiaste</w> <w n="32.2">du</w> <w n="32.3">doux</w> <w n="32.4">art</w>,</l>
					<l n="33" num="6.3"><space quantity="8" unit="char"></space><w n="33.1">Amant</w> <w n="33.2">d</w>’<w n="33.3">Hélène</w>,</l>
					<l n="34" num="6.4"><w n="34.1">Qui</w> <w n="34.2">jadis</w> <w n="34.3">nous</w> <w n="34.4">émerveillais</w></l>
					<l n="35" num="6.5"><w n="35.1">Sur</w> <w n="35.2">les</w> <w n="35.3">roses</w> <w n="35.4">et</w> <w n="35.5">les</w> <w n="35.6">œillets</w></l>
					<l n="36" num="6.6"><space quantity="8" unit="char"></space><w n="36.1">De</w> <w n="36.2">son</w> <w n="36.3">haleine</w> !</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Celle</w> <w n="37.2">que</w> <w n="37.3">je</w> <w n="37.4">chante</w> <w n="37.5">en</w> <w n="37.6">ces</w> <w n="37.7">vers</w></l>
					<l n="38" num="7.2"><w n="38.1">T</w>’<w n="38.2">eût</w> <w n="38.3">donné</w>, <w n="38.4">sous</w> <w n="38.5">tes</w> <w n="38.6">lauriers</w> <w n="38.7">verts</w>,</l>
					<l n="39" num="7.3"><space quantity="8" unit="char"></space><w n="39.1">Plus</w> <w n="39.2">de</w> <w n="39.3">délire</w></l>
					<l n="40" num="7.4"><w n="40.1">Qu</w>’<w n="40.2">il</w> <w n="40.3">n</w>’<w n="40.4">en</w> <w n="40.5">fallut</w> <w n="40.6">pour</w> <w n="40.7">mettre</w> <w n="40.8">au</w> <w n="40.9">jour</w></l>
					<l n="41" num="7.5"><w n="41.1">Les</w> <w n="41.2">cent</w> <w n="41.3">filles</w> <w n="41.4">de</w> <w n="41.5">ton</w> <w n="41.6">amour</w></l>
					<l n="42" num="7.6"><space quantity="8" unit="char"></space><w n="42.1">Et</w> <w n="42.2">de</w> <w n="42.3">ta</w> <w n="42.4">lyre</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Car</w> <w n="43.2">cette</w> <w n="43.3">maîtresse</w> <w n="43.4">aux</w> <w n="43.5">beaux</w> <w n="43.6">yeux</w></l>
					<l n="44" num="8.2"><w n="44.1">Dans</w> <w n="44.2">un</w> <w n="44.3">poëme</w> <w n="44.4">harmonieux</w></l>
					<l n="45" num="8.3"><space quantity="8" unit="char"></space><w n="45.1">N</w>’<w n="45.2">est</w> <w n="45.3">pas</w> <w n="45.4">éclose</w>,</l>
					<l n="46" num="8.4"><w n="46.1">Ni</w> <w n="46.2">dans</w> <w n="46.3">ton</w> <w n="46.4">marbre</w>, <w n="46.5">ô</w> <w n="46.6">Phidias</w>,</l>
					<l n="47" num="8.5"><w n="47.1">Ni</w> <w n="47.2">dans</w> <w n="47.3">les</w> <w n="47.4">grands</w> <w n="47.5">yeux</w> <w n="47.6">de</w> <w n="47.7">Diaz</w></l>
					<l n="48" num="8.6"><space quantity="8" unit="char"></space><w n="48.1">Ivres</w> <w n="48.2">de</w> <w n="48.3">rose</w> !</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">C</w>’<w n="49.2">est</w> <w n="49.3">une</w> <w n="49.4">femme</w> <w n="49.5">aux</w> <w n="49.6">yeux</w> <w n="49.7">plus</w> <w n="49.8">doux</w>,</l>
					<l n="50" num="9.2"><w n="50.1">Vivante</w> <w n="50.2">et</w> <w n="50.3">qui</w> <w n="50.4">peut</w>, <w n="50.5">comme</w> <w n="50.6">nous</w>,</l>
					<l n="51" num="9.3"><space quantity="8" unit="char"></space><w n="51.1">Dire</w> : <w n="51.2">Je</w> <w n="51.3">t</w>’<w n="51.4">aime</w>,</l>
					<l n="52" num="9.4"><w n="52.1">Mais</w> <w n="52.2">qui</w> <w n="52.3">sur</w> <w n="52.4">son</w> <w n="52.5">front</w> <w n="52.6">sidéral</w></l>
					<l n="53" num="9.5"><w n="53.1">Porte</w> <w n="53.2">le</w> <w n="53.3">rhythme</w> <w n="53.4">et</w> <w n="53.5">l</w>’<w n="53.6">idéal</w></l>
					<l n="54" num="9.6"><space quantity="8" unit="char"></space><w n="54.1">Comme</w> <w n="54.2">un</w> <w n="54.3">poëme</w>.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1"><w n="55.1">Ce</w> <w n="55.2">n</w>’<w n="55.3">est</w> <w n="55.4">pas</w> <w n="55.5">un</w> <w n="55.6">rêve</w> <w n="55.7">charmant</w></l>
					<l n="56" num="10.2"><w n="56.1">Qu</w>’<w n="56.2">il</w> <w n="56.3">faudra</w> <w n="56.4">pleurer</w> <w n="56.5">en</w> <w n="56.6">fermant</w></l>
					<l n="57" num="10.3"><space quantity="8" unit="char"></space><w n="57.1">Quelque</w> <w n="57.2">cher</w> <w n="57.3">livre</w>,</l>
					<l n="58" num="10.4"><w n="58.1">Et</w> <w n="58.2">cet</w> <w n="58.3">ange</w> <w n="58.4">aux</w> <w n="58.5">ongles</w> <w n="58.6">d</w>’<w n="58.7">onyx</w>,</l>
					<l n="59" num="10.5"><w n="59.1">Plus</w> <w n="59.2">beau</w> <w n="59.3">que</w> <w n="59.4">Laure</w> <w n="59.5">et</w> <w n="59.6">Béatrix</w>,</l>
					<l n="60" num="10.6"><space quantity="8" unit="char"></space><w n="60.1">On</w> <w n="60.2">le</w> <w n="60.3">sent</w> <w n="60.4">vivre</w> !</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1"><w n="61.1">On</w> <w n="61.2">entend</w>, <w n="61.3">parmi</w> <w n="61.4">le</w> <w n="61.5">satin</w>,</l>
					<l n="62" num="11.2"><w n="62.1">Battre</w> <w n="62.2">son</w> <w n="62.3">cœur</w> <w n="62.4">sous</w> <w n="62.5">son</w> <w n="62.6">beau</w> <w n="62.7">sein</w></l>
					<l n="63" num="11.3"><space quantity="8" unit="char"></space><w n="63.1">Dans</w> <w n="63.2">sa</w> <w n="63.3">poitrine</w>,</l>
					<l n="64" num="11.4"><w n="64.1">Les</w> <w n="64.2">rossignols</w>, <w n="64.3">pleins</w> <w n="64.4">de</w> <w n="64.5">doux</w> <w n="64.6">chants</w>,</l>
					<l n="65" num="11.5"><w n="65.1">Peuvent</w> <w n="65.2">écouter</w> <w n="65.3">dans</w> <w n="65.4">les</w> <w n="65.5">champs</w></l>
					<l n="66" num="11.6"><space quantity="8" unit="char"></space><w n="66.1">Sa</w> <w n="66.2">voix</w> <w n="66.3">divine</w>,</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1"><w n="67.1">Et</w> <w n="67.2">quand</w> <w n="67.3">elle</w> <w n="67.4">s</w>’<w n="67.5">arrête</w> <w n="67.6">au</w> <w n="67.7">bois</w></l>
					<l n="68" num="12.2"><w n="68.1">Pour</w> <w n="68.2">écouter</w> <w n="68.3">sourdre</w> <w n="68.4">les</w> <w n="68.5">voix</w></l>
					<l n="69" num="12.3"><space quantity="8" unit="char"></space><w n="69.1">De</w> <w n="69.2">la</w> <w n="69.3">nature</w>,</l>
					<l n="70" num="12.4"><w n="70.1">A</w> <w n="70.2">travers</w> <w n="70.3">les</w> <w n="70.4">arbres</w> <w n="70.5">du</w> <w n="70.6">parc</w>,</l>
					<l n="71" num="12.5"><w n="71.1">Les</w> <w n="71.2">Naïades</w> <w n="71.3">admirent</w> <w n="71.4">l</w>’<w n="71.5">arc</w></l>
					<l n="72" num="12.6"><space quantity="8" unit="char"></space><w n="72.1">De</w> <w n="72.2">sa</w> <w n="72.3">ceinture</w> !</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1"><w n="73.1">Le</w> <w n="73.2">soir</w>, <w n="73.3">à</w> <w n="73.4">cette</w> <w n="73.5">heure</w> <w n="73.6">de</w> <w n="73.7">feu</w></l>
					<l n="74" num="13.2"><w n="74.1">Où</w> <w n="74.2">se</w> <w n="74.3">pâme</w> <w n="74.4">sous</w> <w n="74.5">le</w> <w n="74.6">ciel</w> <w n="74.7">bleu</w></l>
					<l n="75" num="13.3"><space quantity="12" unit="char"></space><w n="75.1">La</w> <w n="75.2">tubéreuse</w>,</l>
					<l n="76" num="13.4"><w n="76.1">La</w> <w n="76.2">Nuit</w> <w n="76.3">humide</w> <w n="76.4">de</w> <w n="76.5">parfums</w></l>
					<l n="77" num="13.5"><w n="77.1">Se</w> <w n="77.2">mire</w> <w n="77.3">dans</w> <w n="77.4">ses</w> <w n="77.5">grands</w> <w n="77.6">yeux</w> <w n="77.7">bruns</w>,</l>
					<l n="78" num="13.6"><space quantity="8" unit="char"></space><w n="78.1">Tout</w> <w n="78.2">amoureuse</w> ;</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1"><w n="79.1">Et</w> <w n="79.2">les</w> <w n="79.3">extases</w> <w n="79.4">du</w> <w n="79.5">soleil</w></l>
					<l n="80" num="14.2"><w n="80.1">Emplissent</w> <w n="80.2">les</w> <w n="80.3">airs</w> <w n="80.4">d</w>’<w n="80.5">or</w> <w n="80.6">vermeil</w></l>
					<l n="81" num="14.3"><space quantity="8" unit="char"></space><w n="81.1">Et</w> <w n="81.2">d</w>’<w n="81.3">harmonies</w>,</l>
					<l n="82" num="14.4"><w n="82.1">Quand</w> <w n="82.2">les</w> <w n="82.3">beaux</w> <w n="82.4">châles</w> <w n="82.5">d</w>’<w n="82.6">Orient</w></l>
					<l n="83" num="14.5"><w n="83.1">Murmurent</w> <w n="83.2">sur</w> <w n="83.3">son</w> <w n="83.4">cou</w> <w n="83.5">riant</w></l>
					<l n="84" num="14.6"><space quantity="8" unit="char"></space><w n="84.1">Leurs</w> <w n="84.2">symphonies</w> !</l>
				</lg>
				<lg n="15">
					<l n="85" num="15.1"><w n="85.1">Car</w> <w n="85.2">c</w>’<w n="85.3">est</w> <w n="85.4">pour</w> <w n="85.5">orner</w> <w n="85.6">ses</w> <w n="85.7">beaux</w> <w n="85.8">reins</w></l>
					<l n="86" num="15.2"><w n="86.1">Que</w> <w n="86.2">le</w> <w n="86.3">pays</w> <w n="86.4">des</w> <w n="86.5">Dieux</w> <w n="86.6">sereins</w></l>
					<l n="87" num="15.3"><space quantity="8" unit="char"></space><w n="87.1">Aux</w> <w n="87.2">mains</w> <w n="87.3">fleuries</w></l>
					<l n="88" num="15.4"><w n="88.1">Semble</w> <w n="88.2">dans</w> <w n="88.3">un</w> <w n="88.4">tissu</w> <w n="88.5">changeant</w></l>
					<l n="89" num="15.5"><w n="89.1">Tramer</w> <w n="89.2">avec</w> <w n="89.3">l</w>’<w n="89.4">or</w> <w n="89.5">et</w> <w n="89.6">l</w>’<w n="89.7">argent</w></l>
					<l n="90" num="15.6"><space quantity="8" unit="char"></space><w n="90.1">Les</w> <w n="90.2">pierreries</w> !</l>
				</lg>
				<lg n="16">
					<l n="91" num="16.1"><w n="91.1">O</w> <w n="91.2">beau</w> <w n="91.3">songe</w> ! <w n="91.4">sonnet</w> <w n="91.5">vivant</w> !</l>
					<l n="92" num="16.2"><w n="92.1">Calice</w> <w n="92.2">entr</w>’<w n="92.3">ouvert</w> <w n="92.4">que</w> <w n="92.5">le</w> <w n="92.6">vent</w></l>
					<l n="93" num="16.3"><space quantity="8" unit="char"></space><w n="93.1">Jamais</w> <w n="93.2">ne</w> <w n="93.3">fane</w> !</l>
					<l n="94" num="16.4"><w n="94.1">Sa</w> <w n="94.2">main</w> <w n="94.3">blanche</w> <w n="94.4">comme</w> <w n="94.5">le</w> <w n="94.6">lait</w></l>
					<l n="95" num="16.5"><w n="95.1">Passe</w> <w n="95.2">à</w> <w n="95.3">travers</w> <w n="95.4">le</w> <w n="95.5">bracelet</w></l>
					<l n="96" num="16.6"><space quantity="8" unit="char"></space><w n="96.1">D</w>’<w n="96.2">une</w> <w n="96.3">sultane</w> !</l>
				</lg>
				<lg n="17">
					<l n="97" num="17.1"><w n="97.1">Je</w> <w n="97.2">vois</w> <w n="97.3">sous</w> <w n="97.4">les</w> <w n="97.5">pâles</w> <w n="97.6">duvets</w></l>
					<l n="98" num="17.2"><w n="98.1">Ses</w> <w n="98.2">veines</w> <w n="98.3">couleur</w> <w n="98.4">des</w> <w n="98.5">bleuets</w></l>
					<l n="99" num="17.3"><space quantity="8" unit="char"></space><w n="99.1">Et</w> <w n="99.2">des</w> <w n="99.3">pervenches</w>,</l>
					<l n="100" num="17.4"><w n="100.1">Ses</w> <w n="100.2">ongles</w> <w n="100.3">dignes</w> <w n="100.4">de</w> <w n="100.5">Scyllis</w>,</l>
					<l n="101" num="17.5"><w n="101.1">Ses</w> <w n="101.2">bras</w> <w n="101.3">aussi</w> <w n="101.4">blancs</w> <w n="101.5">que</w> <w n="101.6">les</w> <w n="101.7">lys</w>,</l>
					<l n="102" num="17.6"><space quantity="8" unit="char"></space><w n="102.1">Ses</w> <w n="102.2">mains</w> <w n="102.3">plus</w> <w n="102.4">blanches</w> !</l>
				</lg>
				<lg n="18">
					<l n="103" num="18.1"><w n="103.1">Et</w> <w n="103.2">mon</w> <w n="103.3">âme</w> <w n="103.4">pleine</w> <w n="103.5">et</w> <w n="103.6">sans</w> <w n="103.7">fond</w>,</l>
					<l n="104" num="18.2"><w n="104.1">D</w>’<w n="104.2">où</w> <w n="104.3">parfois</w> <w n="104.4">à</w> <w n="104.5">mon</w> <w n="104.6">œil</w> <w n="104.7">profond</w></l>
					<l n="105" num="18.3"><space quantity="8" unit="char"></space><w n="105.1">Monte</w> <w n="105.2">une</w> <w n="105.3">larme</w>,</l>
					<l n="106" num="18.4"><w n="106.1">Partout</w> <w n="106.2">attirée</w> <w n="106.3">à</w> <w n="106.4">la</w> <w n="106.5">fois</w>,</l>
					<l n="107" num="18.5"><w n="107.1">Demeure</w> <w n="107.2">tremblante</w> <w n="107.3">et</w> <w n="107.4">sans</w> <w n="107.5">voix</w></l>
					<l n="108" num="18.6"><space quantity="8" unit="char"></space><w n="108.1">Sous</w> <w n="108.2">tout</w> <w n="108.3">ce</w> <w n="108.4">charme</w> !</l>
				</lg>
				<lg n="19">
					<l n="109" num="19.1"><w n="109.1">Tels</w> <w n="109.2">nous</w> <w n="109.3">sentons</w>, <w n="109.4">irrésolus</w>,</l>
					<l n="110" num="19.2"><w n="110.1">De</w> <w n="110.2">vivants</w> <w n="110.3">désirs</w>, <w n="110.4">qui</w> <w n="110.5">n</w>’<w n="110.6">ont</w> <w n="110.7">plus</w></l>
					<l n="111" num="19.3"><space quantity="8" unit="char"></space><w n="111.1">Rien</w> <w n="111.2">de</w> <w n="111.3">physique</w>,</l>
					<l n="112" num="19.4"><w n="112.1">Couler</w> <w n="112.2">en</w> <w n="112.3">nous</w> <w n="112.4">comme</w> <w n="112.5">des</w> <w n="112.6">flots</w></l>
					<l n="113" num="19.5"><w n="113.1">Avec</w> <w n="113.2">le</w> <w n="113.3">rhythme</w> <w n="113.4">et</w> <w n="113.5">les</w> <w n="113.6">sanglots</w></l>
					<l n="114" num="19.6"><space quantity="8" unit="char"></space><w n="114.1">De</w> <w n="114.2">la</w> <w n="114.3">musique</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1846">Mai 1846.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>