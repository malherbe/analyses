<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN340">
				<head type="main">L’Invincible</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ris</w> <w n="1.2">sous</w> <w n="1.3">la</w> <w n="1.4">griffe</w> <w n="1.5">des</w> <w n="1.6">vautours</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Cœur</w> <w n="2.2">meurtri</w>, <w n="2.3">que</w> <w n="2.4">leur</w> <w n="2.5">bec</w> <w n="2.6">entame</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Vas</w>-<w n="3.2">tu</w> <w n="3.3">te</w> <w n="3.4">plaindre</w> <w n="3.5">d</w>’<w n="3.6">une</w> <w n="3.7">femme</w> ?</l>
					<l n="4" num="1.4"><w n="4.1">Non</w> ! <w n="4.2">je</w> <w n="4.3">veux</w> <w n="4.4">boire</w> <w n="4.5">à</w> <w n="4.6">ses</w> <w n="4.7">amours</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">boirai</w> <w n="5.3">le</w> <w n="5.4">vin</w> <w n="5.5">et</w> <w n="5.6">la</w> <w n="5.7">lie</w>,</l>
					<l n="6" num="1.6"><w n="6.1">O</w> <w n="6.2">Furie</w> <w n="6.3">aux</w> <w n="6.4">cheveux</w> <w n="6.5">flottants</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Pour</w> <w n="7.2">mieux</w> <w n="7.3">pouvoir</w> <w n="7.4">en</w> <w n="7.5">même</w> <w n="7.6">temps</w></l>
					<l n="8" num="1.8"><w n="8.1">Trouver</w> <w n="8.2">la</w> <w n="8.3">haine</w> <w n="8.4">et</w> <w n="8.5">la</w> <w n="8.6">folie</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Dans</w> <w n="9.2">mon</w> <w n="9.3">verre</w> <w n="9.4">entouré</w> <w n="9.5">de</w> <w n="9.6">fleurs</w></l>
					<l n="10" num="2.2"><w n="10.1">S</w>’<w n="10.2">il</w> <w n="10.3">tombe</w> <w n="10.4">une</w> <w n="10.5">larme</w> <w n="10.6">brûlante</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Rassurez</w> <w n="11.2">ma</w> <w n="11.3">main</w> <w n="11.4">chancelante</w>,</l>
					<l n="12" num="2.4"><w n="12.1">Et</w> <w n="12.2">faites</w>-<w n="12.3">moi</w> <w n="12.4">boire</w> <w n="12.5">mes</w> <w n="12.6">pleurs</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Assez</w> <w n="13.2">de</w> <w n="13.3">plaintes</w> <w n="13.4">sérieuses</w></l>
					<l n="14" num="2.6"><w n="14.1">Quand</w> <w n="14.2">le</w> <w n="14.3">bourgogne</w> <w n="14.4">a</w> <w n="14.5">ruisselé</w>,</l>
					<l n="15" num="2.7"><w n="15.1">Sang</w> <w n="15.2">vermeil</w> <w n="15.3">du</w> <w n="15.4">raisin</w> <w n="15.5">foulé</w></l>
					<l n="16" num="2.8"><w n="16.1">Par</w> <w n="16.2">des</w> <w n="16.3">Bacchantes</w> <w n="16.4">furieuses</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Pour</w> <w n="17.2">former</w> <w n="17.3">la</w> <w n="17.4">chaude</w> <w n="17.5">liqueur</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Elles</w> <w n="18.2">n</w>’<w n="18.3">ont</w> <w n="18.4">pas</w>, <w n="18.5">dans</w> <w n="18.6">leurs</w> <w n="18.7">victoires</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Déchiré</w> <w n="19.2">mieux</w> <w n="19.3">les</w> <w n="19.4">grappes</w> <w n="19.5">noires</w></l>
					<l n="20" num="3.4"><w n="20.1">Qu</w>’<w n="20.2">elle</w> <w n="20.3">n</w>’<w n="20.4">a</w> <w n="20.5">déchiré</w> <w n="20.6">mon</w> <w n="20.7">cœur</w>.</l>
					<l n="21" num="3.5"><w n="21.1">Amis</w>, <w n="21.2">vous</w> <w n="21.3">qui</w> <w n="21.4">buvez</w> <w n="21.5">en</w> <w n="21.6">foule</w></l>
					<l n="22" num="3.6"><w n="22.1">Le</w> <w n="22.2">poison</w> <w n="22.3">de</w> <w n="22.4">l</w>’<w n="22.5">amour</w> <w n="22.6">jaloux</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Mon</w> <w n="23.2">cœur</w> <w n="23.3">se</w> <w n="23.4">brise</w> ; <w n="23.5">enivrez</w>-<w n="23.6">vous</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Puisque</w> <w n="24.2">la</w> <w n="24.3">poésie</w> <w n="24.4">en</w> <w n="24.5">coule</w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">C</w>’<w n="25.2">est</w> <w n="25.3">dans</w> <w n="25.4">ce</w> <w n="25.5">calice</w> <w n="25.6">profond</w></l>
					<l n="26" num="4.2"><w n="26.1">Que</w> <w n="26.2">l</w>’<w n="26.3">infidèle</w> <w n="26.4">aimait</w> <w n="26.5">à</w> <w n="26.6">boire</w> :</l>
					<l n="27" num="4.3"><w n="27.1">Puisque</w> <w n="27.2">au</w> <w n="27.3">fond</w> <w n="27.4">reste</w> <w n="27.5">sa</w> <w n="27.6">mémoire</w>,</l>
					<l n="28" num="4.4"><w n="28.1">Noble</w> <w n="28.2">vin</w>, <w n="28.3">cache</w>-<w n="28.4">m</w>’<w n="28.5">en</w> <w n="28.6">le</w> <w n="28.7">fond</w> !</l>
					<l n="29" num="4.5"><w n="29.1">J</w>’<w n="29.2">y</w> <w n="29.3">jetterai</w> <w n="29.4">les</w> <w n="29.5">rêveries</w></l>
					<l n="30" num="4.6"><w n="30.1">Et</w> <w n="30.2">l</w>’<w n="30.3">amour</w> <w n="30.4">que</w> <w n="30.5">j</w>’<w n="30.6">avais</w> <w n="30.7">jadis</w>,</l>
					<l n="31" num="4.7"><w n="31.1">Comme</w> <w n="31.2">autrefois</w> <w n="31.3">ses</w> <w n="31.4">mains</w> <w n="31.5">de</w> <w n="31.6">lys</w></l>
					<l n="32" num="4.8"><w n="32.1">Y</w> <w n="32.2">jetaient</w> <w n="32.3">des</w> <w n="32.4">roses</w> <w n="32.5">fleuries</w> !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Et</w> <w n="33.2">vous</w>, <w n="33.3">mes</w> <w n="33.4">yeux</w>, <w n="33.5">que</w> <w n="33.6">pour</w> <w n="33.7">miroir</w></l>
					<l n="34" num="5.2"><w n="34.1">Prenait</w> <w n="34.2">cette</w> <w n="34.3">ingrate</w> <w n="34.4">maîtresse</w>,</l>
					<l n="35" num="5.3"><w n="35.1">Extasiez</w>-<w n="35.2">vous</w> <w n="35.3">dans</w> <w n="35.4">l</w>’<w n="35.5">ivresse</w></l>
					<l n="36" num="5.4"><w n="36.1">Pour</w> <w n="36.2">lui</w> <w n="36.3">cacher</w> <w n="36.4">mon</w> <w n="36.5">désespoir</w>.</l>
					<l n="37" num="5.5"><w n="37.1">Ces</w> <w n="37.2">lèvres</w>, <w n="37.3">qu</w>’<w n="37.4">elle</w> <w n="37.5">a</w> <w n="37.6">tant</w> <w n="37.7">baisées</w>,</l>
					<l n="38" num="5.6"><w n="38.1">Me</w> <w n="38.2">trahiraient</w> <w n="38.3">par</w> <w n="38.4">leur</w> <w n="38.5">pâleur</w> ;</l>
					<l n="39" num="5.7"><w n="39.1">Je</w> <w n="39.2">vais</w> <w n="39.3">leur</w> <w n="39.4">rendre</w> <w n="39.5">leur</w> <w n="39.6">couleur</w></l>
					<l n="40" num="5.8"><w n="40.1">Dans</w> <w n="40.2">le</w> <w n="40.3">sang</w> <w n="40.4">des</w> <w n="40.5">grappes</w> <w n="40.6">brisées</w>.</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">Je</w> <w n="41.2">noierai</w> <w n="41.3">dans</w> <w n="41.4">ce</w> <w n="41.5">flot</w> <w n="41.6">divin</w></l>
					<l n="42" num="6.2"><w n="42.1">Le</w> <w n="42.2">feu</w> <w n="42.3">vivant</w> <w n="42.4">qui</w> <w n="42.5">me</w> <w n="42.6">dévore</w>.</l>
					<l n="43" num="6.3"><w n="43.1">Mais</w> <w n="43.2">non</w> ! <w n="43.3">Elle</w> <w n="43.4">apparaît</w> <w n="43.5">encore</w></l>
					<l n="44" num="6.4"><w n="44.1">Sous</w> <w n="44.2">les</w> <w n="44.3">douces</w> <w n="44.4">pourpres</w> <w n="44.5">du</w> <w n="44.6">vin</w> !</l>
					<l n="45" num="6.5"><w n="45.1">Oui</w>, <w n="45.2">voilà</w> <w n="45.3">sa</w> <w n="45.4">grâce</w> <w n="45.5">inhumaine</w> !</l>
					<l n="46" num="6.6"><w n="46.1">Et</w> <w n="46.2">cette</w> <w n="46.3">coupe</w> <w n="46.4">est</w> <w n="46.5">une</w> <w n="46.6">mer</w></l>
					<l n="47" num="6.7"><w n="47.1">D</w>’<w n="47.2">où</w> <w n="47.3">naît</w>, <w n="47.4">comme</w> <w n="47.5">du</w> <w n="47.6">flot</w> <w n="47.7">amer</w>,</l>
					<l n="48" num="6.8"><w n="48.1">L</w>’<w n="48.2">invincible</w> <w n="48.3">Anadyomène</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1849">Novembre 1849.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>