<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN371">
				<head type="main">Les Voyageurs</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Couvertes</w> <w n="1.2">de</w> <w n="1.3">haillons</w>, <w n="1.4">deux</w> <w n="1.5">vierges</w> <w n="1.6">magnifiques</w>,</l>
					<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">la</w> <w n="2.3">démarche</w> <w n="2.4">svelte</w>, <w n="2.5">au</w> <w n="2.6">regard</w> <w n="2.7">ingénu</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Vont</w> <w n="3.2">par</w> <w n="3.3">les</w> <w n="3.4">carrefours</w> <w n="3.5">et</w> <w n="3.6">les</w> <w n="3.7">places</w> <w n="3.8">publiques</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">cheveux</w> <w n="4.3">dénoués</w> <w n="4.4">et</w> <w n="4.5">le</w> <w n="4.6">sein</w> <w n="4.7">demi</w>-<w n="4.8">nu</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Toutes</w> <w n="5.2">les</w> <w n="5.3">deux</w> <w n="5.4">font</w> <w n="5.5">voir</w> <w n="5.6">à</w> <w n="5.7">la</w> <w n="5.8">foule</w> <w n="5.9">profonde</w></l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">fier</w> <w n="6.3">sourire</w> <w n="6.4">fait</w> <w n="6.5">pour</w> <w n="6.6">les</w> <w n="6.7">éternités</w>.</l>
					<l n="7" num="2.3"><w n="7.1">La</w> <w n="7.2">prunelle</w> <w n="7.3">céleste</w> <w n="7.4">et</w> <w n="7.5">la</w> <w n="7.6">crinière</w> <w n="7.7">blonde</w></l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">le</w> <w n="8.3">port</w> <w n="8.4">qui</w> <w n="8.5">convient</w> <w n="8.6">à</w> <w n="8.7">des</w> <w n="8.8">divinités</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Près</w> <w n="9.2">d</w>’<w n="9.3">elles</w>, <w n="9.4">et</w> <w n="9.5">parfois</w> <w n="9.6">leur</w> <w n="9.7">prêtant</w> <w n="9.8">son</w> <w n="9.9">épaule</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">nommant</w> <w n="10.3">tour</w> <w n="10.4">à</w> <w n="10.5">tour</w> <w n="10.6">l</w>’<w n="10.7">une</w> <w n="10.8">et</w> <w n="10.9">l</w>’<w n="10.10">autre</w> : <w n="10.11">ma</w> <w n="10.12">sœur</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Passe</w>, <w n="11.2">le</w> <w n="11.3">front</w> <w n="11.4">plus</w> <w n="11.5">pur</w> <w n="11.6">que</w> <w n="11.7">les</w> <w n="11.8">neiges</w> <w n="11.9">du</w> <w n="11.10">pôle</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Un</w> <w n="12.2">grave</w> <w n="12.3">adolescent</w> <w n="12.4">en</w> <w n="12.5">habit</w> <w n="12.6">de</w> <w n="12.7">chasseur</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Il</w> <w n="13.2">les</w> <w n="13.3">console</w> <w n="13.4">ainsi</w> : <w n="13.5">Courage</w>, <w n="13.6">ô</w> <w n="13.7">mes</w> <w n="13.8">compagnes</w> !</l>
					<l n="14" num="4.2"><w n="14.1">Bientôt</w> <w n="14.2">dans</w> <w n="14.3">les</w> <w n="14.4">parfums</w> <w n="14.5">nos</w> <w n="14.6">pieds</w> <w n="14.7">seront</w> <w n="14.8">lavés</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Après</w> <w n="15.2">tant</w> <w n="15.3">de</w> <w n="15.4">forêts</w>, <w n="15.5">de</w> <w n="15.6">champs</w> <w n="15.7">et</w> <w n="15.8">de</w> <w n="15.9">campagnes</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Voici</w> <w n="16.2">Paris</w> <w n="16.3">sans</w> <w n="16.4">doute</w>, <w n="16.5">et</w> <w n="16.6">nous</w> <w n="16.7">sommes</w> <w n="16.8">sauvés</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Ils</w> <w n="17.2">s</w>’<w n="17.3">arrêtent</w> <w n="17.4">d</w>’<w n="17.5">abord</w> <w n="17.6">au</w> <w n="17.7">festin</w> <w n="17.8">plein</w> <w n="17.9">de</w> <w n="17.10">flammes</w></l>
					<l n="18" num="5.2"><w n="18.1">Où</w> <w n="18.2">l</w>’<w n="18.3">or</w>, <w n="18.4">que</w> <w n="18.5">rend</w> <w n="18.6">vivant</w> <w n="18.7">l</w>’<w n="18.8">esprit</w> <w n="18.9">des</w> <w n="18.10">ciseleurs</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Reflète</w> <w n="19.2">follement</w>, <w n="19.3">pour</w> <w n="19.4">enchanter</w> <w n="19.5">nos</w> <w n="19.6">âmes</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Le</w> <w n="20.2">sang</w> <w n="20.3">des</w> <w n="20.4">noirs</w> <w n="20.5">raisins</w> <w n="20.6">et</w> <w n="20.7">les</w> <w n="20.8">lèvres</w> <w n="20.9">des</w> <w n="20.10">fleurs</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Là</w>, <w n="21.2">la</w> <w n="21.3">coupe</w> <w n="21.4">est</w> <w n="21.5">en</w> <w n="21.6">feu</w> <w n="21.7">sous</w> <w n="21.8">les</w> <w n="21.9">tresses</w> <w n="21.10">fleuries</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Tout</w> <w n="22.2">s</w>’<w n="22.3">étale</w> <w n="22.4">à</w> <w n="22.5">souhait</w> <w n="22.6">pour</w> <w n="22.7">ravir</w> <w n="22.8">les</w> <w n="22.9">amants</w> :</l>
					<l n="23" num="6.3"><w n="23.1">Le</w> <w n="23.2">vin</w> <w n="23.3">du</w> <w n="23.4">Rhin</w> <w n="23.5">y</w> <w n="23.6">lutte</w> <w n="23.7">avec</w> <w n="23.8">les</w> <w n="23.9">pierreries</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">la</w> <w n="24.3">blancheur</w> <w n="24.4">du</w> <w n="24.5">lys</w> <w n="24.6">avec</w> <w n="24.7">les</w> <w n="24.8">diamants</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Les</w> <w n="25.2">voyageurs</w> <w n="25.3">divins</w> <w n="25.4">sous</w> <w n="25.5">la</w> <w n="25.6">splendide</w> <w n="25.7">voûte</w></l>
					<l n="26" num="7.2"><w n="26.1">S</w>’<w n="26.2">avancent</w> <w n="26.3">d</w>’<w n="26.4">un</w> <w n="26.5">air</w> <w n="26.6">doux</w> <w n="26.7">et</w> <w n="26.8">cependant</w> <w n="26.9">hautain</w></l>
					<l n="27" num="7.3"><w n="27.1">En</w> <w n="27.2">faisant</w> <w n="27.3">voir</w> <w n="27.4">leurs</w> <w n="27.5">pieds</w> <w n="27.6">tout</w> <w n="27.7">meurtris</w> <w n="27.8">de</w> <w n="27.9">la</w> <w n="27.10">route</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">disent</w> : <w n="28.3">Donnez</w>-<w n="28.4">nous</w> <w n="28.5">une</w> <w n="28.6">place</w> <w n="28.7">au</w> <w n="28.8">festin</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Puis</w> <w n="29.2">ils</w> <w n="29.3">vont</w> <w n="29.4">au</w> <w n="29.5">théâtre</w>, <w n="29.6">au</w> <w n="29.7">cher</w> <w n="29.8">pays</w> <w n="29.9">du</w> <w n="29.10">rêve</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Où</w> <w n="30.2">de</w> <w n="30.3">deux</w> <w n="30.4">bras</w> <w n="30.5">de</w> <w n="30.6">lys</w> <w n="30.7">pour</w> <w n="30.8">une</w> <w n="30.9">heure</w> <w n="30.10">enlacé</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Le</w> <w n="31.2">sublime</w> <w n="31.3">histrion</w>, <w n="31.4">appuyé</w> <w n="31.5">sur</w> <w n="31.6">son</w> <w n="31.7">glaive</w>,</l>
					<l n="32" num="8.4"><w n="32.1">S</w>’<w n="32.2">écrie</w> : <w n="32.3">O</w> <w n="32.4">Juliette</w> ! <w n="32.5">avec</w> <w n="32.6">un</w> <w n="32.7">ton</w> <w n="32.8">glacé</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Ils</w> <w n="33.2">lui</w> <w n="33.3">disent</w> : <w n="33.4">Oh</w> ! <w n="33.5">viens</w>, <w n="33.6">toi</w> <w n="33.7">qui</w> <w n="33.8">connais</w> <w n="33.9">les</w> <w n="33.10">charmes</w></l>
					<l n="34" num="9.2"><w n="34.1">De</w> <w n="34.2">la</w> <w n="34.3">Douleur</w>, <w n="34.4">pareille</w> <w n="34.5">à</w> <w n="34.6">l</w>’<w n="34.7">orage</w> <w n="34.8">des</w> <w n="34.9">flots</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Que</w> <w n="35.2">nous</w> <w n="35.3">te</w> <w n="35.4">racontions</w> <w n="35.5">la</w> <w n="35.6">cause</w> <w n="35.7">de</w> <w n="35.8">nos</w> <w n="35.9">larmes</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">pourquoi</w> <w n="36.3">notre</w> <w n="36.4">cœur</w> <w n="36.5">est</w> <w n="36.6">gonflé</w> <w n="36.7">de</w> <w n="36.8">sanglots</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Puis</w> <w n="37.2">ils</w> <w n="37.3">vont</w> <w n="37.4">au</w> <w n="37.5">dernier</w> <w n="37.6">sanctuaire</w>, <w n="37.7">où</w> <w n="37.8">l</w>’<w n="37.9">artiste</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Pareil</w> <w n="38.2">à</w> <w n="38.3">la</w> <w n="38.4">Pythie</w> <w n="38.5">interrogeant</w> <w n="38.6">l</w>’<w n="38.7">autel</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Se</w> <w n="39.2">demande</w> <w n="39.3">quelle</w> <w n="39.4">est</w> <w n="39.5">la</w> <w n="39.6">tête</w> <w n="39.7">noble</w> <w n="39.8">et</w> <w n="39.9">triste</w></l>
					<l n="40" num="10.4"><w n="40.1">Qui</w> <w n="40.2">mérite</w> <w n="40.3">le</w> <w n="40.4">marbre</w> <w n="40.5">et</w> <w n="40.6">le</w> <w n="40.7">bronze</w> <w n="40.8">immortel</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Et</w> <w n="41.2">tous</w> <w n="41.3">les</w> <w n="41.4">trois</w>, <w n="41.5">calmés</w> <w n="41.6">alors</w>, <w n="41.7">parce</w> <w n="41.8">qu</w>’<w n="41.9">ils</w> <w n="41.10">lisent</w></l>
					<l n="42" num="11.2"><w n="42.1">Sur</w> <w n="42.2">les</w> <w n="42.3">socles</w> <w n="42.4">épars</w> <w n="42.5">des</w> <w n="42.6">noms</w> <w n="42.7">mélodieux</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Parlent</w> <w n="43.2">au</w> <w n="43.3">statuaire</w> <w n="43.4">indécis</w> <w n="43.5">et</w> <w n="43.6">lui</w> <w n="43.7">disent</w> :</l>
					<l n="44" num="11.4"><w n="44.1">Reconnais</w> <w n="44.2">trois</w> <w n="44.3">enfants</w> <w n="44.4">sortis</w> <w n="44.5">du</w> <w n="44.6">sang</w> <w n="44.7">des</w> <w n="44.8">Dieux</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Mais</w> <w n="45.2">tous</w> <w n="45.3">ceux</w> <w n="45.4">qu</w>’<w n="45.5">ils</w> <w n="45.6">avaient</w> <w n="45.7">implorés</w> <w n="45.8">leur</w> <w n="45.9">répondent</w> :</l>
					<l n="46" num="12.2"><w n="46.1">Enfants</w>, <w n="46.2">évitez</w>-<w n="46.3">moi</w> <w n="46.4">des</w> <w n="46.5">efforts</w> <w n="46.6">superflus</w>.</l>
					<l n="47" num="12.3"><w n="47.1">Nos</w> <w n="47.2">villes</w> <w n="47.3">cette</w> <w n="47.4">année</w> <w n="47.5">en</w> <w n="47.6">orphelins</w> <w n="47.7">abondent</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Redites</w>-<w n="48.2">moi</w> <w n="48.3">vos</w> <w n="48.4">noms</w>, <w n="48.5">car</w> <w n="48.6">je</w> <w n="48.7">ne</w> <w n="48.8">les</w> <w n="48.9">sais</w> <w n="48.10">plus</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Déjà</w>, <w n="49.2">pour</w> <w n="49.3">assouvir</w> <w n="49.4">leur</w> <w n="49.5">appétit</w> <w n="49.6">vorace</w>,</l>
					<l n="50" num="13.2"><w n="50.1">On</w> <w n="50.2">posait</w> <w n="50.3">devant</w> <w n="50.4">eux</w> <w n="50.5">le</w> <w n="50.6">vin</w> <w n="50.7">et</w> <w n="50.8">le</w> <w n="50.9">doux</w> <w n="50.10">miel</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Mais</w> <w n="51.2">dès</w> <w n="51.3">qu</w>’<w n="51.4">ils</w> <w n="51.5">ont</w> <w n="51.6">montré</w> <w n="51.7">les</w> <w n="51.8">signes</w> <w n="51.9">de</w> <w n="51.10">leur</w> <w n="51.11">race</w></l>
					<l n="52" num="13.4"><w n="52.1">En</w> <w n="52.2">ajoutant</w> <w n="52.3">ces</w> <w n="52.4">mots</w> : <w n="52.5">Nous</w> <w n="52.6">arrivons</w> <w n="52.7">du</w> <w n="52.8">ciel</w>,</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Nous</w> <w n="53.2">sommes</w> <w n="53.3">la</w> <w n="53.4">Beauté</w>, <w n="53.5">l</w>’<w n="53.6">Amour</w>, <w n="53.7">la</w> <w n="53.8">Poésie</w>,</l>
					<l n="54" num="14.2"><w n="54.1">On</w> <w n="54.2">s</w>’<w n="54.3">écrie</w> <w n="54.4">aussitôt</w> : <w n="54.5">Portez</w> <w n="54.6">ailleurs</w> <w n="54.7">vos</w> <w n="54.8">pas</w>.</l>
					<l n="55" num="14.3"><w n="55.1">Enfants</w> <w n="55.2">déguenillés</w>, <w n="55.3">ô</w> <w n="55.4">buveurs</w> <w n="55.5">d</w>’<w n="55.6">ambroisie</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Passez</w> <w n="56.2">votre</w> <w n="56.3">chemin</w>, <w n="56.4">je</w> <w n="56.5">ne</w> <w n="56.6">vous</w> <w n="56.7">connais</w> <w n="56.8">pas</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1856">Février 1856.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>