<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN367">
				<head type="main">La Charité</head>
				<head type="form">Ode</head>
				<head type="sub_1">écrite pour une représentation <lb></lb>donnée au bénéfice des pauvres</head>
				<div type="section" n="1">
					<head type="main">La Comédienne</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">cœurs</w> <w n="1.3">toujours</w> <w n="1.4">ouverts</w>, <w n="1.5">dont</w> <w n="1.6">la</w> <w n="1.7">pitié</w> <w n="1.8">si</w> <w n="1.9">tendre</w></l>
							<l n="2" num="1.2"><w n="2.1">Va</w> <w n="2.2">chercher</w> <w n="2.3">le</w> <w n="2.4">malheur</w> <w n="2.5">pour</w> <w n="2.6">mieux</w> <w n="2.7">s</w>’<w n="2.8">en</w> <w n="2.9">souvenir</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Écoutez</w>-<w n="3.2">moi</w> : <w n="3.3">c</w>’<w n="3.4">est</w> <w n="3.5">lui</w> <w n="3.6">que</w> <w n="3.7">vous</w> <w n="3.8">allez</w> <w n="3.9">entendre</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">suis</w> <w n="4.3">la</w> <w n="4.4">voix</w> <w n="4.5">de</w> <w n="4.6">ceux</w> <w n="4.7">qui</w> <w n="4.8">veulent</w> <w n="4.9">vous</w> <w n="4.10">bénir</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Eux</w> <w n="5.2">à</w> <w n="5.3">qui</w> <w n="5.4">le</w> <w n="5.5">Seigneur</w> <w n="5.6">donna</w> <w n="5.7">pour</w> <w n="5.8">seules</w> <w n="5.9">armes</w></l>
							<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2">humble</w> <w n="6.3">foi</w> <w n="6.4">du</w> <w n="6.5">croyant</w> <w n="6.6">qui</w> <w n="6.7">le</w> <w n="6.8">prie</w> <w n="6.9">à</w> <w n="6.10">genoux</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">vous</w> <w n="7.3">remercier</w> <w n="7.4">ils</w> <w n="7.5">n</w>’<w n="7.6">avaient</w> <w n="7.7">que</w> <w n="7.8">leurs</w> <w n="7.9">larmes</w> ;</l>
							<l n="8" num="2.4"><w n="8.1">Ils</w> <w n="8.2">m</w>’<w n="8.3">ont</w> <w n="8.4">dit</w> <w n="8.5">en</w> <w n="8.6">pleurant</w> : <w n="8.7">Vous</w> <w n="8.8">parlerez</w> <w n="8.9">pour</w> <w n="8.10">nous</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Aussi</w> <w n="9.2">je</w> <w n="9.3">viens</w> <w n="9.4">vous</w> <w n="9.5">dire</w> <w n="9.6">au</w> <w n="9.7">nom</w> <w n="9.8">des</w> <w n="9.9">pauvres</w> <w n="9.10">mères</w></l>
							<l n="10" num="3.2"><w n="10.1">Dont</w> <w n="10.2">le</w> <w n="10.3">calme</w> <w n="10.4">sourire</w>, <w n="10.5">aujourd</w>’<w n="10.6">hui</w> <w n="10.7">triomphant</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Hier</w> <w n="11.2">dissimulait</w> <w n="11.3">des</w> <w n="11.4">angoisses</w> <w n="11.5">amères</w> :</l>
							<l n="12" num="3.4"><w n="12.1">Merci</w>, <w n="12.2">car</w> <w n="12.3">c</w>’<w n="12.4">est</w> <w n="12.5">à</w> <w n="12.6">vous</w> <w n="12.7">que</w> <w n="12.8">je</w> <w n="12.9">dois</w> <w n="12.10">mon</w> <w n="12.11">enfant</w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">viens</w> <w n="13.3">vous</w> <w n="13.4">dire</w> <w n="13.5">au</w> <w n="13.6">nom</w> <w n="13.7">de</w> <w n="13.8">toutes</w> <w n="13.9">les</w> <w n="13.10">familles</w></l>
							<l n="14" num="4.2"><w n="14.1">Pour</w> <w n="14.2">lesquelles</w> <w n="14.3">demain</w>, <w n="14.4">grâce</w> <w n="14.5">à</w> <w n="14.6">vous</w>, <w n="14.7">sera</w> <w n="14.8">beau</w> :</l>
							<l n="15" num="4.3"><w n="15.1">Merci</w> <w n="15.2">pour</w> <w n="15.3">les</w> <w n="15.4">enfants</w> <w n="15.5">et</w> <w n="15.6">pour</w> <w n="15.7">les</w> <w n="15.8">jeunes</w> <w n="15.9">filles</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Merci</w> <w n="16.2">pour</w> <w n="16.3">les</w> <w n="16.4">vieillards</w> <w n="16.5">courbés</w> <w n="16.6">vers</w> <w n="16.7">le</w> <w n="16.8">tombeau</w> !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">viens</w> <w n="17.3">vous</w> <w n="17.4">dire</w> <w n="17.5">au</w> <w n="17.6">nom</w> <w n="17.7">de</w> <w n="17.8">celui</w> <w n="17.9">qui</w> <w n="17.10">déploie</w></l>
							<l n="18" num="5.2"><w n="18.1">Au</w>-<w n="18.2">dessus</w> <w n="18.3">de</w> <w n="18.4">nos</w> <w n="18.5">fronts</w> <w n="18.6">le</w> <w n="18.7">ciel</w> <w n="18.8">immense</w> <w n="18.9">et</w> <w n="18.10">bleu</w> :</l>
							<l n="19" num="5.3"><w n="19.1">En</w> <w n="19.2">plaisirs</w>, <w n="19.3">en</w> <w n="19.4">bonheur</w>, <w n="19.5">en</w> <w n="19.6">délires</w> <w n="19.7">de</w> <w n="19.8">joie</w></l>
							<l n="20" num="5.4"><w n="20.1">On</w> <w n="20.2">vous</w> <w n="20.3">rendra</w> <w n="20.4">cet</w> <w n="20.5">or</w> <w n="20.6">que</w> <w n="20.7">vous</w> <w n="20.8">prêtez</w> <w n="20.9">à</w> <w n="20.10">Dieu</w> !</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Car</w> <w n="21.2">le</w> <w n="21.3">pauvre</w>, <w n="21.4">c</w>’<w n="21.5">est</w> <w n="21.6">lui</w>. <w n="21.7">Sublime</w> <w n="21.8">poésie</w></l>
							<l n="22" num="6.2"><w n="22.1">Que</w> <w n="22.2">lui</w>-<w n="22.3">même</w> <w n="22.4">enseigna</w> <w n="22.5">pour</w> <w n="22.6">guide</w> <w n="22.7">à</w> <w n="22.8">la</w> <w n="22.9">vertu</w> !</l>
							<l n="23" num="6.3"><w n="23.1">Celui</w> <w n="23.2">qui</w> <w n="23.3">donne</w> <w n="23.4">au</w> <w n="23.5">pauvre</w> <w n="23.6">un</w> <w n="23.7">pain</w>, <w n="23.8">le</w> <w n="23.9">rassasie</w>,</l>
							<l n="24" num="6.4"><w n="24.1">Celui</w> <w n="24.2">qui</w> <w n="24.3">donne</w> <w n="24.4">au</w> <w n="24.5">pauvre</w> <w n="24.6">un</w> <w n="24.7">manteau</w>, <w n="24.8">l</w>’<w n="24.9">a</w> <w n="24.10">vêtu</w> !</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Mais</w> <w n="25.2">ce</w> <w n="25.3">pauvre</w>, <w n="25.4">la</w> <w n="25.5">chair</w> <w n="25.6">de</w> <w n="25.7">sa</w> <w n="25.8">chair</w>, <w n="25.9">et</w> <w n="25.10">qu</w>’<w n="25.11">il</w> <w n="25.12">aime</w></l>
							<l n="26" num="7.2"><w n="26.1">Avant</w> <w n="26.2">tous</w>, <w n="26.3">l</w>’<w n="26.4">indigent</w> <w n="26.5">que</w> <w n="26.6">le</w> <w n="26.7">Christ</w> <w n="26.8">appela</w></l>
							<l n="27" num="7.3"><w n="27.1">A</w> <w n="27.2">s</w>’<w n="27.3">asseoir</w> <w n="27.4">dans</w> <w n="27.5">le</w> <w n="27.6">ciel</w> <w n="27.7">à</w> <w n="27.8">côté</w> <w n="27.9">de</w> <w n="27.10">lui</w>-<w n="27.11">même</w>,</l>
							<l n="28" num="7.4"><w n="28.1">N</w>’<w n="28.2">aura</w> <w n="28.3">besoin</w> <w n="28.4">de</w> <w n="28.5">rien</w> <w n="28.6">tant</w> <w n="28.7">que</w> <w n="28.8">vous</w> <w n="28.9">êtes</w> <w n="28.10">là</w> !</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">C</w>’<w n="29.2">est</w> <w n="29.3">l</w>’<w n="29.4">hiver</w>. <w n="29.5">Tout</w> <w n="29.6">gémit</w> <w n="29.7">dans</w> <w n="29.8">la</w> <w n="29.9">pauvre</w> <w n="29.10">demeure</w>.</l>
							<l n="30" num="8.2"><w n="30.1">Auprès</w> <w n="30.2">de</w> <w n="30.3">son</w> <w n="30.4">vieux</w> <w n="30.5">chien</w> <w n="30.6">qu</w>’<w n="30.7">il</w> <w n="30.8">vient</w> <w n="30.9">de</w> <w n="30.10">rudoyer</w>,</l>
							<l n="31" num="8.3"><w n="31.1">Le</w> <w n="31.2">père</w> <w n="31.3">tout</w> <w n="31.4">pensif</w> <w n="31.5">se</w> <w n="31.6">tait</w>, <w n="31.7">et</w> <w n="31.8">d</w>’<w n="31.9">heure</w> <w n="31.10">en</w> <w n="31.11">heure</w></l>
							<l n="32" num="8.4"><w n="32.1">Le</w> <w n="32.2">pain</w> <w n="32.3">manque</w> <w n="32.4">à</w> <w n="32.5">la</w> <w n="32.6">huche</w> <w n="32.7">et</w> <w n="32.8">le</w> <w n="32.9">bois</w> <w n="32.10">au</w> <w n="32.11">foyer</w> !</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">Les</w> <w n="33.2">petits</w>, <w n="33.3">secouant</w> <w n="33.4">leur</w> <w n="33.5">chevelure</w> <w n="33.6">blonde</w>,</l>
							<l n="34" num="9.2"><w n="34.1">Disent</w> : <w n="34.2">Qui</w> <w n="34.3">soutiendra</w> <w n="34.4">nos</w> <w n="34.5">pas</w>, <w n="34.6">faibles</w> <w n="34.7">roseaux</w>,</l>
							<l n="35" num="9.3"><w n="35.1">Si</w> <w n="35.2">vous</w> <w n="35.3">nous</w> <w n="35.4">oubliez</w>, <w n="35.5">mon</w> <w n="35.6">Dieu</w>, <w n="35.7">maître</w> <w n="35.8">du</w> <w n="35.9">monde</w></l>
							<l n="36" num="9.4"><w n="36.1">Qui</w> <w n="36.2">donnez</w> <w n="36.3">leur</w> <w n="36.4">pâture</w> <w n="36.5">aux</w> <w n="36.6">petits</w> <w n="36.7">des</w> <w n="36.8">oiseaux</w> ?</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">La</w> <w n="37.2">mère</w>, <w n="37.3">elle</w>, <w n="37.4">tressaille</w> <w n="37.5">en</w> <w n="37.6">faisant</w> <w n="37.7">la</w> <w n="37.8">toilette</w></l>
							<l n="38" num="10.2"><w n="38.1">De</w> <w n="38.2">sa</w> <w n="38.3">fille</w>, <w n="38.4">et</w> <w n="38.5">jetant</w>, <w n="38.6">de</w> <w n="38.7">larmes</w> <w n="38.8">arrosé</w>,</l>
							<l n="39" num="10.3"><w n="39.1">Un</w> <w n="39.2">œil</w> <w n="39.3">de</w> <w n="39.4">désespoir</w> <w n="39.5">sur</w> <w n="39.6">l</w>’<w n="39.7">enfant</w> <w n="39.8">qu</w>’<w n="39.9">elle</w> <w n="39.10">allaite</w>,</l>
							<l n="40" num="10.4"><w n="40.1">Le</w> <w n="40.2">berce</w> <w n="40.3">avec</w> <w n="40.4">terreur</w> <w n="40.5">sur</w> <w n="40.6">son</w> <w n="40.7">sein</w> <w n="40.8">épuisé</w>.</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1"><w n="41.1">Mais</w> <w n="41.2">vous</w> <w n="41.3">venez</w>, <w n="41.4">ainsi</w> <w n="41.5">qu</w>’<w n="41.6">une</w> <w n="41.7">aurore</w> <w n="41.8">vermeille</w>,</l>
							<l n="42" num="11.2"><w n="42.1">Des</w> <w n="42.2">rayons</w> <w n="42.3">de</w> <w n="42.4">vos</w> <w n="42.5">yeux</w> <w n="42.6">dorer</w> <w n="42.7">ces</w> <w n="42.8">pauvres</w> <w n="42.9">murs</w>,</l>
							<l n="43" num="11.3"><w n="43.1">Et</w>, <w n="43.2">comme</w> <w n="43.3">un</w> <w n="43.4">serviteur</w> <w n="43.5">qui</w> <w n="43.6">vide</w> <w n="43.7">sa</w> <w n="43.8">corbeille</w>,</l>
							<l n="44" num="11.4"><w n="44.1">Vous</w> <w n="44.2">faites</w> <w n="44.3">de</w> <w n="44.4">vos</w> <w n="44.5">mains</w> <w n="44.6">tomber</w> <w n="44.7">les</w> <w n="44.8">épis</w> <w n="44.9">mûrs</w> !</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1"><w n="45.1">Consolant</w> <w n="45.2">tout</w> <w n="45.3">ce</w> <w n="45.4">monde</w> <w n="45.5">avec</w> <w n="45.6">mélancolie</w>,</l>
							<l n="46" num="12.2"><w n="46.1">Vous</w> <w n="46.2">leur</w> <w n="46.3">dites</w> <w n="46.4">avec</w> <w n="46.5">un</w> <w n="46.6">sourire</w> <w n="46.7">divin</w> :</l>
							<l n="47" num="12.3"><w n="47.1">Celui</w> <w n="47.2">qui</w> <w n="47.3">songe</w> <w n="47.4">à</w> <w n="47.5">tous</w> <w n="47.6">jamais</w> <w n="47.7">ne</w> <w n="47.8">vous</w> <w n="47.9">oublie</w> ;</l>
							<l n="48" num="12.4"><w n="48.1">Mangez</w>, <w n="48.2">voici</w> <w n="48.3">du</w> <w n="48.4">pain</w> ; <w n="48.5">buvez</w>, <w n="48.6">voici</w> <w n="48.7">du</w> <w n="48.8">vin</w>.</l>
						</lg>
						<lg n="13">
							<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">tous</w> <w n="49.3">ces</w> <w n="49.4">malheureux</w>, <w n="49.5">retrouvant</w> <w n="49.6">l</w>’<w n="49.7">espérance</w></l>
							<l n="50" num="13.2"><w n="50.1">Rien</w> <w n="50.2">qu</w>’<w n="50.3">à</w> <w n="50.4">vous</w> <w n="50.5">voir</w> <w n="50.6">ainsi</w>, <w n="50.7">pensent</w> <w n="50.8">avec</w> <w n="50.9">raison</w></l>
							<l n="51" num="13.3"><w n="51.1">Que</w>, <w n="51.2">venus</w> <w n="51.3">de</w> <w n="51.4">là</w>-<w n="51.5">haut</w> <w n="51.6">pour</w> <w n="51.7">calmer</w> <w n="51.8">leur</w> <w n="51.9">souffrance</w>,</l>
							<l n="52" num="13.4"><w n="52.1">Des</w> <w n="52.2">Anges</w> <w n="52.3">de</w> <w n="52.4">lumière</w> <w n="52.5">entrent</w> <w n="52.6">dans</w> <w n="52.7">leur</w> <w n="52.8">maison</w> !</l>
						</lg>
						<lg n="14">
							<l n="53" num="14.1"><w n="53.1">Car</w>, <w n="53.2">lorsque</w> <w n="53.3">pour</w> <w n="53.4">six</w> <w n="53.5">mois</w> <w n="53.6">a</w> <w n="53.7">fui</w> <w n="53.8">la</w> <w n="53.9">saison</w> <w n="53.10">douce</w></l>
							<l n="54" num="14.2"><w n="54.1">Où</w> <w n="54.2">le</w> <w n="54.3">contentement</w> <w n="54.4">tombe</w> <w n="54.5">du</w> <w n="54.6">ciel</w> <w n="54.7">vermeil</w>,</l>
							<l n="55" num="14.3"><w n="55.1">On</w> <w n="55.2">dit</w> : <w n="55.3">Que</w> <w n="55.4">reste</w>-<w n="55.5">t</w>-<w n="55.6">il</w> <w n="55.7">à</w> <w n="55.8">ceux</w> <w n="55.9">que</w> <w n="55.10">tout</w> <w n="55.11">repousse</w></l>
							<l n="56" num="14.4"><w n="56.1">Et</w> <w n="56.2">qui</w> <w n="56.3">n</w>’<w n="56.4">ont</w> <w n="56.5">plus</w> <w n="56.6">pour</w> <w n="56.7">eux</w> <w n="56.8">l</w>’<w n="56.9">air</w> <w n="56.10">pur</w> <w n="56.11">et</w> <w n="56.12">le</w> <w n="56.13">soleil</w> ?</l>
						</lg>
						<lg n="15">
							<l n="57" num="15.1"><w n="57.1">A</w> <w n="57.2">ceux</w>-<w n="57.3">là</w> <w n="57.4">qui</w> <w n="57.5">le</w> <w n="57.6">soir</w> <w n="57.7">souffrent</w> <w n="57.8">un</w> <w n="57.9">long</w> <w n="57.10">martyre</w></l>
							<l n="58" num="15.2"><w n="58.1">En</w> <w n="58.2">voyant</w> <w n="58.3">s</w>’<w n="58.4">allumer</w> <w n="58.5">les</w> <w n="58.6">vitres</w> <w n="58.7">des</w> <w n="58.8">palais</w> ?</l>
							<l n="59" num="15.3"><w n="59.1">Au</w> <w n="59.2">marin</w> <w n="59.3">dont</w> <w n="59.4">la</w> <w n="59.5">mer</w> <w n="59.6">a</w> <w n="59.7">brisé</w> <w n="59.8">le</w> <w n="59.9">navire</w> ?</l>
							<l n="60" num="15.4"><w n="60.1">Au</w> <w n="60.2">pêcheur</w> <w n="60.3">dont</w> <w n="60.4">la</w> <w n="60.5">vague</w> <w n="60.6">a</w> <w n="60.7">troué</w> <w n="60.8">les</w> <w n="60.9">filets</w> ?</l>
						</lg>
						<lg n="16">
							<l n="61" num="16.1"><w n="61.1">On</w> <w n="61.2">dit</w> : <w n="61.3">Que</w> <w n="61.4">reste</w>-<w n="61.5">t</w>-<w n="61.6">il</w> <w n="61.7">à</w> <w n="61.8">toutes</w> <w n="61.9">les</w> <w n="61.10">victimes</w></l>
							<l n="62" num="16.2"><w n="62.1">Qui</w>, <w n="62.2">malgré</w> <w n="62.3">cet</w> <w n="62.4">espoir</w> <w n="62.5">résigné</w> <w n="62.6">du</w> <w n="62.7">chrétien</w>,</l>
							<l n="63" num="16.3"><w n="63.1">Sous</w> <w n="63.2">leurs</w> <w n="63.3">pieds</w> <w n="63.4">frémissants</w> <w n="63.5">ne</w> <w n="63.6">voient</w> <w n="63.7">que</w> <w n="63.8">des</w> <w n="63.9">abîmes</w>,</l>
							<l n="64" num="16.4"><w n="64.1">Enfin</w>, <w n="64.2">que</w> <w n="64.3">reste</w>-<w n="64.4">t</w>-<w n="64.5">il</w> <w n="64.6">à</w> <w n="64.7">ceux</w> <w n="64.8">qui</w> <w n="64.9">n</w>’<w n="64.10">ont</w> <w n="64.11">plus</w> <w n="64.12">rien</w> ?</l>
						</lg>
						<lg n="17">
							<l n="65" num="17.1"><w n="65.1">O</w> <w n="65.2">bons</w> <w n="65.3">cœurs</w>, <w n="65.4">il</w> <w n="65.5">leur</w> <w n="65.6">reste</w> <w n="65.7">encore</w> <w n="65.8">un</w> <w n="65.9">héritage</w></l>
							<l n="66" num="17.2"><w n="66.1">Dont</w> <w n="66.2">aucun</w> <w n="66.3">d</w>’<w n="66.4">eux</w> <w n="66.5">ne</w> <w n="66.6">peut</w> <w n="66.7">être</w> <w n="66.8">déshérité</w>,</l>
							<l n="67" num="17.3"><w n="67.1">Et</w> <w n="67.2">qu</w>’<w n="67.3">ils</w> <w n="67.4">possèdent</w> <w n="67.5">tous</w> <w n="67.6">entier</w> <w n="67.7">et</w> <w n="67.8">sans</w> <w n="67.9">partage</w>,</l>
							<l n="68" num="17.4"><w n="68.1">Ce</w> <w n="68.2">trésor</w> <w n="68.3">infini</w>, <w n="68.4">c</w>’<w n="68.5">est</w> <w n="68.6">votre</w> <w n="68.7">Charité</w> !</l>
						</lg>
						<lg n="18">
							<l n="69" num="18.1"><w n="69.1">C</w>’<w n="69.2">est</w> <w n="69.3">elle</w>, <w n="69.4">Ange</w> <w n="69.5">penché</w> <w n="69.6">partout</w> <w n="69.7">où</w> <w n="69.8">crie</w> <w n="69.9">un</w> <w n="69.10">gouffre</w>,</l>
							<l n="70" num="18.2"><w n="70.1">Amour</w> <w n="70.2">inépuisable</w> <w n="70.3">entre</w> <w n="70.4">tous</w> <w n="70.5">les</w> <w n="70.6">amours</w>,</l>
							<l n="71" num="18.3"><w n="71.1">Qui</w> <w n="71.2">de</w> <w n="71.3">sa</w> <w n="71.4">lèvre</w> <w n="71.5">en</w> <w n="71.6">fleur</w> <w n="71.7">baise</w> <w n="71.8">tout</w> <w n="71.9">ce</w> <w n="71.10">qui</w> <w n="71.11">souffre</w> :</l>
							<l n="72" num="18.4"><w n="72.1">Elle</w> <w n="72.2">est</w> <w n="72.3">le</w> <w n="72.4">bien</w> <w n="72.5">du</w> <w n="72.6">pauvre</w>, <w n="72.7">et</w> <w n="72.8">ce</w> <w n="72.9">soir</w> <w n="72.10">et</w> <w n="72.11">toujours</w> !</l>
						</lg>
						<lg n="19">
							<l n="73" num="19.1"><w n="73.1">Et</w> <w n="73.2">maintenant</w>, <w n="73.3">amis</w>, <w n="73.4">vous</w> <w n="73.5">que</w> <w n="73.6">nous</w> <w n="73.7">implorâmes</w> !</l>
							<l n="74" num="19.2">(<w n="74.1">Quel</w> <w n="74.2">que</w> <w n="74.3">soit</w> <w n="74.4">devant</w> <w n="74.5">vous</w> <w n="74.6">mon</w> <w n="74.7">invincible</w> <w n="74.8">émoi</w>,</l>
							<l n="75" num="19.3"><w n="75.1">Je</w> <w n="75.2">ne</w> <w n="75.3">tremblerai</w> <w n="75.4">pas</w>, <w n="75.5">car</w> <w n="75.6">je</w> <w n="75.7">parle</w> <w n="75.8">à</w> <w n="75.9">vos</w> <w n="75.10">âmes</w>,)</l>
							<l n="76" num="19.4"><w n="76.1">Pour</w> <w n="76.2">les</w> <w n="76.3">pauvres</w> <w n="76.4">encor</w> <w n="76.5">merci</w>, <w n="76.6">merci</w> <w n="76.7">pour</w> <w n="76.8">moi</w> !</l>
						</lg>
						<lg n="20">
							<l n="77" num="20.1"><w n="77.1">L</w>’<w n="77.2">humble</w> <w n="77.3">artiste</w> <w n="77.4">après</w> <w n="77.5">eux</w> <w n="77.6">bénit</w> <w n="77.7">votre</w> <w n="77.8">indulgence</w>,</l>
							<l n="78" num="20.2"><w n="78.1">Car</w> <w n="78.2">vous</w> <w n="78.3">avez</w> <w n="78.4">voulu</w> <w n="78.5">qu</w>’<w n="78.6">en</w> <w n="78.7">ses</w> <w n="78.8">nobles</w> <w n="78.9">chemins</w></l>
							<l n="79" num="20.3"><w n="79.1">Votre</w> <w n="79.2">or</w> <w n="79.3">sanctifié</w>, <w n="79.4">qui</w> <w n="79.5">cherchait</w> <w n="79.6">l</w>’<w n="79.7">indigence</w>,</l>
							<l n="80" num="20.4"><w n="80.1">Pour</w> <w n="80.2">arriver</w> <w n="80.3">au</w> <w n="80.4">but</w> <w n="80.5">ait</w> <w n="80.6">passé</w> <w n="80.7">par</w> <w n="80.8">ses</w> <w n="80.9">mains</w> !</l>
						</lg>
						<closer>
							<dateline>
								<date when="1853">Décembre 1853.</date>
							</dateline>
						</closer>
					</div>
				</div></body></text></TEI>