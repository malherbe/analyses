<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN342">
				<head type="main">Les Souffrances de l’Artiste</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Artiste</w> <w n="1.2">foudroyé</w> <w n="1.3">sans</w> <w n="1.4">cesse</w>, <w n="1.5">ô</w> <w n="1.6">dompteur</w> <w n="1.7">d</w>’<w n="1.8">âmes</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Sagittaire</w> <w n="2.2">à</w> <w n="2.3">l</w>’<w n="2.4">arc</w> <w n="2.5">d</w>’<w n="2.6">or</w>, <w n="2.7">captif</w> <w n="2.8">mélodieux</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">portes</w> <w n="3.3">dans</w> <w n="3.4">tes</w> <w n="3.5">mains</w> <w n="3.6">ton</w> <w n="3.7">bagage</w> <w n="3.8">de</w> <w n="3.9">flammes</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">tes</w> <w n="4.3">soleils</w> <w n="4.4">volés</w> <w n="4.5">autour</w> <w n="4.6">du</w> <w n="4.7">front</w> <w n="4.8">des</w> <w n="4.9">Dieux</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Laisse</w> <w n="5.2">toute</w> <w n="5.3">espérance</w>, <w n="5.4">éternelle</w> <w n="5.5">victime</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">ne</w> <w n="6.3">querelle</w> <w n="6.4">plus</w> <w n="6.5">ton</w> <w n="6.6">désespoir</w> <w n="6.7">amer</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Puisque</w> <w n="7.2">tu</w> <w n="7.3">t</w>’<w n="7.4">es</w> <w n="7.5">chargé</w> <w n="7.6">de</w> <w n="7.7">remplir</w> <w n="7.8">un</w> <w n="7.9">abîme</w></l>
					<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">tu</w> <w n="8.3">verses</w> <w n="8.4">en</w> <w n="8.5">vain</w> <w n="8.6">toute</w> <w n="8.7">l</w>’<w n="8.8">eau</w> <w n="8.9">de</w> <w n="8.10">la</w> <w n="8.11">mer</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Va</w>, <w n="9.2">tu</w> <w n="9.3">peux</w> <w n="9.4">y</w> <w n="9.5">jeter</w> <w n="9.6">les</w> <w n="9.7">océans</w>, <w n="9.8">poëte</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Sans</w> <w n="10.2">étouffer</w> <w n="10.3">ses</w> <w n="10.4">cris</w> <w n="10.5">et</w> <w n="10.6">son</w> <w n="10.7">rire</w> <w n="10.8">moqueur</w>.</l>
					<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">curiosité</w> <w n="11.3">de</w> <w n="11.4">la</w> <w n="11.5">foule</w> <w n="11.6">inquiète</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Voilà</w> <w n="12.2">le</w> <w n="12.3">nom</w> <w n="12.4">du</w> <w n="12.5">gouffre</w> <w n="12.6">où</w> <w n="12.7">tu</w> <w n="12.8">vides</w> <w n="12.9">ton</w> <w n="12.10">cœur</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Un</w> <w n="13.2">mot</w> <w n="13.3">domine</w> <w n="13.4">seul</w> <w n="13.5">ce</w> <w n="13.6">murmure</w> <w n="13.7">sauvage</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Mais</w> <w n="14.2">ce</w> <w n="14.3">mot</w>, <w n="14.4">c</w>’<w n="14.5">est</w> <w n="14.6">le</w> <w n="14.7">clou</w> <w n="14.8">d</w>’<w n="14.9">or</w> <w n="14.10">et</w> <w n="14.11">de</w> <w n="14.12">diamant</w></l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">l</w>’<w n="15.3">anneau</w> <w n="15.4">qui</w> <w n="15.5">te</w> <w n="15.6">rive</w> <w n="15.7">à</w> <w n="15.8">ton</w> <w n="15.9">dur</w> <w n="15.10">esclavage</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Ainsi</w> <w n="16.2">que</w> <w n="16.3">Prométhée</w> <w n="16.4">à</w> <w n="16.5">son</w> <w n="16.6">rocher</w> <w n="16.7">fumant</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Ce</w> <w n="17.2">mot</w> <w n="17.3">terrible</w>, <w n="17.4">c</w>’<w n="17.5">est</w> : <w n="17.6">Après</w> ? <w n="17.7">Toutes</w> <w n="17.8">tes</w> <w n="17.9">veilles</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Donne</w>-<w n="18.2">les</w>, <w n="18.3">et</w> <w n="18.4">plus</w> <w n="18.5">fier</w> <w n="18.6">qu</w>’<w n="18.7">un</w> <w n="18.8">archange</w> <w n="18.9">impuni</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Pose</w> <w n="19.2">sur</w> <w n="19.3">Pélion</w> <w n="19.4">des</w> <w n="19.5">Ossas</w> <w n="19.6">de</w> <w n="19.7">merveilles</w> !</l>
					<l n="20" num="5.4"><w n="20.1">Fais</w> <w n="20.2">l</w>’<w n="20.3">impossible</w>, <w n="20.4">et</w> <w n="20.5">trouve</w> <w n="20.6">un</w> <w n="20.7">corps</w> <w n="20.8">à</w> <w n="20.9">l</w>’<w n="20.10">infini</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Gonfle</w> <w n="21.2">de</w> <w n="21.3">passion</w> <w n="21.4">les</w> <w n="21.5">figures</w> <w n="21.6">d</w>’<w n="21.7">argile</w> !</l>
					<l n="22" num="6.2"><w n="22.1">Crée</w>, <w n="22.2">anime</w>, <w n="22.3">bâtis</w> ! <w n="22.4">Jusque</w> <w n="22.5">sous</w> <w n="22.6">les</w> <w n="22.7">cyprès</w></l>
					<l n="23" num="6.3"><w n="23.1">Dont</w> <w n="23.2">l</w>’<w n="23.3">ombre</w> <w n="23.4">endormira</w> <w n="23.5">ta</w> <w n="23.6">dépouille</w> <w n="23.7">fragile</w>,</l>
					<l n="24" num="6.4"><w n="24.1">L</w>’<w n="24.2">inexorable</w> <w n="24.3">voix</w> <w n="24.4">viendra</w> <w n="24.5">crier</w> : <w n="24.6">Après</w> ?</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Tu</w> <w n="25.2">peux</w>, <w n="25.3">par</w> <w n="25.4">ton</w> <w n="25.5">regard</w> <w n="25.6">effrayant</w> <w n="25.7">les</w> <w n="25.8">désastres</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Dans</w> <w n="26.2">l</w>’<w n="26.3">espace</w> <w n="26.4">que</w> <w n="26.5">Dieu</w> <w n="26.6">pour</w> <w n="26.7">les</w> <w n="26.8">siens</w> <w n="26.9">fit</w> <w n="26.10">exprès</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Enchaîner</w> <w n="27.2">comme</w> <w n="27.3">lui</w> <w n="27.4">des</w> <w n="27.5">mondes</w> <w n="27.6">et</w> <w n="27.7">des</w> <w n="27.8">astres</w> :</l>
					<l n="28" num="7.4"><w n="28.1">Après</w> ? <w n="28.2">dira</w> <w n="28.3">le</w> <w n="28.4">peuple</w> <w n="28.5">insatiable</w>, <w n="28.6">après</w> ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Tu</w> <w n="29.2">peux</w> <w n="29.3">faire</w> <w n="29.4">fleurir</w> <w n="29.5">tout</w> <w n="29.6">le</w> <w n="29.7">jardin</w> <w n="29.8">des</w> <w n="29.9">œuvres</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Et</w>, <w n="30.2">bravant</w> <w n="30.3">leur</w> <w n="30.4">air</w> <w n="30.5">sombre</w> <w n="30.6">et</w> <w n="30.7">pestilentiel</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Dessécher</w> <w n="31.2">les</w> <w n="31.3">marais</w> <w n="31.4">où</w> <w n="31.5">sifflent</w> <w n="31.6">les</w> <w n="31.7">couleuvres</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Après</w> ? <w n="32.2">dira</w> <w n="32.3">toujours</w> <w n="32.4">le</w> <w n="32.5">peuple</w>. — <w n="32.6">Après</w> ? <w n="32.7">O</w> <w n="32.8">ciel</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Après</w> ? <w n="33.2">Mais</w> <w n="33.3">j</w>’<w n="33.4">ai</w> <w n="33.5">vaincu</w> <w n="33.6">la</w> <w n="33.7">forme</w> <w n="33.8">et</w> <w n="33.9">la</w> <w n="33.10">lumière</w> !</l>
					<l n="34" num="9.2"><w n="34.1">Mes</w> <w n="34.2">yeux</w> <w n="34.3">ont</w> <w n="34.4">bu</w> <w n="34.5">l</w>’<w n="34.6">azur</w>, <w n="34.7">et</w> <w n="34.8">j</w>’<w n="34.9">ai</w> <w n="34.10">dans</w> <w n="34.11">mon</w> <w n="34.12">compas</w></l>
					<l n="35" num="9.3"><w n="35.1">Tenu</w> <w n="35.2">la</w> <w n="35.3">voûte</w> <w n="35.4">immense</w> ! <w n="35.5">O</w> <w n="35.6">foule</w> <w n="35.7">coutumière</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Après</w> ? <w n="36.2">après</w> ? <w n="36.3">dis</w>-<w n="36.4">tu</w> ; <w n="36.5">ne</w> <w n="36.6">te</w> <w n="36.7">souviens</w>-<w n="36.8">tu</w> <w n="36.9">pas</w> ?</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Dans</w> <w n="37.2">les</w> <w n="37.3">noires</w> <w n="37.4">forêts</w>, <w n="37.5">sur</w> <w n="37.6">les</w> <w n="37.7">monts</w> <w n="37.8">de</w> <w n="37.9">la</w> <w n="37.10">Thrace</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Par</w> <w n="38.2">les</w> <w n="38.3">pleurs</w> <w n="38.4">de</w> <w n="38.5">ma</w> <w n="38.6">lyre</w> <w n="38.7">enchantant</w> <w n="38.8">leur</w> <w n="38.9">courroux</w>,</l>
					<l n="39" num="10.3"><w n="39.1">J</w>’<w n="39.2">ai</w> <w n="39.3">fait</w> <w n="39.4">bondir</w> <w n="39.5">d</w>’<w n="39.6">amour</w> <w n="39.7">et</w> <w n="39.8">courir</w> <w n="39.9">sur</w> <w n="39.10">ma</w> <w n="39.11">trace</w></l>
					<l n="40" num="10.4"><w n="40.1">Le</w> <w n="40.2">tigre</w> <w n="40.3">et</w> <w n="40.4">la</w> <w n="40.5">panthère</w> <w n="40.6">et</w> <w n="40.7">les</w> <w n="40.8">grands</w> <w n="40.9">lions</w> <w n="40.10">roux</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Et</w> <w n="41.2">les</w> <w n="41.3">gazons</w> <w n="41.4">touffus</w> <w n="41.5">étoilés</w> <w n="41.6">de</w> <w n="41.7">pervenches</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Les</w> <w n="42.2">feuillages</w> <w n="42.3">pendants</w>, <w n="42.4">les</w> <w n="42.5">profondeurs</w> <w n="42.6">des</w> <w n="42.7">bois</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Les</w> <w n="43.2">antres</w>, <w n="43.3">les</w> <w n="43.4">rochers</w> <w n="43.5">et</w> <w n="43.6">les</w> <w n="43.7">cascades</w> <w n="43.8">blanches</w></l>
					<l n="44" num="11.4"><w n="44.1">Au</w> <w n="44.2">tomber</w> <w n="44.3">de</w> <w n="44.4">la</w> <w n="44.5">nuit</w> <w n="44.6">s</w>’<w n="44.7">enivraient</w> <w n="44.8">de</w> <w n="44.9">ma</w> <w n="44.10">voix</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">O</w> <w n="45.2">foule</w> ! <w n="45.3">j</w>’<w n="45.4">ai</w> <w n="45.5">bravé</w> <w n="45.6">l</w>’<w n="45.7">horreur</w> <w n="45.8">des</w> <w n="45.9">flots</w> <w n="45.10">funèbres</w></l>
					<l n="46" num="12.2"><w n="46.1">Sur</w> <w n="46.2">la</w> <w n="46.3">fragile</w> <w n="46.4">barque</w>, <w n="46.5">et</w>, <w n="46.6">divin</w> <w n="46.7">ouvrier</w>,</l>
					<l n="47" num="12.3"><w n="47.1">J</w>’<w n="47.2">ai</w> <w n="47.3">navigué</w> <w n="47.4">vers</w> <w n="47.5">l</w>’<w n="47.6">ombre</w> <w n="47.7">et</w> <w n="47.8">les</w> <w n="47.9">pâles</w> <w n="47.10">ténèbres</w>,</l>
					<l n="48" num="12.4"><w n="48.1">En</w> <w n="48.2">tenant</w> <w n="48.3">dans</w> <w n="48.4">mes</w> <w n="48.5">mains</w> <w n="48.6">un</w> <w n="48.7">rameau</w> <w n="48.8">de</w> <w n="48.9">laurier</w> !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Dans</w> <w n="49.2">les</w> <w n="49.3">cercles</w> <w n="49.4">de</w> <w n="49.5">flamme</w> <w n="49.6">où</w> <w n="49.7">frémissent</w> <w n="49.8">leurs</w> <w n="49.9">ailes</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Les</w> <w n="50.2">âmes</w> <w n="50.3">gémissaient</w> <w n="50.4">d</w>’<w n="50.5">avoir</w> <w n="50.6">perdu</w> <w n="50.7">l</w>’<w n="50.8">amour</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Et</w>, <w n="51.2">saisi</w> <w n="51.3">de</w> <w n="51.4">pitié</w> <w n="51.5">pour</w> <w n="51.6">leurs</w> <w n="51.7">douleurs</w> <w n="51.8">mortelles</w>,</l>
					<l n="52" num="13.4"><w n="52.1">J</w>’<w n="52.2">ai</w> <w n="52.3">pleuré</w> <w n="52.4">de</w> <w n="52.5">tristesse</w> <w n="52.6">en</w> <w n="52.7">remontant</w> <w n="52.8">au</w> <w n="52.9">jour</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Peuple</w>, <w n="53.2">j</w>’<w n="53.3">ai</w> <w n="53.4">combattu</w> <w n="53.5">la</w> <w n="53.6">guerrière</w> <w n="53.7">à</w> <w n="53.8">l</w>’<w n="53.9">œil</w> <w n="53.10">louche</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Et</w> <w n="54.2">pour</w> <w n="54.3">briser</w> <w n="54.4">les</w> <w n="54.5">dents</w> <w n="54.6">de</w> <w n="54.7">celle</w> <w n="54.8">qui</w> <w n="54.9">te</w> <w n="54.10">mord</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Couvert</w> <w n="55.2">de</w> <w n="55.3">la</w> <w n="55.4">toison</w> <w n="55.5">d</w>’<w n="55.6">une</w> <w n="55.7">bête</w> <w n="55.8">farouche</w>,</l>
					<l n="56" num="14.4"><w n="56.1">J</w>’<w n="56.2">ai</w> <w n="56.3">lutté</w> <w n="56.4">sur</w> <w n="56.5">le</w> <w n="56.6">sable</w> <w n="56.7">avec</w> <w n="56.8">la</w> <w n="56.9">froide</w> <w n="56.10">Mort</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Et</w> <w n="57.2">lorsque</w> <w n="57.3">enfin</w> <w n="57.4">meurtrie</w>, <w n="57.5">haletante</w> <w n="57.6">et</w> <w n="57.7">lassée</w>,</l>
					<l n="58" num="15.2"><w n="58.1">Elle</w> <w n="58.2">a</w> <w n="58.3">demandé</w> <w n="58.4">grâce</w> <w n="58.5">en</w> <w n="58.6">secouant</w> <w n="58.7">ses</w> <w n="58.8">fers</w>,</l>
					<l n="59" num="15.3"><w n="59.1">J</w>’<w n="59.2">ai</w> <w n="59.3">repris</w> <w n="59.4">dans</w> <w n="59.5">ses</w> <w n="59.6">bras</w> <w n="59.7">la</w> <w n="59.8">douce</w> <w n="59.9">fiancée</w></l>
					<l n="60" num="15.4"><w n="60.1">Qu</w>’<w n="60.2">elle</w> <w n="60.3">emportait</w> <w n="60.4">déjà</w> <w n="60.5">vers</w> <w n="60.6">la</w> <w n="60.7">nuit</w> <w n="60.8">des</w> <w n="60.9">enfers</w>.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Pour</w> <w n="61.2">rendre</w> <w n="61.3">l</w>’<w n="61.4">ennemie</w> <w n="61.5">encor</w> <w n="61.6">plus</w> <w n="61.7">odieuse</w>,</l>
					<l n="62" num="16.2"><w n="62.1">C</w>’<w n="62.2">est</w> <w n="62.3">moi</w> <w n="62.4">qui</w>, <w n="62.5">de</w> <w n="62.6">la</w> <w n="62.7">lyre</w> <w n="62.8">épandant</w> <w n="62.9">les</w> <w n="62.10">sanglots</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Ai</w> <w n="63.2">fait</w> <w n="63.3">sortir</w> <w n="63.4">charmante</w> <w n="63.5">et</w> <w n="63.6">blonde</w> <w n="63.7">et</w> <w n="63.8">radieuse</w>,</l>
					<l n="64" num="16.4"><w n="64.1">L</w>’<w n="64.2">immortelle</w> <w n="64.3">Beauté</w> <w n="64.4">de</w> <w n="64.5">l</w>’<w n="64.6">écume</w> <w n="64.7">des</w> <w n="64.8">flots</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">C</w>’<w n="65.2">est</w> <w n="65.3">moi</w> <w n="65.4">qui</w>, <w n="65.5">pour</w> <w n="65.6">complaire</w> <w n="65.7">à</w> <w n="65.8">la</w> <w n="65.9">terre</w> <w n="65.10">charmée</w>,</l>
					<l n="66" num="17.2"><w n="66.1">Ai</w> <w n="66.2">conquis</w> <w n="66.3">tout</w> <w n="66.4">un</w> <w n="66.5">monde</w> <w n="66.6">avec</w> <w n="66.7">un</w> <w n="66.8">fruit</w> <w n="66.9">vermeil</w> ;</l>
					<l n="67" num="17.3"><w n="67.1">Des</w> <w n="67.2">femmes</w> <w n="67.3">au</w> <w n="67.4">sein</w> <w n="67.5">nu</w> <w n="67.6">composaient</w> <w n="67.7">mon</w> <w n="67.8">armée</w>,</l>
					<l n="68" num="17.4"><w n="68.1">Et</w> <w n="68.2">j</w>’<w n="68.3">ai</w> <w n="68.4">porté</w> <w n="68.5">la</w> <w n="68.6">vigne</w> <w n="68.7">au</w> <w n="68.8">pays</w> <w n="68.9">du</w> <w n="68.10">soleil</w>.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">O</w> <w n="69.2">foule</w> ! <w n="69.3">né</w> <w n="69.4">chétif</w> <w n="69.5">dans</w> <w n="69.6">le</w> <w n="69.7">troupeau</w> <w n="69.8">des</w> <w n="69.9">hommes</w>,</l>
					<l n="70" num="18.2"><w n="70.1">Pour</w> <w n="70.2">brouter</w> <w n="70.3">la</w> <w n="70.4">verdure</w> <w n="70.5">et</w> <w n="70.6">ramasser</w> <w n="70.7">des</w> <w n="70.8">glands</w>,</l>
					<l n="71" num="18.3"><w n="71.1">Moi</w>, <w n="71.2">qui</w> <w n="71.3">ne</w> <w n="71.4">vous</w> <w n="71.5">semblais</w> <w n="71.6">pas</w> <w n="71.7">plus</w> <w n="71.8">que</w> <w n="71.9">nous</w> <w n="71.10">ne</w> <w n="71.11">sommes</w>,</l>
					<l n="72" num="18.4"><w n="72.1">J</w>’<w n="72.2">ai</w> <w n="72.3">détaché</w> <w n="72.4">les</w> <w n="72.5">Dieux</w> <w n="72.6">de</w> <w n="72.7">leurs</w> <w n="72.8">gibets</w> <w n="72.9">sanglants</w> !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Dans</w> <w n="73.2">une</w> <w n="73.3">eau</w> <w n="73.4">de</w> <w n="73.5">cristal</w> <w n="73.6">j</w>’<w n="73.7">ai</w> <w n="73.8">lavé</w> <w n="73.9">leurs</w> <w n="73.10">blessures</w>.</l>
					<l n="74" num="19.2"><w n="74.1">Ils</w> <w n="74.2">marchent</w> <w n="74.3">maintenant</w> <w n="74.4">libres</w> <w n="74.5">sous</w> <w n="74.6">le</w> <w n="74.7">ciel</w> <w n="74.8">bleu</w>,</l>
					<l n="75" num="19.3"><w n="75.1">Portant</w> <w n="75.2">la</w> <w n="75.3">pourpre</w> <w n="75.4">et</w> <w n="75.5">l</w>’<w n="75.6">or</w> <w n="75.7">sur</w> <w n="75.8">leurs</w> <w n="75.9">belles</w> <w n="75.10">chaussures</w>,</l>
					<l n="76" num="19.4"><w n="76.1">Et</w> <w n="76.2">le</w> <w n="76.3">front</w> <w n="76.4">couronné</w> <w n="76.5">par</w> <w n="76.6">les</w> <w n="76.7">rayons</w> <w n="76.8">du</w> <w n="76.9">feu</w> !</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">Tel</w> <w n="77.2">le</w> <w n="77.3">poëte</w> <w n="77.4">parle</w> <w n="77.5">au</w> <w n="77.6">passant</w> <w n="77.7">toujours</w> <w n="77.8">ivre</w>,</l>
					<l n="78" num="20.2"><w n="78.1">Lorsque</w> <w n="78.2">de</w> <w n="78.3">son</w> <w n="78.4">supplice</w> <w n="78.5">on</w> <w n="78.6">hâte</w> <w n="78.7">les</w> <w n="78.8">apprêts</w>.</l>
					<l n="79" num="20.3"><w n="79.1">Il</w> <w n="79.2">lui</w> <w n="79.3">dit</w> : <w n="79.4">Vois</w> <w n="79.5">ce</w> <w n="79.6">sein</w> <w n="79.7">ouvert</w> <w n="79.8">qui</w> <w n="79.9">t</w>’<w n="79.10">a</w> <w n="79.11">fait</w> <w n="79.12">vivre</w> !</l>
					<l n="80" num="20.4"><w n="80.1">Mais</w> <w n="80.2">le</w> <w n="80.3">passant</w> <w n="80.4">lui</w> <w n="80.5">crie</w> <w n="80.6">encore</w> : <w n="80.7">Après</w> ? — <w n="80.8">Après</w> !</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1"><w n="81.1">Écoute</w> <w n="81.2">cependant</w>, <w n="81.3">spectateur</w> <w n="81.4">à</w> <w n="81.5">l</w>’<w n="81.6">œil</w> <w n="81.7">vide</w> !</l>
					<l n="82" num="21.2"><w n="82.1">Toi</w> <w n="82.2">pour</w> <w n="82.3">qui</w> <w n="82.4">c</w>’<w n="82.5">est</w> <w n="82.6">trop</w> <w n="82.7">peu</w>, <w n="82.8">dans</w> <w n="82.9">ton</w> <w n="82.10">dédain</w> <w n="82.11">jaloux</w>,</l>
					<l n="83" num="21.3"><w n="83.1">De</w> <w n="83.2">toucher</w> <w n="83.3">sur</w> <w n="83.4">ses</w> <w n="83.5">pieds</w> <w n="83.6">et</w> <w n="83.7">sur</w> <w n="83.8">son</w> <w n="83.9">flanc</w> <w n="83.10">livide</w></l>
					<l n="84" num="21.4"><w n="84.1">Le</w> <w n="84.2">trou</w> <w n="84.3">qu</w>’<w n="84.4">a</w> <w n="84.5">fait</w> <w n="84.6">la</w> <w n="84.7">lance</w> <w n="84.8">et</w> <w n="84.9">les</w> <w n="84.10">traces</w> <w n="84.11">des</w> <w n="84.12">clous</w> !</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1"><w n="85.1">Lorsque</w> <w n="85.2">le</w> <w n="85.3">pélican</w> <w n="85.4">ouvre</w> <w n="85.5">sa</w> <w n="85.6">chair</w> <w n="85.7">vivante</w></l>
					<l n="86" num="22.2"><w n="86.1">Pour</w> <w n="86.2">nourrir</w> <w n="86.3">ses</w> <w n="86.4">petits</w>, <w n="86.5">et</w> <w n="86.6">qu</w>’<w n="86.7">ils</w> <w n="86.8">mordent</w> <w n="86.9">son</w> <w n="86.10">flanc</w>,</l>
					<l n="87" num="22.3"><w n="87.1">Avec</w> <w n="87.2">une</w> <w n="87.3">douceur</w> <w n="87.4">dont</w> <w n="87.5">l</w>’<w n="87.6">homme</w> <w n="87.7">s</w>’<w n="87.8">épouvante</w></l>
					<l n="88" num="22.4"><w n="88.1">Il</w> <w n="88.2">regarde</w> <w n="88.3">leurs</w> <w n="88.4">becs</w> <w n="88.5">tout</w> <w n="88.6">rouges</w> <w n="88.7">de</w> <w n="88.8">son</w> <w n="88.9">sang</w>.</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1"><w n="89.1">Écoute</w> ! <w n="89.2">il</w> <w n="89.3">tombe</w>, <w n="89.4">heureux</w> <w n="89.5">de</w> <w n="89.6">voir</w> <w n="89.7">tous</w> <w n="89.8">ceux</w> <w n="89.9">qu</w>’<w n="89.10">il</w> <w n="89.11">aime</w></l>
					<l n="90" num="23.2"><w n="90.1">Bien</w> <w n="90.2">vivants</w> <w n="90.3">par</w> <w n="90.4">sa</w> <w n="90.5">mort</w> <w n="90.6">et</w> <w n="90.7">bien</w> <w n="90.8">rassasiés</w> ;</l>
					<l n="91" num="23.3"><w n="91.1">Mais</w> <w n="91.2">que</w> <w n="91.3">penserait</w>-<w n="91.4">il</w> <w n="91.5">à</w> <w n="91.6">cette</w> <w n="91.7">heure</w> <w n="91.8">suprême</w></l>
					<l n="92" num="23.4"><w n="92.1">En</w> <w n="92.2">fermant</w> <w n="92.3">vers</w> <w n="92.4">le</w> <w n="92.5">ciel</w> <w n="92.6">ses</w> <w n="92.7">yeux</w> <w n="92.8">extasiés</w> ;</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1"><w n="93.1">Quelle</w> <w n="93.2">angoisse</w> <w n="93.3">tordrait</w> <w n="93.4">cette</w> <w n="93.5">pure</w> <w n="93.6">victime</w></l>
					<l n="94" num="24.2"><w n="94.1">Si</w>, <w n="94.2">lorsqu</w>’<w n="94.3">elle</w> <w n="94.4">agonise</w> <w n="94.5">et</w> <w n="94.6">qu</w>’<w n="94.7">elle</w> <w n="94.8">expire</w> <w n="94.9">enfin</w>,</l>
					<l n="95" num="24.3"><w n="95.1">Tout</w> <w n="95.2">gonflés</w> <w n="95.3">et</w> <w n="95.4">repus</w> <w n="95.5">de</w> <w n="95.6">son</w> <w n="95.7">cœur</w> <w n="95.8">magnanime</w>,</l>
					<l n="96" num="24.4"><w n="96.1">Ses</w> <w n="96.2">petits</w> <w n="96.3">lui</w> <w n="96.4">disaient</w> : <w n="96.5">Nous</w> <w n="96.6">avons</w> <w n="96.7">encor</w> <w n="96.8">faim</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1849">Février 1849.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>