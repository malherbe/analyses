<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LE SANG DE LA COUPE</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3154 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_8</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LE SANG DE LA COUPE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillelesangdelacoupe.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres de Théodore de Banville. Le sang de la Coupe, Trente-six Ballades joyeuses, Le Baiser.</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1857">1857</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1857">1857</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN355">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Homme</w>, <w n="1.2">tu</w> <w n="1.3">peux</w> <w n="1.4">faucher</w>, <w n="1.5">par</w> <w n="1.6">un</w> <w n="1.7">sombre</w> <w n="1.8">désastre</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">arbres</w> <w n="2.3">chevelus</w> ; <w n="2.4">tu</w> <w n="2.5">fais</w> <w n="2.6">obéir</w> <w n="2.7">l</w>’<w n="2.8">astre</w></l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">le</w> <w n="3.3">flot</w> ; <w n="3.4">ta</w> <w n="3.5">pensée</w> <w n="3.6">orageuse</w> <w n="3.7">dans</w> <w n="3.8">l</w>’<w n="3.9">air</w></l>
					<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">élance</w> <w n="4.3">avec</w> <w n="4.4">le</w> <w n="4.5">vol</w> <w n="4.6">furieux</w> <w n="4.7">de</w> <w n="4.8">l</w>’<w n="4.9">éclair</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Et</w>, <w n="5.2">nautonier</w>, <w n="5.3">tu</w> <w n="5.4">prends</w> <w n="5.5">les</w> <w n="5.6">cieux</w> <w n="5.7">à</w> <w n="5.8">l</w>’<w n="5.9">abordage</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Cependant</w>, <w n="6.2">le</w> <w n="6.3">plus</w> <w n="6.4">clair</w> <w n="6.5">de</w> <w n="6.6">ton</w> <w n="6.7">vaste</w> <w n="6.8">héritage</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Ce</w> <w n="7.2">que</w> <w n="7.3">tu</w> <w n="7.4">sauveras</w> <w n="7.5">de</w> <w n="7.6">cent</w> <w n="7.7">débris</w> <w n="7.8">flottants</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">trésor</w> <w n="8.3">qui</w> <w n="8.4">te</w> <w n="8.5">reste</w> <w n="8.6">en</w> <w n="8.7">somme</w>, <w n="8.8">et</w> <w n="8.9">que</w> <w n="8.10">le</w> <w n="8.11">Temps</w></l>
					<l n="9" num="1.9"><w n="9.1">Ne</w> <w n="9.2">dispersera</w> <w n="9.3">pas</w> <w n="9.4">avec</w> <w n="9.5">sa</w> <w n="9.6">rude</w> <w n="9.7">haleine</w>,</l>
					<l n="10" num="1.10"><w n="10.1">O</w> <w n="10.2">vainqueur</w> <w n="10.3">des</w> <w n="10.4">soleils</w>, <w n="10.5">c</w>’<w n="10.6">est</w> <w n="10.7">la</w> <w n="10.8">gloire</w> <w n="10.9">d</w>’<w n="10.10">Hélène</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">divin</w> <w n="11.3">Péléide</w> <w n="11.4">en</w> <w n="11.5">pleurs</w> <w n="11.6">pour</w> <w n="11.7">Briséis</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">le</w> <w n="12.3">vieux</w> <w n="12.4">sang</w> <w n="12.5">qui</w> <w n="12.6">fume</w> <w n="12.7">au</w> <w n="12.8">bord</w> <w n="12.9">du</w> <w n="12.10">Simoïs</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1846">Juin 1846.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>