<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN173">
				<head type="main">LES JARDINS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Parfois</w>, <w n="1.2">lorsque</w> <w n="1.3">mon</w> <w n="1.4">âme</w> <w n="1.5">échappe</w> <w n="1.6">aux</w> <w n="1.7">soins</w> <w n="1.8">jaloux</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">revois</w> <w n="2.3">dans</w> <w n="2.4">un</w> <w n="2.5">songe</w> <w n="2.6">épouvantable</w> <w n="2.7">et</w> <w n="2.8">doux</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Plein</w> <w n="3.2">d</w>’<w n="3.3">ombre</w> <w n="3.4">et</w> <w n="3.5">de</w> <w n="3.6">silence</w> <w n="3.7">et</w> <w n="3.8">d</w>’<w n="3.9">épaisses</w> <w n="3.10">ramées</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">jardins</w> <w n="4.3">où</w> <w n="4.4">jadis</w> <w n="4.5">passaient</w> <w n="4.6">mes</w> <w n="4.7">bien</w>-<w n="4.8">aimées</w>.</l>
					<l n="5" num="1.5"><space quantity="2" unit="char"></space><w n="5.1">Mais</w> <w n="5.2">voici</w> <w n="5.3">qu</w>’<w n="5.4">à</w> <w n="5.5">présent</w> <w n="5.6">les</w> <w n="5.7">rosiers</w> <w n="5.8">chevelus</w></l>
					<l n="6" num="1.6"><w n="6.1">Sont</w> <w n="6.2">devenus</w> <w n="6.3">broussaille</w> <w n="6.4">et</w> <w n="6.5">ne</w> <w n="6.6">fleurissent</w> <w n="6.7">plus</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">temps</w> <w n="7.3">a</w> <w n="7.4">fracassé</w> <w n="7.5">le</w> <w n="7.6">marbre</w> <w n="7.7">blanc</w> <w n="7.8">des</w> <w n="7.9">urnes</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">rossignol</w> <w n="8.3">a</w> <w n="8.4">fui</w> <w n="8.5">les</w> <w n="8.6">chênes</w> <w n="8.7">taciturnes</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Les</w> <w n="9.2">nymphes</w> <w n="9.3">de</w> <w n="9.4">Coustou</w>, <w n="9.5">les</w> <w n="9.6">sylvains</w> <w n="9.7">et</w> <w n="9.8">les</w> <w n="9.9">pans</w></l>
					<l n="10" num="1.10"><w n="10.1">S</w>’<w n="10.2">affaissent</w> <w n="10.3">éperdus</w> <w n="10.4">sous</w> <w n="10.5">les</w> <w n="10.6">lierres</w> <w n="10.7">rampants</w> ;</l>
					<l n="11" num="1.11"><w n="11.1">La</w> <w n="11.2">flouve</w>, <w n="11.3">le</w> <w n="11.4">vulpin</w>, <w n="11.5">les</w> <w n="11.6">herbes</w> <w n="11.7">désolées</w></l>
					<l n="12" num="1.12"><w n="12.1">Ont</w> <w n="12.2">envahi</w> <w n="12.3">partout</w> <w n="12.4">le</w> <w n="12.5">sable</w> <w n="12.6">des</w> <w n="12.7">allées</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Les</w> <w n="13.2">larges</w> <w n="13.3">tapis</w> <w n="13.4">d</w>’<w n="13.5">herbe</w> <w n="13.6">aux</w> <w n="13.7">haleines</w> <w n="13.8">de</w> <w n="13.9">thym</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Où</w> <w n="14.2">la</w> <w n="14.3">lune</w> <w n="14.4">éclairait</w> <w n="14.5">les</w> <w n="14.6">habits</w> <w n="14.7">de</w> <w n="14.8">satin</w></l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">les</w> <w n="15.3">pierres</w> <w n="15.4">de</w> <w n="15.5">flamme</w> <w n="15.6">aux</w> <w n="15.7">robes</w> <w n="15.8">assorties</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Foisonnent</w> <w n="16.2">maintenant</w> <w n="16.3">de</w> <w n="16.4">ronces</w> <w n="16.5">et</w> <w n="16.6">d</w>’<w n="16.7">orties</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">Dans</w> <w n="17.2">les</w> <w n="17.3">bassins</w>, <w n="17.4">les</w> <w n="17.5">flots</w> <w n="17.6">aux</w> <w n="17.7">sourires</w> <w n="17.8">blafards</w></l>
					<l n="18" num="1.18"><w n="18.1">Sont</w> <w n="18.2">cachés</w> <w n="18.3">par</w> <w n="18.4">la</w> <w n="18.5">mousse</w> <w n="18.6">et</w> <w n="18.7">par</w> <w n="18.8">les</w> <w n="18.9">nénufars</w> ;</l>
					<l n="19" num="1.19"><w n="19.1">L</w>’<w n="19.2">étang</w>, <w n="19.3">où</w> <w n="19.4">tout</w> <w n="19.5">un</w> <w n="19.6">monde</w> <w n="19.7">effroyable</w> <w n="19.8">pullule</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Ne</w> <w n="20.2">voit</w> <w n="20.3">plus</w> <w n="20.4">sur</w> <w n="20.5">ses</w> <w n="20.6">joncs</w> <w n="20.7">frémir</w> <w n="20.8">de</w> <w n="20.9">libellule</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Le</w> <w n="21.2">chaume</w> <w n="21.3">est</w> <w n="21.4">tout</w> <w n="21.5">couvert</w> <w n="21.6">d</w>’<w n="21.7">iris</w> ; <w n="21.8">les</w> <w n="21.9">églantiers</w></l>
					<l n="22" num="1.22"><w n="22.1">Pendent</w>, <w n="22.2">et</w> <w n="22.3">de</w> <w n="22.4">leurs</w> <w n="22.5">bras</w> <w n="22.6">couvrent</w> <w n="22.7">des</w> <w n="22.8">murs</w> <w n="22.9">entiers</w> ;</l>
					<l n="23" num="1.23"><w n="23.1">L</w>’<w n="23.2">ombre</w> <w n="23.3">triste</w>, <w n="23.4">le</w> <w n="23.5">houx</w> <w n="23.6">luisant</w>, <w n="23.7">les</w> <w n="23.8">eaux</w> <w n="23.9">dormantes</w></l>
					<l n="24" num="1.24"><w n="24.1">Ont</w> <w n="24.2">pris</w> <w n="24.3">les</w> <w n="24.4">oasis</w> <w n="24.5">où</w> <w n="24.6">riaient</w> <w n="24.7">mes</w> <w n="24.8">amantes</w> ;</l>
					<l n="25" num="1.25"><w n="25.1">La</w> <w n="25.2">noire</w> <w n="25.3">frondaison</w> <w n="25.4">me</w> <w n="25.5">dérobe</w> <w n="25.6">les</w> <w n="25.7">cieux</w></l>
					<l n="26" num="1.26"><w n="26.1">Qu</w>’<w n="26.2">elles</w> <w n="26.3">aimaient</w>, <w n="26.4">et</w> <w n="26.5">dans</w> <w n="26.6">ces</w> <w n="26.7">lieux</w> <w n="26.8">délicieux</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Naguère</w> <w n="27.2">tout</w> <w n="27.3">remplis</w> <w n="27.4">d</w>’<w n="27.5">enchantements</w> <w n="27.6">par</w> <w n="27.7">elles</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Meurt</w> <w n="28.2">le</w> <w n="28.3">gémissement</w> <w n="28.4">affreux</w> <w n="28.5">des</w> <w n="28.6">tourterelles</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1860">Nice, mai 1860</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>