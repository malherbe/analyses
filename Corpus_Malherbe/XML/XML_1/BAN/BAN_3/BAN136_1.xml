<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN136">
				<head type="main">LE SANGLIER</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">était</w> <w n="1.3">auprès</w> <w n="1.4">d</w>’<w n="1.5">un</w> <w n="1.6">lac</w> <w n="1.7">sinistre</w>, <w n="1.8">à</w> <w n="1.9">l</w>’<w n="1.10">eau</w> <w n="1.11">dormante</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Enfermé</w> <w n="2.2">dans</w> <w n="2.3">un</w> <w n="2.4">pli</w> <w n="2.5">du</w> <w n="2.6">grand</w> <w n="2.7">mont</w> <w n="2.8">Érymanthe</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">l</w>’<w n="3.3">antre</w> <w n="3.4">paraissait</w> <w n="3.5">gémir</w>, <w n="3.6">et</w>, <w n="3.7">tout</w> <w n="3.8">béant</w>,</l>
					<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">ouvrait</w>, <w n="4.3">comme</w> <w n="4.4">une</w> <w n="4.5">gueule</w> <w n="4.6">affreuse</w> <w n="4.7">du</w> <w n="4.8">néant</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Des</w> <w n="5.2">vapeurs</w> <w n="5.3">en</w> <w n="5.4">sortaient</w>, <w n="5.5">ainsi</w> <w n="5.6">que</w> <w n="5.7">d</w>’<w n="5.8">un</w> <w n="5.9">Averne</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Immobile</w>, <w n="6.2">et</w> <w n="6.3">penché</w> <w n="6.4">pour</w> <w n="6.5">voir</w> <w n="6.6">dans</w> <w n="6.7">la</w> <w n="6.8">caverne</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Hercule</w> <w n="7.2">regarda</w> <w n="7.3">le</w> <w n="7.4">sanglier</w> <w n="7.5">hideux</w>.</l>
					<l n="8" num="1.8"><space quantity="2" unit="char"></space><w n="8.1">Les</w> <w n="8.2">loups</w> <w n="8.3">fuyaient</w> <w n="8.4">de</w> <w n="8.5">peur</w> <w n="8.6">quand</w> <w n="8.7">il</w> <w n="8.8">s</w>’<w n="8.9">approchait</w> <w n="8.10">d</w>’<w n="8.11">eux</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Tant</w> <w n="9.2">le</w> <w n="9.3">monstre</w> <w n="9.4">effaré</w>, <w n="9.5">s</w>’<w n="9.6">il</w> <w n="9.7">grognait</w> <w n="9.8">dans</w> <w n="9.9">sa</w> <w n="9.10">joie</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Semblait</w> <w n="10.2">effrayant</w>, <w n="10.3">même</w> <w n="10.4">à</w> <w n="10.5">des</w> <w n="10.6">bêtes</w> <w n="10.7">de</w> <w n="10.8">proie</w>.</l>
					<l n="11" num="1.11"><w n="11.1">Il</w> <w n="11.2">vivait</w> <w n="11.3">là</w>, <w n="11.4">pensif</w>. <w n="11.5">Lorsque</w> <w n="11.6">venait</w> <w n="11.7">la</w> <w n="11.8">nuit</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Terrible</w>, <w n="12.2">emplissant</w> <w n="12.3">l</w>’<w n="12.4">air</w> <w n="12.5">d</w>’<w n="12.6">épouvante</w> <w n="12.7">et</w> <w n="12.8">de</w> <w n="12.9">bruit</w></l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">cassant</w> <w n="13.3">les</w> <w n="13.4">lauriers</w> <w n="13.5">au</w> <w n="13.6">pied</w> <w n="13.7">des</w> <w n="13.8">monts</w> <w n="13.9">sublimes</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Il</w> <w n="14.2">allait</w> <w n="14.3">dans</w> <w n="14.4">le</w> <w n="14.5">bois</w> <w n="14.6">déchirer</w> <w n="14.7">ses</w> <w n="14.8">victimes</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Puis</w> <w n="15.2">il</w> <w n="15.3">rentrait</w> <w n="15.4">dans</w> <w n="15.5">l</w>’<w n="15.6">antre</w>, <w n="15.7">auprès</w> <w n="15.8">des</w> <w n="15.9">flots</w> <w n="15.10">dormants</w></l>
					<l n="16" num="1.16"><w n="16.1">Couché</w> <w n="16.2">sur</w> <w n="16.3">la</w> <w n="16.4">chair</w> <w n="16.5">morte</w> <w n="16.6">et</w> <w n="16.7">sur</w> <w n="16.8">les</w> <w n="16.9">ossements</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Il</w> <w n="17.2">mangeait</w>, <w n="17.3">la</w> <w n="17.4">narine</w> <w n="17.5">ouverte</w> <w n="17.6">et</w> <w n="17.7">dilatée</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">s</w>’<w n="18.3">étendait</w> <w n="18.4">parmi</w> <w n="18.5">la</w> <w n="18.6">boue</w> <w n="18.7">ensanglantée</w>.</l>
					<l n="19" num="1.19"><w n="19.1">Noir</w>, <w n="19.2">sa</w> <w n="19.3">tanière</w> <w n="19.4">au</w> <w n="19.5">front</w> <w n="19.6">obscur</w> <w n="19.7">lui</w> <w n="19.8">ressemblait</w>.</l>
					<l n="20" num="1.20"><w n="20.1">Les</w> <w n="20.2">ténèbres</w> <w n="20.3">et</w> <w n="20.4">lui</w> <w n="20.5">se</w> <w n="20.6">parlaient</w>. <w n="20.7">Il</w> <w n="20.8">semblait</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Enfoui</w> <w n="21.2">dans</w> <w n="21.3">l</w>’<w n="21.4">horreur</w> <w n="21.5">de</w> <w n="21.6">cette</w> <w n="21.7">prison</w> <w n="21.8">sombre</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Qu</w>’<w n="22.2">il</w> <w n="22.3">mangeait</w> <w n="22.4">de</w> <w n="22.5">la</w> <w n="22.6">nuit</w> <w n="22.7">et</w> <w n="22.8">qu</w>’<w n="22.9">il</w> <w n="22.10">mâchait</w> <w n="22.11">de</w> <w n="22.12">l</w>’<w n="22.13">ombre</w>.</l>
					<l n="23" num="1.23"><space quantity="2" unit="char"></space><w n="23.1">Hercule</w>, <w n="23.2">que</w> <w n="23.3">sa</w> <w n="23.4">vue</w> <w n="23.5">importune</w> <w n="23.6">lassait</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Se</w> <w n="24.2">dit</w> : « <w n="24.3">je</w> <w n="24.4">vais</w> <w n="24.5">serrer</w> <w n="24.6">son</w> <w n="24.7">cou</w> <w n="24.8">dans</w> <w n="24.9">un</w> <w n="24.10">lacet</w> ;</l>
					<l n="25" num="1.25"><w n="25.1">Ma</w> <w n="25.2">main</w> <w n="25.3">étouffera</w> <w n="25.4">ses</w> <w n="25.5">grognements</w> <w n="25.6">obscènes</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Et</w> <w n="26.2">je</w> <w n="26.3">l</w>’<w n="26.4">amènerai</w> <w n="26.5">tout</w> <w n="26.6">vivant</w> <w n="26.7">dans</w> <w n="26.8">Mycènes</w>. »</l>
					<l n="27" num="1.27"><w n="27.1">Et</w> <w n="27.2">le</w> <w n="27.3">héros</w> <w n="27.4">disait</w> <w n="27.5">aussi</w> : « <w n="27.6">qui</w> <w n="27.7">sait</w> <w n="27.8">pourtant</w>,</l>
					<l n="28" num="1.28"><w n="28.1">S</w>’<w n="28.2">il</w> <w n="28.3">voyait</w> <w n="28.4">dans</w> <w n="28.5">les</w> <w n="28.6">cieux</w> <w n="28.7">le</w> <w n="28.8">soleil</w> <w n="28.9">éclatant</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Ce</w> <w n="29.2">que</w> <w n="29.3">redeviendrait</w> <w n="29.4">cet</w> <w n="29.5">animal</w> <w n="29.6">farouche</w> ?</l>
					<l n="30" num="1.30"><w n="30.1">Peut</w>-<w n="30.2">être</w> <w n="30.3">que</w> <w n="30.4">les</w> <w n="30.5">dents</w> <w n="30.6">cruelles</w> <w n="30.7">de</w> <w n="30.8">sa</w> <w n="30.9">bouche</w></l>
					<l n="31" num="1.31"><w n="31.1">Baiseraient</w> <w n="31.2">l</w>’<w n="31.3">herbe</w> <w n="31.4">verte</w> <w n="31.5">et</w> <w n="31.6">frémiraient</w> <w n="31.7">d</w>’<w n="31.8">amour</w>,</l>
					<l n="32" num="1.32"><w n="32.1">S</w>’<w n="32.2">il</w> <w n="32.3">regardait</w> <w n="32.4">l</w>’<w n="32.5">azur</w> <w n="32.6">éblouissant</w> <w n="32.7">du</w> <w n="32.8">jour</w> ! »</l>
					<l n="33" num="1.33"><space quantity="2" unit="char"></space><w n="33.1">Alors</w>, <w n="33.2">entrant</w> <w n="33.3">ses</w> <w n="33.4">doigts</w> <w n="33.5">d</w>’<w n="33.6">acier</w> <w n="33.7">parmi</w> <w n="33.8">les</w> <w n="33.9">soies</w></l>
					<l n="34" num="1.34"><w n="34.1">Du</w> <w n="34.2">sanglier</w> <w n="34.3">courbé</w> <w n="34.4">sur</w> <w n="34.5">des</w> <w n="34.6">restes</w> <w n="34.7">de</w> <w n="34.8">proies</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Il</w> <w n="35.2">le</w> <w n="35.3">traîna</w> <w n="35.4">tout</w> <w n="35.5">près</w> <w n="35.6">du</w> <w n="35.7">lac</w> <w n="35.8">dormant</w>. <w n="35.9">En</w> <w n="35.10">vain</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Blessé</w> <w n="36.2">par</w> <w n="36.3">le</w> <w n="36.4">soleil</w> <w n="36.5">qui</w> <w n="36.6">dorait</w> <w n="36.7">le</w> <w n="36.8">ravin</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Le</w> <w n="37.2">monstre</w> <w n="37.3">déchirait</w> <w n="37.4">le</w> <w n="37.5">roc</w> <w n="37.6">de</w> <w n="37.7">ses</w> <w n="37.8">défenses</w>.</l>
					<l n="38" num="1.38"><w n="38.1">Il</w> <w n="38.2">fuyait</w>. <w n="38.3">Souriant</w> <w n="38.4">de</w> <w n="38.5">ces</w> <w n="38.6">faibles</w> <w n="38.7">offenses</w>,</l>
					<l n="39" num="1.39"><w n="39.1">Hercule</w>, <w n="39.2">soulevant</w> <w n="39.3">ses</w> <w n="39.4">flancs</w> <w n="39.5">hideux</w> <w n="39.6">et</w> <w n="39.7">lourds</w>,</l>
					<l n="40" num="1.40"><w n="40.1">Le</w> <w n="40.2">ramenait</w> <w n="40.3">au</w> <w n="40.4">jour</w> <w n="40.5">lumineux</w>. <w n="40.6">Mais</w> <w n="40.7">toujours</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Attiré</w> <w n="41.2">dans</w> <w n="41.3">sa</w> <w n="41.4">nuit</w> <w n="41.5">par</w> <w n="41.6">un</w> <w n="41.7">amour</w> <w n="41.8">étrange</w>,</l>
					<l n="42" num="1.42"><w n="42.1">Le</w> <w n="42.2">sanglier</w> <w n="42.3">têtu</w> <w n="42.4">retournait</w> <w n="42.5">vers</w> <w n="42.6">la</w> <w n="42.7">fange</w>,</l>
					<l n="43" num="1.43"><w n="43.1">Et</w> <w n="43.2">toujours</w>, <w n="43.3">l</w>’<w n="43.4">effrayant</w> <w n="43.5">d</w>’<w n="43.6">un</w> <w n="43.7">sourire</w> <w n="43.8">vermeil</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Le</w> <w n="44.2">héros</w> <w n="44.3">le</w> <w n="44.4">traînait</w> <w n="44.5">de</w> <w n="44.6">force</w> <w n="44.7">au</w> <w n="44.8">grand</w> <w n="44.9">soleil</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1862">décembre 1862</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>