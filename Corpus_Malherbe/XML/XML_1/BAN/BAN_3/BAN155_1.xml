<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN155">
				<head type="main">À MA MÈRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ô</w> <w n="1.2">ma</w> <w n="1.3">mère</w> <w n="1.4">et</w> <w n="1.5">ma</w> <w n="1.6">nourrice</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Toi</w> <w n="2.2">dont</w> <w n="2.3">l</w>’<w n="2.4">âme</w> <w n="2.5">protectrice</w></l>
					<l n="3" num="1.3"><w n="3.1">Me</w> <w n="3.2">fit</w> <w n="3.3">des</w> <w n="3.4">jours</w> <w n="3.5">composés</w></l>
					<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">un</w> <w n="4.3">bonheur</w> <w n="4.4">si</w> <w n="4.5">rare</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">qui</w> <w n="5.3">ne</w> <w n="5.4">me</w> <w n="5.5">fus</w> <w n="5.6">avare</w></l>
					<l n="6" num="1.6"><w n="6.1">Ni</w> <w n="6.2">de</w> <w n="6.3">lait</w> <w n="6.4">ni</w> <w n="6.5">de</w> <w n="6.6">baisers</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Je</w> <w n="7.2">t</w>’<w n="7.3">adore</w>, <w n="7.4">sois</w> <w n="7.5">bénie</w>.</l>
					<l n="8" num="2.2"><w n="8.1">Tu</w> <w n="8.2">berças</w> <w n="8.3">dans</w> <w n="8.4">l</w>’<w n="8.5">harmonie</w></l>
					<l n="9" num="2.3"><w n="9.1">Mon</w> <w n="9.2">esprit</w> <w n="9.3">aventureux</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">loin</w> <w n="10.3">du</w> <w n="10.4">railleur</w> <w n="10.5">frivole</w></l>
					<l n="11" num="2.5"><w n="11.1">Mon</w> <w n="11.2">ode</w> <w n="11.3">aux</w> <w n="11.4">astres</w> <w n="11.5">s</w>’<w n="11.6">envole</w> :</l>
					<l n="12" num="2.6"><w n="12.1">Sois</w> <w n="12.2">fière</w>, <w n="12.3">je</w> <w n="12.4">suis</w> <w n="12.5">heureux</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">vaincu</w> <w n="13.4">l</w>’<w n="13.5">ombre</w> <w n="13.6">et</w> <w n="13.7">le</w> <w n="13.8">doute</w>.</l>
					<l n="14" num="3.2"><w n="14.1">Qu</w>’<w n="14.2">importe</w> <w n="14.3">si</w> <w n="14.4">l</w>’<w n="14.5">on</w> <w n="14.6">écoute</w></l>
					<l n="15" num="3.3"><w n="15.1">Avec</w> <w n="15.2">dédain</w> <w n="15.3">trop</w> <w n="15.4">souvent</w></l>
					<l n="16" num="3.4"><w n="16.1">Ma</w> <w n="16.2">voix</w> <w n="16.3">par</w> <w n="16.4">les</w> <w n="16.5">pleurs</w> <w n="16.6">voilée</w>.</l>
					<l n="17" num="3.5"><w n="17.1">Quand</w> <w n="17.2">sur</w> <w n="17.3">ma</w> <w n="17.4">lyre</w> <w n="17.5">étoilée</w></l>
					<l n="18" num="3.6"><w n="18.1">Tu</w> <w n="18.2">te</w> <w n="18.3">penches</w> <w n="18.4">en</w> <w n="18.5">rêvant</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Va</w>, <w n="19.2">je</w> <w n="19.3">verrai</w> <w n="19.4">sans</w> <w n="19.5">envie</w></l>
					<l n="20" num="4.2"><w n="20.1">Que</w> <w n="20.2">le</w> <w n="20.3">destin</w> <w n="20.4">de</w> <w n="20.5">ma</w> <w n="20.6">vie</w></l>
					<l n="21" num="4.3"><w n="21.1">N</w>’<w n="21.2">ait</w> <w n="21.3">pas</w> <w n="21.4">pu</w> <w n="21.5">se</w> <w n="21.6">marier</w></l>
					<l n="22" num="4.4"><w n="22.1">Aux</w> <w n="22.2">fortunes</w> <w n="22.3">éclatantes</w>,</l>
					<l n="23" num="4.5"><w n="23.1">Pourvu</w> <w n="23.2">que</w> <w n="23.3">tu</w> <w n="23.4">te</w> <w n="23.5">contentes</w></l>
					<l n="24" num="4.6"><w n="24.1">D</w>’<w n="24.2">un</w> <w n="24.3">petit</w> <w n="24.4">brin</w> <w n="24.5">de</w> <w n="24.6">laurier</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1858">16 février 1858</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>