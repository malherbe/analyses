<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN138">
				<head type="main">L’ANTRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">milieu</w> <w n="1.3">d</w>’<w n="1.4">un</w> <w n="1.5">monceau</w> <w n="1.6">de</w> <w n="1.7">roches</w> <w n="1.8">accroupies</w></l>
					<l n="2" num="1.2"><w n="2.1">Sur</w> <w n="2.2">le</w> <w n="2.3">chemin</w> <w n="2.4">qui</w> <w n="2.5">va</w> <w n="2.6">de</w> <w n="2.7">Leuctres</w> <w n="2.8">à</w> <w n="2.9">Thespies</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">antre</w> <w n="3.3">affreux</w> <w n="3.4">s</w>’<w n="3.5">ouvrait</w>, <w n="3.6">sinistre</w>, <w n="3.7">horrible</w> <w n="3.8">à</w> <w n="3.9">voir</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">buissons</w> <w n="4.3">monstrueux</w> <w n="4.4">tombaient</w> <w n="4.5">de</w> <w n="4.6">son</w> <w n="4.7">flanc</w> <w n="4.8">noir</w></l>
					<l n="5" num="1.5"><w n="5.1">Hérissés</w> <w n="5.2">et</w> <w n="5.3">touffus</w> <w n="5.4">comme</w> <w n="5.5">une</w> <w n="5.6">chevelure</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">dans</w> <w n="6.3">la</w> <w n="6.4">pierre</w> <w n="6.5">en</w> <w n="6.6">feu</w>, <w n="6.7">qu</w>’<w n="6.8">une</w> <w n="6.9">rouge</w> <w n="6.10">brûlure</w></l>
					<l n="7" num="1.7"><w n="7.1">Dévore</w>, <w n="7.2">étaient</w> <w n="7.3">gravés</w> <w n="7.4">sur</w> <w n="7.5">son</w> <w n="7.6">front</w> <w n="7.7">ruiné</w></l>
					<l n="8" num="1.8"><w n="8.1">Ces</w> <w n="8.2">mots</w> : « <w n="8.3">ici</w> <w n="8.4">gémit</w> <w n="8.5">l</w>’<w n="8.6">éternel</w> <w n="8.7">condamné</w>. »</l>
					<l n="9" num="1.9"><space quantity="2" unit="char"></space><w n="9.1">Rien</w> <w n="9.2">n</w>’<w n="9.3">obstruait</w> <w n="9.4">le</w> <w n="9.5">seuil</w> <w n="9.6">de</w> <w n="9.7">la</w> <w n="9.8">sombre</w> <w n="9.9">caverne</w>.</l>
					<l n="10" num="1.10"><w n="10.1">Hercule</w> <w n="10.2">entra</w>. <w n="10.3">Dans</w> <w n="10.4">l</w>’<w n="10.5">ombre</w>, <w n="10.6">auprès</w> <w n="10.7">d</w>’<w n="10.8">une</w> <w n="10.9">citerne</w></l>
					<l n="11" num="1.11"><w n="11.1">Dont</w> <w n="11.2">le</w> <w n="11.3">flot</w> <w n="11.4">n</w>’<w n="11.5">a</w> <w n="11.6">jamais</w> <w n="11.7">regardé</w> <w n="11.8">le</w> <w n="11.9">ciel</w> <w n="11.10">bleu</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Sur</w> <w n="12.2">des</w> <w n="12.3">ossements</w> <w n="12.4">d</w>’<w n="12.5">homme</w> <w n="12.6">était</w> <w n="12.7">assis</w> <w n="12.8">un</w> <w n="12.9">Dieu</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Or</w> <w n="13.2">il</w> <w n="13.3">avait</w> <w n="13.4">vécu</w> <w n="13.5">plus</w> <w n="13.6">d</w>’<w n="13.7">ans</w> <w n="13.8">que</w> <w n="13.9">la</w> <w n="13.10">mémoire</w></l>
					<l n="14" num="1.14"><w n="14.1">N</w>’<w n="14.2">en</w> <w n="14.3">rêve</w> ; <w n="14.4">son</w> <w n="14.5">vieux</w> <w n="14.6">crâne</w> <w n="14.7">était</w> <w n="14.8">comme</w> <w n="14.9">l</w>’<w n="14.10">ivoire</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Lui</w>-<w n="15.2">même</w> <w n="15.3">d</w>’<w n="15.4">une</w> <w n="15.5">flèche</w> <w n="15.6">il</w> <w n="15.7">déchirait</w> <w n="15.8">son</w> <w n="15.9">flanc</w> ;</l>
					<l n="16" num="1.16"><w n="16.1">À</w> <w n="16.2">force</w> <w n="16.3">de</w> <w n="16.4">pleurer</w> <w n="16.5">ses</w> <w n="16.6">yeux</w> <w n="16.7">n</w>’<w n="16.8">étaient</w> <w n="16.9">que</w> <w n="16.10">sang</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Il</w> <w n="17.2">semblait</w> <w n="17.3">un</w> <w n="17.4">oiseau</w> <w n="17.5">farouche</w>, <w n="17.6">pris</w> <w n="17.7">au</w> <w n="17.8">piège</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">le</w> <w n="18.3">vent</w> <w n="18.4">frissonnait</w> <w n="18.5">dans</w> <w n="18.6">sa</w> <w n="18.7">barbe</w> <w n="18.8">de</w> <w n="18.9">neige</w>.</l>
					<l n="19" num="1.19"><w n="19.1">Près</w> <w n="19.2">de</w> <w n="19.3">lui</w>, <w n="19.4">devant</w> <w n="19.5">lui</w>, <w n="19.6">partout</w>, <w n="19.7">des</w> <w n="19.8">ossements</w></l>
					<l n="20" num="1.20"><w n="20.1">Blanchissaient</w> <w n="20.2">sur</w> <w n="20.3">le</w> <w n="20.4">sol</w> <w n="20.5">ténébreux</w>. <w n="20.6">Par</w> <w n="20.7">moments</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Un</w> <w n="21.2">grand</w> <w n="21.3">fleuve</w> <w n="21.4">de</w> <w n="21.5">pleurs</w> <w n="21.6">débordait</w> <w n="21.7">son</w> <w n="21.8">œil</w> <w n="21.9">terne</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Et</w> <w n="22.2">le</w> <w n="22.3">beau</w> <w n="22.4">vieillard</w>-<w n="22.5">dieu</w> <w n="22.6">pleurait</w> <w n="22.7">dans</w> <w n="22.8">la</w> <w n="22.9">citerne</w>.</l>
					<l n="23" num="1.23"><space quantity="2" unit="char"></space><w n="23.1">Le</w> <w n="23.2">fils</w> <w n="23.3">d</w>’<w n="23.4">Amphitryon</w> <w n="23.5">fut</w> <w n="23.6">saisi</w> <w n="23.7">de</w> <w n="23.8">pitié</w>.</l>
					<l n="24" num="1.24">« <w n="24.1">Oh</w> ! <w n="24.2">Dit</w>-<w n="24.3">il</w>, <w n="24.4">sombre</w> <w n="24.5">aïeul</w> <w n="24.6">durement</w> <w n="24.7">châtié</w>,</l>
					<l n="25" num="1.25"><w n="25.1">Que</w> <w n="25.2">fais</w>-<w n="25.3">tu</w> <w n="25.4">loin</w> <w n="25.5">du</w> <w n="25.6">ciel</w> <w n="25.7">dont</w> <w n="25.8">notre</w> <w n="25.9">œil</w> <w n="25.10">est</w> <w n="25.11">avide</w> ?</l>
					<l n="26" num="1.26"><w n="26.1">Qui</w> <w n="26.2">te</w> <w n="26.3">retient</w> <w n="26.4">ainsi</w> <w n="26.5">dans</w> <w n="26.6">ce</w> <w n="26.7">cachot</w> <w n="26.8">livide</w> ?</l>
					<l n="27" num="1.27"><w n="27.1">Ton</w> <w n="27.2">désespoir</w> <w n="27.3">est</w>-<w n="27.4">il</w> <w n="27.5">si</w> <w n="27.6">vaste</w> <w n="27.7">et</w> <w n="27.8">si</w> <w n="27.9">profond</w></l>
					<l n="28" num="1.28"><w n="28.1">Que</w> <w n="28.2">tes</w> <w n="28.3">larmes</w> <w n="28.4">aient</w> <w n="28.5">pu</w> <w n="28.6">remplir</w> <w n="28.7">ce</w> <w n="28.8">puits</w> <w n="28.9">sans</w> <w n="28.10">fond</w> ?</l>
					<l n="29" num="1.29"><w n="29.1">Viens</w> <w n="29.2">dans</w> <w n="29.3">la</w> <w n="29.4">plaine</w>, <w n="29.5">où</w> <w n="29.6">sont</w> <w n="29.7">les</w> <w n="29.8">ruisseaux</w> <w n="29.9">et</w> <w n="29.10">les</w> <w n="29.11">chênes</w> !</l>
					<l n="30" num="1.30"><w n="30.1">Sur</w> <w n="30.2">tes</w> <w n="30.3">bras</w> <w n="30.4">affaiblis</w> <w n="30.5">je</w> <w n="30.6">ne</w> <w n="30.7">vois</w> <w n="30.8">pas</w> <w n="30.9">de</w> <w n="30.10">chaînes</w>.</l>
					<l n="31" num="1.31"><w n="31.1">D</w>’<w n="31.2">ailleurs</w>, <w n="31.3">je</w> <w n="31.4">suis</w> <w n="31.5">celui</w> <w n="31.6">qui</w> <w n="31.7">les</w> <w n="31.8">brise</w> ; <w n="31.9">je</w> <w n="31.10">puis</w>,</l>
					<l n="32" num="1.32"><w n="32.1">Si</w> <w n="32.2">tu</w> <w n="32.3">le</w> <w n="32.4">veux</w>, <w n="32.5">jeter</w> <w n="32.6">ce</w> <w n="32.7">rocher</w> <w n="32.8">dans</w> <w n="32.9">ce</w> <w n="32.10">puits</w> ;</l>
					<l n="33" num="1.33"><w n="33.1">Quelque</w> <w n="33.2">dieu</w> <w n="33.3">qu</w>’<w n="33.4">ait</w> <w n="33.5">maudit</w> <w n="33.6">ta</w> <w n="33.7">bouche</w> <w n="33.8">révoltée</w>,</l>
					<l n="34" num="1.34"><w n="34.1">Je</w> <w n="34.2">te</w> <w n="34.3">délivrerai</w>, <w n="34.4">fusses</w>-<w n="34.5">tu</w> <w n="34.6">Prométhée</w> ! »</l>
					<l n="35" num="1.35"><space quantity="2" unit="char"></space><w n="35.1">Le</w> <w n="35.2">vieillard</w> <w n="35.3">exhalait</w> <w n="35.4">des</w> <w n="35.5">sanglots</w> <w n="35.6">étouffants</w>.</l>
					<l n="36" num="1.36"><w n="36.1">Hercule</w> <w n="36.2">dit</w> : « <w n="36.3">suis</w>-<w n="36.4">moi</w>, <w n="36.5">laisse</w> <w n="36.6">aux</w> <w n="36.7">petits</w> <w n="36.8">enfants</w></l>
					<l n="37" num="1.37"><w n="37.1">Cette</w> <w n="37.2">lâche</w> <w n="37.3">terreur</w> <w n="37.4">et</w> <w n="37.5">cette</w> <w n="37.6">angoisse</w> <w n="37.7">folle</w>.</l>
					<l n="38" num="1.38"><w n="38.1">Il</w> <w n="38.2">n</w>’<w n="38.3">est</w> <w n="38.4">pas</w> <w n="38.5">de</w> <w n="38.6">douleur</w> <w n="38.7">qu</w>’<w n="38.8">un</w> <w n="38.9">ami</w> <w n="38.10">ne</w> <w n="38.11">console</w> ;</l>
					<l n="39" num="1.39"><w n="39.1">Viens</w> <w n="39.2">avec</w> <w n="39.3">moi</w>, <w n="39.4">remonte</w> <w n="39.5">à</w> <w n="39.6">la</w> <w n="39.7">clarté</w> <w n="39.8">du</w> <w n="39.9">jour</w> !</l>
					<l n="40" num="1.40">—<w n="40.1">Non</w>, <w n="40.2">répondit</w> <w n="40.3">le</w> <w n="40.4">grand</w> <w n="40.5">vaincu</w>, <w n="40.6">je</w> <w n="40.7">suis</w> <w n="40.8">l</w>’<w n="40.9">amour</w>. »</l>
				</lg>
				<closer>
					<dateline>
						<date when="1863">janvier 1863</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>