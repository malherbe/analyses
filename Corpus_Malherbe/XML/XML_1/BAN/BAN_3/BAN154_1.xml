<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN154">
				<head type="main">LE PANTIN DE LA PETITE JEANNE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">À</w> <w n="1.2">présent</w>, <w n="1.3">le</w> <w n="1.4">pantin</w> <w n="1.5">est</w> <w n="1.6">accroché</w> <w n="1.7">devant</w></l>
					<l n="2" num="1.2"><w n="2.1">Votre</w> <w n="2.2">table</w>. <w n="2.3">Il</w> <w n="2.4">est</w> <w n="2.5">là</w>, <w n="2.6">bien</w> <w n="2.7">tranquille</w>, <w n="2.8">et</w> <w n="2.9">souvent</w></l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">sourit</w>. <w n="3.3">On</w> <w n="3.4">l</w>’<w n="3.5">a</w> <w n="3.6">fait</w> <w n="3.7">avec</w> <w n="3.8">une</w> <w n="3.9">poupée</w></l>
					<l n="4" num="1.4"><w n="4.1">Habillée</w> <w n="4.2">en</w> <w n="4.3">Pierrot</w>. <w n="4.4">Sa</w> <w n="4.5">taille</w> <w n="4.6">est</w> <w n="4.7">bien</w> <w n="4.8">drapée</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Puis</w> <w n="5.2">il</w> <w n="5.3">est</w> <w n="5.4">gracieux</w> <w n="5.5">comme</w> <w n="5.6">le</w> <w n="5.7">jour</w> <w n="5.8">qui</w> <w n="5.9">naît</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Il</w> <w n="6.2">songe</w>, <w n="6.3">avec</w> <w n="6.4">des</w> <w n="6.5">yeux</w> <w n="6.6">bleu</w> <w n="6.7">sombre</w>. <w n="6.8">Si</w> <w n="6.9">ce</w> <w n="6.10">n</w>’<w n="6.11">est</w></l>
					<l n="7" num="1.7"><w n="7.1">Que</w> <w n="7.2">les</w> <w n="7.3">rubans</w>, <w n="7.4">les</w> <w n="7.5">nœuds</w> <w n="7.6">d</w>’<w n="7.7">amour</w> <w n="7.8">et</w> <w n="7.9">les</w> <w n="7.10">bouffettes</w></l>
					<l n="8" num="1.8"><w n="8.1">De</w> <w n="8.2">son</w> <w n="8.3">habit</w> <w n="8.4">sont</w> <w n="8.5">bleus</w>, <w n="8.6">et</w> <w n="8.7">ses</w> <w n="8.8">deux</w> <w n="8.9">lèvres</w> <w n="8.10">faites</w></l>
					<l n="9" num="1.9"><w n="9.1">En</w> <w n="9.2">vermillon</w>, <w n="9.3">il</w> <w n="9.4">est</w> <w n="9.5">tout</w> <w n="9.6">blanc</w>, <w n="9.7">comme</w> <w n="9.8">l</w>’<w n="9.9">hiver</w>.</l>
					<l n="10" num="1.10"><space quantity="2" unit="char"></space><w n="10.1">À</w> <w n="10.2">son</w> <w n="10.3">petit</w> <w n="10.4">chapeau</w> <w n="10.5">tient</w> <w n="10.6">un</w> <w n="10.7">anneau</w> <w n="10.8">de</w> <w n="10.9">fer</w></l>
					<l n="11" num="1.11"><w n="11.1">Pour</w> <w n="11.2">qu</w>’<w n="11.3">on</w> <w n="11.4">puisse</w> <w n="11.5">le</w> <w n="11.6">pendre</w> <w n="11.7">avec</w> <w n="11.8">un</w> <w n="11.9">fil</w>. <w n="11.10">Sa</w> <w n="11.11">face</w></l>
					<l n="12" num="1.12"><w n="12.1">Est</w> <w n="12.2">d</w>’<w n="12.3">un</w> <w n="12.4">rose</w> <w n="12.5">charmant</w> <w n="12.6">que</w> <w n="12.7">jamais</w> <w n="12.8">rien</w> <w n="12.9">n</w>’<w n="12.10">efface</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">l</w>’<w n="13.3">habit</w> <w n="13.4">est</w> <w n="13.5">de</w> <w n="13.6">neige</w> <w n="13.7">et</w> <w n="13.8">les</w> <w n="13.9">agréments</w> <w n="13.10">bleus</w>.</l>
					<l n="14" num="1.14"><w n="14.1">Il</w> <w n="14.2">garde</w> <w n="14.3">la</w> <w n="14.4">douceur</w> <w n="14.5">des</w> <w n="14.6">êtres</w> <w n="14.7">fabuleux</w> :</l>
					<l n="15" num="1.15"><w n="15.1">Il</w> <w n="15.2">est</w> <w n="15.3">sérieux</w>, <w n="15.4">mais</w> <w n="15.5">avec</w> <w n="15.6">un</w> <w n="15.7">air</w> <w n="15.8">de</w> <w n="15.9">fête</w>.</l>
					<l n="16" num="1.16"><w n="16.1">Il</w> <w n="16.2">est</w> <w n="16.3">blanc</w>. <w n="16.4">Ses</w> <w n="16.5">cheveux</w>, <w n="16.6">qui</w> <w n="16.7">volent</w> <w n="16.8">sur</w> <w n="16.9">sa</w> <w n="16.10">tête</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Sont</w> <w n="17.2">blancs</w> <w n="17.3">aussi</w>, <w n="17.4">naïve</w> <w n="17.5">innocence</w> <w n="17.6">des</w> <w n="17.7">jeux</w> !</l>
					<l n="18" num="1.18"><w n="18.1">Ils</w> <w n="18.2">sont</w> <w n="18.3">en</w> <w n="18.4">ouate</w> ; <w n="18.5">ils</w> <w n="18.6">font</w> <w n="18.7">comme</w> <w n="18.8">un</w> <w n="18.9">ciel</w> <w n="18.10">nuageux</w></l>
					<l n="19" num="1.19"><w n="19.1">Sous</w> <w n="19.2">le</w> <w n="19.3">chapeau</w> <w n="19.4">pointu</w> <w n="19.5">qui</w> <w n="19.6">lui</w> <w n="19.7">couvre</w> <w n="19.8">le</w> <w n="19.9">crâne</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">c</w>’<w n="20.3">était</w> <w n="20.4">le</w> <w n="20.5">joujou</w> <w n="20.6">de</w> <w n="20.7">la</w> <w n="20.8">petite</w> <w n="20.9">Jeanne</w>.</l>
					<l n="21" num="1.21"><space quantity="2" unit="char"></space><w n="21.1">Oh</w> ! <w n="21.2">Je</w> <w n="21.3">vous</w> <w n="21.4">tresse</w>, <w n="21.5">fleurs</w> <w n="21.6">pâles</w> <w n="21.7">du</w> <w n="21.8">souvenir</w> !</l>
					<l n="22" num="1.22"><w n="22.1">Elle</w> <w n="22.2">n</w>’<w n="22.3">aurait</w> <w n="22.4">pas</w> <w n="22.5">eu</w> <w n="22.6">la</w> <w n="22.7">force</w> <w n="22.8">de</w> <w n="22.9">tenir</w></l>
					<l n="23" num="1.23"><w n="23.1">Ce</w> <w n="23.2">jouet</w> <w n="23.3">de</w> <w n="23.4">fillette</w> <w n="23.5">avec</w> <w n="23.6">sa</w> <w n="23.7">main</w> <w n="23.8">trop</w> <w n="23.9">tendre</w> ;</l>
					<l n="24" num="1.24"><w n="24.1">Mais</w> <w n="24.2">on</w> <w n="24.3">avait</w> <w n="24.4">trouvé</w> <w n="24.5">cela</w>, <w n="24.6">de</w> <w n="24.7">le</w> <w n="24.8">suspendre</w></l>
					<l n="25" num="1.25"><w n="25.1">Avec</w> <w n="25.2">un</w> <w n="25.3">léger</w> <w n="25.4">fil</w> <w n="25.5">au</w>-<w n="25.6">dessus</w> <w n="25.7">du</w> <w n="25.8">berceau</w>.</l>
					<l n="26" num="1.26"><w n="26.1">La</w> <w n="26.2">douce</w> <w n="26.3">enfant</w>, <w n="26.4">tremblant</w> <w n="26.5">de</w> <w n="26.6">froid</w> <w n="26.7">comme</w> <w n="26.8">un</w> <w n="26.9">oiseau</w>,</l>
					<l n="27" num="1.27"><w n="27.1">En</w> <w n="27.2">voyant</w> <w n="27.3">la</w> <w n="27.4">poupée</w> <w n="27.5">essayait</w> <w n="27.6">de</w> <w n="27.7">sourire</w>.</l>
					<l n="28" num="1.28"><w n="28.1">Ses</w> <w n="28.2">deux</w> <w n="28.3">mains</w> <w n="28.4">y</w> <w n="28.5">touchaient</w> <w n="28.6">alors</w>, <w n="28.7">chère</w> <w n="28.8">martyre</w> !</l>
					<l n="29" num="1.29"><w n="29.1">D</w>’<w n="29.2">un</w> <w n="29.3">geste</w> <w n="29.4">maladif</w>, <w n="29.5">vaguement</w> <w n="29.6">enfantin</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Et</w> <w n="30.2">l</w>’<w n="30.3">on</w> <w n="30.4">voyait</w> <w n="30.5">trembler</w> <w n="30.6">à</w> <w n="30.7">peine</w> <w n="30.8">le</w> <w n="30.9">pantin</w>.</l>
					<l n="31" num="1.31"><space quantity="2" unit="char"></space><w n="31.1">C</w>’<w n="31.2">est</w> <w n="31.3">qu</w>’<w n="31.4">elle</w> <w n="31.5">était</w> <w n="31.6">si</w> <w n="31.7">faible</w>, <w n="31.8">elle</w> <w n="31.9">était</w> <w n="31.10">si</w> <w n="31.11">petite</w> !</l>
					<l n="32" num="1.32"><w n="32.1">Pensive</w>, <w n="32.2">elle</w> <w n="32.3">ployait</w> <w n="32.4">sous</w> <w n="32.5">l</w>’<w n="32.6">atteinte</w> <w n="32.7">maudite</w></l>
					<l n="33" num="1.33"><w n="33.1">D</w>’<w n="33.2">un</w> <w n="33.3">mal</w> <w n="33.4">mystérieux</w>, <w n="33.5">privée</w> <w n="33.6">encor</w> <w n="33.7">de</w> <w n="33.8">tout</w>,</l>
					<l n="34" num="1.34"><w n="34.1">Ne</w> <w n="34.2">pouvant</w> <w n="34.3">ni</w> <w n="34.4">marcher</w> <w n="34.5">ni</w> <w n="34.6">se</w> <w n="34.7">tenir</w> <w n="34.8">debout</w>.</l>
					<l n="35" num="1.35"><w n="35.1">Pendant</w> <w n="35.2">ce</w> <w n="35.3">temps</w> <w n="35.4">qu</w>’<w n="35.5">elle</w> <w n="35.6">a</w> <w n="35.7">vécu</w>, <w n="35.8">toute</w> <w n="35.9">une</w> <w n="35.10">année</w> !</l>
					<l n="36" num="1.36"><w n="36.1">Elle</w> <w n="36.2">a</w> <w n="36.3">souffert</w> <w n="36.4">toujours</w>, <w n="36.5">pauvre</w> <w n="36.6">rose</w> <w n="36.7">fanée</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Qui</w> <w n="37.2">frissonnait</w>, <w n="37.3">brisée</w> <w n="37.4">et</w> <w n="37.5">blanche</w>, <w n="37.6">au</w> <w n="37.7">moindre</w> <w n="37.8">vent</w>.</l>
					<l n="38" num="1.38"><w n="38.1">Dans</w> <w n="38.2">ses</w> <w n="38.3">profonds</w> <w n="38.4">yeux</w> <w n="38.5">bruns</w> <w n="38.6">brillait</w> <w n="38.7">un</w> <w n="38.8">feu</w> <w n="38.9">mouvant</w></l>
					<l n="39" num="1.39"><w n="39.1">Et</w> <w n="39.2">la</w> <w n="39.3">douleur</w> <w n="39.4">brûlait</w> <w n="39.5">sa</w> <w n="39.6">prunelle</w> <w n="39.7">ingénue</w>.</l>
					<l n="40" num="1.40"><w n="40.1">Mais</w>, <w n="40.2">après</w>, <w n="40.3">elle</w> <w n="40.4">était</w> <w n="40.5">vite</w> <w n="40.6">redevenue</w></l>
					<l n="41" num="1.41"><w n="41.1">Charmante</w>. <w n="41.2">Reposée</w> <w n="41.3">après</w> <w n="41.4">ce</w> <w n="41.5">long</w> <w n="41.6">effort</w>,</l>
					<l n="42" num="1.42"><space quantity="2" unit="char"></space><w n="42.1">Elle</w> <w n="42.2">semblait</w> <w n="42.3">dormir</w> <w n="42.4">tranquillement</w>. <w n="42.5">La</w> <w n="42.6">mort</w></l>
					<l n="43" num="1.43"><w n="43.1">Bienfaisante</w>, <w n="43.2">effaçant</w> <w n="43.3">la</w> <w n="43.4">tristesse</w> <w n="43.5">et</w> <w n="43.6">le</w> <w n="43.7">hâle</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Avait</w> <w n="44.2">rendu</w> <w n="44.3">la</w> <w n="44.4">grâce</w> <w n="44.5">au</w> <w n="44.6">doux</w> <w n="44.7">visage</w> <w n="44.8">pâle</w>,</l>
					<l n="45" num="1.45"><w n="45.1">Et</w> <w n="45.2">sur</w> <w n="45.3">le</w> <w n="45.4">petit</w> <w n="45.5">front</w> <w n="45.6">par</w> <w n="45.7">le</w> <w n="45.8">calme</w> <w n="45.9">enchanté</w></l>
					<l n="46" num="1.46"><w n="46.1">Comme</w> <w n="46.2">un</w> <w n="46.3">lys</w> <w n="46.4">immobile</w> <w n="46.5">avait</w> <w n="46.6">mis</w> <w n="46.7">la</w> <w n="46.8">beauté</w>.</l>
					<l n="47" num="1.47"><w n="47.1">Elle</w> <w n="47.2">était</w> <w n="47.3">belle</w> ; <w n="47.4">mais</w> <w n="47.5">qu</w>’<w n="47.6">elle</w> <w n="47.7">est</w> <w n="47.8">plus</w> <w n="47.9">belle</w> <w n="47.10">encore</w></l>
					<l n="48" num="1.48"><w n="48.1">Aux</w> <w n="48.2">cieux</w> ! <w n="48.3">Elle</w> <w n="48.4">est</w> <w n="48.5">la</w> <w n="48.6">vie</w> <w n="48.7">en</w> <w n="48.8">fleur</w> <w n="48.9">qui</w> <w n="48.10">vient</w> <w n="48.11">d</w>’<w n="48.12">éclore</w>.</l>
					<l n="49" num="1.49"><w n="49.1">Maintenant</w>, <w n="49.2">maintenant</w>, <w n="49.3">mère</w>, <w n="49.4">je</w> <w n="49.5">vous</w> <w n="49.6">le</w> <w n="49.7">dis</w>,</l>
					<l n="50" num="1.50"><w n="50.1">Elle</w> <w n="50.2">est</w> <w n="50.3">là</w>-<w n="50.4">haut</w>, <w n="50.5">avec</w> <w n="50.6">les</w> <w n="50.7">saints</w> <w n="50.8">du</w> <w n="50.9">paradis</w>.</l>
					<l n="51" num="1.51"><w n="51.1">Elle</w> <w n="51.2">est</w> <w n="51.3">forte</w>, <w n="51.4">elle</w> <w n="51.5">peut</w> <w n="51.6">marcher</w> ; <w n="51.7">ses</w> <w n="51.8">pieds</w> <w n="51.9">sont</w> <w n="51.10">lestes</w></l>
					<l n="52" num="1.52"><w n="52.1">Et</w> <w n="52.2">s</w>’<w n="52.3">envolent</w>, <w n="52.4">guidés</w> <w n="52.5">par</w> <w n="52.6">les</w> <w n="52.7">harpes</w> <w n="52.8">célestes</w>.</l>
					<l n="53" num="1.53"><w n="53.1">Son</w> <w n="53.2">front</w> <w n="53.3">est</w> <w n="53.4">plus</w> <w n="53.5">riant</w> <w n="53.6">qu</w>’<w n="53.7">une</w> <w n="53.8">perle</w> <w n="53.9">d</w>’<w n="53.10">Ophir</w>.</l>
					<l n="54" num="1.54"><w n="54.1">Elle</w> <w n="54.2">a</w> <w n="54.3">de</w> <w n="54.4">beaux</w> <w n="54.5">pantins</w> <w n="54.6">d</w>’<w n="54.7">opale</w> <w n="54.8">et</w> <w n="54.9">de</w> <w n="54.10">saphir</w>,</l>
					<l n="55" num="1.55"><w n="55.1">Et</w> <w n="55.2">triomphante</w>, <w n="55.3">et</w> <w n="55.4">rose</w>, <w n="55.5">et</w> <w n="55.6">libre</w> <w n="55.7">de</w> <w n="55.8">ses</w> <w n="55.9">langes</w>,</l>
					<l n="56" num="1.56"><w n="56.1">Elle</w> <w n="56.2">joue</w> <w n="56.3">en</w> <w n="56.4">chantant</w> <w n="56.5">sur</w> <w n="56.6">les</w> <w n="56.7">genoux</w> <w n="56.8">des</w> <w n="56.9">anges</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1863">18-19 avril 1863</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>