<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN175">
				<head type="main">BAUDELAIRE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Toujours</w> <w n="1.2">un</w> <w n="1.3">pur</w> <w n="1.4">rayon</w> <w n="1.5">mystérieux</w> <w n="1.6">éclaire</w></l>
					<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">ses</w> <w n="2.3">replis</w> <w n="2.4">obscurs</w> <w n="2.5">l</w>’<w n="2.6">œuvre</w> <w n="2.7">de</w> <w n="2.8">Baudelaire</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">le</w> <w n="3.3">surnaturel</w>, <w n="3.4">en</w> <w n="3.5">ses</w> <w n="3.6">rêves</w> <w n="3.7">jeté</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Y</w> <w n="4.2">mêle</w> <w n="4.3">son</w> <w n="4.4">extase</w> <w n="4.5">et</w> <w n="4.6">son</w> <w n="4.7">étrangeté</w>.</l>
					<l n="5" num="1.5"><space quantity="2" unit="char"></space><w n="5.1">L</w>’<w n="5.2">homme</w> <w n="5.3">moderne</w>, <w n="5.4">usant</w> <w n="5.5">sa</w> <w n="5.6">bravoure</w> <w n="5.7">stérile</w></l>
					<l n="6" num="1.6"><w n="6.1">En</w> <w n="6.2">d</w>’<w n="6.3">absurdes</w> <w n="6.4">combats</w>, <w n="6.5">plus</w> <w n="6.6">durs</w> <w n="6.7">que</w> <w n="6.8">ceux</w> <w n="6.9">d</w>’<w n="6.10">Achille</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w>, <w n="7.2">fort</w> <w n="7.3">de</w> <w n="7.4">sa</w> <w n="7.5">misère</w> <w n="7.6">et</w> <w n="7.7">de</w> <w n="7.8">son</w> <w n="7.9">désespoir</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Héros</w> <w n="8.2">pensif</w>, <w n="8.3">caché</w> <w n="8.4">dans</w> <w n="8.5">son</w> <w n="8.6">mince</w> <w n="8.7">habit</w> <w n="8.8">noir</w>,</l>
					<l n="9" num="1.9"><w n="9.1">S</w>’<w n="9.2">abreuvant</w> <w n="9.3">à</w> <w n="9.4">longs</w> <w n="9.5">traits</w> <w n="9.6">de</w> <w n="9.7">la</w> <w n="9.8">douleur</w> <w n="9.9">choisie</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Savourant</w> <w n="10.2">lentement</w> <w n="10.3">cette</w> <w n="10.4">amère</w> <w n="10.5">ambroisie</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">gardant</w> <w n="11.3">en</w> <w n="11.4">son</w> <w n="11.5">cœur</w>, <w n="11.6">lutteur</w> <w n="11.7">déshérité</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Le</w> <w n="12.2">culte</w> <w n="12.3">et</w> <w n="12.4">le</w> <w n="12.5">regret</w> <w n="12.6">poignant</w> <w n="12.7">de</w> <w n="12.8">la</w> <w n="12.9">beauté</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">La</w> <w n="13.2">femme</w> <w n="13.3">abandonnée</w> <w n="13.4">à</w> <w n="13.5">son</w> <w n="13.6">ivresse</w> <w n="13.7">folle</w></l>
					<l n="14" num="1.14"><w n="14.1">Se</w> <w n="14.2">parant</w> <w n="14.3">de</w> <w n="14.4">saphirs</w> <w n="14.5">comme</w> <w n="14.6">une</w> <w n="14.7">vaine</w> <w n="14.8">idole</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">tous</w> <w n="15.3">les</w> <w n="15.4">deux</w> <w n="15.5">fuyant</w> <w n="15.6">l</w>’<w n="15.7">épouvante</w> <w n="15.8">du</w> <w n="15.9">jour</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Poursuivis</w> <w n="16.2">par</w> <w n="16.3">le</w> <w n="16.4">fouet</w> <w n="16.5">horrible</w> <w n="16.6">de</w> <w n="16.7">l</w>’<w n="16.8">amour</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">La</w> <w n="17.2">pauvreté</w>, <w n="17.3">l</w>’<w n="17.4">erreur</w>, <w n="17.5">la</w> <w n="17.6">passion</w>, <w n="17.7">le</w> <w n="17.8">vice</w>,</l>
					<l n="18" num="1.18"><w n="18.1">L</w>’<w n="18.2">ennui</w> <w n="18.3">silencieux</w>, <w n="18.4">acharnant</w> <w n="18.5">leur</w> <w n="18.6">sévice</w></l>
					<l n="19" num="1.19"><w n="19.1">Sur</w> <w n="19.2">ce</w> <w n="19.3">couple</w> <w n="19.4">privé</w> <w n="19.5">du</w> <w n="19.6">guide</w> <w n="19.7">essentiel</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">cependant</w> <w n="20.3">mordu</w> <w n="20.4">par</w> <w n="20.5">l</w>’<w n="20.6">appétit</w> <w n="20.7">du</w> <w n="20.8">ciel</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">se</w> <w n="21.3">ressouvenant</w>, <w n="21.4">en</w> <w n="21.5">sa</w> <w n="21.6">splendeur</w> <w n="21.7">première</w>,</l>
					<l n="22" num="1.22"><w n="22.1">D</w>’<w n="22.2">avoir</w> <w n="22.3">été</w> <w n="22.4">pétri</w> <w n="22.5">de</w> <w n="22.6">fange</w> <w n="22.7">et</w> <w n="22.8">de</w> <w n="22.9">lumière</w> ;</l>
					<l n="23" num="1.23"><w n="23.1">L</w>’<w n="23.2">être</w> <w n="23.3">vil</w> <w n="23.4">ne</w> <w n="23.5">pouvant</w> <w n="23.6">cesser</w> <w n="23.7">d</w>’<w n="23.8">être</w> <w n="23.9">divin</w> ;</l>
					<l n="24" num="1.24"><w n="24.1">Le</w> <w n="24.2">malheureux</w> <w n="24.3">noyant</w> <w n="24.4">ses</w> <w n="24.5">soucis</w> <w n="24.6">dans</w> <w n="24.7">le</w> <w n="24.8">vin</w>,</l>
					<l n="25" num="1.25"><w n="25.1">Mais</w> <w n="25.2">sentant</w> <w n="25.3">tout</w> <w n="25.4">à</w> <w n="25.5">coup</w> <w n="25.6">que</w> <w n="25.7">l</w>’<w n="25.8">ivresse</w> <w n="25.9">fatale</w></l>
					<l n="26" num="1.26"><w n="26.1">Ouvre</w> <w n="26.2">dans</w> <w n="26.3">sa</w> <w n="26.4">cervelle</w> <w n="26.5">une</w> <w n="26.6">porte</w> <w n="26.7">idéale</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Et</w>, <w n="27.2">dévoilant</w> <w n="27.3">l</w>’<w n="27.4">azur</w> <w n="27.5">pour</w> <w n="27.6">ses</w> <w n="27.7">sens</w> <w n="27.8">engourdis</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Lui</w> <w n="28.2">donne</w> <w n="28.3">le</w> <w n="28.4">frisson</w> <w n="28.5">des</w> <w n="28.6">vagues</w> <w n="28.7">paradis</w> ;</l>
					<l n="29" num="1.29"><w n="29.1">Le</w> <w n="29.2">libertin</w> <w n="29.3">voyant</w>, <w n="29.4">en</w> <w n="29.5">son</w> <w n="29.6">amer</w> <w n="29.7">délire</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Que</w> <w n="30.2">l</w>’<w n="30.3">ongle</w> <w n="30.4">furieux</w> <w n="30.5">d</w>’<w n="30.6">un</w> <w n="30.7">ange</w> <w n="30.8">le</w> <w n="30.9">déchire</w>,</l>
					<l n="31" num="1.31"><w n="31.1">Et</w> <w n="31.2">le</w> <w n="31.3">force</w>, <w n="31.4">avivant</w> <w n="31.5">cette</w> <w n="31.6">blessure</w> <w n="31.7">en</w> <w n="31.8">feu</w>,</l>
					<l n="32" num="1.32"><w n="32.1">À</w> <w n="32.2">traîner</w> <w n="32.3">sa</w> <w n="32.4">laideur</w> <w n="32.5">sous</w> <w n="32.6">l</w>’<w n="32.7">œil</w> <w n="32.8">même</w> <w n="32.9">de</w> <w n="32.10">Dieu</w> ;</l>
					<l n="33" num="1.33"><w n="33.1">La</w> <w n="33.2">matière</w>, <w n="33.3">céleste</w> <w n="33.4">encor</w> <w n="33.5">même</w> <w n="33.6">en</w> <w n="33.7">sa</w> <w n="33.8">chute</w>,</l>
					<l n="34" num="1.34"><w n="34.1">Impuissante</w> <w n="34.2">à</w> <w n="34.3">créer</w> <w n="34.4">l</w>’<w n="34.5">oubli</w> <w n="34.6">d</w>’<w n="34.7">une</w> <w n="34.8">minute</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Pâture</w> <w n="35.2">du</w> <w n="35.3">désir</w>, <w n="35.4">jouet</w> <w n="35.5">du</w> <w n="35.6">noir</w> <w n="35.7">remord</w>,</l>
					<l n="36" num="1.36"><w n="36.1">Et</w> <w n="36.2">souffrant</w> <w n="36.3">sans</w> <w n="36.4">répit</w> <w n="36.5">jusqu</w>’<w n="36.6">à</w> <w n="36.7">ce</w> <w n="36.8">que</w> <w n="36.9">la</w> <w n="36.10">mort</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Apparaissant</w>, <w n="37.2">la</w> <w n="37.3">baise</w> <w n="37.4">au</w> <w n="37.5">front</w> <w n="37.6">et</w> <w n="37.7">la</w> <w n="37.8">délivre</w> ;</l>
					<l n="38" num="1.38"><w n="38.1">Ô</w> <w n="38.2">mon</w> <w n="38.3">âme</w>, <w n="38.4">voilà</w> <w n="38.5">ce</w> <w n="38.6">qu</w>’<w n="38.7">on</w> <w n="38.8">voit</w> <w n="38.9">dans</w> <w n="38.10">ce</w> <w n="38.11">livre</w></l>
					<l n="39" num="1.39"><w n="39.1">Où</w> <w n="39.2">le</w> <w n="39.3">calme</w> <w n="39.4">songeur</w> <w n="39.5">qui</w> <w n="39.6">vécut</w> <w n="39.7">et</w> <w n="39.8">souffrit</w></l>
					<l n="40" num="1.40"><w n="40.1">Adore</w> <w n="40.2">la</w> <w n="40.3">vertu</w> <w n="40.4">subtile</w> <w n="40.5">de</w> <w n="40.6">l</w>’<w n="40.7">esprit</w> ;</l>
					<l n="41" num="1.41"><w n="41.1">Voilà</w> <w n="41.2">ce</w> <w n="41.3">que</w> <w n="41.4">l</w>’<w n="41.5">on</w> <w n="41.6">voit</w> <w n="41.7">dans</w> <w n="41.8">ces</w> <w n="41.9">vivantes</w> <w n="41.10">rimes</w></l>
					<l n="42" num="1.42"><w n="42.1">Où</w> <w n="42.2">Baudelaire</w>, <w n="42.3">épris</w> <w n="42.4">de</w> <w n="42.5">l</w>’<w n="42.6">horreur</w> <w n="42.7">des</w> <w n="42.8">abîmes</w></l>
					<l n="43" num="1.43"><w n="43.1">Et</w> <w n="43.2">fuyant</w> <w n="43.3">vers</w> <w n="43.4">l</w>’<w n="43.5">azur</w> <w n="43.6">du</w> <w n="43.7">gouffre</w> <w n="43.8">meurtrier</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Dédaigne</w> <w n="44.2">de</w> <w n="44.3">descendre</w> <w n="44.4">au</w> <w n="44.5">terrestre</w> <w n="44.6">laurier</w> ;</l>
					<l n="45" num="1.45"><w n="45.1">Dans</w> <w n="45.2">cette</w> <w n="45.3">œuvre</w> <w n="45.4">d</w>’<w n="45.5">amour</w>, <w n="45.6">d</w>’<w n="45.7">ironie</w> <w n="45.8">et</w> <w n="45.9">de</w> <w n="45.10">fièvre</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Où</w> <w n="46.2">le</w> <w n="46.3">poëte</w> <w n="46.4">au</w> <w n="46.5">cœur</w> <w n="46.6">meurtri</w> <w n="46.7">penche</w> <w n="46.8">sa</w> <w n="46.9">lèvre</w></l>
					<l n="47" num="1.47"><w n="47.1">Que</w> <w n="47.2">les</w> <w n="47.3">mots</w> <w n="47.4">odieux</w> <w n="47.5">ne</w> <w n="47.6">souillèrent</w> <w n="47.7">jamais</w>,</l>
					<l n="48" num="1.48"><w n="48.1">Vers</w> <w n="48.2">la</w> <w n="48.3">foi</w> <w n="48.4">pâlissante</w>, <w n="48.5">ange</w> <w n="48.6">des</w> <w n="48.7">purs</w> <w n="48.8">sommets</w>,</l>
					<l n="49" num="1.49"><w n="49.1">Et</w>, <w n="49.2">triste</w> <w n="49.3">comme</w> <w n="49.4">Hamlet</w> <w n="49.5">au</w> <w n="49.6">tombeau</w> <w n="49.7">d</w>’<w n="49.8">Ophélie</w>,</l>
					<l n="50" num="1.50"><w n="50.1">Pleure</w> <w n="50.2">sur</w> <w n="50.3">notre</w> <w n="50.4">joie</w> <w n="50.5">et</w> <w n="50.6">sur</w> <w n="50.7">notre</w> <w n="50.8">folie</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1874">lundi 7 septembre 1874</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>