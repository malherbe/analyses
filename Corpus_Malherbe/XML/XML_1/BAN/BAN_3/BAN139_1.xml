<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN139">
				<head type="main">LA ROSE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Égaré</w> <w n="1.2">sur</w> <w n="1.3">l</w>’<w n="1.4">Othrys</w> <w n="1.5">après</w> <w n="1.6">un</w> <w n="1.7">jour</w> <w n="1.8">de</w> <w n="1.9">jeûne</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">plus</w> <w n="2.3">ancien</w> <w n="2.4">des</w> <w n="2.5">dieux</w>, <w n="2.6">l</w>’<w n="2.7">éternellement</w> <w n="2.8">jeune</w></l>
					<l n="3" num="1.3"><w n="3.1">Amour</w>, <w n="3.2">le</w> <w n="3.3">dur</w> <w n="3.4">chasseur</w> <w n="3.5">que</w> <w n="3.6">l</w>’<w n="3.7">épouvante</w> <w n="3.8">suit</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Né</w> <w n="4.2">de</w> <w n="4.3">l</w>’<w n="4.4">œuf</w> <w n="4.5">redoutable</w> <w n="4.6">enfanté</w> <w n="4.7">par</w> <w n="4.8">la</w> <w n="4.9">nuit</w></l>
					<l n="5" num="1.5"><w n="5.1">Aux</w> <w n="5.2">noires</w> <w n="5.3">ailes</w>, <w n="5.4">vit</w> <w n="5.5">la</w> <w n="5.6">grande</w> <w n="5.7">Cythérée</w></l>
					<l n="6" num="1.6"><w n="6.1">Dormant</w> <w n="6.2">dans</w> <w n="6.3">son</w> <w n="6.4">chemin</w>, <w n="6.5">sur</w> <w n="6.6">la</w> <w n="6.7">mousse</w> <w n="6.8">altérée</w></l>
					<l n="7" num="1.7"><w n="7.1">Par</w> <w n="7.2">le</w> <w n="7.3">matin</w> <w n="7.4">brûlant</w>, <w n="7.5">et</w>, <w n="7.6">pâle</w> <w n="7.7">d</w>’<w n="7.8">un</w> <w n="7.9">tel</w> <w n="7.10">jeu</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Contempla</w> <w n="8.2">son</w> <w n="8.3">visage</w> <w n="8.4">et</w> <w n="8.5">ses</w> <w n="8.6">lèvres</w> <w n="8.7">de</w> <w n="8.8">feu</w>.</l>
					<l n="9" num="1.9"><space quantity="2" unit="char"></space><w n="9.1">La</w> <w n="9.2">déesse</w>, <w n="9.3">couchée</w> <w n="9.4">entre</w> <w n="9.5">des</w> <w n="9.6">rocs</w> <w n="9.7">de</w> <w n="9.8">marbre</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Reposait</w>, <w n="10.2">les</w> <w n="10.3">cheveux</w> <w n="10.4">épars</w>, <w n="10.5">au</w> <w n="10.6">pied</w> <w n="10.7">d</w>’<w n="10.8">un</w> <w n="10.9">arbre</w></l>
					<l n="11" num="1.11"><w n="11.1">Dont</w> <w n="11.2">l</w>’<w n="11.3">abri</w> <w n="11.4">préservait</w> <w n="11.5">son</w> <w n="11.6">front</w> <w n="11.7">de</w> <w n="11.8">la</w> <w n="11.9">chaleur</w>.</l>
					<l n="12" num="1.12"><w n="12.1">Ses</w> <w n="12.2">beaux</w> <w n="12.3">yeux</w> <w n="12.4">étaient</w> <w n="12.5">clos</w>, <w n="12.6">mais</w> <w n="12.7">sur</w> <w n="12.8">sa</w> <w n="12.9">joue</w> <w n="12.10">en</w> <w n="12.11">fleur</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Dont</w> <w n="13.2">leur</w> <w n="13.3">voile</w> <w n="13.4">exaltait</w> <w n="13.5">l</w>’<w n="13.6">impérieuse</w> <w n="13.7">gloire</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Des</w> <w n="14.2">franges</w> <w n="14.3">de</w> <w n="14.4">longs</w> <w n="14.5">cils</w> <w n="14.6">montraient</w> <w n="14.7">leur</w> <w n="14.8">splendeur</w> <w n="14.9">noire</w>.</l>
					<l n="15" num="1.15"><w n="15.1">Comme</w> <w n="15.2">un</w> <w n="15.3">prince</w> <w n="15.4">jaloux</w> <w n="15.5">qui</w> <w n="15.6">marque</w> <w n="15.7">son</w> <w n="15.8">trésor</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Le</w> <w n="16.2">soleil</w> <w n="16.3">éperdu</w> <w n="16.4">lançait</w> <w n="16.5">des</w> <w n="16.6">flèches</w> <w n="16.7">d</w>’<w n="16.8">or</w></l>
					<l n="17" num="1.17"><w n="17.1">Sur</w> <w n="17.2">son</w> <w n="17.3">sein</w> <w n="17.4">éclatant</w> <w n="17.5">d</w>’<w n="17.6">une</w> <w n="17.7">candeur</w> <w n="17.8">insigne</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">sa</w> <w n="18.3">poitrine</w> <w n="18.4">était</w> <w n="18.5">de</w> <w n="18.6">neige</w> <w n="18.7">comme</w> <w n="18.8">un</w> <w n="18.9">cygne</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">pareille</w> <w n="19.3">aux</w> <w n="19.4">brebis</w> <w n="19.5">errantes</w> <w n="19.6">d</w>’<w n="19.7">un</w> <w n="19.8">troupeau</w>.</l>
					<l n="20" num="1.20"><w n="20.1">Sur</w> <w n="20.2">sa</w> <w n="20.3">crinière</w> <w n="20.4">fauve</w> <w n="20.5">et</w> <w n="20.6">sur</w> <w n="20.7">sa</w> <w n="20.8">blanche</w> <w n="20.9">peau</w></l>
					<l n="21" num="1.21"><w n="21.1">De</w> <w n="21.2">tremblantes</w> <w n="21.3">lueurs</w> <w n="21.4">couraient</w>, <w n="21.5">surnaturelles</w>.</l>
					<l n="22" num="1.22"><w n="22.1">Entre</w> <w n="22.2">ses</w> <w n="22.3">pieds</w> <w n="22.4">ouverts</w> <w n="22.5">dormaient</w> <w n="22.6">deux</w> <w n="22.7">tourterelles</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Le</w> <w n="23.2">radieux</w> <w n="23.3">sourire</w> <w n="23.4">en</w> <w n="23.5">pleurs</w> <w n="23.6">du</w> <w n="23.7">jour</w> <w n="23.8">naissant</w></l>
					<l n="24" num="1.24"><w n="24.1">Folâtrait</w> <w n="24.2">sur</w> <w n="24.3">son</w> <w n="24.4">corps</w> <w n="24.5">de</w> <w n="24.6">vierge</w> <w n="24.7">éblouissant</w>,</l>
					<l n="25" num="1.25"><w n="25.1">Et</w> <w n="25.2">la</w> <w n="25.3">nuit</w> <w n="25.4">du</w> <w n="25.5">feuillage</w> <w n="25.6">et</w> <w n="25.7">l</w>’<w n="25.8">ombre</w> <w n="25.9">des</w> <w n="25.10">érables</w></l>
					<l n="26" num="1.26"><w n="26.1">Y</w> <w n="26.2">caressaient</w>, <w n="26.3">depuis</w> <w n="26.4">les</w> <w n="26.5">masses</w> <w n="26.6">adorables</w></l>
					<l n="27" num="1.27"><w n="27.1">De</w> <w n="27.2">la</w> <w n="27.3">blonde</w> <w n="27.4">toison</w> <w n="27.5">jusqu</w>’<w n="27.6">aux</w> <w n="27.7">divins</w> <w n="27.8">orteils</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Les</w> <w n="28.2">touffes</w> <w n="28.3">d</w>’<w n="28.4">or</w>, <w n="28.5">les</w> <w n="28.6">lys</w> <w n="28.7">vivants</w>, <w n="28.8">les</w> <w n="28.9">feux</w> <w n="28.10">vermeils</w>.</l>
					<l n="29" num="1.29"><space quantity="2" unit="char"></space><w n="29.1">Éros</w> <w n="29.2">la</w> <w n="29.3">vit</w>. <w n="29.4">Il</w> <w n="29.5">vit</w> <w n="29.6">ces</w> <w n="29.7">bras</w> <w n="29.8">que</w> <w n="29.9">tout</w> <w n="29.10">adore</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Et</w> <w n="30.2">ces</w> <w n="30.3">rougeurs</w> <w n="30.4">de</w> <w n="30.5">braise</w> <w n="30.6">et</w> <w n="30.7">ces</w> <w n="30.8">clartés</w> <w n="30.9">d</w>’<w n="30.10">aurore</w> ;</l>
					<l n="31" num="1.31"><w n="31.1">Il</w> <w n="31.2">contempla</w> <w n="31.3">Cypris</w> <w n="31.4">endormie</w>, <w n="31.5">à</w> <w n="31.6">loisir</w>.</l>
					<l n="32" num="1.32"><w n="32.1">Alors</w> <w n="32.2">de</w> <w n="32.3">son</w> <w n="32.4">désir</w>, <w n="32.5">faite</w> <w n="32.6">de</w> <w n="32.7">son</w> <w n="32.8">désir</w>,</l>
					<l n="33" num="1.33"><w n="33.1">Toute</w> <w n="33.2">pareille</w> <w n="33.3">à</w> <w n="33.4">son</w> <w n="33.5">désir</w>, <w n="33.6">naquit</w> <w n="33.7">dans</w> <w n="33.8">l</w>’<w n="33.9">herbe</w></l>
					<l n="34" num="1.34"><w n="34.1">Une</w> <w n="34.2">fleur</w> <w n="34.3">tendre</w>, <w n="34.4">émue</w>, <w n="34.5">ineffable</w>, <w n="34.6">superbe</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Rougissante</w>, <w n="35.2">splendide</w>, <w n="35.3">et</w> <w n="35.4">sous</w> <w n="35.5">son</w> <w n="35.6">fier</w> <w n="35.7">dessin</w></l>
					<l n="36" num="1.36"><w n="36.1">Flamboyante</w>, <w n="36.2">et</w> <w n="36.3">gardant</w> <w n="36.4">la</w> <w n="36.5">fraîcheur</w> <w n="36.6">d</w>’<w n="36.7">un</w> <w n="36.8">beau</w> <w n="36.9">sein</w>.</l>
					<l n="37" num="1.37"><space quantity="2" unit="char"></space><w n="37.1">Et</w> <w n="37.2">c</w>’<w n="37.3">est</w> <w n="37.4">la</w> <w n="37.5">rose</w> ! <w n="37.6">C</w>’<w n="37.7">est</w> <w n="37.8">la</w> <w n="37.9">fleur</w> <w n="37.10">tendre</w> <w n="37.11">et</w> <w n="37.12">farouche</w></l>
					<l n="38" num="1.38"><w n="38.1">Qui</w> <w n="38.2">présente</w> <w n="38.3">à</w> <w n="38.4">Cypris</w> <w n="38.5">l</w>’<w n="38.6">image</w> <w n="38.7">de</w> <w n="38.8">sa</w> <w n="38.9">bouche</w>,</l>
					<l n="39" num="1.39"><w n="39.1">Et</w> <w n="39.2">semble</w> <w n="39.3">avoir</w> <w n="39.4">un</w> <w n="39.5">sang</w> <w n="39.6">de</w> <w n="39.7">pourpre</w> <w n="39.8">sous</w> <w n="39.9">sa</w> <w n="39.10">chair</w>.</l>
					<l n="40" num="1.40"><w n="40.1">Fleur</w>-<w n="40.2">femme</w>, <w n="40.3">elle</w> <w n="40.4">contient</w> <w n="40.5">tout</w> <w n="40.6">ce</w> <w n="40.7">qui</w> <w n="40.8">nous</w> <w n="40.9">est</w> <w n="40.10">cher</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Jour</w>, <w n="41.2">triomphe</w>, <w n="41.3">caresse</w>, <w n="41.4">embrassement</w>, <w n="41.5">sourire</w> :</l>
					<l n="42" num="1.42"><w n="42.1">Voir</w> <w n="42.2">la</w> <w n="42.3">rose</w>, <w n="42.4">c</w>’<w n="42.5">est</w> <w n="42.6">comme</w> <w n="42.7">écouter</w> <w n="42.8">une</w> <w n="42.9">lyre</w> !</l>
					<l n="43" num="1.43"><w n="43.1">Notre</w> <w n="43.2">regard</w> <w n="43.3">ému</w> <w n="43.4">suit</w> <w n="43.5">le</w> <w n="43.6">frémissement</w></l>
					<l n="44" num="1.44"><w n="44.1">De</w> <w n="44.2">son</w> <w n="44.3">délicieux</w> <w n="44.4">épanouissement</w> ;</l>
					<l n="45" num="1.45"><w n="45.1">Sa</w> <w n="45.2">chevelure</w> <w n="45.3">verte</w> <w n="45.4">avec</w> <w n="45.5">orgueil</w> <w n="45.6">la</w> <w n="45.7">couvre</w>.</l>
					<l n="46" num="1.46"><w n="46.1">Quand</w> <w n="46.2">nous</w> <w n="46.3">la</w> <w n="46.4">respirons</w>, <w n="46.5">elle</w> <w n="46.6">est</w> <w n="46.7">pâmée</w>, <w n="46.8">et</w> <w n="46.9">s</w>’<w n="46.10">ouvre</w> :</l>
					<l n="47" num="1.47"><w n="47.1">Son</w> <w n="47.2">parfum</w> <w n="47.3">d</w>’<w n="47.4">ambroisie</w> <w n="47.5">est</w> <w n="47.6">un</w> <w n="47.7">souffle</w>. <w n="47.8">On</w> <w n="47.9">dirait</w></l>
					<l n="48" num="1.48"><w n="48.1">Que</w>, <w n="48.2">par</w> <w n="48.3">je</w> <w n="48.4">ne</w> <w n="48.5">sais</w> <w n="48.6">quel</w> <w n="48.7">ravissement</w> <w n="48.8">secret</w>,</l>
					<l n="49" num="1.49"><w n="49.1">Elle</w> <w n="49.2">prend</w> <w n="49.3">en</w> <w n="49.4">pitié</w> <w n="49.5">notre</w> <w n="49.6">amour</w> <w n="49.7">et</w> <w n="49.8">nos</w> <w n="49.9">fièvres</w>,</l>
					<l n="50" num="1.50"><w n="50.1">Et</w> <w n="50.2">son</w> <w n="50.3">calice</w> <w n="50.4">ouvert</w> <w n="50.5">nous</w> <w n="50.6">baise</w> <w n="50.7">avec</w> <w n="50.8">des</w> <w n="50.9">lèvres</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1863">mars 1863</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>