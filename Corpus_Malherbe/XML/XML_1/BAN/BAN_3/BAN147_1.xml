<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Exilés</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4189 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2012">2012</date>
				<idno type="local">BAN_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>

			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Exilés</title>
						<author>Théodore de BANVILLE</author>
					</titleStmt>
					<publicationStmt>
						<publisher>
							<orgname>
								<choice>
									<abbr>CNRTL</abbr>
									<expan>(Centre National de Ressources Textuelles et Lexicales)</expan>
								</choice>
							</orgname>
							<idno type="URL">http://www.cnrtl.fr/corpus/frantext/frantext.php</idno>
						</publisher>
						<idno type="FRANTEXT">L923</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
									<title>Les Exilés</title>
									<author>Théodore de BANVILLE</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Fasquelle</publisher>
									<date when="1899">1899</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>

		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface n’a pas été encodée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN147">
				<head type="main">DIONÉ</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Abattu</w> <w n="1.2">par</w> <w n="1.3">la</w> <w n="1.4">roche</w> <w n="1.5">énorme</w> <w n="1.6">que</w> <w n="1.7">sans</w> <w n="1.8">aide</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Seul</w>, <w n="2.2">avait</w> <w n="2.3">soulevée</w> <w n="2.4">en</w> <w n="2.5">ses</w> <w n="2.6">mains</w> <w n="2.7">Diomède</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Énée</w> <w n="3.2">était</w> <w n="3.3">tombé</w> <w n="3.4">sous</w> <w n="3.5">le</w> <w n="3.6">char</w> <w n="3.7">de</w> <w n="3.8">l</w>’<w n="3.9">ardent</w></l>
					<l n="4" num="1.4"><w n="4.1">Fils</w> <w n="4.2">de</w> <w n="4.3">Tydée</w>, <w n="4.4">ainsi</w> <w n="4.5">qu</w>’<w n="4.6">un</w> <w n="4.7">chêne</w>, <w n="4.8">et</w> <w n="4.9">cependant</w></l>
					<l n="5" num="1.5"><w n="5.1">Que</w> <w n="5.2">sa</w> <w n="5.3">mère</w> <w n="5.4">Aphrodite</w>, <w n="5.5">au</w> <w n="5.6">vent</w> <w n="5.7">échevelée</w>,</l>
					<l n="6" num="1.6"><w n="6.1">L</w>’<w n="6.2">emportait</w> <w n="6.3">mourant</w> <w n="6.4">loin</w> <w n="6.5">de</w> <w n="6.6">la</w> <w n="6.7">noire</w> <w n="6.8">mêlée</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Diomède</w>, <w n="7.2">sachant</w> <w n="7.3">qu</w>’<w n="7.4">elle</w> <w n="7.5">est</w> <w n="7.6">faible</w>, <w n="7.7">et</w> <w n="7.8">non</w> <w n="7.9">pas</w></l>
					<l n="8" num="1.8"><w n="8.1">Intrépide</w> <w n="8.2">à</w> <w n="8.3">guider</w> <w n="8.4">les</w> <w n="8.5">hommes</w> <w n="8.6">sur</w> <w n="8.7">ses</w> <w n="8.8">pas</w></l>
					<l n="9" num="1.9"><w n="9.1">Vers</w> <w n="9.2">le</w> <w n="9.3">carnage</w>, <w n="9.4">comme</w> <w n="9.5">Ényo</w> <w n="9.6">destructrice</w></l>
					<l n="10" num="1.10"><w n="10.1">Des</w> <w n="10.2">citadelles</w>, <w n="10.3">dont</w> <w n="10.4">la</w> <w n="10.5">mort</w> <w n="10.6">suit</w> <w n="10.7">le</w> <w n="10.8">caprice</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Poursuivit</w> <w n="11.2">Aphrodite</w> <w n="11.3">en</w> <w n="11.4">son</w> <w n="11.5">hardi</w> <w n="11.6">chemin</w> ;</l>
					<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">de</w> <w n="12.3">sa</w> <w n="12.4">lance</w> <w n="12.5">aiguë</w> <w n="12.6">il</w> <w n="12.7">lui</w> <w n="12.8">perça</w> <w n="12.9">la</w> <w n="12.10">main</w>,</l>
					<l n="13" num="1.13"><w n="13.1">D</w>’<w n="13.2">où</w> <w n="13.3">le</w> <w n="13.4">sang</w> <w n="13.5">précieux</w> <w n="13.6">jaillit</w> <w n="13.7">fluide</w> <w n="13.8">et</w> <w n="13.9">rose</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Délicieux</w> <w n="14.2">à</w> <w n="14.3">voir</w> <w n="14.4">comme</w> <w n="14.5">une</w> <w n="14.6">fleur</w> <w n="14.7">éclose</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Riant</w> <w n="15.2">comme</w> <w n="15.3">la</w> <w n="15.4">pourpre</w> <w n="15.5">en</w> <w n="15.6">son</w> <w n="15.7">éclat</w> <w n="15.8">vermeil</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">tout</w> <w n="16.3">éblouissant</w> <w n="16.4">des</w> <w n="16.5">perles</w> <w n="16.6">du</w> <w n="16.7">soleil</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Car</w>, <w n="17.2">pareils</w> <w n="17.3">dans</w> <w n="17.4">leur</w> <w n="17.5">gloire</w> <w n="17.6">à</w> <w n="17.7">la</w> <w n="17.8">blancheur</w> <w n="17.9">du</w> <w n="17.10">cygne</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Les</w> <w n="18.2">dieux</w> <w n="18.3">ne</w> <w n="18.4">boivent</w> <w n="18.5">pas</w> <w n="18.6">le</w> <w n="18.7">vin</w> <w n="18.8">noir</w> <w n="18.9">de</w> <w n="18.10">la</w> <w n="18.11">vigne</w>.</l>
					<l n="19" num="1.19"><w n="19.1">Ces</w> <w n="19.2">rois</w>, <w n="19.3">pétris</w> <w n="19.4">d</w>’<w n="19.5">azur</w>, <w n="19.6">ne</w> <w n="19.7">mangent</w> <w n="19.8">pas</w> <w n="19.9">de</w> <w n="19.10">blé</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">c</w>’<w n="20.3">est</w> <w n="20.4">pourquoi</w> <w n="20.5">leur</w> <w n="20.6">sang</w>, <w n="20.7">qui</w> <w n="20.8">n</w>’<w n="20.9">est</w> <w n="20.10">jamais</w> <w n="20.11">troublé</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Court</w> <w n="21.2">dans</w> <w n="21.3">leurs</w> <w n="21.4">veines</w>, <w n="21.5">beau</w> <w n="21.6">de</w> <w n="21.7">sa</w> <w n="21.8">splendeur</w> <w n="21.9">première</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Comme</w> <w n="22.2">un</w> <w n="22.3">flot</w> <w n="22.4">ruisselant</w> <w n="22.5">d</w>’<w n="22.6">éther</w> <w n="22.7">et</w> <w n="22.8">de</w> <w n="22.9">lumière</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Aphrodite</w> <w n="23.2">poussait</w> <w n="23.3">des</w> <w n="23.4">cris</w>, <w n="23.5">comme</w> <w n="23.6">un</w> <w n="23.7">aiglon</w></l>
					<l n="24" num="1.24"><w n="24.1">Furieux</w>, <w n="24.2">cependant</w> <w n="24.3">que</w> <w n="24.4">Phœbos</w>-<w n="24.5">Apollon</w></l>
					<l n="25" num="1.25"><w n="25.1">Cachait</w> <w n="25.2">Énée</w> <w n="25.3">au</w> <w n="25.4">sein</w> <w n="25.5">d</w>’<w n="25.6">un</w> <w n="25.7">nuage</w> <w n="25.8">de</w> <w n="25.9">flamme</w>,</l>
					<l n="26" num="1.26"><w n="26.1">De</w> <w n="26.2">peur</w> <w n="26.3">qu</w>’<w n="26.4">un</w> <w n="26.5">Danaen</w> <w n="26.6">ne</w> <w n="26.7">lui</w> <w n="26.8">vînt</w> <w n="26.9">ravir</w> <w n="26.10">l</w>’<w n="26.11">âme</w></l>
					<l n="27" num="1.27"><w n="27.1">En</w> <w n="27.2">frappant</w> <w n="27.3">de</w> <w n="27.4">l</w>’<w n="27.5">airain</w> <w n="27.6">ce</w> <w n="27.7">faiseur</w> <w n="27.8">de</w> <w n="27.9">travaux</w>.</l>
					<l n="28" num="1.28"><w n="28.1">Mais</w> <w n="28.2">dans</w> <w n="28.3">le</w> <w n="28.4">char</w> <w n="28.5">brillant</w> <w n="28.6">d</w>’<w n="28.7">Arès</w>, <w n="28.8">dont</w> <w n="28.9">les</w> <w n="28.10">chevaux</w></l>
					<l n="29" num="1.29"><w n="29.1">S</w>’<w n="29.2">envolèrent</w> <w n="29.3">au</w> <w n="29.4">gré</w> <w n="29.5">de</w> <w n="29.6">sa</w> <w n="29.7">fureur</w> <w n="29.8">amère</w>,</l>
					<l n="30" num="1.30"><w n="30.1">Aphrodite</w> <w n="30.2">s</w>’<w n="30.3">enfuit</w> <w n="30.4">vers</w> <w n="30.5">Dioné</w>, <w n="30.6">sa</w> <w n="30.7">mère</w> ;</l>
					<l n="31" num="1.31"><w n="31.1">Iris</w> <w n="31.2">menait</w> <w n="31.3">le</w> <w n="31.4">char</w> <w n="31.5">rapide</w>, <w n="31.6">et</w> <w n="31.7">secouait</w></l>
					<l n="32" num="1.32"><w n="32.1">Les</w> <w n="32.2">rênes</w>, <w n="32.3">et</w> <w n="32.4">tantôt</w> <w n="32.5">frappait</w> <w n="32.6">à</w> <w n="32.7">coups</w> <w n="32.8">de</w> <w n="32.9">fouet</w></l>
					<l n="33" num="1.33"><w n="33.1">Les</w> <w n="33.2">deux</w> <w n="33.3">chevaux</w>, <w n="33.4">tantôt</w> <w n="33.5">pour</w> <w n="33.6">presser</w> <w n="33.7">leur</w> <w n="33.8">allure</w></l>
					<l n="34" num="1.34"><w n="34.1">Leur</w> <w n="34.2">parlait</w>, <w n="34.3">caressant</w> <w n="34.4">leur</w> <w n="34.5">douce</w> <w n="34.6">chevelure</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Employant</w> <w n="35.2">tour</w> <w n="35.3">à</w> <w n="35.4">tour</w> <w n="35.5">la</w> <w n="35.6">colère</w> <w n="35.7">et</w> <w n="35.8">les</w> <w n="35.9">jeux</w>.</l>
					<l n="36" num="1.36"><w n="36.1">Ils</w> <w n="36.2">arrivent</w> <w n="36.3">enfin</w> <w n="36.4">à</w> <w n="36.5">l</w>’<w n="36.6">Olympe</w> <w n="36.7">neigeux</w>,</l>
					<l n="37" num="1.37"><w n="37.1">Et</w> <w n="37.2">dans</w> <w n="37.3">le</w> <w n="37.4">palais</w> <w n="37.5">d</w>’<w n="37.6">ombre</w> <w n="37.7">où</w> <w n="37.8">sur</w> <w n="37.9">son</w> <w n="37.10">trône</w> <w n="37.11">songe</w></l>
					<l n="38" num="1.38"><w n="38.1">Dioné</w>, <w n="38.2">dans</w> <w n="38.3">la</w> <w n="38.4">nue</w> <w n="38.5">où</w> <w n="38.6">sa</w> <w n="38.7">tête</w> <w n="38.8">se</w> <w n="38.9">plonge</w>.</l>
					<l n="39" num="1.39"><w n="39.1">Or</w>, <w n="39.2">lorsque</w> <w n="39.3">sans</w> <w n="39.4">pâlir</w> <w n="39.5">de</w> <w n="39.6">l</w>’<w n="39.7">amère</w> <w n="39.8">douleur</w>,</l>
					<l n="40" num="1.40"><w n="40.1">Calme</w>, <w n="40.2">et</w> <w n="40.3">comme</w> <w n="40.4">une</w> <w n="40.5">rose</w> <w n="40.6">ouvrant</w> <w n="40.7">sa</w> <w n="40.8">bouche</w> <w n="40.9">en</w> <w n="40.10">fleur</w>,</l>
					<l n="41" num="1.41"><w n="41.1">Aphrodite</w> <w n="41.2">eut</w> <w n="41.3">montré</w> <w n="41.4">sa</w> <w n="41.5">blanche</w> <w n="41.6">main</w> <w n="41.7">d</w>’<w n="41.8">ivoire</w></l>
					<l n="42" num="1.42"><w n="42.1">Déchirée</w> <w n="42.2">et</w> <w n="42.3">meurtrie</w> <w n="42.4">et</w> <w n="42.5">qui</w> <w n="42.6">devenait</w> <w n="42.7">noire</w>,</l>
					<l n="43" num="1.43"><w n="43.1">La</w> <w n="43.2">Titane</w> <w n="43.3">au</w> <w n="43.4">grand</w> <w n="43.5">cœur</w> <w n="43.6">si</w> <w n="43.7">souvent</w> <w n="43.8">ulcéré</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Planant</w> <w n="44.2">sinistrement</w> <w n="44.3">d</w>’<w n="44.4">un</w> <w n="44.5">front</w> <w n="44.6">démesuré</w></l>
					<l n="45" num="1.45"><w n="45.1">Sur</w> <w n="45.2">les</w> <w n="45.3">cieux</w> <w n="45.4">dont</w> <w n="45.5">au</w> <w n="45.6">loin</w> <w n="45.7">la</w> <w n="45.8">profondeur</w> <w n="45.9">s</w>’<w n="45.10">azure</w>,</l>
					<l n="46" num="1.46"><w n="46.1">Tressaillit</w> <w n="46.2">dans</w> <w n="46.3">ses</w> <w n="46.4">flancs</w> <w n="46.5">et</w> <w n="46.6">lava</w> <w n="46.7">la</w> <w n="46.8">blessure</w>.</l>
					<l n="47" num="1.47"><w n="47.1">Et</w>, <w n="47.2">rappelant</w> <w n="47.3">ainsi</w> <w n="47.4">des</w> <w n="47.5">crimes</w> <w n="47.6">odieux</w>,</l>
					<l n="48" num="1.48"><w n="48.1">Elle</w> <w n="48.2">nommait</w> <w n="48.3">tout</w> <w n="48.4">bas</w> <w n="48.5">les</w> <w n="48.6">meurtriers</w> <w n="48.7">des</w> <w n="48.8">dieux</w> :</l>
					<l n="49" num="1.49"><w n="49.1">Hercule</w>, <w n="49.2">nourrisson</w> <w n="49.3">de</w> <w n="49.4">la</w> <w n="49.5">guerre</w> <w n="49.6">et</w>, <w n="49.7">comme</w> <w n="49.8">elle</w>,</l>
					<l n="50" num="1.50"><w n="50.1">Ivre</w> <w n="50.2">d</w>’<w n="50.3">horreur</w>, <w n="50.4">blessant</w> <w n="50.5">Hèra</w> <w n="50.6">sous</w> <w n="50.7">la</w> <w n="50.8">mamelle</w> ;</l>
					<l n="51" num="1.51"><w n="51.1">Éphialte</w>, <w n="51.2">en</w> <w n="51.3">dépit</w> <w n="51.4">du</w> <w n="51.5">destin</w> <w n="51.6">souverain</w>,</l>
					<l n="52" num="1.52"><w n="52.1">Mettant</w> <w n="52.2">Arès</w> <w n="52.3">lié</w> <w n="52.4">dans</w> <w n="52.5">un</w> <w n="52.6">cachot</w> <w n="52.7">d</w>’<w n="52.8">airain</w>,</l>
					<l n="53" num="1.53"><w n="53.1">Et</w> <w n="53.2">l</w>’<w n="53.3">emprisonnant</w>, <w n="53.4">seul</w> <w n="53.5">avec</w> <w n="53.6">la</w> <w n="53.7">nuit</w> <w n="53.8">maudite</w>.</l>
					<l n="54" num="1.54"><w n="54.1">Puis</w>, <w n="54.2">prenant</w> <w n="54.3">en</w> <w n="54.4">ses</w> <w n="54.5">bras</w> <w n="54.6">la</w> <w n="54.7">céleste</w> <w n="54.8">Aphrodite</w>,</l>
					<l n="55" num="1.55"><w n="55.1">Sans</w> <w n="55.2">peine</w> <w n="55.3">elle</w> <w n="55.4">étendit</w> <w n="55.5">ses</w> <w n="55.6">membres</w> <w n="55.7">assoupis</w></l>
					<l n="56" num="1.56"><w n="56.1">Sur</w> <w n="56.2">des</w> <w n="56.3">toisons</w> <w n="56.4">sans</w> <w n="56.5">tache</w> <w n="56.6">et</w> <w n="56.7">de</w> <w n="56.8">moelleux</w> <w n="56.9">tapis</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Car</w> <w n="57.2">déjà</w> <w n="57.3">le</w> <w n="57.4">sommeil</w>, <w n="57.5">né</w> <w n="57.6">de</w> <w n="57.7">l</w>’<w n="57.8">ombre</w> <w n="57.9">éternelle</w>,</l>
					<l n="58" num="1.58"><w n="58.1">Roulait</w> <w n="58.2">un</w> <w n="58.3">sable</w> <w n="58.4">fin</w> <w n="58.5">dans</w> <w n="58.6">sa</w> <w n="58.7">noire</w> <w n="58.8">prunelle</w> ;</l>
					<l n="59" num="1.59"><w n="59.1">Et</w> <w n="59.2">comme</w> <w n="59.3">Dioné</w>, <w n="59.4">redoutable</w> <w n="59.5">aux</w> <w n="59.6">méchants</w>,</l>
					<l n="60" num="1.60"><w n="60.1">Se</w> <w n="60.2">souvenait</w> <w n="60.3">encor</w> <w n="60.4">des</w> <w n="60.5">invincibles</w> <w n="60.6">chants</w></l>
					<l n="61" num="1.61"><w n="61.1">Avec</w> <w n="61.2">lesquels</w>, <w n="61.3">avant</w> <w n="61.4">de</w> <w n="61.5">subir</w> <w n="61.6">leurs</w> <w n="61.7">désastres</w>,</l>
					<l n="62" num="1.62"><w n="62.1">Les</w> <w n="62.2">titans</w> <w n="62.3">conduisaient</w> <w n="62.4">le</w> <w n="62.5">blanc</w> <w n="62.6">troupeau</w> <w n="62.7">des</w> <w n="62.8">astres</w></l>
					<l n="63" num="1.63"><w n="63.1">Soucieuse</w> <w n="63.2">de</w> <w n="63.3">voir</w> <w n="63.4">la</w> <w n="63.5">déesse</w> <w n="63.6">frémir</w>,</l>
					<l n="64" num="1.64"><w n="64.1">Elle</w> <w n="64.2">disait</w> <w n="64.3">ces</w> <w n="64.4">chants</w> <w n="64.5">sacrés</w> <w n="64.6">pour</w> <w n="64.7">l</w>’<w n="64.8">endormir</w>,</l>
					<l n="65" num="1.65"><w n="65.1">Douce</w> <w n="65.2">et</w> <w n="65.3">baissant</w> <w n="65.4">la</w> <w n="65.5">voix</w> <w n="65.6">bien</w> <w n="65.7">plus</w> <w n="65.8">qu</w>’<w n="65.9">à</w> <w n="65.10">l</w>’<w n="65.11">ordinaire</w>,</l>
					<l n="66" num="1.66"><w n="66.1">Et</w> <w n="66.2">les</w> <w n="66.3">mortels</w> <w n="66.4">croyaient</w> <w n="66.5">que</w> <w n="66.6">c</w>’<w n="66.7">était</w> <w n="66.8">le</w> <w n="66.9">tonnerre</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1874">jeudi 20 août 1874</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>