<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN220">
				<head type="number">XXVIII</head>
				<head type="main">Petit Noël</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">petit</w> <w n="1.3">à</w> <w n="1.4">face</w> <w n="1.5">minée</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">l</w>’<w n="2.3">œil</w> <w n="2.4">est</w> <w n="2.5">comme</w> <w n="2.6">un</w> <w n="2.7">pâle</w> <w n="2.8">ciel</w>,</l>
					<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2">approche</w> <w n="3.3">de</w> <w n="3.4">la</w> <w n="3.5">cheminée</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Tout</w> <w n="4.2">tremblant</w>, <w n="4.3">le</w> <w n="4.4">soir</w> <w n="4.5">de</w> <w n="4.6">Noël</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pourtant</w>, <w n="5.2">la</w> <w n="5.3">misère</w> <w n="5.4">et</w> <w n="5.5">la</w> <w n="5.6">fièvre</w></l>
					<l n="6" num="2.2"><w n="6.1">N</w>’<w n="6.2">ont</w> <w n="6.3">pas</w> <w n="6.4">diminué</w> <w n="6.5">l</w>’<w n="6.6">air</w> <w n="6.7">fin</w></l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">spirituel</w> <w n="7.3">de</w> <w n="7.4">sa</w> <w n="7.5">lèvre</w>.</l>
					<l n="8" num="2.4"><w n="8.1">Il</w> <w n="8.2">est</w> <w n="8.3">très</w> <w n="8.4">maigre</w>, <w n="8.5">et</w> <w n="8.6">bleu</w> <w n="8.7">de</w> <w n="8.8">faim</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Depuis</w> <w n="9.2">si</w> <w n="9.3">longtemps</w> <w n="9.4">qu</w>’<w n="9.5">il</w> <w n="9.6">l</w>’<w n="9.7">a</w> <w n="9.8">mise</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Traînent</w> <w n="10.2">les</w> <w n="10.3">lambeaux</w> <w n="10.4">décousus</w></l>
					<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">sa</w> <w n="11.3">malheureuse</w> <w n="11.4">chemise</w>.</l>
					<l n="12" num="3.4"><w n="12.1">Oh</w> ! <w n="12.2">dit</w>-<w n="12.3">il</w>, <w n="12.4">bon</w> <w n="12.5">petit</w> <w n="12.6">Jésus</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Toi</w> <w n="13.2">sur</w> <w n="13.3">qui</w> <w n="13.4">la</w> <w n="13.5">lumière</w> <w n="13.6">joue</w></l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">qui</w> <w n="14.3">souris</w> <w n="14.4">dans</w> <w n="14.5">ton</w> <w n="14.6">berceau</w> !</l>
					<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">marche</w> <w n="15.3">pieds</w> <w n="15.4">nus</w> <w n="15.5">dans</w> <w n="15.6">la</w> <w n="15.7">boue</w></l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">dans</w> <w n="16.3">la</w> <w n="16.4">fange</w> <w n="16.5">du</w> <w n="16.6">ruisseau</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">O</w> <w n="17.2">petit</w> <w n="17.3">Jésus</w> <w n="17.4">adorable</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Que</w> <w n="18.2">parent</w> <w n="18.3">de</w> <w n="18.4">riches</w> <w n="18.5">colliers</w> !</l>
					<l n="19" num="5.3"><w n="19.1">Si</w> <w n="19.2">tu</w> <w n="19.3">veux</w> <w n="19.4">m</w>’<w n="19.5">être</w> <w n="19.6">secourable</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Donne</w>-<w n="20.2">moi</w> <w n="20.3">d</w>’<w n="20.4">abord</w> <w n="20.5">des</w> <w n="20.6">souliers</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Des</w> <w n="21.2">souliers</w> <w n="21.3">trop</w> <w n="21.4">neufs</w> <w n="21.5">pour</w> <w n="21.6">se</w> <w n="21.7">taire</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Des</w> <w n="22.2">souliers</w> <w n="22.3">qui</w> <w n="22.4">fassent</w> : <w n="22.5">Coin</w> ! <w n="22.6">coin</w> !</l>
					<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">mènent</w> <w n="23.3">tant</w> <w n="23.4">de</w> <w n="23.5">bruit</w> <w n="23.6">par</w> <w n="23.7">terre</w></l>
					<l n="24" num="6.4"><w n="24.1">Qu</w>’<w n="24.2">on</w> <w n="24.3">m</w>’<w n="24.4">entende</w> <w n="24.5">venir</w> <w n="24.6">de</w> <w n="24.7">loin</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Puis</w>, <w n="25.2">comme</w> <w n="25.3">toi</w> <w n="25.4">seul</w> <w n="25.5">es</w> <w n="25.6">le</w> <w n="25.7">maître</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Afin</w> <w n="26.2">de</w> <w n="26.3">m</w>’<w n="26.4">aiguiser</w> <w n="26.5">les</w> <w n="26.6">dents</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Bon</w> <w n="27.2">Jésus</w>, <w n="27.3">tu</w> <w n="27.4">pourras</w> <w n="27.5">peut</w>-<w n="27.6">être</w></l>
					<l n="28" num="7.4"><w n="28.1">Mettre</w> <w n="28.2">un</w> <w n="28.3">peu</w> <w n="28.4">de</w> <w n="28.5">bonbon</w> <w n="28.6">dedans</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">28 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>