<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN284">
				<head type="number">XCII</head>
				<head type="main">Les Tristes</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Elles</w> <w n="1.2">passent</w> <w n="1.3">insolemment</w></l>
					<l n="2" num="1.2"><w n="2.1">Sur</w> <w n="2.2">le</w> <w n="2.3">dur</w> <w n="2.4">tapis</w> <w n="2.5">du</w> <w n="2.6">bitume</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Appelant</w> <w n="3.2">du</w> <w n="3.3">regard</w> <w n="3.4">l</w>’<w n="3.5">amant</w></l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">pour</w> <w n="4.3">un</w> <w n="4.4">instant</w> <w n="4.5">s</w>’<w n="4.6">accoutume</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Comme</w> <w n="5.2">hier</w> <w n="5.3">et</w> <w n="5.4">comme</w> <w n="5.5">demain</w>,</l>
					<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2">un</w> <w n="6.3">pas</w> <w n="6.4">tantôt</w> <w n="6.5">lent</w> <w n="6.6">ou</w> <w n="6.7">rapide</w></l>
					<l n="7" num="2.3"><w n="7.1">Elles</w> <w n="7.2">arpentent</w> <w n="7.3">le</w> <w n="7.4">chemin</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Calmes</w> <w n="8.2">comme</w> <w n="8.3">un</w> <w n="8.4">bétail</w> <w n="8.5">stupide</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Leurs</w> <w n="9.2">corsages</w> <w n="9.3">voluptueux</w></l>
					<l n="10" num="3.2"><w n="10.1">Provoquent</w> <w n="10.2">des</w> <w n="10.3">épithalames</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Alors</w> <w n="11.2">des</w> <w n="11.3">mortels</w> <w n="11.4">vertueux</w></l>
					<l n="12" num="3.4"><w n="12.1">Passent</w>, <w n="12.2">tenant</w> <w n="12.3">au</w> <w n="12.4">bras</w> <w n="12.5">leurs</w> <w n="12.6">femmes</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Oh</w> ! <w n="13.2">disent</w>-<w n="13.3">ils</w>, <w n="13.4">voilà</w> <w n="13.5">le</w> <w n="13.6">ton</w></l>
					<l n="14" num="4.2"><w n="14.1">Donné</w> <w n="14.2">par</w> <w n="14.3">nos</w> <w n="14.4">littératures</w> !</l>
					<l n="15" num="4.3"><w n="15.1">Tête</w> <w n="15.2">et</w> <w n="15.3">sang</w> ! <w n="15.4">comment</w> <w n="15.5">laisse</w>-<w n="15.6">t</w>-<w n="15.7">on</w></l>
					<l n="16" num="4.4"><w n="16.1">Sortir</w> <w n="16.2">de</w> <w n="16.3">telles</w> <w n="16.4">créatures</w> ?</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Tels</w> <w n="17.2">ces</w> <w n="17.3">orateurs</w> <w n="17.4">oublieux</w></l>
					<l n="18" num="5.2"><w n="18.1">Se</w> <w n="18.2">courroucent</w>, <w n="18.3">et</w> <w n="18.4">leur</w> <w n="18.5">flot</w> <w n="18.6">passe</w>.</l>
					<l n="19" num="5.3"><w n="19.1">Les</w> <w n="19.2">Tristes</w> <w n="19.3">les</w> <w n="19.4">suivent</w> <w n="19.5">des</w> <w n="19.6">yeux</w></l>
					<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">leur</w> <w n="20.3">répondent</w> <w n="20.4">à</w> <w n="20.5">voix</w> <w n="20.6">basse</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Ayant</w> <w n="21.2">pour</w> <w n="21.3">unique</w> <w n="21.4">témoin</w></l>
					<l n="22" num="6.2"><w n="22.1">Le</w> <w n="22.2">souvenir</w> <w n="22.3">d</w>’<w n="22.4">une</w> <w n="22.5">heure</w> <w n="22.6">tendre</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Elles</w> <w n="23.2">disent</w>, <w n="23.3">parlant</w> <w n="23.4">de</w> <w n="23.5">loin</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Comme</w> <w n="24.2">s</w>’<w n="24.3">ils</w> <w n="24.4">pouvaient</w> <w n="24.5">les</w> <w n="24.6">entendre</w> :</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Oui</w>, <w n="25.2">nous</w> <w n="25.3">sommes</w> <w n="25.4">joie</w> <w n="25.5">et</w> <w n="25.6">douleur</w> !</l>
					<l n="26" num="7.2"><w n="26.1">Mais</w> <w n="26.2">n</w>’<w n="26.3">ayez</w> <w n="26.4">pas</w> <w n="26.5">un</w> <w n="26.6">air</w> <w n="26.7">morose</w></l>
					<l n="27" num="7.3"><w n="27.1">En</w> <w n="27.2">voyant</w> <w n="27.3">nos</w> <w n="27.4">lèvres</w> <w n="27.5">en</w> <w n="27.6">fleur</w></l>
					<l n="28" num="7.4"><w n="28.1">Aussi</w> <w n="28.2">banales</w> <w n="28.3">qu</w>’<w n="28.4">une</w> <w n="28.5">rose</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Troupeau</w> <w n="29.2">docile</w> <w n="29.3">et</w> <w n="29.4">châtié</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Nous</w> <w n="30.2">marchons</w> <w n="30.3">là</w>, <w n="30.4">troublantes</w> <w n="30.5">Èves</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Mais</w> <w n="31.2">ayez</w> <w n="31.3">un</w> <w n="31.4">peu</w> <w n="31.5">de</w> <w n="31.6">pitié</w></l>
					<l n="32" num="8.4"><w n="32.1">Pour</w> <w n="32.2">les</w> <w n="32.3">fantômes</w> <w n="32.4">de</w> <w n="32.5">vos</w> <w n="32.6">rêves</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Rasant</w> <w n="33.2">toujours</w> <w n="33.3">à</w> <w n="33.4">pas</w> <w n="33.5">furtifs</w></l>
					<l n="34" num="9.2"><w n="34.1">Les</w> <w n="34.2">murs</w> <w n="34.3">de</w> <w n="34.4">pierres</w> <w n="34.5">ou</w> <w n="34.6">de</w> <w n="34.7">briques</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Nous</w> <w n="35.2">sommes</w> <w n="35.3">des</w> <w n="35.4">êtres</w> <w n="35.5">fictifs</w></l>
					<l n="36" num="9.4"><w n="36.1">Créés</w> <w n="36.2">par</w> <w n="36.3">vos</w> <w n="36.4">désirs</w> <w n="36.5">lubriques</w> ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Vos</w> <w n="37.2">bras</w> <w n="37.3">difformes</w> <w n="37.4">et</w> <w n="37.5">velus</w></l>
					<l n="38" num="10.2"><w n="38.1">Sont</w> <w n="38.2">ceux</w> <w n="38.3">où</w> <w n="38.4">nous</w> <w n="38.5">nous</w> <w n="38.6">reposâmes</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">nous</w> <w n="39.3">ne</w> <w n="39.4">sommes</w> <w n="39.5">rien</w> <w n="39.6">de</w> <w n="39.7">plus</w></l>
					<l n="40" num="10.4"><w n="40.1">Que</w> <w n="40.2">les</w> <w n="40.3">figures</w> <w n="40.4">de</w> <w n="40.5">vos</w> <w n="40.6">âmes</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">12 mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>