<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN269">
				<head type="number">LXXVII</head>
				<head type="main">Vieux Jeu</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">dont</w> <w n="1.3">brillait</w> <w n="1.4">la</w> <w n="1.5">gloire</w> <w n="1.6">éparse</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Nous</w> <w n="2.2">délaissez</w>-<w n="2.3">vous</w>, <w n="2.4">comme</w> <w n="2.5">ingrats</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Apothicaires</w> <w n="3.2">de</w> <w n="3.3">la</w> <w n="3.4">Farce</w> ?</l>
					<l n="4" num="1.4"><w n="4.1">Voici</w> <w n="4.2">venir</w> <w n="4.3">le</w> <w n="4.4">mardi</w>-<w n="4.5">gras</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Cependant</w>, — <w n="5.2">sans</w> <w n="5.3">doute</w> <w n="5.4">on</w> <w n="5.5">vous</w> <w n="5.6">triche</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Voyageurs</w> <w n="6.2">d</w>’<w n="6.3">Aix</w> <w n="6.4">et</w> <w n="6.5">de</w> <w n="6.6">Cognac</w> ! —</l>
					<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">n</w>’<w n="7.3">ai</w> <w n="7.4">pas</w> <w n="7.5">vu</w> <w n="7.6">que</w> <w n="7.7">nulle</w> <w n="7.8">affiche</w></l>
					<l n="8" num="2.4"><w n="8.1">Annonçât</w> <w n="8.2">encor</w> <w n="8.3">Pourceaugnac</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ce</w> <w n="9.2">jour</w>-<w n="9.3">là</w>, <w n="9.4">tout</w> <w n="9.5">ruisselant</w> <w n="9.6">d</w>’<w n="9.7">aise</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Le</w> <w n="10.2">bon</w> <w n="10.3">bourgeois</w>, <w n="10.4">c</w>’<w n="10.5">était</w> <w n="10.6">son</w> <w n="10.7">dû</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Voyait</w>, <w n="11.2">en</w> <w n="11.3">emportant</w> <w n="11.4">sa</w> <w n="11.5">chaise</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Pourceaugnac</w> <w n="12.2">s</w>’<w n="12.3">enfuir</w>, <w n="12.4">éperdu</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Alors</w>, <w n="13.2">oh</w> ! <w n="13.3">que</w> <w n="13.4">d</w>’<w n="13.5">apothicaires</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Minces</w>, <w n="14.2">grands</w>, <w n="14.3">petits</w>, <w n="14.4">bedonnés</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Avec</w> <w n="15.2">des</w> <w n="15.3">jambes</w> <w n="15.4">en</w> <w n="15.5">équerres</w></l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">de</w> <w n="16.3">longs</w> <w n="16.4">nez</w> <w n="16.5">désordonnés</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w>, <w n="17.2">par</w> <w n="17.3">le</w> <w n="17.4">Styx</w> ! <w n="17.5">que</w> <w n="17.6">de</w> <w n="17.7">seringues</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Dont</w> <w n="18.2">les</w> <w n="18.3">porteurs</w> <w n="18.4">affreux</w>, <w n="18.5">galants</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Graves</w> <w n="19.2">comme</w> <w n="19.3">des</w> <w n="19.4">camerlingues</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Ou</w> <w n="20.2">sauvages</w> <w n="20.3">et</w> <w n="20.4">turbulents</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Troupes</w> <w n="21.2">par</w> <w n="21.3">des</w> <w n="21.4">troupes</w> <w n="21.5">rejointes</w>,</l>
					<l n="22" num="6.2"><w n="22.1">En</w> <w n="22.2">leur</w> <w n="22.3">effrayant</w> <w n="22.4">magasin</w></l>
					<l n="23" num="6.3"><w n="23.1">Braquaient</w> <w n="23.2">les</w> <w n="23.3">redoutables</w> <w n="23.4">pointes</w></l>
					<l n="24" num="6.4"><w n="24.1">Vers</w> <w n="24.2">le</w> <w n="24.3">fauteuil</w> <w n="24.4">du</w> <w n="24.5">Limosin</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Des</w> <w n="25.2">filles</w> <w n="25.3">aussi</w>, <w n="25.4">grandes</w> <w n="25.5">bringues</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Jouaient</w>, <w n="26.2">en</w> <w n="26.3">habit</w> <w n="26.4">travesti</w>,</l>
					<l n="27" num="7.3"><w n="27.1">D</w>’<w n="27.2">étranges</w> <w n="27.3">porteurs</w> <w n="27.4">de</w> <w n="27.5">seringues</w></l>
					<l n="28" num="7.4"><w n="28.1">Suivant</w> <w n="28.2">Pourceaugnac</w> <w n="28.3">investi</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">des</w> <w n="29.3">enfants</w>, <w n="29.4">encor</w> <w n="29.5">précaires</w>.</l>
					<l n="30" num="8.2"><w n="30.1">Jouaient</w>, <w n="30.2">ouvrant</w> <w n="30.3">leurs</w> <w n="30.4">yeux</w> <w n="30.5">de</w> <w n="30.6">jais</w>,</l>
					<l n="31" num="8.3"><w n="31.1">De</w> <w n="31.2">tout</w> <w n="31.3">petits</w> <w n="31.4">apothicaires</w></l>
					<l n="32" num="8.4"><w n="32.1">Braquant</w> <w n="32.2">de</w> <w n="32.3">tout</w> <w n="32.4">petits</w> <w n="32.5">objets</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Mais</w> <w n="33.2">quoi</w> ! <w n="33.3">la</w> <w n="33.4">Farce</w> <w n="33.5">est</w> <w n="33.6">abolie</w></l>
					<l n="34" num="9.2"><w n="34.1">Autant</w> <w n="34.2">que</w> <w n="34.3">l</w>’<w n="34.4">Almanach</w> <w n="34.5">Liégeois</w> :</l>
					<l n="35" num="9.3"><w n="35.1">On</w> <w n="35.2">ne</w> <w n="35.3">veut</w> <w n="35.4">plus</w> <w n="35.5">de</w> <w n="35.6">sa</w> <w n="35.7">folie</w>.</l>
					<l n="36" num="9.4"><w n="36.1">Jeunes</w> <w n="36.2">élèves</w> <w n="36.3">et</w> <w n="36.4">bourgeois</w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Soyez</w> <w n="37.2">gais</w>, <w n="37.3">mangez</w> <w n="37.4">des</w> <w n="37.5">meringues</w> !</l>
					<l n="38" num="10.2"><w n="38.1">Mais</w> <w n="38.2">amusez</w>-<w n="38.3">vous</w>, <w n="38.4">gravement</w>.</l>
					<l n="39" num="10.3"><w n="39.1">Les</w> <w n="39.2">matassins</w> <w n="39.3">et</w> <w n="39.4">les</w> <w n="39.5">seringues</w></l>
					<l n="40" num="10.4"><w n="40.1">Ne</w> <w n="40.2">sont</w> <w n="40.3">plus</w> <w n="40.4">dans</w> <w n="40.5">le</w> <w n="40.6">mouvement</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">26 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>