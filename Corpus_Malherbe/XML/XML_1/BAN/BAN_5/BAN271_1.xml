<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN271">
				<head type="number">LXXIX</head>
					<head type="main">Anniversaire</head>
					<head type="sub_1">26 février</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">mon</w> <w n="1.3">Maître</w> ! <w n="1.4">un</w> <w n="1.5">nouveau</w> <w n="1.6">printemps</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">ses</w> <w n="2.3">souffles</w> <w n="2.4">palpitants</w></l>
					<l n="3" num="1.3"><w n="3.1">Baise</w> <w n="3.2">ta</w> <w n="3.3">chevelure</w>, <w n="3.4">insigne</w></l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Comme</w> <w n="4.2">le</w> <w n="4.3">cygne</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Tes</w> <w n="5.2">deux</w> <w n="5.3">enfants</w> <w n="5.4">sont</w> <w n="5.5">dans</w> <w n="5.6">tes</w> <w n="5.7">bras</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">tout</w> <w n="6.3">ce</w> <w n="6.4">que</w> <w n="6.5">tu</w> <w n="6.6">célébras</w></l>
					<l n="7" num="2.3"><w n="7.1">Vient</w> <w n="7.2">acclamer</w> <w n="7.3">ta</w> <w n="7.4">force</w> <w n="7.5">élue</w></l>
					<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">Et</w> <w n="8.2">te</w> <w n="8.3">salue</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Au</w> <w n="9.2">loin</w>, <w n="9.3">sous</w> <w n="9.4">la</w> <w n="9.5">rumeur</w> <w n="9.6">du</w> <w n="9.7">flot</w>,</l>
					<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">mer</w> <w n="10.3">te</w> <w n="10.4">dit</w>, <w n="10.5">dans</w> <w n="10.6">un</w> <w n="10.7">sanglot</w> :</l>
					<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2">ai</w> <w n="11.3">moins</w> <w n="11.4">de</w> <w n="11.5">colère</w> <w n="11.6">et</w> <w n="11.7">de</w> <w n="11.8">rages</w></l>
					<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">Que</w> <w n="12.2">tes</w> <w n="12.3">orages</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">bois</w> <w n="13.3">touffu</w> <w n="13.4">te</w> <w n="13.5">dit</w> : <w n="13.6">J</w>’<w n="13.7">ai</w> <w n="13.8">moins</w></l>
					<l n="14" num="4.2"><w n="14.1">D</w>’<w n="14.2">oiseaux</w>, <w n="14.3">les</w> <w n="14.4">cieux</w> <w n="14.5">m</w>’<w n="14.6">en</w> <w n="14.7">sont</w> <w n="14.8">témoins</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Que</w> <w n="15.2">n</w>’<w n="15.3">en</w> <w n="15.4">accueille</w> <w n="15.5">dans</w> <w n="15.6">son</w> <w n="15.7">ombre</w></l>
					<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">Ta</w> <w n="16.2">strophe</w> <w n="16.3">sombre</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Le</w> <w n="17.2">ciel</w>, <w n="17.3">en</w> <w n="17.4">son</w> <w n="17.5">tragique</w> <w n="17.6">effroi</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Dit</w> : <w n="18.2">Ton</w> <w n="18.3">esprit</w> <w n="18.4">est</w>, <w n="18.5">comme</w> <w n="18.6">moi</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Plein</w> <w n="19.2">de</w> <w n="19.3">gouffres</w> <w n="19.4">et</w> <w n="19.5">de</w> <w n="19.6">désastres</w>,</l>
					<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1">Mais</w> <w n="20.2">criblé</w> <w n="20.3">d</w>’<w n="20.4">astres</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Le</w> <w n="21.2">glaive</w>, <w n="21.3">au</w> <w n="21.4">chaste</w> <w n="21.5">éclair</w> <w n="21.6">d</w>’<w n="21.7">acier</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Te</w> <w n="22.2">dit</w> : <w n="22.3">Poëte</w> <w n="22.4">et</w> <w n="22.5">justicier</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Je</w> <w n="23.2">suis</w> <w n="23.3">effrayant</w>, <w n="23.4">moi</w> <w n="23.5">le</w> <w n="23.6">glaive</w>,</l>
					<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">Moins</w> <w n="24.2">que</w> <w n="24.3">ton</w> <w n="24.4">rêve</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">la</w> <w n="25.3">lyre</w>, <w n="25.4">pleine</w> <w n="25.5">de</w> <w n="25.6">voix</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Que</w> <w n="26.2">seul</w> <w n="26.3">tu</w> <w n="26.4">touches</w> <w n="26.5">et</w> <w n="26.6">tu</w> <w n="26.7">vois</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Murmure</w> : <w n="27.2">Je</w> <w n="27.3">suis</w> <w n="27.4">ta</w> <w n="27.5">servante</w></l>
					<l n="28" num="7.4"><space quantity="8" unit="char"></space><w n="28.1">Et</w> <w n="28.2">je</w> <w n="28.3">m</w>’<w n="28.4">en</w> <w n="28.5">vante</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">les</w> <w n="29.3">humbles</w> <w n="29.4">et</w> <w n="29.5">les</w> <w n="29.6">petits</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Déchirés</w> <w n="30.2">par</w> <w n="30.3">leurs</w> <w n="30.4">appétits</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Les</w> <w n="31.2">groupes</w> <w n="31.3">cent</w> <w n="31.4">fois</w> <w n="31.5">adorables</w></l>
					<l n="32" num="8.4"><space quantity="8" unit="char"></space><w n="32.1">Des</w> <w n="32.2">misérables</w> ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Les</w> <w n="33.2">femmes</w>, <w n="33.3">si</w> <w n="33.4">souvent</w> <w n="33.5">en</w> <w n="33.6">pleurs</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Que</w> <w n="34.2">tout</w> <w n="34.3">blesse</w>, <w n="34.4">comme</w> <w n="34.5">des</w> <w n="34.6">fleurs</w> ;</l>
					<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">les</w> <w n="35.3">cohortes</w> <w n="35.4">vagabondes</w>,</l>
					<l n="36" num="9.4"><space quantity="8" unit="char"></space><w n="36.1">Les</w> <w n="36.2">têtes</w> <w n="36.3">blondes</w> ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Les</w> <w n="37.2">enfants</w>, <w n="37.3">dont</w> <w n="37.4">tu</w> <w n="37.5">sais</w> <w n="37.6">les</w> <w n="37.7">noms</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Te</w> <w n="38.2">disent</w> : <w n="38.3">Maître</w>, <w n="38.4">nous</w> <w n="38.5">venons</w></l>
					<l n="39" num="10.3"><w n="39.1">Louer</w> <w n="39.2">la</w> <w n="39.3">douceur</w> <w n="39.4">infinie</w></l>
					<l n="40" num="10.4"><space quantity="8" unit="char"></space><w n="40.1">De</w> <w n="40.2">ton</w> <w n="40.3">génie</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">O</w> <w n="41.2">grand</w> <w n="41.3">songeur</w> <w n="41.4">plein</w> <w n="41.5">de</w> <w n="41.6">pitié</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Par</w> <w n="42.2">qui</w> <w n="42.3">le</w> <w n="42.4">crime</w> <w n="42.5">est</w> <w n="42.6">châtié</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Terrasse</w> <w n="43.2">la</w> <w n="43.3">haine</w> <w n="43.4">méchante</w> :</l>
					<l n="44" num="11.4"><space quantity="8" unit="char"></space><w n="44.1">Vis</w> ! <w n="44.2">Aime</w> ! <w n="44.3">Chante</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Marche</w>, <w n="45.2">auguste</w>, <w n="45.3">dans</w> <w n="45.4">ton</w> <w n="45.5">chemin</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Et</w> <w n="46.2">contre</w> <w n="46.3">tout</w> <w n="46.4">glaive</w> <w n="46.5">inhumain</w></l>
					<l n="47" num="12.3"><w n="47.1">Lève</w> <w n="47.2">ta</w> <w n="47.3">main</w> <w n="47.4">pensive</w> <w n="47.5">et</w> <w n="47.6">calme</w></l>
					<l n="48" num="12.4"><space quantity="8" unit="char"></space><w n="48.1">Qui</w> <w n="48.2">tient</w> <w n="48.3">la</w> <w n="48.4">palme</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">26 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>