<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN225">
				<head type="number">XXXIII</head>
				<head type="main">Pas de neige</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Paris</w>, <w n="1.2">lorsque</w> <w n="1.3">vient</w> <w n="1.4">la</w> <w n="1.5">froidure</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Aime</w>, <w n="2.2">pendant</w> <w n="2.3">la</w> <w n="2.4">saison</w> <w n="2.5">dure</w>,</l>
					<l n="3" num="1.3"><w n="3.1">A</w> <w n="3.2">s</w>’<w n="3.3">orner</w> <w n="3.4">de</w> <w n="3.5">martre</w> <w n="3.6">et</w> <w n="3.7">de</w> <w n="3.8">vair</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Désireux</w> <w n="4.2">d</w>’<w n="4.3">embellir</w> <w n="4.4">ses</w> <w n="4.5">fêtes</w></l>
					<l n="5" num="1.5"><w n="5.1">Par</w> <w n="5.2">toutes</w> <w n="5.3">ces</w> <w n="5.4">toisons</w> <w n="5.5">de</w> <w n="5.6">bêtes</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Il</w> <w n="6.2">a</w> <w n="6.3">dit</w> <w n="6.4">au</w> <w n="6.5">bonhomme</w> <w n="6.6">Hiver</w> :</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">O</w> <w n="7.2">vieil</w> <w n="7.3">Hiver</w>, <w n="7.4">père</w> <w n="7.5">des</w> <w n="7.6">glaces</w> !</l>
					<l n="8" num="2.2"><w n="8.1">Qu</w>’<w n="8.2">il</w> <w n="8.3">neige</w> <w n="8.4">sur</w> <w n="8.5">mes</w> <w n="8.6">larges</w> <w n="8.7">places</w></l>
					<l n="9" num="2.3"><w n="9.1">Et</w> <w n="9.2">sous</w> <w n="9.3">mes</w> <w n="9.4">horizons</w> <w n="9.5">étroits</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Comme</w> <w n="10.2">là</w>-<w n="10.3">bas</w>, <w n="10.4">dans</w> <w n="10.5">la</w> <w n="10.6">Norvège</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Pour</w> <w n="11.2">que</w> <w n="11.3">je</w> <w n="11.4">voie</w> <w n="11.5">un</w> <w n="11.6">peu</w> <w n="11.7">de</w> <w n="11.8">neige</w></l>
					<l n="12" num="2.6"><w n="12.1">En</w> <w n="12.2">mil</w> <w n="12.3">huit</w> <w n="12.4">cent</w> <w n="12.5">quatre</w>-<w n="12.6">vingt</w>-<w n="12.7">trois</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Oh</w> ! <w n="13.2">que</w> <w n="13.3">la</w> <w n="13.4">neige</w>, <w n="13.5">de</w> <w n="13.6">son</w> <w n="13.7">lustre</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Blanchisse</w> <w n="14.2">mon</w> <w n="14.3">bitume</w> <w n="14.4">illustre</w>,</l>
					<l n="15" num="3.3"><w n="15.1">Pour</w> <w n="15.2">que</w>, <w n="15.3">poëte</w> <w n="15.4">essentiel</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Je</w> <w n="16.2">compare</w>, <w n="16.3">en</w> <w n="16.4">mes</w> <w n="16.5">épigrammes</w>,</l>
					<l n="17" num="3.5"><w n="17.1">La</w> <w n="17.2">neige</w> <w n="17.3">et</w> <w n="17.4">les</w> <w n="17.5">lys</w> <w n="17.6">de</w> <w n="17.7">mes</w> <w n="17.8">femmes</w></l>
					<l n="18" num="3.6"><w n="18.1">Avec</w> <w n="18.2">les</w> <w n="18.3">lys</w> <w n="18.4">tombés</w> <w n="18.5">du</w> <w n="18.6">ciel</w> !</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Tel</w>, <w n="19.2">rêvant</w> <w n="19.3">que</w> <w n="19.4">sa</w> <w n="19.5">face</w> <w n="19.6">usée</w></l>
					<l n="20" num="4.2"><w n="20.1">Fût</w> <w n="20.2">blanche</w> <w n="20.3">comme</w> <w n="20.4">une</w> <w n="20.5">épousée</w>,</l>
					<l n="21" num="4.3"><w n="21.1">Paris</w>, <w n="21.2">en</w> <w n="21.3">son</w> <w n="21.4">désir</w> <w n="21.5">goulu</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Demandait</w> <w n="22.2">que</w> <w n="22.3">la</w> <w n="22.4">neige</w> <w n="22.5">pure</w></l>
					<l n="23" num="4.5"><w n="23.1">L</w>’<w n="23.2">enveloppât</w> <w n="23.3">de</w> <w n="23.4">sa</w> <w n="23.5">guipure</w>.</l>
					<l n="24" num="4.6"><w n="24.1">Le</w> <w n="24.2">vieil</w> <w n="24.3">Hiver</w> <w n="24.4">n</w>’<w n="24.5">a</w> <w n="24.6">pas</w> <w n="24.7">voulu</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Il</w> <w n="25.2">a</w> <w n="25.3">dit</w> : <w n="25.4">O</w> <w n="25.5">ville</w> <w n="25.6">de</w> <w n="25.7">Flore</w>,</l>
					<l n="26" num="5.2"><w n="26.1">Qui</w> <w n="26.2">toujours</w> <w n="26.3">vois</w> <w n="26.4">tes</w> <w n="26.5">lys</w> <w n="26.6">éclore</w></l>
					<l n="27" num="5.3"><w n="27.1">Et</w> <w n="27.2">tes</w> <w n="27.3">diamants</w> <w n="27.4">refleurir</w> ;</l>
					<l n="28" num="5.4"><w n="28.1">Ville</w> <w n="28.2">folle</w>, <w n="28.3">heureuse</w>, <w n="28.4">adulée</w>,</l>
					<l n="29" num="5.5"><w n="29.1">Pour</w> <w n="29.2">toi</w> <w n="29.3">la</w> <w n="29.4">neige</w> <w n="29.5">immaculée</w> ?</l>
					<l n="30" num="5.6"><w n="30.1">Allons</w>, <w n="30.2">tu</w> <w n="30.3">t</w>’<w n="30.4">en</w> <w n="30.5">ferais</w> <w n="30.6">mourir</w> !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Quoi</w> ! <w n="31.2">tes</w> <w n="31.3">histrions</w> <w n="31.4">et</w> <w n="31.5">tes</w> <w n="31.6">grues</w></l>
					<l n="32" num="6.2"><w n="32.1">Sous</w> <w n="32.2">leurs</w> <w n="32.3">semelles</w> <w n="32.4">incongrues</w></l>
					<l n="33" num="6.3"><w n="33.1">Fouleraient</w> <w n="33.2">la</w> <w n="33.3">neige</w> <w n="33.4">au</w> <w n="33.5">flanc</w> <w n="33.6">pur</w>,</l>
					<l n="34" num="6.4"><w n="34.1">La</w> <w n="34.2">neige</w>, <w n="34.3">divine</w> <w n="34.4">pucelle</w>,</l>
					<l n="35" num="6.5"><w n="35.1">Dont</w> <w n="35.2">l</w>’<w n="35.3">âpre</w> <w n="35.4">candeur</w> <w n="35.5">étincelle</w></l>
					<l n="36" num="6.6"><w n="36.1">Sous</w> <w n="36.2">les</w> <w n="36.3">caresses</w> <w n="36.4">de</w> <w n="36.5">l</w>’<w n="36.6">azur</w> !</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Non</w>. <w n="37.2">La</w> <w n="37.3">neige</w> <w n="37.4">avec</w> <w n="37.5">orgueil</w> <w n="37.6">touche</w></l>
					<l n="38" num="7.2"><w n="38.1">Les</w> <w n="38.2">champs</w> <w n="38.3">nus</w> <w n="38.4">où</w> <w n="38.5">l</w>’<w n="38.6">été</w> <w n="38.7">farouche</w></l>
					<l n="39" num="7.3"><w n="39.1">Faisait</w> <w n="39.2">ruisseler</w> <w n="39.3">des</w> <w n="39.4">épis</w></l>
					<l n="40" num="7.4"><w n="40.1">Qui</w> <w n="40.2">sont</w> <w n="40.3">la</w> <w n="40.4">joie</w> <w n="40.5">et</w> <w n="40.6">la</w> <w n="40.7">richesse</w>.</l>
					<l n="41" num="7.5"><w n="41.1">Mais</w> <w n="41.2">toi</w>, <w n="41.3">courtisane</w> <w n="41.4">et</w> <w n="41.5">duchesse</w>,</l>
					<l n="42" num="7.6"><w n="42.1">Marche</w> <w n="42.2">sur</w> <w n="42.3">les</w> <w n="42.4">riches</w> <w n="42.5">tapis</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">La</w> <w n="43.2">neige</w> <w n="43.3">est</w> <w n="43.4">faite</w> <w n="43.5">pour</w> <w n="43.6">les</w> <w n="43.7">cimes</w></l>
					<l n="44" num="8.2"><w n="44.1">Où</w> <w n="44.2">nous</w>, <w n="44.3">les</w> <w n="44.4">Dieux</w>, <w n="44.5">nous</w> <w n="44.6">nous</w> <w n="44.7">assîmes</w> ;</l>
					<l n="45" num="8.3"><w n="45.1">Pour</w> <w n="45.2">les</w> <w n="45.3">monts</w>, <w n="45.4">où</w> <w n="45.5">la</w> <w n="45.6">Vérité</w></l>
					<l n="46" num="8.4"><w n="46.1">N</w>’<w n="46.2">entend</w> <w n="46.3">pas</w> <w n="46.4">de</w> <w n="46.5">sourdes</w> <w n="46.6">huées</w>,</l>
					<l n="47" num="8.5"><w n="47.1">Et</w> <w n="47.2">voit</w> <w n="47.3">déchirer</w> <w n="47.4">les</w> <w n="47.5">nuées</w></l>
					<l n="48" num="8.6"><w n="48.1">Par</w> <w n="48.2">le</w> <w n="48.3">vol</w> <w n="48.4">de</w> <w n="48.5">l</w>’<w n="48.6">aigle</w> <w n="48.7">irrité</w>.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">Toi</w>, <w n="49.2">promène</w>-<w n="49.3">toi</w> <w n="49.4">dans</w> <w n="49.5">la</w> <w n="49.6">boue</w> ;</l>
					<l n="50" num="9.2"><w n="50.1">Et</w>, <w n="50.2">plus</w> <w n="50.3">tard</w>, <w n="50.4">quand</w> <w n="50.5">le</w> <w n="50.6">soleil</w> <w n="50.7">joue</w>,</l>
					<l n="51" num="9.3"><w n="51.1">Dans</w> <w n="51.2">tes</w> <w n="51.3">bois</w> <w n="51.4">aux</w> <w n="51.5">sentiers</w> <w n="51.6">fleuris</w>.</l>
					<l n="52" num="9.4"><w n="52.1">Mais</w> <w n="52.2">quant</w> <w n="52.3">à</w> <w n="52.4">la</w> <w n="52.5">neige</w> <w n="52.6">divine</w>,</l>
					<l n="53" num="9.5"><w n="53.1">Je</w> <w n="53.2">la</w> <w n="53.3">garde</w> <w n="53.4">pour</w> <w n="53.5">la</w> <w n="53.6">ravine</w>.</l>
					<l n="54" num="9.6"><w n="54.1">Tu</w> <w n="54.2">t</w>’<w n="54.3">en</w> <w n="54.4">ferais</w> <w n="54.5">mourir</w>, <w n="54.6">Paris</w>.</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1"><w n="55.1">Laisse</w> <w n="55.2">au</w> <w n="55.3">chamois</w> <w n="55.4">la</w> <w n="55.5">neige</w> <w n="55.6">blanche</w>.</l>
					<l n="56" num="10.2"><w n="56.1">Mais</w> <w n="56.2">toi</w>, <w n="56.3">peureux</w> <w n="56.4">de</w> <w n="56.5">l</w>’<w n="56.6">avalanche</w>,</l>
					<l n="57" num="10.3"><w n="57.1">Au</w> <w n="57.2">son</w> <w n="57.3">du</w> <w n="57.4">luth</w> <w n="57.5">et</w> <w n="57.6">du</w> <w n="57.7">hautbois</w></l>
					<l n="58" num="10.4"><w n="58.1">Dont</w> <w n="58.2">la</w> <w n="58.3">molle</w> <w n="58.4">chanson</w> <w n="58.5">t</w>’<w n="58.6">effleure</w>,</l>
					<l n="59" num="10.5"><w n="59.1">Foule</w>, <w n="59.2">suivant</w> <w n="59.3">le</w> <w n="59.4">jour</w> <w n="59.5">et</w> <w n="59.6">l</w>’<w n="59.7">heure</w>,</l>
					<l n="60" num="10.6"><w n="60.1">Ta</w> <w n="60.2">pourpre</w>, <w n="60.3">ou</w> <w n="60.4">ton</w> <w n="60.5">pavé</w> <w n="60.6">de</w> <w n="60.7">bois</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">4 janvier 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>