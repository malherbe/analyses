<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN234">
				<head type="number">XLII</head>
				<head type="main">Comédiens</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">un</w> <w n="1.3">chariot</w>, <w n="1.4">sur</w> <w n="1.5">la</w> <w n="1.6">place</w></l>
					<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">Mangin</w> <w n="2.3">vendait</w> <w n="2.4">ses</w> <w n="2.5">crayons</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Casqué</w>, <w n="3.2">poli</w> <w n="3.3">comme</w> <w n="3.4">une</w> <w n="3.5">glace</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">la</w> <w n="4.3">gloire</w> <w n="4.4">et</w> <w n="4.5">dans</w> <w n="4.6">les</w> <w n="4.7">rayons</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">autre</w> <w n="5.3">guerrier</w>, <w n="5.4">qui</w> <w n="5.5">se</w> <w n="5.6">hâte</w></l>
					<l n="6" num="2.2"><w n="6.1">Sous</w> <w n="6.2">la</w> <w n="6.3">pluie</w> <w n="6.4">et</w> <w n="6.5">ses</w> <w n="6.6">arrosoirs</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Vend</w> <w n="7.2">avec</w> <w n="7.3">orgueil</w> <w n="7.4">une</w> <w n="7.5">pâte</w></l>
					<l n="8" num="2.4"><w n="8.1">Pour</w> <w n="8.2">faire</w> <w n="8.3">couper</w> <w n="8.4">les</w> <w n="8.5">rasoirs</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">moustachu</w>, <w n="9.3">nullement</w> <w n="9.4">glabre</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Ingénieux</w> <w n="10.2">à</w> <w n="10.3">copier</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">découpe</w> <w n="11.3">avec</w> <w n="11.4">son</w> <w n="11.5">grand</w> <w n="11.6">sabre</w></l>
					<l n="12" num="3.4"><w n="12.1">D</w>’<w n="12.2">étranges</w> <w n="12.3">portraits</w> <w n="12.4">en</w> <w n="12.5">papier</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">tout</w> <w n="13.3">est</w> <w n="13.4">changé</w>, <w n="13.5">hors</w> <w n="13.6">le</w> <w n="13.7">site</w> !</l>
					<l n="14" num="4.2"><w n="14.1">Mangin</w>, <w n="14.2">le</w> <w n="14.3">héros</w> <w n="14.4">sans</w> <w n="14.5">remords</w>,</l>
					<l n="15" num="4.3"><w n="15.1">A</w> <w n="15.2">vu</w> <w n="15.3">le</w> <w n="15.4">flot</w> <w n="15.5">noir</w> <w n="15.6">du</w> <w n="15.7">Cocyte</w>.</l>
					<l n="16" num="4.4"><w n="16.1">Il</w> <w n="16.2">est</w> <w n="16.3">au</w> <w n="16.4">rivage</w> <w n="16.5">des</w> <w n="16.6">morts</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Car</w> <w n="17.2">suffit</w>-<w n="17.3">il</w> <w n="17.4">d</w>’<w n="17.5">avoir</w> <w n="17.6">le</w> <w n="17.7">casque</w></l>
					<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">le</w> <w n="18.3">sabre</w>, <w n="18.4">farouche</w> <w n="18.5">engin</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Pour</w> <w n="19.2">s</w>’<w n="19.3">écrier</w> <w n="19.4">d</w>’<w n="19.5">un</w> <w n="19.6">ton</w> <w n="19.7">fantasque</w> :</l>
					<l n="20" num="5.4"><w n="20.1">Je</w> <w n="20.2">suis</w> <w n="20.3">Ajax</w> ! <w n="20.4">Je</w> <w n="20.5">suis</w> <w n="20.6">Mangin</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Non</w>, <w n="21.2">c</w>’<w n="21.3">en</w> <w n="21.4">est</w> <w n="21.5">fait</w>. <w n="21.6">Le</w> <w n="21.7">cours</w> <w n="21.8">des</w> <w n="21.9">astres</w></l>
					<l n="22" num="6.2"><w n="22.1">Emporte</w> <w n="22.2">dans</w> <w n="22.3">ses</w> <w n="22.4">flots</w> <w n="22.5">vermeils</w></l>
					<l n="23" num="6.3"><w n="23.1">Les</w> <w n="23.2">triomphes</w> <w n="23.3">et</w> <w n="23.4">les</w> <w n="23.5">désastres</w></l>
					<l n="24" num="6.4"><w n="24.1">Des</w> <w n="24.2">Césars</w> <w n="24.3">et</w> <w n="24.4">des</w> <w n="24.5">Rois</w>-<w n="24.6">Soleils</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Mais</w> <w n="25.2">l</w>’<w n="25.3">Histoire</w> <w n="25.4">en</w> <w n="25.5">vain</w> <w n="25.6">se</w> <w n="25.7">dépite</w></l>
					<l n="26" num="7.2"><w n="26.1">En</w> <w n="26.2">embrouillant</w> <w n="26.3">son</w> <w n="26.4">écheveau</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">la</w> <w n="27.3">foule</w> <w n="27.4">se</w> <w n="27.5">précipite</w></l>
					<l n="28" num="7.4"><w n="28.1">Vers</w> <w n="28.2">le</w> <w n="28.3">comédien</w> <w n="28.4">nouveau</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Comédien</w> ? <w n="29.2">Eh</w> <w n="29.3">oui</w>, <w n="29.4">sans</w> <w n="29.5">doute</w> !</l>
					<l n="30" num="8.2"><w n="30.1">Malgré</w> <w n="30.2">les</w> <w n="30.3">anges</w> <w n="30.4">gardiens</w></l>
					<l n="31" num="8.3"><w n="31.1">Qui</w> <w n="31.2">voudraient</w> <w n="31.3">guider</w> <w n="31.4">notre</w> <w n="31.5">route</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Nous</w> <w n="32.2">sommes</w> <w n="32.3">tous</w> <w n="32.4">comédiens</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Ayant</w> <w n="33.2">la</w> <w n="33.3">Mort</w> <w n="33.4">pour</w> <w n="33.5">spectatrice</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Tous</w>, <w n="34.2">frappés</w> <w n="34.3">du</w> <w n="34.4">même</w> <w n="34.5">fléau</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Nous</w> <w n="35.2">jouons</w> <w n="35.3">Hamlet</w> <w n="35.4">et</w> <w n="35.5">Jocrisse</w> ;</l>
					<l n="36" num="9.4"><w n="36.1">Quelques</w>-<w n="36.2">uns</w> <w n="36.3">font</w> <w n="36.4">les</w> <w n="36.5">Roméo</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Tel</w>, <w n="37.2">de</w> <w n="37.3">qui</w> <w n="37.4">la</w> <w n="37.5">folie</w> <w n="37.6">est</w> <w n="37.7">douce</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Met</w> <w n="38.2">sur</w> <w n="38.3">sa</w> <w n="38.4">poitrine</w> <w n="38.5">un</w> <w n="38.6">paillon</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">parmi</w> <w n="39.3">sa</w> <w n="39.4">perruque</w> <w n="39.5">rousse</w></l>
					<l n="40" num="10.4"><w n="40.1">Voltige</w> <w n="40.2">un</w> <w n="40.3">vague</w> <w n="40.4">papillon</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Telle</w>, <w n="41.2">aux</w> <w n="41.3">allures</w> <w n="41.4">inhumaines</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Pour</w> <w n="42.2">laquelle</w> <w n="42.3">nous</w> <w n="42.4">ergotons</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Joue</w> <w n="43.2">en</w> <w n="43.3">riant</w> <w n="43.4">les</w> <w n="43.5">Célimènes</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">telle</w> <w n="44.3">autre</w> <w n="44.4">fait</w> <w n="44.5">les</w> <w n="44.6">Gothons</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Tous</w>, <w n="45.2">Frédéricks</w> <w n="45.3">élémentaires</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Hypothétiques</w> <w n="46.2">Beauvallets</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Font</w> <w n="47.2">les</w> <w n="47.3">Dieux</w>, <w n="47.4">les</w> <w n="47.5">rois</w>, <w n="47.6">les</w> <w n="47.7">notaires</w>,</l>
					<l n="48" num="12.4"><w n="48.1">Les</w> <w n="48.2">bouffons</w>, <w n="48.3">les</w> <w n="48.4">Turcs</w>, <w n="48.5">les</w> <w n="48.6">valets</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Tel</w> <w n="49.2">fait</w> <w n="49.3">le</w> <w n="49.4">capitan</w> <w n="49.5">farouche</w>.</l>
					<l n="50" num="13.2"><w n="50.1">Moi</w>-<w n="50.2">même</w>, <w n="50.3">coiffé</w>, <w n="50.4">sans</w> <w n="50.5">humeur</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Du</w> <w n="51.2">noir</w> <w n="51.3">béret</w> <w n="51.4">de</w> <w n="51.5">Scaramouche</w>,</l>
					<l n="52" num="13.4"><w n="52.1">Je</w> <w n="52.2">joue</w> <w n="52.3">un</w> <w n="52.4">antique</w> <w n="52.5">rimeur</w>,</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Déja</w> <w n="53.2">courbé</w> <w n="53.3">par</w> <w n="53.4">l</w>’<w n="53.5">âge</w> <w n="53.6">impie</w></l>
					<l n="54" num="14.2"><w n="54.1">Et</w> <w n="54.2">par</w> <w n="54.3">son</w> <w n="54.4">souffle</w> <w n="54.5">meurtrier</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Qui</w> <w n="55.2">tousse</w> <w n="55.3">et</w> <w n="55.4">fait</w> <w n="55.5">de</w> <w n="55.6">la</w> <w n="55.7">copie</w></l>
					<l n="56" num="14.4"><w n="56.1">En</w> <w n="56.2">remâchant</w> <w n="56.3">un</w> <w n="56.4">vieux</w> <w n="56.5">laurier</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">11 janvier 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>