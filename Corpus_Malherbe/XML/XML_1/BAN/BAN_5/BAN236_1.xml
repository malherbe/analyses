<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN236">
				<head type="number">XLIV</head>
				<head type="main">Les Grimaces</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">Vérité</w> <w n="1.3">de</w> <w n="1.4">son</w> <w n="1.5">puits</w></l>
					<l n="2" num="1.2"><space quantity="12" unit="char"></space><w n="2.1">Sort</w>, <w n="2.2">et</w> <w n="2.3">puis</w></l>
					<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">leur</w> <w n="3.3">splendeur</w> <w n="3.4">ingénue</w></l>
					<l n="4" num="1.4"><w n="4.1">Montrant</w> <w n="4.2">son</w> <w n="4.3">sein</w> <w n="4.4">et</w> <w n="4.5">son</w> <w n="4.6">flanc</w></l>
					<l n="5" num="1.5"><space quantity="12" unit="char"></space><w n="5.1">De</w> <w n="5.2">lys</w> <w n="5.3">blanc</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Apparaît</w>, <w n="6.2">superbe</w> <w n="6.3">et</w> <w n="6.4">nue</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Mais</w> <w n="7.2">aussitôt</w>, <w n="7.3">les</w> <w n="7.4">satins</w></l>
					<l n="8" num="2.2"><space quantity="12" unit="char"></space><w n="8.1">Des</w> <w n="8.2">catins</w></l>
					<l n="9" num="2.3"><w n="9.1">Se</w> <w n="9.2">hérissent</w> <w n="9.3">d</w>’<w n="9.4">épouvante</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">ce</w> <w n="10.3">peuple</w> <w n="10.4">en</w> <w n="10.5">falbala</w></l>
					<l n="11" num="2.5"><space quantity="12" unit="char"></space><w n="11.1">Traite</w> <w n="11.2">la</w></l>
					<l n="12" num="2.6"><w n="12.1">Nymphe</w>, <w n="12.2">comme</w> <w n="12.3">une</w> <w n="12.4">servante</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Malgré</w> <w n="13.2">sa</w> <w n="13.3">noble</w> <w n="13.4">fraîcheur</w>,</l>
					<l n="14" num="3.2"><space quantity="12" unit="char"></space><w n="14.1">La</w> <w n="14.2">blancheur</w></l>
					<l n="15" num="3.3"><w n="15.1">De</w> <w n="15.2">ces</w> <w n="15.3">poudrederizées</w></l>
					<l n="16" num="3.4"><w n="16.1">Obscurcit</w> <w n="16.2">les</w> <w n="16.3">purs</w> <w n="16.4">accords</w></l>
					<l n="17" num="3.5"><space quantity="12" unit="char"></space><w n="17.1">De</w> <w n="17.2">son</w> <w n="17.3">corps</w>,</l>
					<l n="18" num="3.6"><w n="18.1">Dont</w> <w n="18.2">elles</w> <w n="18.3">font</w> <w n="18.4">des</w> <w n="18.5">risées</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Fi</w> ! <w n="19.2">disent</w>-<w n="19.3">elles</w>. <w n="19.4">Pour</w> <w n="19.5">nous</w>,</l>
					<l n="20" num="4.2"><space quantity="12" unit="char"></space><w n="20.1">Fronts</w> <w n="20.2">si</w> <w n="20.3">doux</w>,</l>
					<l n="21" num="4.3"><w n="21.1">Quel</w> <w n="21.2">deuil</w> <w n="21.3">que</w> <w n="21.4">ce</w> <w n="21.5">jaune</w> <w n="21.6">ivoire</w> !</l>
					<l n="22" num="4.4"><w n="22.1">Elle</w> <w n="22.2">n</w>’<w n="22.3">a</w> <w n="22.4">donc</w> <w n="22.5">ni</w> <w n="22.6">pudeur</w></l>
					<l n="23" num="4.5"><space quantity="12" unit="char"></space><w n="23.1">Ni</w> <w n="23.2">candeur</w> !</l>
					<l n="24" num="4.6"><w n="24.1">La</w> <w n="24.2">vilaine</w>, <w n="24.3">qu</w>’<w n="24.4">elle</w> <w n="24.5">est</w> <w n="24.6">noire</w> !</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">En</w> <w n="25.2">sa</w> <w n="25.3">toilette</w>, <w n="25.4">aucun</w> <w n="25.5">art</w> !</l>
					<l n="26" num="5.2"><space quantity="12" unit="char"></space><w n="26.1">Pas</w> <w n="26.2">de</w> <w n="26.3">nard</w>,</l>
					<l n="27" num="5.3"><w n="27.1">Et</w> <w n="27.2">le</w> <w n="27.3">seul</w> <w n="27.4">zéphyr</w> <w n="27.5">la</w> <w n="27.6">gante</w>.</l>
					<l n="28" num="5.4"><w n="28.1">Sa</w> <w n="28.2">croupe</w> <w n="28.3">même</w> <w n="28.4">est</w> <w n="28.5">en</w> <w n="28.6">vrai</w> !</l>
					<l n="29" num="5.5"><space quantity="12" unit="char"></space><w n="29.1">Sans</w> <w n="29.2">délai</w></l>
					<l n="30" num="5.6"><w n="30.1">Chassez</w>-<w n="30.2">moi</w> <w n="30.3">cette</w> <w n="30.4">arrogante</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">La</w> <w n="31.2">Nymphe</w> <w n="31.3">au</w> <w n="31.4">regard</w> <w n="31.5">divin</w></l>
					<l n="32" num="6.2"><space quantity="12" unit="char"></space><w n="32.1">Tâche</w> <w n="32.2">en</w> <w n="32.3">vain</w></l>
					<l n="33" num="6.3"><w n="33.1">D</w>’<w n="33.2">apaiser</w> <w n="33.3">tout</w> <w n="33.4">ce</w> <w n="33.5">tumulte</w> ;</l>
					<l n="34" num="6.4"><w n="34.1">Avec</w> <w n="34.2">un</w> <w n="34.3">grand</w> <w n="34.4">cri</w> <w n="34.5">moqueur</w>,</l>
					<l n="35" num="6.5"><space quantity="12" unit="char"></space><w n="35.1">Tout</w> <w n="35.2">le</w> <w n="35.3">chœur</w></l>
					<l n="36" num="6.6"><w n="36.1">Des</w> <w n="36.2">filles</w> <w n="36.3">roses</w> <w n="36.4">l</w>’<w n="36.5">insulte</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Ce</w> <w n="37.2">qu</w>’<w n="37.3">il</w> <w n="37.4">vous</w> <w n="37.5">faut</w> <w n="37.6">encor</w>, <w n="37.7">c</w>’<w n="37.8">est</w></l>
					<l n="38" num="7.2"><space quantity="12" unit="char"></space><w n="38.1">Un</w> <w n="38.2">corset</w>,</l>
					<l n="39" num="7.3"><w n="39.1">Disent</w>-<w n="39.2">elles</w>. <w n="39.3">Nous</w>, <w n="39.4">vos</w> <w n="39.5">dupes</w> !</l>
					<l n="40" num="7.4"><w n="40.1">Nenni</w>. <w n="40.2">Pour</w> <w n="40.3">avoir</w> <w n="40.4">du</w> <w n="40.5">chic</w></l>
					<l n="41" num="7.5"><space quantity="12" unit="char"></space><w n="41.1">En</w> <w n="41.2">public</w>,</l>
					<l n="42" num="7.6"><w n="42.1">Vous</w> <w n="42.2">manquez</w> <w n="42.3">par</w> <w n="42.4">trop</w> <w n="42.5">de</w> <w n="42.6">jupes</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Non</w>, <w n="43.2">ce</w> <w n="43.3">qui</w> <w n="43.4">plaît</w> <w n="43.5">et</w> <w n="43.6">fleurit</w></l>
					<l n="44" num="8.2"><space quantity="12" unit="char"></space><w n="44.1">Pour</w> <w n="44.2">l</w>’<w n="44.3">esprit</w>,</l>
					<l n="45" num="8.3"><w n="45.1">C</w>’<w n="45.2">est</w> <w n="45.3">la</w> <w n="45.4">robe</w>, <w n="45.5">quand</w> <w n="45.6">on</w> <w n="45.7">l</w>’<w n="45.8">ouvre</w>.</l>
					<l n="46" num="8.4"><w n="46.1">Belle</w> <w n="46.2">affaire</w>, <w n="46.3">un</w> <w n="46.4">sein</w> <w n="46.5">vivant</w> !</l>
					<l n="47" num="8.5"><space quantity="12" unit="char"></space><w n="47.1">On</w> <w n="47.2">en</w> <w n="47.3">vend</w></l>
					<l n="48" num="8.6"><w n="48.1">Aux</w> <w n="48.2">Grands</w> <w n="48.3">Magasins</w> <w n="48.4">du</w> <w n="48.5">Louvre</w>.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">Cachez</w>-<w n="49.2">le</w>, <w n="49.3">votre</w> <w n="49.4">corps</w> <w n="49.5">beau</w>,</l>
					<l n="50" num="9.2"><space quantity="12" unit="char"></space><w n="50.1">Ce</w> <w n="50.2">corbeau</w></l>
					<l n="51" num="9.3"><w n="51.1">Près</w> <w n="51.2">de</w> <w n="51.3">nos</w> <w n="51.4">blancheurs</w> <w n="51.5">de</w> <w n="51.6">cygne</w> !</l>
					<l n="52" num="9.4"><w n="52.1">Impudente</w>, <w n="52.2">détalez</w>.</l>
					<l n="53" num="9.5"><space quantity="12" unit="char"></space><w n="53.1">Vite</w>, <w n="53.2">allez</w></l>
					<l n="54" num="9.6"><w n="54.1">Mettre</w> <w n="54.2">une</w> <w n="54.3">feuille</w> <w n="54.4">de</w> <w n="54.5">vigne</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">18 janvier 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>