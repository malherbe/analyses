<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN261">
				<head type="number">LXIX</head>
				<head type="main">Jeu</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ils</w> <w n="1.2">sont</w> <w n="1.3">occupés</w> <w n="1.4">à</w> <w n="1.5">jouer</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Tous</w> <w n="2.2">bons</w> <w n="2.3">compagnons</w>, <w n="2.4">dans</w> <w n="2.5">le</w> <w n="2.6">bouge</w>,</l>
					<l n="3" num="1.3"><w n="3.1">En</w> <w n="3.2">buvant</w> <w n="3.3">jusqu</w>’<w n="3.4">à</w> <w n="3.5">s</w>’<w n="3.6">enrouer</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Pâles</w> <w n="4.2">sous</w> <w n="4.3">la</w> <w n="4.4">chandelle</w> <w n="4.5">rouge</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">un</w> <w n="5.3">d</w>’<w n="5.4">eux</w>, <w n="5.5">qui</w> <w n="5.6">s</w>’<w n="5.7">est</w> <w n="5.8">évertué</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Caresse</w> <w n="6.2">une</w> <w n="6.3">femme</w>, <w n="6.4">qui</w> <w n="6.5">rue</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Ils</w> <w n="7.2">ont</w> <w n="7.3">de</w> <w n="7.4">l</w>’<w n="7.5">or</w>, <w n="7.6">ayant</w> <w n="7.7">tué</w></l>
					<l n="8" num="2.4"><w n="8.1">Tout</w> <w n="8.2">à</w> <w n="8.3">l</w>’<w n="8.4">heure</w> <w n="8.5">un</w> <w n="8.6">vieux</w> <w n="8.7">dans</w> <w n="8.8">la</w> <w n="8.9">rue</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Là</w> <w n="9.2">sont</w> <w n="9.3">Pirot</w>, <w n="9.4">Cadet</w>, <w n="9.5">Flanquin</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Mordeval</w>, <w n="10.2">Blésimar</w>, <w n="10.3">Polyte</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Mélasse</w> <w n="11.2">en</w> <w n="11.3">chapeau</w> <w n="11.4">d</w>’<w n="11.5">Arlequin</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Ceinturon</w>, <w n="12.2">Fripouille</w>, <w n="12.3">une</w> <w n="12.4">élite</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">des</w> <w n="13.3">femmes</w> : <w n="13.4">Irma</w> <w n="13.5">Bassin</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Clarinette</w>, <w n="14.2">qui</w> <w n="14.3">vient</w> <w n="14.4">du</w> <w n="14.5">Havre</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Chiffonnette</w>, <w n="15.2">qui</w> <w n="15.3">n</w>’<w n="15.4">a</w> <w n="15.5">qu</w>’<w n="15.6">un</w> <w n="15.7">sein</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Carillon</w>, <w n="16.2">Morphine</w> <w n="16.3">et</w> <w n="16.4">Cadavre</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Avalant</w> <w n="17.2">des</w> <w n="17.3">alcools</w> <w n="17.4">verts</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Elles</w> <w n="18.2">sont</w> <w n="18.3">parfois</w> <w n="18.4">embrassées</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Laissant</w> <w n="19.2">leurs</w> <w n="19.3">corsages</w> <w n="19.4">ouverts</w></l>
					<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">leurs</w> <w n="20.3">sales</w> <w n="20.4">jupes</w> <w n="20.5">troussées</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Chiffonnette</w> <w n="21.2">dit</w> <w n="21.3">à</w> <w n="21.4">Flanquin</w> :</l>
					<l n="22" num="6.2"><w n="22.1">A</w> <w n="22.2">la</w> <w n="22.3">fin</w>, <w n="22.4">laisse</w>-<w n="22.5">moi</w> ; <w n="22.6">ça</w> <w n="22.7">m</w>’<w n="22.8">use</w> !</l>
					<l n="23" num="6.3"><w n="23.1">Irma</w> <w n="23.2">soupire</w> : <w n="23.3">Cré</w> <w n="23.4">coquin</w> !</l>
					<l n="24" num="6.4"><w n="24.1">On</w> <w n="24.2">joue</w>, <w n="24.3">on</w> <w n="24.4">se</w> <w n="24.5">saoule</w>, <w n="24.6">on</w> <w n="24.7">s</w>’<w n="24.8">amuse</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">Carillon</w>, <w n="25.3">qui</w> <w n="25.4">rêve</w> <w n="25.5">encor</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Ainsi</w> <w n="26.2">qu</w>’<w n="26.3">une</w> <w n="26.4">bête</w> <w n="26.5">assouvie</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Voit</w> <w n="27.2">se</w> <w n="27.3">mêler</w> <w n="27.4">le</w> <w n="27.5">ruisseau</w> <w n="27.6">d</w>’<w n="27.7">or</w></l>
					<l n="28" num="7.4"><w n="28.1">Avec</w> <w n="28.2">le</w> <w n="28.3">ruisseau</w> <w n="28.4">d</w>’<w n="28.5">eau</w>-<w n="28.6">de</w>-<w n="28.7">vie</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Un</w> <w n="29.2">rayon</w>, <w n="29.3">comme</w> <w n="29.4">un</w> <w n="29.5">farfadet</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Chatouille</w> <w n="30.2">ces</w> <w n="30.3">femmes</w> <w n="30.4">frivoles</w>.</l>
					<l n="31" num="8.3"><w n="31.1">Mais</w> <w n="31.2">tout</w> <w n="31.3">à</w> <w n="31.4">coup</w> <w n="31.5">le</w> <w n="31.6">grand</w> <w n="31.7">Cadet</w></l>
					<l n="32" num="8.4"><w n="32.1">Dit</w> <w n="32.2">à</w> <w n="32.3">Blésimar</w> : <w n="32.4">Tu</w> <w n="32.5">nous</w> <w n="32.6">voles</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Parbleu</w> ! <w n="33.2">tes</w> <w n="33.3">cartes</w> <w n="33.4">sont</w> <w n="33.5">de</w> <w n="33.6">poids</w>.</l>
					<l n="34" num="9.2"><w n="34.1">Ah</w> ! <w n="34.2">tu</w> <w n="34.3">marches</w> <w n="34.4">bien</w>, <w n="34.5">petit</w> <w n="34.6">homme</w> :</l>
					<l n="35" num="9.3"><w n="35.1">Elles</w> <w n="35.2">ont</w>, <w n="35.3">dessous</w>, <w n="35.4">de</w> <w n="35.5">la</w> <w n="35.6">poix</w>.</l>
					<l n="36" num="9.4"><w n="36.1">Ça</w> <w n="36.2">n</w>’<w n="36.3">est</w> <w n="36.4">pas</w> <w n="36.5">si</w> <w n="36.6">cher</w> <w n="36.7">que</w> <w n="36.8">la</w> <w n="36.9">gomme</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Mais</w> <w n="37.2">Blésimar</w>, <w n="37.3">ce</w> <w n="37.4">garnement</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Dont</w> <w n="38.2">la</w> <w n="38.3">voix</w> <w n="38.4">ainsi</w> <w n="38.5">qu</w>’<w n="38.6">une</w> <w n="38.7">strophe</w></l>
					<l n="39" num="10.3"><w n="39.1">Est</w> <w n="39.2">douce</w>, <w n="39.3">n</w>’<w n="39.4">est</w> <w n="39.5">aucunement</w></l>
					<l n="40" num="10.4"><w n="40.1">Dérouté</w> <w n="40.2">par</w> <w n="40.3">cette</w> <w n="40.4">apostrophe</w> ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Et</w> <w n="41.2">vite</w>, <w n="41.3">enfonçant</w> <w n="41.4">sur</w> <w n="41.5">son</w> <w n="41.6">front</w></l>
					<l n="42" num="11.2"><w n="42.1">Sa</w> <w n="42.2">casquette</w>, <w n="42.3">ignoble</w> <w n="42.4">couvercle</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Il</w> <w n="43.2">dit</w> : <w n="43.3">Eh</w> <w n="43.4">bien</w>, <w n="43.5">quoi</w> ? <w n="43.6">Pas</w> <w n="43.7">d</w>’<w n="43.8">affront</w>.</l>
					<l n="44" num="11.4"><w n="44.1">Je</w> <w n="44.2">vole</w> ; <w n="44.3">après</w> ? <w n="44.4">C</w>’<w n="44.5">est</w> <w n="44.6">comme</w> <w n="44.7">au</w> <w n="44.8">Cercle</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">18 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>