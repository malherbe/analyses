<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN227">
				<head type="number">XXXV</head>
				<head type="main">Politique</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Oui</w>, <w n="1.2">Misère</w> <w n="1.3">est</w> <w n="1.4">toujours</w> <w n="1.5">Misère</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Pâle</w>, <w n="2.2">avec</w> <w n="2.3">son</w> <w n="2.4">rictus</w> <w n="2.5">affreux</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Ainsi</w> <w n="3.2">que</w> <w n="3.3">les</w> <w n="3.4">grains</w> <w n="3.5">d</w>’<w n="3.6">un</w> <w n="3.7">rosaire</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Ses</w> <w n="4.2">jours</w> <w n="4.3">se</w> <w n="4.4">ressemblent</w> <w n="4.5">entre</w> <w n="4.6">eux</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Oui</w>, <w n="5.2">le</w> <w n="5.3">pauvre</w> <w n="5.4">est</w> <w n="5.5">le</w> <w n="5.6">pauvre</w>. <w n="5.7">Jeune</w></l>
					<l n="6" num="2.2"><w n="6.1">Ou</w> <w n="6.2">vieux</w>, <w n="6.3">malgré</w> <w n="6.4">ses</w> <w n="6.5">appétits</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Après</w> <w n="7.2">le</w> <w n="7.3">dur</w> <w n="7.4">travail</w>, <w n="7.5">il</w> <w n="7.6">jeûne</w></l>
					<l n="8" num="2.4"><w n="8.1">Avec</w> <w n="8.2">sa</w> <w n="8.3">femme</w> <w n="8.4">et</w> <w n="8.5">ses</w> <w n="8.6">petits</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Pour</w> <w n="9.2">lui</w> <w n="9.3">le</w> <w n="9.4">bonheur</w> <w n="9.5">est</w> <w n="9.6">un</w> <w n="9.7">mythe</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">est</w> <w n="10.3">le</w> <w n="10.4">vrai</w> <w n="10.5">souverain</w> ; <w n="10.6">mais</w></l>
					<l n="11" num="3.3"><w n="11.1">Quand</w> <w n="11.2">verra</w>-<w n="11.3">t</w>-<w n="11.4">il</w> <w n="11.5">dans</w> <w n="11.6">sa</w> <w n="11.7">marmite</w></l>
					<l n="12" num="3.4"><w n="12.1">Un</w> <w n="12.2">morceau</w> <w n="12.3">de</w> <w n="12.4">viande</w> ? <w n="12.5">Jamais</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">les</w> <w n="13.3">petits</w>, <w n="13.4">dont</w> <w n="13.5">le</w> <w n="13.6">ciel</w> <w n="13.7">aime</w></l>
					<l n="14" num="4.2"><w n="14.1">Les</w> <w n="14.2">doux</w> <w n="14.3">sourires</w> <w n="14.4">familiers</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Noir</w> <w n="15.2">et</w> <w n="15.3">mystérieux</w> <w n="15.4">problème</w> !</l>
					<l n="16" num="4.4"><w n="16.1">Vont</w> <w n="16.2">en</w> <w n="16.3">loques</w> <w n="16.4">et</w> <w n="16.5">sans</w> <w n="16.6">souliers</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w>, <w n="17.2">cependant</w>, <w n="17.3">la</w> <w n="17.4">forte</w>-<w n="17.5">en</w>-<w n="17.6">gueule</w></l>
					<l n="18" num="5.2"><w n="18.1">Qui</w> <w n="18.2">ne</w> <w n="18.3">revient</w> <w n="18.4">pas</w> <w n="18.5">du</w> <w n="18.6">Lignon</w>,</l>
					<l n="19" num="5.3"><w n="19.1">La</w> <w n="19.2">Politique</w>, <w n="19.3">peu</w> <w n="19.4">bégueule</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Hurle</w> <w n="20.2">et</w> <w n="20.3">se</w> <w n="20.4">crêpe</w> <w n="20.5">le</w> <w n="20.6">chignon</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">La</w> <w n="21.2">mégère</w> <w n="21.3">met</w> <w n="21.4">sur</w> <w n="21.5">ses</w> <w n="21.6">hanches</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Parterre</w> <w n="22.2">aux</w> <w n="22.3">maigres</w> <w n="22.4">floraisons</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Ses</w> <w n="23.2">deux</w> <w n="23.3">mains</w> <w n="23.4">qui</w> <w n="23.5">ne</w> <w n="23.6">sont</w> <w n="23.7">pas</w> <w n="23.8">blanches</w> ;</l>
					<l n="24" num="6.4"><w n="24.1">Et</w>, <w n="24.2">faute</w> <w n="24.3">de</w> <w n="24.4">bonnes</w> <w n="24.5">raisons</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Forte</w> <w n="25.2">à</w> <w n="25.3">la</w> <w n="25.4">savate</w>, <w n="25.5">inaugure</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Pour</w> <w n="26.2">tomber</w> <w n="26.3">son</w> <w n="26.4">godelureau</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Le</w> <w n="27.2">vif</w> <w n="27.3">coup</w> <w n="27.4">de</w> <w n="27.5">pied</w> <w n="27.6">de</w> <w n="27.7">figure</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">le</w> <w n="28.3">coup</w> <w n="28.4">de</w> <w n="28.5">front</w> <w n="28.6">du</w> <w n="28.7">taureau</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Rires</w>. <w n="29.2">Clameurs</w>. <w n="29.3">Effroi</w>. <w n="29.4">Tumulte</w>.</l>
					<l n="30" num="8.2"><w n="30.1">On</w> <w n="30.2">dirait</w> <w n="30.3">qu</w>’<w n="30.4">on</w> <w n="30.5">fouette</w> <w n="30.6">un</w> <w n="30.7">marmot</w>.</l>
					<l n="31" num="8.3"><w n="31.1">A</w> <w n="31.2">Chaillot</w> ! — <w n="31.3">C</w>’<w n="31.4">est</w> <w n="31.5">nous</w> <w n="31.6">qu</w>’<w n="31.7">on</w> <w n="31.8">insulte</w> !</l>
					<l n="32" num="8.4"><w n="32.1">Vous</w> <w n="32.2">allez</w> <w n="32.3">retirer</w> <w n="32.4">le</w> <w n="32.5">mot</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">le</w> <w n="33.3">prix</w> <w n="33.4">du</w> <w n="33.5">combat</w> <w n="33.6">sinistre</w></l>
					<l n="34" num="9.2"><w n="34.1">Flotte</w>, <w n="34.2">vaillamment</w> <w n="34.3">disputé</w>.</l>
					<l n="35" num="9.3"><w n="35.1">On</w> <w n="35.2">s</w>’<w n="35.3">explique</w>. — <w n="35.4">Va</w> <w n="35.5">donc</w>, <w n="35.6">ministre</w> !</l>
					<l n="36" num="9.4"><w n="36.1">Ohé</w> ! <w n="36.2">va</w> <w n="36.3">donc</w>, <w n="36.4">toi</w>, <w n="36.5">député</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">La</w> <w n="37.2">Politique</w>, <w n="37.3">fière</w>, <w n="37.4">en</w> <w n="37.5">somme</w>,</l>
					<l n="38" num="10.2"><w n="38.1">De</w> <w n="38.2">ne</w> <w n="38.3">jamais</w> <w n="38.4">amnistier</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Bavarde</w> <w n="39.2">et</w> <w n="39.3">se</w> <w n="39.4">trémousse</w> <w n="39.5">comme</w></l>
					<l n="40" num="10.4"><w n="40.1">Un</w> <w n="40.2">diable</w> <w n="40.3">dans</w> <w n="40.4">un</w> <w n="40.5">bénitier</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Elle</w> <w n="41.2">unit</w>, <w n="41.3">en</w> <w n="41.4">ses</w> <w n="41.5">turlutaines</w>,</l>
					<l n="42" num="11.2"><w n="42.1">L</w>’<w n="42.2">éloquence</w> <w n="42.3">de</w> <w n="42.4">feu</w> <w n="42.5">Dupin</w></l>
					<l n="43" num="11.3"><w n="43.1">Avec</w> <w n="43.2">celle</w> <w n="43.3">de</w> <w n="43.4">Démosthènes</w>.</l>
					<l n="44" num="11.4"><w n="44.1">C</w>’<w n="44.2">est</w> <w n="44.3">un</w> <w n="44.4">beau</w> <w n="44.5">spectacle</w>. — <w n="44.6">ET</w> <w n="44.7">DU</w> <w n="44.8">PAIN</w> ?</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">4 janvier 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>