<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN275">
				<head type="number">LXXXIII</head>
				<head type="main">Prière</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ah</w> ! <w n="1.2">n</w>’<w n="1.3">allons</w> <w n="1.4">pas</w> <w n="1.5">en</w> <w n="1.6">longue</w> <w n="1.7">queue</w>,</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">Humiliés</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Chez</w> <w n="3.2">ce</w> <w n="3.3">traiteur</w> <w n="3.4">de</w> <w n="3.5">la</w> <w n="3.6">banlieue</w></l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Dont</w> <w n="4.2">vous</w> <w n="4.3">parliez</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Fêtons</w> <w n="5.2">notre</w> <w n="5.3">ami</w>, <w n="5.4">sans</w> <w n="5.5">nul</w> <w n="5.6">doute</w>,</l>
					<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1">Quand</w> <w n="6.2">sans</w> <w n="6.3">ennuis</w></l>
					<l n="7" num="2.3"><w n="7.1">Il</w> <w n="7.2">a</w> <w n="7.3">bien</w> <w n="7.4">parcouru</w> <w n="7.5">sa</w> <w n="7.6">route</w>.</l>
					<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">Certes</w>, <w n="8.2">j</w>’<w n="8.3">en</w> <w n="8.4">suis</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Avec</w> <w n="9.2">le</w> <w n="9.3">vin</w> <w n="9.4">de</w> <w n="9.5">la</w> <w n="9.6">vendange</w>,</l>
					<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">Sachons</w> <w n="10.2">encor</w></l>
					<l n="11" num="3.3"><w n="11.1">Lui</w> <w n="11.2">verser</w> <w n="11.3">la</w> <w n="11.4">saine</w> <w n="11.5">louange</w>,</l>
					<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">Comme</w> <w n="12.2">un</w> <w n="12.3">flot</w> <w n="12.4">d</w>’<w n="12.5">or</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">qu</w>’<w n="13.3">alors</w> <w n="13.4">le</w> <w n="13.5">poëte</w> <w n="13.6">en</w> <w n="13.7">flamme</w></l>
					<l n="14" num="4.2"><space quantity="8" unit="char"></space><w n="14.1">Reste</w> <w n="14.2">orateur</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">Mais</w> <w n="15.2">n</w>’<w n="15.3">allons</w> <w n="15.4">pas</w> <w n="15.5">chez</w> <w n="15.6">cet</w> <w n="15.7">infâme</w></l>
					<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">Restaurateur</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Effroi</w> <w n="17.2">de</w> <w n="17.3">la</w> <w n="17.4">race</w> <w n="17.5">latine</w>,</l>
					<l n="18" num="5.2"><space quantity="8" unit="char"></space><w n="18.1">Crime</w> <w n="18.2">formel</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Sa</w> <w n="19.2">soupe</w> <w n="19.3">est</w> <w n="19.4">de</w> <w n="19.5">la</w> <w n="19.6">gélatine</w></l>
					<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1">Au</w> <w n="20.2">caramel</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">On</w> <w n="21.2">entend</w> <w n="21.3">parmi</w> <w n="21.4">ses</w> <w n="21.5">hors</w>-<w n="21.6">d</w>’<w n="21.7">œuvre</w></l>
					<l n="22" num="6.2"><space quantity="8" unit="char"></space><w n="22.1">Un</w> <w n="22.2">cri</w> <w n="22.3">plaintif</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">j</w>’<w n="23.3">aimerais</w> <w n="23.4">mieux</w> <w n="23.5">une</w> <w n="23.6">pieuvre</w></l>
					<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">Que</w> <w n="24.2">son</w> <w n="24.3">rosbeef</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Sa</w> <w n="25.2">volaille</w> <w n="25.3">a</w> <w n="25.4">l</w>’<w n="25.5">aspect</w> <w n="25.6">lubrique</w>,</l>
					<l n="26" num="7.2"><space quantity="8" unit="char"></space><w n="26.1">Et</w> <w n="26.2">ses</w> <w n="26.3">homards</w></l>
					<l n="27" num="7.3"><w n="27.1">Sont</w> <w n="27.2">bons</w> <w n="27.3">pour</w> <w n="27.4">des</w> <w n="27.5">nègres</w> <w n="27.6">d</w>’<w n="27.7">Afrique</w></l>
					<l n="28" num="7.4"><space quantity="8" unit="char"></space><w n="28.1">Aux</w> <w n="28.2">nez</w> <w n="28.3">camards</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Même</w> <w n="29.2">on</w> <w n="29.3">le</w> <w n="29.4">compare</w> <w n="29.5">à</w> <w n="29.6">Procuste</w></l>
					<l n="30" num="8.2"><space quantity="8" unit="char"></space><w n="30.1">Dans</w> <w n="30.2">les</w> <w n="30.3">journaux</w>.</l>
					<l n="31" num="8.3"><w n="31.1">Il</w> <w n="31.2">collabore</w> <w n="31.3">avec</w> <w n="31.4">Locuste</w></l>
					<l n="32" num="8.4"><space quantity="8" unit="char"></space><w n="32.1">Sur</w> <w n="32.2">des</w> <w n="32.3">fourneaux</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Fuyons</w> <w n="33.2">cet</w> <w n="33.3">homme</w> <w n="33.4">à</w> <w n="33.5">l</w>’<w n="33.6">esprit</w> <w n="33.7">large</w>,</l>
					<l n="34" num="9.2"><space quantity="8" unit="char"></space><w n="34.1">Mais</w> <w n="34.2">au</w> <w n="34.3">cœur</w> <w n="34.4">vain</w> ;</l>
					<l n="35" num="9.3"><w n="35.1">Car</w> <w n="35.2">c</w>’<w n="35.3">est</w> <w n="35.4">avec</w> <w n="35.5">de</w> <w n="35.6">la</w> <w n="35.7">litharge</w></l>
					<l n="36" num="9.4"><space quantity="8" unit="char"></space><w n="36.1">Qu</w>’<w n="36.2">il</w> <w n="36.3">fait</w> <w n="36.4">son</w> <w n="36.5">vin</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Craignons</w> <w n="37.2">ses</w> <w n="37.3">crèmes</w> <w n="37.4">éhontées</w></l>
					<l n="38" num="10.2"><space quantity="8" unit="char"></space><w n="38.1">Et</w> <w n="38.2">les</w> <w n="38.3">dégâts</w></l>
					<l n="39" num="10.3"><w n="39.1">Que</w> <w n="39.2">feraient</w> <w n="39.3">ses</w> <w n="39.4">pièces</w> <w n="39.5">montées</w></l>
					<l n="40" num="10.4"><space quantity="8" unit="char"></space><w n="40.1">Et</w> <w n="40.2">ses</w> <w n="40.3">nougats</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Fauchant</w> <w n="41.2">les</w> <w n="41.3">gens</w>, <w n="41.4">comme</w> <w n="41.5">des</w> <w n="41.6">herbes</w>,</l>
					<l n="42" num="11.2"><space quantity="8" unit="char"></space><w n="42.1">Au</w> <w n="42.2">son</w> <w n="42.3">des</w> <w n="42.4">cors</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Il</w> <w n="43.2">prétend</w> <w n="43.3">donner</w> <w n="43.4">de</w> <w n="43.5">superbes</w></l>
					<l n="44" num="11.4"><space quantity="8" unit="char"></space><w n="44.1">Repas</w> <w n="44.2">de</w> <w n="44.3">corps</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Au</w> <w n="45.2">temps</w> <w n="45.3">passé</w>, <w n="45.4">nous</w> <w n="45.5">y</w> <w n="45.6">dînâmes</w></l>
					<l n="46" num="12.2"><space quantity="8" unit="char"></space><w n="46.1">En</w> <w n="46.2">grand</w> <w n="46.3">gala</w> ;</l>
					<l n="47" num="12.3"><w n="47.1">Mais</w> <w n="47.2">il</w> <w n="47.3">ferait</w> <w n="47.4">bientôt</w> <w n="47.5">des</w> <w n="47.6">âmes</w></l>
					<l n="48" num="12.4"><space quantity="8" unit="char"></space><w n="48.1">De</w> <w n="48.2">ces</w> <w n="48.3">corps</w>-<w n="48.4">là</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Évitons</w> <w n="49.2">sa</w> <w n="49.3">cuisine</w> <w n="49.4">atroce</w> ;</l>
					<l n="50" num="13.2"><space quantity="8" unit="char"></space><w n="50.1">Car</w>, <w n="50.2">sans</w> <w n="50.3">honneur</w>,</l>
					<l n="51" num="13.3"><w n="51.1">On</w> <w n="51.2">périrait</w> <w n="51.3">chez</w> <w n="51.4">ce</w> <w n="51.5">féroce</w></l>
					<l n="52" num="13.4"><space quantity="8" unit="char"></space><w n="52.1">Empoisonneur</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">3 mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>