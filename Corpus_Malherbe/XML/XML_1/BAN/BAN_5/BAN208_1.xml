<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN208">
				<head type="number">XVI</head>
				<head type="main">A Sarcey</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Puissiez</w>-<w n="1.2">vous</w>, <w n="1.3">Bertrand</w> <w n="1.4">et</w> <w n="1.5">Raton</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Rendre</w> <w n="2.2">la</w> <w n="2.3">caisse</w> <w n="2.4">pléthorique</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Peut</w>-<w n="3.2">être</w> <w n="3.3">s</w>’<w n="3.4">amusera</w>-<w n="3.5">t</w>-<w n="3.6">on</w></l>
					<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">voir</w> <w n="4.3">cette</w> <w n="4.4">pièce</w> <w n="4.5">historique</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Il</w> <w n="5.2">serait</w> <w n="5.3">mal</w> <w n="5.4">qu</w>’<w n="5.5">on</w> <w n="5.6">m</w>’<w n="5.7">empêchât</w></l>
					<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2">en</w> <w n="6.3">savourer</w> <w n="6.4">la</w> <w n="6.5">moindre</w> <w n="6.6">bribe</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">quoi</w> ! <w n="7.3">j</w>’<w n="7.4">appelle</w> <w n="7.5">un</w> <w n="7.6">chat</w> : <w n="7.7">un</w> <w n="7.8">chat</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">monsieur</w> <w n="8.3">Scribe</w> : <w n="8.4">monsieur</w> <w n="8.5">Scribe</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Vous</w> <w n="9.2">avez</w> <w n="9.3">beau</w> <w n="9.4">frapper</w> <w n="9.5">du</w> <w n="9.6">pied</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Vraiment</w> <w n="10.2">vous</w> <w n="10.3">avez</w> <w n="10.4">tort</w>, <w n="10.5">Francisque</w>.</l>
					<l n="11" num="3.3"><w n="11.1">On</w> <w n="11.2">dit</w> <w n="11.3">bien</w> : <w n="11.4">monsieur</w>, <w n="11.5">comme</w> <w n="11.6">il</w> <w n="11.7">sied</w>.</l>
					<l n="12" num="3.4"><w n="12.1">Quoi</w> <w n="12.2">qu</w>’<w n="12.3">il</w> <w n="12.4">en</w> <w n="12.5">soit</w>, <w n="12.6">j</w>’<w n="12.7">en</w> <w n="12.8">cours</w> <w n="12.9">le</w> <w n="12.10">risque</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Tandis</w> <w n="13.2">que</w> <w n="13.3">l</w>’<w n="13.4">avenir</w> <w n="13.5">accourt</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Vous</w> <w n="14.2">voulez</w>, <w n="14.3">candeur</w> <w n="14.4">enfantine</w> !</l>
					<l n="15" num="4.3"><w n="15.1">L</w>’<w n="15.2">appeler</w> <w n="15.3">par</w> <w n="15.4">son</w> <w n="15.5">nom</w>, <w n="15.6">tout</w> <w n="15.7">court</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Ainsi</w> <w n="16.2">que</w> <w n="16.3">Goethe</w> <w n="16.4">ou</w> <w n="16.5">Lamartine</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Mais</w> <w n="17.2">non</w>, <w n="17.3">enlevez</w>, <w n="17.4">c</w>’<w n="17.5">est</w> <w n="17.6">pesé</w> !</l>
					<l n="18" num="5.2"><w n="18.1">Un</w> <w n="18.2">chou</w> <w n="18.3">n</w>’<w n="18.4">est</w> <w n="18.5">pas</w> <w n="18.6">une</w> <w n="18.7">pervenche</w>.</l>
					<l n="19" num="5.3"><w n="19.1">Par</w> <w n="19.2">lui</w> <w n="19.3">jadis</w> <w n="19.4">martyrisé</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Le</w> <w n="20.2">Vocable</w> <w n="20.3">prend</w> <w n="20.4">sa</w> <w n="20.5">revanche</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">dussiez</w>-<w n="21.3">vous</w> <w n="21.4">ravir</w> <w n="21.5">les</w> <w n="21.6">tiers</w></l>
					<l n="22" num="6.2"><w n="22.1">Par</w> <w n="22.2">votre</w> <w n="22.3">ardente</w> <w n="22.4">diatribe</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Ainsi</w> <w n="23.2">qu</w>’<w n="23.3">on</w> <w n="23.4">disait</w> : <w n="23.5">monsieur</w> <w n="23.6">Thiers</w>,</l>
					<l n="24" num="6.4"><w n="24.1">On</w> <w n="24.2">dira</w> <w n="24.3">toujours</w> : <w n="24.4">monsieur</w> <w n="24.5">Scribe</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Mais</w> <w n="25.2">pour</w> <w n="25.3">tant</w> <w n="25.4">de</w> <w n="25.5">témérité</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Si</w> <w n="26.2">contre</w> <w n="26.3">nous</w>, <w n="26.4">frivole</w> <w n="26.5">engeance</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Bondit</w> <w n="27.2">votre</w> <w n="27.3">cœur</w> <w n="27.4">irrité</w> ;</l>
					<l n="28" num="7.4"><w n="28.1">Si</w> <w n="28.2">vous</w> <w n="28.3">aviez</w> <w n="28.4">soif</w> <w n="28.5">de</w> <w n="28.6">vengeance</w></l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Comme</w> <w n="29.2">le</w> <w n="29.3">désert</w> <w n="29.4">Libyen</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Mon</w> <w n="30.2">ami</w>, <w n="30.3">par</w> <w n="30.4">toute</w> <w n="30.5">la</w> <w n="30.6">ville</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Je</w> <w n="31.2">vous</w> <w n="31.3">autorise</w> <w n="31.4">très</w> <w n="31.5">bien</w></l>
					<l n="32" num="8.4"><w n="32.1">A</w> <w n="32.2">dire</w> : <w n="32.3">monsieur</w> <w n="32.4">de</w> <w n="32.5">Banville</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">7 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>