<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN265">
				<head type="number">LXXIII</head>
				<head type="main">Ave</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Espoir</w> <w n="1.2">des</w> <w n="1.3">rêves</w> <w n="1.4">flottants</w></l>
					<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">l</w>’<w n="2.3">hiver</w> <w n="2.4">et</w> <w n="2.5">le</w> <w n="2.6">printemps</w>,</l>
					<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">en</w> <w n="3.4">vain</w> <w n="3.5">que</w> <w n="3.6">tu</w> <w n="3.7">diffères</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">rien</w> <w n="4.3">qu</w>’<w n="4.4">en</w> <w n="4.5">disant</w> <w n="4.6">un</w> : <w n="4.7">Oui</w>,</l>
					<l n="5" num="1.5"><w n="5.1">L</w>’<w n="5.2">Académie</w> <w n="5.3">aujourd</w>’<w n="5.4">hui</w></l>
					<l n="6" num="1.6"><w n="6.1">Fera</w> <w n="6.2">deux</w> <w n="6.3">bonnes</w> <w n="6.4">affaires</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Jamais</w> <w n="7.2">le</w> <w n="7.3">frisson</w> <w n="7.4">des</w> <w n="7.5">bois</w></l>
					<l n="8" num="2.2"><w n="8.1">Emplis</w> <w n="8.2">de</w> <w n="8.3">chants</w> <w n="8.4">et</w> <w n="8.5">de</w> <w n="8.6">voix</w>,</l>
					<l n="9" num="2.3"><w n="9.1">La</w> <w n="9.2">terre</w> <w n="9.3">de</w> <w n="9.4">pleurs</w> <w n="9.5">trempée</w></l>
					<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">les</w> <w n="10.3">beaux</w> <w n="10.4">couchants</w> <w n="10.5">ardents</w></l>
					<l n="11" num="2.5"><w n="11.1">N</w>’<w n="11.2">ont</w> <w n="11.3">mieux</w> <w n="11.4">rayonné</w> <w n="11.5">que</w> <w n="11.6">dans</w></l>
					<l n="12" num="2.6"><w n="12.1">Les</w> <w n="12.2">vers</w> <w n="12.3">de</w> <w n="12.4">François</w> <w n="12.5">Coppée</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Ce</w> <w n="13.2">pâle</w> <w n="13.3">enfant</w> <w n="13.4">de</w> <w n="13.5">Paris</w></l>
					<l n="14" num="3.2"><w n="14.1">Dans</w> <w n="14.2">les</w> <w n="14.3">gais</w> <w n="14.4">sentiers</w> <w n="14.5">fleuris</w></l>
					<l n="15" num="3.3"><w n="15.1">De</w> <w n="15.2">l</w>’<w n="15.3">églogue</w> <w n="15.4">et</w> <w n="15.5">dans</w> <w n="15.6">le</w> <w n="15.7">drame</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Avec</w> <w n="16.2">l</w>’<w n="16.3">esprit</w> <w n="16.4">et</w> <w n="16.5">l</w>’<w n="16.6">humour</w>,</l>
					<l n="17" num="3.5"><w n="17.1">A</w> <w n="17.2">gardé</w> <w n="17.3">le</w> <w n="17.4">chaste</w> <w n="17.5">amour</w></l>
					<l n="18" num="3.6"><w n="18.1">Et</w> <w n="18.2">le</w> <w n="18.3">respect</w> <w n="18.4">de</w> <w n="18.5">la</w> <w n="18.6">femme</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">L</w>’<w n="19.2">Académie</w> <w n="19.3">a</w> <w n="19.4">raison</w></l>
					<l n="20" num="4.2"><w n="20.1">En</w> <w n="20.2">cueillant</w> <w n="20.3">la</w> <w n="20.4">floraison</w></l>
					<l n="21" num="4.3"><w n="21.1">De</w> <w n="21.2">son</w> <w n="21.3">renom</w> <w n="21.4">populaire</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">gagne</w> <w n="22.3">à</w> <w n="22.4">s</w>’<w n="22.5">associer</w></l>
					<l n="23" num="4.5"><w n="23.1">Ce</w> <w n="23.2">poëte</w> <w n="23.3">aux</w> <w n="23.4">yeux</w> <w n="23.5">d</w>’<w n="23.6">acier</w></l>
					<l n="24" num="4.6"><w n="24.1">Dont</w> <w n="24.2">la</w> <w n="24.3">prunelle</w> <w n="24.4">est</w> <w n="24.5">si</w> <w n="24.6">claire</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Tout</w> <w n="25.2">jeune</w> <w n="25.3">à</w> <w n="25.4">la</w> <w n="25.5">Muse</w> <w n="25.6">offert</w>,</l>
					<l n="26" num="5.2"><w n="26.1">Il</w> <w n="26.2">a</w> <w n="26.3">vécu</w>, <w n="26.4">vu</w>, <w n="26.5">souffert</w> ;</l>
					<l n="27" num="5.3"><w n="27.1">Il</w> <w n="27.2">caresse</w> <w n="27.3">un</w> <w n="27.4">chant</w> <w n="27.5">magique</w></l>
					<l n="28" num="5.4"><w n="28.1">Et</w> <w n="28.2">sait</w>, <w n="28.3">par</w> <w n="28.4">des</w> <w n="28.5">mots</w> <w n="28.6">vainqueurs</w>,</l>
					<l n="29" num="5.5"><w n="29.1">Faire</w> <w n="29.2">vibrer</w> <w n="29.3">dans</w> <w n="29.4">nos</w> <w n="29.5">cœurs</w></l>
					<l n="30" num="5.6"><w n="30.1">L</w>’<w n="30.2">épouvantement</w> <w n="30.3">tragique</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Pour</w> <w n="31.2">Ferdinand</w> <w n="31.3">de</w> <w n="31.4">Lesseps</w>,</l>
					<l n="32" num="6.2"><w n="32.1">C</w>’<w n="32.2">est</w> <w n="32.3">la</w> <w n="32.4">pourpre</w>, <w n="32.5">et</w> <w n="32.6">non</w> <w n="32.7">le</w> <w n="32.8">reps</w>,</l>
					<l n="33" num="6.3"><w n="33.1">Qu</w>’<w n="33.2">il</w> <w n="33.3">faut</w> <w n="33.4">sous</w> <w n="33.5">ses</w> <w n="33.6">pas</w> <w n="33.7">étendre</w>.</l>
					<l n="34" num="6.4"><w n="34.1">L</w>’<w n="34.2">Orient</w> <w n="34.3">au</w> <w n="34.4">ciel</w> <w n="34.5">de</w> <w n="34.6">feu</w>,</l>
					<l n="35" num="6.5"><w n="35.1">Jadis</w>, <w n="35.2">en</w> <w n="35.3">eût</w> <w n="35.4">fait</w> <w n="35.5">un</w> <w n="35.6">dieu</w>,</l>
					<l n="36" num="6.6"><w n="36.1">Comme</w> <w n="36.2">il</w> <w n="36.3">a</w> <w n="36.4">fait</w> <w n="36.5">d</w>’<w n="36.6">Alexandre</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Car</w> <w n="37.2">par</w> <w n="37.3">les</w> <w n="37.4">isthmes</w> <w n="37.5">ouverts</w></l>
					<l n="38" num="7.2"><w n="38.1">Il</w> <w n="38.2">fait</w> <w n="38.3">passer</w> <w n="38.4">les</w> <w n="38.5">flots</w> <w n="38.6">verts</w> ;</l>
					<l n="39" num="7.3"><w n="39.1">Et</w> <w n="39.2">ce</w> <w n="39.3">Titan</w> <w n="39.4">philosophe</w>,</l>
					<l n="40" num="7.4"><w n="40.1">Qui</w> <w n="40.2">brave</w> <w n="40.3">les</w> <w n="40.4">cieux</w> <w n="40.5">tonnants</w>,</l>
					<l n="41" num="7.5"><w n="41.1">Déchire</w> <w n="41.2">les</w> <w n="41.3">continents</w></l>
					<l n="42" num="7.6"><w n="42.1">Comme</w> <w n="42.2">on</w> <w n="42.3">déchire</w> <w n="42.4">une</w> <w n="42.5">étoffe</w>.</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Il</w> <w n="43.2">fait</w> <w n="43.3">des</w> <w n="43.4">flots</w> <w n="43.5">ses</w> <w n="43.6">vassaux</w> ;</l>
					<l n="44" num="8.2"><w n="44.1">Et</w> <w n="44.2">pour</w> <w n="44.3">le</w> <w n="44.4">vol</w> <w n="44.5">des</w> <w n="44.6">vaisseaux</w></l>
					<l n="45" num="8.3"><w n="45.1">Délivrant</w> <w n="45.2">la</w> <w n="45.3">mer</w> <w n="45.4">profonde</w>,</l>
					<l n="46" num="8.4"><w n="46.1">Sa</w> <w n="46.2">grande</w> <w n="46.3">Rébellion</w></l>
					<l n="47" num="8.5"><w n="47.1">Met</w> <w n="47.2">ses</w> <w n="47.3">griffes</w> <w n="47.4">de</w> <w n="47.5">lion</w></l>
					<l n="48" num="8.6"><w n="48.1">Sur</w> <w n="48.2">la</w> <w n="48.3">figure</w> <w n="48.4">du</w> <w n="48.5">monde</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">22 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>