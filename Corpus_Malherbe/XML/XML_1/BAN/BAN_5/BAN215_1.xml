<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN215">
				<head type="number">XXIII</head>
				<head type="main">Philosophie</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Tout</w> <w n="1.2">là</w>-<w n="1.3">bas</w>, <w n="1.4">sur</w> <w n="1.5">un</w> <w n="1.6">boulevard</w></l>
					<l n="2" num="1.2"><w n="2.1">Peuplé</w> <w n="2.2">de</w> <w n="2.3">spectacles</w> <w n="2.4">risibles</w></l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">admire</w> <w n="3.3">le</w> <w n="3.4">passant</w> <w n="3.5">bavard</w>,</l>
					<l n="4" num="1.4"><w n="4.1">On</w> <w n="4.2">vous</w> <w n="4.3">fait</w> <w n="4.4">voir</w> <w n="4.5">les</w> <w n="4.6">Invisibles</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Eh</w> <w n="5.2">quoi</w> ! <w n="5.3">dans</w> <w n="5.4">une</w> <w n="5.5">goutte</w> <w n="5.6">d</w>’<w n="5.7">eau</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Tant</w> <w n="6.2">de</w> <w n="6.3">serpents</w> <w n="6.4">et</w> <w n="6.5">de</w> <w n="6.6">molosses</w></l>
					<l n="7" num="2.3"><w n="7.1">Hideux</w> <w n="7.2">et</w> <w n="7.3">traînant</w> <w n="7.4">leur</w> <w n="7.5">fardeau</w> !</l>
					<l n="8" num="2.4"><w n="8.1">Tant</w> <w n="8.2">d</w>’<w n="8.3">abominables</w> <w n="8.4">colosses</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Monstrueux</w>, <w n="9.2">diffus</w>, <w n="9.3">contournés</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Sombre</w> <w n="10.2">et</w> <w n="10.3">tragique</w> <w n="10.4">phénomène</w>,</l>
					<l n="11" num="3.3"><w n="11.1">On</w> <w n="11.2">pourrait</w> <w n="11.3">croire</w> <w n="11.4">qu</w>’<w n="11.5">ils</w> <w n="11.6">sont</w> <w n="11.7">nés</w></l>
					<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">le</w> <w n="12.3">récit</w> <w n="12.4">de</w> <w n="12.5">Théramène</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Car</w> <w n="13.2">c</w>’<w n="13.3">est</w> <w n="13.4">en</w> <w n="13.5">replis</w> <w n="13.6">tortueux</w></l>
					<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">leur</w> <w n="14.3">croupe</w> <w n="14.4">aussi</w> <w n="14.5">se</w> <w n="14.6">recourbe</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Tout</w> <w n="15.2">en</w> <w n="15.3">eux</w> <w n="15.4">est</w> <w n="15.5">tumultueux</w> :</l>
					<l n="16" num="4.4"><w n="16.1">Ailes</w>, <w n="16.2">écailles</w>, <w n="16.3">regard</w> <w n="16.4">fourbe</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">géants</w> <w n="17.3">altérés</w> <w n="17.4">de</w> <w n="17.5">mort</w></l>
					<l n="18" num="5.2"><w n="18.1">Avec</w> <w n="18.2">leur</w> <w n="18.3">gueule</w> <w n="18.4">ruisselante</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Tout</w> <w n="19.2">cela</w> <w n="19.3">se</w> <w n="19.4">mange</w> <w n="19.5">et</w> <w n="19.6">se</w> <w n="19.7">mord</w></l>
					<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">s</w>’<w n="20.3">éventre</w> <w n="20.4">dans</w> <w n="20.5">l</w>’<w n="20.6">eau</w> <w n="20.7">sanglante</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">De</w> <w n="21.2">combien</w> <w n="21.3">de</w> <w n="21.4">ces</w> <w n="21.5">gouttes</w> <w n="21.6">d</w>’<w n="21.7">eau</w></l>
					<l n="22" num="6.2"><w n="22.1">Se</w> <w n="22.2">compose</w> <w n="22.3">une</w> <w n="22.4">mer</w> <w n="22.5">profonde</w></l>
					<l n="23" num="6.3"><w n="23.1">Soulevant</w> <w n="23.2">son</w> <w n="23.3">épais</w> <w n="23.4">rideau</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">que</w> <w n="24.3">d</w>’<w n="24.4">océans</w> <w n="24.5">dans</w> <w n="24.6">un</w> <w n="24.7">monde</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">qui</w> <w n="25.3">se</w> <w n="25.4">meut</w> <w n="25.5">dans</w> <w n="25.6">l</w>’<w n="25.7">infini</w></l>
					<l n="26" num="7.2"><w n="26.1">Sans</w> <w n="26.2">cieux</w>, <w n="26.3">sans</w> <w n="26.4">limite</w> <w n="26.5">et</w> <w n="26.6">sans</w> <w n="26.7">voiles</w> ?</l>
					<l n="27" num="7.3"><w n="27.1">Un</w> <w n="27.2">troupeau</w> <w n="27.3">toujours</w> <w n="27.4">rajeuni</w></l>
					<l n="28" num="7.4"><w n="28.1">D</w>’<w n="28.2">astres</w>, <w n="28.3">un</w> <w n="28.4">tourbillon</w> <w n="28.5">d</w>’<w n="28.6">étoiles</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Des</w> <w n="29.2">mondes</w>, <w n="29.3">pour</w> <w n="29.4">un</w> <w n="29.5">seul</w> <w n="29.6">témoin</w></l>
					<l n="30" num="8.2"><w n="30.1">Pressant</w> <w n="30.2">leurs</w> <w n="30.3">courses</w> <w n="30.4">vagabondes</w>.</l>
					<l n="31" num="8.3"><w n="31.1">Plus</w> <w n="31.2">loin</w> ? <w n="31.3">Des</w> <w n="31.4">mondes</w>. <w n="31.5">Et</w> <w n="31.6">plus</w> <w n="31.7">loin</w> ?</l>
					<l n="32" num="8.4"><w n="32.1">Toujours</w>, <w n="32.2">toujours</w>, <w n="32.3">toujours</w> <w n="32.4">des</w> <w n="32.5">mondes</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Tous</w> <w n="33.2">ces</w> <w n="33.3">univers</w> <w n="33.4">radieux</w></l>
					<l n="34" num="9.2"><w n="34.1">Vont</w> <w n="34.2">dans</w> <w n="34.3">l</w>’<w n="34.4">éther</w> <w n="34.5">clair</w> <w n="34.6">et</w> <w n="34.7">terrible</w></l>
					<l n="35" num="9.3"><w n="35.1">Menés</w> <w n="35.2">par</w> <w n="35.3">des</w> <w n="35.4">troupeaux</w> <w n="35.5">de</w> <w n="35.6">Dieux</w></l>
					<l n="36" num="9.4"><w n="36.1">Qu</w>’<w n="36.2">à</w> <w n="36.3">son</w> <w n="36.4">tour</w> <w n="36.5">mène</w> <w n="36.6">un</w> <w n="36.7">fouet</w> <w n="36.8">horrible</w> ;</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Emportés</w> <w n="37.2">dans</w> <w n="37.3">l</w>’<w n="37.4">éternité</w></l>
					<l n="38" num="10.2"><w n="38.1">Qui</w> <w n="38.2">ne</w> <w n="38.3">peut</w> <w n="38.4">être</w> <w n="38.5">dépensée</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Par</w> <w n="39.2">le</w> <w n="39.3">calme</w> <w n="39.4">rhythme</w> <w n="39.5">enchanté</w></l>
					<l n="40" num="10.4"><w n="40.1">Né</w> <w n="40.2">dans</w> <w n="40.3">l</w>’<w n="40.4">immuable</w> <w n="40.5">pensée</w> ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Effarés</w>, <w n="41.2">dociles</w>, <w n="41.3">ayant</w></l>
					<l n="42" num="11.2"><w n="42.1">Pour</w> <w n="42.2">but</w> <w n="42.3">d</w>’<w n="42.4">obéir</w> <w n="42.5">à</w> <w n="42.6">la</w> <w n="42.7">Cause</w>.</l>
					<l n="43" num="11.3"><w n="43.1">Oh</w> ! <w n="43.2">dans</w> <w n="43.3">cet</w> <w n="43.4">ensemble</w> <w n="43.5">effrayant</w></l>
					<l n="44" num="11.4"><w n="44.1">Que</w> <w n="44.2">Turlurette</w> <w n="44.3">est</w> <w n="44.4">peu</w> <w n="44.5">de</w> <w n="44.6">chose</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1883">21 décembre 1883.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>