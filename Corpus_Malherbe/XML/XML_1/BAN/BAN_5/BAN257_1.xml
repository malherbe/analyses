<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN257">
				<head type="number">LXV</head>
				<head type="main">Reine-Blanche</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">Reine</w>-<w n="1.3">Blanche</w> <w n="1.4">est</w> <w n="1.5">morte</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">vent</w> <w n="2.3">de</w> <w n="2.4">glace</w> <w n="2.5">emporte</w></l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">disperse</w> <w n="3.3">à</w> <w n="3.4">l</w>’<w n="3.5">entour</w></l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">Son</w> <w n="4.2">vieil</w> <w n="4.3">amour</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">O</w> <w n="5.2">paradis</w> <w n="5.3">terrestre</w> !</l>
					<l n="6" num="2.2"><w n="6.1">Épouvantable</w> <w n="6.2">orchestre</w></l>
					<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">même</w> <w n="7.3">effarouchas</w></l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space><w n="8.1">Les</w> <w n="8.2">pauvres</w> <w n="8.3">chats</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Phrase</w> <w n="9.2">cruelle</w> <w n="9.3">et</w> <w n="9.4">nette</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Que</w> <w n="10.2">dit</w> <w n="10.3">la</w> <w n="10.4">clarinette</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Ou</w> <w n="11.2">que</w> <w n="11.3">nous</w> <w n="11.4">dépistons</w></l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space><w n="12.1">Dans</w> <w n="12.2">les</w> <w n="12.3">pistons</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Saladiers</w> <w n="13.2">sans</w> <w n="13.3">emphase</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Où</w> <w n="14.2">l</w>’<w n="14.3">on</w> <w n="14.4">buvait</w> <w n="14.5">l</w>’<w n="14.6">extase</w></l>
					<l n="15" num="4.3"><w n="15.1">Avec</w> <w n="15.2">le</w> <w n="15.3">flot</w> <w n="15.4">sacré</w></l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space><w n="16.1">Du</w> <w n="16.2">vin</w> <w n="16.3">sucré</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Alphonses</w>, <w n="17.2">divins</w> <w n="17.3">mâles</w> !</l>
					<l n="18" num="5.2"><w n="18.1">Robes</w> <w n="18.2">de</w> <w n="18.3">femmes</w> <w n="18.4">pâles</w></l>
					<l n="19" num="5.3"><w n="19.1">Collant</w> <w n="19.2">comme</w> <w n="19.3">un</w> <w n="19.4">linceul</w> !</l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space><w n="20.1">Cavalier</w> <w n="20.2">seul</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Sous</w> <w n="21.2">le</w> <w n="21.3">gaz</w> <w n="21.4">noir</w> <w n="21.5">qui</w> <w n="21.6">flambe</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Irma</w> <w n="22.2">levant</w> <w n="22.3">la</w> <w n="22.4">jambe</w></l>
					<l n="23" num="6.3"><w n="23.1">En</w> <w n="23.2">l</w>’<w n="23.3">air</w>, <w n="23.4">et</w> <w n="23.5">montrant</w> <w n="23.6">son</w></l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space><w n="24.1">Nez</w> <w n="24.2">polisson</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Femmes</w> <w n="25.2">parfois</w> <w n="25.3">gelées</w></l>
					<l n="26" num="7.2"><w n="26.1">Qui</w> <w n="26.2">dansiez</w>, <w n="26.3">flagellées</w></l>
					<l n="27" num="7.3"><w n="27.1">Par</w> <w n="27.2">le</w> <w n="27.3">fouet</w> <w n="27.4">triste</w> <w n="27.5">et</w> <w n="27.6">fou</w></l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space><w n="28.1">D</w>’<w n="28.2">un</w> <w n="28.3">dieu</w> <w n="28.4">voyou</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Chœur</w> <w n="29.2">plein</w> <w n="29.3">de</w> <w n="29.4">mille</w> <w n="29.5">rages</w></l>
					<l n="30" num="8.2"><w n="30.1">Qui</w>, <w n="30.2">parmi</w> <w n="30.3">des</w> <w n="30.4">orages</w></l>
					<l n="31" num="8.3"><w n="31.1">Assez</w> <w n="31.2">souvent</w> <w n="31.3">décrits</w>,</l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space><w n="32.1">Poussais</w> <w n="32.2">des</w> <w n="32.3">cris</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Ton</w> <w n="33.2">orgie</w> <w n="33.3">indocile</w></l>
					<l n="34" num="9.2"><w n="34.1">Étant</w> <w n="34.2">sans</w> <w n="34.3">domicile</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Suis</w> <w n="35.2">la</w> <w n="35.3">brise</w> <w n="35.4">et</w> <w n="35.5">l</w>’<w n="35.6">autan</w>.</l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space><w n="36.1">Adieu</w>, <w n="36.2">va</w>-<w n="36.3">t</w>’<w n="36.4">en</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Laisse</w> <w n="37.2">ton</w> <w n="37.3">pauvre</w> <w n="37.4">vice</w></l>
					<l n="38" num="10.2"><w n="38.1">Déjà</w> <w n="38.2">hors</w> <w n="38.3">de</w> <w n="38.4">service</w></l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">pratique</w>, <w n="39.3">si</w> <w n="39.4">tu</w></l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space><w n="40.1">Peux</w>, <w n="40.2">la</w> <w n="40.3">vertu</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">14 février 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>