<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">NOUS TOUS</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4011 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_5</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Nous tous</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvillenoustous.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Nous Tous.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et Cie, éditeurs</publisher>
							<date when="1884">1884</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1884">1883-1884</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN278">
				<head type="number">LXXXVI</head>
				<head type="main">Le Bassin</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">Luxembourg</w>, <w n="1.3">que</w> <w n="1.4">je</w> <w n="1.5">dis</w></l>
					<l n="2" num="1.2"><w n="2.1">Beau</w> <w n="2.2">comme</w> <w n="2.3">le</w> <w n="2.4">paradis</w>,</l>
					<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">a</w> <w n="3.3">torturé</w> <w n="3.4">les</w> <w n="3.5">lignes</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">le</w> <w n="4.3">fantasque</w> <w n="4.4">dessin</w></l>
					<l n="5" num="1.5"><w n="5.1">D</w>’<w n="5.2">un</w> <w n="5.3">capricieux</w> <w n="5.4">bassin</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Pour</w> <w n="6.2">les</w> <w n="6.3">canards</w> <w n="6.4">et</w> <w n="6.5">les</w> <w n="6.6">cygnes</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Les</w> <w n="7.2">bleus</w> <w n="7.3">canards</w> <w n="7.4">du</w> <w n="7.5">Japon</w></l>
					<l n="8" num="2.2"><w n="8.1">Semblent</w> <w n="8.2">sortis</w> <w n="8.3">d</w>’<w n="8.4">un</w> <w n="8.5">crépon</w>,</l>
					<l n="9" num="2.3"><w n="9.1">Et</w> <w n="9.2">forment</w> <w n="9.3">un</w> <w n="9.4">long</w> <w n="9.5">cortège</w></l>
					<l n="10" num="2.4"><w n="10.1">A</w> <w n="10.2">l</w>’<w n="10.3">entour</w> <w n="10.4">des</w> <w n="10.5">cygnes</w> <w n="10.6">blancs</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Dont</w> <w n="11.2">les</w> <w n="11.3">ailes</w> <w n="11.4">et</w> <w n="11.5">les</w> <w n="11.6">flancs</w></l>
					<l n="12" num="2.6"><w n="12.1">Sont</w> <w n="12.2">pareils</w> <w n="12.3">à</w> <w n="12.4">de</w> <w n="12.5">la</w> <w n="12.6">neige</w>.</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Tout</w> <w n="13.2">au</w> <w n="13.3">beau</w> <w n="13.4">milieu</w> <w n="13.5">des</w> <w n="13.6">eaux</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Une</w> <w n="14.2">île</w> <w n="14.3">offre</w> <w n="14.4">à</w> <w n="14.5">ces</w> <w n="14.6">oiseaux</w></l>
					<l n="15" num="3.3"><w n="15.1">Le</w> <w n="15.2">gazon</w> <w n="15.3">vert</w>. <w n="15.4">Leur</w> <w n="15.5">royaume</w></l>
					<l n="16" num="3.4"><w n="16.1">Est</w> <w n="16.2">fort</w> <w n="16.3">exigu</w>. <w n="16.4">Mais</w> <w n="16.5">on</w></l>
					<l n="17" num="3.5"><w n="17.1">Leur</w> <w n="17.2">a</w> <w n="17.3">fait</w> <w n="17.4">une</w> <w n="17.5">maison</w></l>
					<l n="18" num="3.6"><w n="18.1">Basse</w>, <w n="18.2">avec</w> <w n="18.3">un</w> <w n="18.4">toit</w> <w n="18.5">de</w> <w n="18.6">chaume</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">En</w> <w n="19.2">leurs</w> <w n="19.3">infinis</w> <w n="19.4">loisirs</w>,</l>
					<l n="20" num="4.2"><w n="20.1">Ils</w> <w n="20.2">savourent</w> <w n="20.3">les</w> <w n="20.4">plaisirs</w></l>
					<l n="21" num="4.3"><w n="21.1">Que</w> <w n="21.2">l</w>’<w n="21.3">oisiveté</w> <w n="21.4">ménage</w> ;</l>
					<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">philosophes</w> <w n="22.3">par</w> <w n="22.4">goût</w>,</l>
					<l n="23" num="4.5"><w n="23.1">Les</w> <w n="23.2">uns</w> <w n="23.3">ne</w> <w n="23.4">font</w> <w n="23.5">rien</w> <w n="23.6">du</w> <w n="23.7">tout</w>,</l>
					<l n="24" num="4.6"><w n="24.1">Pendant</w> <w n="24.2">que</w> <w n="24.3">le</w> <w n="24.4">reste</w> <w n="24.5">nage</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Mais</w> <w n="25.2">dans</w> <w n="25.3">l</w>’<w n="25.4">île</w>, <w n="25.5">sur</w> <w n="25.6">le</w> <w n="25.7">bord</w></l>
					<l n="26" num="5.2"><w n="26.1">Que</w> <w n="26.2">l</w>’<w n="26.3">eau</w> <w n="26.4">caressante</w> <w n="26.5">mord</w></l>
					<l n="27" num="5.3"><w n="27.1">Et</w> <w n="27.2">parmi</w> <w n="27.3">les</w> <w n="27.4">folles</w> <w n="27.5">branches</w>,</l>
					<l n="28" num="5.4"><w n="28.1">Parfois</w>, <w n="28.2">d</w>’<w n="28.3">un</w> <w n="28.4">mouvement</w> <w n="28.5">fou</w>,</l>
					<l n="29" num="5.5"><w n="29.1">Les</w> <w n="29.2">cygnes</w> <w n="29.3">lèvent</w> <w n="29.4">leur</w> <w n="29.5">cou</w></l>
					<l n="30" num="5.6"><w n="30.1">Puis</w> <w n="30.2">ouvrent</w> <w n="30.3">leurs</w> <w n="30.4">ailes</w> <w n="30.5">blanches</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Les</w> <w n="31.2">grands</w> <w n="31.3">cygnes</w> <w n="31.4">fabuleux</w></l>
					<l n="32" num="6.2"><w n="32.1">Et</w> <w n="32.2">les</w> <w n="32.3">petits</w> <w n="32.4">canards</w> <w n="32.5">bleus</w></l>
					<l n="33" num="6.3"><w n="33.1">Respirent</w> <w n="33.2">dans</w> <w n="33.3">la</w> <w n="33.4">nature</w></l>
					<l n="34" num="6.4"><w n="34.1">Et</w>, <w n="34.2">leur</w> <w n="34.3">sens</w> <w n="34.4">étant</w> <w n="34.5">profond</w>,</l>
					<l n="35" num="6.5"><w n="35.1">Ces</w> <w n="35.2">êtres</w> <w n="35.3">ailés</w> <w n="35.4">ne</w> <w n="35.5">font</w></l>
					<l n="36" num="6.6"><w n="36.1">Jamais</w> <w n="36.2">de</w> <w n="36.3">littérature</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">la</w> <w n="37.4">joie</w>, <w n="37.5">argent</w> <w n="37.6">comptant</w>.</l>
					<l n="38" num="7.2"><w n="38.1">Certes</w>, <w n="38.2">je</w> <w n="38.3">serais</w> <w n="38.4">content</w></l>
					<l n="39" num="7.3"><w n="39.1">Si</w> <w n="39.2">de</w> <w n="39.3">tels</w> <w n="39.4">bonheurs</w> <w n="39.5">insignes</w></l>
					<l n="40" num="7.4"><w n="40.1">M</w>’<w n="40.2">étaient</w> <w n="40.3">seulement</w> <w n="40.4">promis</w>,</l>
					<l n="41" num="7.5"><w n="41.1">O</w> <w n="41.2">vous</w>, <w n="41.3">canards</w>, <w n="41.4">mes</w> <w n="41.5">amis</w>,</l>
					<l n="42" num="7.6"><w n="42.1">Et</w> <w n="42.2">vous</w>, <w n="42.3">mes</w> <w n="42.4">confrères</w>, <w n="42.5">cygnes</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1884">6 mars 1884.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>