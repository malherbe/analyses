<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN665">
				<head type="number">XXXII</head>
				<head type="main">Moderne</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">Saint</w> <w n="1.3">dit</w> <w n="1.4">à</w> <w n="1.5">son</w> <w n="1.6">compagnon</w> :</l>
					<l n="2" num="1.2"><w n="2.1">Vil</w> <w n="2.2">animal</w> <w n="2.3">souillé</w> <w n="2.4">de</w> <w n="2.5">crotte</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Puisque</w> <w n="3.2">tu</w> <w n="3.3">prends</w> <w n="3.4">cet</w> <w n="3.5">air</w> <w n="3.6">grognon</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Va</w> <w n="4.2">te</w> <w n="4.3">cacher</w> <w n="4.4">là</w>, <w n="4.5">dans</w> <w n="4.6">la</w> <w n="4.7">grotte</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Laisse</w>-<w n="5.2">moi</w> <w n="5.3">dans</w> <w n="5.4">l</w>’<w n="5.5">air</w> <w n="5.6">nébuleux</w></l>
					<l n="6" num="2.2"><w n="6.1">Regarder</w> <w n="6.2">l</w>’<w n="6.3">Ange</w> <w n="6.4">qui</w> <w n="6.5">s</w>’<w n="6.6">élance</w></l>
					<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">franchir</w> <w n="7.3">les</w> <w n="7.4">escaliers</w> <w n="7.5">bleus</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">d</w>’<w n="8.3">abord</w>, <w n="8.4">garde</w> <w n="8.5">le</w> <w n="8.6">silence</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">le</w> <w n="9.3">rôdeur</w> <w n="9.4">aux</w> <w n="9.5">flancs</w> <w n="9.6">épais</w></l>
					<l n="10" num="3.2"><w n="10.1">Dit</w> : <w n="10.2">Je</w> <w n="10.3">suis</w> <w n="10.4">fatigué</w> <w n="10.5">du</w> <w n="10.6">mythe</w>.</l>
					<l n="11" num="3.3"><w n="11.1">En</w> <w n="11.2">somme</w>, <w n="11.3">accorde</w>-<w n="11.4">moi</w> <w n="11.5">la</w> <w n="11.6">paix</w> !</l>
					<l n="12" num="3.4"><w n="12.1">Ne</w> <w n="12.2">m</w>’<w n="12.3">agace</w> <w n="12.4">pas</w>, <w n="12.5">bon</w> <w n="12.6">ermite</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">bon</w>. <w n="13.4">Laisse</w> <w n="13.5">tes</w> <w n="13.6">vieux</w> <w n="13.7">cheveux</w></l>
					<l n="14" num="4.2"><w n="14.1">Se</w> <w n="14.2">coller</w> <w n="14.3">sur</w> <w n="14.4">ta</w> <w n="14.5">face</w> <w n="14.6">blême</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">je</w> <w n="15.3">parlerai</w>, <w n="15.4">si</w> <w n="15.5">je</w> <w n="15.6">veux</w>.</l>
					<l n="16" num="4.4"><w n="16.1">Garde</w> <w n="16.2">le</w> <w n="16.3">silence</w>, <w n="16.4">toi</w>-<w n="16.5">même</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Les</w> <w n="17.2">instants</w> <w n="17.3">envolés</w> <w n="17.4">sont</w> <w n="17.5">courts</w>.</l>
					<l n="18" num="5.2"><w n="18.1">Bonhomme</w>, <w n="18.2">il</w> <w n="18.3">ne</w> <w n="18.4">faudrait</w> <w n="18.5">pas</w> <w n="18.6">m</w>’<w n="18.7">être</w></l>
					<l n="19" num="5.3"><w n="19.1">Désagréable</w> <w n="19.2">en</w> <w n="19.3">tes</w> <w n="19.4">discours</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">maintenant</w>, <w n="20.3">c</w>’<w n="20.4">est</w> <w n="20.5">moi</w> <w n="20.6">le</w> <w n="20.7">maître</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Tu</w> <w n="21.2">nasilles</w>, <w n="21.3">comme</w> <w n="21.4">au</w> <w n="21.5">lutrin</w>.</l>
					<l n="22" num="6.2"><w n="22.1">Mais</w> <w n="22.2">sache</w>-<w n="22.3">le</w>, <w n="22.4">vieux</w> <w n="22.5">botaniste</w>,</l>
					<l n="23" num="6.3"><w n="23.1">C</w>’<w n="23.2">est</w> <w n="23.3">moi</w> <w n="23.4">seul</w> <w n="23.5">qui</w> <w n="23.6">suis</w> <w n="23.7">dans</w> <w n="23.8">le</w> <w n="23.9">train</w>.</l>
					<l n="24" num="6.4"><w n="24.1">Je</w> <w n="24.2">suis</w> <w n="24.3">le</w> <w n="24.4">Cochon</w> <w n="24.5">moderniste</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Je</w> <w n="25.2">suis</w> <w n="25.3">comme</w> <w n="25.4">un</w> <w n="25.5">roi</w> <w n="25.6">d</w>’<w n="25.7">Orient</w>.</l>
					<l n="26" num="7.2"><w n="26.1">Le</w> <w n="26.2">soleil</w> <w n="26.3">me</w> <w n="26.4">baise</w> <w n="26.5">et</w> <w n="26.6">me</w> <w n="26.7">dore</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">tout</w> <w n="27.3">le</w> <w n="27.4">monde</w>, <w n="27.5">en</w> <w n="27.6">me</w> <w n="27.7">voyant</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Me</w> <w n="28.2">dit</w> : <w n="28.3">Cochon</w>, <w n="28.4">viens</w> <w n="28.5">qu</w>’<w n="28.6">on</w> <w n="28.7">t</w>’<w n="28.8">adore</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Dût</w> <w n="29.2">cette</w> <w n="29.3">existence</w> <w n="29.4">m</w>’<w n="29.5">user</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Parmi</w> <w n="30.2">des</w> <w n="30.3">femmes</w> <w n="30.4">idolâtres</w></l>
					<l n="31" num="8.3"><w n="31.1">Désormais</w> <w n="31.2">je</w> <w n="31.3">veux</w> <w n="31.4">m</w>’<w n="31.5">amuser</w></l>
					<l n="32" num="8.4"><w n="32.1">Dans</w> <w n="32.2">les</w> <w n="32.3">endroits</w> <w n="32.4">les</w> <w n="32.5">plus</w> <w n="32.6">folâtres</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Hôte</w> <w n="33.2">d</w>’<w n="33.3">éblouissants</w> <w n="33.4">palais</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Je</w> <w n="34.2">fréquenterai</w> <w n="34.3">les</w> <w n="34.4">Folies</w>.</l>
					<l n="35" num="9.3"><w n="35.1">Bergère</w>, <w n="35.2">pour</w> <w n="35.3">courtiser</w> <w n="35.4">les</w></l>
					<l n="36" num="9.4"><w n="36.1">Demoiselles</w> <w n="36.2">aux</w> <w n="36.3">mœurs</w> <w n="36.4">polies</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Je</w> <w n="37.2">couronnerai</w> <w n="37.3">mes</w> <w n="37.4">destins</w>.</l>
					<l n="38" num="10.2"><w n="38.1">Puisqu</w>’<w n="38.2">ici</w>-<w n="38.3">bas</w> <w n="38.4">tout</w> <w n="38.5">n</w>’<w n="38.6">est</w> <w n="38.7">que</w> <w n="38.8">rêve</w>,</l>
					<l n="39" num="10.3"><w n="39.1">J</w>’<w n="39.2">organiserai</w> <w n="39.3">des</w> <w n="39.4">festins</w></l>
					<l n="40" num="10.4"><w n="40.1">Si</w> <w n="40.2">plantureux</w> <w n="40.3">que</w> <w n="40.4">l</w>’<w n="40.5">on</w> <w n="40.6">en</w> <w n="40.7">crève</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Je</w> <w n="41.2">veux</w> <w n="41.3">des</w> <w n="41.4">menus</w> <w n="41.5">abondants</w></l>
					<l n="42" num="11.2"><w n="42.1">Que</w> <w n="42.2">nul</w> <w n="42.3">ascète</w> <w n="42.4">ne</w> <w n="42.5">rature</w>.</l>
					<l n="43" num="11.3"><w n="43.1">Et</w> <w n="43.2">puisque</w> <w n="43.3">enfin</w> <w n="43.4">je</w> <w n="43.5">règne</w> <w n="43.6">dans</w></l>
					<l n="44" num="11.4"><w n="44.1">La</w> <w n="44.2">meilleure</w> <w n="44.3">littérature</w>,</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">J</w>’<w n="45.2">irai</w>, <w n="45.3">sans</w> <w n="45.4">demander</w> <w n="45.5">jusqu</w>’<w n="45.6">où</w>.</l>
					<l n="46" num="12.2"><w n="46.1">Tandis</w> <w n="46.2">que</w> <w n="46.3">siffleront</w> <w n="46.4">les</w> <w n="46.5">merles</w>,</l>
					<l n="47" num="12.3"><w n="47.1">On</w> <w n="47.2">attachera</w> <w n="47.3">sur</w> <w n="47.4">mon</w> <w n="47.5">cou</w></l>
					<l n="48" num="12.4"><w n="48.1">Des</w> <w n="48.2">fleurs</w> <w n="48.3">et</w> <w n="48.4">des</w> <w n="48.5">colliers</w> <w n="48.6">de</w> <w n="48.7">perles</w>,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Ermite</w>, <w n="49.2">dans</w> <w n="49.3">les</w> <w n="49.4">cieux</w> <w n="49.5">divins</w></l>
					<l n="50" num="13.2"><w n="50.1">Tu</w> <w n="50.2">peux</w> <w n="50.3">regarder</w> <w n="50.4">fuir</w> <w n="50.5">les</w> <w n="50.6">Anges</w>.</l>
					<l n="51" num="13.3"><w n="51.1">Moi</w> <w n="51.2">je</w> <w n="51.3">me</w> <w n="51.4">soûlerai</w> <w n="51.5">de</w> <w n="51.6">vins</w>,</l>
					<l n="52" num="13.4"><w n="52.1">De</w> <w n="52.2">belles</w> <w n="52.3">chairs</w> <w n="52.4">et</w> <w n="52.5">de</w> <w n="52.6">louanges</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Après</w> <w n="53.2">avoir</w> <w n="53.3">bien</w> <w n="53.4">déliré</w></l>
					<l n="54" num="14.2"><w n="54.1">Dans</w> <w n="54.2">une</w> <w n="54.3">éternelle</w> <w n="54.4">glissade</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Avec</w> <w n="55.2">délices</w> <w n="55.3">je</w> <w n="55.4">lirai</w></l>
					<l n="56" num="14.4"><w n="56.1">Mon</w> <w n="56.2">ravissant</w> <w n="56.3">marquis</w> <w n="56.4">de</w> <w n="56.5">Sade</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Je</w> <w n="57.2">mènerai</w> <w n="57.3">des</w> <w n="57.4">chœurs</w> <w n="57.5">dansants</w>,</l>
					<l n="58" num="15.2"><w n="58.1">Et</w>, <w n="58.2">tout</w> <w n="58.3">le</w> <w n="58.4">temps</w> <w n="58.5">que</w> <w n="58.6">le</w> <w n="58.7">jour</w> <w n="58.8">dure</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Caressé</w>, <w n="59.2">fier</w>, <w n="59.3">ivre</w> <w n="59.4">d</w>’<w n="59.5">encens</w>,</l>
					<l n="60" num="15.4"><w n="60.1">J</w>’<w n="60.2">irai</w> <w n="60.3">me</w> <w n="60.4">vautrer</w> <w n="60.5">dans</w> <w n="60.6">l</w>’<w n="60.7">ordure</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1889"> 2 avril 1889.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>