<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN691">
				<head type="number">LVIII</head>
				<head type="main">Avril</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Oh</w> ! <w n="1.2">sois</w> <w n="1.3">le</w> <w n="1.4">bien</w> <w n="1.5">venu</w>, <w n="1.6">Printemps</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Ami</w> <w n="2.2">joyeux</w> <w n="2.3">qui</w> <w n="2.4">nous</w> <w n="2.5">accueilles</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Fais</w> <w n="3.2">voler</w> <w n="3.3">tes</w> <w n="3.4">cheveux</w> <w n="3.5">flottants</w></l>
					<l n="4" num="1.4"><w n="4.1">Sous</w> <w n="4.2">ton</w> <w n="4.3">riant</w> <w n="4.4">chapeau</w> <w n="4.5">de</w> <w n="4.6">feuilles</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Voici</w> <w n="5.2">le</w> <w n="5.3">doux</w> <w n="5.4">mois</w>, <w n="5.5">cet</w> <w n="5.6">Avril</w></l>
					<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">sur</w> <w n="6.3">l</w>’<w n="6.4">asphalte</w>, <w n="6.5">en</w> <w n="6.6">son</w> <w n="6.7">extase</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Fait</w> <w n="7.2">briller</w> <w n="7.3">le</w> <w n="7.4">chrysobéryl</w></l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">flamber</w> <w n="8.3">la</w> <w n="8.4">jaune</w> <w n="8.5">topaze</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mille</w> <w n="9.2">rameaux</w> <w n="9.3">pleins</w> <w n="9.4">de</w> <w n="9.5">bourgeons</w></l>
					<l n="10" num="3.2"><w n="10.1">Préparent</w> <w n="10.2">leur</w> <w n="10.3">folle</w> <w n="10.4">parure</w>,</l>
					<l n="11" num="3.3"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">pourquoi</w>, <w n="11.4">mes</w> <w n="11.5">amis</w>, <w n="11.6">songeons</w></l>
					<l n="12" num="3.4"><w n="12.1">A</w> <w n="12.2">dépouiller</w> <w n="12.3">notre</w> <w n="12.4">fourrure</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Serrés</w> <w n="13.2">par</w> <w n="13.3">de</w> <w n="13.4">légers</w> <w n="13.5">vestons</w></l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">de</w> <w n="14.3">clair</w> <w n="14.4">soleil</w> <w n="14.5">idolâtres</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Les</w> <w n="15.2">hommes</w>, <w n="15.3">comme</w> <w n="15.4">des</w> <w n="15.5">festons</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Vont</w> <w n="16.2">briller</w> <w n="16.3">en</w> <w n="16.4">taches</w> <w n="16.5">folâtres</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Les</w> <w n="17.2">Halles</w> <w n="17.3">offrent</w> <w n="17.4">leurs</w> <w n="17.5">primeurs</w>.</l>
					<l n="18" num="5.2"><w n="18.1">On</w> <w n="18.2">peut</w> <w n="18.3">admirer</w> <w n="18.4">les</w> <w n="18.5">asperges</w></l>
					<l n="19" num="5.3"><w n="19.1">Grosses</w>, <w n="19.2">pour</w> <w n="19.3">charmer</w> <w n="19.4">les</w> <w n="19.5">rimeurs</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Comme</w> <w n="20.2">des</w> <w n="20.3">bras</w> <w n="20.4">de</w> <w n="20.5">jeunes</w> <w n="20.6">vierges</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Pareille</w> <w n="21.2">aux</w> <w n="21.3">flammes</w> <w n="21.4">d</w>’<w n="21.5">un</w> <w n="21.6">brasier</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Ève</w>, <w n="22.2">la</w> <w n="22.3">jeune</w> <w n="22.4">fleur</w> <w n="22.5">éclose</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Sent</w>, <w n="23.2">comme</w> <w n="23.3">un</w> <w n="23.4">bouton</w> <w n="23.5">de</w> <w n="23.6">rosier</w>,</l>
					<l n="24" num="6.4"><w n="24.1">S</w>’<w n="24.2">épanouir</w> <w n="24.3">sa</w> <w n="24.4">gorge</w> <w n="24.5">rose</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Plus</w> <w n="25.2">grisante</w> <w n="25.3">que</w> <w n="25.4">les</w> <w n="25.5">raisins</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Elle</w> <w n="26.2">va</w>, <w n="26.3">par</w> <w n="26.4">un</w> <w n="26.5">art</w> <w n="26.6">insigne</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Dans</w> <w n="27.2">les</w> <w n="27.3">divers</w> <w n="27.4">Grands</w> <w n="27.5">Magasins</w></l>
					<l n="28" num="7.4"><w n="28.1">Acheter</w> <w n="28.2">sa</w> <w n="28.3">feuille</w> <w n="28.4">de</w> <w n="28.5">vigne</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Prête</w> <w n="29.2">à</w> <w n="29.3">payer</w> <w n="29.4">d</w>’<w n="29.5">un</w> <w n="29.6">seul</w> <w n="29.7">radis</w></l>
					<l n="30" num="8.2"><w n="30.1">Le</w> <w n="30.2">philosophe</w> <w n="30.3">ennuyeux</w>, <w n="30.4">comme</w></l>
					<l n="31" num="8.3"><w n="31.1">Autrefois</w>, <w n="31.2">dans</w> <w n="31.3">le</w> <w n="31.4">paradis</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Elle</w> <w n="32.2">aspire</w> <w n="32.3">à</w> <w n="32.4">manger</w> <w n="32.5">la</w> <w n="32.6">pomme</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">O</w> <w n="33.2">psychologue</w>, <w n="33.3">esprit</w> <w n="33.4">ouvert</w> !</l>
					<l n="34" num="9.2"><w n="34.1">Même</w>, <w n="34.2">il</w> <w n="34.3">faudrait</w> <w n="34.4">que</w> <w n="34.5">tu</w> <w n="34.6">la</w> <w n="34.7">visses</w></l>
					<l n="35" num="9.3"><w n="35.1">Grignoter</w>, <w n="35.2">avant</w> <w n="35.3">ce</w> <w n="35.4">fruit</w> <w n="35.5">vert</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Un</w> <w n="36.2">tas</w> <w n="36.3">de</w> <w n="36.4">rouges</w> <w n="36.5">écrevisses</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Pendant</w> <w n="37.2">ces</w> <w n="37.3">jours</w> <w n="37.4">aventureux</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Le</w> <w n="38.2">Printemps</w>, <w n="38.3">secouant</w> <w n="38.4">ses</w> <w n="38.5">ailes</w></l>
					<l n="39" num="10.3"><w n="39.1">Sur</w> <w n="39.2">tous</w> <w n="39.3">les</w> <w n="39.4">nids</w> <w n="39.5">des</w> <w n="39.6">amoureux</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Dit</w> : <w n="40.2">En</w> <w n="40.3">classe</w>, <w n="40.4">mesdemoiselles</w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Cernay</w>, <w n="41.2">c</w>’<w n="41.3">est</w> <w n="41.4">le</w> <w n="41.5">pays</w> <w n="41.6">charmant</w></l>
					<l n="42" num="11.2"><w n="42.1">Où</w> <w n="42.2">l</w>’<w n="42.3">on</w> <w n="42.4">dit</w> <w n="42.5">à</w> <w n="42.6">Rose</w> : <w n="42.7">Qu</w>’<w n="42.8">a</w>-<w n="42.9">t</w>-<w n="42.10">elle</w> ?</l>
					<l n="43" num="11.3"><w n="43.1">Irisé</w>, <w n="43.2">le</w> <w n="43.3">blanc</w> <w n="43.4">diamant</w></l>
					<l n="44" num="11.4"><w n="44.1">Ruisselle</w> <w n="44.2">de</w> <w n="44.3">la</w> <w n="44.4">cascatelle</w> ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Et</w> <w n="45.2">Corot</w>, <w n="45.3">qui</w> <w n="45.4">fut</w> <w n="45.5">dans</w> <w n="45.6">le</w> <w n="45.7">vrai</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Donne</w>, <w n="46.2">en</w> <w n="46.3">guirlandes</w> <w n="46.4">ingénues</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Aux</w> <w n="47.2">coteaux</w> <w n="47.3">de</w> <w n="47.4">Ville</w>-<w n="47.5">d</w>’<w n="47.6">Avray</w></l>
					<l n="48" num="12.4"><w n="48.1">Un</w> <w n="48.2">chœur</w> <w n="48.3">de</w> <w n="48.4">Nymphes</w> <w n="48.5">toutes</w> <w n="48.6">nues</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Un</w> <w n="49.2">pays</w> <w n="49.3">vraiment</w> <w n="49.4">enjoué</w></l>
					<l n="50" num="13.2"><w n="50.1">Vit</w> <w n="50.2">dans</w> <w n="50.3">la</w> <w n="50.4">maritime</w> <w n="50.5">Asnières</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Où</w> <w n="51.2">l</w>’<w n="51.3">on</w> <w n="51.4">dit</w> <w n="51.5">que</w> <w n="51.6">parfois</w> <w n="51.7">Chloé</w></l>
					<l n="52" num="13.4"><w n="52.1">Subit</w> <w n="52.2">les</w> <w n="52.3">injures</w> <w n="52.4">dernières</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Là</w> <w n="53.2">d</w>’<w n="53.3">aventureux</w> <w n="53.4">matelots</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Prodigues</w> <w n="54.2">du</w> <w n="54.3">temps</w> <w n="54.4">qui</w> <w n="54.5">s</w>’<w n="54.6">envole</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Emportent</w> <w n="55.2">sur</w> <w n="55.3">l</w>’<w n="55.4">azur</w> <w n="55.5">des</w> <w n="55.6">flots</w></l>
					<l n="56" num="14.4"><w n="56.1">Des</w> <w n="56.2">personnes</w> <w n="56.3">d</w>’<w n="56.4">un</w> <w n="56.5">goût</w> <w n="56.6">frivole</w> ;</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Et</w>, <w n="57.2">leurs</w> <w n="57.3">beaux</w> <w n="57.4">seins</w> <w n="57.5">gonflés</w> <w n="57.6">d</w>’<w n="57.7">amour</w>,</l>
					<l n="58" num="15.2"><w n="58.1">Les</w> <w n="58.2">vagues</w> <w n="58.3">apaisent</w> <w n="58.4">l</w>’<w n="58.5">orchestre</w></l>
					<l n="59" num="15.3"><w n="59.1">De</w> <w n="59.2">leurs</w> <w n="59.3">orageux</w> <w n="59.4">sanglots</w>, <w n="59.5">pour</w></l>
					<l n="60" num="15.4"><w n="60.1">Écouter</w> <w n="60.2">les</w> <w n="60.3">vers</w> <w n="60.4">de</w> <w n="60.5">Silvestre</w>.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Nous</w> <w n="61.2">sommes</w> <w n="61.3">las</w> <w n="61.4">de</w> <w n="61.5">réfléchir</w> :</l>
					<l n="62" num="16.2"><w n="62.1">Que</w> <w n="62.2">notre</w> <w n="62.3">âme</w> <w n="62.4">enfin</w> <w n="62.5">s</w>’<w n="62.6">extasie</w> !</l>
					<l n="63" num="16.3"><w n="63.1">Doux</w> <w n="63.2">Printemps</w>, <w n="63.3">viens</w> <w n="63.4">nous</w> <w n="63.5">rafraîchir</w></l>
					<l n="64" num="16.4"><w n="64.1">Avec</w> <w n="64.2">ton</w> <w n="64.3">souffle</w> <w n="64.4">d</w>’<w n="64.5">ambroisie</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Vous</w> <w n="65.2">voilà</w> <w n="65.3">mûrs</w> <w n="65.4">pour</w> <w n="65.5">le</w> <w n="65.6">repos</w>,</l>
					<l n="66" num="17.2"><w n="66.1">Esprit</w> <w n="66.2">banal</w> <w n="66.3">qui</w> <w n="66.4">nous</w> <w n="66.5">écœures</w>,</l>
					<l n="67" num="17.3"><w n="67.1">Vaudeville</w> <w n="67.2">enflant</w> <w n="67.3">tes</w> <w n="67.4">pipeaux</w>,</l>
					<l n="68" num="17.4"><w n="68.1">Et</w> <w n="68.2">vous</w> <w n="68.3">aussi</w>, <w n="68.4">thés</w> <w n="68.5">de</w> <w n="68.6">cinq</w> <w n="68.7">heures</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1890">1er avril 1890.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>