<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN660">
				<head type="number">XXVII</head>
				<head type="main">Jocrisse</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ce</w> <w n="1.2">siècle</w>, <w n="1.3">beau</w>, <w n="1.4">mais</w> <w n="1.5">décadent</w>,</l>
					<l n="2" num="1.2">— <w n="2.1">Comme</w> <w n="2.2">l</w>’<w n="2.3">ont</w> <w n="2.4">prédit</w> <w n="2.5">les</w> <w n="2.6">augures</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Voit</w> <w n="3.2">au</w> <w n="3.3">fond</w> <w n="3.4">du</w> <w n="3.5">rouge</w> <w n="3.6">occident</w></l>
					<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">effacer</w> <w n="4.3">les</w> <w n="4.4">grandes</w> <w n="4.5">figures</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Mangin</w> <w n="5.2">ne</w> <w n="5.3">vend</w> <w n="5.4">plus</w> <w n="5.5">de</w> <w n="5.6">crayons</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Avec</w> <w n="6.2">son</w> <w n="6.3">bagout</w> <w n="6.4">dur</w> <w n="6.5">et</w> <w n="6.6">leste</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Peut</w>-<w n="7.2">être</w> <w n="7.3">qu</w>’<w n="7.4">il</w> <w n="7.5">vend</w> <w n="7.6">des</w> <w n="7.7">rayons</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Dans</w> <w n="8.2">une</w> <w n="8.3">calèche</w> <w n="8.4">céleste</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Robert</w> <w n="9.2">Macaire</w> <w n="9.3">n</w>’<w n="9.4">est</w> <w n="9.5">plus</w> <w n="9.6">roi</w></l>
					<l n="10" num="3.2"><w n="10.1">Au</w> <w n="10.2">bagne</w> <w n="10.3">affreux</w> <w n="10.4">ni</w> <w n="10.5">dans</w> <w n="10.6">le</w> <w n="10.7">bouge</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">ne</w> <w n="11.3">fait</w> <w n="11.4">pas</w> <w n="11.5">naître</w> <w n="11.6">l</w>’<w n="11.7">effroi</w>,</l>
					<l n="12" num="3.4"><w n="12.1">N</w>’<w n="12.2">ayant</w> <w n="12.3">plus</w> <w n="12.4">son</w> <w n="12.5">pantalon</w> <w n="12.6">rouge</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">triste</w>, <w n="13.3">faisant</w> <w n="13.4">son</w> <w n="13.5">paquet</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Emportant</w> <w n="14.2">sa</w> <w n="14.3">malle</w> <w n="14.4">et</w> <w n="14.5">sa</w> <w n="14.6">harpe</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">prodigieux</w> <w n="15.3">Bilboquet</w></l>
					<l n="16" num="4.4"><w n="16.1">Renonce</w> <w n="16.2">à</w> <w n="16.3">marchander</w> <w n="16.4">la</w> <w n="16.5">carpe</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Prudhomme</w>, <w n="17.2">exempt</w> <w n="17.3">de</w> <w n="17.4">tous</w> <w n="17.5">mollets</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Sur</w> <w n="18.2">son</w> <w n="18.3">front</w> <w n="18.4">dévasté</w> <w n="18.5">ramène</w></l>
					<l n="19" num="5.3"><w n="19.1">Des</w> <w n="19.2">crins</w>, <w n="19.3">plus</w> <w n="19.4">étirés</w> <w n="19.5">que</w> <w n="19.6">les</w></l>
					<l n="20" num="5.4"><w n="20.1">Vers</w> <w n="20.2">du</w> <w n="20.3">récit</w> <w n="20.4">de</w> <w n="20.5">Théramène</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Polichinelle</w> <w n="21.2">est</w> <w n="21.3">aboli</w>.</l>
					<l n="22" num="6.2"><w n="22.1">Dans</w> <w n="22.2">la</w> <w n="22.3">neige</w> <w n="22.4">d</w>’<w n="22.5">une</w> <w n="22.6">avalanche</w></l>
					<l n="23" num="6.3"><w n="23.1">Se</w> <w n="23.2">dissipe</w> <w n="23.3">son</w> <w n="23.4">nez</w> <w n="23.5">pâli</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Aussi</w> <w n="24.2">blanc</w> <w n="24.3">qu</w>’<w n="24.4">une</w> <w n="24.5">truffe</w> <w n="24.6">blanche</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Pierrot</w>, <w n="25.2">morne</w> <w n="25.3">et</w> <w n="25.4">l</w>’<w n="25.5">air</w> <w n="25.6">abattu</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Se</w> <w n="26.2">promenant</w> <w n="26.3">à</w> <w n="26.4">Pampelune</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Dit</w> : <w n="27.2">O</w> <w n="27.3">Lune</w>, <w n="27.4">me</w> <w n="27.5">connais</w>-<w n="27.6">tu</w> ?</l>
					<l n="28" num="7.4"><w n="28.1">Pas</w> <w n="28.2">du</w> <w n="28.3">tout</w>, <w n="28.4">dit</w> <w n="28.5">la</w> <w n="28.6">blanche</w> <w n="28.7">Lune</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Oui</w>, <w n="29.2">tous</w> <w n="29.3">ces</w> <w n="29.4">héros</w> <w n="29.5">glorieux</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Que</w> <w n="30.2">les</w> <w n="30.3">cieux</w> <w n="30.4">de</w> <w n="30.5">flamme</w> <w n="30.6">éblouissent</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Dans</w> <w n="31.2">les</w> <w n="31.3">lointains</w> <w n="31.4">mystérieux</w></l>
					<l n="32" num="8.4"><w n="32.1">S</w>’<w n="32.2">effacent</w> <w n="32.3">et</w> <w n="32.4">s</w>’<w n="32.5">évanouissent</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Seul</w>, <w n="33.2">ô</w> <w n="33.3">Jocrisse</w>, <w n="33.4">aimable</w> <w n="33.5">enfant</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Dont</w> <w n="34.2">l</w>’<w n="34.3">œil</w> <w n="34.4">doux</w> <w n="34.5">charmait</w> <w n="34.6">ta</w> <w n="34.7">nourrice</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Toi</w> <w n="35.2">que</w> <w n="35.3">l</w>’<w n="35.4">Illusion</w> <w n="35.5">défend</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Chaste</w>, <w n="36.2">ingénu</w>, <w n="36.3">divin</w> <w n="36.4">Jocrisse</w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Être</w> <w n="37.2">initial</w> <w n="37.3">et</w> <w n="37.4">sans</w> <w n="37.5">prix</w>,</l>
					<l n="38" num="10.2"><w n="38.1">O</w> <w n="38.2">toi</w> <w n="38.3">que</w> <w n="38.4">la</w> <w n="38.5">brise</w> <w n="38.6">courtise</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Au</w> <w n="39.2">milieu</w> <w n="39.3">de</w> <w n="39.4">nous</w> <w n="39.5">tu</w> <w n="39.6">fleuris</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Éternel</w> <w n="40.2">comme</w> <w n="40.3">la</w> <w n="40.4">Bêtise</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Dans</w> <w n="41.2">tes</w> <w n="41.3">petits</w> <w n="41.4">yeux</w> <w n="41.5">radieux</w></l>
					<l n="42" num="11.2"><w n="42.1">La</w> <w n="42.2">Certitude</w> <w n="42.3">heureuse</w> <w n="42.4">éclate</w>.</l>
					<l n="43" num="11.3"><w n="43.1">Naïf</w>, <w n="43.2">tu</w> <w n="43.3">vas</w>, <w n="43.4">comme</w> <w n="43.5">les</w> <w n="43.6">Dieux</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Vêtu</w> <w n="44.2">de</w> <w n="44.3">la</w> <w n="44.4">pourpre</w> <w n="44.5">écarlate</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Plus</w> <w n="45.2">allègre</w> <w n="45.3">que</w> <w n="45.4">Jupillon</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Pareil</w> <w n="46.2">à</w> <w n="46.3">la</w> <w n="46.4">fleur</w> <w n="46.5">sur</w> <w n="46.6">sa</w> <w n="46.7">tige</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Un</w> <w n="47.2">symbolique</w> <w n="47.3">papillon</w></l>
					<l n="48" num="12.4"><w n="48.1">Près</w> <w n="48.2">de</w> <w n="48.3">tes</w> <w n="48.4">cheveux</w> <w n="48.5">roux</w> <w n="48.6">voltige</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">ce</w> <w n="49.3">messager</w> <w n="49.4">du</w> <w n="49.5">ciel</w> <w n="49.6">pur</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Léger</w> <w n="50.2">comme</w> <w n="50.3">ta</w> <w n="50.4">petite</w> <w n="50.5">âme</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Jette</w> <w n="51.2">un</w> <w n="51.3">éclair</w> <w n="51.4">d</w>’<w n="51.5">or</w> <w n="51.6">et</w> <w n="51.7">d</w>’<w n="51.8">azur</w></l>
					<l n="52" num="13.4"><w n="52.1">Dans</w> <w n="52.2">ta</w> <w n="52.3">chevelure</w> <w n="52.4">de</w> <w n="52.5">flamme</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Toi</w>, <w n="53.2">sur</w> <w n="53.3">qui</w> <w n="53.4">la</w> <w n="53.5">Fée</w>, <w n="53.6">en</w> <w n="53.7">rêvant</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Pose</w> <w n="54.2">encor</w> <w n="54.3">sa</w> <w n="54.4">main</w> <w n="54.5">protectrice</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Ami</w> <w n="55.2">du</w> <w n="55.3">soleil</w> <w n="55.4">et</w> <w n="55.5">du</w> <w n="55.6">vent</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Incommensurable</w> <w n="56.2">Jocrisse</w>,</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Ignorant</w> <w n="57.2">ce</w> <w n="57.3">que</w> <w n="57.4">les</w> <w n="57.5">passants</w></l>
					<l n="58" num="15.2"><w n="58.1">Peuvent</w> <w n="58.2">abriter</w> <w n="58.3">sous</w> <w n="58.4">leurs</w> <w n="58.5">crânes</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Tu</w> <w n="59.2">montres</w> <w n="59.3">autant</w> <w n="59.4">de</w> <w n="59.5">bon</w> <w n="59.6">sens</w></l>
					<l n="60" num="15.4"><w n="60.1">Et</w> <w n="60.2">de</w> <w n="60.3">sagesse</w> — <w n="60.4">que</w> <w n="60.5">les</w> <w n="60.6">Ânes</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1889"> 22 janvier 1889.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>