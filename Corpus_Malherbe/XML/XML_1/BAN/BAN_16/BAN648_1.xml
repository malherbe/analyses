<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN648">
				<head type="number">XV</head>
				<head type="main">Margot</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Grosse</w> <w n="1.2">Margot</w>, <w n="1.3">blanche</w> <w n="1.4">nourrice</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">adorait</w>, <w n="2.3">en</w> <w n="2.4">son</w> <w n="2.5">fier</w> <w n="2.6">caprice</w>,</l>
					<l n="3" num="1.3"><space quantity="4" unit="char"></space><w n="3.1">Le</w> <w n="3.2">bon</w> <w n="3.3">rimeur</w> <w n="3.4">Villon</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">toi</w>, <w n="4.3">sa</w> <w n="4.4">conquête</w> <w n="4.5">et</w> <w n="4.6">sa</w> <w n="4.7">proie</w></l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">le</w> <w n="5.3">navire</w> <w n="5.4">de</w> <w n="5.5">sa</w> <w n="5.6">joie</w>,</l>
					<l n="6" num="1.6"><space quantity="4" unit="char"></space><w n="6.1">Flotte</w> <w n="6.2">son</w> <w n="6.3">pavillon</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">En</w> <w n="7.2">son</w> <w n="7.3">âme</w> <w n="7.4">dévotieuse</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Il</w> <w n="8.2">t</w>’<w n="8.3">estimait</w> <w n="8.4">plus</w> <w n="8.5">précieuse</w></l>
					<l n="9" num="2.3"><space quantity="4" unit="char"></space><w n="9.1">Que</w> <w n="9.2">de</w> <w n="9.3">l</w>’<w n="9.4">or</w> <w n="9.5">en</w> <w n="9.6">lingot</w></l>
					<l n="10" num="2.4"><w n="10.1">Et</w>, <w n="10.2">mieux</w> <w n="10.3">qu</w>’<w n="10.4">une</w> <w n="10.5">chair</w> <w n="10.6">de</w> <w n="10.7">princesse</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Il</w> <w n="11.2">aimait</w> <w n="11.3">et</w> <w n="11.4">choyait</w> <w n="11.5">sans</w> <w n="11.6">cesse</w></l>
					<l n="12" num="2.6"><space quantity="4" unit="char"></space><w n="12.1">Ton</w> <w n="12.2">sein</w>, <w n="12.3">grosse</w> <w n="12.4">Margot</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Si</w> <w n="13.2">bien</w> <w n="13.3">qu</w>’<w n="13.4">en</w> <w n="13.5">ta</w> <w n="13.6">jupe</w> <w n="13.7">de</w> <w n="13.8">laine</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Immortelle</w> <w n="14.2">comme</w> <w n="14.3">une</w> <w n="14.4">Hélène</w>,</l>
					<l n="15" num="3.3"><space quantity="4" unit="char"></space><w n="15.1">Ravis</w>, <w n="15.2">nous</w> <w n="15.3">te</w> <w n="15.4">voyons</w></l>
					<l n="16" num="3.4"><w n="16.1">Avec</w> <w n="16.2">ta</w> <w n="16.3">glorieuse</w> <w n="16.4">allure</w>,</l>
					<l n="17" num="3.5"><w n="17.1">Et</w> <w n="17.2">que</w> <w n="17.3">ta</w> <w n="17.4">lourde</w> <w n="17.5">chevelure</w></l>
					<l n="18" num="3.6"><space quantity="4" unit="char"></space><w n="18.1">Est</w> <w n="18.2">pleine</w> <w n="18.3">de</w> <w n="18.4">rayons</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Lui</w>, <w n="19.2">le</w> <w n="19.3">génie</w>, <w n="19.4">et</w> <w n="19.5">toi</w>, <w n="19.6">la</w> <w n="19.7">gouge</w>,</l>
					<l n="20" num="4.2"><w n="20.1">Vous</w> <w n="20.2">buviez</w> <w n="20.3">à</w> <w n="20.4">flots</w> <w n="20.5">du</w> <w n="20.6">vin</w> <w n="20.7">rouge</w>,</l>
					<l n="21" num="4.3"><space quantity="4" unit="char"></space><w n="21.1">Plein</w> <w n="21.2">de</w> <w n="21.3">rubis</w> <w n="21.4">ardents</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Fleurant</w> <w n="22.2">comme</w> <w n="22.3">des</w> <w n="22.4">violettes</w>,</l>
					<l n="23" num="4.5"><w n="23.1">Et</w> <w n="23.2">la</w> <w n="23.3">pourpre</w> <w n="23.4">des</w> <w n="23.5">gouttelettes</w></l>
					<l n="24" num="4.6"><space quantity="4" unit="char"></space><w n="24.1">Ruisselait</w> <w n="24.2">sur</w> <w n="24.3">tes</w> <w n="24.4">dents</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Maintenant</w>, <w n="25.2">ô</w> <w n="25.3">perle</w> <w n="25.4">des</w> <w n="25.5">filles</w>,</l>
					<l n="26" num="5.2"><w n="26.1">Tu</w> <w n="26.2">resplendis</w> <w n="26.3">encor</w>, <w n="26.4">tu</w> <w n="26.5">brilles</w>,</l>
					<l n="27" num="5.3"><space quantity="4" unit="char"></space><w n="27.1">Comme</w> <w n="27.2">de</w> <w n="27.3">l</w>’<w n="27.4">or</w> <w n="27.5">moulu</w>,</l>
					<l n="28" num="5.4"><w n="28.1">Lorsque</w> <w n="28.2">sur</w> <w n="28.3">du</w> <w n="28.4">cuivre</w> <w n="28.5">on</w> <w n="28.6">l</w>’<w n="28.7">applique</w>,</l>
					<l n="29" num="5.5"><w n="29.1">Par</w> <w n="29.2">cette</w> <w n="29.3">raison</w> <w n="29.4">sans</w> <w n="29.5">réplique</w> :</l>
					<l n="30" num="5.6"><w n="30.1">Ton</w> <w n="30.2">François</w> <w n="30.3">l</w>’<w n="30.4">a</w> <w n="30.5">voulu</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Oui</w>, <w n="31.2">ce</w> <w n="31.3">grand</w> <w n="31.4">Villon</w> <w n="31.5">t</w>’<w n="31.6">a</w> <w n="31.7">choisie</w>,</l>
					<l n="32" num="6.2"><w n="32.1">Comme</w> <w n="32.2">un</w> <w n="32.3">dieu</w> <w n="32.4">de</w> <w n="32.5">la</w> <w n="32.6">poésie</w>,</l>
					<l n="33" num="6.3"><space quantity="4" unit="char"></space><w n="33.1">Recueillant</w> <w n="33.2">son</w> <w n="33.3">butin</w>,</l>
					<l n="34" num="6.4"><w n="34.1">Choisit</w> <w n="34.2">la</w> <w n="34.3">fille</w> <w n="34.4">de</w> <w n="34.5">Tyndare</w> ;</l>
					<l n="35" num="6.5"><w n="35.1">Car</w> <w n="35.2">il</w> <w n="35.3">chantait</w> <w n="35.4">comme</w> <w n="35.5">Pindare</w>,</l>
					<l n="36" num="6.6"><space quantity="4" unit="char"></space><w n="36.1">A</w> <w n="36.2">Paris</w>, <w n="36.3">près</w> <w n="36.4">Pantin</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 25 août 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>