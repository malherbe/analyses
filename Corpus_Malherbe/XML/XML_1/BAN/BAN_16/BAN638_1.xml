<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN638">
				<head type="number">V</head>
				<head type="main">Soleil</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lorsque</w> <w n="1.2">Juin</w> <w n="1.3">fait</w> <w n="1.4">même</w> <w n="1.5">sourire</w></l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">Le</w> <w n="2.2">noir</w> <w n="2.3">cachot</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">n</w>’<w n="3.3">aime</w> <w n="3.4">pas</w> <w n="3.5">entendre</w> <w n="3.6">dire</w></l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Qu</w>’<w n="4.2">il</w> <w n="4.3">fait</w> <w n="4.4">trop</w> <w n="4.5">chaud</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Non</w>. <w n="5.2">Pas</w> <w n="5.3">assez</w> <w n="5.4">chaud</w>. <w n="5.5">Que</w> <w n="5.6">notre</w> <w n="5.7">âme</w></l>
					<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1">Au</w> <w n="6.2">jour</w> <w n="6.3">vermeil</w></l>
					<l n="7" num="2.3"><w n="7.1">Renaisse</w>, <w n="7.2">prenne</w> <w n="7.3">un</w> <w n="7.4">bain</w> <w n="7.5">de</w> <w n="7.6">flamme</w></l>
					<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">Et</w> <w n="8.2">de</w> <w n="8.3">soleil</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">O</w> <w n="9.2">Zéphyr</w>, <w n="9.3">tandis</w> <w n="9.4">que</w> <w n="9.5">tu</w> <w n="9.6">bouges</w></l>
					<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">Dans</w> <w n="10.2">le</w> <w n="10.3">ciel</w> <w n="10.4">bleu</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">toutes</w> <w n="11.3">les</w> <w n="11.4">lèvres</w> <w n="11.5">soient</w> <w n="11.6">rouges</w></l>
					<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">Comme</w> <w n="12.2">du</w> <w n="12.3">feu</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Que</w> <w n="13.2">hors</w> <w n="13.3">du</w> <w n="13.4">corsage</w>, <w n="13.5">sans</w> <w n="13.6">honte</w></l>
					<l n="14" num="4.2"><space quantity="8" unit="char"></space><w n="14.1">Les</w> <w n="14.2">jeunes</w> <w n="14.3">seins</w></l>
					<l n="15" num="4.3"><w n="15.1">Tressaillent</w>, <w n="15.2">sans</w> <w n="15.3">rendre</w> <w n="15.4">nul</w> <w n="15.5">compte</w></l>
					<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">De</w> <w n="16.2">leurs</w> <w n="16.3">desseins</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">veux</w> <w n="17.3">dans</w> <w n="17.4">les</w> <w n="17.5">apothéoses</w></l>
					<l n="18" num="5.2"><space quantity="8" unit="char"></space><w n="18.1">Entendre</w>, <w n="18.2">autour</w></l>
					<l n="19" num="5.3"><w n="19.1">Du</w> <w n="19.2">jardin</w>, <w n="19.3">les</w> <w n="19.4">bouches</w> <w n="19.5">des</w> <w n="19.6">roses</w></l>
					<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1">Crier</w> <w n="20.2">d</w>’<w n="20.3">amour</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Oublions</w> <w n="21.2">les</w> <w n="21.3">matins</w> <w n="21.4">livides</w>,</l>
					<l n="22" num="6.2"><space quantity="8" unit="char"></space><w n="22.1">Flore</w> <w n="22.2">aux</w> <w n="22.3">abois</w>,</l>
					<l n="23" num="6.3"><w n="23.1">La</w> <w n="23.2">malignité</w> <w n="23.3">des</w> <w n="23.4">avides</w></l>
					<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">Marchands</w> <w n="24.2">de</w> <w n="24.3">bois</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">voulant</w> <w n="25.3">que</w> <w n="25.4">l</w>’<w n="25.5">azur</w> <w n="25.6">nous</w> <w n="25.7">voie</w></l>
					<l n="26" num="7.2"><space quantity="8" unit="char"></space><w n="26.1">Contents</w>, <w n="26.2">ayons</w></l>
					<l n="27" num="7.3"><w n="27.1">Les</w> <w n="27.2">prunelles</w> <w n="27.3">pleines</w> <w n="27.4">de</w> <w n="27.5">joie</w></l>
					<l n="28" num="7.4"><space quantity="8" unit="char"></space><w n="28.1">Et</w> <w n="28.2">de</w> <w n="28.3">rayons</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1888"> 16 juin 1888.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>