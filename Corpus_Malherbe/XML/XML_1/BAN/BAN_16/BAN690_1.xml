<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN690">
				<head type="number">LVII</head>
				<head type="main">Controverse</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ayant</w> <w n="1.2">du</w> <w n="1.3">loisir</w>, <w n="1.4">et</w> <w n="1.5">comptant</w></l>
					<l n="2" num="1.2"><w n="2.1">M</w>’<w n="2.2">amuser</w>, <w n="2.3">je</w> <w n="2.4">dis</w> <w n="2.5">au</w> <w n="2.6">vieux</w> <w n="2.7">Diable</w> :</l>
					<l n="3" num="1.3"><w n="3.1">Enfin</w> <w n="3.2">tu</w> <w n="3.3">dois</w> <w n="3.4">être</w> <w n="3.5">content</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">chose</w> <w n="4.3">est</w> <w n="4.4">irrémédiable</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Nargue</w> <w n="5.2">le</w> <w n="5.3">ciel</w> <w n="5.4">aérien</w> !</l>
					<l n="6" num="2.2"><w n="6.1">Tu</w> <w n="6.2">triomphes</w> ; <w n="6.3">l</w>’<w n="6.4">heure</w> <w n="6.5">est</w> <w n="6.6">sonnée</w></l>
					<l n="7" num="2.3"><w n="7.1">Où</w> <w n="7.2">la</w> <w n="7.3">Femme</w> <w n="7.4">ne</w> <w n="7.5">vaut</w> <w n="7.6">plus</w> <w n="7.7">rien</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Grâce</w> <w n="8.2">à</w> <w n="8.3">toi</w>, <w n="8.4">qui</w> <w n="8.5">l</w>’<w n="8.6">as</w> <w n="8.7">façonnée</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">se</w> <w n="9.3">mêle</w> <w n="9.4">un</w> <w n="9.5">subtil</w> <w n="9.6">poison</w></l>
					<l n="10" num="3.2"><w n="10.1">Au</w> <w n="10.2">vin</w> <w n="10.3">doré</w> <w n="10.4">qu</w>’<w n="10.5">elle</w> <w n="10.6">nous</w> <w n="10.7">verse</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Blanche</w> <w n="11.2">sous</w> <w n="11.3">la</w> <w n="11.4">folle</w> <w n="11.5">toison</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Elle</w> <w n="12.2">est</w> <w n="12.3">horriblement</w> <w n="12.4">perverse</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Femelle</w> <w n="13.2">avec</w> <w n="13.3">ou</w> <w n="13.4">sans</w> <w n="13.5">petits</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">mènent</w> <w n="14.3">des</w> <w n="14.4">instincts</w> <w n="14.5">atroces</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Elle</w> <w n="15.2">a</w> <w n="15.3">les</w> <w n="15.4">hideux</w> <w n="15.5">appétits</w></l>
					<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">toutes</w> <w n="16.3">les</w> <w n="16.4">bêtes</w> <w n="16.5">féroces</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Elle</w> <w n="17.2">est</w> <w n="17.3">panthère</w> <w n="17.4">aux</w> <w n="17.5">yeux</w> <w n="17.6">ardents</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Elle</w> <w n="18.2">est</w> <w n="18.3">lionne</w> <w n="18.4">en</w> <w n="18.5">son</w> <w n="18.6">repaire</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Elle</w> <w n="19.2">est</w> <w n="19.3">tigresse</w> <w n="19.4">aux</w> <w n="19.5">fortes</w> <w n="19.6">dents</w> ;</l>
					<l n="20" num="5.4"><w n="20.1">Elle</w> <w n="20.2">est</w> <w n="20.3">aussi</w> <w n="20.4">chatte</w> <w n="20.5">et</w> <w n="20.6">vipère</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Elle</w> <w n="21.2">obéit</w> <w n="21.3">à</w> <w n="21.4">son</w> <w n="21.5">destin</w>,</l>
					<l n="22" num="6.2"><w n="22.1">L</w>’<w n="22.2">expérience</w> <w n="22.3">nous</w> <w n="22.4">l</w>’<w n="22.5">enseigne</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Lorsque</w> <w n="23.2">pour</w> <w n="23.3">son</w> <w n="23.4">joyeux</w> <w n="23.5">festin</w></l>
					<l n="24" num="6.4"><w n="24.1">Elle</w> <w n="24.2">dévore</w> <w n="24.3">un</w> <w n="24.4">cœur</w> <w n="24.5">qui</w> <w n="24.6">saigne</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Elle</w> <w n="25.2">apprête</w> <w n="25.3">ses</w> <w n="25.4">trahisons</w></l>
					<l n="26" num="7.2"><w n="26.1">Avec</w> <w n="26.2">une</w> <w n="26.3">rare</w> <w n="26.4">sagesse</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">gratte</w> <w n="27.3">ses</w> <w n="27.4">démangeaisons</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Par</w> <w n="28.2">atavisme</w>, <w n="28.3">étant</w> <w n="28.4">singesse</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Toi</w> <w n="29.2">qui</w> <w n="29.3">l</w>’<w n="29.4">adoptas</w> <w n="29.5">dès</w> <w n="29.6">hier</w></l>
					<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">qui</w> <w n="30.3">l</w>’<w n="30.4">avais</w> <w n="30.5">prise</w> <w n="30.6">en</w> <w n="30.7">sevrage</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Bon</w> <w n="31.2">Diable</w>, <w n="31.3">tu</w> <w n="31.4">dois</w> <w n="31.5">être</w> <w n="31.6">fier</w></l>
					<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">satisfait</w> <w n="32.3">de</w> <w n="32.4">ton</w> <w n="32.5">ouvrage</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Telle</w> <w n="33.2">ma</w> <w n="33.3">colère</w> <w n="33.4">parlait</w>.</l>
					<l n="34" num="9.2"><w n="34.1">Mais</w> <w n="34.2">le</w> <w n="34.3">Diable</w>, <w n="34.4">que</w> <w n="34.5">rien</w> <w n="34.6">n</w>’<w n="34.7">entame</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Dit</w> : <w n="35.2">Je</w> <w n="35.3">suis</w>, <w n="35.4">comme</w> <w n="35.5">un</w> <w n="35.6">autre</w> <w n="35.7">Hamlet</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Assez</w> <w n="36.2">mécontent</w> <w n="36.3">de</w> <w n="36.4">la</w> <w n="36.5">Femme</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Contre</w> <w n="37.2">elle</w> <w n="37.3">acharné</w> <w n="37.4">vainement</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Je</w> <w n="38.2">n</w>’<w n="38.3">ai</w> <w n="38.4">fait</w> <w n="38.5">que</w> <w n="38.6">de</w> <w n="38.7">la</w> <w n="38.8">bouillie</w></l>
					<l n="39" num="10.3"><w n="39.1">Pour</w> <w n="39.2">les</w> <w n="39.3">chats</w>, <w n="39.4">et</w> <w n="39.5">l</w>’<w n="39.6">événement</w></l>
					<l n="40" num="10.4"><w n="40.1">Trompe</w> <w n="40.2">encor</w> <w n="40.3">ma</w> <w n="40.4">ruse</w> <w n="40.5">vieillie</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Faute</w> <w n="41.2">d</w>’<w n="41.3">avoir</w> <w n="41.4">assez</w> <w n="41.5">bien</w> <w n="41.6">lu</w></l>
					<l n="42" num="11.2"><w n="42.1">Ce</w> <w n="42.2">qui</w> <w n="42.3">concerne</w> <w n="42.4">cette</w> <w n="42.5">amante</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Vieux</w> <w n="43.2">logicien</w>, <w n="43.3">j</w>’<w n="43.4">ai</w> <w n="43.5">voulu</w></l>
					<l n="44" num="11.4"><w n="44.1">En</w> <w n="44.2">faire</w> <w n="44.3">un</w> <w n="44.4">monstre</w> : <w n="44.5">elle</w> <w n="44.6">est</w> <w n="44.7">charmante</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Son</w> <w n="45.2">visage</w> <w n="45.3">a</w> <w n="45.4">l</w>’<w n="45.5">éclat</w> <w n="45.6">du</w> <w n="45.7">jour</w></l>
					<l n="46" num="12.2"><w n="46.1">Par</w> <w n="46.2">qui</w> <w n="46.3">tout</w> <w n="46.4">se</w> <w n="46.5">métamorphose</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">le</w> <w n="47.3">souffle</w> <w n="47.4">pur</w> <w n="47.5">de</w> <w n="47.6">l</w>’<w n="47.7">Amour</w></l>
					<l n="48" num="12.4"><w n="48.1">Vient</w> <w n="48.2">fleurir</w> <w n="48.3">ses</w> <w n="48.4">lèvres</w> <w n="48.5">de</w> <w n="48.6">rose</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Par</w> <w n="49.2">leurs</w> <w n="49.3">mystérieux</w> <w n="49.4">accords</w></l>
					<l n="50" num="13.2"><w n="50.1">De</w> <w n="50.2">fruit</w> <w n="50.3">vermeil</w> <w n="50.4">où</w> <w n="50.5">l</w>’<w n="50.6">on</w> <w n="50.7">va</w> <w n="50.8">mordre</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Les</w> <w n="51.2">chairs</w> <w n="51.3">saines</w> <w n="51.4">de</w> <w n="51.5">son</w> <w n="51.6">beau</w> <w n="51.7">corps</w></l>
					<l n="52" num="13.4"><w n="52.1">Nous</w> <w n="52.2">affirment</w> <w n="52.3">le</w> <w n="52.4">rhythme</w> <w n="52.5">et</w> <w n="52.6">l</w>’<w n="52.7">ordre</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Lorsque</w> <w n="53.2">je</w> <w n="53.3">veux</w> <w n="53.4">l</w>’<w n="53.5">exciter</w> <w n="53.6">par</w></l>
					<l n="54" num="14.2"><w n="54.1">La</w> <w n="54.2">saveur</w> <w n="54.3">des</w> <w n="54.4">piments</w> <w n="54.5">étranges</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Son</w> <w n="55.2">âme</w> <w n="55.3">se</w> <w n="55.4">révolte</w>, <w n="55.5">car</w></l>
					<l n="56" num="14.4"><w n="56.1">Elle</w> <w n="56.2">est</w> <w n="56.3">fière</w> <w n="56.4">comme</w> <w n="56.5">les</w> <w n="56.6">Anges</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Même</w>, <w n="57.2">son</w> <w n="57.3">regard</w> <w n="57.4">enchanté</w></l>
					<l n="58" num="15.2"><w n="58.1">Ravit</w> <w n="58.2">les</w> <w n="58.3">astres</w> <w n="58.4">et</w> <w n="58.5">les</w> <w n="58.6">sphères</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Car</w> <w n="59.2">elle</w> <w n="59.3">est</w> <w n="59.4">bonne</w> ; <w n="59.5">et</w> <w n="59.6">sa</w> <w n="59.7">bonté</w></l>
					<l n="60" num="15.4"><w n="60.1">Ne</w> <w n="60.2">fait</w> <w n="60.3">plus</w> <w n="60.4">du</w> <w n="60.5">tout</w> <w n="60.6">mes</w> <w n="60.7">affaires</w></l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Elle</w> <w n="61.2">est</w> <w n="61.3">chaste</w> <w n="61.4">encor</w>, <w n="61.5">dans</w> <w n="61.6">l</w>’<w n="61.7">émoi</w></l>
					<l n="62" num="16.2"><w n="62.1">Où</w> <w n="62.2">l</w>’<w n="62.3">a</w> <w n="62.4">jetée</w> <w n="62.5">un</w> <w n="62.6">mot</w> <w n="62.7">trop</w> <w n="62.8">leste</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Et</w> <w n="63.2">conserve</w>, <w n="63.3">en</w> <w n="63.4">dépit</w> <w n="63.5">de</w> <w n="63.6">moi</w>,</l>
					<l n="64" num="16.4"><w n="64.1">Une</w> <w n="64.2">ingénuité</w> <w n="64.3">céleste</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1890"> 18 mars 1890.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>