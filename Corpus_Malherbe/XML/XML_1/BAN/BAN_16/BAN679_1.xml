<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SONNAILLES ET CLOCHETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3290 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_16</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SONNAILLES ET CLOCHETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1997">1997</date>
								</imprint>
								<biblScope unit="tome">VII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Sonnailles et clochettes</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>G. Charpentier et Cie, Éditeurs</publisher>
						<date when="1890">1890</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1888">1888</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN679">
				<head type="number">XLVI</head>
				<head type="main">La Nuit</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ne</w> <w n="1.2">parle</w> <w n="1.3">pas</w>, <w n="1.4">trop</w> <w n="1.5">parler</w> <w n="1.6">nuit</w> :</l>
					<l n="2" num="1.2"><w n="2.1">Bon</w> <w n="2.2">passant</w>, <w n="2.3">prends</w> <w n="2.4">garde</w> <w n="2.5">à</w> <w n="2.6">ta</w> <w n="2.7">montre</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">elle</w> <w n="3.3">est</w> <w n="3.4">superbe</w>, <w n="3.5">la</w> <w n="3.6">Nuit</w></l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">se</w> <w n="4.3">dérobe</w> <w n="4.4">et</w> <w n="4.5">qui</w> <w n="4.6">se</w> <w n="4.7">montre</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Quand</w> <w n="5.2">cette</w> <w n="5.3">reine</w> <w n="5.4">aux</w> <w n="5.5">doigts</w> <w n="5.6">fleuris</w></l>
					<l n="6" num="2.2"><w n="6.1">Vient</w> <w n="6.2">pour</w> <w n="6.3">charmer</w> <w n="6.4">les</w> <w n="6.5">demoiselles</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Oh</w> ! <w n="7.2">sur</w> <w n="7.3">le</w> <w n="7.4">fabuleux</w> <w n="7.5">Paris</w></l>
					<l n="8" num="2.4"><w n="8.1">Comme</w> <w n="8.2">elle</w> <w n="8.3">étend</w> <w n="8.4">de</w> <w n="8.5">grandes</w> <w n="8.6">ailes</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Vite</w>, <w n="9.2">elle</w> <w n="9.3">a</w> <w n="9.4">fermé</w> <w n="9.5">son</w> <w n="9.6">rideau</w></l>
					<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">cacher</w> <w n="10.3">la</w> <w n="10.4">clarté</w> <w n="10.5">vermeille</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">chante</w> : <w n="11.3">Dodo</w>, <w n="11.4">l</w>’<w n="11.5">enfant</w> <w n="11.6">do</w></l>
					<l n="12" num="3.4"><w n="12.1">A</w> <w n="12.2">l</w>’<w n="12.3">homme</w> <w n="12.4">éreinté</w> <w n="12.5">qui</w> <w n="12.6">sommeille</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">l</w>’<w n="13.3">immense</w> <w n="13.4">ville</w> <w n="13.5">apparaît</w></l>
					<l n="14" num="4.2"><w n="14.1">Avec</w> <w n="14.2">ses</w> <w n="14.3">effrayants</w> <w n="14.4">colosses</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Pareille</w> <w n="15.2">à</w> <w n="15.3">la</w> <w n="15.4">noire</w> <w n="15.5">forêt</w></l>
					<l n="16" num="4.4"><w n="16.1">Que</w> <w n="16.2">peuplent</w> <w n="16.3">les</w> <w n="16.4">bêtes</w> <w n="16.5">féroces</w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">La</w> <w n="17.2">verte</w> <w n="17.3">Seine</w>, <w n="17.4">dont</w> <w n="17.5">le</w> <w n="17.6">flot</w></l>
					<l n="18" num="5.2"><w n="18.1">Brille</w> <w n="18.2">comme</w> <w n="18.3">une</w> <w n="18.4">pertuisane</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Dans</w> <w n="19.2">son</w> <w n="19.3">lit</w>, <w n="19.4">avec</w> <w n="19.5">un</w> <w n="19.6">sanglot</w></l>
					<l n="20" num="5.4"><w n="20.1">S</w>’<w n="20.2">étire</w>, <w n="20.3">et</w> <w n="20.4">fait</w> <w n="20.5">la</w> <w n="20.6">courtisane</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">le</w> <w n="21.3">grand</w> <w n="21.4">Paris</w>, <w n="21.5">traversé</w></l>
					<l n="22" num="6.2"><w n="22.1">Tout</w> <w n="22.2">entier</w> <w n="22.3">par</w> <w n="22.4">ses</w> <w n="22.5">chansons</w> <w n="22.6">vagues</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Se</w> <w n="23.2">réjouit</w> <w n="23.3">d</w>’<w n="23.4">être</w> <w n="23.5">bercé</w></l>
					<l n="24" num="6.4"><w n="24.1">Dans</w> <w n="24.2">le</w> <w n="24.3">murmure</w> <w n="24.4">de</w> <w n="24.5">ses</w> <w n="24.6">vagues</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Les</w> <w n="25.2">amants</w> <w n="25.3">dont</w> <w n="25.4">le</w> <w n="25.5">coup</w> <w n="25.6">d</w>’<w n="25.7">essai</w></l>
					<l n="26" num="7.2"><w n="26.1">De</w> <w n="26.2">nul</w> <w n="26.3">chef</w>-<w n="26.4">d</w>’<w n="26.5">œuvre</w> <w n="26.6">ne</w> <w n="26.7">diffère</w>,</l>
					<l n="27" num="7.3"><w n="27.1">S</w>’<w n="27.2">embrassent</w>, <w n="27.3">et</w> <w n="27.4">comme</w> <w n="27.5">Sarcey</w></l>
					<l n="28" num="7.4"><w n="28.1">L</w>’<w n="28.2">ordonne</w>, <w n="28.3">font</w> <w n="28.4">la</w> <w n="28.5">scène</w> <w n="28.6">à</w> <w n="28.7">faire</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Le</w> <w n="29.2">rimeur</w>, <w n="29.3">en</w> <w n="29.4">son</w> <w n="29.5">rêve</w> <w n="29.6">bleu</w></l>
					<l n="30" num="8.2"><w n="30.1">Que</w> <w n="30.2">nul</w> <w n="30.3">ukase</w> <w n="30.4">ne</w> <w n="30.5">supprime</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Baise</w> <w n="31.2">l</w>’<w n="31.3">ardent</w> <w n="31.4">charbon</w> <w n="31.5">de</w> <w n="31.6">feu</w></l>
					<l n="32" num="8.4"><w n="32.1">Sur</w> <w n="32.2">les</w> <w n="32.3">deux</w> <w n="32.4">lèvres</w> <w n="32.5">de</w> <w n="32.6">la</w> <w n="32.7">Rime</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Ouvrant</w>, <w n="33.2">avec</w> <w n="33.3">un</w> <w n="33.4">geste</w> <w n="33.5">sec</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Leurs</w> <w n="34.2">grands</w> <w n="34.3">coffres</w>-<w n="34.4">forts</w> <w n="34.5">à</w> <w n="34.6">fonds</w> <w n="34.7">doubles</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Comme</w> <w n="35.2">Gigonnet</w> <w n="35.3">et</w> <w n="35.4">Gobseck</w></l>
					<l n="36" num="9.4"><w n="36.1">Les</w> <w n="36.2">éditeurs</w> <w n="36.3">comptent</w> <w n="36.4">des</w> <w n="36.5">roubles</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">En</w> <w n="37.2">prononçant</w> <w n="37.3">de</w> <w n="37.4">vagues</w> <w n="37.5">mots</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Des</w> <w n="38.2">filles</w>, <w n="38.3">seules</w> <w n="38.4">ou</w> <w n="38.5">par</w> <w n="38.6">groupes</w></l>
					<l n="39" num="10.3"><w n="39.1">Vont</w> <w n="39.2">et</w>, <w n="39.3">comme</w> <w n="39.4">les</w> <w n="39.5">animaux</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Font</w> <w n="40.2">saillir</w> <w n="40.3">de</w> <w n="40.4">lascives</w> <w n="40.5">croupes</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Les</w> <w n="41.2">souteneurs</w> <w n="41.3">et</w> <w n="41.4">les</w> <w n="41.5">filous</w></l>
					<l n="42" num="11.2"><w n="42.1">Tentent</w> <w n="42.2">de</w> <w n="42.3">rafler</w> <w n="42.4">des</w> <w n="42.5">sacoches</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Et</w> <w n="43.2">rôdant</w>, <w n="43.3">pareils</w> <w n="43.4">à</w> <w n="43.5">des</w> <w n="43.6">loups</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Touchent</w> <w n="44.2">leurs</w> <w n="44.3">couteaux</w> <w n="44.4">dans</w> <w n="44.5">leurs</w> <w n="44.6">poches</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Ce</w> <w n="45.2">sont</w> <w n="45.3">eux</w>, <w n="45.4">les</w> <w n="45.5">marchands</w> <w n="45.6">d</w>’<w n="45.7">amour</w>.</l>
					<l n="46" num="12.2"><w n="46.1">Avec</w> <w n="46.2">sa</w> <w n="46.3">prunelle</w> <w n="46.4">hagarde</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Au</w> <w n="47.2">coin</w> <w n="47.3">du</w> <w n="47.4">sombre</w> <w n="47.5">carrefour</w></l>
					<l n="48" num="12.4"><w n="48.1">La</w> <w n="48.2">blanche</w> <w n="48.3">Lune</w> <w n="48.4">les</w> <w n="48.5">regarde</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Pâle</w>, <w n="49.2">des</w> <w n="49.3">nappes</w> <w n="49.4">de</w> <w n="49.5">l</w>’<w n="49.6">azur</w></l>
					<l n="50" num="13.2"><w n="50.1">A</w> <w n="50.2">sa</w> <w n="50.3">lumière</w> <w n="50.4">habituées</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Elle</w> <w n="51.2">jette</w> <w n="51.3">ses</w> <w n="51.4">rayons</w> <w n="51.5">sur</w></l>
					<l n="52" num="13.4"><w n="52.1">Les</w> <w n="52.2">petites</w> <w n="52.3">prostituées</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Effroyablement</w>, <w n="53.2">par</w> <w n="53.3">milliers</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Volent</w> <w n="54.2">dans</w> <w n="54.3">le</w> <w n="54.4">gouffre</w> <w n="54.5">des</w> <w n="54.6">nues</w></l>
					<l n="55" num="14.3"><w n="55.1">Des</w> <w n="55.2">Pégases</w>, <w n="55.3">des</w> <w n="55.4">cavaliers</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Des</w> <w n="56.2">monstres</w> <w n="56.3">et</w> <w n="56.4">des</w> <w n="56.5">femmes</w> <w n="56.6">nues</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Et</w> <w n="57.2">dans</w> <w n="57.3">l</w>’<w n="57.4">immensité</w> <w n="57.5">des</w> <w n="57.6">cieux</w></l>
					<l n="58" num="15.2"><w n="58.1">On</w> <w n="58.2">voit</w> <w n="58.3">au</w>-<w n="58.4">dessus</w> <w n="58.5">de</w> <w n="58.6">nos</w> <w n="58.7">fanges</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Comme</w> <w n="59.2">un</w> <w n="59.3">long</w> <w n="59.4">chœur</w> <w n="59.5">silencieux</w></l>
					<l n="60" num="15.4"><w n="60.1">Errer</w> <w n="60.2">les</w> <w n="60.3">figures</w> <w n="60.4">des</w> <w n="60.5">Anges</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1889"> 15 octobre 1889.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>