<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN599">
				<head type="main">A la Jeunesse</head>
				<head type="sub_1">Prologue pour « La Vie de Bohème » au Théâtre de l’Odéon</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Mesdames</w> <w n="1.2">et</w> <w n="1.3">messieurs</w>, <w n="1.4">nous</w> <w n="1.5">vous</w> <w n="1.6">donnons</w> <w n="1.7">La</w> <w n="1.8">Vie</w></l>
					<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">Bohème</w>, <w n="2.3">une</w> <w n="2.4">pièce</w> <w n="2.5">où</w> <w n="2.6">le</w> <w n="2.7">rire</w> <w n="2.8">et</w> <w n="2.9">les</w> <w n="2.10">pleurs</w></l>
					<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">mêlent</w>, <w n="3.3">comme</w> <w n="3.4">aux</w> <w n="3.5">champs</w>, <w n="3.6">où</w> <w n="3.7">notre</w> <w n="3.8">âme</w> <w n="3.9">est</w> <w n="3.10">ravie</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">larmes</w> <w n="4.3">du</w> <w n="4.4">matin</w> <w n="4.5">brillent</w> <w n="4.6">parmi</w> <w n="4.7">les</w> <w n="4.8">fleurs</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pour</w> <w n="5.2">dire</w> <w n="5.3">ce</w> <w n="5.4">refrain</w> <w n="5.5">des</w> <w n="5.6">amours</w> <w n="5.7">éternelles</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Deux</w> <w n="6.2">amis</w>, <w n="6.3">ô</w> <w n="6.4">douleur</w> ! <w n="6.5">séparés</w> <w n="6.6">aujourd</w>’<w n="6.7">hui</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Naguères</w> <w n="7.2">unissaient</w> <w n="7.3">leurs</w> <w n="7.4">deux</w> <w n="7.5">voix</w> <w n="7.6">fraternelles</w> :</l>
					<l n="8" num="2.4"><w n="8.1">Puisque</w> <w n="8.2">l</w>’<w n="8.3">un</w> <w n="8.4">d</w>’<w n="8.5">eux</w> <w n="8.6">s</w>’<w n="8.7">est</w> <w n="8.8">tû</w>, <w n="8.9">ne</w> <w n="8.10">parlons</w> <w n="8.11">que</w> <w n="8.12">de</w> <w n="8.13">lui</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Murger</w>, <w n="9.2">esprit</w> <w n="9.3">ailé</w>, <w n="9.4">poëte</w> <w n="9.5">ivre</w> <w n="9.6">d</w>’<w n="9.7">aurore</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">Muse</w> <w n="10.3">eut</w> <w n="10.4">cette</w> <w n="10.5">sœur</w> <w n="10.6">divine</w> <w n="10.7">du</w> <w n="10.8">Printemps</w>,</l>
					<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">Jeunesse</w>, <w n="11.3">pour</w> <w n="11.4">qui</w> <w n="11.5">les</w> <w n="11.6">roses</w> <w n="11.7">vont</w> <w n="11.8">éclore</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">pour</w> <w n="12.3">devise</w> <w n="12.4">il</w> <w n="12.5">eut</w> <w n="12.6">ces</w> <w n="12.7">mots</w> <w n="12.8">sacrés</w> : <w n="12.9">Vingt</w> <w n="12.10">ans</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">pourquoi</w>, <w n="13.4">tout</w> <w n="13.5">heureux</w> <w n="13.6">de</w> <w n="13.7">se</w> <w n="13.8">regarder</w> <w n="13.9">vivre</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Toujours</w> <w n="14.2">les</w> <w n="14.3">jeunes</w> <w n="14.4">cœurs</w> <w n="14.5">de</w> <w n="14.6">vingt</w> <w n="14.7">ans</w> <w n="14.8">aimeront</w></l>
					<l n="15" num="4.3"><w n="15.1">Ces</w> <w n="15.2">filles</w> <w n="15.3">du</w> <w n="15.4">matin</w> <w n="15.5">qui</w> <w n="15.6">passent</w> <w n="15.7">dans</w> <w n="15.8">son</w> <w n="15.9">livre</w></l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">meurent</w> <w n="16.3">sans</w> <w n="16.4">avoir</w> <w n="16.5">de</w> <w n="16.6">rides</w> <w n="16.7">sur</w> <w n="16.8">leur</w> <w n="16.9">front</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Qui</w> <w n="17.2">ne</w> <w n="17.3">les</w> <w n="17.4">adora</w>, <w n="17.5">ces</w> <w n="17.6">fleurs</w> <w n="17.7">de</w> <w n="17.8">son</w> <w n="17.9">poëme</w> ?</l>
					<l n="18" num="5.2"><w n="18.1">Qui</w> <w n="18.2">de</w> <w n="18.3">nous</w>, <w n="18.4">qui</w> <w n="18.5">de</w> <w n="18.6">nous</w>, <w n="18.7">ô</w> <w n="18.8">rêveuse</w> <w n="18.9">Mimi</w></l>
					<l n="19" num="5.3"><w n="19.1">Enamourée</w> <w n="19.2">encor</w> <w n="19.3">sous</w> <w n="19.4">le</w> <w n="19.5">frisson</w> <w n="19.6">suprême</w>,</l>
					<l n="20" num="5.4"><w n="20.1">N</w>’<w n="20.2">a</w> <w n="20.3">dans</w> <w n="20.4">un</w> <w n="20.5">rêve</w> <w n="20.6">ardent</w> <w n="20.7">baisé</w> <w n="20.8">ton</w> <w n="20.9">front</w> <w n="20.10">blêmi</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">toi</w>, <w n="21.3">Musette</w>, <w n="21.4">reine</w> <w n="21.5">insoucieuse</w> <w n="21.6">et</w> <w n="21.7">folle</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Qui</w> <w n="22.2">n</w>’<w n="22.3">a</w> <w n="22.4">cherché</w> <w n="22.5">tes</w> <w n="22.6">yeux</w>, <w n="22.7">qui</w> <w n="22.8">n</w>’<w n="22.9">a</w> <w n="22.10">redit</w> <w n="22.11">ton</w> <w n="22.12">nom</w> ?</l>
					<l n="23" num="6.3"><w n="23.1">Qui</w> <w n="23.2">sur</w> <w n="23.3">ta</w> <w n="23.4">lèvre</w> <w n="23.5">ouverte</w> <w n="23.6">au</w> <w n="23.7">vent</w>, <w n="23.8">rose</w> <w n="23.9">corolle</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Ne</w> <w n="24.2">retrouve</w> <w n="24.3">à</w> <w n="24.4">la</w> <w n="24.5">fois</w> <w n="24.6">Juliette</w> <w n="24.7">et</w> <w n="24.8">Manon</w> ?</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Oui</w>, <w n="25.2">tant</w> <w n="25.3">qu</w>’<w n="25.4">un</w> <w n="25.5">vin</w> <w n="25.6">pourpré</w> <w n="25.7">frémira</w> <w n="25.8">dans</w> <w n="25.9">nos</w> <w n="25.10">verres</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Ces</w> <w n="26.2">fillettes</w> <w n="26.3">vivront</w>, <w n="26.4">couple</w> <w n="26.5">frais</w> <w n="26.6">et</w> <w n="26.7">vermeil</w>.</l>
					<l n="27" num="7.3"><w n="27.1">Pourquoi</w> ? <w n="27.2">c</w>’<w n="27.3">est</w> <w n="27.4">qu</w>’<w n="27.5">elles</w> <w n="27.6">ont</w> <w n="27.7">l</w>’<w n="27.8">âge</w> <w n="27.9">des</w> <w n="27.10">primevères</w></l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">l</w>’<w n="28.3">actualité</w> <w n="28.4">du</w> <w n="28.5">rayon</w> <w n="28.6">de</w> <w n="28.7">soleil</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Le</w> <w n="29.2">livre</w> <w n="29.3">un</w> <w n="29.4">soir</w> <w n="29.5">devint</w> <w n="29.6">une</w> <w n="29.7">pièce</w> <w n="29.8">applaudie</w></l>
					<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">même</w> <w n="30.3">fit</w> <w n="30.4">fureur</w> <w n="30.5">autant</w> <w n="30.6">qu</w>’<w n="30.7">un</w> <w n="30.8">opéra</w>.</l>
					<l n="31" num="8.3"><w n="31.1">Le</w> <w n="31.2">miracle</w> <w n="31.3">nouveau</w> <w n="31.4">de</w> <w n="31.5">cette</w> <w n="31.6">comédie</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Ce</w> <w n="32.2">fut</w> <w n="32.3">qu</w>’<w n="32.4">en</w> <w n="32.5">l</w>’<w n="32.6">entendant</w> <w n="32.7">l</w>’<w n="32.8">on</w> <w n="32.9">rit</w> <w n="32.10">et</w> <w n="32.11">l</w>’<w n="32.12">on</w> <w n="32.13">pleura</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">On</w> <w n="33.2">s</w>’<w n="33.3">étonnait</w> <w n="33.4">surtout</w> <w n="33.5">qu</w>’<w n="33.6">en</w> <w n="33.7">des</w> <w n="33.8">scènes</w> <w n="33.9">rapides</w></l>
					<l n="34" num="9.2"><w n="34.1">L</w>’<w n="34.2">esprit</w>, <w n="34.3">versant</w> <w n="34.4">la</w> <w n="34.5">joie</w> <w n="34.6">et</w> <w n="34.7">l</w>’<w n="34.8">éblouissement</w></l>
					<l n="35" num="9.3"><w n="35.1">Avec</w> <w n="35.2">son</w> <w n="35.3">carillon</w> <w n="35.4">de</w> <w n="35.5">notes</w> <w n="35.6">d</w>’<w n="35.7">or</w> <w n="35.8">splendides</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Pût</w> <w n="36.2">laisser</w> <w n="36.3">tant</w> <w n="36.4">de</w> <w n="36.5">place</w> <w n="36.6">à</w> <w n="36.7">l</w>’<w n="36.8">attendrissement</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Puis</w> <w n="37.2">l</w>’<w n="37.3">œuvre</w>, <w n="37.4">que</w> <w n="37.5">le</w> <w n="37.6">temps</w> <w n="37.7">jaloux</w> <w n="37.8">n</w>’<w n="37.9">a</w> <w n="37.10">pas</w> <w n="37.11">meurtrie</w>,</l>
					<l n="38" num="10.2"><w n="38.1">De</w> <w n="38.2">théâtre</w> <w n="38.3">en</w> <w n="38.4">théâtre</w> <w n="38.5">a</w> <w n="38.6">suivi</w> <w n="38.7">son</w> <w n="38.8">destin</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Mais</w> <w n="39.2">elle</w> <w n="39.3">trouve</w> <w n="39.4">enfin</w> <w n="39.5">sa</w> <w n="39.6">réelle</w> <w n="39.7">patrie</w></l>
					<l n="40" num="10.4"><w n="40.1">En</w> <w n="40.2">abordant</w> <w n="40.3">ce</w> <w n="40.4">soir</w> <w n="40.5">au</w> <w n="40.6">vieux</w> <w n="40.7">Pays</w> <w n="40.8">Latin</w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">O</w> <w n="41.2">vous</w> <w n="41.3">en</w> <w n="41.4">qui</w> <w n="41.5">sourit</w> <w n="41.6">l</w>’<w n="41.7">avenir</w> <w n="41.8">de</w> <w n="41.9">la</w> <w n="41.10">France</w> !</l>
					<l n="42" num="11.2"><w n="42.1">O</w> <w n="42.2">jeunes</w> <w n="42.3">gens</w>, <w n="42.4">Murger</w> <w n="42.5">calme</w>, <w n="42.6">vaillant</w> <w n="42.7">et</w> <w n="42.8">doux</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Nous</w> <w n="43.2">versait</w> <w n="43.3">en</w> <w n="43.4">pleurant</w> <w n="43.5">le</w> <w n="43.6">vin</w> <w n="43.7">de</w> <w n="43.8">l</w>’<w n="43.9">espérance</w> :</l>
					<l n="44" num="11.4"><w n="44.1">Où</w> <w n="44.2">serait</w>-<w n="44.3">il</w> <w n="44.4">compris</w> <w n="44.5">si</w> <w n="44.6">ce</w> <w n="44.7">n</w>’<w n="44.8">est</w> <w n="44.9">parmi</w> <w n="44.10">vous</w> ?</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Il</w> <w n="45.2">fut</w> <w n="45.3">des</w> <w n="45.4">vôtres</w>, <w n="45.5">car</w> <w n="45.6">il</w> <w n="45.7">eut</w> <w n="45.8">le</w> <w n="45.9">fier</w> <w n="45.10">délire</w></l>
					<l n="46" num="12.2"><w n="46.1">Du</w> <w n="46.2">noble</w> <w n="46.3">dévouement</w> <w n="46.4">et</w> <w n="46.5">des</w> <w n="46.6">belles</w> <w n="46.7">chansons</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">je</w> <w n="47.3">devine</w> <w n="47.4">bien</w> <w n="47.5">que</w> <w n="47.6">vous</w> <w n="47.7">allez</w> <w n="47.8">lui</w> <w n="47.9">dire</w> :</l>
					<l n="48" num="12.4"><w n="48.1">Reste</w> <w n="48.2">avec</w> <w n="48.3">nous</w>. <w n="48.4">C</w>’<w n="48.5">est</w> <w n="48.6">bien</w>. <w n="48.7">Nous</w> <w n="48.8">te</w> <w n="48.9">reconnaissons</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Il</w> <w n="49.2">fut</w> <w n="49.3">de</w> <w n="49.4">votre</w> <w n="49.5">race</w>, <w n="49.6">ô</w> <w n="49.7">nation</w> <w n="49.8">choisie</w> !</l>
					<l n="50" num="13.2"><w n="50.1">Il</w> <w n="50.2">se</w> <w n="50.3">donnait</w> <w n="50.4">à</w> <w n="50.5">vous</w> <w n="50.6">qui</w>, <w n="50.7">malgré</w> <w n="50.8">les</w> <w n="50.9">moqueurs</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Ne</w> <w n="51.2">déserterez</w> <w n="51.3">pas</w> <w n="51.4">la</w> <w n="51.5">sainte</w> <w n="51.6">Poésie</w>,</l>
					<l n="52" num="13.4"><w n="52.1">Et</w> <w n="52.2">dont</w> <w n="52.3">la</w> <w n="52.4">soif</w> <w n="52.5">de</w> <w n="52.6">l</w>’<w n="52.7">or</w> <w n="52.8">n</w>’<w n="52.9">a</w> <w n="52.10">pas</w> <w n="52.11">séché</w> <w n="52.12">les</w> <w n="52.13">cœurs</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Comme</w> <w n="53.2">sa</w> <w n="53.3">comédie</w> <w n="53.4">où</w>, <w n="53.5">voilé</w> <w n="53.6">de</w> <w n="53.7">tristesse</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Murmure</w> <w n="54.2">sous</w> <w n="54.3">les</w> <w n="54.4">cieux</w> <w n="54.5">le</w> <w n="54.6">rire</w> <w n="54.7">aérien</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Est</w> <w n="55.2">à</w> <w n="55.3">vous</w>, <w n="55.4">bataillon</w> <w n="55.5">sacré</w> <w n="55.6">de</w> <w n="55.7">la</w> <w n="55.8">jeunesse</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Nous</w> <w n="56.2">vous</w> <w n="56.3">la</w> <w n="56.4">rapportons</w>. <w n="56.5">Reprenez</w> <w n="56.6">votre</w> <w n="56.7">bien</w> !</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Le</w> <w n="57.2">poëte</w> <w n="57.3">pensif</w> <w n="57.4">qui</w> <w n="57.5">vous</w> <w n="57.6">donna</w> <w n="57.7">La</w> <w n="57.8">Vie</w></l>
					<l n="58" num="15.2"><w n="58.1">De</w> <w n="58.2">Bohème</w>, <w n="58.3">adora</w> <w n="58.4">dans</w> <w n="58.5">ses</w> <w n="58.6">rêves</w> <w n="58.7">d</w>’<w n="58.8">azur</w></l>
					<l n="59" num="15.3"><w n="59.1">La</w> <w n="59.2">gloire</w>, <w n="59.3">cette</w> <w n="59.4">amante</w> <w n="59.5">ardemment</w> <w n="59.6">poursuivie</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Et</w> <w n="60.2">toujours</w> <w n="60.3">se</w> <w n="60.4">garda</w> <w n="60.5">pour</w> <w n="60.6">elle</w> <w n="60.7">honnête</w> <w n="60.8">et</w> <w n="60.9">pur</w>.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Ses</w> <w n="61.2">héros</w> <w n="61.3">sont</w> <w n="61.4">parfois</w> <w n="61.5">mal</w> <w n="61.6">avec</w> <w n="61.7">la</w> <w n="61.8">fortune</w> :</l>
					<l n="62" num="16.2"><w n="62.1">Vous</w> <w n="62.2">les</w> <w n="62.3">voyez</w> <w n="62.4">soupant</w> <w n="62.5">au</w> <w n="62.6">milieu</w> <w n="62.7">des</w> <w n="62.8">hivers</w></l>
					<l n="63" num="16.3"><w n="63.1">D</w>’<w n="63.2">un</w> <w n="63.3">sonnet</w> <w n="63.4">romantique</w> <w n="63.5">ou</w> <w n="63.6">bien</w> <w n="63.7">d</w>’<w n="63.8">un</w> <w n="63.9">clair</w> <w n="63.10">de</w> <w n="63.11">lune</w>,</l>
					<l n="64" num="16.4"><w n="64.1">Mais</w> <w n="64.2">fidèles</w>, <w n="64.3">mais</w> <w n="64.4">vrais</w>, <w n="64.5">mais</w> <w n="64.6">indomptés</w>, <w n="64.7">mais</w> <w n="64.8">fiers</w> !</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Leurs</w> <w n="65.2">châteaux</w> <w n="65.3">éclatants</w>, <w n="65.4">faits</w> <w n="65.5">d</w>’<w n="65.6">un</w> <w n="65.7">rêve</w> <w n="65.8">féerique</w>,</l>
					<l n="66" num="17.2"><w n="66.1">N</w>’<w n="66.2">ont</w> <w n="66.3">encore</w> <w n="66.4">été</w> <w n="66.5">vus</w> <w n="66.6">par</w> <w n="66.7">nul</w> <w n="66.8">historien</w>,</l>
					<l n="67" num="17.3"><w n="67.1">Et</w> <w n="67.2">sont</w> <w n="67.3">bâtis</w> <w n="67.4">dans</w> <w n="67.5">une</w> <w n="67.6">Espagne</w> <w n="67.7">chimérique</w>,</l>
					<l n="68" num="17.4"><w n="68.1">Mais</w> <w n="68.2">enferment</w> <w n="68.3">l</w>’<w n="68.4">honneur</w>, <w n="68.5">sans</w> <w n="68.6">lequel</w> <w n="68.7">tout</w> <w n="68.8">n</w>’<w n="68.9">est</w> <w n="68.10">rien</w>.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Vous</w> <w n="69.2">recevrez</w> <w n="69.3">chez</w> <w n="69.4">vous</w> <w n="69.5">ces</w> <w n="69.6">hôtes</w> <w n="69.7">en</w> <w n="69.8">liesse</w>,</l>
					<l n="70" num="18.2"><w n="70.1">Comme</w> <w n="70.2">des</w> <w n="70.3">voyageurs</w> <w n="70.4">qui</w> <w n="70.5">parlent</w> <w n="70.6">d</w>’<w n="70.7">un</w> <w n="70.8">ami</w>.</l>
					<l n="71" num="18.3"><w n="71.1">Oui</w>, <w n="71.2">vous</w> <w n="71.3">applaudirez</w> <w n="71.4">et</w> <w n="71.5">l</w>’<w n="71.6">esprit</w> <w n="71.7">de</w> <w n="71.8">la</w> <w n="71.9">pièce</w></l>
					<l n="72" num="18.4"><w n="72.1">Et</w> <w n="72.2">votre</w> <w n="72.3">doux</w> <w n="72.4">Murger</w>, <w n="72.5">à</w> <w n="72.6">présent</w> <w n="72.7">endormi</w> !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Et</w> <w n="73.2">vos</w> <w n="73.3">regrets</w> <w n="73.4">amers</w> <w n="73.5">pour</w> <w n="73.6">ce</w> <w n="73.7">jeune</w> <w n="73.8">poëte</w></l>
					<l n="74" num="19.2"><w n="74.1">Emporté</w> <w n="74.2">loin</w> <w n="74.3">de</w> <w n="74.4">nous</w> <w n="74.5">par</w> <w n="74.6">un</w> <w n="74.7">vent</w> <w n="74.8">meurtrier</w></l>
					<l n="75" num="19.3"><w n="75.1">A</w> <w n="75.2">sa</w> <w n="75.3">lyre</w> <w n="75.4">à</w> <w n="75.5">présent</w> <w n="75.6">détendue</w> <w n="75.7">et</w> <w n="75.8">muette</w></l>
					<l n="76" num="19.4"><w n="76.1">Ne</w> <w n="76.2">refuseront</w> <w n="76.3">pas</w> <w n="76.4">quelques</w> <w n="76.5">brins</w> <w n="76.6">de</w> <w n="76.7">laurier</w> !</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">Car</w> <w n="77.2">vous</w> <w n="77.3">êtes</w> <w n="77.4">de</w> <w n="77.5">ceux</w> <w n="77.6">dont</w> <w n="77.7">la</w> <w n="77.8">pitié</w> <w n="77.9">profonde</w></l>
					<l n="78" num="20.2"><w n="78.1">Garde</w> <w n="78.2">les</w> <w n="78.3">verts</w> <w n="78.4">rameaux</w> <w n="78.5">qui</w> <w n="78.6">croissent</w> <w n="78.7">sous</w> <w n="78.8">le</w> <w n="78.9">ciel</w></l>
					<l n="79" num="20.3"><w n="79.1">Pour</w> <w n="79.2">les</w> <w n="79.3">penseurs</w> <w n="79.4">trop</w> <w n="79.5">vite</w> <w n="79.6">exilés</w> <w n="79.7">de</w> <w n="79.8">ce</w> <w n="79.9">monde</w></l>
					<l n="80" num="20.4"><w n="80.1">Et</w> <w n="80.2">pour</w> <w n="80.3">ce</w> <w n="80.4">que</w> <w n="80.5">les</w> <w n="80.6">morts</w> <w n="80.7">nous</w> <w n="80.8">laissent</w> <w n="80.9">d</w>’<w n="80.10">immortels</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1865"> 30 décembre 1865.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>