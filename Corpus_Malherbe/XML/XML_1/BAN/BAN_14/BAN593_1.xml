<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN593">
				<head type="main">L’Échafaud</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Horreur</w> ! <w n="1.2">à</w> <w n="1.3">l</w>’<w n="1.4">heure</w> <w n="1.5">même</w> <w n="1.6">où</w>, <w n="1.7">du</w> <w n="1.8">poteau</w> <w n="1.9">qui</w> <w n="1.10">bouge</w></l>
					<l n="2" num="1.2"><w n="2.1">Rajustant</w> <w n="2.2">les</w> <w n="2.3">étais</w> <w n="2.4">avec</w> <w n="2.5">un</w> <w n="2.6">soin</w> <w n="2.7">jaloux</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ces</w> <w n="3.2">êtres</w>, <w n="3.3">dans</w> <w n="3.4">le</w> <w n="3.5">bruit</w> <w n="3.6">des</w> <w n="3.7">marteaux</w> <w n="3.8">et</w> <w n="3.9">des</w> <w n="3.10">clous</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Dressent</w> <w n="4.2">sinistrement</w> <w n="4.3">cette</w> <w n="4.4">machine</w> <w n="4.5">rouge</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">A</w> <w n="5.2">l</w>’<w n="5.3">heure</w> <w n="5.4">où</w> <w n="5.5">de</w> <w n="5.6">Charonne</w> <w n="5.7">et</w> <w n="5.8">du</w> <w n="5.9">Petit</w>-<w n="5.10">Montrouge</w></l>
					<l n="6" num="2.2"><w n="6.1">Viennent</w> <w n="6.2">ces</w> <w n="6.3">curieux</w>, <w n="6.4">bohèmes</w> <w n="6.5">et</w> <w n="6.6">filous</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">se</w> <w n="7.3">repaissent</w>, <w n="7.4">plus</w> <w n="7.5">féroces</w> <w n="7.6">que</w> <w n="7.7">des</w> <w n="7.8">loups</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Du</w> <w n="8.2">festin</w> <w n="8.3">qu</w>’<w n="8.4">a</w> <w n="8.5">voulu</w> <w n="8.6">l</w>’<w n="8.7">insatiable</w> <w n="8.8">gouge</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">A</w> <w n="9.2">l</w>’<w n="9.3">heure</w> <w n="9.4">où</w>, <w n="9.5">devançant</w> <w n="9.6">le</w> <w n="9.7">matin</w> <w n="9.8">hasardeux</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Ils</w> <w n="10.2">se</w> <w n="10.3">sont</w> <w n="10.4">réunis</w> <w n="10.5">pour</w> <w n="10.6">ce</w> <w n="10.7">complot</w> <w n="10.8">hideux</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Des</w> <w n="11.2">mères</w>, <w n="11.3">sous</w> <w n="11.4">les</w> <w n="11.5">yeux</w> <w n="11.6">de</w> <w n="11.7">cette</w> <w n="11.8">même</w> <w n="11.9">aurore</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Mettent</w> <w n="12.2">dans</w> <w n="12.3">cette</w> <w n="12.4">vie</w>, <w n="12.5">hélas</w> ! <w n="12.6">pleine</w> <w n="12.7">de</w> <w n="12.8">fiel</w>,</l>
					<l n="13" num="4.2"><w n="13.1">De</w> <w n="13.2">beaux</w> <w n="13.3">petits</w> <w n="13.4">enfants</w> <w n="13.5">sur</w> <w n="13.6">lesquels</w> <w n="13.7">brille</w> <w n="13.8">encore</w></l>
					<l n="14" num="4.3"><w n="14.1">La</w> <w n="14.2">majesté</w> <w n="14.3">de</w> <w n="14.4">l</w>’<w n="14.5">Ange</w> <w n="14.6">et</w> <w n="14.7">le</w> <w n="14.8">reflet</w> <w n="14.9">du</w> <w n="14.10">ciel</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Juin 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>