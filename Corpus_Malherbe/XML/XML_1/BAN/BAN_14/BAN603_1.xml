<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN603">
				<head type="main">L’Apothéose de Ronsard</head>
				<head type="sub_1">Prince des Poëtes français</head>
				<opener>
					<salute>A Prosper Blanchemain <lb></lb>le pieux éditeur de Ronsard</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">O</w> <w n="1.2">mon</w> <w n="1.3">Ronsard</w>, <w n="1.4">ô</w> <w n="1.5">maître</w></l>
					<l n="2" num="1.2"><w n="2.1">Victorieux</w> <w n="2.2">du</w> <w n="2.3">mètre</w>,</l>
					<l n="3" num="1.3"><w n="3.1">O</w> <w n="3.2">sublime</w> <w n="3.3">échanson</w></l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">De</w> <w n="4.2">la</w> <w n="4.3">chanson</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Divin</w> <w n="5.2">porteur</w> <w n="5.3">de</w> <w n="5.4">lyre</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Que</w> <w n="6.2">voulurent</w> <w n="6.3">élire</w></l>
					<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">goûter</w> <w n="7.3">leurs</w> <w n="7.4">douceurs</w></l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space><w n="8.1">Les</w> <w n="8.2">chastes</w> <w n="8.3">Sœurs</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Toi</w> <w n="9.2">qui</w>, <w n="9.3">nouveau</w> <w n="9.4">Pindare</w>,</l>
					<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">l</w>’<w n="10.3">art</w> <w n="10.4">savant</w> <w n="10.5">et</w> <w n="10.6">rare</w></l>
					<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">Phœbos</w> <w n="11.3">Cynthien</w></l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space><w n="12.1">Faisant</w> <w n="12.2">le</w> <w n="12.3">tien</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">A</w> <w n="13.2">l</w>’<w n="13.3">ivresse</w> <w n="13.4">physique</w></l>
					<l n="14" num="4.2"><w n="14.1">De</w> <w n="14.2">ta</w> <w n="14.3">folle</w> <w n="14.4">musique</w></l>
					<l n="15" num="4.3"><w n="15.1">Sagement</w> <w n="15.2">as</w> <w n="15.3">mêlé</w></l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space><w n="16.1">Le</w> <w n="16.2">rhythme</w> <w n="16.3">ailé</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Père</w> ! <w n="17.2">que</w> <w n="17.3">ma</w> <w n="17.4">louange</w></l>
					<l n="18" num="5.2"><w n="18.1">Te</w> <w n="18.2">célèbre</w> <w n="18.3">et</w> <w n="18.4">te</w> <w n="18.5">venge</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Et</w>, <w n="19.2">comme</w> <w n="19.3">vers</w> <w n="19.4">mon</w> <w n="19.5">Roi</w>,</l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space><w n="20.1">Monte</w> <w n="20.2">vers</w> <w n="20.3">toi</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">que</w> <w n="21.3">dis</w>-<w n="21.4">je</w> ? <w n="21.5">l</w>’<w n="21.6">Envie</w></l>
					<l n="22" num="6.2"><w n="22.1">Qui</w> <w n="22.2">déchira</w> <w n="22.3">ta</w> <w n="22.4">vie</w></l>
					<l n="23" num="6.3"><w n="23.1">Ne</w> <w n="23.2">mord</w> <w n="23.3">plus</w> <w n="23.4">de</w> <w n="23.5">bon</w> <w n="23.6">cœur</w></l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space><w n="24.1">Ton</w> <w n="24.2">pied</w> <w n="24.3">vainqueur</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w>, <w n="25.2">nette</w> <w n="25.3">de</w> <w n="25.4">souillure</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Ta</w> <w n="26.2">belle</w> <w n="26.3">gloire</w> <w n="26.4">pure</w></l>
					<l n="27" num="7.3"><w n="27.1">Va</w> <w n="27.2">d</w>’<w n="27.3">un</w> <w n="27.4">nouvel</w> <w n="27.5">essor</w></l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space><w n="28.1">Aux</w> <w n="28.2">astres</w> <w n="28.3">d</w>’<w n="28.4">or</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Ton</w> <w n="29.2">nom</w> <w n="29.3">deux</w> <w n="29.4">fois</w> <w n="29.5">illustre</w></l>
					<l n="30" num="8.2"><w n="30.1">A</w> <w n="30.2">retrouvé</w> <w n="30.3">son</w> <w n="30.4">lustre</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Comme</w> <w n="31.2">il</w> <w n="31.3">l</w>’<w n="31.4">avait</w> <w n="31.5">jadis</w></l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space><w n="32.1">Au</w> <w n="32.2">temps</w> <w n="32.3">des</w> <w n="32.4">lys</w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">toi</w>, <w n="33.3">dans</w> <w n="33.4">l</w>’<w n="33.5">aube</w> <w n="33.6">rose</w></l>
					<l n="34" num="9.2"><w n="34.1">De</w> <w n="34.2">ton</w> <w n="34.3">apothéose</w></l>
					<l n="35" num="9.3"><w n="35.1">Tu</w> <w n="35.2">marches</w>, <w n="35.3">l</w>’<w n="35.4">œil</w> <w n="35.5">en</w> <w n="35.6">feu</w>,</l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space><w n="36.1">Ainsi</w> <w n="36.2">qu</w>’<w n="36.3">un</w> <w n="36.4">Dieu</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Tenant</w> <w n="37.2">ton</w> <w n="37.3">luth</w> <w n="37.4">d</w>’<w n="37.5">ivoire</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Près</w> <w n="38.2">d</w>’<w n="38.3">une</w> <w n="38.4">douce</w> <w n="38.5">Loire</w></l>
					<l n="39" num="10.3"><w n="39.1">A</w> <w n="39.2">la</w> <w n="39.3">berceuse</w> <w n="39.4">voix</w>,</l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space><w n="40.1">Je</w> <w n="40.2">te</w> <w n="40.3">revois</w></l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Dans</w> <w n="41.2">un</w> <w n="41.3">jardin</w> <w n="41.4">féerique</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Où</w> <w n="42.2">le</w> <w n="42.3">troupeau</w> <w n="42.4">lyrique</w></l>
					<l n="43" num="11.3"><w n="43.1">Enchante</w> <w n="43.2">de</w> <w n="43.3">tes</w> <w n="43.4">vers</w></l>
					<l n="44" num="11.4"><space quantity="4" unit="char"></space><w n="44.1">Les</w> <w n="44.2">bosquets</w> <w n="44.3">verts</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Là</w>, <w n="45.2">Du</w> <w n="45.3">Bellay</w> <w n="45.4">t</w>’<w n="45.5">honore</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Et</w> <w n="46.2">je</w> <w n="46.3">retrouve</w> <w n="46.4">encore</w></l>
					<l n="47" num="12.3"><w n="47.1">Près</w> <w n="47.2">de</w> <w n="47.3">cette</w> <w n="47.4">belle</w> <w n="47.5">eau</w></l>
					<l n="48" num="12.4"><space quantity="4" unit="char"></space><w n="48.1">Remy</w> <w n="48.2">Belleau</w></l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">Pontus</w> <w n="49.3">et</w> <w n="49.4">Jodelle</w></l>
					<l n="50" num="13.2"><w n="50.1">Et</w> <w n="50.2">Dorat</w>, <w n="50.3">ton</w> <w n="50.4">fidèle</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Et</w> <w n="51.2">ce</w> <w n="51.3">chanteur</w> <w n="51.4">naïf</w>,</l>
					<l n="52" num="13.4"><space quantity="4" unit="char"></space><w n="52.1">Le</w> <w n="52.2">vieux</w> <w n="52.3">Baïf</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Avec</w> <w n="53.2">eux</w>, <w n="53.3">ces</w> <w n="53.4">Déesses</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Les</w> <w n="54.2">hautaines</w> <w n="54.3">Princesses</w></l>
					<l n="55" num="14.3"><w n="55.1">Du</w> <w n="55.2">sang</w> <w n="55.3">pur</w> <w n="55.4">des</w> <w n="55.5">Valois</w>,</l>
					<l n="56" num="14.4"><space quantity="4" unit="char"></space><w n="56.1">Suivent</w> <w n="56.2">tes</w> <w n="56.3">lois</w></l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Et</w> <w n="57.2">servent</w> <w n="57.3">ton</w> <w n="57.4">Hélène</w></l>
					<l n="58" num="15.2"><w n="58.1">A</w> <w n="58.2">la</w> <w n="58.3">suave</w> <w n="58.4">haleine</w>,</l>
					<l n="59" num="15.3"><w n="59.1">De</w> <w n="59.2">qui</w> <w n="59.3">la</w> <w n="59.4">lèvre</w> <w n="59.5">leur</w></l>
					<l n="60" num="15.4"><space quantity="4" unit="char"></space><w n="60.1">Semble</w> <w n="60.2">une</w> <w n="60.3">fleur</w>,</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Et</w> <w n="61.2">Cassandre</w>, <w n="61.3">et</w> <w n="61.4">Marie</w></l>
					<l n="62" num="16.2"><w n="62.1">Qui</w>, <w n="62.2">rêveuse</w>, <w n="62.3">marie</w></l>
					<l n="63" num="16.3"><w n="63.1">La</w> <w n="63.2">rose</w> <w n="63.3">dans</w> <w n="63.4">sa</w> <w n="63.5">main</w></l>
					<l n="64" num="16.4"><space quantity="4" unit="char"></space><w n="64.1">Au</w> <w n="64.2">blanc</w> <w n="64.3">jasmin</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Mais</w> <w n="65.2">Vénus</w> <w n="65.3">parmi</w> <w n="65.4">l</w>’<w n="65.5">herbe</w></l>
					<l n="66" num="17.2"><w n="66.1">Est</w> <w n="66.2">aussi</w> <w n="66.3">là</w>, <w n="66.4">superbe</w> ;</l>
					<l n="67" num="17.3"><w n="67.1">Les</w> <w n="67.2">fleurs</w>, <w n="67.3">pour</w> <w n="67.4">la</w> <w n="67.5">parer</w>,</l>
					<l n="68" num="17.4"><space quantity="4" unit="char"></space><w n="68.1">Laissent</w> <w n="68.2">errer</w></l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Leurs</w> <w n="69.2">ombres</w> <w n="69.3">sur</w> <w n="69.4">sa</w> <w n="69.5">joue</w> ;</l>
					<l n="70" num="18.2"><w n="70.1">Quelquefois</w> <w n="70.2">elle</w> <w n="70.3">joue</w></l>
					<l n="71" num="18.3"><w n="71.1">Avec</w> <w n="71.2">l</w>’<w n="71.3">arc</w> <w n="71.4">triomphant</w></l>
					<l n="72" num="18.4"><space quantity="4" unit="char"></space><w n="72.1">De</w> <w n="72.2">son</w> <w n="72.3">enfant</w>.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Et</w> <w n="73.2">les</w> <w n="73.3">saintes</w> <w n="73.4">pucelles</w>,</l>
					<l n="74" num="19.2"><w n="74.1">Qui</w> <w n="74.2">mêlent</w> <w n="74.3">d</w>’<w n="74.4">étincelles</w></l>
					<l n="75" num="19.3"><w n="75.1">Et</w> <w n="75.2">de</w> <w n="75.3">feux</w> <w n="75.4">adorés</w></l>
					<l n="76" num="19.4"><space quantity="4" unit="char"></space><w n="76.1">Leurs</w> <w n="76.2">crins</w> <w n="76.3">dorés</w>,</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">Levant</w> <w n="77.2">leurs</w> <w n="77.3">bras</w> <w n="77.4">d</w>’<w n="77.5">albâtre</w>,</l>
					<l n="78" num="20.2"><w n="78.1">Vous</w> <w n="78.2">suivent</w>, <w n="78.3">chœur</w> <w n="78.4">folâtre</w></l>
					<l n="79" num="20.3"><w n="79.1">De</w> <w n="79.2">votre</w> <w n="79.3">voix</w> <w n="79.4">épris</w>,</l>
					<l n="80" num="20.4"><space quantity="4" unit="char"></space><w n="80.1">Dans</w> <w n="80.2">ces</w> <w n="80.3">pourpris</w>.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1"><w n="81.1">Mais</w> <w n="81.2">voici</w> <w n="81.3">que</w> <w n="81.4">tu</w> <w n="81.5">chantes</w> !</l>
					<l n="82" num="21.2"><w n="82.1">Et</w> <w n="82.2">tes</w> <w n="82.3">strophes</w> <w n="82.4">touchantes</w></l>
					<l n="83" num="21.3"><w n="83.1">Déroulent</w> <w n="83.2">leurs</w> <w n="83.3">accords</w></l>
					<l n="84" num="21.4"><space quantity="4" unit="char"></space><w n="84.1">Divins</w> ; <w n="84.2">alors</w>,</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1"><w n="85.1">Ronsard</w>, <w n="85.2">tout</w> <w n="85.3">fait</w> <w n="85.4">silence</w> :</l>
					<l n="86" num="22.2"><w n="86.1">La</w> <w n="86.2">fleur</w> <w n="86.3">qui</w> <w n="86.4">se</w> <w n="86.5">balance</w>,</l>
					<l n="87" num="22.3"><w n="87.1">Le</w> <w n="87.2">ruisseau</w> <w n="87.3">clair</w>, <w n="87.4">l</w>’<w n="87.5">oiseau</w></l>
					<l n="88" num="22.4"><space quantity="4" unit="char"></space><w n="88.1">Et</w> <w n="88.2">le</w> <w n="88.3">roseau</w> ;</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1"><w n="89.1">Le</w> <w n="89.2">Fleuve</w> <w n="89.3">à</w> <w n="89.4">la</w> <w n="89.5">voix</w> <w n="89.6">rauque</w>,</l>
					<l n="90" num="23.2"><w n="90.1">Montrant</w> <w n="90.2">sa</w> <w n="90.3">barbe</w> <w n="90.4">glauque</w>,</l>
					<l n="91" num="23.3"><w n="91.1">Fait</w> <w n="91.2">taire</w> <w n="91.3">les</w> <w n="91.4">sanglots</w></l>
					<l n="92" num="23.4"><space quantity="4" unit="char"></space><w n="92.1">De</w> <w n="92.2">ses</w> <w n="92.3">grands</w> <w n="92.4">flots</w> ;</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1"><w n="93.1">Dans</w> <w n="93.2">les</w> <w n="93.3">cieux</w> <w n="93.4">qui</w> <w n="93.5">te</w> <w n="93.6">fêtent</w></l>
					<l n="94" num="24.2"><w n="94.1">Les</w> <w n="94.2">étoiles</w> <w n="94.3">s</w>’<w n="94.4">arrêtent</w></l>
					<l n="95" num="24.3"><w n="95.1">Et</w> <w n="95.2">suspendent</w> <w n="95.3">les</w> <w n="95.4">airs</w></l>
					<l n="96" num="24.4"><space quantity="4" unit="char"></space><w n="96.1">De</w> <w n="96.2">leurs</w> <w n="96.3">concerts</w> ;</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1"><w n="97.1">On</w> <w n="97.2">n</w>’<w n="97.3">entend</w> <w n="97.4">que</w> <w n="97.5">ton</w> <w n="97.6">Ode</w>,</l>
					<l n="98" num="25.2"><w n="98.1">Qu</w>’<w n="98.2">après</w> <w n="98.3">toi</w>, <w n="98.4">dans</w> <w n="98.5">le</w> <w n="98.6">mode</w></l>
					<l n="99" num="25.3"><w n="99.1">Ancien</w>, <w n="99.2">le</w> <w n="99.3">chœur</w> <w n="99.4">ravi</w></l>
					<l n="100" num="25.4"><space quantity="4" unit="char"></space><w n="100.1">Chante</w> <w n="100.2">à</w> <w n="100.3">l</w>’<w n="100.4">envi</w>.</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1"><w n="101.1">Et</w> <w n="101.2">chacun</w> <w n="101.3">s</w>’<w n="101.4">en</w> <w n="101.5">récrée</w>,</l>
					<l n="102" num="26.2"><w n="102.1">Hélène</w>, <w n="102.2">Cythérée</w>,</l>
					<l n="103" num="26.3"><w n="103.1">Déesses</w> <w n="103.2">de</w> <w n="103.3">la</w> <w n="103.4">cour</w>,</l>
					<l n="104" num="26.4"><space quantity="4" unit="char"></space><w n="104.1">Enfant</w> <w n="104.2">Amour</w>,</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1"><w n="105.1">Muses</w> <w n="105.2">aux</w> <w n="105.3">belles</w> <w n="105.4">bouches</w> ;</l>
					<l n="106" num="27.2"><w n="106.1">Et</w> <w n="106.2">les</w> <w n="106.3">astres</w> <w n="106.4">farouches</w></l>
					<l n="107" num="27.3"><w n="107.1">Restent</w> <w n="107.2">silencieux</w></l>
					<l n="108" num="27.4"><space quantity="4" unit="char"></space><w n="108.1">Au</w> <w n="108.2">front</w> <w n="108.3">des</w> <w n="108.4">cieux</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Avril 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>