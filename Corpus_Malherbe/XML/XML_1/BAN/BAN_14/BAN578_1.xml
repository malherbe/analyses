<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN578">
				<head type="main">L’Aube romantique</head>
				<opener>
					<salute>A Charles Asselineau</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Mil</w> <w n="1.2">huit</w> <w n="1.3">cent</w> <w n="1.4">trente</w> ! <w n="1.5">Aurore</w></l>
					<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">m</w>’<w n="2.3">éblouis</w> <w n="2.4">encore</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Promesse</w> <w n="3.2">du</w> <w n="3.3">destin</w>,</l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">Riant</w> <w n="4.2">matin</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Aube</w> <w n="5.2">où</w> <w n="5.3">le</w> <w n="5.4">soleil</w> <w n="5.5">plonge</w> !</l>
					<l n="6" num="2.2"><w n="6.1">Quelquefois</w> <w n="6.2">un</w> <w n="6.3">beau</w> <w n="6.4">songe</w></l>
					<l n="7" num="2.3"><w n="7.1">Me</w> <w n="7.2">rend</w> <w n="7.3">l</w>’<w n="7.4">éclat</w> <w n="7.5">vermeil</w></l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space><w n="8.1">De</w> <w n="8.2">ton</w> <w n="8.3">réveil</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Jetant</w> <w n="9.2">ta</w> <w n="9.3">pourpre</w> <w n="9.4">rose</w></l>
					<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">notre</w> <w n="10.3">ciel</w> <w n="10.4">morose</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Tu</w> <w n="11.2">parais</w>, <w n="11.3">et</w> <w n="11.4">la</w> <w n="11.5">nuit</w></l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space><w n="12.1">Soudain</w> <w n="12.2">s</w>’<w n="12.3">enfuit</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">nymphe</w> <w n="13.3">Poésie</w></l>
					<l n="14" num="4.2"><w n="14.1">Aux</w> <w n="14.2">cheveux</w> <w n="14.3">d</w>’<w n="14.4">ambroisie</w></l>
					<l n="15" num="4.3"><w n="15.1">Avec</w> <w n="15.2">son</w> <w n="15.3">art</w> <w n="15.4">subtil</w></l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space><w n="16.1">Revient</w> <w n="16.2">d</w>’<w n="16.3">exil</w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L</w>’<w n="17.2">Ode</w> <w n="17.3">chante</w>, <w n="17.4">le</w> <w n="17.5">Drame</w></l>
					<l n="18" num="5.2"><w n="18.1">Ourdit</w> <w n="18.2">sa</w> <w n="18.3">riche</w> <w n="18.4">trame</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">L</w>’<w n="19.2">harmonieux</w> <w n="19.3">Sonnet</w></l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space><w n="20.1">Déjà</w> <w n="20.2">renaît</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Ici</w> <w n="21.2">rugit</w> <w n="21.3">Shakspere</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Là</w> <w n="22.2">Pétrarque</w> <w n="22.3">soupire</w> ;</l>
					<l n="23" num="6.3"><w n="23.1">Horace</w> <w n="23.2">bon</w> <w n="23.3">garçon</w></l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space><w n="24.1">Dit</w> <w n="24.2">sa</w> <w n="24.3">chanson</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">Ronsard</w> <w n="25.3">son</w> <w n="25.4">poëme</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Et</w> <w n="26.2">l</w>’<w n="26.3">on</w> <w n="26.4">retrouve</w> <w n="26.5">même</w></l>
					<l n="27" num="7.3"><w n="27.1">L</w>’<w n="27.2">art</w> <w n="27.3">farouche</w> <w n="27.4">et</w> <w n="27.5">naïf</w></l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space><w n="28.1">Du</w> <w n="28.2">vieux</w> <w n="28.3">Baïf</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Tout</w> <w n="29.2">joyeux</w>, <w n="29.3">du</w> <w n="29.4">Cocyte</w></l>
					<l n="30" num="8.2"><w n="30.1">Rabelais</w> <w n="30.2">ressuscite</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Pour</w> <w n="31.2">donner</w> <w n="31.3">au</w> <w n="31.4">roman</w></l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space><w n="32.1">Un</w> <w n="32.2">talisman</w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">l</w>’<w n="33.3">amoureuse</w> <w n="33.4">fièvre</w></l>
					<l n="34" num="9.2"><w n="34.1">Qui</w> <w n="34.2">rougit</w> <w n="34.3">notre</w> <w n="34.4">lèvre</w></l>
					<l n="35" num="9.3"><w n="35.1">Défend</w> <w n="35.2">même</w> <w n="35.3">au</w> <w n="35.4">journal</w></l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space><w n="36.1">D</w>’<w n="36.2">être</w> <w n="36.3">banal</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">La</w> <w n="37.2">grande</w> <w n="37.3">Architecture</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Prière</w> <w n="38.2">sainte</w> <w n="38.3">et</w> <w n="38.4">pure</w></l>
					<l n="39" num="10.3"><w n="39.1">De</w> <w n="39.2">l</w>’<w n="39.3">art</w> <w n="39.4">matériel</w>,</l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space><w n="40.1">Regarde</w> <w n="40.2">au</w> <w n="40.3">ciel</w> ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">La</w> <w n="41.2">Sculpture</w> <w n="41.3">modèle</w></l>
					<l n="42" num="11.2"><w n="42.1">Des</w> <w n="42.2">saints</w> <w n="42.3">au</w> <w n="42.4">cœur</w> <w n="42.5">fidèle</w></l>
					<l n="43" num="11.3"><w n="43.1">Pareils</w> <w n="43.2">aux</w> <w n="43.3">lys</w> <w n="43.4">vêtus</w></l>
					<l n="44" num="11.4"><space quantity="4" unit="char"></space><w n="44.1">De</w> <w n="44.2">leurs</w> <w n="44.3">vertus</w>,</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Et</w> <w n="45.2">la</w> <w n="45.3">Musique</w> <w n="45.4">emporte</w></l>
					<l n="46" num="12.2"><w n="46.1">Notre</w> <w n="46.2">âme</w> <w n="46.3">par</w> <w n="46.4">la</w> <w n="46.5">porte</w></l>
					<l n="47" num="12.3"><w n="47.1">Des</w> <w n="47.2">chants</w> <w n="47.3">délicieux</w></l>
					<l n="48" num="12.4"><space quantity="4" unit="char"></space><w n="48.1">Au</w> <w n="48.2">fond</w> <w n="48.3">des</w> <w n="48.4">cieux</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">O</w> <w n="49.2">grand</w> <w n="49.3">combat</w> <w n="49.4">sublime</w></l>
					<l n="50" num="13.2"><w n="50.1">Du</w> <w n="50.2">Luth</w> <w n="50.3">et</w> <w n="50.4">de</w> <w n="50.5">la</w> <w n="50.6">Rime</w> !</l>
					<l n="51" num="13.3"><w n="51.1">Renouveau</w> <w n="51.2">triomphal</w></l>
					<l n="52" num="13.4"><space quantity="4" unit="char"></space><w n="52.1">De</w> <w n="52.2">l</w>’<w n="52.3">Idéal</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Hugo</w>, <w n="53.2">sombre</w>, <w n="53.3">dédie</w></l>
					<l n="54" num="14.2"><w n="54.1">Sa</w> <w n="54.2">morne</w> <w n="54.3">tragédie</w></l>
					<l n="55" num="14.3"><w n="55.1">Aux</w> <w n="55.2">grands</w> <w n="55.3">cœurs</w> <w n="55.4">désolés</w>,</l>
					<l n="56" num="14.4"><space quantity="4" unit="char"></space><w n="56.1">Aux</w> <w n="56.2">exilés</w>,</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">A</w> <w n="57.2">la</w> <w n="57.3">souffrance</w>, <w n="57.4">au</w> <w n="57.5">rêve</w>.</l>
					<l n="58" num="15.2"><w n="58.1">Il</w> <w n="58.2">embrasse</w>, <w n="58.3">il</w> <w n="58.4">relève</w></l>
					<l n="59" num="15.3"><w n="59.1">Et</w> <w n="59.2">Marion</w>, <w n="59.3">hélas</w> !</l>
					<l n="60" num="15.4"><space quantity="4" unit="char"></space><w n="60.1">Et</w> <w n="60.2">toi</w>, <w n="60.3">Ruy</w> <w n="60.4">Blas</w>.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Et</w> <w n="61.2">déjà</w>, <w n="61.3">comme</w> <w n="61.4">exemple</w>,</l>
					<l n="62" num="16.2"><w n="62.1">David</w>, <w n="62.2">qui</w> <w n="62.3">le</w> <w n="62.4">contemple</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Met</w> <w n="63.2">sur</w> <w n="63.3">son</w> <w n="63.4">front</w> <w n="63.5">guerrier</w></l>
					<l n="64" num="16.4"><space quantity="4" unit="char"></space><w n="64.1">Le</w> <w n="64.2">noir</w> <w n="64.3">laurier</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">George</w> <w n="65.2">Sand</w> <w n="65.3">en</w> <w n="65.4">son</w> <w n="65.5">âme</w></l>
					<l n="66" num="17.2"><w n="66.1">Porte</w> <w n="66.2">un</w> <w n="66.3">éclair</w> <w n="66.4">de</w> <w n="66.5">flamme</w> ;</l>
					<l n="67" num="17.3"><w n="67.1">Musset</w>, <w n="67.2">beau</w> <w n="67.3">cygne</w> <w n="67.4">errant</w>,</l>
					<l n="68" num="17.4"><space quantity="4" unit="char"></space><w n="68.1">Chante</w> <w n="68.2">en</w> <w n="68.3">pleurant</w> ;</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Balzac</w>, <w n="69.2">superbe</w>, <w n="69.3">mène</w></l>
					<l n="70" num="18.2"><w n="70.1">La</w> <w n="70.2">Comédie</w> <w n="70.3">Humaine</w></l>
					<l n="71" num="18.3"><w n="71.1">Et</w> <w n="71.2">nous</w> <w n="71.3">fait</w> <w n="71.4">voir</w> <w n="71.5">à</w> <w n="71.6">nu</w></l>
					<l n="72" num="18.4"><space quantity="4" unit="char"></space><w n="72.1">L</w>’<w n="72.2">homme</w> <w n="72.3">ingénu</w> ;</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Pour</w> <w n="73.2">le</w> <w n="73.3">luth</w> <w n="73.4">Sainte</w>-<w n="73.5">Beuve</w></l>
					<l n="74" num="19.2"><w n="74.1">Trouve</w> <w n="74.2">une</w> <w n="74.3">corde</w> <w n="74.4">neuve</w> ;</l>
					<l n="75" num="19.3"><w n="75.1">Barbier</w> <w n="75.2">lance</w> <w n="75.3">en</w> <w n="75.4">grondant</w></l>
					<l n="76" num="19.4"><space quantity="4" unit="char"></space><w n="76.1">L</w>’<w n="76.2">Iambe</w> <w n="76.3">ardent</w> ;</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">La</w> <w n="77.2">plainte</w> <w n="77.3">de</w> <w n="77.4">Valmore</w></l>
					<l n="78" num="20.2"><w n="78.1">Pleure</w> <w n="78.2">et</w> <w n="78.3">s</w>’<w n="78.4">exhale</w> <w n="78.5">encore</w></l>
					<l n="79" num="20.3"><w n="79.1">En</w> <w n="79.2">sanglots</w> <w n="79.3">plus</w> <w n="79.4">amers</w></l>
					<l n="80" num="20.4"><space quantity="4" unit="char"></space><w n="80.1">Que</w> <w n="80.2">ceux</w> <w n="80.3">des</w> <w n="80.4">mers</w>,</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1"><w n="81.1">Et</w>, <w n="81.2">sur</w> <w n="81.3">un</w> <w n="81.4">mont</w> <w n="81.5">sauvage</w>,</l>
					<l n="82" num="21.2"><w n="82.1">L</w>’<w n="82.2">Art</w> <w n="82.3">jaloux</w> <w n="82.4">donne</w> <w n="82.5">au</w> <w n="82.6">sage</w></l>
					<l n="83" num="21.3"><w n="83.1">Théophile</w> <w n="83.2">Gautier</w></l>
					<l n="84" num="21.4"><space quantity="4" unit="char"></space><w n="84.1">Le</w> <w n="84.2">monde</w> <w n="84.3">entier</w>.</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1"><w n="85.1">En</w> <w n="85.2">ces</w> <w n="85.3">beaux</w> <w n="85.4">jours</w> <w n="85.5">de</w> <w n="85.6">jeûne</w>,</l>
					<l n="86" num="22.2"><w n="86.1">Karr</w> <w n="86.2">a</w> <w n="86.3">plus</w> <w n="86.4">d</w>’<w n="86.5">amour</w> <w n="86.6">jeune</w></l>
					<l n="87" num="22.3"><w n="87.1">Qu</w>’<w n="87.2">un</w> <w n="87.3">vieux</w> <w n="87.4">Rothschild</w> <w n="87.5">pensif</w></l>
					<l n="88" num="22.4"><space quantity="4" unit="char"></space><w n="88.1">N</w>’<w n="88.2">a</w> <w n="88.3">d</w>’<w n="88.4">or</w> <w n="88.5">massif</w> ;</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1"><w n="89.1">De</w> <w n="89.2">sa</w> <w n="89.3">voix</w> <w n="89.4">attendrie</w></l>
					<l n="90" num="23.2"><w n="90.1">Gérard</w> <w n="90.2">dit</w> <w n="90.3">la</w> <w n="90.4">féerie</w></l>
					<l n="91" num="23.3"><w n="91.1">Et</w> <w n="91.2">le</w> <w n="91.3">songe</w> <w n="91.4">riant</w></l>
					<l n="92" num="23.4"><space quantity="4" unit="char"></space><w n="92.1">De</w> <w n="92.2">l</w>’<w n="92.3">Orient</w> ;</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1"><w n="93.1">Les</w> <w n="93.2">Deschamps</w>, <w n="93.3">voix</w> <w n="93.4">jumelles</w>,</l>
					<l n="94" num="24.2"><w n="94.1">Chantent</w> : <w n="94.2">l</w>’<w n="94.3">un</w> <w n="94.4">a</w> <w n="94.5">des</w> <w n="94.6">ailes</w>,</l>
					<l n="95" num="24.3"><w n="95.1">L</w>’<w n="95.2">autre</w> <w n="95.3">parle</w> <w n="95.4">à</w> <w n="95.5">l</w>’<w n="95.6">écho</w></l>
					<l n="96" num="24.4"><space quantity="4" unit="char"></space><w n="96.1">De</w> <w n="96.2">Roméo</w>.</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1"><w n="97.1">Frédérick</w> <w n="97.2">ploie</w> <w n="97.3">et</w> <w n="97.4">mène</w></l>
					<l n="98" num="25.2"><w n="98.1">En</w> <w n="98.2">tyran</w> <w n="98.3">Melpomène</w>,</l>
					<l n="99" num="25.3"><w n="99.1">Et</w> <w n="99.2">la</w> <w n="99.3">grande</w> <w n="99.4">Dorval</w></l>
					<l n="100" num="25.4"><space quantity="4" unit="char"></space><w n="100.1">L</w>’<w n="100.2">a</w> <w n="100.3">pour</w> <w n="100.4">rival</w> ;</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1"><w n="101.1">Berlioz</w>, <w n="101.2">qui</w> <w n="101.3">nous</w> <w n="101.4">étonne</w>,</l>
					<l n="102" num="26.2"><w n="102.1">Avec</w> <w n="102.2">l</w>’<w n="102.3">orage</w> <w n="102.4">tonne</w>,</l>
					<l n="103" num="26.3"><w n="103.1">Et</w> <w n="103.2">parle</w> <w n="103.3">dans</w> <w n="103.4">l</w>’<w n="103.5">éclair</w></l>
					<l n="104" num="26.4"><space quantity="4" unit="char"></space><w n="104.1">A</w> <w n="104.2">Meyerbeer</w> ;</l>
				</lg>
				<lg n="27">
					<l n="105" num="27.1"><w n="105.1">Préault</w>, <w n="105.2">d</w>’<w n="105.3">un</w> <w n="105.4">doigt</w> <w n="105.5">fantasque</w>,</l>
					<l n="106" num="27.2"><w n="106.1">Fait</w> <w n="106.2">trembler</w> <w n="106.3">sur</w> <w n="106.4">un</w> <w n="106.5">masque</w></l>
					<l n="107" num="27.3"><w n="107.1">L</w>’<w n="107.2">immortelle</w> <w n="107.3">pâleur</w></l>
					<l n="108" num="27.4"><space quantity="4" unit="char"></space><w n="108.1">De</w> <w n="108.2">la</w> <w n="108.3">Douleur</w>,</l>
				</lg>
				<lg n="28">
					<l n="109" num="28.1"><w n="109.1">Tandis</w> <w n="109.2">qu</w>’<w n="109.3">à</w> <w n="109.4">chaque</w> <w n="109.5">livre</w></l>
					<l n="110" num="28.2"><w n="110.1">Johannot</w>, <w n="110.2">d</w>’<w n="110.3">amour</w> <w n="110.4">ivre</w>,</l>
					<l n="111" num="28.3"><w n="111.1">Prête</w> <w n="111.2">un</w> <w n="111.3">rêve</w> <w n="111.4">nouveau</w></l>
					<l n="112" num="28.4"><space quantity="4" unit="char"></space><w n="112.1">De</w> <w n="112.2">son</w> <w n="112.3">cerveau</w>.</l>
				</lg>
				<lg n="29">
					<l n="113" num="29.1"><w n="113.1">Pour</w> <w n="113.2">Boulanger</w> <w n="113.3">qui</w> <w n="113.4">l</w>’<w n="113.5">aime</w>,</l>
					<l n="114" num="29.2"><w n="114.1">Facile</w>, <w n="114.2">et</w> <w n="114.3">venant</w> <w n="114.4">même</w></l>
					<l n="115" num="29.3"><w n="115.1">Baiser</w> <w n="115.2">au</w> <w n="115.3">front</w> <w n="115.4">Nanteuil</w></l>
					<l n="116" num="29.4"><space quantity="4" unit="char"></space><w n="116.1">Dans</w> <w n="116.2">son</w> <w n="116.3">fauteuil</w>,</l>
				</lg>
				<lg n="30">
					<l n="117" num="30.1"><w n="117.1">La</w> <w n="117.2">Peinture</w> <w n="117.3">en</w> <w n="117.4">extase</w></l>
					<l n="118" num="30.2"><w n="118.1">Donne</w> <w n="118.2">la</w> <w n="118.3">chrysoprase</w></l>
					<l n="119" num="30.3"><w n="119.1">Et</w> <w n="119.2">le</w> <w n="119.3">rubis</w> <w n="119.4">des</w> <w n="119.5">rois</w></l>
					<l n="120" num="30.4"><space quantity="4" unit="char"></space><w n="120.1">A</w> <w n="120.2">Delacroix</w>.</l>
				</lg>
				<lg n="31">
					<l n="121" num="31.1"><w n="121.1">Daumier</w> <w n="121.2">trouve</w> <w n="121.3">l</w>’<w n="121.4">étrange</w></l>
					<l n="122" num="31.2"><w n="122.1">Crayon</w> <w n="122.2">de</w> <w n="122.3">Michel</w>-<w n="122.4">Ange</w>,</l>
					<l n="123" num="31.3"><w n="123.1">Noble</w> <w n="123.2">vol</w> <w n="123.3">impuni</w> !</l>
					<l n="124" num="31.4"><space quantity="4" unit="char"></space><w n="124.1">Et</w> <w n="124.2">Garvani</w></l>
				</lg>
				<lg n="32">
					<l n="125" num="32.1"><w n="125.1">Court</w>, <w n="125.2">sans</w> <w n="125.3">qu</w>’<w n="125.4">on</w> <w n="125.5">le</w> <w n="125.6">dépasse</w>,</l>
					<l n="126" num="32.2"><w n="126.1">Vers</w> <w n="126.2">l</w>’<w n="126.3">amoureuse</w> <w n="126.4">Grâce</w></l>
					<l n="127" num="32.3"><w n="127.1">Qu</w>’<w n="127.2">à</w> <w n="127.3">l</w>’<w n="127.4">Esprit</w> <w n="127.5">maria</w></l>
					<l n="128" num="32.4"><space quantity="4" unit="char"></space><w n="128.1">Devéria</w> !</l>
				</lg>
				<lg n="33">
					<l n="129" num="33.1"><w n="129.1">Mais</w>, <w n="129.2">hélas</w> ! <w n="129.3">où</w> <w n="129.4">m</w>’<w n="129.5">emporte</w></l>
					<l n="130" num="33.2"><w n="130.1">Le</w> <w n="130.2">songe</w> ! <w n="130.3">Elle</w> <w n="130.4">est</w> <w n="130.5">bien</w> <w n="130.6">morte</w></l>
					<l n="131" num="33.3"><w n="131.1">L</w>’<w n="131.2">époque</w> <w n="131.3">où</w> <w n="131.4">nous</w> <w n="131.5">voyions</w></l>
					<l n="132" num="33.4"><space quantity="4" unit="char"></space><w n="132.1">Tant</w> <w n="132.2">de</w> <w n="132.3">rayons</w> !</l>
				</lg>
				<lg n="34">
					<l n="133" num="34.1"><w n="133.1">Où</w> <w n="133.2">sont</w>-<w n="133.3">ils</w> ? <w n="133.4">les</w> <w n="133.5">poëtes</w></l>
					<l n="134" num="34.2"><w n="134.1">Qui</w> <w n="134.2">nous</w> <w n="134.3">faisaient</w> <w n="134.4">des</w> <w n="134.5">fêtes</w>,</l>
					<l n="135" num="34.3"><w n="135.1">Ces</w> <w n="135.2">vaillants</w>, <w n="135.3">ces</w> <w n="135.4">grands</w> <w n="135.5">cœurs</w>,</l>
					<l n="136" num="34.4"><space quantity="4" unit="char"></space><w n="136.1">Tous</w> <w n="136.2">ces</w> <w n="136.3">vainqueurs</w>,</l>
				</lg>
				<lg n="35">
					<l n="137" num="35.1"><w n="137.1">Ces</w> <w n="137.2">soldats</w>, <w n="137.3">ces</w> <w n="137.4">apôtres</w> ?</l>
					<l n="138" num="35.2"><w n="138.1">Les</w> <w n="138.2">uns</w> <w n="138.3">sont</w> <w n="138.4">morts</w>. <w n="138.5">Les</w> <w n="138.6">autres</w>,</l>
					<l n="139" num="35.3"><w n="139.1">Du</w> <w n="139.2">repos</w> <w n="139.3">envieux</w>,</l>
					<l n="140" num="35.4"><space quantity="4" unit="char"></space><w n="140.1">Sont</w> <w n="140.2">déjà</w> <w n="140.3">vieux</w>.</l>
				</lg>
				<lg n="36">
					<l n="141" num="36.1"><w n="141.1">Leur</w> <w n="141.2">histoire</w> <w n="141.3">si</w> <w n="141.4">grande</w></l>
					<l n="142" num="36.2"><w n="142.1">N</w>’<w n="142.2">est</w> <w n="142.3">plus</w> <w n="142.4">qu</w>’<w n="142.5">une</w> <w n="142.6">légende</w></l>
					<l n="143" num="36.3"><w n="143.1">Qu</w>’<w n="143.2">autour</w> <w n="143.3">du</w> <w n="143.4">foyer</w> <w n="143.5">noir</w></l>
					<l n="144" num="36.4"><space quantity="4" unit="char"></space><w n="144.1">On</w> <w n="144.2">dit</w> <w n="144.3">le</w> <w n="144.4">soir</w>,</l>
				</lg>
				<lg n="37">
					<l n="145" num="37.1"><w n="145.1">Et</w> <w n="145.2">ce</w> <w n="145.3">collier</w> <w n="145.4">illustre</w>,</l>
					<l n="146" num="37.2"><w n="146.1">Qu</w>’<w n="146.2">à</w> <w n="146.3">présent</w> <w n="146.4">touche</w> <w n="146.5">un</w> <w n="146.6">rustre</w>,</l>
					<l n="147" num="37.3"><w n="147.1">Sème</w> <w n="147.2">ses</w> <w n="147.3">grains</w> <w n="147.4">épars</w></l>
					<l n="148" num="37.4"><space quantity="4" unit="char"></space><w n="148.1">De</w> <w n="148.2">toutes</w> <w n="148.3">parts</w>.</l>
				</lg>
				<lg n="38">
					<l n="149" num="38.1"><w n="149.1">Hamlet</w> <w n="149.2">qu</w>’<w n="149.3">on</w> <w n="149.4">abandonne</w></l>
					<l n="150" num="38.2"><w n="150.1">Est</w> <w n="150.2">seul</w> <w n="150.3">et</w> <w n="150.4">sans</w> <w n="150.5">couronne</w></l>
					<l n="151" num="38.3"><w n="151.1">Même</w> <w n="151.2">dans</w> <w n="151.3">Elseneur</w> :</l>
					<l n="152" num="38.4"><space quantity="4" unit="char"></space><w n="152.1">Adieu</w> <w n="152.2">l</w>’<w n="152.3">honneur</w></l>
				</lg>
				<lg n="39">
					<l n="153" num="39.1"><w n="153.1">De</w> <w n="153.2">l</w>’<w n="153.3">âge</w> <w n="153.4">romantique</w> ;</l>
					<l n="154" num="39.2"><w n="154.1">Mais</w> <w n="154.2">de</w> <w n="154.3">la</w> <w n="154.4">chaîne</w> <w n="154.5">antique</w></l>
					<l n="155" num="39.3"><w n="155.1">Garde</w>-<w n="155.2">nous</w> <w n="155.3">chaque</w> <w n="155.4">anneau</w>,</l>
					<l n="156" num="39.4"><space quantity="4" unit="char"></space><w n="156.1">Asselineau</w> !</l>
				</lg>
				<lg n="40">
					<l n="157" num="40.1"><w n="157.1">Comme</w> <w n="157.2">le</w> <w n="157.3">vieil</w> <w n="157.4">Homère</w></l>
					<l n="158" num="40.2"><w n="158.1">Savamment</w> <w n="158.2">énumère</w></l>
					<l n="159" num="40.3"><w n="159.1">Les</w> <w n="159.2">princes</w>, <w n="159.3">les</w> <w n="159.4">vassaux</w></l>
					<l n="160" num="40.4"><space quantity="4" unit="char"></space><w n="160.1">Et</w> <w n="160.2">leurs</w> <w n="160.3">vaisseaux</w>,</l>
				</lg>
				<lg n="41">
					<l n="161" num="41.1"><w n="161.1">Redis</w>-<w n="161.2">nous</w> <w n="161.3">cette</w> <w n="161.4">guère</w> !</l>
					<l n="162" num="41.2"><w n="162.1">Les</w> <w n="162.2">livres</w> <w n="162.3">faits</w> <w n="162.4">naguère</w></l>
					<l n="163" num="41.3"><w n="163.1">Selon</w> <w n="163.2">le</w> <w n="163.3">rituel</w></l>
					<l n="164" num="41.4"><space quantity="4" unit="char"></space><w n="164.1">De</w> <w n="164.2">Renduel</w>,</l>
				</lg>
				<lg n="42">
					<l n="165" num="42.1"><w n="165.1">Fais</w>-<w n="165.2">les</w> <w n="165.3">voir</w> <w n="165.4">à</w> <w n="165.5">la</w> <w n="165.6">file</w> !</l>
					<l n="166" num="42.2"><w n="166.1">Jusqu</w>’<w n="166.2">au</w> <w n="166.3">Bibliophile</w></l>
					<l n="167" num="42.3"><w n="167.1">Montrant</w> <w n="167.2">page</w> <w n="167.3">et</w> <w n="167.4">bourrel</w>,</l>
					<l n="168" num="42.4"><space quantity="4" unit="char"></space><w n="168.1">Jusqu</w>’<w n="168.2">à</w> <w n="168.3">Borel</w> ;</l>
				</lg>
				<lg n="43">
					<l n="169" num="43.1"><w n="169.1">Car</w> <w n="169.2">tu</w> <w n="169.3">sais</w> <w n="169.4">leur</w> <w n="169.5">histoire</w></l>
					<l n="170" num="43.2"><w n="170.1">Si</w> <w n="170.2">bien</w> <w n="170.3">que</w> <w n="170.4">ta</w> <w n="170.5">mémoire</w></l>
					<l n="171" num="43.3"><w n="171.1">N</w>’<w n="171.2">a</w> <w n="171.3">pas</w> <w n="171.4">même</w> <w n="171.5">failli</w></l>
					<l n="172" num="43.4"><space quantity="4" unit="char"></space><w n="172.1">Pour</w> <w n="172.2">Lassailly</w>.</l>
				</lg>
				<lg n="44">
					<l n="173" num="44.1"><w n="173.1">Donc</w>, <w n="173.2">toi</w> <w n="173.3">que</w> <w n="173.4">je</w> <w n="173.5">compare</w></l>
					<l n="174" num="44.2"><w n="174.1">Au</w> <w n="174.2">Héraut</w>, <w n="174.3">qui</w> <w n="174.4">répare</w></l>
					<l n="175" num="44.3"><w n="175.1">Le</w> <w n="175.2">beau</w> <w n="175.3">renom</w> <w n="175.4">des</w> <w n="175.5">vers</w></l>
					<l n="176" num="44.4"><space quantity="4" unit="char"></space><w n="176.1">Par</w> <w n="176.2">l</w>’<w n="176.3">univers</w>,</l>
				</lg>
				<lg n="45">
					<l n="177" num="45.1"><w n="177.1">Dis</w>-<w n="177.2">nous</w> <w n="177.3">Mil</w> <w n="177.4">huit</w> <w n="177.5">cent</w> <w n="177.6">trente</w>,</l>
					<l n="178" num="45.2"><w n="178.1">Époque</w> <w n="178.2">fulgurante</w>,</l>
					<l n="179" num="45.3"><w n="179.1">Ses</w> <w n="179.2">luttes</w>, <w n="179.3">ses</w> <w n="179.4">ardeurs</w></l>
					<l n="180" num="45.4"><space quantity="4" unit="char"></space><w n="180.1">Et</w> <w n="180.2">les</w> <w n="180.3">splendeurs</w></l>
				</lg>
				<lg n="46">
					<l n="181" num="46.1"><w n="181.1">De</w> <w n="181.2">cette</w> <w n="181.3">apocalypse</w>,</l>
					<l n="182" num="46.2"><w n="182.1">Que</w> <w n="182.2">maintenant</w> <w n="182.3">éclipse</w></l>
					<l n="183" num="46.3"><w n="183.1">Le</w> <w n="183.2">puissant</w> <w n="183.3">coryza</w></l>
					<l n="184" num="46.4"><space quantity="4" unit="char"></space><w n="184.1">De</w> <w n="184.2">Thérésa</w> !</l>
				</lg>
				<lg n="47">
					<l n="185" num="47.1"><w n="185.1">Car</w> <w n="185.2">il</w> <w n="185.3">est</w> <w n="185.4">beau</w> <w n="185.5">de</w> <w n="185.6">dire</w></l>
					<l n="186" num="47.2"><w n="186.1">A</w> <w n="186.2">notre</w> <w n="186.3">âge</w> <w n="186.4">en</w> <w n="186.5">délire</w></l>
					<l n="187" num="47.3"><w n="187.1">Courbé</w> <w n="187.2">sur</w> <w n="187.3">des</w> <w n="187.4">écus</w> :</l>
					<l n="188" num="47.4"><space quantity="4" unit="char"></space><w n="188.1">Gloire</w> <w n="188.2">aux</w> <w n="188.3">vaincus</w>.</l>
				</lg>
				<lg n="48">
					<l n="189" num="48.1"><w n="189.1">Envahi</w> <w n="189.2">par</w> <w n="189.3">le</w> <w n="189.4">lierre</w>,</l>
					<l n="190" num="48.2"><w n="190.1">Le</w> <w n="190.2">château</w> <w n="190.3">pierre</w> <w n="190.4">à</w> <w n="190.5">pierre</w></l>
					<l n="191" num="48.3"><w n="191.1">Tombe</w> <w n="191.2">et</w> <w n="191.3">s</w>’<w n="191.4">écroule</w> ; <w n="191.5">mais</w></l>
					<l n="192" num="48.4"><space quantity="4" unit="char"></space><w n="192.1">Rien</w> <w n="192.2">n</w>’<w n="192.3">a</w> <w n="192.4">jamais</w></l>
				</lg>
				<lg n="49">
					<l n="193" num="49.1"><w n="193.1">Dompté</w> <w n="193.2">le</w> <w n="193.3">fanatisme</w></l>
					<l n="194" num="49.2"><w n="194.1">Du</w> <w n="194.2">bon</w> <w n="194.3">vieux</w> <w n="194.4">romantisme</w>,</l>
					<l n="195" num="49.3"><w n="195.1">De</w> <w n="195.2">ce</w> <w n="195.3">Titan</w> <w n="195.4">du</w> <w n="195.5">Rhin</w></l>
					<l n="196" num="49.4"><space quantity="4" unit="char"></space><w n="196.1">Au</w> <w n="196.2">cœur</w> <w n="196.3">d</w>’<w n="196.4">airain</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1866"> 21 juillet 1866.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>