<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">RIMES DORÉES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1492 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_14</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">RIMES DORÉES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN602">
				<head type="main">L’Âme victorieuse du Désir</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">dieu</w> <w n="1.3">Désir</w>, <w n="1.4">l</w>’<w n="1.5">archer</w> <w n="1.6">sauvage</w></l>
					<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">rit</w>, <w n="2.3">sur</w> <w n="2.4">un</w> <w n="2.5">gouffre</w> <w n="2.6">penché</w>,</l>
					<l n="3" num="1.3"><w n="3.1">A</w> <w n="3.2">longtemps</w> <w n="3.3">dans</w> <w n="3.4">un</w> <w n="3.5">dur</w> <w n="3.6">servage</w></l>
					<l n="4" num="1.4"><w n="4.1">Tenu</w> <w n="4.2">la</w> <w n="4.3">tremblante</w> <w n="4.4">Psyché</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Bien</w> <w n="5.2">longtemps</w> <w n="5.3">il</w> <w n="5.4">l</w>’<w n="5.5">a</w> <w n="5.6">torturée</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Piquant</w> <w n="6.2">son</w> <w n="6.3">sein</w> <w n="6.4">charmant</w> <w n="6.5">et</w> <w n="6.6">beau</w></l>
					<l n="7" num="2.3"><w n="7.1">Avec</w> <w n="7.2">une</w> <w n="7.3">flèche</w> <w n="7.4">acérée</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Ou</w> <w n="8.2">la</w> <w n="8.3">brûlant</w> <w n="8.4">de</w> <w n="8.5">son</w> <w n="8.6">flambeau</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">traînant</w> <w n="9.3">dans</w> <w n="9.4">l</w>’<w n="9.5">herbe</w> <w n="9.6">fleurie</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Folle</w> <w n="10.2">sous</w> <w n="10.3">son</w> <w n="10.4">bras</w> <w n="10.5">souverain</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">l</w>’<w n="11.3">a</w> <w n="11.4">déchirée</w> <w n="11.5">et</w> <w n="11.6">meurtrie</w></l>
					<l n="12" num="3.4"><w n="12.1">Avec</w> <w n="12.2">de</w> <w n="12.3">durs</w> <w n="12.4">liens</w> <w n="12.5">d</w>’<w n="12.6">airain</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Encor</w> <w n="13.2">rouge</w> <w n="13.3">de</w> <w n="13.4">sa</w> <w n="13.5">brûlure</w>,</l>
					<l n="14" num="4.2"><w n="14.1">O</w> <w n="14.2">noirs</w> <w n="14.3">crimes</w> <w n="14.4">inexpiés</w>,</l>
					<l n="15" num="4.3"><w n="15.1">En</w> <w n="15.2">marchant</w> <w n="15.3">sur</w> <w n="15.4">sa</w> <w n="15.5">chevelure</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Il</w> <w n="16.2">l</w>’<w n="16.3">a</w> <w n="16.4">longtemps</w> <w n="16.5">foulée</w> <w n="16.6">aux</w> <w n="16.7">pieds</w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">puis</w> <w n="17.3">mourante</w>, <w n="17.4">échevelée</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Plus</w> <w n="18.2">pâle</w> <w n="18.3">que</w> <w n="18.4">le</w> <w n="18.5">nénuphar</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Il</w> <w n="19.2">l</w>’<w n="19.3">a</w>, <w n="19.4">dans</w> <w n="19.5">sa</w> <w n="19.6">rage</w>, <w n="19.7">attelée</w></l>
					<l n="20" num="5.4"><w n="20.1">Comme</w> <w n="20.2">une</w> <w n="20.3">cavale</w>, <w n="20.4">à</w> <w n="20.5">son</w> <w n="20.6">char</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">devant</w> <w n="21.3">lui</w>, <w n="21.4">de</w> <w n="21.5">cette</w> <w n="21.6">vierge</w></l>
					<l n="22" num="6.2"><w n="22.1">Faisant</w> <w n="22.2">sa</w> <w n="22.3">proie</w> <w n="22.4">et</w> <w n="22.5">son</w> <w n="22.6">jouet</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Au</w> <w n="23.2">bord</w> <w n="23.3">du</w> <w n="23.4">fleuve</w>, <w n="23.5">sur</w> <w n="23.6">la</w> <w n="23.7">berge</w></l>
					<l n="24" num="6.4"><w n="24.1">Il</w> <w n="24.2">l</w>’<w n="24.3">a</w> <w n="24.4">chassée</w> <w n="24.5">à</w> <w n="24.6">coups</w> <w n="24.7">de</w> <w n="24.8">fouet</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">vainement</w> <w n="25.3">l</w>’<w n="25.4">humble</w> <w n="25.5">victime</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Dans</w> <w n="26.2">ses</w> <w n="26.3">horribles</w> <w n="26.4">désespoirs</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Adjurait</w> <w n="27.2">le</w> <w n="27.3">grand</w> <w n="27.4">mont</w> <w n="27.5">sublime</w></l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">les</w> <w n="28.3">bois</w> <w n="28.4">frissonnants</w> <w n="28.5">et</w> <w n="28.6">noirs</w> ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">La</w> <w n="29.2">Nature</w>, <w n="29.3">que</w> <w n="29.4">rien</w> <w n="29.5">ne</w> <w n="29.6">touche</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Parmi</w> <w n="30.2">les</w> <w n="30.3">rochers</w> <w n="30.4">arrogants</w></l>
					<l n="31" num="8.3"><w n="31.1">La</w> <w n="31.2">regardait</w> <w n="31.3">passer</w>, <w n="31.4">farouche</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Dans</w> <w n="32.2">les</w> <w n="32.3">cris</w> <w n="32.4">et</w> <w n="32.5">les</w> <w n="32.6">ouragans</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">le</w> <w n="33.3">vent</w> <w n="33.4">courait</w> <w n="33.5">dans</w> <w n="33.6">les</w> <w n="33.7">chênes</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Et</w> <w n="34.2">l</w>’<w n="34.3">imprécation</w> <w n="34.4">des</w> <w n="34.5">flots</w></l>
					<l n="35" num="9.3"><w n="35.1">Étouffait</w> <w n="35.2">le</w> <w n="35.3">bruit</w> <w n="35.4">de</w> <w n="35.5">ses</w> <w n="35.6">chaînes</w></l>
					<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">la</w> <w n="36.3">rumeur</w> <w n="36.4">de</w> <w n="36.5">ses</w> <w n="36.6">sanglots</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Mais</w>, <w n="37.2">longtemps</w> <w n="37.3">mordue</w> <w n="37.4">et</w> <w n="37.5">fouettée</w></l>
					<l n="38" num="10.2"><w n="38.1">Par</w> <w n="38.2">les</w> <w n="38.3">souffles</w> <w n="38.4">éoliens</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Psyché</w> <w n="39.2">s</w>’<w n="39.3">est</w> <w n="39.4">enfin</w> <w n="39.5">révoltée</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Elle</w> <w n="40.2">a</w> <w n="40.3">brisé</w> <w n="40.4">ses</w> <w n="40.5">durs</w> <w n="40.6">liens</w> ;</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Et</w> <w n="41.2">trouvant</w> <w n="41.3">une</w> <w n="41.4">force</w> <w n="41.5">étrange</w></l>
					<l n="42" num="11.2"><w n="42.1">Pour</w> <w n="42.2">l</w>’<w n="42.3">arrêter</w> <w n="42.4">et</w> <w n="42.5">le</w> <w n="42.6">saisir</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Elle</w> <w n="43.2">a</w> <w n="43.3">renversé</w> <w n="43.4">dans</w> <w n="43.5">la</w> <w n="43.6">fange</w></l>
					<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">terrassé</w> <w n="44.3">le</w> <w n="44.4">dieu</w> <w n="44.5">Désir</w> ;</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Tordant</w> <w n="45.2">sa</w> <w n="45.3">bouche</w> <w n="45.4">purpurine</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Elle</w> <w n="46.2">a</w>, <w n="46.3">d</w>’<w n="46.4">un</w> <w n="46.5">beau</w> <w n="46.6">geste</w> <w n="46.7">moqueur</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Broyé</w> <w n="47.2">du</w> <w n="47.3">genou</w> <w n="47.4">la</w> <w n="47.5">poitrine</w></l>
					<l n="48" num="12.4"><w n="48.1">De</w> <w n="48.2">son</w> <w n="48.3">implacable</w> <w n="48.4">vainqueur</w> ;</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">dans</w> <w n="49.3">sa</w> <w n="49.4">fureur</w> <w n="49.5">vengeresse</w></l>
					<l n="50" num="13.2"><w n="50.1">Elle</w> <w n="50.2">a</w>, <w n="50.3">guerrière</w> <w n="50.4">au</w> <w n="50.5">doux</w> <w n="50.6">œil</w> <w n="50.7">bleu</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Fustigé</w> <w n="51.2">de</w> <w n="51.3">sa</w> <w n="51.4">blonde</w> <w n="51.5">tresse</w></l>
					<l n="52" num="13.4"><w n="52.1">Le</w> <w n="52.2">visage</w> <w n="52.3">du</w> <w n="52.4">jeune</w> <w n="52.5">Dieu</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Relevant</w> <w n="53.2">son</w> <w n="53.3">front</w> <w n="53.4">misérable</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Elle</w> <w n="54.2">a</w>, <w n="54.3">riant</w> <w n="54.4">au</w> <w n="54.5">ciel</w> <w n="54.6">serein</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Brisé</w> <w n="55.2">l</w>’<w n="55.3">arc</w> <w n="55.4">fait</w> <w n="55.5">en</w> <w n="55.6">bois</w> <w n="55.7">d</w>’<w n="55.8">érable</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Et</w> <w n="56.2">les</w> <w n="56.3">flèches</w>, <w n="56.4">lourdes</w> <w n="56.5">d</w>’<w n="56.6">airain</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Puis</w>, <w n="57.2">fière</w> <w n="57.3">en</w> <w n="57.4">sa</w> <w n="57.5">métamorphose</w></l>
					<l n="58" num="15.2"><w n="58.1">Qui</w> <w n="58.2">semble</w> <w n="58.3">un</w> <w n="58.4">éblouissement</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Elle</w> <w n="59.2">a</w>, <w n="59.3">sous</w> <w n="59.4">son</w> <w n="59.5">divin</w> <w n="59.6">pied</w> <w n="59.7">rose</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Éteint</w> <w n="60.2">le</w> <w n="60.3">noir</w> <w n="60.4">flambeau</w> <w n="60.5">fumant</w>.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Et</w> <w n="61.2">maintenant</w> <w n="61.3">le</w> <w n="61.4">Dieu</w> <w n="61.5">l</w>’<w n="61.6">adore</w> !</l>
					<l n="62" num="16.2"><w n="62.1">Lui</w>, <w n="62.2">le</w> <w n="62.3">cruel</w> <w n="62.4">Désir</w>, <w n="62.5">touché</w></l>
					<l n="63" num="16.3"><w n="63.1">Par</w> <w n="63.2">la</w> <w n="63.3">grâce</w> <w n="63.4">qui</w> <w n="63.5">la</w> <w n="63.6">décore</w>,</l>
					<l n="64" num="16.4"><w n="64.1">Il</w> <w n="64.2">suit</w> <w n="64.3">la</w> <w n="64.4">trace</w> <w n="64.5">de</w> <w n="64.6">Psyché</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Il</w> <w n="65.2">lui</w> <w n="65.3">dit</w> : <w n="65.4">O</w> <w n="65.5">ma</w> <w n="65.6">jeune</w> <w n="65.7">amante</w> !</l>
					<l n="66" num="17.2"><w n="66.1">O</w> <w n="66.2">mon</w> <w n="66.3">trésor</w> ! <w n="66.4">O</w> <w n="66.5">mon</w> <w n="66.6">seul</w> <w n="66.7">bien</w> !</l>
					<l n="67" num="17.3"><w n="67.1">Parle</w>-<w n="67.2">moi</w> <w n="67.3">de</w> <w n="67.4">ta</w> <w n="67.5">voix</w> <w n="67.6">charmante</w>,</l>
					<l n="68" num="17.4"><w n="68.1">Je</w> <w n="68.2">t</w>’<w n="68.3">obéirai</w> <w n="68.4">comme</w> <w n="68.5">un</w> <w n="68.6">chien</w>.</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Tes</w> <w n="69.2">colères</w> <w n="69.3">seront</w> <w n="69.4">mes</w> <w n="69.5">fêtes</w> ;</l>
					<l n="70" num="18.2"><w n="70.1">Laisse</w>-<w n="70.2">moi</w> <w n="70.3">te</w> <w n="70.4">parer</w> <w n="70.5">de</w> <w n="70.6">fleurs</w>.</l>
					<l n="71" num="18.3"><w n="71.1">Ces</w> <w n="71.2">blessures</w> <w n="71.3">que</w> <w n="71.4">je</w> <w n="71.5">t</w>’<w n="71.6">ai</w> <w n="71.7">faites</w>,</l>
					<l n="72" num="18.4"><w n="72.1">Je</w> <w n="72.2">les</w> <w n="72.3">laverai</w> <w n="72.4">de</w> <w n="72.5">mes</w> <w n="72.6">pleurs</w>.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Tu</w> <w n="73.2">m</w>’<w n="73.3">as</w> <w n="73.4">dompté</w>, <w n="73.5">vierge</w> <w n="73.6">farouche</w>,</l>
					<l n="74" num="19.2"><w n="74.1">Comme</w> <w n="74.2">je</w> <w n="74.3">domptais</w> <w n="74.4">les</w> <w n="74.5">lions</w>.</l>
					<l n="75" num="19.3"><w n="75.1">Ouvre</w> <w n="75.2">les</w> <w n="75.3">roses</w> <w n="75.4">de</w> <w n="75.5">ta</w> <w n="75.6">bouche</w> :</l>
					<l n="76" num="19.4"><w n="76.1">Parle</w> ! <w n="76.2">où</w> <w n="76.3">veux</w>-<w n="76.4">tu</w> <w n="76.5">que</w> <w n="76.6">nous</w> <w n="76.7">allions</w> ?</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">Alors</w>, <w n="77.2">oubliant</w> <w n="77.3">ses</w> <w n="77.4">désastres</w>,</l>
					<l n="78" num="20.2"><w n="78.1">Tournant</w> <w n="78.2">ses</w> <w n="78.3">yeux</w> <w n="78.4">de</w> <w n="78.5">diamant</w></l>
					<l n="79" num="20.3"><w n="79.1">Vers</w> <w n="79.2">l</w>’<w n="79.3">azur</w> <w n="79.4">ou</w> <w n="79.5">brillent</w> <w n="79.6">les</w> <w n="79.7">astres</w>,</l>
					<l n="80" num="20.4"><w n="80.1">Psyché</w> <w n="80.2">lui</w> <w n="80.3">dit</w> : <w n="80.4">O</w> <w n="80.5">mon</w> <w n="80.6">amant</w> !</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1"><w n="81.1">Puisque</w> <w n="81.2">nos</w> <w n="81.3">regards</w> <w n="81.4">se</w> <w n="81.5">dessillent</w>,</l>
					<l n="82" num="21.2"><w n="82.1">Traversons</w> <w n="82.2">l</w>’<w n="82.3">éther</w> <w n="82.4">irrité</w> ;</l>
					<l n="83" num="21.3"><w n="83.1">Allons</w> <w n="83.2">jusqu</w>’<w n="83.3">au</w> <w n="83.4">séjour</w> <w n="83.5">où</w> <w n="83.6">brillent</w></l>
					<l n="84" num="21.4"><w n="84.1">La</w> <w n="84.2">Justice</w> <w n="84.3">et</w> <w n="84.4">la</w> <w n="84.5">Vérité</w> ;</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1"><w n="85.1">Où</w> <w n="85.2">l</w>’<w n="85.3">Être</w> <w n="85.4">enfin</w> <w n="85.5">se</w> <w n="85.6">rassasie</w>,</l>
					<l n="86" num="22.2"><w n="86.1">Délivré</w> <w n="86.2">des</w> <w n="86.3">âpres</w> <w n="86.4">douleurs</w>,</l>
					<l n="87" num="22.3"><w n="87.1">Où</w> <w n="87.2">les</w> <w n="87.3">Dieux</w> <w n="87.4">goûtent</w> <w n="87.5">l</w>’<w n="87.6">ambroisie</w></l>
					<l n="88" num="22.4"><w n="88.1">En</w> <w n="88.2">contemplant</w> <w n="88.3">de</w> <w n="88.4">rouges</w> <w n="88.5">fleurs</w>,</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1"><w n="89.1">Et</w> <w n="89.2">savent</w> <w n="89.3">ce</w> <w n="89.4">que</w> <w n="89.5">l</w>’<w n="89.6">âme</w> <w n="89.7">ignore</w>,</l>
					<l n="90" num="23.2"><w n="90.1">Et</w> <w n="90.2">dans</w> <w n="90.3">un</w> <w n="90.4">ineffable</w> <w n="90.5">jour</w></l>
					<l n="91" num="23.3"><w n="91.1">Sans</w> <w n="91.2">crépuscule</w> <w n="91.3">et</w> <w n="91.4">sans</w> <w n="91.5">aurore</w>,</l>
					<l n="92" num="23.4"><w n="92.1">S</w>’<w n="92.2">enivrent</w> <w n="92.3">de</w> <w n="92.4">l</w>’<w n="92.5">immense</w> <w n="92.6">amour</w> !</l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1"><w n="93.1">Elle</w> <w n="93.2">dit</w>, <w n="93.3">et</w> <w n="93.4">le</w> <w n="93.5">Dieu</w> <w n="93.6">l</w>’<w n="93.7">embrasse</w> ;</l>
					<l n="94" num="24.2"><w n="94.1">Il</w> <w n="94.2">la</w> <w n="94.3">tient</w> <w n="94.4">d</w>’<w n="94.5">un</w> <w n="94.6">bras</w> <w n="94.7">ferme</w> <w n="94.8">et</w> <w n="94.9">sûr</w>,</l>
					<l n="95" num="24.3"><w n="95.1">Et</w> <w n="95.2">tous</w> <w n="95.3">les</w> <w n="95.4">deux</w>, <w n="95.5">laissant</w> <w n="95.6">leur</w> <w n="95.7">trace</w></l>
					<l n="96" num="24.4"><w n="96.1">Lumineuse</w> <w n="96.2">au</w> <w n="96.3">subtil</w> <w n="96.4">azur</w>,</l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1"><w n="97.1">Cherchant</w>, <w n="97.2">par</w> <w n="97.3">delà</w> <w n="97.4">les</w> <w n="97.5">étoiles</w>,</l>
					<l n="98" num="25.2"><w n="98.1">Le</w> <w n="98.2">clair</w> <w n="98.3">Éden</w> <w n="98.4">où</w>, <w n="98.5">pour</w> <w n="98.6">l</w>’<w n="98.7">esprit</w></l>
					<l n="99" num="25.3"><w n="99.1">Enfin</w> <w n="99.2">délivré</w> <w n="99.3">de</w> <w n="99.4">ses</w> <w n="99.5">voiles</w>,</l>
					<l n="100" num="25.4"><w n="100.1">L</w>’<w n="100.2">extase</w>, <w n="100.3">ainsi</w> <w n="100.4">qu</w>’<w n="100.5">un</w> <w n="100.6">lys</w>, <w n="100.7">fleurit</w>,</l>
				</lg>
				<lg n="26">
					<l n="101" num="26.1"><w n="101.1">Et</w> <w n="101.2">le</w> <w n="101.3">flot</w> <w n="101.4">où</w> <w n="101.5">l</w>’<w n="101.6">Âme</w> <w n="101.7">se</w> <w n="101.8">noie</w></l>
					<l n="102" num="26.2"><w n="102.1">Dans</w> <w n="102.2">le</w> <w n="102.3">bonheur</w> <w n="102.4">essentiel</w>,</l>
					<l n="103" num="26.3"><w n="103.1">Ils</w> <w n="103.2">s</w>’<w n="103.3">envolent</w>, <w n="103.4">pâles</w> <w n="103.5">de</w> <w n="103.6">joie</w>,</l>
					<l n="104" num="26.4"><w n="104.1">Jusqu</w>’<w n="104.2">au</w> <w n="104.3">fond</w> <w n="104.4">des</w> <w n="104.5">gouffres</w> <w n="104.6">du</w> <w n="104.7">ciel</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1875"> 19 mai 1875.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>