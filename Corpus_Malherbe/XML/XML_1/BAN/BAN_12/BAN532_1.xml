<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PRINCESSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>294 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvillelesprincesses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1994">1994</date>
								</imprint>
								<biblScope unit="tome">IV</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1891">1889-1891</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VII</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN532">
				<head type="number">XX</head>
				<head type="main">La Princesse Borghèse</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Canova avait fait en 1811 une admirable statue, <lb></lb>
								modelée sur la princesse Borghèse, qui fut envoyée <lb></lb>
								à Turin au prince Borghèse, lequel la tint longtemps <lb></lb>
								placée dans son cabinet, et l’envoya plus tard à <lb></lb>
								Rome, où elle se trouve encore.
						</quote>
							<bibl>
								<name>M-Dj.</name>,<hi rend="ital">Biographie universelle</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">précieux</w> <w n="1.3">joyau</w> <w n="1.4">de</w> <w n="1.5">la</w> <w n="1.6">famille</w> <w n="1.7">corse</w>,</l>
					<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">princesse</w> <w n="2.3">Borghèse</w> <w n="2.4">est</w> <w n="2.5">nue</w>, <w n="2.6">et</w> <w n="2.7">le</w> <w n="2.8">sculpteur</w></l>
					<l n="3" num="1.3"><w n="3.1">Voit</w> <w n="3.2">jaillir</w> <w n="3.3">devant</w> <w n="3.4">lui</w>, <w n="3.5">comme</w> <w n="3.6">un</w> <w n="3.7">lys</w> <w n="3.8">enchanteur</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">jeune</w> <w n="4.3">corps</w>, <w n="4.4">brillant</w> <w n="4.5">de</w> <w n="4.6">jeunesse</w> <w n="4.7">et</w> <w n="4.8">de</w> <w n="4.9">force</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">seins</w> <w n="5.3">en</w> <w n="5.4">fleur</w>, <w n="5.5">les</w> <w n="5.6">plans</w> <w n="5.7">harmonieux</w> <w n="5.8">du</w> <w n="5.9">torse</w></l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">ravissent</w>, <w n="6.3">et</w> <w n="6.4">la</w> <w n="6.5">lumière</w> <w n="6.6">avec</w> <w n="6.7">lenteur</w></l>
					<l n="7" num="2.3"><w n="7.1">Vient</w> <w n="7.2">baigner</w> <w n="7.3">d</w>’<w n="7.4">un</w> <w n="7.5">rayon</w> <w n="7.6">subtil</w> <w n="7.7">et</w> <w n="7.8">créateur</w></l>
					<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">pieds</w> <w n="8.3">charmants</w>, <w n="8.4">posés</w> <w n="8.5">sur</w> <w n="8.6">un</w> <w n="8.7">tapis</w> <w n="8.8">d</w>’<w n="8.9">écorce</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">la</w> <w n="9.3">nymphe</w> <w n="9.4">que</w> <w n="9.5">fait</w> <w n="9.6">renaître</w> <w n="9.7">Canova</w>,</l>
					<l n="10" num="3.2"><w n="10.1">C</w>’<w n="10.2">est</w> <w n="10.3">Pauline</w>, <w n="10.4">effaçant</w> <w n="10.5">l</w>’<w n="10.6">idéal</w> <w n="10.7">qu</w>’<w n="10.8">il</w> <w n="10.9">rêva</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Mais</w> <w n="11.2">c</w>’<w n="11.3">est</w> <w n="11.4">aussi</w> <w n="11.5">Vénus</w>, <w n="11.6">la</w> <w n="11.7">grande</w> <w n="11.8">enchanteresse</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Car</w> <w n="12.2">l</w>’<w n="12.3">Artiste</w> <w n="12.4">enivré</w> <w n="12.5">d</w>’<w n="12.6">accords</w> <w n="12.7">mélodieux</w>,</l>
					<l n="13" num="4.2"><w n="13.1">S</w>’<w n="13.2">il</w> <w n="13.3">lui</w> <w n="13.4">plaît</w>, <w n="13.5">anoblit</w> <w n="13.6">le</w> <w n="13.7">sang</w> <w n="13.8">d</w>’<w n="13.9">une</w> <w n="13.10">princesse</w></l>
					<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">la</w> <w n="14.3">mêle</w> <w n="14.4">vivante</w> <w n="14.5">à</w> <w n="14.6">la</w> <w n="14.7">race</w> <w n="14.8">des</w> <w n="14.9">Dieux</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1874">Juillet 1874.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>