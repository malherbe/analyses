<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PRINCESSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>294 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvillelesprincesses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1994">1994</date>
								</imprint>
								<biblScope unit="tome">IV</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1891">1889-1891</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VII</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN519">
				<head type="number">VII</head>
				<head type="main">Antiope</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Hélas ! sur tous ces corps à la teinte nacrée <lb></lb>
								La Mort a déjà mis sa pâleur azurée, <lb></lb>
									Ils n’ont de rose que le sang. <lb></lb>
								Leurs bras abandonnés trempent, les mains ouvertes, <lb></lb>
								Dans la vase du fleuve, entre des algues vertes <lb></lb>
									Où l’eau les soulève en passant.
							</quote>
							<bibl>
								<name>Théophile Gautier</name>,<hi rend="ital">Le Thermodon</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Près</w> <w n="1.2">du</w> <w n="1.3">clair</w> <w n="1.4">Ilissos</w> <w n="1.5">au</w> <w n="1.6">rivage</w> <w n="1.7">fleuri</w></l>
					<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">indomptable</w> <w n="2.3">Thésée</w> <w n="2.4">a</w> <w n="2.5">vaincu</w> <w n="2.6">les</w> <w n="2.7">guerrières</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Mourantes</w>, <w n="3.2">leurs</w> <w n="3.3">chevaux</w> <w n="3.4">les</w> <w n="3.5">traînent</w> <w n="3.6">dans</w> <w n="3.7">les</w> <w n="3.8">pierres</w> :</l>
					<l n="4" num="1.4"><w n="4.1">Pas</w> <w n="4.2">un</w> <w n="4.3">de</w> <w n="4.4">ces</w> <w n="4.5">beaux</w> <w n="4.6">corps</w> <w n="4.7">qui</w> <w n="4.8">ne</w> <w n="4.9">râle</w> <w n="4.10">meurtri</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">silence</w> <w n="5.3">est</w> <w n="5.4">affreux</w>, <w n="5.5">et</w> <w n="5.6">parfois</w> <w n="5.7">un</w> <w n="5.8">grand</w> <w n="5.9">cri</w></l>
					<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2">interrompt</w>. <w n="6.3">Sous</w> <w n="6.4">l</w>’<w n="6.5">effort</w> <w n="6.6">des</w> <w n="6.7">lances</w> <w n="6.8">meurtrières</w>,</l>
					<l n="7" num="2.3"><w n="7.1">On</w> <w n="7.2">voit</w> <w n="7.3">des</w> <w n="7.4">yeux</w>, <w n="7.5">éteints</w> <w n="7.6">déjà</w>, <w n="7.7">sous</w> <w n="7.8">les</w> <w n="7.9">paupières</w></l>
					<l n="8" num="2.4"><w n="8.1">S</w>’<w n="8.2">entr</w>’<w n="8.3">ouvrir</w>. <w n="8.4">Tout</w> <w n="8.5">ce</w> <w n="8.6">peuple</w> <w n="8.7">adorable</w> <w n="8.8">a</w> <w n="8.9">péri</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Antiope</w> <w n="9.2">blessée</w>, <w n="9.3">haletante</w>, <w n="9.4">épuisée</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Combat</w> <w n="10.2">encor</w>. <w n="10.3">Le</w> <w n="10.4">sang</w>, <w n="10.5">ainsi</w> <w n="10.6">qu</w>’<w n="10.7">une</w> <w n="10.8">rosée</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Coule</w> <w n="11.2">de</w> <w n="11.3">ses</w> <w n="11.4">cheveux</w> <w n="11.5">et</w> <w n="11.6">tombe</w> <w n="11.7">sur</w> <w n="11.8">son</w> <w n="11.9">flanc</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Sa</w> <w n="12.2">poitrine</w> <w n="12.3">superbe</w> <w n="12.4">et</w> <w n="12.5">fière</w> <w n="12.6">en</w> <w n="12.7">est</w> <w n="12.8">trempée</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Et</w> <w n="13.2">sa</w> <w n="13.3">main</w>, <w n="13.4">teinte</w> <w n="13.5">aussi</w> <w n="13.6">dans</w> <w n="13.7">la</w> <w n="13.8">pourpre</w> <w n="13.9">du</w> <w n="13.10">sang</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Agite</w> <w n="14.2">le</w> <w n="14.3">tronçon</w> <w n="14.4">farouche</w> <w n="14.5">d</w>’<w n="14.6">une</w> <w n="14.7">épée</w>.</l>
				</lg>
			</div></body></text></TEI>