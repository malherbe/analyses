<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PRINCESSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>294 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_12</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banvillelesprincesses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Princesses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1994">1994</date>
								</imprint>
								<biblScope unit="tome">IV</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
				<monogr>
					<title>Œuvres</title>
					<author>Théodore de Banville</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonse Lemerre, éditeur</publisher>
						<date when="1891">1889-1891</date>
					</imprint>
				</monogr>
			</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VII</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1874">1874</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN529">
				<head type="number">XVII</head>
				<head type="main">Lucrèce Borgia</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
							Il y a au musée d’Anvers un tableau vénitien qui <lb></lb>
							symbolise admirablement, à l’insu du peintre, cette <lb></lb>
							papauté excentrique. On y voit Alexandre VI présen-<lb></lb>
							tant à saint Pierre l’évêque in partibus de Paphos, <lb></lb>
							qu’il vient de nommer général de ses galères.
							</quote>
							<bibl>
								<name>Paul de Saint-Victor</name>,<hi rend="ital">Hommes et Dieux</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lucrèce</w> <w n="1.2">Borgia</w> <w n="1.3">se</w> <w n="1.4">marie</w> ; <w n="1.5">il</w> <w n="1.6">est</w> <w n="1.7">juste</w></l>
					<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">tous</w> <w n="2.3">les</w> <w n="2.4">cardinaux</w> <w n="2.5">brillent</w> <w n="2.6">à</w> <w n="2.7">ce</w> <w n="2.8">gala</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ceux</w> <w n="3.2">du</w> <w n="3.3">moins</w> <w n="3.4">épargnés</w> <w n="3.5">par</w> <w n="3.6">la</w> <w n="3.7">cantarella</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">poison</w> <w n="4.3">plus</w> <w n="4.4">cruel</w> <w n="4.5">que</w> <w n="4.6">tous</w> <w n="4.7">ceux</w> <w n="4.8">de</w> <w n="4.9">Locuste</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Près</w> <w n="5.2">d</w>’<w n="5.3">eux</w> <w n="5.4">trône</w> <w n="5.5">César</w>, <w n="5.6">jeune</w>, <w n="5.7">féroce</w>, <w n="5.8">auguste</w>.</l>
					<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2">évêque</w> <w n="6.3">de</w> <w n="6.4">Paphos</w>, <w n="6.5">vêtu</w> <w n="6.6">de</w> <w n="6.7">pourpre</w>, <w n="6.8">est</w> <w n="6.9">là</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">le</w> <w n="7.3">pape</w>, <w n="7.4">à</w> <w n="7.5">côté</w> <w n="7.6">de</w> <w n="7.7">Giulia</w> <w n="7.8">Bella</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Montre</w>, <w n="8.2">comme</w> <w n="8.3">un</w> <w n="8.4">vieux</w> <w n="8.5">dieu</w>, <w n="8.6">sa</w> <w n="8.7">poitrine</w> <w n="8.8">robuste</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">parfums</w> <w n="9.3">de</w> <w n="9.4">la</w> <w n="9.5">chair</w> <w n="9.6">et</w> <w n="9.7">des</w> <w n="9.8">cheveux</w> <w n="9.9">flottants</w></l>
					<l n="10" num="3.2"><w n="10.1">S</w>’<w n="10.2">éparpillent</w> <w n="10.3">dans</w> <w n="10.4">l</w>’<w n="10.5">air</w> <w n="10.6">brûlant</w>, <w n="10.7">et</w> <w n="10.8">comme</w> <w n="10.9">au</w> <w n="10.10">temps</w></l>
					<l n="11" num="3.3"><w n="11.1">De</w> <w n="11.2">Caprée</w>, <w n="11.3">où</w> <w n="11.4">Tibère</w> <w n="11.5">épouvantait</w> <w n="11.6">les</w> <w n="11.7">nues</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Entrelaçant</w> <w n="12.2">leurs</w> <w n="12.3">corps</w> <w n="12.4">impudiques</w> <w n="12.5">et</w> <w n="12.6">beaux</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Sur</w> <w n="13.2">les</w> <w n="13.3">rouges</w> <w n="13.4">tapis</w> <w n="13.5">cinquante</w> <w n="13.6">femmes</w> <w n="13.7">nues</w></l>
					<l n="14" num="4.3"><w n="14.1">Dansent</w> <w n="14.2">effrontément</w>, <w n="14.3">aux</w> <w n="14.4">clartés</w> <w n="14.5">des</w> <w n="14.6">flambeaux</w>.</l>
				</lg>
			</div></body></text></TEI>