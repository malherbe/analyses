<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN290">
				<head type="main">A Arsène Houssaye</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Grâce</w> <w n="1.2">aux</w> <w n="1.3">Dalilas</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Nos</w> <w n="2.2">rimeurs</w> <w n="2.3">sont</w> <w n="2.4">las</w></l>
					<l n="3" num="1.3"><space quantity="4" unit="char"></space><w n="3.1">De</w> <w n="3.2">gloire</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w>, <w n="4.2">comme</w> <w n="4.3">un</w> <w n="4.4">hochet</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Ont</w> <w n="5.2">jeté</w> <w n="5.3">l</w>’<w n="5.4">archet</w></l>
					<l n="6" num="1.6"><space quantity="4" unit="char"></space><w n="6.1">D</w>’<w n="6.2">ivoire</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Au</w> <w n="7.2">rhythme</w> <w n="7.3">ailé</w> <w n="7.4">d</w>’<w n="7.5">or</w></l>
					<l n="8" num="2.2"><w n="8.1">Il</w> <w n="8.2">fallait</w> <w n="8.3">encor</w></l>
					<l n="9" num="2.3"><space quantity="4" unit="char"></space><w n="9.1">Un</w> <w n="9.2">maître</w></l>
					<l n="10" num="2.4"><w n="10.1">Fou</w> <w n="10.2">de</w> <w n="10.3">volupté</w>,</l>
					<l n="11" num="2.5"><w n="11.1">Alors</w> <w n="11.2">j</w>’<w n="11.3">ai</w> <w n="11.4">dompté</w></l>
					<l n="12" num="2.6"><space quantity="4" unit="char"></space><w n="12.1">Le</w> <w n="12.2">Mètre</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">repris</w> <w n="13.4">mon</w> <w n="13.5">luth</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Et</w>, <w n="14.2">suivant</w> <w n="14.3">le</w> <w n="14.4">but</w></l>
					<l n="15" num="3.3"><space quantity="4" unit="char"></space><w n="15.1">Féerique</w>,</l>
					<l n="16" num="3.4"><w n="16.1">Je</w> <w n="16.2">m</w>’<w n="16.3">en</w> <w n="16.4">vais</w> <w n="16.5">cherchant</w></l>
					<l n="17" num="3.5"><w n="17.1">Le</w> <w n="17.2">secret</w> <w n="17.3">du</w> <w n="17.4">chant</w></l>
					<l n="18" num="3.6"><space quantity="4" unit="char"></space><w n="18.1">Lyrique</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Œil</w> <w n="19.2">épanoui</w>,</l>
					<l n="20" num="4.2"><w n="20.1">Je</w> <w n="20.2">peins</w> <w n="20.3">ébloui</w></l>
					<l n="21" num="4.3"><space quantity="4" unit="char"></space><w n="21.1">Ou</w> <w n="21.2">triste</w>,</l>
					<l n="22" num="4.4"><w n="22.1">Le</w> <w n="22.2">ciel</w> <w n="22.3">radieux</w>,</l>
					<l n="23" num="4.5"><w n="23.1">Et</w>, <w n="23.2">mélodieux</w></l>
					<l n="24" num="4.6"><space quantity="4" unit="char"></space><w n="24.1">Artiste</w>,</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Près</w> <w n="25.2">du</w> <w n="25.3">fleuve</w> <w n="25.4">grec</w></l>
					<l n="26" num="5.2"><w n="26.1">Murmurant</w> <w n="26.2">avec</w></l>
					<l n="27" num="5.3"><space quantity="4" unit="char"></space><w n="27.1">Les</w> <w n="27.2">cygnes</w></l>
					<l n="28" num="5.4"><w n="28.1">Fiers</w> <w n="28.2">de</w> <w n="28.3">leur</w> <w n="28.4">candeur</w>,</l>
					<l n="29" num="5.5"><w n="29.1">Je</w> <w n="29.2">dis</w> <w n="29.3">la</w> <w n="29.4">splendeur</w></l>
					<l n="30" num="5.6"><space quantity="4" unit="char"></space><w n="30.1">Des</w> <w n="30.2">lignes</w>.</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">Mon</w> <w n="31.2">vin</w> <w n="31.3">triomphant</w>,</l>
					<l n="32" num="6.2"><w n="32.1">Sais</w>-<w n="32.2">tu</w> <w n="32.3">quelle</w> <w n="32.4">enfant</w></l>
					<l n="33" num="6.3"><space quantity="4" unit="char"></space><w n="33.1">Le</w> <w n="33.2">verse</w> ?</l>
					<l n="34" num="6.4"><w n="34.1">Viens</w>, <w n="34.2">et</w> <w n="34.3">tu</w> <w n="34.4">verras</w>,</l>
					<l n="35" num="6.5"><w n="35.1">Poëte</w>, <w n="35.2">quel</w> <w n="35.3">bras</w></l>
					<l n="36" num="6.6"><space quantity="4" unit="char"></space><w n="36.1">Me</w> <w n="36.2">berce</w> !</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">O</w> <w n="37.2">chasseur</w> <w n="37.3">altier</w>,</l>
					<l n="38" num="7.2"><w n="38.1">Qui</w> <w n="38.2">fuis</w> <w n="38.3">le</w> <w n="38.4">sentier</w></l>
					<l n="39" num="7.3"><space quantity="4" unit="char"></space><w n="39.1">Profane</w>,</l>
					<l n="40" num="7.4"><w n="40.1">Songeur</w> <w n="40.2">qu</w>’<w n="40.3">autrefois</w></l>
					<l n="41" num="7.5"><w n="41.1">Rencontrait</w> <w n="41.2">au</w> <w n="41.3">bois</w></l>
					<l n="42" num="7.6"><space quantity="4" unit="char"></space><w n="42.1">Diane</w> !</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Comme</w> <w n="43.2">toi</w>, <w n="43.3">qui</w> <w n="43.4">vins</w></l>
					<l n="44" num="8.2"><w n="44.1">Si</w> <w n="44.2">jeune</w> <w n="44.3">aux</w> <w n="44.4">divins</w></l>
					<l n="45" num="8.3"><space quantity="4" unit="char"></space><w n="45.1">Rivages</w>,</l>
					<l n="46" num="8.4"><w n="46.1">Ami</w>, <w n="46.2">j</w>’<w n="46.3">ai</w> <w n="46.4">toujours</w></l>
					<l n="47" num="8.5"><w n="47.1">Voulu</w> <w n="47.2">des</w> <w n="47.3">amours</w></l>
					<l n="48" num="8.6"><space quantity="4" unit="char"></space><w n="48.1">Sauvages</w>.</l>
				</lg>
				<lg n="9">
					<l n="49" num="9.1"><w n="49.1">Ah</w> ! <w n="49.2">quand</w> <w n="49.3">Mai</w> <w n="49.4">sourit</w></l>
					<l n="50" num="9.2"><w n="50.1">Aux</w> <w n="50.2">prés</w> <w n="50.3">où</w> <w n="50.4">fleurit</w></l>
					<l n="51" num="9.3"><space quantity="4" unit="char"></space><w n="51.1">La</w> <w n="51.2">menthe</w>,</l>
					<l n="52" num="9.4"><w n="52.1">Trouveurs</w> <w n="52.2">de</w> <w n="52.3">loisir</w>,</l>
					<l n="53" num="9.5"><w n="53.1">Sachons</w> <w n="53.2">y</w> <w n="53.3">choisir</w></l>
					<l n="54" num="9.6"><space quantity="4" unit="char"></space><w n="54.1">L</w>’<w n="54.2">amante</w> !</l>
				</lg>
				<lg n="10">
					<l n="55" num="10.1"><w n="55.1">Nymphe</w> <w n="55.2">au</w> <w n="55.3">regard</w> <w n="55.4">bleu</w>,</l>
					<l n="56" num="10.2"><w n="56.1">Si</w> <w n="56.2">sa</w> <w n="56.3">lèvre</w> <w n="56.4">en</w> <w n="56.5">feu</w></l>
					<l n="57" num="10.3"><space quantity="4" unit="char"></space><w n="57.1">Caresse</w></l>
					<l n="58" num="10.4"><w n="58.1">Nos</w> <w n="58.2">fronts</w> <w n="58.3">sans</w> <w n="58.4">témoins</w>,</l>
					<l n="59" num="10.5"><w n="59.1">Qu</w>’<w n="59.2">elle</w> <w n="59.3">soit</w> <w n="59.4">au</w> <w n="59.5">moins</w></l>
					<l n="60" num="10.6"><space quantity="12" unit="char"></space><w n="60.1">Déesse</w> !</l>
				</lg>
				<lg n="11">
					<l n="61" num="11.1"><w n="61.1">Toi</w>, <w n="61.2">pâle</w> <w n="61.3">et</w> <w n="61.4">rêvant</w>,</l>
					<l n="62" num="11.2"><w n="62.1">Au</w> <w n="62.2">bois</w> <w n="62.3">que</w> <w n="62.4">le</w> <w n="62.5">vent</w></l>
					<l n="63" num="11.3"><space quantity="4" unit="char"></space><w n="63.1">Assiège</w>,</l>
					<l n="64" num="11.4"><w n="64.1">Tu</w> <w n="64.2">suis</w> <w n="64.3">à</w> <w n="64.4">dessein</w></l>
					<l n="65" num="11.5"><w n="65.1">La</w> <w n="65.2">guerrière</w> <w n="65.3">au</w> <w n="65.4">sein</w></l>
					<l n="66" num="11.6"><space quantity="4" unit="char"></space><w n="66.1">De</w> <w n="66.2">neige</w> !</l>
				</lg>
				<lg n="12">
					<l n="67" num="12.1"><w n="67.1">Moi</w>, <w n="67.2">parmi</w> <w n="67.3">nos</w> <w n="67.4">jeux</w>,</l>
					<l n="68" num="12.2"><w n="68.1">Mon</w> <w n="68.2">plus</w> <w n="68.3">orageux</w></l>
					<l n="69" num="12.3"><space quantity="4" unit="char"></space><w n="69.1">Délire</w></l>
					<l n="70" num="12.4"><w n="70.1">Toujours</w> <w n="70.2">s</w>’<w n="70.3">en</w> <w n="70.4">revient</w></l>
					<l n="71" num="12.5"><w n="71.1">Vers</w> <w n="71.2">celle</w> <w n="71.3">qui</w> <w n="71.4">tient</w></l>
					<l n="72" num="12.6"><space quantity="4" unit="char"></space><w n="72.1">La</w> <w n="72.2">lyre</w> !</l>
				</lg>
				<lg n="13">
					<l n="73" num="13.1"><w n="73.1">Sans</w> <w n="73.2">doute</w> <w n="73.3">elle</w> <w n="73.4">a</w> <w n="73.5">pris</w></l>
					<l n="74" num="13.2"><w n="74.1">La</w> <w n="74.2">foule</w> <w n="74.3">en</w> <w n="74.4">mépris</w>,</l>
					<l n="75" num="13.3"><space quantity="4" unit="char"></space><w n="75.1">Et</w> <w n="75.2">porte</w></l>
					<l n="76" num="13.4"><w n="76.1">Un</w> <w n="76.2">peu</w> <w n="76.3">trop</w> <w n="76.4">souvent</w></l>
					<l n="77" num="13.5"><w n="77.1">Sa</w> <w n="77.2">crinière</w> <w n="77.3">au</w> <w n="77.4">vent</w>.</l>
					<l n="78" num="13.6"><space quantity="4" unit="char"></space><w n="78.1">Qu</w>’<w n="78.2">importe</w> !</l>
				</lg>
				<lg n="14">
					<l n="79" num="14.1"><w n="79.1">J</w>’<w n="79.2">aime</w> <w n="79.3">sa</w> <w n="79.4">pâleur</w>,</l>
					<l n="80" num="14.2"><w n="80.1">Et</w> <w n="80.2">sa</w> <w n="80.3">bouche</w> <w n="80.4">en</w> <w n="80.5">fleur</w></l>
					<l n="81" num="14.3"><space quantity="4" unit="char"></space><w n="81.1">Est</w> <w n="81.2">saine</w> !</l>
					<l n="82" num="14.4"><w n="82.1">Son</w> <w n="82.2">sang</w> <w n="82.3">et</w> <w n="82.4">sa</w> <w n="82.5">chair</w></l>
					<l n="83" num="14.5"><w n="83.1">Les</w> <w n="83.2">voilà</w>, <w n="83.3">mon</w> <w n="83.4">cher</w></l>
					<l n="84" num="14.6"><space quantity="4" unit="char"></space><w n="84.1">Arsène</w>.</l>
				</lg>
				<lg n="15">
					<l n="85" num="15.1"><w n="85.1">O</w> <w n="85.2">sens</w> <w n="85.3">embrasés</w> !</l>
					<l n="86" num="15.2"><w n="86.1">Maîtresse</w> <w n="86.2">aux</w> <w n="86.3">baisers</w></l>
					<l n="87" num="15.3"><space quantity="4" unit="char"></space><w n="87.1">Savante</w> !</l>
					<l n="88" num="15.4"><w n="88.1">Tendre</w> <w n="88.2">et</w> <w n="88.3">chère</w> <w n="88.4">voix</w>,</l>
					<l n="89" num="15.5"><w n="89.1">Ici</w> <w n="89.2">tu</w> <w n="89.3">la</w> <w n="89.4">vois</w></l>
					<l n="90" num="15.6"><space quantity="4" unit="char"></space><w n="90.1">Vivante</w>.</l>
				</lg>
				<lg n="16">
					<l n="91" num="16.1"><w n="91.1">Dos</w> <w n="91.2">flexible</w> <w n="91.3">et</w> <w n="91.4">nu</w> !</l>
					<l n="92" num="16.2"><w n="92.1">Sourire</w> <w n="92.2">ingénu</w></l>
					<l n="93" num="16.3"><space quantity="4" unit="char"></space><w n="93.1">Qui</w> <w n="93.2">m</w>’<w n="93.3">aime</w> !</l>
					<l n="94" num="16.4"><w n="94.1">L</w>’<w n="94.2">or</w> <w n="94.3">de</w> <w n="94.4">ses</w> <w n="94.5">cheveux</w></l>
					<l n="95" num="16.5"><w n="95.1">M</w>’<w n="95.2">enivre</w>, <w n="95.3">et</w> <w n="95.4">je</w> <w n="95.5">veux</w>,</l>
					<l n="96" num="16.6"><space quantity="4" unit="char"></space><w n="96.1">De</w> <w n="96.2">même</w>,</l>
				</lg>
				<lg n="17">
					<l n="97" num="17.1"><w n="97.1">Dans</w> <w n="97.2">mon</w> <w n="97.3">sang</w> <w n="97.4">qui</w> <w n="97.5">bout</w></l>
					<l n="98" num="17.2"><w n="98.1">Gardant</w> <w n="98.2">jusqu</w>’<w n="98.3">au</w> <w n="98.4">bout</w></l>
					<l n="99" num="17.3"><space quantity="4" unit="char"></space><w n="99.1">Ma</w> <w n="99.2">fièvre</w></l>
					<l n="100" num="17.4"><w n="100.1">Tout</w> <w n="100.2">comme</w> <w n="100.3">à</w> <w n="100.4">présent</w>,</l>
					<l n="101" num="17.5"><w n="101.1">Mourir</w> <w n="101.2">en</w> <w n="101.3">baisant</w></l>
					<l n="102" num="17.6"><space quantity="4" unit="char"></space><w n="102.1">Sa</w> <w n="102.2">lèvre</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>