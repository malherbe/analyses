<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN314">
				<head type="main">A Alfred Dehodencq</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Tenir</w> <w n="1.2">la</w> <w n="1.3">lumière</w> <w n="1.4">asservie</w></l>
					<l n="2" num="1.2"><w n="2.1">Lorsqu</w>’<w n="2.2">elle</w> <w n="2.3">voudrait</w> <w n="2.4">s</w>’<w n="2.5">envoler</w>,</l>
					<l n="3" num="1.3"><space quantity="12" unit="char"></space><w n="3.1">Et</w> <w n="3.2">voler</w></l>
					<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">Dieu</w> <w n="4.3">le</w> <w n="4.4">secret</w> <w n="4.5">de</w> <w n="4.6">la</w> <w n="4.7">vie</w> ;</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pour</w> <w n="5.2">les</w> <w n="5.3">mélanger</w> <w n="5.4">sur</w> <w n="5.5">des</w> <w n="5.6">toiles</w></l>
					<l n="6" num="2.2"><w n="6.1">Dérober</w> <w n="6.2">même</w> <w n="6.3">aux</w> <w n="6.4">cieux</w> <w n="6.5">vengeurs</w></l>
					<l n="7" num="2.3"><space quantity="12" unit="char"></space><w n="7.1">Leurs</w> <w n="7.2">rougeurs</w></l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">le</w> <w n="8.3">blanc</w> <w n="8.4">frisson</w> <w n="8.5">des</w> <w n="8.6">étoiles</w> ;</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Comme</w> <w n="9.2">on</w> <w n="9.3">cueille</w> <w n="9.4">une</w> <w n="9.5">fleur</w> <w n="9.6">éclose</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Ravir</w> <w n="10.2">à</w> <w n="10.3">l</w>’<w n="10.4">Orient</w> <w n="10.5">en</w> <w n="10.6">feu</w></l>
					<l n="11" num="3.3"><space quantity="12" unit="char"></space><w n="11.1">Son</w> <w n="11.2">air</w> <w n="11.3">bleu</w></l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">son</w> <w n="12.3">ciel</w> <w n="12.4">flamboyant</w> <w n="12.5">et</w> <w n="12.6">rose</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Pétrir</w> <w n="13.2">de</w> <w n="13.3">belles</w> <w n="13.4">créatures</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">sur</w> <w n="14.3">d</w>’<w n="14.4">éblouissants</w> <w n="14.5">amas</w></l>
					<l n="15" num="4.3"><space quantity="12" unit="char"></space><w n="15.1">De</w> <w n="15.2">damas</w></l>
					<l n="16" num="4.4"><w n="16.1">Éparpiller</w> <w n="16.2">des</w> <w n="16.3">chevelures</w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Inonder</w> <w n="17.2">de</w> <w n="17.3">sang</w> <w n="17.4">le</w> <w n="17.5">Calvaire</w></l>
					<l n="18" num="5.2"><w n="18.1">Ou</w> <w n="18.2">jeter</w> <w n="18.3">un</w> <w n="18.4">éclat</w> <w n="18.5">divin</w></l>
					<l n="19" num="5.3"><space quantity="12" unit="char"></space><w n="19.1">Sur</w> <w n="19.2">le</w> <w n="19.3">vin</w></l>
					<l n="20" num="5.4"><w n="20.1">Qu</w>’<w n="20.2">un</w> <w n="20.3">buveur</w> <w n="20.4">a</w> <w n="20.5">mis</w> <w n="20.6">dans</w> <w n="20.7">son</w> <w n="20.8">verre</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Se</w> <w n="21.2">réjouir</w> <w n="21.3">des</w> <w n="21.4">pierreries</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">jeter</w> <w n="22.3">le</w> <w n="22.4">baiser</w> <w n="22.5">vermeil</w></l>
					<l n="23" num="6.3"><space quantity="12" unit="char"></space><w n="23.1">Du</w> <w n="23.2">soleil</w></l>
					<l n="24" num="6.4"><w n="24.1">Jusque</w> <w n="24.2">sur</w> <w n="24.3">les</w> <w n="24.4">rouges</w> <w n="24.5">tueries</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Créer</w> <w n="25.2">des</w> <w n="25.3">êtres</w>, <w n="25.4">et</w> <w n="25.5">leur</w> <w n="25.6">dire</w> :</l>
					<l n="26" num="7.2"><w n="26.1">Misérables</w>, <w n="26.2">c</w>’<w n="26.3">est</w> <w n="26.4">votre</w> <w n="26.5">tour</w> !</l>
					<l n="27" num="7.3"><space quantity="12" unit="char"></space><w n="27.1">Que</w> <w n="27.2">l</w>’<w n="27.3">Amour</w></l>
					<l n="28" num="7.4"><w n="28.1">De</w> <w n="28.2">sa</w> <w n="28.3">folle</w> <w n="28.4">main</w> <w n="28.5">vous</w> <w n="28.6">déchire</w> ;</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Enfin</w> <w n="29.2">pour</w> <w n="29.3">ce</w> <w n="29.4">monde</w> <w n="29.5">risible</w></l>
					<l n="30" num="8.2"><w n="30.1">Forçant</w> <w n="30.2">la</w> <w n="30.3">couleur</w> <w n="30.4">à</w> <w n="30.5">chanter</w>,</l>
					<l n="31" num="8.3"><space quantity="12" unit="char"></space><w n="31.1">L</w>’<w n="31.2">enchanter</w></l>
					<l n="32" num="8.4"><w n="32.1">Par</w> <w n="32.2">une</w> <w n="32.3">musique</w> <w n="32.4">visible</w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Voilà</w> <w n="33.2">vraiment</w> <w n="33.3">ce</w> <w n="33.4">que</w> <w n="33.5">vous</w> <w n="33.6">faites</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Peintres</w> ! <w n="34.2">qui</w> <w n="34.3">pour</w> <w n="34.4">nous</w> <w n="34.5">préparez</w></l>
					<l n="35" num="9.3"><space quantity="12" unit="char"></space><w n="35.1">Et</w> <w n="35.2">parez</w></l>
					<l n="36" num="9.4"><w n="36.1">Sans</w> <w n="36.2">repos</w> <w n="36.3">d</w>’<w n="36.4">éternelles</w> <w n="36.5">fêtes</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Ouvriers</w>, <w n="37.2">inventeurs</w>, <w n="37.3">génies</w> !</l>
					<l n="38" num="10.2"><w n="38.1">Par</w> <w n="38.2">un</w> <w n="38.3">miracle</w> <w n="38.4">surhumain</w>,</l>
					<l n="39" num="10.3"><space quantity="12" unit="char"></space><w n="39.1">Votre</w> <w n="39.2">main</w></l>
					<l n="40" num="10.4"><w n="40.1">Réalise</w> <w n="40.2">ces</w> <w n="40.3">harmonies</w></l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Où</w> <w n="41.2">la</w> <w n="41.3">couleur</w> <w n="41.4">qui</w> <w n="41.5">se</w> <w n="41.6">déploie</w></l>
					<l n="42" num="11.2"><w n="42.1">En</w> <w n="42.2">accords</w> <w n="42.3">de</w> <w n="42.4">la</w> <w n="42.5">nuit</w> <w n="42.6">vainqueurs</w>,</l>
					<l n="43" num="11.3"><space quantity="12" unit="char"></space><w n="43.1">Dans</w> <w n="43.2">nos</w> <w n="43.3">cœurs</w></l>
					<l n="44" num="11.4"><w n="44.1">Fait</w> <w n="44.2">jaillir</w> <w n="44.3">des</w> <w n="44.4">sources</w> <w n="44.5">de</w> <w n="44.6">joie</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Et</w> <w n="45.2">nos</w> <w n="45.3">fronts</w> <w n="45.4">sont</w> <w n="45.5">baignés</w> <w n="45.6">d</w>’<w n="45.7">aurore</w>.</l>
					<l n="46" num="12.2"><w n="46.1">Mais</w> <w n="46.2">vous</w>, <w n="46.3">par</w> <w n="46.4">un</w> <w n="46.5">retour</w> <w n="46.6">fatal</w>,</l>
					<l n="47" num="12.3"><space quantity="12" unit="char"></space><w n="47.1">L</w>’<w n="47.2">Idéal</w></l>
					<l n="48" num="12.4"><w n="48.1">Vous</w> <w n="48.2">martyrise</w> <w n="48.3">et</w> <w n="48.4">vous</w> <w n="48.5">dévore</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">vos</w> <w n="49.3">enchantements</w> <w n="49.4">sublimes</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Vous</w> <w n="50.2">les</w> <w n="50.3">payez</w> <w n="50.4">de</w> <w n="50.5">votre</w> <w n="50.6">chair</w> ;</l>
					<l n="51" num="13.3"><space quantity="12" unit="char"></space><w n="51.1">Il</w> <w n="51.2">est</w> <w n="51.3">cher</w>,</l>
					<l n="52" num="13.4"><w n="52.1">Le</w> <w n="52.2">feu</w> <w n="52.3">qu</w>’<w n="52.4">on</w> <w n="52.5">vole</w> <w n="52.6">sur</w> <w n="52.7">les</w> <w n="52.8">cimes</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Si</w> <w n="53.2">tu</w> <w n="53.3">montas</w> <w n="53.4">avec</w> <w n="53.5">délice</w></l>
					<l n="54" num="14.2"><w n="54.1">L</w>’<w n="54.2">escalier</w> <w n="54.3">bleu</w> <w n="54.4">des</w> <w n="54.5">paradis</w></l>
					<l n="55" num="14.3"><space quantity="12" unit="char"></space><w n="55.1">Interdits</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Un</w> <w n="56.2">inexprimable</w> <w n="56.3">supplice</w></l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Te</w> <w n="57.2">punit</w>, <w n="57.3">ô</w> <w n="57.4">rêveur</w> <w n="57.5">étrange</w></l>
					<l n="58" num="15.2"><w n="58.1">Qui</w> <w n="58.2">sus</w> <w n="58.3">donner</w> <w n="58.4">l</w>’<w n="58.5">illusion</w></l>
					<l n="59" num="15.3"><space quantity="12" unit="char"></space><w n="59.1">Du</w> <w n="59.2">rayon</w></l>
					<l n="60" num="15.4"><w n="60.1">De</w> <w n="60.2">lumière</w> <w n="60.3">où</w> <w n="60.4">s</w>’<w n="60.5">envole</w> <w n="60.6">un</w> <w n="60.7">Ange</w> ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Et</w> <w n="61.2">lorsque</w> <w n="61.3">tout</w> <w n="61.4">le</w> <w n="61.5">ciel</w> <w n="61.6">flamboie</w></l>
					<l n="62" num="16.2"><w n="62.1">Dans</w> <w n="62.2">ta</w> <w n="62.3">prunelle</w> <w n="62.4">ivre</w> <w n="62.5">d</w>’<w n="62.6">amour</w>,</l>
					<l n="63" num="16.3"><space quantity="12" unit="char"></space><w n="63.1">Un</w> <w n="63.2">vautour</w></l>
					<l n="64" num="16.4"><w n="64.1">Vient</w> <w n="64.2">manger</w> <w n="64.3">ton</w> <w n="64.4">cœur</w> <w n="64.5">et</w> <w n="64.6">ton</w> <w n="64.7">foie</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1872">24 novembre 1872.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>