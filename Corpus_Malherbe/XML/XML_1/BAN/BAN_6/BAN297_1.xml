<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN297">
				<head type="main">A Léon Gatayès</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Avec</w> <w n="1.2">ses</w> <w n="1.3">sanglots</w>, <w n="1.4">l</w>’<w n="1.5">instrument</w> <w n="1.6">rebelle</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">sent</w> <w n="2.3">un</w> <w n="2.4">pouvoir</w> <w n="2.5">plus</w> <w n="2.6">fort</w> <w n="2.7">que</w> <w n="2.8">le</w> <w n="2.9">sien</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Donne</w> <w n="3.2">l</w>’<w n="3.3">harmonie</w> <w n="3.4">enivrante</w> <w n="3.5">et</w> <w n="3.6">belle</w></l>
					<l n="4" num="1.4"><space quantity="12" unit="char"></space><w n="4.1">Au</w> <w n="4.2">musicien</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">cheval</w> <w n="5.3">meurtri</w>, <w n="5.4">qui</w> <w n="5.5">saigne</w> <w n="5.6">et</w> <w n="5.7">qui</w> <w n="5.8">pleure</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Cède</w> <w n="6.2">au</w> <w n="6.3">cavalier</w>, <w n="6.4">rare</w> <w n="6.5">parmi</w> <w n="6.6">nous</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Dont</w> <w n="7.2">aucun</w> <w n="7.3">effort</w> <w n="7.4">ne</w> <w n="7.5">peut</w> <w n="7.6">avant</w> <w n="7.7">l</w>’<w n="7.8">heure</w></l>
					<l n="8" num="2.4"><space quantity="12" unit="char"></space><w n="8.1">Lasser</w> <w n="8.2">les</w> <w n="8.3">genoux</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">De</w> <w n="9.2">même</w> <w n="9.3">d</w>’<w n="9.4">abord</w>, <w n="9.5">le</w> <w n="9.6">Rhythme</w> <w n="9.7">farouche</w></l>
					<l n="10" num="3.2"><w n="10.1">Devant</w> <w n="10.2">la</w> <w n="10.3">Pensée</w> <w n="10.4">écume</w> <w n="10.5">d</w>’<w n="10.6">horreur</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">pour</w> <w n="11.3">se</w> <w n="11.4">soustraire</w> <w n="11.5">au</w> <w n="11.6">dieu</w> <w n="11.7">qui</w> <w n="11.8">le</w> <w n="11.9">touche</w>,</l>
					<l n="12" num="3.4"><space quantity="12" unit="char"></space><w n="12.1">Se</w> <w n="12.2">cabre</w> <w n="12.3">en</w> <w n="12.4">fureur</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Mais</w> <w n="13.2">bientôt</w>, <w n="13.3">léchant</w> <w n="13.4">la</w> <w n="13.5">main</w> <w n="13.6">qui</w> <w n="13.7">l</w>’<w n="13.8">opprime</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Il</w> <w n="14.2">marche</w> <w n="14.3">en</w> <w n="14.4">cadence</w>, <w n="14.5">et</w> <w n="14.6">comme</w> <w n="14.7">par</w> <w n="14.8">jeu</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Son</w> <w n="15.2">vainqueur</w> <w n="15.3">lui</w> <w n="15.4">met</w> <w n="15.5">le</w> <w n="15.6">mors</w> <w n="15.7">de</w> <w n="15.8">la</w> <w n="15.9">Rime</w></l>
					<l n="16" num="4.4"><space quantity="12" unit="char"></space><w n="16.1">Dans</w> <w n="16.2">sa</w> <w n="16.3">bouche</w> <w n="16.4">en</w> <w n="16.5">feu</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Tu</w> <w n="17.2">le</w> <w n="17.3">sais</w>, <w n="17.4">ami</w>, <w n="17.5">toi</w> <w n="17.6">dont</w> <w n="17.7">l</w>’<w n="17.8">Art</w> <w n="17.9">s</w>’<w n="17.10">honore</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Homme</w> <w n="18.2">à</w> <w n="18.3">la</w> <w n="18.4">main</w> <w n="18.5">souple</w>, <w n="18.6">au</w> <w n="18.7">jarret</w> <w n="18.8">d</w>’<w n="18.9">acier</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Qui</w> <w n="19.2">fais</w> <w n="19.3">obéir</w> <w n="19.4">la</w> <w n="19.5">harpe</w> <w n="19.6">sonore</w></l>
					<l n="20" num="5.4"><space quantity="12" unit="char"></space><w n="20.1">Et</w> <w n="20.2">l</w>’<w n="20.3">ardent</w> <w n="20.4">coursier</w> ;</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Lorsque</w> <w n="21.2">aimé</w> <w n="21.3">d</w>’<w n="21.4">Isis</w> <w n="21.5">aux</w> <w n="21.6">triples</w> <w n="21.7">ceintures</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Un</w> <w n="22.2">homme</w> <w n="22.3">intrépide</w> <w n="22.4">a</w> <w n="22.5">baisé</w> <w n="22.6">son</w> <w n="22.7">sein</w>,</l>
					<l n="23" num="6.3"><w n="23.1">La</w> <w n="23.2">création</w> <w n="23.3">et</w> <w n="23.4">les</w> <w n="23.5">créatures</w></l>
					<l n="24" num="6.4"><space quantity="12" unit="char"></space><w n="24.1">Suivent</w> <w n="24.2">son</w> <w n="24.3">dessein</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Le</w> <w n="25.2">Génie</w> <w n="25.3">en</w> <w n="25.4">feu</w> <w n="25.5">donne</w> <w n="25.6">à</w> <w n="25.7">l</w>’<w n="25.8">âme</w> <w n="25.9">altière</w></l>
					<l n="26" num="7.2"><w n="26.1">Le</w> <w n="26.2">Commandement</w>, <w n="26.3">ce</w> <w n="26.4">charme</w> <w n="26.5">vanté</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">l</w>’<w n="27.3">Esprit</w> <w n="27.4">captif</w> <w n="27.5">dans</w> <w n="27.6">l</w>’<w n="27.7">âpre</w> <w n="27.8">Matière</w></l>
					<l n="28" num="7.4"><space quantity="12" unit="char"></space><w n="28.1">Cède</w> <w n="28.2">épouvanté</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>