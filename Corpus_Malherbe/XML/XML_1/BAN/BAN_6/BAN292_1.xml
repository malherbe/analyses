<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN292">
				<head type="main">A Charles Asselineau</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Vainement</w> <w n="1.2">tu</w> <w n="1.3">lui</w> <w n="1.4">fais</w> <w n="1.5">affront</w>,</l>
					<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">Votre</w> <w n="2.2">brouille</w> <w n="2.3">m</w>’<w n="2.4">amuse</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Car</w> <w n="3.2">je</w> <w n="3.3">reconnais</w> <w n="3.4">sur</w> <w n="3.5">ton</w> <w n="3.6">front</w></l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">Le</w> <w n="4.2">baiser</w> <w n="4.3">de</w> <w n="4.4">la</w> <w n="4.5">Muse</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Tout</w> <w n="5.2">est</w> <w n="5.3">fini</w>, <w n="5.4">si</w> <w n="5.5">tu</w> <w n="5.6">le</w> <w n="5.7">veux</w> ;</l>
					<l n="6" num="2.2"><space quantity="4" unit="char"></space><w n="6.1">Mais</w> <w n="6.2">que</w> <w n="6.3">le</w> <w n="6.4">vent</w> <w n="6.5">les</w> <w n="6.6">bouge</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Vite</w> <w n="7.2">on</w> <w n="7.3">le</w> <w n="7.4">voit</w> <w n="7.5">sous</w> <w n="7.6">tes</w> <w n="7.7">cheveux</w>,</l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space><w n="8.1">La</w> <w n="8.2">place</w> <w n="8.3">est</w> <w n="8.4">encor</w> <w n="8.5">rouge</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Tu</w> <w n="9.2">fuis</w> <w n="9.3">le</w> <w n="9.4">bois</w> <w n="9.5">des</w> <w n="9.6">lauriers</w> <w n="9.7">verts</w></l>
					<l n="10" num="3.2"><space quantity="4" unit="char"></space><w n="10.1">Et</w> <w n="10.2">la</w> <w n="10.3">troupe</w> <w n="10.4">des</w> <w n="10.5">cygnes</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">pour</w> <w n="11.3">mieux</w> <w n="11.4">laisser</w> <w n="11.5">l</w>’<w n="11.6">art</w> <w n="11.7">des</w> <w n="11.8">vers</w></l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space><w n="12.1">A</w> <w n="12.2">des</w> <w n="12.3">chanteurs</w> <w n="12.4">plus</w> <w n="12.5">dignes</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Tu</w> <w n="13.2">ne</w> <w n="13.3">t</w>’<w n="13.4">égares</w> <w n="13.5">plus</w> <w n="13.6">jamais</w></l>
					<l n="14" num="4.2"><space quantity="4" unit="char"></space><w n="14.1">Sous</w> <w n="14.2">la</w> <w n="14.3">lune</w> <w n="14.4">blafarde</w>.</l>
					<l n="15" num="4.3"><w n="15.1">La</w> <w n="15.2">modestie</w> <w n="15.3">est</w> <w n="15.4">bonne</w>, <w n="15.5">mais</w></l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space><w n="16.1">Cette</w> <w n="16.2">fois</w> <w n="16.3">prends</w>-<w n="16.4">y</w> <w n="16.5">garde</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Par</w> <w n="17.2">ces</w> <w n="17.3">scrupules</w> <w n="17.4">obligeants</w>,</l>
					<l n="18" num="5.2"><space quantity="4" unit="char"></space><w n="18.1">Trop</w> <w n="18.2">souvent</w> <w n="18.3">on</w> <w n="18.4">condamne</w></l>
					<l n="19" num="5.3"><w n="19.1">La</w> <w n="19.2">fée</w> <w n="19.3">amoureuse</w> <w n="19.4">à</w> <w n="19.5">des</w> <w n="19.6">gens</w></l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space><w n="20.1">Coiffés</w> <w n="20.2">de</w> <w n="20.3">têtes</w> <w n="20.4">d</w>’<w n="20.5">âne</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Firdusi</w> <w n="21.2">ne</w> <w n="21.3">vit</w> <w n="21.4">plus</w> <w n="21.5">à</w> <w n="21.6">Thus</w> !</l>
					<l n="22" num="6.2"><space quantity="4" unit="char"></space><w n="22.1">Toutes</w> <w n="22.2">les</w> <w n="22.3">nuits</w> <w n="22.4">un</w> <w n="22.5">ange</w></l>
					<l n="23" num="6.3"><w n="23.1">Vient</w> <w n="23.2">baiser</w> <w n="23.3">les</w> <w n="23.4">fleurs</w> <w n="23.5">de</w> <w n="23.6">lotus</w></l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space><w n="24.1">Aux</w> <w n="24.2">bords</w> <w n="24.3">sacrés</w> <w n="24.4">du</w> <w n="24.5">Gange</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">L</w>’<w n="25.2">hyacinthe</w> <w n="25.3">frissonne</w> <w n="25.4">encor</w></l>
					<l n="26" num="7.2"><space quantity="4" unit="char"></space><w n="26.1">Dans</w> <w n="26.2">les</w> <w n="26.3">clairières</w> <w n="26.4">lisses</w> ;</l>
					<l n="27" num="7.3"><w n="27.1">Toujours</w>, <w n="27.2">faisant</w> <w n="27.3">du</w> <w n="27.4">soleil</w> <w n="27.5">d</w>’<w n="27.6">or</w></l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space><w n="28.1">Les</w> <w n="28.2">plus</w> <w n="28.3">chères</w> <w n="28.4">délices</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">La</w> <w n="29.2">rose</w> <w n="29.3">à</w> <w n="29.4">sa</w> <w n="29.5">douce</w> <w n="29.6">senteur</w></l>
					<l n="30" num="8.2"><space quantity="4" unit="char"></space><w n="30.1">Enivre</w> <w n="30.2">Polymnie</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Mais</w> <w n="31.2">je</w> <w n="31.3">connais</w> <w n="31.4">plus</w> <w n="31.5">d</w>’<w n="31.6">un</w> <w n="31.7">auteur</w></l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space><w n="32.1">Qui</w> <w n="32.2">n</w>’<w n="32.3">a</w> <w n="32.4">pas</w> <w n="32.5">de</w> <w n="32.6">génie</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Viens</w> ! <w n="33.2">ne</w> <w n="33.3">laisse</w> <w n="33.4">pas</w> <w n="33.5">galamment</w></l>
					<l n="34" num="9.2"><space quantity="4" unit="char"></space><w n="34.1">Notre</w> <w n="34.2">gentille</w> <w n="34.3">escrime</w></l>
					<l n="35" num="9.3"><w n="35.1">Aux</w> <w n="35.2">sots</w>, <w n="35.3">privés</w> <w n="35.4">également</w></l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space><w n="36.1">De</w> <w n="36.2">raison</w> <w n="36.3">et</w> <w n="36.4">de</w> <w n="36.5">rime</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Au</w> <w n="37.2">moins</w>, <w n="37.3">reprends</w> <w n="37.4">notre</w> <w n="37.5">lien</w></l>
					<l n="38" num="10.2"><space quantity="4" unit="char"></space><w n="38.1">Pour</w> <w n="38.2">une</w> <w n="38.3">année</w> <w n="38.4">entière</w> !</l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">d</w>’<w n="39.3">ailleurs</w>, <w n="39.4">ami</w>, <w n="39.5">tu</w> <w n="39.6">peux</w> <w n="39.7">bien</w></l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space><w n="40.1">Chez</w> <w n="40.2">le</w> <w n="40.3">vieux</w> <w n="40.4">Furetière</w></l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Errer</w> <w n="41.2">comme</w> <w n="41.3">en</w> <w n="41.4">un</w> <w n="41.5">Sahara</w> ;</l>
					<l n="42" num="11.2"><space quantity="4" unit="char"></space><w n="42.1">Acheter</w> <w n="42.2">et</w> <w n="42.3">revendre</w></l>
					<l n="43" num="11.3"><w n="43.1">Des</w> <w n="43.2">bouquins</w> ; <w n="43.3">Érato</w> <w n="43.4">saura</w></l>
					<l n="44" num="11.4"><space quantity="4" unit="char"></space><w n="44.1">Toujours</w> <w n="44.2">où</w> <w n="44.3">te</w> <w n="44.4">reprendre</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Au</w> <w n="45.2">mois</w> <w n="45.3">où</w> <w n="45.4">s</w>’<w n="45.5">ouvrent</w> <w n="45.6">les</w> <w n="45.7">boutons</w>,</l>
					<l n="46" num="12.2"><space quantity="4" unit="char"></space><w n="46.1">Tous</w> <w n="46.2">ceux</w> <w n="46.3">qui</w> <w n="46.4">l</w>’<w n="46.5">ont</w> <w n="46.6">aimée</w></l>
					<l n="47" num="12.3"><w n="47.1">Reviennent</w> <w n="47.2">comme</w> <w n="47.3">des</w> <w n="47.4">moutons</w></l>
					<l n="48" num="12.4"><space quantity="4" unit="char"></space><w n="48.1">Sur</w> <w n="48.2">sa</w> <w n="48.3">trace</w> <w n="48.4">charmée</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Or</w>, <w n="49.2">justement</w>, <w n="49.3">pris</w> <w n="49.4">à</w> <w n="49.5">l</w>’<w n="49.6">attrait</w></l>
					<l n="50" num="13.2"><space quantity="4" unit="char"></space><w n="50.1">De</w> <w n="50.2">mes</w> <w n="50.3">rimes</w> <w n="50.4">prolixes</w>,</l>
					<l n="51" num="13.3"><w n="51.1">J</w>’<w n="51.2">entends</w> <w n="51.3">errer</w> <w n="51.4">dans</w> <w n="51.5">la</w> <w n="51.6">forêt</w></l>
					<l n="52" num="13.4"><space quantity="4" unit="char"></space><w n="52.1">Les</w> <w n="52.2">elfes</w> <w n="52.3">et</w> <w n="52.4">les</w> <w n="52.5">nixes</w> ;</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Et</w>, <w n="53.2">dans</w> <w n="53.3">le</w> <w n="53.4">parc</w> <w n="53.5">où</w> <w n="53.6">nous</w> <w n="53.7">songeons</w>,</l>
					<l n="54" num="14.2"><space quantity="4" unit="char"></space><w n="54.1">La</w> <w n="54.2">sève</w>, <w n="54.3">dont</w> <w n="54.4">la</w> <w n="54.5">force</w></l>
					<l n="55" num="14.3"><w n="55.1">Croît</w>, <w n="55.2">gonfle</w> <w n="55.3">déjà</w> <w n="55.4">les</w> <w n="55.5">bourgeons</w></l>
					<l n="56" num="14.4"><space quantity="4" unit="char"></space><w n="56.1">Prêts</w> <w n="56.2">à</w> <w n="56.3">rompre</w> <w n="56.4">l</w>’<w n="56.5">écorce</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>