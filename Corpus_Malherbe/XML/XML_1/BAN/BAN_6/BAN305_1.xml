<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN305">
				<head type="main">A un riche</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ma</w> <w n="1.2">foi</w>, <w n="1.3">vous</w> <w n="1.4">avez</w> <w n="1.5">bien</w> <w n="1.6">raison</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Vous</w> <w n="2.2">pour</w> <w n="2.3">qui</w> <w n="2.4">tout</w> <w n="2.5">est</w> <w n="2.6">floraison</w></l>
					<l n="3" num="1.3"><space quantity="12" unit="char"></space><w n="3.1">Et</w> <w n="3.2">violettes</w></l>
					<l n="4" num="1.4"><w n="4.1">Parfumant</w> <w n="4.2">les</w> <w n="4.3">pieds</w> <w n="4.4">de</w> <w n="4.5">vos</w> <w n="4.6">lys</w>,</l>
					<l n="5" num="1.5"><w n="5.1">De</w> <w n="5.2">ne</w> <w n="5.3">pas</w> <w n="5.4">célébrer</w> <w n="5.5">Phyllis</w></l>
					<l n="6" num="1.6"><space quantity="12" unit="char"></space><w n="6.1">En</w> <w n="6.2">odelettes</w>.</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><w n="7.1">Vous</w> <w n="7.2">qui</w> <w n="7.3">pouvez</w> <w n="7.4">chaque</w> <w n="7.5">matin</w>,</l>
					<l n="8" num="2.2"><w n="8.1">Bercé</w> <w n="8.2">par</w> <w n="8.3">le</w> <w n="8.4">flot</w> <w n="8.5">de</w> <w n="8.6">satin</w></l>
					<l n="9" num="2.3"><space quantity="12" unit="char"></space><w n="9.1">Qui</w> <w n="9.2">vous</w> <w n="9.3">arrose</w>,</l>
					<l n="10" num="2.4"><w n="10.1">Voir</w> <w n="10.2">dans</w> <w n="10.3">l</w>’<w n="10.4">or</w> <w n="10.5">de</w> <w n="10.6">votre</w> <w n="10.7">salon</w></l>
					<l n="11" num="2.5"><w n="11.1">Tomber</w> <w n="11.2">les</w> <w n="11.3">flèches</w> <w n="11.4">d</w>’<w n="11.5">Apollon</w>,</l>
					<l n="12" num="2.6"><space quantity="12" unit="char"></space><w n="12.1">Parlez</w> <w n="12.2">en</w> <w n="12.3">prose</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Mais</w> <w n="13.2">pour</w> <w n="13.3">nous</w> <w n="13.4">qui</w>, <w n="13.5">jusqu</w>’<w n="13.6">à</w> <w n="13.7">présent</w>,</l>
					<l n="14" num="3.2"><w n="14.1">Soupons</w> <w n="14.2">sous</w> <w n="14.3">la</w> <w n="14.4">treille</w> <w n="14.5">en</w> <w n="14.6">causant</w></l>
					<l n="15" num="3.3"><space quantity="12" unit="char"></space><w n="15.1">Avec</w> <w n="15.2">la</w> <w n="15.3">lune</w>,</l>
					<l n="16" num="3.4">(<w n="16.1">Et</w> <w n="16.2">c</w>’<w n="16.3">est</w> <w n="16.4">notre</w> <w n="16.5">meilleur</w> <w n="16.6">repas</w> !)</l>
					<l n="17" num="3.5"><w n="17.1">Ami</w>, <w n="17.2">ne</w> <w n="17.3">nous</w> <w n="17.4">enlevez</w> <w n="17.5">pas</w></l>
					<l n="18" num="3.6"><space quantity="12" unit="char"></space><w n="18.1">Notre</w> <w n="18.2">fortune</w>.</l>
				</lg>
				<lg n="4">
					<l n="19" num="4.1"><w n="19.1">Dans</w> <w n="19.2">les</w> <w n="19.3">fleurs</w>, <w n="19.4">près</w> <w n="19.5">de</w> <w n="19.6">frais</w> <w n="19.7">bassins</w>,</l>
					<l n="20" num="4.2"><w n="20.1">Nous</w> <w n="20.2">nous</w> <w n="20.3">couchons</w> <w n="20.4">sur</w> <w n="20.5">des</w> <w n="20.6">coussins</w></l>
					<l n="21" num="4.3"><space quantity="12" unit="char"></space><w n="21.1">Très</w> <w n="21.2">prosaïques</w>,</l>
					<l n="22" num="4.4"><w n="22.1">La</w> <w n="22.2">pourpre</w> <w n="22.3">au</w> <w n="22.4">dos</w>, <w n="22.5">vous</w> <w n="22.6">le</w> <w n="22.7">savez</w> !</l>
					<l n="23" num="4.5"><w n="23.1">Et</w> <w n="23.2">dans</w> <w n="23.3">des</w> <w n="23.4">bains</w> <w n="23.5">de</w> <w n="23.6">stuc</w> <w n="23.7">pavés</w></l>
					<l n="24" num="4.6"><space quantity="12" unit="char"></space><w n="24.1">De</w> <w n="24.2">mosaïques</w>.</l>
				</lg>
				<lg n="5">
					<l n="25" num="5.1"><w n="25.1">Le</w> <w n="25.2">col</w> <w n="25.3">paré</w> <w n="25.4">de</w> <w n="25.5">nos</w> <w n="25.6">présents</w>,</l>
					<l n="26" num="5.2"><w n="26.1">De</w> <w n="26.2">belles</w> <w n="26.3">filles</w> <w n="26.4">de</w> <w n="26.5">seize</w> <w n="26.6">ans</w></l>
					<l n="27" num="5.3"><space quantity="12" unit="char"></space><w n="27.1">Nous</w> <w n="27.2">versent</w> <w n="27.3">même</w></l>
					<l n="28" num="5.4"><w n="28.1">Avec</w> <w n="28.2">le</w> <w n="28.3">charme</w> <w n="28.4">oriental</w>,</l>
					<l n="29" num="5.5"><w n="29.1">Le</w> <w n="29.2">vin</w> <w n="29.3">du</w> <w n="29.4">Rhin</w> <w n="29.5">dans</w> <w n="29.6">ton</w> <w n="29.7">cristal</w>,</l>
					<l n="30" num="5.6"><space quantity="12" unit="char"></space><w n="30.1">Sainte</w> <w n="30.2">Bohême</w> !</l>
				</lg>
				<lg n="6">
					<l n="31" num="6.1"><w n="31.1">O</w> <w n="31.2">nuit</w> <w n="31.3">d</w>’<w n="31.4">étoiles</w> <w n="31.5">sous</w> <w n="31.6">les</w> <w n="31.7">cieux</w> !</l>
					<l n="32" num="6.2"><w n="32.1">Jardins</w>, <w n="32.2">nectar</w> <w n="32.3">délicieux</w>,</l>
					<l n="33" num="6.3"><space quantity="12" unit="char"></space><w n="33.1">Voûte</w> <w n="33.2">sublime</w> !</l>
					<l n="34" num="6.4"><w n="34.1">Nous</w> <w n="34.2">les</w> <w n="34.3">possédons</w> <w n="34.4">en</w> <w n="34.5">effet</w>,</l>
					<l n="35" num="6.5"><w n="35.1">Mais</w>, <w n="35.2">hélas</w> ! <w n="35.3">ce</w> <w n="35.4">beau</w> <w n="35.5">monde</w> <w n="35.6">est</w> <w n="35.7">fait</w></l>
					<l n="36" num="6.6"><space quantity="12" unit="char"></space><w n="36.1">Avec</w> <w n="36.2">la</w> <w n="36.3">rime</w>.</l>
				</lg>
				<lg n="7">
					<l n="37" num="7.1"><w n="37.1">Sans</w> <w n="37.2">elle</w> <w n="37.3">et</w> <w n="37.4">ses</w> <w n="37.5">prismes</w> <w n="37.6">fleuris</w>,</l>
					<l n="38" num="7.2"><w n="38.1">Pour</w> <w n="38.2">pouvoir</w> <w n="38.3">chercher</w> <w n="38.4">hors</w> <w n="38.5">Paris</w></l>
					<l n="39" num="7.3"><space quantity="12" unit="char"></space><w n="39.1">L</w>’<w n="39.2">eau</w> <w n="39.3">murmurante</w></l>
					<l n="40" num="7.4"><w n="40.1">Qui</w> <w n="40.2">court</w> <w n="40.3">dans</w> <w n="40.4">les</w> <w n="40.5">gazons</w> <w n="40.6">naissants</w>,</l>
					<l n="41" num="7.5"><w n="41.1">Il</w> <w n="41.2">nous</w> <w n="41.3">faudrait</w> <w n="41.4">bien</w> <w n="41.5">quatre</w> <w n="41.6">cents</w></l>
					<l n="42" num="7.6"><space quantity="12" unit="char"></space><w n="42.1">Écus</w> <w n="42.2">de</w> <w n="42.3">rente</w> !</l>
				</lg>
				<lg n="8">
					<l n="43" num="8.1"><w n="43.1">Ou</w>, <w n="43.2">je</w> <w n="43.3">frissonne</w> <w n="43.4">d</w>’<w n="43.5">y</w> <w n="43.6">penser</w> !</l>
					<l n="44" num="8.2"><w n="44.1">Nous</w> <w n="44.2">n</w>’<w n="44.3">oserions</w> <w n="44.4">pas</w> <w n="44.5">nous</w> <w n="44.6">passer</w></l>
					<l n="45" num="8.3"><space quantity="12" unit="char"></space><w n="45.1">La</w> <w n="45.2">fantaisie</w></l>
					<l n="46" num="8.4"><w n="46.1">De</w> <w n="46.2">perdre</w> <w n="46.3">un</w> <w n="46.4">quart</w> <w n="46.5">d</w>’<w n="46.6">heure</w> <w n="46.7">aux</w> <w n="46.8">genoux</w></l>
					<l n="47" num="8.5"><w n="47.1">De</w> <w n="47.2">Cidalise</w>. <w n="47.3">Ah</w> ! <w n="47.4">laissez</w>-<w n="47.5">nous</w></l>
					<l n="48" num="8.6"><space quantity="12" unit="char"></space><w n="48.1">La</w> <w n="48.2">poésie</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1855">Mai 1855.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>