<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN315">
				<head type="main">Les Muses au Tombeau</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Près</w> <w n="1.2">de</w> <w n="1.3">la</w> <w n="1.4">pierre</w> <w n="1.5">close</w></l>
					<l n="2" num="1.2"><w n="2.1">Sous</w> <w n="2.2">laquelle</w> <w n="2.3">repose</w></l>
					<l n="3" num="1.3"><w n="3.1">Théophile</w> <w n="3.2">Gautier</w>,</l>
					<l n="4" num="1.4"><space quantity="4" unit="char"></space>(<w n="4.1">Non</w> <w n="4.2">tout</w> <w n="4.3">entier</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Car</w> <w n="5.2">par</w> <w n="5.3">son</w> <w n="5.4">œuvre</w> <w n="5.5">altière</w></l>
					<l n="6" num="2.2"><w n="6.1">Ce</w> <w n="6.2">dompteur</w> <w n="6.3">de</w> <w n="6.4">matière</w></l>
					<l n="7" num="2.3"><w n="7.1">Est</w> <w n="7.2">comme</w> <w n="7.3">auparavant</w></l>
					<l n="8" num="2.4"><space quantity="4" unit="char"></space><w n="8.1">Toujours</w> <w n="8.2">vivant</w>,)</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Regardant</w> <w n="9.2">cette</w> <w n="9.3">tombe</w></l>
					<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">leurs</w> <w n="10.3">yeux</w> <w n="10.4">de</w> <w n="10.5">colombe</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">Muses</w> <w n="11.3">vont</w> <w n="11.4">pleurant</w></l>
					<l n="12" num="3.4"><space quantity="4" unit="char"></space><w n="12.1">Et</w> <w n="12.2">soupirant</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Toutes</w> <w n="13.2">se</w> <w n="13.3">plaignent</w> : <w n="13.4">celle</w></l>
					<l n="14" num="4.2"><w n="14.1">Dont</w> <w n="14.2">l</w>’<w n="14.3">œil</w> <w n="14.4">sombre</w> <w n="14.5">étincelle</w></l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">qui</w> <w n="15.3">réveille</w> <w n="15.4">encor</w></l>
					<l n="16" num="4.4"><space quantity="4" unit="char"></space><w n="16.1">Le</w> <w n="16.2">clairon</w> <w n="16.3">d</w>’<w n="16.4">or</w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Celle</w> <w n="17.2">que</w> <w n="17.3">le</w> <w n="17.4">délire</w></l>
					<l n="18" num="5.2"><w n="18.1">Effréné</w> <w n="18.2">de</w> <w n="18.3">la</w> <w n="18.4">Lyre</w></l>
					<l n="19" num="5.3"><w n="19.1">Offre</w> <w n="19.2">aux</w> <w n="19.3">jeux</w> <w n="19.4">arrogants</w></l>
					<l n="20" num="5.4"><space quantity="4" unit="char"></space><w n="20.1">Des</w> <w n="20.2">ouragans</w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Celle</w> <w n="21.2">qui</w> <w n="21.3">rend</w> <w n="21.4">docile</w></l>
					<l n="22" num="6.2"><w n="22.1">Un</w> <w n="22.2">mètre</w> <w n="22.3">de</w> <w n="22.4">Sicile</w></l>
					<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">tire</w> <w n="23.3">du</w> <w n="23.4">roseau</w></l>
					<l n="24" num="6.4"><space quantity="4" unit="char"></space><w n="24.1">Des</w> <w n="24.2">chants</w> <w n="24.3">d</w>’<w n="24.4">oiseau</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Celle</w> <w n="25.2">qui</w>, <w n="25.3">dans</w> <w n="25.4">son</w> <w n="25.5">rêve</w></l>
					<l n="26" num="7.2"><w n="26.1">Farouche</w>, <w n="26.2">porte</w> <w n="26.3">un</w> <w n="26.4">glaive</w></l>
					<l n="27" num="7.3"><w n="27.1">Frissonnant</w> <w n="27.2">sur</w> <w n="27.3">son</w> <w n="27.4">flanc</w></l>
					<l n="28" num="7.4"><space quantity="4" unit="char"></space><w n="28.1">Taché</w> <w n="28.2">de</w> <w n="28.3">sang</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">celle</w> <w n="29.3">qui</w> <w n="29.4">se</w> <w n="29.5">joue</w></l>
					<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">pour</w> <w n="30.3">orner</w> <w n="30.4">sa</w> <w n="30.5">joue</w></l>
					<l n="31" num="8.3"><w n="31.1">Prend</w> <w n="31.2">aux</w> <w n="31.3">coteaux</w> <w n="31.4">voisins</w></l>
					<l n="32" num="8.4"><space quantity="4" unit="char"></space><w n="32.1">Les</w> <w n="32.2">noirs</w> <w n="32.3">raisins</w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">la</w> <w n="33.3">plus</w> <w n="33.4">intrépide</w>,</l>
					<l n="34" num="9.2"><w n="34.1">La</w> <w n="34.2">Nymphe</w> <w n="34.3">au</w> <w n="34.4">pied</w> <w n="34.5">rapide</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Celle</w> <w n="35.2">qui</w>, <w n="35.3">sur</w> <w n="35.4">les</w> <w n="35.5">monts</w></l>
					<l n="36" num="9.4"><space quantity="4" unit="char"></space><w n="36.1">Où</w> <w n="36.2">nous</w> <w n="36.3">l</w>’<w n="36.4">aimons</w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Par</w> <w n="37.2">sa</w> <w n="37.3">grâce</w> <w n="37.4">savante</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Fait</w> <w n="38.2">voir</w>, <w n="38.3">chanson</w> <w n="38.4">vivante</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Les</w> <w n="39.2">rhythmes</w> <w n="39.3">clairs</w> <w n="39.4">dansants</w></l>
					<l n="40" num="10.4"><space quantity="4" unit="char"></space><w n="40.1">Et</w> <w n="40.2">bondissants</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Oui</w>, <w n="41.2">toutes</w> <w n="41.3">se</w> <w n="41.4">lamentent</w></l>
					<l n="42" num="11.2"><w n="42.1">Et</w> <w n="42.2">pieusement</w> <w n="42.3">chantent</w></l>
					<l n="43" num="11.3"><w n="43.1">Dans</w> <w n="43.2">l</w>’<w n="43.3">ombre</w> <w n="43.4">où</w> <w n="43.5">leur</w> <w n="43.6">ami</w></l>
					<l n="44" num="11.4"><space quantity="4" unit="char"></space><w n="44.1">S</w>’<w n="44.2">est</w> <w n="44.3">endormi</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Car</w> <w n="45.2">il</w> <w n="45.3">n</w>’<w n="45.4">en</w> <w n="45.5">est</w> <w n="45.6">pas</w> <w n="45.7">une</w></l>
					<l n="46" num="12.2"><w n="46.1">Qui</w> <w n="46.2">n</w>’<w n="46.3">ait</w> <w n="46.4">eu</w> <w n="46.5">la</w> <w n="46.6">fortune</w></l>
					<l n="47" num="12.3"><w n="47.1">D</w>’<w n="47.2">obtenir</w> <w n="47.3">à</w> <w n="47.4">son</w> <w n="47.5">tour</w></l>
					<l n="48" num="12.4"><space quantity="4" unit="char"></space><w n="48.1">Son</w> <w n="48.2">fier</w> <w n="48.3">amour</w> ;</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Pas</w> <w n="49.2">une</w> <w n="49.3">qu</w>’<w n="49.4">en</w> <w n="49.5">sa</w> <w n="49.6">vie</w></l>
					<l n="50" num="13.2"><w n="50.1">Il</w> <w n="50.2">n</w>’<w n="50.3">ait</w> <w n="50.4">prise</w> <w n="50.5">et</w> <w n="50.6">ravie</w></l>
					<l n="51" num="13.3"><w n="51.1">Par</w> <w n="51.2">un</w> <w n="51.3">chant</w> <w n="51.4">immortel</w></l>
					<l n="52" num="13.4"><space quantity="4" unit="char"></space><w n="52.1">Empli</w> <w n="52.2">de</w> <w n="52.3">ciel</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Ses</w> <w n="53.2">pas</w> <w n="53.3">foulaient</w> <w n="53.4">ta</w> <w n="53.5">cime</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Mont</w> <w n="54.2">neigeux</w> <w n="54.3">et</w> <w n="54.4">sublime</w></l>
					<l n="55" num="14.3"><w n="55.1">Où</w> <w n="55.2">nul</w> <w n="55.3">Dieu</w> <w n="55.4">sans</w> <w n="55.5">effroi</w></l>
					<l n="56" num="14.4"><space quantity="4" unit="char"></space><w n="56.1">Ne</w> <w n="56.2">passe</w> ; <w n="56.3">et</w> <w n="56.4">toi</w>,</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Fontaine</w> <w n="57.2">violette</w>,</l>
					<l n="58" num="15.2"><w n="58.1">Il</w> <w n="58.2">a</w> <w n="58.3">vu</w>, <w n="58.4">ce</w> <w n="58.5">poëte</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Errer</w> <w n="59.2">dans</w> <w n="59.3">tes</w> <w n="59.4">ravins</w></l>
					<l n="60" num="15.4"><space quantity="4" unit="char"></space><w n="60.1">Les</w> <w n="60.2">chœurs</w> <w n="60.3">divins</w> !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Et</w> <w n="61.2">toi</w>, <w n="61.3">monstre</w> <w n="61.4">qui</w> <w n="61.5">passes</w></l>
					<l n="62" num="16.2"><w n="62.1">A</w> <w n="62.2">travers</w> <w n="62.3">les</w> <w n="62.4">espaces</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Usant</w> <w n="63.2">ton</w> <w n="63.3">sabot</w> <w n="63.4">sur</w></l>
					<l n="64" num="16.4"><space quantity="4" unit="char"></space><w n="64.1">Les</w> <w n="64.2">cieux</w> <w n="64.3">d</w>’<w n="64.4">azur</w>,</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Cheval</w> <w n="65.2">aux</w> <w n="65.3">ailes</w> <w n="65.4">blanches</w></l>
					<l n="66" num="17.2"><w n="66.1">Comme</w> <w n="66.2">les</w> <w n="66.3">avalanches</w>,</l>
					<l n="67" num="17.3"><w n="67.1">Tu</w> <w n="67.2">prenais</w> <w n="67.3">ton</w> <w n="67.4">vol</w>, <w n="67.5">l</w>’<w n="67.6">œil</w></l>
					<l n="68" num="17.4"><space quantity="4" unit="char"></space><w n="68.1">Ivre</w> <w n="68.2">d</w>’<w n="68.3">orgueil</w>,</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Quand</w> <w n="69.2">sa</w> <w n="69.3">main</w> <w n="69.4">blanche</w> <w n="69.5">et</w> <w n="69.6">nue</w></l>
					<l n="70" num="18.2"><w n="70.1">T</w>’<w n="70.2">empoignait</w> <w n="70.3">sous</w> <w n="70.4">la</w> <w n="70.5">nue</w>,</l>
					<l n="71" num="18.3"><w n="71.1">Ainsi</w> <w n="71.2">que</w> <w n="71.3">tu</w> <w n="71.4">le</w> <w n="71.5">veux</w>,</l>
					<l n="72" num="18.4"><space quantity="4" unit="char"></space><w n="72.1">Par</w> <w n="72.2">les</w> <w n="72.3">cheveux</w> !</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Mais</w>, <w n="73.2">ô</w> <w n="73.3">Déesses</w> <w n="73.4">pures</w>,</l>
					<l n="74" num="19.2"><w n="74.1">Ornez</w> <w n="74.2">vos</w> <w n="74.3">chevelures</w></l>
					<l n="75" num="19.3"><w n="75.1">De</w> <w n="75.2">couronnes</w> <w n="75.3">de</w> <w n="75.4">fleurs</w>,</l>
					<l n="76" num="19.4"><space quantity="4" unit="char"></space><w n="76.1">Séchez</w> <w n="76.2">vos</w> <w n="76.3">pleurs</w> !</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">Car</w> <w n="77.2">le</w> <w n="77.3">divin</w> <w n="77.4">poëte</w></l>
					<l n="78" num="20.2"><w n="78.1">Que</w> <w n="78.2">votre</w> <w n="78.3">voix</w> <w n="78.4">regrette</w></l>
					<l n="79" num="20.3"><w n="79.1">Va</w> <w n="79.2">sortir</w> <w n="79.3">du</w> <w n="79.4">tombeau</w></l>
					<l n="80" num="20.4"><space quantity="4" unit="char"></space><w n="80.1">Joyeux</w> <w n="80.2">et</w> <w n="80.3">beau</w>.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1"><w n="81.1">Les</w> <w n="81.2">Odes</w> <w n="81.3">qu</w>’<w n="81.4">il</w> <w n="81.5">fit</w> <w n="81.6">naître</w></l>
					<l n="82" num="21.2"><w n="82.1">Lui</w> <w n="82.2">redonneront</w> <w n="82.3">l</w>’<w n="82.4">être</w></l>
					<l n="83" num="21.3"><w n="83.1">A</w> <w n="83.2">leur</w> <w n="83.3">tour</w>, <w n="83.4">et</w> <w n="83.5">feront</w></l>
					<l n="84" num="21.4"><space quantity="4" unit="char"></space><w n="84.1">Croître</w> <w n="84.2">à</w> <w n="84.3">son</w> <w n="84.4">front</w></l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1"><w n="85.1">Victorieux</w> <w n="85.2">de</w> <w n="85.3">l</w>’<w n="85.4">ombre</w>,</l>
					<l n="86" num="22.2"><w n="86.1">L</w>’<w n="86.2">illustre</w> <w n="86.3">laurier</w> <w n="86.4">sombre</w></l>
					<l n="87" num="22.3"><w n="87.1">Que</w> <w n="87.2">rien</w> <w n="87.3">ne</w> <w n="87.4">peut</w> <w n="87.5">faner</w></l>
					<l n="88" num="22.4"><space quantity="4" unit="char"></space><w n="88.1">Ni</w> <w n="88.2">profaner</w>.</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1"><w n="89.1">Toujours</w>, <w n="89.2">parmi</w> <w n="89.3">les</w> <w n="89.4">hommes</w>,</l>
					<l n="90" num="23.2"><w n="90.1">Sur</w> <w n="90.2">la</w> <w n="90.3">terre</w> <w n="90.4">où</w> <w n="90.5">nous</w> <w n="90.6">sommes</w></l>
					<l n="91" num="23.3"><w n="91.1">Il</w> <w n="91.2">restera</w> <w n="91.3">vivant</w>,</l>
					<l n="92" num="23.4"><space quantity="4" unit="char"></space><w n="92.1">Maître</w> <w n="92.2">savant</w></l>
				</lg>
				<lg n="24">
					<l n="93" num="24.1"><w n="93.1">De</w> <w n="93.2">l</w>’<w n="93.3">Ode</w> <w n="93.4">cadencée</w>,</l>
					<l n="94" num="24.2"><w n="94.1">Et</w> <w n="94.2">sa</w> <w n="94.3">noble</w> <w n="94.4">pensée</w></l>
					<l n="95" num="24.3"><w n="95.1">Que</w> <w n="95.2">notre</w> <w n="95.3">âge</w> <w n="95.4">adora</w>,</l>
					<l n="96" num="24.4"><space quantity="4" unit="char"></space><w n="96.1">Joyeuse</w>, <w n="96.2">aura</w></l>
				</lg>
				<lg n="25">
					<l n="97" num="25.1"><w n="97.1">Pour</w> <w n="97.2">voler</w> <w n="97.3">sur</w> <w n="97.4">les</w> <w n="97.5">lèvres</w></l>
					<l n="98" num="25.2"><w n="98.1">Que</w> <w n="98.2">brûleront</w> <w n="98.3">les</w> <w n="98.4">fièvres</w></l>
					<l n="99" num="25.3"><w n="99.1">De</w> <w n="99.2">notre</w> <w n="99.3">humanité</w></l>
					<l n="100" num="25.4"><space quantity="4" unit="char"></space><w n="100.1">L</w>’<w n="100.2">éternité</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1872">Jeudi, 7 novembre 1872.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>