<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ODELETTES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1529 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAN_6</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ODELETTES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/banvilleodelettes.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title> Poésies complètes. Les Éxilés. Odelettes, Améthystes, Rimes dorées, Rondels, les Princesses, Trente-six ballades joyeuses.</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier, Eugène Fasquelle, Éditeur</publisher>
							<date when="1878">1878</date>
						</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1856">1856</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN308">
				<head type="main">La Vendangeuse</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Toi</w> <w n="1.2">dont</w> <w n="1.3">les</w> <w n="1.4">cheveux</w> <w n="1.5">doux</w> <w n="1.6">et</w> <w n="1.7">longs</w></l>
					<l n="2" num="1.2"><w n="2.1">Se</w> <w n="2.2">déroulent</w> <w n="2.3">en</w> <w n="2.4">onde</w> <w n="2.5">fière</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">les</w> <w n="3.3">flots</w> <w n="3.4">de</w> <w n="3.5">ta</w> <w n="3.6">rivière</w>,</l>
					<l n="4" num="1.4"><w n="4.1">O</w> <w n="4.2">belle</w> <w n="4.3">fille</w> <w n="4.4">de</w> <w n="4.5">Châlons</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Penche</w> <w n="5.2">ta</w> <w n="5.3">tête</w> <w n="5.4">parfumée</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Que</w> <w n="6.2">je</w> <w n="6.3">puisse</w>, <w n="6.4">ô</w> <w n="6.5">ma</w> <w n="6.6">bien</w>-<w n="6.7">aimée</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Voir</w> <w n="7.2">baigné</w> <w n="7.3">par</w> <w n="7.4">ces</w> <w n="7.5">cheveux</w> <w n="7.6">blonds</w></l>
					<l n="8" num="1.8"><w n="8.1">Ton</w> <w n="8.2">riant</w> <w n="8.3">profil</w> <w n="8.4">de</w> <w n="8.5">camée</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">O</w> <w n="9.2">fille</w> <w n="9.3">d</w>’<w n="9.4">un</w> <w n="9.5">climat</w> <w n="9.6">divin</w> !</l>
					<l n="10" num="2.2"><w n="10.1">Tu</w> <w n="10.2">naquis</w> <w n="10.3">plus</w> <w n="10.4">blanche</w> <w n="10.5">qu</w>’<w n="10.6">un</w> <w n="10.7">cygne</w></l>
					<l n="11" num="2.3"><w n="11.1">Et</w> <w n="11.2">ton</w> <w n="11.3">grand</w>-<w n="11.4">père</w> <w n="11.5">dans</w> <w n="11.6">sa</w> <w n="11.7">vigne</w></l>
					<l n="12" num="2.4"><w n="12.1">Mouilla</w> <w n="12.2">ta</w> <w n="12.3">lèvre</w> <w n="12.4">avec</w> <w n="12.5">du</w> <w n="12.6">vin</w> !</l>
					<l n="13" num="2.5"><w n="13.1">Aussi</w>, <w n="13.2">lorsque</w> <w n="13.3">la</w> <w n="13.4">primevère</w></l>
					<l n="14" num="2.6"><w n="14.1">Triomphe</w> <w n="14.2">du</w> <w n="14.3">climat</w> <w n="14.4">sévère</w>,</l>
					<l n="15" num="2.7"><w n="15.1">Loin</w> <w n="15.2">du</w> <w n="15.3">monde</w> <w n="15.4">vulgaire</w> <w n="15.5">et</w> <w n="15.6">vain</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Vers</w> <w n="16.2">les</w> <w n="16.3">cieux</w> <w n="16.4">tu</w> <w n="16.5">lèves</w> <w n="16.6">ton</w> <w n="16.7">verre</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Toute</w> <w n="17.2">à</w> <w n="17.3">l</w>’<w n="17.4">instant</w> <w n="17.5">qu</w>’<w n="17.6">il</w> <w n="17.7">faut</w> <w n="17.8">saisir</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Tu</w> <w n="18.2">mords</w>, <w n="18.3">et</w> <w n="18.4">d</w>’<w n="18.5">une</w> <w n="18.6">ardeur</w> <w n="18.7">pareille</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Aux</w> <w n="19.2">raisins</w> <w n="19.3">gonflés</w> <w n="19.4">de</w> <w n="19.5">la</w> <w n="19.6">treille</w></l>
					<l n="20" num="3.4"><w n="20.1">Comme</w> <w n="20.2">à</w> <w n="20.3">la</w> <w n="20.4">grappe</w> <w n="20.5">du</w> <w n="20.6">plaisir</w> !</l>
					<l n="21" num="3.5"><w n="21.1">Et</w> <w n="21.2">sur</w> <w n="21.3">ta</w> <w n="21.4">poitrine</w>, <w n="21.5">où</w> <w n="21.6">se</w> <w n="21.7">noie</w></l>
					<l n="22" num="3.6"><w n="22.1">Une</w> <w n="22.2">lumière</w> <w n="22.3">ivre</w> <w n="22.4">de</w> <w n="22.5">joie</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Mûrissent</w> <w n="23.2">les</w> <w n="23.3">fruits</w> <w n="23.4">du</w> <w n="23.5">Désir</w></l>
					<l n="24" num="3.8"><w n="24.1">Comme</w> <w n="24.2">une</w> <w n="24.3">vendange</w> <w n="24.4">qui</w> <w n="24.5">ploie</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">En</w> <w n="25.2">tes</w> <w n="25.3">veines</w>, <w n="25.4">de</w> <w n="25.5">toutes</w> <w n="25.6">parts</w>,</l>
					<l n="26" num="4.2"><w n="26.1">Bourguignonne</w> <w n="26.2">aux</w> <w n="26.3">tresses</w> <w n="26.4">dorées</w>,</l>
					<l n="27" num="4.3"><w n="27.1">Le</w> <w n="27.2">sang</w> <w n="27.3">des</w> <w n="27.4">Bacchantes</w> <w n="27.5">sacrées</w></l>
					<l n="28" num="4.4"><w n="28.1">Bouillonne</w> <w n="28.2">dans</w> <w n="28.3">ton</w> <w n="28.4">sang</w> <w n="28.5">épars</w>,</l>
					<l n="29" num="4.5"><w n="29.1">Et</w> <w n="29.2">tu</w> <w n="29.3">tiens</w> <w n="29.4">tes</w> <w n="29.5">idolâtries</w></l>
					<l n="30" num="4.6"><w n="30.1">De</w> <w n="30.2">ces</w> <w n="30.3">guerrières</w> <w n="30.4">des</w> <w n="30.5">féeries</w></l>
					<l n="31" num="4.7"><w n="31.1">Qui</w> <w n="31.2">conduisaient</w> <w n="31.3">les</w> <w n="31.4">léopards</w></l>
					<l n="32" num="4.8"><w n="32.1">Avec</w> <w n="32.2">des</w> <w n="32.3">guirlandes</w> <w n="32.4">fleuries</w> !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Il</w> <w n="33.2">fut</w> <w n="33.3">ton</w> <w n="33.4">aïeul</w>, <w n="33.5">cet</w> <w n="33.6">amant</w></l>
					<l n="34" num="5.2"><w n="34.1">De</w> <w n="34.2">la</w> <w n="34.3">chanson</w> <w n="34.4">ivre</w> <w n="34.5">et</w> <w n="34.6">sauvage</w>,</l>
					<l n="35" num="5.3"><w n="35.1">Menant</w> <w n="35.2">sur</w> <w n="35.3">son</w> <w n="35.4">char</w> <w n="35.5">de</w> <w n="35.6">feuillage</w>,</l>
					<l n="36" num="5.4"><w n="36.1">Par</w> <w n="36.2">l</w>’<w n="36.3">Attique</w>, <w n="36.4">un</w> <w n="36.5">troupeau</w> <w n="36.6">charmant</w> !</l>
					<l n="37" num="5.5"><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">pourquoi</w>, <w n="37.4">danseuse</w> <w n="37.5">étourdie</w>,</l>
					<l n="38" num="5.6"><w n="38.1">Tu</w> <w n="38.2">fais</w> <w n="38.3">d</w>’<w n="38.4">une</w> <w n="38.5">main</w> <w n="38.6">si</w> <w n="38.7">hardie</w></l>
					<l n="39" num="5.7"><w n="39.1">Carillonner</w> <w n="39.2">joyeusement</w></l>
					<l n="40" num="5.8"><w n="40.1">Les</w> <w n="40.2">grelots</w> <w n="40.3">de</w> <w n="40.4">la</w> <w n="40.5">Comédie</w> !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">O</w> <w n="41.2">vendangeuse</w> ! <w n="41.3">tu</w> <w n="41.4">souris</w>,</l>
					<l n="42" num="6.2"><w n="42.1">Embrassons</w>-<w n="42.2">nous</w> <w n="42.3">jusqu</w>’<w n="42.4">à</w> <w n="42.5">l</w>’<w n="42.6">ivresse</w> !</l>
					<l n="43" num="6.3"><w n="43.1">Buvons</w> <w n="43.2">encore</w>, <w n="43.3">ô</w> <w n="43.4">ma</w> <w n="43.5">maîtresse</w> !</l>
					<l n="44" num="6.4"><w n="44.1">Déroule</w> <w n="44.2">tes</w> <w n="44.3">cheveux</w> <w n="44.4">chéris</w></l>
					<l n="45" num="6.5"><w n="45.1">Sur</w> <w n="45.2">ces</w> <w n="45.3">raisins</w> ! <w n="45.4">car</w>, <w n="45.5">ô</w> <w n="45.6">merveilles</w> !</l>
					<l n="46" num="6.6"><w n="46.1">Tes</w> <w n="46.2">tresses</w> <w n="46.3">blondes</w> <w n="46.4">sont</w> <w n="46.5">pareilles</w></l>
					<l n="47" num="6.7"><w n="47.1">Au</w> <w n="47.2">soleil</w> <w n="47.3">qui</w> <w n="47.4">les</w> <w n="47.5">a</w> <w n="47.6">mûris</w>,</l>
					<l n="48" num="6.8"><w n="48.1">Et</w> <w n="48.2">ta</w> <w n="48.3">bouche</w> <w n="48.4">aux</w> <w n="48.5">grappes</w> <w n="48.6">vermeilles</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1853">Septembre 1853.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>