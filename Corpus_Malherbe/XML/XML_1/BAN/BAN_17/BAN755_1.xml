<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN755">
				<head type="main">Ciels brouillés</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Campagne</w>, <w n="1.2">où</w> <w n="1.3">sur</w> <w n="1.4">le</w> <w n="1.5">cerisier</w></l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">mange</w> <w n="2.3">à</w> <w n="2.4">même</w> <w n="2.5">des</w> <w n="2.6">cerises</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Chez</w> <w n="3.2">toi</w> <w n="3.3">je</w> <w n="3.4">puis</w> <w n="3.5">m</w>’<w n="3.6">extasier</w> !</l>
					<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">le</w> <w n="4.3">ciel</w> <w n="4.4">t</w>’<w n="4.5">en</w> <w n="4.6">fait</w> <w n="4.7">voir</w> <w n="4.8">de</w> <w n="4.9">grises</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">vrai</w>, <w n="5.4">nous</w> <w n="5.5">sommes</w> <w n="5.6">en</w> <w n="5.7">juillet</w></l>
					<l n="6" num="2.2"><w n="6.1">Par</w> <w n="6.2">ce</w> <w n="6.3">temps</w>-<w n="6.4">là</w>, <w n="6.5">sang</w> <w n="6.6">et</w> <w n="6.7">tonnerre</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Voici</w> <w n="7.2">bien</w> <w n="7.3">la</w> <w n="7.4">rose</w> <w n="7.5">et</w> <w n="7.6">l</w>’<w n="7.7">œillet</w>,</l>
					<l n="8" num="2.4"><w n="8.1">O</w> <w n="8.2">vieux</w> <w n="8.3">siècle</w> <w n="8.4">nonagénaire</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">par</w> <w n="9.3">un</w> <w n="9.4">procédé</w> <w n="9.5">nouveau</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Puisque</w>, <w n="10.2">pour</w> <w n="10.3">imiter</w> <w n="10.4">décembre</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">vent</w> <w n="11.3">pleure</w> <w n="11.4">et</w> <w n="11.5">geint</w> <w n="11.6">comme</w> <w n="11.7">un</w> <w n="11.8">veau</w>,</l>
					<l n="12" num="3.4"><w n="12.1">J</w>’<w n="12.2">allume</w> <w n="12.3">un</w> <w n="12.4">grand</w> <w n="12.5">feu</w> <w n="12.6">dans</w> <w n="12.7">ma</w> <w n="12.8">chambre</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Pluie</w>, <w n="13.2">orage</w>, <w n="13.3">tonnerre</w>, <w n="13.4">éclair</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">vous</w>, <w n="14.3">noirs</w> <w n="14.4">frimas</w> <w n="14.5">que</w> <w n="14.6">j</w>’<w n="14.7">héberge</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Tant</w> <w n="15.2">pis</w> ! <w n="15.3">j</w>’<w n="15.4">allume</w> <w n="15.5">un</w> <w n="15.6">beau</w> <w n="15.7">feu</w> <w n="15.8">clair</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Un</w> <w n="16.2">feu</w> <w n="16.3">de</w> <w n="16.4">forge</w>, <w n="16.5">un</w> <w n="16.6">feu</w> <w n="16.7">d</w>’<w n="16.8">auberge</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Privé</w> <w n="17.2">de</w> <w n="17.3">voir</w> <w n="17.4">le</w> <w n="17.5">doux</w> <w n="17.6">ciel</w> <w n="17.7">bleu</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Je</w> <w n="18.2">mets</w> <w n="18.3">un</w> <w n="18.4">terme</w> <w n="18.5">aux</w> <w n="18.6">dithyrambes</w></l>
					<l n="19" num="5.3"><w n="19.1">Et</w>, <w n="19.2">transi</w>, <w n="19.3">j</w>’<w n="19.4">allume</w> <w n="19.5">ce</w> <w n="19.6">feu</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Afin</w> <w n="20.2">de</w> <w n="20.3">me</w> <w n="20.4">rôtir</w> <w n="20.5">les</w> <w n="20.6">jambes</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">l</w>’<w n="21.3">autan</w> <w n="21.4">noir</w> <w n="21.5">peut</w> <w n="21.6">aboyer</w>.</l>
					<l n="22" num="6.2"><w n="22.1">Pourtant</w>, <w n="22.2">voyant</w> <w n="22.3">la</w> <w n="22.4">flamme</w> <w n="22.5">éparse</w></l>
					<l n="23" num="6.3"><w n="23.1">Rougir</w> <w n="23.2">ma</w> <w n="23.3">vitre</w> <w n="23.4">et</w> <w n="23.5">flamboyer</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Les</w> <w n="24.2">Lys</w> <w n="24.3">disent</w> : <w n="24.4">C</w>’<w n="24.5">est</w> <w n="24.6">une</w> <w n="24.7">farce</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Lys</w> <w n="25.2">pur</w> <w n="25.3">au</w> <w n="25.4">superbe</w> <w n="25.5">appareil</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Vous</w> <w n="26.2">dont</w> <w n="26.3">Hugo</w>, <w n="26.4">dans</w> <w n="26.5">sa</w> <w n="26.6">fournaise</w>,</l>
					<l n="27" num="7.3"><w n="27.1">A</w> <w n="27.2">dit</w> : <w n="27.3">Le</w> <w n="27.4">Lys</w> <w n="27.5">à</w> <w n="27.6">Dieu</w> <w n="27.7">pareil</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Vous</w> <w n="28.2">en</w> <w n="28.3">parlez</w> <w n="28.4">bien</w> <w n="28.5">à</w> <w n="28.6">votre</w> <w n="28.7">aise</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Car</w> <w n="29.2">pourquoi</w>, <w n="29.3">par</w> <w n="29.4">quelles</w> <w n="29.5">raisons</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Renan</w> <w n="30.2">l</w>’<w n="30.3">ignore</w> <w n="30.4">comme</w> <w n="30.5">Taine</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Mais</w> <w n="31.2">on</w> <w n="31.3">voit</w> <w n="31.4">bien</w> <w n="31.5">que</w> <w n="31.6">les</w> <w n="31.7">Saisons</w></l>
					<l n="32" num="8.4"><w n="32.1">Courent</w> <w n="32.2">toutes</w> <w n="32.3">la</w> <w n="32.4">prétentaine</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Par</w> <w n="33.2">un</w> <w n="33.3">délire</w> <w n="33.4">inattendu</w>,</l>
					<l n="34" num="9.2">(<w n="34.1">Qu</w>’<w n="34.2">un</w> <w n="34.3">bon</w> <w n="34.4">coup</w> <w n="34.5">de</w> <w n="34.6">vin</w> <w n="34.7">nous</w> <w n="34.8">console</w> !)</l>
					<l n="35" num="9.3"><w n="35.1">A</w> <w n="35.2">coup</w> <w n="35.3">sûr</w>, <w n="35.4">elles</w> <w n="35.5">ont</w> <w n="35.6">perdu</w></l>
					<l n="36" num="9.4"><w n="36.1">La</w> <w n="36.2">tramontane</w> <w n="36.3">et</w> <w n="36.4">la</w> <w n="36.5">boussole</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Cachant</w> <w n="37.2">sous</w> <w n="37.3">leur</w> <w n="37.4">sombre</w> <w n="37.5">manteau</w></l>
					<l n="38" num="10.2"><w n="38.1">Les</w> <w n="38.2">déluges</w>, <w n="38.3">les</w> <w n="38.4">pleurs</w>, <w n="38.5">les</w> <w n="38.6">houles</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Ces</w> <w n="39.2">vagabondes</w> <w n="39.3">s</w>’<w n="39.4">en</w> <w n="39.5">vont</w> <w n="39.6">au</w></l>
					<l n="40" num="10.4"><w n="40.1">Hasard</w>, <w n="40.2">comme</w> <w n="40.3">des</w> <w n="40.4">femmes</w> <w n="40.5">soûles</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">A</w> <w n="41.2">voir</w> <w n="41.3">leur</w> <w n="41.4">chœur</w> <w n="41.5">aérien</w></l>
					<l n="42" num="11.2"><w n="42.1">S</w>’<w n="42.2">agiter</w> <w n="42.3">dans</w> <w n="42.4">le</w> <w n="42.5">ciel</w> <w n="42.6">qui</w> <w n="42.7">bouge</w>,</l>
					<l n="43" num="11.3"><w n="43.1">On</w> <w n="43.2">songe</w> <w n="43.3">aux</w> <w n="43.4">danseuses</w> <w n="43.5">que</w> <w n="43.6">rien</w></l>
					<l n="44" num="11.4"><w n="44.1">Ne</w> <w n="44.2">déconcerte</w>, <w n="44.3">au</w> <w n="44.4">Moulin</w>-<w n="44.5">Rouge</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Elles</w> <w n="45.2">vont</w>, <w n="45.3">folles</w> <w n="45.4">de</w> <w n="45.5">terreur</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Parmi</w> <w n="46.2">les</w> <w n="46.3">nuits</w> <w n="46.4">hyperborées</w>,</l>
					<l n="47" num="12.3"><w n="47.1">A</w> <w n="47.2">travers</w> <w n="47.3">le</w> <w n="47.4">vague</w> <w n="47.5">et</w> <w n="47.6">l</w>’<w n="47.7">horreur</w></l>
					<l n="48" num="12.4"><w n="48.1">Et</w> <w n="48.2">les</w> <w n="48.3">vertigineux</w> <w n="48.4">Borées</w>,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">découvrant</w> <w n="49.3">leur</w> <w n="49.4">mollet</w> <w n="49.5">noir</w></l>
					<l n="50" num="13.2"><w n="50.1">A</w> <w n="50.2">travers</w> <w n="50.3">la</w> <w n="50.4">nue</w> <w n="50.5">impollue</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Sur</w> <w n="51.2">leurs</w> <w n="51.3">jambes</w> <w n="51.4">semblent</w> <w n="51.5">avoir</w></l>
					<l n="52" num="13.4"><w n="52.1">Des</w> <w n="52.2">bas</w> <w n="52.3">noirs</w>, <w n="52.4">comme</w> <w n="52.5">la</w> <w n="52.6">Goulue</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">En</w> <w n="53.2">se</w> <w n="53.3">tordant</w> <w n="53.4">comme</w> <w n="53.5">des</w> <w n="53.6">flots</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Elles</w> <w n="54.2">s</w>’<w n="54.3">en</w> <w n="54.4">vont</w> <w n="54.5">avec</w> <w n="54.6">des</w> <w n="54.7">rages</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Des</w> <w n="55.2">hurlements</w> <w n="55.3">et</w> <w n="55.4">des</w> <w n="55.5">sanglots</w> ;</l>
					<l n="56" num="14.4"><w n="56.1">Et</w> <w n="56.2">les</w> <w n="56.3">cherchant</w> <w n="56.4">dans</w> <w n="56.5">les</w> <w n="56.6">orages</w>,</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Parfois</w>, <w n="57.2">combat</w> <w n="57.3">mystérieux</w> !</l>
					<l n="58" num="15.2"><w n="58.1">Dans</w> <w n="58.2">le</w> <w n="58.3">désordre</w> <w n="58.4">affreux</w> <w n="58.5">d</w>’<w n="58.6">un</w> <w n="58.7">rêve</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Le</w> <w n="59.2">Soleil</w>, <w n="59.3">astre</w> <w n="59.4">furieux</w>,</l>
					<l n="60" num="15.4"><w n="60.1">Les</w> <w n="60.2">aveugle</w> <w n="60.3">avec</w> <w n="60.4">son</w> <w n="60.5">vieux</w> <w n="60.6">glaive</w>.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Sous</w> <w n="61.2">l</w>’<w n="61.3">éclair</w> <w n="61.4">de</w> <w n="61.5">son</w> <w n="61.6">yatagan</w></l>
					<l n="62" num="16.2"><w n="62.1">Elles</w> <w n="62.2">s</w>’<w n="62.3">en</w> <w n="62.4">vont</w>, <w n="62.5">dégingandées</w></l>
					<l n="63" num="16.3"><w n="63.1">Et</w> <w n="63.2">c</w>’<w n="63.3">est</w> <w n="63.4">le</w> <w n="63.5">sauvage</w> <w n="63.6">Ouragan</w></l>
					<l n="64" num="16.4"><w n="64.1">Qui</w> <w n="64.2">fouaille</w> <w n="64.3">ces</w> <w n="64.4">dévergondées</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1890"> 22 juillet 1890.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>