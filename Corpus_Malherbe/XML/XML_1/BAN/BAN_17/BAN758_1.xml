<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN758">
				<head type="main">La Pomme</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Confesseurs</w>, <w n="1.2">juges</w> <w n="1.3">sans</w> <w n="1.4">appel</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Obstinés</w> <w n="2.2">chercheurs</w> <w n="2.3">de</w> <w n="2.4">problèmes</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Vous</w> <w n="3.2">tenez</w> <w n="3.3">si</w> <w n="3.4">bien</w> <w n="3.5">le</w> <w n="3.6">scalpel</w></l>
					<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">vous</w> <w n="4.3">en</w> <w n="4.4">devenez</w> <w n="4.5">tout</w> <w n="4.6">blêmes</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ainsi</w>, <w n="5.2">de</w> <w n="5.3">tout</w> <w n="5.4">votre</w> <w n="5.5">pouvoir</w>,</l>
					<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">la</w> <w n="6.3">houri</w> <w n="6.4">jusqu</w>’<w n="6.5">aux</w> <w n="6.6">tziganes</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Vous</w> <w n="7.2">fouillez</w> <w n="7.3">la</w> <w n="7.4">Femme</w>, <w n="7.5">pour</w> <w n="7.6">voir</w></l>
					<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">jeu</w> <w n="8.3">secret</w> <w n="8.4">de</w> <w n="8.5">ses</w> <w n="8.6">organes</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ayant</w> <w n="9.2">classifié</w> <w n="9.3">l</w>’<w n="9.4">amour</w></l>
					<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">promenant</w> <w n="10.3">votre</w> <w n="10.4">lanterne</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Vous</w> <w n="11.2">voulez</w> <w n="11.3">traîner</w> <w n="11.4">au</w> <w n="11.5">grand</w> <w n="11.6">jour</w></l>
					<l n="12" num="3.4"><w n="12.1">Le</w> <w n="12.2">secret</w> <w n="12.3">de</w> <w n="12.4">l</w>’<w n="12.5">Ève</w> <w n="12.6">moderne</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Cela</w>, <w n="13.2">vous</w> <w n="13.3">nous</w> <w n="13.4">le</w> <w n="13.5">promettez</w></l>
					<l n="14" num="4.2"><w n="14.1">Avec</w> <w n="14.2">une</w> <w n="14.3">ardente</w> <w n="14.4">mimique</w></l>
					<l n="15" num="4.3"><w n="15.1">Et</w>, <w n="15.2">soigneusement</w>, <w n="15.3">vous</w> <w n="15.4">mettez</w></l>
					<l n="16" num="4.4"><w n="16.1">Au</w> <w n="16.2">net</w>, <w n="16.3">sa</w> <w n="16.4">formule</w> <w n="16.5">chimique</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">A</w> <w n="17.2">ces</w> <w n="17.3">méthodes</w> <w n="17.4">convertis</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Vous</w> <w n="18.2">défendez</w> <w n="18.3">qu</w>’<w n="18.4">elle</w> <w n="18.5">ressente</w></l>
					<l n="19" num="5.3"><w n="19.1">Rien</w>, <w n="19.2">sans</w> <w n="19.3">vous</w> <w n="19.4">avoir</w> <w n="19.5">avertis</w>.</l>
					<l n="20" num="5.4"><w n="20.1">Qu</w>’<w n="20.2">est</w>-<w n="20.3">elle</w>, <w n="20.4">cette</w> <w n="20.5">Ève</w> <w n="20.6">récente</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Ah</w> ! <w n="21.2">que</w> <w n="21.3">vous</w> <w n="21.4">prenez</w> <w n="21.5">de</w> <w n="21.6">tourment</w> !</l>
					<l n="22" num="6.2"><w n="22.1">Cette</w> <w n="22.2">Ève</w> (<w n="22.3">chaque</w> <w n="22.4">âge</w> <w n="22.5">a</w> <w n="22.6">la</w> <w n="22.7">sienne</w>)</l>
					<l n="23" num="6.3"><w n="23.1">Qu</w>’<w n="23.2">est</w>-<w n="23.3">elle</w> ? <w n="23.4">Mais</w> <w n="23.5">exactement</w></l>
					<l n="24" num="6.4"><w n="24.1">La</w> <w n="24.2">même</w> <w n="24.3">en</w> <w n="24.4">tout</w> <w n="24.5">point</w> <w n="24.6">que</w> <w n="24.7">l</w>’<w n="24.8">ancienne</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Car</w>, <w n="25.2">bien</w> <w n="25.3">qu</w>’<w n="25.4">elle</w> <w n="25.5">soit</w> <w n="25.6">plus</w> <w n="25.7">ou</w> <w n="25.8">moins</w></l>
					<l n="26" num="7.2"><w n="26.1">Dans</w> <w n="26.2">tous</w> <w n="26.3">les</w> <w n="26.4">procès</w> <w n="26.5">impliquée</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Sur</w> <w n="27.2">le</w> <w n="27.3">rapport</w> <w n="27.4">de</w> <w n="27.5">cent</w> <w n="27.6">témoins</w>,</l>
					<l n="28" num="7.4"><w n="28.1">La</w> <w n="28.2">Femme</w> <w n="28.3">n</w>’<w n="28.4">est</w> <w n="28.5">pas</w> <w n="28.6">compliquée</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Avec</w> <w n="29.2">ses</w> <w n="29.3">pieds</w> <w n="29.4">fins</w> <w n="29.5">et</w> <w n="29.6">petits</w></l>
					<l n="30" num="8.2"><w n="30.1">Elle</w> <w n="30.2">échappe</w> <w n="30.3">vite</w> <w n="30.4">au</w> <w n="30.5">reproche</w>.</l>
					<l n="31" num="8.3"><w n="31.1">Ce</w> <w n="31.2">que</w> <w n="31.3">veulent</w> <w n="31.4">ses</w> <w n="31.5">appétits</w>,</l>
					<l n="32" num="8.4"><w n="32.1">C</w>’<w n="32.2">est</w> <w n="32.3">clair</w> <w n="32.4">comme</w> <w n="32.5">de</w> <w n="32.6">l</w>’<w n="32.7">eau</w> <w n="32.8">de</w> <w n="32.9">roche</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">En</w> <w n="33.2">mil</w> <w n="33.3">huit</w> <w n="33.4">cent</w> <w n="33.5">quatre</w>-<w n="33.6">vingt</w>-<w n="33.7">dix</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Comme</w> <w n="34.2">au</w> <w n="34.3">temps</w> <w n="34.4">des</w> <w n="34.5">décors</w> <w n="34.6">étrusques</w>,</l>
					<l n="35" num="9.3"><w n="35.1">La</w> <w n="35.2">Femme</w>, <w n="35.3">éprise</w> <w n="35.4">d</w>’<w n="35.5">Amadis</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Aime</w> <w n="36.2">à</w> <w n="36.3">porter</w> <w n="36.4">de</w> <w n="36.5">belles</w> <w n="36.6">frusques</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Elle</w> <w n="37.2">mange</w> <w n="37.3">assez</w> <w n="37.4">volontiers</w></l>
					<l n="38" num="10.2"><w n="38.1">Une</w> <w n="38.2">friture</w> <w n="38.3">à</w> <w n="38.4">la</w> <w n="38.5">campagne</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">boit</w>, <w n="39.3">sans</w> <w n="39.4">nuls</w> <w n="39.5">dédains</w> <w n="39.6">altiers</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Le</w> <w n="40.2">vin</w> <w n="40.3">rose</w> <w n="40.4">ou</w> <w n="40.5">le</w> <w n="40.6">clair</w> <w n="40.7">champagne</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Toujours</w> <w n="41.2">la</w> <w n="41.3">même</w>, <w n="41.4">je</w> <w n="41.5">vous</w> <w n="41.6">dis</w> !</l>
					<l n="42" num="11.2"><w n="42.1">Elle</w> <w n="42.2">veut</w>, <w n="42.3">sans</w> <w n="42.4">billevesée</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Être</w> <w n="43.2">prise</w> <w n="43.3">en</w> <w n="43.4">des</w> <w n="43.5">bras</w> <w n="43.6">hardis</w></l>
					<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">sur</w> <w n="44.3">sa</w> <w n="44.4">bouche</w> <w n="44.5">en</w> <w n="44.6">fleur</w>, <w n="44.7">baisée</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Si</w> <w n="45.2">vous</w> <w n="45.3">voulez</w> <w n="45.4">en</w> <w n="45.5">être</w> <w n="45.6">sûr</w>,</l>
					<l n="46" num="12.2">(<w n="46.1">Ne</w> <w n="46.2">craignez</w> <w n="46.3">pas</w> <w n="46.4">que</w> <w n="46.5">ce</w> <w n="46.6">plan</w> <w n="46.7">rate</w>)</l>
					<l n="47" num="12.3"><w n="47.1">Plantez</w> <w n="47.2">dans</w> <w n="47.3">le</w> <w n="47.4">pays</w> <w n="47.5">d</w>’<w n="47.6">Assur</w></l>
					<l n="48" num="12.4"><w n="48.1">Un</w> <w n="48.2">jardin</w> <w n="48.3">baigné</w> <w n="48.4">par</w> <w n="48.5">l</w>’<w n="48.6">Euphrate</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Là</w>, <w n="49.2">sous</w> <w n="49.3">les</w> <w n="49.4">cieux</w> <w n="49.5">extasiés</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Que</w> <w n="50.2">le</w> <w n="50.3">regard</w> <w n="50.4">enchanté</w> <w n="50.5">voie</w></l>
					<l n="51" num="13.3"><w n="51.1">D</w>’<w n="51.2">immenses</w> <w n="51.3">forêts</w> <w n="51.4">de</w> <w n="51.5">rosiers</w></l>
					<l n="52" num="13.4"><w n="52.1">Et</w> <w n="52.2">d</w>’<w n="52.3">énormes</w> <w n="52.4">lys</w>, <w n="52.5">pleins</w> <w n="52.6">de</w> <w n="52.7">joie</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Oubliant</w> <w n="53.2">les</w> <w n="53.3">rébellions</w></l>
					<l n="54" num="14.2"><w n="54.1">Au</w> <w n="54.2">milieu</w> <w n="54.3">des</w> <w n="54.4">chants</w> <w n="54.5">et</w> <w n="54.6">des</w> <w n="54.7">ailes</w>,</l>
					<l n="55" num="14.3"><w n="55.1">Que</w> <w n="55.2">les</w> <w n="55.3">tigres</w> <w n="55.4">et</w> <w n="55.5">les</w> <w n="55.6">lions</w></l>
					<l n="56" num="14.4"><w n="56.1">Baisent</w> <w n="56.2">tendrement</w> <w n="56.3">les</w> <w n="56.4">gazelles</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Dans</w> <w n="57.2">ces</w> <w n="57.3">paradis</w> <w n="57.4">enchanteurs</w></l>
					<l n="58" num="15.2"><w n="58.1">Mettez</w> <w n="58.2">la</w> <w n="58.3">Femme</w> <w n="58.4">auprès</w> <w n="58.5">de</w> <w n="58.6">l</w>’<w n="58.7">Homme</w></l>
					<l n="59" num="15.3"><w n="59.1">Et</w> <w n="59.2">les</w> <w n="59.3">doux</w> <w n="59.4">rossignols</w> <w n="59.5">chanteurs</w></l>
					<l n="60" num="15.4"><w n="60.1">Et</w> <w n="60.2">l</w>’<w n="60.3">arbre</w> <w n="60.4">céleste</w> <w n="60.5">et</w> <w n="60.6">la</w> <w n="60.7">Pomme</w> ;</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Et</w>, <w n="61.2">comme</w> <w n="61.3">en</w> <w n="61.4">tout</w> <w n="61.5">pays</w>, <w n="61.6">cela</w></l>
					<l n="62" num="16.2"><w n="62.1">Suffit</w> <w n="62.2">pour</w> <w n="62.3">un</w> <w n="62.4">épithalame</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Croyez</w>-<w n="63.2">le</w> <w n="63.3">bien</w>, <w n="63.4">ce</w> <w n="63.5">n</w>’<w n="63.6">est</w> <w n="63.7">pas</w> <w n="63.8">la</w></l>
					<l n="64" num="16.4"><w n="64.1">Pomme</w> <w n="64.2">qui</w> <w n="64.3">mangera</w> <w n="64.4">la</w> <w n="64.5">Femme</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1890"> 2 septembre 1890.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>