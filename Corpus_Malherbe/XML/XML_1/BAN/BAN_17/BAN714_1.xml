<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN714">
				<head type="number">Le Guitariste</head>
				<head type="form">Pantoum</head>
				<opener>
					<salute>A Georges Rochegrosse</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Joue</w> <w n="1.2">encor</w>, <w n="1.3">bon</w> <w n="1.4">guitariste</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Joue</w> <w n="2.2">un</w> <w n="2.3">fandango</w> <w n="2.4">très</w> <w n="2.5">fou</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Oh</w> ! <w n="3.2">mon</w> <w n="3.3">âme</w> <w n="3.4">est</w> <w n="3.5">triste</w>, <w n="3.6">triste</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Comme</w> <w n="4.2">un</w> <w n="4.3">oiseau</w> <w n="4.4">dans</w> <w n="4.5">un</w> <w n="4.6">trou</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Joue</w> <w n="5.2">un</w> <w n="5.3">fandango</w> <w n="5.4">très</w> <w n="5.5">fou</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Voltigeant</w> <w n="6.2">comme</w> <w n="6.3">une</w> <w n="6.4">plume</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Comme</w> <w n="7.2">un</w> <w n="7.3">oiseau</w> <w n="7.4">dans</w> <w n="7.5">un</w> <w n="7.6">trou</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">souvenir</w> <w n="8.3">me</w> <w n="8.4">consume</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Voltigeant</w> <w n="9.2">comme</w> <w n="9.3">une</w> <w n="9.4">plume</w>,</l>
					<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">Danse</w> <w n="10.3">a</w> <w n="10.4">les</w> <w n="10.5">pieds</w> <w n="10.6">légers</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">souvenir</w> <w n="11.3">me</w> <w n="11.4">consume</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Je</w> <w n="12.2">pleure</w> <w n="12.3">en</w> <w n="12.4">mes</w> <w n="12.5">yeux</w> <w n="12.6">rongés</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">Danse</w> <w n="13.3">a</w> <w n="13.4">les</w> <w n="13.5">pieds</w> <w n="13.6">légers</w></l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">les</w> <w n="14.3">jupes</w> <w n="14.4">envolées</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">pleure</w> <w n="15.3">en</w> <w n="15.4">mes</w> <w n="15.5">yeux</w> <w n="15.6">rongés</w></l>
					<l n="16" num="4.4"><w n="16.1">Par</w> <w n="16.2">trop</w> <w n="16.3">de</w> <w n="16.4">larmes</w> <w n="16.5">salées</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">les</w> <w n="17.3">jupes</w> <w n="17.4">envolées</w>,</l>
					<l n="18" num="5.2"><w n="18.1">C</w>’<w n="18.2">est</w> <w n="18.3">le</w> <w n="18.4">rouge</w> <w n="18.5">éclair</w> <w n="18.6">vainqueur</w> !</l>
					<l n="19" num="5.3"><w n="19.1">Par</w> <w n="19.2">trop</w> <w n="19.3">de</w> <w n="19.4">larmes</w> <w n="19.5">salées</w></l>
					<l n="20" num="5.4"><w n="20.1">J</w>’<w n="20.2">ai</w> <w n="20.3">senti</w> <w n="20.4">noyer</w> <w n="20.5">mon</w> <w n="20.6">cœur</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">le</w> <w n="21.4">rouge</w> <w n="21.5">éclair</w> <w n="21.6">vainqueur</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Gracia</w> <w n="22.2">joue</w> <w n="22.3">et</w> <w n="22.4">s</w>’<w n="22.5">élance</w>.</l>
					<l n="23" num="6.3"><w n="23.1">J</w>’<w n="23.2">ai</w> <w n="23.3">senti</w> <w n="23.4">noyer</w> <w n="23.5">mon</w> <w n="23.6">cœur</w></l>
					<l n="24" num="6.4"><w n="24.1">Dans</w> <w n="24.2">la</w> <w n="24.3">nuit</w> <w n="24.4">et</w> <w n="24.5">le</w> <w n="24.6">silence</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Gracia</w> <w n="25.2">joue</w> <w n="25.3">et</w> <w n="25.4">s</w>’<w n="25.5">élance</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Vois</w> <w n="26.2">briller</w> <w n="26.3">son</w> <w n="26.4">front</w> <w n="26.5">charmant</w>.</l>
					<l n="27" num="7.3"><w n="27.1">Dans</w> <w n="27.2">la</w> <w n="27.3">nuit</w> <w n="27.4">et</w> <w n="27.5">le</w> <w n="27.6">silence</w></l>
					<l n="28" num="7.4"><w n="28.1">Je</w> <w n="28.2">soupire</w> <w n="28.3">affreusement</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Vois</w> <w n="29.2">briller</w> <w n="29.3">son</w> <w n="29.4">front</w> <w n="29.5">charmant</w></l>
					<l n="30" num="8.2"><w n="30.1">Dans</w> <w n="30.2">l</w>’<w n="30.3">or</w> <w n="30.4">de</w> <w n="30.5">sa</w> <w n="30.6">chevelure</w>.</l>
					<l n="31" num="8.3"><w n="31.1">Je</w> <w n="31.2">soupire</w> <w n="31.3">affreusement</w>.</l>
					<l n="32" num="8.4"><w n="32.1">Oh</w> ! <w n="32.2">la</w> <w n="32.3">cuisante</w> <w n="32.4">brûlure</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Dans</w> <w n="33.2">l</w>’<w n="33.3">or</w> <w n="33.4">de</w> <w n="33.5">sa</w> <w n="33.6">chevelure</w></l>
					<l n="34" num="9.2"><w n="34.1">Une</w> <w n="34.2">fleur</w> <w n="34.3">se</w> <w n="34.4">fane</w> <w n="34.5">un</w> <w n="34.6">peu</w>.</l>
					<l n="35" num="9.3"><w n="35.1">Oh</w> ! <w n="35.2">la</w> <w n="35.3">cuisante</w> <w n="35.4">brûlure</w> !</l>
					<l n="36" num="9.4"><w n="36.1">C</w>’<w n="36.2">est</w> <w n="36.3">dans</w> <w n="36.4">ma</w> <w n="36.5">poitrine</w> <w n="36.6">en</w> <w n="36.7">feu</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Une</w> <w n="37.2">fleur</w> <w n="37.3">se</w> <w n="37.4">fane</w> <w n="37.5">un</w> <w n="37.6">peu</w>.</l>
					<l n="38" num="10.2"><w n="38.1">Où</w> <w n="38.2">donc</w> <w n="38.3">gémit</w> <w n="38.4">le</w> <w n="38.5">fleuriste</w> ?</l>
					<l n="39" num="10.3"><w n="39.1">C</w>’<w n="39.2">est</w> <w n="39.3">dans</w> <w n="39.4">ma</w> <w n="39.5">poitrine</w> <w n="39.6">en</w> <w n="39.7">feu</w>.</l>
					<l n="40" num="10.4"><w n="40.1">Joue</w> <w n="40.2">encor</w>, <w n="40.3">bon</w> <w n="40.4">guitariste</w>.</l>
				</lg>
			</div></body></text></TEI>