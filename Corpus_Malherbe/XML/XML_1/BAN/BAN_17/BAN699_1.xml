<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN699">
				<head type="main">Massacre</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Elle</w> <w n="1.2">n</w>’<w n="1.3">a</w> <w n="1.4">pas</w> <w n="1.5">treize</w> <w n="1.6">ans</w> ; <w n="1.7">fillette</w> <w n="1.8">à</w> <w n="1.9">peine</w> <w n="1.10">éclose</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Sa</w> <w n="2.2">bouche</w> <w n="2.3">en</w> <w n="2.4">fleur</w> <w n="2.5">a</w> <w n="2.6">l</w>’<w n="2.7">air</w> <w n="2.8">d</w>’<w n="2.9">une</w> <w n="2.10">petite</w> <w n="2.11">rose</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Avec</w> <w n="3.2">un</w> <w n="3.3">doux</w> <w n="3.4">ruban</w> <w n="3.5">d</w>’<w n="3.6">azur</w> <w n="3.7">autour</w> <w n="3.8">du</w> <w n="3.9">cou</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Elle</w> <w n="4.2">va</w> <w n="4.3">devant</w> <w n="4.4">elle</w> <w n="4.5">et</w> <w n="4.6">sans</w> <w n="4.7">savoir</w> <w n="4.8">jusqu</w>’<w n="4.9">où</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Affamée</w> <w n="5.2">elle</w> <w n="5.3">mange</w> <w n="5.4">et</w> <w n="5.5">dévore</w> <w n="5.6">des</w> <w n="5.7">pommes</w></l>
					<l n="6" num="1.6"><w n="6.1">Avec</w> <w n="6.2">ses</w> <w n="6.3">dents</w> <w n="6.4">de</w> <w n="6.5">nacre</w>, <w n="6.6">et</w> <w n="6.7">regarde</w> <w n="6.8">les</w> <w n="6.9">hommes</w></l>
					<l n="7" num="1.7"><w n="7.1">D</w>’<w n="7.2">un</w> <w n="7.3">air</w> <w n="7.4">effronté</w>, <w n="7.5">mais</w> <w n="7.6">cependant</w> <w n="7.7">ingénu</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Elle</w> <w n="8.2">se</w> <w n="8.3">réjouit</w> <w n="8.4">de</w> <w n="8.5">montrer</w> <w n="8.6">son</w> <w n="8.7">bras</w> <w n="8.8">nu</w></l>
					<l n="9" num="1.9"><w n="9.1">En</w> <w n="9.2">lorgnant</w> <w n="9.3">au</w> <w n="9.4">bazar</w> <w n="9.5">quelque</w> <w n="9.6">bijou</w> <w n="9.7">de</w> <w n="9.8">cuivre</w>.</l>
					<l n="10" num="1.10"><w n="10.1">Si</w> <w n="10.2">parfois</w> <w n="10.3">un</w> <w n="10.4">passant</w> <w n="10.5">fait</w> <w n="10.6">mine</w> <w n="10.7">de</w> <w n="10.8">la</w> <w n="10.9">suivre</w></l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">semble</w> <w n="11.3">affriandé</w> <w n="11.4">par</w> <w n="11.5">ses</w> <w n="11.6">minces</w> <w n="11.7">appas</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Vite</w> <w n="12.2">elle</w> <w n="12.3">fait</w> <w n="12.4">la</w> <w n="12.5">dame</w> <w n="12.6">et</w> <w n="12.7">ralentit</w> <w n="12.8">son</w> <w n="12.9">pas</w>.</l>
					<l n="13" num="1.13"><w n="13.1">On</w> <w n="13.2">voit</w> <w n="13.3">je</w> <w n="13.4">ne</w> <w n="13.5">sais</w> <w n="13.6">quel</w> <w n="13.7">mystérieux</w> <w n="13.8">délire</w></l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">quel</w> <w n="14.3">affolement</w> <w n="14.4">dans</w> <w n="14.5">son</w> <w n="14.6">vague</w> <w n="14.7">sourire</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">pourtant</w>, <w n="15.3">malgré</w> <w n="15.4">son</w> <w n="15.5">manège</w> <w n="15.6">triomphant</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Elle</w> <w n="16.2">a</w> <w n="16.3">bien</w> <w n="16.4">l</w>’<w n="16.5">ignorance</w> <w n="16.6">auguste</w> <w n="16.7">de</w> <w n="16.8">l</w>’<w n="16.9">enfant</w></l>
					<l n="17" num="1.17"><w n="17.1">Dans</w> <w n="17.2">ses</w> <w n="17.3">yeux</w> <w n="17.4">pleins</w> <w n="17.5">de</w> <w n="17.6">grâce</w> <w n="17.7">et</w> <w n="17.8">de</w> <w n="17.9">mélancolie</w>.</l>
					<l n="18" num="1.18"><w n="18.1">Oh</w> ! <w n="18.2">quel</w> <w n="18.3">deuil</w>, <w n="18.4">la</w> <w n="18.5">naïve</w> <w n="18.6">innocence</w> <w n="18.7">avilie</w> !</l>
					<l n="19" num="1.19"><w n="19.1">Chantonnant</w> <w n="19.2">son</w> <w n="19.3">refrain</w> <w n="19.4">comme</w> <w n="19.5">un</w> <w n="19.6">oiseau</w> <w n="19.7">bavard</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Elle</w> <w n="20.2">va</w> <w n="20.3">sans</w> <w n="20.4">repos</w> <w n="20.5">le</w> <w n="20.6">long</w> <w n="20.7">du</w> <w n="20.8">boulevart</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Traînant</w> <w n="21.2">son</w> <w n="21.3">corps</w> <w n="21.4">fragile</w> <w n="21.5">et</w> <w n="21.6">son</w> <w n="21.7">âme</w> <w n="21.8">tuée</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Pauvre</w> <w n="22.2">petite</w>, <w n="22.3">hélas</w> ! <w n="22.4">déjà</w> <w n="22.5">prostituée</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1887">Mercredi, 12 janvier 1887.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>