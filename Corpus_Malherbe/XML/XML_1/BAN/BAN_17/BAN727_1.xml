<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN727">
				<head type="main">Aimer Paris</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Artiste</w>, <w n="1.2">désormais</w> <w n="1.3">tu</w> <w n="1.4">veux</w> <w n="1.5">peindre</w> <w n="1.6">la</w> <w n="1.7">Vie</w></l>
					<l n="2" num="1.2"><w n="2.1">Moderne</w>, <w n="2.2">frémissante</w>, <w n="2.3">avide</w>, <w n="2.4">inassouvie</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Belle</w> <w n="3.2">de</w> <w n="3.3">douleur</w> <w n="3.4">calme</w> <w n="3.5">et</w> <w n="3.6">de</w> <w n="3.7">sévérité</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Car</w> <w n="4.2">ton</w> <w n="4.3">esprit</w> <w n="4.4">sincère</w> <w n="4.5">a</w> <w n="4.6">soif</w> <w n="4.7">de</w> <w n="4.8">vérité</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Vois</w>, <w n="5.2">comme</w> <w n="5.3">une</w> <w n="5.4">forêt</w> <w n="5.5">d</w>’<w n="5.6">arbres</w>, <w n="5.7">la</w> <w n="5.8">ville</w> <w n="5.9">immense</w></l>
					<l n="6" num="1.6"><w n="6.1">Murmure</w> <w n="6.2">sous</w> <w n="6.3">l</w>’<w n="6.4">orage</w> <w n="6.5">et</w> <w n="6.6">le</w> <w n="6.7">vent</w> <w n="6.8">en</w> <w n="6.9">démence</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Ses</w> <w n="7.2">entassements</w> <w n="7.3">noirs</w> <w n="7.4">de</w> <w n="7.5">toits</w> <w n="7.6">et</w> <w n="7.7">de</w> <w n="7.8">maisons</w></l>
					<l n="8" num="1.8"><w n="8.1">Ont</w> <w n="8.2">le</w> <w n="8.3">charme</w> <w n="8.4">effrayant</w> <w n="8.5">des</w> <w n="8.6">larges</w> <w n="8.7">frondaisons</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Aime</w> <w n="9.2">ses</w> <w n="9.3">bruits</w>, <w n="9.4">ses</w> <w n="9.5">voix</w>, <w n="9.6">ses</w> <w n="9.7">rires</w>, <w n="9.8">son</w> <w n="9.9">tumulte</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Ses</w> <w n="10.2">monuments</w> <w n="10.3">qu</w>’<w n="10.4">en</w> <w n="10.5">vain</w> <w n="10.6">le</w> <w n="10.7">Temps</w> <w n="10.8">railleur</w> <w n="10.9">insulte</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Ses</w> <w n="11.2">marchés</w>, <w n="11.3">ses</w> <w n="11.4">jardins</w> ; <w n="11.5">aime</w> <w n="11.6">ses</w> <w n="11.7">pauvres</w> <w n="11.8">cieux</w></l>
					<l n="12" num="1.12"><w n="12.1">Toujours</w> <w n="12.2">mornes</w>, <w n="12.3">d</w>’<w n="12.4">un</w> <w n="12.5">gris</w> <w n="12.6">terne</w> <w n="12.7">et</w> <w n="12.8">délicieux</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Surtout</w>, <w n="13.2">n</w>’<w n="13.3">imite</w> <w n="13.4">pas</w> <w n="13.5">Hamlet</w> ; <w n="13.6">sans</w> <w n="13.7">épigramme</w></l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">d</w>’<w n="14.3">un</w> <w n="14.4">cœur</w> <w n="14.5">chaleureux</w>, <w n="14.6">aime</w> <w n="14.7">l</w>’<w n="14.8">Homme</w> <w n="14.9">et</w> <w n="14.10">la</w> <w n="14.11">Femme</w>.</l>
					<l n="15" num="1.15"><w n="15.1">La</w> <w n="15.2">Femme</w> <w n="15.3">surtout</w> ! <w n="15.4">Suis</w> <w n="15.5">de</w> <w n="15.6">l</w>’<w n="15.7">œil</w> <w n="15.8">ces</w> <w n="15.9">bataillons</w></l>
					<l n="16" num="1.16"><w n="16.1">De</w> <w n="16.2">gamines</w> <w n="16.3">qui</w> <w n="16.4">vont</w>, <w n="16.5">blanches</w> <w n="16.6">sous</w> <w n="16.7">les</w> <w n="16.8">haillons</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">qui</w>, <w n="17.3">montrant</w> <w n="17.4">leurs</w> <w n="17.5">dents</w>, <w n="17.6">croquent</w> <w n="17.7">de</w> <w n="17.8">jaunes</w> <w n="17.9">pommes</w></l>
					<l n="18" num="1.18"><w n="18.1">De</w> <w n="18.2">terre</w> <w n="18.3">frites</w>, <w n="18.4">sous</w> <w n="18.5">l</w>’<w n="18.6">œil</w> <w n="18.7">allumé</w> <w n="18.8">des</w> <w n="18.9">hommes</w> !</l>
					<l n="19" num="1.19"><w n="19.1">Peins</w> <w n="19.2">la</w> <w n="19.3">svelte</w> <w n="19.4">maigreur</w> <w n="19.5">aux</w> <w n="19.6">méplats</w> <w n="19.7">séduisants</w></l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">la</w> <w n="20.3">gracilité</w> <w n="20.4">des</w> <w n="20.5">filles</w> <w n="20.6">de</w> <w n="20.7">seize</w> <w n="20.8">ans</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Va</w>, <w n="21.2">ne</w> <w n="21.3">dédaigne</w> <w n="21.4">rien</w>, <w n="21.5">ni</w> <w n="21.6">la</w> <w n="21.7">bourgeoise</w> <w n="21.8">obèse</w></l>
					<l n="22" num="1.22"><w n="22.1">Ni</w> <w n="22.2">la</w> <w n="22.3">duchesse</w> <w n="22.4">au</w> <w n="22.5">front</w> <w n="22.6">d</w>’<w n="22.7">or</w> <w n="22.8">que</w> <w n="22.9">le</w> <w n="22.10">zéphyr</w> <w n="22.11">baise</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Ni</w> <w n="23.2">la</w> <w n="23.3">pierreuse</w>, <w n="23.4">proie</w> <w n="23.5">offerte</w> <w n="23.6">au</w> <w n="23.7">noir</w> <w n="23.8">filou</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Qui</w> <w n="24.2">peigne</w> <w n="24.3">ses</w> <w n="24.4">cheveux</w> <w n="24.5">lourds</w> <w n="24.6">avec</w> <w n="24.7">un</w> <w n="24.8">vieux</w> <w n="24.9">clou</w>,</l>
					<l n="25" num="1.25"><w n="25.1">Ni</w> <w n="25.2">la</w> <w n="25.3">bonne</w> <w n="25.4">admirant</w>, <w n="25.5">parmi</w> <w n="25.6">la</w> <w n="25.7">transparence</w></l>
					<l n="26" num="1.26"><w n="26.1">Des</w> <w n="26.2">bassins</w>, <w n="26.3">le</w> <w n="26.4">reflet</w> <w n="26.5">d</w>’<w n="26.6">un</w> <w n="26.7">pantalon</w> <w n="26.8">garance</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Ni</w> <w n="27.2">la</w> <w n="27.3">vieille</w> <w n="27.4">qui</w>, <w n="27.5">pour</w> <w n="27.6">implorer</w> <w n="27.7">un</w> <w n="27.8">secours</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Se</w> <w n="28.2">coiffe</w> <w n="28.3">d</w>’<w n="28.4">un</w> <w n="28.5">madras</w> <w n="28.6">et</w> <w n="28.7">chante</w> <w n="28.8">dans</w> <w n="28.9">les</w> <w n="28.10">cours</w>,</l>
					<l n="29" num="1.29"><w n="29.1">Ni</w> <w n="29.2">ces</w> <w n="29.3">filles</w> <w n="29.4">de</w> <w n="29.5">joie</w> <w n="29.6">aux</w> <w n="29.7">tragiques</w> <w n="29.8">allures</w></l>
					<l n="30" num="1.30"><w n="30.1">Offrant</w> <w n="30.2">au</w> <w n="30.3">vent</w> <w n="30.4">furtif</w> <w n="30.5">leurs</w> <w n="30.6">roses</w> <w n="30.7">chevelures</w>,</l>
					<l n="31" num="1.31"><w n="31.1">Et</w> <w n="31.2">poursuivant</w>, <w n="31.3">les</w> <w n="31.4">soirs</w>, <w n="31.5">leur</w> <w n="31.6">patient</w> <w n="31.7">calcul</w></l>
					<l n="32" num="1.32"><w n="32.1">Devant</w> <w n="32.2">les</w> <w n="32.3">Nouveautés</w> <w n="32.4">et</w> <w n="32.5">le</w> <w n="32.6">café</w> <w n="32.7">Méhul</w>,</l>
					<l n="33" num="1.33"><w n="33.1">Catins</w> <w n="33.2">dont</w> <w n="33.3">les</w> <w n="33.4">satins</w>, <w n="33.5">sans</w> <w n="33.6">jamais</w> <w n="33.7">faire</w> <w n="33.8">halte</w>,</l>
					<l n="34" num="1.34"><w n="34.1">Comme</w> <w n="34.2">des</w> <w n="34.3">serpents</w> <w n="34.4">noirs</w> <w n="34.5">se</w> <w n="34.6">traînent</w> <w n="34.7">sur</w> <w n="34.8">l</w>’<w n="34.9">asphalte</w> !</l>
					<l n="35" num="1.35"><w n="35.1">Regarde</w> <w n="35.2">l</w>’<w n="35.3">Homme</w> <w n="35.4">aussi</w> ! <w n="35.5">Peins</w> <w n="35.6">tous</w> <w n="35.7">les</w> <w n="35.8">noirs</w> <w n="35.9">troupeaux</w></l>
					<l n="36" num="1.36"><w n="36.1">Des</w> <w n="36.2">hommes</w>, <w n="36.3">sénateurs</w> <w n="36.4">on</w> <w n="36.5">bien</w> <w n="36.6">marchands</w> <w n="36.7">de</w> <w n="36.8">peaux</w></l>
					<l n="37" num="1.37"><w n="37.1">De</w> <w n="37.2">lapins</w> ; <w n="37.3">droit</w>, <w n="37.4">bossu</w>, <w n="37.5">formidable</w> <w n="37.6">ou</w> <w n="37.7">bancroche</w>,</l>
					<l n="38" num="1.38"><w n="38.1">Vois</w> <w n="38.2">l</w>’<w n="38.3">Homme</w>, <w n="38.4">vois</w>-<w n="38.5">le</w> <w n="38.6">bien</w>, <w n="38.7">de</w> <w n="38.8">d</w>’<w n="38.9">Arthez</w> <w n="38.10">à</w> <w n="38.11">Gavroche</w> !</l>
					<l n="39" num="1.39"><w n="39.1">L</w>’<w n="39.2">homme</w> <w n="39.3">actuel</w>, <w n="39.4">sublime</w> <w n="39.5">à</w> <w n="39.6">la</w> <w n="39.7">fois</w> <w n="39.8">et</w> <w n="39.9">mesquin</w>,</l>
					<l n="40" num="1.40"><w n="40.1">Est</w> <w n="40.2">vêtu</w> <w n="40.3">d</w>’<w n="40.4">un</w> <w n="40.5">complet</w>, <w n="40.6">comme</w> <w n="40.7">un</w> <w n="40.8">Américain</w> ;</l>
					<l n="41" num="1.41"><w n="41.1">Mais</w> <w n="41.2">tel</w> <w n="41.3">qu</w>’<w n="41.4">il</w> <w n="41.5">est</w>, <w n="41.6">ce</w> <w n="41.7">pitre</w>, <w n="41.8">épris</w> <w n="41.9">de</w> <w n="41.10">Navarette</w>,</l>
					<l n="42" num="1.42"><w n="42.1">Qui</w> <w n="42.2">dans</w> <w n="42.3">ses</w> <w n="42.4">doigts</w> <w n="42.5">pâlis</w> <w n="42.6">roule</w> <w n="42.7">une</w> <w n="42.8">cigarette</w>,</l>
					<l n="43" num="1.43"><w n="43.1">Lit</w> <w n="43.2">dans</w> <w n="43.3">les</w> <w n="43.4">astres</w> <w n="43.5">noirs</w> <w n="43.6">d</w>’<w n="43.7">un</w> <w n="43.8">œil</w> <w n="43.9">terrible</w> <w n="43.10">et</w> <w n="43.11">sûr</w>,</l>
					<l n="44" num="1.44"><w n="44.1">Voleur</w> <w n="44.2">divin</w>, <w n="44.3">saisit</w> <w n="44.4">Isis</w> <w n="44.5">en</w> <w n="44.6">plein</w> <w n="44.7">azur</w>,</l>
					<l n="45" num="1.45"><w n="45.1">Pose</w> <w n="45.2">un</w> <w n="45.3">baiser</w> <w n="45.4">brutal</w> <w n="45.5">sur</w> <w n="45.6">ses</w> <w n="45.7">yeux</w> <w n="45.8">pleins</w> <w n="45.9">d</w>’<w n="45.10">étoiles</w>,</l>
					<l n="46" num="1.46"><w n="46.1">D</w>’<w n="46.2">un</w> <w n="46.3">ongle</w> <w n="46.4">furieux</w> <w n="46.5">déchire</w> <w n="46.6">tous</w> <w n="46.7">ses</w> <w n="46.8">voiles</w>,</l>
					<l n="47" num="1.47"><w n="47.1">Comme</w> <w n="47.2">un</w> <w n="47.3">fer</w> <w n="47.4">rouge</w> <w n="47.5">met</w> <w n="47.6">la</w> <w n="47.7">lèvre</w> <w n="47.8">sur</w> <w n="47.9">son</w> <w n="47.10">col</w></l>
					<l n="48" num="1.48"><w n="48.1">Et</w> <w n="48.2">la</w> <w n="48.3">contemple</w>, <w n="48.4">et</w> <w n="48.5">pâle</w> <w n="48.6">encor</w> <w n="48.7">de</w> <w n="48.8">son</w> <w n="48.9">viol</w>,</l>
					<l n="49" num="1.49"><w n="49.1">A</w> <w n="49.2">ses</w> <w n="49.3">pieds</w> <w n="49.4">gémissant</w> <w n="49.5">une</w> <w n="49.6">plainte</w> <w n="49.7">ingénue</w></l>
					<l n="50" num="1.50"><w n="50.1">Regarde</w> <w n="50.2">la</w> <w n="50.3">Nature</w> <w n="50.4">échevelée</w> <w n="50.5">et</w> <w n="50.6">nue</w>.</l>
					<l n="51" num="1.51"><w n="51.1">Oui</w>, <w n="51.2">l</w>’<w n="51.3">Homme</w>, <w n="51.4">vois</w>-<w n="51.5">le</w> <w n="51.6">bien</w>, <w n="51.7">tire</w> <w n="51.8">parti</w> <w n="51.9">de</w> <w n="51.10">tout</w> !</l>
					<l n="52" num="1.52"><w n="52.1">Il</w> <w n="52.2">est</w> <w n="52.3">beau</w>, <w n="52.4">l</w>’<w n="52.5">orateur</w> <w n="52.6">farouche</w>, <w n="52.7">qui</w> <w n="52.8">debout</w>,</l>
					<l n="53" num="1.53"><w n="53.1">Du</w> <w n="53.2">Progrès</w> <w n="53.3">fugitif</w> <w n="53.4">embrassant</w> <w n="53.5">la</w> <w n="53.6">chimère</w>,</l>
					<l n="54" num="1.54"><w n="54.1">Parle</w> <w n="54.2">et</w> <w n="54.3">courbe</w> <w n="54.4">les</w> <w n="54.5">fronts</w> <w n="54.6">sous</w> <w n="54.7">sa</w> <w n="54.8">parole</w> <w n="54.9">amère</w> ;</l>
					<l n="55" num="1.55"><w n="55.1">Mais</w> <w n="55.2">le</w> <w n="55.3">vieux</w> <w n="55.4">chiffonnier</w>, <w n="55.5">qui</w> <w n="55.6">sous</w> <w n="55.7">le</w> <w n="55.8">ciel</w> <w n="55.9">changeant</w></l>
					<l n="56" num="1.56"><w n="56.1">Montre</w> <w n="56.2">son</w> <w n="56.3">crochet</w> <w n="56.4">noir</w> <w n="56.5">et</w> <w n="56.6">sa</w> <w n="56.7">barbe</w> <w n="56.8">d</w>’<w n="56.9">argent</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Près</w> <w n="57.2">de</w> <w n="57.3">la</w> <w n="57.4">verte</w> <w n="57.5">Seine</w> <w n="57.6">a</w> <w n="57.7">des</w> <w n="57.8">beautés</w> <w n="57.9">de</w> <w n="57.10">Fleuve</w>.</l>
					<l n="58" num="1.58"><w n="58.1">Et</w> <w n="58.2">c</w>’<w n="58.3">est</w> <w n="58.4">un</w> <w n="58.5">beau</w> <w n="58.6">modèle</w>, <w n="58.7">avec</w> <w n="58.8">sa</w> <w n="58.9">blouse</w> <w n="58.10">neuve</w>,</l>
					<l n="59" num="1.59"><w n="59.1">Que</w> <w n="59.2">l</w>’<w n="59.3">Alphonse</w> <w n="59.4">blêmi</w>, <w n="59.5">fashionable</w> <w n="59.6">et</w> <w n="59.7">vainqueur</w>,</l>
					<l n="60" num="1.60"><w n="60.1">Dont</w> <w n="60.2">la</w> <w n="60.3">cravate</w> <w n="60.4">rose</w> <w n="60.5">et</w> <w n="60.6">les</w> <w n="60.7">accroche</w>-<w n="60.8">cœur</w></l>
					<l n="61" num="1.61"><w n="61.1">Font</w> <w n="61.2">fanatisme</w>, <w n="61.3">et</w> <w n="61.4">qui</w>, <w n="61.5">doux</w> <w n="61.6">jeune</w> <w n="61.7">homme</w> <w n="61.8">de</w> <w n="61.9">joie</w>,</l>
					<l n="62" num="1.62"><w n="62.1">Tortille</w> <w n="62.2">crânement</w> <w n="62.3">sa</w> <w n="62.4">casquette</w> <w n="62.5">de</w> <w n="62.6">soie</w>.</l>
					<l n="63" num="1.63"><w n="63.1">Oh</w> ! <w n="63.2">ne</w> <w n="63.3">dédaigne</w> <w n="63.4">rien</w> <w n="63.5">dans</w> <w n="63.6">ta</w> <w n="63.7">ville</w> ! <w n="63.8">Chéris</w></l>
					<l n="64" num="1.64"><w n="64.1">Les</w> <w n="64.2">parcs</w> <w n="64.3">éblouissants</w>, <w n="64.4">ces</w> <w n="64.5">jardins</w> <w n="64.6">de</w> <w n="64.7">Paris</w></l>
					<l n="65" num="1.65"><w n="65.1">Où</w> <w n="65.2">pour</w> <w n="65.3">nous</w> <w n="65.4">réjouir</w>, <w n="65.5">en</w> <w n="65.6">leurs</w> <w n="65.7">apothéoses</w></l>
					<l n="66" num="1.66"><w n="66.1">Brillent</w> <w n="66.2">les</w> <w n="66.3">cœurs</w> <w n="66.4">sanglants</w> <w n="66.5">et</w> <w n="66.6">fulgurants</w> <w n="66.7">des</w> <w n="66.8">roses</w> ;</l>
					<l n="67" num="1.67"><w n="67.1">Mais</w>, <w n="67.2">artiste</w>, <w n="67.3">aime</w> <w n="67.4">aussi</w> <w n="67.5">les</w> <w n="67.6">pauvres</w> <w n="67.7">talus</w> <w n="67.8">des</w></l>
					<l n="68" num="1.68"><w n="68.1">Fortifications</w>, <w n="68.2">où</w> <w n="68.3">sous</w> <w n="68.4">le</w> <w n="68.5">triste</w> <w n="68.6">dais</w></l>
					<l n="69" num="1.69"><w n="69.1">Du</w> <w n="69.2">ciel</w> <w n="69.3">gris</w>, <w n="69.4">l</w>’<w n="69.5">herbe</w> <w n="69.6">jaune</w> <w n="69.7">et</w> <w n="69.8">sèche</w> <w n="69.9">qui</w> <w n="69.10">se</w> <w n="69.11">pèle</w></l>
					<l n="70" num="1.70"><w n="70.1">Semble</w> <w n="70.2">un</w> <w n="70.3">front</w> <w n="70.4">dévoré</w> <w n="70.5">par</w> <w n="70.6">un</w> <w n="70.7">érésipèle</w> ;</l>
					<l n="71" num="1.71"><w n="71.1">Car</w> <w n="71.2">c</w>’<w n="71.3">est</w> <w n="71.4">là</w> <w n="71.5">que</w>, <w n="71.6">toujours</w> <w n="71.7">las</w> <w n="71.8">de</w> <w n="71.9">voir</w> <w n="71.10">empirer</w></l>
					<l n="72" num="1.72"><w n="72.1">Son</w> <w n="72.2">destin</w>, <w n="72.3">l</w>’<w n="72.4">ouvrier</w> <w n="72.5">captif</w> <w n="72.6">vient</w> <w n="72.7">respirer</w></l>
					<l n="73" num="1.73"><w n="73.1">Et</w> <w n="73.2">que</w> <w n="73.3">la</w> <w n="73.4">jeune</w> <w n="73.5">fille</w> <w n="73.6">heureuse</w>, <w n="73.7">en</w> <w n="73.8">mince</w> <w n="73.9">robe</w>,</l>
					<l n="74" num="1.74"><w n="74.1">Laissant</w> <w n="74.2">errer</w> <w n="74.3">son</w> <w n="74.4">clair</w> <w n="74.5">sourire</w>, <w n="74.6">où</w> <w n="74.7">se</w> <w n="74.8">dérobe</w></l>
					<l n="75" num="1.75"><w n="75.1">Quelque</w> <w n="75.2">rêve</w> <w n="75.3">secret</w> <w n="75.4">de</w> <w n="75.5">ménage</w> <w n="75.6">et</w> <w n="75.7">d</w>’<w n="75.8">amour</w>,</l>
					<l n="76" num="1.76"><w n="76.1">Avec</w> <w n="76.2">ses</w> <w n="76.3">yeux</w> <w n="76.4">brûlants</w> <w n="76.5">vient</w> <w n="76.6">boire</w> <w n="76.7">un</w> <w n="76.8">peu</w> <w n="76.9">de</w> <w n="76.10">jour</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1879"> 10 avril 1879.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>