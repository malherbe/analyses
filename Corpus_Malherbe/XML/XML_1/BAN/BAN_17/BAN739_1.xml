<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN739">
				<head type="main">La Coupe</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">poëte</w> <w n="1.3">en</w> <w n="1.4">sa</w> <w n="1.5">coupe</w>, <w n="1.6">orgueil</w> <w n="1.7">du</w> <w n="1.8">ciseleur</w>,</l>
					<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">enivre</w>, <w n="2.3">et</w> <w n="2.4">boit</w> <w n="2.5">le</w> <w n="2.6">vin</w> <w n="2.7">amer</w> <w n="2.8">de</w> <w n="2.9">la</w> <w n="2.10">douleur</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Puis</w>, <w n="3.2">après</w> <w n="3.3">avoir</w> <w n="3.4">bu</w> <w n="3.5">le</w> <w n="3.6">vin</w>, <w n="3.7">il</w> <w n="3.8">boit</w> <w n="3.9">la</w> <w n="3.10">lie</w></l>
					<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">dorment</w> <w n="4.3">la</w> <w n="4.4">tristesse</w> <w n="4.5">et</w> <w n="4.6">la</w> <w n="4.7">mélancolie</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">puis</w>, <w n="5.3">après</w> <w n="5.4">la</w> <w n="5.5">lie</w> <w n="5.6">encore</w>, <w n="5.7">tout</w> <w n="5.8">au</w> <w n="5.9">fond</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Dorment</w> <w n="6.2">en</w> <w n="6.3">un</w> <w n="6.4">flot</w> <w n="6.5">noir</w> <w n="6.6">l</w>’<w n="6.7">accablement</w> <w n="6.8">profond</w></l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">inutile</w> <w n="7.4">amour</w> <w n="7.5">de</w> <w n="7.6">l</w>’<w n="7.7">Idéal</w> <w n="7.8">qui</w> <w n="7.9">lève</w></l>
					<l n="8" num="1.8"><w n="8.1">Son</w> <w n="8.2">front</w> <w n="8.3">chaste</w>, <w n="8.4">et</w> <w n="8.5">l</w>’<w n="8.6">horreur</w> <w n="8.7">effrayante</w> <w n="8.8">du</w> <w n="8.9">rêve</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">comme</w>, <w n="9.3">en</w> <w n="9.4">regardant</w> <w n="9.5">longtemps</w> <w n="9.6">ce</w> <w n="9.7">flot</w> <w n="9.8">moqueur</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Le</w> <w n="10.2">poëte</w> <w n="10.3">qui</w> <w n="10.4">sent</w> <w n="10.5">se</w> <w n="10.6">soulever</w> <w n="10.7">son</w> <w n="10.8">cœur</w>,</l>
					<l n="11" num="1.11"><w n="11.1">A</w> <w n="11.2">dans</w> <w n="11.3">ses</w> <w n="11.4">sombres</w> <w n="11.5">yeux</w> <w n="11.6">l</w>’<w n="11.7">égarement</w> <w n="11.8">d</w>’<w n="11.9">Oreste</w>,</l>
					<l n="12" num="1.12"><w n="12.1">La</w> <w n="12.2">Muse</w> <w n="12.3">lui</w> <w n="12.4">dit</w> : <w n="12.5">Mon</w> <w n="12.6">bien</w>-<w n="12.7">aimé</w>, <w n="12.8">bois</w> <w n="12.9">le</w> <w n="12.10">reste</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1886">Paris, le dimanche 5 septembre 1886.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>