<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DANS LA FOURNAISE</title>
				<title type="sub_2">Dernières Poésies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4181 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_17</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">DANS LA FOURNAISE</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="2001">2001</date>
								</imprint>
								<biblScope unit="tome">VIII</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Dans la fournaise</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>G. Charpentier et E. Fasquelle, Éditeurs</publisher>
							<date when="1892">1892</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1892">1892</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN698">
				<head type="main">Mourir, dormir</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">boite</w> <w n="1.3">affreusement</w>, <w n="1.4">ce</w> <w n="1.5">vieux</w> <w n="1.6">cheval</w> <w n="1.7">de</w> <w n="1.8">fiacre</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Ses</w> <w n="2.2">yeux</w> <w n="2.3">tout</w> <w n="2.4">grands</w> <w n="2.5">ouverts</w> <w n="2.6">ont</w> <w n="2.7">des</w> <w n="2.8">blancheurs</w> <w n="2.9">de</w> <w n="2.10">nacre</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">voudrait</w> <w n="3.3">se</w> <w n="3.4">coucher</w>, <w n="3.5">dormir</w> ; <w n="3.6">il</w> <w n="3.7">ne</w> <w n="3.8">peut</w> <w n="3.9">pas</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">le</w> <w n="4.3">pavé</w> <w n="4.4">glissant</w> <w n="4.5">il</w> <w n="4.6">bute</w> <w n="4.7">à</w> <w n="4.8">chaque</w> <w n="4.9">pas</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">ressemble</w> <w n="5.3">à</w> <w n="5.4">ces</w> <w n="5.5">morts</w> <w n="5.6">qu</w>’<w n="5.7">on</w> <w n="5.8">traîne</w> <w n="5.9">sur</w> <w n="5.10">des</w> <w n="5.11">claies</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">Ses</w> <w n="6.2">jambes</w> <w n="6.3">et</w> <w n="6.4">ses</w> <w n="6.5">flancs</w> <w n="6.6">sont</w> <w n="6.7">tout</w> <w n="6.8">couverts</w> <w n="6.9">de</w> <w n="6.10">plaies</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Sa</w> <w n="7.2">bouche</w> <w n="7.3">molle</w> <w n="7.4">et</w> <w n="7.5">noire</w> <w n="7.6">est</w> <w n="7.7">gonflée</w> <w n="7.8">en</w> <w n="7.9">dedans</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Tragique</w>, <w n="8.2">il</w> <w n="8.3">mord</w> <w n="8.4">le</w> <w n="8.5">vide</w> <w n="8.6">avec</w> <w n="8.7">ses</w> <w n="8.8">longues</w> <w n="8.9">dents</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Tandis</w> <w n="9.2">que</w> <w n="9.3">le</w> <w n="9.4">cocher</w> <w n="9.5">l</w>’<w n="9.6">injurie</w> <w n="9.7">et</w> <w n="9.8">le</w> <w n="9.9">fouaille</w></l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">chaque</w> <w n="10.3">fois</w> <w n="10.4">déchire</w> <w n="10.5">une</w> <w n="10.6">nouvelle</w> <w n="10.7">entaille</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Gros</w> <w n="11.2">homme</w> <w n="11.3">rouge</w>, <w n="11.4">avec</w> <w n="11.5">des</w> <w n="11.6">gaîtés</w> <w n="11.7">de</w> <w n="11.8">noceur</w>.</l>
					<l n="12" num="1.12"><w n="12.1">En</w> <w n="12.2">quelque</w> <w n="12.3">horrible</w> <w n="12.4">songe</w> <w n="12.5">il</w> <w n="12.6">voit</w> <w n="12.7">l</w>’<w n="12.8">équarrisseur</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Alors</w>, <w n="13.2">comme</w> <w n="13.3">il</w> <w n="13.4">trébuche</w>, <w n="13.5">accablé</w> <w n="13.6">par</w> <w n="13.7">ce</w> <w n="13.8">rêve</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Bien</w> <w n="14.2">vite</w>, <w n="14.3">à</w> <w n="14.4">coups</w> <w n="14.5">de</w> <w n="14.6">fouet</w> <w n="14.7">son</w> <w n="14.8">bourreau</w> <w n="14.9">le</w> <w n="14.10">relève</w>.</l>
					<l n="15" num="1.15"><w n="15.1">Allons</w>, <w n="15.2">hue</w> ! <w n="15.3">Eh</w> ! <w n="15.4">va</w> <w n="15.5">donc</w>, <w n="15.6">carcan</w> ! <w n="15.7">va</w> <w n="15.8">donc</w>, <w n="15.9">chahut</w> !</l>
					<l n="16" num="1.16"><w n="16.1">Eh</w> ! <w n="16.2">va</w> <w n="16.3">donc</w>, <w n="16.4">président</w> ! <w n="16.5">carcasse</w> ! <w n="16.6">Gamahut</w> !</l>
					<l n="17" num="1.17"><w n="17.1">Sur</w> <w n="17.2">le</w> <w n="17.3">cheval</w>, <w n="17.4">en</w> <w n="17.5">proie</w> <w n="17.6">aux</w> <w n="17.7">angoisses</w> <w n="17.8">dernières</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Le</w> <w n="18.2">fouet</w>, <w n="18.3">ivre</w> <w n="18.4">et</w> <w n="18.5">féroce</w>, <w n="18.6">enlève</w> <w n="18.7">des</w> <w n="18.8">lanières</w>.</l>
					<l n="19" num="1.19"><w n="19.1">Ce</w> <w n="19.2">pauvre</w> <w n="19.3">être</w> <w n="19.4">perclus</w>, <w n="19.5">battu</w>, <w n="19.6">martyrisé</w></l>
					<l n="20" num="1.20"><w n="20.1">Que</w> <w n="20.2">tourmente</w> <w n="20.3">un</w> <w n="20.4">rayon</w> <w n="20.5">du</w> <w n="20.6">soleil</w> <w n="20.7">irisé</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Cet</w> <w n="21.2">affamé</w> <w n="21.3">qui</w> <w n="21.4">n</w>’<w n="21.5">a</w> <w n="21.6">pas</w> <w n="21.7">eu</w> <w n="21.8">d</w>’<w n="21.9">avoine</w>, <w n="21.10">en</w> <w n="21.11">somme</w></l>
					<l n="22" num="1.22"><w n="22.1">N</w>’<w n="22.2">est</w> <w n="22.3">qu</w>’<w n="22.4">une</w> <w n="22.5">rosse</w>. <w n="22.6">Il</w> <w n="22.7">est</w> <w n="22.8">malheureux</w> <w n="22.9">comme</w> <w n="22.10">un</w> <w n="22.11">homme</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1887">Mercredi, 12 janvier 1887.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>