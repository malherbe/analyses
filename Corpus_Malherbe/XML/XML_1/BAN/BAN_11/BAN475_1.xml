<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN475">
					<head type="number">I</head>
					<head type="main">Ballade de ses regrets pour l’an mil huit cent trente</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">veux</w> <w n="1.3">chanter</w> <w n="1.4">ma</w> <w n="1.5">ballade</w> <w n="1.6">à</w> <w n="1.7">mon</w> <w n="1.8">tour</w> !</l>
						<l n="2" num="1.2"><w n="2.1">O</w> <w n="2.2">Poésie</w>, <w n="2.3">ô</w> <w n="2.4">ma</w> <w n="2.5">mère</w> <w n="2.6">mourante</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">tes</w> <w n="3.3">fils</w> <w n="3.4">t</w>’<w n="3.5">aimaient</w> <w n="3.6">d</w>’<w n="3.7">un</w> <w n="3.8">grand</w> <w n="3.9">amour</w></l>
						<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">ce</w> <w n="4.3">Paris</w>, <w n="4.4">en</w> <w n="4.5">l</w>’<w n="4.6">an</w> <w n="4.7">mil</w> <w n="4.8">huit</w> <w n="4.9">cent</w> <w n="4.10">trente</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Pour</w> <w n="5.2">eux</w> <w n="5.3">les</w> <w n="5.4">docks</w>, <w n="5.5">l</w>’<w n="5.6">autrichien</w>, <w n="5.7">la</w> <w n="5.8">rente</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">mots</w> <w n="6.3">de</w> <w n="6.4">bourse</w> <w n="6.5">étaient</w> <w n="6.6">du</w> <w n="6.7">pur</w> <w n="6.8">hébreu</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Enfant</w> <w n="7.2">divin</w>, <w n="7.3">plus</w> <w n="7.4">beau</w> <w n="7.5">que</w> <w n="7.6">Richelieu</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Musset</w> <w n="8.2">chantait</w>, <w n="8.3">Hugo</w> <w n="8.4">tenait</w> <w n="8.5">la</w> <w n="8.6">lyre</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Jeune</w>, <w n="9.2">superbe</w>, <w n="9.3">écouté</w> <w n="9.4">comme</w> <w n="9.5">un</w> <w n="9.6">dieu</w>.</l>
						<l n="10" num="1.10"><w n="10.1">Mais</w> <w n="10.2">à</w> <w n="10.3">présent</w>, <w n="10.4">c</w>’<w n="10.5">est</w> <w n="10.6">bien</w> <w n="10.7">fini</w> <w n="10.8">de</w> <w n="10.9">rire</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">chez</w> <w n="11.4">Nodier</w> <w n="11.5">que</w> <w n="11.6">se</w> <w n="11.7">tenait</w> <w n="11.8">la</w> <w n="11.9">cour</w>.</l>
						<l n="12" num="2.2"><w n="12.1">Les</w> <w n="12.2">deux</w> <w n="12.3">Deschamps</w> <w n="12.4">à</w> <w n="12.5">la</w> <w n="12.6">voix</w> <w n="12.7">enivrante</w></l>
						<l n="13" num="2.3"><w n="13.1">Et</w> <w n="13.2">de</w> <w n="13.3">Vigny</w> <w n="13.4">charmaient</w> <w n="13.5">ce</w> <w n="13.6">clair</w> <w n="13.7">séjour</w>.</l>
						<l n="14" num="2.4"><w n="14.1">Dorval</w> <w n="14.2">en</w> <w n="14.3">pleurs</w>, <w n="14.4">tragique</w> <w n="14.5">et</w> <w n="14.6">déchirante</w>,</l>
						<l n="15" num="2.5"><w n="15.1">Galvanisait</w> <w n="15.2">la</w> <w n="15.3">foule</w> <w n="15.4">indifférente</w>.</l>
						<l n="16" num="2.6"><w n="16.1">Les</w> <w n="16.2">diamants</w> <w n="16.3">foisonnaient</w> <w n="16.4">au</w> <w n="16.5">ciel</w> <w n="16.6">bleu</w> !</l>
						<l n="17" num="2.7"><w n="17.1">Passât</w> <w n="17.2">la</w> <w n="17.3">Gloire</w> <w n="17.4">avec</w> <w n="17.5">son</w> <w n="17.6">char</w> <w n="17.7">de</w> <w n="17.8">feu</w>,</l>
						<l n="18" num="2.8"><w n="18.1">On</w> <w n="18.2">y</w> <w n="18.3">courait</w> <w n="18.4">comme</w> <w n="18.5">un</w> <w n="18.6">juste</w> <w n="18.7">au</w> <w n="18.8">martyre</w>,</l>
						<l n="19" num="2.9"><w n="19.1">Dût</w>-<w n="19.2">on</w> <w n="19.3">se</w> <w n="19.4">voir</w> <w n="19.5">écrasé</w> <w n="19.6">sous</w> <w n="19.7">l</w>’<w n="19.8">essieu</w>.</l>
						<l n="20" num="2.10"><w n="20.1">Mais</w> <w n="20.2">à</w> <w n="20.3">présent</w>, <w n="20.4">c</w>’<w n="20.5">est</w> <w n="20.6">bien</w> <w n="20.7">fini</w> <w n="20.8">de</w> <w n="20.9">rire</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Des</w> <w n="21.2">joailliers</w> <w n="21.3">connus</w> <w n="21.4">dans</w> <w n="21.5">Visapour</w></l>
						<l n="22" num="3.2"><w n="22.1">Et</w> <w n="22.2">des</w> <w n="22.3">seigneurs</w> <w n="22.4">arrivés</w> <w n="22.5">de</w> <w n="22.6">Tarente</w></l>
						<l n="23" num="3.3"><w n="23.1">Pour</w> <w n="23.2">Cidalise</w> <w n="23.3">ou</w> <w n="23.4">pour</w> <w n="23.5">la</w> <w n="23.6">Pompadour</w></l>
						<l n="24" num="3.4"><w n="24.1">Se</w> <w n="24.2">provoquaient</w> <w n="24.3">de</w> <w n="24.4">façon</w> <w n="24.5">conquérante</w>,</l>
						<l n="25" num="3.5"><w n="25.1">La</w> <w n="25.2">brise</w> <w n="25.3">en</w> <w n="25.4">fleur</w> <w n="25.5">nous</w> <w n="25.6">venait</w> <w n="25.7">de</w> <w n="25.8">Sorrente</w> !</l>
						<l n="26" num="3.6"><w n="26.1">A</w> <w n="26.2">ce</w> <w n="26.3">jourd</w>’<w n="26.4">hui</w> <w n="26.5">les</w> <w n="26.6">rimeurs</w>, <w n="26.7">ventrebleu</w> !</l>
						<l n="27" num="3.7"><w n="27.1">Savent</w> <w n="27.2">le</w> <w n="27.3">prix</w> <w n="27.4">d</w>’<w n="27.5">un</w> <w n="27.6">lys</w> <w n="27.7">et</w> <w n="27.8">d</w>’<w n="27.9">un</w> <w n="27.10">cheveu</w> ;</l>
						<l n="28" num="3.8"><w n="28.1">Ils</w> <w n="28.2">comptent</w> <w n="28.3">bien</w> ; <w n="28.4">plus</w> <w n="28.5">de</w> <w n="28.6">sacré</w> <w n="28.7">délire</w> !</l>
						<l n="29" num="3.9"><w n="29.1">Tout</w> <w n="29.2">est</w> <w n="29.3">conquis</w> <w n="29.4">par</w> <w n="29.5">des</w> <w n="29.6">fesse</w>-<w n="29.7">Mathieu</w> :</l>
						<l n="30" num="3.10"><w n="30.1">Mais</w> <w n="30.2">à</w> <w n="30.3">présent</w>, <w n="30.4">c</w>’<w n="30.5">est</w> <w n="30.6">bien</w> <w n="30.7">fini</w> <w n="30.8">de</w> <w n="30.9">rire</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="31" num="4.1"><w n="31.1">En</w> <w n="31.2">ce</w> <w n="31.3">temps</w>-<w n="31.4">là</w>, <w n="31.5">moi</w>-<w n="31.6">même</w>, <w n="31.7">pour</w> <w n="31.8">un</w> <w n="31.9">peu</w>,</l>
						<l n="32" num="4.2"><w n="32.1">Féru</w> <w n="32.2">d</w>’<w n="32.3">amour</w> <w n="32.4">pour</w> <w n="32.5">celle</w> <w n="32.6">dont</w> <w n="32.7">l</w>’<w n="32.8">aveu</w></l>
						<l n="33" num="4.3"><w n="33.1">Fait</w> <w n="33.2">ici</w>-<w n="33.3">bas</w> <w n="33.4">les</w> <w n="33.5">Dante</w> <w n="33.6">et</w> <w n="33.7">les</w> <w n="33.8">Shakspere</w>,</l>
						<l n="34" num="4.4"><w n="34.1">J</w>’<w n="34.2">aurais</w> <w n="34.3">baisé</w> <w n="34.4">son</w> <w n="34.5">brodequin</w> <w n="34.6">par</w> <w n="34.7">jeu</w> !</l>
						<l n="35" num="4.5"><w n="35.1">Mais</w> <w n="35.2">à</w> <w n="35.3">présent</w>, <w n="35.4">c</w>’<w n="35.5">est</w> <w n="35.6">bien</w> <w n="35.7">fini</w> <w n="35.8">de</w> <w n="35.9">rire</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1862">Janvier 1862.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>