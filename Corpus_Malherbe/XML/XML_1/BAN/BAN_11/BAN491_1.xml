<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN491">
					<head type="number">XVII</head>
					<head type="main">Ballade pour annoncer le Printemps</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Elle</w> <w n="1.2">frémit</w>, <w n="1.3">la</w> <w n="1.4">brise</w> <w n="1.5">pure</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">ce</w> <w n="2.3">beau</w> <w n="2.4">jardin</w> <w n="2.5">de</w> <w n="2.6">féerie</w></l>
						<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">le</w> <w n="3.3">ruisseau</w> <w n="3.4">jaseur</w> <w n="3.5">murmure</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">printemps</w> <w n="4.3">affolé</w> <w n="4.4">varie</w></l>
						<l n="5" num="1.5"><w n="5.1">Sa</w> <w n="5.2">merveilleuse</w> <w n="5.3">broderie</w>,</l>
						<l n="6" num="1.6"><w n="6.1">L</w>’<w n="6.2">eau</w> <w n="6.3">chante</w> <w n="6.4">sous</w> <w n="6.5">les</w> <w n="6.6">passerelles</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Tout</w> <w n="7.2">tressaille</w> <w n="7.3">dans</w> <w n="7.4">la</w> <w n="7.5">prairie</w></l>
						<l n="8" num="1.8"><w n="8.1">A</w> <w n="8.2">la</w> <w n="8.3">façon</w> <w n="8.4">des</w> <w n="8.5">tourterelles</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Les</w> <w n="9.2">arbres</w> <w n="9.3">dans</w> <w n="9.4">l</w>’<w n="9.5">allée</w> <w n="9.6">obscure</w></l>
						<l n="10" num="2.2"><w n="10.1">Où</w> <w n="10.2">babille</w> <w n="10.3">la</w> <w n="10.4">causerie</w></l>
						<l n="11" num="2.3"><w n="11.1">Laissent</w> <w n="11.2">leur</w> <w n="11.3">jeune</w> <w n="11.4">chevelure</w></l>
						<l n="12" num="2.4"><w n="12.1">Flotter</w> <w n="12.2">avec</w> <w n="12.3">coquetterie</w>.</l>
						<l n="13" num="2.5"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">le</w> <w n="13.4">temps</w> <w n="13.5">où</w> <w n="13.6">le</w> <w n="13.7">ciel</w> <w n="13.8">vous</w> <w n="13.9">crie</w></l>
						<l n="14" num="2.6"><w n="14.1">D</w>’<w n="14.2">oublier</w> <w n="14.3">chagrins</w> <w n="14.4">et</w> <w n="14.5">querelles</w>,</l>
						<l n="15" num="2.7"><w n="15.1">Et</w> <w n="15.2">de</w> <w n="15.3">vivre</w> <w n="15.4">en</w> <w n="15.5">galanterie</w></l>
						<l n="16" num="2.8"><w n="16.1">A</w> <w n="16.2">la</w> <w n="16.3">façon</w> <w n="16.4">des</w> <w n="16.5">tourterelles</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">L</w>’<w n="17.2">insecte</w> <w n="17.3">court</w> <w n="17.4">dans</w> <w n="17.5">la</w> <w n="17.6">verdure</w>.</l>
						<l n="18" num="3.2"><w n="18.1">Le</w> <w n="18.2">bois</w> <w n="18.3">est</w> <w n="18.4">plein</w> <w n="18.5">de</w> <w n="18.6">rêverie</w> ;</l>
						<l n="19" num="3.3"><w n="19.1">La</w> <w n="19.2">nymphe</w> <w n="19.3">a</w> <w n="19.4">quitté</w> <w n="19.5">sa</w> <w n="19.6">ceinture</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Le</w> <w n="20.2">sylphe</w> <w n="20.3">avec</w> <w n="20.4">idolâtrie</w></l>
						<l n="21" num="3.5"><w n="21.1">Baise</w> <w n="21.2">la</w> <w n="21.3">pelouse</w> <w n="21.4">fleurie</w>,</l>
						<l n="22" num="3.6"><w n="22.1">Les</w> <w n="22.2">fleurs</w> <w n="22.3">ont</w> <w n="22.4">ouvert</w> <w n="22.5">leurs</w> <w n="22.6">ombrelles</w> ;</l>
						<l n="23" num="3.7"><w n="23.1">Enfants</w>, <w n="23.2">il</w> <w n="23.3">faut</w> <w n="23.4">qu</w>’<w n="23.5">on</w> <w n="23.6">se</w> <w n="23.7">marie</w></l>
						<l n="24" num="3.8"><w n="24.1">A</w> <w n="24.2">la</w> <w n="24.3">façon</w> <w n="24.4">des</w> <w n="24.5">tourterelles</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="25" num="4.1"><w n="25.1">La</w> <w n="25.2">colombe</w> <w n="25.3">murmure</w> <w n="25.4">et</w> <w n="25.5">prie</w></l>
						<l n="26" num="4.2"><w n="26.1">Et</w> <w n="26.2">chuchote</w> <w n="26.3">sur</w> <w n="26.4">les</w> <w n="26.5">tourelles</w> :</l>
						<l n="27" num="4.3"><w n="27.1">Mariez</w>-<w n="27.2">vous</w>, <w n="27.3">belle</w> <w n="27.4">Marie</w>,</l>
						<l n="28" num="4.4"><w n="28.1">A</w> <w n="28.2">la</w> <w n="28.3">façon</w> <w n="28.4">des</w> <w n="28.5">tourterelles</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1861">Avril 1861.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>