<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN480">
					<head type="number">VI</head>
					<head type="main">Ballade de sa fidélité à la Poésie</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Chacun</w> <w n="1.2">s</w>’<w n="1.3">écrie</w> <w n="1.4">avec</w> <w n="1.5">un</w> <w n="1.6">air</w> <w n="1.7">de</w> <w n="1.8">gloire</w> :</l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">moi</w> <w n="2.3">le</w> <w n="2.4">sac</w>, <w n="2.5">à</w> <w n="2.6">moi</w> <w n="2.7">le</w> <w n="2.8">million</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">veux</w> <w n="3.3">jouir</w>, <w n="3.4">je</w> <w n="3.5">veux</w> <w n="3.6">manger</w> <w n="3.7">et</w> <w n="3.8">boire</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Donnez</w>-<w n="4.2">moi</w> <w n="4.3">vite</w>, <w n="4.4">et</w> <w n="4.5">sans</w> <w n="4.6">rébellion</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Ma</w> <w n="5.2">part</w> <w n="5.3">d</w>’<w n="5.4">argent</w> ; <w n="5.5">on</w> <w n="5.6">me</w> <w n="5.7">nomme</w> <w n="5.8">Lion</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">Dieux</w> <w n="6.3">sont</w> <w n="6.4">morts</w>, <w n="6.5">et</w> <w n="6.6">morte</w> <w n="6.7">l</w>’<w n="6.8">allégresse</w>,</l>
						<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">art</w> <w n="7.3">défleurit</w>, <w n="7.4">la</w> <w n="7.5">muse</w> <w n="7.6">en</w> <w n="7.7">sa</w> <w n="7.8">détresse</w></l>
						<l n="8" num="1.8"><w n="8.1">Fuit</w>, <w n="8.2">les</w> <w n="8.3">seins</w> <w n="8.4">nus</w>, <w n="8.5">sous</w> <w n="8.6">un</w> <w n="8.7">vent</w> <w n="8.8">meurtrier</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">cependant</w> <w n="9.3">tu</w> <w n="9.4">demandes</w>, <w n="9.5">maîtresse</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Pourquoi</w> <w n="10.2">je</w> <w n="10.3">vis</w> ? <w n="10.4">Pour</w> <w n="10.5">l</w>’<w n="10.6">amour</w> <w n="10.7">du</w> <w n="10.8">laurier</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">O</w> <w n="11.2">Piéride</w>, <w n="11.3">ô</w> <w n="11.4">fille</w> <w n="11.5">de</w> <w n="11.6">Mémoire</w>,</l>
						<l n="12" num="2.2"><w n="12.1">Trouvons</w> <w n="12.2">des</w> <w n="12.3">vers</w> <w n="12.4">dignes</w> <w n="12.5">de</w> <w n="12.6">Pollion</w> !</l>
						<l n="13" num="2.3"><w n="13.1">Non</w>, <w n="13.2">mon</w> <w n="13.3">ami</w>, <w n="13.4">vends</w> <w n="13.5">ta</w> <w n="13.6">prose</w> <w n="13.7">à</w> <w n="13.8">la</w> <w n="13.9">foire</w>.</l>
						<l n="14" num="2.4"><w n="14.1">Il</w> <w n="14.2">s</w>’<w n="14.3">agit</w> <w n="14.4">bien</w> <w n="14.5">de</w> <w n="14.6">chanter</w> <w n="14.7">Ilion</w> !</l>
						<l n="15" num="2.5"><w n="15.1">Cours</w> <w n="15.2">de</w> <w n="15.3">ce</w> <w n="15.4">pas</w> <w n="15.5">chez</w> <w n="15.6">le</w> <w n="15.7">tabellion</w>.</l>
						<l n="16" num="2.6"><w n="16.1">Les</w> <w n="16.2">coteaux</w> <w n="16.3">verts</w> <w n="16.4">n</w>’<w n="16.5">ont</w> <w n="16.6">plus</w> <w n="16.7">d</w>’<w n="16.8">enchanteresse</w> ;</l>
						<l n="17" num="2.7"><w n="17.1">On</w> <w n="17.2">ne</w> <w n="17.3">va</w> <w n="17.4">plus</w> <w n="17.5">suivre</w> <w n="17.6">la</w> <w n="17.7">Chasseresse</w></l>
						<l n="18" num="2.8"><w n="18.1">Sur</w> <w n="18.2">l</w>’<w n="18.3">herbe</w> <w n="18.4">fraîche</w> <w n="18.5">où</w> <w n="18.6">court</w> <w n="18.7">son</w> <w n="18.8">lévrier</w>.</l>
						<l n="19" num="2.9"><w n="19.1">Si</w>, <w n="19.2">nous</w> <w n="19.3">irons</w>, <w n="19.4">ô</w> <w n="19.5">Lyre</w> <w n="19.6">vengeresse</w>.</l>
						<l n="20" num="2.10"><w n="20.1">Pourquoi</w> <w n="20.2">je</w> <w n="20.3">vis</w> ? <w n="20.4">Pour</w> <w n="20.5">l</w>’<w n="20.6">amour</w> <w n="20.7">du</w> <w n="20.8">laurier</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Et</w> <w n="21.2">Galatée</w> <w n="21.3">à</w> <w n="21.4">la</w> <w n="21.5">gorge</w> <w n="21.6">d</w>’<w n="21.7">ivoire</w></l>
						<l n="22" num="3.2"><w n="22.1">Chaque</w> <w n="22.2">matin</w> <w n="22.3">dit</w> <w n="22.4">à</w> <w n="22.5">Pygmalion</w> :</l>
						<l n="23" num="3.3"><w n="23.1">Oui</w>, <w n="23.2">j</w>’<w n="23.3">aimerai</w> <w n="23.4">ta</w> <w n="23.5">barbe</w> <w n="23.6">rude</w> <w n="23.7">et</w> <w n="23.8">noire</w>,</l>
						<l n="24" num="3.4"><w n="24.1">Mais</w> <w n="24.2">que</w> <w n="24.3">je</w> <w n="24.4">morde</w> <w n="24.5">à</w> <w n="24.6">même</w> <w n="24.7">un</w> <w n="24.8">galion</w> !</l>
						<l n="25" num="3.5"><w n="25.1">Il</w> <w n="25.2">est</w> <w n="25.3">venu</w>, <w n="25.4">l</w>’<w n="25.5">âge</w> <w n="25.6">du</w> <w n="25.7">talion</w> :</l>
						<l n="26" num="3.6"><w n="26.1">As</w>-<w n="26.2">tu</w> <w n="26.3">de</w> <w n="26.4">l</w>’<w n="26.5">or</w> ? <w n="26.6">voilà</w> <w n="26.7">de</w> <w n="26.8">la</w> <w n="26.9">tendresse</w>,</l>
						<l n="27" num="3.7"><w n="27.1">Et</w> <w n="27.2">tout</w> <w n="27.3">se</w> <w n="27.4">vend</w>, <w n="27.5">la</w> <w n="27.6">divine</w> <w n="27.7">caresse</w></l>
						<l n="28" num="3.8"><w n="28.1">Et</w> <w n="28.2">la</w> <w n="28.3">vertu</w> ; <w n="28.4">rien</w> <w n="28.5">ne</w> <w n="28.6">sert</w> <w n="28.7">de</w> <w n="28.8">prier</w> ;</l>
						<l n="29" num="3.9"><w n="29.1">Le</w> <w n="29.2">lait</w> <w n="29.3">qu</w>’<w n="29.4">on</w> <w n="29.5">suce</w> <w n="29.6">est</w> <w n="29.7">un</w> <w n="29.8">lait</w> <w n="29.9">de</w> <w n="29.10">tigresse</w>.</l>
						<l n="30" num="3.10"><w n="30.1">Pourquoi</w> <w n="30.2">je</w> <w n="30.3">vis</w> ? <w n="30.4">Pour</w> <w n="30.5">l</w>’<w n="30.6">amour</w> <w n="30.7">du</w> <w n="30.8">laurier</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="31" num="4.1"><w n="31.1">Siècle</w> <w n="31.2">de</w> <w n="31.3">fer</w>, <w n="31.4">crève</w> <w n="31.5">de</w> <w n="31.6">sécheresse</w> ;</l>
						<l n="32" num="4.2"><w n="32.1">Frappe</w> <w n="32.2">et</w> <w n="32.3">meurtris</w> <w n="32.4">l</w>’<w n="32.5">Ange</w> <w n="32.6">à</w> <w n="32.7">la</w> <w n="32.8">blonde</w> <w n="32.9">tresse</w>.</l>
						<l n="33" num="4.3"><w n="33.1">Moi</w>, <w n="33.2">je</w> <w n="33.3">me</w> <w n="33.4">sens</w> <w n="33.5">le</w> <w n="33.6">cœur</w> <w n="33.7">d</w>’<w n="33.8">un</w> <w n="33.9">ouvrier</w></l>
						<l n="34" num="4.4"><w n="34.1">Pareil</w> <w n="34.2">à</w> <w n="34.3">ceux</w> <w n="34.4">qui</w> <w n="34.5">florissaient</w> <w n="34.6">en</w> <w n="34.7">Grèce</w>.</l>
						<l n="35" num="4.5"><w n="35.1">Pourquoi</w> <w n="35.2">je</w> <w n="35.3">vis</w> ? <w n="35.4">Pour</w> <w n="35.5">l</w>’<w n="35.6">amour</w> <w n="35.7">du</w> <w n="35.8">laurier</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1861">Juillet 1861.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>