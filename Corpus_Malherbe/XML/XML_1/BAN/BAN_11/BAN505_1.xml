<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN505">
					<head type="number">XXXI</head>
					<head type="main">Ballade, à sa Mère, <lb></lb>Madame Élisabeth Zélie de Banville</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Toujours</w> <w n="1.2">charmé</w> <w n="1.3">par</w> <w n="1.4">la</w> <w n="1.5">douceur</w> <w n="1.6">des</w> <w n="1.7">vers</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ne</w> <w n="2.2">pense</w> <w n="2.3">pas</w> <w n="2.4">que</w> <w n="2.5">je</w> <w n="2.6">m</w>’<w n="2.7">en</w> <w n="2.8">rassasie</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Même</w> <w n="3.2">à</w> <w n="3.3">cette</w> <w n="3.4">heure</w>, <w n="3.5">en</w> <w n="3.6">dépit</w> <w n="3.7">des</w> <w n="3.8">hivers</w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">ai</w> <w n="4.3">sur</w> <w n="4.4">la</w> <w n="4.5">lèvre</w> <w n="4.6">un</w> <w n="4.7">parfum</w> <w n="4.8">d</w>’<w n="4.9">ambroisie</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Né</w> <w n="5.2">pour</w> <w n="5.3">le</w> <w n="5.4">rhythme</w> <w n="5.5">et</w> <w n="5.6">pour</w> <w n="5.7">la</w> <w n="5.8">poésie</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Dans</w> <w n="6.2">nos</w> <w n="6.3">pays</w>, <w n="6.4">où</w>, <w n="6.5">tenant</w> <w n="6.6">son</w> <w n="6.7">fuseau</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Le</w> <w n="7.2">long</w> <w n="7.3">des</w> <w n="7.4">prés</w> <w n="7.5">où</w> <w n="7.6">chante</w> <w n="7.7">un</w> <w n="7.8">gai</w> <w n="7.9">ruisseau</w></l>
						<l n="8" num="1.8"><w n="8.1">Va</w> <w n="8.2">la</w> <w n="8.3">bergère</w> <w n="8.4">au</w> <w n="8.5">gré</w> <w n="8.6">de</w> <w n="8.7">son</w> <w n="8.8">caprice</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Je</w> <w n="9.2">surprenais</w> <w n="9.3">les</w> <w n="9.4">soupirs</w> <w n="9.5">du</w> <w n="9.6">roseau</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Tu</w> <w n="10.2">le</w> <w n="10.3">sais</w>, <w n="10.4">toi</w>, <w n="10.5">ma</w> <w n="10.6">mère</w> <w n="10.7">et</w> <w n="10.8">ma</w> <w n="10.9">nourrice</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">Tout</w> <w n="11.2">a</w> <w n="11.3">son</w> <w n="11.4">prix</w> ; <w n="11.5">mais</w> <w n="11.6">hors</w> <w n="11.7">les</w> <w n="11.8">lauriers</w> <w n="11.9">verts</w>,</l>
						<l n="12" num="2.2"><w n="12.1">Je</w> <w n="12.2">puis</w> <w n="12.3">encor</w> <w n="12.4">tout</w> <w n="12.5">voir</w> <w n="12.6">sans</w> <w n="12.7">jalousie</w>,</l>
						<l n="13" num="2.3"><w n="13.1">Car</w> <w n="13.2">chanter</w> <w n="13.3">juste</w> <w n="13.4">en</w> <w n="13.5">des</w> <w n="13.6">mètres</w> <w n="13.7">divers</w></l>
						<l n="14" num="2.4"><w n="14.1">Serait</w> <w n="14.2">ma</w> <w n="14.3">loi</w>, <w n="14.4">si</w> <w n="14.5">je</w> <w n="14.6">l</w>’<w n="14.7">avais</w> <w n="14.8">choisie</w>.</l>
						<l n="15" num="2.5"><w n="15.1">Quand</w> <w n="15.2">m</w>’<w n="15.3">emporta</w> <w n="15.4">la</w> <w n="15.5">sainte</w> <w n="15.6">frénésie</w>,</l>
						<l n="16" num="2.6"><w n="16.1">Parfois</w>, <w n="16.2">montant</w> <w n="16.3">Pégase</w> <w n="16.4">au</w> <w n="16.5">fier</w> <w n="16.6">naseau</w>,</l>
						<l n="17" num="2.7"><w n="17.1">J</w>’<w n="17.2">ai</w> <w n="17.3">de</w> <w n="17.4">ma</w> <w n="17.5">chair</w> <w n="17.6">laissé</w> <w n="17.7">quelque</w> <w n="17.8">morceau</w></l>
						<l n="18" num="2.8"><w n="18.1">Parmi</w> <w n="18.2">les</w> <w n="18.3">rocs</w> ; <w n="18.4">plus</w> <w n="18.5">d</w>’<w n="18.6">une</w> <w n="18.7">cicatrice</w></l>
						<l n="19" num="2.9"><w n="19.1">Marquait</w> <w n="19.2">alors</w> <w n="19.3">mon</w> <w n="19.4">front</w> <w n="19.5">de</w> <w n="19.6">jouvenceau</w>,</l>
						<l n="20" num="2.10"><w n="20.1">Tu</w> <w n="20.2">le</w> <w n="20.3">sais</w>, <w n="20.4">toi</w>, <w n="20.5">ma</w> <w n="20.6">mère</w> <w n="20.7">et</w> <w n="20.8">ma</w> <w n="20.9">nourrice</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Et</w> <w n="21.2">je</w> <w n="21.3">me</w> <w n="21.4">crois</w> <w n="21.5">maître</w> <w n="21.6">de</w> <w n="21.7">l</w>’<w n="21.8">univers</w> !</l>
						<l n="22" num="3.2"><w n="22.1">Car</w> <w n="22.2">pour</w> <w n="22.3">orner</w> <w n="22.4">ma</w> <w n="22.5">riche</w> <w n="22.6">fantaisie</w>,</l>
						<l n="23" num="3.3"><w n="23.1">J</w>’<w n="23.2">ai</w> <w n="23.3">des</w> <w n="23.4">rubis</w> <w n="23.5">en</w> <w n="23.6">mes</w> <w n="23.7">coffres</w> <w n="23.8">ouverts</w>,</l>
						<l n="24" num="3.4"><w n="24.1">Tels</w> <w n="24.2">qu</w>’<w n="24.3">un</w> <w n="24.4">avare</w> <w n="24.5">ou</w> <w n="24.6">qu</w>’<w n="24.7">un</w> <w n="24.8">sultan</w> <w n="24.9">d</w>’<w n="24.10">Asie</w>.</l>
						<l n="25" num="3.5"><w n="25.1">Foin</w> <w n="25.2">de</w> <w n="25.3">l</w>’<w n="25.4">orgueil</w> <w n="25.5">et</w> <w n="25.6">de</w> <w n="25.7">l</w>’<w n="25.8">hypocrisie</w> !</l>
						<l n="26" num="3.6"><w n="26.1">Comme</w> <w n="26.2">un</w> <w n="26.3">orfèvre</w>, <w n="26.4">avec</w> <w n="26.5">le</w> <w n="26.6">dur</w> <w n="26.7">ciseau</w></l>
						<l n="27" num="3.7"><w n="27.1">Dont</w> <w n="27.2">mainte</w> <w n="27.3">lime</w> <w n="27.4">affûte</w> <w n="27.5">le</w> <w n="27.6">biseau</w>,</l>
						<l n="28" num="3.8"><w n="28.1">Je</w> <w n="28.2">dompte</w> <w n="28.3">l</w>’<w n="28.4">or</w> <w n="28.5">sous</w> <w n="28.6">ma</w> <w n="28.7">main</w> <w n="28.8">créatrice</w>,</l>
						<l n="29" num="3.9"><w n="29.1">Car</w> <w n="29.2">une</w> <w n="29.3">fée</w> <w n="29.4">enchanta</w> <w n="29.5">mon</w> <w n="29.6">berceau</w>,</l>
						<l n="30" num="3.10"><w n="30.1">Tu</w> <w n="30.2">le</w> <w n="30.3">sais</w>, <w n="30.4">toi</w>, <w n="30.5">ma</w> <w n="30.6">mère</w> <w n="30.7">et</w> <w n="30.8">ma</w> <w n="30.9">nourrice</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="31" num="4.1"><w n="31.1">Ma</w> <w n="31.2">mère</w>, <w n="31.3">ainsi</w> <w n="31.4">j</w>’<w n="31.5">aurai</w> <w n="31.6">fui</w> <w n="31.7">tout</w> <w n="31.8">réseau</w>,</l>
						<l n="32" num="4.2"><w n="32.1">N</w>’<w n="32.2">étant</w> <w n="32.3">valet</w>, <w n="32.4">seigneur</w> <w n="32.5">ni</w> <w n="32.6">damoiseau</w>.</l>
						<l n="33" num="4.3">(<w n="33.1">Que</w> <w n="33.2">de</w> <w n="33.3">ce</w> <w n="33.4">mal</w> <w n="33.5">jamais</w> <w n="33.6">je</w> <w n="33.7">ne</w> <w n="33.8">guérisse</w> !)</l>
						<l n="34" num="4.4"><w n="34.1">J</w>’<w n="34.2">aurai</w> <w n="34.3">vécu</w> <w n="34.4">libre</w> <w n="34.5">comme</w> <w n="34.6">un</w> <w n="34.7">oiseau</w>,</l>
						<l n="35" num="4.5"><w n="35.1">Tu</w> <w n="35.2">le</w> <w n="35.3">sais</w>, <w n="35.4">toi</w>, <w n="35.5">ma</w> <w n="35.6">mère</w> <w n="35.7">et</w> <w n="35.8">ma</w> <w n="35.9">nourrice</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1869"> 19 Novembre 1869.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>