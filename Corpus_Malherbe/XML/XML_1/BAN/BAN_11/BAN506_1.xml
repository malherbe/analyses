<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">TRENTE-SIX BALLADES JOYEUSES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1215 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_11</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/banville36ballades.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Trente-six Ballades joyeuses</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">http://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Genève</pubPlace>
							<publisher>Slatkine Reprints</publisher>
							<date when="1972">1972</date>
						</imprint>
						<biblScope unit="tome">VI</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN506">
					<head type="number">XXXII</head>
					<head type="main">Ballade à la louange des Roses</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">veux</w> <w n="1.3">encor</w> <w n="1.4">d</w>’<w n="1.5">un</w> <w n="1.6">vers</w> <w n="1.7">audacieux</w></l>
						<l n="2" num="1.2"><w n="2.1">Louer</w> <w n="2.2">la</w> <w n="2.3">fleur</w> <w n="2.4">adorable</w> <w n="2.5">et</w> <w n="2.6">sanglante</w></l>
						<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">dit</w> : <w n="3.3">Amour</w> ! <w n="3.4">sous</w> <w n="3.5">l</w>’<w n="3.6">œil</w> <w n="3.7">charmé</w> <w n="3.8">des</w> <w n="3.9">cieux</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">fleur</w> <w n="4.3">qui</w> <w n="4.4">semble</w> <w n="4.5">une</w> <w n="4.6">lèvre</w> <w n="4.7">vivante</w></l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">qui</w> <w n="5.3">nous</w> <w n="5.4">baise</w>, <w n="5.5">et</w> <w n="5.6">dont</w> <w n="5.7">la</w> <w n="5.8">couleur</w> <w n="5.9">chante</w></l>
						<l n="6" num="1.6"><w n="6.1">Dans</w> <w n="6.2">ses</w> <w n="6.3">rougeurs</w> <w n="6.4">un</w> <w n="6.5">bel</w> <w n="6.6">hymne</w> <w n="6.7">idéal</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Par</w> <w n="7.2">ce</w> <w n="7.3">matin</w> <w n="7.4">vermeil</w> <w n="7.5">de</w> <w n="7.6">Floréal</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Je</w> <w n="8.2">veux</w> <w n="8.3">chanter</w> <w n="8.4">le</w> <w n="8.5">calice</w> <w n="8.6">où</w> <w n="8.7">repose</w></l>
						<l n="9" num="1.9"><w n="9.1">L</w>’<w n="9.2">enivrement</w> <w n="9.3">du</w> <w n="9.4">parfum</w> <w n="9.5">nuptial</w>.</l>
						<l n="10" num="1.10"><w n="10.1">Sur</w> <w n="10.2">toutes</w> <w n="10.3">fleurs</w> <w n="10.4">je</w> <w n="10.5">veux</w> <w n="10.6">louer</w> <w n="10.7">la</w> <w n="10.8">Rose</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">La</w> <w n="11.2">Rose</w> <w n="11.3">ouvrait</w> <w n="11.4">son</w> <w n="11.5">cœur</w> <w n="11.6">délicieux</w>.</l>
						<l n="12" num="2.2"><w n="12.1">Dans</w> <w n="12.2">les</w> <w n="12.3">sentiers</w> <w n="12.4">où</w> <w n="12.5">verdissait</w> <w n="12.6">l</w>’<w n="12.7">acanthe</w></l>
						<l n="13" num="2.3"><w n="13.1">Tu</w> <w n="13.2">la</w> <w n="13.3">rougis</w> <w n="13.4">de</w> <w n="13.5">ton</w> <w n="13.6">sang</w> <w n="13.7">précieux</w>,</l>
						<l n="14" num="2.4"><w n="14.1">Reine</w> <w n="14.2">de</w> <w n="14.3">Cypre</w>, <w n="14.4">ô</w> <w n="14.5">Cypris</w> <w n="14.6">triomphante</w> !</l>
						<l n="15" num="2.5"><w n="15.1">La</w> <w n="15.2">violette</w> <w n="15.3">est</w> <w n="15.4">sa</w> <w n="15.5">pâle</w> <w n="15.6">servante</w>.</l>
						<l n="16" num="2.6"><w n="16.1">Le</w> <w n="16.2">chaste</w> <w n="16.3">lys</w> <w n="16.4">près</w> <w n="16.5">du</w> <w n="16.6">flot</w> <w n="16.7">de</w> <w n="16.8">cristal</w></l>
						<l n="17" num="2.7"><w n="17.1">Reste</w> <w n="17.2">épris</w> <w n="17.3">d</w>’<w n="17.4">elle</w>, <w n="17.5">et</w> <w n="17.6">n</w>’<w n="17.7">est</w> <w n="17.8">que</w> <w n="17.9">le</w> <w n="17.10">vassal</w></l>
						<l n="18" num="2.8"><w n="18.1">De</w> <w n="18.2">sa</w> <w n="18.3">splendeur</w> <w n="18.4">suave</w> <w n="18.5">et</w> <w n="18.6">grandiose</w>,</l>
						<l n="19" num="2.9"><w n="19.1">Et</w> <w n="19.2">l</w>’<w n="19.3">astre</w> <w n="19.4">seul</w> <w n="19.5">croit</w> <w n="19.6">qu</w>’<w n="19.7">il</w> <w n="19.8">est</w> <w n="19.9">son</w> <w n="19.10">égal</w>.</l>
						<l n="20" num="2.10"><w n="20.1">Sur</w> <w n="20.2">toutes</w> <w n="20.3">fleurs</w> <w n="20.4">je</w> <w n="20.5">veux</w> <w n="20.6">louer</w> <w n="20.7">la</w> <w n="20.8">Rose</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Sans</w> <w n="21.2">dérider</w> <w n="21.3">le</w> <w n="21.4">Roi</w> <w n="21.5">silencieux</w>,</l>
						<l n="22" num="3.2"><w n="22.1">Vivant</w> <w n="22.2">rubis</w>, <w n="22.3">une</w> <w n="22.4">Rose</w> <w n="22.5">galante</w></l>
						<l n="23" num="3.3"><w n="23.1">Égaye</w>, <w n="23.2">au</w> <w n="23.3">sein</w> <w n="23.4">du</w> <w n="23.5">palais</w> <w n="23.6">soucieux</w>,</l>
						<l n="24" num="3.4"><w n="24.1">Les</w> <w n="24.2">cheveux</w> <w n="24.3">blonds</w> <w n="24.4">de</w> <w n="24.5">la</w> <w n="24.6">petite</w> <w n="24.7">Infante</w>.</l>
						<l n="25" num="3.5"><w n="25.1">Et</w> <w n="25.2">cependant</w>, <w n="25.3">sans</w> <w n="25.4">voir</w> <w n="25.5">son</w> <w n="25.6">épouvante</w>,</l>
						<l n="26" num="3.6"><w n="26.1">Pareil</w> <w n="26.2">lui</w>-<w n="26.3">même</w> <w n="26.4">au</w> <w n="26.5">sombre</w> <w n="26.6">Escurial</w>,</l>
						<l n="27" num="3.7"><w n="27.1">Son</w> <w n="27.2">père</w> <w n="27.3">au</w> <w n="27.4">front</w> <w n="27.5">livide</w> <w n="27.6">et</w> <w n="27.7">glacial</w></l>
						<l n="28" num="3.8"><w n="28.1">Se</w> <w n="28.2">tient</w> <w n="28.3">auprès</w> <w n="28.4">d</w>’<w n="28.5">une</w> <w n="28.6">fenêtre</w> <w n="28.7">close</w>,</l>
						<l n="29" num="3.9"><w n="29.1">Pâle</w> <w n="29.2">à</w> <w n="29.3">jamais</w> <w n="29.4">de</w> <w n="29.5">son</w> <w n="29.6">ennui</w> <w n="29.7">royal</w>.</l>
						<l n="30" num="3.10"><w n="30.1">Sur</w> <w n="30.2">toutes</w> <w n="30.3">fleurs</w> <w n="30.4">je</w> <w n="30.5">veux</w> <w n="30.6">louer</w> <w n="30.7">la</w> <w n="30.8">Rose</w>.</l>
					</lg>
					<lg n="4">
						<head type="form">Envoi</head>
						<l n="31" num="4.1"><w n="31.1">Prince</w>, <w n="31.2">un</w> <w n="31.3">divin</w> <w n="31.4">poëte</w> <w n="31.5">oriental</w></l>
						<l n="32" num="4.2"><w n="32.1">Chanta</w> <w n="32.2">jadis</w> <w n="32.3">pour</w> <w n="32.4">son</w> <w n="32.5">pays</w> <w n="32.6">natal</w></l>
						<l n="33" num="4.3"><w n="33.1">Ma</w> <w n="33.2">fleur</w> <w n="33.3">de</w> <w n="33.4">pourpre</w> <w n="33.5">et</w> <w n="33.6">son</w> <w n="33.7">apothéose</w>.</l>
						<l n="34" num="4.4"><w n="34.1">Tel</w>, <w n="34.2">après</w> <w n="34.3">lui</w>, <w n="34.4">dans</w> <w n="34.5">un</w> <w n="34.6">chant</w> <w n="34.7">triomphal</w>,</l>
						<l n="35" num="4.5"><w n="35.1">Sur</w> <w n="35.2">toutes</w> <w n="35.3">fleurs</w> <w n="35.4">je</w> <w n="35.5">veux</w> <w n="35.6">louer</w> <w n="35.7">la</w> <w n="35.8">Rose</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1869">Mai 1869.</date>
						</dateline>
					</closer>
				</div></body></text></TEI>