<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ROSES DE NOËL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>600 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_15</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ROSES DE NOËL</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1878">1878</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN630">
				<head type="main">Les Jardins</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Mère</w>, <w n="1.2">qu</w>’<w n="1.3">il</w> <w n="1.4">soit</w> <w n="1.5">béni</w>, <w n="1.6">le</w> <w n="1.7">grand</w> <w n="1.8">jardin</w> <w n="1.9">de</w> <w n="1.10">fleurs</w></l>
					<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">vit</w>, <w n="2.3">petite</w> <w n="2.4">enfant</w>, <w n="2.5">ton</w> <w n="2.6">sourire</w> <w n="2.7">et</w> <w n="2.8">tes</w> <w n="2.9">pleurs</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Là</w>, <w n="3.2">ta</w> <w n="3.3">mère</w> <w n="3.4">aux</w> <w n="3.5">beaux</w> <w n="3.6">yeux</w>, <w n="3.7">jeune</w> <w n="3.8">et</w> <w n="3.9">pleine</w> <w n="3.10">de</w> <w n="3.11">grâce</w></l>
					<l n="4" num="1.4"><w n="4.1">Te</w> <w n="4.2">chantait</w> <w n="4.3">des</w> <w n="4.4">chansons</w> <w n="4.5">de</w> <w n="4.6">nourrice</w> <w n="4.7">à</w> <w n="4.8">voix</w> <w n="4.9">basse</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Ton</w> <w n="5.2">père</w>, <w n="5.3">sérieux</w>, <w n="5.4">te</w> <w n="5.5">prenait</w> <w n="5.6">dans</w> <w n="5.7">ses</w> <w n="5.8">bras</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">t</w>’<w n="6.3">écoutant</w>, <w n="6.4">ravi</w>, <w n="6.5">dès</w> <w n="6.6">que</w> <w n="6.7">tu</w> <w n="6.8">murmuras</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Disait</w> : <w n="7.2">O</w> <w n="7.3">frêle</w> <w n="7.4">enfant</w> ! <w n="7.5">il</w> <w n="7.6">faut</w> <w n="7.7">veiller</w> <w n="7.8">sur</w> <w n="7.9">elle</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">c</w>’<w n="8.3">était</w> <w n="8.4">entre</w> <w n="8.5">eux</w> <w n="8.6">deux</w> <w n="8.7">une</w> <w n="8.8">folle</w> <w n="8.9">querelle</w></l>
					<l n="9" num="1.9"><w n="9.1">De</w> <w n="9.2">lutter</w> <w n="9.3">pour</w> <w n="9.4">donner</w> <w n="9.5">une</w> <w n="9.6">joie</w> <w n="9.7">à</w> <w n="9.8">tes</w> <w n="9.9">yeux</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">de</w> <w n="10.3">savoir</w> <w n="10.4">lequel</w> <w n="10.5">t</w>’<w n="10.6">obéirait</w> <w n="10.7">le</w> <w n="10.8">mieux</w>.</l>
					<l n="11" num="1.11"><space quantity="2" unit="char"></space><w n="11.1">O</w> <w n="11.2">Dieu</w> ! <w n="11.3">le</w> <w n="11.4">temps</w> <w n="11.5">s</w>’<w n="11.6">envole</w> <w n="11.7">ainsi</w> <w n="11.8">que</w> <w n="11.9">des</w> <w n="11.10">fumées</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Emportant</w> <w n="12.2">loin</w> <w n="12.3">de</w> <w n="12.4">nous</w> <w n="12.5">les</w> <w n="12.6">âmes</w> <w n="12.7">bien</w> <w n="12.8">aimées</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Nos</w> <w n="13.2">rêves</w>, <w n="13.3">nos</w> <w n="13.4">désirs</w>, <w n="13.5">tout</w> <w n="13.6">ce</w> <w n="13.7">qui</w> <w n="13.8">nous</w> <w n="13.9">fut</w> <w n="13.10">cher</w>.</l>
					<l n="14" num="1.14"><w n="14.1">Le</w> <w n="14.2">froid</w> <w n="14.3">du</w> <w n="14.4">soir</w> <w n="14.5">qui</w> <w n="14.6">tombe</w> <w n="14.7">entre</w> <w n="14.8">dans</w> <w n="14.9">notre</w> <w n="14.10">chair</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">cependant</w> <w n="15.3">toujours</w> <w n="15.4">les</w> <w n="15.5">voix</w> <w n="15.6">qui</w> <w n="15.7">nous</w> <w n="15.8">émurent</w></l>
					<l n="16" num="1.16"><w n="16.1">Comme</w> <w n="16.2">en</w> <w n="16.3">un</w> <w n="16.4">vague</w> <w n="16.5">songe</w> <w n="16.6">autour</w> <w n="16.7">de</w> <w n="16.8">nous</w> <w n="16.9">murmurent</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">Elles</w> <w n="17.2">ont</w> <w n="17.3">la</w> <w n="17.4">douceur</w> <w n="17.5">sereine</w> <w n="17.6">de</w> <w n="17.7">l</w>’<w n="17.8">espoir</w></l>
					<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">nous</w> <w n="18.3">les</w> <w n="18.4">entendons</w> <w n="18.5">qui</w> <w n="18.6">disent</w> : <w n="18.7">Au</w> <w n="18.8">revoir</w> !</l>
					<l n="19" num="1.19"><w n="19.1">Nos</w> <w n="19.2">Anges</w>, <w n="19.3">dans</w> <w n="19.4">cette</w> <w n="19.5">ombre</w> <w n="19.6">où</w> <w n="19.7">notre</w> <w n="19.8">pas</w> <w n="19.9">vacille</w></l>
					<l n="20" num="1.20"><w n="20.1">Nous</w> <w n="20.2">regardent</w> <w n="20.3">souffrir</w> <w n="20.4">d</w>’<w n="20.5">un</w> <w n="20.6">œil</w> <w n="20.7">doux</w> <w n="20.8">et</w> <w n="20.9">tranquille</w></l>
					<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">tandis</w> <w n="21.3">que</w> <w n="21.4">leur</w> <w n="21.5">vol</w> <w n="21.6">mystérieux</w> <w n="21.7">nous</w> <w n="21.8">suit</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Au</w>-<w n="22.2">dessus</w> <w n="22.3">de</w> <w n="22.4">nos</w> <w n="22.5">fronts</w> <w n="22.6">envahis</w> <w n="22.7">par</w> <w n="22.8">la</w> <w n="22.9">nuit</w></l>
					<l n="23" num="1.23"><w n="23.1">Nous</w> <w n="23.2">voyons</w> <w n="23.3">l</w>’<w n="23.4">avenir</w> <w n="23.5">sortir</w> <w n="23.6">d</w>’<w n="23.7">un</w> <w n="23.8">sombre</w> <w n="23.9">voile</w></l>
					<l n="24" num="1.24"><w n="24.1">Sous</w> <w n="24.2">la</w> <w n="24.3">nue</w>, <w n="24.4">et</w> <w n="24.5">grandir</w> <w n="24.6">comme</w> <w n="24.7">une</w> <w n="24.8">blanche</w> <w n="24.9">étoile</w>.</l>
					<l n="25" num="1.25"><w n="25.1">Oh</w> ! <w n="25.2">sois</w> <w n="25.3">heureuse</w> ! <w n="25.4">et</w> <w n="25.5">quand</w> <w n="25.6">frémit</w> <w n="25.7">l</w>’<w n="25.8">aile</w> <w n="25.9">du</w> <w n="25.10">soir</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Songe</w> <w n="26.2">aux</w> <w n="26.3">chers</w> <w n="26.4">cœurs</w> <w n="26.5">avec</w> <w n="26.6">le</w> <w n="26.7">plus</w> <w n="26.8">tranquille</w> <w n="26.9">espoir</w>,</l>
					<l n="27" num="1.27"><w n="27.1">Car</w> <w n="27.2">un</w> <w n="27.3">pressentiment</w> <w n="27.4">céleste</w> <w n="27.5">nous</w> <w n="27.6">enivre</w></l>
					<l n="28" num="1.28"><w n="28.1">Dans</w> <w n="28.2">cette</w> <w n="28.3">solitude</w> <w n="28.4">où</w> <w n="28.5">nous</w> <w n="28.6">les</w> <w n="28.7">sentons</w> <w n="28.8">vivre</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1874"> 16 février 1874.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>