<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ROSES DE NOËL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>600 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_15</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ROSES DE NOËL</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1878">1878</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN628">
				<head type="main">Pourquoi seuls ?</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Eh</w> <w n="1.2">bien</w> ! <w n="1.3">mère</w>, <w n="1.4">prenons</w> <w n="1.5">les</w> <w n="1.6">souvenirs</w> <w n="1.7">si</w> <w n="1.8">doux</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">temps</w> <w n="2.3">où</w> <w n="2.4">tes</w> <w n="2.5">enfants</w> <w n="2.6">jouaient</w> <w n="2.7">sur</w> <w n="2.8">tes</w> <w n="2.9">genoux</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ta</w> <w n="3.2">mère</w> <w n="3.3">qui</w> <w n="3.4">savait</w> <w n="3.5">encor</w> <w n="3.6">comme</w> <w n="3.7">on</w> <w n="3.8">espère</w>,</l>
					<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">grandeur</w>, <w n="4.3">la</w> <w n="4.4">bonté</w> <w n="4.5">charmante</w> <w n="4.6">de</w> <w n="4.7">ton</w> <w n="4.8">père</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">le</w> <w n="5.3">mien</w> <w n="5.4">tout</w> <w n="5.5">amour</w>, <w n="5.6">comme</w> <w n="5.7">je</w> <w n="5.8">le</w> <w n="5.9">revois</w>,</l>
					<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">Font</w>-<w n="6.3">Georges</w> <w n="6.4">vermeille</w> <w n="6.5">où</w> <w n="6.6">se</w> <w n="6.7">mêlaient</w> <w n="6.8">nos</w> <w n="6.9">voix</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">ma</w> <w n="7.3">petite</w> <w n="7.4">sœur</w> <w n="7.5">qui</w> <w n="7.6">passait</w> <w n="7.7">dans</w> <w n="7.8">les</w> <w n="7.9">herbes</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Avec</w> <w n="8.2">sa</w> <w n="8.3">bouche</w> <w n="8.4">rose</w> <w n="8.5">et</w> <w n="8.6">ses</w> <w n="8.7">grands</w> <w n="8.8">yeux</w> <w n="8.9">superbes</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">ses</w> <w n="9.3">cheveux</w> <w n="9.4">si</w> <w n="9.5">fins</w> <w n="9.6">dans</w> <w n="9.7">la</w> <w n="9.8">brise</w> <w n="9.9">envolés</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Ce</w> <w n="10.2">triomphe</w> <w n="10.3">éclatant</w> <w n="10.4">des</w> <w n="10.5">bleuets</w> <w n="10.6">dans</w> <w n="10.7">les</w> <w n="10.8">blés</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">tes</w> <w n="11.3">enfants</w> <w n="11.4">jaseurs</w> <w n="11.5">qui</w>, <w n="11.6">lassés</w> <w n="11.7">de</w> <w n="11.8">leur</w> <w n="11.9">course</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Tous</w> <w n="12.2">deux</w> <w n="12.3">s</w>’<w n="12.4">agenouillaient</w> <w n="12.5">et</w> <w n="12.6">buvaient</w> <w n="12.7">à</w> <w n="12.8">la</w> <w n="12.9">source</w> !</l>
					<l n="13" num="1.13"><w n="13.1">O</w> <w n="13.2">mère</w> ! <w n="13.3">plongeons</w>-<w n="13.4">nous</w> <w n="13.5">dans</w> <w n="13.6">ce</w> <w n="13.7">flot</w> ! <w n="13.8">Revoyons</w></l>
					<l n="14" num="1.14"><w n="14.1">Les</w> <w n="14.2">peupliers</w>, <w n="14.3">les</w> <w n="14.4">eaux</w> <w n="14.5">tremblantes</w>, <w n="14.6">les</w> <w n="14.7">rayons</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Vos</w> <w n="15.2">projets</w> <w n="15.3">merveilleux</w>, <w n="15.4">tout</w> <w n="15.5">ce</w> <w n="15.6">temps</w> <w n="15.7">où</w> <w n="15.8">la</w> <w n="15.9">vie</w></l>
					<l n="16" num="1.16"><w n="16.1">De</w> <w n="16.2">pourpre</w> <w n="16.3">et</w> <w n="16.4">d</w>’<w n="16.5">or</w>, <w n="16.6">était</w> <w n="16.7">comme</w> <w n="16.8">une</w> <w n="16.9">aube</w> <w n="16.10">ravie</w></l>
					<l n="17" num="1.17"><w n="17.1">Jetant</w> <w n="17.2">ses</w> <w n="17.3">feux</w> <w n="17.4">rosés</w> <w n="17.5">dans</w> <w n="17.6">l</w>’<w n="17.7">azur</w> <w n="17.8">empli</w> <w n="17.9">d</w>’<w n="17.10">yeux</w> ;</l>
					<l n="18" num="1.18"><w n="18.1">Prenons</w> <w n="18.2">ces</w> <w n="18.3">souvenirs</w>, <w n="18.4">ce</w> <w n="18.5">passé</w> <w n="18.6">radieux</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Qui</w> <w n="19.2">devant</w> <w n="19.3">nous</w> <w n="19.4">comme</w> <w n="19.5">un</w> <w n="19.6">riant</w> <w n="19.7">matin</w> <w n="19.8">flamboie</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">renouvelons</w>-<w n="20.3">nous</w> <w n="20.4">dans</w> <w n="20.5">ce</w> <w n="20.6">trésor</w> <w n="20.7">de</w> <w n="20.8">joie</w> !</l>
					<l n="21" num="1.21"><space quantity="2" unit="char"></space><w n="21.1">Même</w> <w n="21.2">quand</w> <w n="21.3">le</w> <w n="21.4">printemps</w> <w n="21.5">neige</w> <w n="21.6">sur</w> <w n="21.7">les</w> <w n="21.8">tilleuls</w></l>
					<l n="22" num="1.22"><w n="22.1">Et</w> <w n="22.2">resplendit</w>, <w n="22.3">pourquoi</w> <w n="22.4">nous</w> <w n="22.5">sentirions</w>-<w n="22.6">nous</w> <w n="22.7">seuls</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Puisque</w>, <w n="23.2">gardant</w> <w n="23.3">toujours</w> <w n="23.4">aux</w> <w n="23.5">nôtres</w> <w n="23.6">nos</w> <w n="23.7">tendresses</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Nos</w> <w n="24.2">baisers</w>, <w n="24.3">notre</w> <w n="24.4">amour</w>, <w n="24.5">nos</w> <w n="24.6">meilleures</w> <w n="24.7">caresses</w>,</l>
					<l n="25" num="1.25"><w n="25.1">Nous</w> <w n="25.2">n</w>’<w n="25.3">avons</w> <w n="25.4">pas</w> <w n="25.5">des</w> <w n="25.6">cœurs</w> <w n="25.7">lâches</w> <w n="25.8">ni</w> <w n="25.9">paresseux</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Et</w> <w n="26.2">puisque</w>, <w n="26.3">pleins</w> <w n="26.4">encor</w> <w n="26.5">du</w> <w n="26.6">cher</w> <w n="26.7">esprit</w> <w n="26.8">de</w> <w n="26.9">ceux</w></l>
					<l n="27" num="1.27"><w n="27.1">Qui</w> <w n="27.2">revivent</w> <w n="27.3">baignés</w> <w n="27.4">par</w> <w n="27.5">les</w> <w n="27.6">clartés</w> <w n="27.7">divines</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Nous</w> <w n="28.2">les</w> <w n="28.3">sentons</w> <w n="28.4">vivants</w> <w n="28.5">encor</w> <w n="28.6">dans</w> <w n="28.7">nos</w> <w n="28.8">poitrines</w> ?</l>
				</lg>
				<closer>
					<dateline>
						<date when="1872"> 19 novembre 1872.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>