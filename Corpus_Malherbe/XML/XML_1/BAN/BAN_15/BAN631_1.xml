<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ROSES DE NOËL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>600 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_15</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">ROSES DE NOËL</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1999">1999</date>
								</imprint>
								<biblScope unit="tome">VI</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1878">1878</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN631">
				<head type="main">Nous voilà tous</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Mère</w>, <w n="1.2">nous</w> <w n="1.3">voilà</w> <w n="1.4">tous</w>, <w n="1.5">moi</w> <w n="1.6">ton</w> <w n="1.7">fils</w>, <w n="1.8">qui</w> <w n="1.9">te</w> <w n="1.10">fête</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">celle</w> <w n="2.3">qui</w> <w n="2.4">pour</w> <w n="2.5">moi</w> <w n="2.6">Dieu</w> <w n="2.7">lui</w>-<w n="2.8">même</w> <w n="2.9">avait</w> <w n="2.10">faite</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">l</w>’<w n="3.3">enfant</w> <w n="3.4">adoré</w> <w n="3.5">qui</w> <w n="3.6">porte</w> <w n="3.7">dans</w> <w n="3.8">ses</w> <w n="3.9">yeux</w></l>
					<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">monde</w> <w n="4.3">qui</w> <w n="4.4">s</w>’<w n="4.5">agite</w>, <w n="4.6">encor</w> <w n="4.7">mystérieux</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">toi</w>, <w n="5.3">tu</w> <w n="5.4">nous</w> <w n="5.5">bénis</w>, <w n="5.6">o</w> <w n="5.7">ma</w> <w n="5.8">chère</w> <w n="5.9">nourrice</w> !</l>
					<l n="6" num="1.6"><w n="6.1">O</w> <w n="6.2">mère</w>, <w n="6.3">que</w> <w n="6.4">toujours</w> <w n="6.5">l</w>’<w n="6.6">espoir</w> <w n="6.7">en</w> <w n="6.8">toi</w> <w n="6.9">fleurisse</w> !</l>
					<l n="7" num="1.7"><w n="7.1">Nous</w> <w n="7.2">ne</w> <w n="7.3">sommes</w> <w n="7.4">pas</w> <w n="7.5">seuls</w> <w n="7.6">à</w> <w n="7.7">baiser</w> <w n="7.8">doucement</w></l>
					<l n="8" num="1.8"><w n="8.1">Ta</w> <w n="8.2">tête</w> <w n="8.3">calme</w> <w n="8.4">où</w> <w n="8.5">luit</w> <w n="8.6">comme</w> <w n="8.7">un</w> <w n="8.8">éclair</w> <w n="8.9">charmant</w>.</l>
					<l n="9" num="1.9"><space quantity="2" unit="char"></space><w n="9.1">Car</w> <w n="9.2">lorsque</w> <w n="9.3">dans</w> <w n="9.4">le</w> <w n="9.5">ciel</w> <w n="9.6">grandit</w> <w n="9.7">l</w>’<w n="9.8">aube</w> <w n="9.9">vermeille</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Le</w> <w n="10.2">murmure</w> <w n="10.3">étouffé</w> <w n="10.4">de</w> <w n="10.5">tout</w> <w n="10.6">ce</w> <w n="10.7">qui</w> <w n="10.8">s</w>’<w n="10.9">éveille</w></l>
					<l n="11" num="1.11"><w n="11.1">Court</w> <w n="11.2">sur</w> <w n="11.3">les</w> <w n="11.4">arbres</w> <w n="11.5">nus</w> <w n="11.6">et</w> <w n="11.7">sur</w> <w n="11.8">les</w> <w n="11.9">claires</w> <w n="11.10">eaux</w>.</l>
					<l n="12" num="1.12"><w n="12.1">L</w>’<w n="12.2">air</w> <w n="12.3">est</w> <w n="12.4">plein</w> <w n="12.5">du</w> <w n="12.6">frisson</w> <w n="12.7">des</w> <w n="12.8">ailes</w> <w n="12.9">des</w> <w n="12.10">oiseaux</w></l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">des</w> <w n="13.3">âmes</w> <w n="13.4">des</w> <w n="13.5">morts</w> <w n="13.6">et</w> <w n="13.7">du</w> <w n="13.8">souffle</w> <w n="13.9">des</w> <w n="13.10">Anges</w> ;</l>
					<l n="14" num="1.14"><w n="14.1">Celui</w> <w n="14.2">vers</w> <w n="14.3">qui</w> <w n="14.4">toujours</w> <w n="14.5">monte</w> <w n="14.6">un</w> <w n="14.7">flot</w> <w n="14.8">de</w> <w n="14.9">louanges</w></l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">qui</w> <w n="15.3">de</w> <w n="15.4">nos</w> <w n="15.5">douleurs</w> <w n="15.6">a</w> <w n="15.7">fait</w> <w n="15.8">des</w> <w n="15.9">voluptés</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Nous</w> <w n="16.2">dit</w> <w n="16.3">alors</w> <w n="16.4">tout</w> <w n="16.5">bas</w> : <w n="16.6">Voici</w> <w n="16.7">l</w>’<w n="16.8">heure</w>. <w n="16.9">Écoutez</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">plus</w> <w n="17.3">faibles</w> <w n="17.4">qu</w>’<w n="17.5">un</w> <w n="17.6">vol</w> <w n="17.7">d</w>’<w n="17.8">abeilles</w> <w n="17.9">sur</w> <w n="17.10">les</w> <w n="17.11">mousses</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Nous</w> <w n="18.2">entendons</w> <w n="18.3">les</w> <w n="18.4">voix</w> <w n="18.5">qui</w> <w n="18.6">nous</w> <w n="18.7">semblaient</w> <w n="18.8">si</w> <w n="18.9">douces</w></l>
					<l n="19" num="1.19"><w n="19.1">Jadis</w> ; <w n="19.2">car</w> <w n="19.3">rien</w> <w n="19.4">ne</w> <w n="19.5">meurt</w>, <w n="19.6">la</w> <w n="19.7">tombe</w> <w n="19.8">n</w>’<w n="19.9">a</w> <w n="19.10">rien</w> <w n="19.11">pris</w></l>
					<l n="20" num="1.20"><w n="20.1">De</w> <w n="20.2">la</w> <w n="20.3">clarté</w> <w n="20.4">sereine</w> <w n="20.5">et</w> <w n="20.6">pure</w> <w n="20.7">des</w> <w n="20.8">esprits</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">Dieu</w>, <w n="21.3">qui</w> <w n="21.4">les</w> <w n="21.5">créa</w> <w n="21.6">dans</w> <w n="21.7">leur</w> <w n="21.8">splendeur</w> <w n="21.9">première</w>,</l>
					<l n="22" num="1.22"><w n="22.1">N</w>’<w n="22.2">a</w> <w n="22.3">pas</w> <w n="22.4">fait</w> <w n="22.5">du</w> <w n="22.6">néant</w> <w n="22.7">avec</w> <w n="22.8">de</w> <w n="22.9">la</w> <w n="22.10">lumière</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1875"> 19 Novembre 1875.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>