<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN558">
				<head type="main">Embellissements</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Si</w> <w n="1.2">vous</w> <w n="1.3">le</w> <w n="1.4">pouvez</w>, <w n="1.5">d</w>’<w n="1.6">un</w> <w n="1.7">œil</w> <w n="1.8">sec</w></l>
					<l n="2" num="1.2"><w n="2.1">Regardez</w> <w n="2.2">cela</w>. <w n="2.3">C</w>’<w n="2.4">est</w> <w n="2.5">la</w> <w n="2.6">rue</w></l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">la</w> <w n="3.3">Paix</w>. <w n="3.4">Dieux</w> <w n="3.5">puissants</w> ! <w n="3.6">avec</w></l>
					<l n="4" num="1.4"><w n="4.1">Quelle</w> <w n="4.2">fureur</w> <w n="4.3">le</w> <w n="4.4">pic</w> <w n="4.5">s</w>’<w n="4.6">y</w> <w n="4.7">rue</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Dégringolez</w>, <w n="5.2">façades</w>, <w n="5.3">coins</w> !</l>
					<l n="6" num="2.2"><w n="6.1">En</w> <w n="6.2">avant</w> <w n="6.3">la</w> <w n="6.4">pelle</w> <w n="6.5">et</w> <w n="6.6">la</w> <w n="6.7">pioche</w> !</l>
					<l n="7" num="2.3"><w n="7.1">O</w> <w n="7.2">rue</w> <w n="7.3">historique</w>, <w n="7.4">rejoins</w></l>
					<l n="8" num="2.4"><w n="8.1">Celles</w> <w n="8.2">de</w> <w n="8.3">Tyr</w> <w n="8.4">et</w> <w n="8.5">d</w>’<w n="8.6">Antioche</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">spectacle</w> <w n="9.3">est</w> <w n="9.4">superbe</w>, <w n="9.5">car</w></l>
					<l n="10" num="3.2"><w n="10.1">Des</w> <w n="10.2">hordes</w>, <w n="10.3">comme</w> <w n="10.4">en</w> <w n="10.5">rêve</w> <w n="10.6">entrées</w></l>
					<l n="11" num="3.3"><w n="11.1">Dans</w> <w n="11.2">ces</w> <w n="11.3">maisons</w>, <w n="11.4">en</w> <w n="11.5">sortent</w> <w n="11.6">par</w></l>
					<l n="12" num="3.4"><w n="12.1">Les</w> <w n="12.2">trous</w> <w n="12.3">des</w> <w n="12.4">chambres</w> <w n="12.5">éventrées</w> ;</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Tous</w> <w n="13.2">ces</w> <w n="13.3">palais</w> <w n="13.4">sur</w> <w n="13.5">leurs</w> <w n="13.6">genoux</w></l>
					<l n="14" num="4.2"><w n="14.1">Laissent</w> <w n="14.2">ruisseler</w> <w n="14.3">leurs</w> <w n="14.4">entrailles</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">On</w> <w n="15.2">voit</w>, <w n="15.3">comme</w> <w n="15.4">des</w> <w n="15.5">aigles</w> <w n="15.6">fous</w>,</l>
					<l n="16" num="4.4"><w n="16.1">S</w>’<w n="16.2">envoler</w> <w n="16.3">des</w> <w n="16.4">pans</w> <w n="16.5">de</w> <w n="16.6">murailles</w> ;</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">les</w> <w n="17.3">plâtras</w> <w n="17.4">et</w> <w n="17.5">les</w> <w n="17.6">gravats</w>,</l>
					<l n="18" num="5.2"><w n="18.1">O</w> <w n="18.2">dieu</w> <w n="18.3">de</w> <w n="18.4">notre</w> <w n="18.5">préfecture</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Couvrent</w> <w n="19.2">la</w> <w n="19.3">ville</w> <w n="19.4">où</w> <w n="19.5">tu</w> <w n="19.6">gravas</w></l>
					<l n="20" num="5.4"><w n="20.1">Ton</w> <w n="20.2">nom</w> <w n="20.3">pour</w> <w n="20.4">la</w> <w n="20.5">race</w> <w n="20.6">future</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Blanc</w> <w n="21.2">comme</w> <w n="21.3">Avril</w> <w n="21.4">en</w> <w n="21.5">floraison</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Le</w> <w n="22.2">passant</w> <w n="22.3">gémit</w>, <w n="22.4">pleure</w> <w n="22.5">et</w> <w n="22.6">beugle</w>.</l>
					<l n="23" num="6.3"><w n="23.1">Désormais</w> <w n="23.2">on</w> <w n="23.3">a</w> <w n="23.4">bien</w> <w n="23.5">raison</w></l>
					<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">dire</w> <w n="24.3">que</w> <w n="24.4">l</w>’<w n="24.5">homme</w> <w n="24.6">est</w> <w n="24.7">aveugle</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Car</w>, <w n="25.2">ainsi</w> <w n="25.3">masqué</w> <w n="25.4">jusqu</w>’<w n="25.5">aux</w> <w n="25.6">dents</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Le</w> <w n="26.2">Français</w>, <w n="26.3">qui</w> <w n="26.4">devient</w> <w n="26.5">farouche</w>,</l>
					<l n="27" num="7.3"><w n="27.1">A</w> <w n="27.2">du</w> <w n="27.3">plâtre</w> <w n="27.4">dans</w> <w n="27.5">les</w> <w n="27.6">yeux</w>, <w n="27.7">dans</w></l>
					<l n="28" num="7.4"><w n="28.1">Les</w> <w n="28.2">narines</w> <w n="28.3">et</w> <w n="28.4">dans</w> <w n="28.5">la</w> <w n="28.6">bouche</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">O</w> <w n="29.2">Parisien</w>, <w n="29.3">ta</w> <w n="29.4">cité</w></l>
					<l n="30" num="8.2"><w n="30.1">A</w> <w n="30.2">présent</w> <w n="30.3">n</w>’<w n="30.4">a</w> <w n="30.5">plus</w> <w n="30.6">de</w> <w n="30.7">rivales</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Mais</w>, <w n="31.2">selon</w> <w n="31.3">ta</w> <w n="31.4">capacité</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Ce</w> <w n="32.2">plâtre</w>, <w n="32.3">il</w> <w n="32.4">faut</w> <w n="32.5">que</w> <w n="32.6">tu</w> <w n="32.7">l</w>’<w n="32.8">avales</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">voici</w>, <w n="33.3">dans</w> <w n="33.4">tout</w> <w n="33.5">ce</w> <w n="33.6">mic</w>-<w n="33.7">mac</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Le</w> <w n="34.2">plus</w> <w n="34.3">clair</w> <w n="34.4">de</w> <w n="34.5">tes</w> <w n="34.6">héritages</w> :</l>
					<l n="35" num="9.3"><w n="35.1">Tu</w> <w n="35.2">dois</w> <w n="35.3">avoir</w> <w n="35.4">dans</w> <w n="35.5">l</w>’<w n="35.6">estomac</w></l>
					<l n="36" num="9.4"><w n="36.1">Quelques</w> <w n="36.2">maisons</w> <w n="36.3">à</w> <w n="36.4">cinq</w> <w n="36.5">étages</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Hurrah</w> ! <w n="37.2">Le</w> <w n="37.3">fauve</w> <w n="37.4">Sahara</w></l>
					<l n="38" num="10.2"><w n="38.1">Croît</w> <w n="38.2">et</w> <w n="38.3">grandit</w>, <w n="38.4">où</w> <w n="38.5">fut</w> <w n="38.6">la</w> <w n="38.7">rue</w></l>
					<l n="39" num="10.3"><w n="39.1">De</w> <w n="39.2">la</w> <w n="39.3">Paix</w> ; <w n="39.4">bientôt</w> <w n="39.5">l</w>’<w n="39.6">on</w> <w n="39.7">aura</w></l>
					<l n="40" num="10.4"><w n="40.1">Coupé</w> <w n="40.2">cette</w> <w n="40.3">immense</w> <w n="40.4">verrue</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Bon</w> <w n="41.2">Paris</w>, <w n="41.3">patiente</w> <w n="41.4">encor</w> :</l>
					<l n="42" num="11.2"><w n="42.1">Bientôt</w>, <w n="42.2">pourvu</w> <w n="42.3">qu</w>’<w n="42.4">on</w> <w n="42.5">démolisse</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Tu</w> <w n="43.2">deviendras</w> <w n="43.3">le</w> <w n="43.4">sable</w> <w n="43.5">d</w>’<w n="43.6">or</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Le</w> <w n="44.2">désert</w> <w n="44.3">parfaitement</w> <w n="44.4">lisse</w>,</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">O</w> <w n="45.2">ville</w>, — <w n="45.3">et</w>, <w n="45.4">prudents</w> <w n="45.5">animaux</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Au</w> <w n="46.2">lieu</w> <w n="46.3">même</w> <w n="46.4">où</w> <w n="46.5">tu</w> <w n="46.6">te</w> <w n="46.7">pavanes</w></l>
					<l n="47" num="12.3"><w n="47.1">Les</w> <w n="47.2">doux</w> <w n="47.3">et</w> <w n="47.4">patients</w> <w n="47.5">chameaux</w></l>
					<l n="48" num="12.4"><w n="48.1">Iront</w> <w n="48.2">en</w> <w n="48.3">longues</w> <w n="48.4">caravanes</w> !</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Paix</w> <w n="49.2">divine</w> ! <w n="49.3">ce</w> <w n="49.4">n</w>’<w n="49.5">est</w> <w n="49.6">plus</w> <w n="49.7">qu</w>’<w n="49.8">aux</w></l>
					<l n="50" num="13.2"><w n="50.1">Antipodes</w> <w n="50.2">que</w> <w n="50.3">l</w>’<w n="50.4">on</w> <w n="50.5">te</w> <w n="50.6">souffre</w> ;</l>
					<l n="51" num="13.3"><w n="51.1">L</w>’<w n="51.2">Europe</w> <w n="51.3">est</w> <w n="51.4">ivre</w> <w n="51.5">de</w> <w n="51.6">shakos</w>,</l>
					<l n="52" num="13.4"><w n="52.1">De</w> <w n="52.2">canons</w> <w n="52.3">rayés</w> <w n="52.4">et</w> <w n="52.5">de</w> <w n="52.6">soufre</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Tu</w> <w n="53.2">souris</w>, <w n="53.3">efforts</w> <w n="53.4">superflus</w> !</l>
					<l n="54" num="14.2"><w n="54.1">Ta</w> <w n="54.2">détresse</w>, <w n="54.3">hélas</w> ! <w n="54.4">s</w>’<w n="54.5">est</w> <w n="54.6">accrue</w>.</l>
					<l n="55" num="14.3"><w n="55.1">Chez</w> <w n="55.2">nous</w> <w n="55.3">il</w> <w n="55.4">ne</w> <w n="55.5">te</w> <w n="55.6">restait</w> <w n="55.7">plus</w></l>
					<l n="56" num="14.4"><w n="56.1">Rien</w>, <w n="56.2">Déesse</w>, <w n="56.3">qu</w>’<w n="56.4">un</w> <w n="56.5">nom</w> <w n="56.6">de</w> <w n="56.7">rue</w> ;</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">On</w> <w n="57.2">te</w> <w n="57.3">le</w> <w n="57.4">reprend</w> ! <w n="57.5">Il</w> <w n="57.6">est</w> <w n="57.7">sûr</w></l>
					<l n="58" num="15.2"><w n="58.1">Qu</w>’<w n="58.2">un</w> <w n="58.3">édile</w> <w n="58.4">sévère</w> <w n="58.5">et</w> <w n="58.6">tendre</w></l>
					<l n="59" num="15.3"><w n="59.1">Ne</w> <w n="59.2">peut</w> <w n="59.3">pas</w> <w n="59.4">laisser</w> <w n="59.5">ton</w> <w n="59.6">nom</w> <w n="59.7">sur</w></l>
					<l n="60" num="15.4"><w n="60.1">Des</w> <w n="60.2">démolitions</w> <w n="60.3">à</w> <w n="60.4">vendre</w> !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Ouvrière</w>, <w n="61.2">qui</w> <w n="61.3">n</w>’<w n="61.4">as</w> <w n="61.5">souci</w></l>
					<l n="62" num="16.2"><w n="62.1">Que</w> <w n="62.2">d</w>’<w n="62.3">une</w> <w n="62.4">œuvre</w> <w n="62.5">amoureuse</w> <w n="62.6">et</w> <w n="62.7">lente</w>,</l>
					<l n="63" num="16.3"><w n="63.1">Le</w> <w n="63.2">préfet</w> <w n="63.3">te</w> <w n="63.4">chassa</w> <w n="63.5">d</w>’<w n="63.6">ici</w></l>
					<l n="64" num="16.4"><w n="64.1">Comme</w> <w n="64.2">une</w> <w n="64.3">marchande</w> <w n="64.4">ambulante</w> ;</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Ce</w> <w n="65.2">maître</w> <w n="65.3">a</w> <w n="65.4">brisé</w> <w n="65.5">ton</w> <w n="65.6">collier</w></l>
					<l n="66" num="17.2"><w n="66.1">Et</w> <w n="66.2">l</w>’<w n="66.3">a</w> <w n="66.4">jeté</w> <w n="66.5">dans</w> <w n="66.6">le</w> <w n="66.7">cloaque</w>,</l>
					<l n="67" num="17.3"><w n="67.1">Et</w>, <w n="67.2">pour</w> <w n="67.3">te</w> <w n="67.4">mieux</w> <w n="67.5">humilier</w>,</l>
					<l n="68" num="17.4"><w n="68.1">T</w>’<w n="68.2">a</w> <w n="68.3">même</w> <w n="68.4">retiré</w> <w n="68.5">ta</w> <w n="68.6">plaque</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Juillet 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>