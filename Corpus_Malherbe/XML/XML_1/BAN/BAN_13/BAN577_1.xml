<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN577">
				<head type="main">Chez Bignon</head>
				<head type="form">Églogue</head>
				<head type="sub_2">Rose, Rosette, Palémon</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Prends</w> <w n="1.2">ta</w> <w n="1.3">flûte</w> <w n="1.4">légère</w>, <w n="1.5">ô</w> <w n="1.6">muse</w> <w n="1.7">de</w> <w n="1.8">Sicile</w> !</l>
					<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">voyait</w> <w n="2.3">là</w> <w n="2.4">Finette</w>, <w n="2.5">Héloïse</w>, <w n="2.6">Lucile</w> :</l>
					<l n="3" num="1.3"><w n="3.1">Nous</w> <w n="3.2">soupions</w> <w n="3.3">au</w> <w n="3.4">sortir</w> <w n="3.5">du</w> <w n="3.6">bal</w>. <w n="3.7">Quelques</w> <w n="3.8">gandins</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Portant</w> <w n="4.2">des</w> <w n="4.3">favoris</w> <w n="4.4">découpés</w> <w n="4.5">en</w> <w n="4.6">jardins</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Faisaient</w> <w n="5.2">assaut</w> <w n="5.3">d</w>’<w n="5.4">esprit</w> <w n="5.5">avec</w> <w n="5.6">des</w> <w n="5.7">femmes</w> <w n="5.8">rousses</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Deux</w> <w n="6.2">dominos</w> <w n="6.3">pourtant</w>, <w n="6.4">dont</w> <w n="6.5">les</w> <w n="6.6">allures</w> <w n="6.7">douces</w></l>
					<l n="7" num="1.7"><w n="7.1">Nous</w> <w n="7.2">ravirent</w>, <w n="7.3">causaient</w> <w n="7.4">poésie</w> <w n="7.5">à</w> <w n="7.6">l</w>’<w n="7.7">écart</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">rien</w> <w n="8.3">qu</w>’<w n="8.4">en</w> <w n="8.5">transcrivant</w>, <w n="8.6">à</w> <w n="8.7">sept</w> <w n="8.8">heures</w> <w n="8.9">et</w> <w n="8.10">quart</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Leurs</w> <w n="9.2">propos</w> <w n="9.3">familiers</w> <w n="9.4">d</w>’<w n="9.5">hétaïres</w> <w n="9.6">en</w> <w n="9.7">vogue</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Un</w> <w n="10.2">poëte</w> <w n="10.3">essaya</w> <w n="10.4">cette</w> <w n="10.5">ébauche</w> <w n="10.6">d</w>’<w n="10.7">églogue</w>.</l>
				</lg>
				<lg n="2">
					<head type="main">Rose</head>
					<l n="11" num="2.1"><w n="11.1">Oui</w>, <w n="11.2">tu</w> <w n="11.3">dis</w> <w n="11.4">bien</w>, <w n="11.5">oui</w>, <w n="11.6">Scholl</w> <w n="11.7">est</w> <w n="11.8">vraiment</w> <w n="11.9">l</w>’<w n="11.10">Amadis</w></l>
					<l n="12" num="2.2"><w n="12.1">De</w> <w n="12.2">la</w> <w n="12.3">littérature</w> <w n="12.4">aimable</w>, — <w n="12.5">mais</w>, <w n="12.6">tandis</w></l>
					<l n="13" num="2.3"><w n="13.1">Que</w>, <w n="13.2">perdant</w> <w n="13.3">sa</w> <w n="13.4">chaleur</w> <w n="13.5">aux</w> <w n="13.6">soleils</w> <w n="13.7">d</w>’<w n="13.8">or</w> <w n="13.9">volée</w>,</l>
					<l n="14" num="2.4"><w n="14.1">Ce</w> <w n="14.2">Cliquot</w> <w n="14.3">rafraîchit</w> <w n="14.4">dans</w> <w n="14.5">la</w> <w n="14.6">glace</w> <w n="14.7">pilée</w></l>
					<l n="15" num="2.5"><w n="15.1">Qu</w>’<w n="15.2">à</w> <w n="15.3">ses</w> <w n="15.4">pieds</w> <w n="15.5">le</w> <w n="15.6">garçon</w> <w n="15.7">naguère</w> <w n="15.8">amoncelait</w>,</l>
					<l n="16" num="2.6"><w n="16.1">Rosette</w>, <w n="16.2">mon</w> <w n="16.3">cher</w> <w n="16.4">cœur</w>, <w n="16.5">parlons</w> <w n="16.6">de</w> <w n="16.7">Monselet</w>.</l>
				</lg>
				<lg n="3">
					<head type="main">Rosette</head>
					<l n="17" num="3.1"><w n="17.1">Monselet</w> <w n="17.2">est</w> <w n="17.3">joli</w>. <w n="17.4">Comme</w> <w n="17.5">une</w> <w n="17.6">vague</w> <w n="17.7">aurore</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Son</w> <w n="18.2">visage</w> <w n="18.3">est</w> <w n="18.4">vermeil</w> <w n="18.5">et</w> <w n="18.6">de</w> <w n="18.7">fleurs</w> <w n="18.8">se</w> <w n="18.9">décore</w>.</l>
					<l n="19" num="3.3"><w n="19.1">Je</w> <w n="19.2">vois</w> <w n="19.3">sa</w> <w n="19.4">lèvre</w> <w n="19.5">en</w> <w n="19.6">feu</w> <w n="19.7">dans</w> <w n="19.8">le</w> <w n="19.9">vin</w> <w n="19.10">que</w> <w n="19.11">je</w> <w n="19.12">bois</w>.</l>
					<l n="20" num="3.4"><w n="20.1">Quand</w> <w n="20.2">il</w> <w n="20.3">était</w> <w n="20.4">petit</w>, <w n="20.5">les</w> <w n="20.6">roses</w> <w n="20.7">dans</w> <w n="20.8">le</w> <w n="20.9">bois</w></l>
					<l n="21" num="3.5"><w n="21.1">Cachaient</w>, <w n="21.2">en</w> <w n="21.3">le</w> <w n="21.4">voyant</w>, <w n="21.5">leur</w> <w n="21.6">aiguillon</w> <w n="21.7">farouche</w>,</l>
					<l n="22" num="3.6"><w n="22.1">Et</w> <w n="22.2">les</w> <w n="22.3">abeilles</w> <w n="22.4">d</w>’<w n="22.5">or</w> <w n="22.6">voltigeaient</w> <w n="22.7">sur</w> <w n="22.8">sa</w> <w n="22.9">bouche</w>.</l>
				</lg>
				<lg n="4">
					<head type="main">Rose</head>
					<l n="23" num="4.1"><w n="23.1">Et</w> <w n="23.2">quel</w> <w n="23.3">esprit</w> <w n="23.4">charmant</w> ! <w n="23.5">Comme</w> <w n="23.6">il</w> <w n="23.7">frappe</w> <w n="23.8">d</w>’<w n="23.9">estoc</w></l>
					<l n="24" num="4.2"><w n="24.1">Et</w> <w n="24.2">de</w> <w n="24.3">taille</w> ! <w n="24.4">Et</w> <w n="24.5">pour</w> <w n="24.6">la</w> <w n="24.7">gaieté</w>, <w n="24.8">c</w>’<w n="24.9">est</w> <w n="24.10">Paul</w> <w n="24.11">de</w> <w n="24.12">Kock</w>.</l>
				</lg>
				<lg n="5">
					<head type="main">Rosette</head>
					<l n="25" num="5.1"><w n="25.1">Paul</w> <w n="25.2">de</w> <w n="25.3">Kock</w>, <w n="25.4">en</w> <w n="25.5">effet</w>, <w n="25.6">mais</w> <w n="25.7">avec</w> <w n="25.8">plus</w> <w n="25.9">de</w> <w n="25.10">style</w>.</l>
					<l n="26" num="5.2"><w n="26.1">On</w> <w n="26.2">entre</w> <w n="26.3">à</w> <w n="26.4">son</w> <w n="26.5">caveau</w> <w n="26.6">par</w> <w n="26.7">un</w> <w n="26.8">blanc</w> <w n="26.9">péristyle</w>.</l>
				</lg>
				<lg n="6">
					<head type="main">Rose</head>
					<l n="27" num="6.1"><w n="27.1">Wateau</w>, <w n="27.2">peintre</w> <w n="27.3">du</w> <w n="27.4">beau</w>, <w n="27.5">que</w> <w n="27.6">son</w> <w n="27.7">temps</w> <w n="27.8">violait</w>,</l>
					<l n="28" num="6.2"><w n="28.1">Eût</w> <w n="28.2">fait</w> <w n="28.3">de</w> <w n="28.4">lui</w> <w n="28.5">sans</w> <w n="28.6">doute</w> <w n="28.7">un</w> <w n="28.8">abbé</w> <w n="28.9">violet</w></l>
					<l n="29" num="6.3"><w n="29.1">Épris</w> <w n="29.2">de</w> <w n="29.3">Colombine</w>, <w n="29.4">et</w> <w n="29.5">dans</w> <w n="29.6">la</w> <w n="29.7">nuit</w> <w n="29.8">avare</w></l>
					<l n="30" num="6.4"><w n="30.1">Éveillant</w> <w n="30.2">doucement</w> <w n="30.3">l</w>’<w n="30.4">âme</w> <w n="30.5">d</w>’<w n="30.6">une</w> <w n="30.7">guitare</w>.</l>
				</lg>
				<lg n="7">
					<head type="main">Rosette</head>
					<l n="31" num="7.1"><w n="31.1">Les</w> <w n="31.2">Grâces</w> <w n="31.3">le</w> <w n="31.4">font</w> <w n="31.5">vivre</w> <w n="31.6">et</w> <w n="31.7">l</w>’<w n="31.8">ont</w> <w n="31.9">accrédité</w>.</l>
					<l n="32" num="7.2"><w n="32.1">Dans</w> <w n="32.2">sa</w> <w n="32.3">prose</w> <w n="32.4">on</w> <w n="32.5">les</w> <w n="32.6">voit</w>, <w n="32.7">cachant</w> <w n="32.8">leur</w> <w n="32.9">nudité</w></l>
					<l n="33" num="7.3"><w n="33.1">Et</w> <w n="33.2">leurs</w> <w n="33.3">bras</w> <w n="33.4">blancs</w> <w n="33.5">pareils</w> <w n="33.6">à</w> <w n="33.7">des</w> <w n="33.8">anses</w> <w n="33.9">d</w>’<w n="33.10">amphores</w>,</l>
					<l n="34" num="7.4"><w n="34.1">Sous</w> <w n="34.2">des</w> <w n="34.3">bouquets</w> <w n="34.4">riants</w> <w n="34.5">de</w> <w n="34.6">fraîches</w> <w n="34.7">métaphores</w> !</l>
				</lg>
				<lg n="8">
					<head type="main">Rose</head>
					<l n="35" num="8.1"><w n="35.1">Rire</w>, <w n="35.2">charmer</w>, <w n="35.3">pleurer</w> <w n="35.4">parfois</w>, <w n="35.5">c</w>’<w n="35.6">est</w> <w n="35.7">son</w> <w n="35.8">destin</w>.</l>
				</lg>
				<lg n="9">
					<head type="main">Rosette</head>
					<l n="36" num="9.1"><w n="36.1">Qu</w>’<w n="36.2">il</w> <w n="36.3">est</w> <w n="36.4">ingénieux</w> <w n="36.5">et</w> <w n="36.6">fou</w> <w n="36.7">dans</w> <w n="36.8">un</w> <w n="36.9">festin</w> !</l>
				</lg>
				<lg n="10">
					<head type="main">Rose</head>
					<l n="37" num="10.1"><w n="37.1">Rosette</w>, <w n="37.2">il</w> <w n="37.3">faut</w> <w n="37.4">le</w> <w n="37.5">voir</w> <w n="37.6">quand</w>, <w n="37.7">faisant</w> <w n="37.8">leur</w> <w n="37.9">entrée</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Les</w> <w n="38.2">truffes</w> <w n="38.3">ont</w> <w n="38.4">couvert</w> <w n="38.5">la</w> <w n="38.6">volaille</w> <w n="38.7">éventrée</w>.</l>
				</lg>
				<lg n="11">
					<head type="main">Rosette</head>
					<l n="39" num="11.1"><w n="39.1">Et</w> <w n="39.2">quand</w> <w n="39.3">le</w> <w n="39.4">Romanée</w> <w n="39.5">a</w> <w n="39.6">mis</w> <w n="39.7">sur</w> <w n="39.8">le</w> <w n="39.9">mur</w> <w n="39.10">blanc</w></l>
					<l n="40" num="11.2"><w n="40.1">Son</w> <w n="40.2">reflet</w> <w n="40.3">écarlate</w> <w n="40.4">et</w> <w n="40.5">sa</w> <w n="40.6">lueur</w> <w n="40.7">de</w> <w n="40.8">sang</w> !</l>
				</lg>
				<lg n="12">
					<head type="main">Rose</head>
					<l n="41" num="12.1"><w n="41.1">Il</w> <w n="41.2">n</w>’<w n="41.3">est</w> <w n="41.4">pas</w> <w n="41.5">de</w> <w n="41.6">printemps</w>, <w n="41.7">mon</w> <w n="41.8">cœur</w>, <w n="41.9">sans</w> <w n="41.10">violette</w> ;</l>
					<l n="42" num="12.2"><w n="42.1">Sans</w> <w n="42.2">les</w> <w n="42.3">clairs</w> <w n="42.4">diamants</w>, <w n="42.5">il</w> <w n="42.6">n</w>’<w n="42.7">est</w> <w n="42.8">pas</w> <w n="42.9">de</w> <w n="42.10">toilette</w>,</l>
					<l n="43" num="12.3"><w n="43.1">Comme</w> <w n="43.2">sans</w> <w n="43.3">Monselet</w>, <w n="43.4">chanteur</w> <w n="43.5">aérien</w>,</l>
					<l n="44" num="12.4"><w n="44.1">Un</w> <w n="44.2">dîner</w>, <w n="44.3">même</w> <w n="44.4">chaud</w>, <w n="44.5">ne</w> <w n="44.6">valut</w> <w n="44.7">jamais</w> <w n="44.8">rien</w>.</l>
				</lg>
				<lg n="13">
					<head type="main">Rosette</head>
					<l n="45" num="13.1"><w n="45.1">Il</w> <w n="45.2">a</w> <w n="45.3">fait</w> <w n="45.4">des</w> <w n="45.5">romans</w> <w n="45.6">que</w> <w n="45.7">s</w>’<w n="45.8">arrachaient</w> <w n="45.9">les</w> <w n="45.10">dames</w>,</l>
					<l n="46" num="13.2"><w n="46.1">Et</w> <w n="46.2">dont</w> <w n="46.3">la</w> <w n="46.4">verte</w> <w n="46.5">allure</w> <w n="46.6">enchanta</w> <w n="46.7">les</w> <w n="46.8">vidames</w> !</l>
					<l n="47" num="13.3"><w n="47.1">Alors</w> <w n="47.2">la</w> <w n="47.3">châtelaine</w>, <w n="47.4">errante</w> <w n="47.5">au</w> <w n="47.6">fond</w> <w n="47.7">du</w> <w n="47.8">val</w>,</l>
					<l n="48" num="13.4"><w n="48.1">L</w>’<w n="48.2">emportait</w> <w n="48.3">sous</w> <w n="48.4">son</w> <w n="48.5">châle</w>, <w n="48.6">ainsi</w> <w n="48.7">que</w> <w n="48.8">Paul</w> <w n="48.9">Féval</w>.</l>
				</lg>
				<lg n="14">
					<head type="main">Rose</head>
					<l n="49" num="14.1"><w n="49.1">Mais</w> <w n="49.2">à</w> <w n="49.3">présent</w> <w n="49.4">il</w> <w n="49.5">est</w> <w n="49.6">cygne</w> <w n="49.7">parmi</w> <w n="49.8">les</w> <w n="49.9">cygnes</w>.</l>
				</lg>
				<lg n="15">
					<head type="main">Rosette</head>
					<l n="50" num="15.1"><w n="50.1">A</w> <w n="50.2">présent</w> <w n="50.3">il</w> <w n="50.4">sait</w> <w n="50.5">faire</w> <w n="50.6">un</w> <w n="50.7">chef</w>-<w n="50.8">d</w>’<w n="50.9">œuvre</w> <w n="50.10">en</w> <w n="50.11">cent</w> <w n="50.12">lignes</w>.</l>
				</lg>
				<lg n="16">
					<head type="main">Rose</head>
					<l n="51" num="16.1"><w n="51.1">Que</w> <w n="51.2">j</w>’<w n="51.3">en</w> <w n="51.4">ai</w> <w n="51.5">vu</w> <w n="51.6">mourir</w>, <w n="51.7">non</w> <w n="51.8">pas</w> <w n="51.9">mille</w>, <w n="51.10">mais</w> <w n="51.11">cent</w></l>
					<l n="52" num="16.2"><w n="52.1">Mille</w>, <w n="52.2">mais</w> <w n="52.3">deux</w> <w n="52.4">cent</w> <w n="52.5">mille</w>, <w n="52.6">avec</w> <w n="52.7">Villemessant</w>,</l>
					<l n="53" num="16.3"><w n="53.1">De</w> <w n="53.2">ces</w> <w n="53.3">ténors</w> ! <w n="53.4">Mais</w>, <w n="53.5">seul</w>, <w n="53.6">Monselet</w> <w n="53.7">a</w> <w n="53.8">l</w>’<w n="53.9">ut</w> <w n="53.10">dièze</w>.</l>
				</lg>
				<lg n="17">
					<head type="main">Rosette</head>
					<l n="54" num="17.1"><w n="54.1">Quand</w> <w n="54.2">il</w> <w n="54.3">écrit</w>, <w n="54.4">l</w>’<w n="54.5">Europe</w> <w n="54.6">entière</w> <w n="54.7">en</w> <w n="54.8">est</w> <w n="54.9">bien</w> <w n="54.10">aise</w>,</l>
					<l n="55" num="17.2"><w n="55.1">Et</w>, <w n="55.2">comme</w> <w n="55.3">s</w>’<w n="55.4">ils</w> <w n="55.5">tombaient</w> <w n="55.6">de</w> <w n="55.7">l</w>’<w n="55.8">outre</w> <w n="55.9">de</w> <w n="55.10">Sancho</w>,</l>
					<l n="56" num="17.3"><w n="56.1">Les</w> <w n="56.2">vins</w> <w n="56.3">les</w> <w n="56.4">plus</w> <w n="56.5">pompeux</w> <w n="56.6">coulent</w> <w n="56.7">chez</w> <w n="56.8">Dinochau</w>.</l>
				</lg>
				<lg n="18">
					<head type="main">Rose</head>
					<l n="57" num="18.1"><w n="57.1">Parfois</w> <w n="57.2">Le</w> <w n="57.3">Figaro</w> <w n="57.4">plane</w> <w n="57.5">moins</w> <w n="57.6">que</w> <w n="57.7">Pindare</w></l>
					<l n="58" num="18.2"><w n="58.1">Sur</w> <w n="58.2">l</w>’<w n="58.3">éther</w>, <w n="58.4">mais</w> <w n="58.5">on</w> <w n="58.6">croit</w> <w n="58.7">écouter</w> <w n="58.8">la</w> <w n="58.9">fanfare</w></l>
					<l n="59" num="18.3"><w n="59.1">De</w> <w n="59.2">l</w>’<w n="59.3">alouette</w>, <w n="59.4">unie</w> <w n="59.5">au</w> <w n="59.6">chant</w> <w n="59.7">de</w> <w n="59.8">do</w>¤<w n="59.9">a</w> <w n="59.10">Sol</w>,</l>
					<l n="60" num="18.4"><w n="60.1">Les</w> <w n="60.2">jours</w> <w n="60.3">où</w> <w n="60.4">Monselet</w> <w n="60.5">s</w>’<w n="60.6">y</w> <w n="60.7">rencontre</w> <w n="60.8">avec</w> <w n="60.9">Scholl</w> !</l>
				</lg>
				<lg n="19">
					<head type="main">Rosette</head>
					<l n="61" num="19.1"><w n="61.1">Figaro</w>, — <w n="61.2">trop</w> <w n="61.3">souvent</w> <w n="61.4">écrit</w> <w n="61.5">pour</w> <w n="61.6">les</w> <w n="61.7">dentistes</w>,</l>
					<l n="62" num="19.2"><w n="62.1">Est</w> <w n="62.2">charmant</w> <w n="62.3">quand</w> <w n="62.4">il</w> <w n="62.5">a</w> <w n="62.6">ces</w> <w n="62.7">deux</w> <w n="62.8">instrumentistes</w>.</l>
				</lg>
				<lg n="20">
					<head type="main">Rose</head>
					<l n="63" num="20.1"><w n="63.1">Alors</w> <w n="63.2">c</w>’<w n="63.3">est</w> <w n="63.4">un</w> <w n="63.5">oiseau</w> <w n="63.6">qui</w> <w n="63.7">mêle</w> <w n="63.8">sur</w> <w n="63.9">son</w> <w n="63.10">flanc</w></l>
					<l part="I" n="64" num="20.2"><w n="64.1">L</w>’<w n="64.2">émeraude</w> <w n="64.3">et</w> <w n="64.4">l</w>’<w n="64.5">azur</w>. </l>
				</lg>
				<lg n="21">
					<head type="main">Rosette</head>
					<l part="F" n="64">— <w n="64.6">C</w>’<w n="64.7">est</w> <w n="64.8">le</w> <w n="64.9">rose</w> <w n="64.10">et</w> <w n="64.11">le</w> <w n="64.12">blanc</w></l>
					<l n="65" num="21.1"><w n="65.1">Unissant</w> <w n="65.2">leurs</w> <w n="65.3">splendeurs</w> <w n="65.4">pour</w> <w n="65.5">une</w> <w n="65.6">apothéose</w>.</l>
				</lg>
				<lg n="22">
					<head type="main">Rose</head>
					<l part="I" n="66" num="22.1"><w n="66.1">Scholl</w> <w n="66.2">aime</w> <w n="66.3">mieux</w> <w n="66.4">le</w> <w n="66.5">blanc</w>. </l>
				</lg>
				<lg n="23">
					<head type="main">Rosette</head>
					<l part="F" n="66">— <w n="66.6">Et</w> <w n="66.7">Monselet</w> <w n="66.8">le</w> <w n="66.9">rose</w>.</l>
				</lg>
				<lg n="24">
					<head type="main">Rose</head>
					<l n="67" num="24.1"><w n="67.1">Qui</w> <w n="67.2">sait</w> <w n="67.3">parler</w> <w n="67.4">ses</w> <w n="67.5">vers</w> <w n="67.6">comme</w> <w n="67.7">toi</w>, <w n="67.8">Monselet</w> ?</l>
				</lg>
				<lg n="25">
					<head type="main">Rosette</head>
					<l n="68" num="25.1"><w n="68.1">Qui</w> <w n="68.2">mieux</w> <w n="68.3">que</w> <w n="68.4">lui</w>, <w n="68.5">ma</w> <w n="68.6">sœur</w>, <w n="68.7">chante</w> <w n="68.8">un</w> <w n="68.9">petit</w> <w n="68.10">couplet</w> ?</l>
				</lg>
				<lg n="26">
					<head type="main">Rose</head>
					<l n="69" num="26.1"><w n="69.1">Jamais</w>, <w n="69.2">lorsqu</w>’<w n="69.3">il</w> <w n="69.4">le</w> <w n="69.5">dit</w>, <w n="69.6">un</w> <w n="69.7">mot</w> <w n="69.8">léger</w> <w n="69.9">n</w>’<w n="69.10">offusque</w>,</l>
					<l n="70" num="26.2"><w n="70.1">Et</w> <w n="70.2">j</w>’<w n="70.3">aime</w> <w n="70.4">éperdument</w> <w n="70.5">son</w> <w n="70.6">Espion</w> <w n="70.7">Étrusque</w>.</l>
				</lg>
				<lg n="27">
					<head type="main">Rosette</head>
					<l n="71" num="27.1"><w n="71.1">Il</w> <w n="71.2">le</w> <w n="71.3">conte</w> <w n="71.4">si</w> <w n="71.5">bien</w> <w n="71.6">qu</w>’<w n="71.7">on</w> <w n="71.8">voit</w> <w n="71.9">le</w> <w n="71.10">champion</w></l>
					<l n="72" num="27.2"><w n="72.1">S</w>’<w n="72.2">escrimer</w> <w n="72.3">dans</w> <w n="72.4">la</w> <w n="72.5">nuit</w> <w n="72.6">contre</w> <w n="72.7">cet</w> — <w n="72.8">espion</w>.</l>
				</lg>
				<lg n="28">
					<head type="main">Rose</head>
					<l n="73" num="28.1"><w n="73.1">J</w>’<w n="73.2">aime</w> <w n="73.3">son</w> <w n="73.4">feuilleton</w>. <w n="73.5">Comme</w> <w n="73.6">il</w> <w n="73.7">voit</w> <w n="73.8">bien</w> <w n="73.9">les</w> <w n="73.10">pièces</w> !</l>
				</lg>
				<lg n="29">
					<head type="main">Rosette</head>
					<l n="74" num="29.1"><w n="74.1">Les</w> <w n="74.2">contes</w> <w n="74.3">qu</w>’<w n="74.4">il</w> <w n="74.5">en</w> <w n="74.6">fait</w> <w n="74.7">enchantent</w> <w n="74.8">mes</w> <w n="74.9">deux</w> <w n="74.10">nièces</w>.</l>
				</lg>
				<lg n="30">
					<head type="main">Rose</head>
					<l n="75" num="30.1"><w n="75.1">Ses</w> <w n="75.2">caprices</w> <w n="75.3">railleurs</w> <w n="75.4">valent</w> <w n="75.5">ceux</w> <w n="75.6">de</w> <w n="75.7">Goya</w>.</l>
				</lg>
				<lg n="31">
					<head type="main">Rosette</head>
					<l n="76" num="31.1"><w n="76.1">Même</w> <w n="76.2">Buloz</w> <w n="76.3">un</w> <w n="76.4">jour</w> <w n="76.5">grâce</w> <w n="76.6">à</w> <w n="76.7">lui</w> <w n="76.8">s</w>’<w n="76.9">égaya</w> !</l>
				</lg>
				<lg n="32">
					<head type="main">Rose</head>
					<l n="77" num="32.1"><w n="77.1">Monsieur</w> <w n="77.2">de</w> <w n="77.3">Cupidon</w>, <w n="77.4">roué</w> <w n="77.5">qui</w> <w n="77.6">nous</w> <w n="77.7">défie</w>,</l>
					<l n="78" num="32.2"><w n="78.1">C</w>’<w n="78.2">était</w> <w n="78.3">là</w> <w n="78.4">de</w> <w n="78.5">la</w> <w n="78.6">bonne</w> <w n="78.7">autobiographie</w> ;</l>
					<l n="79" num="32.3"><w n="79.1">C</w>’<w n="79.2">est</w> <w n="79.3">l</w>’<w n="79.4">auteur</w> <w n="79.5">qui</w>, <w n="79.6">jetant</w> <w n="79.7">sa</w> <w n="79.8">tunique</w> <w n="79.9">de</w> <w n="79.10">lin</w>,</l>
					<l n="80" num="32.4"><w n="80.1">Exécute</w> <w n="80.2">ce</w> <w n="80.3">rôle</w> <w n="80.4">en</w> <w n="80.5">habit</w> <w n="80.6">zinzolin</w> !</l>
				</lg>
				<lg n="33">
					<head type="main">Rosette</head>
					<l n="81" num="33.1"><w n="81.1">Lorsque</w> <w n="81.2">l</w>’<w n="81.3">Amour</w>, <w n="81.4">perçant</w> <w n="81.5">les</w> <w n="81.6">cœurs</w> <w n="81.7">par</w> <w n="81.8">ribambelles</w>,</l>
					<l n="82" num="33.2"><w n="82.1">Bat</w> <w n="82.2">les</w> <w n="82.3">forêts</w> <w n="82.4">de</w> <w n="82.5">Cypre</w> <w n="82.6">et</w> <w n="82.7">fait</w> <w n="82.8">la</w> <w n="82.9">chasse</w> <w n="82.10">aux</w> <w n="82.11">belles</w>,</l>
					<l n="83" num="33.3"><w n="83.1">C</w>’<w n="83.2">est</w> <w n="83.3">lui</w> <w n="83.4">qui</w>, <w n="83.5">sur</w> <w n="83.6">son</w> <w n="83.7">cor</w>, <w n="83.8">vient</w> <w n="83.9">sonner</w> <w n="83.10">l</w>’<w n="83.11">hallali</w>.</l>
					<l n="84" num="33.4"><w n="84.1">Si</w> <w n="84.2">Gaiffe</w> <w n="84.3">est</w> <w n="84.4">toujours</w> <w n="84.5">beau</w>, <w n="84.6">Monselet</w> <w n="84.7">est</w> <w n="84.8">joli</w>.</l>
				</lg>
				<lg n="34">
					<head type="main">Rose</head>
					<l n="85" num="34.1"><w n="85.1">Monselet</w> <w n="85.2">est</w> <w n="85.3">joli</w>, <w n="85.4">cela</w> <w n="85.5">je</w> <w n="85.6">te</w> <w n="85.7">l</w>’<w n="85.8">accorde</w>.</l>
					<l n="86" num="34.2"><w n="86.1">Comme</w> <w n="86.2">un</w> <w n="86.3">Américain</w> <w n="86.4">voltigeant</w> <w n="86.5">sur</w> <w n="86.6">la</w> <w n="86.7">corde</w></l>
					<l n="87" num="34.3"><w n="87.1">Tout</w> <w n="87.2">vêtu</w> <w n="87.3">de</w> <w n="87.4">soleil</w> <w n="87.5">et</w> <w n="87.6">d</w>’<w n="87.7">écailles</w> <w n="87.8">d</w>’<w n="87.9">argent</w>,</l>
					<l n="88" num="34.4"><w n="88.1">Il</w> <w n="88.2">jette</w> <w n="88.3">à</w> <w n="88.4">l</w>’<w n="88.5">azur</w> <w n="88.6">même</w> <w n="88.7">un</w> <w n="88.8">regard</w> <w n="88.9">indulgent</w> !</l>
				</lg>
				<lg n="35">
					<head type="main">Rosette</head>
					<l n="89" num="35.1"><w n="89.1">On</w> <w n="89.2">peut</w> <w n="89.3">aimer</w> <w n="89.4">un</w> <w n="89.5">pitre</w>, <w n="89.6">un</w> <w n="89.7">notaire</w>, <w n="89.8">un</w> <w n="89.9">Osage</w>.</l>
					<l n="90" num="35.2"><w n="90.1">Tel</w> <w n="90.2">s</w>’<w n="90.3">éprend</w> <w n="90.4">d</w>’<w n="90.5">une</w> <w n="90.6">femme</w> <w n="90.7">au</w> <w n="90.8">gracieux</w> <w n="90.9">visage</w></l>
					<l n="91" num="35.3"><w n="91.1">Rencontrée</w> <w n="91.2">au</w> <w n="91.3">Brésil</w> <w n="91.4">ou</w> <w n="91.5">dans</w> <w n="91.6">Piccadilly</w> :</l>
					<l n="92" num="35.4"><w n="92.1">Avant</w> <w n="92.2">tout</w>, <w n="92.3">à</w> <w n="92.4">mes</w> <w n="92.5">yeux</w>, <w n="92.6">Monselet</w> <w n="92.7">est</w> <w n="92.8">joli</w>.</l>
				</lg>
				<lg n="36">
					<head type="main">Palémon</head>
					<l n="93" num="36.1"><w n="93.1">Enfants</w>, <w n="93.2">vous</w> <w n="93.3">parlez</w> <w n="93.4">bien</w> ; <w n="93.5">mais</w> <w n="93.6">qui</w> <w n="93.7">pourrait</w> <w n="93.8">tout</w> <w n="93.9">dire</w> ?</l>
					<l n="94" num="36.2"><w n="94.1">Laisse</w> <w n="94.2">là</w> <w n="94.3">ton</w> <w n="94.4">crayon</w>, <w n="94.5">toi</w>, <w n="94.6">rimeur</w> <w n="94.7">en</w> <w n="94.8">délire</w> ;</l>
					<l n="95" num="36.3"><w n="95.1">Buvons</w>, <w n="95.2">et</w> <w n="95.3">ne</w> <w n="95.4">perds</w> <w n="95.5">pas</w> <w n="95.6">tous</w> <w n="95.7">ces</w> <w n="95.8">instants</w> <w n="95.9">si</w> <w n="95.10">courts</w></l>
					<l n="96" num="36.4"><w n="96.1">A</w> <w n="96.2">sténographier</w> <w n="96.3">mot</w> <w n="96.4">à</w> <w n="96.5">mot</w> <w n="96.6">les</w> <w n="96.7">discours</w></l>
					<l n="97" num="36.5"><w n="97.1">De</w> <w n="97.2">ces</w> <w n="97.3">buveuses</w> <w n="97.4">d</w>’<w n="97.5">or</w> <w n="97.6">à</w> <w n="97.7">la</w> <w n="97.8">fauve</w> <w n="97.9">crinière</w>.</l>
					<l n="98" num="36.6"><w n="98.1">Elles</w> <w n="98.2">causaient</w> <w n="98.3">de</w> <w n="98.4">chose</w> <w n="98.5">et</w> <w n="98.6">d</w>’<w n="98.7">autre</w>, <w n="98.8">à</w> <w n="98.9">la</w> <w n="98.10">manière</w></l>
					<l n="99" num="36.7"><w n="99.1">Des</w> <w n="99.2">bergers</w> <w n="99.3">de</w> <w n="99.4">Sicile</w> <w n="99.5">essayant</w> <w n="99.6">leurs</w> <w n="99.7">pipeaux</w>,</l>
					<l n="100" num="36.8"><w n="100.1">Et</w> <w n="100.2">n</w>’<w n="100.3">avaient</w> <w n="100.4">pas</w> <w n="100.5">tenu</w> <w n="100.6">ces</w> <w n="100.7">frivoles</w> <w n="100.8">propos</w></l>
					<l n="101" num="36.9"><w n="101.1">Littéraires</w>, <w n="101.2">afin</w> <w n="101.3">que</w> <w n="101.4">tu</w> <w n="101.5">les</w> <w n="101.6">écrivisses</w>.</l>
					<l n="102" num="36.10"><w n="102.1">Mais</w> <w n="102.2">voici</w> <w n="102.3">le</w> <w n="102.4">champagne</w> <w n="102.5">avec</w> <w n="102.6">les</w> <w n="102.7">écrevisses</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1862">Mars 1862.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>