<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">OCCIDENTALES</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAN">
					<name>
						<forename>Théodore</forename>
						<nameLink>de</nameLink>
						<surname>BANVILLE</surname>
					</name>
					<date from="1823" to="1891">1823-1891</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2572 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BAN_13</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">OCCIDENTALES</title>
						<author>Théodore de Banville</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Peter Edwards</publisher>
						<pubPlace>Mount Allison University, Sackville, N.B. Canada E4L 1C7</pubPlace>
						<idno type="URL">https://www.mta.ca/banville/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>OEUVRES POETIQUES COMPLETES</title>
								<author>Théodore de Banville</author>
								<editor>Edition critique publiée sous la direction de Peter J. Edwards par Peter S. Hambly</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Honoré Champion</publisher>
									<date when="1998">1998</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres</title>
						<author>Théodore de Banville</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Alphonse Lemerre, éditeur</publisher>
							<date when="1891">1889-1891</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1875">1875</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface et la dédicace du recueil ne sont pas intégrées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-03-13" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAN547">
				<head type="main">L’Œil crevé</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Fronts</w> <w n="1.2">échevelés</w> <w n="1.3">dans</w> <w n="1.4">la</w> <w n="1.5">brise</w>,</l>
					<l n="2" num="1.2"><w n="2.1">O</w> <w n="2.2">fantômes</w> <w n="2.3">des</w> <w n="2.4">cieux</w> <w n="2.5">mouvants</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">flottez</w> <w n="3.3">dans</w> <w n="3.4">l</w>’<w n="3.5">ombre</w> <w n="3.6">indécise</w></l>
					<l n="4" num="1.4"><w n="4.1">Entre</w> <w n="4.2">les</w> <w n="4.3">morts</w> <w n="4.4">et</w> <w n="4.5">les</w> <w n="4.6">vivants</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Vous</w> <w n="5.2">dont</w> <w n="5.3">l</w>’<w n="5.4">aile</w> <w n="5.5">semble</w> <w n="5.6">si</w> <w n="5.7">lasse</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Parlez</w>, <w n="6.2">spectres</w> <w n="6.3">mystérieux</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Dites</w>-<w n="7.2">moi</w> <w n="7.3">vos</w> <w n="7.4">noms</w> <w n="7.5">à</w> <w n="7.6">voix</w> <w n="7.7">basse</w>.</l>
					<l n="8" num="2.4"><w n="8.1">Oh</w> ! <w n="8.2">ne</w> <w n="8.3">détournez</w> <w n="8.4">pas</w> <w n="8.5">les</w> <w n="8.6">yeux</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Vous</w> <w n="9.2">d</w>’<w n="9.3">abord</w>, <w n="9.4">ô</w> <w n="9.5">couple</w> <w n="9.6">martyre</w></l>
					<l n="10" num="3.2"><w n="10.1">Qui</w> <w n="10.2">gémissez</w> <w n="10.3">en</w> <w n="10.4">mots</w> <w n="10.5">plus</w> <w n="10.6">doux</w></l>
					<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">la</w> <w n="11.3">caresse</w> <w n="11.4">d</w>’<w n="11.5">une</w> <w n="11.6">lyre</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Ici</w>-<w n="12.2">bas</w>, <w n="12.3">dites</w>, <w n="12.4">qu</w>’<w n="12.5">étiez</w>-<w n="12.6">vous</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Bon</w> <w n="13.2">passant</w>, <w n="13.3">nous</w> <w n="13.4">étions</w> <w n="13.5">les</w> <w n="13.6">Drames</w></l>
					<l n="14" num="4.2"><w n="14.1">Sur</w> <w n="14.2">lesquels</w> <w n="14.3">se</w> <w n="14.4">lamente</w>, <w n="14.5">hélas</w> !</l>
					<l n="15" num="4.3"><w n="15.1">La</w> <w n="15.2">muse</w>, <w n="15.3">que</w> <w n="15.4">nous</w> <w n="15.5">adorâmes</w> :</l>
					<l n="16" num="4.4"><w n="16.1">Marion</w> <w n="16.2">Delorme</w> <w n="16.3">et</w> <w n="16.4">Ruy</w> <w n="16.5">Blas</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Toi</w>, <w n="17.2">qu</w>’<w n="17.3">es</w>-<w n="17.4">tu</w>, <w n="17.5">Victoire</w> <w n="17.6">ou</w> <w n="17.7">Génie</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Guerrière</w> <w n="18.2">au</w> <w n="18.3">casque</w> <w n="18.4">dénoué</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Qui</w> <w n="19.2">portes</w> <w n="19.3">dans</w> <w n="19.4">ta</w> <w n="19.5">main</w> <w n="19.6">bénie</w></l>
					<l n="20" num="5.4"><w n="20.1">Un</w> <w n="20.2">drapeau</w>, <w n="20.3">de</w> <w n="20.4">balles</w> <w n="20.5">troué</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Dis</w> ! — <w n="21.2">Je</w> <w n="21.3">suis</w> <w n="21.4">la</w> <w n="21.5">Chanson</w> <w n="21.6">épique</w></l>
					<l n="22" num="6.2"><w n="22.1">Dont</w> <w n="22.2">le</w> <w n="22.3">souffle</w> <w n="22.4">sur</w> <w n="22.5">l</w>’<w n="22.6">escadron</w></l>
					<l n="23" num="6.3"><w n="23.1">Fait</w> <w n="23.2">au</w> <w n="23.3">loin</w> <w n="23.4">frissonner</w> <w n="23.5">la</w> <w n="23.6">pique</w></l>
					<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">mugir</w> <w n="24.3">le</w> <w n="24.4">sombre</w> <w n="24.5">clairon</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Je</w> <w n="25.2">suis</w> <w n="25.3">l</w>’<w n="25.4">Ode</w> <w n="25.5">aux</w> <w n="25.6">voix</w> <w n="25.7">enflammées</w></l>
					<l n="26" num="7.2"><w n="26.1">Qui</w> <w n="26.2">sur</w> <w n="26.3">l</w>’<w n="26.4">Europe</w>, <w n="26.5">en</w> <w n="26.6">un</w> <w n="26.7">seul</w> <w n="26.8">jour</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Faisait</w> <w n="27.2">bondir</w> <w n="27.3">quatorze</w> <w n="27.4">armées</w></l>
					<l n="28" num="7.4"><w n="28.1">Ivres</w> <w n="28.2">d</w>’<w n="28.3">espérance</w> <w n="28.4">et</w> <w n="28.5">d</w>’<w n="28.6">amour</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">toi</w>, <w n="29.3">qu</w>’<w n="29.4">es</w>-<w n="29.5">tu</w>, <w n="29.6">dis</w> ? — <w n="29.7">Je</w> <w n="29.8">suis</w> <w n="29.9">Celle</w></l>
					<l n="30" num="8.2"><w n="30.1">Que</w> <w n="30.2">l</w>’<w n="30.3">on</w> <w n="30.4">nomme</w> <w n="30.5">à</w> <w n="30.6">présent</w> <w n="30.7">tout</w> <w n="30.8">bas</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Celle</w> <w n="31.2">dont</w> <w n="31.3">l</w>’<w n="31.4">œil</w> <w n="31.5">fauve</w> <w n="31.6">étincelle</w></l>
					<l n="32" num="8.4"><w n="32.1">Dans</w> <w n="32.2">la</w> <w n="32.3">paix</w> <w n="32.4">et</w> <w n="32.5">dans</w> <w n="32.6">les</w> <w n="32.7">combats</w> ;</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Celle</w> <w n="33.2">qui</w>, <w n="33.3">dans</w> <w n="33.4">les</w> <w n="33.5">jours</w> <w n="33.6">prospères</w></l>
					<l n="34" num="9.2"><w n="34.1">Où</w> <w n="34.2">s</w>’<w n="34.3">alluma</w> <w n="34.4">le</w> <w n="34.5">grand</w> <w n="34.6">flambeau</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Était</w> <w n="35.2">l</w>’<w n="35.3">amante</w> <w n="35.4">de</w> <w n="35.5">vos</w> <w n="35.6">pères</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Lorsque</w> <w n="36.2">le</w> <w n="36.3">géant</w> <w n="36.4">Mirabeau</w></l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Terrassait</w>, <w n="37.2">en</w> <w n="37.3">pleine</w> <w n="37.4">assemblée</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Une</w> <w n="38.2">antique</w> <w n="38.3">rébellion</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">secouait</w> <w n="39.3">dans</w> <w n="39.4">la</w> <w n="39.5">mêlée</w></l>
					<l n="40" num="10.4"><w n="40.1">Sa</w> <w n="40.2">chevelure</w> <w n="40.3">de</w> <w n="40.4">lion</w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">O</w> <w n="41.2">figures</w> <w n="41.3">habituées</w></l>
					<l n="42" num="11.2"><w n="42.1">A</w> <w n="42.2">ce</w> <w n="42.3">vertigineux</w> <w n="42.4">essor</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Envolez</w>-<w n="43.2">vous</w> <w n="43.3">dans</w> <w n="43.4">les</w> <w n="43.5">nuées</w> !</l>
					<l n="44" num="11.4"><w n="44.1">Ce</w> <w n="44.2">n</w>’<w n="44.3">est</w> <w n="44.4">pas</w> <w n="44.5">votre</w> <w n="44.6">jour</w> <w n="44.7">encor</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Vous</w> <w n="45.2">voulez</w> <w n="45.3">parler</w> <w n="45.4">à</w> <w n="45.5">des</w> <w n="45.6">hommes</w></l>
					<l n="46" num="12.2"><w n="46.1">Faits</w> <w n="46.2">de</w> <w n="46.3">devoir</w> <w n="46.4">et</w> <w n="46.5">de</w> <w n="46.6">pitié</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">nous</w>, <w n="47.3">spectres</w> <w n="47.4">divins</w>, <w n="47.5">nous</w> <w n="47.6">sommes</w></l>
					<l n="48" num="12.4"><w n="48.1">Presque</w> <w n="48.2">aveugles</w>, <w n="48.3">sourds</w> <w n="48.4">à</w> <w n="48.5">moitié</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Nous</w> <w n="49.2">sommes</w>, <w n="49.3">fronts</w> <w n="49.4">coiffés</w> <w n="49.5">en</w> <w n="49.6">touffe</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Cols</w> <w n="50.2">serrés</w> <w n="50.3">dans</w> <w n="50.4">un</w> <w n="50.5">court</w> <w n="50.6">feston</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Les</w> <w n="51.2">gens</w> <w n="51.3">de</w> <w n="51.4">la</w> <w n="51.5">musique</w> <w n="51.6">bouffe</w>,</l>
					<l n="52" num="13.4"><w n="52.1">Des</w> <w n="52.2">cocottes</w> <w n="52.3">et</w> <w n="52.4">du</w> <w n="52.5">veston</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Le</w> <w n="53.2">mot</w> <w n="53.3">d</w>’<w n="53.4">Hervé</w>, <w n="53.5">c</w>’<w n="53.6">est</w> <w n="53.7">notre</w> <w n="53.8">histoire</w> !</l>
					<l n="54" num="14.2"><w n="54.1">Car</w>, <w n="54.2">s</w>’<w n="54.3">il</w> <w n="54.4">faut</w> <w n="54.5">que</w> <w n="54.6">nos</w> <w n="54.7">passions</w></l>
					<l n="55" num="14.3"><w n="55.1">Se</w> <w n="55.2">rallument</w> <w n="55.3">dans</w> <w n="55.4">l</w>’<w n="55.5">ombre</w> <w n="55.6">noire</w></l>
					<l n="56" num="14.4"><w n="56.1">Et</w> <w n="56.2">que</w> <w n="56.3">nous</w> <w n="56.4">vous</w> <w n="56.5">reconnaissions</w>,</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Vous</w> <w n="57.2">qui</w> <w n="57.3">fûtes</w> <w n="57.4">notre</w> <w n="57.5">délire</w>,</l>
					<l n="58" num="15.2"><w n="58.1">Notre</w> <w n="58.2">trésor</w> <w n="58.3">et</w> <w n="58.4">notre</w> <w n="58.5">orgueil</w>,</l>
					<l n="59" num="15.3"><w n="59.1">Attendez</w> <w n="59.2">que</w> <w n="59.3">l</w>’<w n="59.4">on</w> <w n="59.5">nous</w> <w n="59.6">retire</w></l>
					<l n="60" num="15.4"><w n="60.1">La</w> <w n="60.2">flèche</w> <w n="60.3">qui</w> <w n="60.4">nous</w> <w n="60.5">sort</w> <w n="60.6">de</w> <w n="60.7">l</w>’<w n="60.8">œil</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1868">Janvier 1868.</date>
					</dateline>
				</closer>
			</div></body></text></TEI>