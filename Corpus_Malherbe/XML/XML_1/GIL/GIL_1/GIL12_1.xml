<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CAP ÉTERNITÉ</head><div type="poem" key="GIL12">
					<head type="number">Chant XI</head>
					<head type="main">Vers la Cîme</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Combien</w> <w n="1.2">d</w>’<w n="1.3">heures</w>, <w n="1.4">hélas</w> ! <w n="1.5">trop</w> <w n="1.6">brèves</w>, <w n="1.7">sont</w> <w n="1.8">passées</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Pendant</w> <w n="2.2">que</w> <w n="2.3">jusqu</w>’<w n="2.4">à</w> <w n="2.5">Dieu</w> <w n="2.6">s</w>’<w n="2.7">élevaient</w> <w n="2.8">nos</w> <w n="2.9">pensées</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">que</w>, <w n="3.3">dans</w> <w n="3.4">le</w> <w n="3.5">repos</w> <w n="3.6">du</w> <w n="3.7">jour</w> <w n="3.8">silencieux</w>,</l>
						<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">enivrais</w> <w n="4.3">de</w> <w n="4.4">grandeur</w> <w n="4.5">mon</w> <w n="4.6">esprit</w> <w n="4.7">et</w> <w n="4.8">mes</w> <w n="4.9">yeux</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">soleil</w> <w n="5.3">au</w> <w n="5.4">zénith</w> <w n="5.5">couronnait</w> <w n="5.6">sa</w> <w n="5.7">carrière</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Mon</w> <w n="6.2">rapide</w> <w n="6.3">aviron</w> <w n="6.4">troubla</w> <w n="6.5">la</w> <w n="6.6">pureté</w></l>
						<l n="7" num="1.7"><w n="7.1">De</w> <w n="7.2">l</w>’<w n="7.3">onde</w> <w n="7.4">chatoyante</w> <w n="7.5">où</w> <w n="7.6">jouait</w> <w n="7.7">la</w> <w n="7.8">lumière</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">j</w>’<w n="8.3">atteignis</w> <w n="8.4">bientôt</w> <w n="8.5">le</w> <w n="8.6">Cap</w> <w n="8.7">Éternité</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Dans</w> <w n="9.2">l</w>’<w n="9.3">anse</w> <w n="9.4">où</w> <w n="9.5">les</w> <w n="9.6">cailloux</w> <w n="9.7">éboulés</w> <w n="9.8">forment</w> <w n="9.9">chaîne</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Le</w> <w n="10.2">rocher</w> <w n="10.3">moins</w> <w n="10.4">abrupt</w> <w n="10.5">me</w> <w n="10.6">permit</w> <w n="10.7">d</w>’<w n="10.8">aborder</w></l>
						<l n="11" num="1.11"><w n="11.1">Près</w> <w n="11.2">d</w>’<w n="11.3">un</w> <w n="11.4">torrent</w> <w n="11.5">que</w> <w n="11.6">j</w>’<w n="11.7">entendais</w> <w n="11.8">déjà</w> <w n="11.9">gronder</w>.</l>
						<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<ab type="dot"> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
						<l n="12" num="1.12"><w n="12.1">J</w>’<w n="12.2">ai</w> <w n="12.3">l</w>’<w n="12.4">orgueil</w> <w n="12.5">de</w> <w n="12.6">gravir</w> <w n="12.7">la</w> <w n="12.8">cime</w> <w n="12.9">souveraine</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Je</w> <w n="13.2">veux</w> <w n="13.3">escalader</w> <w n="13.4">le</w> <w n="13.5">fier</w> <w n="13.6">dominateur</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Je</w> <w n="14.2">veux</w> <w n="14.3">aller</w> <w n="14.4">baigner</w> <w n="14.5">mon</w> <w n="14.6">front</w> <w n="14.7">dans</w> <w n="14.8">ses</w> <w n="14.9">nuages</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Côtoyer</w> <w n="15.2">son</w> <w n="15.3">abîme</w>, <w n="15.4">éprouver</w> <w n="15.5">ses</w> <w n="15.6">orages</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Et</w>, <w n="16.2">plus</w> <w n="16.3">près</w> <w n="16.4">de</w> <w n="16.5">l</w>’<w n="16.6">azur</w>, <w n="16.7">m</w>’<w n="16.8">enivrer</w> <w n="16.9">de</w> <w n="16.10">grandeur</w>.</l>
					</lg>
					<lg n="2">
						<l n="17" num="2.1"><w n="17.1">J</w>’<w n="17.2">hésite</w>, <w n="17.3">en</w> <w n="17.4">parcourant</w> <w n="17.5">du</w> <w n="17.6">regard</w> <w n="17.7">l</w>’<w n="17.8">âpre</w> <w n="17.9">pente</w> ;</l>
						<l n="18" num="2.2"><w n="18.1">Mais</w> <w n="18.2">le</w> <w n="18.3">lit</w> <w n="18.4">du</w> <w n="18.5">torrent</w> <w n="18.6">m</w>’<w n="18.7">indiquant</w> <w n="18.8">un</w> <w n="18.9">chemin</w>,</l>
						<l n="19" num="2.3"><w n="19.1">J</w>’<w n="19.2">aventure</w> <w n="19.3">mes</w> <w n="19.4">pas</w> <w n="19.5">au</w> <w n="19.6">revers</w> <w n="19.7">du</w> <w n="19.8">ravin</w></l>
						<l n="20" num="2.4"><w n="20.1">Qui</w>, <w n="20.2">le</w> <w n="20.3">long</w> <w n="20.4">du</w> <w n="20.5">flanc</w> <w n="20.6">roide</w>, <w n="20.7">obliquement</w> <w n="20.8">serpente</w>.</l>
						<l n="21" num="2.5"><w n="21.1">Le</w> <w n="21.2">torrent</w>, <w n="21.3">par</w> <w n="21.4">endroits</w>, <w n="21.5">sur</w> <w n="21.6">le</w> <w n="21.7">roc</w> <w n="21.8">vertical</w></l>
						<l n="22" num="2.6"><w n="22.1">Brise</w> <w n="22.2">sa</w> <w n="22.3">nappe</w> <w n="22.4">d</w>’<w n="22.5">eau</w> <w n="22.6">qui</w> <w n="22.7">tombe</w> <w n="22.8">en</w> <w n="22.9">cascatelle</w> ;</l>
						<l n="23" num="2.7"><w n="23.1">Plus</w> <w n="23.2">loin</w>, <w n="23.3">du</w> <w n="23.4">drap</w> <w n="23.5">lamé</w> <w n="23.6">l</w>’<w n="23.7">écluse</w> <w n="23.8">naturelle</w></l>
						<l n="24" num="2.8"><w n="24.1">Sous</w> <w n="24.2">le</w> <w n="24.3">dôme</w> <w n="24.4">des</w> <w n="24.5">pins</w> <w n="24.6">retient</w> <w n="24.7">son</w> <w n="24.8">frais</w> <w n="24.9">cristal</w>.</l>
						<l n="25" num="2.9"><w n="25.1">Le</w> <w n="25.2">torrent</w> <w n="25.3">me</w> <w n="25.4">conduit</w> <w n="25.5">à</w> <w n="25.6">mi</w>-<w n="25.7">chemin</w> <w n="25.8">du</w> <w n="25.9">faîte</w>.</l>
						<l n="26" num="2.10"><w n="26.1">Contre</w> <w n="26.2">la</w> <w n="26.3">forêt</w> <w n="26.4">vierge</w> <w n="26.5">il</w> <w n="26.6">me</w> <w n="26.7">faut</w> <w n="26.8">batailler</w> :</l>
						<l n="27" num="2.11"><w n="27.1">Là</w>, <w n="27.2">grimpant</w> <w n="27.3">au</w> <w n="27.4">bouleau</w> <w n="27.5">quand</w> <w n="27.6">l</w>’<w n="27.7">obstacle</w> <w n="27.8">m</w>’<w n="27.9">arrête</w>,</l>
						<l n="28" num="2.12"><w n="28.1">Ici</w>, <w n="28.2">me</w> <w n="28.3">cramponnant</w> <w n="28.4">au</w> <w n="28.5">souple</w> <w n="28.6">coudrier</w>.</l>
						<l n="29" num="2.13"><w n="29.1">Et</w>, <w n="29.2">quoique</w> <w n="29.3">sans</w> <w n="29.4">péril</w>, <w n="29.5">la</w> <w n="29.6">lutte</w> <w n="29.7">est</w> <w n="29.8">belle</w> <w n="29.9">et</w> <w n="29.10">rude</w>,</l>
						<l n="30" num="2.14"><w n="30.1">Plus</w> <w n="30.2">je</w> <w n="30.3">m</w>’<w n="30.4">engage</w> <w n="30.5">avant</w> <w n="30.6">dans</w> <w n="30.7">cette</w> <w n="30.8">solitude</w>.</l>
						<l n="31" num="2.15"><w n="31.1">Il</w> <w n="31.2">me</w> <w n="31.3">faut</w> <w n="31.4">contourner</w> <w n="31.5">d</w>’<w n="31.6">énormes</w> <w n="31.7">rochers</w> <w n="31.8">roux</w></l>
						<l n="32" num="2.16"><w n="32.1">Que</w>, <w n="32.2">de</w> <w n="32.3">loin</w>, <w n="32.4">j</w>’<w n="32.5">avais</w> <w n="32.6">pris</w> <w n="32.7">pour</w> <w n="32.8">de</w> <w n="32.9">simples</w> <w n="32.10">cailloux</w> ;</l>
						<l n="33" num="2.17"><w n="33.1">Les</w> <w n="33.2">buissons</w> <w n="33.3">épineux</w> <w n="33.4">où</w> <w n="33.5">mon</w> <w n="33.6">pas</w> <w n="33.7">s</w>’<w n="33.8">enchevêtre</w>,</l>
						<l n="34" num="2.18"><w n="34.1">Les</w> <w n="34.2">bocages</w> <w n="34.3">touffus</w> <w n="34.4">de</w> <w n="34.5">l</w>’<w n="34.6">érable</w> <w n="34.7">et</w> <w n="34.8">du</w> <w n="34.9">hêtre</w>,</l>
						<l n="35" num="2.19"><w n="35.1">M</w>’<w n="35.2">avaient</w> <w n="35.3">paru</w> <w n="35.4">d</w>’<w n="35.5">en</w> <w n="35.6">bas</w> <w n="35.7">un</w> <w n="35.8">tapis</w> <w n="35.9">de</w> <w n="35.10">gazon</w>.</l>
						<l n="36" num="2.20"><w n="36.1">Toute</w> <w n="36.2">une</w> <w n="36.3">virginale</w> <w n="36.4">et</w> <w n="36.5">simple</w> <w n="36.6">floraison</w></l>
						<l n="37" num="2.21"><w n="37.1">Étale</w> <w n="37.2">ses</w> <w n="37.3">couleurs</w> <w n="37.4">sous</w> <w n="37.5">l</w>’<w n="37.6">épaisse</w> <w n="37.7">ramure</w>.</l>
					</lg>
					<lg n="3">
						<l n="38" num="3.1"><w n="38.1">Je</w> <w n="38.2">cueille</w> <w n="38.3">le</w> <w n="38.4">bluet</w>, <w n="38.5">la</w> <w n="38.6">noisette</w>, <w n="38.7">la</w> <w n="38.8">mure</w>,</l>
						<l n="39" num="3.2"><w n="39.1">Et</w> <w n="39.2">certain</w> <w n="39.3">petit</w> <w n="39.4">fruit</w> <w n="39.5">rouge</w> <w n="39.6">et</w> <w n="39.7">délicieux</w></l>
						<l n="40" num="3.3"><w n="40.1">Qui</w> <w n="40.2">croît</w> <w n="40.3">en</w> <w n="40.4">abondance</w> <w n="40.5">au</w> <w n="40.6">milieu</w> <w n="40.7">de</w> <w n="40.8">la</w> <w n="40.9">mousse</w>.</l>
						<l n="41" num="3.4"><w n="41.1">Ô</w> <w n="41.2">pins</w> <w n="41.3">harmonieux</w>, <w n="41.4">comme</w> <w n="41.5">votre</w> <w n="41.6">ombre</w> <w n="41.7">est</w> <w n="41.8">douce</w> !</l>
						<l n="42" num="3.5"><w n="42.1">Je</w> <w n="42.2">dîne</w> <w n="42.3">en</w> <w n="42.4">un</w> <w n="42.5">palais</w> <w n="42.6">où</w> <w n="42.7">dîneraient</w> <w n="42.8">les</w> <w n="42.9">dieux</w> :</l>
						<l n="43" num="3.6"><w n="43.1">Ma</w> <w n="43.2">nappe</w> <w n="43.3">immaculée</w> <w n="43.4">est</w> <w n="43.5">un</w> <w n="43.6">fragment</w> <w n="43.7">de</w> <w n="43.8">marbre</w>,</l>
						<l n="44" num="3.7"><w n="44.1">Mon</w> <w n="44.2">cellier</w> <w n="44.3">est</w> <w n="44.4">un</w> <w n="44.5">lac</w> <w n="44.6">endormi</w> <w n="44.7">sous</w> <w n="44.8">les</w> <w n="44.9">bois</w>,</l>
						<l n="45" num="3.8"><w n="45.1">Et</w> <w n="45.2">l</w>’<w n="45.3">écorce</w> <w n="45.4">argentée</w> <w n="45.5">est</w> <w n="45.6">la</w> <w n="45.7">coupe</w> <w n="45.8">où</w> <w n="45.9">je</w> <w n="45.10">bois</w>.</l>
						<l n="46" num="3.9"><w n="46.1">Un</w> <w n="46.2">rêve</w> <w n="46.3">musical</w> <w n="46.4">frissonne</w> <w n="46.5">dans</w> <w n="46.6">un</w> <w n="46.7">arbre</w></l>
						<l n="47" num="3.10"><w n="47.1">Où</w> <w n="47.2">d</w>’<w n="47.3">invisibles</w> <w n="47.4">chœurs</w> <w n="47.5">gazouillent</w> <w n="47.6">un</w> <w n="47.7">concert</w>.</l>
						<l n="48" num="3.11"><w n="48.1">Dans</w> <w n="48.2">ma</w> <w n="48.3">coupe</w> <w n="48.4">d</w>’<w n="48.5">écorce</w>, <w n="48.6">au</w> <w n="48.7">ruisseau</w> <w n="48.8">qui</w> <w n="48.9">murmure</w>,</l>
						<l n="49" num="3.12"><w n="49.1">Une</w> <w n="49.2">dernière</w> <w n="49.3">fois</w> <w n="49.4">je</w> <w n="49.5">puise</w> <w n="49.6">l</w>’<w n="49.7">onde</w> <w n="49.8">pure</w>,</l>
						<l n="50" num="3.13"><w n="50.1">Et</w>, <w n="50.2">convive</w> <w n="50.3">poli</w>, <w n="50.4">quand</w> <w n="50.5">finit</w> <w n="50.6">le</w> <w n="50.7">dessert</w>,</l>
						<l n="51" num="3.14"><w n="51.1">Je</w> <w n="51.2">bois</w> <w n="51.3">à</w> <w n="51.4">mon</w> <w n="51.5">hôtesse</w>, <w n="51.6">à</w> <w n="51.7">la</w> <w n="51.8">grande</w> <w n="51.9">Nature</w>.</l>
					</lg>
					<lg n="4">
						<l n="52" num="4.1"><w n="52.1">Le</w> <w n="52.2">souci</w> <w n="52.3">d</w>’<w n="52.4">arriver</w> <w n="52.5">abrège</w> <w n="52.6">mon</w> <w n="52.7">repos</w>.</l>
						<l n="53" num="4.2"><w n="53.1">Je</w> <w n="53.2">reprends</w>, <w n="53.3">maintenant</w> <w n="53.4">plus</w> <w n="53.5">fort</w> <w n="53.6">et</w> <w n="53.7">plus</w> <w n="53.8">dispos</w>,</l>
						<l n="54" num="4.3"><w n="54.1">À</w> <w n="54.2">même</w> <w n="54.3">la</w> <w n="54.4">forêt</w> <w n="54.5">l</w>’<w n="54.6">interminable</w> <w n="54.7">lutte</w>,</l>
						<l n="55" num="4.4"><w n="55.1">Car</w> <w n="55.2">déjà</w> <w n="55.3">le</w> <w n="55.4">soleil</w> <w n="55.5">penche</w> <w n="55.6">vers</w> <w n="55.7">son</w> <w n="55.8">déclin</w>,</l>
						<l n="56" num="4.5"><w n="56.1">Et</w> <w n="56.2">je</w> <w n="56.3">crains</w> <w n="56.4">que</w> <w n="56.5">la</w> <w n="56.6">nuit</w> <w n="56.7">ne</w> <w n="56.8">m</w>’<w n="56.9">arrête</w> <w n="56.10">en</w> <w n="56.11">chemin</w>.</l>
						<l n="57" num="4.6"><w n="57.1">Je</w> <w n="57.2">me</w> <w n="57.3">hâte</w> ; <w n="57.4">l</w>’<w n="57.5">écho</w> <w n="57.6">sonore</w> <w n="57.7">répercute</w></l>
						<l n="58" num="4.7"><w n="58.1">Tantôt</w> <w n="58.2">le</w> <w n="58.3">craquement</w> <w n="58.4">des</w> <w n="58.5">branches</w> <w n="58.6">sous</w> <w n="58.7">mes</w> <w n="58.8">pas</w>,</l>
						<l n="59" num="4.8"><w n="59.1">Tantôt</w> <w n="59.2">le</w> <w n="59.3">bruit</w> <w n="59.4">plus</w> <w n="59.5">sourd</w> <w n="59.6">d</w>’<w n="59.7">une</w> <w n="59.8">pierre</w> <w n="59.9">ébranlée</w>.</l>
						<l n="60" num="4.9"><w n="60.1">Il</w> <w n="60.2">me</w> <w n="60.3">semble</w> <w n="60.4">parfois</w> <w n="60.5">que</w> <w n="60.6">je</w> <w n="60.7">n</w>’<w n="60.8">atteindrai</w> <w n="60.9">pas</w></l>
						<l n="61" num="4.10"><w n="61.1">La</w> <w n="61.2">cime</w> <w n="61.3">toute</w> <w n="61.4">bleue</w> <w n="61.5">et</w> <w n="61.6">de</w> <w n="61.7">pins</w> <w n="61.8">dentelée</w>,</l>
						<l n="62" num="4.11"><w n="62.1">Qui</w> <w n="62.2">toujours</w> <w n="62.3">se</w> <w n="62.4">dérobe</w> <w n="62.5">et</w> <w n="62.6">parait</w> <w n="62.7">au</w> <w n="62.8">regard</w></l>
						<l n="63" num="4.12"><w n="63.1">Toujours</w> <w n="63.2">de</w> <w n="63.3">plus</w> <w n="63.4">en</w> <w n="63.5">plus</w> <w n="63.6">hautaine</w> <w n="63.7">et</w> <w n="63.8">reculée</w>.</l>
						<l n="64" num="4.13"><w n="64.1">L</w>’<w n="64.2">heure</w> <w n="64.3">rapide</w> <w n="64.4">passe</w> ; <w n="64.5">et</w> <w n="64.6">je</w> <w n="64.7">songe</w> : « <w n="64.8">Il</w> <w n="64.9">est</w> <w n="64.10">tard</w> !</l>
						<l n="65" num="4.14"><w n="65.1">Je</w> <w n="65.2">suis</w> <w n="65.3">bien</w> <w n="65.4">las</w> !… <w n="65.5">Pourtant</w>, <w n="65.6">ô</w> <w n="65.7">cime</w> <w n="65.8">inaccessible</w>,</l>
						<l n="66" num="4.15"><w n="66.1">Ce</w> <w n="66.2">qui</w> <w n="66.3">dépend</w> <w n="66.4">de</w> <w n="66.5">nous</w> <w n="66.6">en</w> <w n="66.7">ce</w> <w n="66.8">monde</w>, <w n="66.9">est</w> <w n="66.10">possible</w> !…</l>
						<l n="67" num="4.16"><w n="67.1">Tu</w> <w n="67.2">fuis</w> ! <w n="67.3">En</w> <w n="67.4">m</w>’<w n="67.5">épuisant</w>, <w n="67.6">vers</w> <w n="67.7">toi</w> <w n="67.8">je</w> <w n="67.9">suis</w> <w n="67.10">monté</w> ;</l>
						<l n="68" num="4.17"><w n="68.1">Ma</w> <w n="68.2">force</w> <w n="68.3">m</w>’<w n="68.4">abandonne</w>, <w n="68.5">et</w> <w n="68.6">tu</w> <w n="68.7">fuis</w> <w n="68.8">à</w> <w n="68.9">mesure</w> ;</l>
						<l n="69" num="4.18"><w n="69.1">Mais</w>, <w n="69.2">ô</w> <w n="69.3">cime</w> <w n="69.4">orgueilleuse</w>, <w n="69.5">il</w> <w n="69.6">est</w> <w n="69.7">dans</w> <w n="69.8">ma</w> <w n="69.9">nature</w></l>
						<l n="70" num="4.19"><w n="70.1">Un</w> <w n="70.2">pouvoir</w> <w n="70.3">en</w> <w n="70.4">réserve</w>, <w n="70.5">et</w> <w n="70.6">c</w>’<w n="70.7">est</w> <w n="70.8">la</w> <w n="70.9">volonté</w> ! »</l>
					</lg>
					<lg n="5">
						<l n="71" num="5.1"><w n="71.1">La</w> <w n="71.2">dure</w> <w n="71.3">ascension</w> <w n="71.4">de</w> <w n="71.5">nouveau</w> <w n="71.6">recommence</w> :</l>
						<l n="72" num="5.2"><w n="72.1">Je</w> <w n="72.2">grimpe</w> <w n="72.3">de</w> <w n="72.4">biais</w> <w n="72.5">le</w> <w n="72.6">long</w> <w n="72.7">du</w> <w n="72.8">flanc</w> <w n="72.9">immense</w>,</l>
						<l n="73" num="5.3"><w n="73.1">Harassé</w>, <w n="73.2">haletant</w>, <w n="73.3">et</w> <w n="73.4">m</w>’<w n="73.5">aidant</w> <w n="73.6">de</w> <w n="73.7">mes</w> <w n="73.8">bras</w></l>
						<l n="74" num="5.4"><w n="74.1">Quand</w> <w n="74.2">d</w>’<w n="74.3">un</w> <w n="74.4">plan</w> <w n="74.5">vertical</w> <w n="74.6">j</w>’<w n="74.7">entreprends</w> <w n="74.8">l</w>’<w n="74.9">escalade</w>,</l>
						<l n="75" num="5.5"><w n="75.1">Ou</w> <w n="75.2">que</w> <w n="75.3">des</w> <w n="75.4">arbres</w> <w n="75.5">morts</w> <w n="75.6">l</w>’<w n="75.7">inextricable</w> <w n="75.8">amas</w></l>
						<l n="76" num="5.6"><w n="76.1">Se</w> <w n="76.2">dresse</w> <w n="76.3">devant</w> <w n="76.4">moi</w> <w n="76.5">comme</w> <w n="76.6">une</w> <w n="76.7">barricade</w>.</l>
					</lg>
					<lg n="6">
						<l n="77" num="6.1"><w n="77.1">Partout</w>, <w n="77.2">le</w> <w n="77.3">blanc</w> <w n="77.4">bouleau</w>, <w n="77.5">le</w> <w n="77.6">tremble</w>, <w n="77.7">le</w> <w n="77.8">sapin</w>,</l>
						<l n="78" num="6.2"><w n="78.1">Et</w> <w n="78.2">l</w>’<w n="78.3">érable</w> <w n="78.4">sacré</w>, <w n="78.5">le</w> <w n="78.6">hêtre</w>, <w n="78.7">l</w>’<w n="78.8">épinette</w>,</l>
						<l n="79" num="6.3"><w n="79.1">Et</w> <w n="79.2">le</w> <w n="79.3">vieux</w> <w n="79.4">chêne</w> <w n="79.5">aussi</w> <w n="79.6">mêlent</w> <w n="79.7">leur</w> <w n="79.8">silhouette</w></l>
						<l n="80" num="6.4"><w n="80.1">Que</w>, <w n="80.2">çà</w> <w n="80.3">et</w> <w n="80.4">là</w>, <w n="80.5">domine</w> <w n="80.6">un</w> <w n="80.7">gigantesque</w> <w n="80.8">pin</w>…</l>
					</lg>
					<lg n="7">
						<l n="81" num="7.1"><w n="81.1">Le</w> <w n="81.2">soleil</w> <w n="81.3">flamboyant</w> <w n="81.4">vers</w> <w n="81.5">l</w>’<w n="81.6">horizon</w> <w n="81.7">s</w>’<w n="81.8">incline</w> ;</l>
						<l n="82" num="7.2"><w n="82.1">Voici</w> <w n="82.2">bientôt</w> <w n="82.3">venir</w> <w n="82.4">la</w> <w n="82.5">minute</w> <w n="82.6">divine</w></l>
						<l n="83" num="7.3"><w n="83.1">Où</w> <w n="83.2">tout</w> <w n="83.3">va</w> <w n="83.4">se</w> <w n="83.5">parer</w> <w n="83.6">de</w> <w n="83.7">son</w> <w n="83.8">poudroiement</w> <w n="83.9">d</w>’<w n="83.10">or</w>.</l>
					</lg>
					<lg n="8">
						<l n="84" num="8.1"><w n="84.1">Tout</w> <w n="84.2">se</w> <w n="84.3">tait</w> <w n="84.4">dans</w> <w n="84.5">les</w> <w n="84.6">cieux</w>. <w n="84.7">J</w>’<w n="84.8">approche</w> <w n="84.9">de</w> <w n="84.10">la</w> <w n="84.11">cime</w>,</l>
						<l n="85" num="8.2"><w n="85.1">Et</w> <w n="85.2">mes</w> <w n="85.3">pas</w>, <w n="85.4">les</w> <w n="85.5">premiers</w>, <w n="85.6">foulent</w> <w n="85.7">ce</w> <w n="85.8">lieu</w> <w n="85.9">sublime</w> !</l>
						<l n="86" num="8.3"><w n="86.1">Deux</w> <w n="86.2">mamelons</w> <w n="86.3">boisés</w> <w n="86.4">m</w>’<w n="86.5">en</w> <w n="86.6">séparent</w> <w n="86.7">encor</w> :</l>
						<l n="87" num="8.4"><w n="87.1">Je</w> <w n="87.2">vole</w> <w n="87.3">à</w> <w n="87.4">son</w> <w n="87.5">assaut</w> ; <w n="87.6">enfin</w>, <w n="87.7">je</w> <w n="87.8">vais</w> <w n="87.9">l</w>’<w n="87.10">atteindre</w> !…</l>
						<l n="88" num="8.5"><w n="88.1">J</w>’<w n="88.2">y</w> <w n="88.3">parviens</w> ! <w n="88.4">Il</w> <w n="88.5">est</w> <w n="88.6">temps</w>, <w n="88.7">car</w> <w n="88.8">le</w> <w n="88.9">jour</w> <w n="88.10">va</w> <w n="88.11">s</w>’<w n="88.12">éteindre</w>.</l>
						<l n="89" num="8.6"><w n="89.1">Mais</w> <w n="89.2">autour</w> <w n="89.3">du</w> <w n="89.4">sommet</w> <w n="89.5">se</w> <w n="89.6">dresse</w> <w n="89.7">un</w> <w n="89.8">vert</w> <w n="89.9">rempart</w> :</l>
						<l n="90" num="8.7"><w n="90.1">La</w> <w n="90.2">couronne</w> <w n="90.3">des</w> <w n="90.4">pins</w>, <w n="90.5">des</w> <w n="90.6">cèdres</w> <w n="90.7">et</w> <w n="90.8">des</w> <w n="90.9">ormes</w>,</l>
						<l n="91" num="8.8"><w n="91.1">À</w> <w n="91.2">ses</w> <w n="91.3">fleurons</w> <w n="91.4">altiers</w> <w n="91.5">arrête</w> <w n="91.6">mon</w> <w n="91.7">regard</w>.</l>
					</lg>
					<lg n="9">
						<l n="92" num="9.1"><w n="92.1">Sur</w> <w n="92.2">le</w> <w n="92.3">granit</w> <w n="92.4">poli</w> <w n="92.5">des</w> <w n="92.6">chauves</w> <w n="92.7">plate</w>-<w n="92.8">formes</w>,</l>
						<l n="93" num="9.2"><w n="93.1">Par</w> <w n="93.2">mon</w> <w n="93.3">ombre</w> <w n="93.4">vers</w> <w n="93.5">l</w>’<w n="93.6">Est</w> <w n="93.7">loin</w> <w n="93.8">de</w> <w n="93.9">moi</w> <w n="93.10">précédé</w>,</l>
						<l n="94" num="9.3"><w n="94.1">Je</w> <w n="94.2">cours</w> <w n="94.3">vers</w> <w n="94.4">un</w> <w n="94.5">plateau</w> <w n="94.6">rugueux</w> <w n="94.7">et</w> <w n="94.8">dénudé</w></l>
						<l n="95" num="9.4"><w n="95.1">D</w>’<w n="95.2">où</w> <w n="95.3">rien</w> <w n="95.4">ne</w> <w n="95.5">rétrécit</w> <w n="95.6">le</w> <w n="95.7">solennel</w> <w n="95.8">espace</w>.</l>
						<l n="96" num="9.5"><w n="96.1">Le</w> <w n="96.2">vaste</w> <w n="96.3">écartement</w> <w n="96.4">de</w> <w n="96.5">l</w>’<w n="96.6">angle</w> <w n="96.7">que</w> <w n="96.8">j</w>’<w n="96.9">embrasse</w></l>
						<l n="97" num="9.6"><w n="97.1">Entraîne</w> <w n="97.2">ma</w> <w n="97.3">pensée</w> <w n="97.4">au</w> <w n="97.5">seuil</w> <w n="97.6">de</w> <w n="97.7">l</w>’<w n="97.8">Infini</w>.</l>
					</lg>
					<lg n="10">
						<l n="98" num="10.1"><w n="98.1">Sous</w> <w n="98.2">les</w> <w n="98.3">rayons</w> <w n="98.4">dorés</w>, <w n="98.5">les</w> <w n="98.6">montagnes</w> <w n="98.7">sereines</w></l>
						<l n="99" num="10.2"><w n="99.1">Jusques</w> <w n="99.2">à</w> <w n="99.3">l</w>’<w n="99.4">horizon</w> <w n="99.5">développent</w> <w n="99.6">leurs</w> <w n="99.7">chaînes</w></l>
						<l n="100" num="10.3"><w n="100.1">Dont</w> <w n="100.2">l</w>’<w n="100.3">orgueilleux</w> <w n="100.4">profil</w> <w n="100.5">enfin</w> <w n="100.6">s</w>’<w n="100.7">est</w> <w n="100.8">aplani</w>,</l>
						<l n="101" num="10.4"><w n="101.1">Et</w>, <w n="101.2">ruban</w> <w n="101.3">satiné</w>, <w n="101.4">s</w>’<w n="101.5">allonge</w> <w n="101.6">sous</w> <w n="101.7">la</w> <w n="101.8">nue</w>,</l>
						<l n="102" num="10.5"><w n="102.1">Comme</w> <w n="102.2">pour</w> <w n="102.3">défiler</w>, <w n="102.4">au</w> <w n="102.5">fond</w> <w n="102.6">de</w> <w n="102.7">l</w>’<w n="102.8">étendue</w>,</l>
						<l n="103" num="10.6"><w n="103.1">Devant</w> <w n="103.2">le</w> <w n="103.3">sceptre</w> <w n="103.4">d</w>’<w n="103.5">or</w> <w n="103.6">de</w> <w n="103.7">quelque</w> <w n="103.8">majesté</w></l>
						<l n="104" num="10.7"><w n="104.1">Régnant</w> <w n="104.2">sur</w> <w n="104.3">la</w> <w n="104.4">lumière</w> <w n="104.5">et</w> <w n="104.6">sur</w> <w n="104.7">l</w>’<w n="104.8">immensité</w>.</l>
					</lg>
					<lg n="11">
						<l n="105" num="11.1"><w n="105.1">Serait</w>-<w n="105.2">ce</w> <w n="105.3">une</w> <w n="105.4">féerique</w> <w n="105.5">illusion</w> <w n="105.6">des</w> <w n="105.7">choses</w> ?</l>
						<l n="106" num="11.2"><w n="106.1">Ou</w> <w n="106.2">bien</w>, <w n="106.3">dans</w> <w n="106.4">le</w> <w n="106.5">recul</w> <w n="106.6">des</w> <w n="106.7">solitudes</w> <w n="106.8">roses</w>,</l>
						<l n="107" num="11.3"><w n="107.1">Par</w> <w n="107.2">delà</w> <w n="107.3">l</w>’<w n="107.4">Océan</w> <w n="107.5">des</w> <w n="107.6">monts</w> <w n="107.7">échelonnés</w>,</l>
						<l n="108" num="11.4"><w n="108.1">Les</w> <w n="108.2">sommets</w> <w n="108.3">glorieux</w> <w n="108.4">se</w> <w n="108.5">sont</w>-<w n="108.6">ils</w> <w n="108.7">prosternés</w> ?</l>
						<l n="109" num="11.5"><w n="109.1">Devant</w> <w n="109.2">tant</w> <w n="109.3">de</w> <w n="109.4">grandeur</w>, <w n="109.5">la</w> <w n="109.6">main</w> <w n="109.7">de</w> <w n="109.8">Dieu</w> <w n="109.9">m</w>’<w n="109.10">écrase</w>.</l>
						<l n="110" num="11.6"><w n="110.1">J</w>’<w n="110.2">entre</w> <w n="110.3">en</w> <w n="110.4">communion</w> <w n="110.5">dans</w> <w n="110.6">cet</w> <w n="110.7">immense</w> <w n="110.8">amour</w></l>
						<l n="111" num="11.7"><w n="111.1">Qui</w> <w n="111.2">monte</w> <w n="111.3">de</w> <w n="111.4">la</w> <w n="111.5">terre</w> <w n="111.6">au</w> <w n="111.7">soleil</w> <w n="111.8">qui</w> <w n="111.9">l</w>’<w n="111.10">embrase</w>.</l>
						<l n="112" num="11.8"><w n="112.1">Je</w> <w n="112.2">suis</w> <w n="112.3">pris</w> <w n="112.4">du</w> <w n="112.5">vertige</w> <w n="112.6">où</w> <w n="112.7">défaille</w> <w n="112.8">le</w> <w n="112.9">jour</w> ;</l>
						<l n="113" num="11.9"><w n="113.1">J</w>’<w n="113.2">éprouve</w> <w n="113.3">la</w> <w n="113.4">splendeur</w> <w n="113.5">de</w> <w n="113.6">sa</w> <w n="113.7">brève</w> <w n="113.8">agonie</w>.</l>
						<l n="114" num="11.10"><w n="114.1">Parmi</w> <w n="114.2">les</w> <w n="114.3">frissons</w> <w n="114.4">d</w>’<w n="114.5">or</w> <w n="114.6">de</w> <w n="114.7">la</w> <w n="114.8">limpidité</w>,</l>
						<l n="115" num="11.11"><w n="115.1">Mes</w> <w n="115.2">sens</w> <w n="115.3">extasiés</w> <w n="115.4">vibrent</w> <w n="115.5">en</w> <w n="115.6">harmonie</w></l>
						<l n="116" num="11.12"><w n="116.1">Avec</w> <w n="116.2">la</w> <w n="116.3">chatoyante</w> <w n="116.4">et</w> <w n="116.5">magique</w> <w n="116.6">beauté</w></l>
						<l n="117" num="11.13"><w n="117.1">De</w> <w n="117.2">tout</w> <w n="117.3">ce</w> <w n="117.4">que</w> <w n="117.5">le</w> <w n="117.6">cœur</w> <w n="117.7">par</w> <w n="117.8">les</w> <w n="117.9">yeux</w> <w n="117.10">peut</w> <w n="117.11">comprendre</w> !</l>
						<l n="118" num="11.14"><w n="118.1">Et</w> <w n="118.2">comme</w> <w n="118.3">sur</w> <w n="118.4">le</w> <w n="118.5">monde</w> <w n="118.6">où</w> <w n="118.7">la</w> <w n="118.8">nuit</w> <w n="118.9">va</w> <w n="118.10">descendre</w>,</l>
						<l n="119" num="11.15"><w n="119.1">Dans</w> <w n="119.2">mon</w> <w n="119.3">être</w> <w n="119.4">attendri</w> <w n="119.5">passe</w> <w n="119.6">un</w> <w n="119.7">tressaillement</w>.</l>
						<l n="120" num="11.16"><w n="120.1">Aux</w> <w n="120.2">suprêmes</w> <w n="120.3">rayons</w> <w n="120.4">de</w> <w n="120.5">la</w> <w n="120.6">mourante</w> <w n="120.7">flamme</w></l>
						<l n="121" num="11.17"><w n="121.1">En</w> <w n="121.2">moi</w> <w n="121.3">je</w> <w n="121.4">sens</w> <w n="121.5">pâlir</w> <w n="121.6">la</w> <w n="121.7">lumière</w> <w n="121.8">de</w> <w n="121.9">l</w>’<w n="121.10">âme</w>,</l>
						<l n="122" num="11.18"><w n="122.1">Et</w> <w n="122.2">je</w> <w n="122.3">tombe</w> <w n="122.4">à</w> <w n="122.5">genoux</w> <w n="122.6">près</w> <w n="122.7">de</w> <w n="122.8">l</w>’<w n="122.9">escarpement</w>.</l>
					</lg>
				</div></body></text></TEI>