<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉTOILES FILANTES</head><div type="poem" key="GIL32">
					<head type="main">La Conférence interrompue</head>
					<opener>
						<salute>A Marcel Dugas.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Avant</w> <w n="1.2">que</w> <w n="1.3">la</w> <w n="1.4">sublime</w> <w n="1.5">aurore</w> <w n="1.6">de</w> <w n="1.7">l</w>’<w n="1.8">histoire</w></l>
						<l n="2" num="1.2"><w n="2.1">Auréole</w> <w n="2.2">leurs</w> <w n="2.3">fronts</w> <w n="2.4">par</w> <w n="2.5">la</w> <w n="2.6">Muse</w> <w n="2.7">ennoblis</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Nos</w> <w n="3.2">aëdes</w> <w n="3.3">en</w> <w n="3.4">vain</w> <w n="3.5">luttent</w> <w n="3.6">dans</w> <w n="3.7">la</w> <w n="3.8">nuit</w> <w n="3.9">noire</w></l>
						<l n="4" num="1.4"><w n="4.1">Dont</w> <w n="4.2">le</w> <w n="4.3">morne</w> <w n="4.4">linceul</w> <w n="4.5">les</w> <w n="4.6">couvre</w> <w n="4.7">de</w> <w n="4.8">ses</w> <w n="4.9">plis</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Merci</w> <w n="5.2">d</w>’<w n="5.3">avoir</w>, <w n="5.4">au</w> <w n="5.5">seuil</w> <w n="5.6">des</w> <w n="5.7">injustes</w> <w n="5.8">oublis</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Pieusement</w> <w n="6.2">tressé</w> <w n="6.3">pour</w> <w n="6.4">honorer</w> <w n="6.5">leur</w> <w n="6.6">gloire</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Le</w> <w n="7.2">laurier</w> <w n="7.3">solennel</w>, <w n="7.4">les</w> <w n="7.5">roses</w> <w n="7.6">et</w> <w n="7.7">les</w> <w n="7.8">lys</w></l>
						<l n="8" num="2.4"><w n="8.1">Sur</w> <w n="8.2">l</w>’<w n="8.3">emblème</w> <w n="8.4">sacré</w> <w n="8.5">de</w> <w n="8.6">la</w> <w n="8.7">lyre</w> <w n="8.8">d</w>’<w n="8.9">ivoire</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Bon</w> <w n="9.2">jardinier</w> <w n="9.3">d</w>’<w n="9.4">Athène</w>, <w n="9.5">avec</w> <w n="9.6">ces</w> <w n="9.7">rares</w> <w n="9.8">fleurs</w></l>
						<l n="10" num="3.2"><w n="10.1">Vous</w> <w n="10.2">tendiez</w> <w n="10.3">en</w> <w n="10.4">hommage</w> <w n="10.5">aux</w> <w n="10.6">discrètes</w> <w n="10.7">douleurs</w></l>
						<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">douce</w> <w n="11.3">pâquerette</w> <w n="11.4">et</w> <w n="11.5">la</w> <w n="11.6">divine</w> <w n="11.7">sauge</w>…</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Mais</w> <w n="12.2">voilà</w> <w n="12.3">que</w>, <w n="12.4">grognant</w>, <w n="12.5">s</w>’<w n="12.6">éveillèrent</w> <w n="12.7">soudain</w></l>
						<l n="13" num="4.2"><w n="13.1">Ceux</w> <w n="13.2">qui</w> <w n="13.3">dorment</w> <w n="13.4">si</w> <w n="13.5">mal</w> <w n="13.6">au</w> <w n="13.7">fond</w> <w n="13.8">du</w> <w n="13.9">cœur</w> <w n="13.10">humain</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Car</w> <w n="14.2">vous</w> <w n="14.3">aviez</w> <w n="14.4">jeté</w> <w n="14.5">des</w> <w n="14.6">perles</w> <w n="14.7">dans</w> <w n="14.8">leur</w> <w n="14.9">auge</w>.</l>
					</lg>
				</div></body></text></TEI>