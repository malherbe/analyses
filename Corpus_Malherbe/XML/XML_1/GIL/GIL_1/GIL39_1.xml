<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉTOILES FILANTES</head><div type="poem" key="GIL39">
					<head type="main">Mortuae, Moriturus</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Éternel</w> <w n="1.2">souvenir</w> <w n="1.3">d</w>’<w n="1.4">une</w> <w n="1.5">époque</w> <w n="1.6">trop</w> <w n="1.7">brève</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">m</w>’<w n="2.3">as</w> <w n="2.4">bien</w> <w n="2.5">fait</w> <w n="2.6">pleurer</w> ! — <w n="2.7">Au</w> <w n="2.8">bord</w> <w n="2.9">du</w> <w n="2.10">lac</w> <w n="2.11">dormant</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Jouvenceaux</w>, <w n="3.2">nous</w> <w n="3.3">avions</w>, <w n="3.4">dans</w> <w n="3.5">l</w>’<w n="3.6">ivresse</w> <w n="3.7">du</w> <w n="3.8">rêve</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Engagé</w> <w n="4.2">nos</w> <w n="4.3">deux</w> <w n="4.4">cœurs</w> <w n="4.5">par</w> <w n="4.6">un</w> <w n="4.7">même</w> <w n="4.8">serment</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Mais</w> <w n="5.2">la</w> <w n="5.3">Mort</w> <w n="5.4">a</w> <w n="5.5">tué</w> <w n="5.6">le</w> <w n="5.7">fol</w> <w n="5.8">espoir</w> <w n="5.9">qui</w> <w n="5.10">ment</w> —</l>
						<l n="6" num="2.2"><w n="6.1">Elle</w> <w n="6.2">a</w> <w n="6.3">signé</w> <w n="6.4">pour</w> <w n="6.5">nous</w> <w n="6.6">l</w>’<w n="6.7">irrévocable</w> <w n="6.8">trêve</w></l>
						<l n="7" num="2.3"><w n="7.1">Sans</w> <w n="7.2">pouvoir</w> <w n="7.3">conjurer</w> <w n="7.4">ton</w> <w n="7.5">doux</w> <w n="7.6">enchantement</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Ô</w> <w n="8.2">vainqueur</w> <w n="8.3">de</w> <w n="8.4">la</w> <w n="8.5">tombe</w>, <w n="8.6">amour</w> <w n="8.7">que</w> <w n="8.8">rien</w> <w n="8.9">n</w>’<w n="8.10">enlève</w> !…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Un</w> <w n="9.2">serment</w> <w n="9.3">fait</w> <w n="9.4">par</w> <w n="9.5">elle</w> <w n="9.6">et</w> <w n="9.7">lu</w> <w n="9.8">dans</w> <w n="9.9">ses</w> <w n="9.10">grands</w> <w n="9.11">yeux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Va</w> <w n="10.2">plus</w> <w n="10.3">loin</w> <w n="10.4">que</w> <w n="10.5">la</w> <w n="10.6">vie</w> <w n="10.7">et</w> <w n="10.8">que</w> <w n="10.9">le</w> <w n="10.10">cimetière</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">sonna</w> <w n="11.3">donc</w> <w n="11.4">en</w> <w n="11.5">vain</w>, <w n="11.6">le</w> <w n="11.7">glas</w> <w n="11.8">de</w> <w n="11.9">nos</w> <w n="11.10">adieux</w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Éva</w> ! <w n="12.2">pour</w> <w n="12.3">que</w> <w n="12.4">mon</w> <w n="12.5">âme</w>, <w n="12.6">au</w> <w n="12.7">réveil</w> <w n="12.8">de</w> <w n="12.9">lumière</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Ne</w> <w n="13.2">fasse</w> <w n="13.3">pas</w> <w n="13.4">rougir</w> <w n="13.5">votre</w> <w n="13.6">front</w> <w n="13.7">radieux</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Souvenez</w>-<w n="14.2">vous</w> ! <w n="14.3">priez</w>, <w n="14.4">bel</w> <w n="14.5">ange</w>, <w n="14.6">dans</w> <w n="14.7">les</w> <w n="14.8">cieux</w> !</l>
					</lg>
				</div></body></text></TEI>