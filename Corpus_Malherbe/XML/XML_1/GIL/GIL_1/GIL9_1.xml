<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CAP ÉTERNITÉ</head><div type="poem" key="GIL9">
					<head type="number">Chant VIII</head>
					<head type="main">Le Cap Trinité</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ce</w> <w n="1.2">rocher</w> <w n="1.3">qui</w> <w n="1.4">de</w> <w n="1.5">Dieu</w> <w n="1.6">montre</w> <w n="1.7">la</w> <w n="1.8">majesté</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">dresse</w> <w n="2.3">sur</w> <w n="2.4">le</w> <w n="2.5">ciel</w> <w n="2.6">ses</w> <w n="2.7">trois</w> <w n="2.8">gradins</w> <w n="2.9">énormes</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">verticalement</w> <w n="3.3">divise</w> <w n="3.4">en</w> <w n="3.5">trois</w> <w n="3.6">ses</w> <w n="3.7">formes</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">mérite</w> <w n="4.3">trois</w> <w n="4.4">fois</w> <w n="4.5">son</w> <w n="4.6">nom</w> <w n="4.7">de</w> <w n="4.8">Trinité</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Son</w> <w n="5.2">flanc</w> <w n="5.3">vertigineux</w>, <w n="5.4">creusé</w> <w n="5.5">de</w> <w n="5.6">cicatrices</w></l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">plein</w> <w n="6.3">d</w>’<w n="6.4">âpres</w> <w n="6.5">reliefs</w> <w n="6.6">qu</w>’<w n="6.7">effleure</w> <w n="6.8">le</w> <w n="6.9">soleil</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Aux</w> <w n="7.2">grimoires</w> <w n="7.3">sacrés</w> <w n="7.4">de</w> <w n="7.5">l</w>’<w n="7.6">Égypte</w> <w n="7.7">est</w> <w n="7.8">pareil</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Quand</w> <w n="8.2">l</w>’<w n="8.3">ombre</w> <w n="8.4">et</w> <w n="8.5">la</w> <w n="8.6">lumière</w> <w n="8.7">y</w> <w n="8.8">mêlent</w> <w n="8.9">leurs</w> <w n="8.10">caprices</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">bruns</w>, <w n="9.3">les</w> <w n="9.4">gris</w>, <w n="9.5">les</w> <w n="9.6">ors</w>, <w n="9.7">les</w> <w n="9.8">tendres</w> <w n="9.9">violets</w>,</l>
						<l n="10" num="3.2"><w n="10.1">À</w> <w n="10.2">ces</w> <w n="10.3">signes</w> <w n="10.4">précis</w> <w n="10.5">joignent</w> <w n="10.6">des</w> <w n="10.7">traits</w> <w n="10.8">plus</w> <w n="10.9">vagues</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">le</w> <w n="11.3">céleste</w> <w n="11.4">azur</w> <w n="11.5">y</w> <w n="11.6">flotte</w> <w n="11.7">au</w> <w n="11.8">gré</w> <w n="11.9">des</w> <w n="11.10">vagues</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">dans</w> <w n="12.3">les</w> <w n="12.4">plis</w> <w n="12.5">profonds</w> <w n="12.6">dardent</w> <w n="12.7">leurs</w> <w n="12.8">gais</w> <w n="12.9">reflets</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Est</w>-<w n="13.2">ce</w> <w n="13.3">quelque</w> <w n="13.4">Titan</w>, <w n="13.5">est</w>-<w n="13.6">ce</w> <w n="13.7">plutôt</w> <w n="13.8">la</w> <w n="13.9">foudre</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Qui</w> <w n="14.2">voulut</w> <w n="14.3">imprimer</w> <w n="14.4">ici</w> <w n="14.5">le</w> <w n="14.6">mot</w> « <w n="14.7">toujours</w> » ?</l>
						<l n="15" num="4.3"><w n="15.1">Quels</w> <w n="15.2">sens</w> <w n="15.3">recèlent</w> <w n="15.4">donc</w> <w n="15.5">ces</w> <w n="15.6">étranges</w> <w n="15.7">contours</w> ?</l>
						<l n="16" num="4.4"><w n="16.1">Pour</w> <w n="16.2">la</w> <w n="16.3">postérité</w> <w n="16.4">quel</w> <w n="16.5">problème</w> <w n="16.6">à</w> <w n="16.7">résoudre</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ô</w> <w n="17.2">Cap</w> ! <w n="17.3">en</w> <w n="17.4">confiant</w> <w n="17.5">au</w> <w n="17.6">vertige</w> <w n="17.7">des</w> <w n="17.8">cieux</w></l>
						<l n="18" num="5.2"><w n="18.1">Notre</w> <w n="18.2">globe</w> <w n="18.3">éperdu</w> <w n="18.4">dans</w> <w n="18.5">la</w> <w n="18.6">nuit</w> <w n="18.7">séculaire</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">Seigneur</w> <w n="19.3">s</w>’<w n="19.4">est</w> <w n="19.5">penché</w> <w n="19.6">sur</w> <w n="19.7">ta</w> <w n="19.8">page</w> <w n="19.9">de</w> <w n="19.10">pierre</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Digne</w> <w n="20.2">de</w> <w n="20.3">relater</w> <w n="20.4">des</w> <w n="20.5">faits</w> <w n="20.6">prodigieux</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Il</w> <w n="21.2">a</w> <w n="21.3">mis</w> <w n="21.4">sur</w> <w n="21.5">ton</w> <w n="21.6">front</w> <w n="21.7">l</w>’<w n="21.8">obscur</w> <w n="21.9">secret</w> <w n="21.10">des</w> <w n="21.11">causes</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Les</w> <w n="22.2">lois</w> <w n="22.3">de</w> <w n="22.4">la</w> <w n="22.5">nature</w> <w n="22.6">et</w> <w n="22.7">ses</w> <w n="22.8">frémissements</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Pendant</w> <w n="23.2">qu</w>’<w n="23.3">elle</w> <w n="23.4">assignait</w> <w n="23.5">leur</w> <w n="23.6">forme</w> <w n="23.7">aux</w> <w n="23.8">éléments</w></l>
						<l n="24" num="6.4"><w n="24.1">Dans</w> <w n="24.2">l</w>’<w n="24.3">infini</w> <w n="24.4">creuset</w> <w n="24.5">de</w> <w n="24.6">ses</w> <w n="24.7">métamorphoses</w> ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Et</w>, <w n="25.2">scellant</w> <w n="25.3">à</w> <w n="25.4">jamais</w> <w n="25.5">les</w> <w n="25.6">arrêts</w> <w n="25.7">du</w> <w n="25.8">destin</w></l>
						<l n="26" num="7.2"><w n="26.1">Avec</w> <w n="26.2">l</w>’<w n="26.3">ardent</w> <w n="26.4">burin</w> <w n="26.5">de</w> <w n="26.6">la</w> <w n="26.7">foudre</w> <w n="26.8">qui</w> <w n="26.9">gronde</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Il</w> <w n="27.2">a</w>, <w n="27.3">dans</w> <w n="27.4">ton</w> <w n="27.5">granit</w>, <w n="27.6">gravé</w> <w n="27.7">le</w> <w n="27.8">sort</w> <w n="27.9">du</w> <w n="27.10">monde</w>,</l>
						<l n="28" num="7.4"><w n="28.1">En</w> <w n="28.2">symboles</w> <w n="28.3">trop</w> <w n="28.4">grands</w> <w n="28.5">pour</w> <w n="28.6">le</w> <w n="28.7">génie</w> <w n="28.8">humain</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">En</w> <w n="29.2">signes</w> <w n="29.3">trop</w> <w n="29.4">profonds</w>, <w n="29.5">pour</w> <w n="29.6">que</w> <w n="29.7">notre</w> <w n="29.8">œil</w> <w n="29.9">pénètre</w></l>
						<l n="30" num="8.2"><w n="30.1">La</w> <w n="30.2">simple</w> <w n="30.3">vérité</w> <w n="30.4">des</w> <w n="30.5">terrestres</w> <w n="30.6">secrets</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Pendant</w> <w n="31.2">que</w> <w n="31.3">nous</w> <w n="31.4">osons</w> <w n="31.5">forger</w> <w n="31.6">des</w> <w n="31.7">mots</w> <w n="31.8">abstraits</w></l>
						<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">sonder</w> <w n="32.3">le</w> <w n="32.4">mystère</w> <w n="32.5">insondable</w> <w n="32.6">de</w> <w n="32.7">l</w>’<w n="32.8">être</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">La</w> <w n="33.2">Nature</w> <w n="33.3">nous</w> <w n="33.4">parle</w> <w n="33.5">et</w> <w n="33.6">nous</w> <w n="33.7">l</w>’<w n="33.8">interrompons</w> !</l>
						<l n="34" num="9.2"><w n="34.1">Aveugles</w> <w n="34.2">aux</w> <w n="34.3">rayons</w> <w n="34.4">de</w> <w n="34.5">la</w> <w n="34.6">sainte</w> <w n="34.7">lumière</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Sourds</w> <w n="35.2">aux</w> <w n="35.3">enseignements</w> <w n="35.4">antiques</w> <w n="35.5">de</w> <w n="35.6">la</w> <w n="35.7">terre</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Nous</w> <w n="36.2">ne</w> <w n="36.3">connaissons</w> <w n="36.4">pas</w> <w n="36.5">le</w> <w n="36.6">sol</w> <w n="36.7">où</w> <w n="36.8">nous</w> <w n="36.9">rampons</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Nous</w> <w n="37.2">n</w>’<w n="37.3">avons</w> <w n="37.4">pas</w> <w n="37.5">assez</w> <w n="37.6">contemplé</w> <w n="37.7">les</w> <w n="37.8">aurores</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Nous</w> <w n="38.2">n</w>’<w n="38.3">avons</w> <w n="38.4">pas</w> <w n="38.5">assez</w> <w n="38.6">frémi</w> <w n="38.7">devant</w> <w n="38.8">la</w> <w n="38.9">nuit</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Mornes</w> <w n="39.2">vivants</w> <w n="39.3">dont</w> <w n="39.4">l</w>’<w n="39.5">âme</w> <w n="39.6">est</w> <w n="39.7">en</w> <w n="39.8">proie</w> <w n="39.9">au</w> <w n="39.10">vain</w> <w n="39.11">bruit</w></l>
						<l n="40" num="10.4"><w n="40.1">Des</w> <w n="40.2">savantes</w> <w n="40.3">erreurs</w> <w n="40.4">et</w> <w n="40.5">des</w> <w n="40.6">longs</w> <w n="40.7">mots</w> <w n="40.8">sonores</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">En</w> <w n="41.2">vain</w> <w n="41.3">la</w> <w n="41.4">Vérité</w> <w n="41.5">s</w>’<w n="41.6">offre</w> <w n="41.7">à</w> <w n="41.8">notre</w> <w n="41.9">compas</w></l>
						<l n="42" num="11.2"><w n="42.1">Et</w> <w n="42.2">la</w> <w n="42.3">Création</w> <w n="42.4">ouvre</w> <w n="42.5">pour</w> <w n="42.6">nous</w> <w n="42.7">son</w> <w n="42.8">livre</w> :</l>
						<l n="43" num="11.3"><w n="43.1">Avides</w> <w n="43.2">des</w> <w n="43.3">secrets</w> <w n="43.4">radieux</w> <w n="43.5">qu</w>’<w n="43.6">il</w> <w n="43.7">nous</w> <w n="43.8">livre</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Nous</w> <w n="44.2">les</w> <w n="44.3">cherchons</w> <w n="44.4">ailleurs</w> <w n="44.5">et</w> <w n="44.6">ne</w> <w n="44.7">les</w> <w n="44.8">trouvons</w> <w n="44.9">pas</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Nous</w> <w n="45.2">n</w>’<w n="45.3">avons</w> <w n="45.4">pas</w> <w n="45.5">appris</w> <w n="45.6">le</w> <w n="45.7">langage</w> <w n="45.8">des</w> <w n="45.9">cimes</w> :</l>
						<l n="46" num="12.2"><w n="46.1">Nous</w> <w n="46.2">ne</w> <w n="46.3">comprenons</w> <w n="46.4">pas</w> <w n="46.5">ce</w> <w n="46.6">que</w> <w n="46.7">clament</w> <w n="46.8">leur</w> <w n="46.9">voix</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Quand</w> <w n="47.2">les</w> <w n="47.3">cris</w> <w n="47.4">de</w> <w n="47.5">l</w>’<w n="47.6">enfer</w> <w n="47.7">et</w> <w n="47.8">du</w> <w n="47.9">ciel</w> <w n="47.10">à</w> <w n="47.11">la</w> <w n="47.12">fois</w></l>
						<l n="48" num="12.4"><w n="48.1">Semblent</w> <w n="48.2">venir</w> <w n="48.3">à</w> <w n="48.4">nous</w> <w n="48.5">dans</w> <w n="48.6">l</w>’<w n="48.7">écho</w> <w n="48.8">des</w> <w n="48.9">abîmes</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Et</w> <w n="49.2">l</w>’<w n="49.3">ange</w> <w n="49.4">qui</w> <w n="49.5">régit</w> <w n="49.6">l</w>’<w n="49.7">or</w>, <w n="49.8">le</w> <w n="49.9">rose</w> <w n="49.10">et</w> <w n="49.11">le</w> <w n="49.12">bleu</w>.</l>
						<l n="50" num="13.2"><w n="50.1">Pour</w> <w n="50.2">nos</w> <w n="50.3">yeux</w> <w n="50.4">sans</w> <w n="50.5">regard</w> <w n="50.6">n</w>’<w n="50.7">écarte</w> <w n="50.8">pas</w> <w n="50.9">ses</w> <w n="50.10">voiles</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Quand</w> <w n="51.2">le</w> <w n="51.3">roi</w> <w n="51.4">des</w> <w n="51.5">rochers</w> <w n="51.6">et</w> <w n="51.7">le</w> <w n="51.8">roi</w> <w n="51.9">des</w> <w n="51.10">étoiles</w></l>
						<l n="52" num="13.4"><w n="52.1">Nous</w> <w n="52.2">parlent</w> <w n="52.3">à</w> <w n="52.4">midi</w> <w n="52.5">dans</w> <w n="52.6">le</w> <w n="52.7">style</w> <w n="52.8">de</w> <w n="52.9">Dieu</w>.</l>
					</lg>
				</div></body></text></TEI>