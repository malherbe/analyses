<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉTOILES FILANTES</head><div type="poem" key="GIL41">
					<head type="main">Larmes d’en haut</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">portiez</w> <w n="1.3">à</w> <w n="1.4">ce</w> <w n="1.5">bal</w> <w n="1.6">les</w> <w n="1.7">deux</w> <w n="1.8">plus</w> <w n="1.9">belles</w> <w n="1.10">roses</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">les</w> <w n="2.3">entrelaçant</w> <w n="2.4">dans</w> <w n="2.5">l</w>’<w n="2.6">or</w> <w n="2.7">de</w> <w n="2.8">vos</w> <w n="2.9">cheveux</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Naïf</w>, <w n="3.2">je</w> <w n="3.3">leur</w> <w n="3.4">avais</w> <w n="3.5">confié</w> <w n="3.6">les</w> <w n="3.7">aveux</w></l>
						<l n="4" num="1.4"><w n="4.1">Lâchement</w> <w n="4.2">retenus</w> <w n="4.3">entre</w> <w n="4.4">mes</w> <w n="4.5">lèvres</w> <w n="4.6">closes</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Vous</w> <w n="5.2">en</w> <w n="5.3">avez</w> <w n="5.4">flétri</w> <w n="5.5">l</w>’<w n="5.6">éphémère</w> <w n="5.7">splendeur</w></l>
						<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">l</w>’<w n="6.3">étourdissement</w> <w n="6.4">des</w> <w n="6.5">valses</w> <w n="6.6">enivrantes</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">leur</w> <w n="7.3">âme</w> <w n="7.4">a</w> <w n="7.5">mêlé</w> <w n="7.6">ses</w> <w n="7.7">ondes</w> <w n="7.8">odorantes</w></l>
						<l n="8" num="2.4"><w n="8.1">Aux</w> <w n="8.2">sons</w> <w n="8.3">harmonieux</w> <w n="8.4">du</w> <w n="8.5">violon</w> <w n="8.6">rêveur</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">puisque</w>, <w n="9.3">désormais</w>, <w n="9.4">leur</w> <w n="9.5">beauté</w> <w n="9.6">disparue</w></l>
						<l n="10" num="3.2"><w n="10.1">Ne</w> <w n="10.2">pouvait</w> <w n="10.3">à</w> <w n="10.4">la</w> <w n="10.5">vôtre</w> <w n="10.6">ajouter</w> <w n="10.7">d</w>’<w n="10.8">apparat</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">vous</w> <w n="11.3">vis</w> <w n="11.4">les</w> <w n="11.5">livrer</w> <w n="11.6">aux</w> <w n="11.7">hasards</w> <w n="11.8">de</w> <w n="11.9">la</w> <w n="11.10">rue</w></l>
						<l n="12" num="3.4"><w n="12.1">Comme</w> <w n="12.2">un</w> <w n="12.3">vil</w> <w n="12.4">oripeau</w> <w n="12.5">qui</w> <w n="12.6">perdrait</w> <w n="12.7">son</w> <w n="12.8">éclat</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Vous</w> <w n="13.2">n</w>’<w n="13.3">auriez</w> <w n="13.4">pas</w> <w n="13.5">jeté</w> <w n="13.6">du</w> <w n="13.7">rêve</w> <w n="13.8">aux</w> <w n="13.9">gémonies</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Si</w> <w n="14.2">vous</w> <w n="14.3">aviez</w> <w n="14.4">compris</w> <w n="14.5">ces</w> <w n="14.6">messagers</w> <w n="14.7">des</w> <w n="14.8">cœurs</w> !…</l>
						<l n="15" num="4.3"><w n="15.1">Combien</w> <w n="15.2">d</w>’<w n="15.3">illusions</w>, <w n="15.4">à</w> <w n="15.5">tout</w> <w n="15.6">jamais</w> <w n="15.7">bannies</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Roulèrent</w> <w n="16.2">au</w> <w n="16.3">trottoir</w> <w n="16.4">avec</w> <w n="16.5">les</w> <w n="16.6">pauvres</w> <w n="16.7">fleurs</w> !…</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Dès</w> <w n="17.2">qu</w>’<w n="17.3">aux</w> <w n="17.4">premiers</w> <w n="17.5">rayons</w> <w n="17.6">l</w>’<w n="17.7">aurore</w> <w n="17.8">ouvrit</w> <w n="17.9">ses</w> <w n="17.10">portes</w>,</l>
						<l n="18" num="5.2"><w n="18.1">J</w>’<w n="18.2">allai</w> <w n="18.3">les</w> <w n="18.4">recueillir</w> ; <w n="18.5">le</w> <w n="18.6">frimas</w> <w n="18.7">matinal</w></l>
						<l n="19" num="5.3"><w n="19.1">Émaillait</w> <w n="19.2">leurs</w> <w n="19.3">débris</w> <w n="19.4">de</w> <w n="19.5">larmes</w> <w n="19.6">de</w> <w n="19.7">cristal</w> :</l>
						<l n="20" num="5.4"><w n="20.1">La</w> <w n="20.2">nuit</w> <w n="20.3">avait</w> <w n="20.4">pleuré</w> <w n="20.5">sur</w> <w n="20.6">les</w> <w n="20.7">deux</w> <w n="20.8">roses</w> <w n="20.9">mortes</w>.</l>
					</lg>
				</div></body></text></TEI>