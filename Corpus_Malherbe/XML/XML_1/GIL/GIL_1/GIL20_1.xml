<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉTOILES FILANTES</head><div type="poem" key="GIL20">
					<head type="main">L’Aigle</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">cette</w> <w n="1.3">cage</w> <w n="1.4">où</w> <w n="1.5">des</w> <w n="1.6">bourreaux</w> <w n="1.7">l</w>’<w n="1.8">avaient</w> <w n="1.9">jeté</w>,</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">espérance</w> <w n="2.3">faisait</w> <w n="2.4">frémir</w> <w n="2.5">ses</w> <w n="2.6">grandes</w> <w n="2.7">ailes</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">sans</w> <w n="3.3">que</w> <w n="3.4">le</w> <w n="3.5">malheur</w> <w n="3.6">eût</w> <w n="3.7">vaincu</w> <w n="3.8">sa</w> <w n="3.9">fierté</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Son</w> <w n="4.2">regard</w> <w n="4.3">convoitait</w> <w n="4.4">les</w> <w n="4.5">sphères</w> <w n="4.6">éternelles</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">mis</w> <w n="5.3">fin</w> <w n="5.4">à</w> <w n="5.5">l</w>’<w n="5.6">horreur</w> <w n="5.7">de</w> <w n="5.8">sa</w> <w n="5.9">captivité</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Son</w> <w n="6.2">âme</w> <w n="6.3">illumina</w> <w n="6.4">ses</w> <w n="6.5">puissantes</w> <w n="6.6">prunelles</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Quand</w>, <w n="7.2">déployant</w> <w n="7.3">l</w>’<w n="7.4">ampleur</w> <w n="7.5">de</w> <w n="7.6">ses</w> <w n="7.7">formes</w> <w n="7.8">si</w> <w n="7.9">belles</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Il</w> <w n="8.2">monta</w> <w n="8.3">dans</w> <w n="8.4">l</w>’<w n="8.5">azur</w> <w n="8.6">et</w> <w n="8.7">dans</w> <w n="8.8">la</w> <w n="8.9">liberté</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Si</w> <w n="9.2">ton</w> <w n="9.3">cœur</w> <w n="9.4">m</w>’<w n="9.5">a</w> <w n="9.6">gardé</w> <w n="9.7">de</w> <w n="9.8">la</w> <w n="9.9">reconnaissance</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Tu</w> <w n="10.2">peux</w> <w n="10.3">payer</w> <w n="10.4">bien</w> <w n="10.5">cher</w> <w n="10.6">ta</w> <w n="10.7">simple</w> <w n="10.8">délivrance</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Toi</w> <w n="11.2">qui</w> <w n="11.3">fuis</w> <w n="11.4">maintenant</w> <w n="11.5">vers</w> <w n="11.6">les</w> <w n="11.7">astres</w> <w n="11.8">de</w> <w n="11.9">Dieu</w> !</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Conquérant</w> <w n="12.2">de</w> <w n="12.3">l</w>’<w n="12.4">espace</w>, <w n="12.5">emporte</w> <w n="12.6">ma</w> <w n="12.7">mémoire</w> !</l>
						<l n="13" num="4.2"><w n="13.1">Daigne</w> <w n="13.2">m</w>’<w n="13.3">associer</w> <w n="13.4">à</w> <w n="13.5">ton</w> <w n="13.6">immense</w> <w n="13.7">gloire</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Lorsque</w> <w n="14.2">tu</w> <w n="14.3">planeras</w> <w n="14.4">dans</w> <w n="14.5">le</w> <w n="14.6">beau</w> <w n="14.7">pays</w> <w n="14.8">bleu</w> !</l>
					</lg>
				</div></body></text></TEI>