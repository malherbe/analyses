<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES ÉTOILES FILANTES</head><div type="poem" key="GIL36">
					<head type="main">Premier Amour III</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">ils</w> <w n="1.3">coulaient</w> <w n="1.4">heureux</w> <w n="1.5">ces</w> <w n="1.6">beaux</w> <w n="1.7">jours</w> <w n="1.8">d</w>’<w n="1.9">autrefois</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">nous</w> <w n="2.3">nous</w> <w n="2.4">aimions</w> <w n="2.5">avec</w> <w n="2.6">nos</w> <w n="2.7">âmes</w> <w n="2.8">blanches</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">les</w> <w n="3.3">sentiers</w> <w n="3.4">discrets</w> <w n="3.5">émaillés</w> <w n="3.6">de</w> <w n="3.7">pervenches</w></l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">épargnaient</w> <w n="4.3">en</w> <w n="4.4">passant</w> <w n="4.5">ses</w> <w n="4.6">brodequins</w> <w n="4.7">étroits</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Nous</w> <w n="5.2">allions</w> <w n="5.3">écouter</w> <w n="5.4">l</w>’<w n="5.5">harmonieuse</w> <w n="5.6">voix</w></l>
						<l n="6" num="2.2"><w n="6.1">Des</w> <w n="6.2">souffles</w> <w n="6.3">attiédis</w> <w n="6.4">qui</w> <w n="6.5">chantaient</w> <w n="6.6">dans</w> <w n="6.7">les</w> <w n="6.8">branches</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Nous</w> <w n="7.2">mêlions</w> <w n="7.3">au</w> <w n="7.4">murmure</w> <w n="7.5">infini</w> <w n="7.6">des</w> <w n="7.7">grands</w> <w n="7.8">bois</w></l>
						<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">écho</w> <w n="8.3">de</w> <w n="8.4">nos</w> <w n="8.5">serments</w> <w n="8.6">et</w> <w n="8.7">de</w> <w n="8.8">nos</w> <w n="8.9">gaîtés</w> <w n="8.10">franches</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Fervents</w> <w n="9.2">du</w> <w n="9.3">clair</w> <w n="9.4">de</w> <w n="9.5">lune</w> <w n="9.6">et</w> <w n="9.7">des</w> <w n="9.8">soirs</w> <w n="9.9">étoilés</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Nous</w> <w n="10.2">allions</w> <w n="10.3">réveiller</w> <w n="10.4">les</w> <w n="10.5">nénufars</w> <w n="10.6">des</w> <w n="10.7">plages</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Inclinant</w> <w n="11.2">sur</w> <w n="11.3">les</w> <w n="11.4">flots</w> <w n="11.5">leurs</w> <w n="11.6">corps</w> <w n="11.7">immaculés</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">nous</w> <w n="12.3">aimions</w> <w n="12.4">unir</w> <w n="12.5">nos</w> <w n="12.6">riantes</w> <w n="12.7">images</w></l>
						<l n="13" num="4.2"><w n="13.1">Aux</w> <w n="13.2">scintillants</w> <w n="13.3">reflets</w> <w n="13.4">des</w> <w n="13.5">milliers</w> <w n="13.6">d</w>’<w n="13.7">astres</w> <w n="13.8">d</w>’<w n="13.9">or</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Dans</w> <w n="14.2">l</w>’<w n="14.3">immense</w> <w n="14.4">miroir</w> <w n="14.5">du</w> <w n="14.6">Saint</w>-<w n="14.7">Laurent</w> <w n="14.8">qui</w> <w n="14.9">dort</w>.</l>
					</lg>
				</div></body></text></TEI>