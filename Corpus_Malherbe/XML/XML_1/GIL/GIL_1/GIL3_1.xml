<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cap Éternité</title>
				<title type="medium">Édition électronique</title>
				<author key="GIL">
					<name>
						<forename>Charles</forename>
						<surname>GILL</surname>
					</name>
					<date from="1871" to="1918">1871-1918</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2317 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">GIL_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cap Éternité</title>
						<author>Charles Gill</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/charlesgilllcapeeternite.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le Cap Éternité</title>
								<author>Charles Gill</author>
								<idno type="URL">https://archive.org/details/lecapternitpomes00gill</idno>
								<imprint>
									<pubPlace>Montréal</pubPlace>
									<publisher>Édition du devoir</publisher>
									<date when="1919">1919</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1919">1919</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le poème "Premier amour" a été divisé en autant de poèmes qu’il y a de sonnets.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CAP ÉTERNITÉ</head><div type="poem" key="GIL3">
					<head type="number">Chant II</head>
					<head type="main">La Cloche de Tadoussac</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">errais</w> <w n="1.3">seul</w>, <w n="1.4">à</w> <w n="1.5">minuit</w>, <w n="1.6">près</w> <w n="1.7">de</w> <w n="1.8">la</w> <w n="1.9">pauvre</w> <w n="1.10">église</w>.</l>
						<l n="2" num="1.2"><w n="2.1">À</w> <w n="2.2">la</w> <w n="2.3">lueur</w> <w n="2.4">de</w> <w n="2.5">mon</w> <w n="2.6">flambeau</w>, <w n="2.7">je</w> <w n="2.8">pouvais</w> <w n="2.9">voir</w></l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">bords</w> <w n="3.3">de</w> <w n="3.4">l</w>’<w n="3.5">estuaire</w> <w n="3.6">où</w> <w n="3.7">dansait</w> <w n="3.8">le</w> <w n="3.9">flot</w> <w n="3.10">noir</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">le</w> <w n="4.3">petit</w> <w n="4.4">clocher</w> <w n="4.5">que</w> <w n="4.6">le</w> <w n="4.7">temps</w> <w n="4.8">solennise</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Quelle</w> <w n="5.2">nuit</w> ! <w n="5.3">Le</w> <w n="5.4">Surouet</w> <w n="5.5">grondait</w> <w n="5.6">dans</w> <w n="5.7">les</w> <w n="5.8">bouleaux</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Geignait</w> <w n="6.2">le</w> <w n="6.3">long</w> <w n="6.4">des</w> <w n="6.5">murs</w> <w n="6.6">du</w> <w n="6.7">temple</w> <w n="6.8">séculaire</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w>, <w n="7.2">fraternel</w>, <w n="7.3">entre</w> <w n="7.4">les</w> <w n="7.5">croix</w> <w n="7.6">du</w> <w n="7.7">cimetière</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Sur</w> <w n="8.2">les</w> <w n="8.3">tombes</w> <w n="8.4">sans</w> <w n="8.5">nom</w> <w n="8.6">égrenait</w> <w n="8.7">des</w> <w n="8.8">sanglots</w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ô</w> <w n="9.2">fière</w> <w n="9.3">nation</w> <w n="9.4">sur</w> <w n="9.5">qui</w> <w n="9.6">la</w> <w n="9.7">terre</w> <w n="9.8">pèse</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Où</w> <w n="10.2">sont</w> <w n="10.3">tes</w> <w n="10.4">dignes</w> <w n="10.5">chefs</w> <w n="10.6">et</w> <w n="10.7">tes</w> <w n="10.8">guerriers</w> <w n="10.9">sans</w> <w n="10.10">peur</w> ?</l>
						<l n="11" num="3.3"><w n="11.1">Hélas</w> ! <w n="11.2">devant</w> <w n="11.3">ces</w> <w n="11.4">croix</w>, <w n="11.5">le</w> <w n="11.6">pèlerin</w> <w n="11.7">songeur</w></l>
						<l n="12" num="3.4"><w n="12.1">Peut</w> <w n="12.2">se</w> <w n="12.3">dire</w> : — <w n="12.4">Ici</w> <w n="12.5">gît</w> <w n="12.6">la</w> <w n="12.7">race</w> <w n="12.8">Montagnaise</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Elle</w> <w n="13.2">est</w> <w n="13.3">là</w> <w n="13.4">tout</w> <w n="13.5">entière</w> : <w n="13.6">en</w> <w n="13.7">voici</w> <w n="13.8">le</w> <w n="13.9">cercueil</w> !…</l>
						<l n="14" num="4.2"><w n="14.1">C</w>’<w n="14.2">était</w> <w n="14.3">une</w> <w n="14.4">alliée</w> <w n="14.5">à</w> <w n="14.6">la</w> <w n="14.7">France</w> <w n="14.8">fidèle</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Que</w> <w n="15.2">les</w> <w n="15.3">tendres</w> <w n="15.4">bouleaux</w> <w n="15.5">pleurent</w> <w n="15.6">en</w> <w n="15.7">paix</w> <w n="15.8">sur</w> <w n="15.9">elle</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">que</w> <w n="16.3">les</w> <w n="16.4">sapins</w> <w n="16.5">noirs</w> <w n="16.6">portent</w> <w n="16.7">longtemps</w> <w n="16.8">son</w> <w n="16.9">deuil</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« <w n="17.1">Dongne</w> ! <w n="17.2">dongne</w> ! » <w n="17.3">entendit</w> <w n="17.4">mon</w> <w n="17.5">oreille</w> <w n="17.6">inquiète</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Le</w> <w n="18.2">salutaire</w> <w n="18.3">airain</w> <w n="18.4">que</w> <w n="18.5">rien</w> <w n="18.6">ne</w> <w n="18.7">troublait</w> <w n="18.8">plus</w></l>
						<l n="19" num="5.3"><w n="19.1">Dans</w> <w n="19.2">l</w>’<w n="19.3">évocation</w> <w n="19.4">des</w> <w n="19.5">saints</w> <w n="19.6">jours</w> <w n="19.7">révolus</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Avait</w> <w n="20.2">jeté</w> <w n="20.3">ce</w> <w n="20.4">cri</w> <w n="20.5">sonore</w> <w n="20.6">à</w> <w n="20.7">la</w> <w n="20.8">tempête</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">— <w n="21.1">Sans</w> <w n="21.2">doute</w> <w n="21.3">il</w> <w n="21.4">se</w> <w n="21.5">souvient</w>, <w n="21.6">le</w> <w n="21.7">bronze</w> <w n="21.8">abandonné</w> ;</l>
						<l n="22" num="6.2"><w n="22.1">Il</w> <w n="22.2">dort</w>, <w n="22.3">et</w> <w n="22.4">son</w> <w n="22.5">printemps</w> <w n="22.6">regretté</w> <w n="22.7">se</w> <w n="22.8">prolonge</w></l>
						<l n="23" num="6.3"><w n="23.1">Dans</w> <w n="23.2">les</w> <w n="23.3">vibrations</w> <w n="23.4">berceuses</w> <w n="23.5">d</w>’<w n="23.6">un</w> <w n="23.7">beau</w> <w n="23.8">songe</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">la</w> <w n="24.3">chanson</w> <w n="24.4">de</w> <w n="24.5">sa</w> <w n="24.6">Jeunesse</w> <w n="24.7">a</w> <w n="24.8">résonné</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Après</w> <w n="25.2">les</w> <w n="25.3">temps</w> <w n="25.4">troublés</w>, <w n="25.5">quand</w> <w n="25.6">vient</w> <w n="25.7">la</w> <w n="25.8">paix</w> <w n="25.9">amie</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Les</w> <w n="26.2">choses</w>, <w n="26.3">comme</w> <w n="26.4">nous</w>, <w n="26.5">ont</w> <w n="26.6">leur</w> <w n="26.7">rêve</w> <w n="26.8">éternel</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Pensais</w>-<w n="27.2">je</w> <w n="27.3">en</w> <w n="27.4">écoutant</w> <w n="27.5">s</w>’<w n="27.6">envoler</w> <w n="27.7">vers</w> <w n="27.8">le</w> <w n="27.9">ciel</w></l>
						<l n="28" num="7.4"><w n="28.1">Le</w> <w n="28.2">rêve</w> <w n="28.3">harmonieux</w> <w n="28.4">de</w> <w n="28.5">la</w> <w n="28.6">cloche</w> <w n="28.7">endormie</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Mais</w> <w n="29.2">non</w> ! <w n="29.3">sur</w> <w n="29.4">son</w> <w n="29.5">appui</w> <w n="29.6">rustique</w> <w n="29.7">elle</w> <w n="29.8">oscillait</w>.</l>
						<l n="30" num="8.2"><w n="30.1">Un</w> <w n="30.2">invisible</w> <w n="30.3">bras</w> <w n="30.4">réglait</w> <w n="30.5">donc</w> <w n="30.6">cette</w> <w n="30.7">plainte</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">Une</w> <w n="31.2">douleur</w> <w n="31.3">humaine</w> <w n="31.4">inspirait</w> <w n="31.5">la</w> <w n="31.6">voix</w> <w n="31.7">sainte</w> :</l>
						<l n="32" num="8.4"><w n="32.1">Ce</w> <w n="32.2">n</w>’<w n="32.3">est</w> <w n="32.4">pas</w> <w n="32.5">en</w> <w n="32.6">rêvant</w> <w n="32.7">que</w> <w n="32.8">le</w> <w n="32.9">bronze</w> <w n="32.10">parlait</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Lors</w> <w n="33.2">j</w>’<w n="33.3">ai</w> <w n="33.4">crié</w> : — <w n="33.5">Quel</w> <w n="33.6">Montagnais</w> <w n="33.7">dans</w> <w n="33.8">l</w>’<w n="33.9">ombre</w> <w n="33.10">pleure</w></l>
						<l n="34" num="9.2"><w n="34.1">Le</w> <w n="34.2">regret</w> <w n="34.3">d</w>’<w n="34.4">autrefois</w> <w n="34.5">au</w> <w n="34.6">clocher</w> <w n="34.7">des</w> <w n="34.8">aïeux</w> ?</l>
						<l n="35" num="9.3"><w n="35.1">J</w>’<w n="35.2">irai</w> <w n="35.3">te</w> <w n="35.4">voir</w> <w n="35.5">sonner</w>, <w n="35.6">sonneur</w> <w n="35.7">mystérieux</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">je</w> <w n="36.3">saurai</w> <w n="36.4">pourquoi</w> <w n="36.5">tu</w> <w n="36.6">sonnes</w> <w n="36.7">à</w> <w n="36.8">cette</w> <w n="36.9">heure</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">J</w>’<w n="37.2">hésitai</w> <w n="37.3">sur</w> <w n="37.4">le</w> <w n="37.5">seuil</w> <w n="37.6">du</w> <w n="37.7">monument</w> <w n="37.8">sacré</w></l>
						<l n="38" num="10.2"><w n="38.1">Par</w> <w n="38.2">les</w> <w n="38.3">rayons</w> <w n="38.4">du</w> <w n="38.5">ciel</w> <w n="38.6">et</w> <w n="38.7">par</w> <w n="38.8">ceux</w> <w n="38.9">de</w> <w n="38.10">l</w>’<w n="38.11">histoire</w> ;</l>
						<l n="39" num="10.3"><w n="39.1">Mais</w> <w n="39.2">la</w> <w n="39.3">porte</w>, <w n="39.4">en</w> <w n="39.5">grinçant</w>, <w n="39.6">démasqua</w> <w n="39.7">la</w> <w n="39.8">nef</w> <w n="39.9">noire</w>.</l>
						<l n="40" num="10.4"><w n="40.1">Démasqua</w> <w n="40.2">la</w> <w n="40.3">nef</w> <w n="40.4">noire</w> <w n="40.5">en</w> <w n="40.6">grinçant</w> !… <w n="40.7">et</w> <w n="40.8">j</w>’<w n="40.9">entrai</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Vainement</w> <w n="41.2">par</w> <w n="41.3">trois</w> <w n="41.4">fois</w> <w n="41.5">j</w>’<w n="41.6">appelai</w>. <w n="41.7">Rien</w> ! <w n="41.8">Personne</w> !</l>
						<l n="42" num="11.2"><w n="42.1">Le</w> <w n="42.2">silence</w> <w n="42.3">gardait</w> <w n="42.4">les</w> <w n="42.5">secrets</w> <w n="42.6">du</w> <w n="42.7">passé</w>.</l>
						<l n="43" num="11.3"><w n="43.1">Épris</w> <w n="43.2">de</w> <w n="43.3">l</w>’<w n="43.4">invisible</w>, <w n="43.5">inquiet</w>, <w n="43.6">j</w>’<w n="43.7">avançai</w></l>
						<l n="44" num="11.4"><w n="44.1">Dans</w> <w n="44.2">la</w> <w n="44.3">terreur</w> <w n="44.4">muette</w> <w n="44.5">où</w> <w n="44.6">l</w>’<w n="44.7">inconnu</w> <w n="44.8">frissonne</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Devant</w> <w n="45.2">l</w>’<w n="45.3">autel</w> <w n="45.4">par</w> <w n="45.5">la</w> <w n="45.6">veilleuse</w> <w n="45.7">abandonné</w>,</l>
						<l n="46" num="12.2"><w n="46.1">Veille</w> <w n="46.2">dans</w> <w n="46.3">son</w> <w n="46.4">cercueil</w> <w n="46.5">l</w>’<w n="46.6">humble</w> <w n="46.7">missionnaire</w> ;</l>
						<l n="47" num="12.3"><w n="47.1">Son</w> <w n="47.2">ombre</w> <w n="47.3">plaît</w> <w n="47.4">au</w> <w n="47.5">Christ</w> <w n="47.6">autant</w> <w n="47.7">qu</w>’<w n="47.8">une</w> <w n="47.9">lumière</w> !</l>
						<l n="48" num="12.4"><w n="48.1">Sur</w> <w n="48.2">ce</w> <w n="48.3">grand</w> <w n="48.4">souvenir</w> <w n="48.5">je</w> <w n="48.6">me</w> <w n="48.7">suis</w> <w n="48.8">incliné</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Était</w>-<w n="49.2">ce</w> <w n="49.3">lui</w>, <w n="49.4">l</w>’<w n="49.5">apôtre</w> <w n="49.6">intrépide</w> <w n="49.7">au</w> <w n="49.8">cœur</w> <w n="49.9">tendre</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Qui</w>, <w n="50.2">réveillant</w> <w n="50.3">la</w> <w n="50.4">cloche</w> <w n="50.5">au</w> <w n="50.6">fond</w> <w n="50.7">des</w> <w n="50.8">vieux</w> <w n="50.9">oublis</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Venait</w> <w n="51.2">renouveler</w> <w n="51.3">pour</w> <w n="51.4">les</w> <w n="51.5">ensevelis</w></l>
						<l n="52" num="13.4">« <w n="52.1">Le</w> <w n="52.2">plaisir</w> <w n="52.3">nompareil</w> <w n="52.4">qu</w>’<w n="52.5">ils</w> <w n="52.6">prenoient</w> <w n="52.7">à</w> <w n="52.8">l</w>’<w n="52.9">entendre</w> » ?</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Au</w> <w n="53.2">charme</w> <w n="53.3">évocateur</w> <w n="53.4">et</w> <w n="53.5">magique</w> <w n="53.6">des</w> <w n="53.7">sons</w>,</l>
						<l n="54" num="14.2"><w n="54.1">Un</w> <w n="54.2">peuple</w> <w n="54.3">mort</w> <w n="54.4">s</w>’<w n="54.5">est</w> <w n="54.6">réveillé</w> <w n="54.7">dans</w> <w n="54.8">ma</w> <w n="54.9">pensée</w> ;</l>
						<l n="55" num="14.3"><w n="55.1">Mon</w> <w n="55.2">cœur</w> <w n="55.3">a</w> <w n="55.4">pris</w> <w n="55.5">le</w> <w n="55.6">deuil</w> <w n="55.7">de</w> <w n="55.8">sa</w> <w n="55.9">gloire</w> <w n="55.10">passée</w>,</l>
						<l n="56" num="14.4"><w n="56.1">Que</w> <w n="56.2">par</w> <w n="56.3">notre</w> <w n="56.4">silence</w> <w n="56.5">ingrat</w> <w n="56.6">nous</w> <w n="56.7">offensons</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">La</w> <w n="57.2">cloche</w> <w n="57.3">fit</w> <w n="57.4">chanter</w> <w n="57.5">l</w>’<w n="57.6">écho</w> <w n="57.7">des</w> <w n="57.8">murs</w> <w n="57.9">antiques</w> ;</l>
						<l n="58" num="15.2"><w n="58.1">Et</w> <w n="58.2">les</w> <w n="58.3">chœurs</w> <w n="58.4">endormis</w> <w n="58.5">depuis</w> <w n="58.6">le</w> <w n="58.7">temps</w> <w n="58.8">jadis</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Fervents</w> <w n="59.2">ainsi</w> <w n="59.3">qu</w>’<w n="59.4">aux</w> <w n="59.5">jours</w> <w n="59.6">des</w> <w n="59.7">nobles</w> <w n="59.8">fleurs</w> <w n="59.9">de</w> <w n="59.10">lys</w>,</l>
						<l n="60" num="15.4"><w n="60.1">Dans</w> <w n="60.2">l</w>’<w n="60.3">église</w> <w n="60.4">déserte</w> <w n="60.5">ont</w> <w n="60.6">redit</w> <w n="60.7">leurs</w> <w n="60.8">cantiques</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Je</w> <w n="61.2">t</w>’<w n="61.3">évoquais</w>, <w n="61.4">cloche</w> <w n="61.5">des</w> <w n="61.6">deuils</w> <w n="61.7">et</w> <w n="61.8">des</w> <w n="61.9">adieux</w>.</l>
						<l n="62" num="16.2"><w n="62.1">Et</w> <w n="62.2">cloche</w> <w n="62.3">des</w> <w n="62.4">fiertés</w> <w n="62.5">joyeusement</w> <w n="62.6">sonore</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Saluant</w> <w n="63.2">par</w> <w n="63.3">ton</w> <w n="63.4">chant</w> <w n="63.5">virginal</w> <w n="63.6">dans</w> <w n="63.7">l</w>’<w n="63.8">aurore</w>,</l>
						<l n="64" num="16.4"><w n="64.1">Le</w> <w n="64.2">chef</w> <w n="64.3">Tacouérima</w> <w n="64.4">toujours</w> <w n="64.5">victorieux</w> !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Je</w> <w n="65.2">t</w>’<w n="65.3">entendais</w> <w n="65.4">frémir</w> <w n="65.5">d</w>’<w n="65.6">allégresse</w> <w n="65.7">au</w> <w n="65.8">baptême</w>,</l>
						<l n="66" num="17.2"><w n="66.1">Saluer</w> <w n="66.2">le</w> <w n="66.3">secret</w> <w n="66.4">profond</w> <w n="66.5">de</w> <w n="66.6">l</w>’<w n="66.7">Ostensoir</w>,</l>
						<l n="67" num="17.3"><w n="67.1">Convier</w> <w n="67.2">les</w> <w n="67.3">croyants</w> <w n="67.4">à</w> <w n="67.5">l</w>’<w n="67.6">oraison</w> <w n="67.7">du</w> <w n="67.8">soir</w>.</l>
						<l n="68" num="17.4"><w n="68.1">Et</w> <w n="68.2">sur</w> <w n="68.3">les</w> <w n="68.4">trépassés</w> <w n="68.5">gémir</w> <w n="68.6">l</w>’<w n="68.7">adieu</w> <w n="68.8">suprême</w>.</l>
					</lg>
					<lg n="18">
						<l n="69" num="18.1"><w n="69.1">Je</w> <w n="69.2">t</w>’<w n="69.3">évoquais</w>, <w n="69.4">sonnant</w> <w n="69.5">bien</w> <w n="69.6">loin</w> <w n="69.7">dans</w> <w n="69.8">l</w>’<w n="69.9">Autrefois</w>,</l>
						<l n="70" num="18.2"><w n="70.1">Pour</w> <w n="70.2">le</w> <w n="70.3">retour</w> <w n="70.4">du</w> <w n="70.5">brave</w> <w n="70.6">à</w> <w n="70.7">la</w> <w n="70.8">plage</w> <w n="70.9">natale</w>,</l>
						<l n="71" num="18.3"><w n="71.1">Pour</w> <w n="71.2">le</w> <w n="71.3">pêcheur</w> <w n="71.4">perdu</w> <w n="71.5">dans</w> <w n="71.6">la</w> <w n="71.7">brume</w> <w n="71.8">automnale</w>,</l>
						<l n="72" num="18.4"><w n="72.1">Et</w> <w n="72.2">qui</w> <w n="72.3">revient</w> <w n="72.4">au</w> <w n="72.5">port</w>, <w n="72.6">appelé</w> <w n="72.7">par</w> <w n="72.8">ta</w> <w n="72.9">voix</w>.</l>
					</lg>
					<lg n="19">
						<l n="73" num="19.1"><w n="73.1">Je</w> <w n="73.2">revoyais</w> <w n="73.3">aussi</w> <w n="73.4">les</w> <w n="73.5">sveltes</w> <w n="73.6">sauvagesses</w>,</l>
						<l n="74" num="19.2"><w n="74.1">Au</w> <w n="74.2">frôlement</w> <w n="74.3">silencieux</w> <w n="74.4">de</w> <w n="74.5">leurs</w> <w n="74.6">souliers</w></l>
						<l n="75" num="19.3"><w n="75.1">S</w>’<w n="75.2">avancer</w> <w n="75.3">vers</w> <w n="75.4">l</w>’<w n="75.5">autel</w> <w n="75.6">avec</w> <w n="75.7">les</w> <w n="75.8">fiers</w> <w n="75.9">guerriers</w>,</l>
						<l n="76" num="19.4"><w n="76.1">En</w> <w n="76.2">inclinant</w> <w n="76.3">leur</w> <w n="76.4">front</w> <w n="76.5">orné</w> <w n="76.6">de</w> <w n="76.7">noires</w> <w n="76.8">tresses</w>.</l>
					</lg>
					<lg n="20">
						<l n="77" num="20.1"><w n="77.1">Je</w> <w n="77.2">t</w>’<w n="77.3">entendais</w> <w n="77.4">encor</w>, <w n="77.5">dominant</w> <w n="77.6">tout</w> <w n="77.7">le</w> <w n="77.8">bruit</w></l>
						<l n="78" num="20.2"><w n="78.1">De</w> <w n="78.2">la</w> <w n="78.3">bourgade</w> <w n="78.4">en</w> <w n="78.5">feu</w>, <w n="78.6">quand</w> <w n="78.7">ton</w> <w n="78.8">bronze</w> <w n="78.9">tragique</w>,</l>
						<l n="79" num="20.3"><w n="79.1">Parmi</w> <w n="79.2">les</w> <w n="79.3">hurlements</w> <w n="79.4">de</w> <w n="79.5">la</w> <w n="79.6">folle</w> <w n="79.7">panique</w>,</l>
						<l n="80" num="20.4"><w n="80.1">Jeta</w> <w n="80.2">les</w> <w n="80.3">sons</w> <w n="80.4">affreux</w> <w n="80.5">du</w> <w n="80.6">tocsin</w> <w n="80.7">dans</w> <w n="80.8">la</w> <w n="80.9">nuit</w>.</l>
					</lg>
					<lg n="21">
						<l n="81" num="21.1"><w n="81.1">J</w>’<w n="81.2">évoquais</w> <w n="81.3">tes</w> <w n="81.4">Noëls</w> <w n="81.5">perdus</w>… <w n="81.6">Mais</w> <w n="81.7">la</w> <w n="81.8">rafale</w></l>
						<l n="82" num="21.2"><w n="82.1">S</w>’<w n="82.2">engouffrant</w> <w n="82.3">dans</w> <w n="82.4">la</w> <w n="82.5">nef</w>, <w n="82.6">éteignit</w> <w n="82.7">mon</w> <w n="82.8">flambeau</w>.</l>
						<l n="83" num="21.3"><w n="83.1">La</w> <w n="83.2">nuit</w> <w n="83.3">m</w>’<w n="83.4">enveloppa</w> <w n="83.5">d</w>’<w n="83.6">horreur</w> <w n="83.7">près</w> <w n="83.8">du</w> <w n="83.9">tombeau</w>,</l>
						<l n="84" num="21.4"><w n="84.1">Et</w> <w n="84.2">l</w>’<w n="84.3">aile</w> <w n="84.4">de</w> <w n="84.5">la</w> <w n="84.6">Mort</w> <w n="84.7">effleura</w> <w n="84.8">mon</w> <w n="84.9">front</w> <w n="84.10">pâle</w>.</l>
					</lg>
					<lg n="22">
						<l n="85" num="22.1">« <w n="85.1">Dongne</w> <w n="85.2">don</w> ! <w n="85.3">dogne</w> <w n="85.4">don</w> ! » <w n="85.5">gémit</w> <w n="85.6">l</w>’<w n="85.7">airain</w> <w n="85.8">plus</w> <w n="85.9">bas</w></l>
						<l n="86" num="22.2"><w n="86.1">Dans</w> <w n="86.2">l</w>’<w n="86.3">épouvantement</w> <w n="86.4">des</w> <w n="86.5">profondes</w> <w n="86.6">ténèbres</w>.</l>
						<l n="87" num="22.3"><w n="87.1">Un</w> <w n="87.2">frisson</w> <w n="87.3">glacial</w> <w n="87.4">parcourut</w> <w n="87.5">mes</w> <w n="87.6">vertèbres</w>,</l>
						<l n="88" num="22.4"><w n="88.1">Car</w> <w n="88.2">j</w>’<w n="88.3">avais</w> <w n="88.4">reconnu</w> <w n="88.5">le</w> <w n="88.6">rythme</w> <w n="88.7">lent</w> <w n="88.8">du</w> <w n="88.9">glas</w>.</l>
					</lg>
					<lg n="23">
						<l n="89" num="23.1"><w n="89.1">Comment</w> <w n="89.2">suis</w>-<w n="89.3">je</w> <w n="89.4">sorti</w> <w n="89.5">vivant</w> <w n="89.6">de</w> <w n="89.7">cette</w> <w n="89.8">tombe</w> ?</l>
						<l n="90" num="23.2"><w n="90.1">Je</w> <w n="90.2">ne</w> <w n="90.3">sais</w> <w n="90.4">quels</w> <w n="90.5">esprits</w> <w n="90.6">m</w>’<w n="90.7">ont</w> <w n="90.8">entraîné</w> <w n="90.9">dehors</w>,</l>
						<l n="91" num="23.3"><w n="91.1">Mais</w> <w n="91.2">après</w> <w n="91.3">tant</w> <w n="91.4">de</w> <w n="91.5">jours</w> <w n="91.6">écoulés</w> <w n="91.7">depuis</w> <w n="91.8">lors</w>,</l>
						<l n="92" num="23.4"><w n="92.1">Le</w> <w n="92.2">tintement</w> <w n="92.3">fatal</w> <w n="92.4">dans</w> <w n="92.5">ma</w> <w n="92.6">mémoire</w> <w n="92.7">tombe</w> !</l>
					</lg>
					<lg n="24">
						<l n="93" num="24.1"><w n="93.1">Le</w> <w n="93.2">souffle</w> <w n="93.3">furibond</w> <w n="93.4">de</w> <w n="93.5">l</w>’<w n="93.6">ouragan</w> <w n="93.7">s</w>’<w n="93.8">accrut</w>,</l>
						<l n="94" num="24.2"><w n="94.1">La</w> <w n="94.2">plainte</w> <w n="94.3">résonna</w>, <w n="94.4">plus</w> <w n="94.5">lugubre</w> <w n="94.6">et</w> <w n="94.7">plus</w> <w n="94.8">longue</w> :</l>
						<l n="95" num="24.3"><w n="95.1">Dongue</w> ! <w n="95.2">dongue</w>-<w n="95.3">dongdon</w> ! <w n="95.4">daïngne</w> ! <w n="95.5">don</w> ! <w n="95.6">dôgne</w>-<w n="95.7">dongue</w> !</l>
						<l n="96" num="24.4"><w n="96.1">Puis</w> <w n="96.2">l</w>’<w n="96.3">ouragan</w> <w n="96.4">fit</w> <w n="96.5">trêve</w> <w n="96.6">et</w> <w n="96.7">la</w> <w n="96.8">cloche</w> <w n="96.9">se</w> <w n="96.10">tut</w>.</l>
					</lg>
					<lg n="25">
						<l n="97" num="25.1"><w n="97.1">L</w>’<w n="97.2">âme</w> <w n="97.3">de</w> <w n="97.4">Nelligan</w> <w n="97.5">m</w>’<w n="97.6">a</w> <w n="97.7">prêté</w> <w n="97.8">son</w> <w n="97.9">génie</w></l>
						<l n="98" num="25.2"><w n="98.1">Pour</w> <w n="98.2">clamer</w> : <w n="98.3">Qui</w> <w n="98.4">soupire</w> <w n="98.5">ici</w> <w n="98.6">des</w> <w n="98.7">désespoirs</w> ?</l>
						<l n="99" num="25.3"><w n="99.1">Cloche</w> <w n="99.2">des</w> <w n="99.3">âges</w> <w n="99.4">morts</w> <w n="99.5">sonnant</w> <w n="99.6">à</w> <w n="99.7">timbres</w> <w n="99.8">noirs</w>,</l>
						<l n="100" num="25.4"><w n="100.1">Dis</w>-<w n="100.2">moi</w> <w n="100.3">quelle</w> <w n="100.4">douleur</w> <w n="100.5">vibre</w> <w n="100.6">en</w> <w n="100.7">ton</w> <w n="100.8">harmonie</w> !</l>
					</lg>
					<lg n="26">
						<l n="101" num="26.1"><w n="101.1">Un</w> <w n="101.2">affreux</w> <w n="101.3">tourbillon</w> <w n="101.4">fit</w> <w n="101.5">rugir</w> <w n="101.6">la</w> <w n="101.7">forêt</w></l>
						<l n="102" num="26.2"><w n="102.1">Et</w> <w n="102.2">les</w> <w n="102.3">flots</w> <w n="102.4">fracassés</w> <w n="102.5">sur</w> <w n="102.6">la</w> <w n="102.7">rive</w> <w n="102.8">écumante</w> ;</l>
						<l n="103" num="26.3"><w n="103.1">Alors</w> <w n="103.2">je</w> <w n="103.3">crus</w> <w n="103.4">entendre</w>, <w n="103.5">au</w> <w n="103.6">sein</w> <w n="103.7">de</w> <w n="103.8">la</w> <w n="103.9">tourmente</w>,</l>
						<l n="104" num="26.4"><w n="104.1">Une</w> <w n="104.2">voix</w> <w n="104.3">tristement</w> <w n="104.4">humaine</w> <w n="104.5">qui</w> <w n="104.6">criait</w> :</l>
					</lg>
					<lg n="27">
						<l n="105" num="27.1">— <w n="105.1">Je</w> <w n="105.2">suis</w> <w n="105.3">l</w>’<w n="105.4">âme</w> <w n="105.5">qui</w> <w n="105.6">pleure</w> <w n="105.7">au</w> <w n="105.8">pied</w> <w n="105.9">de</w> <w n="105.10">la</w> <w n="105.11">montagne</w>…</l>
						<l n="106" num="27.2"><w n="106.1">Le</w> <w n="106.2">roi</w> <w n="106.3">du</w> <w n="106.4">fleuve</w> <w n="106.5">noir</w>… <w n="106.6">le</w> <w n="106.7">vieillard</w> <w n="106.8">du</w> <w n="106.9">passé</w>…</l>
						<l n="107" num="27.3"><w n="107.1">Devant</w> <w n="107.2">l</w>’<w n="107.3">oubli</w> <w n="107.4">fatal</w> <w n="107.5">mon</w> <w n="107.6">fantôme</w> <w n="107.7">est</w> <w n="107.8">dressé</w>,</l>
						<l n="108" num="27.4"><w n="108.1">Et</w> <w n="108.2">le</w> <w n="108.3">suprême</w> <w n="108.4">adieu</w> <w n="108.5">du</w> <w n="108.6">destin</w> <w n="108.7">m</w>’<w n="108.8">accompagne</w> !</l>
					</lg>
					<lg n="28">
						<l n="109" num="28.1"><w n="109.1">Et</w> <w n="109.2">j</w>’<w n="109.3">ai</w> <w n="109.4">dit</w> : — <w n="109.5">Descends</w> <w n="109.6">donc</w> <w n="109.7">à</w> <w n="109.8">mon</w> <w n="109.9">entendement</w> !</l>
						<l n="110" num="28.2"><w n="110.1">Ton</w> <w n="110.2">verbe</w> <w n="110.3">aérien</w> <w n="110.4">loin</w> <w n="110.5">de</w> <w n="110.6">mon</w> <w n="110.7">cœur</w> <w n="110.8">s</w>’<w n="110.9">envole</w>,</l>
						<l n="111" num="28.3"><w n="111.1">Car</w> <w n="111.2">je</w> <w n="111.3">ne</w> <w n="111.4">comprends</w> <w n="111.5">pas</w> <w n="111.6">si</w> <w n="111.7">profonde</w> <w n="111.8">parole</w>.</l>
						<l n="112" num="28.4"><w n="112.1">Alors</w>, <w n="112.2">tout</w> <w n="112.3">près</w> <w n="112.4">de</w> <w n="112.5">moi</w>, <w n="112.6">j</w>’<w n="112.7">entendis</w> <w n="112.8">clairement</w> :</l>
					</lg>
					<lg n="29">
						<l n="113" num="29.1">— <w n="113.1">Je</w> <w n="113.2">suis</w> <w n="113.3">Tacouérima</w>, <w n="113.4">que</w> <w n="113.5">le</w> <w n="113.6">chagrin</w> <w n="113.7">emporte</w>,</l>
						<l n="114" num="29.2"><w n="114.1">Sur</w> <w n="114.2">les</w> <w n="114.3">ailes</w> <w n="114.4">du</w> <w n="114.5">vent</w>, <w n="114.6">au</w> <w n="114.7">pays</w> <w n="114.8">montagnais</w> ;</l>
						<l n="115" num="29.3"><w n="115.1">Je</w> <w n="115.2">viens</w> <w n="115.3">du</w> <w n="115.4">souvenir</w> <w n="115.5">où</w> <w n="115.6">je</w> <w n="115.7">veille</w> <w n="115.8">à</w> <w n="115.9">jamais</w>,</l>
						<l n="116" num="29.4"><w n="116.1">Et</w> <w n="116.2">j</w>’<w n="116.3">ai</w> <w n="116.4">sonné</w> <w n="116.5">le</w> <w n="116.6">glas</w> <w n="116.7">de</w> <w n="116.8">ma</w> <w n="116.9">nation</w> <w n="116.10">morte</w> !</l>
					</lg>
				</div></body></text></TEI>