<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANAGRAMMÉANA</title>
				<title type="sub_1">POËME EN HUIT CHANTS</title>
				<title type="medium">Édition électronique</title>
				<author key="HEC">
					<name>
						<forename>Gabriel-Antoine-Joseph</forename>
						<surname>HÉCART</surname>
					</name>
					<date from="1755" to="1838">1755-1838</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>962 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HEC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ANAGRAMMÉANA, POËME EN HUIT CHANTS</title>
						<author>Gabriel-Antoine-Joseph HÉCART</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/64792</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ANAGRAMMÉANA, POËME EN HUIT CHANTS</title>
								<author>Gabriel-Antoine-Joseph HÉCART</author>
								<edition>95e édition</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k6128824n?rk=21459;2</idno>
								<imprint>
									<pubPlace>Lille</pubPlace>
									<publisher>impr. de Horemans (Anagrammatopolis)</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ANAGRAMMÉANA, POËME EN HUIT CHANTS</title>
						<author>Gabriel-Antoine-Joseph HÉCART</author>
						<edition>Texte présenté et annoté par Alain Chevrier</edition>
						<imprint>
							<pubPlace>Lille</pubPlace>
							<publisher>PLEIN CHANT, IMPRIMEUR-ÉDITEUR</publisher>
							<date when="2007">2007</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1821">1821</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-04-27" who="RR">Plusieurs erreurs dans l’édition de 1867 corrigée à partir de l’édition d’Alain Chevrier de 2007.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="HEC2">
				<head type="main">CHANT DEUXIÈME</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lorsqu</w>’<w n="1.2">un</w> <w n="1.3">Priape</w> <w n="1.4">de</w> <w n="1.5">papier</w></l>
					<l n="2" num="1.2"><w n="2.1">Dérive</w> <w n="2.2">avant</w> <w n="2.3">de</w> <w n="2.4">dévier</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">fait</w> <w n="3.3">l</w>’<w n="3.4">éloge</w> <w n="3.5">de</w> <w n="3.6">géole</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">se</w> <w n="4.3">voile</w> <w n="4.4">d</w>’<w n="4.5">une</w> <w n="4.6">viole</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">veut</w> <w n="5.3">verser</w> <w n="5.4">pour</w> <w n="5.5">se</w> <w n="5.6">sevrer</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Trouer</w> <w n="6.2">la</w> <w n="6.3">roture</w> <w n="6.4">et</w> <w n="6.5">l</w>’<w n="6.6">outrer</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Il</w> <w n="7.2">met</w> <w n="7.3">ses</w> <w n="7.4">veines</w> <w n="7.5">dans</w> <w n="7.6">Venise</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Sa</w> <w n="8.2">salive</w> <w n="8.3">dans</w> <w n="8.4">sa</w> <w n="8.5">valise</w>,</l>
					<l n="9" num="1.9"><w n="9.1">La</w> <w n="9.2">plus</w> <w n="9.3">jeune</w> <w n="9.4">dans</w> <w n="9.5">son</w> <w n="9.6">enjeu</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">la</w> <w n="10.3">plus</w> <w n="10.4">neuve</w> <w n="10.5">à</w> <w n="10.6">son</w> <w n="10.7">neveu</w> :</l>
					<l n="11" num="1.11"><w n="11.1">Aussi</w> <w n="11.2">sa</w> <w n="11.3">grive</w> <w n="11.4">est</w>-<w n="11.5">elle</w> <w n="11.6">givre</w></l>
					<l n="12" num="1.12"><w n="12.1">Dans</w> <w n="12.2">un</w> <w n="12.3">cuvier</w> <w n="12.4">tout</w> <w n="12.5">plein</w> <w n="12.6">de</w> <w n="12.7">cuivre</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Jusqu</w>’<w n="13.2">en</w> <w n="13.3">son</w> <w n="13.4">verre</w> <w n="13.5">il</w> <w n="13.6">veut</w> <w n="13.7">rêver</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">dans</w> <w n="14.3">son</w> <w n="14.4">verger</w> <w n="14.5">se</w> <w n="14.6">gréver</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">Il</w> <w n="15.2">recule</w> <w n="15.3">en</w> <w n="15.4">voyant</w> <w n="15.5">l</w>’<w n="15.6">ulcère</w>.</l>
					<l n="16" num="1.16"><w n="16.1">Privée</w> <w n="16.2">et</w> <w n="16.3">pris</w> <w n="16.4">par</w> <w n="16.5">sa</w> <w n="16.6">vipère</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Il</w> <w n="17.2">unit</w> <w n="17.3">les</w> <w n="17.4">époux</w> <w n="17.5">la</w> <w n="17.6">nuit</w></l>
					<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">trop</w> <w n="18.3">mutin</w> <w n="18.4">il</w> <w n="18.5">les</w> <w n="18.6">munit</w></l>
					<l n="19" num="1.19"><w n="19.1">D</w>’<w n="19.2">un</w> <w n="19.3">alfier</w> <w n="19.4">qui</w> <w n="19.5">vole</w> <w n="19.6">et</w> <w n="19.7">qui</w> <w n="19.8">flaire</w></l>
					<l n="20" num="1.20"><w n="20.1">Pour</w> <w n="20.2">égaler</w> <w n="20.3">une</w> <w n="20.4">galère</w>.</l>
					<l n="21" num="1.21"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">un</w> <w n="21.4">agent</w> <w n="21.5">comme</w> <w n="21.6">un</w> <w n="21.7">géant</w> ;</l>
					<l n="22" num="1.22"><w n="22.1">Il</w> <w n="22.2">est</w> <w n="22.3">ganté</w> <w n="22.4">comme</w> <w n="22.5">un</w> <w n="22.6">étang</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Il</w> <w n="23.2">veut</w> <w n="23.3">huer</w> <w n="23.4">avec</w> <w n="23.5">sa</w> <w n="23.6">hure</w>,</l>
					<l n="24" num="1.24"><w n="24.1">A</w> <w n="24.2">puer</w> <w n="24.3">elle</w> <w n="24.4">devient</w> <w n="24.5">pure</w> ;</l>
					<l n="25" num="1.25"><w n="25.1">De</w> <w n="25.2">tendre</w> <w n="25.3">elle</w> <w n="25.4">ferait</w> <w n="25.5">denter</w>,</l>
					<l n="26" num="1.26"><w n="26.1">Trop</w> <w n="26.2">brute</w> <w n="26.3">elle</w> <w n="26.4">ferait</w> <w n="26.5">buter</w>.</l>
					<l n="27" num="1.27"><w n="27.1">L</w>’<w n="27.2">ilote</w> <w n="27.3">prendra</w> <w n="27.4">de</w> <w n="27.5">la</w> <w n="27.6">toile</w></l>
					<l n="28" num="1.28"><w n="28.1">Avec</w> <w n="28.2">une</w> <w n="28.3">olive</w> <w n="28.4">de</w> <w n="28.5">voile</w>,</l>
					<l n="29" num="1.29"><w n="29.1">S</w>’<w n="29.2">il</w> <w n="29.3">rencontre</w> <w n="29.4">un</w> <w n="29.5">algérien</w> ;</l>
					<l n="30" num="1.30"><w n="30.1">Il</w> <w n="30.2">le</w> <w n="30.3">fera</w> <w n="30.4">galérien</w>.</l>
					<l n="31" num="1.31"><w n="31.1">De</w> <w n="31.2">l</w>’<w n="31.3">alezan</w> <w n="31.4">la</w> <w n="31.5">voix</w> <w n="31.6">nazale</w></l>
					<l n="32" num="1.32"><w n="32.1">Des</w> <w n="32.2">faciles</w> <w n="32.3">fera</w> <w n="32.4">fiscale</w> ;</l>
					<l n="33" num="1.33"><w n="33.1">Il</w> <w n="33.2">ira</w> <w n="33.3">chercher</w> <w n="33.4">l</w>’<w n="33.5">alevin</w></l>
					<l n="34" num="1.34"><w n="34.1">Pour</w> <w n="34.2">lui</w> <w n="34.3">tenir</w> <w n="34.4">lieu</w> <w n="34.5">de</w> <w n="34.6">levain</w>.</l>
					<l n="35" num="1.35"><w n="35.1">Le</w> <w n="35.2">gardien</w> <w n="35.3">prendra</w> <w n="35.4">la</w> <w n="35.5">gradine</w></l>
					<l n="36" num="1.36"><w n="36.1">Pour</w> <w n="36.2">aller</w> <w n="36.3">dans</w> <w n="36.4">l</w>’<w n="36.5">Inde</w> <w n="36.6">où</w> <w n="36.7">l</w>’<w n="36.8">on</w> <w n="36.9">dine</w> ;</l>
					<l n="37" num="1.37"><w n="37.1">S</w>’<w n="37.2">il</w> <w n="37.3">fait</w> <w n="37.4">un</w> <w n="37.5">diné</w> <w n="37.6">d</w>’<w n="37.7">un</w> <w n="37.8">déni</w></l>
					<l n="38" num="1.38"><w n="38.1">Par</w> <w n="38.2">le</w> <w n="38.3">nitre</w> <w n="38.4">il</w> <w n="38.5">sera</w> <w n="38.6">terni</w>.</l>
					<l n="39" num="1.39"><w n="39.1">Dans</w> <w n="39.2">Ternate</w> <w n="39.3">il</w> <w n="39.4">verra</w> <w n="39.5">Tarente</w></l>
					<l n="40" num="1.40"><w n="40.1">Et</w> <w n="40.2">dans</w> <w n="40.3">tenter</w> <w n="40.4">il</w> <w n="40.5">mettra</w> <w n="40.6">trente</w>,</l>
					<l n="41" num="1.41"><w n="41.1">La</w> <w n="41.2">tarte</w> <w n="41.3">le</w> <w n="41.4">fera</w> <w n="41.5">tâter</w></l>
					<l n="42" num="1.42"><w n="42.1">Et</w> <w n="42.2">tarer</w> <w n="42.3">le</w> <w n="42.4">fera</w> <w n="42.5">rater</w>.</l>
					<l n="43" num="1.43"><w n="43.1">Mettez</w> <w n="43.2">une</w> <w n="43.3">clape</w> <w n="43.4">à</w> <w n="43.5">sa</w> <w n="43.6">place</w></l>
					<l n="44" num="1.44"><w n="44.1">Le</w> <w n="44.2">ressac</w> <w n="44.3">donnera</w> <w n="44.4">la</w> <w n="44.5">crasse</w></l>
					<l n="45" num="1.45"><w n="45.1">Par</w> <w n="45.2">la</w> <w n="45.3">ponte</w> <w n="45.4">ayez</w> <w n="45.5">le</w> <w n="45.6">peton</w></l>
					<l n="46" num="1.46"><w n="46.1">Et</w> <w n="46.2">par</w> <w n="46.3">le</w> <w n="46.4">potin</w> <w n="46.5">le</w> <w n="46.6">piton</w>,</l>
					<l n="47" num="1.47"><w n="47.1">Il</w> <w n="47.2">s</w>’<w n="47.3">est</w> <w n="47.4">perdu</w> <w n="47.5">pour</w> <w n="47.6">une</w> <w n="47.7">prude</w></l>
					<l n="48" num="1.48"><w n="48.1">Dont</w> <w n="48.2">la</w> <w n="48.3">mine</w> <w n="48.4">était</w> <w n="48.5">dure</w> <w n="48.6">et</w> <w n="48.7">rude</w></l>
					<l n="49" num="1.49"><w n="49.1">Prenant</w> <w n="49.2">le</w> <w n="49.3">platré</w> <w n="49.4">d</w>’<w n="49.5">un</w> <w n="49.6">prélat</w>,</l>
					<l n="50" num="1.50"><w n="50.1">D</w>’<w n="50.2">un</w> <w n="50.3">lacet</w> <w n="50.4">vous</w> <w n="50.5">verrez</w> <w n="50.6">l</w>’<w n="50.7">éclat</w> ;</l>
					<l n="51" num="1.51"><w n="51.1">A</w> <w n="51.2">tarder</w> <w n="51.3">vous</w> <w n="51.4">aurez</w> <w n="51.5">la</w> <w n="51.6">dartre</w>,</l>
					<l n="52" num="1.52"><w n="52.1">Mais</w> <w n="52.2">pour</w> <w n="52.3">tramer</w> <w n="52.4">prenez</w> <w n="52.5">la</w> <w n="52.6">martre</w>.</l>
					<l n="53" num="1.53"><w n="53.1">L</w>’<w n="53.2">hymne</w> <w n="53.3">vous</w> <w n="53.4">donnera</w> <w n="53.5">l</w>’<w n="53.6">hymen</w></l>
					<l n="54" num="1.54"><w n="54.1">Pour</w> <w n="54.2">chanter</w> <w n="54.3">l</w>’<w n="54.4">ange</w> <w n="54.5">dans</w> <w n="54.6">Agen</w>.</l>
					<l n="55" num="1.55"><w n="55.1">Avec</w> <w n="55.2">l</w>’<w n="55.3">armet</w> <w n="55.4">on</w> <w n="55.5">suit</w> <w n="55.6">la</w> <w n="55.7">trame</w></l>
					<l n="56" num="1.56"><w n="56.1">Dans</w> <w n="56.2">la</w> <w n="56.3">mare</w> <w n="56.4">on</w> <w n="56.5">verra</w> <w n="56.6">la</w> <w n="56.7">rame</w> ;</l>
					<l n="57" num="1.57"><w n="57.1">Pour</w> <w n="57.2">noter</w> <w n="57.3">avec</w> <w n="57.4">un</w> <w n="57.5">tenor</w></l>
					<l n="58" num="1.58"><w n="58.1">Il</w> <w n="58.2">lui</w> <w n="58.3">donne</w> <w n="58.4">la</w> <w n="58.5">corne</w> <w n="58.6">encore</w>.</l>
					<l n="59" num="1.59"><w n="59.1">La</w> <w n="59.2">brue</w> <w n="59.3">aura</w> <w n="59.4">l</w>’<w n="59.5">habit</w> <w n="59.6">de</w> <w n="59.7">bure</w>,</l>
					<l n="60" num="1.60"><w n="60.1">La</w> <w n="60.2">sueur</w> <w n="60.3">viendra</w> <w n="60.4">de</w> <w n="60.5">l</w>’<w n="60.6">usure</w> ;</l>
					<l n="61" num="1.61"><w n="61.1">Lorsqu</w>’<w n="61.2">on</w> <w n="61.3">est</w> <w n="61.4">sure</w> <w n="61.5">un</w> <w n="61.6">peut</w> <w n="61.7">suer</w></l>
					<l n="62" num="1.62"><w n="62.1">Et</w> <w n="62.2">si</w> <w n="62.3">l</w>’<w n="62.4">on</w> <w n="62.5">ruse</w> <w n="62.6">il</w> <w n="62.7">faut</w> <w n="62.8">user</w>.</l>
					<l n="63" num="1.63"><w n="63.1">Lorsque</w> <w n="63.2">du</w> <w n="63.3">porche</w> <w n="63.4">on</w> <w n="63.5">sera</w> <w n="63.6">proche</w></l>
					<l n="64" num="1.64"><w n="64.1">Le</w> <w n="64.2">cocher</w> <w n="64.3">buveur</w> <w n="64.4">ira</w> <w n="64.5">croche</w> ;</l>
					<l n="65" num="1.65"><w n="65.1">Caduque</w> <w n="65.2">puise</w> <w n="65.3">à</w> <w n="65.4">l</w>’<w n="65.5">aqueduc</w></l>
					<l n="66" num="1.66"><w n="66.1">Pour</w> <w n="66.2">y</w> <w n="66.3">trouver</w> <w n="66.4">le</w> <w n="66.5">cul</w> <w n="66.6">de</w> <w n="66.7">Luc</w> ;</l>
					<l n="67" num="1.67"><w n="67.1">S</w>’<w n="67.2">il</w> <w n="67.3">en</w> <w n="67.4">voit</w> <w n="67.5">la</w> <w n="67.6">couleur</w> <w n="67.7">vermeille</w>,</l>
					<l n="68" num="1.68"><w n="68.1">Il</w> <w n="68.2">crie</w> : <w n="68.3">ah</w> ! <w n="68.4">c</w>’<w n="68.5">est</w> <w n="68.6">une</w> <w n="68.7">merveille</w> !</l>
					<l n="69" num="1.69"><w n="69.1">Ici</w> <w n="69.2">le</w> <w n="69.3">brave</w> <w n="69.4">doit</w> <w n="69.5">baver</w>,</l>
					<l n="70" num="1.70"><w n="70.1">Pour</w> <w n="70.2">veler</w> <w n="70.3">il</w> <w n="70.4">faut</w> <w n="70.5">se</w> <w n="70.6">lever</w>,</l>
					<l n="71" num="1.71"><w n="71.1">Car</w> <w n="71.2">de</w> <w n="71.3">Minerve</w> <w n="71.4">la</w> <w n="71.5">vermine</w></l>
					<l n="72" num="1.72"><w n="72.1">De</w> <w n="72.2">saine</w> <w n="72.3">deviendrait</w> <w n="72.4">asine</w> :</l>
					<l n="73" num="1.73"><w n="73.1">Si</w> <w n="73.2">dans</w> <w n="73.3">le</w> <w n="73.4">Mein</w> <w n="73.5">tu</w> <w n="73.6">vois</w> <w n="73.7">le</w> <w n="73.8">mien</w>,</l>
					<l n="74" num="1.74"><w n="74.1">Dans</w> <w n="74.2">ton</w> <w n="74.3">sein</w> <w n="74.4">tu</w> <w n="74.5">verras</w> <w n="74.6">le</w> <w n="74.7">sien</w>.</l>
					<l n="75" num="1.75"><w n="75.1">Lorsque</w> <w n="75.2">dans</w> <w n="75.3">la</w> <w n="75.4">Meuse</w> <w n="75.5">une</w> <w n="75.6">truite</w></l>
					<l n="76" num="1.76"><w n="76.1">Se</w> <w n="76.2">repait</w> <w n="76.3">avec</w> <w n="76.4">la</w> <w n="76.5">turite</w> ;</l>
					<l n="77" num="1.77"><w n="77.1">Le</w> <w n="77.2">fiacre</w> <w n="77.3">aura</w> <w n="77.4">soin</w> <w n="77.5">du</w> <w n="77.6">cafier</w></l>
					<l n="78" num="1.78"><w n="78.1">D</w>’<w n="78.2">Hipocrate</w> <w n="78.3">pot</w> <w n="78.4">à</w> <w n="78.5">chier</w>.</l>
					<l n="79" num="1.79"><w n="79.1">Si</w> <w n="79.2">tu</w> <w n="79.3">mets</w> <w n="79.4">la</w> <w n="79.5">tortue</w> <w n="79.6">en</w> <w n="79.7">tourte</w>,</l>
					<l n="80" num="1.80"><w n="80.1">Ne</w> <w n="80.2">fais</w> <w n="80.3">pas</w> <w n="80.4">la</w> <w n="80.5">croute</w> <w n="80.6">trop</w> <w n="80.7">courte</w> ;</l>
					<l n="81" num="1.81"><w n="81.1">La</w> <w n="81.2">souris</w> <w n="81.3">puera</w> <w n="81.4">le</w> <w n="81.5">roussi</w>,</l>
					<l n="82" num="1.82"><w n="82.1">Le</w> <w n="82.2">crime</w> <w n="82.3">nous</w> <w n="82.4">criera</w> <w n="82.5">merci</w>.</l>
					<l n="83" num="1.83"><w n="83.1">Si</w> <w n="83.2">l</w>’<w n="83.3">on</w> <w n="83.4">voulait</w> <w n="83.5">ambrer</w> <w n="83.6">du</w> <w n="83.7">marbre</w></l>
					<l n="84" num="1.84"><w n="84.1">D</w>’<w n="84.2">une</w> <w n="84.3">barre</w> <w n="84.4">on</w> <w n="84.5">ferait</w> <w n="84.6">un</w> <w n="84.7">arbre</w>.</l>
					<l n="85" num="1.85"><w n="85.1">Quoique</w> <w n="85.2">le</w> <w n="85.3">lac</w> <w n="85.4">vous</w> <w n="85.5">fasse</w> <w n="85.6">un</w> <w n="85.7">cal</w>,</l>
					<l n="86" num="1.86"><w n="86.1">Soyez</w> <w n="86.2">malingre</w> <w n="86.3">en</w> <w n="86.4">germinal</w>.</l>
					<l n="87" num="1.87"><w n="87.1">La</w> <w n="87.2">cigale</w> <w n="87.3">abonde</w> <w n="87.4">en</w> <w n="87.5">Galice</w>,</l>
					<l n="88" num="1.88"><w n="88.1">Avec</w> <w n="88.2">la</w> <w n="88.3">cive</w> <w n="88.4">on</w> <w n="88.5">a</w> <w n="88.6">le</w> <w n="88.7">vice</w> :</l>
					<l n="89" num="1.89"><w n="89.1">Mon</w> <w n="89.2">dessein</w> <w n="89.3">est</w> <w n="89.4">de</w> <w n="89.5">le</w> <w n="89.6">claquer</w></l>
					<l n="90" num="1.90"><w n="90.1">Parce</w> <w n="90.2">qu</w>’<w n="90.3">il</w> <w n="90.4">veut</w> <w n="90.5">toujours</w> <w n="90.6">calquer</w>.</l>
					<l n="91" num="1.91"><w n="91.1">Lorsque</w> <w n="91.2">les</w> <w n="91.3">ânes</w> <w n="91.4">sont</w> <w n="91.5">dans</w> <w n="91.6">l</w>’<w n="91.7">anse</w>,</l>
					<l n="92" num="1.92"><w n="92.1">Le</w> <w n="92.2">nacre</w> <w n="92.3">peut</w> <w n="92.4">devenir</w> <w n="92.5">rance</w> ;</l>
					<l n="93" num="1.93"><w n="93.1">L</w>’<w n="93.2">ingrat</w> <w n="93.3">mangera</w> <w n="93.4">le</w> <w n="93.5">gratin</w>,</l>
					<l n="94" num="1.94"><w n="94.1">La</w> <w n="94.2">graine</w> <w n="94.3">fournit</w> <w n="94.4">le</w> <w n="94.5">regain</w>.</l>
					<l n="95" num="1.95"><w n="95.1">Le</w> <w n="95.2">créateur</w>, <w n="95.3">la</w> <w n="95.4">créature</w> ;</l>
					<l n="96" num="1.96"><w n="96.1">Et</w> <w n="96.2">le</w> <w n="96.3">graveur</w> <w n="96.4">et</w> <w n="96.5">la</w> <w n="96.6">gravure</w>,</l>
					<l n="97" num="1.97"><w n="97.1">Font</w> <w n="97.2">que</w> <w n="97.3">les</w> <w n="97.4">grains</w> <w n="97.5">sont</w> <w n="97.6">bien</w> <w n="97.7">garnis</w>,</l>
					<l n="98" num="1.98"><w n="98.1">Que</w> <w n="98.2">par</w> <w n="98.3">le</w> <w n="98.4">supin</w> <w n="98.5">je</w> <w n="98.6">punis</w>.</l>
					<l n="99" num="1.99"><w n="99.1">Certaine</w> <w n="99.2">drogue</w> <w n="99.3">est</w> <w n="99.4">une</w> <w n="99.5">gourde</w></l>
					<l n="100" num="1.100"><w n="100.1">Qu</w>’<w n="100.2">on</w> <w n="100.3">veut</w> <w n="100.4">souder</w> <w n="100.5">lorsqu</w>’<w n="100.6">elle</w> <w n="100.7">est</w> <w n="100.8">sourde</w>.</l>
					<l n="101" num="1.101"><w n="101.1">Nicodème</w> <w n="101.2">est</w> <w n="101.3">comédien</w></l>
					<l n="102" num="1.102"><w n="102.1">Lorsqu</w>’<w n="102.2">on</w> <w n="102.3">veut</w> <w n="102.4">nier</w> <w n="102.5">on</w> <w n="102.6">n</w>’<w n="102.7">a</w> <w n="102.8">rien</w>.</l>
					<l n="103" num="1.103"><w n="103.1">Si</w> <w n="103.2">mon</w> <w n="103.3">ménage</w> <w n="103.4">est</w> <w n="103.5">un</w> <w n="103.6">manége</w>,</l>
					<l n="104" num="1.104"><w n="104.1">Mon</w> <w n="104.2">génie</w> <w n="104.3">ira</w> <w n="104.4">dans</w> <w n="104.5">la</w> <w n="104.6">neige</w>.</l>
					<l n="105" num="1.105"><w n="105.1">Si</w> <w n="105.2">vous</w> <w n="105.3">voyez</w> <w n="105.4">pauvre</w> <w n="105.5">paveur</w></l>
					<l n="106" num="1.106"><w n="106.1">Garantissez</w>-<w n="106.2">lui</w> <w n="106.3">la</w> <w n="106.4">vapeur</w>.</l>
					<l n="107" num="1.107"><w n="107.1">Avec</w> <w n="107.2">de</w> <w n="107.3">l</w>’<w n="107.4">encre</w> <w n="107.5">on</w> <w n="107.6">fait</w> <w n="107.7">le</w> <w n="107.8">cerne</w></l>
					<l n="108" num="1.108"><w n="108.1">Lorsque</w> <w n="108.2">l</w>’<w n="108.3">on</w> <w n="108.4">entre</w> <w n="108.5">l</w>’<w n="108.6">on</w> <w n="108.7">est</w> <w n="108.8">terne</w>.</l>
					<l n="109" num="1.109"><w n="109.1">Le</w> <w n="109.2">bétail</w> <w n="109.3">est</w> <w n="109.4">bien</w> <w n="109.5">établi</w></l>
					<l n="110" num="1.110"><w n="110.1">Lorsque</w> <w n="110.2">son</w> <w n="110.3">poil</w> <w n="110.4">est</w> <w n="110.5">bien</w> <w n="110.6">poli</w>.</l>
					<l n="111" num="1.111"><w n="111.1">Je</w> <w n="111.2">vous</w> <w n="111.3">accorde</w> <w n="111.4">une</w> <w n="111.5">cocarde</w>,</l>
					<l n="112" num="1.112"><w n="112.1">Prenez</w>-<w n="112.2">donc</w> <w n="112.3">le</w> <w n="112.4">grade</w> <w n="112.5">du</w> <w n="112.6">garde</w>.</l>
					<l n="113" num="1.113"><w n="113.1">Pour</w> <w n="113.2">admirer</w> <w n="113.3">un</w> <w n="113.4">madrier</w>,</l>
					<l n="114" num="1.114"><w n="114.1">J</w>’<w n="114.2">admire</w> <w n="114.3">le</w> <w n="114.4">jeu</w> <w n="114.5">du</w> <w n="114.6">damier</w>.</l>
					<l n="115" num="1.115"><w n="115.1">L</w>’<w n="115.2">épicerie</w> <w n="115.3">et</w> <w n="115.4">l</w>’<w n="115.5">épicière</w></l>
					<l n="116" num="1.116"><w n="116.1">Font</w> <w n="116.2">un</w> <w n="116.3">recueil</w> <w n="116.4">de</w> <w n="116.5">la</w> <w n="116.6">culière</w>.</l>
					<l n="117" num="1.117"><w n="117.1">Au</w> <w n="117.2">carmel</w> <w n="117.3">on</w> <w n="117.4">verra</w> <w n="117.5">Marcel</w>,</l>
					<l n="118" num="1.118"><w n="118.1">Prendre</w> <w n="118.2">les</w>, <w n="118.3">pronom</w>, <w n="118.4">pour</w> <w n="118.5">du</w> <w n="118.6">sel</w>.</l>
					<l n="119" num="1.119"><w n="119.1">L</w>’<w n="119.2">émigré</w> <w n="119.3">fuit</w> <w n="119.4">notre</w> <w n="119.5">régime</w>,</l>
					<l n="120" num="1.120"><w n="120.1">C</w>’<w n="120.2">est</w> <w n="120.3">sa</w> <w n="120.4">manie</w>, <w n="120.5">elle</w> <w n="120.6">l</w>’<w n="120.7">anime</w>.</l>
					<l n="121" num="1.121"><w n="121.1">Un</w> <w n="121.2">haricot</w> <w n="121.3">comme</w> <w n="121.4">un</w> <w n="121.5">chariot</w></l>
					<l n="122" num="1.122"><w n="122.1">Porte</w> <w n="122.2">un</w> <w n="122.3">trope</w> <w n="122.4">comme</w> <w n="122.5">un</w> <w n="122.6">Perot</w>.</l>
					<l n="123" num="1.123"><w n="123.1">Il</w> <w n="123.2">est</w> <w n="123.3">digne</w> <w n="123.4">d</w>’<w n="123.5">aller</w> <w n="123.6">à</w> <w n="123.7">Gnide</w>,</l>
					<l n="124" num="1.124"><w n="124.1">D</w>’<w n="124.2">en</w> <w n="124.3">passer</w> <w n="124.4">la</w> <w n="124.5">digue</w> <w n="124.6">sans</w> <w n="124.7">guide</w>.</l>
					<l n="125" num="1.125"><w n="125.1">On</w> <w n="125.2">a</w> <w n="125.3">beau</w> <w n="125.4">curer</w> <w n="125.5">un</w> <w n="125.6">recru</w>,</l>
					<l n="126" num="1.126"><w n="126.1">Duper</w> <w n="126.2">la</w> <w n="126.3">prude</w>, <w n="126.4">il</w> <w n="126.5">est</w> <w n="126.6">perdu</w>.</l>
					<l n="127" num="1.127"><w n="127.1">A</w> <w n="127.2">ce</w> <w n="127.3">singe</w> <w n="127.4">faites</w>-<w n="127.5">vous</w> <w n="127.6">signe</w> ?</l>
					<l n="128" num="1.128"><w n="128.1">Le</w> <w n="128.2">linge</w> <w n="128.3">sera</w> <w n="128.4">sur</w> <w n="128.5">la</w> <w n="128.6">ligne</w> ;</l>
					<l n="129" num="1.129"><w n="129.1">Si</w> <w n="129.2">le</w> <w n="129.3">frêne</w> <w n="129.4">chauffe</w> <w n="129.5">l</w>’<w n="129.6">enfer</w>,</l>
					<l n="130" num="1.130"><w n="130.1">Le</w> <w n="130.2">cancre</w> <w n="130.3">ira</w> <w n="130.4">vers</w> <w n="130.5">le</w> <w n="130.6">cancer</w>.</l>
				</lg>
			</div></body></text></TEI>