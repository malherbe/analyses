<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ANAGRAMMÉANA</title>
				<title type="sub_1">POËME EN HUIT CHANTS</title>
				<title type="medium">Édition électronique</title>
				<author key="HEC">
					<name>
						<forename>Gabriel-Antoine-Joseph</forename>
						<surname>HÉCART</surname>
					</name>
					<date from="1755" to="1838">1755-1838</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>962 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HEC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>ANAGRAMMÉANA, POËME EN HUIT CHANTS</title>
						<author>Gabriel-Antoine-Joseph HÉCART</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/64792</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>ANAGRAMMÉANA, POËME EN HUIT CHANTS</title>
								<author>Gabriel-Antoine-Joseph HÉCART</author>
								<edition>95e édition</edition>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k6128824n?rk=21459;2</idno>
								<imprint>
									<pubPlace>Lille</pubPlace>
									<publisher>impr. de Horemans (Anagrammatopolis)</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>ANAGRAMMÉANA, POËME EN HUIT CHANTS</title>
						<author>Gabriel-Antoine-Joseph HÉCART</author>
						<edition>Texte présenté et annoté par Alain Chevrier</edition>
						<imprint>
							<pubPlace>Lille</pubPlace>
							<publisher>PLEIN CHANT, IMPRIMEUR-ÉDITEUR</publisher>
							<date when="2007">2007</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1821">1821</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-04-27" who="RR">Plusieurs erreurs dans l’édition de 1867 corrigée à partir de l’édition d’Alain Chevrier de 2007.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="HEC6">
				<head type="main">CHANT SIXIÈME</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Lorsque</w> <w n="1.2">Seba</w> <w n="1.3">prit</w> <w n="1.4">une</w> <w n="1.5">base</w></l>
					<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">la</w> <w n="2.3">fit</w> <w n="2.4">de</w> <w n="2.5">sable</w> <w n="2.6">qui</w> <w n="2.7">blase</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">son</w> <w n="3.3">cabat</w> <w n="3.4">mit</w> <w n="3.5">du</w> <w n="3.6">tabac</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">trouva</w> <w n="4.3">son</w> <w n="4.4">cas</w> <w n="4.5">dans</w> <w n="4.6">son</w> <w n="4.7">sac</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Qu</w>’<w n="5.2">il</w> <w n="5.3">changera</w> <w n="5.4">pour</w> <w n="5.5">un</w> <w n="5.6">archange</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">D</w>’<w n="6.2">Agen</w> <w n="6.3">il</w> <w n="6.4">ira</w> <w n="6.5">trouver</w> <w n="6.6">l</w>’<w n="6.7">ange</w>.</l>
					<l n="7" num="1.7"><w n="7.1">En</w> <w n="7.2">prenant</w> <w n="7.3">le</w> <w n="7.4">taureau</w> <w n="7.5">d</w>’<w n="7.6">Autreau</w>,</l>
					<l n="8" num="1.8"><w n="8.1">A</w> <w n="8.2">sa</w> <w n="8.3">cause</w> <w n="8.4">il</w> <w n="8.5">mettra</w> <w n="8.6">le</w> <w n="8.7">sceau</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Il</w> <w n="9.2">est</w> <w n="9.3">altier</w> <w n="9.4">en</w> <w n="9.5">sa</w> <w n="9.6">latrie</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Prend</w> <w n="10.2">l</w>’<w n="10.3">ancolie</w> <w n="10.4">en</w> <w n="10.5">Laconie</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">pour</w> <w n="11.3">mieux</w> <w n="11.4">parler</w> <w n="11.5">le</w> <w n="11.6">latin</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Il</w> <w n="12.2">va</w> <w n="12.3">l</w>’<w n="12.4">apprendre</w> <w n="12.5">dans</w> <w n="12.6">Altin</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Il</w> <w n="13.2">est</w> <w n="13.3">d</w>’<w n="13.4">un</w> <w n="13.5">caractère</w> <w n="13.6">aimable</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Docile</w>, <w n="14.2">charmant</w>, <w n="14.3">amiable</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">C</w>’<w n="15.2">est</w> <w n="15.3">ainsi</w> <w n="15.4">qu</w>’<w n="15.5">on</w> <w n="15.6">le</w> <w n="15.7">dit</w> <w n="15.8">niais</w></l>
					<l n="16" num="1.16"><w n="16.1">Pour</w> <w n="16.2">être</w> <w n="16.3">aux</w> <w n="16.4">aguets</w> <w n="16.5">des</w> <w n="16.6">augets</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Pour</w> <w n="17.2">nous</w> <w n="17.3">aider</w> <w n="17.4">il</w> <w n="17.5">est</w> <w n="17.6">aride</w>,</l>
					<l n="18" num="1.18"><w n="18.1">L</w>’<w n="18.2">arsacide</w> <w n="18.3">est</w> <w n="18.4">un</w> <w n="18.5">ascaride</w> ;</l>
					<l n="19" num="1.19"><w n="19.1">Il</w> <w n="19.2">est</w> <w n="19.3">traître</w> <w n="19.4">pour</w> <w n="19.5">attirer</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Il</w> <w n="20.2">est</w> <w n="20.3">de</w> <w n="20.4">marbre</w> <w n="20.5">pour</w> <w n="20.6">ambrer</w> ;</l>
					<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">quoi</w> <w n="21.3">qu</w>’<w n="21.4">il</w> <w n="21.5">ait</w> <w n="21.6">l</w>’<w n="21.7">ambre</w> <w n="21.8">du</w> <w n="21.9">brame</w>,</l>
					<l n="22" num="1.22"><w n="22.1">Il</w> <w n="22.2">ne</w> <w n="22.3">va</w> <w n="22.4">que</w> <w n="22.5">l</w>’<w n="22.6">amble</w> <w n="22.7">du</w> <w n="22.8">blâme</w>.</l>
					<l n="23" num="1.23"><w n="23.1">Pour</w> <w n="23.2">le</w> <w n="23.3">carner</w> <w n="23.4">il</w> <w n="23.5">faut</w> <w n="23.6">l</w>’<w n="23.7">ancrer</w></l>
					<l n="24" num="1.24"><w n="24.1">Et</w> <w n="24.2">pour</w> <w n="24.3">ramer</w> <w n="24.4">il</w> <w n="24.5">va</w> <w n="24.6">marer</w></l>
					<l n="25" num="1.25"><w n="25.1">Il</w> <w n="25.2">prend</w> <w n="25.3">la</w> <w n="25.4">marée</w> <w n="25.5">à</w> <w n="25.6">l</w>’<w n="25.7">armée</w></l>
					<l n="26" num="1.26"><w n="26.1">Quelquefois</w> <w n="26.2">même</w> <w n="26.3">à</w> <w n="26.4">la</w> <w n="26.5">ramée</w>.</l>
					<l n="27" num="1.27"><w n="27.1">A</w> <w n="27.2">s</w>’<w n="27.3">égarer</w> <w n="27.4">pour</w> <w n="27.5">agréer</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Il</w> <w n="28.2">régorge</w> <w n="28.3">pour</w> <w n="28.4">s</w>’<w n="28.5">égorger</w>.</l>
					<l n="29" num="1.29"><w n="29.1">De</w> <w n="29.2">la</w> <w n="29.3">rame</w> <w n="29.4">il</w> <w n="29.5">se</w> <w n="29.6">fait</w> <w n="29.7">une</w> <w n="29.8">arme</w></l>
					<l n="30" num="1.30"><w n="30.1">Et</w> <w n="30.2">de</w> <w n="30.3">marle</w> <w n="30.4">il</w> <w n="30.5">prend</w> <w n="30.6">une</w> <w n="30.7">larme</w> :</l>
					<l n="31" num="1.31"><w n="31.1">Le</w> <w n="31.2">camus</w> <w n="31.3">prendra</w> <w n="31.4">du</w> <w n="31.5">sumac</w></l>
					<l n="32" num="1.32"><w n="32.1">Il</w> <w n="32.2">mettra</w> <w n="32.3">son</w> <w n="32.4">cal</w> <w n="32.5">dans</w> <w n="32.6">le</w> <w n="32.7">lac</w> ;</l>
					<l n="33" num="1.33"><w n="33.1">A</w> <w n="33.2">facer</w> <w n="33.3">la</w> <w n="33.4">mine</w> <w n="33.5">d</w>’<w n="33.6">un</w> <w n="33.7">Cafre</w>,</l>
					<l n="34" num="1.34"><w n="34.1">Les</w> <w n="34.2">affres</w> <w n="34.3">donneront</w> <w n="34.4">du</w> <w n="34.5">saffre</w> ;</l>
					<l n="35" num="1.35"><w n="35.1">Le</w> <w n="35.2">carlet</w> <w n="35.3">offre</w> <w n="35.4">le</w> <w n="35.5">cartel</w></l>
					<l n="36" num="1.36"><w n="36.1">A</w> <w n="36.2">Marcel</w> <w n="36.3">sur</w> <w n="36.4">le</w> <w n="36.5">mont</w> <w n="36.6">Carmel</w> ;</l>
					<l n="37" num="1.37"><w n="37.1">Près</w> <w n="37.2">de</w> <w n="37.3">Nicole</w> <w n="37.4">et</w> <w n="37.5"><choice hand="RR" reason="analysis" type="missing"><sic> </sic><corr source="édition 2007">de</corr></choice>Coline</w>,</l>
					<l n="38" num="1.38"><w n="38.1">Va</w> <w n="38.2">tatiner</w> <w n="38.3">une</w> <w n="38.4">tartine</w>.</l>
					<l n="39" num="1.39"><w n="39.1">Je</w> <w n="39.2">courberai</w> <w n="39.3">le</w> <w n="39.4">caroubier</w>,</l>
					<l n="40" num="1.40"><w n="40.1">Dans</w> <w n="40.2">la</w> <w n="40.3">varice</w> <w n="40.4">d</w>’<w n="40.5">un</w> <w n="40.6">cavier</w>.</l>
					<l n="41" num="1.41"><w n="41.1">La</w> <w n="41.2">centurie</w> <w n="41.3">à</w> <w n="41.4">la</w> <w n="41.5">ceinture</w></l>
					<l n="42" num="1.42"><w n="42.1">Crêüse</w> <w n="42.2">a</w> <w n="42.3">creusé</w> <w n="42.4">la</w> <w n="42.5">césure</w></l>
					<l n="43" num="1.43"><w n="43.1">Et</w> <w n="43.2">voulant</w> <w n="43.3">ficher</w> <w n="43.4">un</w> <w n="43.5">chérif</w></l>
					<l n="44" num="1.44"><w n="44.1">Lui</w> <w n="44.2">donna</w> <w n="44.3">le</w> <w n="44.4">fichet</w> <w n="44.5">chétif</w>.</l>
					<l n="45" num="1.45"><w n="45.1">Voulez</w>-<w n="45.2">vous</w> <w n="45.3">nuire</w> <w n="45.4">à</w> <w n="45.5">sa</w> <w n="45.6">ruine</w> ?</l>
					<l n="46" num="1.46"><w n="46.1">Faites</w>-<w n="46.2">lui</w> <w n="46.3">lacher</w> <w n="46.4">son</w> <w n="46.5">urine</w> ;</l>
					<l n="47" num="1.47"><w n="47.1">Vous</w> <w n="47.2">lui</w> <w n="47.3">direz</w>, <w n="47.4">s</w>’<w n="47.5">il</w> <w n="47.6">veut</w> <w n="47.7">plaider</w>,</l>
					<l n="48" num="1.48"><w n="48.1">Qu</w>’<w n="48.2">il</w> <w n="48.3">peut</w> <w n="48.4">se</w> <w n="48.5">faire</w> <w n="48.6">lapider</w>.</l>
					<l n="49" num="1.49"><w n="49.1">Le</w> <w n="49.2">nomade</w> <w n="49.3">a</w> <w n="49.4">mis</w> <w n="49.5">la</w> <w n="49.6">madone</w></l>
					<l n="50" num="1.50"><w n="50.1">A</w> <w n="50.2">la</w> <w n="50.3">poterne</w> <w n="50.4">de</w> <w n="50.5">Pétrone</w>.</l>
					<l n="51" num="1.51"><w n="51.1">A</w> <w n="51.2">Rouen</w> <w n="51.3">faut</w>-<w n="51.4">il</w> <w n="51.5">tant</w> <w n="51.6">nouer</w>,</l>
					<l n="52" num="1.52"><w n="52.1">Se</w> <w n="52.2">ruiner</w> <w n="52.3">pour</w> <w n="52.4">uriner</w> ?</l>
					<l n="53" num="1.53"><w n="53.1">Les</w> <w n="53.2">caniches</w> <w n="53.3">font</w> <w n="53.4">des</w> <w n="53.5">chicanes</w></l>
					<l n="54" num="1.54"><w n="54.1">Pour</w> <w n="54.2">mettre</w> <w n="54.3">l</w>’<w n="54.4">anse</w> <w n="54.5">au</w> <w n="54.6">dos</w> <w n="54.7">des</w> <w n="54.8">ânes</w> ;</l>
					<l n="55" num="1.55"><w n="55.1">Dans</w> <w n="55.2">le</w> <w n="55.3">curoir</w> <w n="55.4">il</w> <w n="55.5">faut</w> <w n="55.6">courir</w></l>
					<l n="56" num="1.56"><w n="56.1">Et</w> <w n="56.2">prendre</w> <w n="56.3">zirphé</w> <w n="56.4">pour</w> <w n="56.5">zéphir</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Prendre</w> <w n="57.2">la</w> <w n="57.3">treille</w> <w n="57.4">d</w>’<w n="57.5">une</w> <w n="57.6">étrille</w>,</l>
					<l n="58" num="1.58"><w n="58.1">Et</w> <w n="58.2">pille</w> <w n="58.3">étron</w> <w n="58.4">de</w> <w n="58.5">Pétronille</w>.</l>
					<l n="59" num="1.59"><w n="59.1">Par</w> <w n="59.2">son</w> <w n="59.3">rhume</w> <w n="59.4">il</w> <w n="59.5">voulait</w> <w n="59.6">l</w>’<w n="59.7">humer</w></l>
					<l n="60" num="1.60"><w n="60.1">Pour</w> <w n="60.2">le</w> <w n="60.3">marcher</w> <w n="60.4">et</w> <w n="60.5">le</w> <w n="60.6">charmer</w>.</l>
					<l n="61" num="1.61"><w n="61.1">Quand</w> <w n="61.2">le</w> <w n="61.3">grand</w> <w n="61.4">Dacier</w> <w n="61.5">était</w> <w n="61.6">diacre</w>,</l>
					<l n="62" num="1.62"><w n="62.1">Le</w> <w n="62.2">cafier</w> <w n="62.3">cultivé</w> <w n="62.4">du</w> <w n="62.5">fiacre</w>,</l>
					<l n="63" num="1.63"><w n="63.1">Faisait</w> <w n="63.2">le</w> <w n="63.3">lopin</w> <w n="63.4">d</w>’<w n="63.5">un</w> <w n="63.6">pilon</w></l>
					<l n="64" num="1.64"><w n="64.1">Pour</w> <w n="64.2">nourrir</w> <w n="64.3">de</w> <w n="64.4">loin</w> <w n="64.5">le</w> <w n="64.6">lion</w> ;</l>
					<l n="65" num="1.65"><w n="65.1">Il</w> <w n="65.2">l</w>’<w n="65.3">avait<choice hand="RR" reason="analysis" type="missing"><sic>porté</sic><corr source="édition 2007">portée</corr></choice></w> <w n="65.4">à</w> <choice hand="RR" reason="analysis" type="missing"><sic>la</sic><corr source="édition 2007"></corr></choice> <w n="65.5">Protée</w>.</l>
					<l n="66" num="1.66"><choice hand="RR" reason="analysis" type="missing"><sic> </sic><corr source="édition 2007"><w n="66.1">Par</w> <w n="66.2">une</w> <w n="66.3">Péote</w> <w n="66.4">à</w> <w n="66.5">la</w> <w n="66.6">potée</w></corr></choice></l>
					<l n="67" num="1.67"><w n="67.1">Au</w> <w n="67.2">prêtre</w> <w n="67.3">il</w> <w n="67.4">a</w> <w n="67.5">voulu</w> <w n="67.6">prêter</w>,</l>
					<l n="68" num="1.68"><w n="68.1">Une</w> <w n="68.2">porte</w> <w n="68.3">mais</w> <w n="68.4">pour</w> <w n="68.5">opter</w>,</l>
					<l n="69" num="1.69"><w n="69.1">Faite</w> <w n="69.2">à</w> <w n="69.3">Naples</w> <w n="69.4">avec</w> <w n="69.5">des</w> <w n="69.6">planes</w></l>
					<l n="70" num="1.70"><w n="70.1">Tirés</w> <w n="70.2">des</w> <w n="70.3">plus</w> <w n="70.4">saintes</w> <w n="70.5">tisanes</w> ;</l>
					<l n="71" num="1.71"><w n="71.1">Au</w> <w n="71.2">Liban</w> <w n="71.3">il</w> <w n="71.4">fit</w> <w n="71.5">son</w> <w n="71.6">bilan</w>,</l>
					<l n="72" num="1.72"><w n="72.1">Et</w> <w n="72.2">mit</w> <w n="72.3">une</w> <w n="72.4">canne</w> <w n="72.5">à</w> <w n="72.6">l</w>’<w n="72.7">encan</w> ;</l>
					<l n="73" num="1.73"><w n="73.1">Mit</w> <w n="73.2">en</w> <w n="73.3">canelle</w> <w n="73.4">sa</w> <w n="73.5">nacelle</w>,</l>
					<l n="74" num="1.74"><w n="74.1">Pour</w> <w n="74.2">la</w> <w n="74.3">porter</w> <w n="74.4">à</w> <w n="74.5">mère</w> <w n="74.6">Ancelle</w> ;</l>
					<l n="75" num="1.75"><w n="75.1">Par</w> <w n="75.2">le</w> <w n="75.3">brai</w> <w n="75.4">la</w> <w n="75.5">met</w> <w n="75.6">à</w> <w n="75.7">l</w>’<w n="75.8">abri</w></l>
					<l n="76" num="1.76"><w n="76.1">D</w>’<w n="76.2">être</w> <w n="76.3">filtré</w> <w n="76.4">s</w>’<w n="76.5">il</w> <w n="76.6">n</w>’<w n="76.7">est</w> <w n="76.8">flétri</w></l>
					<l n="77" num="1.77"><w n="77.1">Pour</w> <w n="77.2">la</w> <w n="77.3">carpe</w> <w n="77.4">prenons</w> <w n="77.5">la</w> <w n="77.6">câpre</w></l>
					<l n="78" num="1.78"><w n="78.1">Qui</w> <w n="78.2">se</w> <w n="78.3">pare</w> <w n="78.4">en</w> <w n="78.5">devenant</w> <w n="78.6">âpre</w>.</l>
					<l n="79" num="1.79"><w n="79.1">Soyez</w> <w n="79.2">cruel</w> <w n="79.3">envers</w> <w n="79.4">Nevers</w>,</l>
					<l n="80" num="1.80"><w n="80.1">Ce</w> <w n="80.2">sera</w> <w n="80.3">verser</w> <w n="80.4">le</w> <w n="80.5">revers</w>.</l>
					<l n="81" num="1.81"><w n="81.1">Oui</w>, <w n="81.2">la</w> <w n="81.3">gorgée</w> <w n="81.4">égorge</w> <w n="81.5">George</w>,</l>
					<l n="82" num="1.82"><w n="82.1">Mais</w> <w n="82.2">à</w> <w n="82.3">l</w>’<w n="82.4">égorger</w> <w n="82.5">il</w> <w n="82.6">regorge</w>.</l>
					<l n="83" num="1.83"><w n="83.1">Fréron</w> <w n="83.2">était</w> <w n="83.3">moins</w> <w n="83.4">qu</w>’<w n="83.5">un</w> <w n="83.6">ferron</w> ;</l>
					<l n="84" num="1.84"><w n="84.1">Bon</w> ! <w n="84.2">il</w> <w n="84.3">ronfle</w> <w n="84.4">comme</w> <w n="84.5">un</w> <w n="84.6">frelon</w>,</l>
					<l n="85" num="1.85"><w n="85.1">Avec</w> <w n="85.2">sa</w> <w n="85.3">gourme</w> <w n="85.4">il</w> <w n="85.5">a</w> <w n="85.6">la</w> <w n="85.7">morgue</w></l>
					<l n="86" num="1.86"><w n="86.1">Et</w> <w n="86.2">devient</w> <w n="86.3">rogue</w> <w n="86.4">comme</w> <w n="86.5">un</w> <w n="86.6">orgue</w> ;</l>
					<l n="87" num="1.87"><w n="87.1">Aussi</w> <w n="87.2">bigle</w> <w n="87.3">que</w> <w n="87.4">le</w> <w n="87.5">Gibel</w></l>
					<l n="88" num="1.88"><w n="88.1">Il</w> <w n="88.2">frappait</w> <w n="88.3">Léon</w> <w n="88.4">et</w> <w n="88.5">Noël</w> ;</l>
					<l n="89" num="1.89"><w n="89.1">De</w> <w n="89.2">lipe</w> <w n="89.3">il</w> <w n="89.4">faisait</w> <w n="89.5">une</w> <w n="89.6">plie</w></l>
					<l n="90" num="1.90"><w n="90.1">Prenait</w> <w n="90.2">l</w>’<w n="90.3">étoile</w> <w n="90.4">en</w> <w n="90.5">Étolie</w> ;</l>
					<l n="91" num="1.91"><w n="91.1">Le</w> <w n="91.2">varech</w> <w n="91.3">faisait</w> <w n="91.4">un</w> <w n="91.5">vacher</w></l>
					<l n="92" num="1.92"><w n="92.1">Avec</w> <w n="92.2">la</w> <w n="92.3">croche</w> <w n="92.4">du</w> <w n="92.5">cocher</w>,</l>
					<l n="93" num="1.93"><w n="93.1">Pour</w> <w n="93.2">la</w> <w n="93.3">garance</w> <w n="93.4">il</w> <w n="93.5">prit</w> <w n="93.6">caragne</w></l>
					<l n="94" num="1.94"><w n="94.1">Près</w> <w n="94.2">de</w> <w n="94.3">la</w> <w n="94.4">maligne</w> <w n="94.5">Limagne</w>,</l>
					<l n="95" num="1.95"><w n="95.1">Éacus</w> <w n="95.2">fut</w> <w n="95.3">si</w> <w n="95.4">bien</w> <w n="95.5">saucé</w>,</l>
					<l n="96" num="1.96"><w n="96.1">Si</w> <w n="96.2">dessalé</w>, <w n="96.3">si</w> <w n="96.4">délassé</w>,</l>
					<l n="97" num="1.97"><w n="97.1">Qu</w>’<w n="97.2">il</w> <w n="97.3">mit</w> <w n="97.4">l</w>’<w n="97.5">ucher</w> <w n="97.6">dans</w> <w n="97.7">une</w> <w n="97.8">ruche</w>,</l>
					<l n="98" num="1.98"><w n="98.1">Et</w> <w n="98.2">dans</w> <w n="98.3">le</w> <w n="98.4">bûcher</w> <w n="98.5">prit</w> <w n="98.6">la</w> <w n="98.7">bruche</w> ;</l>
					<l n="99" num="1.99"><w n="99.1">En</w> <w n="99.2">voulant</w> <w n="99.3">voir</w> <w n="99.4">huer</w> <w n="99.5">son</w> <w n="99.6">heur</w>,</l>
					<l n="100" num="1.100"><w n="100.1">Il</w> <w n="100.2">fut</w> <w n="100.3">heurter</w> <w n="100.4">chez</w> <w n="100.5">un</w> <w n="100.6">rhéteur</w> ;</l>
					<l n="101" num="1.101"><w n="101.1">Mais</w> <w n="101.2">la</w> <w n="101.3">gantelée</w> <w n="101.4">élégante</w>,</l>
					<l n="102" num="1.102"><w n="102.1">Étant</w> <w n="102.2">mise</w> <w n="102.3">auprès</w> <w n="102.4">de</w> <w n="102.5">sa</w> <w n="102.6">tante</w></l>
					<l n="103" num="1.103"><w n="103.1">A</w> <w n="103.2">son</w> <w n="103.3">réveil</w> <w n="103.4">suivit</w> <w n="103.5">un</w> <w n="103.6">lièvre</w></l>
					<l n="104" num="1.104"><w n="104.1">Qui</w> <w n="104.2">voulut</w> <w n="104.3">vêler</w> <w n="104.4">par</w> <w n="104.5">sa</w> <w n="104.6">lèvre</w> ;</l>
					<l n="105" num="1.105"><w n="105.1">Avec</w> <w n="105.2">le</w> <w n="105.3">titre</w> <w n="105.4">d</w>’<w n="105.5">un</w> <w n="105.6">tiret</w>,</l>
					<l n="106" num="1.106"><w n="106.1">Dans</w> <w n="106.2">la</w> <w n="106.3">Thrace</w> <w n="106.4">il</w> <w n="106.5">fit</w> <w n="106.6">un</w> <w n="106.7">archet</w>,</l>
					<l n="107" num="1.107"><w n="107.1">Il</w> <w n="107.2">est</w> <w n="107.3">si</w> <w n="107.4">sobre</w> <w n="107.5">comme</w> <w n="107.6">sorbe</w>,</l>
					<l n="108" num="1.108"><w n="108.1">Broute</w> <w n="108.2">la</w> <w n="108.3">tourbe</w> <w n="108.4">et</w> <w n="108.5">le</w> <w n="108.6">tuorbe</w>,</l>
					<l n="109" num="1.109"><w n="109.1">Prend</w> <w n="109.2">de</w> <w n="109.3">l</w>’<w n="109.4">arbois</w> <w n="109.5">chez</w> <w n="109.6">le</w> <w n="109.7">Barois</w>,</l>
					<l n="110" num="1.110"><w n="110.1">Des</w> <w n="110.2">viragos</w> <w n="110.3">dans</w> <w n="110.4">les</w> <w n="110.5">gravois</w> :</l>
					<l n="111" num="1.111"><w n="111.1">S</w>’<w n="111.2">il</w> <w n="111.3">change</w> <w n="111.4">il</w> <w n="111.5">mérite</w> <w n="111.6">la</w> <w n="111.7">ganche</w> ;</l>
					<l n="112" num="1.112"><w n="112.1">Il</w> <w n="112.2">chancèle</w> <w n="112.3">pour</w> <w n="112.4">une</w> <w n="112.5">éclanche</w> ;</l>
					<l n="113" num="1.113"><w n="113.1">Mais</w> <w n="113.2">l</w>’<w n="113.3">anis</w> <w n="113.4">le</w> <w n="113.5">rendra</w> <w n="113.6">plus</w> <w n="113.7">sain</w>,</l>
					<l n="114" num="1.114"><w n="114.1">Ou</w> <w n="114.2">le</w> <w n="114.3">milan</w> <w n="114.4">sera</w> <w n="114.5">malin</w>,</l>
					<l n="115" num="1.115"><w n="115.1">Le</w> <w n="115.2">more</w> <w n="115.3">ira</w> <w n="115.4">tout</w> <w n="115.5">près</w> <w n="115.6">de</w> <w n="115.7">l</w>’<w n="115.8">orme</w></l>
					<l n="116" num="1.116"><w n="116.1">Faire</w> <w n="116.2">la</w> <w n="116.3">morce</w> <w n="116.4">d</w>’<w n="116.5">une</w> <w n="116.6">corme</w></l>
					<l n="117" num="1.117"><w n="117.1">Près</w> <w n="117.2">de</w> <w n="117.3">minot</w>, <w n="117.4">miton</w>, <w n="117.5">timon</w>,</l>
					<l n="118" num="1.118"><w n="118.1">Il</w> <w n="118.2">mettra</w> <w n="118.3">moins</w>, <w n="118.4">Minos</w>, <w n="118.5">Simon</w>.</l>
					<l n="119" num="1.119"><w n="119.1">Il</w> <w n="119.2">punit</w> <w n="119.3">le</w> <w n="119.4">reste</w> <w n="119.5">des</w> <w n="119.6">êtres</w></l>
					<l n="120" num="1.120"><w n="120.1">D</w>’<w n="120.2">avoir</w> <w n="120.3">pris</w> <w n="120.4">les</w> <w n="120.5">éthers</w> <w n="120.6">des</w> <w n="120.7">hêtres</w>.</l>
				</lg>
			</div></body></text></TEI>