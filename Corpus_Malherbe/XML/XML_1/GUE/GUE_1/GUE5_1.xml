<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><div type="poem" key="GUE5" rhyme="none">
					<head type="number">V</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">grain</w> <w n="1.3">de</w> <w n="1.4">blé</w> <w n="1.5">qu</w>’<w n="1.6">on</w> <w n="1.7">va</w> <w n="1.8">moudre</w> <w n="1.9">contient</w> <w n="1.10">l</w>’<w n="1.11">hostie</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">vin</w> <w n="2.3">se</w> <w n="2.4">change</w> <w n="2.5">au</w> <w n="2.6">sang</w> <w n="2.7">divin</w> <w n="2.8">de</w> <w n="2.9">la</w> <w n="2.10">victime</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">fruit</w> <w n="3.3">tire</w> <w n="3.4">son</w> <w n="3.5">suc</w> <w n="3.6">de</w> <w n="3.7">la</w> <w n="3.8">branche</w> <w n="3.9">brisée</w>.</l>
						<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">rose</w>, <w n="4.3">vierge</w> <w n="4.4">en</w> <w n="4.5">pleurs</w>, <w n="4.6">fléchit</w> <w n="4.7">sous</w> <w n="4.8">la</w> <w n="4.9">rosée</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">le</w> <w n="5.3">miel</w> <w n="5.4">alourdit</w> <w n="5.5">l</w>’<w n="5.6">abeille</w> <w n="5.7">suspendue</w></l>
						<l n="6" num="1.6"><w n="6.1">Qu</w>’<w n="6.2">un</w> <w n="6.3">souffle</w> <w n="6.4">d</w>’<w n="6.5">air</w> <w n="6.6">balance</w> <w n="6.7">aux</w> <w n="6.8">lèvres</w> <w n="6.9">de</w> <w n="6.10">la</w> <w n="6.11">rose</w>.</l>
						<l n="7" num="1.7"><w n="7.1">La</w> <w n="7.2">nuit</w>, <w n="7.3">épouse</w> <w n="7.4">obscure</w>, <w n="7.5">est</w> <w n="7.6">grosse</w> <w n="7.7">de</w> <w n="7.8">l</w>’<w n="7.9">aurore</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">la</w> <w n="8.3">mer</w> <w n="8.4">sourdement</w> <w n="8.5">couve</w> <w n="8.6">un</w> <w n="8.7">nouveau</w> <w n="8.8">déluge</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Chaque</w> <w n="9.2">être</w>, <w n="9.3">de</w> <w n="9.4">la</w> <w n="9.5">plante</w> <w n="9.6">au</w> <w n="9.7">poète</w> <w n="9.8">qui</w> <w n="9.9">prie</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Supporte</w> <w n="10.2">son</w> <w n="10.3">anneau</w> <w n="10.4">dans</w> <w n="10.5">la</w> <w n="10.6">chaîne</w> <w n="10.7">infinie</w> :</l>
						<l n="11" num="1.11"><w n="11.1">L</w>’<w n="11.2">enfant</w> <w n="11.3">déjà</w> <w n="11.4">mûrit</w> <w n="11.5">au</w> <w n="11.6">coeur</w> <w n="11.7">des</w> <w n="11.8">fiancées</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">le</w> <w n="12.3">vieillard</w>, <w n="12.4">tout</w> <w n="12.5">près</w> <w n="12.6">de</w> <w n="12.7">Dieu</w>, <w n="12.8">traîne</w> <w n="12.9">sa</w> <w n="12.10">vie</w>.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><w n="13.1">Poète</w>, <w n="13.2">sois</w> <w n="13.3">un</w> <w n="13.4">arbre</w> <w n="13.5">aux</w> <w n="13.6">fruits</w> <w n="13.7">lourds</w> <w n="13.8">de</w> <w n="13.9">pensée</w>.</l>
					</lg>
				</div></body></text></TEI>