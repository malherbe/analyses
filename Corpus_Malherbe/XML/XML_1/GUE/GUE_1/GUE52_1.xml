<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VI</head><head type="main_part">A ÉMile Krantz</head><div type="poem" key="GUE52" rhyme="none">
					<head type="number">LII</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Puisque</w> <w n="1.2">l</w>’<w n="1.3">ennui</w>, <w n="1.4">pauvre</w> <w n="1.5">homme</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Te</w> <w n="2.2">jette</w> <w n="2.3">encore</w> <w n="2.4">à</w> <w n="2.5">de</w> <w n="2.6">nouveaux</w> <w n="2.7">voyages</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Emporte</w> <w n="3.2">au</w> <w n="3.3">moins</w> <w n="3.4">dans</w> <w n="3.5">l</w>’<w n="3.6">âme</w></l>
						<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">adieu</w> <w n="4.3">doré</w> <w n="4.4">des</w> <w n="4.5">beaux</w> <w n="4.6">jours</w> <w n="4.7">de</w> <w n="4.8">l</w>’<w n="4.9">automne</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Comme</w> <w n="5.2">un</w> <w n="5.3">baiser</w> <w n="5.4">l</w>’<w n="5.5">après</w>-<w n="5.6">midi</w> <w n="5.7">s</w>’<w n="5.8">achève</w> ;</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">La</w> <w n="6.2">brise</w> <w n="6.3">est</w> <w n="6.4">large</w> <w n="6.5">et</w> <w n="6.6">pure</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">toute</w> <w n="7.3">voix</w> <w n="7.4">se</w> <w n="7.5">fond</w> <w n="7.6">dans</w> <w n="7.7">le</w> <w n="7.8">murmure</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Religieux</w> <w n="8.2">des</w> <w n="8.3">chênes</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">Le</w> <w n="9.2">meunier</w> <w n="9.3">passe</w>, <w n="9.4">écoute</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Son</w> <w n="10.2">grelot</w> <w n="10.3">clair</w> <w n="10.4">tinte</w> <w n="10.5">au</w> <w n="10.6">loin</w> <w n="10.7">sur</w> <w n="10.8">la</w> <w n="10.9">route</w> ;</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Et</w> <w n="11.2">l</w>’<w n="11.3">eau</w> <w n="11.4">mélodieuse</w></l>
						<l n="12" num="3.4"><w n="12.1">Se</w> <w n="12.2">baise</w> <w n="12.3">et</w> <w n="12.4">rit</w> <w n="12.5">dans</w> <w n="12.6">les</w> <w n="12.7">canaux</w> <w n="12.8">d</w>’<w n="12.9">yeuse</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">L</w>’<w n="13.2">ombre</w> <w n="13.3">descend</w> <w n="13.4">les</w> <w n="13.5">berges</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Déjà</w> <w n="14.2">les</w> <w n="14.3">champs</w> <w n="14.4">sont</w> <w n="14.5">pleins</w> <w n="14.6">de</w> <w n="14.7">brumes</w> <w n="14.8">bleues</w> ;</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Le</w> <w n="15.2">ruisseau</w> <w n="15.3">dans</w> <w n="15.4">les</w> <w n="15.5">herbes</w></l>
						<l n="16" num="4.4"><w n="16.1">Y</w> <w n="16.2">fait</w> <w n="16.3">briller</w> <w n="16.4">ses</w> <w n="16.5">reflets</w> <w n="16.6">de</w> <w n="16.7">couleuvre</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space><w n="17.1">Ton</w> <w n="17.2">esprit</w> <w n="17.3">se</w> <w n="17.4">recueille</w> ;</l>
						<l n="18" num="5.2"><w n="18.1">Secrètement</w> <w n="18.2">le</w> <w n="18.3">cristal</w> <w n="18.4">de</w> <w n="18.5">ton</w> <w n="18.6">âme</w></l>
						<l n="19" num="5.3"><w n="19.1">S</w>’<w n="19.2">émeut</w> <w n="19.3">des</w> <w n="19.4">cris</w> <w n="19.5">d</w>’<w n="19.6">oiseaux</w>, <w n="19.7">d</w>’<w n="19.8">un</w> <w n="19.9">pas</w> <w n="19.10">de</w> <w n="19.11">femme</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Qui</w> <w n="20.2">craque</w> <w n="20.3">dans</w> <w n="20.4">les</w> <w n="20.5">feuilles</w>…</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="8"></space><w n="21.1">Regagne</w> <w n="21.2">la</w> <w n="21.3">maison</w> ;</l>
						<l n="22" num="6.2"><w n="22.1">Comme</w> <w n="22.2">toujours</w> <w n="22.3">elle</w> <w n="22.4">est</w> <w n="22.5">blanche</w>, <w n="22.6">humble</w> <w n="22.7">et</w> <w n="22.8">calme</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">sur</w> <w n="23.3">son</w> <w n="23.4">mur</w> <w n="23.5">les</w> <w n="23.6">branches</w> <w n="23.7">du</w> <w n="23.8">platane</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Entrecroisent</w> <w n="24.2">leurs</w> <w n="24.3">ombres</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Le</w> <w n="25.2">vieux</w> <w n="25.3">soleil</w>, <w n="25.4">tombant</w> <w n="25.5">en</w> <w n="25.6">molles</w> <w n="25.7">nappes</w>,</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Bénit</w> <w n="26.2">les</w> <w n="26.3">vieilles</w> <w n="26.4">pierres</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">La</w> <w n="27.2">vigne</w> <w n="27.3">jaune</w> <w n="27.4">à</w> <w n="27.5">sa</w> <w n="27.6">tiède</w> <w n="27.7">lumière</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Mûrit</w> <w n="28.2">encor</w> <w n="28.3">des</w> <w n="28.4">grappes</w> ;</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><space unit="char" quantity="8"></space><w n="29.1">Et</w> <w n="29.2">dans</w> <w n="29.3">la</w> <w n="29.4">chambre</w> <w n="29.5">basse</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Au</w> <w n="30.2">souffle</w> <w n="30.3">d</w>’<w n="30.4">air</w> <w n="30.5">qui</w> <w n="30.6">pousse</w> <w n="30.7">la</w> <w n="30.8">fenêtre</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Obscurément</w> <w n="31.2">celles</w> <w n="31.3">qui</w> <w n="31.4">t</w>’<w n="31.5">ont</w> <w n="31.6">vu</w> <w n="31.7">naître</w></l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Parlent</w> <w n="32.2">de</w> <w n="32.3">leurs</w> <w n="32.4">voix</w> <w n="32.5">lasses</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">En</w> <w n="33.2">maniant</w> <w n="33.3">leurs</w> <w n="33.4">crochets</w> <w n="33.5">à</w> <w n="33.6">dentelle</w></l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">Aux</w> <w n="34.2">derniers</w> <w n="34.3">feux</w> <w n="34.4">du</w> <w n="34.5">jour</w> :</l>
						<l n="35" num="9.3">« <w n="35.1">Reverrons</w>-<w n="35.2">nous</w> <w n="35.3">notre</w> <w n="35.4">enfant</w> ? « <w n="35.5">disent</w>-<w n="35.6">elles</w> ;</l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Et</w> <w n="36.2">la</w> <w n="36.3">tristesse</w>, <w n="36.4">aux</w> <w n="36.5">joues</w></l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><space unit="char" quantity="8"></space><w n="37.1">Des</w> <w n="37.2">aïeules</w> <w n="37.3">pensives</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Suspend</w> <w n="38.2">des</w> <w n="38.3">pleurs</w> <w n="38.4">lourds</w> <w n="38.5">comme</w> <w n="38.6">les</w> <w n="38.7">années</w> ;</l>
						<l n="39" num="10.3"><w n="39.1">Le</w> <w n="39.2">soir</w> <w n="39.3">ainsi</w> <w n="39.4">fait</w> <w n="39.5">trembler</w> <w n="39.6">sa</w> <w n="39.7">rosée</w></l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">Sur</w> <w n="40.2">les</w> <w n="40.3">roses</w> <w n="40.4">tardives</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><space unit="char" quantity="8"></space><w n="41.1">Séjour</w>, <w n="41.2">heures</w> <w n="41.3">paisibles</w> !</l>
						<l n="42" num="11.2"><w n="42.1">Demain</w> <w n="42.2">pourtant</w> <w n="42.3">les</w> <w n="42.4">premières</w> <w n="42.5">étoiles</w></l>
						<l n="43" num="11.3"><w n="43.1">Verront</w> <w n="43.2">ton</w> <w n="43.3">navire</w> <w n="43.4">arrondir</w> <w n="43.5">ses</w> <w n="43.6">voiles</w></l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space><w n="44.1">Et</w> <w n="44.2">voguer</w> <w n="44.3">vers</w> <w n="44.4">les</w> <w n="44.5">îles</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Avant</w> <w n="45.2">d</w>’<w n="45.3">aller</w> <w n="45.4">traîner</w> <w n="45.5">ta</w> <w n="45.6">vieille</w> <w n="45.7">peine</w></l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space><w n="46.1">Sur</w> <w n="46.2">de</w> <w n="46.3">lointains</w> <w n="46.4">rivages</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Écoute</w> <w n="47.2">encor</w> <w n="47.3">la</w> <w n="47.4">voix</w> <w n="47.5">grave</w> <w n="47.6">des</w> <w n="47.7">chênes</w>,</l>
						<l n="48" num="12.4"><space unit="char" quantity="8"></space><w n="48.1">Contemple</w> <w n="48.2">le</w> <w n="48.3">village</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><space unit="char" quantity="8"></space><w n="49.1">Harmonie</w> <w n="49.2">et</w> <w n="49.3">douceur</w> !</l>
						<l n="50" num="13.2"><w n="50.1">Le</w> <w n="50.2">toit</w> <w n="50.3">natal</w> <w n="50.4">fume</w> <w n="50.5">dans</w> <w n="50.6">la</w> <w n="50.7">lumière</w>…</l>
						<l n="51" num="13.3"><space unit="char" quantity="8"></space><w n="51.1">Parle</w>, <w n="51.2">dis</w>-<w n="51.3">moi</w>, <w n="51.4">mon</w> <w n="51.5">frère</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Pourquoi</w> <w n="52.2">si</w> <w n="52.3">loin</w> <w n="52.4">chercher</w> <w n="52.5">la</w> <w n="52.6">paix</w> <w n="52.7">du</w> <w n="52.8">coeur</w> ?</l>
					</lg>
				</div></body></text></TEI>