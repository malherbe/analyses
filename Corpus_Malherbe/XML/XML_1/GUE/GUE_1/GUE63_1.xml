<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">VIII</head><head type="main_part">L’INQUIETUDE DE DIEU</head><div type="poem" key="GUE63">
					<head type="number">LXIII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Celui</w> <w n="1.2">qui</w> <w n="1.3">n</w>’<w n="1.4">a</w> <w n="1.5">que</w> <w n="1.6">sa</w> <w n="1.7">tristesse</w> <w n="1.8">pour</w> <w n="1.9">génie</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Las</w> <w n="2.2">d</w>’<w n="2.3">être</w> <w n="2.4">comme</w> <w n="2.5">un</w> <w n="2.6">saule</w> <w n="2.7">au</w> <w n="2.8">feuillage</w> <w n="2.9">flottant</w></l>
						<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">pâlit</w> <w n="3.3">sous</w> <w n="3.4">la</w> <w n="3.5">lune</w> <w n="3.6">et</w> <w n="3.7">tremble</w> <w n="3.8">sous</w> <w n="3.9">le</w> <w n="3.10">vent</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Sort</w> <w n="4.2">par</w> <w n="4.3">un</w> <w n="4.4">crépuscule</w> <w n="4.5">attendri</w> <w n="4.6">du</w> <w n="4.7">printemps</w>.</l>
						<l n="5" num="1.5"><w n="5.1">L</w>’<w n="5.2">heure</w> <w n="5.3">est</w> <w n="5.4">pensive</w> <w n="5.5">et</w> <w n="5.6">pure</w> <w n="5.7">et</w> <w n="5.8">simple</w>, <w n="5.9">elle</w> <w n="5.10">est</w> <w n="5.11">bénie</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">Toute</w> <w n="6.2">chose</w> <w n="6.3">au</w> <w n="6.4">sommeil</w> <w n="6.5">s</w>’<w n="6.6">incline</w>, <w n="6.7">et</w> <w n="6.8">c</w>’<w n="6.9">est</w> <w n="6.10">la</w> <w n="6.11">fin</w></l>
						<l n="7" num="1.7"><w n="7.1">Du</w> <w n="7.2">jour</w> <w n="7.3">et</w> <w n="7.4">des</w> <w n="7.5">rumeurs</w> <w n="7.6">et</w> <w n="7.7">du</w> <w n="7.8">labeur</w> <w n="7.9">humain</w>.</l>
						<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">soir</w> <w n="8.3">avant</w> <w n="8.4">la</w> <w n="8.5">nuit</w> <w n="8.6">se</w> <w n="8.7">prolonge</w>, <w n="8.8">tranquille</w></l>
						<l n="9" num="1.9"><w n="9.1">Comme</w> <w n="9.2">au</w> <w n="9.3">pas</w> <w n="9.4">de</w> <w n="9.5">sa</w> <w n="9.6">porte</w> <w n="9.7">une</w> <w n="9.8">aïeule</w> <w n="9.9">qui</w> <w n="9.10">file</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">L</w>’<w n="10.2">acier</w> <w n="10.3">net</w> <w n="10.4">d</w>’<w n="10.5">un</w> <w n="10.6">soc</w> <w n="10.7">luit</w> <w n="10.8">sur</w> <w n="10.9">la</w> <w n="10.10">glèbe</w> ; <w n="10.11">un</w> <w n="10.12">oiseau</w></l>
						<l n="11" num="1.11"><w n="11.1">Chante</w> <w n="11.2">encore</w> <w n="11.3">parmi</w> <w n="11.4">l</w>’<w n="11.5">éternel</w> <w n="11.6">bruit</w> <w n="11.7">de</w> <w n="11.8">l</w>’<w n="11.9">eau</w> ;</l>
						<l n="12" num="1.12"><w n="12.1">Une</w> <w n="12.2">étoile</w> <w n="12.3">apparaît</w> <w n="12.4">au</w> <w n="12.5">ciel</w>… <w n="12.6">le</w> <w n="12.7">solitaire</w></l>
						<l n="13" num="1.13"><w n="13.1">Respire</w> <w n="13.2">la</w> <w n="13.3">senteur</w> <w n="13.4">puissante</w> <w n="13.5">de</w> <w n="13.6">la</w> <w n="13.7">terre</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Ce</w> <w n="14.2">soir</w> <w n="14.3">limpide</w> <w n="14.4">et</w> <w n="14.5">bleu</w> <w n="14.6">lui</w> <w n="14.7">semble</w> <w n="14.8">trop</w> <w n="14.9">obscur</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Malgré</w> <w n="15.2">là</w>-<w n="15.3">bas</w> <w n="15.4">l</w>’<w n="15.5">oiseau</w> <w n="15.6">qui</w> <w n="15.7">chante</w> <w n="15.8">et</w> <w n="15.9">l</w>’<w n="15.10">eau</w> <w n="15.11">qui</w> <w n="15.12">pleure</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">malgré</w> <w n="16.3">la</w> <w n="16.4">première</w> <w n="16.5">étoile</w> <w n="16.6">dans</w> <w n="16.7">l</w>’<w n="16.8">azur</w> ;</l>
						<l n="17" num="1.17"><w n="17.1">Ce</w> <w n="17.2">soir</w> <w n="17.3">n</w>’<w n="17.4">apporte</w> <w n="17.5">pas</w> <w n="17.6">la</w> <w n="17.7">paix</w> <w n="17.8">intérieure</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Ce</w> <w n="18.2">doux</w> <w n="18.3">soir</w> <w n="18.4">de</w> <w n="18.5">printemps</w> <w n="18.6">que</w> <w n="18.7">traverse</w> <w n="18.8">un</w> <w n="18.9">vent</w> <w n="18.10">clair</w></l>
						<l n="19" num="1.19"><w n="19.1">Ravive</w> <w n="19.2">le</w> <w n="19.3">brasier</w> <w n="19.4">défaillant</w> <w n="19.5">de</w> <w n="19.6">la</w> <w n="19.7">chair</w> ;</l>
						<l n="20" num="1.20"><w n="20.1">Et</w> <w n="20.2">l</w>’<w n="20.3">homme</w> <w n="20.4">à</w> <w n="20.5">qui</w> <w n="20.6">l</w>’<w n="20.7">amour</w> <w n="20.8">mouvant</w> <w n="20.9">comme</w> <w n="20.10">la</w> <w n="20.11">mer</w></l>
						<l n="21" num="1.21"><w n="21.1">En</w> <w n="21.2">fuyant</w> <w n="21.3">ne</w> <w n="21.4">laissa</w> <w n="21.5">que</w> <w n="21.6">l</w>’<w n="21.7">âcreté</w> <w n="21.8">d</w>’<w n="21.9">un</w> <w n="21.10">rêve</w></l>
						<l n="22" num="1.22"><w n="22.1">Pareil</w> <w n="22.2">au</w> <w n="22.3">sel</w> <w n="22.4">que</w> <w n="22.5">l</w>’<w n="22.6">onde</w> <w n="22.7">a</w> <w n="22.8">laissé</w> <w n="22.9">sur</w> <w n="22.10">la</w> <w n="22.11">grève</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Et</w> <w n="23.2">qui</w>, <w n="23.3">près</w> <w n="23.4">de</w> <w n="23.5">l</w>’<w n="23.6">amour</w> <w n="23.7">encor</w>, <w n="23.8">reste</w> <w n="23.9">indécis</w></l>
						<l n="24" num="1.24"><w n="24.1">À</w> <w n="24.2">murmurer</w> <w n="24.3">comme</w> <w n="24.4">une</w> <w n="24.5">abeille</w> <w n="24.6">au</w> <w n="24.7">bord</w> <w n="24.8">d</w>’<w n="24.9">un</w> <w n="24.10">lys</w>,</l>
						<l n="25" num="1.25"><w n="25.1">Le</w> <w n="25.2">poète</w> <w n="25.3">subtil</w> <w n="25.4">et</w> <w n="25.5">maladif</w> <w n="25.6">qu</w>’<w n="25.7">enivre</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Par</w> <w n="26.2">ce</w> <w n="26.3">soir</w> <w n="26.4">vaporeux</w> <w n="26.5">et</w> <w n="26.6">caressant</w> <w n="26.7">d</w>’<w n="26.8">avril</w>,</l>
						<l n="27" num="1.27"><w n="27.1">À</w> <w n="27.2">lui</w> <w n="27.3">fondre</w> <w n="27.4">le</w> <w n="27.5">coeur</w> <w n="27.6">la</w> <w n="27.7">volupté</w> <w n="27.8">de</w> <w n="27.9">vivre</w>,</l>
						<l n="28" num="1.28"><w n="28.1">Regrette</w> <w n="28.2">l</w>’<w n="28.3">impassible</w> <w n="28.4">austérité</w> <w n="28.5">des</w> <w n="28.6">livres</w>.</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1"><w n="29.1">Ton</w> <w n="29.2">serviteur</w> <w n="29.3">est</w> <w n="29.4">là</w>, <w n="29.5">seigneur</w>, <w n="29.6">murmure</w>-<w n="29.7">t</w>-<w n="29.8">il</w>,</l>
						<l n="30" num="2.2"><w n="30.1">Un</w> <w n="30.2">serviteur</w>, <w n="30.3">hélas</w> ! <w n="30.4">Luxurieux</w> <w n="30.5">et</w> <w n="30.6">vil</w>,</l>
						<l n="31" num="2.3"><w n="31.1">Mais</w> <w n="31.2">qui</w> <w n="31.3">sanglote</w> <w n="31.4">et</w> <w n="31.5">te</w> <w n="31.6">supplie</w> <w n="31.7">et</w> <w n="31.8">qui</w> <w n="31.9">s</w>’<w n="31.10">accuse</w> ;</l>
						<l n="32" num="2.4"><w n="32.1">Il</w> <w n="32.2">tend</w> <w n="32.3">ses</w> <w n="32.4">pauvres</w> <w n="32.5">mains</w> <w n="32.6">pleines</w> <w n="32.7">de</w> <w n="32.8">péchés</w>. <w n="32.9">Or</w></l>
						<l n="33" num="2.5"><w n="33.1">Je</w> <w n="33.2">te</w> <w n="33.3">sais</w> <w n="33.4">humble</w> <w n="33.5">avec</w> <w n="33.6">les</w> <w n="33.7">humbles</w>, <w n="33.8">dur</w> <w n="33.9">au</w> <w n="33.10">fort</w> ;</l>
						<l n="34" num="2.6"><w n="34.1">Sois</w> <w n="34.2">rude</w> <w n="34.3">au</w> <w n="34.4">pénitent</w>, <w n="34.5">flagelle</w> <w n="34.6">mes</w> <w n="34.7">sens</w>, <w n="34.8">use</w></l>
						<l n="35" num="2.7"><w n="35.1">Mes</w> <w n="35.2">genoux</w> <w n="35.3">sur</w> <w n="35.4">ton</w> <w n="35.5">seuil</w> <w n="35.6">et</w> <w n="35.7">mon</w> <w n="35.8">coeur</w> <w n="35.9">à</w> <w n="35.10">t</w>’<w n="35.11">aimer</w>,</l>
						<l n="36" num="2.8"><w n="36.1">Mais</w> <w n="36.2">laisse</w> <w n="36.3">mon</w> <w n="36.4">banc</w> <w n="36.5">vide</w> <w n="36.6">aux</w> <w n="36.7">noces</w> <w n="36.8">de</w> <w n="36.9">la</w> <w n="36.10">terre</w> ;</l>
						<l n="37" num="2.9"><w n="37.1">Veuille</w> <w n="37.2">que</w> <w n="37.3">je</w> <w n="37.4">demeure</w> <w n="37.5">à</w> <w n="37.6">jamais</w> <w n="37.7">solitaire</w>,</l>
						<l n="38" num="2.10"><w n="38.1">Épi</w> <w n="38.2">choisi</w> <w n="38.3">pour</w> <w n="38.4">le</w> <w n="38.5">bon</w> <w n="38.6">pain</w> <w n="38.7">par</w> <w n="38.8">l</w>’<w n="38.9">ouvrier</w>.</l>
						<l n="39" num="2.11"><w n="39.1">Ah</w> ! <w n="39.2">La</w> <w n="39.3">vie</w> <w n="39.4">est</w> <w n="39.5">ce</w> <w n="39.6">soir</w> <w n="39.7">trop</w> <w n="39.8">vivante</w> <w n="39.9">et</w> <w n="39.10">trop</w> <w n="39.11">belle</w>,</l>
						<l n="40" num="2.12"><w n="40.1">Le</w> <w n="40.2">désir</w> <w n="40.3">alanguit</w> <w n="40.4">l</w>’<w n="40.5">épouse</w> <w n="40.6">qui</w> <w n="40.7">m</w>’<w n="40.8">appelle</w>…</l>
						<l n="41" num="2.13"><w n="41.1">Je</w> <w n="41.2">suis</w> <w n="41.3">faible</w>, <w n="41.4">et</w> <w n="41.5">ce</w> <w n="41.6">soir</w> <w n="41.7">de</w> <w n="41.8">printemps</w> <w n="41.9">m</w>’<w n="41.10">a</w> <w n="41.11">tenté</w>.</l>
						<l n="42" num="2.14"><w n="42.1">Seigneur</w>, <w n="42.2">protège</w>-<w n="42.3">moi</w> <w n="42.4">contre</w> <w n="42.5">la</w> <w n="42.6">volupté</w>,</l>
						<l n="43" num="2.15"><w n="43.1">Donne</w> <w n="43.2">à</w> <w n="43.3">l</w>’<w n="43.4">arbre</w> <w n="43.5">la</w> <w n="43.6">fleur</w> <w n="43.7">et</w> <w n="43.8">le</w> <w n="43.9">fruit</w>, <w n="43.10">donne</w> <w n="43.11">aux</w> <w n="43.12">femmes</w></l>
						<l n="44" num="2.16"><w n="44.1">La</w> <w n="44.2">grâce</w> <w n="44.3">de</w> <w n="44.4">régler</w> <w n="44.5">la</w> <w n="44.6">musique</w> <w n="44.7">des</w> <w n="44.8">âmes</w>,</l>
						<l n="45" num="2.17"><w n="45.1">Donne</w> <w n="45.2">aux</w> <w n="45.3">prés</w> <w n="45.4">la</w> <w n="45.5">rosée</w> <w n="45.6">et</w> <w n="45.7">la</w> <w n="45.8">pluie</w>, <w n="45.9">à</w> <w n="45.10">l</w>’<w n="45.11">amant</w></l>
						<l n="46" num="2.18"><w n="46.1">La</w> <w n="46.2">tendresse</w> <w n="46.3">et</w> <w n="46.4">la</w> <w n="46.5">force</w>, <w n="46.6">et</w> <w n="46.7">l</w>’<w n="46.8">aube</w> <w n="46.9">au</w> <w n="46.10">firmament</w> :</l>
						<l n="47" num="2.19"><w n="47.1">À</w> <w n="47.2">celui</w> <w n="47.3">que</w> <w n="47.4">sa</w> <w n="47.5">chair</w> <w n="47.6">perverse</w> <w n="47.7">embrase</w>, <w n="47.8">donne</w></l>
						<l n="48" num="2.20"><w n="48.1">La</w> <w n="48.2">paix</w> <w n="48.3">chaste</w>, <w n="48.4">seigneur</w>, <w n="48.5">d</w>’<w n="48.6">un</w> <w n="48.7">immortel</w> <w n="48.8">automne</w>.</l>
					</lg>
				</div></body></text></TEI>