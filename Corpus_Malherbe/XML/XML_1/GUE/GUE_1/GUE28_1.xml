<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">MÉLANCOLIES A VIOLLIS</head><div type="poem" key="GUE28">
					<head type="number">XXVIII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">maison</w> <w n="1.3">serait</w> <w n="1.4">blanche</w> <w n="1.5">et</w> <w n="1.6">le</w> <w n="1.7">jardin</w> <w n="1.8">sonore</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">De</w> <w n="2.2">bruits</w> <w n="2.3">d</w>’<w n="2.4">eaux</w> <w n="2.5">vives</w> <w n="2.6">et</w> <w n="2.7">d</w>’<w n="2.8">oiseaux</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">le</w> <w n="3.3">lierre</w> <w n="3.4">du</w> <w n="3.5">mur</w> <w n="3.6">qui</w> <w n="3.7">regarde</w> <w n="3.8">l</w>’<w n="3.9">aurore</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Broderait</w> <w n="4.2">d</w>’<w n="4.3">ombres</w> <w n="4.4">les</w> <w n="4.5">rideaux</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Du</w> <w n="5.2">lit</w> <w n="5.3">tiède</w> <w n="5.4">où</w>, <w n="5.5">mêlés</w> <w n="5.6">comme</w> <w n="5.7">deux</w> <w n="5.8">tourterelles</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Las</w> <w n="6.2">d</w>’<w n="6.3">un</w> <w n="6.4">voluptueux</w> <w n="6.5">sommeil</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Nous</w> <w n="7.2">souririons</w>, <w n="7.3">heureux</w> <w n="7.4">de</w> <w n="7.5">nous</w> <w n="7.6">sentir</w> <w n="7.7">des</w> <w n="7.8">ailes</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Aux</w> <w n="8.2">premiers</w> <w n="8.3">rayons</w> <w n="8.4">du</w> <w n="8.5">soleil</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Cette</w> <w n="9.2">maison</w> <w n="9.3">n</w>’<w n="9.4">aurait</w> <w n="9.5">sous</w> <w n="9.6">l</w>’<w n="9.7">auvent</w> <w n="9.8">qu</w>’<w n="9.9">un</w> <w n="9.10">étage</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Au</w> <w n="10.2">balcon</w> <w n="10.3">noyé</w> <w n="10.4">de</w> <w n="10.5">jasmins</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">fleurs</w>, <w n="11.3">le</w> <w n="11.4">miel</w>, <w n="11.5">ô</w> <w n="11.6">mon</w> <w n="11.7">amie</w>, <w n="11.8">et</w> <w n="11.9">le</w> <w n="11.10">laitage</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Aromatiseraient</w> <w n="12.2">tes</w> <w n="12.3">mains</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Un</w> <w n="13.2">fleuve</w> <w n="13.3">baignerait</w> <w n="13.4">nos</w> <w n="13.5">vergers</w>, <w n="13.6">et</w> <w n="13.7">sa</w> <w n="13.8">rive</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Cacherait</w> <w n="14.2">parmi</w> <w n="14.3">les</w> <w n="14.4">roseaux</w></l>
						<l n="15" num="4.3"><w n="15.1">Une</w> <w n="15.2">barque</w> <w n="15.3">bercée</w> <w n="15.4">et</w> <w n="15.5">dont</w> <w n="15.6">la</w> <w n="15.7">rame</w> <w n="15.8">oisive</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Miroite</w> <w n="16.2">en</w> <w n="16.3">divisant</w> <w n="16.4">les</w> <w n="16.5">eaux</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Nous</w> <w n="17.2">resterions</w> <w n="17.3">longtemps</w> <w n="17.4">assis</w> <w n="17.5">sur</w> <w n="17.6">la</w> <w n="17.7">terrasse</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Le</w> <w n="18.2">soir</w>, <w n="18.3">lorsqu</w>’<w n="18.4">entre</w> <w n="18.5">ciel</w> <w n="18.6">et</w> <w n="18.7">champ</w></l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">piétinant</w> <w n="19.3">troupeau</w> <w n="19.4">pressé</w> <w n="19.5">des</w> <w n="19.6">brebis</w> <w n="19.7">passe</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Dans</w> <w n="20.2">la</w> <w n="20.3">lumière</w> <w n="20.4">du</w> <w n="20.5">couchant</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">nos</w> <w n="21.3">coeurs</w> <w n="21.4">répondraient</w> <w n="21.5">à</w> <w n="21.6">l</w>’<w n="21.7">angelus</w> <w n="21.8">qui</w> <w n="21.9">sonne</w></l>
						<l n="22" num="6.2"><w n="22.1">Avec</w> <w n="22.2">la</w> <w n="22.3">foi</w> <w n="22.4">des</w> <w n="22.5">coeurs</w> <w n="22.6">à</w> <w n="22.7">qui</w> <w n="22.8">la</w> <w n="22.9">vie</w> <w n="22.10">est</w> <w n="22.11">bonne</w>.</l>
					</lg>
					<lg n="7">
						<l n="23" num="7.1"><w n="23.1">Plus</w> <w n="23.2">tard</w>, <w n="23.3">sur</w> <w n="23.4">le</w> <w n="23.5">balcon</w> <w n="23.6">rempli</w> <w n="23.7">d</w>’<w n="23.8">ombre</w>, <w n="23.9">muets</w>,</l>
						<l n="24" num="7.2"><w n="24.1">L</w>’<w n="24.2">oreille</w> <w n="24.3">ouverte</w> <w n="24.4">au</w> <w n="24.5">bruit</w> <w n="24.6">des</w> <w n="24.7">trains</w> <w n="24.8">dans</w> <w n="24.9">la</w> <w n="24.10">vallée</w>,</l>
						<l n="25" num="7.3"><w n="25.1">Goûtant</w> <w n="25.2">tout</w> <w n="25.3">ce</w> <w n="25.4">qu</w>’<w n="25.5">un</w> <w n="25.6">sage</w> <w n="25.7">amour</w> <w n="25.8">contient</w> <w n="25.9">de</w> <w n="25.10">paix</w>,</l>
						<l n="26" num="7.4"><w n="26.1">Nos</w> <w n="26.2">âmes</w> <w n="26.3">se</w> <w n="26.4">fondraient</w> <w n="26.5">dans</w> <w n="26.6">la</w> <w n="26.7">nuit</w> <w n="26.8">étoilée</w>.</l>
					</lg>
					<lg n="8">
						<l n="27" num="8.1"><w n="27.1">Écoutant</w> <w n="27.2">nos</w> <w n="27.3">enfants</w> <w n="27.4">dormir</w> <w n="27.5">derrière</w> <w n="27.6">nous</w>,</l>
						<l n="28" num="8.2"><w n="28.1">Pâle</w> <w n="28.2">dans</w> <w n="28.3">tes</w> <w n="28.4">cheveux</w> <w n="28.5">libres</w> <w n="28.6">où</w> <w n="28.7">l</w>’<w n="28.8">air</w> <w n="28.9">se</w> <w n="28.10">joue</w>,</l>
						<l n="29" num="8.3"><w n="29.1">Ta</w> <w n="29.2">main</w> <w n="29.3">fraîche</w> <w n="29.4">liée</w> <w n="29.5">aux</w> <w n="29.6">miennes</w> : « <w n="29.7">qu</w>’<w n="29.8">il</w> <w n="29.9">est</w> <w n="29.10">doux</w>,</l>
						<l n="30" num="8.4"><w n="30.1">Qu</w>’<w n="30.2">il</w> <w n="30.3">est</w> <w n="30.4">doux</w>, <w n="30.5">dirais</w>-<w n="30.6">tu</w>, <w n="30.7">les</w> <w n="30.8">cils</w> <w n="30.9">contre</w> <w n="30.10">ma</w> <w n="30.11">joue</w>,</l>
					</lg>
					<lg n="9">
						<l n="31" num="9.1"><w n="31.1">Quand</w> <w n="31.2">on</w> <w n="31.3">sait</w> <w n="31.4">où</w> <w n="31.5">poser</w> <w n="31.6">la</w> <w n="31.7">tête</w>, <w n="31.8">d</w>’<w n="31.9">être</w> <w n="31.10">las</w> !  »</l>
						<l n="32" num="9.2"><w n="32.1">Mes</w> <w n="32.2">lèvres</w> <w n="32.3">fermeraient</w> <w n="32.4">ta</w> <w n="32.5">paupière</w> <w n="32.6">endormie</w>.</l>
						<l n="33" num="9.3"><w n="33.1">Cher</w> <w n="33.2">asile</w>, <w n="33.3">jardin</w>, <w n="33.4">maison</w> <w n="33.5">rustique</w>… <w n="33.6">hélas</w> !</l>
						<l n="34" num="9.4"><w n="34.1">Car</w> <w n="34.2">nous</w> <w n="34.3">rêvons</w> <w n="34.4">quand</w> <w n="34.5">il</w> <w n="34.6">faut</w> <w n="34.7">vivre</w>, <w n="34.8">ô</w> <w n="34.9">mon</w> <w n="34.10">amie</w> !</l>
					</lg>
				</div></body></text></TEI>