<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">MÉLANCOLIES A VIOLLIS</head><div type="poem" key="GUE34" rhyme="none">
					<head type="number">XXXIV</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">la</w> <w n="1.3">nuit</w> <w n="1.4">m</w>’<w n="1.5">enveloppe</w> <w n="1.6">et</w> <w n="1.7">dorlote</w> <w n="1.8">ma</w> <w n="1.9">peine</w></l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">toute</w> <w n="2.3">sa</w> <w n="2.4">bonté</w>, <w n="2.5">de</w> <w n="2.6">toute</w> <w n="2.7">sa</w> <w n="2.8">douceur</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">les</w> <w n="3.3">flocons</w> <w n="3.4">légers</w> <w n="3.5">de</w> <w n="3.6">la</w> <w n="3.7">neige</w> <w n="3.8">à</w> <w n="3.9">mon</w> <w n="3.10">coeur</w></l>
						<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">enroulent</w> <w n="4.3">comme</w> <w n="4.4">au</w> <w n="4.5">noir</w> <w n="4.6">rouet</w> <w n="4.7">la</w> <w n="4.8">blanche</w> <w n="4.9">laine</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">chambre</w> <w n="5.3">est</w> <w n="5.4">une</w> <w n="5.5">tendre</w> <w n="5.6">aïeule</w> <w n="5.7">qui</w> <w n="5.8">me</w> <w n="5.9">berce</w></l>
						<l n="6" num="2.2"><w n="6.1">Des</w> <w n="6.2">chansons</w> <w n="6.3">qui</w> <w n="6.4">berçaient</w> <w n="6.5">mon</w> <w n="6.6">enfance</w> <w n="6.7">première</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">ces</w> <w n="7.3">chansons</w> <w n="7.4">font</w> <w n="7.5">battre</w> <w n="7.6">et</w> <w n="7.7">mouillent</w> <w n="7.8">mes</w> <w n="7.9">paupière</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Avec</w> <w n="8.2">l</w>’<w n="8.3">anxiété</w> <w n="8.4">des</w> <w n="8.5">feuilles</w> <w n="8.6">sous</w> <w n="8.7">l</w>’<w n="8.8">averse</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">déjà</w> <w n="9.4">le</w> <w n="9.5">sommeil</w> <w n="9.6">où</w> <w n="9.7">soupirent</w> <w n="9.8">les</w> <w n="9.9">choses</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Une</w> <w n="10.2">agonie</w> <w n="10.3">indéfinie</w> <w n="10.4">et</w> <w n="10.5">le</w> <w n="10.6">silence</w></l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">l</w>’<w n="11.3">ombre</w> <w n="11.4">où</w> <w n="11.5">l</w>’<w n="11.6">on</w> <w n="11.7">entend</w> <w n="11.8">tinter</w> <w n="11.9">d</w>’<w n="11.10">un</w> <w n="11.11">timbre</w> <w n="11.12">étrange</w></l>
						<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2">horloge</w> <w n="12.3">au</w> <w n="12.4">cadran</w> <w n="12.5">jaune</w> <w n="12.6">enguirlandé</w> <w n="12.7">de</w> <w n="12.8">roses</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Nue</w> <w n="13.2">et</w> <w n="13.3">câline</w>, <w n="13.4">sous</w> <w n="13.5">son</w> <w n="13.6">voile</w> <w n="13.7">de</w> <w n="13.8">dentelles</w>,</l>
						<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2">heure</w> <w n="14.3">aux</w> <w n="14.4">doigts</w> <w n="14.5">sourds</w>, <w n="14.6">au</w> <w n="14.7">pas</w> <w n="14.8">flexible</w>, <w n="14.9">ah</w> ! <w n="14.10">Vienne</w>-<w n="14.11">t</w>-<w n="14.12">elle</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Qui</w> <w n="15.2">fait</w> <w n="15.3">mourir</w> <w n="15.4">les</w> <w n="15.5">feux</w> <w n="15.6">au</w> <w n="15.7">fond</w> <w n="15.8">des</w> <w n="15.9">chambres</w> <w n="15.10">lasses</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Qui</w> <w n="16.2">fait</w> <w n="16.3">mourir</w> <w n="16.4">l</w>’<w n="16.5">amour</w> <w n="16.6">dans</w> <w n="16.7">la</w> <w n="16.8">cendre</w> <w n="16.9">des</w> <w n="16.10">âmes</w>.</l>
					</lg>
				</div></body></text></TEI>