<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">A LA MEMOIRE DE SAMAIN</head><div type="poem" key="GUE46">
					<head type="number">XLVI</head>
					<opener>
						<salute>A José-Maria De Hérédia.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Être</w> <w n="1.2">le</w> <w n="1.3">jeune</w> <w n="1.4">Adam</w>, <w n="1.5">grâce</w> <w n="1.6">et</w> <w n="1.7">force</w> <w n="1.8">première</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">les</w> <w n="2.3">yeux</w> <w n="2.4">lourds</w> <w n="2.5">encor</w> <w n="2.6">s</w>’<w n="2.7">ouvrent</w> <w n="2.8">à</w> <w n="2.9">la</w> <w n="2.10">lumière</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">s</w>’<w n="3.3">étonne</w>, <w n="3.4">se</w> <w n="3.5">tait</w>, <w n="3.6">regarde</w> <w n="3.7">autour</w> <w n="3.8">de</w> <w n="3.9">lui</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Marche</w> <w n="4.2">avec</w> <w n="4.3">les</w> <w n="4.4">lenteurs</w> <w n="4.5">d</w>’<w n="4.6">un</w> <w n="4.7">enfant</w> <w n="4.8">ébloui</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Se</w> <w n="5.2">voit</w> <w n="5.3">nu</w>, <w n="5.4">se</w> <w n="5.5">caresse</w> <w n="5.6">et</w> <w n="5.7">s</w>’<w n="5.8">admire</w>, <w n="5.9">et</w> <w n="5.10">soudain</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Enivré</w> <w n="6.2">par</w> <w n="6.3">l</w>’<w n="6.4">odeur</w> <w n="6.5">des</w> <w n="6.6">sèves</w> <w n="6.7">de</w> <w n="6.8">l</w>’<w n="6.9">éden</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Gravit</w> <w n="7.2">d</w>’<w n="7.3">un</w> <w n="7.4">jarret</w> <w n="7.5">prompt</w> <w n="7.6">les</w> <w n="7.7">collines</w> <w n="7.8">bleuâtres</w></l>
						<l n="8" num="1.8"><w n="8.1">Parmi</w> <w n="8.2">l</w>’<w n="8.3">herbe</w> <w n="8.4">mouillée</w> <w n="8.5">et</w> <w n="8.6">les</w> <w n="8.7">troupeaux</w> <w n="8.8">sans</w> <w n="8.9">pâtres</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Il</w> <w n="9.2">s</w>’<w n="9.3">arrête</w> <w n="9.4">à</w> <w n="9.5">goûter</w> <w n="9.6">l</w>’<w n="9.7">ombre</w> <w n="9.8">de</w> <w n="9.9">la</w> <w n="9.10">forêt</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Et</w>, <w n="10.2">couchant</w>, <w n="10.3">près</w> <w n="10.4">d</w>’<w n="10.5">une</w> <w n="10.6">eau</w> <w n="10.7">qui</w> <w n="10.8">murmure</w> <w n="10.9">en</w> <w n="10.10">secret</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Son</w> <w n="11.2">corps</w> <w n="11.3">souple</w> <w n="11.4">où</w> <w n="11.5">la</w> <w n="11.6">ronce</w> <w n="11.7">inoffensive</w> <w n="11.8">glisse</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Il</w> <w n="12.2">respire</w> <w n="12.3">l</w>’<w n="12.4">air</w> <w n="12.5">neuf</w> <w n="12.6">de</w> <w n="12.7">l</w>’<w n="12.8">aube</w> <w n="12.9">avec</w> <w n="12.10">délice</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Chevauche</w> <w n="13.2">le</w> <w n="13.3">rayon</w>, <w n="13.4">chante</w>, <w n="13.5">éveille</w> <w n="13.6">l</w>’<w n="13.7">écho</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Sourit</w> <w n="14.2">à</w> <w n="14.3">l</w>’<w n="14.4">inconnu</w> <w n="14.5">qu</w>’<w n="14.6">il</w> <w n="14.7">voit</w>, <w n="14.8">dans</w> <w n="14.9">le</w> <w n="14.10">ruisseau</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Vermeil</w> <w n="15.2">et</w> <w n="15.3">languissant</w> <w n="15.4">de</w> <w n="15.5">bonheur</w>, <w n="15.6">lui</w> <w n="15.7">sourire</w>.</l>
						<l n="16" num="1.16"><w n="16.1">Un</w> <w n="16.2">large</w> <w n="16.3">papillon</w> <w n="16.4">qui</w> <w n="16.5">se</w> <w n="16.6">pose</w> <w n="16.7">l</w>’<w n="16.8">attire</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Deux</w> <w n="17.2">chevreaux</w> <w n="17.3">affrontés</w> <w n="17.4">le</w> <w n="17.5">charment</w> <w n="17.6">par</w> <w n="17.7">leurs</w> <w n="17.8">jeux</w>.</l>
						<l n="18" num="1.18"><w n="18.1">Mais</w> <w n="18.2">le</w> <w n="18.3">rouge</w> <w n="18.4">zénith</w> <w n="18.5">épanouit</w> <w n="18.6">ses</w> <w n="18.7">feux</w>.</l>
						<l n="19" num="1.19"><w n="19.1">Adam</w> <w n="19.2">que</w> <w n="19.3">le</w> <w n="19.4">désir</w> <w n="19.5">caresse</w> <w n="19.6">de</w> <w n="19.7">son</w> <w n="19.8">aile</w></l>
						<l n="20" num="1.20"><w n="20.1">Étreint</w> <w n="20.2">entre</w> <w n="20.3">ses</w> <w n="20.4">bras</w> <w n="20.5">la</w> <w n="20.6">terre</w> <w n="20.7">maternelle</w>.</l>
						<l n="21" num="1.21"><w n="21.1">Sar</w> <w n="21.2">chair</w> <w n="21.3">où</w> <w n="21.4">le</w> <w n="21.5">limon</w> <w n="21.6">se</w> <w n="21.7">mêle</w> <w n="21.8">avec</w> <w n="21.9">le</w> <w n="21.10">jour</w></l>
						<l n="22" num="1.22"><w n="22.1">Appelle</w> <w n="22.2">sourdement</w> <w n="22.3">une</w> <w n="22.4">épouse</w> <w n="22.5">et</w> <w n="22.6">l</w>’<w n="22.7">amour</w> ;</l>
						<l n="23" num="1.23"><w n="23.1">Il</w> <w n="23.2">meurtrit</w> <w n="23.3">de</w> <w n="23.4">baisers</w> <w n="23.5">l</w>’<w n="23.6">arbre</w>, <w n="23.7">embrasse</w> <w n="23.8">l</w>’<w n="23.9">écorce</w>,</l>
						<l n="24" num="1.24"><w n="24.1">S</w>’<w n="24.2">épuise</w> <w n="24.3">à</w> <w n="24.4">déplorer</w> <w n="24.5">son</w> <w n="24.6">inutile</w> <w n="24.7">force</w>…</l>
						<l n="25" num="1.25"><w n="25.1">Bientôt</w> <w n="25.2">la</w> <w n="25.3">lente</w> <w n="25.4">nuit</w>, <w n="25.5">le</w> <w n="25.6">remplissant</w> <w n="25.7">d</w>’<w n="25.8">horreur</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Recouvre</w> <w n="26.2">d</w>’<w n="26.3">une</w> <w n="26.4">ruche</w> <w n="26.5">obscure</w> <w n="26.6">la</w> <w n="26.7">rumeur</w></l>
						<l n="27" num="1.27"><w n="27.1">Que</w> <w n="27.2">font</w> <w n="27.3">les</w> <w n="27.4">eaux</w> <w n="27.5">des</w> <w n="27.6">mers</w> <w n="27.7">et</w> <w n="27.8">le</w> <w n="27.9">vent</w> <w n="27.10">des</w> <w n="27.11">ravines</w>.</l>
						<l n="28" num="1.28"><w n="28.1">La</w> <w n="28.2">lune</w>, <w n="28.3">pâle</w> <w n="28.4">encor</w>, <w n="28.5">change</w> <w n="28.6">sur</w> <w n="28.7">les</w> <w n="28.8">collines</w></l>
						<l n="29" num="1.29"><w n="29.1">Toute</w> <w n="29.2">rosée</w> <w n="29.3">en</w> <w n="29.4">perle</w> <w n="29.5">et</w> <w n="29.6">toute</w> <w n="29.7">fleur</w> <w n="29.8">en</w> <w n="29.9">lys</w>.</l>
						<l n="30" num="1.30"><w n="30.1">La</w> <w n="30.2">brise</w> <w n="30.3">porte</w> <w n="30.4">au</w> <w n="30.5">loin</w> <w n="30.6">des</w> <w n="30.7">échos</w> <w n="30.8">affaiblis</w></l>
						<l n="31" num="1.31"><w n="31.1">De</w> <w n="31.2">ramages</w>, <w n="31.3">d</w>’<w n="31.4">appels</w> <w n="31.5">amoureux</w>, <w n="31.6">de</w> <w n="31.7">murmures</w>.</l>
						<l n="32" num="1.32"><w n="32.1">Le</w> <w n="32.2">flot</w> <w n="32.3">paresseux</w> <w n="32.4">roule</w> <w n="32.5">un</w> <w n="32.6">ciel</w> <w n="32.7">d</w>’<w n="32.8">étoiles</w> <w n="32.9">pures</w>,</l>
						<l n="33" num="1.33"><w n="33.1">Et</w>, <w n="33.2">sous</w> <w n="33.3">les</w> <w n="33.4">voûtes</w> <w n="33.5">d</w>’<w n="33.6">ombre</w> <w n="33.7">où</w> <w n="33.8">les</w> <w n="33.9">grands</w> <w n="33.10">animaux</w></l>
						<l n="34" num="1.34"><w n="34.1">D</w>’<w n="34.2">un</w> <w n="34.3">front</w> <w n="34.4">lourd</w> <w n="34.5">en</w> <w n="34.6">passant</w> <w n="34.7">écartent</w> <w n="34.8">les</w> <w n="34.9">rameaux</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Le</w> <w n="35.2">jeune</w> <w n="35.3">Adam</w>, <w n="35.4">muet</w> <w n="35.5">d</w>’<w n="35.6">ivresse</w> <w n="35.7">et</w> <w n="35.8">d</w>’<w n="35.9">épouvante</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Dans</w> <w n="36.2">ses</w> <w n="36.3">flancs</w> <w n="36.4">douloureux</w> <w n="36.5">sent</w> <w n="36.6">vivre</w> <w n="36.7">Ève</w> <w n="36.8">naissante</w>.</l>
						<l n="37" num="1.37"><w n="37.1">Adam</w>, <w n="37.2">le</w> <w n="37.3">front</w> <w n="37.4">rougi</w> <w n="37.5">du</w> <w n="37.6">soleil</w> <w n="37.7">levant</w>, <w n="37.8">rêve</w></l>
						<l n="38" num="1.38"><w n="38.1">Auprès</w> <w n="38.2">du</w> <w n="38.3">corps</w> <w n="38.4">humide</w> <w n="38.5">et</w> <w n="38.6">voluptueux</w> <w n="38.7">d</w>’<w n="38.8">Ève</w>.</l>
						<l n="39" num="1.39"><w n="39.1">Ève</w> <w n="39.2">dort</w> <w n="39.3">sur</w> <w n="39.4">un</w> <w n="39.5">lit</w> <w n="39.6">fléchissant</w> <w n="39.7">de</w> <w n="39.8">roseaux</w>,</l>
						<l n="40" num="1.40"><w n="40.1">Dans</w> <w n="40.2">l</w>’<w n="40.3">azur</w> <w n="40.4">frais</w>, <w n="40.5">au</w> <w n="40.6">bruit</w> <w n="40.7">du</w> <w n="40.8">feuillage</w> <w n="40.9">et</w> <w n="40.10">des</w> <w n="40.11">eaux</w>.</l>
						<l n="41" num="1.41"><w n="41.1">Ève</w> <w n="41.2">est</w> <w n="41.3">nue</w>, <w n="41.4">Ève</w> <w n="41.5">est</w> <w n="41.6">blanche</w>, <w n="41.7">Ève</w> <w n="41.8">a</w> <w n="41.9">les</w> <w n="41.10">lignes</w> <w n="41.11">pures</w></l>
						<l n="42" num="1.42"><w n="42.1">Des</w> <w n="42.2">longs</w> <w n="42.3">cygnes</w> <w n="42.4">cambrés</w> <w n="42.5">aux</w> <w n="42.6">neigeuses</w> <w n="42.7">voilures</w>.</l>
						<l n="43" num="1.43"><w n="43.1">Comme</w> <w n="43.2">une</w> <w n="43.3">aile</w> <w n="43.4">elle</w> <w n="43.5">agite</w> <w n="43.6">un</w> <w n="43.7">bras</w>, <w n="43.8">puis</w> <w n="43.9">l</w>’<w n="43.10">autre</w>, <w n="43.11">et</w> <w n="43.12">rit</w>.</l>
						<l n="44" num="1.44"><w n="44.1">Sa</w> <w n="44.2">bouche</w>, <w n="44.3">rose</w> <w n="44.4">en</w> <w n="44.5">feu</w>, <w n="44.6">lente</w> <w n="44.7">à</w> <w n="44.8">s</w>’<w n="44.9">ouvrir</w>, <w n="44.10">fleurit</w>.</l>
						<l n="45" num="1.45"><w n="45.1">Ève</w> <w n="45.2">est</w> <w n="45.3">nue</w> ; <w n="45.4">elle</w> <w n="45.5">dort</w>. <w n="45.6">Sa</w> <w n="45.7">chevelure</w> <w n="45.8">blonde</w></l>
						<l n="46" num="1.46"><w n="46.1">Sur</w> <w n="46.2">ses</w> <w n="46.3">formes</w> <w n="46.4">répand</w> <w n="46.5">les</w> <w n="46.6">mollesses</w> <w n="46.7">d</w>’<w n="46.8">une</w> <w n="46.9">onde</w>.</l>
						<l n="47" num="1.47"><w n="47.1">Son</w> <w n="47.2">haleine</w> <w n="47.3">paisible</w> <w n="47.4">élève</w> <w n="47.5">un</w> <w n="47.6">double</w> <w n="47.7">fruit</w></l>
						<l n="48" num="1.48"><w n="48.1">Gonflé</w> <w n="48.2">qui</w> <w n="48.3">tour</w> <w n="48.4">à</w> <w n="48.5">tour</w> <w n="48.6">se</w> <w n="48.7">rapproche</w> <w n="48.8">et</w> <w n="48.9">se</w> <w n="48.10">fuit</w>.</l>
						<l n="49" num="1.49"><w n="49.1">Adam</w> <w n="49.2">parcourt</w> <w n="49.3">des</w> <w n="49.4">yeux</w> <w n="49.5">sa</w> <w n="49.6">fille</w> <w n="49.7">et</w> <w n="49.8">la</w> <w n="49.9">convoite</w>.</l>
						<l n="50" num="1.50"><w n="50.1">Il</w> <w n="50.2">ose</w> <w n="50.3">la</w> <w n="50.4">flatter</w> <w n="50.5">d</w>’<w n="50.6">une</w> <w n="50.7">main</w> <w n="50.8">maladroite</w>.</l>
						<l n="51" num="1.51"><w n="51.1">Jamais</w> <w n="51.2">dans</w> <w n="51.3">la</w> <w n="51.4">nature</w> <w n="51.5">heureuse</w> <w n="51.6">il</w> <w n="51.7">n</w>’<w n="51.8">a</w> <w n="51.9">connu</w></l>
						<l n="52" num="1.52"><w n="52.1">Le</w> <w n="52.2">délice</w> <w n="52.3">qu</w>’<w n="52.4">il</w> <w n="52.5">goûte</w> <w n="52.6">à</w> <w n="52.7">toucher</w> <w n="52.8">ce</w> <w n="52.9">sein</w> <w n="52.10">nu</w>.</l>
						<l n="53" num="1.53"><w n="53.1">Les</w> <w n="53.2">cheveux</w> <w n="53.3">qu</w>’<w n="53.4">il</w> <w n="53.5">respire</w> <w n="53.6">ont</w> <w n="53.7">une</w> <w n="53.8">odeur</w> <w n="53.9">obscure</w> ;</l>
						<l n="54" num="1.54"><w n="54.1">Ni</w> <w n="54.2">le</w> <w n="54.3">miel</w>, <w n="54.4">ni</w> <w n="54.5">le</w> <w n="54.6">col</w> <w n="54.7">des</w> <w n="54.8">cygnes</w> <w n="54.9">qu</w>’<w n="54.10">il</w> <w n="54.11">capture</w>,</l>
						<l n="55" num="1.55"><w n="55.1">Ni</w> <w n="55.2">la</w> <w n="55.3">feuille</w> <w n="55.4">du</w> <w n="55.5">lys</w>, <w n="55.6">ni</w> <w n="55.7">l</w>’<w n="55.8">enivrant</w> <w n="55.9">pollen</w>,</l>
						<l n="56" num="1.56"><w n="56.1">Ne</w> <w n="56.2">lui</w> <w n="56.3">semblent</w> <w n="56.4">si</w> <w n="56.5">doux</w> <w n="56.6">sous</w> <w n="56.7">le</w> <w n="56.8">ciel</w> <w n="56.9">de</w> <w n="56.10">l</w>’<w n="56.11">éden</w></l>
						<l n="57" num="1.57"><w n="57.1">Que</w> <w n="57.2">cette</w> <w n="57.3">large</w> <w n="57.4">fleur</w> <w n="57.5">de</w> <w n="57.6">chair</w> <w n="57.7">épanouie</w>.</l>
						<l n="58" num="1.58"><w n="58.1">Il</w> <w n="58.2">la</w> <w n="58.3">caresse</w> <w n="58.4">encor</w> <w n="58.5">d</w>’<w n="58.6">une</w> <w n="58.7">vue</w> <w n="58.8">éblouie</w>,</l>
						<l n="59" num="1.59"><w n="59.1">Y</w> <w n="59.2">porte</w> <w n="59.3">son</w> <w n="59.4">désir</w> <w n="59.5">de</w> <w n="59.6">contour</w> <w n="59.7">en</w> <w n="59.8">contour</w> ;</l>
						<l n="60" num="1.60"><w n="60.1">Enfin</w>, <w n="60.2">docile</w> <w n="60.3">aux</w> <w n="60.4">lois</w> <w n="60.5">secrètes</w> <w n="60.6">de</w> <w n="60.7">l</w>’<w n="60.8">amour</w></l>
						<l n="61" num="1.61"><w n="61.1">Qui</w> <w n="61.2">font</w> <w n="61.3">qu</w>’<w n="61.4">un</w> <w n="61.5">même</w> <w n="61.6">nid</w> <w n="61.7">cache</w> <w n="61.8">deux</w> <w n="61.9">tourterelles</w></l>
						<l n="62" num="1.62"><w n="62.1">Et</w> <w n="62.2">que</w> <w n="62.3">les</w> <w n="62.4">fleurs</w> <w n="62.5">de</w> <w n="62.6">loin</w> <w n="62.7">se</w> <w n="62.8">fécondent</w> <w n="62.9">entre</w> <w n="62.10">elles</w>,</l>
						<l n="63" num="1.63"><w n="63.1">Adam</w>, <w n="63.2">fort</w> <w n="63.3">de</w> <w n="63.4">la</w> <w n="63.5">joie</w> <w n="63.6">immortelle</w> <w n="63.7">du</w> <w n="63.8">sang</w>,</l>
						<l n="64" num="1.64"><w n="64.1">Presse</w> <w n="64.2">le</w> <w n="64.3">corps</w> <w n="64.4">promis</w>, <w n="64.5">et</w> <w n="64.6">déjà</w> <w n="64.7">rougissant</w>,</l>
						<l n="65" num="1.65"><w n="65.1">D</w>’<w n="65.2">un</w> <w n="65.3">long</w> <w n="65.4">baiser</w> <w n="65.5">qui</w> <w n="65.6">laisse</w> <w n="65.7">une</w> <w n="65.8">trace</w> <w n="65.9">vermeille</w>.</l>
						<l n="66" num="1.66"><w n="66.1">Ève</w>, <w n="66.2">les</w> <w n="66.3">bras</w> <w n="66.4">ouverts</w> <w n="66.5">au</w> <w n="66.6">jeune</w> <w n="66.7">époux</w>, <w n="66.8">s</w>’<w n="66.9">éveille</w>.</l>
					</lg>
				</div></body></text></TEI>