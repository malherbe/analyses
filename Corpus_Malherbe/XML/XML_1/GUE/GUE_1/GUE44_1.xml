<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">A LA MEMOIRE DE SAMAIN</head><div type="poem" key="GUE44">
					<head type="number">XLIV</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">ton</w> <w n="1.3">décor</w> <w n="1.4">naïf</w> <w n="1.5">tu</w> <w n="1.6">m</w>’<w n="1.7">apparais</w>, <w n="1.8">Jenny</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Doux</w> <w n="2.2">fantôme</w> <w n="2.3">où</w> <w n="2.4">revit</w> <w n="2.5">la</w> <w n="2.6">romance</w>, <w n="2.7">ouvrière</w></l>
						<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">brodais</w> <w n="3.3">en</w> <w n="3.4">levant</w> <w n="3.5">parfois</w> <w n="3.6">sur</w> <w n="3.7">l</w>’<w n="3.8">infini</w></l>
						<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">cils</w>-<w n="4.3">rêveurs</w> <w n="4.4">et</w> <w n="4.5">deux</w> <w n="4.6">yeux</w> <w n="4.7">purs</w> <w n="4.8">pleins</w> <w n="4.9">de</w> <w n="4.10">prière</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ta</w> <w n="5.2">mansarde</w> <w n="5.3">fleurie</w> <w n="5.4">ouvre</w> <w n="5.5">sur</w> <w n="5.6">l</w>’<w n="5.7">orient</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Prompte</w> <w n="6.2">à</w> <w n="6.3">quitter</w> <w n="6.4">ton</w> <w n="6.5">lit</w> <w n="6.6">de</w> <w n="6.7">vierge</w>, <w n="6.8">tu</w> <w n="6.9">vois</w> <w n="6.10">naître</w></l>
						<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">aube</w> <w n="7.3">qui</w> <w n="7.4">te</w> <w n="7.5">regarde</w> <w n="7.6">à</w> <w n="7.7">son</w> <w n="7.8">tour</w> <w n="7.9">en</w> <w n="7.10">riant</w></l>
						<l n="8" num="2.4"><w n="8.1">Arroser</w>, <w n="8.2">ô</w> <w n="8.3">Jenny</w>, <w n="8.4">les</w> <w n="8.5">lys</w> <w n="8.6">de</w> <w n="8.7">ta</w> <w n="8.8">fenêtre</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">À</w> <w n="9.2">ton</w> <w n="9.3">poignet</w> <w n="9.4">glissant</w> <w n="9.5">déjà</w> <w n="9.6">ta</w> <w n="9.7">boîte</w> <w n="9.8">au</w> <w n="9.9">lait</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Devant</w> <w n="10.2">le</w> <w n="10.3">bruit</w> <w n="10.4">que</w> <w n="10.5">font</w> <w n="10.6">tes</w> <w n="10.7">colombes</w> <w n="10.8">entre</w> <w n="10.9">elles</w></l>
						<l n="11" num="3.3"><w n="11.1">Tu</w> <w n="11.2">hausses</w> <w n="11.3">ton</w> <w n="11.4">bras</w> <w n="11.5">nu</w> <w n="11.6">pour</w> <w n="11.7">suspendre</w> <w n="11.8">au</w> <w n="11.9">volet</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">La</w> <w n="12.2">cage</w> <w n="12.3">de</w> <w n="12.4">ces</w> <w n="12.5">tourterelles</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Puis</w> <w n="13.2">tu</w> <w n="13.3">descends</w> <w n="13.4">d</w>’<w n="13.5">un</w> <w n="13.6">pas</w> <w n="13.7">léger</w> <w n="13.8">d</w>’<w n="13.9">ombre</w> <w n="13.10">ou</w> <w n="13.11">d</w>’<w n="13.12">oiseau</w></l>
						<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2">escalier</w> <w n="14.3">pauvre</w> <w n="14.4">où</w> <w n="14.5">flotte</w> <w n="14.6">une</w> <w n="14.7">triste</w> <w n="14.8">lumière</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Svelte</w> <w n="15.2">fille</w>, <w n="15.3">et</w> <w n="15.4">ta</w> <w n="15.5">main</w> <w n="15.6">balance</w> <w n="15.7">le</w> <w n="15.8">réseau</w></l>
						<l n="16" num="4.4"><w n="16.1">Que</w> <w n="16.2">va</w> <w n="16.3">gonfler</w> <w n="16.4">d</w>’<w n="16.5">achats</w> <w n="16.6">modestes</w> <w n="16.7">la</w> <w n="16.8">crémière</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Assise</w> <w n="17.2">à</w> <w n="17.3">ta</w> <w n="17.4">croisée</w> <w n="17.5">et</w> <w n="17.6">sa</w> <w n="17.7">plus</w> <w n="17.8">fraîche</w> <w n="17.9">fleur</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Tu</w> <w n="18.2">courberas</w>, <w n="18.3">Jenny</w>, <w n="18.4">tout</w> <w n="18.5">le</w> <w n="18.6">jour</w>, <w n="18.7">sur</w> <w n="18.8">l</w>’<w n="18.9">ouvrage</w></l>
						<l n="19" num="5.3"><w n="19.1">Ce</w> <w n="19.2">cou</w> <w n="19.3">dont</w> <w n="19.4">un</w> <w n="19.5">ruban</w> <w n="19.6">relève</w> <w n="19.7">la</w> <w n="19.8">pâleur</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Ce</w> <w n="20.2">front</w> <w n="20.3">blanc</w> <w n="20.4">qu</w>’<w n="20.5">un</w> <w n="20.6">bandeau</w> <w n="20.7">de</w> <w n="20.8">noirs</w> <w n="20.9">cheveux</w> <w n="20.10">ombrage</w> ;</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Tout</w> <w n="21.2">le</w> <w n="21.3">jour</w>, <w n="21.4">détournant</w> <w n="21.5">tes</w> <w n="21.6">songes</w> <w n="21.7">du</w> <w n="21.8">baiser</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Au</w> <w n="22.2">lieu</w> <w n="22.3">d</w>’<w n="22.4">un</w> <w n="22.5">compagnon</w> <w n="22.6">fidèle</w> <w n="22.7">qui</w> <w n="22.8">t</w>’<w n="22.9">enlace</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Sur</w> <w n="23.2">tes</w> <w n="23.3">chastes</w> <w n="23.4">genoux</w> <w n="23.5">tu</w> <w n="23.6">sentiras</w> <w n="23.7">peser</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">La</w> <w n="24.2">toile</w> <w n="24.3">âpre</w> <w n="24.4">aux</w> <w n="24.5">doigts</w> <w n="24.6">qu</w>’<w n="24.7">elle</w> <w n="24.8">lasse</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Mais</w> <w n="25.2">que</w> <w n="25.3">le</w> <w n="25.4">soir</w>, <w n="25.5">voilé</w> <w n="25.6">de</w> <w n="25.7">bleu</w>, <w n="25.8">sur</w> <w n="25.9">la</w> <w n="25.10">cité</w></l>
						<l n="26" num="7.2"><w n="26.1">Répande</w> <w n="26.2">sa</w> <w n="26.3">tendresse</w> <w n="26.4">obscure</w> <w n="26.5">et</w> <w n="26.6">son</w> <w n="26.7">mystère</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Ton</w> <w n="27.2">coeur</w> <w n="27.3">secrètement</w> <w n="27.4">alors</w> <w n="27.5">sollicité</w></l>
						<l n="28" num="7.4"><w n="28.1">Regrettera</w> <w n="28.2">l</w>’<w n="28.3">amour</w> <w n="28.4">et</w> <w n="28.5">d</w>’<w n="28.6">être</w> <w n="28.7">solitaire</w> ;</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Et</w>, <w n="29.2">réduite</w> <w n="29.3">aux</w> <w n="29.4">langueurs</w> <w n="29.5">d</w>’<w n="29.6">un</w> <w n="29.7">plaisir</w> <w n="29.8">décevant</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Sombre</w>, <w n="30.2">avec</w> <w n="30.3">un</w> <w n="30.4">soupir</w> <w n="30.5">de</w> <w n="30.6">feu</w>, <w n="30.7">la</w> <w n="30.8">nuit</w> <w n="30.9">venue</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Tu</w> <w n="31.2">laisseras</w> <w n="31.3">le</w> <w n="31.4">souffle</w> <w n="31.5">insidieux</w> <w n="31.6">du</w> <w n="31.7">vent</w></l>
						<l n="32" num="8.4"><w n="32.1">Émouvoir</w> <w n="32.2">d</w>’<w n="32.3">un</w> <w n="32.4">désir</w> <w n="32.5">amer</w> <w n="32.6">ta</w> <w n="32.7">gorge</w> <w n="32.8">nue</w>.</l>
					</lg>
				</div></body></text></TEI>