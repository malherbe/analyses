<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">V</head><head type="main_part">A LA MEMOIRE DE SAMAIN</head><div type="poem" key="GUE42">
					<head type="number">XLII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">nuit</w>, <w n="1.3">quand</w> <w n="1.4">le</w> <w n="1.5">vieux</w> <w n="1.6">chêne</w> <w n="1.7">aux</w> <w n="1.8">flancs</w> <w n="1.9">jaloux</w> <w n="1.10">sommeille</w>,</l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">nymphe</w> <w n="2.3">forestière</w> <w n="2.4">en</w> <w n="2.5">sort</w> <w n="2.6">comme</w> <w n="2.7">une</w> <w n="2.8">abeille</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Légère</w>, <w n="3.2">frémissante</w>, <w n="3.3">heureuse</w> <w n="3.4">de</w> <w n="3.5">baigner</w></l>
						<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">l</w>’<w n="4.3">air</w> <w n="4.4">vierge</w> <w n="4.5">son</w> <w n="4.6">corps</w> <w n="4.7">trop</w> <w n="4.8">longtemps</w> <w n="4.9">prisonnier</w></l>
						<l n="5" num="1.5"><w n="5.1">De</w> <w n="5.2">l</w>’<w n="5.3">infécond</w> <w n="5.4">amour</w> <w n="5.5">du</w> <w n="5.6">geôlier</w> <w n="5.7">séculaire</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Ses</w> <w n="6.2">pas</w> <w n="6.3">font</w> <w n="6.4">soupirer</w> <w n="6.5">la</w> <w n="6.6">mousse</w> <w n="6.7">humide</w>, <w n="6.8">elle</w> <w n="6.9">erre</w>,</l>
						<l n="7" num="1.7"><w n="7.1">À</w> <w n="7.2">cette</w> <w n="7.3">heure</w> <w n="7.4">où</w> <w n="7.5">les</w> <w n="7.6">bois</w> <w n="7.7">profonds</w> <w n="7.8">n</w>’<w n="7.9">ont</w> <w n="7.10">pas</w> <w n="7.11">d</w>’<w n="7.12">échos</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Sous</w> <w n="8.2">les</w> <w n="8.3">arbres</w> <w n="8.4">obscurs</w> <w n="8.5">recourbés</w> <w n="8.6">en</w> <w n="8.7">berceaux</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Elle</w> <w n="9.2">songe</w> ; <w n="9.3">elle</w> <w n="9.4">prête</w> <w n="9.5">une</w> <w n="9.6">oreille</w> <w n="9.7">incertaine</w></l>
						<l n="10" num="1.10"><w n="10.1">Au</w> <w n="10.2">vent</w> <w n="10.3">qui</w> <w n="10.4">fuit</w> <w n="10.5">de</w> <w n="10.6">feuille</w> <w n="10.7">en</w> <w n="10.8">feuille</w>, <w n="10.9">à</w> <w n="10.10">la</w> <w n="10.11">fontaine</w></l>
						<l n="11" num="1.11"><w n="11.1">Qui</w> <w n="11.2">tinte</w> <w n="11.3">goutte</w> <w n="11.4">à</w> <w n="11.5">goutte</w> <w n="11.6">au</w> <w n="11.7">creux</w> <w n="11.8">de</w> <w n="11.9">son</w> <w n="11.10">rocher</w>.</l>
						<l n="12" num="1.12"><w n="12.1">Soudain</w> <w n="12.2">elle</w> <w n="12.3">tressaille</w>, <w n="12.4">et</w> <w n="12.5">cesse</w> <w n="12.6">de</w> <w n="12.7">marcher</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">demeure</w>, <w n="13.3">éblouie</w>, <w n="13.4">au</w> <w n="13.5">bord</w> <w n="13.6">d</w>’<w n="13.7">une</w> <w n="13.8">clairière</w></l>
						<l n="14" num="1.14"><w n="14.1">Qui</w> <w n="14.2">creuse</w> <w n="14.3">là</w> <w n="14.4">dans</w> <w n="14.5">l</w>’<w n="14.6">ombre</w> <w n="14.7">un</w> <w n="14.8">cirque</w> <w n="14.9">de</w> <w n="14.10">lumière</w>.</l>
						<l n="15" num="1.15"><w n="15.1">Sous</w> <w n="15.2">la</w> <w n="15.3">lune</w>, <w n="15.4">éclatante</w> <w n="15.5">et</w> <w n="15.6">large</w> <w n="15.7">nappe</w> <w n="15.8">d</w>’<w n="15.9">or</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Endormi</w> <w n="16.2">près</w> <w n="16.3">d</w>’<w n="16.4">un</w> <w n="16.5">feu</w> <w n="16.6">couvert</w> <w n="16.7">qui</w> <w n="16.8">fume</w> <w n="16.9">encor</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Un</w> <w n="17.2">bûcheron</w> <w n="17.3">couché</w> <w n="17.4">figure</w> <w n="17.5">un</w> <w n="17.6">dieu</w> <w n="17.7">de</w> <w n="17.8">marbre</w>.</l>
						<l n="18" num="1.18"><w n="18.1">La</w> <w n="18.2">nymphe</w> <w n="18.3">qui</w> <w n="18.4">le</w> <w n="18.5">voit</w> <w n="18.6">se</w> <w n="18.7">glisse</w> <w n="18.8">d</w>’<w n="18.9">arbre</w> <w n="18.10">en</w> <w n="18.11">arbre</w>,</l>
						<l n="19" num="1.19"><w n="19.1">L</w>’<w n="19.2">épie</w>, <w n="19.3">hésite</w>, <w n="19.4">approche</w> <w n="19.5">enfin</w>, <w n="19.6">penche</w> <w n="19.7">le</w> <w n="19.8">front</w>,</l>
						<l n="20" num="1.20"><w n="20.1">S</w>’<w n="20.2">agenouille</w>, <w n="20.3">et</w>, <w n="20.4">fermant</w> <w n="20.5">les</w> <w n="20.6">yeux</w>, <w n="20.7">d</w>’<w n="20.8">un</w> <w n="20.9">geste</w> <w n="20.10">prompt</w></l>
						<l n="21" num="1.21"><w n="21.1">Jette</w> <w n="21.2">un</w> <w n="21.3">baiser</w> <w n="21.4">craintif</w> <w n="21.5">sur</w> <w n="21.6">une</w> <w n="21.7">âpre</w> <w n="21.8">moustache</w> ;</l>
						<l n="22" num="1.22"><w n="22.1">Puis</w>, <w n="22.2">pour</w> <w n="22.3">miroir</w> <w n="22.4">prenant</w> <w n="22.5">le</w> <w n="22.6">plat</w> <w n="22.7">d</w>’<w n="22.8">un</w> <w n="22.9">fer</w> <w n="22.10">de</w> <w n="22.11">hache</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Elle</w> <w n="23.2">tresse</w> <w n="23.3">des</w> <w n="23.4">brins</w> <w n="23.5">d</w>’<w n="23.6">herbe</w> <w n="23.7">dans</w> <w n="23.8">ses</w> <w n="23.9">cheveux</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Se</w> <w n="24.2">peigne</w>, <w n="24.3">ou</w>, <w n="24.4">variant</w> <w n="24.5">la</w> <w n="24.6">grâce</w> <w n="24.7">de</w> <w n="24.8">ses</w> <w n="24.9">jeux</w>,</l>
						<l n="25" num="1.25"><w n="25.1">Hausse</w> <w n="25.2">les</w> <w n="25.3">bras</w>, <w n="25.4">attire</w> <w n="25.5">un</w> <w n="25.6">rameau</w> <w n="25.7">plein</w> <w n="25.8">de</w> <w n="25.9">force</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Étreint</w> <w n="26.2">avec</w> <w n="26.3">un</w> <w n="26.4">long</w> <w n="26.5">frisson</w> <w n="26.6">la</w> <w n="26.7">froide</w> <w n="26.8">écorce</w>,</l>
						<l n="27" num="1.27"><w n="27.1">Se</w> <w n="27.2">balance</w>, <w n="27.3">et</w>, <w n="27.4">laissant</w> <w n="27.5">sa</w> <w n="27.6">tête</w> <w n="27.7">défaillir</w>,</l>
						<l n="28" num="1.28"><w n="28.1">Regarde</w> <w n="28.2">au</w> <w n="28.3">firmament</w> <w n="28.4">les</w> <w n="28.5">étoiles</w> <w n="28.6">pâlir</w>.</l>
						<l n="29" num="1.29"><w n="29.1">Déjà</w> <w n="29.2">l</w>’<w n="29.3">aube</w> <w n="29.4">en</w> <w n="29.5">ouvrant</w> <w n="29.6">ses</w> <w n="29.7">vastes</w> <w n="29.8">ailes</w> <w n="29.9">blanches</w></l>
						<l n="30" num="1.30"><w n="30.1">Dessine</w> <w n="30.2">sur</w> <w n="30.3">le</w> <w n="30.4">ciel</w> <w n="30.5">les</w> <w n="30.6">noirs</w> <w n="30.7">contours</w> <w n="30.8">des</w> <w n="30.9">branches</w>.</l>
						<l n="31" num="1.31"><w n="31.1">La</w> <w n="31.2">forêt</w> <w n="31.3">par</w> <w n="31.4">degrés</w> <w n="31.5">renaît</w> <w n="31.6">de</w> <w n="31.7">son</w> <w n="31.8">sommeil</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Et</w> <w n="32.2">bientôt</w> <w n="32.3">d</w>’<w n="32.4">un</w> <w n="32.5">rayon</w> <w n="32.6">oblique</w> <w n="32.7">le</w> <w n="32.8">soleil</w></l>
						<l n="33" num="1.33"><w n="33.1">Empourpre</w> <w n="33.2">au</w> <w n="33.3">flanc</w> <w n="33.4">d</w>’<w n="33.5">un</w> <w n="33.6">chêne</w> <w n="33.7">une</w> <w n="33.8">serpe</w> <w n="33.9">enfoncée</w>.</l>
						<l n="34" num="1.34"><w n="34.1">L</w>’<w n="34.2">arbre</w> <w n="34.3">saignant</w> <w n="34.4">trahit</w> <w n="34.5">la</w> <w n="34.6">dryade</w> <w n="34.7">blessée</w>.</l>
						<l n="35" num="1.35"><w n="35.1">Expirante</w>, <w n="35.2">les</w> <w n="35.3">doigts</w> <w n="35.4">tranchés</w>, <w n="35.5">frappée</w> <w n="35.6">au</w> <w n="35.7">coeur</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Par</w> <w n="36.2">la</w> <w n="36.3">voix</w> <w n="36.4">du</w> <w n="36.5">feuillage</w> <w n="36.6">elle</w> <w n="36.7">nomme</w> <w n="36.8">sa</w> <w n="36.9">soeur</w>.</l>
						<l n="37" num="1.37">« <w n="37.1">Hélas</w> ! <w n="37.2">Le</w> <w n="37.3">temps</w> <w n="37.4">pieux</w> <w n="37.5">n</w>’<w n="37.6">est</w> <w n="37.7">plus</w>, <w n="37.8">gémissent</w>-<w n="37.9">elles</w>,</l>
						<l n="38" num="1.38"><w n="38.1">Où</w> <w n="38.2">la</w> <w n="38.3">foi</w> <w n="38.4">des</w> <w n="38.5">humains</w> <w n="38.6">faisait</w> <w n="38.7">des</w> <w n="38.8">immortelles</w> !</l>
						<l n="39" num="1.39"><w n="39.1">Alors</w> <w n="39.2">la</w> <w n="39.3">source</w> <w n="39.4">vive</w> <w n="39.5">offrait</w> <w n="39.6">au</w> <w n="39.7">faune</w> <w n="39.8">en</w> <w n="39.9">feu</w></l>
						<l n="40" num="1.40"><w n="40.1">Sa</w> <w n="40.2">bouche</w> <w n="40.3">épanouie</w> <w n="40.4">en</w> <w n="40.5">urne</w> <w n="40.6">de</w> <w n="40.7">lys</w> <w n="40.8">bleu</w>,</l>
						<l n="41" num="1.41"><w n="41.1">Les</w> <w n="41.2">montagnes</w>, <w n="41.3">les</w> <w n="41.4">bois</w>, <w n="41.5">les</w> <w n="41.6">antres</w>, <w n="41.7">les</w> <w n="41.8">ravines</w>,</l>
						<l n="42" num="1.42"><w n="42.1">Étaient</w> <w n="42.2">le</w> <w n="42.3">libre</w> <w n="42.4">lieu</w> <w n="42.5">de</w> <w n="42.6">nos</w> <w n="42.7">amours</w> <w n="42.8">divines</w>,</l>
						<l n="43" num="1.43"><w n="43.1">Et</w> <w n="43.2">l</w>’<w n="43.3">homme</w>, <w n="43.4">saintement</w> <w n="43.5">rêveur</w> <w n="43.6">comme</w> <w n="43.7">un</w> <w n="43.8">enfant</w>,</l>
						<l n="44" num="1.44"><w n="44.1">Écoutait</w> <w n="44.2">battre</w> <w n="44.3">un</w> <w n="44.4">coeur</w> <w n="44.5">dans</w> <w n="44.6">l</w>’<w n="44.7">univers</w> <w n="44.8">vivant</w> ;</l>
						<l n="45" num="1.45"><w n="45.1">Alors</w> <w n="45.2">il</w> <w n="45.3">respectait</w> <w n="45.4">des</w> <w n="45.5">dieux</w> <w n="45.6">dans</w> <w n="45.7">la</w> <w n="45.8">nature</w>.</l>
						<l n="46" num="1.46"><w n="46.1">Mais</w> <w n="46.2">son</w> <w n="46.3">génie</w> <w n="46.4">athée</w> <w n="46.5">aujourd</w>’<w n="46.6">hui</w> <w n="46.7">nous</w> <w n="46.8">torture</w>.</l>
						<l n="47" num="1.47"><w n="47.1">Aux</w> <w n="47.2">travaux</w> <w n="47.3">de</w> <w n="47.4">la</w> <w n="47.5">forge</w> <w n="47.6">il</w> <w n="47.7">a</w> <w n="47.8">vaincu</w> <w n="47.9">Vulcain</w>,</l>
						<l n="48" num="1.48"><w n="48.1">Il</w> <w n="48.2">l</w>’<w n="48.3">enferme</w>, <w n="48.4">il</w> <w n="48.5">l</w>’<w n="48.6">attelle</w> <w n="48.7">à</w> <w n="48.8">ses</w> <w n="48.9">monstres</w> <w n="48.10">d</w>’<w n="48.11">airain</w>.</l>
						<l n="49" num="1.49"><w n="49.1">Ses</w> <w n="49.2">coins</w>, <w n="49.3">sa</w> <w n="49.4">serpe</w> <w n="49.5">fourbe</w> <w n="49.6">et</w> <w n="49.7">sa</w> <w n="49.8">hache</w> <w n="49.9">qui</w> <w n="49.10">vibre</w></l>
						<l n="50" num="1.50"><w n="50.1">Dénudent</w> <w n="50.2">jusqu</w>’<w n="50.3">au</w> <w n="50.4">coeur</w> <w n="50.5">nos</w> <w n="50.6">chênes</w> <w n="50.7">fibre</w> <w n="50.8">à</w> <w n="50.9">fibre</w>.</l>
						<l n="51" num="1.51"><w n="51.1">Il</w> <w n="51.2">a</w> <w n="51.3">dompté</w> <w n="51.4">la</w> <w n="51.5">mer</w> <w n="51.6">fougueuse</w>, <w n="51.7">il</w> <w n="51.8">a</w> <w n="51.9">dompté</w></l>
						<l n="52" num="1.52"><w n="52.1">La</w> <w n="52.2">foudre</w>, <w n="52.3">et</w> <w n="52.4">les</w> <w n="52.5">torrents</w> <w n="52.6">servent</w> <w n="52.7">sa</w> <w n="52.8">volonté</w>.</l>
						<l n="53" num="1.53"><w n="53.1">Hélas</w> ! <w n="53.2">L</w>’<w n="53.3">heure</w> <w n="53.4">est</w> <w n="53.5">fatale</w> <w n="53.6">et</w> <w n="53.7">nos</w> <w n="53.8">plaintes</w> <w n="53.9">sont</w> <w n="53.10">vaines</w>,</l>
						<l n="54" num="1.54"><w n="54.1">Car</w> <w n="54.2">la</w> <w n="54.3">sève</w> <w n="54.4">immortelle</w> <w n="54.5">abandonne</w> <w n="54.6">nos</w> <w n="54.7">veines</w>.  »</l>
						<l n="55" num="1.55"><w n="55.1">Or</w>, <w n="55.2">tout</w> <w n="55.3">à</w> <w n="55.4">coup</w>, <w n="55.5">pendant</w> <w n="55.6">que</w> <w n="55.7">les</w> <w n="55.8">nymphes</w> <w n="55.9">du</w> <w n="55.10">bois</w></l>
						<l n="56" num="1.56"><w n="56.1">Accordent</w> <w n="56.2">leurs</w> <w n="56.3">soupirs</w> <w n="56.4">et</w> <w n="56.5">confondent</w> <w n="56.6">leurs</w> <w n="56.7">voix</w>,</l>
						<l n="57" num="1.57"><w n="57.1">La</w> <w n="57.2">forêt</w>, <w n="57.3">solitaire</w> <w n="57.4">et</w> <w n="57.5">d</w>’<w n="57.6">aurore</w> <w n="57.7">baignée</w>,</l>
						<l n="58" num="1.58"><w n="58.1">Répercute</w> <w n="58.2">le</w> <w n="58.3">bruit</w> <w n="58.4">puissant</w> <w n="58.5">d</w>’<w n="58.6">une</w> <w n="58.7">cognée</w>.</l>
					</lg>
				</div></body></text></TEI>