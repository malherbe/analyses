<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><head type="main_part">FENÊTRES SUR LA VIE</head><div type="poem" key="GUE16" rhyme="none">
					<head type="number">XVI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">ciel</w> <w n="1.3">est</w> <w n="1.4">pur</w>, <w n="1.5">l</w>’<w n="1.6">eau</w> <w n="1.7">transparente</w>, <w n="1.8">et</w> <w n="1.9">l</w>’<w n="1.10">air</w> <w n="1.11">du</w> <w n="1.12">soir</w></l>
						<l n="2" num="1.2"><w n="2.1">Léger</w> <w n="2.2">comme</w> <w n="2.3">un</w> <w n="2.4">baiser</w> <w n="2.5">fugitif</w> <w n="2.6">sur</w> <w n="2.7">ma</w> <w n="2.8">joue</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">vent</w> <w n="3.3">dans</w> <w n="3.4">mes</w> <w n="3.5">cheveux</w> <w n="3.6">semble</w> <w n="3.7">un</w> <w n="3.8">enfant</w> <w n="3.9">qui</w> <w n="3.10">joue</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">je</w> <w n="4.3">vais</w>, <w n="4.4">parmi</w> <w n="4.5">l</w>’<w n="4.6">herbe</w> <w n="4.7">encor</w> <w n="4.8">chaude</w>, <w n="4.9">m</w>’<w n="4.10">asseoir</w></l>
						<l n="5" num="1.5"><w n="5.1">En</w> <w n="5.2">face</w> <w n="5.3">du</w> <w n="5.4">limpide</w> <w n="5.5">et</w> <w n="5.6">pensif</w> <w n="5.7">horizon</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Haleine</w> <w n="6.2">de</w> <w n="6.3">la</w> <w n="6.4">nuit</w> <w n="6.5">qui</w> <w n="6.6">dévale</w>, <w n="6.7">silence</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Espace</w> <w n="7.2">où</w> <w n="7.3">d</w>’<w n="7.4">un</w> <w n="7.5">agile</w> <w n="7.6">essor</w> <w n="7.7">l</w>’<w n="7.8">âme</w> <w n="7.9">s</w>’<w n="7.10">élance</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Solitude</w> ! <w n="8.2">Une</w> <w n="8.3">cloche</w> <w n="8.4">aux</w> <w n="8.5">derniers</w> <w n="8.6">nids</w> <w n="8.7">répond</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Dans</w> <w n="9.2">les</w> <w n="9.3">champs</w> <w n="9.4">où</w> <w n="9.5">le</w> <w n="9.6">soir</w> <w n="9.7">traîne</w> <w n="9.8">ses</w> <w n="9.9">voiles</w> <w n="9.10">bleus</w></l>
						<l n="10" num="1.10"><w n="10.1">Un</w> <w n="10.2">attelage</w> <w n="10.3">au</w> <w n="10.4">pas</w> <w n="10.5">sûr</w> <w n="10.6">et</w> <w n="10.7">lent</w> <w n="10.8">se</w> <w n="10.9">balance</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">soleil</w> <w n="11.3">met</w> <w n="11.4">de</w> <w n="11.5">l</w>’<w n="11.6">or</w> <w n="11.7">sur</w> <w n="11.8">les</w> <w n="11.9">cornes</w> <w n="11.10">des</w> <w n="11.11">boeufs</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Le</w> <w n="12.2">soleil</w> <w n="12.3">qui</w> <w n="12.4">descend</w> <w n="12.5">au</w> <w n="12.6">delà</w> <w n="12.7">des</w> <w n="12.8">collines</w></l>
						<l n="13" num="1.13"><w n="13.1">Verse</w> <w n="13.2">l</w>’<w n="13.3">adieu</w> <w n="13.4">de</w> <w n="13.5">ses</w> <w n="13.6">rayons</w> <w n="13.7">mélancoliques</w></l>
						<l n="14" num="1.14"><w n="14.1">Sur</w> <w n="14.2">la</w> <w n="14.3">mouvante</w> <w n="14.4">mer</w> <w n="14.5">sans</w> <w n="14.6">fin</w> <w n="14.7">des</w> <w n="14.8">moissons</w> <w n="14.9">mûres</w></l>
						<l n="15" num="1.15"><w n="15.1">Dont</w> <w n="15.2">la</w> <w n="15.3">houle</w>, <w n="15.4">semblable</w> <w n="15.5">au</w> <w n="15.6">vaste</w> <w n="15.7">rêve</w> <w n="15.8">humain</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Roule</w>, <w n="16.2">se</w> <w n="16.3">gonfle</w> <w n="16.4">et</w> <w n="16.5">meurt</w> <w n="16.6">et</w> <w n="16.7">frissonne</w> <w n="16.8">et</w> <w n="16.9">murmure</w></l>
						<l n="17" num="1.17"><w n="17.1">Vers</w> <w n="17.2">la</w> <w n="17.3">ligne</w> <w n="17.4">de</w> <w n="17.5">ciel</w> <w n="17.6">qu</w>’<w n="17.7">elle</w> <w n="17.8">n</w>’<w n="17.9">atteint</w> <w n="17.10">jamais</w>.</l>
						<l n="18" num="1.18"><w n="18.1">Je</w> <w n="18.2">regarde</w>, <w n="18.3">et</w> <w n="18.4">mon</w> <w n="18.5">front</w> <w n="18.6">retombe</w> <w n="18.7">dans</w> <w n="18.8">mes</w> <w n="18.9">mains</w></l>
						<l n="19" num="1.19"><w n="19.1">Comme</w> <w n="19.2">un</w> <w n="19.3">fruit</w> <w n="19.4">délicat</w> <w n="19.5">s</w>’<w n="19.6">abrite</w> <w n="19.7">sous</w> <w n="19.8">ses</w> <w n="19.9">feuilles</w>.</l>
						<l n="20" num="1.20"><w n="20.1">Ô</w> <w n="20.2">silence</w> <w n="20.3">des</w> <w n="20.4">soirs</w> <w n="20.5">d</w>’<w n="20.6">été</w>, <w n="20.7">profonde</w> <w n="20.8">paix</w></l>
						<l n="21" num="1.21"><w n="21.1">Où</w>, <w n="21.2">comme</w> <w n="21.3">en</w> <w n="21.4">un</w> <w n="21.5">miroir</w>, <w n="21.6">l</w>’<w n="21.7">esprit</w> <w n="21.8">qui</w> <w n="21.9">se</w> <w n="21.10">recueille</w></l>
						<l n="22" num="1.22"><w n="22.1">Voit</w> <w n="22.2">flotter</w> <w n="22.3">l</w>’<w n="22.4">horizon</w> <w n="22.5">nocturne</w> <w n="22.6">du</w> <w n="22.7">passé</w> !</l>
						<l n="23" num="1.23"><w n="23.1">Nous</w> <w n="23.2">nous</w> <w n="23.3">sommes</w> <w n="23.4">aimés</w> <w n="23.5">un</w> <w n="23.6">jour</w>, <w n="23.7">et</w> <w n="23.8">ce</w> <w n="23.9">fut</w> <w n="23.10">vain</w></l>
						<l n="24" num="1.24"><w n="24.1">Comme</w> <w n="24.2">un</w> <w n="24.3">rosier</w> <w n="24.4">sur</w> <w n="24.5">un</w> <w n="24.6">tombeau</w>. <w n="24.7">Je</w> <w n="24.8">me</w> <w n="24.9">souviens</w> ;</l>
						<l n="25" num="1.25"><w n="25.1">J</w>’<w n="25.2">écoute</w> <w n="25.3">bourdonner</w> <w n="25.4">en</w> <w n="25.5">moi</w> <w n="25.6">l</w>’<w n="25.7">amour</w> <w n="25.8">ancien</w> ;</l>
						<l n="26" num="1.26"><w n="26.1">J</w>’<w n="26.2">ai</w> <w n="26.3">peur</w> <w n="26.4">de</w> <w n="26.5">cette</w> <w n="26.6">guêpe</w> <w n="26.7">impossible</w> <w n="26.8">à</w> <w n="26.9">chasser</w>.</l>
						<l n="27" num="1.27"><w n="27.1">Coeur</w> <w n="27.2">lacéré</w>, <w n="27.3">pareil</w> <w n="27.4">à</w> <w n="27.5">l</w>’<w n="27.6">arbre</w> <w n="27.7">qui</w> <w n="27.8">renforce</w></l>
						<l n="28" num="1.28"><w n="28.1">En</w> <w n="28.2">vieillissant</w> <w n="28.3">les</w> <w n="28.4">noms</w> <w n="28.5">gravés</w> <w n="28.6">sur</w> <w n="28.7">son</w> <w n="28.8">écorce</w>,</l>
						<l n="29" num="1.29"><w n="29.1">Quand</w> <w n="29.2">pourrons</w>-<w n="29.3">nous</w> <w n="29.4">aimer</w> <w n="29.5">sans</w> <w n="29.6">mémoire</w> ? <w n="29.7">En</w> <w n="29.8">quel</w> <w n="29.9">lit</w></l>
						<l n="30" num="1.30"><w n="30.1">Saurai</w>-<w n="30.2">je</w> <w n="30.3">enfin</w> <w n="30.4">trouver</w> <w n="30.5">le</w> <w n="30.6">véritable</w> <w n="30.7">oubli</w> ?</l>
						<l n="31" num="1.31"><w n="31.1">Le</w> <w n="31.2">soleil</w> <w n="31.3">sur</w> <w n="31.4">les</w> <w n="31.5">blés</w> <w n="31.6">et</w> <w n="31.7">les</w> <w n="31.8">coteaux</w> <w n="31.9">se</w> <w n="31.10">couche</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Mais</w> <w n="32.2">ses</w> <w n="32.3">rayons</w> <w n="32.4">mourants</w> <w n="32.5">me</w> <w n="32.6">rendront</w>-<w n="32.7">ils</w> <w n="32.8">la</w> <w n="32.9">bouche</w></l>
						<l n="33" num="1.33"><w n="33.1">Comme</w> <w n="33.2">eux</w> <w n="33.3">voluptueuse</w> <w n="33.4">et</w> <w n="33.5">large</w> <w n="33.6">et</w> <w n="33.7">tiède</w> <w n="33.8">et</w> <w n="33.9">rouge</w></l>
						<l n="34" num="1.34"><w n="34.1">De</w> <w n="34.2">l</w>’<w n="34.3">amante</w> <w n="34.4">qui</w> <w n="34.5">m</w>’<w n="34.6">a</w> <w n="34.7">pressé</w> <w n="34.8">dans</w> <w n="34.9">ses</w> <w n="34.10">bras</w> <w n="34.11">forts</w> ?</l>
						<l n="35" num="1.35"><w n="35.1">Les</w> <w n="35.2">épis</w> <w n="35.3">aux</w> <w n="35.4">lourds</w> <w n="35.5">flots</w> <w n="35.6">fauves</w> <w n="35.7">me</w> <w n="35.8">rendront</w>-<w n="35.9">ils</w></l>
						<l n="36" num="1.36"><w n="36.1">Ses</w> <w n="36.2">cheveux</w> <w n="36.3">déroulés</w> <w n="36.4">sur</w> <w n="36.5">elle</w> <w n="36.6">en</w> <w n="36.7">ondes</w> <w n="36.8">d</w>’<w n="36.9">or</w> ?</l>
						<l n="37" num="1.37"><w n="37.1">Les</w> <w n="37.2">coteaux</w> <w n="37.3">dont</w> <w n="37.4">la</w> <w n="37.5">nuit</w> <w n="37.6">découpe</w> <w n="37.7">le</w> <w n="37.8">profil</w></l>
						<l n="38" num="1.38"><w n="38.1">N</w>’<w n="38.2">ont</w> <w n="38.3">pas</w> <w n="38.4">l</w>’<w n="38.5">inflexion</w> <w n="38.6">charmante</w> <w n="38.7">de</w> <w n="38.8">son</w> <w n="38.9">corps</w>,</l>
						<l n="39" num="1.39"><w n="39.1">Et</w> <w n="39.2">les</w> <w n="39.3">parfums</w> <w n="39.4">de</w> <w n="39.5">fleurs</w> <w n="39.6">que</w> <w n="39.7">ces</w> <w n="39.8">plaines</w> <w n="39.9">exhalent</w></l>
						<l n="40" num="1.40"><w n="40.1">Ne</w> <w n="40.2">sont</w> <w n="40.3">pas</w> <w n="40.4">doux</w> <w n="40.5">à</w> <w n="40.6">respirer</w> <w n="40.7">comme</w> <w n="40.8">son</w> <w n="40.9">âme</w>.</l>
					</lg>
					<lg n="2">
						<l n="41" num="2.1"><w n="41.1">Un</w> <w n="41.2">éclair</w> <w n="41.3">de</w> <w n="41.4">chaleur</w> <w n="41.5">fouille</w> <w n="41.6">le</w> <w n="41.7">crépuscule</w> :</l>
						<l n="42" num="2.2"><w n="42.1">Ainsi</w> <w n="42.2">le</w> <w n="42.3">souvenir</w> <w n="42.4">me</w> <w n="42.5">déchire</w> <w n="42.6">et</w> <w n="42.7">me</w> <w n="42.8">brûle</w>.</l>
					</lg>
					<lg n="3">
						<l n="43" num="3.1"><w n="43.1">Dans</w> <w n="43.2">ces</w> <w n="43.3">soirs</w> <w n="43.4">de</w> <w n="43.5">splendeur</w> <w n="43.6">pacifique</w> <w n="43.7">où</w> <w n="43.8">l</w>’<w n="43.9">on</w> <w n="43.10">souffre</w></l>
						<l n="44" num="3.2"><w n="44.1">À</w> <w n="44.2">sentir</w> <w n="44.3">sa</w> <w n="44.4">bassesse</w> <w n="44.5">et</w> <w n="44.6">sa</w> <w n="44.7">pauvreté</w> <w n="44.8">d</w>’<w n="44.9">homme</w>,</l>
						<l n="45" num="3.3"><w n="45.1">Où</w> <w n="45.2">l</w>’<w n="45.3">esprit</w> <w n="45.4">aveuglé</w> <w n="45.5">de</w> <w n="45.6">lumière</w> <w n="45.7">tâtonne</w>,</l>
						<l n="46" num="3.4"><w n="46.1">Où</w> <w n="46.2">le</w> <w n="46.3">coeur</w> <w n="46.4">enivré</w> <w n="46.5">d</w>’<w n="46.6">azur</w> <w n="46.7">et</w> <w n="46.8">d</w>’<w n="46.9">air</w> <w n="46.10">étouffe</w>,</l>
						<l n="47" num="3.5"><w n="47.1">On</w> <w n="47.2">a</w> <w n="47.3">des</w> <w n="47.4">mots</w> <w n="47.5">d</w>’<w n="47.6">enfant</w> <w n="47.7">qui</w> <w n="47.8">pleurent</w> <w n="47.9">et</w> <w n="47.10">supplient</w></l>
						<l n="48" num="3.6"><w n="48.1">Vers</w> <w n="48.2">ce</w> <w n="48.3">vaste</w> <w n="48.4">univers</w> <w n="48.5">qu</w>’<w n="48.6">on</w> <w n="48.7">voudrait</w> <w n="48.8">croire</w> <w n="48.9">Dieu</w>.</l>
						<l n="49" num="3.7">« <w n="49.1">Ah</w> ! <w n="49.2">Dit</w>-<w n="49.3">on</w>, <w n="49.4">remplir</w> <w n="49.5">l</w>’<w n="49.6">orbe</w> <w n="49.7">immense</w> <w n="49.8">de</w> <w n="49.9">la</w> <w n="49.10">vie</w>,</l>
						<l n="50" num="3.8"><w n="50.1">Ouvrir</w> <w n="50.2">comme</w> <w n="50.3">l</w>’<w n="50.4">étoile</w> <w n="50.5">un</w> <w n="50.6">jour</w> <w n="50.7">sur</w> <w n="50.8">d</w>’<w n="50.9">autres</w> <w n="50.10">cieux</w>,</l>
						<l n="51" num="3.9"><w n="51.1">Comme</w> <w n="51.2">le</w> <w n="51.3">roc</w> <w n="51.4">porter</w> <w n="51.5">le</w> <w n="51.6">fer</w>, <w n="51.7">l</w>’<w n="51.8">or</w> <w n="51.9">et</w> <w n="51.10">le</w> <w n="51.11">feu</w>,</l>
						<l n="52" num="3.10"><w n="52.1">Tressaillir</w> <w n="52.2">au</w> <w n="52.3">printemps</w> <w n="52.4">nouveau</w>, <w n="52.5">pousser</w> <w n="52.6">des</w> <w n="52.7">feuilles</w>,</l>
						<l n="53" num="3.11"><w n="53.1">Être</w> <w n="53.2">la</w> <w n="53.3">brume</w>, <w n="53.4">l</w>’<w n="53.5">eau</w> <w n="53.6">du</w> <w n="53.7">puits</w>, <w n="53.8">le</w> <w n="53.9">fruit</w> <w n="53.10">qu</w>’<w n="53.11">on</w> <w n="53.12">cueille</w>,</l>
						<l n="54" num="3.12"><w n="54.1">Vivre</w> <w n="54.2">enfin</w> <w n="54.3">sans</w> <w n="54.4">se</w> <w n="54.5">voir</w> <w n="54.6">vivre</w> ! <w n="54.7">Puissante</w> <w n="54.8">mère</w>,</l>
						<l n="55" num="3.13"><w n="55.1">Prends</w>-<w n="55.2">moi</w>, <w n="55.3">terre</w> <w n="55.4">des</w> <w n="55.5">morts</w>, <w n="55.6">terre</w> <w n="55.7">des</w> <w n="55.8">blés</w>, <w n="55.9">ô</w> <w n="55.10">terre</w> !</l>
						<l n="56" num="3.14"><w n="56.1">Mêle</w> <w n="56.2">mon</w> <w n="56.3">corps</w> <w n="56.4">vivant</w> <w n="56.5">à</w> <w n="56.6">ta</w> <w n="56.7">grande</w> <w n="56.8">poussière</w> !  »</l>
						<l n="57" num="3.15"><w n="57.1">Mais</w> <w n="57.2">la</w> <w n="57.3">nature</w> <w n="57.4">avec</w> <w n="57.5">orgueil</w> <w n="57.6">poursuit</w> <w n="57.7">son</w> <w n="57.8">rêve</w> :</l>
						<l n="58" num="3.16"><w n="58.1">Elle</w> <w n="58.2">n</w>’<w n="58.3">alliera</w> <w n="58.4">pas</w> <w n="58.5">notre</w> <w n="58.6">sang</w> <w n="58.7">à</w> <w n="58.8">ses</w> <w n="58.9">sèves</w>.</l>
						<l n="59" num="3.17"><w n="59.1">Le</w> <w n="59.2">jeune</w> <w n="59.3">avril</w>, <w n="59.4">le</w> <w n="59.5">bel</w> <w n="59.6">été</w>, <w n="59.7">le</w> <w n="59.8">vieil</w> <w n="59.9">automne</w></l>
						<l n="60" num="3.18"><w n="60.1">Mènent</w> <w n="60.2">leur</w> <w n="60.3">ronde</w> <w n="60.4">autour</w> <w n="60.5">du</w> <w n="60.6">linceul</w> <w n="60.7">de</w> <w n="60.8">l</w>’<w n="60.9">hiver</w></l>
						<l n="61" num="3.19"><w n="61.1">Sans</w> <w n="61.2">savoir</w> <w n="61.3">qu</w>’<w n="61.4">ils</w> <w n="61.5">font</w> <w n="61.6">naître</w>, <w n="61.7">aimer</w> <w n="61.8">et</w> <w n="61.9">souffrir</w> <w n="61.10">l</w>’<w n="61.11">homme</w>.</l>
						<l n="62" num="3.20"><w n="62.1">Dans</w> <w n="62.2">sa</w> <w n="62.3">joie</w> <w n="62.4">égoïste</w> <w n="62.5">et</w> <w n="62.6">pleine</w>, <w n="62.7">l</w>’<w n="62.8">univers</w></l>
						<l n="63" num="3.21"><w n="63.1">Reste</w> <w n="63.2">sourd</w> <w n="63.3">au</w> <w n="63.4">désir</w> <w n="63.5">fraternel</w> <w n="63.6">de</w> <w n="63.7">la</w> <w n="63.8">chair</w> ;</l>
						<l n="64" num="3.22"><w n="64.1">L</w>’<w n="64.2">âme</w> <w n="64.3">contre</w> <w n="64.4">le</w> <w n="64.5">noir</w> <w n="64.6">grillage</w> <w n="64.7">qui</w> <w n="64.8">l</w>’<w n="64.9">enferme</w></l>
						<l n="65" num="3.23"><w n="65.1">S</w>’<w n="65.2">élance</w>, <w n="65.3">oiseau</w> <w n="65.4">captif</w>, <w n="65.5">et</w> <w n="65.6">se</w> <w n="65.7">brise</w> <w n="65.8">les</w> <w n="65.9">ailes</w> :</l>
						<l n="66" num="3.24"><w n="66.1">Nous</w> <w n="66.2">ne</w> <w n="66.3">connaîtrons</w> <w n="66.4">rien</w> <w n="66.5">de</w> <w n="66.6">la</w> <w n="66.7">mère</w> <w n="66.8">éternelle</w>.</l>
						<l n="67" num="3.25"><w n="67.1">Et</w> <w n="67.2">nous</w> <w n="67.3">aurons</w> <w n="67.4">un</w> <w n="67.5">mal</w> <w n="67.6">secret</w> <w n="67.7">de</w> <w n="67.8">la</w> <w n="67.9">voir</w> <w n="67.10">belle</w>,</l>
						<l n="68" num="3.26"><w n="68.1">D</w>’<w n="68.2">épier</w> <w n="68.3">vainement</w> <w n="68.4">l</w>’<w n="68.5">obscur</w> <w n="68.6">travail</w> <w n="68.7">des</w> <w n="68.8">germes</w></l>
						<l n="69" num="3.27"><w n="69.1">Dont</w> <w n="69.2">la</w> <w n="69.3">sourde</w> <w n="69.4">harmonie</w> <w n="69.5">échappe</w> <w n="69.6">à</w> <w n="69.7">nos</w> <w n="69.8">oreilles</w> ;</l>
						<l n="70" num="3.28"><w n="70.1">Nous</w> <w n="70.2">ne</w> <w n="70.3">mûrirons</w> <w n="70.4">pas</w> <w n="70.5">dans</w> <w n="70.6">les</w> <w n="70.7">grappes</w> <w n="70.8">des</w> <w n="70.9">treilles</w>,</l>
						<l n="71" num="3.29"><w n="71.1">Ni</w> <w n="71.2">dans</w> <w n="71.3">le</w> <w n="71.4">fruit</w>, <w n="71.5">ni</w> <w n="71.6">dans</w> <w n="71.7">le</w> <w n="71.8">blé</w>, <w n="71.9">ni</w> <w n="71.10">dans</w> <w n="71.11">la</w> <w n="71.12">pierre</w> ;</l>
						<l n="72" num="3.30"><w n="72.1">L</w>’<w n="72.2">eau</w> <w n="72.3">nous</w> <w n="72.4">refusera</w> <w n="72.5">son</w> <w n="72.6">étreinte</w>, <w n="72.7">le</w> <w n="72.8">chêne</w></l>
						<l n="73" num="3.31"><w n="73.1">Ne</w> <w n="73.2">nous</w> <w n="73.3">livrera</w> <w n="73.4">pas</w> <w n="73.5">la</w> <w n="73.6">nymphe</w> <w n="73.7">qu</w>’<w n="73.8">il</w> <w n="73.9">enchaîne</w>,</l>
						<l n="74" num="3.32"><w n="74.1">Et</w> <w n="74.2">si</w> <w n="74.3">nous</w> <w n="74.4">prions</w> <w n="74.5">trop</w> <w n="74.6">le</w> <w n="74.7">soleil</w>, <w n="74.8">sa</w> <w n="74.9">lumière</w></l>
						<l n="75" num="3.33"><w n="75.1">Calcinera</w> <w n="75.2">nos</w> <w n="75.3">yeux</w> <w n="75.4">à</w> <w n="75.5">travers</w> <w n="75.6">nos</w> <w n="75.7">paupières</w>.</l>
					</lg>
					<lg n="4">
						<l n="76" num="4.1"><w n="76.1">La</w> <w n="76.2">nature</w>, <w n="76.3">d</w>’<w n="76.4">un</w> <w n="76.5">geste</w> <w n="76.6">ennuyé</w> <w n="76.7">de</w> <w n="76.8">marâtre</w>,</l>
						<l n="77" num="4.2"><w n="77.1">Écarte</w> <w n="77.2">notre</w> <w n="77.3">soif</w> <w n="77.4">de</w> <w n="77.5">ses</w> <w n="77.6">larges</w> <w n="77.7">mamelles</w>.</l>
						<l n="78" num="4.3"><w n="78.1">Elle</w> <w n="78.2">va</w> ; <w n="78.3">nos</w> <w n="78.4">amours</w>, <w n="78.5">nos</w> <w n="78.6">rêves</w> <w n="78.7">et</w> <w n="78.8">nos</w> <w n="78.9">peines</w></l>
						<l n="79" num="4.4"><w n="79.1">Étendus</w> <w n="79.2">à</w> <w n="79.3">ses</w> <w n="79.4">pieds</w> <w n="79.5">craquent</w> <w n="79.6">comme</w> <w n="79.7">les</w> <w n="79.8">faînes</w></l>
						<l n="80" num="4.5"><w n="80.1">Éclatent</w> <w n="80.2">sous</w> <w n="80.3">les</w> <w n="80.4">pas</w> <w n="80.5">indifférents</w> <w n="80.6">du</w> <w n="80.7">pâtre</w>.</l>
						<l n="81" num="4.6"><w n="81.1">Et</w> <w n="81.2">pourtant</w>, <w n="81.3">sous</w> <w n="81.4">le</w> <w n="81.5">ciel</w> <w n="81.6">des</w> <w n="81.7">soirs</w> <w n="81.8">d</w>’<w n="81.9">été</w> <w n="81.10">sans</w> <w n="81.11">fin</w>,</l>
						<l n="82" num="4.7"><w n="82.1">Encor</w>, <w n="82.2">toujours</w>, <w n="82.3">jusqu</w>’<w n="82.4">à</w> <w n="82.5">la</w> <w n="82.6">nuit</w> <w n="82.7">où</w> <w n="82.8">le</w> <w n="82.9">destin</w></l>
						<l n="83" num="4.8"><w n="83.1">Voudra</w> <w n="83.2">fermer</w> <w n="83.3">les</w> <w n="83.4">yeux</w> <w n="83.5">à</w> <w n="83.6">l</w>’<w n="83.7">humanité</w> <w n="83.8">lasse</w>,</l>
						<l n="84" num="4.9"><w n="84.1">D</w>’<w n="84.2">autres</w> <w n="84.3">viendront</w>, <w n="84.4">pareils</w> <w n="84.5">à</w> <w n="84.6">moi</w> <w n="84.7">dans</w> <w n="84.8">leur</w> <w n="84.9">chair</w> <w n="84.10">veuve</w>,</l>
						<l n="85" num="4.10"><w n="85.1">Le</w> <w n="85.2">coeur</w> <w n="85.3">amer</w> <w n="85.4">d</w>’<w n="85.5">un</w> <w n="85.6">vieil</w> <w n="85.7">amour</w> <w n="85.8">resté</w> <w n="85.9">vivace</w>,</l>
						<l n="86" num="4.11"><w n="86.1">Voir</w>, <w n="86.2">parmi</w> <w n="86.3">les</w> <w n="86.4">corbeaux</w> <w n="86.5">qui</w> <w n="86.6">volent</w> <w n="86.7">vers</w> <w n="86.8">sa</w> <w n="86.9">face</w>,</l>
						<l n="87" num="4.12"><w n="87.1">Le</w> <w n="87.2">soleil</w> <w n="87.3">se</w> <w n="87.4">coucher</w> <w n="87.5">sur</w> <w n="87.6">des</w> <w n="87.7">moissons</w> <w n="87.8">heureuses</w>.</l>
					</lg>
				</div></body></text></TEI>