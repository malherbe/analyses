<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">MÉLANCOLIES A VIOLLIS</head><div type="poem" key="GUE20">
					<head type="number">XX</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">ciel</w> <w n="1.3">pâlit</w>. <w n="1.4">La</w> <w n="1.5">terre</w> <w n="1.6">humide</w> <w n="1.7">et</w> <w n="1.8">reposée</w></l>
						<l n="2" num="1.2"><w n="2.1">Respire</w>. <w n="2.2">Messager</w> <w n="2.3">du</w> <w n="2.4">matin</w>, <w n="2.5">le</w> <w n="2.6">vent</w> <w n="2.7">pur</w></l>
						<l n="3" num="1.3"><w n="3.1">Agite</w> <w n="3.2">faiblement</w> <w n="3.3">la</w> <w n="3.4">vigne</w> <w n="3.5">sur</w> <w n="3.6">le</w> <w n="3.7">mur</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">d</w>’<w n="4.3">une</w> <w n="4.4">main</w> <w n="4.5">timide</w> <w n="4.6">entr</w>’<w n="4.7">ouvre</w> <w n="4.8">ma</w> <w n="4.9">croisée</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Une</w> <w n="5.2">lueur</w> <w n="5.3">d</w>’<w n="5.4">argent</w> <w n="5.5">lustre</w> <w n="5.6">l</w>’<w n="5.7">herbe</w> <w n="5.8">brisée</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Car</w>, <w n="6.2">des</w> <w n="6.3">coteaux</w> <w n="6.4">de</w> <w n="6.5">l</w>’<w n="6.6">est</w> <w n="6.7">au</w> <w n="6.8">val</w> <w n="6.9">encore</w> <w n="6.10">obscur</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Dénouant</w> <w n="7.2">ses</w> <w n="7.3">cheveux</w> <w n="7.4">de</w> <w n="7.5">genêt</w> <w n="7.6">dans</w> <w n="7.7">l</w>’<w n="7.8">azur</w>,</l>
						<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">aube</w> <w n="8.3">en</w> <w n="8.4">riant</w> <w n="8.5">chemine</w> <w n="8.6">à</w> <w n="8.7">travers</w> <w n="8.8">la</w> <w n="8.9">rosée</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">brouillard</w> <w n="9.3">se</w> <w n="9.4">déchire</w> <w n="9.5">aux</w> <w n="9.6">branches</w> <w n="9.7">du</w> <w n="9.8">verger</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Le</w> <w n="10.2">soleil</w> <w n="10.3">qui</w> <w n="10.4">se</w> <w n="10.5">lève</w> <w n="10.6">y</w> <w n="10.7">jette</w> <w n="10.8">un</w> <w n="10.9">or</w> <w n="10.10">léger</w></l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">baigne</w> <w n="11.3">au</w> <w n="11.4">bord</w> <w n="11.5">du</w> <w n="11.6">toit</w> <w n="11.7">les</w> <w n="11.8">grappes</w> <w n="11.9">de</w> <w n="11.10">glycine</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">À</w> <w n="12.2">l</w>’<w n="12.3">orient</w> <w n="12.4">d</w>’<w n="12.5">un</w> <w n="12.6">coeur</w> <w n="12.7">nocturne</w>, <w n="12.8">ainsi</w> <w n="12.9">l</w>’<w n="12.10">amour</w></l>
						<l n="13" num="4.2"><w n="13.1">Déploiera</w> <w n="13.2">son</w> <w n="13.3">plumage</w> <w n="13.4">éblouissant</w> <w n="13.5">de</w> <w n="13.6">cygne</w> ;</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">j</w>’<w n="14.3">en</w> <w n="14.4">ai</w> <w n="14.5">peur</w> <w n="14.6">comme</w> <w n="14.7">une</w> <w n="14.8">étoile</w> <w n="14.9">a</w> <w n="14.10">peur</w> <w n="14.11">du</w> <w n="14.12">jour</w>.</l>
					</lg>
				</div></body></text></TEI>