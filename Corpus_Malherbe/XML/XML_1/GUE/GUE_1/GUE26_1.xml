<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Le Cœur Solitaire</title>
				<title type="medium">Édition électronique</title>
				<author key="GUE">
					<name>
						<forename>Charles</forename>
						<surname>GUÉRIN</surname>
					</name>
					<date from="1873" to="1907">1873-1907</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2116 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">GUE_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https://www.poesies.net/charlesguerinlecœursolitaire.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition papier d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">https://archive.org/details/lecoeursolitair00gu</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>LES ÉDITIONS G. GRÈS ET Cie</publisher>
							<date when="1922">1922</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Le Cœur Solitaire</title>
						<author>Charles Guérin</author>
						<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54873c.r=%22charles%20gu%C3%A9rin%22%22le%20coeur%20solitaire%22?rk=64378;0</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Mercure de France</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les majuscules en début de vers ont été rétablies.</p>
					<p>Les guillemets simples ont été remplacés par des guillemets français.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-08" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><head type="main_part">MÉLANCOLIES A VIOLLIS</head><div type="poem" key="GUE26" rhyme="none">
					<head type="number">XXVI</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Souvent</w>, <w n="1.2">le</w> <w n="1.3">front</w> <w n="1.4">posé</w> <w n="1.5">sur</w> <w n="1.6">tes</w> <w n="1.7">genoux</w>, <w n="1.8">je</w> <w n="1.9">pleure</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Plus</w> <w n="2.2">faible</w> <w n="2.3">que</w> <w n="2.4">ton</w> <w n="2.5">coeur</w> <w n="2.6">amoureux</w>, <w n="2.7">faible</w> <w n="2.8">femme</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">ma</w> <w n="3.3">main</w> <w n="3.4">qui</w> <w n="3.5">frémit</w> <w n="3.6">en</w> <w n="3.7">recevant</w> <w n="3.8">tes</w> <w n="3.9">larmes</w></l>
						<l n="4" num="1.4"><w n="4.1">Se</w> <w n="4.2">dérobe</w> <w n="4.3">aux</w> <w n="4.4">baisers</w> <w n="4.5">de</w> <w n="4.6">feu</w> <w n="4.7">dont</w> <w n="4.8">tu</w> <w n="4.9">l</w>’<w n="4.10">effleures</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">Mais</w>, <w n="5.2">dis</w>-<w n="5.3">tu</w>, <w n="5.4">cher</w> <w n="5.5">petit</w> <w n="5.6">enfant</w>, <w n="5.7">tu</w> <w n="5.8">m</w>’<w n="5.9">inquiètes</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">J</w>’<w n="6.2">ai</w> <w n="6.3">peur</w> <w n="6.4">obscurément</w> <w n="6.5">de</w> <w n="6.6">cette</w> <w n="6.7">peine</w> <w n="6.8">étrange</w> :</l>
						<l n="7" num="2.3"><w n="7.1">Quel</w> <w n="7.2">incurable</w> <w n="7.3">rêve</w> <w n="7.4">ignoré</w> <w n="7.5">des</w> <w n="7.6">amantes</w></l>
						<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">infini</w> <w n="8.3">met</w>-<w n="8.4">il</w> <w n="8.5">donc</w> <w n="8.6">au</w> <w n="8.7">coeur</w> <w n="8.8">de</w> <w n="8.9">ces</w> <w n="8.10">poètes</w> ?  »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">ne</w> <w n="9.3">faut</w> <w n="9.4">plus</w> <w n="9.5">parler</w>, <w n="9.6">ma</w> <w n="9.7">bien</w>-<w n="9.8">aimée</w>. <w n="9.9">Ah</w> ! <w n="9.10">Laisse</w>…</l>
						<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">douceur</w> <w n="10.3">de</w> <w n="10.4">tes</w> <w n="10.5">doigts</w> <w n="10.6">à</w> <w n="10.7">mes</w> <w n="10.8">tempes</w> <w n="10.9">me</w> <w n="10.10">blesse</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Sache</w> <w n="11.2">qu</w>’<w n="11.3">il</w> <w n="11.4">est</w> <w n="11.5">ainsi</w> <w n="11.6">d</w>’<w n="11.7">immenses</w> <w n="11.8">nuits</w> <w n="11.9">d</w>’<w n="11.10">étoiles</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Où</w> <w n="12.2">j</w>’<w n="12.3">implore</w>, <w n="12.4">malgré</w> <w n="12.5">mon</w> <w n="12.6">coeur</w>, <w n="12.7">que</w> <w n="12.8">tu</w> <w n="12.9">t</w>’<w n="12.10">éloignes</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Où</w> <w n="13.2">ta</w> <w n="13.3">voix</w>, <w n="13.4">tes</w> <w n="13.5">serments</w>, <w n="13.6">ta</w> <w n="13.7">bouche</w> <w n="13.8">et</w> <w n="13.9">ta</w> <w n="13.10">chair</w> <w n="13.11">nue</w></l>
						<l n="14" num="4.3"><w n="14.1">Ne</w> <w n="14.2">font</w> <w n="14.3">qu</w>’<w n="14.4">approfondir</w> <w n="14.5">ma</w> <w n="14.6">détresse</w> <w n="14.7">inconnue</w>.</l>
					</lg>
				</div></body></text></TEI>