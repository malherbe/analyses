<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="COR129">
					<head type="main">PRÉFACE</head>
					<lg n="1">
						<l n="1" num="1.1">« <w n="1.1">Vous</w> <w n="1.2">dont</w> <w n="1.3">un</w> <w n="1.4">poids</w> <w n="1.5">trop</w> <w n="1.6">lourd</w> <w n="1.7">étouffe</w> <w n="1.8">la</w> <w n="1.9">vigueur</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Vous</w> <w n="2.2">que</w> <w n="2.3">je</w> <w n="2.4">vois</w> <w n="2.5">gémir</w> <w n="2.6">sous</w> <w n="2.7">un</w> <w n="2.8">travail</w> <w n="2.9">trop</w> <w n="2.10">rude</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Accourez</w> <w n="3.2">tous</w> <w n="3.3">à</w> <w n="3.4">moi</w>, <w n="3.5">venez</w>, <w n="3.6">dit</w> <w n="3.7">le</w> <w n="3.8">seigneur</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Venez</w>, <w n="4.2">je</w> <w n="4.3">vous</w> <w n="4.4">rendrai</w> <w n="4.5">de</w> <w n="4.6">la</w> <w n="4.7">force</w> <w n="4.8">et</w> <w n="4.9">du</w> <w n="4.10">cœur</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">vous</w> <w n="5.3">affranchirai</w> <w n="5.4">de</w> <w n="5.5">toute</w> <w n="5.6">lassitude</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">pain</w> <w n="6.3">que</w> <w n="6.4">je</w> <w n="6.5">réserve</w> <w n="6.6">à</w> <w n="6.7">qui</w> <w n="6.8">me</w> <w n="6.9">sait</w> <w n="6.10">chercher</w></l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">N</w>’<w n="7.2">est</w> <w n="7.3">autre</w> <w n="7.4">que</w> <w n="7.5">ma</w> <w n="7.6">propre</w> <w n="7.7">chair</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Que</w> <w n="8.2">je</w> <w n="8.3">dois</w> <w n="8.4">à</w> <w n="8.5">mon</w> <w n="8.6">père</w> <w n="8.7">offrir</w> <w n="8.8">pour</w> <w n="8.9">votre</w> <w n="8.10">vie</w> :</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Prenez</w>, <w n="9.2">mangez</w>, <w n="9.3">c</w>’<w n="9.4">est</w> <w n="9.5">mon</w> <w n="9.6">vrai</w> <w n="9.7">corps</w></l>
						<l n="10" num="1.10"><w n="10.1">Qu</w>’<w n="10.2">on</w> <w n="10.3">livrera</w> <w n="10.4">pour</w> <w n="10.5">vous</w> <w n="10.6">aux</w> <w n="10.7">rages</w> <w n="10.8">de</w> <w n="10.9">l</w>’<w n="10.10">envie</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">qui</w> <w n="11.3">d</w>’<w n="11.4">un</w> <w n="11.5">pain</w> <w n="11.6">visible</w> <w n="11.7">emprunte</w> <w n="11.8">les</w> <w n="11.9">dehors</w>.</l>
						<l n="12" num="1.12">« <w n="12.1">Faites</w> <w n="12.2">en</w> <w n="12.3">ma</w> <w n="12.4">mémoire</w> <w n="12.5">un</w> <w n="12.6">jour</w> <w n="12.7">à</w> <w n="12.8">votre</w> <w n="12.9">rang</w></l>
						<l n="13" num="1.13"><w n="13.1">Ce</w> <w n="13.2">qu</w>’<w n="13.3">à</w> <w n="13.4">vos</w> <w n="13.5">yeux</w> <w n="13.6">je</w> <w n="13.7">fais</w> <w n="13.8">avant</w> <w n="13.9">ma</w> <w n="13.10">dernière</w> <w n="13.11">heure</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Ceux</w> <w n="14.2">qui</w> <w n="14.3">mangent</w> <w n="14.4">ma</w> <w n="14.5">chair</w>, <w n="14.6">ceux</w> <w n="14.7">qui</w> <w n="14.8">boivent</w> <w n="14.9">mon</w> <w n="14.10">sang</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Ce</w> <w n="15.2">sang</w> <w n="15.3">qui</w> <w n="15.4">dans</w> <w n="15.5">ce</w> <w n="15.6">vase</w> <w n="15.7">est</w> <w n="15.8">tel</w> <w n="15.9">que</w> <w n="15.10">dans</w> <w n="15.11">mon</w> <w n="15.12">flanc</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Demeurent</w> <w n="16.2">dans</w> <w n="16.3">moi</w>-<w n="16.4">même</w>, <w n="16.5">et</w> <w n="16.6">dans</w> <w n="16.7">eux</w> <w n="16.8">je</w> <w n="16.9">demeure</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Dites</w> <w n="17.2">ce</w> <w n="17.3">que</w> <w n="17.4">je</w> <w n="17.5">dis</w> <w n="17.6">pour</w> <w n="17.7">faire</w> <w n="17.8">comme</w> <w n="17.9">moi</w> :</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">L</w>’<w n="18.2">efficace</w> <w n="18.3">de</w> <w n="18.4">votre</w> <w n="18.5">foi</w></l>
						<l n="19" num="1.19"><w n="19.1">Produira</w> <w n="19.2">même</w> <w n="19.3">effet</w> <w n="19.4">par</w> <w n="19.5">les</w> <w n="19.6">paroles</w> <w n="19.7">mêmes</w> ;</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">Donnez</w> <w n="20.2">aux</w> <w n="20.3">miennes</w> <w n="20.4">plein</w> <w n="20.5">crédit</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">n</w>’<w n="21.3">oubliez</w> <w n="21.4">jamais</w> <w n="21.5">que</w> <w n="21.6">mes</w> <w n="21.7">bontés</w> <w n="21.8">suprêmes</w></l>
						<l n="22" num="1.22"><w n="22.1">Les</w> <w n="22.2">remplissent</w> <w n="22.3">toujours</w> <w n="22.4">et</w> <w n="22.5">de</w> <w n="22.6">vie</w> <w n="22.7">et</w> <w n="22.8">d</w>’<w n="22.9">esprit</w>. »</l>
					</lg>
				</div></body></text></TEI>