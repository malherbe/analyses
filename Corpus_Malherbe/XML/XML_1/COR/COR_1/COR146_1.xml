<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="COR146">
					<head type="main">CHAPITRE XVII</head>
					<head type="sub">Du desir ardent de recevoir Jésus-Christ.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Avec</w> <w n="1.2">tous</w> <w n="1.3">les</w> <w n="1.4">transports</w> <w n="1.5">dont</w> <w n="1.6">est</w> <w n="1.7">capable</w> <w n="1.8">une</w> <w n="1.9">âme</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">toute</w> <w n="2.3">l</w>’<w n="2.4">ardeur</w> <w n="2.5">d</w>’<w n="2.6">une</w> <w n="2.7">céleste</w> <w n="2.8">flamme</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Avec</w> <w n="3.2">tous</w> <w n="3.3">les</w> <w n="3.4">élans</w> <w n="3.5">d</w>’<w n="3.6">un</w> <w n="3.7">zèle</w> <w n="3.8">affectueux</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">les</w> <w n="4.3">humbles</w> <w n="4.4">devoirs</w> <w n="4.5">d</w>’<w n="4.6">un</w> <w n="4.7">cœur</w> <w n="4.8">respectueux</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">souhaite</w> <w n="5.3">approcher</w> <w n="5.4">de</w> <w n="5.5">ta</w> <w n="5.6">divine</w> <w n="5.7">table</w>,</l>
						<l n="6" num="1.6"><w n="6.1">J</w>’<w n="6.2">y</w> <w n="6.3">souhaite</w> <w n="6.4">porter</w> <w n="6.5">cet</w> <w n="6.6">amour</w> <w n="6.7">véritable</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Cette</w> <w n="7.2">ferveur</w> <w n="7.3">sincère</w> <w n="7.4">et</w> <w n="7.5">ces</w> <w n="7.6">fermes</w> <w n="7.7">propos</w></l>
						<l n="8" num="1.8"><w n="8.1">Qu</w>’<w n="8.2">y</w> <w n="8.3">portèrent</w> <w n="8.4">jadis</w> <w n="8.5">tant</w> <w n="8.6">d</w>’<w n="8.7">illustres</w> <w n="8.8">dévots</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Tant</w> <w n="9.2">d</w>’<w n="9.3">élus</w>, <w n="9.4">tant</w> <w n="9.5">de</w> <w n="9.6">saints</w>, <w n="9.7">dont</w> <w n="9.8">la</w> <w n="9.9">vie</w> <w n="9.10">exemplaire</w></l>
						<l n="10" num="1.10"><w n="10.1">Sut</w> <w n="10.2">le</w> <w n="10.3">mieux</w> <w n="10.4">pratiquer</w> <w n="10.5">le</w> <w n="10.6">grand</w> <w n="10.7">art</w> <w n="10.8">de</w> <w n="10.9">te</w> <w n="10.10">plaire</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Oui</w>, <w n="11.2">mon</w> <w n="11.3">Dieu</w>, <w n="11.4">mon</w> <w n="11.5">seul</w> <w n="11.6">bien</w>, <w n="11.7">mon</w> <w n="11.8">amour</w> <w n="11.9">éternel</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Tout</w> <w n="12.2">chétif</w> <w n="12.3">que</w> <w n="12.4">je</w> <w n="12.5">suis</w>, <w n="12.6">tout</w> <w n="12.7">lâche</w> <w n="12.8">et</w> <w n="12.9">criminel</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Je</w> <w n="13.2">veux</w> <w n="13.3">te</w> <w n="13.4">recevoir</w> <w n="13.5">avec</w> <w n="13.6">autant</w> <w n="13.7">de</w> <w n="13.8">zèle</w></l>
						<l n="14" num="1.14"><w n="14.1">Que</w> <w n="14.2">jamais</w> <w n="14.3">de</w> <w n="14.4">tes</w> <w n="14.5">saints</w> <w n="14.6">ait</w> <w n="14.7">eu</w> <w n="14.8">le</w> <w n="14.9">plus</w> <w n="14.10">fidèle</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">je</w> <w n="15.3">souhaiterois</w> <w n="15.4">qu</w>’<w n="15.5">il</w> <w n="15.6">fût</w> <w n="15.7">en</w> <w n="15.8">mon</w> <w n="15.9">pouvoir</w></l>
						<l n="16" num="1.16"><w n="16.1">D</w>’<w n="16.2">en</w> <w n="16.3">avoir</w> <w n="16.4">encor</w> <w n="16.5">plus</w> <w n="16.6">qu</w>’<w n="16.7">il</w> <w n="16.8">n</w>’<w n="16.9">en</w> <w n="16.10">put</w> <w n="16.11">concevoir</w>.</l>
					</lg>
					<lg n="2">
						<l n="17" num="2.1"><w n="17.1">Je</w> <w n="17.2">sais</w> <w n="17.3">qu</w>’<w n="17.4">à</w> <w n="17.5">ces</w> <w n="17.6">desirs</w> <w n="17.7">en</w> <w n="17.8">vain</w> <w n="17.9">mon</w> <w n="17.10">cœur</w> <w n="17.11">s</w>’<w n="17.12">excite</w> :</l>
						<l n="18" num="2.2"><w n="18.1">Ils</w> <w n="18.2">passent</w> <w n="18.3">de</w> <w n="18.4">trop</w> <w n="18.5">loin</w> <w n="18.6">sa</w> <w n="18.7">force</w> <w n="18.8">et</w> <w n="18.9">son</w> <w n="18.10">mérite</w> ;</l>
						<l n="19" num="2.3"><w n="19.1">Mais</w> <w n="19.2">tu</w> <w n="19.3">vois</w> <w n="19.4">sa</w> <w n="19.5">portée</w>, <w n="19.6">il</w> <w n="19.7">va</w> <w n="19.8">jusques</w> <w n="19.9">au</w> <w n="19.10">bout</w> :</l>
						<l n="20" num="2.4"><w n="20.1">Il</w> <w n="20.2">t</w>’<w n="20.3">offre</w> <w n="20.4">ce</w> <w n="20.5">qu</w>’<w n="20.6">il</w> <w n="20.7">a</w>, <w n="20.8">comme</w> <w n="20.9">s</w>’<w n="20.10">il</w> <w n="20.11">avoit</w> <w n="20.12">tout</w>,</l>
						<l n="21" num="2.5"><w n="21.1">Comme</w> <w n="21.2">s</w>’<w n="21.3">il</w> <w n="21.4">avoit</w> <w n="21.5">seul</w> <w n="21.6">en</w> <w n="21.7">sa</w> <w n="21.8">pleine</w> <w n="21.9">puissance</w></l>
						<l n="22" num="2.6"><w n="22.1">Ces</w> <w n="22.2">grands</w> <w n="22.3">efforts</w> <w n="22.4">d</w>’<w n="22.5">amour</w> <w n="22.6">et</w> <w n="22.7">de</w> <w n="22.8">reconnoissance</w>,</l>
						<l n="23" num="2.7"><w n="23.1">Comme</w> <w n="23.2">s</w>’<w n="23.3">il</w> <w n="23.4">avoit</w> <w n="23.5">seul</w> <w n="23.6">tous</w> <w n="23.7">les</w> <w n="23.8">pieux</w> <w n="23.9">desirs</w></l>
						<l n="24" num="2.8"><w n="24.1">Qui</w> <w n="24.2">d</w>’<w n="24.3">une</w> <w n="24.4">âme</w> <w n="24.5">épurée</w> <w n="24.6">enflamment</w> <w n="24.7">les</w> <w n="24.8">soupirs</w>,</l>
						<l n="25" num="2.9"><w n="25.1">Comme</w> <w n="25.2">s</w>’<w n="25.3">il</w> <w n="25.4">avoit</w> <w n="25.5">seul</w> <w n="25.6">toute</w> <w n="25.7">l</w>’<w n="25.8">ardeur</w> <w n="25.9">secrète</w>,</l>
						<l n="26" num="2.10"><w n="26.1">Tous</w> <w n="26.2">les</w> <w n="26.3">profonds</w> <w n="26.4">respects</w> <w n="26.5">d</w>’<w n="26.6">une</w> <w n="26.7">vertu</w> <w n="26.8">parfaite</w>.</l>
					</lg>
					<lg n="3">
						<l n="27" num="3.1"><w n="27.1">Si</w> <w n="27.2">ce</w> <w n="27.3">qu</w>’<w n="27.4">il</w> <w n="27.5">t</w>’<w n="27.6">offre</w> <w n="27.7">est</w> <w n="27.8">peu</w>, <w n="27.9">du</w> <w n="27.10">moins</w> <w n="27.11">c</w>’<w n="27.12">est</w> <w n="27.13">tout</w> <w n="27.14">son</w> <w n="27.15">bien</w> :</l>
						<l n="28" num="3.2"><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">te</w> <w n="28.4">donner</w> <w n="28.5">beaucoup</w> <w n="28.6">que</w> <w n="28.7">ne</w> <w n="28.8">réserver</w> <w n="28.9">rien</w>.</l>
						<l n="29" num="3.3"><w n="29.1">Qui</w> <w n="29.2">de</w> <w n="29.3">tout</w> <w n="29.4">ce</w> <w n="29.5">qu</w>’<w n="29.6">il</w> <w n="29.7">a</w> <w n="29.8">te</w> <w n="29.9">fait</w> <w n="29.10">un</w> <w n="29.11">plein</w> <w n="29.12">hommage</w></l>
						<l n="30" num="3.4"><w n="30.1">T</w>’<w n="30.2">offriroit</w> <w n="30.3">beaucoup</w> <w n="30.4">plus</w>, <w n="30.5">s</w>’<w n="30.6">il</w> <w n="30.7">pouvoit</w> <w n="30.8">davantage</w>.</l>
						<l n="31" num="3.5"><w n="31.1">Je</w> <w n="31.2">m</w>’<w n="31.3">offre</w> <w n="31.4">donc</w> <w n="31.5">entier</w>, <w n="31.6">et</w> <w n="31.7">tout</w> <w n="31.8">ce</w> <w n="31.9">que</w> <w n="31.10">je</w> <w n="31.11">puis</w>,</l>
						<l n="32" num="3.6"><w n="32.1">Sans</w> <w n="32.2">rien</w> <w n="32.3">garder</w> <w n="32.4">pour</w> <w n="32.5">moi</w> <w n="32.6">de</w> <w n="32.7">tout</w> <w n="32.8">ce</w> <w n="32.9">que</w> <w n="32.10">je</w> <w n="32.11">suis</w> :</l>
						<l n="33" num="3.7"><w n="33.1">Je</w> <w n="33.2">m</w>’<w n="33.3">immole</w> <w n="33.4">moi</w>-<w n="33.5">même</w>, <w n="33.6">et</w> <w n="33.7">pour</w> <w n="33.8">toute</w> <w n="33.9">ma</w> <w n="33.10">vie</w>,</l>
						<l n="34" num="3.8"><w n="34.1">Au</w> <w n="34.2">pied</w> <w n="34.3">de</w> <w n="34.4">tes</w> <w n="34.5">autels</w>, <w n="34.6">en</w> <w n="34.7">volontaire</w> <w n="34.8">hostie</w>.</l>
					</lg>
					<lg n="4">
						<l n="35" num="4.1"><w n="35.1">Que</w> <w n="35.2">ne</w> <w n="35.3">puis</w>-<w n="35.4">je</w>, <w n="35.5">ô</w> <w n="35.6">mon</w> <w n="35.7">Dieu</w>, <w n="35.8">suppléer</w> <w n="35.9">mon</w> <w n="35.10">défaut</w></l>
						<l n="36" num="4.2"><w n="36.1">Par</w> <w n="36.2">tout</w> <w n="36.3">ce</w> <w n="36.4">qu</w>’<w n="36.5">après</w> <w n="36.6">toi</w> <w n="36.7">le</w> <w n="36.8">ciel</w> <w n="36.9">a</w> <w n="36.10">de</w> <w n="36.11">plus</w> <w n="36.12">haut</w> !</l>
						<l n="37" num="4.3"><w n="37.1">Et</w> <w n="37.2">pour</w> <w n="37.3">mieux</w> <w n="37.4">exprimer</w> <w n="37.5">tout</w> <w n="37.6">ce</w> <w n="37.7">que</w> <w n="37.8">je</w> <w n="37.9">desire</w></l>
						<l n="38" num="4.4">(<w n="38.1">mais</w>, <w n="38.2">ô</w> <w n="38.3">mon</w> <w n="38.4">rédempteur</w>, <w n="38.5">t</w>’<w n="38.6">oserai</w>-<w n="38.7">je</w> <w n="38.8">le</w> <w n="38.9">dire</w> ?</l>
						<l n="39" num="4.5"><w n="39.1">Si</w> <w n="39.2">je</w> <w n="39.3">te</w> <w n="39.4">fais</w> <w n="39.5">l</w>’<w n="39.6">aveu</w> <w n="39.7">de</w> <w n="39.8">ma</w> <w n="39.9">témérité</w>,</l>
						<l n="40" num="4.6"><w n="40.1">Lui</w> <w n="40.2">pardonneras</w>-<w n="40.3">tu</w> <w n="40.4">d</w>’<w n="40.5">avoir</w> <w n="40.6">tant</w> <w n="40.7">souhaité</w> ? ),</l>
						<l n="41" num="4.7"><w n="41.1">Je</w> <w n="41.2">souhaite</w> <w n="41.3">aujourd</w>’<w n="41.4">hui</w> <w n="41.5">recevoir</w> <w n="41.6">ce</w> <w n="41.7">mystère</w></l>
						<l n="42" num="4.8"><w n="42.1">Ainsi</w> <w n="42.2">que</w> <w n="42.3">te</w> <w n="42.4">reçut</w> <w n="42.5">ta</w> <w n="42.6">glorieuse</w> <w n="42.7">mère</w>,</l>
						<l n="43" num="4.9"><w n="43.1">Lorsqu</w>’<w n="43.2">aux</w> <w n="43.3">avis</w> <w n="43.4">qu</w>’<w n="43.5">un</w> <w n="43.6">ange</w> <w n="43.7">exprès</w> <w n="43.8">lui</w> <w n="43.9">vint</w> <w n="43.10">donner</w></l>
						<l n="44" num="4.10"><w n="44.1">Du</w> <w n="44.2">choix</w> <w n="44.3">que</w> <w n="44.4">faisoit</w> <w n="44.5">d</w>’<w n="44.6">elle</w> <w n="44.7">un</w> <w n="44.8">Dieu</w> <w n="44.9">pour</w> <w n="44.10">s</w>’<w n="44.11">incarner</w>,</l>
						<l n="45" num="4.11"><w n="45.1">Elle</w> <w n="45.2">lui</w> <w n="45.3">répondit</w> <w n="45.4">et</w> <w n="45.5">confuse</w> <w n="45.6">et</w> <w n="45.7">constante</w> :</l>
						<l n="46" num="4.12">« <w n="46.1">Je</w> <w n="46.2">ne</w> <w n="46.3">suis</w> <w n="46.4">du</w> <w n="46.5">seigneur</w> <w n="46.6">que</w> <w n="46.7">l</w>’<w n="46.8">indigne</w> <w n="46.9">servante</w> ;</l>
						<l n="47" num="4.13"><w n="47.1">Qu</w>’<w n="47.2">il</w> <w n="47.3">fasse</w> <w n="47.4">agir</w> <w n="47.5">sur</w> <w n="47.6">moi</w> <w n="47.7">son</w> <w n="47.8">pouvoir</w> <w n="47.9">absolu</w>,</l>
						<l n="48" num="4.14"><w n="48.1">Comme</w> <w n="48.2">tu</w> <w n="48.3">me</w> <w n="48.4">le</w> <w n="48.5">dis</w> <w n="48.6">et</w> <w n="48.7">qu</w>’<w n="48.8">il</w> <w n="48.9">l</w>’<w n="48.10">a</w> <w n="48.11">résolu</w>. »</l>
						<l n="49" num="4.15"><w n="49.1">Tout</w> <w n="49.2">ce</w> <w n="49.3">qu</w>’<w n="49.4">elle</w> <w n="49.5">eut</w> <w n="49.6">alors</w> <w n="49.7">pour</w> <w n="49.8">toi</w> <w n="49.9">de</w> <w n="49.10">révérence</w>,</l>
						<l n="50" num="4.16"><w n="50.1">De</w> <w n="50.2">louanges</w>, <w n="50.3">d</w>’<w n="50.4">amour</w>, <w n="50.5">et</w> <w n="50.6">de</w> <w n="50.7">reconnoissance</w>,</l>
						<l n="51" num="4.17"><w n="51.1">Tout</w> <w n="51.2">ce</w> <w n="51.3">qu</w>’<w n="51.4">elle</w> <w n="51.5">eut</w> <w n="51.6">de</w> <w n="51.7">foi</w>, <w n="51.8">d</w>’<w n="51.9">espoir</w>, <w n="51.10">de</w> <w n="51.11">pureté</w>,</l>
						<l n="52" num="4.18"><w n="52.1">Durant</w> <w n="52.2">ce</w> <w n="52.3">digne</w> <w n="52.4">effort</w> <w n="52.5">de</w> <w n="52.6">son</w> <w n="52.7">humilité</w>,</l>
						<l n="53" num="4.19"><w n="53.1">Je</w> <w n="53.2">voudrois</w> <w n="53.3">tout</w> <w n="53.4">porter</w> <w n="53.5">à</w> <w n="53.6">cette</w> <w n="53.7">sainte</w> <w n="53.8">table</w></l>
						<l n="54" num="4.20"><w n="54.1">Où</w> <w n="54.2">tu</w> <w n="54.3">repais</w> <w n="54.4">les</w> <w n="54.5">tiens</w> <w n="54.6">de</w> <w n="54.7">ton</w> <w n="54.8">corps</w> <w n="54.9">adorable</w>.</l>
						<l n="55" num="4.21"><w n="55.1">Que</w> <w n="55.2">ne</w> <w n="55.3">puis</w>-<w n="55.4">je</w> <w n="55.5">du</w> <w n="55.6">moins</w> <w n="55.7">par</w> <w n="55.8">un</w> <w n="55.9">céleste</w> <w n="55.10">feu</w></l>
						<l n="56" num="4.22"><w n="56.1">À</w> <w n="56.2">ton</w> <w n="56.3">grand</w> <w n="56.4">précurseur</w> <w n="56.5">ressembler</w> <w n="56.6">tant</w> <w n="56.7">soit</w> <w n="56.8">peu</w>,</l>
						<l n="57" num="4.23"><w n="57.1">À</w> <w n="57.2">cet</w> <w n="57.3">illustre</w> <w n="57.4">saint</w>, <w n="57.5">dont</w> <w n="57.6">la</w> <w n="57.7">haute</w> <w n="57.8">excellence</w></l>
						<l n="58" num="4.24"><w n="58.1">Semble</w> <w n="58.2">sur</w> <w n="58.3">tout</w> <w n="58.4">le</w> <w n="58.5">reste</w> <w n="58.6">emporter</w> <w n="58.7">la</w> <w n="58.8">balance</w> !</l>
						<l n="59" num="4.25"><w n="59.1">Que</w> <w n="59.2">n</w>’<w n="59.3">ai</w>-<w n="59.4">je</w> <w n="59.5">les</w> <w n="59.6">élans</w> <w n="59.7">dont</w> <w n="59.8">il</w> <w n="59.9">fut</w> <w n="59.10">animé</w></l>
						<l n="60" num="4.26"><w n="60.1">Lorsqu</w>’<w n="60.2">aux</w> <w n="60.3">flancs</w> <w n="60.4">maternels</w> <w n="60.5">encor</w> <w n="60.6">tout</w> <w n="60.7">enfermé</w>,</l>
						<l n="61" num="4.27"><w n="61.1">Impatient</w> <w n="61.2">déjà</w> <w n="61.3">de</w> <w n="61.4">préparer</w> <w n="61.5">ta</w> <w n="61.6">voie</w>,</l>
						<l n="62" num="4.28"><w n="62.1">Il</w> <w n="62.2">sentit</w> <w n="62.3">ta</w> <w n="62.4">présence</w>, <w n="62.5">et</w> <w n="62.6">tressaillit</w> <w n="62.7">de</w> <w n="62.8">joie</w>,</l>
						<l n="63" num="4.29"><w n="63.1">Mais</w> <w n="63.2">d</w>’<w n="63.3">une</w> <w n="63.4">sainte</w> <w n="63.5">joie</w> <w n="63.6">et</w> <w n="63.7">d</w>’<w n="63.8">un</w> <w n="63.9">tressaillement</w></l>
						<l n="64" num="4.30"><w n="64.1">Dont</w> <w n="64.2">le</w> <w n="64.3">Saint</w>-<w n="64.4">Esprit</w> <w n="64.5">seul</w> <w n="64.6">formoit</w> <w n="64.7">le</w> <w n="64.8">mouvement</w> !</l>
					</lg>
					<lg n="5">
						<l n="65" num="5.1"><w n="65.1">Lorsqu</w>’<w n="65.2">il</w> <w n="65.3">te</w> <w n="65.4">vit</w> <w n="65.5">ensuite</w> <w n="65.6">être</w> <w n="65.7">ce</w> <w n="65.8">que</w> <w n="65.9">nous</w> <w n="65.10">sommes</w>,</l>
						<l n="66" num="5.2"><w n="66.1">Converser</w>, <w n="66.2">enseigner</w>, <w n="66.3">vivre</w> <w n="66.4">parmi</w> <w n="66.5">les</w> <w n="66.6">hommes</w>,</l>
						<l n="67" num="5.3"><w n="67.1">Tout</w> <w n="67.2">enflammé</w> <w n="67.3">d</w>’<w n="67.4">ardeur</w> : « <w n="67.5">quiconque</w> <w n="67.6">aime</w> <w n="67.7">l</w>’<w n="67.8">époux</w>,</l>
						<l n="68" num="5.4"><w n="68.1">Cria</w>-<w n="68.2">t</w>-<w n="68.3">il</w>, <w n="68.4">de</w> <w n="68.5">sa</w> <w n="68.6">voix</w> <w n="68.7">trouve</w> <w n="68.8">l</w>’<w n="68.9">accent</w> <w n="68.10">si</w> <w n="68.11">doux</w>,</l>
						<l n="69" num="5.5"><w n="69.1">Que</w> <w n="69.2">de</w> <w n="69.3">ses</w> <w n="69.4">tons</w> <w n="69.5">charmeurs</w> <w n="69.6">l</w>’<w n="69.7">amoureuse</w> <w n="69.8">tendresse</w>,</l>
						<l n="70" num="5.6"><w n="70.1">Sitôt</w> <w n="70.2">qu</w>’<w n="70.3">il</w> <w n="70.4">les</w> <w n="70.5">entend</w>, <w n="70.6">le</w> <w n="70.7">comble</w> <w n="70.8">d</w>’<w n="70.9">allégresse</w>. »</l>
						<l n="71" num="5.7"><w n="71.1">Que</w> <w n="71.2">n</w>’<w n="71.3">ai</w>-<w n="71.4">je</w> <w n="71.5">ainsi</w> <w n="71.6">que</w> <w n="71.7">lui</w> <w n="71.8">ces</w> <w n="71.9">hauts</w> <w n="71.10">ravissements</w>,</l>
						<l n="72" num="5.8"><w n="72.1">Ces</w> <w n="72.2">desirs</w> <w n="72.3">embrasés</w>, <w n="72.4">et</w> <w n="72.5">ces</w> <w n="72.6">grands</w> <w n="72.7">sentiments</w>,</l>
						<l n="73" num="5.9"><w n="73.1">Afin</w> <w n="73.2">que</w> <w n="73.3">tout</w> <w n="73.4">mon</w> <w n="73.5">cœur</w> <w n="73.6">dans</w> <w n="73.7">un</w> <w n="73.8">transport</w> <w n="73.9">sublime</w></l>
						<l n="74" num="5.10"><w n="74.1">T</w>’<w n="74.2">offre</w> <w n="74.3">une</w> <w n="74.4">plus</w> <w n="74.5">entière</w> <w n="74.6">et</w> <w n="74.7">plus</w> <w n="74.8">noble</w> <w n="74.9">victime</w> ?</l>
					</lg>
					<lg n="6">
						<l n="75" num="6.1"><w n="75.1">J</w>’<w n="75.2">ajoute</w> <w n="75.3">donc</w> <w n="75.4">au</w> <w n="75.5">peu</w> <w n="75.6">qu</w>’<w n="75.7">il</w> <w n="75.8">m</w>’<w n="75.9">est</w> <w n="75.10">permis</w> <w n="75.11">d</w>’<w n="75.12">avoir</w></l>
						<l n="76" num="6.2"><w n="76.1">Tout</w> <w n="76.2">ce</w> <w n="76.3">que</w> <w n="76.4">tes</w> <w n="76.5">dévots</w> <w n="76.6">en</w> <w n="76.7">peuvent</w> <w n="76.8">concevoir</w>,</l>
					</lg>
					<lg n="7">
						<l n="77" num="7.1"><w n="77.1">Ces</w> <w n="77.2">entretiens</w> <w n="77.3">ardents</w>, <w n="77.4">ces</w> <w n="77.5">ferveurs</w> <w n="77.6">extatiques</w></l>
						<l n="78" num="7.2"><w n="78.1">Où</w> <w n="78.2">seul</w> <w n="78.3">à</w> <w n="78.4">seul</w> <w n="78.5">toi</w>-<w n="78.6">même</w> <w n="78.7">avec</w> <w n="78.8">eux</w> <w n="78.9">tu</w> <w n="78.10">t</w>’<w n="78.11">expliques</w>,</l>
						<l n="79" num="7.3"><w n="79.1">Ces</w> <w n="79.2">lumières</w> <w n="79.3">d</w>’<w n="79.4">en</w> <w n="79.5">haut</w> <w n="79.6">qui</w> <w n="79.7">leur</w> <w n="79.8">ouvrent</w> <w n="79.9">les</w> <w n="79.10">cieux</w>,</l>
						<l n="80" num="7.4"><w n="80.1">Ces</w> <w n="80.2">claires</w> <w n="80.3">visions</w> <w n="80.4">pour</w> <w n="80.5">qui</w> <w n="80.6">l</w>’<w n="80.7">âme</w> <w n="80.8">a</w> <w n="80.9">des</w> <w n="80.10">yeux</w>,</l>
						<l n="81" num="7.5"><w n="81.1">Ces</w> <w n="81.2">amas</w> <w n="81.3">de</w> <w n="81.4">vertus</w>, <w n="81.5">ces</w> <w n="81.6">concerts</w> <w n="81.7">de</w> <w n="81.8">louanges</w>,</l>
						<l n="82" num="7.6"><w n="82.1">Que</w> <w n="82.2">les</w> <w n="82.3">hommes</w> <w n="82.4">sur</w> <w n="82.5">terre</w> <w n="82.6">et</w> <w n="82.7">qu</w>’<w n="82.8">au</w> <w n="82.9">ciel</w> <w n="82.10">tous</w> <w n="82.11">les</w> <w n="82.12">anges</w>,</l>
						<l n="83" num="7.7"><w n="83.1">Que</w> <w n="83.2">toute</w> <w n="83.3">créature</w> <w n="83.4">enfin</w> <w n="83.5">pour</w> <w n="83.6">tes</w> <w n="83.7">bienfaits</w></l>
						<l n="84" num="7.8"><w n="84.1">Et</w> <w n="84.2">te</w> <w n="84.3">rend</w> <w n="84.4">chaque</w> <w n="84.5">jour</w>, <w n="84.6">et</w> <w n="84.7">te</w> <w n="84.8">rendra</w> <w n="84.9">jamais</w>.</l>
						<l n="85" num="7.9"><w n="85.1">J</w>’<w n="85.2">offre</w> <w n="85.3">tous</w> <w n="85.4">ces</w> <w n="85.5">desirs</w>, <w n="85.6">ces</w> <w n="85.7">ardeurs</w>, <w n="85.8">ces</w> <w n="85.9">lumières</w>,</l>
						<l n="86" num="7.10"><w n="86.1">Pour</w> <w n="86.2">moi</w>, <w n="86.3">pour</w> <w n="86.4">les</w> <w n="86.5">pécheurs</w> <w n="86.6">commis</w> <w n="86.7">à</w> <w n="86.8">mes</w> <w n="86.9">prières</w>,</l>
						<l n="87" num="7.11"><w n="87.1">Pour</w> <w n="87.2">nous</w> <w n="87.3">unir</w> <w n="87.4">ensemble</w> <w n="87.5">et</w> <w n="87.6">nous</w> <w n="87.7">sacrifier</w></l>
						<l n="88" num="7.12"><w n="88.1">À</w> <w n="88.2">te</w> <w n="88.3">louer</w> <w n="88.4">sans</w> <w n="88.5">cesse</w> <w n="88.6">et</w> <w n="88.7">te</w> <w n="88.8">glorifier</w>.</l>
					</lg>
					<lg n="8">
						<l n="89" num="8.1"><w n="89.1">Reçois</w> <w n="89.2">de</w> <w n="89.3">moi</w> <w n="89.4">ces</w> <w n="89.5">vœux</w> <w n="89.6">d</w>’<w n="89.7">allégresse</w> <w n="89.8">infinie</w>,</l>
						<l n="90" num="8.2"><w n="90.1">Ces</w> <w n="90.2">desirs</w> <w n="90.3">que</w> <w n="90.4">partout</w> <w n="90.5">ta</w> <w n="90.6">bonté</w> <w n="90.7">soit</w> <w n="90.8">bénie</w>,</l>
						<l n="91" num="8.3"><w n="91.1">Ces</w> <w n="91.2">vœux</w> <w n="91.3">justement</w> <w n="91.4">dus</w> <w n="91.5">à</w> <w n="91.6">ton</w> <w n="91.7">infinité</w>,</l>
						<l n="92" num="8.4"><w n="92.1">Ces</w> <w n="92.2">desirs</w> <w n="92.3">que</w> <w n="92.4">tout</w> <w n="92.5">doit</w> <w n="92.6">à</w> <w n="92.7">ton</w> <w n="92.8">immensité</w> :</l>
						<l n="93" num="8.5"><w n="93.1">Je</w> <w n="93.2">te</w> <w n="93.3">les</w> <w n="93.4">rends</w>, <w n="93.5">seigneur</w>, <w n="93.6">et</w> <w n="93.7">je</w> <w n="93.8">te</w> <w n="93.9">les</w> <w n="93.10">veux</w> <w n="93.11">rendre</w>,</l>
						<l n="94" num="8.6"><w n="94.1">Tant</w> <w n="94.2">que</w> <w n="94.3">de</w> <w n="94.4">mon</w> <w n="94.5">exil</w> <w n="94.6">le</w> <w n="94.7">cours</w> <w n="94.8">pourra</w> <w n="94.9">s</w>’<w n="94.10">étendre</w>,</l>
						<l n="95" num="8.7"><w n="95.1">Chaque</w> <w n="95.2">jour</w>, <w n="95.3">chaque</w> <w n="95.4">instant</w>, <w n="95.5">devant</w> <w n="95.6">tous</w>, <w n="95.7">en</w> <w n="95.8">tous</w> <w n="95.9">lieux</w>.</l>
						<l n="96" num="8.8"><w n="96.1">Puisse</w> <w n="96.2">tout</w> <w n="96.3">ce</w> <w n="96.4">qu</w>’<w n="96.5">il</w> <w n="96.6">est</w> <w n="96.7">d</w>’<w n="96.8">esprits</w> <w n="96.9">saints</w> <w n="96.10">dans</w> <w n="96.11">les</w> <w n="96.12">cieux</w>,</l>
						<l n="97" num="8.9"><w n="97.1">Puisse</w> <w n="97.2">tout</w> <w n="97.3">ce</w> <w n="97.4">qu</w>’<w n="97.5">il</w> <w n="97.6">est</w> <w n="97.7">en</w> <w n="97.8">terre</w> <w n="97.9">de</w> <w n="97.10">fidèles</w>,</l>
						<l n="98" num="8.10"><w n="98.1">Te</w> <w n="98.2">rendre</w> <w n="98.3">ainsi</w> <w n="98.4">que</w> <w n="98.5">moi</w> <w n="98.6">des</w> <w n="98.7">grâces</w> <w n="98.8">éternelles</w>,</l>
						<l n="99" num="8.11"><w n="99.1">Te</w> <w n="99.2">bénir</w> <w n="99.3">avec</w> <w n="99.4">moi</w> <w n="99.5">de</w> <w n="99.6">l</w>’<w n="99.7">excès</w> <w n="99.8">de</w> <w n="99.9">tes</w> <w n="99.10">biens</w>,</l>
						<l n="100" num="8.12"><w n="100.1">Et</w> <w n="100.2">joindre</w> <w n="100.3">avec</w> <w n="100.4">ferveur</w> <w n="100.5">tous</w> <w n="100.6">leurs</w> <w n="100.7">encens</w> <w n="100.8">aux</w> <w n="100.9">miens</w> !</l>
					</lg>
					<lg n="9">
						<l n="101" num="9.1"><w n="101.1">Que</w> <w n="101.2">des</w> <w n="101.3">peuples</w> <w n="101.4">divers</w> <w n="101.5">les</w> <w n="101.6">différents</w> <w n="101.7">langages</w></l>
						<l n="102" num="9.2"><w n="102.1">Ne</w> <w n="102.2">fassent</w> <w n="102.3">qu</w>’<w n="102.4">une</w> <w n="102.5">voix</w> <w n="102.6">pour</w> <w n="102.7">t</w>’<w n="102.8">offrir</w> <w n="102.9">leurs</w> <w n="102.10">hommages</w> !</l>
						<l n="103" num="9.3"><w n="103.1">Que</w> <w n="103.2">tous</w> <w n="103.3">mettent</w> <w n="103.4">leur</w> <w n="103.5">gloire</w> <w n="103.6">et</w> <w n="103.7">leur</w> <w n="103.8">ambition</w></l>
						<l n="104" num="9.4"><w n="104.1">À</w> <w n="104.2">louer</w> <w n="104.3">à</w> <w n="104.4">l</w>’<w n="104.5">envi</w> <w n="104.6">les</w> <w n="104.7">grandeurs</w> <w n="104.8">de</w> <w n="104.9">ton</w> <w n="104.10">nom</w> !</l>
					</lg>
					<lg n="10">
						<l n="105" num="10.1"><w n="105.1">Fais</w>, <w n="105.2">seigneur</w>, <w n="105.3">que</w> <w n="105.4">tous</w> <w n="105.5">ceux</w> <w n="105.6">qu</w>’<w n="105.7">un</w> <w n="105.8">zèle</w> <w n="105.9">véritable</w></l>
						<l n="106" num="10.2"><w n="106.1">Anime</w> <w n="106.2">à</w> <w n="106.3">célébrer</w> <w n="106.4">ton</w> <w n="106.5">mystère</w> <w n="106.6">adorable</w>,</l>
						<l n="107" num="10.3"><w n="107.1">Que</w> <w n="107.2">tous</w> <w n="107.3">ceux</w> <w n="107.4">dont</w> <w n="107.5">l</w>’<w n="107.6">amour</w> <w n="107.7">te</w> <w n="107.8">reçoit</w> <w n="107.9">avec</w> <w n="107.10">foi</w></l>
						<l n="108" num="10.4"><w n="108.1">Obtiennent</w> <w n="108.2">pour</w> <w n="108.3">eux</w> <w n="108.4">grâce</w> <w n="108.5">et</w> <w n="108.6">t</w>’<w n="108.7">invoquent</w> <w n="108.8">pour</w> <w n="108.9">moi</w>.</l>
						<l n="109" num="10.5"><w n="109.1">Quand</w> <w n="109.2">la</w> <w n="109.3">sainte</w> <w n="109.4">union</w> <w n="109.5">où</w> <w n="109.6">leurs</w> <w n="109.7">souhaits</w> <w n="109.8">aspirent</w></l>
						<l n="110" num="10.6"><w n="110.1">Les</w> <w n="110.2">aura</w> <w n="110.3">tous</w> <w n="110.4">remplis</w> <w n="110.5">des</w> <w n="110.6">douceurs</w> <w n="110.7">qu</w>’<w n="110.8">ils</w> <w n="110.9">desirent</w>,</l>
						<l n="111" num="10.7"><w n="111.1">Qu</w>’<w n="111.2">ils</w> <w n="111.3">sentiront</w> <w n="111.4">en</w> <w n="111.5">eux</w> <w n="111.6">ces</w> <w n="111.7">consolations</w></l>
						<l n="112" num="10.8"><w n="112.1">Que</w> <w n="112.2">versent</w> <w n="112.3">à</w> <w n="112.4">grands</w> <w n="112.5">flots</w> <w n="112.6">tes</w> <w n="112.7">bénédictions</w>,</l>
						<l n="113" num="10.9"><w n="113.1">Qu</w>’<w n="113.2">ils</w> <w n="113.3">sortiront</w> <w n="113.4">ravis</w> <w n="113.5">de</w> <w n="113.6">ta</w> <w n="113.7">céleste</w> <w n="113.8">table</w>,</l>
						<l n="114" num="10.10"><w n="114.1">Fais</w> <w n="114.2">qu</w>’<w n="114.3">ils</w> <w n="114.4">prennent</w> <w n="114.5">souci</w> <w n="114.6">d</w>’<w n="114.7">aider</w> <w n="114.8">un</w> <w n="114.9">misérable</w>,</l>
						<l n="115" num="10.11"><w n="115.1">Et</w> <w n="115.2">que</w> <w n="115.3">leurs</w> <w n="115.4">saints</w> <w n="115.5">transports</w>, <w n="115.6">avant</w> <w n="115.7">que</w> <w n="115.8">de</w> <w n="115.9">finir</w>,</l>
						<l n="116" num="10.12"><w n="116.1">D</w>’<w n="116.2">un</w> <w n="116.3">pécheur</w> <w n="116.4">comme</w> <w n="116.5">moi</w> <w n="116.6">daignent</w> <w n="116.7">se</w> <w n="116.8">souvenir</w>.</l>
					</lg>
				</div></body></text></TEI>