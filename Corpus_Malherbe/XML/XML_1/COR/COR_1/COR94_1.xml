<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR94">
					<head type="main">CHAPITRE XXV</head>
					<head type="sub">En quoi consiste la véritable paix.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">l</w>’<w n="1.3">ai</w> <w n="1.4">dit</w> <w n="1.5">autrefois</w> : « <w n="1.6">je</w> <w n="1.7">vous</w> <w n="1.8">laisse</w> <w n="1.9">ma</w> <w n="1.10">paix</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">vous</w> <w n="2.3">la</w> <w n="2.4">donne</w> <w n="2.5">à</w> <w n="2.6">tous</w>, <w n="2.7">et</w> <w n="2.8">les</w> <w n="2.9">dons</w> <w n="2.10">que</w> <w n="2.11">je</w> <w n="2.12">fais</w></l>
						<l n="3" num="1.3"><w n="3.1">N</w>’<w n="3.2">ont</w> <w n="3.3">rien</w> <w n="3.4">de</w> <w n="3.5">périssable</w>, <w n="3.6">ainsi</w> <w n="3.7">que</w> <w n="3.8">ceux</w> <w n="3.9">du</w> <w n="3.10">monde</w>. »</l>
						<l n="4" num="1.4"><w n="4.1">Tous</w> <w n="4.2">aiment</w> <w n="4.3">cette</w> <w n="4.4">paix</w>, <w n="4.5">tous</w> <w n="4.6">voudroient</w> <w n="4.7">la</w> <w n="4.8">trouver</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Mais</w> <w n="5.2">tous</w> <w n="5.3">ne</w> <w n="5.4">cherchent</w> <w n="5.5">pas</w> <w n="5.6">le</w> <w n="5.7">secret</w> <w n="5.8">où</w> <w n="5.9">se</w> <w n="5.10">fonde</w></l>
						<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">bien</w> <w n="6.3">de</w> <w n="6.4">l</w>’<w n="6.5">acquérir</w> <w n="6.6">et</w> <w n="6.7">de</w> <w n="6.8">la</w> <w n="6.9">conserver</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Ma</w> <w n="7.2">paix</w> <w n="7.3">est</w> <w n="7.4">avec</w> <w n="7.5">l</w>’<w n="7.6">humble</w>, <w n="7.7">avec</w> <w n="7.8">le</w> <w n="7.9">cœur</w> <w n="7.10">bénin</w> ;</l>
						<l n="8" num="2.2"><w n="8.1">Si</w> <w n="8.2">tu</w> <w n="8.3">veux</w> <w n="8.4">posséder</w> <w n="8.5">un</w> <w n="8.6">bonheur</w> <w n="8.7">si</w> <w n="8.8">divin</w>,</l>
						<l n="9" num="2.3"><w n="9.1">Joins</w> <w n="9.2">à</w> <w n="9.3">ces</w> <w n="9.4">deux</w> <w n="9.5">vertus</w> <w n="9.6">beaucoup</w> <w n="9.7">de</w> <w n="9.8">patience</w> ;</l>
						<l n="10" num="2.4"><w n="10.1">Mais</w> <w n="10.2">ce</w> <w n="10.3">n</w>’<w n="10.4">est</w> <w n="10.5">pas</w> <w n="10.6">encore</w> <w n="10.7">assez</w> <w n="10.8">pour</w> <w n="10.9">l</w>’<w n="10.10">obtenir</w> :</l>
						<l n="11" num="2.5"><w n="11.1">Prête</w>-<w n="11.2">moi</w> <w n="11.3">donc</w>, <w n="11.4">mon</w> <w n="11.5">fils</w>, <w n="11.6">un</w> <w n="11.7">moment</w> <w n="11.8">de</w> <w n="11.9">silence</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Et</w> <w n="12.2">je</w> <w n="12.3">t</w>’<w n="12.4">enseignerai</w> <w n="12.5">tout</w> <w n="12.6">l</w>’<w n="12.7">art</w> <w n="12.8">d</w>’<w n="12.9">y</w> <w n="12.10">parvenir</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Tiens</w> <w n="13.2">la</w> <w n="13.3">bride</w> <w n="13.4">sévère</w> <w n="13.5">à</w> <w n="13.6">tous</w> <w n="13.7">tes</w> <w n="13.8">appétits</w> ;</l>
						<l n="14" num="3.2"><w n="14.1">Prends</w> <w n="14.2">garde</w> <w n="14.3">exactement</w> <w n="14.4">à</w> <w n="14.5">tout</w> <w n="14.6">ce</w> <w n="14.7">que</w> <w n="14.8">tu</w> <w n="14.9">dis</w> ;</l>
						<l n="15" num="3.3"><w n="15.1">N</w>’<w n="15.2">examine</w> <w n="15.3">pas</w> <w n="15.4">moins</w> <w n="15.5">tout</w> <w n="15.6">ce</w> <w n="15.7">que</w> <w n="15.8">tu</w> <w n="15.9">veux</w> <w n="15.10">faire</w> ;</l>
						<l n="16" num="3.4"><w n="16.1">Et</w> <w n="16.2">donne</w> <w n="16.3">à</w> <w n="16.4">tes</w> <w n="16.5">desirs</w> <w n="16.6">pour</w> <w n="16.7">immuable</w> <w n="16.8">loi</w></l>
						<l n="17" num="3.5"><w n="17.1">Que</w> <w n="17.2">leur</w> <w n="17.3">unique</w> <w n="17.4">objet</w> <w n="17.5">soit</w> <w n="17.6">le</w> <w n="17.7">bien</w> <w n="17.8">de</w> <w n="17.9">me</w> <w n="17.10">plaire</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Et</w> <w n="18.2">leur</w> <w n="18.3">unique</w> <w n="18.4">but</w> <w n="18.5">de</w> <w n="18.6">ne</w> <w n="18.7">chercher</w> <w n="18.8">que</w> <w n="18.9">moi</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Ne</w> <w n="19.2">t</w>’<w n="19.3">embarrasse</w> <w n="19.4">point</w> <w n="19.5">des</w> <w n="19.6">actions</w> <w n="19.7">d</w>’<w n="19.8">autrui</w> :</l>
						<l n="20" num="4.2"><w n="20.1">Laisse</w> <w n="20.2">là</w> <w n="20.3">ce</w> <w n="20.4">qu</w>’<w n="20.5">il</w> <w n="20.6">dit</w> <w n="20.7">et</w> <w n="20.8">ce</w> <w n="20.9">qu</w>’<w n="20.10">on</w> <w n="20.11">dit</w> <w n="20.12">de</w> <w n="20.13">lui</w>,</l>
						<l n="21" num="4.3"><w n="21.1">À</w> <w n="21.2">moins</w> <w n="21.3">qu</w>’<w n="21.4">à</w> <w n="21.5">tes</w> <w n="21.6">soucis</w> <w n="21.7">sa</w> <w n="21.8">garde</w> <w n="21.9">soit</w> <w n="21.10">commise</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">Chasse</w> <w n="22.2">enfin</w> <w n="22.3">tout</w> <w n="22.4">frivole</w> <w n="22.5">et</w> <w n="22.6">vain</w> <w n="22.7">empressement</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Et</w> <w n="23.2">le</w> <w n="23.3">trouble</w> <w n="23.4">en</w> <w n="23.5">ton</w> <w n="23.6">cœur</w> <w n="23.7">trouvera</w> <w n="23.8">peu</w> <w n="23.9">de</w> <w n="23.10">prise</w>,</l>
						<l n="24" num="4.6"><w n="24.1">Ou</w> <w n="24.2">s</w>’<w n="24.3">il</w> <w n="24.4">l</w>’<w n="24.5">agite</w> <w n="24.6">encor</w>, <w n="24.7">ce</w> <w n="24.8">sera</w> <w n="24.9">rarement</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Mais</w> <w n="25.2">ne</w> <w n="25.3">t</w>’<w n="25.4">y</w> <w n="25.5">trompe</w> <w n="25.6">pas</w>, <w n="25.7">vivre</w> <w n="25.8">exempt</w> <w n="25.9">de</w> <w n="25.10">malheur</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Le</w> <w n="26.2">cœur</w> <w n="26.3">libre</w> <w n="26.4">d</w>’<w n="26.5">ennuis</w>, <w n="26.6">et</w> <w n="26.7">le</w> <w n="26.8">corps</w> <w n="26.9">de</w> <w n="26.10">douleur</w>,</l>
						<l n="27" num="5.3"><w n="27.1">N</w>’<w n="27.2">être</w> <w n="27.3">jamais</w> <w n="27.4">troublé</w> <w n="27.5">d</w>’<w n="27.6">aucune</w> <w n="27.7">inquiétude</w>,</l>
						<l n="28" num="5.4"><w n="28.1">Ce</w> <w n="28.2">n</w>’<w n="28.3">est</w> <w n="28.4">point</w> <w n="28.5">un</w> <w n="28.6">vrai</w> <w n="28.7">calme</w> <w n="28.8">en</w> <w n="28.9">ces</w> <w n="28.10">terrestres</w> <w n="28.11">lieux</w> ;</l>
						<l n="29" num="5.5"><w n="29.1">Et</w> <w n="29.2">ce</w> <w n="29.3">don</w> <w n="29.4">n</w>’<w n="29.5">appartient</w> <w n="29.6">qu</w>’<w n="29.7">à</w> <w n="29.8">la</w> <w n="29.9">béatitude</w></l>
						<l n="30" num="5.6"><w n="30.1">Que</w> <w n="30.2">pour</w> <w n="30.3">l</w>’<w n="30.4">éternité</w> <w n="30.5">je</w> <w n="30.6">te</w> <w n="30.7">réserve</w> <w n="30.8">aux</w> <w n="30.9">cieux</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Ainsi</w> <w n="31.2">quand</w> <w n="31.3">tu</w> <w n="31.4">te</w> <w n="31.5">vois</w> <w n="31.6">sans</w> <w n="31.7">aucuns</w> <w n="31.8">déplaisirs</w>,</l>
						<l n="32" num="6.2"><w n="32.1">Que</w> <w n="32.2">tout</w> <w n="32.3">de</w> <w n="32.4">tous</w> <w n="32.5">côtés</w> <w n="32.6">répond</w> <w n="32.7">à</w> <w n="32.8">tes</w> <w n="32.9">desirs</w>,</l>
						<l n="33" num="6.3"><w n="33.1">Qu</w>’<w n="33.2">il</w> <w n="33.3">ne</w> <w n="33.4">t</w>’<w n="33.5">arrive</w> <w n="33.6">rien</w> <w n="33.7">d</w>’<w n="33.8">amer</w> <w n="33.9">ni</w> <w n="33.10">de</w> <w n="33.11">contraire</w>,</l>
						<l n="34" num="6.4"><w n="34.1">N</w>’<w n="34.2">estime</w> <w n="34.3">pas</w> <w n="34.4">encore</w> <w n="34.5">avoir</w> <w n="34.6">trouvé</w> <w n="34.7">la</w> <w n="34.8">paix</w>,</l>
						<l n="35" num="6.5"><w n="35.1">Ni</w> <w n="35.2">que</w> <w n="35.3">tout</w> <w n="35.4">soit</w> <w n="35.5">en</w> <w n="35.6">toi</w> <w n="35.7">si</w> <w n="35.8">bon</w>, <w n="35.9">si</w> <w n="35.10">salutaire</w>,</l>
						<l n="36" num="6.6"><w n="36.1">Qu</w>’<w n="36.2">on</w> <w n="36.3">ait</w> <w n="36.4">lieu</w> <w n="36.5">de</w> <w n="36.6">te</w> <w n="36.7">mettre</w> <w n="36.8">au</w> <w n="36.9">nombre</w> <w n="36.10">des</w> <w n="36.11">parfaits</w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Ne</w> <w n="37.2">te</w> <w n="37.3">crois</w> <w n="37.4">pas</w> <w n="37.5">non</w> <w n="37.6">plus</w> <w n="37.7">ni</w> <w n="37.8">grand</w> <w n="37.9">ni</w> <w n="37.10">bien</w> <w n="37.11">aimé</w>,</l>
						<l n="38" num="7.2"><w n="38.1">Pour</w> <w n="38.2">te</w> <w n="38.3">sentir</w> <w n="38.4">un</w> <w n="38.5">zèle</w> <w n="38.6">à</w> <w n="38.7">ce</w> <w n="38.8">point</w> <w n="38.9">enflammé</w>,</l>
						<l n="39" num="7.3"><w n="39.1">Qu</w>’<w n="39.2">à</w> <w n="39.3">force</w> <w n="39.4">de</w> <w n="39.5">tendresse</w> <w n="39.6">il</w> <w n="39.7">te</w> <w n="39.8">baigne</w> <w n="39.9">de</w> <w n="39.10">larmes</w> :</l>
						<l n="40" num="7.4"><w n="40.1">Des</w> <w n="40.2">solides</w> <w n="40.3">vertus</w> <w n="40.4">la</w> <w n="40.5">vraie</w> <w n="40.6">affection</w></l>
						<l n="41" num="7.5"><w n="41.1">Ne</w> <w n="41.2">fait</w> <w n="41.3">point</w> <w n="41.4">consister</w> <w n="41.5">en</w> <w n="41.6">tous</w> <w n="41.7">ces</w> <w n="41.8">petits</w> <w n="41.9">charmes</w></l>
						<l n="42" num="7.6"><w n="42.1">Ni</w> <w n="42.2">ton</w> <w n="42.3">avancement</w> <w n="42.4">ni</w> <w n="42.5">ta</w> <w n="42.6">perfection</w>.</l>
					</lg>
					<lg n="8">
						<l n="43" num="8.1">« <w n="43.1">En</w> <w n="43.2">quoi</w> <w n="43.3">donc</w>, <w n="43.4">me</w> <w n="43.5">dis</w>-<w n="43.6">tu</w>, <w n="43.7">consiste</w> <w n="43.8">pleinement</w></l>
						<l n="44" num="8.2"><w n="44.1">Cette</w> <w n="44.2">perfection</w> <w n="44.3">et</w> <w n="44.4">cet</w> <w n="44.5">avancement</w> ?</l>
						<l n="45" num="8.3"><w n="45.1">Cette</w> <w n="45.2">paix</w> <w n="45.3">véritable</w>, <w n="45.4">où</w> <w n="45.5">se</w> <w n="45.6">rencontre</w>-<w n="45.7">t</w>-<w n="45.8">elle</w> ? »</l>
						<l n="46" num="8.4"><w n="46.1">Je</w> <w n="46.2">veux</w> <w n="46.3">bien</w> <w n="46.4">te</w> <w n="46.5">l</w>’<w n="46.6">apprendre</w> : <w n="46.7">elle</w> <w n="46.8">est</w>, <w n="46.9">en</w> <w n="46.10">premier</w> <w n="46.11">lieu</w>,</l>
						<l n="47" num="8.5"><w n="47.1">À</w> <w n="47.2">t</w>’<w n="47.3">offrir</w> <w n="47.4">tout</w> <w n="47.5">entier</w> <w n="47.6">d</w>’<w n="47.7">un</w> <w n="47.8">cœur</w> <w n="47.9">vraiment</w> <w n="47.10">fidèle</w></l>
						<l n="48" num="8.6"><w n="48.1">Aux</w> <w n="48.2">ordres</w> <w n="48.3">souverains</w> <w n="48.4">du</w> <w n="48.5">vouloir</w> <w n="48.6">de</w> <w n="48.7">ton</w> <w n="48.8">dieu</w>.</l>
					</lg>
					<lg n="9">
						<l n="49" num="9.1"><w n="49.1">Cette</w> <w n="49.2">soumission</w> <w n="49.3">à</w> <w n="49.4">mes</w> <w n="49.5">sacrés</w> <w n="49.6">décrets</w></l>
						<l n="50" num="9.2"><w n="50.1">Te</w> <w n="50.2">doit</w> <w n="50.3">fermer</w> <w n="50.4">les</w> <w n="50.5">yeux</w> <w n="50.6">pour</w> <w n="50.7">tous</w> <w n="50.8">tes</w> <w n="50.9">intérêts</w>,</l>
						<l n="51" num="9.3"><w n="51.1">Soit</w> <w n="51.2">qu</w>’<w n="51.3">ils</w> <w n="51.4">soient</w> <w n="51.5">de</w> <w n="51.6">petite</w> <w n="51.7">ou</w> <w n="51.8">de</w> <w n="51.9">grande</w> <w n="51.10">importance</w> :</l>
						<l n="52" num="9.4"><w n="52.1">N</w>’<w n="52.2">en</w> <w n="52.3">cherche</w> <w n="52.4">dans</w> <w n="52.5">le</w> <w n="52.6">temps</w> <w n="52.7">ni</w> <w n="52.8">dans</w> <w n="52.9">l</w>’<w n="52.10">éternité</w>,</l>
						<l n="53" num="9.5"><w n="53.1">Et</w> <w n="53.2">souhaite</w> <w n="53.3">le</w> <w n="53.4">ciel</w>, <w n="53.5">moins</w> <w n="53.6">pour</w> <w n="53.7">ta</w> <w n="53.8">récompense</w>,</l>
						<l n="54" num="9.6"><w n="54.1">Que</w> <w n="54.2">pour</w> <w n="54.3">y</w> <w n="54.4">voir</w> <w n="54.5">mon</w> <w n="54.6">nom</w> <w n="54.7">à</w> <w n="54.8">jamais</w> <w n="54.9">exalté</w>.</l>
					</lg>
					<lg n="10">
						<l n="55" num="10.1"><w n="55.1">Montre</w> <w n="55.2">un</w> <w n="55.3">visage</w> <w n="55.4">égal</w> <w n="55.5">aux</w> <w n="55.6">changements</w> <w n="55.7">divers</w> :</l>
						<l n="56" num="10.2"><w n="56.1">Dans</w> <w n="56.2">le</w> <w n="56.3">plus</w> <w n="56.4">doux</w> <w n="56.5">bonheur</w>, <w n="56.6">dans</w> <w n="56.7">le</w> <w n="56.8">plus</w> <w n="56.9">dur</w> <w n="56.10">revers</w>,</l>
						<l n="57" num="10.3"><w n="57.1">Rends</w>-<w n="57.2">moi</w>, <w n="57.3">sans</w> <w n="57.4">t</w>’<w n="57.5">émouvoir</w>, <w n="57.6">même</w> <w n="57.7">action</w> <w n="57.8">de</w> <w n="57.9">grâces</w> ;</l>
						<l n="58" num="10.4"><w n="58.1">Tiens</w> <w n="58.2">la</w> <w n="58.3">balance</w> <w n="58.4">droite</w> <w n="58.5">à</w> <w n="58.6">chaque</w> <w n="58.7">événement</w>,</l>
						<l n="59" num="10.5"><w n="59.1">Tiens</w>-<w n="59.2">la</w> <w n="59.3">ferme</w> <w n="59.4">à</w> <w n="59.5">tel</w> <w n="59.6">point</w>, <w n="59.7">que</w> <w n="59.8">jamais</w> <w n="59.9">tu</w> <w n="59.10">ne</w> <w n="59.11">passes</w></l>
						<l n="60" num="10.6"><w n="60.1">Jusque</w> <w n="60.2">dans</w> <w n="60.3">la</w> <w n="60.4">foiblesse</w> <w n="60.5">ou</w> <w n="60.6">dans</w> <w n="60.7">l</w>’<w n="60.8">emportement</w>.</l>
					</lg>
					<lg n="11">
						<l n="61" num="11.1"><w n="61.1">Si</w> <w n="61.2">tu</w> <w n="61.3">sens</w> <w n="61.4">qu</w>’<w n="61.5">au</w> <w n="61.6">milieu</w> <w n="61.7">des</w> <w n="61.8">tribulations</w></l>
						<l n="62" num="11.2"><w n="62.1">Je</w> <w n="62.2">retire</w> <w n="62.3">de</w> <w n="62.4">toi</w> <w n="62.5">mes</w> <w n="62.6">consolations</w>,</l>
						<l n="63" num="11.3"><w n="63.1">Et</w> <w n="63.2">te</w> <w n="63.3">laisse</w> <w n="63.4">accablé</w> <w n="63.5">sous</w> <w n="63.6">ce</w> <w n="63.7">qui</w> <w n="63.8">te</w> <w n="63.9">ravage</w>,</l>
						<l n="64" num="11.4"><w n="64.1">Forme</w> <w n="64.2">des</w> <w n="64.3">sentiments</w> <w n="64.4">d</w>’<w n="64.5">autant</w> <w n="64.6">plus</w> <w n="64.7">résolus</w>,</l>
						<l n="65" num="11.5"><w n="65.1">Et</w> <w n="65.2">soutiens</w> <w n="65.3">ton</w> <w n="65.4">espoir</w> <w n="65.5">avec</w> <w n="65.6">tant</w> <w n="65.7">de</w> <w n="65.8">courage</w>,</l>
						<l n="66" num="11.6"><w n="66.1">Qu</w>’<w n="66.2">il</w> <w n="66.3">prépare</w> <w n="66.4">ton</w> <w n="66.5">cœur</w> <w n="66.6">à</w> <w n="66.7">souffrir</w> <w n="66.8">encor</w> <w n="66.9">plus</w>.</l>
					</lg>
					<lg n="12">
						<l n="67" num="12.1"><w n="67.1">Ne</w> <w n="67.2">te</w> <w n="67.3">retranche</w> <w n="67.4">point</w> <w n="67.5">sur</w> <w n="67.6">ton</w> <w n="67.7">intégrité</w>,</l>
						<l n="68" num="12.2"><w n="68.1">Comme</w> <w n="68.2">si</w> <w n="68.3">tu</w> <w n="68.4">souffrois</w> <w n="68.5">sans</w> <w n="68.6">l</w>’<w n="68.7">avoir</w> <w n="68.8">mérité</w>,</l>
						<l n="69" num="12.3"><w n="69.1">Et</w> <w n="69.2">que</w> <w n="69.3">pour</w> <w n="69.4">tes</w> <w n="69.5">vertus</w> <w n="69.6">ce</w> <w n="69.7">fût</w> <w n="69.8">un</w> <w n="69.9">exercice</w> :</l>
						<l n="70" num="12.4"><w n="70.1">Fuis</w> <w n="70.2">cette</w> <w n="70.3">vaine</w> <w n="70.4">idée</w>, <w n="70.5">et</w> <w n="70.6">comme</w> <w n="70.7">criminel</w>,</l>
						<l n="71" num="12.5"><w n="71.1">En</w> <w n="71.2">toutes</w> <w n="71.3">mes</w> <w n="71.4">rigueurs</w> <w n="71.5">adore</w> <w n="71.6">ma</w> <w n="71.7">justice</w>,</l>
						<l n="72" num="12.6"><w n="72.1">Et</w> <w n="72.2">bénis</w> <w n="72.3">mon</w> <w n="72.4">courroux</w> <w n="72.5">et</w> <w n="72.6">saint</w> <w n="72.7">et</w> <w n="72.8">paternel</w>.</l>
					</lg>
					<lg n="13">
						<l n="73" num="13.1"><w n="73.1">C</w>’<w n="73.2">est</w> <w n="73.3">comme</w> <w n="73.4">il</w> <w n="73.5">te</w> <w n="73.6">faut</w> <w n="73.7">mettre</w> <w n="73.8">au</w> <w n="73.9">droit</w> <w n="73.10">et</w> <w n="73.11">vrai</w> <w n="73.12">chemin</w>,</l>
						<l n="74" num="13.2"><w n="74.1">Qui</w> <w n="74.2">seul</w> <w n="74.3">te</w> <w n="74.4">peut</w> <w n="74.5">conduire</w> <w n="74.6">à</w> <w n="74.7">cette</w> <w n="74.8">paix</w> <w n="74.9">sans</w> <w n="74.10">fin</w></l>
						<l n="75" num="13.3"><w n="75.1">Qu</w>’<w n="75.2">à</w> <w n="75.3">mes</w> <w n="75.4">plus</w> <w n="75.5">chers</w> <w n="75.6">amis</w> <w n="75.7">moi</w>-<w n="75.8">même</w> <w n="75.9">j</w>’<w n="75.10">ai</w> <w n="75.11">laissée</w> :</l>
						<l n="76" num="13.4"><w n="76.1">Suis</w>-<w n="76.2">le</w> <w n="76.3">sur</w> <w n="76.4">ma</w> <w n="76.5">parole</w>, <w n="76.6">et</w> <w n="76.7">crois</w> <w n="76.8">sans</w> <w n="76.9">t</w>’<w n="76.10">ébranler</w></l>
						<l n="77" num="13.5"><w n="77.1">Qu</w>’<w n="77.2">après</w> <w n="77.3">ta</w> <w n="77.4">patience</w> <w n="77.5">à</w> <w n="77.6">mon</w> <w n="77.7">choix</w> <w n="77.8">exercée</w>,</l>
						<l n="78" num="13.6"><w n="78.1">Mes</w> <w n="78.2">clartés</w> <w n="78.3">de</w> <w n="78.4">nouveau</w> <w n="78.5">te</w> <w n="78.6">viendront</w> <w n="78.7">consoler</w>.</l>
					</lg>
					<lg n="14">
						<l n="79" num="14.1"><w n="79.1">Que</w> <w n="79.2">si</w> <w n="79.3">jamais</w> <w n="79.4">l</w>’<w n="79.5">effort</w> <w n="79.6">d</w>’<w n="79.7">un</w> <w n="79.8">zèle</w> <w n="79.9">tout</w> <w n="79.10">de</w> <w n="79.11">foi</w></l>
						<l n="80" num="14.2"><w n="80.1">Par</w> <w n="80.2">un</w> <w n="80.3">parfait</w> <w n="80.4">mépris</w> <w n="80.5">te</w> <w n="80.6">détache</w> <w n="80.7">de</w> <w n="80.8">toi</w></l>
						<l n="81" num="14.3"><w n="81.1">Pour</w> <w n="81.2">ne</w> <w n="81.3">plus</w> <w n="81.4">respirer</w> <w n="81.5">que</w> <w n="81.6">sous</w> <w n="81.7">ma</w> <w n="81.8">providence</w>,</l>
						<l n="82" num="14.4"><w n="82.1">Sache</w> <w n="82.2">qu</w>’<w n="82.3">alors</w> <w n="82.4">tes</w> <w n="82.5">sens</w>, <w n="82.6">à</w> <w n="82.7">moi</w> <w n="82.8">seul</w> <w n="82.9">asservis</w>,</l>
						<l n="83" num="14.5"><w n="83.1">Posséderont</w> <w n="83.2">la</w> <w n="83.3">paix</w> <w n="83.4">dans</w> <w n="83.5">sa</w> <w n="83.6">pleine</w> <w n="83.7">abondance</w>,</l>
						<l n="84" num="14.6"><w n="84.1">Autant</w> <w n="84.2">qu</w>’<w n="84.3">en</w> <w n="84.4">peut</w> <w n="84.5">souffrir</w> <w n="84.6">cet</w> <w n="84.7">exil</w> <w n="84.8">où</w> <w n="84.9">tu</w> <w n="84.10">vis</w>.</l>
					</lg>
				</div></body></text></TEI>