<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE PREMIER</head><div type="poem" key="COR34">
					<head type="main">CHAPITRE II</head>
					<head type="sub">Du feu d’estime de soi-même.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">desir</w> <w n="1.3">de</w> <w n="1.4">savoir</w> <w n="1.5">est</w> <w n="1.6">naturel</w> <w n="1.7">aux</w> <w n="1.8">hommes</w> :</l>
						<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">naît</w> <w n="2.3">dans</w> <w n="2.4">leur</w> <w n="2.5">berceau</w> <w n="2.6">sans</w> <w n="2.7">mourir</w> <w n="2.8">qu</w>’<w n="2.9">avec</w> <w n="2.10">eux</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Mais</w>, <w n="3.2">ô</w> <w n="3.3">Dieu</w>, <w n="3.4">dont</w> <w n="3.5">la</w> <w n="3.6">main</w> <w n="3.7">nous</w> <w n="3.8">fait</w> <w n="3.9">ce</w> <w n="3.10">que</w> <w n="3.11">nous</w> <w n="3.12">sommes</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">peut</w>-<w n="4.3">il</w> <w n="4.4">sans</w> <w n="4.5">ta</w> <w n="4.6">crainte</w> <w n="4.7">avoir</w> <w n="4.8">de</w> <w n="4.9">fructueux</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">paysan</w> <w n="5.3">stupide</w> <w n="5.4">et</w> <w n="5.5">sans</w> <w n="5.6">expérience</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">ne</w> <w n="6.3">sait</w> <w n="6.4">que</w> <w n="6.5">t</w>’<w n="6.6">aimer</w> <w n="6.7">et</w> <w n="6.8">n</w>’<w n="6.9">a</w> <w n="6.10">que</w> <w n="6.11">de</w> <w n="6.12">la</w> <w n="6.13">foi</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Vaut</w> <w n="7.2">mieux</w> <w n="7.3">qu</w>’<w n="7.4">un</w> <w n="7.5">philosophe</w> <w n="7.6">enflé</w> <w n="7.7">de</w> <w n="7.8">sa</w> <w n="7.9">science</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">pénètre</w> <w n="8.3">les</w> <w n="8.4">cieux</w>, <w n="8.5">sans</w> <w n="8.6">réfléchir</w> <w n="8.7">sur</w> <w n="8.8">soi</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Qui</w> <w n="9.2">se</w> <w n="9.3">connoît</w> <w n="9.4">soi</w>-<w n="9.5">même</w> <w n="9.6">en</w> <w n="9.7">a</w> <w n="9.8">l</w>’<w n="9.9">âme</w> <w n="9.10">peu</w> <w n="9.11">vaine</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Sa</w> <w n="10.2">propre</w> <w n="10.3">connoissance</w> <w n="10.4">en</w> <w n="10.5">met</w> <w n="10.6">bien</w> <w n="10.7">bas</w> <w n="10.8">le</w> <w n="10.9">prix</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">tout</w> <w n="11.3">le</w> <w n="11.4">faux</w> <w n="11.5">éclat</w> <w n="11.6">de</w> <w n="11.7">la</w> <w n="11.8">louange</w> <w n="11.9">humaine</w></l>
						<l n="12" num="3.4"><w n="12.1">N</w>’<w n="12.2">est</w> <w n="12.3">pour</w> <w n="12.4">lui</w> <w n="12.5">que</w> <w n="12.6">l</w>’<w n="12.7">objet</w> <w n="12.8">d</w>’<w n="12.9">un</w> <w n="12.10">généreux</w> <w n="12.11">mépris</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Au</w> <w n="13.2">grand</w> <w n="13.3">jour</w> <w n="13.4">du</w> <w n="13.5">seigneur</w> <w n="13.6">sera</w>-<w n="13.7">ce</w> <w n="13.8">un</w> <w n="13.9">grand</w> <w n="13.10">refuge</w></l>
						<l n="14" num="4.2"><w n="14.1">D</w>’<w n="14.2">avoir</w> <w n="14.3">connu</w> <w n="14.4">de</w> <w n="14.5">tout</w> <w n="14.6">et</w> <w n="14.7">la</w> <w n="14.8">cause</w> <w n="14.9">et</w> <w n="14.10">l</w>’<w n="14.11">effet</w> ?</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">ce</w> <w n="15.3">qu</w>’<w n="15.4">on</w> <w n="15.5">aura</w> <w n="15.6">su</w> <w n="15.7">fléchira</w>-<w n="15.8">t</w>-<w n="15.9">il</w> <w n="15.10">un</w> <w n="15.11">juge</w></l>
						<l n="16" num="4.4"><w n="16.1">Qui</w> <w n="16.2">ne</w> <w n="16.3">regardera</w> <w n="16.4">que</w> <w n="16.5">ce</w> <w n="16.6">qu</w>’<w n="16.7">on</w> <w n="16.8">aura</w> <w n="16.9">fait</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Borne</w> <w n="17.2">tous</w> <w n="17.3">tes</w> <w n="17.4">desirs</w> <w n="17.5">à</w> <w n="17.6">ce</w> <w n="17.7">qu</w>’<w n="17.8">il</w> <w n="17.9">te</w> <w n="17.10">faut</w> <w n="17.11">faire</w> ;</l>
						<l n="18" num="5.2"><w n="18.1">Ne</w> <w n="18.2">les</w> <w n="18.3">porte</w> <w n="18.4">plus</w> <w n="18.5">trop</w> <w n="18.6">vers</w> <w n="18.7">l</w>’<w n="18.8">amas</w> <w n="18.9">du</w> <w n="18.10">savoir</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Les</w> <w n="19.2">soins</w> <w n="19.3">de</w> <w n="19.4">l</w>’<w n="19.5">acquérir</w> <w n="19.6">ne</w> <w n="19.7">font</w> <w n="19.8">que</w> <w n="19.9">te</w> <w n="19.10">distraire</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">quand</w> <w n="20.3">tu</w> <w n="20.4">l</w>’<w n="20.5">as</w> <w n="20.6">acquis</w>, <w n="20.7">il</w> <w n="20.8">peut</w> <w n="20.9">te</w> <w n="20.10">décevoir</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Les</w> <w n="21.2">savants</w> <w n="21.3">d</w>’<w n="21.4">ordinaire</w> <w n="21.5">aiment</w> <w n="21.6">qu</w>’<w n="21.7">on</w> <w n="21.8">les</w> <w n="21.9">regarde</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Qu</w>’<w n="22.2">on</w> <w n="22.3">murmure</w> <w n="22.4">autour</w> <w n="22.5">d</w>’<w n="22.6">eux</w> : « <w n="22.7">voilà</w> <w n="22.8">ces</w> <w n="22.9">grands</w> <w n="22.10">esprits</w> ! »</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">s</w>’<w n="23.3">ils</w> <w n="23.4">ne</w> <w n="23.5">font</w> <w n="23.6">du</w> <w n="23.7">cœur</w> <w n="23.8">une</w> <w n="23.9">soigneuse</w> <w n="23.10">garde</w>,</l>
						<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">cet</w> <w n="24.3">orgueil</w> <w n="24.4">secret</w> <w n="24.5">ils</w> <w n="24.6">sont</w> <w n="24.7">toujours</w> <w n="24.8">surpris</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Qu</w>’<w n="25.2">on</w> <w n="25.3">ne</w> <w n="25.4">s</w>’<w n="25.5">y</w> <w n="25.6">trompe</w> <w n="25.7">point</w> : <w n="25.8">s</w>’<w n="25.9">il</w> <w n="25.10">est</w> <w n="25.11">quelques</w> <w n="25.12">sciences</w></l>
						<l n="26" num="7.2"><w n="26.1">Qui</w> <w n="26.2">puissent</w> <w n="26.3">d</w>’<w n="26.4">un</w> <w n="26.5">savant</w> <w n="26.6">faire</w> <w n="26.7">un</w> <w n="26.8">homme</w> <w n="26.9">de</w> <w n="26.10">bien</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Il</w> <w n="27.2">en</w> <w n="27.3">est</w> <w n="27.4">beaucoup</w> <w n="27.5">plus</w> <w n="27.6">de</w> <w n="27.7">qui</w> <w n="27.8">les</w> <w n="27.9">connoissances</w></l>
						<l n="28" num="7.4"><w n="28.1">Ne</w> <w n="28.2">servent</w> <w n="28.3">guère</w> <w n="28.4">à</w> <w n="28.5">l</w>’<w n="28.6">âme</w>, <w n="28.7">ou</w> <w n="28.8">ne</w> <w n="28.9">servent</w> <w n="28.10">de</w> <w n="28.11">rien</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Par</w> <w n="29.2">là</w> <w n="29.3">tu</w> <w n="29.4">peux</w> <w n="29.5">juger</w> <w n="29.6">à</w> <w n="29.7">quels</w> <w n="29.8">périls</w> <w n="29.9">s</w>’<w n="29.10">expose</w></l>
						<l n="30" num="8.2"><w n="30.1">Celui</w> <w n="30.2">qui</w> <w n="30.3">du</w> <w n="30.4">savoir</w> <w n="30.5">fait</w> <w n="30.6">son</w> <w n="30.7">unique</w> <w n="30.8">but</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">combien</w> <w n="31.3">se</w> <w n="31.4">méprend</w> <w n="31.5">qui</w> <w n="31.6">songe</w> <w n="31.7">à</w> <w n="31.8">quelque</w> <w n="31.9">chose</w></l>
						<l n="32" num="8.4"><w n="32.1">Qu</w>’<w n="32.2">à</w> <w n="32.3">ce</w> <w n="32.4">qui</w> <w n="32.5">peut</w> <w n="32.6">conduire</w> <w n="32.7">au</w> <w n="32.8">chemin</w> <w n="32.9">du</w> <w n="32.10">salut</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Le</w> <w n="33.2">plus</w> <w n="33.3">profond</w> <w n="33.4">savoir</w> <w n="33.5">n</w>’<w n="33.6">assouvit</w> <w n="33.7">point</w> <w n="33.8">une</w> <w n="33.9">âme</w> ;</l>
						<l n="34" num="9.2"><w n="34.1">Mais</w> <w n="34.2">une</w> <w n="34.3">bonne</w> <w n="34.4">vie</w> <w n="34.5">a</w> <w n="34.6">de</w> <w n="34.7">quoi</w> <w n="34.8">la</w> <w n="34.9">calmer</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">jette</w> <w n="35.3">dans</w> <w n="35.4">le</w> <w n="35.5">cœur</w> <w n="35.6">qu</w>’<w n="35.7">un</w> <w n="35.8">saint</w> <w n="35.9">desir</w> <w n="35.10">enflamme</w></l>
						<l n="36" num="9.4"><w n="36.1">La</w> <w n="36.2">pleine</w> <w n="36.3">confiance</w> <w n="36.4">au</w> <w n="36.5">Dieu</w> <w n="36.6">qu</w>’<w n="36.7">il</w> <w n="36.8">doit</w> <w n="36.9">aimer</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Au</w> <w n="37.2">reste</w>, <w n="37.3">plus</w> <w n="37.4">tu</w> <w n="37.5">sais</w>, <w n="37.6">et</w> <w n="37.7">plus</w> <w n="37.8">a</w> <w n="37.9">de</w> <w n="37.10">lumière</w></l>
						<l n="38" num="10.2"><w n="38.1">Le</w> <w n="38.2">jour</w> <w n="38.3">qui</w> <w n="38.4">se</w> <w n="38.5">répand</w> <w n="38.6">sur</w> <w n="38.7">ton</w> <w n="38.8">entendement</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Plus</w> <w n="39.2">tu</w> <w n="39.3">seras</w> <w n="39.4">coupable</w> <w n="39.5">à</w> <w n="39.6">ton</w> <w n="39.7">heure</w> <w n="39.8">dernière</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Si</w> <w n="40.2">tu</w> <w n="40.3">n</w>’<w n="40.4">en</w> <w n="40.5">as</w> <w n="40.6">vécu</w> <w n="40.7">d</w>’<w n="40.8">autant</w> <w n="40.9">plus</w> <w n="40.10">saintement</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">La</w> <w n="41.2">vanité</w> <w n="41.3">par</w> <w n="41.4">là</w> <w n="41.5">ne</w> <w n="41.6">te</w> <w n="41.7">doit</w> <w n="41.8">point</w> <w n="41.9">surprendre</w> :</l>
						<l n="42" num="11.2"><w n="42.1">Le</w> <w n="42.2">savoir</w> <w n="42.3">t</w>’<w n="42.4">est</w> <w n="42.5">donné</w> <w n="42.6">pour</w> <w n="42.7">guide</w> <w n="42.8">à</w> <w n="42.9">moins</w> <w n="42.10">faillir</w> ;</l>
						<l n="43" num="11.3"><w n="43.1">Il</w> <w n="43.2">te</w> <w n="43.3">donne</w> <w n="43.4">lui</w>-<w n="43.5">même</w> <w n="43.6">un</w> <w n="43.7">plus</w> <w n="43.8">grand</w> <w n="43.9">compte</w> <w n="43.10">à</w> <w n="43.11">rendre</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">plus</w> <w n="44.3">lieu</w> <w n="44.4">de</w> <w n="44.5">trembler</w> <w n="44.6">que</w> <w n="44.7">de</w> <w n="44.8">t</w>’<w n="44.9">enorgueillir</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Trouve</w> <w n="45.2">à</w> <w n="45.3">t</w>’<w n="45.4">humilier</w> <w n="45.5">même</w> <w n="45.6">dans</w> <w n="45.7">ta</w> <w n="45.8">doctrine</w> :</l>
						<l n="46" num="12.2"><w n="46.1">Quiconque</w> <w n="46.2">en</w> <w n="46.3">sait</w> <w n="46.4">beaucoup</w> <w n="46.5">en</w> <w n="46.6">ignore</w> <w n="46.7">encor</w> <w n="46.8">plus</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">qui</w> <w n="47.3">sans</w> <w n="47.4">se</w> <w n="47.5">flatter</w> <w n="47.6">en</w> <w n="47.7">secret</w> <w n="47.8">s</w>’<w n="47.9">examine</w></l>
						<l n="48" num="12.4"><w n="48.1">Est</w> <w n="48.2">de</w> <w n="48.3">son</w> <w n="48.4">ignorance</w> <w n="48.5">heureusement</w> <w n="48.6">confus</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Quand</w> <w n="49.2">pour</w> <w n="49.3">quelques</w> <w n="49.4">clartés</w> <w n="49.5">dont</w> <w n="49.6">ton</w> <w n="49.7">esprit</w> <w n="49.8">abonde</w></l>
						<l n="50" num="13.2"><w n="50.1">Ton</w> <w n="50.2">orgueil</w> <w n="50.3">à</w> <w n="50.4">quelque</w> <w n="50.5">autre</w> <w n="50.6">ose</w> <w n="50.7">te</w> <w n="50.8">préférer</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Vois</w> <w n="51.2">qu</w>’<w n="51.3">il</w> <w n="51.4">en</w> <w n="51.5">est</w> <w n="51.6">encor</w> <w n="51.7">de</w> <w n="51.8">plus</w> <w n="51.9">savants</w> <w n="51.10">au</w> <w n="51.11">monde</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Qu</w>’<w n="52.2">il</w> <w n="52.3">en</w> <w n="52.4">est</w> <w n="52.5">que</w> <w n="52.6">le</w> <w n="52.7">ciel</w> <w n="52.8">daigne</w> <w n="52.9">mieux</w> <w n="52.10">éclairer</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Fuis</w> <w n="53.2">la</w> <w n="53.3">haute</w> <w n="53.4">science</w>, <w n="53.5">et</w> <w n="53.6">cours</w> <w n="53.7">après</w> <w n="53.8">la</w> <w n="53.9">bonne</w> :</l>
						<l n="54" num="14.2"><w n="54.1">Apprends</w> <w n="54.2">celle</w> <w n="54.3">de</w> <w n="54.4">vivre</w> <w n="54.5">ici</w>-<w n="54.6">bas</w> <w n="54.7">sans</w> <w n="54.8">éclat</w> ;</l>
						<l n="55" num="14.3"><w n="55.1">Aime</w> <w n="55.2">à</w> <w n="55.3">n</w>’<w n="55.4">être</w> <w n="55.5">connu</w>, <w n="55.6">s</w>’<w n="55.7">il</w> <w n="55.8">se</w> <w n="55.9">peut</w>, <w n="55.10">de</w> <w n="55.11">personne</w>,</l>
						<l n="56" num="14.4"><w n="56.1">Ou</w> <w n="56.2">du</w> <w n="56.3">moins</w> <w n="56.4">aime</w> <w n="56.5">à</w> <w n="56.6">voir</w> <w n="56.7">qu</w>’<w n="56.8">aucun</w> <w n="56.9">n</w>’<w n="56.10">en</w> <w n="56.11">fasse</w> <w n="56.12">état</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Cette</w> <w n="57.2">unique</w> <w n="57.3">leçon</w>, <w n="57.4">dont</w> <w n="57.5">le</w> <w n="57.6">parfait</w> <w n="57.7">usage</w></l>
						<l n="58" num="15.2"><w n="58.1">Consiste</w> <w n="58.2">à</w> <w n="58.3">se</w> <w n="58.4">bien</w> <w n="58.5">voir</w> <w n="58.6">et</w> <w n="58.7">n</w>’<w n="58.8">en</w> <w n="58.9">rien</w> <w n="58.10">présumer</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Est</w> <w n="59.2">la</w> <w n="59.3">plus</w> <w n="59.4">digne</w> <w n="59.5">étude</w> <w n="59.6">où</w> <w n="59.7">s</w>’<w n="59.8">occupe</w> <w n="59.9">le</w> <w n="59.10">sage</w></l>
						<l n="60" num="15.4"><w n="60.1">Pour</w> <w n="60.2">estimer</w> <w n="60.3">tout</w> <w n="60.4">autre</w>, <w n="60.5">et</w> <w n="60.6">se</w> <w n="60.7">mésestimer</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Si</w> <w n="61.2">tu</w> <w n="61.3">vois</w> <w n="61.4">donc</w> <w n="61.5">un</w> <w n="61.6">homme</w> <w n="61.7">abîmé</w> <w n="61.8">dans</w> <w n="61.9">l</w>’<w n="61.10">offense</w>,</l>
						<l n="62" num="16.2"><w n="62.1">Ne</w> <w n="62.2">te</w> <w n="62.3">tiens</w> <w n="62.4">pas</w> <w n="62.5">plus</w> <w n="62.6">juste</w> <w n="62.7">ou</w> <w n="62.8">moins</w> <w n="62.9">pécheur</w> <w n="62.10">que</w> <w n="62.11">lui</w> :</l>
						<l n="63" num="16.3"><w n="63.1">Tu</w> <w n="63.2">peux</w> <w n="63.3">en</w> <w n="63.4">un</w> <w n="63.5">moment</w> <w n="63.6">perdre</w> <w n="63.7">ton</w> <w n="63.8">innocence</w>,</l>
						<l n="64" num="16.4"><w n="64.1">Et</w> <w n="64.2">n</w>’<w n="64.3">être</w> <w n="64.4">pas</w> <w n="64.5">demain</w> <w n="64.6">le</w> <w n="64.7">même</w> <w n="64.8">qu</w>’<w n="64.9">aujourd</w>’<w n="64.10">hui</w>.</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Souvent</w> <w n="65.2">l</w>’<w n="65.3">esprit</w> <w n="65.4">est</w> <w n="65.5">foible</w> <w n="65.6">et</w> <w n="65.7">les</w> <w n="65.8">sens</w> <w n="65.9">indociles</w>,</l>
						<l n="66" num="17.2"><w n="66.1">L</w>’<w n="66.2">amour</w>-<w n="66.3">propre</w> <w n="66.4">leur</w> <w n="66.5">fait</w> <w n="66.6">ou</w> <w n="66.7">la</w> <w n="66.8">guerre</w> <w n="66.9">ou</w> <w n="66.10">la</w> <w n="66.11">loi</w> ;</l>
						<l n="67" num="17.3"><w n="67.1">Mais</w> <w n="67.2">bien</w> <w n="67.3">qu</w>’<w n="67.4">en</w> <w n="67.5">général</w> <w n="67.6">nous</w> <w n="67.7">soyons</w> <w n="67.8">tous</w> <w n="67.9">fragiles</w>,</l>
						<l n="68" num="17.4"><w n="68.1">Tu</w> <w n="68.2">n</w>’<w n="68.3">en</w> <w n="68.4">dois</w> <w n="68.5">croire</w> <w n="68.6">aucun</w> <w n="68.7">si</w> <w n="68.8">fragile</w> <w n="68.9">que</w> <w n="68.10">toi</w>.</l>
					</lg>
				</div></body></text></TEI>