<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR104">
					<head type="main">CHAPITRE XXXV</head>
					<head type="sub">Que durant cette vie on n’est jamais en sureté <lb></lb>contre les tentations.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">vie</w> <w n="1.3">est</w> <w n="1.4">un</w> <w n="1.5">torrent</w> <w n="1.6">d</w>’<w n="1.7">éternelles</w> <w n="1.8">disgrâces</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Jamais</w> <w n="2.2">la</w> <w n="2.3">sûreté</w> <w n="2.4">n</w>’<w n="2.5">accompagne</w> <w n="2.6">son</w> <w n="2.7">cours</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Entre</w> <w n="3.2">mille</w> <w n="3.3">ennemis</w> <w n="3.4">il</w> <w n="3.5">faut</w> <w n="3.6">que</w> <w n="3.7">tu</w> <w n="3.8">la</w> <w n="3.9">passes</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">À</w> <w n="4.2">la</w> <w n="4.3">gauche</w>, <w n="4.4">à</w> <w n="4.5">la</w> <w n="4.6">droite</w>, <w n="4.7">il</w> <w n="4.8">en</w> <w n="4.9">renaît</w> <w n="4.10">toujours</w> :</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Ce</w> <w n="5.2">sont</w> <w n="5.3">guerres</w> <w n="5.4">continuelles</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">portent</w> <w n="6.3">dans</w> <w n="6.4">ton</w> <w n="6.5">sein</w> <w n="6.6">chaque</w> <w n="6.7">jour</w> <w n="6.8">mille</w> <w n="6.9">morts</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Si</w> <w n="7.2">tu</w> <w n="7.3">n</w>’<w n="7.4">es</w> <w n="7.5">bien</w> <w n="7.6">muni</w> <w n="7.7">d</w>’<w n="7.8">armes</w> <w n="7.9">spirituelles</w></l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Pour</w> <w n="8.2">en</w> <w n="8.3">repousser</w> <w n="8.4">les</w> <w n="8.5">efforts</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">De</w> <w n="9.2">leur</w> <w n="9.3">succès</w> <w n="9.4">douteux</w> <w n="9.5">la</w> <w n="9.6">juste</w> <w n="9.7">défiance</w></l>
						<l n="10" num="2.2"><w n="10.1">Demande</w> <w n="10.2">à</w> <w n="10.3">ta</w> <w n="10.4">vertu</w> <w n="10.5">de</w> <w n="10.6">vigoureux</w> <w n="10.7">apprêts</w> ;</l>
						<l n="11" num="2.3"><w n="11.1">Mais</w> <w n="11.2">il</w> <w n="11.3">te</w> <w n="11.4">faut</w> <w n="11.5">surtout</w> <w n="11.6">l</w>’<w n="11.7">écu</w> <w n="11.8">de</w> <w n="11.9">patience</w></l>
						<l n="12" num="2.4"><w n="12.1">Qui</w> <w n="12.2">te</w> <w n="12.3">dérobe</w> <w n="12.4">entier</w> <w n="12.5">aux</w> <w n="12.6">pointes</w> <w n="12.7">de</w> <w n="12.8">leurs</w> <w n="12.9">traits</w>.</l>
						<l n="13" num="2.5"><space unit="char" quantity="8"></space><w n="13.1">Que</w> <w n="13.2">de</w> <w n="13.3">tous</w> <w n="13.4">côtés</w> <w n="13.5">il</w> <w n="13.6">te</w> <w n="13.7">couvre</w>,</l>
						<l n="14" num="2.6"><w n="14.1">Sans</w> <w n="14.2">que</w> <w n="14.3">par</w> <w n="14.4">art</w> <w n="14.5">ni</w> <w n="14.6">force</w> <w n="14.7">il</w> <w n="14.8">puisse</w> <w n="14.9">être</w> <w n="14.10">enfoncé</w> ;</l>
						<l n="15" num="2.7"><w n="15.1">Autrement</w> <w n="15.2">tiens</w>-<w n="15.3">toi</w> <w n="15.4">sûr</w> <w n="15.5">que</w> <w n="15.6">pour</w> <w n="15.7">peu</w> <w n="15.8">qu</w>’<w n="15.9">il</w> <w n="15.10">s</w>’<w n="15.11">entr</w>’<w n="15.12">ouvre</w>,</l>
						<l n="16" num="2.8"><space unit="char" quantity="8"></space><w n="16.1">Tu</w> <w n="16.2">te</w> <w n="16.3">verras</w> <w n="16.4">soudain</w> <w n="16.5">percé</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">À</w> <w n="17.2">moins</w> <w n="17.3">qu</w>’<w n="17.4">à</w> <w n="17.5">mes</w> <w n="17.6">bontés</w> <w n="17.7">ton</w> <w n="17.8">âme</w> <w n="17.9">abandonnée</w></l>
						<l n="18" num="3.2"><w n="18.1">Embrasse</w> <w n="18.2">aveuglément</w> <w n="18.3">ce</w> <w n="18.4">que</w> <w n="18.5">j</w>’<w n="18.6">aurai</w> <w n="18.7">voulu</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Et</w> <w n="19.2">qu</w>’<w n="19.3">une</w> <w n="19.4">volonté</w> <w n="19.5">ferme</w> <w n="19.6">et</w> <w n="19.7">déterminée</w></l>
						<l n="20" num="3.4"><w n="20.1">À</w> <w n="20.2">tout</w> <w n="20.3">souffrir</w> <w n="20.4">pour</w> <w n="20.5">moi</w> <w n="20.6">te</w> <w n="20.7">tienne</w> <w n="20.8">résolu</w>,</l>
						<l n="21" num="3.5"><space unit="char" quantity="8"></space><w n="21.1">Ne</w> <w n="21.2">te</w> <w n="21.3">promets</w> <w n="21.4">point</w> <w n="21.5">cette</w> <w n="21.6">gloire</w></l>
						<l n="22" num="3.6"><w n="22.1">De</w> <w n="22.2">pouvoir</w> <w n="22.3">soutenir</w> <w n="22.4">l</w>’<w n="22.5">ardeur</w> <w n="22.6">d</w>’<w n="22.7">un</w> <w n="22.8">tel</w> <w n="22.9">combat</w>,</l>
						<l n="23" num="3.7"><w n="23.1">Et</w> <w n="23.2">d</w>’<w n="23.3">emporter</w> <w n="23.4">enfin</w> <w n="23.5">cette</w> <w n="23.6">pleine</w> <w n="23.7">victoire</w></l>
						<l n="24" num="3.8"><space unit="char" quantity="8"></space><w n="24.1">Qui</w> <w n="24.2">de</w> <w n="24.3">mes</w> <w n="24.4">saints</w> <w n="24.5">fait</w> <w n="24.6">tout</w> <w n="24.7">l</w>’<w n="24.8">éclat</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Tu</w> <w n="25.2">dois</w> <w n="25.3">donc</w>, <w n="25.4">ô</w> <w n="25.5">mon</w> <w n="25.6">fils</w> ! <w n="25.7">Franchir</w> <w n="25.8">avec</w> <w n="25.9">courage</w></l>
						<l n="26" num="4.2"><w n="26.1">Les</w> <w n="26.2">plus</w> <w n="26.3">affreux</w> <w n="26.4">périls</w> <w n="26.5">qui</w> <w n="26.6">t</w>’<w n="26.7">osent</w> <w n="26.8">menacer</w>,</l>
						<l n="27" num="4.3"><w n="27.1">Et</w> <w n="27.2">d</w>’<w n="27.3">une</w> <w n="27.4">main</w> <w n="27.5">puissante</w> <w n="27.6">arracher</w> <w n="27.7">l</w>’<w n="27.8">avantage</w></l>
						<l n="28" num="4.4"><w n="28.1">Aux</w> <w n="28.2">plus</w> <w n="28.3">fiers</w> <w n="28.4">escadrons</w> <w n="28.5">qui</w> <w n="28.6">te</w> <w n="28.7">veuillent</w> <w n="28.8">forcer</w>.</l>
						<l n="29" num="4.5"><space unit="char" quantity="8"></space><w n="29.1">Je</w> <w n="29.2">vois</w> <w n="29.3">d</w>’<w n="29.4">en</w> <w n="29.5">haut</w> <w n="29.6">tout</w> <w n="29.7">comme</w> <w n="29.8">père</w>,</l>
						<l n="30" num="4.6"><w n="30.1">Prêt</w> <w n="30.2">à</w> <w n="30.3">donner</w> <w n="30.4">la</w> <w n="30.5">manne</w> <w n="30.6">au</w> <w n="30.7">généreux</w> <w n="30.8">vainqueur</w> ;</l>
						<l n="31" num="4.7"><w n="31.1">Mais</w> <w n="31.2">je</w> <w n="31.3">réserve</w> <w n="31.4">aussi</w> <w n="31.5">misère</w> <w n="31.6">sur</w> <w n="31.7">misère</w></l>
						<l n="32" num="4.8"><space unit="char" quantity="8"></space><w n="32.1">À</w> <w n="32.2">quiconque</w> <w n="32.3">manque</w> <w n="32.4">de</w> <w n="32.5">cœur</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Si</w> <w n="33.2">durant</w> <w n="33.3">une</w> <w n="33.4">vie</w> <w n="33.5">où</w> <w n="33.6">rien</w> <w n="33.7">n</w>’<w n="33.8">est</w> <w n="33.9">perdurable</w>,</l>
						<l n="34" num="5.2"><w n="34.1">Tu</w> <w n="34.2">te</w> <w n="34.3">rends</w> <w n="34.4">amoureux</w> <w n="34.5">de</w> <w n="34.6">la</w> <w n="34.7">tranquillité</w>,</l>
						<l n="35" num="5.3"><w n="35.1">Oseras</w>-<w n="35.2">tu</w> <w n="35.3">prétendre</w> <w n="35.4">à</w> <w n="35.5">ce</w> <w n="35.6">calme</w> <w n="35.7">ineffable</w></l>
						<l n="36" num="5.4"><w n="36.1">Que</w> <w n="36.2">gardent</w> <w n="36.3">les</w> <w n="36.4">trésors</w> <w n="36.5">de</w> <w n="36.6">mon</w> <w n="36.7">éternité</w> ?</l>
						<l n="37" num="5.5"><space unit="char" quantity="8"></space><w n="37.1">Quitte</w> <w n="37.2">ces</w> <w n="37.3">folles</w> <w n="37.4">espérances</w>,</l>
						<l n="38" num="5.6"><w n="38.1">Préfère</w> <w n="38.2">à</w> <w n="38.3">ces</w> <w n="38.4">desirs</w> <w n="38.5">les</w> <w n="38.6">desirs</w> <w n="38.7">d</w>’<w n="38.8">endurer</w>,</l>
						<l n="39" num="5.7"><w n="39.1">Et</w> <w n="39.2">sache</w> <w n="39.3">que</w> <w n="39.4">ce</w> <w n="39.5">n</w>’<w n="39.6">est</w> <w n="39.7">qu</w>’<w n="39.8">à</w> <w n="39.9">de</w> <w n="39.10">longues</w> <w n="39.11">souffrances</w></l>
						<l n="40" num="5.8"><space unit="char" quantity="8"></space><w n="40.1">Que</w> <w n="40.2">ton</w> <w n="40.3">cœur</w> <w n="40.4">se</w> <w n="40.5">doit</w> <w n="40.6">préparer</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">La</w> <w n="41.2">véritable</w> <w n="41.3">paix</w> <w n="41.4">a</w> <w n="41.5">des</w> <w n="41.6">douceurs</w> <w n="41.7">bien</w> <w n="41.8">pures</w>,</l>
						<l n="42" num="6.2"><w n="42.1">Mais</w> <w n="42.2">en</w> <w n="42.3">vain</w> <w n="42.4">sur</w> <w n="42.5">la</w> <w n="42.6">terre</w> <w n="42.7">on</w> <w n="42.8">pense</w> <w n="42.9">l</w>’<w n="42.10">obtenir</w> :</l>
						<l n="43" num="6.3"><w n="43.1">Il</w> <w n="43.2">n</w>’<w n="43.3">est</w> <w n="43.4">aucuns</w> <w n="43.5">mortels</w>, <w n="43.6">aucunes</w> <w n="43.7">créatures</w>,</l>
						<l n="44" num="6.4"><w n="44.1">Dont</w> <w n="44.2">les</w> <w n="44.3">secours</w> <w n="44.4">unis</w> <w n="44.5">y</w> <w n="44.6">fassent</w> <w n="44.7">parvenir</w>.</l>
						<l n="45" num="6.5"><space unit="char" quantity="8"></space><w n="45.1">C</w>’<w n="45.2">est</w> <w n="45.3">moi</w>, <w n="45.4">c</w>’<w n="45.5">est</w> <w n="45.6">moi</w> <w n="45.7">seul</w> <w n="45.8">qui</w> <w n="45.9">la</w> <w n="45.10">donne</w>,</l>
						<l n="46" num="6.6"><w n="46.1">Ne</w> <w n="46.2">la</w> <w n="46.3">cherche</w> <w n="46.4">qu</w>’<w n="46.5">au</w> <w n="46.6">ciel</w>, <w n="46.7">ne</w> <w n="46.8">l</w>’<w n="46.9">attends</w> <w n="46.10">que</w> <w n="46.11">de</w> <w n="46.12">moi</w> ;</l>
						<l n="47" num="6.7"><w n="47.1">Mais</w> <w n="47.2">apprends</w> <w n="47.3">qu</w>’<w n="47.4">il</w> <w n="47.5">t</w>’<w n="47.6">en</w> <w n="47.7">faut</w> <w n="47.8">acheter</w> <w n="47.9">la</w> <w n="47.10">couronne</w></l>
						<l n="48" num="6.8"><space unit="char" quantity="8"></space><w n="48.1">Par</w> <w n="48.2">les</w> <w n="48.3">épreuves</w> <w n="48.4">de</w> <w n="48.5">ta</w> <w n="48.6">foi</w>.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1"><w n="49.1">Les</w> <w n="49.2">travaux</w>, <w n="49.3">les</w> <w n="49.4">douleurs</w>, <w n="49.5">les</w> <w n="49.6">ennuis</w>, <w n="49.7">les</w> <w n="49.8">injures</w>,</l>
						<l n="50" num="7.2"><w n="50.1">La</w> <w n="50.2">pauvreté</w>, <w n="50.3">le</w> <w n="50.4">trouble</w> <w n="50.5">et</w> <w n="50.6">les</w> <w n="50.7">anxiétés</w>,</l>
						<l n="51" num="7.3"><w n="51.1">Souffrir</w> <w n="51.2">la</w> <w n="51.3">réprimande</w>, <w n="51.4">endurer</w> <w n="51.5">les</w> <w n="51.6">murmures</w>,</l>
						<l n="52" num="7.4"><w n="52.1">Ne</w> <w n="52.2">se</w> <w n="52.3">point</w> <w n="52.4">rebuter</w> <w n="52.5">de</w> <w n="52.6">mille</w> <w n="52.7">infirmités</w>,</l>
						<l n="53" num="7.5"><space unit="char" quantity="8"></space><w n="53.1">Accepter</w> <w n="53.2">pour</w> <w n="53.3">moi</w> <w n="53.4">les</w> <w n="53.5">rudesses</w>,</l>
						<l n="54" num="7.6"><w n="54.1">L</w>’<w n="54.2">humiliation</w>, <w n="54.3">les</w> <w n="54.4">affronts</w>, <w n="54.5">les</w> <w n="54.6">mépris</w>,</l>
						<l n="55" num="7.7"><w n="55.1">Prendre</w> <w n="55.2">tout</w> <w n="55.3">de</w> <w n="55.4">ma</w> <w n="55.5">main</w> <w n="55.6">comme</w> <w n="55.7">autant</w> <w n="55.8">de</w> <w n="55.9">caresses</w>,</l>
						<l n="56" num="7.8"><space unit="char" quantity="8"></space><w n="56.1">C</w>’<w n="56.2">en</w> <w n="56.3">est</w> <w n="56.4">le</w> <w n="56.5">véritable</w> <w n="56.6">prix</w>.</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1"><w n="57.1">C</w>’<w n="57.2">est</w> <w n="57.3">par</w> <w n="57.4">de</w> <w n="57.5">tels</w> <w n="57.6">sentiers</w> <w n="57.7">qu</w>’<w n="57.8">enfin</w> <w n="57.9">la</w> <w n="57.10">patience</w></l>
						<l n="58" num="8.2"><w n="58.1">À</w> <w n="58.2">la</w> <w n="58.3">haute</w> <w n="58.4">vertu</w> <w n="58.5">guide</w> <w n="58.6">un</w> <w n="58.7">nouveau</w> <w n="58.8">soldat</w> ;</l>
						<l n="59" num="8.3"><w n="59.1">C</w>’<w n="59.2">est</w> <w n="59.3">par</w> <w n="59.4">cette</w> <w n="59.5">fâcheuse</w> <w n="59.6">et</w> <w n="59.7">rude</w> <w n="59.8">expérience</w></l>
						<l n="60" num="8.4"><w n="60.1">Qu</w>’<w n="60.2">il</w> <w n="60.3">trouve</w> <w n="60.4">un</w> <w n="60.5">diadème</w> <w n="60.6">au</w> <w n="60.7">sortir</w> <w n="60.8">du</w> <w n="60.9">combat</w>.</l>
						<l n="61" num="8.5"><space unit="char" quantity="8"></space><w n="61.1">Ainsi</w> <w n="61.2">d</w>’<w n="61.3">une</w> <w n="61.4">peine</w> <w n="61.5">légère</w></l>
						<l n="62" num="8.6"><w n="62.1">La</w> <w n="62.2">longue</w> <w n="62.3">récompense</w> <w n="62.4">est</w> <w n="62.5">un</w> <w n="62.6">repos</w> <w n="62.7">divin</w>,</l>
						<l n="63" num="8.7"><w n="63.1">Et</w> <w n="63.2">pour</w> <w n="63.3">quelques</w> <w n="63.4">moments</w> <w n="63.5">de</w> <w n="63.6">honte</w> <w n="63.7">passagère</w></l>
						<l n="64" num="8.8"><space unit="char" quantity="8"></space><w n="64.1">Je</w> <w n="64.2">rends</w> <w n="64.3">une</w> <w n="64.4">gloire</w> <w n="64.5">sans</w> <w n="64.6">fin</w>.</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1"><w n="65.1">Cependant</w> <w n="65.2">tu</w> <w n="65.3">te</w> <w n="65.4">plains</w> <w n="65.5">sitôt</w> <w n="65.6">que</w> <w n="65.7">sans</w> <w n="65.8">tendresse</w></l>
						<l n="66" num="9.2"><w n="66.1">Je</w> <w n="66.2">laisse</w> <w n="66.3">un</w> <w n="66.4">peu</w> <w n="66.5">durer</w> <w n="66.6">les</w> <w n="66.7">tribulations</w> ;</l>
						<l n="67" num="9.3"><w n="67.1">Comme</w> <w n="67.2">si</w> <w n="67.3">ma</w> <w n="67.4">bonté</w>, <w n="67.5">soumise</w> <w n="67.6">à</w> <w n="67.7">ta</w> <w n="67.8">foiblesse</w>,</l>
						<l n="68" num="9.4"><w n="68.1">Devoit</w> <w n="68.2">à</w> <w n="68.3">point</w> <w n="68.4">nommé</w> <w n="68.5">ses</w> <w n="68.6">consolations</w> !</l>
						<l n="69" num="9.5"><space unit="char" quantity="8"></space><w n="69.1">Tous</w> <w n="69.2">mes</w> <w n="69.3">saints</w> <w n="69.4">ne</w> <w n="69.5">les</w> <w n="69.6">ont</w> <w n="69.7">pas</w> <w n="69.8">eues</w>,</l>
						<l n="70" num="9.6"><w n="70.1">Alors</w> <w n="70.2">que</w> <w n="70.3">sur</w> <w n="70.4">la</w> <w n="70.5">terre</w> <w n="70.6">ils</w> <w n="70.7">vivoient</w> <w n="70.8">exilés</w>,</l>
						<l n="71" num="9.7"><w n="71.1">Et</w> <w n="71.2">dans</w> <w n="71.3">leurs</w> <w n="71.4">plus</w> <w n="71.5">grands</w> <w n="71.6">maux</w> <w n="71.7">mes</w> <w n="71.8">faveurs</w> <w n="71.9">suspendues</w></l>
						<l n="72" num="9.8"><space unit="char" quantity="8"></space><w n="72.1">Souvent</w> <w n="72.2">les</w> <w n="72.3">laissoient</w> <w n="72.4">désolés</w>.</l>
					</lg>
					<lg n="10">
						<l n="73" num="10.1"><w n="73.1">Mais</w> <w n="73.2">dans</w> <w n="73.3">ces</w> <w n="73.4">mêmes</w> <w n="73.5">maux</w> <w n="73.6">qui</w> <w n="73.7">sembloient</w> <w n="73.8">sans</w> <w n="73.9">limites</w>,</l>
						<l n="74" num="10.2"><w n="74.1">Armés</w> <w n="74.2">de</w> <w n="74.3">patience</w>, <w n="74.4">ils</w> <w n="74.5">souffroient</w> <w n="74.6">jusqu</w>’<w n="74.7">au</w> <w n="74.8">bout</w>,</l>
						<l n="75" num="10.3"><w n="75.1">Et</w> <w n="75.2">s</w>’<w n="75.3">assuroient</w> <w n="75.4">bien</w> <w n="75.5">moins</w> <w n="75.6">en</w> <w n="75.7">leurs</w> <w n="75.8">propres</w> <w n="75.9">mérites</w></l>
						<l n="76" num="10.4"><w n="76.1">Qu</w>’<w n="76.2">en</w> <w n="76.3">la</w> <w n="76.4">bonté</w> <w n="76.5">d</w>’<w n="76.6">un</w> <w n="76.7">dieu</w> <w n="76.8">dont</w> <w n="76.9">ils</w> <w n="76.10">espéroient</w> <w n="76.11">tout</w> :</l>
						<l n="77" num="10.5"><space unit="char" quantity="8"></space><w n="77.1">Ils</w> <w n="77.2">savoient</w> <w n="77.3">bien</w>, <w n="77.4">ces</w> <w n="77.5">vrais</w> <w n="77.6">fidèles</w>,</l>
						<l n="78" num="10.6"><w n="78.1">De</w> <w n="78.2">quel</w> <w n="78.3">immense</w> <w n="78.4">prix</w> <w n="78.5">étoit</w> <w n="78.6">l</w>’<w n="78.7">éternité</w>,</l>
						<l n="79" num="10.7"><w n="79.1">Et</w> <w n="79.2">que</w> <w n="79.3">pour</w> <w n="79.4">l</w>’<w n="79.5">obtenir</w> <w n="79.6">les</w> <w n="79.7">gênes</w> <w n="79.8">temporelles</w></l>
						<l n="80" num="10.8"><space unit="char" quantity="8"></space><w n="80.1">N</w>’<w n="80.2">avoient</w> <w n="80.3">point</w> <w n="80.4">de</w> <w n="80.5">condignité</w>.</l>
					</lg>
					<lg n="11">
						<l n="81" num="11.1"><w n="81.1">As</w>-<w n="81.2">tu</w> <w n="81.3">droit</w> <w n="81.4">de</w> <w n="81.5">vouloir</w> <w n="81.6">dès</w> <w n="81.7">les</w> <w n="81.8">moindres</w> <w n="81.9">alarmes</w>,</l>
						<l n="82" num="11.2"><w n="82.1">Toi</w> <w n="82.2">qui</w> <w n="82.3">n</w>’<w n="82.4">es</w> <w n="82.5">en</w> <w n="82.6">effet</w> <w n="82.7">qu</w>’<w n="82.8">ordure</w> <w n="82.9">et</w> <w n="82.10">que</w> <w n="82.11">péché</w>,</l>
						<l n="83" num="11.3"><w n="83.1">Ce</w> <w n="83.2">qu</w>’<w n="83.3">en</w> <w n="83.4">un</w> <w n="83.5">siècle</w> <w n="83.6">entier</w> <w n="83.7">de</w> <w n="83.8">travaux</w> <w n="83.9">et</w> <w n="83.10">de</w> <w n="83.11">larmes</w></l>
						<l n="84" num="11.4"><w n="84.1">Tant</w> <w n="84.2">et</w> <w n="84.3">tant</w> <w n="84.4">de</w> <w n="84.5">parfaits</w> <w n="84.6">m</w>’<w n="84.7">ont</w> <w n="84.8">à</w> <w n="84.9">peine</w> <w n="84.10">arraché</w> ?</l>
						<l n="85" num="11.5"><space unit="char" quantity="8"></space><w n="85.1">Attends</w> <w n="85.2">que</w> <w n="85.3">l</w>’<w n="85.4">heure</w> <w n="85.5">en</w> <w n="85.6">soit</w> <w n="85.7">venue</w>,</l>
						<l n="86" num="11.6"><w n="86.1">Cette</w> <w n="86.2">heure</w> <w n="86.3">où</w> <w n="86.4">tu</w> <w n="86.5">seras</w> <w n="86.6">visité</w> <w n="86.7">du</w> <w n="86.8">seigneur</w> ;</l>
						<l n="87" num="11.7"><w n="87.1">Travaille</w> <w n="87.2">en</w> <w n="87.3">l</w>’<w n="87.4">attendant</w>, <w n="87.5">commence</w>, <w n="87.6">et</w> <w n="87.7">continue</w></l>
						<l n="88" num="11.8"><space unit="char" quantity="8"></space><w n="88.1">Avec</w> <w n="88.2">grand</w> <w n="88.3">amour</w> <w n="88.4">et</w> <w n="88.5">grand</w> <w n="88.6">cœur</w>.</l>
					</lg>
					<lg n="12">
						<l n="89" num="12.1"><w n="89.1">Ne</w> <w n="89.2">relâche</w> <w n="89.3">jamais</w>, <w n="89.4">jamais</w> <w n="89.5">ne</w> <w n="89.6">te</w> <w n="89.7">défie</w>,</l>
						<l n="90" num="12.2"><w n="90.1">Quelques</w> <w n="90.2">tristes</w> <w n="90.3">succès</w> <w n="90.4">qui</w> <w n="90.5">suivent</w> <w n="90.6">tes</w> <w n="90.7">efforts</w> ;</l>
						<l n="91" num="12.3"><w n="91.1">Redouble</w> <w n="91.2">ta</w> <w n="91.3">constance</w>, <w n="91.4">expose</w> <w n="91.5">et</w> <w n="91.6">sacrifie</w></l>
						<l n="92" num="12.4"><w n="92.1">Pour</w> <w n="92.2">ma</w> <w n="92.3">plus</w> <w n="92.4">grande</w> <w n="92.5">gloire</w> <w n="92.6">et</w> <w n="92.7">ton</w> <w n="92.8">âme</w> <w n="92.9">et</w> <w n="92.10">ton</w> <w n="92.11">corps</w>.</l>
						<l n="93" num="12.5"><space unit="char" quantity="8"></space><w n="93.1">Je</w> <w n="93.2">rendrai</w> <w n="93.3">tout</w> <w n="93.4">avec</w> <w n="93.5">usure</w> ;</l>
						<l n="94" num="12.6"><w n="94.1">Je</w> <w n="94.2">suis</w> <w n="94.3">dans</w> <w n="94.4">le</w> <w n="94.5">combat</w> <w n="94.6">sans</w> <w n="94.7">cesse</w> <w n="94.8">à</w> <w n="94.9">tes</w> <w n="94.10">côtés</w>,</l>
						<l n="95" num="12.7"><w n="95.1">Et</w> <w n="95.2">je</w> <w n="95.3">reconnoîtrai</w> <w n="95.4">ce</w> <w n="95.5">que</w> <w n="95.6">ton</w> <w n="95.7">cœur</w> <w n="95.8">endure</w></l>
						<l n="96" num="12.8"><space unit="char" quantity="8"></space><w n="96.1">Par</w> <w n="96.2">de</w> <w n="96.3">pleines</w> <w n="96.4">félicités</w>.</l>
					</lg>
				</div></body></text></TEI>