<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR97">
					<head type="main">CHAPITRE XXVIII</head>
					<head type="sub">Contre les langues médisantes.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">fils</w>, <w n="1.3">si</w> <w n="1.4">quelques</w>-<w n="1.5">uns</w> <w n="1.6">forment</w> <w n="1.7">des</w> <w n="1.8">sentiments</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Qui</w> <w n="2.2">soient</w> <w n="2.3">à</w> <w n="2.4">ton</w> <w n="2.5">désavantage</w>,</l>
						<l n="3" num="1.3"><w n="3.1">S</w>’<w n="3.2">ils</w> <w n="3.3">tiennent</w> <w n="3.4">des</w> <w n="3.5">discours</w>, <w n="3.6">s</w>’<w n="3.7">ils</w> <w n="3.8">font</w> <w n="3.9">des</w> <w n="3.10">jugements</w></l>
						<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">ternissent</w> <w n="4.3">ta</w> <w n="4.4">gloire</w> <w n="4.5">et</w> <w n="4.6">te</w> <w n="4.7">fassent</w> <w n="4.8">outrage</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Ne</w> <w n="5.2">t</w>’<w n="5.3">en</w> <w n="5.4">indigne</w> <w n="5.5">point</w>, <w n="5.6">n</w>’<w n="5.7">en</w> <w n="5.8">fais</w> <w n="5.9">point</w> <w n="5.10">le</w> <w n="5.11">surpris</w> :</l>
						<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Quels</w> <w n="6.2">que</w> <w n="6.3">soient</w> <w n="6.4">leurs</w> <w n="6.5">mépris</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Ton</w> <w n="7.2">estime</w> <w n="7.3">pour</w> <w n="7.4">toi</w> <w n="7.5">doit</w> <w n="7.6">être</w> <w n="7.7">encor</w> <w n="7.8">plus</w> <w n="7.9">basse</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">Tu</w> <w n="8.2">dois</w> <w n="8.3">croire</w>, <w n="8.4">au</w> <w n="8.5">milieu</w> <w n="8.6">de</w> <w n="8.7">leur</w> <w n="8.8">indignité</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Quelque</w> <w n="9.2">puissante</w> <w n="9.3">en</w> <w n="9.4">toi</w> <w n="9.5">que</w> <w n="9.6">tu</w> <w n="9.7">sentes</w> <w n="9.8">ma</w> <w n="9.9">grâce</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Qu</w>’<w n="10.2">il</w> <w n="10.3">n</w>’<w n="10.4">est</w> <w n="10.5">foiblesse</w> <w n="10.6">égale</w> <w n="10.7">à</w> <w n="10.8">ton</w> <w n="10.9">infirmité</w>.</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">Si</w> <w n="11.2">dans</w> <w n="11.3">l</w>’<w n="11.4">intérieur</w> <w n="11.5">un</w> <w n="11.6">bon</w> <w n="11.7">et</w> <w n="11.8">saint</w> <w n="11.9">emploi</w></l>
						<l n="12" num="2.2"><space unit="char" quantity="8"></space><w n="12.1">Te</w> <w n="12.2">donne</w> <w n="12.3">une</w> <w n="12.4">démarche</w> <w n="12.5">forte</w>,</l>
						<l n="13" num="2.3"><w n="13.1">Tu</w> <w n="13.2">ne</w> <w n="13.3">prendras</w> <w n="13.4">jamais</w> <w n="13.5">le</w> <w n="13.6">mal</w> <w n="13.7">qu</w>’<w n="13.8">on</w> <w n="13.9">dit</w> <w n="13.10">de</w> <w n="13.11">toi</w></l>
						<l n="14" num="2.4"><w n="14.1">Que</w> <w n="14.2">pour</w> <w n="14.3">un</w> <w n="14.4">son</w> <w n="14.5">volage</w> <w n="14.6">et</w> <w n="14.7">que</w> <w n="14.8">le</w> <w n="14.9">vent</w> <w n="14.10">emporte</w>.</l>
						<l n="15" num="2.5"><w n="15.1">Il</w> <w n="15.2">faut</w> <w n="15.3">de</w> <w n="15.4">la</w> <w n="15.5">prudence</w> <w n="15.6">en</w> <w n="15.7">ces</w> <w n="15.8">moments</w> <w n="15.9">fâcheux</w> ;</l>
						<l n="16" num="2.6"><space unit="char" quantity="12"></space><w n="16.1">Et</w> <w n="16.2">celle</w> <w n="16.3">que</w> <w n="16.4">je</w> <w n="16.5">veux</w>,</l>
						<l n="17" num="2.7"><w n="17.1">Celle</w> <w n="17.2">que</w> <w n="17.3">je</w> <w n="17.4">demande</w>, <w n="17.5">est</w> <w n="17.6">qu</w>’<w n="17.7">on</w> <w n="17.8">sache</w> <w n="17.9">se</w> <w n="17.10">taire</w>,</l>
						<l n="18" num="2.8"><w n="18.1">Qu</w>’<w n="18.2">on</w> <w n="18.3">sache</w> <w n="18.4">au</w> <w n="18.5">fond</w> <w n="18.6">du</w> <w n="18.7">cœur</w> <w n="18.8">vers</w> <w n="18.9">moi</w> <w n="18.10">se</w> <w n="18.11">retourner</w>,</l>
						<l n="19" num="2.9"><w n="19.1">Sans</w> <w n="19.2">relâcher</w> <w n="19.3">en</w> <w n="19.4">rien</w> <w n="19.5">son</w> <w n="19.6">allure</w> <w n="19.7">ordinaire</w>,</l>
						<l n="20" num="2.10"><w n="20.1">Pour</w> <w n="20.2">chose</w> <w n="20.3">que</w> <w n="20.4">le</w> <w n="20.5">monde</w> <w n="20.6">en</w> <w n="20.7">veuille</w> <w n="20.8">condamner</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Ne</w> <w n="21.2">fais</w> <w n="21.3">point</w> <w n="21.4">cet</w> <w n="21.5">honneur</w> <w n="21.6">aux</w> <w n="21.7">hommes</w> <w n="21.8">imparfaits</w>,</l>
						<l n="22" num="3.2"><space unit="char" quantity="8"></space><w n="22.1">Que</w> <w n="22.2">leur</w> <w n="22.3">vain</w> <w n="22.4">langage</w> <w n="22.5">te</w> <w n="22.6">touche</w> ;</l>
						<l n="23" num="3.3"><w n="23.1">Ne</w> <w n="23.2">fais</w> <w n="23.3">point</w> <w n="23.4">consister</w> <w n="23.5">ta</w> <w n="23.6">gloire</w> <w n="23.7">ni</w> <w n="23.8">ta</w> <w n="23.9">paix</w></l>
						<l n="24" num="3.4"><w n="24.1">En</w> <w n="24.2">ces</w> <w n="24.3">discours</w> <w n="24.4">en</w> <w n="24.5">l</w>’<w n="24.6">air</w> <w n="24.7">qui</w> <w n="24.8">sortent</w> <w n="24.9">de</w> <w n="24.10">leur</w> <w n="24.11">bouche</w>.</l>
						<l n="25" num="3.5"><w n="25.1">Que</w> <w n="25.2">de</w> <w n="25.3">tes</w> <w n="25.4">actions</w> <w n="25.5">ils</w> <w n="25.6">jugent</w> <w n="25.7">bien</w> <w n="25.8">ou</w> <w n="25.9">mal</w>,</l>
						<l n="26" num="3.6"><space unit="char" quantity="12"></space><w n="26.1">Tout</w> <w n="26.2">n</w>’<w n="26.3">est</w>-<w n="26.4">il</w> <w n="26.5">pas</w> <w n="26.6">égal</w> ?</l>
						<l n="27" num="3.7"><w n="27.1">Ton</w> <w n="27.2">âme</w> <w n="27.3">en</w> <w n="27.4">devient</w>-<w n="27.5">elle</w> <w n="27.6">ou</w> <w n="27.7">plus</w> <w n="27.8">nette</w> <w n="27.9">ou</w> <w n="27.10">plus</w> <w n="27.11">noire</w> ?</l>
						<l n="28" num="3.8"><w n="28.1">En</w> <w n="28.2">as</w>-<w n="28.3">tu</w> <w n="28.4">plus</w> <w n="28.5">ou</w> <w n="28.6">moins</w> <w n="28.7">ou</w> <w n="28.8">d</w>’<w n="28.9">amour</w> <w n="28.10">ou</w> <w n="28.11">de</w> <w n="28.12">foi</w> ?</l>
						<l n="29" num="3.9"><w n="29.1">Et</w> <w n="29.2">pour</w> <w n="29.3">tout</w> <w n="29.4">dire</w> <w n="29.5">enfin</w>, <w n="29.6">la</w> <w n="29.7">véritable</w> <w n="29.8">gloire</w>,</l>
						<l n="30" num="3.10"><w n="30.1">La</w> <w n="30.2">véritable</w> <w n="30.3">paix</w>, <w n="30.4">est</w>-<w n="30.5">elle</w> <w n="30.6">ailleurs</w> <w n="30.7">qu</w>’<w n="30.8">en</w> <w n="30.9">moi</w> ?</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1"><w n="31.1">Si</w> <w n="31.2">tu</w> <w n="31.3">peux</w> <w n="31.4">t</w>’<w n="31.5">affranchir</w> <w n="31.6">de</w> <w n="31.7">cette</w> <w n="31.8">lâcheté</w>,</l>
						<l n="32" num="4.2"><space unit="char" quantity="8"></space><w n="32.1">Dont</w> <w n="32.2">l</w>’<w n="32.3">esclavage</w> <w n="32.4">volontaire</w></l>
						<l n="33" num="4.3"><w n="33.1">Cherche</w> <w n="33.2">à</w> <w n="33.3">leur</w> <w n="33.4">agréer</w> <w n="33.5">avec</w> <w n="33.6">avidité</w>,</l>
						<l n="34" num="4.4"><w n="34.1">Et</w> <w n="34.2">compte</w> <w n="34.3">à</w> <w n="34.4">grand</w> <w n="34.5">malheur</w> <w n="34.6">celui</w> <w n="34.7">de</w> <w n="34.8">leur</w> <w n="34.9">déplaire</w>,</l>
						<l n="35" num="4.5"><w n="35.1">Tu</w> <w n="35.2">jouiras</w> <w n="35.3">alors</w> <w n="35.4">d</w>’<w n="35.5">une</w> <w n="35.6">profonde</w> <w n="35.7">paix</w>,</l>
						<l n="36" num="4.6"><space unit="char" quantity="12"></space><w n="36.1">Et</w> <w n="36.2">dans</w> <w n="36.3">tous</w> <w n="36.4">tes</w> <w n="36.5">souhaits</w></l>
						<l n="37" num="4.7"><w n="37.1">Tu</w> <w n="37.2">la</w> <w n="37.3">verras</w> <w n="37.4">passer</w> <w n="37.5">en</w> <w n="37.6">heureuse</w> <w n="37.7">habitude</w>.</l>
						<l n="38" num="4.8"><w n="38.1">Les</w> <w n="38.2">indignes</w> <w n="38.3">frayeurs</w>, <w n="38.4">le</w> <w n="38.5">fol</w> <w n="38.6">emportement</w>,</l>
						<l n="39" num="4.9"><w n="39.1">C</w>’<w n="39.2">est</w> <w n="39.3">ce</w> <w n="39.4">qui</w> <w n="39.5">dans</w> <w n="39.6">ton</w> <w n="39.7">cœur</w> <w n="39.8">jette</w> <w n="39.9">l</w>’<w n="39.10">inquiétude</w>,</l>
						<l n="40" num="4.10"><w n="40.1">C</w>’<w n="40.2">est</w> <w n="40.3">ce</w> <w n="40.4">qui</w> <w n="40.5">de</w> <w n="40.6">tes</w> <w n="40.7">sens</w> <w n="40.8">fait</w> <w n="40.9">tout</w> <w n="40.10">l</w>’<w n="40.11">égarement</w>.</l>
					</lg>
				</div></body></text></TEI>