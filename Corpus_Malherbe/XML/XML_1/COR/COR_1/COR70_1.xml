<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR70">
					<head type="main">CHAPITRE I</head>
					<head type="sub">De l’entretien intérieur de Jésus-Christ <lb></lb>avec l’âme fidèle.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">prêterai</w> <w n="1.3">l</w>’<w n="1.4">oreille</w> <w n="1.5">à</w> <w n="1.6">cette</w> <w n="1.7">voix</w> <w n="1.8">secrète</w></l>
						<l n="2" num="1.2"><w n="2.1">Par</w> <w n="2.2">qui</w> <w n="2.3">le</w> <w n="2.4">tout</w>-<w n="2.5">puissant</w> <w n="2.6">s</w>’<w n="2.7">explique</w> <w n="2.8">au</w> <w n="2.9">fond</w> <w n="2.10">du</w> <w n="2.11">cœur</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">la</w> <w n="3.3">veux</w> <w n="3.4">écouter</w>, <w n="3.5">cette</w> <w n="3.6">aimable</w> <w n="3.7">interprète</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">ce</w> <w n="4.3">qu</w>’<w n="4.4">à</w> <w n="4.5">ses</w> <w n="4.6">élus</w> <w n="4.7">demande</w> <w n="4.8">le</w> <w n="4.9">seigneur</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Oh</w> ! <w n="5.2">Qu</w>’<w n="5.3">heureuse</w> <w n="5.4">est</w> <w n="5.5">une</w> <w n="5.6">âme</w> <w n="5.7">alors</w> <w n="5.8">qu</w>’<w n="5.9">elle</w> <w n="5.10">l</w>’<w n="5.11">écoute</w> !</l>
						<l n="6" num="1.6"><w n="6.1">Qu</w>’<w n="6.2">elle</w> <w n="6.3">devient</w> <w n="6.4">savante</w> <w n="6.5">à</w> <w n="6.6">marcher</w> <w n="6.7">dans</w> <w n="6.8">sa</w> <w n="6.9">route</w> !</l>
						<l n="7" num="1.7"><w n="7.1">Qu</w>’<w n="7.2">elle</w> <w n="7.3">amasse</w> <w n="7.4">de</w> <w n="7.5">force</w> <w n="7.6">à</w> <w n="7.7">l</w>’<w n="7.8">entendre</w> <w n="7.9">parler</w> !</l>
						<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">que</w> <w n="8.3">dans</w> <w n="8.4">ses</w> <w n="8.5">malheurs</w> <w n="8.6">son</w> <w n="8.7">bonheur</w> <w n="8.8">est</w> <w n="8.9">extrême</w>,</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Quand</w> <w n="9.2">de</w> <w n="9.3">la</w> <w n="9.4">bouche</w> <w n="9.5">de</w> <w n="9.6">Dieu</w> <w n="9.7">même</w></l>
						<l n="10" num="1.10"><w n="10.1">Sa</w> <w n="10.2">misère</w> <w n="10.3">reçoit</w> <w n="10.4">de</w> <w n="10.5">quoi</w> <w n="10.6">se</w> <w n="10.7">consoler</w> !</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">Heureuses</w> <w n="11.2">donc</w> <w n="11.3">cent</w> <w n="11.4">fois</w>, <w n="11.5">heureuses</w> <w n="11.6">les</w> <w n="11.7">oreilles</w></l>
						<l n="12" num="2.2"><w n="12.1">Qui</w> <w n="12.2">s</w>’<w n="12.3">ouvrent</w> <w n="12.4">sans</w> <w n="12.5">relâche</w> <w n="12.6">à</w> <w n="12.7">ses</w> <w n="12.8">divins</w> <w n="12.9">accents</w>,</l>
						<l n="13" num="2.3"><w n="13.1">Et</w> <w n="13.2">pleines</w> <w n="13.3">qu</w>’<w n="13.4">elles</w> <w n="13.5">sont</w> <w n="13.6">de</w> <w n="13.7">leurs</w> <w n="13.8">hautes</w> <w n="13.9">merveilles</w>,</l>
						<l n="14" num="2.4"><w n="14.1">Se</w> <w n="14.2">ferment</w> <w n="14.3">au</w> <w n="14.4">tumulte</w> <w n="14.5">et</w> <w n="14.6">du</w> <w n="14.7">monde</w> <w n="14.8">et</w> <w n="14.9">des</w> <w n="14.10">sens</w> !</l>
						<l n="15" num="2.5"><w n="15.1">Oui</w>, <w n="15.2">je</w> <w n="15.3">dirai</w> <w n="15.4">cent</w> <w n="15.5">fois</w> <w n="15.6">ces</w> <w n="15.7">oreilles</w> <w n="15.8">heureuses</w></l>
						<l n="16" num="2.6"><w n="16.1">Qui</w> <w n="16.2">de</w> <w n="16.3">la</w> <w n="16.4">voix</w> <w n="16.5">de</w> <w n="16.6">Dieu</w> <w n="16.7">saintement</w> <w n="16.8">amoureuses</w>,</l>
						<l n="17" num="2.7"><w n="17.1">Méprisent</w> <w n="17.2">ces</w> <w n="17.3">faux</w> <w n="17.4">tons</w> <w n="17.5">qui</w> <w n="17.6">font</w> <w n="17.7">bruit</w> <w n="17.8">au</w> <w n="17.9">dehors</w>,</l>
						<l n="18" num="2.8"><w n="18.1">Pour</w> <w n="18.2">entendre</w> <w n="18.3">au</w> <w n="18.4">dedans</w> <w n="18.5">la</w> <w n="18.6">vérité</w> <w n="18.7">parlante</w>,</l>
						<l n="19" num="2.9"><space unit="char" quantity="8"></space><w n="19.1">De</w> <w n="19.2">qui</w> <w n="19.3">la</w> <w n="19.4">parole</w> <w n="19.5">instruisante</w></l>
						<l n="20" num="2.10"><w n="20.1">N</w>’<w n="20.2">a</w> <w n="20.3">pour</w> <w n="20.4">se</w> <w n="20.5">faire</w> <w n="20.6">ouïr</w> <w n="20.7">que</w> <w n="20.8">de</w> <w n="20.9">muets</w> <w n="20.10">accords</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Heureux</w> <w n="21.2">aussi</w> <w n="21.3">les</w> <w n="21.4">yeux</w> <w n="21.5">que</w> <w n="21.6">les</w> <w n="21.7">objets</w> <w n="21.8">sensibles</w></l>
						<l n="22" num="3.2"><w n="22.1">Ne</w> <w n="22.2">peuvent</w> <w n="22.3">éblouir</w> <w n="22.4">ni</w> <w n="22.5">surprendre</w> <w n="22.6">un</w> <w n="22.7">moment</w> !</l>
						<l n="23" num="3.3"><w n="23.1">Heureux</w> <w n="23.2">ces</w> <w n="23.3">mêmes</w> <w n="23.4">yeux</w> <w n="23.5">que</w> <w n="23.6">les</w> <w n="23.7">dons</w> <w n="23.8">invisibles</w></l>
						<l n="24" num="3.4"><w n="24.1">Tiennent</w> <w n="24.2">sur</w> <w n="24.3">leurs</w> <w n="24.4">trésors</w> <w n="24.5">fixés</w> <w n="24.6">incessamment</w> !</l>
						<l n="25" num="3.5"><w n="25.1">Heureux</w> <w n="25.2">encor</w> <w n="25.3">l</w>’<w n="25.4">esprit</w> <w n="25.5">que</w> <w n="25.6">de</w> <w n="25.7">saints</w> <w n="25.8">exercices</w></l>
						<l n="26" num="3.6"><w n="26.1">Préparent</w> <w n="26.2">chaque</w> <w n="26.3">jour</w> <w n="26.4">par</w> <w n="26.5">la</w> <w n="26.6">fuite</w> <w n="26.7">des</w> <w n="26.8">vices</w></l>
						<l n="27" num="3.7"><w n="27.1">Aux</w> <w n="27.2">secrets</w> <w n="27.3">que</w> <w n="27.4">découvre</w> <w n="27.5">un</w> <w n="27.6">si</w> <w n="27.7">doux</w> <w n="27.8">entretien</w> !</l>
						<l n="28" num="3.8"><w n="28.1">Heureux</w> <w n="28.2">tout</w> <w n="28.3">l</w>’<w n="28.4">homme</w> <w n="28.5">enfin</w> <w n="28.6">que</w> <w n="28.7">ces</w> <w n="28.8">petits</w> <w n="28.9">miracles</w></l>
						<l n="29" num="3.9"><space unit="char" quantity="8"></space><w n="29.1">Purgent</w> <w n="29.2">si</w> <w n="29.3">bien</w> <w n="29.4">de</w> <w n="29.5">tous</w> <w n="29.6">obstacles</w>,</l>
						<l n="30" num="3.10"><w n="30.1">Qu</w>’<w n="30.2">il</w> <w n="30.3">n</w>’<w n="30.4">écoute</w>, <w n="30.5">hors</w> <w n="30.6">Dieu</w>, <w n="30.7">ne</w> <w n="30.8">voit</w>, <w n="30.9">ne</w> <w n="30.10">cherche</w> <w n="30.11">rien</w> !</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1"><w n="31.1">Prends</w>-<w n="31.2">y</w> <w n="31.3">garde</w>, <w n="31.4">mon</w> <w n="31.5">âme</w>, <w n="31.6">et</w> <w n="31.7">ferme</w> <w n="31.8">bien</w> <w n="31.9">la</w> <w n="31.10">porte</w></l>
						<l n="32" num="4.2"><w n="32.1">Aux</w> <w n="32.2">plaisirs</w> <w n="32.3">que</w> <w n="32.4">tes</w> <w n="32.5">sens</w> <w n="32.6">refusent</w> <w n="32.7">de</w> <w n="32.8">bannir</w>,</l>
						<l n="33" num="4.3"><w n="33.1">Pour</w> <w n="33.2">te</w> <w n="33.3">mettre</w> <w n="33.4">en</w> <w n="33.5">état</w> <w n="33.6">d</w>’<w n="33.7">entendre</w> <w n="33.8">en</w> <w n="33.9">quelque</w> <w n="33.10">sorte</w></l>
						<l n="34" num="4.4"><w n="34.1">Ce</w> <w n="34.2">dont</w> <w n="34.3">ton</w> <w n="34.4">bien</w>-<w n="34.5">aimé</w> <w n="34.6">te</w> <w n="34.7">veut</w> <w n="34.8">entretenir</w>.</l>
						<l n="35" num="4.5">« <w n="35.1">Je</w> <w n="35.2">suis</w>, <w n="35.3">te</w> <w n="35.4">dira</w>-<w n="35.5">t</w>-<w n="35.6">il</w>, <w n="35.7">ton</w> <w n="35.8">salut</w> <w n="35.9">et</w> <w n="35.10">ta</w> <w n="35.11">vie</w> :</l>
						<l n="36" num="4.6"><w n="36.1">Si</w> <w n="36.2">tu</w> <w n="36.3">peux</w> <w n="36.4">avec</w> <w n="36.5">moi</w> <w n="36.6">demeurer</w> <w n="36.7">bien</w> <w n="36.8">unie</w>,</l>
						<l n="37" num="4.7"><w n="37.1">Le</w> <w n="37.2">vrai</w> <w n="37.3">calme</w> <w n="37.4">avec</w> <w n="37.5">toi</w> <w n="37.6">demeurera</w> <w n="37.7">toujours</w> :</l>
						<l n="38" num="4.8"><w n="38.1">Renonce</w> <w n="38.2">pour</w> <w n="38.3">m</w>’<w n="38.4">aimer</w> <w n="38.5">aux</w> <w n="38.6">douceurs</w> <w n="38.7">temporelles</w> ;</l>
						<l n="39" num="4.9"><space unit="char" quantity="8"></space><w n="39.1">N</w>’<w n="39.2">aspire</w> <w n="39.3">plus</w> <w n="39.4">qu</w>’<w n="39.5">aux</w> <w n="39.6">éternelles</w> ;</l>
						<l n="40" num="4.10"><w n="40.1">Et</w> <w n="40.2">ce</w> <w n="40.3">calme</w> <w n="40.4">naîtra</w> <w n="40.5">de</w> <w n="40.6">nos</w> <w n="40.7">saintes</w> <w n="40.8">amours</w>. »</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1"><w n="41.1">Que</w> <w n="41.2">peuvent</w> <w n="41.3">après</w> <w n="41.4">tout</w> <w n="41.5">ces</w> <w n="41.6">délices</w> <w n="41.7">impures</w>,</l>
						<l n="42" num="5.2"><w n="42.1">Ces</w> <w n="42.2">plaisirs</w> <w n="42.3">passagers</w>, <w n="42.4">que</w> <w n="42.5">séduire</w> <w n="42.6">ton</w> <w n="42.7">cœur</w> ?</l>
						<l n="43" num="5.3"><w n="43.1">De</w> <w n="43.2">quoi</w> <w n="43.3">te</w> <w n="43.4">serviront</w> <w n="43.5">toutes</w> <w n="43.6">les</w> <w n="43.7">créatures</w>,</l>
						<l n="44" num="5.4"><w n="44.1">Si</w> <w n="44.2">tu</w> <w n="44.3">perds</w> <w n="44.4">une</w> <w n="44.5">fois</w> <w n="44.6">l</w>’<w n="44.7">appui</w> <w n="44.8">du</w> <w n="44.9">créateur</w> ?</l>
						<l n="45" num="5.5"><w n="45.1">Défais</w>-<w n="45.2">toi</w>, <w n="45.3">défais</w>-<w n="45.4">toi</w> <w n="45.5">de</w> <w n="45.6">toute</w> <w n="45.7">autre</w> <w n="45.8">habitude</w> ;</l>
						<l n="46" num="5.6"><w n="46.1">À</w> <w n="46.2">ne</w> <w n="46.3">plaire</w> <w n="46.4">qu</w>’<w n="46.5">à</w> <w n="46.6">Dieu</w> <w n="46.7">mets</w> <w n="46.8">toute</w> <w n="46.9">ton</w> <w n="46.10">étude</w> ;</l>
						<l n="47" num="5.7"><w n="47.1">Porte</w>-<w n="47.2">lui</w> <w n="47.3">tous</w> <w n="47.4">tes</w> <w n="47.5">vœux</w> <w n="47.6">avec</w> <w n="47.7">fidélité</w> :</l>
						<l n="48" num="5.8"><w n="48.1">Tu</w> <w n="48.2">trouveras</w> <w n="48.3">ainsi</w> <w n="48.4">la</w> <w n="48.5">véritable</w> <w n="48.6">joie</w>,</l>
						<l n="49" num="5.9"><space unit="char" quantity="8"></space><w n="49.1">Tu</w> <w n="49.2">trouveras</w> <w n="49.3">ainsi</w> <w n="49.4">la</w> <w n="49.5">voie</w></l>
						<l n="50" num="5.10"><w n="50.1">Qui</w> <w n="50.2">seule</w> <w n="50.3">peut</w> <w n="50.4">conduire</w> <w n="50.5">à</w> <w n="50.6">la</w> <w n="50.7">félicité</w>.</l>
					</lg>
				</div></body></text></TEI>