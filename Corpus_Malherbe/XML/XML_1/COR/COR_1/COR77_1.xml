<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR77">
					<head type="main">CHAPITRE VIII</head>
					<head type="sub">Du peu d’estime de soi-même en la présence de Dieu.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Seigneur</w>, <w n="1.2">t</w>’<w n="1.3">oserai</w>-<w n="1.4">je</w> <w n="1.5">parler</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Moi</w> <w n="2.2">qui</w> <w n="2.3">ne</w> <w n="2.4">suis</w> <w n="2.5">que</w> <w n="2.6">cendre</w> <w n="2.7">et</w> <w n="2.8">que</w> <w n="2.9">poussière</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Qu</w>’<w n="3.2">un</w> <w n="3.3">vil</w> <w n="3.4">extrait</w> <w n="3.5">d</w>’<w n="3.6">une</w> <w n="3.7">impure</w> <w n="3.8">matière</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Qu</w>’<w n="4.2">au</w> <w n="4.3">seul</w> <w n="4.4">néant</w> <w n="4.5">on</w> <w n="4.6">a</w> <w n="4.7">droit</w> <w n="4.8">d</w>’<w n="4.9">égaler</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Si</w> <w n="5.2">je</w> <w n="5.3">me</w> <w n="5.4">prise</w> <w n="5.5">davantage</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Je</w> <w n="6.2">t</w>’<w n="6.3">oblige</w> <w n="6.4">à</w> <w n="6.5">t</w>’<w n="6.6">en</w> <w n="6.7">ressentir</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">vois</w> <w n="7.3">tous</w> <w n="7.4">mes</w> <w n="7.5">péchés</w> <w n="7.6">soudain</w> <w n="7.7">me</w> <w n="7.8">démentir</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="4"></space><w n="8.1">Et</w> <w n="8.2">contre</w> <w n="8.3">moi</w> <w n="8.4">porter</w> <w n="8.5">un</w> <w n="8.6">témoignage</w></l>
						<l n="9" num="2.5"><space unit="char" quantity="8"></space><w n="9.1">Où</w> <w n="9.2">je</w> <w n="9.3">n</w>’<w n="9.4">ai</w> <w n="9.5">rien</w> <w n="9.6">à</w> <w n="9.7">repartir</w>.</l>
					</lg>
					<lg n="3">
						<l n="10" num="3.1"><space unit="char" quantity="8"></space><w n="10.1">Mais</w> <w n="10.2">si</w> <w n="10.3">je</w> <w n="10.4">m</w>’<w n="10.5">abaisse</w> <w n="10.6">et</w> <w n="10.7">m</w>’<w n="10.8">obstine</w></l>
						<l n="11" num="3.2"><space unit="char" quantity="4"></space><w n="11.1">À</w> <w n="11.2">me</w> <w n="11.3">réduire</w> <w n="11.4">au</w> <w n="11.5">néant</w> <w n="11.6">dont</w> <w n="11.7">je</w> <w n="11.8">viens</w>,</l>
						<l n="12" num="3.3"><w n="12.1">Si</w> <w n="12.2">toute</w> <w n="12.3">estime</w> <w n="12.4">propre</w> <w n="12.5">en</w> <w n="12.6">moi</w> <w n="12.7">se</w> <w n="12.8">déracine</w>,</l>
						<l n="13" num="3.4"><space unit="char" quantity="4"></space><w n="13.1">Et</w> <w n="13.2">qu</w>’<w n="13.3">en</w> <w n="13.4">dépit</w> <w n="13.5">de</w> <w n="13.6">tous</w> <w n="13.7">ses</w> <w n="13.8">entretiens</w></l>
						<l n="14" num="3.5"><w n="14.1">Je</w> <w n="14.2">rentre</w> <w n="14.3">en</w> <w n="14.4">cette</w> <w n="14.5">poudre</w> <w n="14.6">où</w> <w n="14.7">fut</w> <w n="14.8">mon</w> <w n="14.9">origine</w>,</l>
						<l n="15" num="3.6"><space unit="char" quantity="8"></space><w n="15.1">Ta</w> <w n="15.2">grâce</w> <w n="15.3">avec</w> <w n="15.4">pleine</w> <w n="15.5">vigueur</w></l>
						<l n="16" num="3.7"><space unit="char" quantity="8"></space><w n="16.1">Est</w> <w n="16.2">soudain</w> <w n="16.3">propice</w> <w n="16.4">à</w> <w n="16.5">mon</w> <w n="16.6">âme</w>,</l>
						<l n="17" num="3.8"><space unit="char" quantity="4"></space><w n="17.1">Et</w> <w n="17.2">les</w> <w n="17.3">rayons</w> <w n="17.4">de</w> <w n="17.5">ta</w> <w n="17.6">céleste</w> <w n="17.7">flamme</w></l>
						<l n="18" num="3.9"><space unit="char" quantity="8"></space><w n="18.1">Descendent</w> <w n="18.2">au</w> <w n="18.3">fond</w> <w n="18.4">de</w> <w n="18.5">mon</w> <w n="18.6">cœur</w>.</l>
						<l n="19" num="3.10"><space unit="char" quantity="8"></space><w n="19.1">L</w>’<w n="19.2">orgueil</w>, <w n="19.3">contraint</w> <w n="19.4">à</w> <w n="19.5">disparoître</w>,</l>
						<l n="20" num="3.11"><w n="20.1">Ne</w> <w n="20.2">laisse</w> <w n="20.3">dans</w> <w n="20.4">ce</w> <w n="20.5">cœur</w> <w n="20.6">aucun</w> <w n="20.7">vain</w> <w n="20.8">sentiment</w></l>
						<l n="21" num="3.12"><w n="21.1">Qui</w> <w n="21.2">ne</w> <w n="21.3">soit</w> <w n="21.4">abîmé</w>, <w n="21.5">pour</w> <w n="21.6">petit</w> <w n="21.7">qu</w>’<w n="21.8">il</w> <w n="21.9">puisse</w> <w n="21.10">être</w>,</l>
						<l n="22" num="3.13"><space unit="char" quantity="8"></space><w n="22.1">Dans</w> <w n="22.2">cet</w> <w n="22.3">anéantissement</w>,</l>
						<l n="23" num="3.14"><space unit="char" quantity="8"></space><w n="23.1">Sans</w> <w n="23.2">pouvoir</w> <w n="23.3">jamais</w> <w n="23.4">y</w> <w n="23.5">renaître</w>.</l>
					</lg>
					<lg n="4">
						<l n="24" num="4.1"><space unit="char" quantity="8"></space><w n="24.1">Ta</w> <w n="24.2">clarté</w> <w n="24.3">m</w>’<w n="24.4">expose</w> <w n="24.5">à</w> <w n="24.6">mes</w> <w n="24.7">yeux</w>,</l>
						<l n="25" num="4.2"><w n="25.1">Je</w> <w n="25.2">me</w> <w n="25.3">vois</w> <w n="25.4">tout</w> <w n="25.5">entier</w>, <w n="25.6">et</w> <w n="25.7">j</w>’<w n="25.8">en</w> <w n="25.9">vois</w> <w n="25.10">d</w>’<w n="25.11">autant</w> <w n="25.12">mieux</w></l>
						<l n="26" num="4.3"><w n="26.1">Quels</w> <w n="26.2">défauts</w> <w n="26.3">ont</w> <w n="26.4">suivi</w> <w n="26.5">ma</w> <w n="26.6">honteuse</w> <w n="26.7">naissance</w> :</l>
						<l n="27" num="4.4"><w n="27.1">Je</w> <w n="27.2">vois</w> <w n="27.3">ce</w> <w n="27.4">que</w> <w n="27.5">je</w> <w n="27.6">suis</w>, <w n="27.7">je</w> <w n="27.8">vois</w> <w n="27.9">ce</w> <w n="27.10">que</w> <w n="27.11">je</w> <w n="27.12">fus</w>,</l>
						<l n="28" num="4.5"><space unit="char" quantity="8"></space><w n="28.1">Je</w> <w n="28.2">vois</w> <w n="28.3">d</w>’<w n="28.4">où</w> <w n="28.5">je</w> <w n="28.6">viens</w>, <w n="28.7">et</w> <w n="28.8">confus</w></l>
						<l n="29" num="4.6"><space unit="char" quantity="8"></space><w n="29.1">De</w> <w n="29.2">ne</w> <w n="29.3">voir</w> <w n="29.4">que</w> <w n="29.5">de</w> <w n="29.6">l</w>’<w n="29.7">impuissance</w>,</l>
						<l n="30" num="4.7"><w n="30.1">Je</w> <w n="30.2">m</w>’<w n="30.3">écrie</w> : « <w n="30.4">ô</w> <w n="30.5">mon</w> <w n="30.6">Dieu</w>, <w n="30.7">que</w> <w n="30.8">je</w> <w n="30.9">m</w>’<w n="30.10">étois</w> <w n="30.11">déçu</w> !</l>
						<l n="31" num="4.8"><space unit="char" quantity="4"></space><w n="31.1">Je</w> <w n="31.2">ne</w> <w n="31.3">suis</w> <w n="31.4">rien</w>, <w n="31.5">et</w> <w n="31.6">n</w>’<w n="31.7">en</w> <w n="31.8">avois</w> <w n="31.9">rien</w> <w n="31.10">su</w>. »</l>
					</lg>
					<lg n="5">
						<l n="32" num="5.1"><space unit="char" quantity="8"></space><w n="32.1">Si</w> <w n="32.2">tu</w> <w n="32.3">me</w> <w n="32.4">laisses</w> <w n="32.5">à</w> <w n="32.6">moi</w>-<w n="32.7">même</w>,</l>
						<l n="33" num="5.2"><w n="33.1">Je</w> <w n="33.2">n</w>’<w n="33.3">ai</w> <w n="33.4">dans</w> <w n="33.5">mon</w> <w n="33.6">néant</w> <w n="33.7">que</w> <w n="33.8">foiblesse</w> <w n="33.9">et</w> <w n="33.10">qu</w>’<w n="33.11">effroi</w> ;</l>
						<l n="34" num="5.3"><w n="34.1">Mais</w> <w n="34.2">si</w> <w n="34.3">dans</w> <w n="34.4">mes</w> <w n="34.5">ennuis</w> <w n="34.6">tu</w> <w n="34.7">jettes</w> <w n="34.8">l</w>’<w n="34.9">œil</w> <w n="34.10">sur</w> <w n="34.11">moi</w>,</l>
						<l n="35" num="5.4"><w n="35.1">Soudain</w> <w n="35.2">je</w> <w n="35.3">deviens</w> <w n="35.4">fort</w>, <w n="35.5">et</w> <w n="35.6">ma</w> <w n="35.7">joie</w> <w n="35.8">est</w> <w n="35.9">extrême</w>.</l>
					</lg>
					<lg n="6">
						<l n="36" num="6.1"><space unit="char" quantity="8"></space><w n="36.1">Merveille</w>, <w n="36.2">que</w> <w n="36.3">de</w> <w n="36.4">ces</w> <w n="36.5">bas</w> <w n="36.6">lieux</w>,</l>
						<l n="37" num="6.2"><w n="37.1">Élevé</w> <w n="37.2">tout</w> <w n="37.3">à</w> <w n="37.4">coup</w> <w n="37.5">au</w>-<w n="37.6">dessus</w> <w n="37.7">du</w> <w n="37.8">tonnerre</w>,</l>
						<l n="38" num="6.3"><space unit="char" quantity="8"></space><w n="38.1">Je</w> <w n="38.2">vole</w> <w n="38.3">ainsi</w> <w n="38.4">jusques</w> <w n="38.5">aux</w> <w n="38.6">cieux</w>,</l>
						<l n="39" num="6.4"><w n="39.1">Moi</w> <w n="39.2">que</w> <w n="39.3">mon</w> <w n="39.4">propre</w> <w n="39.5">poids</w> <w n="39.6">rabat</w> <w n="39.7">toujours</w> <w n="39.8">en</w> <w n="39.9">terre</w> !</l>
						<l n="40" num="6.5"><space unit="char" quantity="4"></space><w n="40.1">Que</w> <w n="40.2">tout</w> <w n="40.3">à</w> <w n="40.4">coup</w> <w n="40.5">de</w> <w n="40.6">saints</w> <w n="40.7">élancements</w>,</l>
						<l n="41" num="6.6"><w n="41.1">Tout</w> <w n="41.2">chargé</w> <w n="41.3">que</w> <w n="41.4">je</w> <w n="41.5">suis</w> <w n="41.6">d</w>’<w n="41.7">une</w> <w n="41.8">masse</w> <w n="41.9">grossière</w>,</l>
						<l n="42" num="6.7"><w n="42.1">Jusque</w> <w n="42.2">dans</w> <w n="42.3">ces</w> <w n="42.4">palais</w> <w n="42.5">de</w> <w n="42.6">gloire</w> <w n="42.7">et</w> <w n="42.8">de</w> <w n="42.9">lumière</w></l>
						<l n="43" num="6.8"><w n="43.1">Me</w> <w n="43.2">fassent</w> <w n="43.3">recevoir</w> <w n="43.4">tes</w> <w n="43.5">doux</w> <w n="43.6">embrassements</w> !</l>
					</lg>
					<lg n="7">
						<l n="44" num="7.1"><space unit="char" quantity="8"></space><w n="44.1">Ton</w> <w n="44.2">amour</w> <w n="44.3">fait</w> <w n="44.4">tous</w> <w n="44.5">ces</w> <w n="44.6">miracles</w> :</l>
						<l n="45" num="7.2"><w n="45.1">C</w>’<w n="45.2">est</w> <w n="45.3">lui</w> <w n="45.4">qui</w> <w n="45.5">me</w> <w n="45.6">prévient</w> <w n="45.7">sans</w> <w n="45.8">l</w>’<w n="45.9">avoir</w> <w n="45.10">mérité</w> ;</l>
						<l n="46" num="7.3"><space unit="char" quantity="8"></space><w n="46.1">C</w>’<w n="46.2">est</w> <w n="46.3">lui</w> <w n="46.4">qui</w> <w n="46.5">brise</w> <w n="46.6">les</w> <w n="46.7">obstacles</w></l>
						<l n="47" num="7.4"><w n="47.1">Qui</w> <w n="47.2">naissent</w> <w n="47.3">des</w> <w n="47.4">besoins</w> <w n="47.5">de</w> <w n="47.6">mon</w> <w n="47.7">infirmité</w> ;</l>
						<l n="48" num="7.5"><space unit="char" quantity="8"></space><w n="48.1">C</w>’<w n="48.2">est</w> <w n="48.3">lui</w> <w n="48.4">qui</w> <w n="48.5">soutient</w> <w n="48.6">ma</w> <w n="48.7">foiblesse</w>,</l>
						<l n="49" num="7.6"><space unit="char" quantity="8"></space><w n="49.1">Et</w> <w n="49.2">quelque</w> <w n="49.3">péril</w> <w n="49.4">qui</w> <w n="49.5">me</w> <w n="49.6">presse</w>,</l>
						<l n="50" num="7.7"><w n="50.1">C</w>’<w n="50.2">est</w> <w n="50.3">lui</w> <w n="50.4">qui</w> <w n="50.5">m</w>’<w n="50.6">en</w> <w n="50.7">préserve</w> <w n="50.8">et</w> <w n="50.9">le</w> <w n="50.10">sait</w> <w n="50.11">détourner</w> ;</l>
						<l n="51" num="7.8"><w n="51.1">C</w>’<w n="51.2">est</w> <w n="51.3">lui</w> <w n="51.4">qui</w> <w n="51.5">m</w>’<w n="51.6">affranchit</w>, <w n="51.7">c</w>’<w n="51.8">est</w> <w n="51.9">lui</w> <w n="51.10">qui</w> <w n="51.11">me</w> <w n="51.12">retire</w></l>
						<l n="52" num="7.9"><space unit="char" quantity="8"></space><w n="52.1">De</w> <w n="52.2">tant</w> <w n="52.3">de</w> <w n="52.4">malheurs</w>, <w n="52.5">qu</w>’<w n="52.6">on</w> <w n="52.7">peut</w> <w n="52.8">dire</w></l>
						<l n="53" num="7.10"><w n="53.1">Que</w> <w n="53.2">leur</w> <w n="53.3">nombre</w> <w n="53.4">sans</w> <w n="53.5">lui</w> <w n="53.6">ne</w> <w n="53.7">se</w> <w n="53.8">pourroit</w> <w n="53.9">borner</w>.</l>
					</lg>
					<lg n="8">
						<l n="54" num="8.1"><w n="54.1">Ces</w> <w n="54.2">malheurs</w>, <w n="54.3">ces</w> <w n="54.4">périls</w>, <w n="54.5">ces</w> <w n="54.6">besoins</w>, <w n="54.7">ces</w> <w n="54.8">foiblesses</w>,</l>
						<l n="55" num="8.2"><w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">ce</w> <w n="55.4">que</w> <w n="55.5">l</w>’<w n="55.6">amour</w>-<w n="55.7">propre</w> <w n="55.8">en</w> <w n="55.9">nos</w> <w n="55.10">cœurs</w> <w n="55.11">a</w> <w n="55.12">semé</w>,</l>
						<l n="56" num="8.3"><w n="56.1">C</w>’<w n="56.2">est</w> <w n="56.3">ce</w> <w n="56.4">qu</w>’<w n="56.5">on</w> <w n="56.6">a</w> <w n="56.7">pour</w> <w n="56.8">fruit</w> <w n="56.9">de</w> <w n="56.10">ses</w> <w n="56.11">molles</w> <w n="56.12">tendresses</w>,</l>
						<l n="57" num="8.4"><w n="57.1">Et</w> <w n="57.2">je</w> <w n="57.3">me</w> <w n="57.4">suis</w> <w n="57.5">perdu</w> <w n="57.6">quand</w> <w n="57.7">je</w> <w n="57.8">me</w> <w n="57.9">suis</w> <w n="57.10">aimé</w> ;</l>
						<l n="58" num="8.5"><space unit="char" quantity="8"></space><w n="58.1">Mais</w> <w n="58.2">quand</w> <w n="58.3">détaché</w> <w n="58.4">de</w> <w n="58.5">moi</w>-<w n="58.6">même</w>,</l>
						<l n="59" num="8.6"><w n="59.1">Je</w> <w n="59.2">t</w>’<w n="59.3">aime</w> <w n="59.4">purement</w> <w n="59.5">et</w> <w n="59.6">ne</w> <w n="59.7">cherche</w> <w n="59.8">que</w> <w n="59.9">toi</w>,</l>
						<l n="60" num="8.7"><w n="60.1">Je</w> <w n="60.2">trouve</w> <w n="60.3">ce</w> <w n="60.4">que</w> <w n="60.5">j</w>’<w n="60.6">aime</w> <w n="60.7">en</w> <w n="60.8">un</w> <w n="60.9">si</w> <w n="60.10">digne</w> <w n="60.11">emploi</w>,</l>
						<l n="61" num="8.8"><w n="61.1">Je</w> <w n="61.2">me</w> <w n="61.3">retrouve</w> <w n="61.4">encor</w>, <w n="61.5">seigneur</w>, <w n="61.6">en</w> <w n="61.7">ce</w> <w n="61.8">que</w> <w n="61.9">j</w>’<w n="61.10">aime</w> ;</l>
						<l n="62" num="8.9"><w n="62.1">Et</w> <w n="62.2">ce</w> <w n="62.3">feu</w> <w n="62.4">tout</w> <w n="62.5">divin</w>, <w n="62.6">plus</w> <w n="62.7">il</w> <w n="62.8">sait</w> <w n="62.9">pénétrer</w>,</l>
						<l n="63" num="8.10"><w n="63.1">Plus</w> <w n="63.2">dans</w> <w n="63.3">mon</w> <w n="63.4">vrai</w> <w n="63.5">néant</w> <w n="63.6">il</w> <w n="63.7">m</w>’<w n="63.8">apprend</w> <w n="63.9">à</w> <w n="63.10">rentrer</w>.</l>
					</lg>
					<lg n="9">
						<l n="64" num="9.1"><w n="64.1">Ton</w> <w n="64.2">amour</w> <w n="64.3">à</w> <w n="64.4">t</w>’<w n="64.5">aimer</w> <w n="64.6">ainsi</w> <w n="64.7">me</w> <w n="64.8">sollicite</w>,</l>
						<l n="65" num="9.2"><space unit="char" quantity="8"></space><w n="65.1">Et</w> <w n="65.2">me</w> <w n="65.3">rappelle</w> <w n="65.4">à</w> <w n="65.5">mon</w> <w n="65.6">devoir</w></l>
						<l n="66" num="9.3"><space unit="char" quantity="4"></space><w n="66.1">Par</w> <w n="66.2">des</w> <w n="66.3">faveurs</w> <w n="66.4">qui</w> <w n="66.5">passent</w> <w n="66.6">mon</w> <w n="66.7">mérite</w>,</l>
						<l n="67" num="9.4"><space unit="char" quantity="4"></space><w n="67.1">Et</w> <w n="67.2">par</w> <w n="67.3">des</w> <w n="67.4">biens</w> <w n="67.5">plus</w> <w n="67.6">grands</w> <w n="67.7">que</w> <w n="67.8">mon</w> <w n="67.9">espoir</w>.</l>
					</lg>
					<lg n="10">
						<l n="68" num="10.1"><space unit="char" quantity="8"></space><w n="68.1">Je</w> <w n="68.2">t</w>’<w n="68.3">en</w> <w n="68.4">bénis</w>, <w n="68.5">être</w> <w n="68.6">suprême</w>,</l>
						<l n="69" num="10.2"><space unit="char" quantity="8"></space><w n="69.1">Dont</w> <w n="69.2">l</w>’<w n="69.3">immense</w> <w n="69.4">bénignité</w></l>
						<l n="70" num="10.3"><space unit="char" quantity="8"></space><w n="70.1">Étend</w> <w n="70.2">sa</w> <w n="70.3">libéralité</w></l>
						<l n="71" num="10.4"><space unit="char" quantity="8"></space><w n="71.1">Sur</w> <w n="71.2">l</w>’<w n="71.3">indigne</w> <w n="71.4">et</w> <w n="71.5">sur</w> <w n="71.6">l</w>’<w n="71.7">ingrat</w> <w n="71.8">même</w>.</l>
						<l n="72" num="10.5"><w n="72.1">Ce</w> <w n="72.2">torrent</w> <w n="72.3">que</w> <w n="72.4">jamais</w> <w n="72.5">tu</w> <w n="72.6">ne</w> <w n="72.7">laisses</w> <w n="72.8">tarir</w></l>
						<l n="73" num="10.6"><space unit="char" quantity="8"></space><w n="73.1">Ne</w> <w n="73.2">se</w> <w n="73.3">lasse</w> <w n="73.4">point</w> <w n="73.5">de</w> <w n="73.6">courir</w></l>
						<l n="74" num="10.7"><space unit="char" quantity="8"></space><w n="74.1">Même</w> <w n="74.2">vers</w> <w n="74.3">ceux</w> <w n="74.4">qui</w> <w n="74.5">s</w>’<w n="74.6">en</w> <w n="74.7">éloignent</w> ;</l>
						<l n="75" num="10.8"><space unit="char" quantity="8"></space><w n="75.1">Et</w> <w n="75.2">souvent</w> <w n="75.3">sur</w> <w n="75.4">l</w>’<w n="75.5">aversion</w></l>
						<l n="76" num="10.9"><space unit="char" quantity="8"></space><w n="76.1">Que</w> <w n="76.2">les</w> <w n="76.3">plus</w> <w n="76.4">endurcis</w> <w n="76.5">témoignent</w>,</l>
						<l n="77" num="10.10"><w n="77.1">Il</w> <w n="77.2">roule</w> <w n="77.3">les</w> <w n="77.4">trésors</w> <w n="77.5">de</w> <w n="77.6">ton</w> <w n="77.7">affection</w>.</l>
					</lg>
					<lg n="11">
						<l n="78" num="11.1"><space unit="char" quantity="8"></space><w n="78.1">De</w> <w n="78.2">ces</w> <w n="78.3">sources</w> <w n="78.4">inépuisables</w></l>
						<l n="79" num="11.2"><space unit="char" quantity="8"></space><w n="79.1">Fais</w> <w n="79.2">sur</w> <w n="79.3">nous</w> <w n="79.4">déborder</w> <w n="79.5">les</w> <w n="79.6">flots</w> ;</l>
						<l n="80" num="11.3"><space unit="char" quantity="8"></space><w n="80.1">Rends</w>-<w n="80.2">nous</w> <w n="80.3">humbles</w>, <w n="80.4">rends</w>-<w n="80.5">nous</w> <w n="80.6">dévots</w>,</l>
						<l n="81" num="11.4"><w n="81.1">Rends</w>-<w n="81.2">nous</w> <w n="81.3">reconnoissants</w>, <w n="81.4">rends</w>-<w n="81.5">nous</w> <w n="81.6">inébranlables</w> ;</l>
						<l n="82" num="11.5"><w n="82.1">Relève</w>-<w n="82.2">nous</w> <w n="82.3">le</w> <w n="82.4">cœur</w> <w n="82.5">sous</w> <w n="82.6">nos</w> <w n="82.7">maux</w> <w n="82.8">abattu</w>,</l>
						<l n="83" num="11.6"><w n="83.1">Attire</w>-<w n="83.2">nous</w> <w n="83.3">à</w> <w n="83.4">toi</w> <w n="83.5">par</w> <w n="83.6">cette</w> <w n="83.7">sainte</w> <w n="83.8">amorce</w>,</l>
						<l n="84" num="11.7"><space unit="char" quantity="8"></space><w n="84.1">Toi</w> <w n="84.2">qui</w> <w n="84.3">seul</w> <w n="84.4">es</w> <w n="84.5">notre</w> <w n="84.6">vertu</w>,</l>
						<l n="85" num="11.8"><space unit="char" quantity="8"></space><w n="85.1">Notre</w> <w n="85.2">salut</w> <w n="85.3">et</w> <w n="85.4">notre</w> <w n="85.5">force</w>.</l>
					</lg>
				</div></body></text></TEI>