<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE DEUXIÈME</head><div type="poem" key="COR68">
					<head type="main">CHAPITRE XI</head>
					<head type="sub">Du petit nombre de ceux qui aiment la croix <lb></lb>e Jésus-Christ.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">d</w>’<w n="1.3">hommes</w> <w n="1.4">amoureux</w> <w n="1.5">de</w> <w n="1.6">la</w> <w n="1.7">gloire</w> <w n="1.8">céleste</w></l>
						<l n="2" num="1.2"><w n="2.1">Envisagent</w> <w n="2.2">la</w> <w n="2.3">croix</w> <w n="2.4">comme</w> <w n="2.5">un</w> <w n="2.6">fardeau</w> <w n="2.7">funeste</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">cherchent</w> <w n="3.3">à</w> <w n="3.4">goûter</w> <w n="3.5">les</w> <w n="3.6">consolations</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Sans</w> <w n="4.2">vouloir</w> <w n="4.3">faire</w> <w n="4.4">essai</w> <w n="4.5">des</w> <w n="4.6">tribulations</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Jésus</w>-<w n="5.2">Christ</w> <w n="5.3">voit</w> <w n="5.4">partout</w> <w n="5.5">cette</w> <w n="5.6">humeur</w> <w n="5.7">variable</w> :</l>
						<l n="6" num="1.6"><w n="6.1">Il</w> <w n="6.2">n</w>’<w n="6.3">a</w> <w n="6.4">que</w> <w n="6.5">trop</w> <w n="6.6">d</w>’<w n="6.7">amis</w> <w n="6.8">pour</w> <w n="6.9">se</w> <w n="6.10">seoir</w> <w n="6.11">à</w> <w n="6.12">sa</w> <w n="6.13">table</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Aucun</w> <w n="7.2">dans</w> <w n="7.3">le</w> <w n="7.4">banquet</w> <w n="7.5">ne</w> <w n="7.6">veut</w> <w n="7.7">l</w>’<w n="7.8">abandonner</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">Mais</w> <w n="8.2">au</w> <w n="8.3">fond</w> <w n="8.4">du</w> <w n="8.5">désert</w> <w n="8.6">il</w> <w n="8.7">est</w> <w n="8.8">seul</w> <w n="8.9">à</w> <w n="8.10">jeûner</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Tous</w> <w n="9.2">lui</w> <w n="9.3">demandent</w> <w n="9.4">part</w> <w n="9.5">à</w> <w n="9.6">sa</w> <w n="9.7">pleine</w> <w n="9.8">allégresse</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Mais</w> <w n="10.2">aucun</w> <w n="10.3">n</w>’<w n="10.4">en</w> <w n="10.5">veut</w> <w n="10.6">prendre</w> <w n="10.7">à</w> <w n="10.8">sa</w> <w n="10.9">pleine</w> <w n="10.10">tristesse</w> ;</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">ceux</w> <w n="11.3">que</w> <w n="11.4">l</w>’<w n="11.5">on</w> <w n="11.6">a</w> <w n="11.7">vus</w> <w n="11.8">les</w> <w n="11.9">plus</w> <w n="11.10">prompts</w> <w n="11.11">à</w> <w n="11.12">s</w>’<w n="11.13">offrir</w></l>
						<l n="12" num="1.12"><w n="12.1">Le</w> <w n="12.2">quittent</w> <w n="12.3">les</w> <w n="12.4">premiers</w> <w n="12.5">quand</w> <w n="12.6">il</w> <w n="12.7">lui</w> <w n="12.8">faut</w> <w n="12.9">souffrir</w>.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><w n="13.1">Jusqu</w>’<w n="13.2">à</w> <w n="13.3">la</w> <w n="13.4">fraction</w> <w n="13.5">de</w> <w n="13.6">ce</w> <w n="13.7">pain</w> <w n="13.8">qu</w>’<w n="13.9">il</w> <w n="13.10">nous</w> <w n="13.11">donne</w>,</l>
						<l n="14" num="2.2"><w n="14.1">Assez</w> <w n="14.2">de</w> <w n="14.3">monde</w> <w n="14.4">ici</w> <w n="14.5">le</w> <w n="14.6">suit</w> <w n="14.7">et</w> <w n="14.8">l</w>’<w n="14.9">environne</w> ;</l>
						<l n="15" num="2.3"><w n="15.1">Mais</w> <w n="15.2">peu</w> <w n="15.3">de</w> <w n="15.4">son</w> <w n="15.5">amour</w> <w n="15.6">s</w>’<w n="15.7">y</w> <w n="15.8">laissent</w> <w n="15.9">enflammer</w></l>
						<l n="16" num="2.4"><w n="16.1">Jusqu</w>’<w n="16.2">à</w> <w n="16.3">boire</w> <w n="16.4">avec</w> <w n="16.5">lui</w> <w n="16.6">dans</w> <w n="16.7">le</w> <w n="16.8">calice</w> <w n="16.9">amer</w>.</l>
						<l n="17" num="2.5"><w n="17.1">Les</w> <w n="17.2">miracles</w> <w n="17.3">brillants</w> <w n="17.4">dont</w> <w n="17.5">il</w> <w n="17.6">sème</w> <w n="17.7">sa</w> <w n="17.8">vie</w></l>
						<l n="18" num="2.6"><w n="18.1">Par</w> <w n="18.2">leur</w> <w n="18.3">éclat</w> <w n="18.4">à</w> <w n="18.5">peine</w> <w n="18.6">échauffent</w> <w n="18.7">notre</w> <w n="18.8">envie</w>,</l>
						<l n="19" num="2.7"><w n="19.1">Que</w> <w n="19.2">sa</w> <w n="19.3">honteuse</w> <w n="19.4">mort</w> <w n="19.5">refroidit</w> <w n="19.6">nos</w> <w n="19.7">esprits</w></l>
						<l n="20" num="2.8"><w n="20.1">Jusqu</w>’<w n="20.2">à</w> <w n="20.3">ne</w> <w n="20.4">vouloir</w> <w n="20.5">plus</w> <w n="20.6">de</w> <w n="20.7">ce</w> <w n="20.8">don</w> <w n="20.9">à</w> <w n="20.10">ce</w> <w n="20.11">prix</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Beaucoup</w> <w n="21.2">avec</w> <w n="21.3">chaleur</w> <w n="21.4">l</w>’<w n="21.5">aiment</w> <w n="21.6">et</w> <w n="21.7">le</w> <w n="21.8">bénissent</w></l>
						<l n="22" num="3.2"><w n="22.1">Dont</w>, <w n="22.2">au</w> <w n="22.3">premier</w> <w n="22.4">revers</w>, <w n="22.5">les</w> <w n="22.6">louanges</w> <w n="22.7">tarissent</w>.</l>
						<l n="23" num="3.3"><w n="23.1">Tant</w> <w n="23.2">qu</w>’<w n="23.3">ils</w> <w n="23.4">n</w>’<w n="23.5">ont</w> <w n="23.6">à</w> <w n="23.7">gémir</w> <w n="23.8">d</w>’<w n="23.9">aucune</w> <w n="23.10">adversité</w>,</l>
						<l n="24" num="3.4"><w n="24.1">Qu</w>’<w n="24.2">il</w> <w n="24.3">n</w>’<w n="24.4">épanche</w> <w n="24.5">sur</w> <w n="24.6">eux</w> <w n="24.7">que</w> <w n="24.8">sa</w> <w n="24.9">bénignité</w>,</l>
						<l n="25" num="3.5"><w n="25.1">Cette</w> <w n="25.2">faveur</w> <w n="25.3">sensible</w> <w n="25.4">aisément</w> <w n="25.5">sert</w> <w n="25.6">d</w>’<w n="25.7">amorce</w></l>
						<l n="26" num="3.6"><w n="26.1">À</w> <w n="26.2">soutenir</w> <w n="26.3">leur</w> <w n="26.4">zèle</w> <w n="26.5">et</w> <w n="26.6">conserver</w> <w n="26.7">leur</w> <w n="26.8">force</w> ;</l>
						<l n="27" num="3.7"><w n="27.1">Mais</w> <w n="27.2">lorsque</w> <w n="27.3">sa</w> <w n="27.4">bonté</w> <w n="27.5">se</w> <w n="27.6">cache</w> <w n="27.7">tant</w> <w n="27.8">soit</w> <w n="27.9">peu</w>,</l>
						<l n="28" num="3.8"><w n="28.1">Une</w> <w n="28.2">soudaine</w> <w n="28.3">glace</w> <w n="28.4">amortit</w> <w n="28.5">tout</w> <w n="28.6">ce</w> <w n="28.7">feu</w>,</l>
						<l n="29" num="3.9"><w n="29.1">Et</w> <w n="29.2">les</w> <w n="29.3">restes</w> <w n="29.4">fumants</w> <w n="29.5">de</w> <w n="29.6">leur</w> <w n="29.7">ferveur</w> <w n="29.8">éteinte</w></l>
						<l n="30" num="3.10"><w n="30.1">Ne</w> <w n="30.2">font</w> <w n="30.3">partir</w> <w n="30.4">du</w> <w n="30.5">cœur</w> <w n="30.6">que</w> <w n="30.7">murmure</w> <w n="30.8">et</w> <w n="30.9">que</w> <w n="30.10">plainte</w>,</l>
						<l n="31" num="3.11"><w n="31.1">Tandis</w> <w n="31.2">qu</w>’<w n="31.3">au</w> <w n="31.4">fond</w> <w n="31.5">de</w> <w n="31.6">l</w>’<w n="31.7">âme</w> <w n="31.8">un</w> <w n="31.9">lâche</w> <w n="31.10">étonnement</w></l>
						<l n="32" num="3.12"><w n="32.1">Va</w> <w n="32.2">de</w> <w n="32.3">la</w> <w n="32.4">fermeté</w> <w n="32.5">jusqu</w>’<w n="32.6">à</w> <w n="32.7">l</w>’<w n="32.8">abattement</w>.</l>
					</lg>
					<lg n="4">
						<l n="33" num="4.1"><w n="33.1">En</w> <w n="33.2">usez</w>-<w n="33.3">vous</w> <w n="33.4">ainsi</w>, <w n="33.5">vous</w> <w n="33.6">dont</w> <w n="33.7">l</w>’<w n="33.8">amour</w> <w n="33.9">extrême</w></l>
						<l n="34" num="4.2"><w n="34.1">N</w>’<w n="34.2">embrasse</w> <w n="34.3">Jésus</w>-<w n="34.4">Christ</w> <w n="34.5">qu</w>’<w n="34.6">à</w> <w n="34.7">cause</w> <w n="34.8">de</w> <w n="34.9">lui</w>-<w n="34.10">même</w>,</l>
						<l n="35" num="4.3"><w n="35.1">Et</w> <w n="35.2">qui</w> <w n="35.3">sans</w> <w n="35.4">regarder</w> <w n="35.5">votre</w> <w n="35.6">propre</w> <w n="35.7">intérêt</w>,</l>
						<l n="36" num="4.4"><w n="36.1">N</w>’<w n="36.2">avez</w> <w n="36.3">de</w> <w n="36.4">passion</w> <w n="36.5">que</w> <w n="36.6">pour</w> <w n="36.7">ce</w> <w n="36.8">qui</w> <w n="36.9">lui</w> <w n="36.10">plaît</w> ?</l>
						<l n="37" num="4.5"><w n="37.1">Vous</w> <w n="37.2">voyez</w> <w n="37.3">d</w>’<w n="37.4">un</w> <w n="37.5">même</w> <w n="37.6">œil</w> <w n="37.7">tout</w> <w n="37.8">ce</w> <w n="37.9">qu</w>’<w n="37.10">il</w> <w n="37.11">vous</w> <w n="37.12">envoie</w> :</l>
						<l n="38" num="4.6"><w n="38.1">Vous</w> <w n="38.2">l</w>’<w n="38.3">aimez</w> <w n="38.4">dans</w> <w n="38.5">l</w>’<w n="38.6">angoisse</w> <w n="38.7">ainsi</w> <w n="38.8">que</w> <w n="38.9">dans</w> <w n="38.10">la</w> <w n="38.11">joie</w> ;</l>
						<l n="39" num="4.7"><w n="39.1">Vous</w> <w n="39.2">le</w> <w n="39.3">savez</w> <w n="39.4">bénir</w> <w n="39.5">dans</w> <w n="39.6">la</w> <w n="39.7">prospérité</w>,</l>
						<l n="40" num="4.8"><w n="40.1">Vous</w> <w n="40.2">le</w> <w n="40.3">savez</w> <w n="40.4">louer</w> <w n="40.5">dans</w> <w n="40.6">la</w> <w n="40.7">calamité</w> ;</l>
						<l n="41" num="4.9"><w n="41.1">Une</w> <w n="41.2">égale</w> <w n="41.3">constance</w> <w n="41.4">attachée</w> <w n="41.5">à</w> <w n="41.6">ses</w> <w n="41.7">traces</w></l>
						<l n="42" num="4.10"><w n="42.1">Dans</w> <w n="42.2">l</w>’<w n="42.3">un</w> <w n="42.4">et</w> <w n="42.5">l</w>’<w n="42.6">autre</w> <w n="42.7">sort</w> <w n="42.8">trouve</w> <w n="42.9">à</w> <w n="42.10">lui</w> <w n="42.11">rendre</w> <w n="42.12">grâces</w> ;</l>
						<l n="43" num="4.11"><w n="43.1">Et</w> <w n="43.2">quand</w> <w n="43.3">jamais</w> <w n="43.4">pour</w> <w n="43.5">vous</w> <w n="43.6">il</w> <w n="43.7">n</w>’<w n="43.8">auroit</w> <w n="43.9">que</w> <w n="43.10">rigueurs</w>,</l>
						<l n="44" num="4.12"><w n="44.1">Mêmes</w> <w n="44.2">remercîments</w> <w n="44.3">partiroient</w> <w n="44.4">de</w> <w n="44.5">vos</w> <w n="44.6">cœurs</w>.</l>
					</lg>
					<lg n="5">
						<l n="45" num="5.1"><w n="45.1">Pur</w> <w n="45.2">amour</w> <w n="45.3">de</w> <w n="45.4">Jésus</w>, <w n="45.5">que</w> <w n="45.6">ta</w> <w n="45.7">force</w> <w n="45.8">est</w> <w n="45.9">étrange</w>,</l>
						<l n="46" num="5.2"><w n="46.1">Quand</w> <w n="46.2">l</w>’<w n="46.3">amour</w>-<w n="46.4">propre</w> <w n="46.5">en</w> <w n="46.6">toi</w> <w n="46.7">ne</w> <w n="46.8">fait</w> <w n="46.9">aucun</w> <w n="46.10">mélange</w>,</l>
						<l n="47" num="5.3"><w n="47.1">Et</w> <w n="47.2">que</w> <w n="47.3">de</w> <w n="47.4">l</w>’<w n="47.5">intérêt</w> <w n="47.6">pleinement</w> <w n="47.7">dépouillé</w></l>
						<l n="48" num="5.4"><w n="48.1">D</w>’<w n="48.2">aucun</w> <w n="48.3">regard</w> <w n="48.4">vers</w> <w n="48.5">nous</w> <w n="48.6">tu</w> <w n="48.7">ne</w> <w n="48.8">te</w> <w n="48.9">vois</w> <w n="48.10">souillé</w> !</l>
						<l n="49" num="5.5"><w n="49.1">N</w>’<w n="49.2">ont</w>-<w n="49.3">ils</w> <w n="49.4">pas</w> <w n="49.5">un</w> <w n="49.6">amour</w> <w n="49.7">servile</w> <w n="49.8">et</w> <w n="49.9">mercenaire</w>,</l>
						<l n="50" num="5.6"><w n="50.1">Ces</w> <w n="50.2">cœurs</w> <w n="50.3">qui</w> <w n="50.4">n</w>’<w n="50.5">aiment</w> <w n="50.6">Dieu</w> <w n="50.7">que</w> <w n="50.8">pour</w> <w n="50.9">se</w> <w n="50.10">satisfaire</w>,</l>
						<l n="51" num="5.7"><w n="51.1">Et</w> <w n="51.2">ne</w> <w n="51.3">le</w> <w n="51.4">font</w> <w n="51.5">l</w>’<w n="51.6">objet</w> <w n="51.7">de</w> <w n="51.8">leurs</w> <w n="51.9">affections</w></l>
						<l n="52" num="5.8"><w n="52.1">Que</w> <w n="52.2">pour</w> <w n="52.3">en</w> <w n="52.4">recevoir</w> <w n="52.5">des</w> <w n="52.6">consolations</w> ?</l>
					</lg>
					<lg n="6">
						<l n="53" num="6.1"><w n="53.1">Aimer</w> <w n="53.2">Dieu</w> <w n="53.3">de</w> <w n="53.4">la</w> <w n="53.5">sorte</w> <w n="53.6">et</w> <w n="53.7">pour</w> <w n="53.8">nos</w> <w n="53.9">avantages</w>,</l>
						<l n="54" num="6.2"><w n="54.1">C</w>’<w n="54.2">est</w> <w n="54.3">mettre</w> <w n="54.4">indignement</w> <w n="54.5">ses</w> <w n="54.6">bontés</w> <w n="54.7">à</w> <w n="54.8">nos</w> <w n="54.9">gages</w>,</l>
						<l n="55" num="6.3"><w n="55.1">Croire</w> <w n="55.2">d</w>’<w n="55.3">un</w> <w n="55.4">peu</w> <w n="55.5">de</w> <w n="55.6">vœux</w> <w n="55.7">payer</w> <w n="55.8">tout</w> <w n="55.9">son</w> <w n="55.10">appui</w>,</l>
						<l n="56" num="6.4"><w n="56.1">Et</w> <w n="56.2">nous</w>-<w n="56.3">mêmes</w> <w n="56.4">enfin</w> <w n="56.5">nous</w> <w n="56.6">aimer</w> <w n="56.7">plus</w> <w n="56.8">que</w> <w n="56.9">lui</w> ;</l>
						<l n="57" num="6.5"><w n="57.1">Mais</w> <w n="57.2">où</w> <w n="57.3">trouvera</w>-<w n="57.4">t</w>-<w n="57.5">on</w> <w n="57.6">une</w> <w n="57.7">âme</w> <w n="57.8">si</w> <w n="57.9">purgée</w>,</l>
						<l n="58" num="6.6"><w n="58.1">D</w>’<w n="58.2">espoir</w> <w n="58.3">de</w> <w n="58.4">tout</w> <w n="58.5">salaire</w> <w n="58.6">à</w> <w n="58.7">ce</w> <w n="58.8">point</w> <w n="58.9">dégagée</w>,</l>
						<l n="59" num="6.7"><w n="59.1">Qu</w>’<w n="59.2">elle</w> <w n="59.3">aime</w> <w n="59.4">à</w> <w n="59.5">servir</w> <w n="59.6">Dieu</w> <w n="59.7">sans</w> <w n="59.8">se</w> <w n="59.9">considérer</w>,</l>
						<l n="60" num="6.8"><w n="60.1">Et</w> <w n="60.2">ne</w> <w n="60.3">cherche</w> <w n="60.4">en</w> <w n="60.5">l</w>’<w n="60.6">aimant</w> <w n="60.7">que</w> <w n="60.8">l</w>’<w n="60.9">heur</w> <w n="60.10">de</w> <w n="60.11">l</w>’<w n="60.12">adorer</w> ?</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1"><w n="61.1">Certes</w> <w n="61.2">il</w> <w n="61.3">s</w>’<w n="61.4">en</w> <w n="61.5">voit</w> <w n="61.6">peu</w> <w n="61.7">de</w> <w n="61.8">qui</w> <w n="61.9">l</w>’<w n="61.10">amour</w> <w n="61.11">soit</w> <w n="61.12">pure</w></l>
						<l n="62" num="7.2"><w n="62.1">Jusqu</w>’<w n="62.2">à</w> <w n="62.3">se</w> <w n="62.4">dépouiller</w> <w n="62.5">de</w> <w n="62.6">toute</w> <w n="62.7">créature</w> ;</l>
						<l n="63" num="7.3"><w n="63.1">Et</w> <w n="63.2">s</w>’<w n="63.3">il</w> <w n="63.4">est</w> <w n="63.5">sur</w> <w n="63.6">la</w> <w n="63.7">terre</w> <w n="63.8">un</w> <w n="63.9">vrai</w> <w n="63.10">pauvre</w> <w n="63.11">d</w>’<w n="63.12">esprit</w>,</l>
						<l n="64" num="7.4"><w n="64.1">Qui</w> <w n="64.2">détaché</w> <w n="64.3">de</w> <w n="64.4">tout</w>, <w n="64.5">soit</w> <w n="64.6">tout</w> <w n="64.7">à</w> <w n="64.8">Jésus</w>-<w n="64.9">Christ</w>,</l>
						<l n="65" num="7.5"><w n="65.1">C</w>’<w n="65.2">est</w> <w n="65.3">un</w> <w n="65.4">trésor</w> <w n="65.5">si</w> <w n="65.6">grand</w>, <w n="65.7">que</w> <w n="65.8">ces</w> <w n="65.9">mines</w> <w n="65.10">fécondes</w></l>
						<l n="66" num="7.6"><w n="66.1">Que</w> <w n="66.2">la</w> <w n="66.3">nature</w> <w n="66.4">écarte</w> <w n="66.5">au</w> <w n="66.6">bout</w> <w n="66.7">des</w> <w n="66.8">nouveaux</w> <w n="66.9">mondes</w>,</l>
						<l n="67" num="7.7"><w n="67.1">Ces</w> <w n="67.2">mers</w> <w n="67.3">où</w> <w n="67.4">se</w> <w n="67.5">durcit</w> <w n="67.6">la</w> <w n="67.7">perle</w> <w n="67.8">et</w> <w n="67.9">le</w> <w n="67.10">coral</w>,</l>
						<l n="68" num="7.8"><w n="68.1">N</w>’<w n="68.2">en</w> <w n="68.3">ont</w> <w n="68.4">jamais</w> <w n="68.5">conçu</w> <w n="68.6">qui</w> <w n="68.7">fût</w> <w n="68.8">d</w>’<w n="68.9">un</w> <w n="68.10">prix</w> <w n="68.11">égal</w>.</l>
					</lg>
					<lg n="8">
						<l n="69" num="8.1"><w n="69.1">Mais</w> <w n="69.2">aussi</w> <w n="69.3">ce</w> <w n="69.4">n</w>’<w n="69.5">est</w> <w n="69.6">pas</w> <w n="69.7">une</w> <w n="69.8">conquête</w> <w n="69.9">aisée</w></l>
						<l n="70" num="8.2"><w n="70.1">Qu</w>’<w n="70.2">à</w> <w n="70.3">ses</w> <w n="70.4">premiers</w> <w n="70.5">desirs</w> <w n="70.6">l</w>’<w n="70.7">homme</w> <w n="70.8">trouve</w> <w n="70.9">exposée</w> :</l>
						<l n="71" num="8.3"><w n="71.1">Quand</w> <w n="71.2">pour</w> <w n="71.3">y</w> <w n="71.4">parvenir</w> <w n="71.5">il</w> <w n="71.6">donne</w> <w n="71.7">tout</w> <w n="71.8">son</w> <w n="71.9">bien</w>,</l>
						<l n="72" num="8.4"><w n="72.1">Avec</w> <w n="72.2">ce</w> <w n="72.3">grand</w> <w n="72.4">effort</w> <w n="72.5">il</w> <w n="72.6">ne</w> <w n="72.7">fait</w> <w n="72.8">encor</w> <w n="72.9">rien</w> ;</l>
						<l n="73" num="8.5"><w n="73.1">Quelque</w> <w n="73.2">âpre</w> <w n="73.3">pénitence</w> <w n="73.4">ici</w>-<w n="73.5">bas</w> <w n="73.6">qu</w>’<w n="73.7">il</w> <w n="73.8">s</w>’<w n="73.9">impose</w>,</l>
						<l n="74" num="8.6"><w n="74.1">Ses</w> <w n="74.2">plus</w> <w n="74.3">longues</w> <w n="74.4">rigueurs</w> <w n="74.5">sont</w> <w n="74.6">encor</w> <w n="74.7">peu</w> <w n="74.8">de</w> <w n="74.9">chose</w> ;</l>
						<l n="75" num="8.7"><w n="75.1">Que</w> <w n="75.2">sur</w> <w n="75.3">chaque</w> <w n="75.4">science</w> <w n="75.5">il</w> <w n="75.6">applique</w> <w n="75.7">son</w> <w n="75.8">soin</w>,</l>
						<l n="76" num="8.8"><w n="76.1">Qu</w>’<w n="76.2">il</w> <w n="76.3">la</w> <w n="76.4">possède</w> <w n="76.5">entière</w>, <w n="76.6">il</w> <w n="76.7">est</w> <w n="76.8">encor</w> <w n="76.9">bien</w> <w n="76.10">loin</w> ;</l>
						<l n="77" num="8.9"><w n="77.1">Qu</w>’<w n="77.2">il</w> <w n="77.3">ait</w> <w n="77.4">mille</w> <w n="77.5">vertus</w> <w n="77.6">dont</w> <w n="77.7">l</w>’<w n="77.8">heureux</w> <w n="77.9">assemblage</w></l>
						<l n="78" num="8.10"><w n="78.1">De</w> <w n="78.2">tous</w> <w n="78.3">leurs</w> <w n="78.4">ornements</w> <w n="78.5">pare</w> <w n="78.6">son</w> <w n="78.7">grand</w> <w n="78.8">courage</w> ;</l>
						<l n="79" num="8.11"><w n="79.1">Que</w> <w n="79.2">sa</w> <w n="79.3">dévotion</w>, <w n="79.4">que</w> <w n="79.5">ses</w> <w n="79.6">hautes</w> <w n="79.7">ferveurs</w></l>
						<l n="80" num="8.12"><w n="80.1">Attirent</w> <w n="80.2">chaque</w> <w n="80.3">jour</w> <w n="80.4">de</w> <w n="80.5">nouvelles</w> <w n="80.6">faveurs</w> :</l>
						<l n="81" num="8.13"><w n="81.1">Sache</w> <w n="81.2">qu</w>’<w n="81.3">il</w> <w n="81.4">lui</w> <w n="81.5">demeure</w> <w n="81.6">encor</w> <w n="81.7">beaucoup</w> <w n="81.8">à</w> <w n="81.9">faire</w>,</l>
						<l n="82" num="8.14"><w n="82.1">S</w>’<w n="82.2">il</w> <w n="82.3">manque</w> <w n="82.4">à</w> <w n="82.5">ce</w> <w n="82.6">point</w> <w n="82.7">seul</w>, <w n="82.8">qui</w> <w n="82.9">seul</w> <w n="82.10">est</w> <w n="82.11">nécessaire</w>.</l>
						<l n="83" num="8.15"><w n="83.1">Tu</w> <w n="83.2">sais</w> <w n="83.3">quel</w> <w n="83.4">est</w> <w n="83.5">ce</w> <w n="83.6">point</w>, <w n="83.7">je</w> <w n="83.8">l</w>’<w n="83.9">ai</w> <w n="83.10">trop</w> <w n="83.11">répété</w> :</l>
						<l n="84" num="8.16"><w n="84.1">C</w>’<w n="84.2">est</w> <w n="84.3">qu</w>’<w n="84.4">il</w> <w n="84.5">se</w> <w n="84.6">quitte</w> <w n="84.7">encor</w>, <w n="84.8">quand</w> <w n="84.9">il</w> <w n="84.10">a</w> <w n="84.11">tout</w> <w n="84.12">quitté</w>,</l>
						<l n="85" num="8.17"><w n="85.1">Que</w> <w n="85.2">de</w> <w n="85.3">tout</w> <w n="85.4">l</w>’<w n="85.5">amour</w>-<w n="85.6">propre</w> <w n="85.7">il</w> <w n="85.8">fasse</w> <w n="85.9">un</w> <w n="85.10">sacrifice</w>,</l>
						<l n="86" num="8.18"><w n="86.1">Que</w> <w n="86.2">de</w> <w n="86.3">lui</w>-<w n="86.4">même</w> <w n="86.5">enfin</w> <w n="86.6">lui</w>-<w n="86.7">même</w> <w n="86.8">il</w> <w n="86.9">se</w> <w n="86.10">bannisse</w>,</l>
						<l n="87" num="8.19"><w n="87.1">Et</w> <w n="87.2">qu</w>’<w n="87.3">élevé</w> <w n="87.4">par</w> <w n="87.5">là</w> <w n="87.6">dans</w> <w n="87.7">un</w> <w n="87.8">état</w> <w n="87.9">parfait</w>,</l>
						<l n="88" num="8.20"><w n="88.1">Il</w> <w n="88.2">croie</w>, <w n="88.3">ayant</w> <w n="88.4">fait</w> <w n="88.5">tout</w>, <w n="88.6">n</w>’<w n="88.7">avoir</w> <w n="88.8">encor</w> <w n="88.9">rien</w> <w n="88.10">fait</w>.</l>
					</lg>
					<lg n="9">
						<l n="89" num="9.1"><w n="89.1">Qu</w>’<w n="89.2">il</w> <w n="89.3">estime</w> <w n="89.4">fort</w> <w n="89.5">peu</w>, <w n="89.6">suivant</w> <w n="89.7">cette</w> <w n="89.8">maxime</w>,</l>
						<l n="90" num="9.2"><w n="90.1">Tout</w> <w n="90.2">ce</w> <w n="90.3">qui</w> <w n="90.4">peut</w> <w n="90.5">en</w> <w n="90.6">lui</w> <w n="90.7">mériter</w> <w n="90.8">quelque</w> <w n="90.9">estime</w> ;</l>
						<l n="91" num="9.3"><w n="91.1">Que</w> <w n="91.2">lui</w>-<w n="91.3">même</w> <w n="91.4">il</w> <w n="91.5">se</w> <w n="91.6">die</w>, <w n="91.7">et</w> <w n="91.8">du</w> <w n="91.9">fond</w> <w n="91.10">de</w> <w n="91.11">son</w> <w n="91.12">cœur</w>,</l>
						<l n="92" num="9.4"><w n="92.1">Serviteur</w> <w n="92.2">inutile</w> <w n="92.3">aux</w> <w n="92.4">emplois</w> <w n="92.5">du</w> <w n="92.6">seigneur</w>.</l>
						<l n="93" num="9.5"><w n="93.1">La</w> <w n="93.2">vérité</w> <w n="93.3">l</w>’<w n="93.4">ordonne</w> : « <w n="93.5">après</w> <w n="93.6">avoir</w>, <w n="93.7">dit</w>-<w n="93.8">elle</w>,</l>
						<l n="94" num="9.6"><w n="94.1">Rempli</w> <w n="94.2">tous</w> <w n="94.3">les</w> <w n="94.4">devoirs</w> <w n="94.5">où</w> <w n="94.6">ma</w> <w n="94.7">voix</w> <w n="94.8">vous</w> <w n="94.9">appelle</w>,</l>
						<l n="95" num="9.7"><w n="95.1">Après</w> <w n="95.2">avoir</w> <w n="95.3">fait</w> <w n="95.4">tout</w> <w n="95.5">ce</w> <w n="95.6">que</w> <w n="95.7">je</w> <w n="95.8">vous</w> <w n="95.9">prescris</w>,</l>
						<l n="96" num="9.8"><w n="96.1">Gardez</w> <w n="96.2">encor</w> <w n="96.3">pour</w> <w n="96.4">vous</w> <w n="96.5">un</w> <w n="96.6">sincère</w> <w n="96.7">mépris</w>,</l>
						<l n="97" num="9.9"><w n="97.1">Et</w> <w n="97.2">nommez</w>-<w n="97.3">vous</w> <w n="97.4">encor</w> <w n="97.5">disciples</w> <w n="97.6">indociles</w>,</l>
						<l n="98" num="9.10"><w n="98.1">Serviteurs</w> <w n="98.2">fainéants</w>, <w n="98.3">esclaves</w> <w n="98.4">inutiles</w>. »</l>
						<l n="99" num="9.11"><w n="99.1">Ainsi</w> <w n="99.2">vraiment</w> <w n="99.3">tout</w> <w n="99.4">nu</w>, <w n="99.5">vraiment</w> <w n="99.6">pauvre</w> <w n="99.7">d</w>’<w n="99.8">esprit</w>,</l>
						<l n="100" num="9.12"><w n="100.1">Tout</w> <w n="100.2">détaché</w> <w n="100.3">de</w> <w n="100.4">tout</w>, <w n="100.5">et</w> <w n="100.6">tout</w> <w n="100.7">à</w> <w n="100.8">Jésus</w>-<w n="100.9">Christ</w>,</l>
						<l n="101" num="9.13"><w n="101.1">Avec</w> <w n="101.2">le</w> <w n="101.3">roi</w> <w n="101.4">prophète</w> <w n="101.5">il</w> <w n="101.6">aura</w> <w n="101.7">lieu</w> <w n="101.8">de</w> <w n="101.9">dire</w> :</l>
						<l n="102" num="9.14">« <w n="102.1">Je</w> <w n="102.2">n</w>’<w n="102.3">ai</w> <w n="102.4">plus</w> <w n="102.5">rien</w> <w n="102.6">en</w> <w n="102.7">moi</w> <w n="102.8">que</w> <w n="102.9">ce</w> <w n="102.10">que</w> <w n="102.11">Dieu</w> <w n="102.12">m</w>’<w n="102.13">inspire</w> ;</l>
						<l n="103" num="9.15"><w n="103.1">J</w>’<w n="103.2">y</w> <w n="103.3">suis</w> <w n="103.4">seul</w>, <w n="103.5">j</w>’<w n="103.6">y</w> <w n="103.7">suis</w> <w n="103.8">pauvre</w>. « <w n="103.9">aucun</w> <w n="103.10">n</w>’<w n="103.11">est</w> <w n="103.12">toutefois</w></l>
						<l n="104" num="9.16"><w n="104.1">Ni</w> <w n="104.2">plus</w> <w n="104.3">riche</w> <w n="104.4">en</w> <w n="104.5">vrais</w> <w n="104.6">biens</w>, <w n="104.7">ni</w> <w n="104.8">plus</w> <w n="104.9">libre</w> <w n="104.10">en</w> <w n="104.11">son</w> <w n="104.12">choix</w>,</l>
						<l n="105" num="9.17"><w n="105.1">Ni</w> <w n="105.2">plus</w> <w n="105.3">puissant</w> <w n="105.4">enfin</w> <w n="105.5">que</w> <w n="105.6">ce</w> <w n="105.7">chétif</w> <w n="105.8">esclave</w></l>
						<l n="106" num="9.18"><w n="106.1">Qui</w> <w n="106.2">foulant</w> <w n="106.3">tout</w> <w n="106.4">aux</w> <w n="106.5">pieds</w>, <w n="106.6">lui</w>-<w n="106.7">même</w> <w n="106.8">encor</w> <w n="106.9">se</w> <w n="106.10">brave</w>,</l>
						<l n="107" num="9.19"><w n="107.1">Et</w> <w n="107.2">rompant</w> <w n="107.3">avec</w> <w n="107.4">soi</w> <w n="107.5">pour</w> <w n="107.6">s</w>’<w n="107.7">unir</w> <w n="107.8">à</w> <w n="107.9">son</w> <w n="107.10">Dieu</w>,</l>
						<l n="108" num="9.20"><w n="108.1">Sait</w> <w n="108.2">en</w> <w n="108.3">tout</w> <w n="108.4">et</w> <w n="108.5">partout</w> <w n="108.6">se</w> <w n="108.7">mettre</w> <w n="108.8">au</w> <w n="108.9">plus</w> <w n="108.10">bas</w> <w n="108.11">lieu</w>.</l>
					</lg>
				</div></body></text></TEI>