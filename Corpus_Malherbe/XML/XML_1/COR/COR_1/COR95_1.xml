<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR95">
					<head type="main">CHAPITRE XXVI</head>
					<head type="sub">Des excellences de l’âme libre.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">Seigneur</w>, <w n="1.2">qu</w>’<w n="1.3">il</w> <w n="1.4">faut</w> <w n="1.5">être</w> <w n="1.6">parfait</w></l>
						<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">tenir</w> <w n="2.3">vers</w> <w n="2.4">le</w> <w n="2.5">ciel</w> <w n="2.6">l</w>’<w n="2.7">âme</w> <w n="2.8">toujours</w> <w n="2.9">tendue</w>.</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Sans</w> <w n="3.2">jamais</w> <w n="3.3">relâcher</w> <w n="3.4">la</w> <w n="3.5">vue</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Vers</w> <w n="4.2">ce</w> <w n="4.3">que</w> <w n="4.4">sur</w> <w n="4.5">la</w> <w n="4.6">terre</w> <w n="4.7">on</w> <w n="4.8">fait</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">À</w> <w n="5.2">travers</w> <w n="5.3">tant</w> <w n="5.4">de</w> <w n="5.5">soins</w> <w n="5.6">cuisants</w></l>
						<l n="6" num="2.2"><w n="6.1">Passer</w> <w n="6.2">comme</w> <w n="6.3">sans</w> <w n="6.4">soin</w>, <w n="6.5">non</w> <w n="6.6">ainsi</w> <w n="6.7">qu</w>’<w n="6.8">un</w> <w n="6.9">stupide</w></l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Que</w> <w n="7.2">son</w> <w n="7.3">esprit</w> <w n="7.4">morne</w> <w n="7.5">et</w> <w n="7.6">languide</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Assoupit</w> <w n="8.2">sous</w> <w n="8.3">les</w> <w n="8.4">plus</w> <w n="8.5">pesants</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">Mais</w> <w n="9.2">par</w> <w n="9.3">la</w> <w n="9.4">digne</w> <w n="9.5">fermeté</w></l>
						<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2">une</w> <w n="10.3">âme</w> <w n="10.4">toute</w> <w n="10.5">pure</w> <w n="10.6">et</w> <w n="10.7">toute</w> <w n="10.8">inébranlable</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Par</w> <w n="11.2">un</w> <w n="11.3">privilége</w> <w n="11.4">admirable</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">De</w> <w n="12.2">son</w> <w n="12.3">entière</w> <w n="12.4">liberté</w>,</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">Détacher</w> <w n="13.2">son</w> <w n="13.3">affection</w></l>
						<l n="14" num="4.2"><w n="14.1">De</w> <w n="14.2">tout</w> <w n="14.3">ce</w> <w n="14.4">qu</w>’<w n="14.5">ici</w>-<w n="14.6">bas</w> <w n="14.7">un</w> <w n="14.8">cœur</w> <w n="14.9">mondain</w> <w n="14.10">adore</w> :</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Seigneur</w>, <w n="15.2">j</w>’<w n="15.3">ose</w> <w n="15.4">le</w> <w n="15.5">dire</w> <w n="15.6">encore</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Qu</w>’<w n="16.2">il</w> <w n="16.3">y</w> <w n="16.4">faut</w> <w n="16.5">de</w> <w n="16.6">perfection</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><space unit="char" quantity="8"></space><w n="17.1">Ô</w> <w n="17.2">Dieu</w> <w n="17.3">tout</w> <w n="17.4">bon</w>, <w n="17.5">Dieu</w> <w n="17.6">tout</w>-<w n="17.7">puissant</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Défends</w>-<w n="18.2">moi</w> <w n="18.3">des</w> <w n="18.4">soucis</w> <w n="18.5">où</w> <w n="18.6">cette</w> <w n="18.7">vie</w> <w n="18.8">engage</w>,</l>
						<l n="19" num="5.3"><space unit="char" quantity="8"></space><w n="19.1">Qu</w>’<w n="19.2">ils</w> <w n="19.3">n</w>’<w n="19.4">enveloppent</w> <w n="19.5">mon</w> <w n="19.6">courage</w></l>
						<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">D</w>’<w n="20.2">un</w> <w n="20.3">amas</w> <w n="20.4">trop</w> <w n="20.5">embarrassant</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><space unit="char" quantity="8"></space><w n="21.1">Sauve</w>-<w n="21.2">moi</w> <w n="21.3">des</w> <w n="21.4">nécessités</w></l>
						<l n="22" num="6.2"><w n="22.1">Dont</w> <w n="22.2">le</w> <w n="22.3">soutien</w> <w n="22.4">du</w> <w n="22.5">corps</w> <w n="22.6">m</w>’<w n="22.7">importune</w> <w n="22.8">sans</w> <w n="22.9">cesse</w>,</l>
						<l n="23" num="6.3"><space unit="char" quantity="8"></space><w n="23.1">Que</w> <w n="23.2">leur</w> <w n="23.3">surprise</w> <w n="23.4">ou</w> <w n="23.5">leur</w> <w n="23.6">mollesse</w></l>
						<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Ne</w> <w n="24.2">donne</w> <w n="24.3">entrée</w> <w n="24.4">aux</w> <w n="24.5">voluptés</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><space unit="char" quantity="8"></space><w n="25.1">Enfin</w> <w n="25.2">délivre</w>-<w n="25.3">moi</w>, <w n="25.4">seigneur</w>,</l>
						<l n="26" num="7.2"><w n="26.1">De</w> <w n="26.2">tout</w> <w n="26.3">ce</w> <w n="26.4">qui</w> <w n="26.5">peut</w> <w n="26.6">faire</w> <w n="26.7">un</w> <w n="26.8">obstacle</w> <w n="26.9">à</w> <w n="26.10">mon</w> <w n="26.11">âme</w>,</l>
						<l n="27" num="7.3"><space unit="char" quantity="8"></space><w n="27.1">Et</w> <w n="27.2">changer</w> <w n="27.3">sa</w> <w n="27.4">plus</w> <w n="27.5">vive</w> <w n="27.6">flamme</w></l>
						<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">En</w> <w n="28.2">quelque</w> <w n="28.3">mourante</w> <w n="28.4">langueur</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><space unit="char" quantity="8"></space><w n="29.1">Ne</w> <w n="29.2">m</w>’<w n="29.3">affranchis</w> <w n="29.4">pas</w> <w n="29.5">seulement</w></l>
						<l n="30" num="8.2"><w n="30.1">Des</w> <w n="30.2">folles</w> <w n="30.3">passions</w> <w n="30.4">dont</w> <w n="30.5">la</w> <w n="30.6">terre</w> <w n="30.7">est</w> <w n="30.8">si</w> <w n="30.9">pleine</w>,</l>
						<l n="31" num="8.3"><space unit="char" quantity="8"></space><w n="31.1">Et</w> <w n="31.2">que</w> <w n="31.3">la</w> <w n="31.4">vanité</w> <w n="31.5">mondaine</w></l>
						<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Suit</w> <w n="32.2">avec</w> <w n="32.3">tant</w> <w n="32.4">d</w>’<w n="32.5">empressement</w> ;</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><space unit="char" quantity="8"></space><w n="33.1">Mais</w> <w n="33.2">de</w> <w n="33.3">tous</w> <w n="33.4">ces</w> <w n="33.5">petits</w> <w n="33.6">malheurs</w></l>
						<l n="34" num="9.2"><w n="34.1">Dont</w> <w n="34.2">répand</w> <w n="34.3">à</w> <w n="34.4">toute</w> <w n="34.5">heure</w> <w n="34.6">une</w> <w n="34.7">foule</w> <w n="34.8">importune</w></l>
						<l n="35" num="9.3"><space unit="char" quantity="8"></space><w n="35.1">La</w> <w n="35.2">malédiction</w> <w n="35.3">commune</w></l>
						<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Pour</w> <w n="36.2">peine</w> <w n="36.3">sur</w> <w n="36.4">tous</w> <w n="36.5">les</w> <w n="36.6">pécheurs</w> ;</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><space unit="char" quantity="8"></space><w n="37.1">De</w> <w n="37.2">tout</w> <w n="37.3">ce</w> <w n="37.4">qui</w> <w n="37.5">peut</w> <w n="37.6">retarder</w></l>
						<l n="38" num="10.2"><w n="38.1">La</w> <w n="38.2">liberté</w> <w n="38.3">d</w>’<w n="38.4">esprit</w> <w n="38.5">où</w> <w n="38.6">ta</w> <w n="38.7">bonté</w> <w n="38.8">m</w>’<w n="38.9">exhorte</w>,</l>
						<l n="39" num="10.3"><space unit="char" quantity="8"></space><w n="39.1">Et</w> <w n="39.2">semble</w> <w n="39.3">lui</w> <w n="39.4">fermer</w> <w n="39.5">la</w> <w n="39.6">porte</w>,</l>
						<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">Quand</w> <w n="40.2">tu</w> <w n="40.3">veux</w> <w n="40.4">bien</w> <w n="40.5">me</w> <w n="40.6">l</w>’<w n="40.7">accorder</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><space unit="char" quantity="8"></space><w n="41.1">Ineffable</w> <w n="41.2">et</w> <w n="41.3">pleine</w> <w n="41.4">douceur</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Daigne</w>, <w n="42.2">ô</w> <w n="42.3">mon</w> <w n="42.4">Dieu</w>, <w n="42.5">pour</w> <w n="42.6">moi</w> <w n="42.7">changer</w> <w n="42.8">en</w> <w n="42.9">amertume</w></l>
						<l n="43" num="11.3"><space unit="char" quantity="8"></space><w n="43.1">Tout</w> <w n="43.2">ce</w> <w n="43.3">que</w> <w n="43.4">le</w> <w n="43.5">monde</w> <w n="43.6">présume</w></l>
						<l n="44" num="11.4"><space unit="char" quantity="8"></space><w n="44.1">Couler</w> <w n="44.2">de</w> <w n="44.3">plus</w> <w n="44.4">doux</w> <w n="44.5">dans</w> <w n="44.6">mon</w> <w n="44.7">cœur</w>.</l>
						<l n="45" num="11.5"><space unit="char" quantity="8"></space><w n="45.1">Bannis</w> <w n="45.2">ces</w> <w n="45.3">consolations</w></l>
						<l n="46" num="11.6"><w n="46.1">Qui</w> <w n="46.2">peuvent</w> <w n="46.3">émousser</w> <w n="46.4">le</w> <w n="46.5">goût</w> <w n="46.6">des</w> <w n="46.7">éternelles</w>,</l>
						<l n="47" num="11.7"><space unit="char" quantity="8"></space><w n="47.1">Et</w> <w n="47.2">livrer</w> <w n="47.3">mes</w> <w n="47.4">sens</w> <w n="47.5">infidèles</w></l>
						<l n="48" num="11.8"><space unit="char" quantity="8"></space><w n="48.1">À</w> <w n="48.2">leurs</w> <w n="48.3">folles</w> <w n="48.4">impressions</w>.</l>
					</lg>
					<lg n="12">
						<l n="49" num="12.1"><space unit="char" quantity="8"></space><w n="49.1">Bannis</w> <w n="49.2">tout</w> <w n="49.3">ce</w> <w n="49.4">qui</w> <w n="49.5">fait</w> <w n="49.6">chérir</w></l>
						<l n="50" num="12.2"><w n="50.1">L</w>’<w n="50.2">ombre</w> <w n="50.3">d</w>’<w n="50.4">un</w> <w n="50.5">bien</w> <w n="50.6">présent</w> <w n="50.7">sous</w> <w n="50.8">un</w> <w n="50.9">attrait</w> <w n="50.10">sensible</w>,</l>
						<l n="51" num="12.3"><space unit="char" quantity="8"></space><w n="51.1">Et</w> <w n="51.2">dont</w> <w n="51.3">le</w> <w n="51.4">piége</w> <w n="51.5">imperceptible</w></l>
						<l n="52" num="12.4"><space unit="char" quantity="8"></space><w n="52.1">Nous</w> <w n="52.2">met</w> <w n="52.3">en</w> <w n="52.4">état</w> <w n="52.5">de</w> <w n="52.6">périr</w>.</l>
					</lg>
					<lg n="13">
						<l n="53" num="13.1"><space unit="char" quantity="8"></space><w n="53.1">Fais</w>, <w n="53.2">seigneur</w>, <w n="53.3">avorter</w> <w n="53.4">en</w> <w n="53.5">moi</w></l>
						<l n="54" num="13.2"><w n="54.1">De</w> <w n="54.2">la</w> <w n="54.3">chair</w> <w n="54.4">et</w> <w n="54.5">du</w> <w n="54.6">sang</w> <w n="54.7">les</w> <w n="54.8">dangereux</w> <w n="54.9">intrigues</w> ;</l>
						<l n="55" num="13.3"><space unit="char" quantity="8"></space><w n="55.1">Fais</w> <w n="55.2">que</w> <w n="55.3">leurs</w> <w n="55.4">ruses</w> <w n="55.5">ni</w> <w n="55.6">leurs</w> <w n="55.7">ligues</w></l>
						<l n="56" num="13.4"><space unit="char" quantity="8"></space><w n="56.1">Ne</w> <w n="56.2">me</w> <w n="56.3">fassent</w> <w n="56.4">jamais</w> <w n="56.5">la</w> <w n="56.6">loi</w> ;</l>
					</lg>
					<lg n="14">
						<l n="57" num="14.1"><space unit="char" quantity="8"></space><w n="57.1">Fais</w> <w n="57.2">que</w> <w n="57.3">cet</w> <w n="57.4">éclat</w> <w n="57.5">d</w>’<w n="57.6">un</w> <w n="57.7">moment</w></l>
						<l n="58" num="14.2"><w n="58.1">Dont</w> <w n="58.2">le</w> <w n="58.3">monde</w> <w n="58.4">éblouit</w> <w n="58.5">quiconque</w> <w n="58.6">ose</w> <w n="58.7">le</w> <w n="58.8">croire</w>,</l>
						<l n="59" num="14.3"><space unit="char" quantity="8"></space><w n="59.1">Cette</w> <w n="59.2">brillante</w> <w n="59.3">et</w> <w n="59.4">fausse</w> <w n="59.5">gloire</w>,</l>
						<l n="60" num="14.4"><space unit="char" quantity="8"></space><w n="60.1">Ne</w> <w n="60.2">me</w> <w n="60.3">déçoive</w> <w n="60.4">aucunement</w>.</l>
					</lg>
					<lg n="15">
						<l n="61" num="15.1"><space unit="char" quantity="8"></space><w n="61.1">Quoi</w> <w n="61.2">que</w> <w n="61.3">le</w> <w n="61.4">diable</w> <w n="61.5">ose</w> <w n="61.6">inventer</w></l>
						<l n="62" num="15.2"><w n="62.1">Pour</w> <w n="62.2">ouvrir</w> <w n="62.3">sous</w> <w n="62.4">mes</w> <w n="62.5">pas</w> <w n="62.6">un</w> <w n="62.7">mortel</w> <w n="62.8">précipice</w>,</l>
						<l n="63" num="15.3"><space unit="char" quantity="8"></space><w n="63.1">Fais</w> <w n="63.2">que</w> <w n="63.3">sa</w> <w n="63.4">plus</w> <w n="63.5">noire</w> <w n="63.6">malice</w></l>
						<l n="64" num="15.4"><space unit="char" quantity="8"></space><w n="64.1">N</w>’<w n="64.2">ait</w> <w n="64.3">point</w> <w n="64.4">de</w> <w n="64.5">quoi</w> <w n="64.6">me</w> <w n="64.7">supplanter</w>.</l>
					</lg>
					<lg n="16">
						<l n="65" num="16.1"><space unit="char" quantity="8"></space><w n="65.1">Pour</w> <w n="65.2">combattre</w> <w n="65.3">et</w> <w n="65.4">pour</w> <w n="65.5">souffrir</w> <w n="65.6">tout</w>,</l>
						<l n="66" num="16.2"><w n="66.1">Donne</w>-<w n="66.2">moi</w> <w n="66.3">de</w> <w n="66.4">la</w> <w n="66.5">force</w> <w n="66.6">et</w> <w n="66.7">de</w> <w n="66.8">la</w> <w n="66.9">patience</w> :</l>
						<l n="67" num="16.3"><space unit="char" quantity="8"></space><w n="67.1">Donne</w> <w n="67.2">à</w> <w n="67.3">mon</w> <w n="67.4">cœur</w> <w n="67.5">une</w> <w n="67.6">constance</w></l>
						<l n="68" num="16.4"><space unit="char" quantity="8"></space><w n="68.1">Qui</w> <w n="68.2">persévère</w> <w n="68.3">jusqu</w>’<w n="68.4">au</w> <w n="68.5">bout</w>.</l>
					</lg>
					<lg n="17">
						<l n="69" num="17.1"><space unit="char" quantity="8"></space><w n="69.1">Fais</w> <w n="69.2">que</w> <w n="69.3">j</w>’<w n="69.4">en</w> <w n="69.5">puisse</w> <w n="69.6">voir</w> <w n="69.7">proscrit</w></l>
						<l n="70" num="17.2"><w n="70.1">Le</w> <w n="70.2">goût</w> <w n="70.3">de</w> <w n="70.4">ces</w> <w n="70.5">douceurs</w> <w n="70.6">où</w> <w n="70.7">le</w> <w n="70.8">monde</w> <w n="70.9">préside</w> :</l>
						<l n="71" num="17.3"><space unit="char" quantity="8"></space><w n="71.1">Fais</w> <w n="71.2">qu</w>’<w n="71.3">il</w> <w n="71.4">laisse</w> <w n="71.5">la</w> <w n="71.6">place</w> <w n="71.7">vide</w></l>
						<l n="72" num="17.4"><space unit="char" quantity="8"></space><w n="72.1">À</w> <w n="72.2">l</w>’<w n="72.3">onction</w> <w n="72.4">de</w> <w n="72.5">ton</w> <w n="72.6">esprit</w>.</l>
					</lg>
					<lg n="18">
						<l n="73" num="18.1"><space unit="char" quantity="8"></space><w n="73.1">Au</w> <w n="73.2">lieu</w> <w n="73.3">de</w> <w n="73.4">cet</w> <w n="73.5">amour</w> <w n="73.6">charnel</w></l>
						<l n="74" num="18.2"><w n="74.1">Dont</w> <w n="74.2">l</w>’<w n="74.3">impure</w> <w n="74.4">chaleur</w> <w n="74.5">souille</w> <w n="74.6">ce</w> <w n="74.7">qu</w>’<w n="74.8">elle</w> <w n="74.9">enflamme</w>,</l>
						<l n="75" num="18.3"><space unit="char" quantity="8"></space><w n="75.1">Fais</w> <w n="75.2">couler</w> <w n="75.3">au</w> <w n="75.4">fond</w> <w n="75.5">de</w> <w n="75.6">mon</w> <w n="75.7">âme</w></l>
						<l n="76" num="18.4"><space unit="char" quantity="8"></space><w n="76.1">Celui</w> <w n="76.2">de</w> <w n="76.3">ton</w> <w n="76.4">nom</w> <w n="76.5">éternel</w>.</l>
					</lg>
					<lg n="19">
						<l n="77" num="19.1"><space unit="char" quantity="8"></space><w n="77.1">Boire</w>, <w n="77.2">et</w> <w n="77.3">manger</w>, <w n="77.4">et</w> <w n="77.5">se</w> <w n="77.6">vêtir</w>,</l>
						<l n="78" num="19.2"><w n="78.1">Sont</w> <w n="78.2">d</w>’<w n="78.3">étranges</w> <w n="78.4">fardeaux</w> <w n="78.5">qu</w>’<w n="78.6">impose</w> <w n="78.7">la</w> <w n="78.8">nature</w> :</l>
						<l n="79" num="19.3"><space unit="char" quantity="8"></space><w n="79.1">Oh</w> ! <w n="79.2">Qu</w>’<w n="79.3">un</w> <w n="79.4">esprit</w> <w n="79.5">fervent</w> <w n="79.6">endure</w></l>
						<l n="80" num="19.4"><space unit="char" quantity="8"></space><w n="80.1">Quand</w> <w n="80.2">il</w> <w n="80.3">s</w>’<w n="80.4">y</w> <w n="80.5">faut</w> <w n="80.6">assujettir</w> !</l>
					</lg>
					<lg n="20">
						<l n="81" num="20.1"><space unit="char" quantity="8"></space><w n="81.1">Fais</w>-<w n="81.2">m</w>’<w n="81.3">en</w> <w n="81.4">user</w> <w n="81.5">si</w> <w n="81.6">sobrement</w></l>
						<l n="82" num="20.2"><w n="82.1">Pour</w> <w n="82.2">réparer</w> <w n="82.3">un</w> <w n="82.4">corps</w> <w n="82.5">où</w> <w n="82.6">l</w>’<w n="82.7">âme</w> <w n="82.8">est</w> <w n="82.9">enfermée</w>,</l>
						<l n="83" num="20.3"><space unit="char" quantity="8"></space><w n="83.1">Qu</w>’<w n="83.2">elle</w> <w n="83.3">ne</w> <w n="83.4">soit</w> <w n="83.5">point</w> <w n="83.6">trop</w> <w n="83.7">charmée</w></l>
						<l n="84" num="20.4"><space unit="char" quantity="8"></space><w n="84.1">De</w> <w n="84.2">ce</w> <w n="84.3">qu</w>’<w n="84.4">ils</w> <w n="84.5">ont</w> <w n="84.6">d</w>’<w n="84.7">alléchement</w>.</l>
					</lg>
					<lg n="21">
						<l n="85" num="21.1"><space unit="char" quantity="8"></space><w n="85.1">Leur</w> <w n="85.2">bon</w> <w n="85.3">usage</w> <w n="85.4">est</w> <w n="85.5">un</w> <w n="85.6">effet</w></l>
						<l n="86" num="21.2"><w n="86.1">Que</w> <w n="86.2">le</w> <w n="86.3">propre</w> <w n="86.4">soutien</w> <w n="86.5">a</w> <w n="86.6">rendu</w> <w n="86.7">nécessaire</w>,</l>
						<l n="87" num="21.3"><space unit="char" quantity="8"></space><w n="87.1">Et</w> <w n="87.2">ce</w> <w n="87.3">corps</w> <w n="87.4">qu</w>’<w n="87.5">il</w> <w n="87.6">faut</w> <w n="87.7">satisfaire</w></l>
						<l n="88" num="21.4"><space unit="char" quantity="8"></space><w n="88.1">N</w>’<w n="88.2">y</w> <w n="88.3">peut</w> <w n="88.4">renoncer</w> <w n="88.5">tout</w> <w n="88.6">à</w> <w n="88.7">fait</w> ;</l>
					</lg>
					<lg n="22">
						<l n="89" num="22.1"><space unit="char" quantity="8"></space><w n="89.1">Mais</w> <w n="89.2">de</w> <w n="89.3">cette</w> <w n="89.4">nécessité</w></l>
						<l n="90" num="22.2"><w n="90.1">Aller</w> <w n="90.2">au</w> <w n="90.3">superflu</w>, <w n="90.4">passer</w> <w n="90.5">jusqu</w>’<w n="90.6">aux</w> <w n="90.7">délices</w>,</l>
						<l n="91" num="22.3"><space unit="char" quantity="8"></space><w n="91.1">Et</w> <w n="91.2">par</w> <w n="91.3">de</w> <w n="91.4">lâches</w> <w n="91.5">artifices</w></l>
						<l n="92" num="22.4"><space unit="char" quantity="8"></space><w n="92.1">Y</w> <w n="92.2">chercher</w> <w n="92.3">sa</w> <w n="92.4">félicité</w> :</l>
					</lg>
					<lg n="23">
						<l n="93" num="23.1"><space unit="char" quantity="8"></space><w n="93.1">C</w>’<w n="93.2">est</w> <w n="93.3">ce</w> <w n="93.4">que</w> <w n="93.5">nous</w> <w n="93.6">défend</w> <w n="93.7">ta</w> <w n="93.8">loi</w>,</l>
						<l n="94" num="23.2"><w n="94.1">De</w> <w n="94.2">peur</w> <w n="94.3">que</w> <w n="94.4">de</w> <w n="94.5">la</w> <w n="94.6">chair</w> <w n="94.7">l</w>’<w n="94.8">insolence</w> <w n="94.9">rebelle</w></l>
						<l n="95" num="23.3"><space unit="char" quantity="8"></space><w n="95.1">À</w> <w n="95.2">son</w> <w n="95.3">tour</w> <w n="95.4">ne</w> <w n="95.5">range</w> <w n="95.6">sous</w> <w n="95.7">elle</w></l>
						<l n="96" num="23.4"><space unit="char" quantity="8"></space><w n="96.1">L</w>’<w n="96.2">esprit</w> <w n="96.3">qui</w> <w n="96.4">doit</w> <w n="96.5">être</w> <w n="96.6">son</w> <w n="96.7">roi</w>.</l>
					</lg>
					<lg n="24">
						<l n="97" num="24.1"><space unit="char" quantity="8"></space><w n="97.1">Entre</w> <w n="97.2">ces</w> <w n="97.3">deux</w> <w n="97.4">extrémités</w>,</l>
						<l n="98" num="24.2"><w n="98.1">De</w> <w n="98.2">leur</w> <w n="98.3">juste</w> <w n="98.4">milieu</w> <w n="98.5">daigne</w> <w n="98.6">si</w> <w n="98.7">bien</w> <w n="98.8">m</w>’<w n="98.9">instruire</w>,</l>
						<l n="99" num="24.3"><space unit="char" quantity="8"></space><w n="99.1">Que</w> <w n="99.2">les</w> <w n="99.3">excès</w> <w n="99.4">qui</w> <w n="99.5">peuvent</w> <w n="99.6">nuire</w></l>
						<l n="100" num="24.4"><space unit="char" quantity="8"></space><w n="100.1">Soient</w> <w n="100.2">de</w> <w n="100.3">part</w> <w n="100.4">et</w> <w n="100.5">d</w>’<w n="100.6">autre</w> <w n="100.7">évités</w>.</l>
					</lg>
				</div></body></text></TEI>