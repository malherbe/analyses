<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR98">
					<head type="main">CHAPITRE XXIX</head>
					<head type="sub">Comment il faut invoquer Dieu, et le bénir <lb></lb>aux approches de la tribulation.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">le</w> <w n="1.3">veux</w>, <w n="1.4">ô</w> <w n="1.5">mon</w> <w n="1.6">Dieu</w>, <w n="1.7">que</w> <w n="1.8">cette</w> <w n="1.9">inquiétude</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ce</w> <w n="2.2">profond</w> <w n="2.3">déplaisir</w>, <w n="2.4">vienne</w> <w n="2.5">troubler</w> <w n="2.6">ma</w> <w n="2.7">paix</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Après</w> <w n="3.2">tant</w> <w n="3.3">de</w> <w n="3.4">douceurs</w> <w n="3.5">ta</w> <w n="3.6">main</w> <w n="3.7">veut</w> <w n="3.8">m</w>’<w n="3.9">être</w> <w n="3.10">rude</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">moi</w> <w n="4.3">j</w>’<w n="4.4">en</w> <w n="4.5">veux</w> <w n="4.6">bénir</w> <w n="4.7">ton</w> <w n="4.8">saint</w> <w n="4.9">nom</w> <w n="4.10">à</w> <w n="4.11">jamais</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">ne</w> <w n="5.3">saurois</w> <w n="5.4">parer</w> <w n="5.5">ce</w> <w n="5.6">grand</w> <w n="5.7">coup</w> <w n="5.8">de</w> <w n="5.9">tempête</w> :</l>
						<l n="6" num="2.2"><w n="6.1">Ses</w> <w n="6.2">approches</w> <w n="6.3">déjà</w> <w n="6.4">me</w> <w n="6.5">font</w> <w n="6.6">pâlir</w> <w n="6.7">d</w>’<w n="6.8">effroi</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">tout</w> <w n="7.3">ce</w> <w n="7.4">que</w> <w n="7.5">je</w> <w n="7.6">puis</w>, <w n="7.7">c</w>’<w n="7.8">est</w> <w n="7.9">de</w> <w n="7.10">baisser</w> <w n="7.11">la</w> <w n="7.12">tête</w>,</l>
						<l n="8" num="2.4"><w n="8.1">C</w>’<w n="8.2">est</w> <w n="8.3">de</w> <w n="8.4">forcer</w> <w n="8.5">mon</w> <w n="8.6">cœur</w> <w n="8.7">à</w> <w n="8.8">recourir</w> <w n="8.9">à</w> <w n="8.10">toi</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">ne</w> <w n="9.3">demande</w> <w n="9.4">point</w> <w n="9.5">que</w> <w n="9.6">tu</w> <w n="9.7">m</w>’<w n="9.8">en</w> <w n="9.9">garantisses</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Il</w> <w n="10.2">suffit</w> <w n="10.3">que</w> <w n="10.4">ton</w> <w n="10.5">bras</w> <w n="10.6">daigne</w> <w n="10.7">être</w> <w n="10.8">mon</w> <w n="10.9">appui</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">que</w> <w n="11.3">l</w>’<w n="11.4">heureux</w> <w n="11.5">succès</w> <w n="11.6">de</w> <w n="11.7">tes</w> <w n="11.8">bontés</w> <w n="11.9">propices</w></l>
						<l n="12" num="3.4"><w n="12.1">Me</w> <w n="12.2">rende</w> <w n="12.3">salutaire</w> <w n="12.4">un</w> <w n="12.5">si</w> <w n="12.6">cuisant</w> <w n="12.7">ennui</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">le</w> <w n="13.3">sens</w> <w n="13.4">qui</w> <w n="13.5">m</w>’<w n="13.6">accable</w> : <w n="13.7">ah</w> ! <w n="13.8">Seigneur</w>, <w n="13.9">que</w> <w n="13.10">j</w>’<w n="13.11">endure</w> !</l>
						<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">d</w>’<w n="14.3">agitations</w> <w n="14.4">me</w> <w n="14.5">déchirent</w> <w n="14.6">le</w> <w n="14.7">cœur</w> !</l>
						<l n="15" num="4.3"><w n="15.1">Qu</w>’<w n="15.2">il</w> <w n="15.3">se</w> <w n="15.4">trouve</w> <w n="15.5">au</w> <w n="15.6">milieu</w> <w n="15.7">d</w>’<w n="15.8">une</w> <w n="15.9">étrange</w> <w n="15.10">torture</w> !</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">qu</w>’<w n="16.3">il</w> <w n="16.4">y</w> <w n="16.5">soutient</w> <w n="16.6">mal</w> <w n="16.7">sa</w> <w n="16.8">mourante</w> <w n="16.9">vigueur</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Père</w> <w n="17.2">doux</w> <w n="17.3">et</w> <w n="17.4">bénin</w>, <w n="17.5">qui</w> <w n="17.6">connois</w> <w n="17.7">ma</w> <w n="17.8">foiblesse</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Que</w> <w n="18.2">faut</w>-<w n="18.3">il</w> <w n="18.4">que</w> <w n="18.5">je</w> <w n="18.6">die</w> <w n="18.7">en</w> <w n="18.8">cet</w> <w n="18.9">accablement</w> ?</l>
						<l n="19" num="5.3"><w n="19.1">Tu</w> <w n="19.2">vois</w> <w n="19.3">de</w> <w n="19.4">toutes</w> <w n="19.5">parts</w> <w n="19.6">quelle</w> <w n="19.7">rigueur</w> <w n="19.8">me</w> <w n="19.9">presse</w> :</l>
						<l n="20" num="5.4"><w n="20.1">Sauve</w>-<w n="20.2">moi</w>, <w n="20.3">mon</w> <w n="20.4">sauveur</w>, <w n="20.5">d</w>’<w n="20.6">un</w> <w n="20.7">si</w> <w n="20.8">cruel</w> <w n="20.9">moment</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">il</w> <w n="21.3">n</w>’<w n="21.4">est</w> <w n="21.5">arrivé</w>, <w n="21.6">ce</w> <w n="21.7">moment</w> <w n="21.8">qui</w> <w n="21.9">me</w> <w n="21.10">tue</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Qu</w>’<w n="22.2">à</w> <w n="22.3">dessein</w> <w n="22.4">que</w> <w n="22.5">ta</w> <w n="22.6">gloire</w> <w n="22.7">en</w> <w n="22.8">prenne</w> <w n="22.9">plus</w> <w n="22.10">d</w>’<w n="22.11">éclat</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Lorsqu</w>’<w n="23.2">après</w> <w n="23.3">avoir</w> <w n="23.4">vu</w> <w n="23.5">ma</w> <w n="23.6">constance</w> <w n="23.7">abattue</w>,</l>
						<l n="24" num="6.4"><w n="24.1">On</w> <w n="24.2">la</w> <w n="24.3">verra</w> <w n="24.4">par</w> <w n="24.5">toi</w> <w n="24.6">braver</w> <w n="24.7">ce</w> <w n="24.8">qui</w> <w n="24.9">l</w>’<w n="24.10">abat</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Étends</w> <w n="25.2">donc</w> <w n="25.3">cette</w> <w n="25.4">main</w> <w n="25.5">puissante</w> <w n="25.6">et</w> <w n="25.7">débonnaire</w></l>
						<l n="26" num="7.2"><w n="26.1">Qui</w> <w n="26.2">par</w> <w n="26.3">notre</w> <w n="26.4">triomphe</w> <w n="26.5">achève</w> <w n="26.6">nos</w> <w n="26.7">combats</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">Car</w> <w n="27.2">chétif</w> <w n="27.3">que</w> <w n="27.4">je</w> <w n="27.5">suis</w>, <w n="27.6">sans</w> <w n="27.7">toi</w> <w n="27.8">que</w> <w n="27.9">puis</w>-<w n="27.10">je</w> <w n="27.11">faire</w> ?</l>
						<l n="28" num="7.4"><w n="28.1">De</w> <w n="28.2">quel</w> <w n="28.3">côté</w> <w n="28.4">sans</w> <w n="28.5">toi</w> <w n="28.6">puis</w>-<w n="28.7">je</w> <w n="28.8">tourner</w> <w n="28.9">mes</w> <w n="28.10">pas</w> ?</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Encor</w> <w n="29.2">pour</w> <w n="29.3">cette</w> <w n="29.4">fois</w> <w n="29.5">donne</w>-<w n="29.6">moi</w> <w n="29.7">patience</w> :</l>
						<l n="30" num="8.2"><w n="30.1">Aide</w>-<w n="30.2">moi</w> <w n="30.3">par</w> <w n="30.4">ta</w> <w n="30.5">grâce</w> <w n="30.6">à</w> <w n="30.7">ne</w> <w n="30.8">point</w> <w n="30.9">murmurer</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">je</w> <w n="31.3">ne</w> <w n="31.4">craindrai</w> <w n="31.5">point</w> <w n="31.6">sur</w> <w n="31.7">cette</w> <w n="31.8">confiance</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Pour</w> <w n="32.2">grands</w> <w n="32.3">que</w> <w n="32.4">soient</w> <w n="32.5">les</w> <w n="32.6">maux</w> <w n="32.7">qu</w>’<w n="32.8">il</w> <w n="32.9">me</w> <w n="32.10">faille</w> <w n="32.11">endurer</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Cependant</w> <w n="33.2">derechef</w> <w n="33.3">que</w> <w n="33.4">faut</w>-<w n="33.5">il</w> <w n="33.6">que</w> <w n="33.7">je</w> <w n="33.8">die</w> ?</l>
						<l n="34" num="9.2"><w n="34.1">Ton</w> <w n="34.2">saint</w> <w n="34.3">vouloir</w> <w n="34.4">soit</w> <w n="34.5">fait</w>, <w n="34.6">ton</w> <w n="34.7">ordre</w> <w n="34.8">exécuté</w>.</l>
						<l n="35" num="9.3"><w n="35.1">Perte</w> <w n="35.2">de</w> <w n="35.3">biens</w>, <w n="35.4">disgrâce</w>, <w n="35.5">opprobre</w>, <w n="35.6">maladie</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Tout</w> <w n="36.2">est</w> <w n="36.3">juste</w>, <w n="36.4">seigneur</w>, <w n="36.5">et</w> <w n="36.6">j</w>’<w n="36.7">ai</w> <w n="36.8">tout</w> <w n="36.9">mérité</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">à</w> <w n="37.4">moi</w> <w n="37.5">de</w> <w n="37.6">souffrir</w>, <w n="37.7">et</w> <w n="37.8">plaise</w> <w n="37.9">à</w> <w n="37.10">ta</w> <w n="37.11">clémence</w></l>
						<l n="38" num="10.2"><w n="38.1">Que</w> <w n="38.2">ce</w> <w n="38.3">soit</w> <w n="38.4">sans</w> <w n="38.5">chagrin</w>, <w n="38.6">sans</w> <w n="38.7">bruit</w>, <w n="38.8">sans</w> <w n="38.9">m</w>’<w n="38.10">échapper</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Jusqu</w>’<w n="39.2">à</w> <w n="39.3">ce</w> <w n="39.4">que</w> <w n="39.5">l</w>’<w n="39.6">orage</w> <w n="39.7">ait</w> <w n="39.8">moins</w> <w n="39.9">de</w> <w n="39.10">véhémence</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Jusqu</w>’<w n="40.2">à</w> <w n="40.3">ce</w> <w n="40.4">que</w> <w n="40.5">le</w> <w n="40.6">calme</w> <w n="40.7">ait</w> <w n="40.8">pu</w> <w n="40.9">le</w> <w n="40.10">dissiper</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Ta</w> <w n="41.2">main</w> <w n="41.3">toute</w>-<w n="41.4">puissante</w> <w n="41.5">est</w> <w n="41.6">encore</w> <w n="41.7">aussi</w> <w n="41.8">forte</w></l>
						<l n="42" num="11.2"><w n="42.1">Que</w> <w n="42.2">l</w>’<w n="42.3">ont</w> <w n="42.4">sentie</w> <w n="42.5">en</w> <w n="42.6">moi</w> <w n="42.7">tant</w> <w n="42.8">d</w>’<w n="42.9">autres</w> <w n="42.10">déplaisirs</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Et</w> <w n="43.2">peut</w> <w n="43.3">rompre</w> <w n="43.4">le</w> <w n="43.5">coup</w> <w n="43.6">que</w> <w n="43.7">celui</w>-<w n="43.8">ci</w> <w n="43.9">me</w> <w n="43.10">porte</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Comme</w> <w n="44.2">elle</w> <w n="44.3">a</w> <w n="44.4">mille</w> <w n="44.5">fois</w> <w n="44.6">arrêté</w> <w n="44.7">mes</w> <w n="44.8">soupirs</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Elle</w> <w n="45.2">qui</w> <w n="45.3">de</w> <w n="45.4">mes</w> <w n="45.5">maux</w> <w n="45.6">domptant</w> <w n="45.7">la</w> <w n="45.8">barbarie</w>,</l>
						<l n="46" num="12.2"><w n="46.1">A</w> <w n="46.2">souvent</w> <w n="46.3">des</w> <w n="46.4">abois</w> <w n="46.5">rappelé</w> <w n="46.6">ma</w> <w n="46.7">vertu</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Peut</w> <w n="47.2">encor</w> <w n="47.3">de</w> <w n="47.4">ceux</w>-<w n="47.5">ci</w> <w n="47.6">modérer</w> <w n="47.7">la</w> <w n="47.8">furie</w>,</l>
						<l n="48" num="12.4"><w n="48.1">De</w> <w n="48.2">peur</w> <w n="48.3">que</w> <w n="48.4">je</w> <w n="48.5">n</w>’<w n="48.6">en</w> <w n="48.7">sois</w> <w n="48.8">tout</w> <w n="48.9">à</w> <w n="48.10">fait</w> <w n="48.11">abattu</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Oui</w>, <w n="49.2">ta</w> <w n="49.3">pitié</w>, <w n="49.4">mon</w> <w n="49.5">dieu</w>, <w n="49.6">soutenant</w> <w n="49.7">mon</w> <w n="49.8">courage</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Peut</w> <w n="50.2">le</w> <w n="50.3">rendre</w> <w n="50.4">vainqueur</w> <w n="50.5">de</w> <w n="50.6">leur</w> <w n="50.7">plus</w> <w n="50.8">rude</w> <w n="50.9">assaut</w> ;</l>
						<l n="51" num="13.3"><w n="51.1">Et</w> <w n="51.2">plus</w> <w n="51.3">ce</w> <w n="51.4">changement</w> <w n="51.5">m</w>’<w n="51.6">est</w> <w n="51.7">un</w> <w n="51.8">pénible</w> <w n="51.9">ouvrage</w>,</l>
						<l n="52" num="13.4"><w n="52.1">Plus</w> <w n="52.2">je</w> <w n="52.3">le</w> <w n="52.4">vois</w> <w n="52.5">facile</w> <w n="52.6">à</w> <w n="52.7">la</w> <w n="52.8">main</w> <w n="52.9">du</w> <w n="52.10">très</w>-<w n="52.11">haut</w>.</l>
					</lg>
				</div></body></text></TEI>