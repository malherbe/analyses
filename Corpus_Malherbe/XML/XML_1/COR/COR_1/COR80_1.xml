<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR80">
					<head type="main">CHAPITRE XI</head>
					<head type="sub">Qu’il faut examiner soigneusement les desirs du cœur, <lb></lb>et prendre peine à les modérer.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">vois</w> <w n="1.3">qu</w>’<w n="1.4">à</w> <w n="1.5">me</w> <w n="1.6">servir</w> <w n="1.7">enfin</w> <w n="1.8">tu</w> <w n="1.9">te</w> <w n="1.10">disposes</w> ;</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Mais</w> <w n="2.2">n</w>’<w n="2.3">en</w> <w n="2.4">espère</w> <w n="2.5">pas</w> <w n="2.6">grand</w> <w n="2.7">fruit</w>,</l>
						<l n="3" num="1.3"><w n="3.1">À</w> <w n="3.2">moins</w> <w n="3.3">que</w> <w n="3.4">je</w> <w n="3.5">t</w>’<w n="3.6">apprenne</w> <w n="3.7">encor</w> <w n="3.8">beaucoup</w> <w n="3.9">de</w> <w n="3.10">choses</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Dont</w> <w n="4.2">tu</w> <w n="4.3">n</w>’<w n="4.4">es</w> <w n="4.5">pas</w> <w n="4.6">encore</w> <w n="4.7">assez</w> <w n="4.8">instruit</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="10"></space><w n="5.1">Seigneur</w>, <w n="5.2">que</w> <w n="5.3">veux</w>-<w n="5.4">tu</w> <w n="5.5">m</w>’<w n="5.6">apprendre</w> ?</l>
						<l n="6" num="2.2"><space unit="char" quantity="10"></space><w n="6.1">Je</w> <w n="6.2">suis</w> <w n="6.3">prêt</w> <w n="6.4">de</w> <w n="6.5">t</w>’<w n="6.6">écouter</w> ;</l>
						<l n="7" num="2.3"><space unit="char" quantity="10"></space><w n="7.1">Joins</w> <w n="7.2">à</w> <w n="7.3">la</w> <w n="7.4">grâce</w> <w n="7.5">d</w>’<w n="7.6">entendre</w></l>
						<l n="8" num="2.4"><space unit="char" quantity="10"></space><w n="8.1">La</w> <w n="8.2">force</w> <w n="8.3">d</w>’<w n="8.4">exécuter</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Toutes</w> <w n="9.2">tes</w> <w n="9.3">volontés</w> <w n="9.4">doivent</w> <w n="9.5">être</w> <w n="9.6">soumises</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Purement</w> <w n="10.2">à</w> <w n="10.3">mon</w> <w n="10.4">bon</w> <w n="10.5">plaisir</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Jusqu</w>’<w n="11.2">à</w> <w n="11.3">ne</w> <w n="11.4">souhaiter</w> <w n="11.5">en</w> <w n="11.6">toutes</w> <w n="11.7">entreprises</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="4"></space><w n="12.1">Que</w> <w n="12.2">les</w> <w n="12.3">succès</w> <w n="12.4">que</w> <w n="12.5">je</w> <w n="12.6">voudrai</w> <w n="12.7">choisir</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Tu</w> <w n="13.2">ne</w> <w n="13.3">dois</w> <w n="13.4">point</w> <w n="13.5">t</w>’<w n="13.6">aimer</w>, <w n="13.7">tu</w> <w n="13.8">ne</w> <w n="13.9">dois</w> <w n="13.10">point</w> <w n="13.11">te</w> <w n="13.12">plaire</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Dans</w> <w n="14.2">tes</w> <w n="14.3">propres</w> <w n="14.4">contentements</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Tu</w> <w n="15.2">dois</w> <w n="15.3">n</w>’<w n="15.4">être</w> <w n="15.5">jaloux</w> <w n="15.6">que</w> <w n="15.7">de</w> <w n="15.8">me</w> <w n="15.9">satisfaire</w>,</l>
						<l n="16" num="4.4"><space unit="char" quantity="4"></space><w n="16.1">Et</w> <w n="16.2">d</w>’<w n="16.3">obéir</w> <w n="16.4">à</w> <w n="16.5">mes</w> <w n="16.6">commandements</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Quel</w> <w n="17.2">que</w> <w n="17.3">soit</w> <w n="17.4">le</w> <w n="17.5">desir</w> <w n="17.6">qui</w> <w n="17.7">t</w>’<w n="17.8">échauffe</w> <w n="17.9">et</w> <w n="17.10">te</w> <w n="17.11">pique</w>,</l>
						<l n="18" num="5.2"><space unit="char" quantity="8"></space><w n="18.1">Considère</w> <w n="18.2">ce</w> <w n="18.3">qui</w> <w n="18.4">t</w>’<w n="18.5">en</w> <w n="18.6">plaît</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">vois</w> <w n="19.3">si</w> <w n="19.4">sa</w> <w n="19.5">chaleur</w> <w n="19.6">à</w> <w n="19.7">ma</w> <w n="19.8">gloire</w> <w n="19.9">s</w>’<w n="19.10">applique</w>,</l>
						<l n="20" num="5.4"><space unit="char" quantity="4"></space><w n="20.1">Ou</w> <w n="20.2">s</w>’<w n="20.3">il</w> <w n="20.4">t</w>’<w n="20.5">émeut</w> <w n="20.6">par</w> <w n="20.7">ton</w> <w n="20.8">propre</w> <w n="20.9">intérêt</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Lorsque</w> <w n="21.2">ce</w> <w n="21.3">n</w>’<w n="21.4">est</w> <w n="21.5">qu</w>’<w n="21.6">à</w> <w n="21.7">moi</w> <w n="21.8">que</w> <w n="21.9">ce</w> <w n="21.10">desir</w> <w n="21.11">se</w> <w n="21.12">donne</w>,</l>
						<l n="22" num="6.2"><space unit="char" quantity="8"></space><w n="22.1">Qu</w>’<w n="22.2">il</w> <w n="22.3">n</w>’<w n="22.4">a</w> <w n="22.5">pour</w> <w n="22.6">but</w> <w n="22.7">que</w> <w n="22.8">mon</w> <w n="22.9">honneur</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Quelque</w> <w n="23.2">effet</w> <w n="23.3">qui</w> <w n="23.4">le</w> <w n="23.5">suive</w>, <w n="23.6">et</w> <w n="23.7">quoi</w> <w n="23.8">que</w> <w n="23.9">j</w>’<w n="23.10">en</w> <w n="23.11">ordonne</w>,</l>
						<l n="24" num="6.4"><space unit="char" quantity="4"></space><w n="24.1">Ta</w> <w n="24.2">fermeté</w> <w n="24.3">tient</w> <w n="24.4">tout</w> <w n="24.5">à</w> <w n="24.6">grand</w> <w n="24.7">bonheur</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Mais</w> <w n="25.2">lorsque</w> <w n="25.3">l</w>’<w n="25.4">amour</w>-<w n="25.5">propre</w> <w n="25.6">y</w> <w n="25.7">garde</w> <w n="25.8">encor</w> <w n="25.9">sa</w> <w n="25.10">place</w>,</l>
						<l n="26" num="7.2"><space unit="char" quantity="8"></space><w n="26.1">Quoique</w> <w n="26.2">secret</w> <w n="26.3">et</w> <w n="26.4">déguisé</w>,</l>
						<l n="27" num="7.3"><w n="27.1">C</w>’<w n="27.2">est</w> <w n="27.3">là</w> <w n="27.4">ce</w> <w n="27.5">qui</w> <w n="27.6">te</w> <w n="27.7">gêne</w> <w n="27.8">et</w> <w n="27.9">ce</w> <w n="27.10">qui</w> <w n="27.11">t</w>’<w n="27.12">embarrasse</w>,</l>
						<l n="28" num="7.4"><space unit="char" quantity="4"></space><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">ce</w> <w n="28.4">qui</w> <w n="28.5">pèse</w> <w n="28.6">à</w> <w n="28.7">ton</w> <w n="28.8">cœur</w> <w n="28.9">divisé</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Défends</w>-<w n="29.2">toi</w> <w n="29.3">donc</w>, <w n="29.4">mon</w> <w n="29.5">fils</w>, <w n="29.6">de</w> <w n="29.7">la</w> <w n="29.8">première</w> <w n="29.9">amorce</w></l>
						<l n="30" num="8.2"><space unit="char" quantity="8"></space><w n="30.1">D</w>’<w n="30.2">un</w> <w n="30.3">desir</w> <w n="30.4">mal</w> <w n="30.5">prémédité</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">N</w>’<w n="31.2">y</w> <w n="31.3">prends</w> <w n="31.4">aucun</w> <w n="31.5">appui</w>, <w n="31.6">n</w>’<w n="31.7">y</w> <w n="31.8">donne</w> <w n="31.9">aucune</w> <w n="31.10">force</w></l>
						<l n="32" num="8.4"><space unit="char" quantity="4"></space><w n="32.1">Qu</w>’<w n="32.2">après</w> <w n="32.3">m</w>’<w n="32.4">avoir</w> <w n="32.5">pleinement</w> <w n="32.6">consulté</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Ce</w> <w n="33.2">qui</w> <w n="33.3">t</w>’<w n="33.4">en</w> <w n="33.5">plaît</w> <w n="33.6">d</w>’<w n="33.7">abord</w> <w n="33.8">peut</w> <w n="33.9">bientôt</w> <w n="33.10">te</w> <w n="33.11">déplaire</w>,</l>
						<l n="34" num="9.2"><space unit="char" quantity="8"></space><w n="34.1">Et</w> <w n="34.2">te</w> <w n="34.3">réduire</w> <w n="34.4">au</w> <w n="34.5">repentir</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">tu</w> <w n="35.3">rougiras</w> <w n="35.4">lors</w> <w n="35.5">de</w> <w n="35.6">ce</w> <w n="35.7">qu</w>’<w n="35.8">aura</w> <w n="35.9">pu</w> <w n="35.10">faire</w></l>
						<l n="36" num="9.4"><space unit="char" quantity="4"></space><w n="36.1">Cette</w> <w n="36.2">chaleur</w> <w n="36.3">trop</w> <w n="36.4">prompte</w> <w n="36.5">à</w> <w n="36.6">consentir</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Tout</w> <w n="37.2">ce</w> <w n="37.3">qui</w> <w n="37.4">paroît</w> <w n="37.5">bon</w> <w n="37.6">n</w>’<w n="37.7">est</w> <w n="37.8">pas</w> <w n="37.9">toujours</w> <w n="37.10">à</w> <w n="37.11">suivre</w>,</l>
						<l n="38" num="10.2"><space unit="char" quantity="8"></space><w n="38.1">Ni</w> <w n="38.2">son</w> <w n="38.3">contraire</w> <w n="38.4">à</w> <w n="38.5">rejeter</w> ;</l>
						<l n="39" num="10.3"><w n="39.1">L</w>’<w n="39.2">ardeur</w> <w n="39.3">impétueuse</w> <w n="39.4">à</w> <w n="39.5">mille</w> <w n="39.6">erreurs</w> <w n="39.7">te</w> <w n="39.8">livre</w>,</l>
						<l n="40" num="10.4"><space unit="char" quantity="4"></space><w n="40.1">Et</w> <w n="40.2">trop</w> <w n="40.3">courir</w> <w n="40.4">c</w>’<w n="40.5">est</w> <w n="40.6">te</w> <w n="40.7">précipiter</w>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">La</w> <w n="41.2">bride</w> <w n="41.3">est</w> <w n="41.4">souvent</w> <w n="41.5">bonne</w>, <w n="41.6">et</w> <w n="41.7">même</w> <w n="41.8">il</w> <w n="41.9">en</w> <w n="41.10">faut</w> <w n="41.11">une</w></l>
						<l n="42" num="11.2"><space unit="char" quantity="8"></space><w n="42.1">À</w> <w n="42.2">la</w> <w n="42.3">plus</w> <w n="42.4">sainte</w> <w n="42.5">affection</w> ;</l>
						<l n="43" num="11.3"><w n="43.1">Son</w> <w n="43.2">trop</w> <w n="43.3">d</w>’<w n="43.4">empressement</w> <w n="43.5">la</w> <w n="43.6">peut</w> <w n="43.7">rendre</w> <w n="43.8">importune</w>,</l>
						<l n="44" num="11.4"><space unit="char" quantity="4"></space><w n="44.1">Et</w> <w n="44.2">te</w> <w n="44.3">pousser</w> <w n="44.4">dans</w> <w n="44.5">la</w> <w n="44.6">distraction</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Il</w> <w n="45.2">te</w> <w n="45.3">peut</w> <w n="45.4">emporter</w> <w n="45.5">hors</w> <w n="45.6">de</w> <w n="45.7">la</w> <w n="45.8">discipline</w>,</l>
						<l n="46" num="12.2"><space unit="char" quantity="8"></space><w n="46.1">Sous</w> <w n="46.2">pretexte</w> <w n="46.3">de</w> <w n="46.4">faire</w> <w n="46.5">mieux</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">laisser</w> <w n="47.3">du</w> <w n="47.4">scandale</w> <w n="47.5">à</w> <w n="47.6">qui</w> <w n="47.7">ne</w> <w n="47.8">l</w>’<w n="47.9">examine</w></l>
						<l n="48" num="12.4"><space unit="char" quantity="4"></space><w n="48.1">Que</w> <w n="48.2">par</w> <w n="48.3">la</w> <w n="48.4">règle</w> <w n="48.5">où</w> <w n="48.6">s</w>’<w n="48.7">attachent</w> <w n="48.8">ses</w> <w n="48.9">yeux</w>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Il</w> <w n="49.2">peut</w> <w n="49.3">faire</w> <w n="49.4">en</w> <w n="49.5">autrui</w> <w n="49.6">naître</w> <w n="49.7">une</w> <w n="49.8">résistance</w></l>
						<l n="50" num="13.2"><space unit="char" quantity="8"></space><w n="50.1">Que</w> <w n="50.2">tu</w> <w n="50.3">n</w>’<w n="50.4">auras</w> <w n="50.5">daigné</w> <w n="50.6">prévoir</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Et</w> <w n="51.2">de</w> <w n="51.3">qui</w> <w n="51.4">la</w> <w n="51.5">surprise</w> <w n="51.6">ébranlant</w> <w n="51.7">ta</w> <w n="51.8">constance</w></l>
						<l n="52" num="13.4"><space unit="char" quantity="4"></space><w n="52.1">La</w> <w n="52.2">troublera</w> <w n="52.3">jusqu</w>’<w n="52.4">à</w> <w n="52.5">ta</w> <w n="52.6">faire</w> <w n="52.7">choir</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Un</w> <w n="53.2">peu</w> <w n="53.3">de</w> <w n="53.4">violence</w> <w n="53.5">est</w> <w n="53.6">souvent</w> <w n="53.7">nécessaire</w></l>
						<l n="54" num="14.2"><space unit="char" quantity="8"></space><w n="54.1">Contre</w> <w n="54.2">les</w> <w n="54.3">appétits</w> <w n="54.4">des</w> <w n="54.5">sens</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Même</w> <w n="55.2">quand</w> <w n="55.3">leur</w> <w n="55.4">effet</w> <w n="55.5">te</w> <w n="55.6">paroît</w> <w n="55.7">salutaire</w>,</l>
						<l n="56" num="14.4"><space unit="char" quantity="4"></space><w n="56.1">Quand</w> <w n="56.2">leurs</w> <w n="56.3">desirs</w> <w n="56.4">te</w> <w n="56.5">semblent</w> <w n="56.6">innocents</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Ne</w> <w n="57.2">demande</w> <w n="57.3">jamais</w> <w n="57.4">à</w> <w n="57.5">ta</w> <w n="57.6">chair</w> <w n="57.7">infidèle</w></l>
						<l n="58" num="15.2"><space unit="char" quantity="8"></space><w n="58.1">Ce</w> <w n="58.2">qu</w>’<w n="58.3">elle</w> <w n="58.4">veut</w> <w n="58.5">ou</w> <w n="58.6">ne</w> <w n="58.7">veut</w> <w n="58.8">pas</w> ;</l>
						<l n="59" num="15.3"><w n="59.1">Range</w>-<w n="59.2">la</w> <w n="59.3">sous</w> <w n="59.4">l</w>’<w n="59.5">esprit</w>, <w n="59.6">et</w> <w n="59.7">fais</w> <w n="59.8">qu</w>’<w n="59.9">en</w> <w n="59.10">dépit</w> <w n="59.11">d</w>’<w n="59.12">elle</w></l>
						<l n="60" num="15.4"><space unit="char" quantity="4"></space><w n="60.1">Son</w> <w n="60.2">esclavage</w> <w n="60.3">ait</w> <w n="60.4">pour</w> <w n="60.5">toi</w> <w n="60.6">des</w> <w n="60.7">appas</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Qu</w>’<w n="61.2">en</w> <w n="61.3">maître</w>, <w n="61.4">qu</w>’<w n="61.5">en</w> <w n="61.6">tyran</w> <w n="61.7">cet</w> <w n="61.8">esprit</w> <w n="61.9">la</w> <w n="61.10">châtie</w>,</l>
						<l n="62" num="16.2"><space unit="char" quantity="8"></space><w n="62.1">Qu</w>’<w n="62.2">il</w> <w n="62.3">l</w>’<w n="62.4">enchaîne</w> <w n="62.5">de</w> <w n="62.6">rudes</w> <w n="62.7">nœuds</w>,</l>
						<l n="63" num="16.3"><w n="63.1">Jusqu</w>’<w n="63.2">à</w> <w n="63.3">ce</w> <w n="63.4">que</w> <w n="63.5">domptée</w> <w n="63.6">et</w> <w n="63.7">bien</w> <w n="63.8">assujettie</w>,</l>
						<l n="64" num="16.4"><space unit="char" quantity="4"></space><w n="64.1">Elle</w> <w n="64.2">soit</w> <w n="64.3">prête</w> <w n="64.4">à</w> <w n="64.5">tout</w> <w n="64.6">ce</w> <w n="64.7">que</w> <w n="64.8">tu</w> <w n="64.9">veux</w> ;</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Jusqu</w>’<w n="65.2">à</w> <w n="65.3">ce</w> <w n="65.4">que</w> <w n="65.5">de</w> <w n="65.6">peu</w> <w n="65.7">satisfaite</w> <w n="65.8">et</w> <w n="65.9">contente</w>,</l>
						<l n="66" num="17.2"><space unit="char" quantity="8"></space><w n="66.1">Elle</w> <w n="66.2">aime</w> <w n="66.3">la</w> <w n="66.4">simplicité</w>,</l>
						<l n="67" num="17.3"><w n="67.1">Et</w> <w n="67.2">que</w> <w n="67.3">chaque</w> <w n="67.4">revers</w> <w n="67.5">qui</w> <w n="67.6">trompe</w> <w n="67.7">son</w> <w n="67.8">attente</w></l>
						<l n="68" num="17.4"><space unit="char" quantity="4"></space><w n="68.1">Sans</w> <w n="68.2">murmurer</w> <w n="68.3">en</w> <w n="68.4">puisse</w> <w n="68.5">être</w> <w n="68.6">accepté</w>.</l>
					</lg>
				</div></body></text></TEI>