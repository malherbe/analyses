<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE TROISIÈME</head><div type="poem" key="COR81">
					<head type="main">CHAPITRE XII</head>
					<head type="sub">Comme il se faut faire à la patience, et combattre <lb></lb>les passions.</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space><w n="1.1">À</w> <w n="1.2">ce</w> <w n="1.3">que</w> <w n="1.4">je</w> <w n="1.5">puis</w> <w n="1.6">voir</w>, <w n="1.7">seigneur</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">J</w>’<w n="2.2">ai</w> <w n="2.3">grand</w> <w n="2.4">besoin</w> <w n="2.5">de</w> <w n="2.6">patience</w></l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Contre</w> <w n="3.2">la</w> <w n="3.3">rude</w> <w n="3.4">expérience</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Où</w> <w n="4.2">cette</w> <w n="4.3">vie</w> <w n="4.4">engage</w> <w n="4.5">un</w> <w n="4.6">cœur</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Elle</w> <w n="5.2">n</w>’<w n="5.3">est</w> <w n="5.4">qu</w>’<w n="5.5">un</w> <w n="5.6">gouffre</w> <w n="5.7">de</w> <w n="5.8">maux</w>,</l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">D</w>’<w n="6.2">accidents</w> <w n="6.3">fâcheux</w> <w n="6.4">et</w> <w n="6.5">contraires</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="8"></space><w n="7.1">Qu</w>’<w n="7.2">un</w> <w n="7.3">accablement</w> <w n="7.4">de</w> <w n="7.5">misères</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">D</w>’<w n="8.2">où</w> <w n="8.3">naissent</w> <w n="8.4">travaux</w> <w n="8.5">sur</w> <w n="8.6">travaux</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="8"></space><w n="9.1">Je</w> <w n="9.2">n</w>’<w n="9.3">y</w> <w n="9.4">termine</w> <w n="9.5">aucuns</w> <w n="9.6">combats</w></l>
						<l n="10" num="3.2"><space unit="char" quantity="8"></space><w n="10.1">Que</w> <w n="10.2">chaque</w> <w n="10.3">instant</w> <w n="10.4">ne</w> <w n="10.5">renouvelle</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="8"></space><w n="11.1">Et</w> <w n="11.2">ma</w> <w n="11.3">paix</w> <w n="11.4">y</w> <w n="11.5">traîne</w> <w n="11.6">avec</w> <w n="11.7">elle</w></l>
						<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">La</w> <w n="12.2">guerre</w> <w n="12.3">attachée</w> <w n="12.4">à</w> <w n="12.5">mes</w> <w n="12.6">pas</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><space unit="char" quantity="8"></space><w n="13.1">Les</w> <w n="13.2">soins</w> <w n="13.3">même</w> <w n="13.4">de</w> <w n="13.5">l</w>’<w n="13.6">affermir</w></l>
						<l n="14" num="4.2"><space unit="char" quantity="8"></space><w n="14.1">Ne</w> <w n="14.2">sont</w> <w n="14.3">en</w> <w n="14.4">effet</w> <w n="14.5">qu</w>’<w n="14.6">une</w> <w n="14.7">guerre</w>,</l>
						<l n="15" num="4.3"><space unit="char" quantity="8"></space><w n="15.1">Et</w> <w n="15.2">tout</w> <w n="15.3">mon</w> <w n="15.4">séjour</w> <w n="15.5">sur</w> <w n="15.6">la</w> <w n="15.7">terre</w></l>
						<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Qu</w>’<w n="16.2">une</w> <w n="16.3">occasion</w> <w n="16.4">de</w> <w n="16.5">gémir</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Tu</w> <w n="17.2">dis</w> <w n="17.3">vrai</w>, <w n="17.4">mon</w> <w n="17.5">enfant</w> ; <w n="17.6">aussi</w> <w n="17.7">ne</w> <w n="17.8">veux</w>-<w n="17.9">je</w> <w n="17.10">pas</w></l>
						<l n="18" num="5.2"><w n="18.1">Que</w> <w n="18.2">tu</w> <w n="18.3">cherches</w> <w n="18.4">en</w> <w n="18.5">terre</w> <w n="18.6">une</w> <w n="18.7">paix</w> <w n="18.8">sans</w> <w n="18.9">combats</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Un</w> <w n="19.2">repos</w> <w n="19.3">sans</w> <w n="19.4">tumulte</w>, <w n="19.5">un</w> <w n="19.6">calme</w> <w n="19.7">sans</w> <w n="19.8">orage</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Où</w> <w n="20.2">toujours</w> <w n="20.3">la</w> <w n="20.4">fortune</w> <w n="20.5">ait</w> <w n="20.6">un</w> <w n="20.7">même</w> <w n="20.8">visage</w>,</l>
						<l n="21" num="5.5"><w n="21.1">Et</w> <w n="21.2">semble</w> <w n="21.3">par</w> <w n="21.4">le</w> <w n="21.5">cours</w> <w n="21.6">de</w> <w n="21.7">ses</w> <w n="21.8">événements</w></l>
						<l n="22" num="5.6"><w n="22.1">S</w>’<w n="22.2">asservir</w> <w n="22.3">en</w> <w n="22.4">esclave</w> <w n="22.5">à</w> <w n="22.6">tes</w> <w n="22.7">contentements</w>.</l>
						<l n="23" num="5.7"><w n="23.1">Je</w> <w n="23.2">veux</w> <w n="23.3">te</w> <w n="23.4">voir</w> <w n="23.5">en</w> <w n="23.6">paix</w>, <w n="23.7">mais</w> <w n="23.8">parmi</w> <w n="23.9">les</w> <w n="23.10">traverses</w>,</l>
						<l n="24" num="5.8"><w n="24.1">Parmi</w> <w n="24.2">les</w> <w n="24.3">changements</w> <w n="24.4">des</w> <w n="24.5">fortunes</w> <w n="24.6">diverses</w> ;</l>
						<l n="25" num="5.9"><w n="25.1">Je</w> <w n="25.2">veux</w> <w n="25.3">y</w> <w n="25.4">voir</w> <w n="25.5">ton</w> <w n="25.6">calme</w>, <w n="25.7">et</w> <w n="25.8">que</w> <w n="25.9">l</w>’<w n="25.10">adversité</w></l>
						<l n="26" num="5.10"><w n="26.1">Te</w> <w n="26.2">serve</w> <w n="26.3">à</w> <w n="26.4">t</w>’<w n="26.5">affermir</w> <w n="26.6">dans</w> <w n="26.7">la</w> <w n="26.8">tranquillité</w>.</l>
						<l n="27" num="5.11">« <w n="27.1">Tu</w> <w n="27.2">ne</w> <w n="27.3">peux</w>, <w n="27.4">me</w> <w n="27.5">dis</w>-<w n="27.6">tu</w>, <w n="27.7">souffrir</w> <w n="27.8">beaucoup</w> <w n="27.9">de</w> <w n="27.10">choses</w> ;</l>
						<l n="28" num="5.12"><w n="28.1">En</w> <w n="28.2">vain</w> <w n="28.3">tu</w> <w n="28.4">t</w>’<w n="28.5">y</w> <w n="28.6">résous</w>, <w n="28.7">en</w> <w n="28.8">vain</w> <w n="28.9">tu</w> <w n="28.10">t</w>’<w n="28.11">y</w> <w n="28.12">disposes</w>,</l>
						<l n="29" num="5.13"><w n="29.1">Tu</w> <w n="29.2">sens</w> <w n="29.3">une</w> <w n="29.4">révolte</w> <w n="29.5">en</w> <w n="29.6">ton</w> <w n="29.7">cœur</w> <w n="29.8">mutiné</w></l>
						<l n="30" num="5.14"><w n="30.1">Contre</w> <w n="30.2">la</w> <w n="30.3">patience</w> <w n="30.4">où</w> <w n="30.5">tu</w> <w n="30.6">l</w>’<w n="30.7">as</w> <w n="30.8">condamné</w>. »</l>
						<l n="31" num="5.15"><w n="31.1">Lâche</w>, <w n="31.2">qu</w>’<w n="31.3">oses</w>-<w n="31.4">tu</w> <w n="31.5">dire</w> ? <w n="31.6">Ainsi</w> <w n="31.7">le</w> <w n="31.8">purgatoire</w>,</l>
						<l n="32" num="5.16"><w n="32.1">Ainsi</w> <w n="32.2">ses</w> <w n="32.3">feux</w> <w n="32.4">cuisants</w> <w n="32.5">sont</w> <w n="32.6">hors</w> <w n="32.7">de</w> <w n="32.8">ta</w> <w n="32.9">mémoire</w> ?</l>
						<l n="33" num="5.17"><w n="33.1">Auras</w>-<w n="33.2">tu</w> <w n="33.3">plus</w> <w n="33.4">de</w> <w n="33.5">force</w> ? <w n="33.6">Ou</w> <w n="33.7">les</w> <w n="33.8">présumes</w>-<w n="33.9">tu</w></l>
						<l n="34" num="5.18"><w n="34.1">Plus</w> <w n="34.2">aisés</w> <w n="34.3">à</w> <w n="34.4">souffrir</w> <w n="34.5">à</w> <w n="34.6">ce</w> <w n="34.7">cœur</w> <w n="34.8">abattu</w> ?</l>
						<l n="35" num="5.19"><w n="35.1">Apprends</w> <w n="35.2">que</w> <w n="35.3">de</w> <w n="35.4">deux</w> <w n="35.5">maux</w> <w n="35.6">il</w> <w n="35.7">faut</w> <w n="35.8">choisir</w> <w n="35.9">le</w> <w n="35.10">moindre</w>,</l>
						<l n="36" num="5.20"><w n="36.1">Que</w> <w n="36.2">tes</w> <w n="36.3">soins</w> <w n="36.4">en</w> <w n="36.5">ce</w> <w n="36.6">but</w> <w n="36.7">se</w> <w n="36.8">doivent</w> <w n="36.9">tous</w> <w n="36.10">rejoindre</w>,</l>
						<l n="37" num="5.21"><w n="37.1">Et</w> <w n="37.2">que</w> <w n="37.3">pour</w> <w n="37.4">éviter</w> <w n="37.5">les</w> <w n="37.6">tourments</w> <w n="37.7">éternels</w>,</l>
						<l n="38" num="5.22"><w n="38.1">Tu</w> <w n="38.2">dois</w> <w n="38.3">traiter</w> <w n="38.4">tes</w> <w n="38.5">sens</w> <w n="38.6">d</w>’<w n="38.7">infâmes</w> <w n="38.8">criminels</w>,</l>
						<l n="39" num="5.23"><w n="39.1">Braver</w> <w n="39.2">leurs</w> <w n="39.3">appétits</w>, <w n="39.4">leur</w> <w n="39.5">imposer</w> <w n="39.6">des</w> <w n="39.7">gênes</w>,</l>
						<l n="40" num="5.24"><w n="40.1">Préparer</w> <w n="40.2">ta</w> <w n="40.3">constance</w> <w n="40.4">aux</w> <w n="40.5">misères</w> <w n="40.6">humaines</w>,</l>
						<l n="41" num="5.25"><w n="41.1">Les</w> <w n="41.2">souffrir</w> <w n="41.3">sans</w> <w n="41.4">murmure</w>, <w n="41.5">et</w> <w n="41.6">recevoir</w> <w n="41.7">les</w> <w n="41.8">croix</w></l>
						<l n="42" num="5.26"><w n="42.1">Ainsi</w> <w n="42.2">que</w> <w n="42.3">des</w> <w n="42.4">faveurs</w> <w n="42.5">qui</w> <w n="42.6">viennent</w> <w n="42.7">de</w> <w n="42.8">mon</w> <w n="42.9">choix</w>.</l>
					</lg>
					<lg n="6">
						<l n="43" num="6.1"><w n="43.1">Crois</w>-<w n="43.2">tu</w> <w n="43.3">les</w> <w n="43.4">gens</w> <w n="43.5">du</w> <w n="43.6">monde</w> <w n="43.7">exempts</w> <w n="43.8">d</w>’<w n="43.9">inquiétude</w> ?</l>
						<l n="44" num="6.2"><w n="44.1">Ne</w> <w n="44.2">vois</w>-<w n="44.3">tu</w> <w n="44.4">rien</w> <w n="44.5">pour</w> <w n="44.6">eux</w> <w n="44.7">ni</w> <w n="44.8">d</w>’<w n="44.9">amer</w> <w n="44.10">ni</w> <w n="44.11">de</w> <w n="44.12">rude</w> ?</l>
						<l n="45" num="6.3"><w n="45.1">Va</w> <w n="45.2">chez</w> <w n="45.3">ces</w> <w n="45.4">délicats</w> <w n="45.5">qui</w> <w n="45.6">n</w>’<w n="45.7">ont</w> <w n="45.8">soin</w> <w n="45.9">que</w> <w n="45.10">d</w>’<w n="45.11">unir</w></l>
						<l n="46" num="6.4"><w n="46.1">Le</w> <w n="46.2">choix</w> <w n="46.3">des</w> <w n="46.4">voluptés</w> <w n="46.5">aux</w> <w n="46.6">moyens</w> <w n="46.7">d</w>’<w n="46.8">y</w> <w n="46.9">fournir</w> :</l>
						<l n="47" num="6.5"><w n="47.1">Si</w> <w n="47.2">tu</w> <w n="47.3">crois</w> <w n="47.4">y</w> <w n="47.5">trouver</w> <w n="47.6">des</w> <w n="47.7">roses</w> <w n="47.8">sans</w> <w n="47.9">épines</w>,</l>
						<l n="48" num="6.6"><w n="48.1">Tu</w> <w n="48.2">n</w>’<w n="48.3">y</w> <w n="48.4">trouveras</w> <w n="48.5">point</w> <w n="48.6">ce</w> <w n="48.7">que</w> <w n="48.8">tu</w> <w n="48.9">t</w>’<w n="48.10">imagines</w>.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1">« <w n="49.1">Mais</w> <w n="49.2">ils</w> <w n="49.3">suivent</w>, <w n="49.4">dis</w>-<w n="49.5">tu</w>, <w n="49.6">leurs</w> <w n="49.7">inclinations</w> ;</l>
						<l n="50" num="7.2"><w n="50.1">Leur</w> <w n="50.2">seule</w> <w n="50.3">volonté</w> <w n="50.4">règle</w> <w n="50.5">leurs</w> <w n="50.6">actions</w>,</l>
						<l n="51" num="7.3"><w n="51.1">Et</w> <w n="51.2">l</w>’<w n="51.3">excès</w> <w n="51.4">des</w> <w n="51.5">plaisirs</w> <w n="51.6">en</w> <w n="51.7">un</w> <w n="51.8">moment</w> <w n="51.9">consume</w></l>
						<l n="52" num="7.4"><w n="52.1">Ce</w> <w n="52.2">peu</w> <w n="52.3">qui</w> <w n="52.4">par</w> <w n="52.5">hasard</w> <w n="52.6">s</w>’<w n="52.7">y</w> <w n="52.8">coule</w> <w n="52.9">d</w>’<w n="52.10">amertume</w>. »</l>
						<l n="53" num="7.5"><w n="53.1">Eh</w> <w n="53.2">bien</w> ! <w n="53.3">Soit</w>, <w n="53.4">je</w> <w n="53.5">le</w> <w n="53.6">veux</w>, <w n="53.7">ils</w> <w n="53.8">ont</w> <w n="53.9">tout</w> <w n="53.10">à</w> <w n="53.11">souhait</w> ;</l>
						<l n="54" num="7.6"><w n="54.1">Mais</w> <w n="54.2">combien</w> <w n="54.3">doit</w> <w n="54.4">durer</w> <w n="54.5">un</w> <w n="54.6">bonheur</w> <w n="54.7">si</w> <w n="54.8">parfait</w> ?</l>
					</lg>
					<lg n="8">
						<l n="55" num="8.1"><w n="55.1">Ces</w> <w n="55.2">riches</w>, <w n="55.3">que</w> <w n="55.4">du</w> <w n="55.5">siècle</w> <w n="55.6">adore</w> <w n="55.7">l</w>’<w n="55.8">imprudence</w>,</l>
						<l n="56" num="8.2"><w n="56.1">Passent</w> <w n="56.2">comme</w> <w n="56.3">fumée</w> <w n="56.4">avec</w> <w n="56.5">leur</w> <w n="56.6">abondance</w>,</l>
						<l n="57" num="8.3"><w n="57.1">Et</w> <w n="57.2">de</w> <w n="57.3">leurs</w> <w n="57.4">voluptés</w> <w n="57.5">le</w> <w n="57.6">plus</w> <w n="57.7">doux</w> <w n="57.8">souvenir</w>,</l>
						<l n="58" num="8.4"><w n="58.1">S</w>’<w n="58.2">il</w> <w n="58.3">ne</w> <w n="58.4">passe</w> <w n="58.5">avec</w> <w n="58.6">eux</w>, <w n="58.7">ne</w> <w n="58.8">sert</w> <w n="58.9">qu</w>’<w n="58.10">à</w> <w n="58.11">les</w> <w n="58.12">punir</w>.</l>
						<l n="59" num="8.5"><w n="59.1">Celles</w> <w n="59.2">que</w> <w n="59.3">leur</w> <w n="59.4">permet</w> <w n="59.5">une</w> <w n="59.6">si</w> <w n="59.7">triste</w> <w n="59.8">vie</w></l>
						<l n="60" num="8.6"><w n="60.1">Sont</w> <w n="60.2">dignes</w> <w n="60.3">de</w> <w n="60.4">pitié</w> <w n="60.5">beaucoup</w> <w n="60.6">plus</w> <w n="60.7">que</w> <w n="60.8">d</w>’<w n="60.9">envie</w> :</l>
						<l n="61" num="8.7"><w n="61.1">Elles</w> <w n="61.2">vont</w> <w n="61.3">rarement</w> <w n="61.4">sans</w> <w n="61.5">mélange</w> <w n="61.6">d</w>’<w n="61.7">ennuis</w> ;</l>
						<l n="62" num="8.8"><w n="62.1">Leurs</w> <w n="62.2">jours</w> <w n="62.3">les</w> <w n="62.4">plus</w> <w n="62.5">brillants</w> <w n="62.6">ont</w> <w n="62.7">les</w> <w n="62.8">plus</w> <w n="62.9">sombres</w> <w n="62.10">nuits</w> ;</l>
						<l n="63" num="8.9"><w n="63.1">Souvent</w> <w n="63.2">mille</w> <w n="63.3">chagrins</w> <w n="63.4">empoisonnent</w> <w n="63.5">leurs</w> <w n="63.6">charmes</w>,</l>
						<l n="64" num="8.10"><w n="64.1">Souvent</w> <w n="64.2">mille</w> <w n="64.3">terreurs</w> <w n="64.4">y</w> <w n="64.5">jettent</w> <w n="64.6">mille</w> <w n="64.7">alarmes</w>,</l>
						<l n="65" num="8.11"><w n="65.1">Et</w> <w n="65.2">souvent</w> <w n="65.3">des</w> <w n="65.4">objets</w> <w n="65.5">d</w>’<w n="65.6">où</w> <w n="65.7">naissent</w> <w n="65.8">leurs</w> <w n="65.9">plaisirs</w></l>
						<l n="66" num="8.12"><w n="66.1">Ma</w> <w n="66.2">justice</w> <w n="66.3">en</w> <w n="66.4">courroux</w> <w n="66.5">fait</w> <w n="66.6">naître</w> <w n="66.7">leurs</w> <w n="66.8">soupirs</w>.</l>
						<l n="67" num="8.13"><w n="67.1">L</w>’<w n="67.2">impétuosité</w> <w n="67.3">qui</w> <w n="67.4">les</w> <w n="67.5">porte</w> <w n="67.6">aux</w> <w n="67.7">délices</w></l>
						<l n="68" num="8.14"><w n="68.1">Elle</w>-<w n="68.2">même</w> <w n="68.3">à</w> <w n="68.4">leur</w> <w n="68.5">joie</w> <w n="68.6">enchaîne</w> <w n="68.7">les</w> <w n="68.8">supplices</w>,</l>
						<l n="69" num="8.15"><w n="69.1">Et</w> <w n="69.2">joint</w> <w n="69.3">aux</w> <w n="69.4">vains</w> <w n="69.5">appas</w> <w n="69.6">d</w>’<w n="69.7">un</w> <w n="69.8">peu</w> <w n="69.9">d</w>’<w n="69.10">illusion</w></l>
						<l n="70" num="8.16"><w n="70.1">Le</w> <w n="70.2">repentir</w>, <w n="70.3">le</w> <w n="70.4">trouble</w> <w n="70.5">et</w> <w n="70.6">la</w> <w n="70.7">confusion</w>.</l>
					</lg>
					<lg n="9">
						<l n="71" num="9.1"><w n="71.1">Toutes</w> <w n="71.2">ces</w> <w n="71.3">voluptés</w> <w n="71.4">sont</w> <w n="71.5">courtes</w> <w n="71.6">et</w> <w n="71.7">menteuses</w>,</l>
						<l n="72" num="9.2"><w n="72.1">Toutes</w> <w n="72.2">n</w>’<w n="72.3">ont</w> <w n="72.4">que</w> <w n="72.5">désordre</w>, <w n="72.6">et</w> <w n="72.7">toutes</w> <w n="72.8">sont</w> <w n="72.9">honteuses</w>.</l>
						<l n="73" num="9.3"><w n="73.1">Les</w> <w n="73.2">hommes</w> <w n="73.3">cependant</w> <w n="73.4">n</w>’<w n="73.5">en</w> <w n="73.6">aperçoivent</w> <w n="73.7">rien</w> ;</l>
						<l n="74" num="9.4"><w n="74.1">Enivrés</w> <w n="74.2">qu</w>’<w n="74.3">ils</w> <w n="74.4">en</w> <w n="74.5">sont</w>, <w n="74.6">ils</w> <w n="74.7">en</w> <w n="74.8">font</w> <w n="74.9">tout</w> <w n="74.10">leur</w> <w n="74.11">bien</w> :</l>
						<l n="75" num="9.5"><w n="75.1">Ils</w> <w n="75.2">suivent</w> <w n="75.3">en</w> <w n="75.4">tous</w> <w n="75.5">lieux</w>, <w n="75.6">comme</w> <w n="75.7">bêtes</w> <w n="75.8">stupides</w>,</l>
						<l n="76" num="9.6"><w n="76.1">Leurs</w> <w n="76.2">sens</w> <w n="76.3">pour</w> <w n="76.4">souverains</w>, <w n="76.5">leurs</w> <w n="76.6">passions</w> <w n="76.7">pour</w> <w n="76.8">guides</w> ;</l>
						<l n="77" num="9.7"><w n="77.1">Et</w> <w n="77.2">pour</w> <w n="77.3">l</w>’<w n="77.4">indigne</w> <w n="77.5">attrait</w> <w n="77.6">d</w>’<w n="77.7">un</w> <w n="77.8">faux</w> <w n="77.9">chatouillement</w>,</l>
						<l n="78" num="9.8"><w n="78.1">Pour</w> <w n="78.2">un</w> <w n="78.3">bien</w> <w n="78.4">passager</w>, <w n="78.5">un</w> <w n="78.6">plaisir</w> <w n="78.7">d</w>’<w n="78.8">un</w> <w n="78.9">moment</w>,</l>
						<l n="79" num="9.9"><w n="79.1">Amoureux</w> <w n="79.2">d</w>’<w n="79.3">une</w> <w n="79.4">vie</w> <w n="79.5">ingrate</w> <w n="79.6">et</w> <w n="79.7">fugitive</w>,</l>
						<l n="80" num="9.10"><w n="80.1">Ils</w> <w n="80.2">acceptent</w> <w n="80.3">pour</w> <w n="80.4">l</w>’<w n="80.5">âme</w> <w n="80.6">une</w> <w n="80.7">mort</w> <w n="80.8">toujours</w> <w n="80.9">vive</w>,</l>
						<l n="81" num="9.11"><w n="81.1">Où</w> <w n="81.2">mourant</w> <w n="81.3">à</w> <w n="81.4">toute</w> <w n="81.5">heure</w>, <w n="81.6">et</w> <w n="81.7">ne</w> <w n="81.8">pouvant</w> <w n="81.9">mourir</w>,</l>
						<l n="82" num="9.12"><w n="82.1">Ils</w> <w n="82.2">ne</w> <w n="82.3">sont</w> <w n="82.4">immortels</w> <w n="82.5">que</w> <w n="82.6">pour</w> <w n="82.7">toujours</w> <w n="82.8">souffrir</w>.</l>
					</lg>
					<lg n="10">
						<l n="83" num="10.1"><w n="83.1">Plus</w> <w n="83.2">sage</w> <w n="83.3">à</w> <w n="83.4">leurs</w> <w n="83.5">dépens</w>, <w n="83.6">donne</w> <w n="83.7">moins</w> <w n="83.8">de</w> <w n="83.9">puissance</w></l>
						<l n="84" num="10.2"><w n="84.1">Aux</w> <w n="84.2">brutales</w> <w n="84.3">fureurs</w> <w n="84.4">de</w> <w n="84.5">ta</w> <w n="84.6">concupiscence</w> ;</l>
					</lg>
					<lg n="11">
						<l n="85" num="11.1"><w n="85.1">Garde</w>-<w n="85.2">toi</w> <w n="85.3">de</w> <w n="85.4">courir</w> <w n="85.5">après</w> <w n="85.6">les</w> <w n="85.7">voluptés</w>,</l>
						<l n="86" num="11.2"><w n="86.1">Captive</w> <w n="86.2">tes</w> <w n="86.3">desirs</w>, <w n="86.4">brise</w> <w n="86.5">tes</w> <w n="86.6">volontés</w>,</l>
						<l n="87" num="11.3"><w n="87.1">Mets</w> <w n="87.2">en</w> <w n="87.3">moi</w> <w n="87.4">seul</w> <w n="87.5">ta</w> <w n="87.6">joie</w>, <w n="87.7">et</w> <w n="87.8">m</w>’<w n="87.9">en</w> <w n="87.10">fais</w> <w n="87.11">une</w> <w n="87.12">offrande</w>,</l>
						<l n="88" num="11.4"><w n="88.1">Et</w> <w n="88.2">je</w> <w n="88.3">t</w>’<w n="88.4">accorderai</w> <w n="88.5">ce</w> <w n="88.6">que</w> <w n="88.7">ton</w> <w n="88.8">cœur</w> <w n="88.9">demande</w>.</l>
					</lg>
					<lg n="12">
						<l n="89" num="12.1"><w n="89.1">Oui</w>, <w n="89.2">ce</w> <w n="89.3">cœur</w> <w n="89.4">ainsi</w> <w n="89.5">libre</w>, <w n="89.6">ainsi</w> <w n="89.7">désabusé</w>,</l>
						<l n="90" num="12.2"><w n="90.1">Ne</w> <w n="90.2">peut</w>, <w n="90.3">quoi</w> <w n="90.4">qu</w>’<w n="90.5">il</w> <w n="90.6">demande</w>, <w n="90.7">en</w> <w n="90.8">être</w> <w n="90.9">refusé</w> ;</l>
						<l n="91" num="12.3"><w n="91.1">Et</w> <w n="91.2">si</w> <w n="91.3">tu</w> <w n="91.4">veux</w> <w n="91.5">goûter</w> <w n="91.6">des</w> <w n="91.7">plaisirs</w> <w n="91.8">véritables</w>,</l>
						<l n="92" num="12.4"><w n="92.1">Des</w> <w n="92.2">consolations</w> <w n="92.3">et</w> <w n="92.4">pleines</w> <w n="92.5">et</w> <w n="92.6">durables</w>,</l>
						<l n="93" num="12.5"><w n="93.1">Tu</w> <w n="93.2">n</w>’<w n="93.3">as</w> <w n="93.4">qu</w>’<w n="93.5">à</w> <w n="93.6">dédaigner</w> <w n="93.7">par</w> <w n="93.8">un</w> <w n="93.9">noble</w> <w n="93.10">mépris</w></l>
						<l n="94" num="12.6"><w n="94.1">Cet</w> <w n="94.2">éclat</w> <w n="94.3">dont</w> <w n="94.4">le</w> <w n="94.5">monde</w> <w n="94.6">éblouit</w> <w n="94.7">tant</w> <w n="94.8">d</w>’<w n="94.9">esprits</w> ;</l>
						<l n="95" num="12.7"><w n="95.1">Tu</w> <w n="95.2">n</w>’<w n="95.3">as</w> <w n="95.4">qu</w>’<w n="95.5">à</w> <w n="95.6">t</w>’<w n="95.7">arracher</w> <w n="95.8">à</w> <w n="95.9">ces</w> <w n="95.10">voluptés</w> <w n="95.11">basses</w></l>
						<l n="96" num="12.8"><w n="96.1">Qui</w> <w n="96.2">repoussent</w> <w n="96.3">des</w> <w n="96.4">cœurs</w> <w n="96.5">les</w> <w n="96.6">effets</w> <w n="96.7">de</w> <w n="96.8">mes</w> <w n="96.9">grâces</w> ;</l>
						<l n="97" num="12.9"><w n="97.1">Tu</w> <w n="97.2">n</w>’<w n="97.3">as</w> <w n="97.4">qu</w>’<w n="97.5">à</w> <w n="97.6">te</w> <w n="97.7">soustraire</w> <w n="97.8">à</w> <w n="97.9">leur</w> <w n="97.10">malignité</w>,</l>
						<l n="98" num="12.10"><w n="98.1">Et</w> <w n="98.2">je</w> <w n="98.3">te</w> <w n="98.4">rendrai</w> <w n="98.5">plus</w> <w n="98.6">que</w> <w n="98.7">tu</w> <w n="98.8">n</w>’<w n="98.9">auras</w> <w n="98.10">quitté</w>.</l>
						<l n="99" num="12.11"><w n="99.1">Plus</w> <w n="99.2">à</w> <w n="99.3">leurs</w> <w n="99.4">faux</w> <w n="99.5">attraits</w> <w n="99.6">tu</w> <w n="99.7">fermeras</w> <w n="99.8">de</w> <w n="99.9">portes</w>,</l>
						<l n="100" num="12.12"><w n="100.1">Plus</w> <w n="100.2">mes</w> <w n="100.3">faveurs</w> <w n="100.4">seront</w> <w n="100.5">et</w> <w n="100.6">charmantes</w> <w n="100.7">et</w> <w n="100.8">fortes</w> ;</l>
						<l n="101" num="12.13"><w n="101.1">Et</w> <w n="101.2">moins</w> <w n="101.3">la</w> <w n="101.4">créature</w> <w n="101.5">aura</w> <w n="101.6">chez</w> <w n="101.7">toi</w> <w n="101.8">d</w>’<w n="101.9">accès</w>,</l>
						<l n="102" num="12.14"><w n="102.1">Et</w> <w n="102.2">plus</w> <w n="102.3">du</w> <w n="102.4">créateur</w> <w n="102.5">les</w> <w n="102.6">dons</w> <w n="102.7">auront</w> <w n="102.8">d</w>’<w n="102.9">excès</w>.</l>
					</lg>
					<lg n="13">
						<l n="103" num="13.1"><w n="103.1">Ne</w> <w n="103.2">crois</w> <w n="103.3">pas</w> <w n="103.4">toutefois</w> <w n="103.5">sans</w> <w n="103.6">peine</w> <w n="103.7">et</w> <w n="103.8">sans</w> <w n="103.9">tristesse</w></l>
						<l n="104" num="13.2"><w n="104.1">À</w> <w n="104.2">ce</w> <w n="104.3">détachement</w> <w n="104.4">élever</w> <w n="104.5">ta</w> <w n="104.6">foiblesse</w> :</l>
						<l n="105" num="13.3"><w n="105.1">Une</w> <w n="105.2">vieille</w> <w n="105.3">habitude</w> <w n="105.4">y</w> <w n="105.5">voudra</w> <w n="105.6">résister</w>,</l>
						<l n="106" num="13.4"><w n="106.1">Mais</w> <w n="106.2">par</w> <w n="106.3">une</w> <w n="106.4">meilleure</w> <w n="106.5">il</w> <w n="106.6">faudra</w> <w n="106.7">la</w> <w n="106.8">dompter</w> ;</l>
					</lg>
					<lg n="14">
						<l n="107" num="14.1"><w n="107.1">Ta</w> <w n="107.2">chair</w> <w n="107.3">murmurera</w>, <w n="107.4">mais</w> <w n="107.5">de</w> <w n="107.6">tout</w> <w n="107.7">son</w> <w n="107.8">murmure</w></l>
						<l n="108" num="14.2"><w n="108.1">La</w> <w n="108.2">ferveur</w> <w n="108.3">de</w> <w n="108.4">l</w>’<w n="108.5">esprit</w> <w n="108.6">convaincra</w> <w n="108.7">l</w>’<w n="108.8">imposture</w> ;</l>
						<l n="109" num="14.3"><w n="109.1">Enfin</w> <w n="109.2">le</w> <w n="109.3">vieux</w> <w n="109.4">serpent</w> <w n="109.5">tâchera</w> <w n="109.6">de</w> <w n="109.7">t</w>’<w n="109.8">aigrir</w></l>
						<l n="110" num="14.4"><w n="110.1">Contre</w> <w n="110.2">les</w> <w n="110.3">moindres</w> <w n="110.4">maux</w> <w n="110.5">que</w> <w n="110.6">tu</w> <w n="110.7">voudras</w> <w n="110.8">souffrir</w> ;</l>
						<l n="111" num="14.5"><w n="111.1">Il</w> <w n="111.2">fera</w> <w n="111.3">mille</w> <w n="111.4">efforts</w> <w n="111.5">pour</w> <w n="111.6">brouiller</w> <w n="111.7">ta</w> <w n="111.8">conduite</w> ;</l>
						<l n="112" num="14.6"><w n="112.1">Mais</w> <w n="112.2">avec</w> <w n="112.3">l</w>’<w n="112.4">oraison</w> <w n="112.5">tu</w> <w n="112.6">le</w> <w n="112.7">mettras</w> <w n="112.8">en</w> <w n="112.9">fuite</w>,</l>
						<l n="113" num="14.7"><w n="113.1">Et</w> <w n="113.2">l</w>’<w n="113.3">obstination</w> <w n="113.4">d</w>’<w n="113.5">un</w> <w n="113.6">saint</w> <w n="113.7">et</w> <w n="113.8">digne</w> <w n="113.9">emploi</w></l>
						<l n="114" num="14.8"><w n="114.1">Ne</w> <w n="114.2">lui</w> <w n="114.3">laissera</w> <w n="114.4">plus</w> <w n="114.5">aucun</w> <w n="114.6">pouvoir</w> <w n="114.7">sur</w> <w n="114.8">toi</w>.</l>
					</lg>
				</div></body></text></TEI>