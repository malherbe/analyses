<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Imitation De Jésus-Christ</title>
				<title type="medium">Édition électronique</title>
				<author key="COR">
					<name>
						<forename>Pierre</forename>
						<surname>CORNEILLE</surname>
					</name>
					<date from="1606" to="1684">1606-1684</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>13205 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2016">2016</date>
				<idno type="local">COR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierrecorneilleimmitadiondejesuschrist.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Imitation De Jésus-Christ</title>
						<author>Pierre Corneille</author>
						<editor>M. Ch. Marty-Laveaux</editor>
						<edition>nouvelle édition</edition>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k51199.r=pierre%20corneille?rk=85837;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Librairie de L. Hachette et Cie</publisher>
							<date when="1862">1862</date>
						</imprint>
						<biblScope unit="tome">Tome 8</biblScope>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1656">1656</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les chiffres arabes des livres et chapitres ont été remplacés par des chiffres romains</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
	</teiHeader><text><body><head type="main_part">LIVRE QUATRIÈME</head><div type="poem" key="COR136">
					<head type="main">CHAPITRE VII</head>
					<head type="sub">De l’examen de sa conscience, et du propos de s’amender.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Prêtre</w>, <w n="1.2">qui</w> <w n="1.3">que</w> <w n="1.4">tu</w> <w n="1.5">sois</w>, <w n="1.6">qui</w> <w n="1.7">vas</w> <w n="1.8">sur</w> <w n="1.9">mon</w> <w n="1.10">autel</w></l>
						<l n="2" num="1.2"><w n="2.1">Offrir</w> <w n="2.2">un</w> <w n="2.3">Dieu</w> <w n="2.4">vivant</w> <w n="2.5">à</w> <w n="2.6">son</w> <w n="2.7">père</w> <w n="2.8">immortel</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">tenir</w> <w n="3.3">en</w> <w n="3.4">tes</w> <w n="3.5">mains</w> <w n="3.6">et</w> <w n="3.7">recevoir</w> <w n="3.8">toi</w>-<w n="3.9">même</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">mon</w> <w n="4.3">amour</w> <w n="4.4">pour</w> <w n="4.5">toi</w> <w n="4.6">le</w> <w n="4.7">mystère</w> <w n="4.8">suprême</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Approche</w>, <w n="5.2">mais</w> <w n="5.3">surtout</w> <w n="5.4">prépare</w> <w n="5.5">dans</w> <w n="5.6">ton</w> <w n="5.7">sein</w></l>
						<l n="6" num="1.6"><w n="6.1">Une</w> <w n="6.2">humilité</w> <w n="6.3">forte</w>, <w n="6.4">un</w> <w n="6.5">respect</w> <w n="6.6">souverain</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Une</w> <w n="7.2">foi</w> <w n="7.3">pleine</w> <w n="7.4">et</w> <w n="7.5">ferme</w>, <w n="7.6">une</w> <w n="7.7">intention</w> <w n="7.8">pure</w></l>
						<l n="8" num="1.8"><w n="8.1">D</w>’<w n="8.2">honorer</w>, <w n="8.3">de</w> <w n="8.4">bénir</w> <w n="8.5">l</w>’<w n="8.6">auteur</w> <w n="8.7">de</w> <w n="8.8">la</w> <w n="8.9">nature</w> :</l>
						<l n="9" num="1.9"><w n="9.1">Sur</w> <w n="9.2">ton</w> <w n="9.3">intérieur</w> <w n="9.4">jette</w> <w n="9.5">l</w>’<w n="9.6">œil</w> <w n="9.7">avec</w> <w n="9.8">soin</w>,</l>
						<l n="10" num="1.10"><w n="10.1">En</w> <w n="10.2">juge</w> <w n="10.3">incorruptible</w>, <w n="10.4">en</w> <w n="10.5">fidèle</w> <w n="10.6">témoin</w> ;</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">si</w> <w n="11.3">de</w> <w n="11.4">mon</w> <w n="11.5">honneur</w> <w n="11.6">un</w> <w n="11.7">vrai</w> <w n="11.8">souci</w> <w n="11.9">te</w> <w n="11.10">touche</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Fais</w> <w n="12.2">que</w> <w n="12.3">le</w> <w n="12.4">cœur</w> <w n="12.5">contrit</w> <w n="12.6">et</w> <w n="12.7">l</w>’<w n="12.8">humble</w> <w n="12.9">aveu</w> <w n="12.10">de</w> <w n="12.11">bouche</w></l>
						<l n="13" num="1.13"><w n="13.1">Sachent</w> <w n="13.2">si</w> <w n="13.3">bien</w> <w n="13.4">purger</w> <w n="13.5">le</w> <w n="13.6">désordre</w> <w n="13.7">caché</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Que</w> <w n="14.2">rien</w> <w n="14.3">par</w> <w n="14.4">le</w> <w n="14.5">remords</w> <w n="14.6">ne</w> <w n="14.7">te</w> <w n="14.8">soit</w> <w n="14.9">reproché</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Que</w> <w n="15.2">rien</w> <w n="15.3">plus</w> <w n="15.4">ne</w> <w n="15.5">te</w> <w n="15.6">pèse</w>, <w n="15.7">et</w> <w n="15.8">que</w> <w n="15.9">rien</w> <w n="15.10">que</w> <w n="15.11">tu</w> <w n="15.12">saches</w></l>
						<l n="16" num="1.16"><w n="16.1">N</w>’<w n="16.2">empêche</w> <w n="16.3">un</w> <w n="16.4">libre</w> <w n="16.5">accès</w> <w n="16.6">par</w> <w n="16.7">ses</w> <w n="16.8">honteuses</w> <w n="16.9">taches</w>.</l>
					</lg>
					<lg n="2">
						<l n="17" num="2.1"><w n="17.1">Porte</w> <w n="17.2">empreint</w> <w n="17.3">sur</w> <w n="17.4">ce</w> <w n="17.5">cœur</w> <w n="17.6">un</w> <w n="17.7">regret</w> <w n="17.8">général</w></l>
						<l n="18" num="2.2"><w n="18.1">Pour</w> <w n="18.2">tout</w> <w n="18.3">ce</w> <w n="18.4">que</w> <w n="18.5">jamais</w> <w n="18.6">il</w> <w n="18.7">a</w> <w n="18.8">commis</w> <w n="18.9">de</w> <w n="18.10">mal</w> ;</l>
						<l n="19" num="2.3"><w n="19.1">Joins</w> <w n="19.2">à</w> <w n="19.3">ce</w> <w n="19.4">déplaisir</w> <w n="19.5">des</w> <w n="19.6">douleurs</w> <w n="19.7">singulières</w></l>
						<l n="20" num="2.4"><w n="20.1">Pour</w> <w n="20.2">les</w> <w n="20.3">infirmités</w> <w n="20.4">qui</w> <w n="20.5">te</w> <w n="20.6">sont</w> <w n="20.7">journalières</w> ;</l>
						<l n="21" num="2.5"><w n="21.1">Et</w> <w n="21.2">si</w> <w n="21.3">l</w>’<w n="21.4">heure</w> <w n="21.5">le</w> <w n="21.6">souffre</w>, <w n="21.7">en</w> <w n="21.8">secret</w> <w n="21.9">devant</w> <w n="21.10">Dieu</w></l>
						<l n="22" num="2.6"><w n="22.1">Repasses</w>-<w n="22.2">en</w> <w n="22.3">le</w> <w n="22.4">nombre</w>, <w n="22.5">et</w> <w n="22.6">le</w> <w n="22.7">temps</w>, <w n="22.8">et</w> <w n="22.9">le</w> <w n="22.10">lieu</w> ;</l>
						<l n="23" num="2.7"><w n="23.1">Et</w> <w n="23.2">de</w> <w n="23.3">tous</w> <w n="23.4">les</w> <w n="23.5">défauts</w> <w n="23.6">où</w> <w n="23.7">ton</w> <w n="23.8">âme</w> <w n="23.9">s</w>’<w n="23.10">engage</w>,</l>
						<l n="24" num="2.8"><w n="24.1">Étends</w> <w n="24.2">devant</w> <w n="24.3">ses</w> <w n="24.4">yeux</w> <w n="24.5">la</w> <w n="24.6">pitoyable</w> <w n="24.7">image</w>.</l>
					</lg>
					<lg n="3">
						<l n="25" num="3.1"><w n="25.1">Gémis</w>, <w n="25.2">soupire</w>, <w n="25.3">pleure</w> <w n="25.4">aux</w> <w n="25.5">pieds</w> <w n="25.6">de</w> <w n="25.7">l</w>’<w n="25.8">éternel</w>,</l>
						<l n="26" num="3.2"><w n="26.1">D</w>’<w n="26.2">être</w> <w n="26.3">encor</w> <w n="26.4">si</w> <w n="26.5">mondain</w>, <w n="26.6">d</w>’<w n="26.7">être</w> <w n="26.8">encor</w> <w n="26.9">si</w> <w n="26.10">charnel</w>,</l>
						<l n="27" num="3.3"><w n="27.1">D</w>’<w n="27.2">avoir</w> <w n="27.3">des</w> <w n="27.4">passions</w> <w n="27.5">si</w> <w n="27.6">peu</w> <w n="27.7">mortifiées</w>,</l>
						<l n="28" num="3.4"><w n="28.1">Des</w> <w n="28.2">inclinations</w> <w n="28.3">si</w> <w n="28.4">mal</w> <w n="28.5">purifiées</w>,</l>
						<l n="29" num="3.5"><w n="29.1">Que</w> <w n="29.2">les</w> <w n="29.3">mauvais</w> <w n="29.4">desirs</w> <w n="29.5">demeurent</w> <w n="29.6">tout</w>-<w n="29.7">puissants</w></l>
						<l n="30" num="3.6"><w n="30.1">Sur</w> <w n="30.2">qui</w> <w n="30.3">veille</w> <w n="30.4">si</w> <w n="30.5">mal</w> <w n="30.6">à</w> <w n="30.7">la</w> <w n="30.8">garde</w> <w n="30.9">des</w> <w n="30.10">sens</w>.</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1"><w n="31.1">Gémis</w> <w n="31.2">d</w>’<w n="31.3">en</w> <w n="31.4">voir</w> <w n="31.5">souvent</w> <w n="31.6">les</w> <w n="31.7">approches</w> <w n="31.8">saisies</w></l>
						<l n="32" num="4.2"><w n="32.1">Par</w> <w n="32.2">les</w> <w n="32.3">vains</w> <w n="32.4">embarras</w> <w n="32.5">de</w> <w n="32.6">tant</w> <w n="32.7">de</w> <w n="32.8">fantaisies</w>,</l>
						<l n="33" num="4.3"><w n="33.1">D</w>’<w n="33.2">avoir</w> <w n="33.3">pour</w> <w n="33.4">le</w> <w n="33.5">dehors</w> <w n="33.6">tant</w> <w n="33.7">de</w> <w n="33.8">soupirs</w> <w n="33.9">ardents</w>,</l>
						<l n="34" num="4.4"><w n="34.1">Et</w> <w n="34.2">si</w> <w n="34.3">peu</w> <w n="34.4">de</w> <w n="34.5">retour</w> <w n="34.6">aux</w> <w n="34.7">choses</w> <w n="34.8">du</w> <w n="34.9">dedans</w> ;</l>
						<l n="35" num="4.5"><w n="35.1">De</w> <w n="35.2">souffrir</w> <w n="35.3">que</w> <w n="35.4">ton</w> <w n="35.5">âme</w> <w n="35.6">à</w> <w n="35.7">toute</w> <w n="35.8">heure</w> <w n="35.9">n</w>’<w n="35.10">aspire</w></l>
						<l n="36" num="4.6"><w n="36.1">Qu</w>’<w n="36.2">à</w> <w n="36.3">ce</w> <w n="36.4">qui</w> <w n="36.5">divertit</w>, <w n="36.6">qu</w>’<w n="36.7">à</w> <w n="36.8">ce</w> <w n="36.9">qui</w> <w n="36.10">te</w> <w n="36.11">fait</w> <w n="36.12">rire</w>,</l>
						<l n="37" num="4.7"><w n="37.1">Tandis</w> <w n="37.2">que</w> <w n="37.3">pour</w> <w n="37.4">les</w> <w n="37.5">pleurs</w> <w n="37.6">et</w> <w n="37.7">la</w> <w n="37.8">componction</w></l>
						<l n="38" num="4.8"><w n="38.1">Ton</w> <w n="38.2">endurcissement</w> <w n="38.3">a</w> <w n="38.4">tant</w> <w n="38.5">d</w>’<w n="38.6">aversion</w> ;</l>
					</lg>
					<lg n="5">
						<l n="39" num="5.1"><w n="39.1">De</w> <w n="39.2">te</w> <w n="39.3">voir</w> <w n="39.4">tant</w> <w n="39.5">de</w> <w n="39.6">pente</w> <w n="39.7">à</w> <w n="39.8">vivre</w> <w n="39.9">plus</w> <w n="39.10">au</w> <w n="39.11">large</w>,</l>
						<l n="40" num="5.2"><w n="40.1">Dans</w> <w n="40.2">l</w>’<w n="40.3">aise</w> <w n="40.4">et</w> <w n="40.5">les</w> <w n="40.6">plaisirs</w> <w n="40.7">d</w>’<w n="40.8">une</w> <w n="40.9">chair</w> <w n="40.10">qui</w> <w n="40.11">te</w> <w n="40.12">charge</w>,</l>
						<l n="41" num="5.3"><w n="41.1">Cependant</w> <w n="41.2">que</w> <w n="41.3">ton</w> <w n="41.4">cœur</w> <w n="41.5">a</w> <w n="41.6">tant</w> <w n="41.7">de</w> <w n="41.8">lâcheté</w></l>
						<l n="42" num="5.4"><w n="42.1">Pour</w> <w n="42.2">la</w> <w n="42.3">ferveur</w> <w n="42.4">du</w> <w n="42.5">zèle</w> <w n="42.6">et</w> <w n="42.7">pour</w> <w n="42.8">l</w>’<w n="42.9">austérité</w> ;</l>
						<l n="43" num="5.5"><w n="43.1">D</w>’<w n="43.2">être</w> <w n="43.3">si</w> <w n="43.4">curieux</w> <w n="43.5">d</w>’<w n="43.6">entendre</w> <w n="43.7">des</w> <w n="43.8">nouvelles</w>,</l>
						<l n="44" num="5.6"><w n="44.1">De</w> <w n="44.2">voir</w> <w n="44.3">des</w> <w n="44.4">raretés</w> <w n="44.5">surprenantes</w> <w n="44.6">et</w> <w n="44.7">belles</w>,</l>
						<l n="45" num="5.7"><w n="45.1">Et</w> <w n="45.2">si</w> <w n="45.3">lent</w> <w n="45.4">à</w> <w n="45.5">choisir</w> <w n="45.6">de</w> <w n="45.7">ces</w> <w n="45.8">emplois</w> <w n="45.9">abjets</w></l>
						<l n="46" num="5.8"><w n="46.1">Que</w> <w n="46.2">prend</w> <w n="46.3">l</w>’<w n="46.4">humilité</w> <w n="46.5">pour</w> <w n="46.6">ses</w> <w n="46.7">plus</w> <w n="46.8">doux</w> <w n="46.9">objets</w>.</l>
					</lg>
					<lg n="6">
						<l n="47" num="6.1"><w n="47.1">Gémis</w> <w n="47.2">de</w> <w n="47.3">tant</w> <w n="47.4">d</w>’<w n="47.5">ardeur</w> <w n="47.6">pour</w> <w n="47.7">amasser</w> <w n="47.8">et</w> <w n="47.9">prendre</w>,</l>
						<l n="48" num="6.2"><w n="48.1">Et</w> <w n="48.2">de</w> <w n="48.3">tant</w> <w n="48.4">de</w> <w n="48.5">réserve</w> <w n="48.6">à</w> <w n="48.7">départir</w> <w n="48.8">ou</w> <w n="48.9">rendre</w>,</l>
						<l n="49" num="6.3"><w n="49.1">Qu</w>’<w n="49.2">on</w> <w n="49.3">a</w> <w n="49.4">raison</w> <w n="49.5">de</w> <w n="49.6">croire</w> <w n="49.7">et</w> <w n="49.8">de</w> <w n="49.9">te</w> <w n="49.10">reprocher</w></l>
						<l n="50" num="6.4"><w n="50.1">Que</w> <w n="50.2">ce</w> <w n="50.3">que</w> <w n="50.4">tient</w> <w n="50.5">ta</w> <w n="50.6">main</w> <w n="50.7">ne</w> <w n="50.8">s</w>’<w n="50.9">en</w> <w n="50.10">peut</w> <w n="50.11">détacher</w>.</l>
					</lg>
					<lg n="7">
						<l n="51" num="7.1"><w n="51.1">Pleure</w> <w n="51.2">ton</w> <w n="51.3">peu</w> <w n="51.4">de</w> <w n="51.5">soin</w> <w n="51.6">à</w> <w n="51.7">régler</w> <w n="51.8">tes</w> <w n="51.9">paroles</w>,</l>
						<l n="52" num="7.2"><w n="52.1">Ton</w> <w n="52.2">silence</w> <w n="52.3">rempli</w> <w n="52.4">d</w>’<w n="52.5">égarements</w> <w n="52.6">frivoles</w>,</l>
						<l n="53" num="7.3"><w n="53.1">Le</w> <w n="53.2">peu</w> <w n="53.3">d</w>’<w n="53.4">ordre</w> <w n="53.5">en</w> <w n="53.6">tes</w> <w n="53.7">mœurs</w>, <w n="53.8">le</w> <w n="53.9">peu</w> <w n="53.10">de</w> <w n="53.11">jugement</w></l>
						<l n="54" num="7.4"><w n="54.1">Que</w> <w n="54.2">dans</w> <w n="54.3">tes</w> <w n="54.4">actions</w> <w n="54.5">fait</w> <w n="54.6">voir</w> <w n="54.7">chaque</w> <w n="54.8">moment</w>.</l>
						<l n="55" num="7.5"><w n="55.1">Gémis</w> <w n="55.2">d</w>’<w n="55.3">avoir</w> <w n="55.4">aimé</w> <w n="55.5">les</w> <w n="55.6">plaisirs</w> <w n="55.7">de</w> <w n="55.8">la</w> <w n="55.9">table</w>,</l>
						<l n="56" num="7.6"><w n="56.1">Et</w> <w n="56.2">fait</w> <w n="56.3">la</w> <w n="56.4">sourde</w> <w n="56.5">oreille</w> <w n="56.6">à</w> <w n="56.7">ma</w> <w n="56.8">voix</w> <w n="56.9">adorable</w> ;</l>
						<l n="57" num="7.7"><w n="57.1">D</w>’<w n="57.2">avoir</w> <w n="57.3">pris</w> <w n="57.4">pour</w> <w n="57.5">vrai</w> <w n="57.6">bien</w> <w n="57.7">la</w> <w n="57.8">molle</w> <w n="57.9">oisiveté</w>,</l>
						<l n="58" num="7.8"><w n="58.1">D</w>’<w n="58.2">avoir</w> <w n="58.3">pris</w> <w n="58.4">le</w> <w n="58.5">travail</w> <w n="58.6">pour</w> <w n="58.7">infélicité</w> ;</l>
						<l n="59" num="7.9"><w n="59.1">Pour</w> <w n="59.2">des</w> <w n="59.3">contes</w> <w n="59.4">en</w> <w n="59.5">l</w>’<w n="59.6">air</w> <w n="59.7">eu</w> <w n="59.8">vigilance</w> <w n="59.9">entière</w>,</l>
						<l n="60" num="7.10"><w n="60.1">Long</w> <w n="60.2">assoupissement</w> <w n="60.3">pour</w> <w n="60.4">la</w> <w n="60.5">sainte</w> <w n="60.6">prière</w>,</l>
						<l n="61" num="7.11"><w n="61.1">Hâte</w> <w n="61.2">d</w>’<w n="61.3">être</w> <w n="61.4">à</w> <w n="61.5">la</w> <w n="61.6">fin</w>, <w n="61.7">et</w> <w n="61.8">l</w>’<w n="61.9">esprit</w> <w n="61.10">vagabond</w></l>
					</lg>
					<lg n="8">
						<l n="62" num="8.1"><w n="62.1">Vers</w> <w n="62.2">ce</w> <w n="62.3">qu</w>’<w n="62.4">il</w> <w n="62.5">ne</w> <w n="62.6">fait</w> <w n="62.7">pas</w> <w n="62.8">ou</w> <w n="62.9">que</w> <w n="62.10">les</w> <w n="62.11">autres</w> <w n="62.12">font</w>.</l>
						<l n="63" num="8.2"><w n="63.1">Pleure</w> <w n="63.2">ta</w> <w n="63.3">nonchalance</w> <w n="63.4">à</w> <w n="63.5">rendre</w> <w n="63.6">ton</w> <w n="63.7">office</w>,</l>
						<l n="64" num="8.3"><w n="64.1">Gémis</w> <w n="64.2">de</w> <w n="64.3">ta</w> <w n="64.4">tiédeur</w> <w n="64.5">pendant</w> <w n="64.6">ton</w> <w n="64.7">sacrifice</w>,</l>
						<l n="65" num="8.4"><w n="65.1">De</w> <w n="65.2">tant</w> <w n="65.3">d</w>’<w n="65.4">aridité</w> <w n="65.5">dans</w> <w n="65.6">tes</w> <w n="65.7">communions</w>,</l>
						<l n="66" num="8.5"><w n="66.1">De</w> <w n="66.2">tant</w> <w n="66.3">de</w> <w n="66.4">complaisance</w> <w n="66.5">en</w> <w n="66.6">tes</w> <w n="66.7">distractions</w> ;</l>
						<l n="67" num="8.6"><w n="67.1">D</w>’<w n="67.2">avoir</w> <w n="67.3">si</w> <w n="67.4">rarement</w> <w n="67.5">l</w>’<w n="67.6">âme</w> <w n="67.7">bien</w> <w n="67.8">recueillie</w>,</l>
						<l n="68" num="8.7"><w n="68.1">De</w> <w n="68.2">faire</w> <w n="68.3">hors</w> <w n="68.4">de</w> <w n="68.5">toi</w> <w n="68.6">toujours</w> <w n="68.7">quelque</w> <w n="68.8">saillie</w>,</l>
						<l n="69" num="8.8"><w n="69.1">Prompt</w> <w n="69.2">à</w> <w n="69.3">te</w> <w n="69.4">courroucer</w>, <w n="69.5">prompt</w> <w n="69.6">à</w> <w n="69.7">fâcher</w> <w n="69.8">autrui</w>,</l>
						<l n="70" num="8.9"><w n="70.1">Sévère</w> <w n="70.2">à</w> <w n="70.3">le</w> <w n="70.4">reprendre</w>, <w n="70.5">et</w> <w n="70.6">juger</w> <w n="70.7">mal</w> <w n="70.8">de</w> <w n="70.9">lui</w>.</l>
						<l n="71" num="8.10"><w n="71.1">Pleure</w> <w n="71.2">l</w>’<w n="71.3">emportement</w> <w n="71.4">de</w> <w n="71.5">tes</w> <w n="71.6">humeurs</w> <w n="71.7">diverses</w>,</l>
						<l n="72" num="8.11"><w n="72.1">Qu</w>’<w n="72.2">enflent</w> <w n="72.3">les</w> <w n="72.4">bons</w> <w n="72.5">succès</w>, <w n="72.6">qu</w>’<w n="72.7">abattent</w> <w n="72.8">les</w> <w n="72.9">traverses</w> ;</l>
						<l n="73" num="8.12"><w n="73.1">Pleure</w> <w n="73.2">enfin</w> <w n="73.3">ta</w> <w n="73.4">misère</w>, <w n="73.5">et</w> <w n="73.6">l</w>’<w n="73.7">ouvrage</w> <w n="73.8">imparfait</w></l>
						<l n="74" num="8.13"><w n="74.1">De</w> <w n="74.2">tant</w> <w n="74.3">de</w> <w n="74.4">bons</w> <w n="74.5">desseins</w> <w n="74.6">que</w> <w n="74.7">suit</w> <w n="74.8">si</w> <w n="74.9">peu</w> <w n="74.10">d</w>’<w n="74.11">effet</w>.</l>
					</lg>
					<lg n="9">
						<l n="75" num="9.1"><w n="75.1">Ces</w> <w n="75.2">défauts</w> <w n="75.3">déplorés</w>, <w n="75.4">et</w> <w n="75.5">tout</w> <w n="75.6">ce</w> <w n="75.7">qui</w> <w n="75.8">t</w>’<w n="75.9">en</w> <w n="75.10">reste</w>,</l>
						<l n="76" num="9.2"><w n="76.1">Avec</w> <w n="76.2">un</w> <w n="76.3">vif</w> <w n="76.4">regret</w> <w n="76.5">d</w>’<w n="76.6">un</w> <w n="76.7">cœur</w> <w n="76.8">qui</w> <w n="76.9">les</w> <w n="76.10">déteste</w>,</l>
						<l n="77" num="9.3"><w n="77.1">Avec</w> <w n="77.2">de</w> <w n="77.3">ta</w> <w n="77.4">foiblesse</w> <w n="77.5">un</w> <w n="77.6">aveu</w> <w n="77.7">douloureux</w>,</l>
						<l n="78" num="9.4"><w n="78.1">D</w>’<w n="78.2">où</w> <w n="78.3">naisse</w> <w n="78.4">un</w> <w n="78.5">déplaisir</w> <w n="78.6">cuisant</w>, <w n="78.7">mais</w> <w n="78.8">amoureux</w>,</l>
						<l n="79" num="9.5"><w n="79.1">Passe</w> <w n="79.2">au</w> <w n="79.3">ferme</w> <w n="79.4">propos</w> <w n="79.5">de</w> <w n="79.6">corriger</w> <w n="79.7">ta</w> <w n="79.8">vie</w>,</l>
						<l n="80" num="9.6"><w n="80.1">D</w>’<w n="80.2">avancer</w> <w n="80.3">aux</w> <w n="80.4">vertus</w> <w n="80.5">où</w> <w n="80.6">ma</w> <w n="80.7">voix</w> <w n="80.8">te</w> <w n="80.9">convie</w>,</l>
						<l n="81" num="9.7"><w n="81.1">D</w>’<w n="81.2">élever</w> <w n="81.3">tes</w> <w n="81.4">desirs</w> <w n="81.5">sans</w> <w n="81.6">plus</w> <w n="81.7">les</w> <w n="81.8">ravaler</w>,</l>
					</lg>
					<lg n="10">
						<l n="82" num="10.1"><w n="82.1">D</w>’<w n="82.2">aller</w> <w n="82.3">de</w> <w n="82.4">mieux</w> <w n="82.5">en</w> <w n="82.6">mieux</w> <w n="82.7">sans</w> <w n="82.8">jamais</w> <w n="82.9">reculer</w> ;</l>
						<l n="83" num="10.2"><w n="83.1">Puis</w> <w n="83.2">d</w>’<w n="83.3">une</w> <w n="83.4">volonté</w> <w n="83.5">fortement</w> <w n="83.6">résignée</w>,</l>
						<l n="84" num="10.3"><w n="84.1">Qui</w> <w n="84.2">tienne</w> <w n="84.3">sous</w> <w n="84.4">tes</w> <w n="84.5">pieds</w> <w n="84.6">la</w> <w n="84.7">terre</w> <w n="84.8">dédaignée</w>,</l>
						<l n="85" num="10.4"><w n="85.1">Offre</w>-<w n="85.2">toi</w> <w n="85.3">tout</w> <w n="85.4">entier</w> <w n="85.5">toi</w>-<w n="85.6">même</w> <w n="85.7">en</w> <w n="85.8">mon</w> <w n="85.9">honneur</w></l>
						<l n="86" num="10.5"><w n="86.1">Pour</w> <w n="86.2">holocauste</w> <w n="86.3">pur</w> <w n="86.4">sur</w> <w n="86.5">l</w>’<w n="86.6">autel</w> <w n="86.7">de</w> <w n="86.8">ton</w> <w n="86.9">cœur</w> ;</l>
						<l n="87" num="10.6"><w n="87.1">Remets</w> <w n="87.2">entre</w> <w n="87.3">mes</w> <w n="87.4">mains</w> <w n="87.5">et</w> <w n="87.6">ton</w> <w n="87.7">corps</w> <w n="87.8">et</w> <w n="87.9">ton</w> <w n="87.10">âme</w>,</l>
						<l n="88" num="10.7"><w n="88.1">Afin</w> <w n="88.2">que</w> <w n="88.3">tout</w> <w n="88.4">rempli</w> <w n="88.5">d</w>’<w n="88.6">une</w> <w n="88.7">céleste</w> <w n="88.8">flamme</w>,</l>
						<l n="89" num="10.8"><w n="89.1">Tu</w> <w n="89.2">sois</w> <w n="89.3">en</w> <w n="89.4">digne</w> <w n="89.5">état</w> <w n="89.6">par</w> <w n="89.7">cet</w> <w n="89.8">humble</w> <w n="89.9">devoir</w></l>
						<l n="90" num="10.9"><w n="90.1">De</w> <w n="90.2">consacrer</w> <w n="90.3">mon</w> <w n="90.4">corps</w> <w n="90.5">et</w> <w n="90.6">de</w> <w n="90.7">le</w> <w n="90.8">recevoir</w>.</l>
					</lg>
					<lg n="11">
						<l n="91" num="11.1"><w n="91.1">Car</w>, <w n="91.2">si</w> <w n="91.3">tu</w> <w n="91.4">ne</w> <w n="91.5">le</w> <w n="91.6">sais</w>, <w n="91.7">pour</w> <w n="91.8">plaire</w> <w n="91.9">au</w> <w n="91.10">dieu</w> <w n="91.11">qui</w> <w n="91.12">t</w>’<w n="91.13">aime</w>,</l>
						<l n="92" num="11.2"><w n="92.1">L</w>’<w n="92.2">offrande</w> <w n="92.3">la</w> <w n="92.4">plus</w> <w n="92.5">digne</w> <w n="92.6">est</w> <w n="92.7">celle</w> <w n="92.8">de</w> <w n="92.9">toi</w>-<w n="92.10">même</w> :</l>
						<l n="93" num="11.3"><w n="93.1">C</w>’<w n="93.2">est</w> <w n="93.3">elle</w> <w n="93.4">qu</w>’<w n="93.5">il</w> <w n="93.6">faut</w> <w n="93.7">joindre</w> <w n="93.8">à</w> <w n="93.9">celle</w> <w n="93.10">de</w> <w n="93.11">mon</w> <w n="93.12">corps</w></l>
						<l n="94" num="11.4"><w n="94.1">Par</w> <w n="94.2">d</w>’<w n="94.3">amoureux</w> <w n="94.4">élans</w>, <w n="94.5">par</w> <w n="94.6">de</w> <w n="94.7">sacrés</w> <w n="94.8">transports</w>,</l>
						<l n="95" num="11.5"><w n="95.1">Qui</w> <w n="95.2">puissent</w> <w n="95.3">jusqu</w>’<w n="95.4">à</w> <w n="95.5">moi</w> <w n="95.6">les</w> <w n="95.7">élever</w> <w n="95.8">unies</w>,</l>
						<l n="96" num="11.6"><w n="96.1">Et</w> <w n="96.2">quand</w> <w n="96.3">tu</w> <w n="96.4">dis</w> <w n="96.5">la</w> <w n="96.6">messe</w>, <w n="96.7">et</w> <w n="96.8">quand</w> <w n="96.9">tu</w> <w n="96.10">communies</w>.</l>
						<l n="97" num="11.7"><w n="97.1">Rien</w> <w n="97.2">ne</w> <w n="97.3">t</w>’<w n="97.4">affranchit</w> <w n="97.5">mieux</w> <w n="97.6">de</w> <w n="97.7">ce</w> <w n="97.8">qu</w>’<w n="97.9">a</w> <w n="97.10">mérité</w></l>
						<l n="98" num="11.8"><w n="98.1">Ou</w> <w n="98.2">ta</w> <w n="98.3">noire</w> <w n="98.4">malice</w>, <w n="98.5">ou</w> <w n="98.6">ta</w> <w n="98.7">fragilité</w>,</l>
						<l n="99" num="11.9"><w n="99.1">Et</w> <w n="99.2">rien</w> <w n="99.3">n</w>’<w n="99.4">efface</w> <w n="99.5">mieux</w> <w n="99.6">les</w> <w n="99.7">taches</w> <w n="99.8">de</w> <w n="99.9">tes</w> <w n="99.10">crimes</w>,</l>
						<l n="100" num="11.10"><w n="100.1">Que</w> <w n="100.2">la</w> <w n="100.3">sainte</w> <w n="100.4">union</w> <w n="100.5">qu</w>’<w n="100.6">ont</w> <w n="100.7">lors</w> <w n="100.8">ces</w> <w n="100.9">deux</w> <w n="100.10">victimes</w>.</l>
					</lg>
					<lg n="12">
						<l n="101" num="12.1"><w n="101.1">Quand</w> <w n="101.2">le</w> <w n="101.3">pécheur</w> <w n="101.4">a</w> <w n="101.5">fait</w> <w n="101.6">autant</w> <w n="101.7">qu</w>’<w n="101.8">il</w> <w n="101.9">est</w> <w n="101.10">en</w> <w n="101.11">lui</w>,</l>
						<l n="102" num="12.2"><w n="102.1">Qu</w>’<w n="102.2">une</w> <w n="102.3">douleur</w> <w n="102.4">sensible</w>, <w n="102.5">un</w> <w n="102.6">véritable</w> <w n="102.7">ennui</w>,</l>
						<l n="103" num="12.3"><w n="103.1">Un</w> <w n="103.2">profond</w> <w n="103.3">repentir</w> <w n="103.4">le</w> <w n="103.5">prosterne</w> <w n="103.6">à</w> <w n="103.7">ma</w> <w n="103.8">face</w>,</l>
						<l n="104" num="12.4"><w n="104.1">Pour</w> <w n="104.2">obtenir</w> <w n="104.3">pardon</w> <w n="104.4">et</w> <w n="104.5">me</w> <w n="104.6">demander</w> <w n="104.7">grâce</w> ;</l>
						<l n="105" num="12.5"><w n="105.1">Je</w> <w n="105.2">suis</w> <w n="105.3">le</w> <w n="105.4">Dieu</w> <w n="105.5">vivant</w> <w n="105.6">qui</w> <w n="105.7">ne</w> <w n="105.8">veux</w> <w n="105.9">point</w> <w n="105.10">sa</w> <w n="105.11">mort</w>,</l>
						<l n="106" num="12.6"><w n="106.1">Mais</w> <w n="106.2">qu</w>’<w n="106.3">à</w> <w n="106.4">se</w> <w n="106.5">convertir</w> <w n="106.6">il</w> <w n="106.7">fasse</w> <w n="106.8">un</w> <w n="106.9">digne</w> <w n="106.10">effort</w> ;</l>
						<l n="107" num="12.7"><w n="107.1">Qu</w>’<w n="107.2">il</w> <w n="107.3">vive</w> <w n="107.4">en</w> <w n="107.5">mon</w> <w n="107.6">amour</w> <w n="107.7">pour</w> <w n="107.8">revivre</w> <w n="107.9">en</w> <w n="107.10">ma</w> <w n="107.11">gloire</w>,</l>
						<l n="108" num="12.8"><w n="108.1">Et</w> <w n="108.2">de</w> <w n="108.3">tous</w> <w n="108.4">ses</w> <w n="108.5">péchés</w> <w n="108.6">je</w> <w n="108.7">perdrai</w> <w n="108.8">la</w> <w n="108.9">mémoire</w> :</l>
						<l n="109" num="12.9"><w n="109.1">Tous</w> <w n="109.2">lui</w> <w n="109.3">seront</w> <w n="109.4">par</w> <w n="109.5">moi</w> <w n="109.6">si</w> <w n="109.7">pleinement</w> <w n="109.8">remis</w>,</l>
						<l n="110" num="12.10"><w n="110.1">Qu</w>’<w n="110.2">il</w> <w n="110.3">aura</w> <w n="110.4">place</w> <w n="110.5">au</w> <w n="110.6">rang</w> <w n="110.7">de</w> <w n="110.8">mes</w> <w n="110.9">plus</w> <w n="110.10">chers</w> <w n="110.11">amis</w>.</l>
					</lg>
				</div></body></text></TEI>