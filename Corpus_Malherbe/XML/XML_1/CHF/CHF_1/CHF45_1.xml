<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF45">
					<head type="main">CANDIDE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Candide</w> <w n="1.2">est</w> <w n="1.3">un</w> <w n="1.4">petit</w> <w n="1.5">vaurien</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">n</w>’<w n="2.3">a</w> <w n="2.4">ni</w> <w n="2.5">pudeur</w> <w n="2.6">ni</w> <w n="2.7">cervelle</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">A</w> <w n="3.2">ses</w> <w n="3.3">traits</w> <w n="3.4">on</w> <w n="3.5">reconnaît</w> <w n="3.6">bien</w></l>
						<l n="4" num="1.4"><w n="4.1">Frère</w> <w n="4.2">cadet</w> <w n="4.3">de</w> <w n="4.4">la</w> <w n="4.5">Pucelle</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Leur</w> <w n="5.2">vieux</w> <w n="5.3">papa</w>, <w n="5.4">pour</w> <w n="5.5">rajeunir</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Donnerait</w> <w n="6.2">une</w> <w n="6.3">belle</w> <w n="6.4">somme</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Sa</w> <w n="7.2">jeunesse</w> <w n="7.3">va</w> <w n="7.4">revenir</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Il</w> <w n="8.2">fait</w> <w n="8.3">des</w> <w n="8.4">œuvres</w> <w n="8.5">de</w> <w n="8.6">jeune</w> <w n="8.7">homme</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Tout</w> <w n="9.2">n</w>’<w n="9.3">est</w> <w n="9.4">pas</w> <w n="9.5">bien</w> : <w n="9.6">lisez</w> <w n="9.7">l</w>’<w n="9.8">écrit</w>,</l>
						<l n="10" num="1.10"><w n="10.1">La</w> <w n="10.2">preuve</w> <w n="10.3">en</w> <w n="10.4">est</w> <w n="10.5">à</w> <w n="10.6">chaque</w> <w n="10.7">page</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Vous</w> <w n="11.2">verrez</w> <w n="11.3">même</w> <w n="11.4">en</w> <w n="11.5">cet</w> <w n="11.6">ouvrage</w></l>
						<l n="12" num="1.12"><w n="12.1">Que</w> <w n="12.2">tout</w> <w n="12.3">est</w> <w n="12.4">mal</w> <w n="12.5">comme</w> <w n="12.6">il</w> <w n="12.7">le</w> <w n="12.8">dit</w>.</l>
					</lg>
				</div></body></text></TEI>