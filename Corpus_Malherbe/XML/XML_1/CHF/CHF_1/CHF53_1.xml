<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF53">
					<head type="main">MADRIGAL</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Elle</w> <w n="1.2">est</w> <w n="1.3">à</w> <w n="1.4">moi</w>, <w n="1.5">si</w> <w n="1.6">parfaitement</w> <w n="1.7">toute</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">elle</w> <w n="2.3">et</w> <w n="2.4">nul</w> <w n="2.5">autre</w> <w n="2.6">en</w> <w n="2.7">elle</w> <w n="2.8">n</w>’<w n="2.9">ont</w> <w n="2.10">plus</w> <w n="2.11">rien</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">je</w> <w n="3.3">n</w>’<w n="3.4">aurai</w> <w n="3.5">moins</w> <w n="3.6">tort</w> <w n="3.7">d</w>’<w n="3.8">en</w> <w n="3.9">faire</w> <w n="3.10">doute</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">elle</w> <w n="4.3">a</w> <w n="4.4">penser</w> <w n="4.5">qu</w>’<w n="4.6">on</w> <w n="4.7">puisse</w> <w n="4.8">être</w> <w n="4.9">plus</w> <w n="4.10">sien</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Aucun</w> <w n="5.2">ennui</w> <w n="5.3">n</w>’<w n="5.4">a</w> <w n="5.5">su</w> <w n="5.6">troubler</w> <w n="5.7">mon</w> <w n="5.8">bien</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">Rien</w> <w n="6.2">qui</w> <w n="6.3">m</w>’<w n="6.4">afflige</w> <w n="6.5">et</w> <w n="6.6">rien</w> <w n="6.7">que</w> <w n="6.8">je</w> <w n="6.9">redoute</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Hors</w> <w n="7.2">qu</w>’<w n="7.3">il</w> <w n="7.4">me</w> <w n="7.5">peine</w> <w n="7.6">à</w> <w n="7.7">me</w> <w n="7.8">trop</w> <w n="7.9">souvenir</w></l>
						<l n="8" num="1.8"><w n="8.1">D</w>’<w n="8.2">un</w> <w n="8.3">qui</w> <w n="8.4">l</w>’<w n="8.5">avait</w> <w n="8.6">pour</w> <w n="8.7">maîtresse</w> <w n="8.8">choisie</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">rien</w> <w n="9.3">que</w> <w n="9.4">mal</w> <w n="9.5">n</w>’<w n="9.6">a</w> <w n="9.7">pu</w> <w n="9.8">d</w>’<w n="9.9">elle</w> <w n="9.10">obtenir</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">Mais</w> <w n="10.2">mal</w> <w n="10.3">et</w> <w n="10.4">bien</w> <w n="10.5">m</w>’<w n="10.6">en</w> <w n="10.7">doit</w> <w n="10.8">appartenir</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">du</w> <w n="11.3">passé</w> <w n="11.4">je</w> <w n="11.5">suis</w> <w n="11.6">en</w> <w n="11.7">jalousie</w>.</l>
					</lg>
				</div></body></text></TEI>