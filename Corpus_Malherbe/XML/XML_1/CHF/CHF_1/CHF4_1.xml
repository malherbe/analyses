<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉPITRES</head><div type="poem" key="CHF4">
					<head type="main">ÉPITRE A M. ***</head>
					<head type="sub_1">QUI AVAIT FAIT AFFICHER CHEZ SON SUISSE <lb></lb>UN ORDRE EN VERS, DE N’OUVRIR QU’AU MÉRITE, <lb></lb>ET DE REFUSER LA PORTE A LA FORTUNE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">l</w>’<w n="1.3">ai</w> <w n="1.4">vu</w> <w n="1.5">cet</w> <w n="1.6">ordre</w> <w n="1.7">authentique</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Mis</w> <w n="2.2">en</w> <w n="2.3">vers</w> <w n="2.4">joliment</w> <w n="2.5">tournés</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Cette</w> <w n="3.2">consigne</w> <w n="3.3">poétique</w></l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">à</w> <w n="4.3">votre</w> <w n="4.4">Suisse</w> <w n="4.5">vous</w> <w n="4.6">donnez</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Mais</w> <w n="5.2">elle</w> <w n="5.3">est</w> <w n="5.4">trop</w> <w n="5.5">philosophique</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Ou</w> <w n="6.2">trop</w> <w n="6.3">peu</w>. <w n="6.4">Quoi</w> ! <w n="6.5">vous</w> <w n="6.6">ordonnez</w></l>
						<l n="7" num="1.7"><w n="7.1">Que</w> <w n="7.2">l</w>’<w n="7.3">on</w> <w n="7.4">ferme</w> <w n="7.5">la</w> <w n="7.6">porte</w> <w n="7.7">au</w> <w n="7.8">nez</w></l>
						<l n="8" num="1.8"><w n="8.1">A</w> <w n="8.2">la</w> <w n="8.3">Fortune</w> ! <w n="8.4">Et</w> <w n="8.5">pourquoi</w> <w n="8.6">faire</w> ?</l>
						<l n="9" num="1.9"><w n="9.1">Est</w>-<w n="9.2">ce</w> <w n="9.3">humeur</w>, <w n="9.4">faiblesse</w> <w n="9.5">ou</w> <w n="9.6">colère</w> ?</l>
						<l n="10" num="1.10"><w n="10.1">Vous</w> <w n="10.2">avez</w> <w n="10.3">tort</w> ; <w n="10.4">mais</w> <w n="10.5">apprenez</w></l>
						<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">dénoûment</w> <w n="11.3">de</w> <w n="11.4">cette</w> <w n="11.5">affaire</w>.</l>
						<l n="12" num="1.12"><w n="12.1">Après</w> <w n="12.2">ce</w> <w n="12.3">refus</w> <w n="12.4">insultant</w></l>
						<l n="13" num="1.13"><w n="13.1">Que</w> <w n="13.2">fit</w> <w n="13.3">la</w> <w n="13.4">belle</w> <w n="13.5">aventurière</w> ?</l>
						<l n="14" num="1.14"><w n="14.1">Surprise</w> <w n="14.2">de</w> <w n="14.3">ce</w> <w n="14.4">compliment</w>,</l>
						<l n="15" num="1.15"><w n="15.1">De</w> <w n="15.2">la</w> <w n="15.3">rebufade</w> <w n="15.4">impolie</w></l>
						<l n="16" num="1.16"><w n="16.1">D</w>’<w n="16.2">un</w> <w n="16.3">portier</w> <w n="16.4">qui</w> <w n="16.5">la</w> <w n="16.6">congédie</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Croiriez</w>-<w n="17.2">vous</w> <w n="17.3">que</w> <w n="17.4">dans</w> <w n="17.5">cet</w> <w n="17.6">instant</w></l>
						<l n="18" num="1.18">(<w n="18.1">Voyez</w> <w n="18.2">un</w> <w n="18.3">peu</w> <w n="18.4">quelle</w> <w n="18.5">étourdie</w> !)</l>
						<l n="19" num="1.19"><w n="19.1">Elle</w> <w n="19.2">vint</w> <w n="19.3">chez</w> <w n="19.4">moi</w> <w n="19.5">brusquement</w> ?</l>
						<l n="20" num="1.20"><w n="20.1">Je</w> <w n="20.2">sortais</w> : <w n="20.3">j</w>’<w n="20.4">ouvre</w>… — <w n="20.5">La</w> <w n="20.6">fortune</w> !</l>
						<l n="21" num="1.21"><w n="21.1">Ne</w> <w n="21.2">vous</w> <w n="21.3">suis</w>-<w n="21.4">je</w> <w n="21.5">pas</w> <w n="21.6">importune</w> ?</l>
						<l n="22" num="1.22"><w n="22.1">Le</w> <w n="22.2">cas</w> <w n="22.3">arrive</w> <w n="22.4">rarement</w>.</l>
						<l n="23" num="1.23">— <w n="23.1">Il</w> <w n="23.2">arrive</w> <w n="23.3">dans</w> <w n="23.4">ce</w> <w n="23.5">moment</w>.</l>
						<l n="24" num="1.24"><choice hand="RR" reason="analysis" type="missing"><sic> </sic><corr source="édition 1794"><w n="24.1">Elle</w> <w n="24.2">me</w> <w n="24.3">conte</w> <w n="24.4">l</w>’<w n="24.5">aventure</w></corr></choice></l>
						<l n="25" num="1.25"><choice hand="RR" reason="analysis" type="variant"><sic>Elle m’étonna, je vous jure.</sic><corr source="édition 1794"><w n="25.1">Qui</w> <w n="25.2">m</w>’<w n="25.3">étonna</w> , <w n="25.4">je</w> <w n="25.5">vous</w> <w n="25.6">assure</w>.</corr></choice></l>
						<l n="26" num="1.26"><w n="26.1">J</w>’<w n="26.2">excusai</w> <w n="26.3">le</w> <w n="26.4">sage</w> <w n="26.5">imprudent</w></l>
						<l n="27" num="1.27"><w n="27.1">Qui</w> <w n="27.2">brusquait</w> <w n="27.3">ainsi</w> <w n="27.4">la</w> <w n="27.5">déesse</w> ;</l>
						<l n="28" num="1.28"><w n="28.1">Il</w> <w n="28.2">a</w> <w n="28.3">tort</w> <w n="28.4">d</w>’<w n="28.5">outrer</w> <w n="28.6">la</w> <w n="28.7">sagesse</w>.</l>
						<l n="29" num="1.29">— <w n="29.1">Vous</w> <w n="29.2">raillez</w>, <w n="29.3">je</w> <w n="29.4">crois</w>. — <w n="29.5">Nullement</w>.</l>
						<l n="30" num="1.30"><w n="30.1">Il</w> <w n="30.2">fallait</w> <w n="30.3">au</w> <w n="30.4">moins</w> <w n="30.5">vous</w> <w n="30.6">admettre</w>,</l>
						<l n="31" num="1.31"><w n="31.1">En</w> <w n="31.2">faisant</w> <w n="31.3">des</w> <w n="31.4">conditions</w>…</l>
						<l n="32" num="1.32">— <w n="32.1">A</w> <w n="32.2">moi</w> ! — <w n="32.3">Sans</w> <w n="32.4">doute</w>. — <w n="32.5">Eh</w> <w n="32.6">bien</w> ! <w n="32.7">voyons</w>.</l>
						<l n="33" num="1.33"><w n="33.1">Faites</w> <w n="33.2">les</w> <w n="33.3">vôtres</w>. — <w n="33.4">A</w> <w n="33.5">la</w> <w n="33.6">lettre</w></l>
						<l n="34" num="1.34"><w n="34.1">Vous</w> <w n="34.2">les</w> <w n="34.3">suivrez</w> ? <w n="34.4">Premièrement</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Je</w> <w n="35.2">vous</w> <w n="35.3">dois</w> <w n="35.4">un</w> <w n="35.5">remercîment</w> :</l>
						<l n="36" num="1.36"><w n="36.1">Vous</w> <w n="36.2">voilà</w> <w n="36.3">sans</w> <w n="36.4">qu</w>’<w n="36.5">on</w> <w n="36.6">vous</w> <w n="36.7">appelle</w>,</l>
						<l n="37" num="1.37"><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">ce</w> <w n="37.4">qu</w>’<w n="37.5">il</w> <w n="37.6">me</w> <w n="37.7">faut</w> <w n="37.8">justement</w>.</l>
						<l n="38" num="1.38">— <w n="38.1">Vous</w> <w n="38.2">me</w> <w n="38.3">plaisez</w> <w n="38.4">assez</w>, <w n="38.5">dit</w>-<w n="38.6">elle</w>.</l>
						<l n="39" num="1.39">— <w n="39.1">Tant</w> <w n="39.2">mieux</w>. — <w n="39.3">Convenons</w> <w n="39.4">de</w> <w n="39.5">nos</w> <w n="39.6">faits</w>.</l>
						<l n="40" num="1.40">— <w n="40.1">Vous</w> <w n="40.2">ne</w> <w n="40.3">prétendrez</w> <w n="40.4"><choice hand="RR" reason="analysis" type="missing"><sic> </sic><corr source="édition 1794">donc</corr></choice>jamais</w></l>
						<l n="41" num="1.41"><w n="41.1">A</w> <w n="41.2">changer</w> <w n="41.3">le</w> <w n="41.4">fond</w> <w n="41.5">de</w> <w n="41.6">ma</w> <w n="41.7">vie</w> ;</l>
						<l n="42" num="1.42"><w n="42.1">Vous</w> <w n="42.2">respecterez</w> <w n="42.3">sans</w> <w n="42.4">aigreur</w></l>
						<l n="43" num="1.43"><w n="43.1">Mon</w> <w n="43.2">caractère</w>, <w n="43.3">mon</w> <w n="43.4">humeur</w>,</l>
						<l n="44" num="1.44"><w n="44.1">Et</w> <w n="44.2">même</w> <w n="44.3">un</w> <w n="44.4">peu</w> <w n="44.5">ma</w> <w n="44.6">fantaisie</w>.</l>
						<l n="45" num="1.45"><w n="45.1">Je</w> <w n="45.2">conserverai</w> <w n="45.3">mes</w> <w n="45.4">amis</w>,</l>
						<l n="46" num="1.46"><w n="46.1">Vous</w> <w n="46.2">ne</w> <w n="46.3">m</w>’<w n="46.4">en</w> <w n="46.5">donnerez</w> <w n="46.6">point</w> <w n="46.7">d</w>’<w n="46.8">autres</w> :</l>
						<l n="47" num="1.47"><w n="47.1">A</w> <w n="47.2">moi</w> <w n="47.3">les</w> <w n="47.4">miens</w>, <w n="47.5">à</w> <w n="47.6">vous</w> <w n="47.7">les</w> <w n="47.8">vôtres</w>.</l>
						<l n="48" num="1.48"><w n="48.1">Le</w> <w n="48.2">sentiment</w> <w n="48.3">sera</w> <w n="48.4">permis</w></l>
						<l n="49" num="1.49"><w n="49.1">A</w> <w n="49.2">mon</w> <w n="49.3">cœur</w> <w n="49.4">né</w> <w n="49.5">sensible</w> <w n="49.6">et</w> <w n="49.7">tendre</w> ;</l>
						<l n="50" num="1.50"><w n="50.1">De</w> <w n="50.2">moi</w> <w n="50.3">vous</w> <w n="50.4">ne</w> <w n="50.5">devrez</w> <w n="50.6">attendre</w></l>
						<l n="51" num="1.51"><w n="51.1">Que</w> <w n="51.2">des</w> <w n="51.3">soins</w>, <w n="51.4">et</w> <w n="51.5">non</w> <w n="51.6">des</w> <w n="51.7">soucis</w> ;</l>
						<l n="52" num="1.52"><w n="52.1">Je</w> <w n="52.2">n</w>’<w n="52.3">en</w> <w n="52.4">veux</w> <w n="52.5">ni</w> <w n="52.6">donner</w> <w n="52.7">ni</w> <w n="52.8">prendre</w>.</l>
						<l n="53" num="1.53"><w n="53.1">Si</w>, <w n="53.2">par</w> <w n="53.3">l</w>’<w n="53.4">effet</w> <w n="53.5">de</w> <w n="53.6">vos</w> <w n="53.7">faveurs</w>,</l>
						<l n="54" num="1.54"><w n="54.1">Je</w> <w n="54.2">dois</w> <w n="54.3">approcher</w> <w n="54.4">des</w> <w n="54.5">grandeurs</w>,</l>
						<l n="55" num="1.55"><w n="55.1">Partout</w>, <w n="55.2">à</w> <w n="55.3">la</w> <w n="55.4">cour</w>, <w n="55.5">à</w> <w n="55.6">la</w> <w n="55.7">ville</w>,</l>
						<l n="56" num="1.56"><w n="56.1">Je</w> <w n="56.2">serai</w>, <w n="56.3">rien</w> <w n="56.4">n</w>’<w n="56.5">est</w> <w n="56.6">plus</w> <w n="56.7">facile</w>,</l>
						<l n="57" num="1.57"><w n="57.1">Sans</w> <w n="57.2">orgueil</w>, <w n="57.3">mais</w> <w n="57.4">non</w> <w n="57.5">sans</w> <w n="57.6">fierté</w>,</l>
						<l n="58" num="1.58"><w n="58.1">Vrai</w> <w n="58.2">sans</w> <w n="58.3">rudesse</w>, <w n="58.4">sans</w> <w n="58.5">audace</w>,</l>
						<l n="59" num="1.59"><w n="59.1">Et</w> <w n="59.2">libre</w> <w n="59.3">sans</w> <w n="59.4">légèreté</w>.</l>
						<l n="60" num="1.60"><w n="60.1">Auprès</w> <w n="60.2">de</w> <w n="60.3">mes</w> <w n="60.4">amis</w> <w n="60.5">en</w> <w n="60.6">place</w></l>
						<l n="61" num="1.61"><w n="61.1">J</w>’<w n="61.2">aurai</w> <w n="61.3">peu</w> <w n="61.4">d</w>’<w n="61.5">assiduité</w>,</l>
						<l n="62" num="1.62"><w n="62.1">La</w> <w n="62.2">réservant</w> <w n="62.3">pour</w> <w n="62.4">leur</w> <w n="62.5">disgrâce</w>.</l>
						<l n="63" num="1.63"><w n="63.1">Permettez</w>-<w n="63.2">vous</w> ? — <w n="63.3">Accordé</w>, <w n="63.4">passe</w>.</l>
						<l n="64" num="1.64">— <w n="64.1">Avec</w> <w n="64.2">le</w> <w n="64.3">mérite</w>, <w n="64.4">l</w>’<w n="64.5">honneur</w>,</l>
						<l n="65" num="1.65"><w n="65.1">Je</w> <w n="65.2">n</w>’<w n="65.3">entre</w> <w n="65.4">point</w> <w n="65.5">dans</w> <w n="65.6">vos</w> <w n="65.7">querelles</w> ;</l>
						<l n="66" num="1.66"><w n="66.1">Je</w> <w n="66.2">veux</w> <w n="66.3">rester</w> <w n="66.4">leur</w> <w n="66.5">serviteur</w>,</l>
						<l n="67" num="1.67"><w n="67.1">Et</w> <w n="67.2">les</w> <w n="67.3">tiens</w> <w n="67.4">pour</w> <w n="67.5">amis</w> <w n="67.6">fidèles</w>.</l>
						<l n="68" num="1.68">— <w n="68.1">Ah</w> ! <w n="68.2">nous</w> <w n="68.3">nous</w> <w n="68.4">brouillerons</w>. — <w n="68.5">Tant</w> <w n="68.6">pis</w></l>
						<l n="69" num="1.69">— <w n="69.1">Un</w> <w n="69.2">mot</w> <w n="69.3">encor</w>. <w n="69.4">Toujours</w> <w n="69.5">admis</w>,</l>
						<l n="70" num="1.70"><w n="70.1">Chez</w> <w n="70.2">moi</w> <w n="70.3">le</w> <w n="70.4">mérite</w> <w n="70.5">aura</w> <w n="70.6">place</w></l>
						<l n="71" num="1.71"><w n="71.1">Au</w>-<w n="71.2">dessus</w> <w n="71.3">de</w> <w n="71.4">vos</w> <w n="71.5">favoris</w> :</l>
						<l n="72" num="1.72"><w n="72.1">C</w>’<w n="72.2">est</w> <w n="72.3">la</w> <w n="72.4">sienne</w>, <w n="72.5">quoique</w> <w n="72.6">l</w>’<w n="72.7">on</w> <w n="72.8">fasse</w>.</l>
						<l n="73" num="1.73"><w n="73.1">Refusé</w> <w n="73.2">net</w>. — <w n="73.3">La</w> <w n="73.4">déïté</w></l>
						<l n="74" num="1.74"><w n="74.1">Me</w> <w n="74.2">dit</w>, <w n="74.3">d</w>’<w n="74.4">un</w> <w n="74.5">ton</w> <w n="74.6">de</w> <w n="74.7">bonhommie</w> :</l>
						<l n="75" num="1.75"><w n="75.1">Moi</w>, <w n="75.2">j</w>’<w n="75.3">ai</w> <w n="75.4">de</w> <w n="75.5">la</w> <w n="75.6">facilité</w> ;</l>
						<l n="76" num="1.76"><w n="76.1">Mais</w> <w n="76.2">cet</w> <w n="76.3">article</w> <w n="76.4">du</w> <w n="76.5">traité</w>,</l>
						<l n="77" num="1.77"><w n="77.1">Par</w> <w n="77.2">quel</w> <w n="77.3">art</w>, <w n="77.4">par</w> <w n="77.5">quelle</w> <w n="77.6">industrie</w>,</l>
						<l n="78" num="1.78"><w n="78.1">Le</w> <w n="78.2">faire</w> <w n="78.3">signer</w>, <w n="78.4">je</w> <w n="78.5">vous</w> <w n="78.6">prie</w>,</l>
						<l n="79" num="1.79"><w n="79.1">A</w> <w n="79.2">ma</w> <w n="79.3">sœur</w> ? — <w n="79.4">Qui</w> ? — <w n="79.5">La</w> <w n="79.6">vanité</w>.</l>
						<l n="80" num="1.80"><w n="80.1">Adieu</w>. — <w n="80.2">Soit</w>. — <w n="80.3">La</w> <w n="80.4">folle</w> <w n="80.5">immortelle</w></l>
						<l n="81" num="1.81"><w n="81.1">Part</w> <w n="81.2">et</w> <w n="81.3">s</w>’<w n="81.4">envole</w> <w n="81.5">à</w> <w n="81.6">tire</w> <w n="81.7">d</w>’<w n="81.8">aile</w>,</l>
						<l n="82" num="1.82"><w n="82.1">Me</w> <w n="82.2">supposant</w> <w n="82.3">de</w> <w n="82.4">vains</w> <w n="82.5">regrets</w>,</l>
						<l n="83" num="1.83"><w n="83.1">Je</w> <w n="83.2">le</w> <w n="83.3">soupçonne</w> ; <w n="83.4">car</w> <w n="83.5">la</w> <w n="83.6">belle</w>,</l>
						<l n="84" num="1.84"><w n="84.1">Tout</w> <w n="84.2">en</w> <w n="84.3">me</w> <w n="84.4">quittant</w> <w n="84.5">pour</w> <w n="84.6">jamais</w>,</l>
						<l n="85" num="1.85"><w n="85.1">Regardait</w> <w n="85.2">parfois</w> <w n="85.3">derrière</w> <w n="85.4">elle</w>,</l>
						<l n="86" num="1.86"><w n="86.1">Pour</w> <w n="86.2">voir</w> <w n="86.3">si</w> <w n="86.4">je</w> <w n="86.5">la</w> <w n="86.6">rappelais</w> ;</l>
						<l n="87" num="1.87"><w n="87.1">Mais</w> <w n="87.2">je</w> <w n="87.3">laissai</w> <w n="87.4">fuir</w> <w n="87.5">l</w>’<w n="87.6">infidelle</w>,</l>
						<l n="88" num="1.88"><w n="88.1">Et</w> <w n="88.2">mes</w> <w n="88.3">voisins</w> <w n="88.4">courent</w> <w n="88.5">après</w>.</l>
					</lg>
				</div></body></text></TEI>