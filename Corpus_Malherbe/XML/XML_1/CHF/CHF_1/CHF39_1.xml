<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF39">
					<head type="main">BACAROLE</head>
					<head type="sub_1">IMITÉE DE L’ITALIEN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Aux</w> <w n="1.2">bords</w> <w n="1.3">fleuris</w> <w n="1.4">d</w>’<w n="1.5">une</w> <w n="1.6">fontaine</w>,</l>
						<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2">ai</w> <w n="2.3">vu</w>, <w n="2.4">dans</w> <w n="2.5">les</w> <w n="2.6">bras</w> <w n="2.7">du</w> <w n="2.8">sommeil</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">cœurs</w> <w n="3.3">la</w> <w n="3.4">jeune</w> <w n="3.5">souveraine</w>,</l>
						<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">œil</w> <w n="4.3">demi</w>-<w n="4.4">clos</w>, <w n="4.5">le</w> <w n="4.6">teint</w> <w n="4.7">vermeil</w> :</l>
						<l n="5" num="1.5"><w n="5.1">Ah</w> ! <w n="5.2">qu</w>’<w n="5.3">en</w> <w n="5.4">dormant</w> <w n="5.5">elle</w> <w n="5.6">était</w> <w n="5.7">belle</w> !</l>
						<l n="6" num="1.6"><w n="6.1">Que</w> <w n="6.2">son</w> <w n="6.3">réveil</w> <w n="6.4">me</w> <w n="6.5">charmera</w> !</l>
						<l n="7" num="1.7"><w n="7.1">Besoin</w> <w n="7.2">d</w>’<w n="7.3">amour</w> <w n="7.4">dort</w> <w n="7.5">avec</w> <w n="7.6">elle</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">Avec</w> <w n="8.2">elle</w> <w n="8.3">il</w> <w n="8.4">s</w>’<w n="8.5">éveillera</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Sa</w> <w n="9.2">bouche</w> <w n="9.3">a</w> <w n="9.4">l</w>’<w n="9.5">éclat</w> <w n="9.6">de</w> <w n="9.7">la</w> <w n="9.8">rose</w>,</l>
						<l n="10" num="2.2"><w n="10.1">Qu</w>’<w n="10.2">au</w> <w n="10.3">premier</w> <w n="10.4">souffle</w> <w n="10.5">du</w> <w n="10.6">printemps</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Avril</w> <w n="11.2">respire</w>, <w n="11.3">fraîche</w> <w n="11.4">éclose</w></l>
						<l n="12" num="2.4"><w n="12.1">Du</w> <w n="12.2">sein</w> <w n="12.3">des</w> <w n="12.4">frimats</w> <w n="12.5">expirans</w> :</l>
						<l n="13" num="2.5"><w n="13.1">Ah</w> ! <w n="13.2">qu</w>’<w n="13.3">en</w> <w n="13.4">dormant</w> <w n="13.5">elle</w> <w n="13.6">était</w> <w n="13.7">belle</w> !</l>
						<l n="14" num="2.6"><w n="14.1">Que</w> <w n="14.2">son</w> <w n="14.3">réveil</w> <w n="14.4">me</w> <w n="14.5">charmera</w> !</l>
						<l n="15" num="2.7"><w n="15.1">Besoin</w> <w n="15.2">d</w>’<w n="15.3">amour</w> <w n="15.4">dort</w> <w n="15.5">avec</w> <w n="15.6">elle</w> ;</l>
						<l n="16" num="2.8"><w n="16.1">Avec</w> <w n="16.2">elle</w> <w n="16.3">il</w> <w n="16.4">s</w>’<w n="16.5">éveillera</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Sur</w> <w n="17.2">sa</w> <w n="17.3">main</w> <w n="17.4">sa</w> <w n="17.5">tête</w> <w n="17.6">appuyée</w></l>
						<l n="18" num="3.2"><w n="18.1">Ressemble</w> <w n="18.2">au</w> <w n="18.3">lis</w> <w n="18.4">qui</w> <w n="18.5">mollement</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Sur</w> <w n="19.2">sa</w> <w n="19.3">tige</w> <w n="19.4">aux</w> <w n="19.5">vents</w> <w n="19.6">déployée</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Reste</w> <w n="20.2">penché</w> <w n="20.3">languissamment</w>.</l>
						<l n="21" num="3.5"><w n="21.1">Ah</w> ! <w n="21.2">qu</w>’<w n="21.3">en</w> <w n="21.4">donnant</w> <w n="21.5">elle</w> <w n="21.6">était</w> <w n="21.7">belle</w> !</l>
						<l n="22" num="3.6"><w n="22.1">Que</w> <w n="22.2">son</w> <w n="22.3">réveil</w> <w n="22.4">me</w> <w n="22.5">charmera</w> !</l>
						<l n="23" num="3.7"><w n="23.1">Besoin</w> <w n="23.2">d</w>’<w n="23.3">amour</w> <w n="23.4">dort</w> <w n="23.5">avec</w> <w n="23.6">elle</w> ;</l>
						<l n="24" num="3.8"><w n="24.1">Avec</w> <w n="24.2">elle</w> <w n="24.3">il</w> <w n="24.4">s</w>’<w n="24.5">éveillera</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Et</w> <w n="25.2">sous</w> <w n="25.3">cette</w> <w n="25.4">gaze</w> <w n="25.5">mouvante</w></l>
						<l n="26" num="4.2"><w n="26.1">Que</w> <w n="26.2">soulève</w> <w n="26.3">un</w> <w n="26.4">zéphir</w> <w n="26.5">malin</w>,</l>
						<l n="27" num="4.3"><w n="27.1">Palpite</w> <w n="27.2">une</w> <w n="27.3">gorge</w> <w n="27.4">naissante</w></l>
						<l n="28" num="4.4"><w n="28.1">Qu</w>’<w n="28.2">envîrait</w> <w n="28.3">la</w> <w n="28.4">fleur</w> <w n="28.5">du</w> <w n="28.6">matin</w>.</l>
						<l n="29" num="4.5"><w n="29.1">Ah</w> ! <w n="29.2">qu</w>’<w n="29.3">en</w> <w n="29.4">dormant</w> <w n="29.5">elle</w> <w n="29.6">était</w> <w n="29.7">belle</w> !</l>
						<l n="30" num="4.6"><w n="30.1">Que</w> <w n="30.2">son</w> <w n="30.3">réveil</w> <w n="30.4">me</w> <w n="30.5">charmera</w> !</l>
						<l n="31" num="4.7"><w n="31.1">Besoin</w> <w n="31.2">d</w>’<w n="31.3">amour</w> <w n="31.4">dort</w> <w n="31.5">avec</w> <w n="31.6">elle</w></l>
						<l n="32" num="4.8"><w n="32.1">Avec</w> <w n="32.2">elle</w> <w n="32.3">il</w> <w n="32.4">s</w>’<w n="32.5">éveillera</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Sa</w> <w n="33.2">longue</w> <w n="33.3">et</w> <w n="33.4">blonde</w> <w n="33.5">chevelure</w>,</l>
						<l n="34" num="5.2"><w n="34.1">Errant</w> <w n="34.2">au</w> <w n="34.3">caprice</w> <w n="34.4">du</w> <w n="34.5">vent</w>,</l>
						<l n="35" num="5.3"><w n="35.1">Tantôt</w> <w n="35.2">flotte</w> <w n="35.3">sur</w> <w n="35.4">sa</w> <w n="35.5">figure</w>,</l>
						<l n="36" num="5.4"><w n="36.1">Et</w> <w n="36.2">tantôt</w> <w n="36.3">sur</w> <w n="36.4">son</w> <w n="36.5">col</w> <w n="36.6">descend</w>.</l>
						<l n="37" num="5.5"><w n="37.1">Ah</w> ! <w n="37.2">qu</w>’<w n="37.3">en</w> <w n="37.4">dormant</w> <w n="37.5">elle</w> <w n="37.6">était</w> <w n="37.7">belle</w> !</l>
						<l n="38" num="5.6"><w n="38.1">Que</w> <w n="38.2">son</w> <w n="38.3">réveil</w> <w n="38.4">me</w> <w n="38.5">charmera</w> !</l>
						<l n="39" num="5.7"><w n="39.1">Besoin</w> <w n="39.2">d</w>’<w n="39.3">amour</w> <w n="39.4">dort</w> <w n="39.5">avec</w> <w n="39.6">elle</w> ;</l>
						<l n="40" num="5.8"><w n="40.1">Avec</w> <w n="40.2">elle</w> <w n="40.3">il</w> <w n="40.4">s</w>’<w n="40.5">éveillera</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">Morphée</w>, <w n="41.2">ô</w> <w n="41.3">toi</w> <w n="41.4">par</w> <w n="41.5">qui</w> <w n="41.6">reposent</w></l>
						<l n="42" num="6.2"><w n="42.1">Tant</w> <w n="42.2">d</w>’<w n="42.3">appas</w> <w n="42.4">offerts</w> <w n="42.5">à</w> <w n="42.6">mes</w> <w n="42.7">yeux</w>,</l>
						<l n="43" num="6.3"><w n="43.1">Permets</w> <w n="43.2">qu</w>’<w n="43.3">en</w> <w n="43.4">son</w> <w n="43.5">sein</w> <w n="43.6">je</w> <w n="43.7">dépose</w></l>
						<l n="44" num="6.4"><w n="44.1">L</w>’<w n="44.2">ardeur</w> <w n="44.3">des</w> <w n="44.4">plus</w> <w n="44.5">aimables</w> <w n="44.6">feux</w>.</l>
						<l n="45" num="6.5"><w n="45.1">Ah</w> ! <w n="45.2">qu</w>’<w n="45.3">en</w> <w n="45.4">dormant</w> <w n="45.5">elle</w> <w n="45.6">était</w> <w n="45.7">belle</w> !</l>
						<l n="46" num="6.6"><w n="46.1">Que</w> <w n="46.2">son</w> <w n="46.3">réveil</w> <w n="46.4">me</w> <w n="46.5">charmera</w> !</l>
						<l n="47" num="6.7"><w n="47.1">Besoin</w> <w n="47.2">d</w>’<w n="47.3">amour</w> <w n="47.4">dort</w> <w n="47.5">avec</w> <w n="47.6">elle</w> ;</l>
						<l n="48" num="6.8"><w n="48.1">Avec</w> <w n="48.2">elle</w> <w n="48.3">il</w> <w n="48.4">s</w>’<w n="48.5">éveillera</w>.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1"><w n="49.1">De</w> <w n="49.2">nos</w> <w n="49.3">baisers</w> <w n="49.4">le</w> <w n="49.5">doux</w> <w n="49.6">échange</w></l>
						<l n="50" num="7.2"><w n="50.1">Dans</w> <w n="50.2">son</w> <w n="50.3">cœur</w> <w n="50.4">portera</w> <w n="50.5">l</w>’<w n="50.6">amour</w> :</l>
						<l n="51" num="7.3"><w n="51.1">Transports</w> <w n="51.2">charmans</w> ! <w n="51.3">divin</w> <w n="51.4">mélange</w> !</l>
						<l n="52" num="7.4"><w n="52.1">Je</w> <w n="52.2">vous</w> <w n="52.3">devrai</w> <w n="52.4">mon</w> <w n="52.5">plus</w> <w n="52.6">beau</w> <w n="52.7">jour</w>.</l>
						<l n="53" num="7.5"><w n="53.1">Ah</w> ! <w n="53.2">qu</w>’<w n="53.3">en</w> <w n="53.4">dormant</w> <w n="53.5">elle</w> <w n="53.6">était</w> <w n="53.7">belle</w> !</l>
						<l n="54" num="7.6"><w n="54.1">Que</w> <w n="54.2">son</w> <w n="54.3">réveil</w> <w n="54.4">me</w> <w n="54.5">charmera</w> !</l>
						<l n="55" num="7.7"><w n="55.1">Besoin</w> <w n="55.2">d</w>’<w n="55.3">amour</w> <w n="55.4">dort</w> <w n="55.5">avec</w> <w n="55.6">elle</w> ;</l>
						<l n="56" num="7.8"><w n="56.1">Avec</w> <w n="56.2">elle</w> <w n="56.3">il</w> <w n="56.4">s</w>’<w n="56.5">éveillera</w>.</l>
					</lg>
				</div></body></text></TEI>