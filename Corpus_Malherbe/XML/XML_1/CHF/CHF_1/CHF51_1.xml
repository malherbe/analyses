<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">ŒUVRES COMPLÈTES</title>
				<title type="sub">(POÉSIES)</title>
				<title type="medium">Édition électronique</title>
				<author key="CHF">
					<name>
						<forename>Sébastien-Roch-Nicolas</forename>
						<nameLink>de</nameLink>
						<surname>CHAMFORT</surname>
					</name>
					<date from="1741" to="1794">1741-1794</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4341 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">CHF_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Œuvres complètes</title>
						<author>Sébastien-Roch-Nicolas de Chamfort</author>
					</titleStmt>
					<publicationStmt>
						<publisher>ÉFÉLÉ</publisher>
						<idno type="URI">http://efele.net/ebooks/livres/c00020/index.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">Œuvres complètes</title>
								<author>Sébastien-Roch-Nicolas de Chamfort</author>
								<editor>Pierre-René Auguis</editor>
								<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k2026541</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Chez CHAUMEROT JEUNE, LIBRAIRE</publisher>
									<date when="1825">1824-1825</date>
								</imprint>
								<biblScope unit="tome">V</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Oeuvres de Chamfort</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>recueillies et publiées par un de ses amis</editor>
					<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k96080030</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Impr. des sciences et des arts</publisher>
						<date when="1794">1794</date>
					</imprint>
					<biblScope unit="tome">II</biblScope>
				</monogr>
			</biblStruct>
		</sourceDesc>
		<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Nouvelle anthologie</title>
					<title type="main">ou choix de chansons anciennes et modernes</title>
					<author>Sébastien-Roch-Nicolas de Chamfort</author>
					<editor>Publiées par L. Castel</editor>
					<idno type="URI">https://www.google.fr/books/edition/Nouvelle_anthologie_ou_choix_de_chansons</idno>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>LIBRAIRIE ANCIENNE ET MODERNE</publisher>
						<date when="1828">1828</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="CHF51">
					<head type="main">LES JEUNES GENS DU SIÈCLE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Beautés</w> <w n="1.2">qui</w> <w n="1.3">fuyez</w> <w n="1.4">la</w> <w n="1.5">licence</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Évitez</w> <w n="2.2">tous</w> <w n="2.3">nos</w> <w n="2.4">jeunes</w> <w n="2.5">gens</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">Amour</w> <w n="3.3">a</w> <w n="3.4">déserté</w> <w n="3.5">la</w> <w n="3.6">France</w></l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">l</w>’<w n="4.3">aspect</w> <w n="4.4">de</w> <w n="4.5">ces</w> <w n="4.6">grands</w> <w n="4.7">enfans</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Ils</w> <w n="5.2">ont</w>, <w n="5.3">par</w> <w n="5.4">leur</w> <w n="5.5">ton</w>, <w n="5.6">leur</w> <w n="5.7">langage</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Effarouché</w> <w n="6.2">la</w> <w n="6.3">volupté</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">gardé</w> <w n="7.3">pour</w> <w n="7.4">tout</w> <w n="7.5">apanage</w></l>
						<l n="8" num="1.8"><w n="8.1">L</w>’<w n="8.2">ignorance</w> <w n="8.3">et</w> <w n="8.4">la</w> <w n="8.5">nullité</w> ;</l>
						<l n="9" num="1.9"><w n="9.1">Malgré</w> <w n="9.2">leur</w> <w n="9.3">tournure</w> <w n="9.4">fragile</w>,</l>
						<l n="10" num="1.10"><w n="10.1">A</w> <w n="10.2">courir</w> <w n="10.3">ils</w> <w n="10.4">passent</w> <w n="10.5">leur</w> <w n="10.6">temps</w> ;</l>
						<l n="11" num="1.11"><w n="11.1">Ils</w> <w n="11.2">sont</w> <w n="11.3">importuns</w> <w n="11.4">à</w> <w n="11.5">la</w> <w n="11.6">ville</w>,</l>
						<l n="12" num="1.12"><w n="12.1">A</w> <w n="12.2">la</w> <w n="12.3">cour</w> <w n="12.4">ils</w> <w n="12.5">sont</w> <w n="12.6">importans</w> ;</l>
						<l n="13" num="1.13"><w n="13.1">Dans</w> <w n="13.2">le</w> <w n="13.3">monde</w> <w n="13.4">en</w> <w n="13.5">rois</w> <w n="13.6">ils</w> <w n="13.7">décident</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Au</w> <w n="14.2">spectacle</w> <w n="14.3">ils</w> <w n="14.4">ont</w> <w n="14.5">l</w>’<w n="14.6">air</w> <w n="14.7">méchant</w> ;</l>
						<l n="15" num="1.15"><w n="15.1">Partout</w> <w n="15.2">leurs</w> <w n="15.3">sottises</w> <w n="15.4">les</w> <w n="15.5">guident</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Partout</w> <w n="16.2">le</w> <w n="16.3">mépris</w> <w n="16.4">les</w> <w n="16.5">attend</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Pour</w> <w n="17.2">eux</w> <w n="17.3">les</w> <w n="17.4">soins</w> <w n="17.5">sont</w> <w n="17.6">des</w> <w n="17.7">vétilles</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Et</w> <w n="18.2">l</w>’<w n="18.3">esprit</w> <w n="18.4">n</w>’<w n="18.5">est</w> <w n="18.6">qu</w>’<w n="18.7">un</w> <w n="18.8">lourd</w> <w n="18.9">bon</w> <w n="18.10">sens</w> ;</l>
						<l n="19" num="1.19"><w n="19.1">Ils</w> <w n="19.2">sont</w> <w n="19.3">gauches</w> <w n="19.4">auprès</w> <w n="19.5">des</w> <w n="19.6">filles</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Auprès</w> <w n="20.2">des</w> <w n="20.3">femmes</w> <w n="20.4">indécens</w>.</l>
						<l n="21" num="1.21"><w n="21.1">Leur</w> <w n="21.2">jargon</w> <w n="21.3">ne</w> <w n="21.4">pouvant</w> <w n="21.5">s</w>’<w n="21.6">entendre</w>,</l>
						<l n="22" num="1.22"><w n="22.1">Si</w> <w n="22.2">leur</w> <w n="22.3">jeunesse</w> <w n="22.4">peut</w> <w n="22.5">tenter</w></l>
						<l n="23" num="1.23"><w n="23.1">Ceux</w> <w n="23.2">que</w> <w n="23.3">le</w> <w n="23.4">besoin</w> <w n="23.5">a</w> <w n="23.6">fait</w> <w n="23.7">prendre</w>,</l>
						<l n="24" num="1.24"><w n="24.1">L</w>’<w n="24.2">ennui</w> <w n="24.3">bientôt</w> <w n="24.4">les</w> <w n="24.5">fait</w> <w n="24.6">quitter</w>.</l>
						<l n="25" num="1.25"><w n="25.1">Sur</w> <w n="25.2">leurs</w> <w n="25.3">airs</w> <w n="25.4">et</w> <w n="25.5">sur</w> <w n="25.6">leur</w> <w n="25.7">figure</w></l>
						<l n="26" num="1.26"><w n="26.1">Presque</w> <w n="26.2">tous</w> <w n="26.3">fondent</w> <w n="26.4">leur</w> <w n="26.5">espoir</w> ;</l>
						<l n="27" num="1.27"><w n="27.1">Il</w> <w n="27.2">font</w> <w n="27.3">entrer</w> <w n="27.4">dans</w> <w n="27.5">leur</w> <w n="27.6">parure</w></l>
						<l n="28" num="1.28"><w n="28.1">Tout</w> <w n="28.2">le</w> <w n="28.3">goût</w> <w n="28.4">qu</w>’<w n="28.5">ils</w> <w n="28.6">pensent</w> <w n="28.7">avoir</w>.</l>
						<l n="29" num="1.29"><w n="29.1">Dans</w> <w n="29.2">le</w> <w n="29.3">cercle</w> <w n="29.4">de</w> <w n="29.5">quelques</w> <w n="29.6">belles</w></l>
						<l n="30" num="1.30"><w n="30.1">Ils</w> <w n="30.2">vont</w> <w n="30.3">s</w>’<w n="30.4">établir</w> <w n="30.5">en</w> <w n="30.6">vainqueurs</w> ;</l>
						<l n="31" num="1.31"><w n="31.1">Mais</w> <w n="31.2">ils</w> <w n="31.3">ont</w> <w n="31.4">toujours</w> <w n="31.5">auprès</w> <w n="31.6">d</w>’<w n="31.7">elles</w></l>
						<l n="32" num="1.32"><w n="32.1">Plus</w> <w n="32.2">d</w>’<w n="32.3">aisance</w> <w n="32.4">que</w> <w n="32.5">de</w> <w n="32.6">faveurs</w>.</l>
						<l n="33" num="1.33"><w n="33.1">De</w> <w n="33.2">toutes</w> <w n="33.3">leurs</w> <w n="33.4">bonnes</w> <w n="33.5">fortunes</w></l>
						<l n="34" num="1.34"><w n="34.1">Ils</w> <w n="34.2">ne</w> <w n="34.3">se</w> <w n="34.4">prévalent</w> <w n="34.5">jamais</w>,</l>
						<l n="35" num="1.35"><w n="35.1">Leurs</w> <w n="35.2">maîtresses</w> <w n="35.3">sont</w> <w n="35.4">si</w> <w n="35.5">communes</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Que</w> <w n="36.2">la</w> <w n="36.3">honte</w> <w n="36.4">les</w> <w n="36.5">rend</w> <w n="36.6">discrets</w>.</l>
						<l n="37" num="1.37"><w n="37.1">Ils</w> <w n="37.2">préfèrent</w>, <w n="37.3">dans</w> <w n="37.4">leur</w> <w n="37.5">ivresse</w>,</l>
						<l n="38" num="1.38"><w n="38.1">La</w> <w n="38.2">débauche</w> <w n="38.3">aux</w> <w n="38.4">plus</w> <w n="38.5">doux</w> <w n="38.6">plaisirs</w>,</l>
						<l n="39" num="1.39"><w n="39.1">Et</w> <w n="39.2">goûtent</w> <w n="39.3">sans</w> <w n="39.4">délicatesse</w></l>
						<l n="40" num="1.40"><w n="40.1">Des</w> <w n="40.2">jouissances</w> <w n="40.3">sans</w> <w n="40.4">désirs</w>.</l>
						<l n="41" num="1.41"><w n="41.1">Puissent</w> <w n="41.2">la</w> <w n="41.3">volupté</w>, <w n="41.4">les</w> <w n="41.5">grâces</w>,</l>
						<l n="42" num="1.42"><w n="42.1">Les</w> <w n="42.2">expulser</w> <w n="42.3">loin</w> <w n="42.4">de</w> <w n="42.5">leur</w> <w n="42.6">cour</w>,</l>
						<l n="43" num="1.43"><w n="43.1">Et</w> <w n="43.2">favoriser</w> <w n="43.3">en</w> <w n="43.4">leurs</w> <w n="43.5">places</w></l>
						<l n="44" num="1.44"><w n="44.1">La</w> <w n="44.2">gaîté</w>, <w n="44.3">l</w>’<w n="44.4">esprit</w> <w n="44.5">et</w> <w n="44.6">l</w>’<w n="44.7">amour</w> !</l>
						<l n="45" num="1.45"><w n="45.1">Les</w> <w n="45.2">déserteurs</w> <w n="45.3">de</w> <w n="45.4">la</w> <w n="45.5">tendresse</w></l>
						<l n="46" num="1.46"><w n="46.1">Doivent</w>-<w n="46.2">ils</w> <w n="46.3">goûter</w> <w n="46.4">ses</w> <w n="46.5">douceurs</w> ?</l>
						<l n="47" num="1.47"><w n="47.1">Quand</w> <w n="47.2">ils</w> <w n="47.3">dégradent</w> <w n="47.4">la</w> <w n="47.5">jeunesse</w>,</l>
						<l n="48" num="1.48"><w n="48.1">En</w> <w n="48.2">doivent</w>-<w n="48.3">ils</w> <w n="48.4">cueillir</w> <w n="48.5">les</w> <w n="48.6">fleurs</w> ?</l>
					</lg>
				</div></body></text></TEI>