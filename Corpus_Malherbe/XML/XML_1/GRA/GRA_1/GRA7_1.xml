<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SEXTINES</title>
				<title type="medium">Édition électronique</title>
				<author key="GRA">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>GRAMONT</surname>
					</name>
					<date when="1897">1815-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>390 vers39 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2015</date>
				<idno type="local">GRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SEXTINES</title>
						<author>Comte de Gramont</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Livre :Gramont_-_Sextines,_1872.djvu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">SEXTINES</title>
								<author>Comte de Gramont</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRA7">
				<head type="main">SEXTINE VII.</head>
				<head type="sub">REGARD EN ARRIÈRE.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">arrive</w>, <w n="1.3">et</w> <w n="1.4">le</w> <w n="1.5">fait</w> <w n="1.6">n</w>’<w n="1.7">a</w> <w n="1.8">même</w> <w n="1.9">rien</w> <w n="1.10">d</w>’<w n="1.11">étrange</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">des</w> <w n="2.3">lieux</w> <w n="2.4">qu</w>’<w n="2.5">on</w> <w n="2.6">a</w> <w n="2.7">vus</w> <w n="2.8">jadis</w> <w n="2.9">avec</w> <w n="2.10">plaisir</w></l>
						<l n="3" num="1.3"><w n="3.1">Paraissent</w> <w n="3.2">à</w> <w n="3.3">présent</w> <w n="3.4">sans</w> <w n="3.5">charme</w>, <w n="3.6">et</w> <w n="3.7">qu</w>’<w n="3.8">en</w> <w n="3.9">échange</w></l>
						<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2">autres</w> <w n="4.3">qu</w>’<w n="4.4">on</w> <w n="4.5">n</w>’<w n="4.6">aimait</w> <w n="4.7">pas</w> <w n="4.8">sont</w>, <w n="4.9">par</w> <w n="4.10">quelque</w> <w n="4.11">mélange</w></l>
						<l n="5" num="1.5"><w n="5.1">De</w> <w n="5.2">sentiments</w> <w n="5.3">nouveaux</w>, <w n="5.4">un</w> <w n="5.5">objet</w> <w n="5.6">de</w> <w n="5.7">désir</w>.</l>
						<l n="6" num="1.6"><w n="6.1">On</w> <w n="6.2">y</w> <w n="6.3">pense</w>, <w n="6.4">on</w> <w n="6.5">voudrait</w> <w n="6.6">les</w> <w n="6.7">revoir</w> <w n="6.8">à</w> <w n="6.9">loisir</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Jeune</w>, <w n="7.2">bien</w> <w n="7.3">à</w> <w n="7.4">regret</w> <w n="7.5">quittant</w> <w n="7.6">un</w> <w n="7.7">cher</w> <w n="7.8">loisir</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Je</w> <w n="8.2">fus</w> <w n="8.3">l</w>’<w n="8.4">hôte</w> <w n="8.5">six</w> <w n="8.6">mois</w> <w n="8.7">d</w>’<w n="8.8">une</w> <w n="8.9">contrée</w> <w n="8.10">étrange</w>.</l>
						<l n="9" num="2.3"><w n="9.1">Désolée</w> ; <w n="9.2">un</w> <w n="9.3">désert</w>. <w n="9.4">Mon</w> <w n="9.5">unique</w> <w n="9.6">désir</w></l>
						<l n="10" num="2.4"><w n="10.1">Était</w> <w n="10.2">de</w> <w n="10.3">retourner</w> <w n="10.4">à</w> <w n="10.5">Paris</w>, <w n="10.6">mon</w> <w n="10.7">plaisir</w></l>
						<l n="11" num="2.5"><w n="11.1">D</w>’<w n="11.2">en</w> <w n="11.3">retrouver</w> <w n="11.4">la</w> <w n="11.5">vie</w> <w n="11.6">avec</w> <w n="11.7">tout</w> <w n="11.8">son</w> <w n="11.9">mélange</w></l>
						<l n="12" num="2.6"><w n="12.1">De</w> <w n="12.2">mouvements</w>, <w n="12.3">de</w> <w n="12.4">bruits</w>, <w n="12.5">de</w> <w n="12.6">pensers</w> <w n="12.7">qu</w>’<w n="12.8">on</w> <w n="12.9">échange</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Joyeux</w>, <w n="13.2">j</w>’<w n="13.3">abjurai</w> <w n="13.4">donc</w> <w n="13.5">ce</w> <w n="13.6">séjour</w> ; <w n="13.7">mais</w> <w n="13.8">l</w>’<w n="13.9">échange</w></l>
						<l n="14" num="3.2"><w n="14.1">Aux</w> <w n="14.2">rêves</w> <w n="14.3">poursuivis</w> <w n="14.4">rendit</w> <w n="14.5">peu</w> <w n="14.6">de</w> <w n="14.7">loisir</w>.</l>
						<l n="15" num="3.3"><w n="15.1">Que</w> <w n="15.2">de</w> <w n="15.3">déceptions</w> <w n="15.4">m</w>’<w n="15.5">attendaient</w> ! <w n="15.6">Quel</w> <w n="15.7">mélange</w></l>
						<l n="16" num="3.4"><w n="16.1">De</w> <w n="16.2">soucis</w>, <w n="16.3">de</w> <w n="16.4">dégoûts</w> <w n="16.5">dans</w> <w n="16.6">un</w> <w n="16.7">exil</w> <w n="16.8">étrange</w></l>
						<l n="17" num="3.5"><w n="17.1">M</w>’<w n="17.2">étreignit</w>, <w n="17.3">sans</w> <w n="17.4">jamais</w> <w n="17.5">qu</w>’<w n="17.6">un</w> <w n="17.7">vulgaire</w> <w n="17.8">plaisir</w></l>
						<l n="18" num="3.6"><w n="18.1">Usurpât</w> <w n="18.2">de</w> <w n="18.3">mon</w> <w n="18.4">cœur</w> <w n="18.5">le</w> <w n="18.6">farouche</w> <w n="18.7">désir</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Il</w> <w n="19.2">n</w>’<w n="19.3">était</w> <w n="19.4">qu</w>’<w n="19.5">un</w> <w n="19.6">seul</w> <w n="19.7">prix</w> <w n="19.8">pour</w> <w n="19.9">combler</w> <w n="19.10">ce</w> <w n="19.11">désir</w> ;</l>
						<l n="20" num="4.2"><w n="20.1">Tout</w> <w n="20.2">autre</w> <w n="20.3">m</w>’<w n="20.4">m</w>’<w n="20.5">eût</w> <w n="20.6">offert</w> <w n="20.7">un</w> <w n="20.8">misérable</w> <w n="20.9">échange</w>.</l>
						<l n="21" num="4.3"><w n="21.1">Quoi</w> ! <w n="21.2">de</w> <w n="21.3">l</w>’<w n="21.4">astre</w> <w n="21.5">au</w> <w n="21.6">lampion</w> <w n="21.7">restreindre</w> <w n="21.8">son</w> <w n="21.9">plaisir</w></l>
						<l n="22" num="4.4"><w n="22.1">On</w> <w n="22.2">en</w> <w n="22.3">vient</w> <w n="22.4">là</w> <w n="22.5">pourtant</w>. <w n="22.6">Je</w> <w n="22.7">n</w>’<w n="22.8">eus</w> <w n="22.9">pas</w> <w n="22.10">ce</w> <w n="22.11">loisir</w>.</l>
						<l n="23" num="4.5"><w n="23.1">Je</w> <w n="23.2">m</w>’<w n="23.3">enfuis</w>, <w n="23.4">ne</w> <w n="23.5">gardant</w> <w n="23.6">que</w> <w n="23.7">ce</w> <w n="23.8">trésor</w> <w n="23.9">étrange</w>.</l>
						<l n="24" num="4.6"><w n="24.1">Un</w> <w n="24.2">vouloir</w> <w n="24.3">lumineux</w>, <w n="24.4">sans</w> <w n="24.5">nul</w> <w n="24.6">douteux</w> <w n="24.7">mélange</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Quelque</w> <w n="25.2">refuge</w> <w n="25.3">alors</w>, <w n="25.4">libre</w> <w n="25.5">de</w> <w n="25.6">tout</w> <w n="25.7">mélange</w></l>
						<l n="26" num="5.2"><w n="26.1">De</w> <w n="26.2">pas</w>, <w n="26.3">d</w>’<w n="26.4">échos</w> <w n="26.5">humains</w>, <w n="26.6">ce</w> <w n="26.7">fut</w> <w n="26.8">là</w> <w n="26.9">mon</w> <w n="26.10">désir</w> ;</l>
						<l n="27" num="5.3"><w n="27.1">Et</w> <w n="27.2">l</w>’<w n="27.3">aspect</w> <w n="27.4">me</w> <w n="27.5">revint</w> <w n="27.6">de</w> <w n="27.7">la</w> <w n="27.8">contrée</w> <w n="27.9">étrange</w>.</l>
						<l n="28" num="5.4"><w n="28.1">Mon</w> <w n="28.2">ennui</w> <w n="28.3">d</w>’<w n="28.4">autrefois</w> ; <w n="28.5">et</w> <w n="28.6">j</w>’<w n="28.7">invoquai</w> <w n="28.8">l</w>’<w n="28.9">échange</w></l>
						<l n="29" num="5.5"><w n="29.1">Qui</w> <w n="29.2">m</w>’<w n="29.3">eût</w> <w n="29.4">des</w> <w n="29.5">bois</w> <w n="29.6">riants</w> <w n="29.7">où</w> <w n="29.8">s</w>’<w n="29.9">ébat</w> <w n="29.10">le</w> <w n="29.11">loisir</w></l>
						<l n="30" num="5.6"><w n="30.1">Remis</w> <w n="30.2">en</w> <w n="30.3">ces</w> <w n="30.4">déserts</w> <w n="30.5">hostiles</w> <w n="30.6">au</w> <w n="30.7">plaisir</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Leur</w> <w n="31.2">calme</w>, <w n="31.3">leur</w> <w n="31.4">silence</w> <w n="31.5">eût</w> <w n="31.6">été</w> <w n="31.7">mon</w> <w n="31.8">plaisir</w>.</l>
						<l n="32" num="6.2"><w n="32.1">La</w> <w n="32.2">terre</w> <w n="32.3">s</w>’<w n="32.4">effaçait</w> ; <w n="32.5">le</w> <w n="32.6">grand</w> <w n="32.7">ciel</w> <w n="32.8">sans</w> <w n="32.9">mélange</w>,</l>
						<l n="33" num="6.3"><w n="33.1">M</w>’<w n="33.2">embrassant</w>, <w n="33.3">ne</w> <w n="33.4">laissait</w> <w n="33.5">aux</w> <w n="33.6">sens</w> <w n="33.7">aucun</w> <w n="33.8">loisir</w>.</l>
						<l n="34" num="6.4"><w n="34.1">Plus</w> <w n="34.2">de</w> <w n="34.3">souffles</w> <w n="34.4">troublant</w> <w n="34.5">le</w> <w n="34.6">vol</w> <w n="34.7">de</w> <w n="34.8">mon</w> <w n="34.9">désir</w> !</l>
						<l n="35" num="6.5"><w n="35.1">J</w>’<w n="35.2">aurais</w>, <w n="35.3">sans</w> <w n="35.4">craindre</w> <w n="35.5">même</w> <w n="35.6">un</w> <w n="35.7">éphémère</w> <w n="35.8">échange</w>,</l>
						<l n="36" num="6.6"><w n="36.1">Brûlé</w> <w n="36.2">ma</w> <w n="36.3">vie</w> <w n="36.4">entière</w> <w n="36.5">en</w> <w n="36.6">une</w> <w n="36.7">extase</w> <w n="36.8">étrange</w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Chose</w> <w n="37.2">étrange</w> ! <w n="37.3">et</w> <w n="37.4">j</w>’<w n="37.5">ai</w> <w n="37.6">dû</w>, <w n="37.7">bien</w> <w n="37.8">loin</w> <w n="37.9">d</w>’<w n="37.10">un</w> <w n="37.11">tel</w> <w n="37.12">plaisir</w>,</l>
						<l n="38" num="7.2"><w n="38.1">En</w> <w n="38.2">échange</w> <w n="38.3">accepter</w> <w n="38.4">ce</w> <w n="38.5">douloureux</w> <w n="38.6">mélange</w></l>
						<l n="39" num="7.3"><w n="39.1">Du</w> <w n="39.2">désir</w> <w n="39.3">rayonnant</w> <w n="39.4">joint</w> <w n="39.5">au</w> <w n="39.6">morne</w> <w n="39.7">loisir</w>.</l>
					</lg>
			</div></body></text></TEI>