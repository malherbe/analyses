<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SEXTINES</title>
				<title type="medium">Édition électronique</title>
				<author key="GRA">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>GRAMONT</surname>
					</name>
					<date when="1897">1815-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>390 vers39 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2015</date>
				<idno type="local">GRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SEXTINES</title>
						<author>Comte de Gramont</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Livre :Gramont_-_Sextines,_1872.djvu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">SEXTINES</title>
								<author>Comte de Gramont</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRA2">
				<head type="main">SEXTINE II.</head>
				<head type="sub">AUTOUR D’UN ÉTANG.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">étang</w> <w n="1.3">qui</w> <w n="1.4">s</w>’<w n="1.5">éclaircit</w> <w n="1.6">au</w> <w n="1.7">milieu</w> <w n="1.8">des</w> <w n="1.9">feuillages</w>,</l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">mare</w> <w n="2.3">avec</w> <w n="2.4">ses</w> <w n="2.5">joncs</w> <w n="2.6">rubanant</w> <w n="2.7">au</w> <w n="2.8">soleil</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ses</w> <w n="3.2">flottilles</w> <w n="3.3">de</w> <w n="3.4">fleurs</w>, <w n="3.5">ses</w> <w n="3.6">insectes</w> <w n="3.7">volages</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Me</w> <w n="4.2">charment</w>. <w n="4.3">Longuement</w> <w n="4.4">au</w> <w n="4.5">creux</w> <w n="4.6">de</w> <w n="4.7">leurs</w> <w n="4.8">rivages</w></l>
						<l n="5" num="1.5"><w n="5.1">J</w>’<w n="5.2">erre</w>, <w n="5.3">et</w>, <w n="5.4">les</w> <w n="5.5">yeux</w> <w n="5.6">remplis</w> <w n="5.7">d</w>’<w n="5.8">un</w> <w n="5.9">mirage</w> <w n="5.10">vermeil</w>,</l>
						<l n="6" num="1.6"><w n="6.1">J</w>’<w n="6.2">écoute</w> <w n="6.3">l</w>’<w n="6.4">eau</w> <w n="6.5">qui</w> <w n="6.6">rêve</w> <w n="6.7">en</w> <w n="6.8">son</w> <w n="6.9">tiède</w> <w n="6.10">sommeil</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Moi</w>-<w n="7.2">même</w> <w n="7.3">j</w>’<w n="7.4">ai</w> <w n="7.5">mon</w> <w n="7.6">rêve</w> <w n="7.7">et</w> <w n="7.8">mon</w> <w n="7.9">demi</w>-<w n="7.10">sommeil</w>.</l>
						<l n="8" num="2.2"><w n="8.1">De</w> <w n="8.2">fêriques</w> <w n="8.3">sentiers</w> <w n="8.4">s</w>’<w n="8.5">ouvrent</w> <w n="8.6">sous</w> <w n="8.7">les</w> <w n="8.8">feuillages</w></l>
						<l n="9" num="2.3"><w n="9.1">Les</w> <w n="9.2">uns</w>, <w n="9.3">en</w> <w n="9.4">se</w> <w n="9.5">hâtant</w> <w n="9.6">vers</w> <w n="9.7">le</w> <w n="9.8">coteau</w> <w n="9.9">vermeil</w>.</l>
						<l n="10" num="2.4"><w n="10.1">Ondulent</w> <w n="10.2">transpercés</w> <w n="10.3">d</w>’<w n="10.4">un</w> <w n="10.5">rayon</w> <w n="10.6">de</w> <w n="10.7">soleil</w> ;</l>
						<l n="11" num="2.5"><w n="11.1">Les</w> <w n="11.2">autres</w> <w n="11.3">indécis</w>, <w n="11.4">contournant</w> <w n="11.5">les</w> <w n="11.6">rivages</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Foisonnent</w> <w n="12.2">d</w>’<w n="12.3">ombre</w> <w n="12.4">bleue</w> <w n="12.5">et</w> <w n="12.6">de</w> <w n="12.7">lueurs</w> <w n="12.8">volages</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Tous</w> <w n="13.2">se</w> <w n="13.3">peuplent</w> <w n="13.4">pour</w> <w n="13.5">moi</w> <w n="13.6">de</w> <w n="13.7">figures</w> <w n="13.8">volages</w></l>
						<l n="14" num="3.2"><w n="14.1">Qu</w>’<w n="14.2">à</w> <w n="14.3">mon</w> <w n="14.4">chevet</w> <w n="14.5">parfois</w> <w n="14.6">évoque</w> <w n="14.7">le</w> <w n="14.8">sommeil</w>.</l>
						<l n="15" num="3.3"><w n="15.1">Mais</w> <w n="15.2">qui</w> <w n="15.3">bien</w> <w n="15.4">mieux</w> <w n="15.5">encor</w> <w n="15.6">sur</w> <w n="15.7">ces</w> <w n="15.8">vagues</w> <w n="15.9">rivages</w></l>
						<l n="16" num="3.4"><w n="16.1">Reviennent</w>, <w n="16.2">souriant</w> <w n="16.3">aux</w> <w n="16.4">mailles</w> <w n="16.5">des</w> <w n="16.6">feuillages</w> :</l>
						<l n="17" num="3.5"><w n="17.1">Fantômes</w> <w n="17.2">lumineux</w>, <w n="17.3">songes</w> <w n="17.4">du</w> <w n="17.5">plein</w> <w n="17.6">soleil</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Visions</w> <w n="18.2">qui</w> <w n="18.3">font</w> <w n="18.4">l</w>’<w n="18.5">air</w> <w n="18.6">comme</w> <w n="18.7">au</w> <w n="18.8">matin</w> <w n="18.9">vermeil</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">l</w>’<w n="19.4">Ondine</w> <w n="19.5">sur</w> <w n="19.6">l</w>’<w n="19.7">eau</w> <w n="19.8">montrant</w> <w n="19.9">son</w> <w n="19.10">front</w> <w n="19.11">vermeil</w></l>
						<l n="20" num="4.2"><w n="20.1">Un</w> <w n="20.2">instant</w> ; <w n="20.3">c</w>’<w n="20.4">est</w> <w n="20.5">l</w>’<w n="20.6">éclair</w> <w n="20.7">des</w> <w n="20.8">Sylphides</w> <w n="20.9">volages</w></l>
						<l n="21" num="4.3"><w n="21.1">D</w>’<w n="21.2">un</w> <w n="21.3">sillage</w> <w n="21.4">argentin</w> <w n="21.5">rayant</w> <w n="21.6">l</w>’<w n="21.7">or</w> <w n="21.8">du</w> <w n="21.9">soleil</w> ;</l>
						<l n="22" num="4.4"><w n="22.1">C</w>’<w n="22.2">est</w> <w n="22.3">la</w> <w n="22.4">Muse</w> <w n="22.5">ondoyant</w> <w n="22.6">comme</w> <w n="22.7">au</w> <w n="22.8">sein</w> <w n="22.9">du</w> <w n="22.10">sommeil</w></l>
						<l n="23" num="4.5"><w n="23.1">Et</w> <w n="23.2">qui</w> <w n="23.3">dit</w> : « <w n="23.4">Me</w> <w n="23.5">voici</w> ! » <w n="23.6">c</w>’<w n="23.7">est</w> <w n="23.8">parmi</w> <w n="23.9">les</w> <w n="23.10">feuillages</w></l>
						<l n="24" num="4.6"><w n="24.1">Quelque</w> <w n="24.2">blancheur</w> <w n="24.3">de</w> <w n="24.4">fée</w>… <w n="24.5">O</w> <w n="24.6">gracieux</w> <w n="24.7">rivages</w>,</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">En</w> <w n="25.2">vain</w> <w n="25.3">j</w>’<w n="25.4">irais</w> <w n="25.5">chercher</w> <w n="25.6">de</w> <w n="25.7">plus</w> <w n="25.8">nobles</w> <w n="25.9">rivages</w> :</l>
						<l n="26" num="5.2"><w n="26.1">Pactole</w> <w n="26.2">aux</w> <w n="26.3">sables</w> <w n="26.4">d</w>’<w n="26.5">or</w>, <w n="26.6">Bosphore</w> <w n="26.7">au</w> <w n="26.8">flot</w> <w n="26.9">vermeil</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Aganippe</w>, <w n="27.2">Permesse</w> <w n="27.3">aux</w> <w n="27.4">éloquents</w> <w n="27.5">feuillages</w>,</l>
						<l n="28" num="5.4"><w n="28.1">Pénée</w> <w n="28.2">avec</w> <w n="28.3">ses</w> <w n="28.4">fleurs</w>, <w n="28.5">Hèbre</w> <w n="28.6">et</w> <w n="28.7">ses</w> <w n="28.8">chœurs</w> <w n="28.9">volages</w>,</l>
						<l n="29" num="5.5"><w n="29.1">Éridan</w> <w n="29.2">mugissant</w>, <w n="29.3">Mincie</w> <w n="29.4">au</w> <w n="29.5">frais</w> <w n="29.6">sommeil</w>.</l>
						<l n="30" num="5.6"><w n="30.1">Et</w> <w n="30.2">Tibre</w> <w n="30.3">que</w> <w n="30.4">couronne</w> <w n="30.5">un</w> <w n="30.6">éternel</w> <w n="30.7">soleil</w> ;</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Non</w>, <w n="31.2">tous</w> <w n="31.3">ces</w> <w n="31.4">bords</w> <w n="31.5">fameux</w> <w n="31.6">n</w>’<w n="31.7">auraient</w> <w n="31.8">point</w> <w n="31.9">ce</w> <w n="31.10">soleil</w></l>
						<l n="32" num="6.2"><w n="32.1">Que</w> <w n="32.2">me</w> <w n="32.3">rend</w> <w n="32.4">votre</w> <w n="32.5">aspect</w>, <w n="32.6">anonymes</w> <w n="32.7">rivages</w> !</l>
						<l n="33" num="6.3"><w n="33.1">Du</w> <w n="33.2">présent</w> <w n="33.3">nébuleux</w> <w n="33.4">animant</w> <w n="33.5">le</w> <w n="33.6">sommeil</w>,</l>
						<l n="34" num="6.4"><w n="34.1">Il</w> <w n="34.2">y</w> <w n="34.3">fait</w> <w n="34.4">refleurir</w> <w n="34.5">le</w> <w n="34.6">souvenir</w> <w n="34.7">vermeil</w></l>
						<l n="35" num="6.5"><w n="35.1">Et</w> <w n="35.2">sonner</w> <w n="35.3">du</w> <w n="35.4">printemps</w> <w n="35.5">tous</w> <w n="35.6">les</w> <w n="35.7">échos</w> <w n="35.8">volages</w></l>
						<l n="36" num="6.6"><w n="36.1">Dans</w> <w n="36.2">les</w> <w n="36.3">rameaux</w> <w n="36.4">jaunis</w> <w n="36.5">non</w> <w n="36.6">moins</w> <w n="36.7">qu</w>’<w n="36.8">aux</w> <w n="36.9">verts</w> <w n="36.10">feuillages</w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">Doux</w> <w n="37.2">feuillages</w>, <w n="37.3">adieu</w>. <w n="37.4">Vainement</w> <w n="37.5">du</w> <w n="37.6">soleil</w></l>
						<l n="38" num="7.2"><w n="38.1">Les</w> <w n="38.2">volages</w> <w n="38.3">clartés</w> <w n="38.4">auront</w> <w n="38.5">fui</w> <w n="38.6">ces</w> <w n="38.7">rivages</w>.</l>
						<l n="39" num="7.3"><w n="39.1">Ce</w> <w n="39.2">jour</w> <w n="39.3">vermeil</w> <w n="39.4">luira</w> <w n="39.5">jusque</w> <w n="39.6">dans</w> <w n="39.7">mon</w> <w n="39.8">sommeil</w>.</l>
					</lg>
			</div></body></text></TEI>