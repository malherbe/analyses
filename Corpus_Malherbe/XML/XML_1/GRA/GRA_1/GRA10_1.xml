<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">SEXTINES</title>
				<title type="medium">Édition électronique</title>
				<author key="GRA">
					<name>
						<forename>Ferdinand</forename>
						<nameLink>de</nameLink>
						<surname>GRAMONT</surname>
					</name>
					<date when="1897">1815-1897</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>390 vers39 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2015</date>
				<idno type="local">GRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">SEXTINES</title>
						<author>Comte de Gramont</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Livre :Gramont_-_Sextines,_1872.djvu</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">SEXTINES</title>
								<author>Comte de Gramont</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
									<date when="1872">1872</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1872">1872</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique).
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-27" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="GRA10">
				<head type="main">SEXTINE X.</head>
				<head type="sub">FUITE AU DÉSERT.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qui</w>, <w n="1.2">dans</w> <w n="1.3">ces</w> <w n="1.4">tristes</w> <w n="1.5">Jours</w>, <w n="1.6">n</w>’<w n="1.7">a</w> <w n="1.8">rêvé</w> <w n="1.9">d</w>’<w n="1.10">une</w> <w n="1.11">hutte</w>,</l>
						<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2">a</w> <w n="2.3">rêvé</w> <w n="2.4">de</w> <w n="2.5">s</w>’<w n="2.6">enfuir</w> <w n="2.7">quelque</w> <w n="2.8">part</w> <w n="2.9">au</w> <w n="2.10">désert</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Loin</w> <w n="3.2">de</w> <w n="3.3">ces</w> <w n="3.4">vils</w> <w n="3.5">tracas</w> <w n="3.6">oui</w> <w n="3.7">la</w> <w n="3.8">vie</w> <w n="3.9">est</w> <w n="3.10">en</w> <w n="3.11">butte</w> !</l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">quoi</w> <w n="4.3">bon</w> <w n="4.4">prolonger</w> <w n="4.5">une</w> <w n="4.6">stérile</w> <w n="4.7">lutte</w> ?</l>
						<l n="5" num="1.5"><w n="5.1">Les</w> <w n="5.2">bois</w> <w n="5.3">vont</w> <w n="5.4">me</w> <w n="5.5">donner</w> <w n="5.6">le</w> <w n="5.7">vivre</w> <w n="5.8">et</w> <w n="5.9">le</w> <w n="5.10">couvert</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">leurs</w> <w n="6.3">voix</w> <w n="6.4">m</w>’<w n="6.5">entourer</w> <w n="6.6">d</w>’<w n="6.7">un</w> <w n="6.8">bienfaisant</w> <w n="6.9">concert</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Ou</w> <w n="7.2">bien</w> <w n="7.3">au</w> <w n="7.4">bord</w> <w n="7.5">des</w> <w n="7.6">flots</w>, <w n="7.7">bercé</w> <w n="7.8">par</w> <w n="7.9">leur</w> <w n="7.10">concert</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Dans</w> <w n="8.2">un</w> <w n="8.3">creux</w> <w n="8.4">de</w> <w n="8.5">rochers</w> <w n="8.6">j</w>’<w n="8.7">attacherai</w> <w n="8.8">ma</w> <w n="8.9">hutte</w>.</l>
						<l n="9" num="2.3"><w n="9.1">Battu</w> <w n="9.2">des</w> <w n="9.3">vents</w>, <w n="9.4">du</w> <w n="9.5">moins</w> <w n="9.6">là</w> <w n="9.7">je</w> <w n="9.8">suis</w> <w n="9.9">à</w> <w n="9.10">couvert</w></l>
						<l n="10" num="2.4"><w n="10.1">Des</w> <w n="10.2">souffles</w> <w n="10.3">qui</w> <w n="10.4">bientôt</w> <w n="10.5">font</w> <w n="10.6">de</w> <w n="10.7">l</w>’<w n="10.8">âme</w> <w n="10.9">un</w> <w n="10.10">désert</w>.</l>
						<l n="11" num="2.5"><w n="11.1">Que</w> <w n="11.2">la</w> <w n="11.3">tempête</w> <w n="11.4">arrive</w>, <w n="11.5">et</w> <w n="11.6">la</w> <w n="11.7">foudre</w> <w n="11.8">et</w> <w n="11.9">la</w> <w n="11.10">lutte</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Que</w> <w n="12.2">l</w>’<w n="12.3">ombre</w> <w n="12.4">sous</w> <w n="12.5">les</w> <w n="12.6">deux</w>, <w n="12.7">comme</w> <w n="12.8">une</w> <w n="12.9">énorme</w> <w n="12.10">butte</w></l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Monte</w> ; <w n="13.2">paisiblement</w> <w n="13.3">du</w> <w n="13.4">haut</w> <w n="13.5">de</w> <w n="13.6">quelque</w> <w n="13.7">butte</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Cependant</w> <w n="14.2">que</w> <w n="14.3">mugit</w> <w n="14.4">le</w> <w n="14.5">sauvage</w> <w n="14.6">concert</w>.</l>
						<l n="15" num="3.3"><w n="15.1">Des</w> <w n="15.2">fauves</w> <w n="15.3">éléments</w> <w n="15.4">Je</w> <w n="15.5">contemple</w> <w n="15.6">la</w> <w n="15.7">lutte</w>.</l>
						<l n="16" num="3.4"><w n="16.1">Dussé</w>-<w n="16.2">Je</w> <w n="16.3">en</w> <w n="16.4">débris</w> <w n="16.5">même</w> <w n="16.6">y</w> <w n="16.7">voir</w> <w n="16.8">crouler</w> <w n="16.9">ma</w> <w n="16.10">hutte</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Qu</w>’<w n="17.2">importe</w> ! <w n="17.3">Les</w> <w n="17.4">buissons</w>, <w n="17.5">les</w> <w n="17.6">antres</w> <w n="17.7">du</w> <w n="17.8">désert</w></l>
						<l n="18" num="3.6"><w n="18.1">A</w> <w n="18.2">l</w>’<w n="18.3">hôte</w> <w n="18.4">familier</w> <w n="18.5">suffiront</w> <w n="18.6">pour</w> <w n="18.7">couvert</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Puis</w>, <w n="19.2">le</w> <w n="19.3">beau</w> <w n="19.4">ciel</w> <w n="19.5">d</w>’<w n="19.6">azur</w> <w n="19.7">succède</w> <w n="19.8">au</w> <w n="19.9">ciel</w> <w n="19.10">couvert</w>.</l>
						<l n="20" num="4.2"><w n="20.1">Les</w> <w n="20.2">grèves</w> <w n="20.3">et</w> <w n="20.4">les</w> <w n="20.5">eaux</w>, <w n="20.6">les</w> <w n="20.7">gazons</w> <w n="20.8">de</w> <w n="20.9">la</w> <w n="20.10">butte</w>,</l>
						<l n="21" num="4.3"><w n="21.1">Tout</w> <w n="21.2">brille</w>. <w n="21.3">Un</w> <w n="21.4">manteau</w> <w n="21.5">d</w>’<w n="21.6">or</w> <w n="21.7">flotte</w> <w n="21.8">sur</w> <w n="21.9">le</w> <w n="21.10">désert</w>.</l>
						<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">murmures</w>, <w n="22.3">rayons</w>, <w n="22.4">atomes</w> <w n="22.5">de</w> <w n="22.6">concert</w></l>
						<l n="23" num="4.5"><w n="23.1">Semblent</w> <w n="23.2">me</w> <w n="23.3">convier</w> <w n="23.4">à</w> <w n="23.5">relever</w> <w n="23.6">la</w> <w n="23.7">hutte</w>.</l>
						<l n="24" num="4.6"><w n="24.1">Une</w> <w n="24.2">immense</w> <w n="24.3">caresse</w> <w n="24.4">a</w> <w n="24.5">remplacé</w> <w n="24.6">la</w> <w n="24.7">lutte</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Rien</w> <w n="25.2">d</w>’<w n="25.3">impur</w>, <w n="25.4">de</w> <w n="25.5">malsain</w> <w n="25.6">ne</w> <w n="25.7">survit</w> <w n="25.8">à</w> <w n="25.9">la</w> <w n="25.10">lutte</w>.</l>
						<l n="26" num="5.2"><w n="26.1">Pas</w> <w n="26.2">de</w> <w n="26.3">levain</w> <w n="26.4">haineux</w>, <w n="26.5">pas</w> <w n="26.6">de</w> <w n="26.7">poison</w> <w n="26.8">couvert</w> !</l>
						<l n="27" num="5.3"><w n="27.1">Ah</w> ! <w n="27.2">lorsqu</w>’<w n="27.3">un</w> <w n="27.4">autre</w> <w n="27.5">orage</w> <w n="27.6">a</w> <w n="27.7">fait</w> <w n="27.8">rugir</w> <w n="27.9">la</w> <w n="27.10">hutte</w>.</l>
						<l n="28" num="5.4"><w n="28.1">Le</w> <w n="28.2">bouge</w>, <w n="28.3">le</w> <w n="28.4">taudis</w> <w n="28.5">à</w> <w n="28.6">tant</w> <w n="28.7">de</w> <w n="28.8">maux</w> <w n="28.9">en</w> <w n="28.10">butte</w>,</l>
						<l n="29" num="5.5"><w n="29.1">Si</w> <w n="29.2">des</w> <w n="29.3">groupes</w> <w n="29.4">humains</w> <w n="29.5">reparaît</w> <w n="29.6">le</w> <w n="29.7">concert</w>,</l>
						<l n="30" num="5.6"><w n="30.1">Ce</w> <w n="30.2">n</w>’<w n="30.3">est</w> <w n="30.4">qu</w>’<w n="30.5">à</w> <w n="30.6">la</w> <w n="30.7">surface</w> <w n="30.8">et</w> <w n="30.9">non</w> <w n="30.10">comme</w> <w n="30.11">au</w> <w n="30.12">désert</w>.</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">O</w> <w n="31.2">mon</w> <w n="31.3">âme</w>, <w n="31.4">plus</w> <w n="31.5">loin</w>, <w n="31.6">plus</w> <w n="31.7">haut</w> <w n="31.8">que</w> <w n="31.9">le</w> <w n="31.10">désert</w></l>
						<l n="32" num="6.2"><w n="32.1">Ta</w> <w n="32.2">jailliras</w> ! <w n="32.3">Encor</w> <w n="32.4">quelques</w> <w n="32.5">instants</w> <w n="32.6">de</w> <w n="32.7">lutte</w>,</l>
						<l n="33" num="6.3"><w n="33.1">Et</w> <w n="33.2">dans</w> <w n="33.3">l</w>’<w n="33.4">éther</w> <w n="33.5">sacré</w>, <w n="33.6">l</w>’<w n="33.7">infaillible</w> <w n="33.8">concert</w>,</l>
						<l n="34" num="6.4"><w n="34.1">Calme</w>, <w n="34.2">tu</w> <w n="34.3">planeras</w>, <w n="34.4">pour</w> <w n="34.5">jamais</w> <w n="34.6">à</w> <w n="34.7">couvert</w></l>
						<l n="35" num="6.5"><w n="35.1">Des</w> <w n="35.2">sinistres</w> <w n="35.3">dégoûts</w> <w n="35.4">auxquels</w> <w n="35.5">tu</w> <w n="35.6">fus</w> <w n="35.7">en</w> <w n="35.8">butte</w>.</l>
						<l n="36" num="6.6"><w n="36.1">Émanés</w> <w n="36.2">des</w> <w n="36.3">palais</w> <w n="36.4">ou</w> <w n="36.5">sourdant</w> <w n="36.6">de</w> <w n="36.7">la</w> <w n="36.8">hutte</w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">La</w> <w n="37.2">hutte</w> <w n="37.3">solitude</w> <w n="37.4">est</w> <w n="37.5">partout</w> ; <w n="37.6">le</w> <w n="37.7">désert</w></l>
						<l n="38" num="7.2"><w n="38.1">Et</w> <w n="38.2">la</w> <w n="38.3">butte</w> <w n="38.4">où</w> <w n="38.5">gravir</w> <w n="38.6">sont</w> <w n="38.7">trouvés</w> <w n="38.8">pour</w> <w n="38.9">qui</w> <w n="38.10">lutte</w></l>
						<l n="39" num="7.3"><w n="39.1">Couvert</w> <w n="39.2">d</w>’<w n="39.3">une</w> <w n="39.4">foi</w> <w n="39.5">ferme</w> <w n="39.6">en</w> <w n="39.7">l</w>’<w n="39.8">éternel</w> <w n="39.9">concert</w>.</l>
					</lg>
			</div></body></text></TEI>