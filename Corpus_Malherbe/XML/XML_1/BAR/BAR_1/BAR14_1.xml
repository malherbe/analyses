<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR14">
				<head type="main">Le Buste Jaune</head>
				<opener>
					<salute>À mon grand ami le comte Roselly de Lorgues.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">Jour</w> <w n="1.3">meurt</w>, — <w n="1.4">et</w> <w n="1.5">la</w> <w n="1.6">Nuit</w> <w n="1.7">met</w> <w n="1.8">le</w> <w n="1.9">pied</w> <w n="1.10">sur</w> <w n="1.11">sa</w> <w n="1.12">tombe</w></l>
					<l n="2" num="1.2"><w n="2.1">Avec</w> <w n="2.2">le</w> <w n="2.3">noir</w> <w n="2.4">orgueil</w> <w n="2.5">d</w>’<w n="2.6">avoir</w> <w n="2.7">tué</w> <w n="2.8">le</w> <w n="2.9">Jour</w>.</l>
					<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">la</w> <w n="3.3">patère</w> <w n="3.4">au</w> <w n="3.5">sphinx</w> <w n="3.6">l</w>’<w n="3.7">épais</w> <w n="3.8">rideau</w> <w n="3.9">retombe</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">le</w> <w n="4.3">salon</w> <w n="4.4">désert</w> <w n="4.5">dans</w> <w n="4.6">son</w> <w n="4.7">vaste</w> <w n="4.8">pourtour</w></l>
					<l n="5" num="1.5"><space quantity="12" unit="char"></space><w n="5.1">A</w> <w n="5.2">pris</w> <w n="5.3">des</w> <w n="5.4">airs</w> <w n="5.5">de</w> <w n="5.6">catacombe</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Et</w> <w n="6.2">les</w> <w n="6.3">volets</w> <w n="6.4">fermés</w> <w n="6.5">par</w>-<w n="6.6">dessus</w> <w n="6.7">le</w> <w n="6.8">rideau</w></l>
					<l n="7" num="2.2"><w n="7.1">Ont</w> <w n="7.2">fait</w> <w n="7.3">comme</w> <w n="7.4">un</w> <w n="7.5">cercueil</w> <w n="7.6">à</w> <w n="7.7">ma</w> <w n="7.8">sombre</w> <w n="7.9">pensée</w>…</l>
					<l n="8" num="2.3"><w n="8.1">Je</w> <w n="8.2">suis</w> <w n="8.3">seul</w> <w n="8.4">comme</w> <w n="8.5">un</w> <w n="8.6">mort</w> ; — <w n="8.7">et</w> <w n="8.8">la</w> <w n="8.9">lampe</w> <w n="8.10">baissée</w></l>
					<l n="9" num="2.4"><w n="9.1">Sous</w> <w n="9.2">son</w> <w n="9.3">capuchon</w> <w n="9.4">noir</w> <w n="9.5">près</w> <w n="9.6">de</w> <w n="9.7">moi</w> <w n="9.8">déposée</w></l>
					<l n="10" num="2.5"><space quantity="12" unit="char"></space><w n="10.1">Semble</w> <w n="10.2">un</w> <w n="10.3">moine</w> <w n="10.4">sur</w> <w n="10.5">un</w> <w n="10.6">tombeau</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Et</w> <w n="11.2">les</w> <w n="11.3">vases</w> <w n="11.4">d</w>’<w n="11.5">albâtre</w> <w n="11.6">au</w> <w n="11.7">fond</w> <w n="11.8">des</w> <w n="11.9">encoignures</w></l>
					<l n="12" num="3.2"><w n="12.1">Blêmissent</w> <w n="12.2">vaporeux</w>, <w n="12.3">mais</w> <w n="12.4">paraissent</w> <w n="12.5">encor</w>.</l>
					<l n="13" num="3.3"><w n="13.1">Rien</w> <w n="13.2">ne</w> <w n="13.3">fait</w> <w n="13.4">plus</w> <w n="13.5">bouger</w> <w n="13.6">les</w> <w n="13.7">plis</w> <w n="13.8">lourds</w> <w n="13.9">des</w> <w n="13.10">tentures</w>…</l>
					<l n="14" num="3.4"><w n="14.1">Tout</w> <w n="14.2">se</w> <w n="14.3">tait</w>, — <w n="14.4">excepté</w> <w n="14.5">le</w> <w n="14.6">vent</w> <w n="14.7">du</w> <w n="14.8">corridor</w></l>
					<l n="15" num="3.5"><space quantity="12" unit="char"></space><w n="15.1">Qui</w> <w n="15.2">pleure</w> <w n="15.3">aussi</w> <w n="15.4">sur</w> <w n="15.5">les</w> <w n="15.6">toitures</w> !</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">Et</w> <w n="16.2">par</w> <w n="16.3">le</w> <w n="16.4">capuchon</w> <w n="16.5">de</w> <w n="16.6">la</w> <w n="16.7">lampe</w> <w n="16.8">assombris</w></l>
					<l n="17" num="4.2"><w n="17.1">Les</w> <w n="17.2">grands</w> <w n="17.3">murs</w> <w n="17.4">du</w> <w n="17.5">salon</w> <w n="17.6">semblent</w> <w n="17.7">plus</w> <w n="17.8">longs</w> <w n="17.9">d</w>’<w n="17.10">une</w> <w n="17.11">aune</w>…</l>
					<l n="18" num="4.3"><w n="18.1">Et</w> <w n="18.2">dans</w> <w n="18.3">le</w> <w n="18.4">clair</w>-<w n="18.5">obscur</w>, <w n="18.6">oscillant</w>, <w n="18.7">vague</w>, <w n="18.8">atone</w>,</l>
					<l n="19" num="4.4"><w n="19.1">On</w> <w n="19.2">voit</w> <w n="19.3">se</w> <w n="19.4">détacher</w> <w n="19.5">un</w> <w n="19.6">buste</w>, — <w n="19.7">un</w> <w n="19.8">buste</w> <w n="19.9">jaune</w></l>
					<l n="20" num="4.5"><space quantity="12" unit="char"></space><w n="20.1">Bombant</w> <w n="20.2">d</w>’<w n="20.3">un</w> <w n="20.4">angle</w> <w n="20.5">de</w> <w n="20.6">lambris</w>.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">un</w> <w n="21.4">beau</w> <w n="21.5">buste</w> <w n="21.6">blond</w>, — <w n="21.7">d</w>’<w n="21.8">un</w> <w n="21.9">blond</w> <w n="21.10">pâle</w>, — <w n="21.11">en</w> <w n="21.12">argile</w>,</l>
					<l n="22" num="5.2"><w n="22.1">Moulé</w> <w n="22.2">divinement</w> <w n="22.3">avec</w> <w n="22.4">un</w> <w n="22.5">art</w> <w n="22.6">charmant</w>.</l>
					<l n="23" num="5.3"><w n="23.1">Aucun</w> <w n="23.2">nom</w> <w n="23.3">ne</w> <w n="23.4">se</w> <w n="23.5">lit</w> <w n="23.6">sur</w> <w n="23.7">son</w> <w n="23.8">socle</w> <w n="23.9">fragile</w>.</l>
					<l n="24" num="5.4"><w n="24.1">Je</w> <w n="24.2">l</w>’<w n="24.3">ai</w> <w n="24.4">toujours</w> <w n="24.5">vu</w> <w n="24.6">là</w>, <w n="24.7">dans</w> <w n="24.8">ce</w> <w n="24.9">coin</w>, <w n="24.10">y</w> <w n="24.11">restant</w></l>
					<l n="25" num="5.5"><space quantity="12" unit="char"></space><w n="25.1">Comme</w> <w n="25.2">un</w> <w n="25.3">rêve</w>, — <w n="25.4">un</w> <w n="25.5">rêve</w> <w n="25.6">immobile</w>.</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">C</w>’<w n="26.2">est</w> <w n="26.3">un</w> <w n="26.4">buste</w> <w n="26.5">de</w> <w n="26.6">femme</w> <w n="26.7">aux</w> <w n="26.8">traits</w> <w n="26.9">busqués</w> <w n="26.10">et</w> <w n="26.11">fins</w>,</l>
					<l n="27" num="6.2"><w n="27.1">Aux</w> <w n="27.2">cheveux</w> <w n="27.3">relevés</w>, <w n="27.4">aux</w> <w n="27.5">tempes</w> <w n="27.6">découvertes</w>,</l>
					<l n="28" num="6.3"><w n="28.1">Et</w> <w n="28.2">qui</w>, — <w n="28.3">là</w>, — <w n="28.4">de</w> <w n="28.5">ce</w> <w n="28.6">coin</w>, <w n="28.7">voilé</w> <w n="28.8">d</w>’<w n="28.9">ombres</w> <w n="28.10">discrètes</w>,</l>
					<l n="29" num="6.4"><w n="29.1">Vous</w> <w n="29.2">allonge</w>, <w n="29.3">en</w> <w n="29.4">trois</w> <w n="29.5">quarts</w>, <w n="29.6">les</w> <w n="29.7">paupières</w> <w n="29.8">ouvertes</w>,</l>
					<l n="30" num="6.5"><space quantity="12" unit="char"></space><w n="30.1">De</w> <w n="30.2">hautains</w> <w n="30.3">regards</w> <w n="30.4">incertains</w>.</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><w n="31.1">Ce</w> <w n="31.2">fut</w> <w n="31.3">pour</w> <w n="31.4">moi</w> <w n="31.5">toujours</w> <w n="31.6">une</w> <w n="31.7">étrange</w> <w n="31.8">figure</w></l>
					<l n="32" num="7.2"><w n="32.1">Que</w> <w n="32.2">ce</w> <w n="32.3">buste</w> <w n="32.4">de</w> <w n="32.5">femme</w>, — <w n="32.6">et</w> <w n="32.7">dès</w> <w n="32.8">mes</w> <w n="32.9">premiers</w> <w n="32.10">ans</w>,</l>
					<l n="33" num="7.3"><w n="33.1">Je</w> <w n="33.2">la</w> <w n="33.3">cherchais</w> <w n="33.4">des</w> <w n="33.5">yeux</w> <w n="33.6">dans</w> <w n="33.7">sa</w> <w n="33.8">pénombre</w> <w n="33.9">obscure</w>…</l>
					<l n="34" num="7.4"><w n="34.1">Puis</w>, <w n="34.2">lorsque</w> <w n="34.3">j</w>’<w n="34.4">en</w> <w n="34.5">fus</w> <w n="34.6">loin</w> <w n="34.7">par</w> <w n="34.8">l</w>’<w n="34.9">espace</w> <w n="34.10">et</w> <w n="34.11">le</w> <w n="34.12">temps</w>,</l>
					<l n="35" num="7.5"><space quantity="12" unit="char"></space><w n="35.1">Dans</w> <w n="35.2">mon</w> <w n="35.3">cœur</w>, — <w n="35.4">cette</w> <w n="35.5">autre</w> <w n="35.6">encoignure</w> !</l>
				</lg>
				<lg n="8">
					<l n="36" num="8.1"><w n="36.1">Car</w> <w n="36.2">ce</w> <w n="36.3">buste</w>, <w n="36.4">ce</w> <w n="36.5">fut</w>… <w n="36.6">oui</w> ! <w n="36.7">mon</w> <w n="36.8">premier</w> <w n="36.9">amour</w>,</l>
					<l n="37" num="8.2"><w n="37.1">Le</w> <w n="37.2">premier</w> <w n="37.3">amour</w> <w n="37.4">fou</w> <w n="37.5">de</w> <w n="37.6">mon</w> <w n="37.7">cœur</w> <w n="37.8">solitaire</w> !</l>
					<l n="38" num="8.3"><w n="38.1">La</w> <w n="38.2">femme</w> <w n="38.3">qu</w>’<w n="38.4">il</w> <w n="38.5">était</w> <w n="38.6">est</w> <w n="38.7">restée</w> <w n="38.8">un</w> <w n="38.9">mystère</w>…</l>
					<l n="39" num="8.4"><w n="39.1">C</w>’<w n="39.2">était</w> — <w n="39.3">m</w>’<w n="39.4">avait</w>-<w n="39.5">on</w> <w n="39.6">dit</w> — <w n="39.7">la</w> <w n="39.8">tante</w> <w n="39.9">de</w> <w n="39.10">ma</w> <w n="39.11">mère</w>,</l>
					<l n="40" num="8.5"><space quantity="12" unit="char"></space><w n="40.1">Une</w> <w n="40.2">dame</w> <w n="40.3">de</w> <w n="40.4">Chavincour</w></l>
				</lg>
				<lg n="9">
					<l n="41" num="9.1"><w n="41.1">Morte</w> <w n="41.2">vers</w> <w n="41.3">les</w> <w n="41.4">trente</w> <w n="41.5">ans</w>… <w n="41.6">Rien</w> <w n="41.7">de</w> <w n="41.8">plus</w>. <w n="41.9">Sa</w> <w n="41.10">toilette</w></l>
					<l n="42" num="9.2"><w n="42.1">En</w> <w n="42.2">ce</w> <w n="42.3">buste</w> <w n="42.4">est</w> <w n="42.5">très</w> <w n="42.6">simple</w> <w n="42.7">et</w> <w n="42.8">celle</w> <w n="42.9">de</w> <w n="42.10">son</w> <w n="42.11">temps</w>.</l>
					<l n="43" num="9.3"><w n="43.1">Ses</w> <w n="43.2">cheveux</w> <w n="43.3">étagés</w> <w n="43.4">n</w>’<w n="43.5">ont</w> <w n="43.6">pas</w> <w n="43.7">même</w> <w n="43.8">une</w> <w n="43.9">aigrette</w>.</l>
					<l n="44" num="9.4"><w n="44.1">On</w> <w n="44.2">dirait</w>, <w n="44.3">mais</w> <w n="44.4">alors</w> <w n="44.5">sans</w> <w n="44.6">nœuds</w> <w n="44.7">et</w> <w n="44.8">sans</w> <w n="44.9">rubans</w>,</l>
					<l n="45" num="9.5"><space quantity="12" unit="char"></space><w n="45.1">La</w> <w n="45.2">Reine</w> <w n="45.3">Marie</w>-<w n="45.4">Antoinette</w>.</l>
				</lg>
				<lg n="10">
					<l n="46" num="10.1"><w n="46.1">C</w>’<w n="46.2">est</w> <w n="46.3">bien</w> <w n="46.4">là</w> <w n="46.5">ce</w> <w n="46.6">collier</w>, — <w n="46.7">ce</w> <w n="46.8">collier</w> <w n="46.9">de</w> <w n="46.10">sequins</w></l>
					<l n="47" num="10.2"><w n="47.1">Que</w> <w n="47.2">les</w> <w n="47.3">femmes</w> <w n="47.4">serraient</w> <w n="47.5">comme</w> <w n="47.6">on</w> <w n="47.7">fait</w> <w n="47.8">sa</w> <w n="47.9">ceinture</w>,</l>
					<l n="48" num="10.3"><w n="48.1">La</w> <w n="48.2">cravate</w> <w n="48.3">du</w> <w n="48.4">cou</w>, <w n="48.5">bien</w> <w n="48.6">plus</w> <w n="48.7">que</w> <w n="48.8">sa</w> <w n="48.9">parure</w>…</l>
					<l n="49" num="10.4"><w n="49.1">Et</w> <w n="49.2">ce</w> <w n="49.3">corsage</w> <w n="49.4">aussi</w>, <w n="49.5">dont</w> <w n="49.6">la</w> <w n="49.7">brusque</w> <w n="49.8">échancrure</w></l>
					<l n="50" num="10.5"><space quantity="12" unit="char"></space><w n="50.1">Descend</w> <w n="50.2">jusqu</w>’<w n="50.3">entre</w> <w n="50.4">les</w> <w n="50.5">deux</w> <w n="50.6">seins</w>.</l>
				</lg>
				<lg n="11">
					<l n="51" num="11.1"><w n="51.1">O</w> <w n="51.2">buste</w>, <w n="51.3">idolâtré</w> <w n="51.4">de</w> <w n="51.5">mon</w> <w n="51.6">enfance</w> <w n="51.7">folle</w>,</l>
					<l n="52" num="11.2"><w n="52.1">Buste</w> <w n="52.2">mystérieux</w> <w n="52.3">que</w> <w n="52.4">je</w> <w n="52.5">revois</w> <w n="52.6">ce</w> <w n="52.7">soir</w> !…</l>
					<l n="53" num="11.3"><w n="53.1">Quand</w> <w n="53.2">rien</w>, <w n="53.3">rien</w> <w n="53.4">dans</w> <w n="53.5">mon</w> <w n="53.6">cœur</w> <w n="53.7">n</w>’<w n="53.8">a</w> <w n="53.9">plus</w> <w n="53.10">une</w> <w n="53.11">auréole</w>,</l>
					<l n="54" num="11.4"><w n="54.1">Tu</w> <w n="54.2">rayonnes</w> <w n="54.3">toujours</w>, <w n="54.4">jaune</w>, <w n="54.5">dans</w> <w n="54.6">ton</w> <w n="54.7">coin</w> <w n="54.8">noir</w>,</l>
					<l n="55" num="11.5"><space quantity="12" unit="char"></space><w n="55.1">O</w> <w n="55.2">buste</w> ! <w n="55.3">ma</w> <w n="55.4">première</w> <w n="55.5">idole</w> !</l>
				</lg>
				<lg n="12">
					<l n="56" num="12.1"><w n="56.1">Tous</w> <w n="56.2">les</w> <w n="56.3">bustes</w> <w n="56.4">vivants</w> <w n="56.5">que</w> <w n="56.6">j</w>’<w n="56.7">ai</w> <w n="56.8">pris</w> <w n="56.9">sur</w> <w n="56.10">mon</w> <w n="56.11">cœur</w></l>
					<l n="57" num="12.2"><w n="57.1">S</w>’<w n="57.2">y</w> <w n="57.3">sont</w> <w n="57.4">brisés</w>, <w n="57.5">usés</w>, <w n="57.6">déformés</w> <w n="57.7">par</w> <w n="57.8">la</w> <w n="57.9">vie</w>…</l>
					<l n="58" num="12.3"><w n="58.1">Leur</w> <w n="58.2">argile</w> <w n="58.3">de</w> <w n="58.4">chair</w> <w n="58.5">s</w>’<w n="58.6">est</w> <w n="58.7">plus</w> <w n="58.8">vite</w> <w n="58.9">amollie</w></l>
					<l n="59" num="12.4"><w n="59.1">Que</w> <w n="59.2">ton</w> <w n="59.3">argile</w>, <w n="59.4">ô</w> <w n="59.5">buste</w> ! <w n="59.6">immobile</w> <w n="59.7">effigie</w></l>
					<l n="60" num="12.5"><space quantity="12" unit="char"></space><w n="60.1">Et</w> <w n="60.2">du</w> <w n="60.3">temps</w> <w n="60.4">inerte</w> <w n="60.5">vainqueur</w> !</l>
				</lg>
				<lg n="13">
					<l n="61" num="13.1"><w n="61.1">Toi</w> <w n="61.2">seul</w> <w n="61.3">n</w>’<w n="61.4">as</w> <w n="61.5">pas</w> <w n="61.6">bougé</w>, <w n="61.7">buste</w> ! <w n="61.8">forme</w> <w n="61.9">et</w> <w n="61.10">matière</w>,</l>
					<l n="62" num="13.2"><w n="62.1">La</w> <w n="62.2">vie</w>, <w n="62.3">en</w> <w n="62.4">s</w>’<w n="62.5">écoulant</w>, <w n="62.6">n</w>’<w n="62.7">a</w> <w n="62.8">pu</w> <w n="62.9">rien</w> <w n="62.10">t</w>’<w n="62.11">enlever</w>…</l>
					<l n="63" num="13.3"><w n="63.1">Mon</w> <w n="63.2">rêve</w>, <w n="63.3">auprès</w> <w n="63.4">de</w> <w n="63.5">toi</w>, <w n="63.6">je</w> <w n="63.7">le</w> <w n="63.8">viens</w> <w n="63.9">achever</w>…</l>
					<l n="64" num="13.4"><w n="64.1">Je</w> <w n="64.2">songerai</w> <w n="64.3">de</w> <w n="64.4">toi</w> <w n="64.5">jusques</w> <w n="64.6">au</w> <w n="64.7">cimetière</w>,</l>
					<l n="65" num="13.5"><w n="65.1">Mais</w>, <w n="65.2">ô</w> <w n="65.3">buste</w> ! <w n="65.4">après</w> <w n="65.5">moi</w>, <w n="65.6">quel</w> <w n="65.7">cœur</w> <w n="65.8">fera</w> <w n="65.9">rêver</w></l>
					<l n="66" num="13.6"><space quantity="12" unit="char"></space><w n="66.1">Ton</w> <w n="66.2">argile</w>, — <w n="66.3">sur</w> <w n="66.4">ma</w> <w n="66.5">poussière</w> ?…</l>
				</lg>
			</div></body></text></TEI>