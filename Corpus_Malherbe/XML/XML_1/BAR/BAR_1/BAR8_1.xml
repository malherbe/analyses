<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR8">
				<head type="main">La Maîtresse Rousse</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">pris</w> <w n="1.3">pour</w> <w n="1.4">maître</w>, <w n="1.5">un</w> <w n="1.6">jour</w>, <w n="1.7">une</w> <w n="1.8">rude</w> <w n="1.9">Maîtresse</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Plus</w> <w n="2.2">fauve</w> <w n="2.3">qu</w>’<w n="2.4">un</w> <w n="2.5">jaguar</w>, <w n="2.6">plus</w> <w n="2.7">rousse</w> <w n="2.8">qu</w>’<w n="2.9">un</w> <w n="2.10">lion</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">l</w>’<w n="3.3">aimais</w> <w n="3.4">ardemment</w>, — <w n="3.5">âprement</w>, — <w n="3.6">sans</w> <w n="3.7">tendresse</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">possession</w> <w n="4.3">plus</w> <w n="4.4">qu</w>’<w n="4.5">adoration</w> !</l>
					<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2">était</w> <w n="5.3">ma</w> <w n="5.4">rage</w>, <w n="5.5">à</w> <w n="5.6">moi</w> ! <w n="5.7">la</w> <w n="5.8">dernière</w> <w n="5.9">folie</w></l>
					<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">saisit</w>, — <w n="6.3">quand</w>, <w n="6.4">touché</w> <w n="6.5">par</w> <w n="6.6">l</w>’<w n="6.7">âge</w> <w n="6.8">et</w> <w n="6.9">le</w> <w n="6.10">malheur</w>,</l>
					<l n="7" num="1.7"><w n="7.1">On</w> <w n="7.2">sent</w> <w n="7.3">au</w> <w n="7.4">fond</w> <w n="7.5">de</w> <w n="7.6">soi</w> <w n="7.7">la</w> <w n="7.8">jeunesse</w> <w n="7.9">finie</w>…</l>
					<l n="8" num="1.8"><w n="8.1">Car</w> <w n="8.2">le</w> <w n="8.3">soleil</w> <w n="8.4">des</w> <w n="8.5">jours</w> <w n="8.6">monte</w> <w n="8.7">encor</w> <w n="8.8">dans</w> <w n="8.9">la</w> <w n="8.10">vie</w>,</l>
					<l n="9" num="1.9"><space quantity="12" unit="char"></space><w n="9.1">Qu</w>’<w n="9.2">il</w> <w n="9.3">s</w>’<w n="9.4">en</w> <w n="9.5">va</w> <w n="9.6">baissant</w> <w n="9.7">dans</w> <w n="9.8">le</w> <w n="9.9">cœur</w> !</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1"><w n="10.1">Je</w> <w n="10.2">l</w>’<w n="10.3">aimais</w> <w n="10.4">et</w> <w n="10.5">jamais</w> <w n="10.6">je</w> <w n="10.7">n</w>’<w n="10.8">avais</w> <w n="10.9">assez</w> <w n="10.10">d</w>’<w n="10.11">elle</w> !</l>
					<l n="11" num="2.2"><w n="11.1">Je</w> <w n="11.2">lui</w> <w n="11.3">disais</w> : « <w n="11.4">Démon</w> <w n="11.5">des</w> <w n="11.6">dernières</w> <w n="11.7">amours</w>,</l>
					<l n="12" num="2.3"><w n="12.1">Salamandre</w> <w n="12.2">d</w>’<w n="12.3">enfer</w>, <w n="12.4">à</w> <w n="12.5">l</w>’<w n="12.6">ivresse</w> <w n="12.7">mortelle</w>,</l>
					<l n="13" num="2.4"><w n="13.1">Quand</w> <w n="13.2">les</w> <w n="13.3">cœurs</w> <w n="13.4">sont</w> <w n="13.5">si</w> <w n="13.6">froids</w>, <w n="13.7">embrase</w>-<w n="13.8">moi</w> <w n="13.9">toujours</w> !</l>
					<l n="14" num="2.5"><w n="14.1">Verse</w>-<w n="14.2">moi</w> <w n="14.3">dans</w> <w n="14.4">tes</w> <w n="14.5">feux</w> <w n="14.6">les</w> <w n="14.7">feux</w> <w n="14.8">que</w> <w n="14.9">je</w> <w n="14.10">regrette</w>,</l>
					<l n="15" num="2.6"><w n="15.1">Ces</w> <w n="15.2">beaux</w> <w n="15.3">feux</w> <w n="15.4">qu</w>’<w n="15.5">autrefois</w> <w n="15.6">j</w>’<w n="15.7">allumais</w> <w n="15.8">d</w>’<w n="15.9">un</w> <w n="15.10">regard</w> !</l>
					<l n="16" num="2.7"><w n="16.1">Rajeunis</w> <w n="16.2">le</w> <w n="16.3">rêveur</w>, <w n="16.4">réchauffe</w> <w n="16.5">le</w> <w n="16.6">poète</w>,</l>
					<l n="17" num="2.8"><w n="17.1">Et</w>, <w n="17.2">puisqu</w>’<w n="17.3">il</w> <w n="17.4">faut</w> <w n="17.5">mourir</w>, <w n="17.6">que</w> <w n="17.7">je</w> <w n="17.8">meure</w>, <w n="17.9">ô</w> <w n="17.10">Fillette</w> !</l>
					<l n="18" num="2.9"><space quantity="12" unit="char"></space><w n="18.1">Sous</w> <w n="18.2">tes</w> <w n="18.3">morsures</w> <w n="18.4">de</w> <w n="18.5">jaguar</w> ! »</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1"><w n="19.1">Alors</w> <w n="19.2">je</w> <w n="19.3">la</w> <w n="19.4">prenais</w>, <w n="19.5">dans</w> <w n="19.6">son</w> <w n="19.7">corset</w> <w n="19.8">de</w> <w n="19.9">verre</w>,</l>
					<l n="20" num="3.2"><w n="20.1">Et</w> <w n="20.2">sur</w> <w n="20.3">ma</w> <w n="20.4">lèvre</w> <w n="20.5">en</w> <w n="20.6">feu</w>, <w n="20.7">qu</w>’<w n="20.8">elle</w> <w n="20.9">enflammait</w> <w n="20.10">encor</w>,</l>
					<l n="21" num="3.3"><w n="21.1">J</w>’<w n="21.2">aimais</w> <w n="21.3">à</w> <w n="21.4">la</w> <w n="21.5">pencher</w>, <w n="21.6">coupe</w> <w n="21.7">ardente</w> <w n="21.8">et</w> <w n="21.9">légère</w>,</l>
					<l n="22" num="3.4"><w n="22.1">Cette</w> <w n="22.2">rousse</w> <w n="22.3">beauté</w>, <w n="22.4">ce</w> <w n="22.5">poison</w> <w n="22.6">dans</w> <w n="22.7">de</w> <w n="22.8">l</w>’<w n="22.9">or</w> !</l>
					<l n="23" num="3.5"><w n="23.1">Et</w> <w n="23.2">c</w>’<w n="23.3">étaient</w> <w n="23.4">des</w> <w n="23.5">baisers</w> !… <w n="23.6">Jamais</w>, <w n="23.7">jamais</w> <w n="23.8">vampire</w></l>
					<l n="24" num="3.6"><w n="24.1">Ne</w> <w n="24.2">suça</w> <w n="24.3">d</w>’<w n="24.4">une</w> <w n="24.5">enfant</w> <w n="24.6">le</w> <w n="24.7">cou</w> <w n="24.8">charmant</w> <w n="24.9">et</w> <w n="24.10">frais</w></l>
					<l n="25" num="3.7"><w n="25.1">Comme</w> <w n="25.2">moi</w> <w n="25.3">je</w> <w n="25.4">suçais</w>, <w n="25.5">ô</w> <w n="25.6">ma</w> <w n="25.7">rousse</w> <w n="25.8">hétaïre</w>,</l>
					<l n="26" num="3.8"><w n="26.1">La</w> <w n="26.2">lèvre</w> <w n="26.3">de</w> <w n="26.4">cristal</w> <w n="26.5">où</w> <w n="26.6">buvait</w> <w n="26.7">mon</w> <w n="26.8">délire</w></l>
					<l n="27" num="3.9"><space quantity="12" unit="char"></space><w n="27.1">Et</w> <w n="27.2">sur</w> <w n="27.3">laquelle</w> <w n="27.4">tu</w> <w n="27.5">brûlais</w> !</l>
				</lg>
				<lg n="4">
					<l n="28" num="4.1"><w n="28.1">Et</w> <w n="28.2">je</w> <w n="28.3">sentais</w> <w n="28.4">alors</w> <w n="28.5">ta</w> <w n="28.6">foudroyante</w> <w n="28.7">haleine</w></l>
					<l n="29" num="4.2"><w n="29.1">Qui</w> <w n="29.2">passait</w> <w n="29.3">dans</w> <w n="29.4">la</w> <w n="29.5">mienne</w> <w n="29.6">et</w>, <w n="29.7">tombant</w> <w n="29.8">dans</w> <w n="29.9">mon</w> <w n="29.10">cœur</w>,</l>
					<l n="30" num="4.3"><w n="30.1">Y</w> <w n="30.2">redoublait</w> <w n="30.3">la</w> <w n="30.4">vie</w>, <w n="30.5">en</w> <w n="30.6">effaçait</w> <w n="30.7">la</w> <w n="30.8">peine</w>,</l>
					<l n="31" num="4.4"><w n="31.1">Et</w> <w n="31.2">pour</w> <w n="31.3">quelques</w> <w n="31.4">instants</w> <w n="31.5">en</w> <w n="31.6">ravivait</w> <w n="31.7">l</w>’<w n="31.8">ardeur</w> !</l>
					<l n="32" num="4.5"><w n="32.1">Alors</w>, <w n="32.2">Fille</w> <w n="32.3">de</w> <w n="32.4">Feu</w>, <w n="32.5">maîtresse</w> <w n="32.6">sans</w> <w n="32.7">rivale</w>,</l>
					<l n="33" num="4.6"><w n="33.1">J</w>’<w n="33.2">aimais</w> <w n="33.3">à</w> <w n="33.4">me</w> <w n="33.5">sentir</w> <w n="33.6">incendié</w> <w n="33.7">par</w> <w n="33.8">toi</w></l>
					<l n="34" num="4.7"><w n="34.1">Et</w> <w n="34.2">voulais</w> <w n="34.3">m</w>’<w n="34.4">endormir</w>, <w n="34.5">l</w>’<w n="34.6">air</w> <w n="34.7">joyeux</w>, <w n="34.8">le</w> <w n="34.9">front</w> <w n="34.10">pâle</w>,</l>
					<l n="35" num="4.8"><w n="35.1">Sur</w> <w n="35.2">un</w> <w n="35.3">bûcher</w> <w n="35.4">brillant</w>, <w n="35.5">comme</w> <w n="35.6">Sardanapale</w>,</l>
					<l n="36" num="4.9"><space quantity="12" unit="char"></space><w n="36.1">Et</w> <w n="36.2">le</w> <w n="36.3">bûcher</w> <w n="36.4">était</w> <w n="36.5">en</w> <w n="36.6">moi</w> !</l>
				</lg>
				<lg n="5">
					<l n="37" num="5.1">« <w n="37.1">Ah</w> ! <w n="37.2">du</w> <w n="37.3">moins</w> <w n="37.4">celle</w>-<w n="37.5">là</w> <w n="37.6">sait</w> <w n="37.7">nous</w> <w n="37.8">rester</w> <w n="37.9">fidèle</w>, —</l>
					<l n="38" num="5.2"><w n="38.1">Me</w> <w n="38.2">disais</w>-<w n="38.3">je</w>, — <w n="38.4">et</w> <w n="38.5">la</w> <w n="38.6">main</w> <w n="38.7">la</w> <w n="38.8">retrouve</w> <w n="38.9">toujours</w>,</l>
					<l n="39" num="5.3"><w n="39.1">Toujours</w> <w n="39.2">prête</w> <w n="39.3">à</w> <w n="39.4">qui</w> <w n="39.5">l</w>’<w n="39.6">aime</w> <w n="39.7">et</w> <w n="39.8">vit</w> <w n="39.9">altéré</w> <w n="39.10">d</w>’<w n="39.11">elle</w>,</l>
					<l n="40" num="5.4"><w n="40.1">Et</w> <w n="40.2">veut</w> <w n="40.3">dans</w> <w n="40.4">son</w> <w n="40.5">amour</w> <w n="40.6">perdre</w> <w n="40.7">tous</w> <w n="40.8">ses</w> <w n="40.9">amours</w> ! »</l>
					<l n="41" num="5.5"><w n="41.1">Un</w> <w n="41.2">jour</w> <w n="41.3">elles</w> <w n="41.4">s</w>’<w n="41.5">en</w> <w n="41.6">vont</w>, <w n="41.7">nos</w> <w n="41.8">plus</w> <w n="41.9">chères</w> <w n="41.10">maîtresses</w> ;</l>
					<l n="42" num="5.6"><w n="42.1">Par</w> <w n="42.2">elles</w>, <w n="42.3">de</w> <w n="42.4">l</w>’<w n="42.5">Oubli</w> <w n="42.6">nous</w> <w n="42.7">buvons</w> <w n="42.8">le</w> <w n="42.9">poison</w>,</l>
					<l n="43" num="5.7"><w n="43.1">Tandis</w> <w n="43.2">que</w> <w n="43.3">cette</w> <w n="43.4">Rousse</w>, <w n="43.5">indomptable</w> <w n="43.6">aux</w> <w n="43.7">caresses</w>,</l>
					<l n="44" num="5.8"><w n="44.1">Peut</w> <w n="44.2">nous</w> <w n="44.3">tuer</w> <w n="44.4">aussi</w>, — <w n="44.5">mais</w> <w n="44.6">à</w> <w n="44.7">force</w> <w n="44.8">d</w>’<w n="44.9">ivresses</w>,</l>
					<l n="45" num="5.9"><space quantity="12" unit="char"></space><w n="45.1">Et</w> <w n="45.2">non</w> <w n="45.3">pas</w> <w n="45.4">par</w> <w n="45.5">la</w> <w n="45.6">trahison</w> !</l>
				</lg>
				<lg n="6">
					<l n="46" num="6.1"><w n="46.1">Et</w> <w n="46.2">je</w> <w n="46.3">la</w> <w n="46.4">préférais</w>, <w n="46.5">féroce</w>, <w n="46.6">mais</w> <w n="46.7">sincère</w>,</l>
					<l n="47" num="6.2"><w n="47.1">A</w> <w n="47.2">ces</w> <w n="47.3">douces</w> <w n="47.4">beautés</w>, <w n="47.5">au</w> <w n="47.6">sourire</w> <w n="47.7">trompeur</w>,</l>
					<l n="48" num="6.3"><w n="48.1">Payant</w> <w n="48.2">les</w> <w n="48.3">cœurs</w> <w n="48.4">loyaux</w> <w n="48.5">d</w>’<w n="48.6">un</w> <w n="48.7">amour</w> <w n="48.8">de</w> <w n="48.9">faussaire</w>…</l>
					<l n="49" num="6.4"><w n="49.1">Je</w> <w n="49.2">savais</w> <w n="49.3">sur</w> <w n="49.4">quel</w> <w n="49.5">cœur</w> <w n="49.6">je</w> <w n="49.7">dormais</w> <w n="49.8">sur</w> <w n="49.9">son</w> <w n="49.10">cœur</w> !</l>
					<l n="50" num="6.5"><w n="50.1">L</w>’<w n="50.2">or</w> <w n="50.3">qu</w>’<w n="50.4">elle</w> <w n="50.5">me</w> <w n="50.6">versait</w> <w n="50.7">et</w> <w n="50.8">qui</w> <w n="50.9">dorait</w> <w n="50.10">ma</w> <w n="50.11">vie</w>,</l>
					<l n="51" num="6.6"><w n="51.1">Soleillant</w> <w n="51.2">dans</w> <w n="51.3">ma</w> <w n="51.4">coupe</w>, <w n="51.5">était</w> <w n="51.6">un</w> <w n="51.7">vrai</w> <w n="51.8">trésor</w> !</l>
					<l n="52" num="6.7"><w n="52.1">Aussi</w> <w n="52.2">ce</w> <w n="52.3">n</w>’<w n="52.4">était</w> <w n="52.5">pas</w> <w n="52.6">pour</w> <w n="52.7">le</w> <w n="52.8">temps</w> <w n="52.9">d</w>’<w n="52.10">une</w> <w n="52.11">orgie</w>,</l>
					<l n="53" num="6.8"><w n="53.1">Mais</w> <w n="53.2">pour</w> <w n="53.3">l</w>’<w n="53.4">éternité</w>, <w n="53.5">que</w> <w n="53.6">je</w> <w n="53.7">l</w>’<w n="53.8">avais</w> <w n="53.9">choisie</w> :</l>
					<l n="54" num="6.9"><space quantity="12" unit="char"></space><w n="54.1">Ma</w> <w n="54.2">compagne</w> <w n="54.3">jusqu</w>’<w n="54.4">à</w> <w n="54.5">la</w> <w n="54.6">mort</w> !</l>
				</lg>
				<lg n="7">
					<l n="55" num="7.1"><w n="55.1">Et</w> <w n="55.2">toujours</w> <w n="55.3">agrafée</w> <w n="55.4">à</w> <w n="55.5">moi</w> <w n="55.6">comme</w> <w n="55.7">une</w> <w n="55.8">esclave</w>,</l>
					<l n="56" num="7.2"><w n="56.1">Car</w> <w n="56.2">le</w> <w n="56.3">tyran</w> <w n="56.4">se</w> <w n="56.5">rive</w> <w n="56.6">aux</w> <w n="56.7">fers</w> <w n="56.8">qu</w>’<w n="56.9">il</w> <w n="56.10">fait</w> <w n="56.11">porter</w>,</l>
					<l n="57" num="7.3"><w n="57.1">Je</w> <w n="57.2">l</w>’<w n="57.3">emportais</w> <w n="57.4">partout</w> <w n="57.5">dans</w> <w n="57.6">son</w> <w n="57.7">flacon</w> <w n="57.8">de</w> <w n="57.9">lave</w>,</l>
					<l n="58" num="7.4"><w n="58.1">Ma</w> <w n="58.2">topaze</w> <w n="58.3">de</w> <w n="58.4">feu</w>, <w n="58.5">toujours</w> <w n="58.6">près</w> <w n="58.7">d</w>’<w n="58.8">éclater</w> !</l>
					<l n="59" num="7.5"><w n="59.1">Je</w> <w n="59.2">ressentais</w> <w n="59.3">pour</w> <w n="59.4">elle</w> <w n="59.5">un</w> <w n="59.6">amour</w> <w n="59.7">de</w> <w n="59.8">corsaire</w>,</l>
					<l n="60" num="7.6"><w n="60.1">Un</w> <w n="60.2">amour</w> <w n="60.3">de</w> <w n="60.4">sauvage</w>, <w n="60.5">effréné</w>, <w n="60.6">fol</w>, <w n="60.7">ardent</w> !</l>
					<l n="61" num="7.7"><w n="61.1">Cet</w> <w n="61.2">amour</w> <w n="61.3">qu</w>’<w n="61.4">Hégésippe</w> <w n="61.5">avait</w>, <w n="61.6">dans</w> <w n="61.7">sa</w> <w n="61.8">misère</w>,</l>
					<l n="62" num="7.8"><w n="62.1">Qui</w> <w n="62.2">nous</w> <w n="62.3">tient</w> <w n="62.4">lieu</w> <w n="62.5">de</w> <w n="62.6">tout</w>, <w n="62.7">quand</w> <w n="62.8">la</w> <w n="62.9">vie</w> <w n="62.10">est</w> <w n="62.11">amère</w>,</l>
					<l n="63" num="7.9"><space quantity="12" unit="char"></space><w n="63.1">Et</w> <w n="63.2">qui</w> <w n="63.3">fit</w> <w n="63.4">mourir</w> <w n="63.5">Sheridan</w> !</l>
				</lg>
				<lg n="8">
					<l n="64" num="8.1"><w n="64.1">Et</w> <w n="64.2">c</w>’<w n="64.3">était</w> <w n="64.4">un</w> <w n="64.5">amour</w> <w n="64.6">toujours</w> <w n="64.7">plus</w> <w n="64.8">implacable</w>,</l>
					<l n="65" num="8.2"><w n="65.1">Toujours</w> <w n="65.2">plus</w> <w n="65.3">dévorant</w>, <w n="65.4">toujours</w> <w n="65.5">plus</w> <w n="65.6">insensé</w> !</l>
					<l n="66" num="8.3"><w n="66.1">C</w>’<w n="66.2">était</w> <w n="66.3">comme</w> <w n="66.4">la</w> <w n="66.5">soif</w>, <w n="66.6">la</w> <w n="66.7">soif</w> <w n="66.8">inexorable</w></l>
					<l n="67" num="8.4"><w n="67.1">Qu</w>’<w n="67.2">allumait</w> <w n="67.3">autrefois</w> <w n="67.4">le</w> <w n="67.5">philtre</w> <w n="67.6">de</w> <w n="67.7">Circé</w>.</l>
					<l n="68" num="8.5"><w n="68.1">Je</w> <w n="68.2">te</w> <w n="68.3">reconnaissais</w>, <w n="68.4">voluptueux</w> <w n="68.5">supplice</w> !</l>
					<l n="69" num="8.6"><w n="69.1">Quand</w> <w n="69.2">l</w>’<w n="69.3">homme</w> <w n="69.4">cherche</w>, <w n="69.5">hélas</w> ! <w n="69.6">dans</w> <w n="69.7">ses</w> <w n="69.8">maux</w> <w n="69.9">oubliés</w>,</l>
					<l n="70" num="8.7"><w n="70.1">De</w> <w n="70.2">l</w>’<w n="70.3">abrutissement</w> <w n="70.4">le</w> <w n="70.5">monstrueux</w> <w n="70.6">délice</w>…</l>
					<l n="71" num="8.8"><w n="71.1">Et</w> <w n="71.2">n</w>’<w n="71.3">est</w> — <w n="71.4">Circé</w> ! — <w n="71.5">jamais</w> <w n="71.6">assez</w>, <w n="71.7">à</w> <w n="71.8">son</w> <w n="71.9">caprice</w>,</l>
					<l n="72" num="8.9"><space quantity="12" unit="char"></space><w n="72.1">La</w> <w n="72.2">Bête</w> <w n="72.3">qui</w> <w n="72.4">lèche</w> <w n="72.5">tes</w> <w n="72.6">pieds</w> !</l>
				</lg>
				<lg n="9">
					<l n="73" num="9.1"><w n="73.1">Pauvre</w> <w n="73.2">amour</w>, — <w n="73.3">le</w> <w n="73.4">dernier</w>, — <w n="73.5">que</w> <w n="73.6">les</w> <w n="73.7">heureux</w> <w n="73.8">du</w> <w n="73.9">monde</w>,</l>
					<l n="74" num="9.2"><w n="74.1">Dans</w> <w n="74.2">leur</w> <w n="74.3">dégoût</w> <w n="74.4">hautain</w>, <w n="74.5">s</w>’<w n="74.6">amusent</w> <w n="74.7">à</w> <w n="74.8">flétrir</w>,</l>
					<l n="75" num="9.3"><w n="75.1">Mais</w> <w n="75.2">que</w> <w n="75.3">doit</w> <w n="75.4">excuser</w> <w n="75.5">toute</w> <w n="75.6">âme</w> <w n="75.7">un</w> <w n="75.8">peu</w> <w n="75.9">profonde</w></l>
					<l n="76" num="9.4"><w n="76.1">Et</w> <w n="76.2">qu</w>’<w n="76.3">un</w> <w n="76.4">Dieu</w> <w n="76.5">de</w> <w n="76.6">bonté</w> <w n="76.7">ne</w> <w n="76.8">voudra</w> <w n="76.9">point</w> <w n="76.10">punir</w> !</l>
					<l n="77" num="9.5"><w n="77.1">Pour</w> <w n="77.2">bien</w> <w n="77.3">apprécier</w> <w n="77.4">sa</w> <w n="77.5">douceur</w> <w n="77.6">mensongère</w>,</l>
					<l n="78" num="9.6"><w n="78.1">Il</w> <w n="78.2">faudrait</w>, <w n="78.3">quand</w> <w n="78.4">tout</w> <w n="78.5">brille</w> <w n="78.6">au</w> <w n="78.7">plafond</w> <w n="78.8">du</w> <w n="78.9">banquet</w>,</l>
					<l n="79" num="9.7"><w n="79.1">Avoir</w> <w n="79.2">caché</w> <w n="79.3">ses</w> <w n="79.4">yeux</w> <w n="79.5">dans</w> <w n="79.6">l</w>’<w n="79.7">ombre</w> <w n="79.8">de</w> <w n="79.9">son</w> <w n="79.10">verre</w></l>
					<l n="80" num="9.8"><w n="80.1">Et</w> <w n="80.2">pleuré</w> <w n="80.3">dans</w> <w n="80.4">cette</w> <w n="80.5">ombre</w>, — <w n="80.6">et</w> <w n="80.7">bu</w> <w n="80.8">la</w> <w n="80.9">larme</w> <w n="80.10">amère</w></l>
					<l n="81" num="9.9"><space quantity="12" unit="char"></space><w n="81.1">Qui</w> <w n="81.2">tombait</w> <w n="81.3">et</w> <w n="81.4">qui</w> <w n="81.5">s</w>’<w n="81.6">y</w> <w n="81.7">fondait</w> !</l>
				</lg>
				<lg n="10">
					<l n="82" num="10.1"><w n="82.1">Un</w> <w n="82.2">soir</w> <w n="82.3">je</w> <w n="82.4">la</w> <w n="82.5">buvais</w>, <w n="82.6">cette</w> <w n="82.7">larme</w>, <w n="82.8">en</w> <w n="82.9">silence</w>…</l>
					<l n="83" num="10.2"><w n="83.1">Et</w>, <w n="83.2">replongeant</w> <w n="83.3">ma</w> <w n="83.4">lèvre</w> <w n="83.5">entre</w> <w n="83.6">tes</w> <w n="83.7">lèvres</w> <w n="83.8">d</w>’<w n="83.9">or</w>,</l>
					<l n="84" num="10.3"><w n="84.1">Je</w> <w n="84.2">venais</w> <w n="84.3">de</w> <w n="84.4">reprendre</w>, <w n="84.5">ô</w> <w n="84.6">ma</w> <w n="84.7">sombre</w> <w n="84.8">Démence</w> !</l>
					<l n="85" num="10.4"><w n="85.1">L</w>’<w n="85.2">ironie</w>, <w n="85.3">et</w> <w n="85.4">l</w>’<w n="85.5">ivresse</w>, <w n="85.6">et</w> <w n="85.7">du</w> <w n="85.8">courage</w> <w n="85.9">encor</w> !</l>
					<l n="86" num="10.5"><w n="86.1">L</w>’<w n="86.2">Esprit</w> — <w n="86.3">l</w>’<w n="86.4">Aigle</w> <w n="86.5">vengeur</w> <w n="86.6">qui</w> <w n="86.7">plane</w> <w n="86.8">sur</w> <w n="86.9">la</w> <w n="86.10">vie</w> —</l>
					<l n="87" num="10.6"><w n="87.1">Revenait</w> <w n="87.2">à</w> <w n="87.3">ma</w> <w n="87.4">lèvre</w>, <w n="87.5">à</w> <w n="87.6">son</w> <w n="87.7">sanglant</w> <w n="87.8">perchoir</w>…</l>
					<l n="88" num="10.7"><w n="88.1">J</w>’<w n="88.2">allais</w> <w n="88.3">recommencer</w> <w n="88.4">mes</w> <w n="88.5">accès</w> <w n="88.6">de</w> <w n="88.7">folie</w></l>
					<l n="89" num="10.8"><w n="89.1">Et</w> <w n="89.2">rire</w> <w n="89.3">de</w> <w n="89.4">nouveau</w> <w n="89.5">du</w> <w n="89.6">rire</w> <w n="89.7">qui</w> <w n="89.8">défie</w>…</l>
					<l n="90" num="10.9"><space quantity="12" unit="char"></space><w n="90.1">Quand</w> <w n="90.2">une</w> <w n="90.3">femme</w>, <w n="90.4">en</w> <w n="90.5">corset</w> <w n="90.6">noir</w>,</l>
				</lg>
				<lg n="11">
					<l n="91" num="11.1"><w n="91.1">Une</w> <w n="91.2">femme</w>… <w n="91.3">Je</w> <w n="91.4">crus</w> <w n="91.5">que</w> <w n="91.6">c</w>’<w n="91.7">était</w> <w n="91.8">une</w> <w n="91.9">femme</w>,</l>
					<l n="92" num="11.2"><w n="92.1">Mais</w> <w n="92.2">depuis</w>… <w n="92.3">Ah</w> ! <w n="92.4">j</w>’<w n="92.5">ai</w> <w n="92.6">vu</w> <w n="92.7">combien</w> <w n="92.8">je</w> <w n="92.9">me</w> <w n="92.10">trompais</w>,</l>
					<l n="93" num="11.3"><w n="93.1">Et</w> <w n="93.2">que</w> <w n="93.3">c</w>’<w n="93.4">était</w> <w n="93.5">un</w> <w n="93.6">Ange</w>, <w n="93.7">et</w> <w n="93.8">que</w> <w n="93.9">c</w>’<w n="93.10">était</w> <w n="93.11">une</w> <w n="93.12">Âme</w>,</l>
					<l n="94" num="11.4"><w n="94.1">De</w> <w n="94.2">rafraîchissement</w>, <w n="94.3">de</w> <w n="94.4">lumière</w> <w n="94.5">et</w> <w n="94.6">de</w> <w n="94.7">paix</w> !</l>
					<l n="95" num="11.5"><w n="95.1">Au</w> <w n="95.2">milieu</w> <w n="95.3">de</w> <w n="95.4">nous</w> <w n="95.5">tous</w>, <w n="95.6">charmante</w> <w n="95.7">Solitaire</w>,</l>
					<l n="96" num="11.6"><w n="96.1">Elle</w> <w n="96.2">avait</w> <w n="96.3">les</w> <w n="96.4">yeux</w> <w n="96.5">pleins</w> <w n="96.6">de</w> <w n="96.7">toutes</w> <w n="96.8">les</w> <w n="96.9">pitiés</w>.</l>
					<l n="97" num="11.7"><w n="97.1">Elle</w> <w n="97.2">prit</w> <w n="97.3">ses</w> <w n="97.4">gants</w> <w n="97.5">blancs</w> <w n="97.6">et</w> <w n="97.7">les</w> <w n="97.8">mit</w> <w n="97.9">dans</w> <w n="97.10">mon</w> <w n="97.11">verre</w>,</l>
					<l n="98" num="11.8"><w n="98.1">Et</w> <w n="98.2">me</w> <w n="98.3">dit</w> <w n="98.4">en</w> <w n="98.5">riant</w>, <w n="98.6">de</w> <w n="98.7">sa</w> <w n="98.8">voix</w> <w n="98.9">douce</w> <w n="98.10">et</w> <w n="98.11">claire</w></l>
					<l n="99" num="11.9"><space quantity="12" unit="char"></space>« <w n="99.1">Je</w> <w n="99.2">ne</w> <w n="99.3">veux</w> <w n="99.4">plus</w> <w n="99.5">que</w> <w n="99.6">vous</w> <w n="99.7">buviez</w> ! »</l>
				</lg>
				<lg n="12">
					<l n="100" num="12.1"><w n="100.1">Et</w> <w n="100.2">ce</w> <w n="100.3">simple</w> <w n="100.4">mot</w>-<w n="100.5">là</w> <w n="100.6">décida</w> <w n="100.7">de</w> <w n="100.8">ma</w> <w n="100.9">vie</w>,</l>
					<l n="101" num="12.2"><w n="101.1">Et</w> <w n="101.2">fut</w> <w n="101.3">le</w> <w n="101.4">coup</w> <w n="101.5">de</w> <w n="101.6">Dieu</w> <w n="101.7">qui</w> <w n="101.8">changea</w> <w n="101.9">mon</w> <w n="101.10">destin</w>.</l>
					<l n="102" num="12.3"><w n="102.1">Et</w> <w n="102.2">quand</w> <w n="102.3">elle</w> <w n="102.4">le</w> <w n="102.5">dit</w>, <w n="102.6">sûre</w> <w n="102.7">d</w>’<w n="102.8">être</w> <w n="102.9">obéie</w>,</l>
					<l n="103" num="12.4"><w n="103.1">Sa</w> <w n="103.2">main</w> <w n="103.3">vint</w> <w n="103.4">chastement</w> <w n="103.5">s</w>’<w n="103.6">appuyer</w> <w n="103.7">sur</w> <w n="103.8">ma</w> <w n="103.9">main</w>.</l>
					<l n="104" num="12.5"><w n="104.1">Et</w>, <w n="104.2">depuis</w> <w n="104.3">ce</w> <w n="104.4">temps</w>-<w n="104.5">là</w>, <w n="104.6">j</w>’<w n="104.7">allai</w> <w n="104.8">chercher</w> <w n="104.9">l</w>’<w n="104.10">ivresse</w></l>
					<l n="105" num="12.6"><w n="105.1">Ailleurs</w>… <w n="105.2">que</w> <w n="105.3">dans</w> <w n="105.4">la</w> <w n="105.5">coupe</w> <w n="105.6">où</w> <w n="105.7">bouillait</w> <w n="105.8">ton</w> <w n="105.9">poison</w>,</l>
					<l n="106" num="12.7"><w n="106.1">Sorcière</w> <w n="106.2">abandonnée</w>, <w n="106.3">ô</w> <w n="106.4">ma</w> <w n="106.5">Rousse</w> <w n="106.6">Maîtresse</w> !</l>
					<l n="107" num="12.8"><w n="107.1">Bel</w> <w n="107.2">exemple</w> <w n="107.3">de</w> <w n="107.4">plus</w> <w n="107.5">que</w> <w n="107.6">Dieu</w> <w n="107.7">dans</w> <w n="107.8">sa</w> <w n="107.9">sagesse</w>,</l>
					<l n="108" num="12.9"><space quantity="12" unit="char"></space><w n="108.1">Mit</w> <w n="108.2">l</w>’<w n="108.3">Ange</w> <w n="108.4">au</w>-<w n="108.5">dessus</w> <w n="108.6">du</w> <w n="108.7">démon</w> !</l>
				</lg>
			</div></body></text></TEI>