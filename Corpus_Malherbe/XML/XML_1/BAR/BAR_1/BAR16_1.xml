<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR16">
				<lg n="1">
					<l n="1" num="1.1"><space quantity="12" unit="char"></space><w n="1.1">À</w> <w n="1.2">qui</w> <w n="1.3">rêves</w>-<w n="1.4">tu</w> <w n="1.5">si</w> <w n="1.6">tu</w> <w n="1.7">rêve</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Front</w> <w n="2.2">bombé</w> <w n="2.3">que</w> <w n="2.4">j</w>’<w n="2.5">adore</w> <w n="2.6">et</w> <w n="2.7">voudrais</w> <w n="2.8">entr</w>’<w n="2.9">ouvrir</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Entr</w>’<w n="3.2">ouvrir</w> <w n="3.3">d</w>’<w n="3.4">un</w> <w n="3.5">baiser</w> <w n="3.6">pénétrant</w> <w n="3.7">comme</w> <w n="3.8">un</w> <w n="3.9">glaive</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Pour</w> <w n="4.2">voir</w> <w n="4.3">si</w> <w n="4.4">c</w>’<w n="4.5">est</w> <w n="4.6">à</w> <w n="4.7">moi</w>, — <w n="4.8">que</w> <w n="4.9">tu</w> <w n="4.10">fais</w> <w n="4.11">tant</w> <w n="4.12">souffrir</w> !</l>
					<l n="5" num="1.5"><w n="5.1">O</w> <w n="5.2">front</w> <w n="5.3">idolâtré</w>, <w n="5.4">mais</w> <w n="5.5">fermé</w>, — <w n="5.6">noir</w> <w n="5.7">mystère</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Plus</w> <w n="6.2">noir</w> <w n="6.3">que</w> <w n="6.4">ces</w> <w n="6.5">yeux</w> <w n="6.6">noirs</w> <w n="6.7">qui</w> <w n="6.8">font</w> <w n="6.9">la</w> <w n="6.10">Nuit</w> <w n="6.11">en</w> <w n="6.12">moi</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">dont</w> <w n="7.3">le</w> <w n="7.4">sombre</w> <w n="7.5">feu</w> <w n="7.6">nourrit</w> <w n="7.7">et</w> <w n="7.8">désespère</w></l>
					<l n="8" num="1.8"><space quantity="12" unit="char"></space><w n="8.1">L</w>’<w n="8.2">amour</w> <w n="8.3">affreux</w> <w n="8.4">que</w> <w n="8.5">j</w>’<w n="8.6">ai</w> <w n="8.7">pour</w> <w n="8.8">toi</w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space quantity="12" unit="char"></space><w n="9.1">Je</w> <w n="9.2">n</w>’<w n="9.3">ai</w> <w n="9.4">su</w> <w n="9.5">jamais</w> <w n="9.6">si</w> <w n="9.7">tu</w> <w n="9.8">pense</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Si</w> <w n="10.2">tu</w> <w n="10.3">sens</w>, — <w n="10.4">si</w> <w n="10.5">ton</w> <w n="10.6">cœur</w> <w n="10.7">bat</w> <w n="10.8">comme</w> <w n="10.9">un</w> <w n="10.10">autre</w> <w n="10.11">cœur</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Et</w> <w n="11.2">s</w>’<w n="11.3">il</w> <w n="11.4">est</w> <w n="11.5">quelque</w> <w n="11.6">chose</w> <w n="11.7">au</w> <w n="11.8">fond</w> <w n="11.9">de</w> <w n="11.10">ton</w> <w n="11.11">silence</w></l>
					<l n="12" num="2.4"><w n="12.1">Obstinément</w> <w n="12.2">gardé</w>, <w n="12.3">cruellement</w> <w n="12.4">boudeur</w> !</l>
				</lg>
				<lg n="3">
					<l n="13" num="3.1"><w n="13.1">Non</w> ! <w n="13.2">je</w> <w n="13.3">n</w>’<w n="13.4">ai</w> <w n="13.5">jamais</w> <w n="13.6">su</w> <w n="13.7">s</w>’<w n="13.8">il</w> <w n="13.9">était</w> <w n="13.10">dans</w> <w n="13.11">ton</w> <w n="13.12">âme</w></l>
					<l n="14" num="3.2"><w n="14.1">Une</w> <w n="14.2">place</w> <w n="14.3">où</w> <w n="14.4">plus</w> <w n="14.5">tard</w> <w n="14.6">pût</w> <w n="14.7">naître</w> <w n="14.8">un</w> <w n="14.9">sentiment</w>,</l>
					<l n="15" num="3.3"><w n="15.1">Ou</w> <w n="15.2">si</w> <w n="15.3">tu</w> <w n="15.4">dois</w> <w n="15.5">rester</w> <w n="15.6">une</w> <w n="15.7">enfant</w>, <w n="15.8">quoique</w> <w n="15.9">femme</w>,</l>
					<l n="16" num="3.4"><space quantity="12" unit="char"></space><w n="16.1">Une</w> <w n="16.2">enfant</w> ! <w n="16.3">pas</w> <w n="16.4">même</w> ! — <w n="16.5">un</w> <w n="16.6">néant</w> !</l>
				</lg>
				<lg n="4">
					<l n="17" num="4.1"><space quantity="12" unit="char"></space><w n="17.1">Un</w> <w n="17.2">néant</w> <w n="17.3">qui</w> <w n="17.4">semble</w> <w n="17.5">la</w> <w n="17.6">vie</w> !</l>
					<l n="18" num="4.2"><w n="18.1">Mais</w> <w n="18.2">qui</w> <w n="18.3">fait</w> <w n="18.4">tout</w> <w n="18.5">oser</w> <w n="18.6">aux</w> <w n="18.7">cœurs</w> <w n="18.8">comme</w> <w n="18.9">le</w> <w n="18.10">mien</w> ;</l>
					<l n="19" num="4.3"><w n="19.1">Car</w> <w n="19.2">l</w>’<w n="19.3">être</w> <w n="19.4">inanimé</w> <w n="19.5">qu</w>’<w n="19.6">on</w> <w n="19.7">aime</w>, <w n="19.8">nous</w> <w n="19.9">défie</w> !</l>
					<l n="20" num="4.4"><w n="20.1">On</w> <w n="20.2">brûlerait</w> <w n="20.3">le</w> <w n="20.4">marbre</w> <w n="20.5">en</w> <w n="20.6">l</w>’<w n="20.7">aimant</w> ! — <w n="20.8">Mais</w> <w n="20.9">le</w> <w n="20.10">rien</w> !!</l>
					<l ana="incomplete" where="F" n="21" num="4.5"><w n="21.1">Le</w> <w n="21.2">rien</w> <w n="21.3">vêtu</w> <w n="21.4">d</w>’<w n="21.5">un</w> <w n="21.6">corps</w>................................</l>
				</lg>
			</div></body></text></TEI>