<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR21">
				<head type="main">À Roger de Beauvoir</head>
				<head type="sub_1">en lui envoyant la Bague d’Annibal</head>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="12" unit="char"></space><w n="1.1">Poète</w> <w n="1.2">de</w> <w n="1.3">cape</w> <w n="1.4">et</w> <w n="1.5">d</w>’<w n="1.6">épée</w></l>
					<l n="2" num="1.2"><space quantity="12" unit="char"></space><w n="2.1">À</w> <w n="2.2">qui</w> <w n="2.3">n</w>’<w n="2.4">a</w> <w n="2.5">jamais</w> <w n="2.6">résisté</w></l>
					<l n="3" num="1.3"><space quantity="12" unit="char"></space><w n="3.1">Ni</w> <w n="3.2">la</w> <w n="3.3">Muse</w> <w n="3.4">ni</w> <w n="3.5">la</w> <w n="3.6">Beauté</w>,</l>
					<l n="4" num="1.4"><space quantity="12" unit="char"></space><w n="4.1">Ni</w> <w n="4.2">la</w> <w n="4.3">Grâce</w> <w n="4.4">désoccupée</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Thaumaturge</w> <w n="5.2">d</w>’<w n="5.3">amour</w>, <w n="5.4">qui</w> <w n="5.5">peux</w> <w n="5.6">d</w>’<w n="5.7">une</w> <w n="5.8">poupée</w></l>
					<l n="6" num="1.6"><space quantity="12" unit="char"></space><w n="6.1">Faire</w> <w n="6.2">un</w> <w n="6.3">démon</w> <w n="6.4">de</w> <w n="6.5">volupté</w> !</l>
				</lg>
				<lg n="2">
					<l n="7" num="2.1"><space quantity="12" unit="char"></space><w n="7.1">Tu</w> <w n="7.2">redemandes</w> <w n="7.3">cette</w> <w n="7.4">histoire</w></l>
					<l n="8" num="2.2"><space quantity="12" unit="char"></space><w n="8.1">Qu</w>’<w n="8.2">aux</w> <w n="8.3">temps</w> <w n="8.4">si</w> <w n="8.5">fous</w> <w n="8.6">de</w> <w n="8.7">mon</w> <w n="8.8">passé</w></l>
					<l n="9" num="2.3"><space quantity="12" unit="char"></space><w n="9.1">J</w>’<w n="9.2">écrivis</w>, <w n="9.3">un</w> <w n="9.4">soir</w>, <w n="9.5">de</w> <w n="9.6">mémoire</w>,</l>
					<l n="10" num="2.4"><space quantity="12" unit="char"></space><w n="10.1">Avec</w> <w n="10.2">de</w> <w n="10.3">l</w>’<w n="10.4">encre</w> <w n="10.5">rose</w> <w n="10.6">et</w> <w n="10.7">noire</w>,</l>
					<l n="11" num="2.5"><space quantity="12" unit="char"></space><w n="11.1">Et</w> <w n="11.2">la</w> <w n="11.3">gaieté</w> <w n="11.4">d</w>’<w n="11.5">un</w> <w n="11.6">cœur</w> <w n="11.7">brisé</w>.</l>
				</lg>
				<lg n="3">
					<l n="12" num="3.1"><space quantity="12" unit="char"></space><w n="12.1">Revois</w> <w n="12.2">ce</w> <w n="12.3">portrait</w> <w n="12.4">d</w>’<w n="12.5">une</w> <w n="12.6">femme</w></l>
					<l n="13" num="3.2"><space quantity="12" unit="char"></space><w n="13.1">Dont</w> <w n="13.2">le</w> <w n="13.3">sourire</w> <w n="13.4">était</w> <w n="13.5">mortel</w>,</l>
					<l n="14" num="3.3"><w n="14.1">Argile</w> <w n="14.2">inaccessible</w> <w n="14.3">aux</w> <w n="14.4">chaleurs</w> <w n="14.5">dé</w> <w n="14.6">la</w> <w n="14.7">flamme</w>,</l>
					<l n="15" num="3.4"><space quantity="12" unit="char"></space><w n="15.1">Corps</w> <w n="15.2">charmant</w>, <w n="15.3">mais</w> <w n="15.4">vide</w> <w n="15.5">d</w>’<w n="15.6">une</w> <w n="15.7">âme</w>…</l>
					<l n="16" num="3.5"><space quantity="12" unit="char"></space><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">de</w> <w n="16.4">la</w> <w n="16.5">vengeance</w>… <w n="16.6">au</w> <w n="16.7">pastel</w> !</l>
				</lg>
				<lg n="4">
					<l n="17" num="4.1"><space quantity="12" unit="char"></space><w n="17.1">Une</w> <w n="17.2">vengeance</w>… <w n="17.3">faible</w> <w n="17.4">chose</w> !</l>
					<l n="18" num="4.2"><w n="18.1">Qui</w> <w n="18.2">ne</w> <w n="18.3">rachète</w> <w n="18.4">rien</w> <w n="18.5">des</w> <w n="18.6">maux</w> <w n="18.7">qu</w>’<w n="18.8">on</w> <w n="18.9">a</w> <w n="18.10">soufferts</w> !</l>
					<l n="19" num="4.3"><space quantity="12" unit="char"></space><w n="19.1">Elle</w> <w n="19.2">s</w>’<w n="19.3">énerve</w> <w n="19.4">dans</w> <w n="19.5">ma</w> <w n="19.6">prose</w>…</l>
					<l n="20" num="4.4"><w n="20.1">Mais</w> <w n="20.2">comme</w> <w n="20.3">un</w> <w n="20.4">fort</w> <w n="20.5">poison</w> <w n="20.6">dans</w> <w n="20.7">des</w> <w n="20.8">parfums</w> <w n="20.9">de</w> <w n="20.10">rose</w>,</l>
					<l n="21" num="4.5"><space quantity="12" unit="char"></space><w n="21.1">Elle</w> <w n="21.2">enivrerait</w> <w n="21.3">dans</w> <w n="21.4">tes</w> <w n="21.5">vers</w> !</l>
				</lg>
			</div></body></text></TEI>