<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poussières</title>
				<title type="medium">Édition électronique</title>
				<author key="BAR">
					<name>
						<forename>Jules</forename>
						<surname>BARBEY D’AUREVILLY</surname>
					</name>
					<date from="1808" to="1889">1808-1889</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1129 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">BAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poussières</title>
						<author>Barbey d’Aurevilly</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/barbeydaurevillypoussieres.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title type="main">Poussières</title>
					<author>Barbey d’Aurevilly</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Alphonose Lemerre, Éditeur</publisher>
						<date when="1897">1897</date>
					</imprint>
				</monogr>
				<note>Édition de référence pour les corrections métriques</note>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1854">1854</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-01" who="RR">Révision de l’entête pour validation</change>
			<change when="2016-03-01" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BAR17">
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Si</w> <w n="1.2">tu</w> <w n="1.3">pleures</w> <w n="1.4">jamais</w>, <w n="1.5">que</w> <w n="1.6">ce</w> <w n="1.7">soit</w> <w n="1.8">en</w> <w n="1.9">silence</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Si</w> <w n="2.2">l</w>’<w n="2.3">on</w> <w n="2.4">te</w> <w n="2.5">voit</w> <w n="2.6">pleurer</w>, <w n="2.7">essuie</w> <w n="2.8">au</w> <w n="2.9">moins</w> <w n="2.10">tes</w> <w n="2.11">pleurs</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Car</w> <w n="3.2">tu</w> <w n="3.3">ne</w> <w n="3.4">peux</w> <w n="3.5">trouver</w> <w n="3.6">au</w> <w n="3.7">fond</w> <w n="3.8">de</w> <w n="3.9">ta</w> <w n="3.10">souffrance</w></l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">calme</w> <w n="4.3">fier</w> <w n="4.4">qui</w> <w n="4.5">naît</w> <w n="4.6">des</w> <w n="4.7">injustes</w> <w n="4.8">douleurs</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Non</w> ! <w n="5.2">tu</w> <w n="5.3">ne</w> <w n="5.4">le</w> <w n="5.5">peux</w> <w n="5.6">pas</w>. <w n="5.7">Si</w> <w n="5.8">ta</w> <w n="5.9">vie</w> <w n="5.10">est</w> <w n="5.11">brisée</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">me</w> <w n="6.3">brisa</w> <w n="6.4">le</w> <w n="6.5">cœur</w> <w n="6.6">où</w> <w n="6.7">tu</w> <w n="6.8">vivais</w> ? <w n="6.9">Dis</w>-<w n="6.10">moi</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Dis</w>-<w n="7.2">moi</w> <w n="7.3">qui</w> <w n="7.4">l</w>’<w n="7.5">a</w> <w n="7.6">voulu</w>, <w n="7.7">si</w> <w n="7.8">je</w> <w n="7.9">t</w>’<w n="7.10">ai</w> <w n="7.11">délaissée</w> ?</l>
					<l n="8" num="2.4"><w n="8.1">Tes</w> <w n="8.2">pleurs</w> <w n="8.3">amers</w> <w n="8.4">et</w> <w n="8.5">vains</w> <w n="8.6">n</w>’<w n="8.7">accuseraient</w> <w n="8.8">que</w> <w n="8.9">toi</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">femmes</w> <w n="9.3">sont</w> <w n="9.4">ainsi</w> ! <w n="9.5">Que</w> <w n="9.6">je</w> <w n="9.7">t</w>’<w n="9.8">eusse</w> <w n="9.9">trahie</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Tu</w> <w n="10.2">reviendrais</w> <w n="10.3">m</w>’<w n="10.4">offrir</w> <w n="10.5">à</w> <w n="10.6">genoux</w> <w n="10.7">mon</w> <w n="10.8">pardon</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Si</w> <w n="11.2">tu</w> <w n="11.3">m</w>’<w n="11.4">aimais</w>, <w n="11.5">pourquoi</w> <w n="11.6">cette</w> <w n="11.7">triste</w> <w n="11.8">folie</w></l>
					<l n="12" num="3.4"><w n="12.1">D</w>’<w n="12.2">implorer</w> <w n="12.3">de</w> <w n="12.4">l</w>’<w n="12.5">amour</w> <w n="12.6">la</w> <w n="12.7">fuite</w> <w n="12.8">et</w> <w n="12.9">l</w>’<w n="12.10">abandon</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Mon</w> <w n="13.2">orgueil</w> <w n="13.3">t</w>’<w n="13.4">obéit</w> <w n="13.5">sans</w> <w n="13.6">risquer</w> <w n="13.7">un</w> <w n="13.8">murmure</w>.</l>
					<l n="14" num="4.2"><w n="14.1">A</w> <w n="14.2">ce</w> <w n="14.3">monde</w> <w n="14.4">sans</w> <w n="14.5">cœur</w> <w n="14.6">je</w> <w n="14.7">cache</w> <w n="14.8">mes</w> <w n="14.9">regrets</w> ;</l>
					<l n="15" num="4.3"><w n="15.1">Sous</w> <w n="15.2">un</w> <w n="15.3">dédain</w> <w n="15.4">léger</w> <w n="15.5">je</w> <w n="15.6">voile</w> <w n="15.7">ma</w> <w n="15.8">torture</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">si</w> <w n="16.3">bien</w> — <w n="16.4">que</w> <w n="16.5">toi</w>-<w n="16.6">même</w> <w n="16.7">aussi</w> <w n="16.8">t</w>’<w n="16.9">y</w> <w n="16.10">tromperais</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">tu</w> <w n="17.3">m</w>’<w n="17.4">aimas</w> <w n="17.5">pourtant</w> ! <w n="17.6">Amour</w> <w n="17.7">triste</w> <w n="17.8">et</w> <w n="17.9">rapide</w> !</l>
					<l n="18" num="5.2"><w n="18.1">Ne</w> <w n="18.2">me</w> <w n="18.3">semblait</w>-<w n="18.4">il</w> <w n="18.5">pas</w> <w n="18.6">le</w> <w n="18.7">plus</w> <w n="18.8">profond</w> <w n="18.9">des</w> <w n="18.10">deux</w> ?</l>
					<l n="19" num="5.3"><w n="19.1">Sans</w> <w n="19.2">moi</w> <w n="19.3">de</w> <w n="19.4">quel</w> <w n="19.5">bonheur</w> <w n="19.6">étais</w>-<w n="19.7">tu</w> <w n="19.8">donc</w> <w n="19.9">avide</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Puisqu</w>’<w n="20.2">avec</w> <w n="20.3">moi</w> <w n="20.4">jamais</w> <w n="20.5">tu</w> <w n="20.6">n</w>’<w n="20.7">avais</w> <w n="20.8">l</w>’<w n="20.9">air</w> <w n="20.10">heureux</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">à</w> <w n="21.3">présent</w> <w n="21.4">sans</w> <w n="21.5">moi</w> <w n="21.6">plus</w> <w n="21.7">heureuse</w>, <w n="21.8">j</w>’<w n="21.9">espère</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Si</w> <w n="22.2">tu</w> <w n="22.3">penses</w> <w n="22.4">parfois</w> <w n="22.5">à</w> <w n="22.6">celui</w> <w n="22.7">qui</w> <w n="22.8">t</w>’<w n="22.9">aimait</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Ne</w> <w n="23.2">te</w> <w n="23.3">repens</w>-<w n="23.4">tu</w> <w n="23.5">pas</w> <w n="23.6">d</w>’<w n="23.7">avoir</w> <w n="23.8">fait</w> <w n="23.9">un</w> <w n="23.10">mystère</w></l>
					<l n="24" num="6.4"><w n="24.1">Du</w> <w n="24.2">mal</w> <w n="24.3">que</w> <w n="24.4">tu</w> <w n="24.5">cachais</w> <w n="24.6">et</w> <w n="24.7">qui</w> <w n="24.8">l</w>’<w n="24.9">inquiétait</w> ?</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">si</w> <w n="25.3">tu</w> <w n="25.4">t</w>’<w n="25.5">en</w> <w n="25.6">repens</w>, <w n="25.7">cache</w>-<w n="25.8">le</w> <w n="25.9">dans</w> <w n="25.10">ton</w> <w n="25.11">âme</w>.</l>
					<l n="26" num="7.2"><w n="26.1">Tout</w> <w n="26.2">n</w>’<w n="26.3">est</w>-<w n="26.4">il</w> <w n="26.5">pas</w>, <w n="26.6">hélas</w> ! <w n="26.7">entre</w> <w n="26.8">nous</w> <w n="26.9">consommé</w> ?</l>
					<l n="27" num="7.3"><w n="27.1">O</w> <w n="27.2">toi</w> <w n="27.3">qui</w> <w n="27.4">n</w>’<w n="27.5">eus</w> <w n="27.6">jamais</w> <w n="27.7">l</w>’<w n="27.8">abandon</w> <w n="27.9">d</w>’<w n="27.10">une</w> <w n="27.11">femme</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Reste</w> <w n="28.2">ce</w> <w n="28.3">que</w> <w n="28.4">tu</w> <w n="28.5">fus</w>, <w n="28.6">ô</w> <w n="28.7">blond</w> <w n="28.8">Sphinx</w> <w n="28.9">trop</w> <w n="28.10">aimé</w> !</l>
				</lg>
			</div></body></text></TEI>