<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX51">
				<head type="main">MARCHE FUNÈBRE</head>
				<head type="sub_1">CHŒUR DES DERNIERS HOMMES</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">temps</w> <w n="1.3">sont</w> <w n="1.4">arrivés</w>, <w n="1.5">des</w> <w n="1.6">vieilles</w> <w n="1.7">prophéties</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Ils</w> <w n="2.2">sont</w> <w n="2.3">venus</w>, <w n="2.4">les</w> <w n="2.5">jours</w> <w n="2.6">d</w>’<w n="2.7">universelle</w> <w n="2.8">horreur</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">ombres</w> <w n="3.3">du</w> <w n="3.4">néant</w>, <w n="3.5">d</w>’<w n="3.6">heure</w> <w n="3.7">en</w> <w n="3.8">heure</w> <w n="3.9">épaissies</w>,</l>
					<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">allongent</w> <w n="4.3">sur</w> <w n="4.4">nos</w> <w n="4.5">fronts</w> <w n="4.6">écrasés</w> <w n="4.7">de</w> <w n="4.8">terreur</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Nous</w> <w n="5.2">les</w> <w n="5.3">vivons</w>, <w n="5.4">les</w> <w n="5.5">jours</w> <w n="5.6">d</w>’<w n="5.7">agonie</w> <w n="5.8">et</w> <w n="5.9">de</w> <w n="5.10">râle</w> !</l>
					<l n="6" num="2.2"><w n="6.1">À</w> <w n="6.2">l</w>’<w n="6.3">orient</w>, <w n="6.4">jamais</w> <w n="6.5">plus</w> <w n="6.6">de</w> <w n="6.7">matins</w> <w n="6.8">nouveaux</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Comme</w> <w n="7.2">le</w> <w n="7.3">bronze</w> <w n="7.4">noir</w> <w n="7.5">qui</w> <w n="7.6">ferme</w> <w n="7.7">les</w> <w n="7.8">caveaux</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">sol</w> <w n="8.3">frappé</w> <w n="8.4">résonne</w> <w n="8.5">en</w> <w n="8.6">rumeur</w> <w n="8.7">sépulcrale</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">ténèbres</w> <w n="9.3">sur</w> <w n="9.4">nous</w> <w n="9.5">amassent</w> <w n="9.6">leurs</w> <w n="9.7">replis</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Là</w>-<w n="10.2">haut</w>, <w n="10.3">rien</w> <w n="10.4">désormais</w> <w n="10.5">qui</w> <w n="10.6">regarde</w> <w n="10.7">ou</w> <w n="10.8">réponde</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Derniers</w> <w n="11.2">fils</w> <w n="11.3">de</w> <w n="11.4">Caïn</w> ! <w n="11.5">Les</w> <w n="11.6">temps</w> <w n="11.7">sont</w> <w n="11.8">accomplis</w>.</l>
					<l n="12" num="3.4"><w n="12.1">Pour</w> <w n="12.2">toujours</w>, <w n="12.3">cette</w> <w n="12.4">fois</w>, <w n="12.5">la</w> <w n="12.6">mort</w> <w n="12.7">est</w> <w n="12.8">dans</w> <w n="12.9">le</w> <w n="12.10">monde</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Sous</w> <w n="13.2">les</w> <w n="13.3">astres</w> <w n="13.4">éteints</w>, <w n="13.5">sous</w> <w n="13.6">le</w> <w n="13.7">terne</w> <w n="13.8">soleil</w>,</l>
					<l n="14" num="4.2"><w n="14.1">La</w> <w n="14.2">nuit</w> <w n="14.3">funèbre</w> <w n="14.4">étend</w> <w n="14.5">ses</w> <w n="14.6">suaires</w> <w n="14.7">immenses</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">sein</w> <w n="15.3">froid</w> <w n="15.4">de</w> <w n="15.5">la</w> <w n="15.6">terre</w> <w n="15.7">a</w> <w n="15.8">gardé</w> <w n="15.9">les</w> <w n="15.10">semences</w>.</l>
					<l n="16" num="4.4"><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">à</w> <w n="16.4">son</w> <w n="16.5">tour</w> <w n="16.6">d</w>’<w n="16.7">entrer</w> <w n="16.8">dans</w> <w n="16.9">l</w>’<w n="16.10">éternel</w> <w n="16.11">sommeil</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Les</w> <w n="17.2">derniers</w> <w n="17.3">dieux</w> <w n="17.4">sont</w> <w n="17.5">morts</w>, <w n="17.6">et</w> <w n="17.7">morte</w> <w n="17.8">est</w> <w n="17.9">la</w> <w n="17.10">prière</w>.</l>
					<l n="18" num="5.2"><w n="18.1">Nous</w> <w n="18.2">avons</w> <w n="18.3">renié</w> <w n="18.4">nos</w> <w n="18.5">héros</w> <w n="18.6">et</w> <w n="18.7">leurs</w> <w n="18.8">lois</w>.</l>
					<l n="19" num="5.3"><w n="19.1">Nul</w> <w n="19.2">espoir</w> <w n="19.3">ne</w> <w n="19.4">reluit</w> <w n="19.5">devant</w> <w n="19.6">nous</w> ; <w n="19.7">et</w>, <w n="19.8">derrière</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Ils</w> <w n="20.2">ne</w> <w n="20.3">renaîtront</w> <w n="20.4">plus</w>, <w n="20.5">les</w> <w n="20.6">rêves</w> <w n="20.7">d</w>’<w n="20.8">autrefois</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Sur</w> <w n="21.2">l</w>’<w n="21.3">univers</w> <w n="21.4">entier</w> <w n="21.5">la</w> <w n="21.6">mort</w> <w n="21.7">ouvre</w> <w n="21.8">son</w> <w n="21.9">aile</w></l>
					<l n="22" num="6.2"><w n="22.1">Lugubre</w>. <w n="22.2">Sous</w> <w n="22.3">nos</w> <w n="22.4">pas</w> <w n="22.5">le</w> <w n="22.6">sol</w> <w n="22.7">dur</w> <w n="22.8">sonne</w> <w n="22.9">creux</w>.</l>
					<l n="23" num="6.3"><w n="23.1">N</w>’<w n="23.2">y</w> <w n="23.3">cherchons</w> <w n="23.4">plus</w> <w n="23.5">le</w> <w n="23.6">pain</w> <w n="23.7">des</w> <w n="23.8">jours</w> <w n="23.9">aventureux</w>.</l>
					<l n="24" num="6.4"><w n="24.1">Dans</w> <w n="24.2">nos</w> <w n="24.3">veines</w> <w n="24.4">la</w> <w n="24.5">sève</w> <w n="24.6">est</w> <w n="24.7">morte</w> <w n="24.8">comme</w> <w n="24.9">en</w> <w n="24.10">elle</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Hommes</w> ! <w n="25.2">Contemplons</w>-<w n="25.3">nous</w> <w n="25.4">dans</w> <w n="25.5">toutes</w> <w n="25.6">nos</w> <w n="25.7">laideurs</w>.</l>
					<l n="26" num="7.2"><w n="26.1">Ô</w> <w n="26.2">rayons</w> <w n="26.3">qui</w> <w n="26.4">brilliez</w> <w n="26.5">aux</w> <w n="26.6">yeux</w> <w n="26.7">clairs</w> <w n="26.8">des</w> <w n="26.9">ancêtres</w> !</l>
					<l n="27" num="7.3"><w n="27.1">Nos</w> <w n="27.2">yeux</w> <w n="27.3">caves</w>, <w n="27.4">chargés</w> <w n="27.5">d</w>’<w n="27.6">ennuis</w> <w n="27.7">et</w> <w n="27.8">de</w> <w n="27.9">lourdeurs</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Se</w> <w n="28.2">tournent</w> <w n="28.3">hébétés</w> <w n="28.4">des</w> <w n="28.5">choses</w> <w n="28.6">vers</w> <w n="28.7">les</w> <w n="28.8">êtres</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Spectre</w> <w n="29.2">charmant</w>, <w n="29.3">amour</w>, <w n="29.4">qui</w> <w n="29.5">consolais</w> <w n="29.6">du</w> <w n="29.7">ciel</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Amour</w>, <w n="30.2">toi</w> <w n="30.3">qu</w>’<w n="30.4">ont</w> <w n="30.5">chanté</w> <w n="30.6">les</w> <w n="30.7">aïeux</w> <w n="30.8">incrédules</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Nul</w> <w n="31.2">de</w> <w n="31.3">nous</w> <w n="31.4">ne</w> <w n="31.5">t</w>’<w n="31.6">a</w> <w n="31.7">vu</w> <w n="31.8">dans</w> <w n="31.9">nos</w> <w n="31.10">froids</w> <w n="31.11">crépuscules</w>.</l>
					<l n="32" num="8.4"><w n="32.1">Meurs</w>, <w n="32.2">vieux</w> <w n="32.3">spectre</w> <w n="32.4">gonflé</w> <w n="32.5">de</w> <w n="32.6">mensonge</w> <w n="32.7">et</w> <w n="32.8">de</w> <w n="32.9">fiel</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Notre</w> <w n="33.2">œil</w> <w n="33.3">n</w>’<w n="33.4">a</w> <w n="33.5">plus</w> <w n="33.6">de</w> <w n="33.7">pleurs</w>, <w n="33.8">plus</w> <w n="33.9">de</w> <w n="33.10">sang</w> <w n="33.11">notre</w> <w n="33.12">artère</w>.</l>
					<l n="34" num="9.2"><w n="34.1">Nos</w> <w n="34.2">rires</w> <w n="34.3">ont</w> <w n="34.4">bavé</w> <w n="34.5">sur</w> <w n="34.6">ton</w> <w n="34.7">fatal</w> <w n="34.8">flambeau</w>.</l>
					<l n="35" num="9.3"><w n="35.1">Si</w> <w n="35.2">jamais</w> <w n="35.3">tu</w> <w n="35.4">fis</w> <w n="35.5">battre</w> <w n="35.6">un</w> <w n="35.7">cœur</w> <w n="35.8">d</w>’<w n="35.9">homme</w> <w n="35.10">sur</w> <w n="35.11">terre</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Amour</w>, <w n="36.2">notre</w> <w n="36.3">âme</w> <w n="36.4">vide</w> <w n="36.5">est</w> <w n="36.6">ton</w> <w n="36.7">affreux</w> <w n="36.8">tombeau</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Le</w> <w n="37.2">repentir</w> <w n="37.3">est</w> <w n="37.4">mort</w> <w n="37.5">dans</w> <w n="37.6">nos</w> <w n="37.7">églises</w> <w n="37.8">sourdes</w>.</l>
					<l n="38" num="10.2"><w n="38.1">Après</w> <w n="38.2">l</w>’<w n="38.3">amour</w>, <w n="38.4">est</w> <w n="38.5">morte</w> <w n="38.6">aussi</w> <w n="38.7">la</w> <w n="38.8">volupté</w>.</l>
					<l n="39" num="10.3"><w n="39.1">Nul</w> <w n="39.2">espoir</w> <w n="39.3">devant</w> <w n="39.4">nous</w> ; <w n="39.5">au</w> <w n="39.6">ciel</w>, <w n="39.7">nulle</w> <w n="39.8">clarté</w>.</l>
					<l n="40" num="10.4"><w n="40.1">Rions</w> <w n="40.2">affreusement</w> <w n="40.3">dans</w> <w n="40.4">les</w> <w n="40.5">ténèbres</w> <w n="40.6">lourdes</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">L</w>’<w n="41.2">ancien</w> <w n="41.3">orgueil</w> <w n="41.4">n</w>’<w n="41.5">est</w> <w n="41.6">plus</w>, <w n="41.7">ô</w> <w n="41.8">peuples</w> <w n="41.9">endormis</w> !</l>
					<l n="42" num="11.2"><w n="42.1">Qui</w> <w n="42.2">flamboyait</w> <w n="42.3">encor</w> <w n="42.4">sur</w> <w n="42.5">votre</w> <w n="42.6">front</w> <w n="42.7">naguère</w>.</l>
					<l n="43" num="11.3"><w n="43.1">L</w>’<w n="43.2">orgueil</w> <w n="43.3">a</w> <w n="43.4">terrassé</w> <w n="43.5">les</w> <w n="43.6">dieux</w>, <w n="43.7">ses</w> <w n="43.8">ennemis</w> ;</l>
					<l n="44" num="11.4"><w n="44.1">Il</w> <w n="44.2">est</w> <w n="44.3">mort</w> <w n="44.4">de</w> <w n="44.5">sa</w> <w n="44.6">gloire</w> <w n="44.7">en</w> <w n="44.8">regrettant</w> <w n="44.9">la</w> <w n="44.10">guerre</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Aux</w> <w n="45.2">dernières</w> <w n="45.3">clartés</w> <w n="45.4">de</w> <w n="45.5">nos</w> <w n="45.6">feux</w>, <w n="45.7">en</w> <w n="45.8">troupeau</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Mêlés</w> <w n="46.2">au</w> <w n="46.3">vil</w> <w n="46.4">bétail</w> <w n="46.5">que</w> <w n="46.6">courbe</w> <w n="46.7">l</w>’<w n="46.8">épouvante</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Attendons</w> <w n="47.2">les</w> <w n="47.3">yeux</w> <w n="47.4">bas</w>, <w n="47.5">n</w>’<w n="47.6">ayant</w> <w n="47.7">plus</w> <w n="47.8">de</w> <w n="47.9">vivante</w></l>
					<l n="48" num="12.4"><w n="48.1">En</w> <w n="48.2">nous</w> <w n="48.3">que</w> <w n="48.4">la</w> <w n="48.5">terreur</w> <w n="48.6">qui</w> <w n="48.7">court</w> <w n="48.8">sous</w> <w n="48.9">notre</w> <w n="48.10">peau</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Quelqu</w>’<w n="49.2">un</w> <w n="49.3">sent</w>-<w n="49.4">il</w> <w n="49.5">vers</w> <w n="49.6">l</w>’<w n="49.7">or</w> <w n="49.8">frémir</w> <w n="49.9">ses</w> <w n="49.10">doigts</w> <w n="49.11">inertes</w>,</l>
					<l n="50" num="13.2"><w n="50.1">Et</w> <w n="50.2">le</w> <w n="50.3">honteux</w> <w n="50.4">prurit</w> <w n="50.5">crisper</w> <w n="50.6">encor</w> <w n="50.7">sa</w> <w n="50.8">chair</w> ?</l>
					<l n="51" num="13.3"><w n="51.1">Non</w>, <w n="51.2">tout</w> <w n="51.3">désir</w> <w n="51.4">s</w>’<w n="51.5">éteint</w> <w n="51.6">dans</w> <w n="51.7">nos</w> <w n="51.8">âmes</w> <w n="51.9">désertes</w>.</l>
					<l n="52" num="13.4"><w n="52.1">Plus</w> <w n="52.2">rien</w> <w n="52.3">qui</w> <w n="52.4">dans</w> <w n="52.5">nos</w> <w n="52.6">cils</w> <w n="52.7">allume</w> <w n="52.8">un</w> <w n="52.9">seul</w> <w n="52.10">éclair</w>.</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Soif</w> <w n="53.2">du</w> <w n="53.3">sang</w> <w n="53.4">fraternel</w>, <w n="53.5">fièvre</w> <w n="53.6">chaude</w> <w n="53.7">du</w> <w n="53.8">crime</w>,</l>
					<l n="54" num="14.2"><w n="54.1">Vous</w> <w n="54.2">attestiez</w> <w n="54.3">la</w> <w n="54.4">vie</w> <w n="54.5">au</w> <w n="54.6">moins</w> <w n="54.7">par</w> <w n="54.8">le</w> <w n="54.9">combat</w>.</l>
					<l n="55" num="14.3"><w n="55.1">Le</w> <w n="55.2">mal</w> <w n="55.3">qui</w> <w n="55.4">vous</w> <w n="55.5">leurrait</w> <w n="55.6">de</w> <w n="55.7">son</w> <w n="55.8">sinistre</w> <w n="55.9">appât</w>,</l>
					<l n="56" num="14.4"><w n="56.1">Par</w> <w n="56.2">deux</w> <w n="56.3">vertus</w> <w n="56.4">peut</w>-<w n="56.5">être</w> <w n="56.6">ennoblissait</w> <w n="56.7">l</w>’<w n="56.8">abîme</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><w n="57.1">Force</w> <w n="57.2">et</w> <w n="57.3">courage</w> <w n="57.4">en</w> <w n="57.5">nous</w> <w n="57.6">sont</w> <w n="57.7">morts</w> <w n="57.8">avec</w> <w n="57.9">le</w> <w n="57.10">mal</w>.</l>
					<l n="58" num="15.2"><w n="58.1">Les</w> <w n="58.2">vices</w> <w n="58.3">n</w>’<w n="58.4">ont</w> <w n="58.5">plus</w> <w n="58.6">rien</w> <w n="58.7">en</w> <w n="58.8">nos</w> <w n="58.9">cœurs</w> <w n="58.10">qui</w> <w n="58.11">fermente</w>.</l>
					<l n="59" num="15.3"><w n="59.1">Sur</w> <w n="59.2">l</w>’<w n="59.3">esprit</w> <w n="59.4">avili</w> <w n="59.5">triomphe</w> <w n="59.6">l</w>’<w n="59.7">animal</w></l>
					<l n="60" num="15.4"><w n="60.1">Qui</w> <w n="60.2">vers</w> <w n="60.3">un</w> <w n="60.4">imminent</w> <w n="60.5">inconnu</w> <w n="60.6">se</w> <w n="60.7">lamente</w>.</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">Qui</w> <w n="61.2">d</w>’<w n="61.3">entre</w> <w n="61.4">nous</w> <w n="61.5">jamais</w> <w n="61.6">t</w>’<w n="61.7">a</w> <w n="61.8">pris</w> <w n="61.9">pour</w> <w n="61.10">guide</w>,<w n="61.11">honneur</w> ?</l>
					<l n="62" num="16.2"><w n="62.1">A</w> <w n="62.2">senti</w> <w n="62.3">ton</w> <w n="62.4">levain</w> <w n="62.5">soulever</w> <w n="62.6">sa</w> <w n="62.7">colère</w> ?</l>
					<l n="63" num="16.3"><w n="63.1">Il</w> <w n="63.2">gît</w> <w n="63.3">sous</w> <w n="63.4">nos</w> <w n="63.5">fumiers</w>, <w n="63.6">ton</w> <w n="63.7">dogme</w> <w n="63.8">tutélaire</w>.</l>
					<l n="64" num="16.4"><w n="64.1">Tu</w> <w n="64.2">dors</w> <w n="64.3">depuis</w> <w n="64.4">longtemps</w>, <w n="64.5">fantôme</w> <w n="64.6">raisonneur</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><w n="65.1">Sur</w> <w n="65.2">les</w> <w n="65.3">cercueils</w> <w n="65.4">fermés</w> <w n="65.5">plus</w> <w n="65.6">un</w> <w n="65.7">seul</w> <w n="65.8">glas</w> <w n="65.9">qui</w> <w n="65.10">sonne</w>.</l>
					<l n="66" num="17.2"><w n="66.1">Dans</w> <w n="66.2">l</w>’<w n="66.3">insondable</w> <w n="66.4">oubli</w> <w n="66.5">sombrent</w> <w n="66.6">les</w> <w n="66.7">noms</w> <w n="66.8">fameux</w>.</l>
					<l n="67" num="17.3"><w n="67.1">Qui</w> <w n="67.2">de</w> <w n="67.3">nous</w> <w n="67.4">s</w>’<w n="67.5">en</w> <w n="67.6">souvient</w> ? <w n="67.7">Qui</w> <w n="67.8">les</w> <w n="67.9">pleure</w> ? <w n="67.10">Personne</w>.</l>
					<l n="68" num="17.4"><w n="68.1">Ô</w> <w n="68.2">gloire</w> ! <w n="68.3">Nul</w> <w n="68.4">de</w> <w n="68.5">nous</w> <w n="68.6">en</w> <w n="68.7">toi</w> <w n="68.8">n</w>’<w n="68.9">a</w> <w n="68.10">cru</w> <w n="68.11">comme</w> <w n="68.12">eux</w> !</l>
				</lg>
				<lg n="18">
					<l n="69" num="18.1"><w n="69.1">Soleil</w>, <w n="69.2">qui</w> <w n="69.3">mûrissais</w> <w n="69.4">beauté</w>, <w n="69.5">forme</w> <w n="69.6">et</w> <w n="69.7">jeunesse</w>,</l>
					<l n="70" num="18.2"><w n="70.1">Faisais</w> <w n="70.2">chanter</w> <w n="70.3">les</w> <w n="70.4">bois</w> <w n="70.5">et</w> <w n="70.6">rire</w> <w n="70.7">les</w> <w n="70.8">remords</w>,</l>
					<l n="71" num="18.3"><w n="71.1">Nous</w> <w n="71.2">n</w>’<w n="71.3">avons</w>, <w n="71.4">nous</w>, <w n="71.5">connu</w>, <w n="71.6">soleil</w> <w n="71.7">des</w> <w n="71.8">siècles</w> <w n="71.9">morts</w> !</l>
					<l n="72" num="18.4"><w n="72.1">Que</w> <w n="72.2">ta</w> <w n="72.3">lueur</w> <w n="72.4">fumeuse</w> <w n="72.5">et</w> <w n="72.6">ta</w> <w n="72.7">triste</w> <w n="72.8">caresse</w>.</l>
				</lg>
				<lg n="19">
					<l n="73" num="19.1"><w n="73.1">Toute</w> <w n="73.2">une</w> <w n="73.3">mer</w> <w n="73.4">d</w>’<w n="73.5">effrois</w>, <w n="73.6">femmes</w>, <w n="73.7">remonte</w> <w n="73.8">en</w> <w n="73.9">vous</w>,</l>
					<l n="74" num="19.2"><w n="74.1">Devant</w> <w n="74.2">l</w>’<w n="74.3">abjection</w> <w n="74.4">cynique</w> <w n="74.5">de</w> <w n="74.6">nos</w> <w n="74.7">faces</w>.</l>
					<l n="75" num="19.3"><w n="75.1">Quand</w> <w n="75.2">nous</w> <w n="75.3">avons</w> <w n="75.4">cherché</w> <w n="75.5">vos</w> <w n="75.6">corps</w>, <w n="75.7">nous</w> <w n="75.8">avons</w> <w n="75.9">tous</w></l>
					<l n="76" num="19.4"><w n="76.1">Abhorré</w> <w n="76.2">le</w> <w n="76.3">désir</w> <w n="76.4">dompteur</w> <w n="76.5">des</w> <w n="76.6">jeunes</w> <w n="76.7">races</w>.</l>
				</lg>
				<lg n="20">
					<l n="77" num="20.1"><w n="77.1">La</w> <w n="77.2">haine</w> <w n="77.3">est</w> <w n="77.4">morte</w>. <w n="77.5">Seul</w> <w n="77.6">a</w> <w n="77.7">survécu</w> <w n="77.8">l</w>’<w n="77.9">ennui</w>,</l>
					<l n="78" num="20.2"><w n="78.1">L</w>’<w n="78.2">insurmontable</w> <w n="78.3">ennui</w> <w n="78.4">de</w> <w n="78.5">nos</w> <w n="78.6">hideurs</w> <w n="78.7">jumelles</w>,</l>
					<l n="79" num="20.3"><w n="79.1">Qui</w> <w n="79.2">tarit</w> <w n="79.3">pour</w> <w n="79.4">toujours</w> <w n="79.5">le</w> <w n="79.6">lait</w> <w n="79.7">dans</w> <w n="79.8">vos</w> <w n="79.9">mamelles</w>,</l>
					<l n="80" num="20.4"><w n="80.1">Et</w> <w n="80.2">nous</w> <w n="80.3">roule</w> <w n="80.4">au</w> <w n="80.5">néant</w> <w n="80.6">moins</w> <w n="80.7">noir</w> <w n="80.8">encor</w> <w n="80.9">que</w> <w n="80.10">lui</w>.</l>
				</lg>
				<lg n="21">
					<l n="81" num="21.1"><w n="81.1">Et</w> <w n="81.2">toi</w>, <w n="81.3">dont</w> <w n="81.4">la</w> <w n="81.5">beauté</w> <w n="81.6">ravissait</w> <w n="81.7">les</w> <w n="81.8">aurores</w>,</l>
					<l n="82" num="21.2"><w n="82.1">Fille</w> <w n="82.2">de</w> <w n="82.3">la</w> <w n="82.4">lumière</w>, <w n="82.5">amante</w> <w n="82.6">des</w> <w n="82.7">grandeurs</w>,</l>
					<l n="83" num="21.3"><w n="83.1">Dont</w> <w n="83.2">les</w> <w n="83.3">hautes</w> <w n="83.4">forêts</w> <w n="83.5">vibraient</w>, <w n="83.6">manteaux</w> <w n="83.7">sonores</w>,</l>
					<l n="84" num="21.4"><w n="84.1">Et</w> <w n="84.2">parfumaient</w> <w n="84.3">le</w> <w n="84.4">ciel</w> <w n="84.5">de</w> <w n="84.6">leurs</w> <w n="84.7">vertes</w> <w n="84.8">splendeurs</w> ;</l>
				</lg>
				<lg n="22">
					<l n="85" num="22.1"><w n="85.1">Terre</w>, <w n="85.2">toi</w>-<w n="85.3">même</w> <w n="85.4">au</w> <w n="85.5">bout</w> <w n="85.6">du</w> <w n="85.7">destin</w> <w n="85.8">qui</w> <w n="85.9">nous</w> <w n="85.10">lie</w>,</l>
					<l n="86" num="22.2"><w n="86.1">Comme</w> <w n="86.2">un</w> <w n="86.3">crâne</w> <w n="86.4">vidé</w>, <w n="86.5">nue</w>, <w n="86.6">horrible</w> <w n="86.7">et</w> <w n="86.8">sans</w> <w n="86.9">voix</w>,</l>
					<l n="87" num="22.3"><w n="87.1">Retourne</w> <w n="87.2">à</w> <w n="87.3">ton</w> <w n="87.4">soleil</w> ! <w n="87.5">Une</w> <w n="87.6">seconde</w> <w n="87.7">fois</w>,</l>
					<l n="88" num="22.4"><w n="88.1">S</w>’<w n="88.2">il</w> <w n="88.3">brûle</w> <w n="88.4">encor</w>, <w n="88.5">renais</w> <w n="88.6">à</w> <w n="88.7">sa</w> <w n="88.8">flamme</w> <w n="88.9">pâlie</w> !</l>
				</lg>
				<lg n="23">
					<l n="89" num="23.1"><w n="89.1">Mais</w> <w n="89.2">au</w> <w n="89.3">globe</w> <w n="89.4">épuisé</w> <w n="89.5">heurtant</w> <w n="89.6">ton</w> <w n="89.7">globe</w> <w n="89.8">impur</w>,</l>
					<l n="90" num="23.2"><w n="90.1">Puisses</w>-<w n="90.2">tu</w> <w n="90.3">revomir</w> <w n="90.4">nos</w> <w n="90.5">os</w> <w n="90.6">sans</w> <w n="90.7">nombre</w>, <w n="90.8">ô</w> <w n="90.9">terre</w> !</l>
					<l n="91" num="23.3"><w n="91.1">Dans</w> <w n="91.2">le</w> <w n="91.3">vide</w> <w n="91.4">où</w> <w n="91.5">ne</w> <w n="91.6">germe</w> <w n="91.7">aucun</w> <w n="91.8">monde</w> <w n="91.9">futur</w></l>
					<l n="92" num="23.4"><w n="92.1">Tous</w> <w n="92.2">à</w> <w n="92.3">jamais</w> <w n="92.4">lancés</w> <w n="92.5">par</w> <w n="92.6">le</w> <w n="92.7">même</w> <w n="92.8">cratère</w> !</l>
				</lg>
			</div></body></text></TEI>