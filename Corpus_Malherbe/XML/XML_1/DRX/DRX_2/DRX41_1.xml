<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX41">
				<head type="main">LA PRIÈRE D’ADAM</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Songe</w> <w n="1.2">horrible</w> ! — <w n="1.3">la</w> <w n="1.4">foule</w> <w n="1.5">innombrable</w> <w n="1.6">des</w> <w n="1.7">âmes</w></l>
					<l n="2" num="1.2"><w n="2.1">M</w>’<w n="2.2">entourait</w>. <w n="2.3">Immobile</w> <w n="2.4">et</w> <w n="2.5">muet</w>, <w n="2.6">devant</w> <w n="2.7">nous</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Beau</w> <w n="3.2">comme</w> <w n="3.3">un</w> <w n="3.4">dieu</w>, <w n="3.5">mais</w> <w n="3.6">triste</w> <w n="3.7">et</w> <w n="3.8">pliant</w> <w n="3.9">les</w> <w n="3.10">genoux</w>,</l>
					<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">ancêtre</w> <w n="4.3">restait</w> <w n="4.4">loin</w> <w n="4.5">des</w> <w n="4.6">hommes</w> <w n="4.7">et</w> <w n="4.8">des</w> <w n="4.9">femmes</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">le</w> <w n="5.3">rayonnement</w> <w n="5.4">de</w> <w n="5.5">sa</w> <w n="5.6">mâle</w> <w n="5.7">beauté</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Sa</w> <w n="6.2">force</w>, <w n="6.3">son</w> <w n="6.4">orgueil</w>, <w n="6.5">son</w> <w n="6.6">remords</w>, <w n="6.7">tout</w> <w n="6.8">son</w> <w n="6.9">être</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Forme</w> <w n="7.2">du</w> <w n="7.3">premier</w> <w n="7.4">rêve</w> <w n="7.5">où</w> <w n="7.6">s</w>’<w n="7.7">admira</w> <w n="7.8">son</w> <w n="7.9">maître</w>,</l>
					<l n="8" num="2.4"><w n="8.1">S</w>’<w n="8.2">illuminait</w> <w n="8.3">du</w> <w n="8.4">sceau</w> <w n="8.5">de</w> <w n="8.6">la</w> <w n="8.7">virginité</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Tous</w> <w n="9.2">écoutaient</w>, <w n="9.3">penchés</w> <w n="9.4">sur</w> <w n="9.5">les</w> <w n="9.6">espaces</w> <w n="9.7">blêmes</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Monter</w> <w n="10.2">du</w> <w n="10.3">plus</w> <w n="10.4">lointain</w> <w n="10.5">de</w> <w n="10.6">l</w>’<w n="10.7">abîme</w> <w n="10.8">des</w> <w n="10.9">cieux</w></l>
					<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2">inextinguible</w> <w n="11.3">écho</w> <w n="11.4">des</w> <w n="11.5">vivants</w> <w n="11.6">vers</w> <w n="11.7">les</w> <w n="11.8">dieux</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Les</w> <w n="12.2">rires</w> <w n="12.3">fous</w>, <w n="12.4">les</w> <w n="12.5">cris</w> <w n="12.6">de</w> <w n="12.7">rage</w> <w n="12.8">et</w> <w n="12.9">les</w> <w n="12.10">blasphèmes</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">plus</w> <w n="13.3">triste</w> <w n="13.4">toujours</w>, <w n="13.5">Adam</w>, <w n="13.6">seul</w>, <w n="13.7">prosterné</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Priait</w> ; <w n="14.2">et</w> <w n="14.3">sa</w> <w n="14.4">poitrine</w> <w n="14.5">était</w> <w n="14.6">rougie</w> <w n="14.7">encore</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Chaque</w> <w n="15.2">fois</w> <w n="15.3">qu</w>’<w n="15.4">éclatait</w> <w n="15.5">dans</w> <w n="15.6">la</w> <w n="15.7">brume</w> <w n="15.8">sonore</w></l>
					<l n="16" num="4.4"><w n="16.1">Ces</w> <w n="16.2">mots</w> <w n="16.3">sans</w> <w n="16.4">trêve</w> : « <w n="16.5">Adam</w>, <w n="16.6">un</w> <w n="16.7">nouvel</w> <w n="16.8">homme</w> <w n="16.9">est</w> <w n="16.10">né</w> ! »</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">— « <w n="17.1">Seigneur</w> ! <w n="17.2">Murmurait</w>-<w n="17.3">il</w>, <w n="17.4">qu</w>’<w n="17.5">il</w> <w n="17.6">est</w> <w n="17.7">long</w>, <w n="17.8">ce</w> <w n="17.9">supplice</w> !</l>
					<l n="18" num="5.2"><w n="18.1">Mes</w> <w n="18.2">fils</w> <w n="18.3">ont</w> <w n="18.4">bien</w> <w n="18.5">assez</w> <w n="18.6">pullulé</w> <w n="18.7">sous</w> <w n="18.8">ta</w> <w n="18.9">loi</w>.</l>
					<l n="19" num="5.3"><w n="19.1">N</w>’<w n="19.2">entendrai</w>-<w n="19.3">je</w> <w n="19.4">jamais</w> <w n="19.5">la</w> <w n="19.6">nuit</w> <w n="19.7">crier</w> <w n="19.8">vers</w> <w n="19.9">moi</w> :</l>
					<l n="20" num="5.4">« <w n="20.1">Le</w> <w n="20.2">dernier</w> <w n="20.3">homme</w> <w n="20.4">est</w> <w n="20.5">mort</w> ! <w n="20.6">Et</w> <w n="20.7">que</w> <w n="20.8">tout</w> <w n="20.9">s</w>’<w n="20.10">accomplisse</w> ! »</l>
				</lg>
			</div></body></text></TEI>