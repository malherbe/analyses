<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX37">
				<head type="main">SOIR D’OCTOBRE</head>
				<opener>
					<salute>A Catulle Mendès.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">long</w> <w n="1.3">frisson</w> <w n="1.4">descend</w> <w n="1.5">des</w> <w n="1.6">coteaux</w> <w n="1.7">aux</w> <w n="1.8">vallées</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Des</w> <w n="2.2">coteaux</w> <w n="2.3">et</w> <w n="2.4">des</w> <w n="2.5">bois</w>, <w n="2.6">dans</w> <w n="2.7">la</w> <w n="2.8">plaine</w> <w n="2.9">et</w> <w n="2.10">les</w> <w n="2.11">champs</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">frisson</w> <w n="3.3">de</w> <w n="3.4">la</w> <w n="3.5">nuit</w> <w n="3.6">passe</w> <w n="3.7">vers</w> <w n="3.8">les</w> <w n="3.9">allées</w>.</l>
					<l n="4" num="1.4">— <w n="4.1">Oh</w> ! <w n="4.2">L</w>’<w n="4.3">angelus</w> <w n="4.4">du</w> <w n="4.5">soir</w> <w n="4.6">dans</w> <w n="4.7">les</w> <w n="4.8">soleils</w> <w n="4.9">couchants</w> ! —</l>
					<l n="5" num="1.5"><w n="5.1">Sous</w> <w n="5.2">une</w> <w n="5.3">haleine</w> <w n="5.4">froide</w> <w n="5.5">au</w> <w n="5.6">loin</w> <w n="5.7">meurent</w> <w n="5.8">les</w> <w n="5.9">chants</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">rires</w> <w n="6.3">et</w> <w n="6.4">les</w> <w n="6.5">chants</w> <w n="6.6">dans</w> <w n="6.7">les</w> <w n="6.8">brumes</w> <w n="6.9">épaisses</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Dans</w> <w n="7.2">la</w> <w n="7.3">brume</w> <w n="7.4">qui</w> <w n="7.5">monte</w> <w n="7.6">ondule</w> <w n="7.7">un</w> <w n="7.8">souffle</w> <w n="7.9">lent</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Un</w> <w n="8.2">souffle</w> <w n="8.3">lent</w> <w n="8.4">répand</w> <w n="8.5">ses</w> <w n="8.6">dernières</w> <w n="8.7">caresses</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Sa</w> <w n="9.2">caresse</w> <w n="9.3">attristée</w> <w n="9.4">au</w> <w n="9.5">fond</w> <w n="9.6">du</w> <w n="9.7">bois</w> <w n="9.8">tremblant</w> ;</l>
					<l n="10" num="1.10"><w n="10.1">Les</w> <w n="10.2">bois</w> <w n="10.3">tremblent</w> ; <w n="10.4">la</w> <w n="10.5">feuille</w> <w n="10.6">en</w> <w n="10.7">flocon</w> <w n="10.8">sec</w> <w n="10.9">tournoie</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Tournoie</w> <w n="11.2">et</w> <w n="11.3">tombe</w> <w n="11.4">au</w> <w n="11.5">bord</w> <w n="11.6">des</w> <w n="11.7">sentiers</w> <w n="11.8">désertés</w>.</l>
					<l n="12" num="1.12"><w n="12.1">Sur</w> <w n="12.2">la</w> <w n="12.3">route</w> <w n="12.4">déserte</w> <w n="12.5">un</w> <w n="12.6">brouillard</w> <w n="12.7">qui</w> <w n="12.8">la</w> <w n="12.9">noie</w>,</l>
					<l n="13" num="1.13"><w n="13.1">Un</w> <w n="13.2">brouillard</w> <w n="13.3">jaune</w> <w n="13.4">étend</w> <w n="13.5">ses</w> <w n="13.6">blafardes</w> <w n="13.7">clartés</w> ;</l>
					<l n="14" num="1.14"><w n="14.1">Vers</w> <w n="14.2">l</w>’<w n="14.3">occident</w> <w n="14.4">blafard</w> <w n="14.5">traîne</w> <w n="14.6">une</w> <w n="14.7">rose</w> <w n="14.8">trace</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">les</w> <w n="15.3">bleus</w> <w n="15.4">horizons</w> <w n="15.5">roulent</w> <w n="15.6">comme</w> <w n="15.7">des</w> <w n="15.8">flots</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Roulent</w> <w n="16.2">comme</w> <w n="16.3">une</w> <w n="16.4">mer</w> <w n="16.5">dont</w> <w n="16.6">le</w> <w n="16.7">flot</w> <w n="16.8">nous</w> <w n="16.9">embrasse</w>,</l>
					<l n="17" num="1.17"><w n="17.1">Nous</w> <w n="17.2">enlace</w>, <w n="17.3">et</w> <w n="17.4">remplit</w> <w n="17.5">la</w> <w n="17.6">gorge</w> <w n="17.7">de</w> <w n="17.8">sanglots</w>.</l>
					<l n="18" num="1.18"><w n="18.1">Plein</w> <w n="18.2">du</w> <w n="18.3">pressentiment</w> <w n="18.4">des</w> <w n="18.5">saisons</w> <w n="18.6">pluviales</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Le</w> <w n="19.2">premier</w> <w n="19.3">vent</w> <w n="19.4">d</w>’<w n="19.5">octobre</w> <w n="19.6">épanche</w> <w n="19.7">ses</w> <w n="19.8">adieux</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Ses</w> <w n="20.2">adieux</w> <w n="20.3">frémissants</w> <w n="20.4">sous</w> <w n="20.5">les</w> <w n="20.6">feuillages</w> <w n="20.7">pâles</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Nostalgiques</w> <w n="21.2">enfants</w> <w n="21.3">des</w> <w n="21.4">soleils</w> <w n="21.5">radieux</w>.</l>
					<l n="22" num="1.22"><w n="22.1">Les</w> <w n="22.2">jours</w> <w n="22.3">frileux</w> <w n="22.4">et</w> <w n="22.5">courts</w> <w n="22.6">arrivent</w>. <w n="22.7">C</w>’<w n="22.8">est</w> <w n="22.9">l</w>’<w n="22.10">automne</w>.</l>
					<l n="23" num="1.23">— <w n="23.1">Comme</w> <w n="23.2">elle</w> <w n="23.3">vibre</w> <w n="23.4">en</w> <w n="23.5">nous</w>, <w n="23.6">la</w> <w n="23.7">cloche</w> <w n="23.8">qui</w> <w n="23.9">bourdonne</w> ! —</l>
					<l n="24" num="1.24"><w n="24.1">L</w>’<w n="24.2">automne</w>, <w n="24.3">avec</w> <w n="24.4">la</w> <w n="24.5">pluie</w> <w n="24.6">et</w> <w n="24.7">les</w> <w n="24.8">neiges</w>, <w n="24.9">demain</w></l>
					<l n="25" num="1.25"><w n="25.1">Versera</w> <w n="25.2">les</w> <w n="25.3">regrets</w> <w n="25.4">et</w> <w n="25.5">l</w>’<w n="25.6">ennui</w> <w n="25.7">monotone</w> ;</l>
					<l n="26" num="1.26"><w n="26.1">Le</w> <w n="26.2">monotone</w> <w n="26.3">ennui</w> <w n="26.4">de</w> <w n="26.5">vivre</w> <w n="26.6">est</w> <w n="26.7">en</w> <w n="26.8">chemin</w> !</l>
					<l n="27" num="1.27"><w n="27.1">Plus</w> <w n="27.2">de</w> <w n="27.3">joyeux</w> <w n="27.4">appels</w> <w n="27.5">sous</w> <w n="27.6">les</w> <w n="27.7">voûtes</w> <w n="27.8">ombreuses</w> ;</l>
					<l n="28" num="1.28"><w n="28.1">Plus</w> <w n="28.2">d</w>’<w n="28.3">hymnes</w> <w n="28.4">à</w> <w n="28.5">l</w>’<w n="28.6">aurore</w>, <w n="28.7">ou</w> <w n="28.8">de</w> <w n="28.9">voix</w> <w n="28.10">dans</w> <w n="28.11">le</w> <w n="28.12">soir</w></l>
					<l n="29" num="1.29"><w n="29.1">Peuplant</w> <w n="29.2">l</w>’<w n="29.3">air</w> <w n="29.4">embaumé</w> <w n="29.5">de</w> <w n="29.6">chansons</w> <w n="29.7">amoureuses</w> !</l>
					<l n="30" num="1.30"><w n="30.1">Voici</w> <w n="30.2">l</w>’<w n="30.3">automne</w> ! <w n="30.4">Adieu</w>, <w n="30.5">le</w> <w n="30.6">splendide</w> <w n="30.7">encensoir</w></l>
					<l n="31" num="1.31"><w n="31.1">Des</w> <w n="31.2">prés</w> <w n="31.3">en</w> <w n="31.4">fleurs</w> <w n="31.5">fumant</w> <w n="31.6">dans</w> <w n="31.7">le</w> <w n="31.8">chaud</w> <w n="31.9">crépuscule</w>.</l>
					<l n="32" num="1.32"><w n="32.1">Dans</w> <w n="32.2">l</w>’<w n="32.3">or</w> <w n="32.4">du</w> <w n="32.5">crépuscule</w>, <w n="32.6">adieu</w>, <w n="32.7">les</w> <w n="32.8">yeux</w> <w n="32.9">baissés</w>,</l>
					<l n="33" num="1.33"><w n="33.1">Les</w> <w n="33.2">couples</w> <w n="33.3">chuchotants</w> <w n="33.4">dont</w> <w n="33.5">le</w> <w n="33.6">cœur</w> <w n="33.7">bat</w> <w n="33.8">et</w> <w n="33.9">brûle</w>,</l>
					<l n="34" num="1.34"><w n="34.1">Qui</w> <w n="34.2">vont</w> <w n="34.3">la</w> <w n="34.4">joue</w> <w n="34.5">en</w> <w n="34.6">feu</w>, <w n="34.7">les</w> <w n="34.8">bras</w> <w n="34.9">entrelacés</w>,</l>
					<l n="35" num="1.35"><w n="35.1">Les</w> <w n="35.2">bras</w> <w n="35.3">entrelacés</w> <w n="35.4">quand</w> <w n="35.5">le</w> <w n="35.6">soleil</w> <w n="35.7">décline</w>.</l>
					<l n="36" num="1.36">— <w n="36.1">La</w> <w n="36.2">cloche</w> <w n="36.3">lentement</w> <w n="36.4">tinte</w> <w n="36.5">sur</w> <w n="36.6">la</w> <w n="36.7">colline</w>. —</l>
					<l n="37" num="1.37"><w n="37.1">Adieu</w>, <w n="37.2">la</w> <w n="37.3">ronde</w> <w n="37.4">ardente</w>, <w n="37.5">et</w> <w n="37.6">les</w> <w n="37.7">rires</w> <w n="37.8">d</w>’<w n="37.9">enfants</w>,</l>
					<l n="38" num="1.38"><w n="38.1">Et</w> <w n="38.2">les</w> <w n="38.3">vierges</w>, <w n="38.4">le</w> <w n="38.5">long</w> <w n="38.6">du</w> <w n="38.7">sentier</w> <w n="38.8">qui</w> <w n="38.9">chemine</w>,</l>
					<l n="39" num="1.39"><w n="39.1">Rêvant</w> <w n="39.2">d</w>’<w n="39.3">amour</w> <w n="39.4">tout</w> <w n="39.5">bas</w> <w n="39.6">sous</w> <w n="39.7">les</w> <w n="39.8">cieux</w> <w n="39.9">étouffants</w> !</l>
					<l n="40" num="1.40">— <w n="40.1">Âme</w> <w n="40.2">de</w> <w n="40.3">l</w>’<w n="40.4">homme</w>, <w n="40.5">écoute</w> <w n="40.6">en</w> <w n="40.7">frémissant</w> <w n="40.8">comme</w> <w n="40.9">elle</w></l>
					<l n="41" num="1.41"><w n="41.1">L</w>’<w n="41.2">âme</w> <w n="41.3">immense</w> <w n="41.4">du</w> <w n="41.5">monde</w> <w n="41.6">autour</w> <w n="41.7">de</w> <w n="41.8">toi</w> <w n="41.9">frémir</w> !</l>
					<l n="42" num="1.42"><w n="42.1">Ensemble</w> <w n="42.2">frémissez</w> <w n="42.3">d</w>’<w n="42.4">une</w> <w n="42.5">douleur</w> <w n="42.6">jumelle</w>.</l>
					<l n="43" num="1.43"><w n="43.1">Vois</w> <w n="43.2">les</w> <w n="43.3">pâles</w> <w n="43.4">reflets</w> <w n="43.5">des</w> <w n="43.6">bois</w> <w n="43.7">qui</w> <w n="43.8">vont</w> <w n="43.9">jaunir</w> ;</l>
					<l n="44" num="1.44"><w n="44.1">Savoure</w> <w n="44.2">leur</w> <w n="44.3">tristesse</w> <w n="44.4">et</w> <w n="44.5">leurs</w> <w n="44.6">senteurs</w> <w n="44.7">dernières</w>,</l>
					<l n="45" num="1.45"><w n="45.1">Les</w> <w n="45.2">dernières</w> <w n="45.3">senteurs</w> <w n="45.4">de</w> <w n="45.5">l</w>’<w n="45.6">été</w> <w n="45.7">disparu</w> ;</l>
					<l n="46" num="1.46">— <w n="46.1">Et</w> <w n="46.2">le</w> <w n="46.3">son</w> <w n="46.4">de</w> <w n="46.5">la</w> <w n="46.6">cloche</w> <w n="46.7">au</w> <w n="46.8">milieu</w> <w n="46.9">des</w> <w n="46.10">chaumières</w> ! —</l>
					<l n="47" num="1.47"><w n="47.1">L</w>’<w n="47.2">été</w> <w n="47.3">meurt</w> ; <w n="47.4">son</w> <w n="47.5">soupir</w> <w n="47.6">glisse</w> <w n="47.7">dans</w> <w n="47.8">les</w> <w n="47.9">lisières</w>.</l>
					<l n="48" num="1.48"><w n="48.1">Sous</w> <w n="48.2">le</w> <w n="48.3">dôme</w> <w n="48.4">éclairci</w> <w n="48.5">des</w> <w n="48.6">chênes</w> <w n="48.7">a</w> <w n="48.8">couru</w></l>
					<l n="49" num="1.49"><w n="49.1">Leur</w> <w n="49.2">râle</w> <w n="49.3">entre</w>-<w n="49.4">choquant</w> <w n="49.5">les</w> <w n="49.6">ramures</w> <w n="49.7">livides</w>.</l>
					<l n="50" num="1.50"><w n="50.1">Elle</w> <w n="50.2">est</w> <w n="50.3">flétrie</w> <w n="50.4">aussi</w>, <w n="50.5">ta</w> <w n="50.6">riche</w> <w n="50.7">floraison</w>,</l>
					<l n="51" num="1.51"><w n="51.1">L</w>’<w n="51.2">orgueil</w> <w n="51.3">de</w> <w n="51.4">ta</w> <w n="51.5">jeunesse</w> ! <w n="51.6">Et</w> <w n="51.7">bien</w> <w n="51.8">des</w> <w n="51.9">nids</w> <w n="51.10">sont</w> <w n="51.11">vides</w>,</l>
					<l n="52" num="1.52"><w n="52.1">Âme</w> <w n="52.2">humaine</w>, <w n="52.3">où</w> <w n="52.4">chantaient</w> <w n="52.5">dans</w> <w n="52.6">ta</w> <w n="52.7">jeune</w> <w n="52.8">saison</w></l>
					<l n="53" num="1.53"><w n="53.1">Les</w> <w n="53.2">désirs</w> <w n="53.3">gazouillants</w> <w n="53.4">de</w> <w n="53.5">tes</w> <w n="53.6">aurores</w> <w n="53.7">brèves</w>.</l>
					<l n="54" num="1.54"><w n="54.1">Âme</w> <w n="54.2">crédule</w> ! <w n="54.3">écoute</w> <w n="54.4">en</w> <w n="54.5">toi</w> <w n="54.6">frémir</w> <w n="54.7">encor</w>,</l>
					<l n="55" num="1.55"><w n="55.1">Avec</w> <w n="55.2">ces</w> <w n="55.3">tintements</w> <w n="55.4">douloureux</w> <w n="55.5">et</w> <w n="55.6">sans</w> <w n="55.7">trêves</w>,</l>
					<l n="56" num="1.56"><w n="56.1">Frémir</w> <w n="56.2">depuis</w> <w n="56.3">longtemps</w> <w n="56.4">l</w>’<w n="56.5">automne</w> <w n="56.6">dans</w> <w n="56.7">tes</w> <w n="56.8">rêves</w>,</l>
					<l n="57" num="1.57"><w n="57.1">Dans</w> <w n="57.2">tes</w> <w n="57.3">rêves</w> <w n="57.4">tombés</w> <w n="57.5">dès</w> <w n="57.6">leur</w> <w n="57.7">premier</w> <w n="57.8">essor</w>.</l>
					<l n="58" num="1.58"><w n="58.1">Tandis</w> <w n="58.2">que</w> <w n="58.3">l</w>’<w n="58.4">homme</w> <w n="58.5">va</w>, <w n="58.6">le</w> <w n="58.7">front</w> <w n="58.8">bas</w>, <w n="58.9">toi</w>, <w n="58.10">son</w> <w n="58.11">âme</w>,</l>
					<l n="59" num="1.59"><w n="59.1">Écoute</w> <w n="59.2">le</w> <w n="59.3">passé</w> <w n="59.4">qui</w> <w n="59.5">gémit</w> <w n="59.6">dans</w> <w n="59.7">les</w> <w n="59.8">bois</w> !</l>
					<l n="60" num="1.60"><w n="60.1">Écoute</w>, <w n="60.2">écoute</w> <w n="60.3">en</w> <w n="60.4">toi</w>, <w n="60.5">sous</w> <w n="60.6">leur</w> <w n="60.7">cendre</w> <w n="60.8">et</w> <w n="60.9">sans</w> <w n="60.10">flamme</w>,</l>
					<l n="61" num="1.61"><w n="61.1">Tous</w> <w n="61.2">tes</w> <w n="61.3">chers</w> <w n="61.4">souvenirs</w> <w n="61.5">tressaillir</w> <w n="61.6">à</w> <w n="61.7">la</w> <w n="61.8">fois</w></l>
					<l n="62" num="1.62"><w n="62.1">Avec</w> <w n="62.2">le</w> <w n="62.3">glas</w> <w n="62.4">mourant</w> <w n="62.5">de</w> <w n="62.6">la</w> <w n="62.7">cloche</w> <w n="62.8">lointaine</w> !</l>
					<l n="63" num="1.63"><w n="63.1">Une</w> <w n="63.2">autre</w> <w n="63.3">maintenant</w> <w n="63.4">lui</w> <w n="63.5">répond</w> <w n="63.6">à</w> <w n="63.7">voix</w> <w n="63.8">pleine</w>.</l>
					<l n="64" num="1.64"><w n="64.1">Écoute</w> <w n="64.2">à</w> <w n="64.3">travers</w> <w n="64.4">l</w>’<w n="64.5">ombre</w>, <w n="64.6">entends</w> <w n="64.7">avec</w> <w n="64.8">langueur</w></l>
					<l n="65" num="1.65"><w n="65.1">Ces</w> <w n="65.2">cloches</w> <w n="65.3">tristement</w> <w n="65.4">qui</w> <w n="65.5">sonnent</w> <w n="65.6">dans</w> <w n="65.7">la</w> <w n="65.8">plaine</w>,</l>
					<l n="66" num="1.66"><w n="66.1">Qui</w> <w n="66.2">vibrent</w> <w n="66.3">tristement</w>, <w n="66.4">longuement</w>, <w n="66.5">dans</w> <w n="66.6">ton</w> <w n="66.7">cœur</w> !</l>
				</lg>
			</div></body></text></TEI>