<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX29">
				<head type="main">CE SOIR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">à</w> <w n="1.3">travers</w> <w n="1.4">un</w> <w n="1.5">triple</w> <w n="1.6">et</w> <w n="1.7">magique</w> <w n="1.8">bandeau</w>,</l>
					<l n="2" num="1.2">— <w n="2.1">Ô</w> <w n="2.2">nuit</w> ! <w n="2.3">ô</w> <w n="2.4">solitude</w> ! <w n="2.5">ô</w> <w n="2.6">silence</w> ! — <w n="2.7">mon</w> <w n="2.8">âme</w></l>
					<l n="3" num="1.3"><w n="3.1">À</w> <w n="3.2">travers</w> <w n="3.3">vous</w>, <w n="3.4">ce</w> <w n="3.5">soir</w>, <w n="3.6">près</w> <w n="3.7">du</w> <w n="3.8">foyer</w> <w n="3.9">sans</w> <w n="3.10">flamme</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Regarde</w> <w n="4.2">par</w> <w n="4.3">delà</w> <w n="4.4">les</w> <w n="4.5">portes</w> <w n="4.6">du</w> <w n="4.7">tombeau</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ce</w> <w n="5.2">soir</w>, <w n="5.3">plein</w> <w n="5.4">de</w> <w n="5.5">l</w>’<w n="5.6">horreur</w> <w n="5.7">d</w>’<w n="5.8">un</w> <w n="5.9">vaincu</w> <w n="5.10">qu</w>’<w n="5.11">on</w> <w n="5.12">assaille</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">sens</w> <w n="6.3">les</w> <w n="6.4">morts</w> <w n="6.5">chéris</w> <w n="6.6">surgir</w> <w n="6.7">autour</w> <w n="6.8">de</w> <w n="6.9">moi</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Leurs</w> <w n="7.2">yeux</w>, <w n="7.3">comme</w> <w n="7.4">pour</w> <w n="7.5">lire</w> <w n="7.6">au</w> <w n="7.7">fond</w> <w n="7.8">de</w> <w n="7.9">mon</w> <w n="7.10">effroi</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Luisent</w> <w n="8.2">distinctement</w> <w n="8.3">dans</w> <w n="8.4">l</w>’<w n="8.5">ombre</w> <w n="8.6">qui</w> <w n="8.7">tressaille</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Derrière</w> <w n="9.2">moi</w>, <w n="9.3">ce</w> <w n="9.4">soir</w>, <w n="9.5">quelqu</w>’<w n="9.6">un</w> <w n="9.7">est</w> <w n="9.8">là</w>, <w n="9.9">tout</w> <w n="9.10">près</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Je</w> <w n="10.2">sais</w> <w n="10.3">qu</w>’<w n="10.4">il</w> <w n="10.5">me</w> <w n="10.6">regarde</w>, <w n="10.7">et</w> <w n="10.8">je</w> <w n="10.9">sens</w> <w n="10.10">qu</w>’<w n="10.11">il</w> <w n="10.12">me</w> <w n="10.13">frôle</w>.</l>
					<l n="11" num="3.3"><w n="11.1">Quelle</w> <w n="11.2">angoisse</w> ! <w n="11.3">Il</w> <w n="11.4">est</w> <w n="11.5">là</w>, <w n="11.6">derrière</w> <w n="11.7">mon</w> <w n="11.8">épaule</w>.</l>
					<l n="12" num="3.4"><w n="12.1">Si</w> <w n="12.2">je</w> <w n="12.3">me</w> <w n="12.4">retournais</w>, <w n="12.5">à</w> <w n="12.6">coup</w> <w n="12.7">sûr</w> <w n="12.8">je</w> <w n="12.9">mourrais</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Du</w> <w n="13.2">fond</w> <w n="13.3">d</w>’<w n="13.4">une</w> <w n="13.5">autre</w> <w n="13.6">vie</w>, <w n="13.7">une</w> <w n="13.8">voix</w> <w n="13.9">très</w> <w n="13.10">lointaine</w></l>
					<l n="14" num="4.2"><w n="14.1">Ce</w> <w n="14.2">soir</w> <w n="14.3">a</w> <w n="14.4">dit</w> <w n="14.5">mon</w> <w n="14.6">nom</w>, <w n="14.7">ô</w> <w n="14.8">terreur</w> ! <w n="14.9">Et</w> <w n="14.10">ce</w> <w n="14.11">bruit</w></l>
					<l n="15" num="4.3"><w n="15.1">Que</w> <w n="15.2">j</w>’<w n="15.3">écoute</w>-<w n="15.4">ô</w> <w n="15.5">silence</w> ! <w n="15.6">ô</w> <w n="15.7">solitude</w> ! <w n="15.8">ô</w> <w n="15.9">nuit</w> ! —</l>
					<l n="16" num="4.4"><w n="16.1">Semble</w> <w n="16.2">être</w> <w n="16.3">né</w> <w n="16.4">jadis</w>, <w n="16.5">avec</w> <w n="16.6">la</w> <w n="16.7">race</w> <w n="16.8">humaine</w> !</l>
				</lg>
			</div></body></text></TEI>