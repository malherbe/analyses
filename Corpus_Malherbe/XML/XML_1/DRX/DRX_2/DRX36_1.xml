<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX36">
				<head type="main">L’ORGUEIL</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Monts</w> <w n="1.2">superbes</w>, <w n="1.3">dressez</w> <w n="1.4">vos</w> <w n="1.5">pics</w> <w n="1.6">inaccessibles</w></l>
					<l n="2" num="1.2"><w n="2.1">Sur</w> <w n="2.2">le</w> <w n="2.3">cirque</w> <w n="2.4">brumeux</w> <w n="2.5">où</w> <w n="2.6">plongent</w> <w n="2.7">vos</w> <w n="2.8">flancs</w> <w n="2.9">verts</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Métaux</w>, <w n="3.2">dans</w> <w n="3.3">le</w> <w n="3.4">regret</w> <w n="3.5">des</w> <w n="3.6">chaleurs</w> <w n="3.7">impossibles</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Durcissez</w>-<w n="4.2">vous</w> <w n="4.3">au</w> <w n="4.4">fond</w> <w n="4.5">des</w> <w n="4.6">volcans</w> <w n="4.7">entr</w>’<w n="4.8">ouverts</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1">— <w n="5.1">Hérisse</w>, <w n="5.2">amer</w> <w n="5.3">orgueil</w>, <w n="5.4">ta</w> <w n="5.5">muraille</w> <w n="5.6">rigide</w></l>
					<l n="6" num="2.2"><w n="6.1">Sur</w> <w n="6.2">le</w> <w n="6.3">cœur</w> <w n="6.4">que</w> <w n="6.5">des</w> <w n="6.6">yeux</w> <w n="6.7">de</w> <w n="6.8">femme</w> <w n="6.9">ont</w> <w n="6.10">perforé</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Désirs</w> <w n="7.2">inassouvis</w>, <w n="7.3">sous</w> <w n="7.4">cette</w> <w n="7.5">fière</w> <w n="7.6">égide</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Mornes</w>, <w n="8.2">endormez</w>-<w n="8.3">vous</w> <w n="8.4">dans</w> <w n="8.5">le</w> <w n="8.6">sommeil</w> <w n="8.7">sacré</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1">— <w n="9.1">L</w>’<w n="9.2">antique</w> <w n="9.3">orage</w> <w n="9.4">habite</w>, <w n="9.5">ô</w> <w n="9.6">monts</w> ! <w n="9.7">Dans</w> <w n="9.8">vos</w> <w n="9.9">abîmes</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">prolonge</w> <w n="10.3">sans</w> <w n="10.4">fin</w> <w n="10.5">sous</w> <w n="10.6">les</w> <w n="10.7">cèdres</w> <w n="10.8">vibrants</w></l>
					<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">sonores</w> <w n="11.3">échos</w> <w n="11.4">de</w> <w n="11.5">ses</w> <w n="11.6">éclats</w> <w n="11.7">sublimes</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">des</w> <w n="12.3">troncs</w> <w n="12.4">fracassés</w> <w n="12.5">qu</w>’<w n="12.6">emportent</w> <w n="12.7">les</w> <w n="12.8">torrents</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1">— <w n="13.1">Orgueil</w>, <w n="13.2">derrière</w> <w n="13.3">toi</w> <w n="13.4">l</w>’<w n="13.5">amour</w> <w n="13.6">est</w> <w n="13.7">là</w>, <w n="13.8">qui</w> <w n="13.9">gronde</w></l>
					<l n="14" num="4.2"><w n="14.1">Toujours</w>, <w n="14.2">et</w> <w n="14.3">fait</w> <w n="14.4">crier</w> <w n="14.5">l</w>’<w n="14.6">ombre</w> <w n="14.7">des</w> <w n="14.8">rêves</w> <w n="14.9">morts</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Aux</w> <w n="15.2">lugubres</w> <w n="15.3">appels</w> <w n="15.4">de</w> <w n="15.5">l</w>’<w n="15.6">angoisse</w> <w n="15.7">inféconde</w></l>
					<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">des</w> <w n="16.3">vieux</w> <w n="16.4">désespoirs</w> <w n="16.5">perdus</w> <w n="16.6">dans</w> <w n="16.7">les</w> <w n="16.8">remords</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1">— <w n="17.1">Sur</w> <w n="17.2">les</w> <w n="17.3">ébranlements</w>, <w n="17.4">les</w> <w n="17.5">éclairs</w>, <w n="17.6">les</w> <w n="17.7">écumes</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Pics</w> <w n="18.2">songeurs</w>, <w n="18.3">vous</w> <w n="18.4">gardez</w> <w n="18.5">votre</w> <w n="18.6">sérénité</w>.</l>
					<l n="19" num="5.3"><w n="19.1">Du</w> <w n="19.2">côté</w> <w n="19.3">de</w> <w n="19.4">la</w> <w n="19.5">plaine</w>, <w n="19.6">ô</w> <w n="19.7">monts</w> ! <w n="19.8">Vierges</w> <w n="19.9">de</w> <w n="19.10">brumes</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Vos</w> <w n="20.2">sommets</w> <w n="20.3">radieux</w> <w n="20.4">baignent</w> <w n="20.5">dans</w> <w n="20.6">la</w> <w n="20.7">clarté</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">— <w n="21.1">Sur</w> <w n="21.2">les</w> <w n="21.3">déchirements</w>, <w n="21.4">les</w> <w n="21.5">sanglots</w>, <w n="21.6">les</w> <w n="21.7">rancunes</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Fermez</w>, <w n="22.2">orgueil</w>, <w n="22.3">fierté</w>, <w n="22.4">votre</w> <w n="22.5">ceinture</w> <w n="22.6">d</w>’<w n="22.7">or</w>.</l>
					<l n="23" num="6.3"><w n="23.1">Du</w> <w n="23.2">côté</w> <w n="23.3">de</w> <w n="23.4">la</w> <w n="23.5">vie</w> <w n="23.6">aux</w> <w n="23.7">rumeurs</w> <w n="23.8">importunes</w></l>
					<l n="24" num="6.4"><w n="24.1">Reluisez</w> <w n="24.2">au</w> <w n="24.3">soleil</w>, <w n="24.4">et</w> <w n="24.5">souriez</w> <w n="24.6">encor</w> !</l>
				</lg>
			</div></body></text></TEI>