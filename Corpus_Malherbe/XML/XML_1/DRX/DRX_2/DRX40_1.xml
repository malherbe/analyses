<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES LÈVRES CLOSES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1986 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les lèvres closes</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxleslevrescloses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
				<p>Le poème "la Beauté" a été ajouté à partir d’une saisie manuelle</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX40">
				<head type="main">LE RÊVE DE LA MORT</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">ange</w> <w n="1.3">sur</w> <w n="1.4">mon</w> <w n="1.5">front</w> <w n="1.6">déploya</w> <w n="1.7">sa</w> <w n="1.8">grande</w> <w n="1.9">aile</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Une</w> <w n="2.2">ombre</w> <w n="2.3">lentement</w> <w n="2.4">descendit</w> <w n="2.5">vers</w> <w n="2.6">mes</w> <w n="2.7">yeux</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">sur</w> <w n="3.3">chaque</w> <w n="3.4">paupière</w> <w n="3.5">un</w> <w n="3.6">doigt</w> <w n="3.7">impérieux</w></l>
						<l n="4" num="1.4"><w n="4.1">Vint</w> <w n="4.2">alourdir</w> <w n="4.3">la</w> <w n="4.4">nuit</w> <w n="4.5">plus</w> <w n="4.6">épaisse</w> <w n="4.7">autour</w> <w n="4.8">d</w>’<w n="4.9">elle</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Un</w> <w n="5.2">ange</w> <w n="5.3">lentement</w> <w n="5.4">déploya</w> <w n="5.5">sa</w> <w n="5.6">grande</w> <w n="5.7">aile</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">sous</w> <w n="6.3">ses</w> <w n="6.4">doigts</w> <w n="6.5">de</w> <w n="6.6">plomb</w> <w n="6.7">s</w>’<w n="6.8">enfoncèrent</w> <w n="6.9">mes</w> <w n="6.10">yeux</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Puis</w> <w n="7.2">tout</w> <w n="7.3">s</w>’<w n="7.4">évanouit</w>, <w n="7.5">douleur</w>, <w n="7.6">efforts</w>, <w n="7.7">mémoire</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">je</w> <w n="8.3">sentais</w> <w n="8.4">flotter</w> <w n="8.5">ma</w> <w n="8.6">forme</w> <w n="8.7">devant</w> <w n="8.8">moi</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">mes</w> <w n="9.3">pensers</w> <w n="9.4">de</w> <w n="9.5">même</w>, <w n="9.6">ou</w> <w n="9.7">de</w> <w n="9.8">honte</w> <w n="9.9">ou</w> <w n="9.10">de</w> <w n="9.11">gloire</w>,</l>
						<l n="10" num="1.10"><w n="10.1">S</w>’<w n="10.2">échappaient</w> <w n="10.3">de</w> <w n="10.4">mon</w> <w n="10.5">corps</w> <w n="10.6">pêle</w>-<w n="10.7">mêle</w>, <w n="10.8">et</w> <w n="10.9">sans</w> <w n="10.10">loi</w>.</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="11" num="1.1"><w n="11.1">Une</w> <w n="11.2">forme</w> <w n="11.3">flottait</w>, <w n="11.4">qui</w> <w n="11.5">semblait</w> <w n="11.6">mon</w> <w n="11.7">image</w>.</l>
						<l n="12" num="1.2"><w n="12.1">L</w>’<w n="12.2">ai</w>-<w n="12.3">je</w> <w n="12.4">suivie</w> <w n="12.5">une</w> <w n="12.6">heure</w> <w n="12.7">ou</w> <w n="12.8">cent</w> <w n="12.9">ans</w> ? <w n="12.10">Je</w> <w n="12.11">ne</w> <w n="12.12">sais</w>.</l>
						<l n="13" num="1.3"><w n="13.1">Mais</w> <w n="13.2">j</w>’<w n="13.3">ai</w> <w n="13.4">gardé</w> <w n="13.5">l</w>’<w n="13.6">effroi</w> <w n="13.7">des</w> <w n="13.8">lieux</w> <w n="13.9">où</w> <w n="13.10">je</w> <w n="13.11">passais</w>.</l>
						<l n="14" num="1.4"><w n="14.1">La</w> <w n="14.2">sueur</w> <w n="14.3">me</w> <w n="14.4">glaça</w> <w n="14.5">de</w> <w n="14.6">l</w>’<w n="14.7">orteil</w> <w n="14.8">au</w> <w n="14.9">visage</w></l>
						<l n="15" num="1.5"><w n="15.1">Derrière</w> <w n="15.2">cette</w> <w n="15.3">forme</w> <w n="15.4">où</w> <w n="15.5">vivait</w> <w n="15.6">mon</w> <w n="15.7">image</w>.</l>
						<l n="16" num="1.6"><w n="16.1">Pendant</w> <w n="16.2">combien</w> <w n="16.3">de</w> <w n="16.4">jours</w> <w n="16.5">terrestres</w> ? <w n="16.6">Je</w> <w n="16.7">ne</w> <w n="16.8">sais</w>.</l>
						<l n="17" num="1.7"><w n="17.1">Mais</w> <w n="17.2">sous</w> <w n="17.3">des</w> <w n="17.4">horizons</w> <w n="17.5">tout</w> <w n="17.6">d</w>’<w n="17.7">encre</w> <w n="17.8">ou</w> <w n="17.9">tout</w> <w n="17.10">de</w> <w n="17.11">flamme</w>,</l>
						<l n="18" num="1.8"><w n="18.1">Pour</w> <w n="18.2">toujours</w> <w n="18.3">je</w> <w n="18.4">sentais</w> <w n="18.5">quelque</w> <w n="18.6">chose</w> <w n="18.7">en</w> <w n="18.8">mon</w> <w n="18.9">cœur</w></l>
						<l n="19" num="1.9"><w n="19.1">Voler</w> <w n="19.2">vers</w> <w n="19.3">cet</w> <w n="19.4">éclat</w> <w n="19.5">pour</w> <w n="19.6">se</w> <w n="19.7">perdre</w> <w n="19.8">en</w> <w n="19.9">sa</w> <w n="19.10">trame</w>,</l>
						<l n="20" num="1.10"><w n="20.1">Quelque</w> <w n="20.2">chose</w> <w n="20.3">de</w> <w n="20.4">moi</w> <w n="20.5">qui</w> <w n="20.6">faisait</w> <w n="20.7">ma</w> <w n="20.8">vigueur</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="21" num="1.1"><w n="21.1">Et</w> <w n="21.2">voilà</w> <w n="21.3">devant</w> <w n="21.4">nous</w> <w n="21.5">qu</w>’<w n="21.6">une</w> <w n="21.7">forêt</w> <w n="21.8">géante</w></l>
						<l n="22" num="1.2"><w n="22.1">Brusquement</w> <w n="22.2">balança</w> <w n="22.3">dans</w> <w n="22.4">l</w>’<w n="22.5">espace</w> <w n="22.6">embrasé</w></l>
						<l n="23" num="1.3"><w n="23.1">Son</w> <w n="23.2">manteau</w> <w n="23.3">par</w> <w n="23.4">un</w> <w n="23.5">sang</w> <w n="23.6">vif</w> <w n="23.7">et</w> <w n="23.8">tiède</w> <w n="23.9">arrosé</w>.</l>
						<l n="24" num="1.4"><w n="24.1">Comme</w> <w n="24.2">un</w> <w n="24.3">rouge</w> <w n="24.4">flocon</w> <w n="24.5">d</w>’<w n="24.6">une</w> <w n="24.7">neige</w> <w n="24.8">brûlante</w>,</l>
						<l n="25" num="1.5"><w n="25.1">Un</w> <w n="25.2">âpre</w> <w n="25.3">vent</w>, <w n="25.4">du</w> <w n="25.5">haut</w> <w n="25.6">de</w> <w n="25.7">la</w> <w n="25.8">forêt</w> <w n="25.9">géante</w></l>
						<l n="26" num="1.6"><w n="26.1">Jusqu</w>’<w n="26.2">au</w> <w n="26.3">sol</w> <w n="26.4">par</w> <w n="26.5">les</w> <w n="26.6">feux</w> <w n="26.7">du</w> <w n="26.8">soleil</w> <w n="26.9">embrasé</w>,</l>
						<l n="27" num="1.7"><w n="27.1">Secouait</w> <w n="27.2">chaque</w> <w n="27.3">feuille</w> <w n="27.4">à</w> <w n="27.5">travers</w> <w n="27.6">les</w> <w n="27.7">ramures</w>.</l>
						<l n="28" num="1.8"><w n="28.1">Et</w> <w n="28.2">de</w> <w n="28.3">mon</w> <w n="28.4">front</w> <w n="28.5">aussi</w> <w n="28.6">chaque</w> <w n="28.7">rêve</w> <w n="28.8">tombait</w>,</l>
						<l n="29" num="1.9"><w n="29.1">Et</w> <w n="29.2">dans</w> <w n="29.3">mon</w> <w n="29.4">spectre</w>, <w n="29.5">avec</w> <w n="29.6">de</w> <w n="29.7">très</w> <w n="29.8">lointains</w> <w n="29.9">murmures</w>,</l>
						<l n="30" num="1.10"><w n="30.1">Chaque</w> <w n="30.2">rêve</w> <w n="30.3">tombé</w> <w n="30.4">de</w> <w n="30.5">mon</w> <w n="30.6">front</w> <w n="30.7">s</w>’<w n="30.8">absorbait</w>.</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l n="31" num="1.1"><w n="31.1">Sur</w> <w n="31.2">ma</w> <w n="31.3">tête</w> <w n="31.4">sifflaient</w> <w n="31.5">de</w> <w n="31.6">lugubres</w> <w n="31.7">rafales</w> ;</l>
						<l n="32" num="1.2"><w n="32.1">Et</w> <w n="32.2">le</w> <w n="32.3">gémissement</w> <w n="32.4">surhumain</w> <w n="32.5">de</w> <w n="32.6">ce</w> <w n="32.7">bois</w></l>
						<l n="33" num="1.3"><w n="33.1">Semblait</w> <w n="33.2">l</w>’<w n="33.3">appel</w> <w n="33.4">perdu</w> <w n="33.5">de</w> <w n="33.6">millions</w> <w n="33.7">de</w> <w n="33.8">voix</w>.</l>
						<l n="34" num="1.4"><w n="34.1">C</w>’<w n="34.2">était</w> <w n="34.3">le</w> <w n="34.4">long</w> <w n="34.5">sanglot</w> <w n="34.6">des</w> <w n="34.7">morts</w>, <w n="34.8">par</w> <w n="34.9">intervalles</w>,</l>
						<l n="35" num="1.5"><w n="35.1">Qui</w> <w n="35.2">de</w> <w n="35.3">tous</w> <w n="35.4">les</w> <w n="35.5">confins</w> <w n="35.6">passait</w> <w n="35.7">dans</w> <w n="35.8">ces</w> <w n="35.9">rafales</w>.</l>
						<l n="36" num="1.6"><w n="36.1">Un</w> <w n="36.2">lac</w> <w n="36.3">de</w> <w n="36.4">sang</w> <w n="36.5">luisait</w> <w n="36.6">au</w> <w n="36.7">milieu</w> <w n="36.8">de</w> <w n="36.9">ce</w> <w n="36.10">bois</w>,</l>
						<l n="37" num="1.7"><w n="37.1">Épanché</w> <w n="37.2">d</w>’<w n="37.3">un</w> <w n="37.4">soleil</w> <w n="37.5">aux</w> <w n="37.6">ondes</w> <w n="37.7">écarlates</w>.</l>
						<l n="38" num="1.8"><w n="38.1">Et</w> <w n="38.2">mes</w> <w n="38.3">anciens</w> <w n="38.4">désirs</w> <w n="38.5">ruisselaient</w> <w n="38.6">au</w> <w n="38.7">dehors</w> ;</l>
						<l n="39" num="1.9"><w n="39.1">Vers</w> <w n="39.2">mon</w> <w n="39.3">fantôme</w> <w n="39.4">clair</w>, <w n="39.5">avec</w> <w n="39.6">leurs</w> <w n="39.7">tristes</w> <w n="39.8">dates</w>,</l>
						<l n="40" num="1.10"><w n="40.1">Mes</w> <w n="40.2">désirs</w> <w n="40.3">ruisselaient</w> <w n="40.4">et</w> <w n="40.5">désertaient</w> <w n="40.6">mon</w> <w n="40.7">corps</w>.</l>
					</lg>
				</div>
				<div type="section" n="5">
					<head type="number">V</head>
					<lg n="1">
						<l n="41" num="1.1"><w n="41.1">Et</w> <w n="41.2">ce</w> <w n="41.3">lac</w> <w n="41.4">grandit</w>, <w n="41.5">tel</w> <w n="41.6">qu</w>’<w n="41.7">une</w> <w n="41.8">mer</w> <w n="41.9">sans</w> <w n="41.10">rivage</w> ;</l>
						<l n="42" num="1.2"><w n="42.1">Et</w> <w n="42.2">ce</w> <w n="42.3">globe</w> <w n="42.4">penché</w> <w n="42.5">sur</w> <w n="42.6">l</w>’<w n="42.7">horizon</w> <w n="42.8">semblait</w></l>
						<l n="43" num="1.3"><w n="43.1">Un</w> <w n="43.2">cœur</w> <w n="43.3">énorme</w> <w n="43.4">au</w> <w n="43.5">loin</w> <w n="43.6">dardant</w> <w n="43.7">son</w> <w n="43.8">vif</w> <w n="43.9">reflet</w>.</l>
						<l n="44" num="1.4"><w n="44.1">C</w>’<w n="44.2">était</w> <w n="44.3">le</w> <w n="44.4">vaste</w> <w n="44.5">cœur</w> <w n="44.6">des</w> <w n="44.7">peuples</w> <w n="44.8">d</w>’<w n="44.9">âge</w> <w n="44.10">en</w> <w n="44.11">âge</w>,</l>
						<l n="45" num="1.5"><w n="45.1">Saignant</w> <w n="45.2">sur</w> <w n="45.3">cette</w> <w n="45.4">mer</w> <w n="45.5">étrange</w> <w n="45.6">et</w> <w n="45.7">sans</w> <w n="45.8">rivage</w>.</l>
						<l n="46" num="1.6"><w n="46.1">Et</w> <w n="46.2">ce</w> <w n="46.3">qui</w> <w n="46.4">s</w>’<w n="46.5">écoulait</w> <w n="46.6">de</w> <w n="46.7">cet</w> <w n="46.8">astre</w> <w n="46.9">semblait</w></l>
						<l n="47" num="1.7"><w n="47.1">Le</w> <w n="47.2">sang</w>, <w n="47.3">le</w> <w n="47.4">propre</w> <w n="47.5">sang</w> <w n="47.6">de</w> <w n="47.7">l</w>’<w n="47.8">humanité</w> <w n="47.9">morte</w> ;</l>
						<l n="48" num="1.8"><w n="48.1">Et</w> <w n="48.2">nous</w> <w n="48.3">voguions</w> <w n="48.4">tous</w> <w n="48.5">deux</w> <w n="48.6">sur</w> <w n="48.7">ce</w> <w n="48.8">flot</w> <w n="48.9">abhorré</w>.</l>
						<l n="49" num="1.9"><w n="49.1">Mon</w> <w n="49.2">image</w> <w n="49.3">brillait</w> <w n="49.4">plus</w> <w n="49.5">distincte</w> <w n="49.6">et</w> <w n="49.7">plus</w> <w n="49.8">forte</w></l>
						<l n="50" num="1.10"><w n="50.1">Et</w> <w n="50.2">j</w>’<w n="50.3">y</w> <w n="50.4">sentais</w> <w n="50.5">partout</w> <w n="50.6">mon</w> <w n="50.7">esprit</w> <w n="50.8">aspiré</w>.</l>
					</lg>
				</div>
				<div type="section" n="6">
					<head type="number">VI</head>
					<lg n="1">
						<l n="51" num="1.1"><w n="51.1">Sous</w> <w n="51.2">la</w> <w n="51.3">nappe</w> <w n="51.4">sans</w> <w n="51.5">bord</w> <w n="51.6">de</w> <w n="51.7">cette</w> <w n="51.8">pourpre</w> <w n="51.9">horrible</w></l>
						<l n="52" num="1.2"><w n="52.1">Le</w> <w n="52.2">soleil</w> <w n="52.3">s</w>’<w n="52.4">éclipsa</w> <w n="52.5">d</w>’<w n="52.6">un</w> <w n="52.7">coup</w> <w n="52.8">brusque</w>, <w n="52.9">et</w> <w n="52.10">le</w> <w n="52.11">ciel</w></l>
						<l n="53" num="1.3"><w n="53.1">À</w> <w n="53.2">sa</w> <w n="53.3">place</w> <w n="53.4">creusait</w> <w n="53.5">son</w> <w n="53.6">azur</w> <w n="53.7">solennel</w>,</l>
						<l n="54" num="1.4"><w n="54.1">Par</w> <w n="54.2">delà</w> <w n="54.3">le</w> <w n="54.4">regard</w>, <w n="54.5">par</w> <w n="54.6">delà</w> <w n="54.7">l</w>’<w n="54.8">invisible</w>.</l>
						<l n="55" num="1.5"><w n="55.1">Et</w> <w n="55.2">dans</w> <w n="55.3">l</w>’<w n="55.4">éther</w> <w n="55.5">profond</w>, <w n="55.6">sous</w> <w n="55.7">cette</w> <w n="55.8">pourpre</w> <w n="55.9">horrible</w>,</l>
						<l n="56" num="1.6"><w n="56.1">Des</w> <w n="56.2">astres</w> <w n="56.3">inconnus</w> <w n="56.4">s</w>’<w n="56.5">enfonçaient</w> <w n="56.6">dans</w> <w n="56.7">le</w> <w n="56.8">ciel</w>,</l>
						<l n="57" num="1.7"><w n="57.1">Toujours</w>, <w n="57.2">toujours</w> <w n="57.3">plus</w> <w n="57.4">loin</w>, <w n="57.5">au</w> <w n="57.6">fond</w> <w n="57.7">de</w> <w n="57.8">l</w>’<w n="57.9">insondable</w>.</l>
						<l n="58" num="1.8"><w n="58.1">L</w>’<w n="58.2">éclair</w> <w n="58.3">de</w> <w n="58.4">chacun</w> <w n="58.5">d</w>’<w n="58.6">eux</w> <w n="58.7">m</w>’<w n="58.8">emplissait</w> <w n="58.9">comme</w> <w n="58.10">un</w> <w n="58.11">son</w> ;</l>
						<l n="59" num="1.9"><w n="59.1">Et</w> <w n="59.2">tous</w> <w n="59.3">mes</w> <w n="59.4">sens</w>, <w n="59.5">vers</w> <w n="59.6">l</w>’<w n="59.7">être</w> <w n="59.8">à</w> <w n="59.9">mon</w> <w n="59.10">reflet</w> <w n="59.11">semblable</w>,</l>
						<l n="60" num="1.10"><w n="60.1">Abandonnaient</w> <w n="60.2">mon</w> <w n="60.3">corps</w> <w n="60.4">dans</w> <w n="60.5">un</w> <w n="60.6">dernier</w> <w n="60.7">frisson</w>.</l>
					</lg>
				</div>
				<div type="section" n="7">
					<head type="number">VII</head>
					<lg n="1">
						<l n="61" num="1.1"><w n="61.1">Comme</w> <w n="61.2">un</w> <w n="61.3">épais</w> <w n="61.4">rideau</w> <w n="61.5">fait</w> <w n="61.6">d</w>’<w n="61.7">un</w> <w n="61.8">velours</w> <w n="61.9">rigide</w>,</l>
						<l n="62" num="1.2"><w n="62.1">Montait</w> <w n="62.2">derrière</w> <w n="62.3">nous</w> <w n="62.4">l</w>’<w n="62.5">ombre</w> <w n="62.6">du</w> <w n="62.7">dernier</w> <w n="62.8">soir</w> ;</l>
						<l n="63" num="1.3"><w n="63.1">Le</w> <w n="63.2">rouge</w> <w n="63.3">de</w> <w n="63.4">la</w> <w n="63.5">mer</w> <w n="63.6">se</w> <w n="63.7">fondait</w> <w n="63.8">dans</w> <w n="63.9">le</w> <w n="63.10">noir</w> ;</l>
						<l n="64" num="1.4"><w n="64.1">Maintenant</w> <w n="64.2">rien</w> <w n="64.3">de</w> <w n="64.4">moi</w> <w n="64.5">n</w>’<w n="64.6">allait</w> <w n="64.7">plus</w> <w n="64.8">vers</w> <w n="64.9">mon</w> <w n="64.10">guide</w> ;</l>
						<l n="65" num="1.5"><w n="65.1">Et</w> <w n="65.2">sur</w> <w n="65.3">nous</w> <w n="65.4">s</w>’<w n="65.5">élevait</w> <w n="65.6">comme</w> <w n="65.7">un</w> <w n="65.8">rideau</w> <w n="65.9">rigide</w></l>
						<l n="66" num="1.6"><w n="66.1">Une</w> <w n="66.2">éternelle</w> <w n="66.3">nuit</w> <w n="66.4">après</w> <w n="66.5">le</w> <w n="66.6">dernier</w> <w n="66.7">soir</w>.</l>
						<l n="67" num="1.7"><w n="67.1">Et</w> <w n="67.2">là</w>, <w n="67.3">tout</w> <w n="67.4">près</w> <w n="67.5">de</w> <w n="67.6">moi</w>, <w n="67.7">ce</w> <w n="67.8">double</w> <w n="67.9">de</w> <w n="67.10">moi</w>-<w n="67.11">même</w>,</l>
						<l n="68" num="1.8"><w n="68.1">Qui</w> <w n="68.2">me</w> <w n="68.3">regardait</w>, <w n="68.4">plein</w> <w n="68.5">d</w>’<w n="68.6">un</w> <w n="68.7">dédain</w> <w n="68.8">envieux</w>,</l>
						<l n="69" num="1.9"><w n="69.1">C</w>’<w n="69.2">était</w>, <w n="69.3">je</w> <w n="69.4">le</w> <w n="69.5">compris</w>, <w n="69.6">prête</w> <w n="69.7">à</w> <w n="69.8">l</w>’<w n="69.9">adieu</w> <w n="69.10">suprême</w>,</l>
						<l n="70" num="1.10"><w n="70.1">Mon</w> <w n="70.2">âme</w> <w n="70.3">à</w> <w n="70.4">tout</w> <w n="70.5">jamais</w> <w n="70.6">libre</w> <w n="70.7">sous</w> <w n="70.8">les</w> <w n="70.9">grands</w> <w n="70.10">cieux</w>.</l>
					</lg>
				</div>
				<div type="section" n="8">
					<head type="number">VIII</head>
					<lg n="1">
						<l n="71" num="1.1"><w n="71.1">Comme</w> <w n="71.2">un</w> <w n="71.3">glaive</w> <w n="71.4">éclatant</w> <w n="71.5">hors</w> <w n="71.6">d</w>’<w n="71.7">une</w> <w n="71.8">affreuse</w> <w n="71.9">gaîne</w>,</l>
						<l n="72" num="1.2"><w n="72.1">Elle</w> <w n="72.2">était</w> <w n="72.3">là</w> <w n="72.4">debout</w> <w n="72.5">avec</w> <w n="72.6">son</w> <w n="72.7">regard</w> <w n="72.8">clair</w>,</l>
						<l n="73" num="1.3"><w n="73.1">Dont</w> <w n="73.2">je</w> <w n="73.3">sentais</w> <w n="73.4">l</w>’<w n="73.5">acier</w> <w n="73.6">pénétrer</w> <w n="73.7">dans</w> <w n="73.8">ma</w> <w n="73.9">chair</w>.</l>
						<l n="74" num="1.4"><w n="74.1">Elle</w> <w n="74.2">était</w> <w n="74.3">là</w> <w n="74.4">visible</w>, <w n="74.5">et</w> <w n="74.6">désormais</w> <w n="74.7">sans</w> <w n="74.8">chaîne</w> ;</l>
						<l n="75" num="1.5"><w n="75.1">Telle</w> <w n="75.2">qu</w>’<w n="75.3">un</w> <w n="75.4">glaive</w> <w n="75.5">nu</w> <w n="75.6">debout</w> <w n="75.7">près</w> <w n="75.8">de</w> <w n="75.9">sa</w> <w n="75.10">gaîne</w>,</l>
						<l n="76" num="1.6"><w n="76.1">Elle</w> <w n="76.2">m</w>’<w n="76.3">enveloppait</w> <w n="76.4">avec</w> <w n="76.5">son</w> <w n="76.6">regard</w> <w n="76.7">clair</w>.</l>
						<l n="77" num="1.7"><w n="77.1">Et</w> <w n="77.2">tout</w> <w n="77.3">me</w> <w n="77.4">regardait</w>, <w n="77.5">conscience</w>, <w n="77.6">pensées</w>,</l>
						<l n="78" num="1.8"><w n="78.1">Esprit</w>, <w n="78.2">rêves</w>, <w n="78.3">désirs</w>, <w n="78.4">joie</w>, <w n="78.5">espoirs</w> <w n="78.6">et</w> <w n="78.7">douleurs</w>,</l>
						<l n="79" num="1.9"><w n="79.1">Qui</w> <w n="79.2">reprenaient</w>, <w n="79.3">au</w> <w n="79.4">glas</w> <w n="79.5">des</w> <w n="79.6">souffrances</w> <w n="79.7">passées</w>,</l>
						<l n="80" num="1.10"><w n="80.1">Leurs</w> <w n="80.2">formes</w>, <w n="80.3">leurs</w> <w n="80.4">parfums</w>, <w n="80.5">leurs</w> <w n="80.6">sons</w> <w n="80.7">et</w> <w n="80.8">leurs</w> <w n="80.9">couleurs</w>.</l>
					</lg>
				</div>
				<div type="section" n="9">
					<head type="number">IX</head>
					<lg n="1">
						<l n="81" num="1.1"><w n="81.1">Et</w> <w n="81.2">voilà</w> <w n="81.3">cette</w> <w n="81.4">fois</w> <w n="81.5">qu</w>’<w n="81.6">une</w> <w n="81.7">arche</w> <w n="81.8">de</w> <w n="81.9">lumière</w>,</l>
						<l n="82" num="1.2"><w n="82.1">Jusqu</w>’<w n="82.2">au</w> <w n="82.3">ciel</w>, <w n="82.4">par</w>-<w n="82.5">dessus</w> <w n="82.6">les</w> <w n="82.7">étoiles</w>, <w n="82.8">d</w>’<w n="82.9">un</w> <w n="82.10">jet</w>,</l>
						<l n="83" num="1.3"><w n="83.1">Près</w> <w n="83.2">de</w> <w n="83.3">nous</w>, <w n="83.4">comme</w> <w n="83.5">un</w> <w n="83.6">pont</w> <w n="83.7">sans</w> <w n="83.8">limite</w> <w n="83.9">émergeait</w>,</l>
						<l n="84" num="1.4"><w n="84.1">Un</w> <w n="84.2">chemin</w> <w n="84.3">idéal</w> <w n="84.4">fait</w> <w n="84.5">d</w>’<w n="84.6">astres</w> <w n="84.7">en</w> <w n="84.8">poussière</w>.</l>
						<l n="85" num="1.5"><w n="85.1">Mon</w> <w n="85.2">âme</w> <w n="85.3">alors</w> <w n="85.4">me</w> <w n="85.5">dit</w> : « <w n="85.6">cette</w> <w n="85.7">arche</w> <w n="85.8">de</w> <w n="85.9">lumière</w></l>
						<l n="86" num="1.6"><w n="86.1">Qui</w> <w n="86.2">traverse</w> <w n="86.3">les</w> <w n="86.4">cieux</w> <w n="86.5">révélés</w> <w n="86.6">d</w>’<w n="86.7">un</w> <w n="86.8">seul</w> <w n="86.9">jet</w>,</l>
						<l n="87" num="1.7"><w n="87.1">Sort</w> <w n="87.2">du</w> <w n="87.3">temps</w>, <w n="87.4">et</w> <w n="87.5">tout</w> <w n="87.6">droit</w> <w n="87.7">vers</w> <w n="87.8">l</w>’<w n="87.9">éternité</w> <w n="87.10">mène</w>.</l>
						<l n="88" num="1.8"><w n="88.1">Boue</w> <w n="88.2">inerte</w>, <w n="88.3">matière</w>, <w n="88.4">ô</w> <w n="88.5">corps</w> ! <w n="88.6">Vieux</w> <w n="88.7">ennemis</w>,</l>
						<l n="89" num="1.9"><w n="89.1">Je</w> <w n="89.2">vous</w> <w n="89.3">repousse</w> <w n="89.4">enfin</w>, <w n="89.5">geôliers</w> <w n="89.6">de</w> <w n="89.7">l</w>’<w n="89.8">âme</w> <w n="89.9">humaine</w> ;</l>
						<l n="90" num="1.10"><w n="90.1">Retournez</w> <w n="90.2">par</w> <w n="90.3">la</w> <w n="90.4">mort</w> <w n="90.5">dans</w> <w n="90.6">le</w> <w n="90.7">néant</w> <w n="90.8">promis</w> ! »</l>
					</lg>
				</div>
				<div type="section" n="10">
					<head type="number">X</head>
					<lg n="1">
						<l n="91" num="1.1">— « <w n="91.1">Reste</w> ! <w n="91.2">Cria</w> <w n="91.3">le</w> <w n="91.4">corps</w>, <w n="91.5">reste</w> <w n="91.6">près</w> <w n="91.7">de</w> <w n="91.8">ton</w> <w n="91.9">frère</w> !</l>
						<l n="92" num="1.2">— <w n="92.1">Faible</w> <w n="92.2">et</w> <w n="92.3">vil</w> <w n="92.4">compagnon</w>, <w n="92.5">je</w> <w n="92.6">t</w>’<w n="92.7">ai</w> <w n="92.8">toujours</w> <w n="92.9">haï</w>.</l>
						<l n="93" num="1.3">— <w n="93.1">N</w>’<w n="93.2">ai</w>-<w n="93.3">je</w> <w n="93.4">pas</w> <w n="93.5">chaque</w> <w n="93.6">jour</w> <w n="93.7">à</w> <w n="93.8">ton</w> <w n="93.9">ordre</w> <w n="93.10">obéi</w> ?</l>
						<l n="94" num="1.4">— <w n="94.1">Tu</w> <w n="94.2">mens</w>, <w n="94.3">et</w> <w n="94.4">ton</w> <w n="94.5">désir</w> <w n="94.6">était</w> <w n="94.7">au</w> <w n="94.8">mien</w> <w n="94.9">contraire</w>.</l>
						<l n="95" num="1.5">— <w n="95.1">Reste</w>, <w n="95.2">je</w> <w n="95.3">me</w> <w n="95.4">soumets</w>, <w n="95.5">prends</w> <w n="95.6">pitié</w> <w n="95.7">de</w> <w n="95.8">ton</w> <w n="95.9">frère</w> !</l>
						<l n="96" num="1.6">— <w n="96.1">Meurs</w> ! <w n="96.2">Tu</w> <w n="96.3">me</w> <w n="96.4">hais</w> <w n="96.5">autant</w> <w n="96.6">que</w>, <w n="96.7">moi</w>, <w n="96.8">je</w> <w n="96.9">t</w>’<w n="96.10">ai</w> <w n="96.11">haï</w>.</l>
						<l n="97" num="1.7">— <w n="97.1">Reste</w> ! <w n="97.2">Je</w> <w n="97.3">t</w>’<w n="97.4">aimerai</w>, <w n="97.5">ton</w> <w n="97.6">départ</w> <w n="97.7">m</w>’<w n="97.8">épouvante</w>.</l>
						<l n="98" num="1.8">— <w n="98.1">Mes</w> <w n="98.2">remords</w> <w n="98.3">sont</w> <w n="98.4">tes</w> <w n="98.5">fils</w>, <w n="98.6">seule</w> <w n="98.7">il</w> <w n="98.8">m</w>’<w n="98.9">en</w> <w n="98.10">faut</w> <w n="98.11">souffrir</w>.</l>
						<l n="99" num="1.9">— <w n="99.1">Moi</w>, <w n="99.2">j</w>’<w n="99.3">ai</w> <w n="99.4">souffert</w> <w n="99.5">aussi</w> <w n="99.6">par</w> <w n="99.7">toi</w>, <w n="99.8">sœur</w> <w n="99.9">décevante</w>.</l>
						<l n="100" num="1.10">— <w n="100.1">L</w>’<w n="100.2">oubli</w> <w n="100.3">gît</w> <w n="100.4">dans</w> <w n="100.5">la</w> <w n="100.6">terre</w> <w n="100.7">où</w> <w n="100.8">tes</w> <w n="100.9">os</w> <w n="100.10">vont</w> <w n="100.11">pourrir</w>.</l>
					</lg>
				</div>
				<div type="section" n="11">
					<head type="number">XI</head>
					<lg n="1">
						<l n="101" num="1.1">— « <w n="101.1">Qui</w> <w n="101.2">me</w> <w n="101.3">consolera</w> <w n="101.4">dans</w> <w n="101.5">le</w> <w n="101.6">vide</w> <w n="101.7">où</w> <w n="101.8">je</w> <w n="101.9">sombre</w> ?</l>
						<l n="102" num="1.2">— <w n="102.1">En</w> <w n="102.2">moi</w> <w n="102.3">qui</w> <w n="102.4">versera</w> <w n="102.5">le</w> <w n="102.6">repos</w> <w n="102.7">et</w> <w n="102.8">la</w> <w n="102.9">paix</w> ?</l>
						<l n="103" num="1.3">— <w n="103.1">Oh</w> ! <w n="103.2">Mourir</w> ; <w n="103.3">ne</w> <w n="103.4">plus</w> <w n="103.5">voir</w> <w n="103.6">le</w> <w n="103.7">clair</w> <w n="103.8">soleil</w> <w n="103.9">jamais</w> !</l>
						<l n="104" num="1.4">— <w n="104.1">Oh</w> ! <w n="104.2">Revivre</w>, <w n="104.3">et</w> <w n="104.4">jamais</w> <w n="104.5">ne</w> <w n="104.6">s</w>’<w n="104.7">endormir</w> <w n="104.8">dans</w> <w n="104.9">l</w>’<w n="104.10">ombre</w> !</l>
						<l n="105" num="1.5">— <w n="105.1">Le</w> <w n="105.2">froid</w> <w n="105.3">terrible</w> <w n="105.4">règne</w> <w n="105.5">en</w> <w n="105.6">ce</w> <w n="105.7">vide</w> <w n="105.8">où</w> <w n="105.9">je</w> <w n="105.10">sombre</w> !</l>
						<l n="106" num="1.6">— <w n="106.1">L</w>’<w n="106.2">infini</w> <w n="106.3">qui</w> <w n="106.4">m</w>’<w n="106.5">étreint</w> <w n="106.6">ignore</w>, <w n="106.7">hélas</w> ! <w n="106.8">La</w> <w n="106.9">paix</w> !</l>
						<l n="107" num="1.7">— <w n="107.1">La</w> <w n="107.2">mort</w> <w n="107.3">rit</w> <w n="107.4">et</w> <w n="107.5">m</w>’<w n="107.6">attend</w> ! — <w n="107.7">un</w> <w n="107.8">ange</w> <w n="107.9">aussi</w> <w n="107.10">m</w>’<w n="107.11">appelle</w> !</l>
						<l n="108" num="1.8">— <w n="108.1">Je</w> <w n="108.2">maudis</w> <w n="108.3">ton</w> <w n="108.4">orgueil</w> ! — <w n="108.5">et</w> <w n="108.6">moi</w>, <w n="108.7">ta</w> <w n="108.8">lâcheté</w> !</l>
						<l n="109" num="1.9">— <w n="109.1">Ah</w> ! <w n="109.2">L</w>’<w n="109.3">horreur</w> <w n="109.4">du</w> <w n="109.5">néant</w> <w n="109.6">crispe</w> <w n="109.7">ma</w> <w n="109.8">chair</w> <w n="109.9">mortelle</w> !</l>
						<l n="110" num="1.10">— <w n="110.1">Et</w> <w n="110.2">moi</w>, <w n="110.3">pleine</w> <w n="110.4">d</w>’<w n="110.5">horreur</w>, <w n="110.6">j</w>’<w n="110.7">entre</w> <w n="110.8">en</w> <w n="110.9">l</w>’<w n="110.10">éternité</w> ! »</l>
					</lg>
				</div>
				<div type="section" n="12">
					<head type="number">XII</head>
					<lg n="1">
						<l n="111" num="1.1"><w n="111.1">Un</w> <w n="111.2">choc</w> <w n="111.3">intérieur</w> <w n="111.4">traversa</w> <w n="111.5">tout</w> <w n="111.6">mon</w> <w n="111.7">être</w>.</l>
						<l n="112" num="1.2"><w n="112.1">Tout</w> <w n="112.2">disparut</w>. <w n="112.3">Mon</w> <w n="112.4">corps</w> <w n="112.5">était</w> <w n="112.6">resté</w> <w n="112.7">tout</w> <w n="112.8">seul</w>,</l>
						<l n="113" num="1.3"><w n="113.1">Et</w> <w n="113.2">la</w> <w n="113.3">nuit</w> <w n="113.4">l</w>’<w n="113.5">embrassa</w> <w n="113.6">de</w> <w n="113.7">son</w> <w n="113.8">épais</w> <w n="113.9">linceul</w>,</l>
						<l n="114" num="1.4"><w n="114.1">Nuit</w> <w n="114.2">telle</w> <w n="114.3">qu</w>’<w n="114.4">un</w> <w n="114.5">vivant</w> <w n="114.6">n</w>’<w n="114.7">en</w> <w n="114.8">peut</w> <w n="114.9">jamais</w> <w n="114.10">connaître</w>.</l>
						<l n="115" num="1.5"><w n="115.1">Un</w> <w n="115.2">frisson</w> <w n="115.3">glacial</w> <w n="115.4">courut</w> <w n="115.5">dans</w> <w n="115.6">tout</w> <w n="115.7">mon</w> <w n="115.8">être</w>,</l>
						<l n="116" num="1.6"><w n="116.1">Et</w> <w n="116.2">dans</w> <w n="116.3">un</w> <w n="116.4">puits</w> <w n="116.5">sans</w> <w n="116.6">fond</w> <w n="116.7">je</w> <w n="116.8">croyais</w> <w n="116.9">choir</w> <w n="116.10">tout</w> <w n="116.11">seul</w>.</l>
						<l n="117" num="1.7"><w n="117.1">L</w>’<w n="117.2">angoisse</w> <w n="117.3">de</w> <w n="117.4">la</w> <w n="117.5">chute</w> <w n="117.6">était</w> <w n="117.7">l</w>’<w n="117.8">idée</w> <w n="117.9">unique</w></l>
						<l n="118" num="1.8"><w n="118.1">Et</w> <w n="118.2">nette</w> <w n="118.3">survivante</w> <w n="118.4">encore</w> <w n="118.5">en</w> <w n="118.6">mon</w> <w n="118.7">cerveau</w> ;</l>
						<l n="119" num="1.9"><w n="119.1">Puis</w> <w n="119.2">insensiblement</w> <w n="119.3">la</w> <w n="119.4">terreur</w> <w n="119.5">tyrannique</w></l>
						<l n="120" num="1.10"><w n="120.1">S</w>’<w n="120.2">enfuit</w> <w n="120.3">pour</w> <w n="120.4">me</w> <w n="120.5">laisser</w> <w n="120.6">jouir</w> <w n="120.7">d</w>’<w n="120.8">un</w> <w n="120.9">sens</w> <w n="120.10">nouveau</w>.</l>
					</lg>
				</div>
				<div type="section" n="13">
					<head type="number">XIII</head>
					<lg n="1">
						<l n="121" num="1.1"><w n="121.1">La</w> <w n="121.2">nuit</w> <w n="121.3">filtrait</w> <w n="121.4">en</w> <w n="121.5">moi</w>, <w n="121.6">fraîche</w> <w n="121.7">comme</w> <w n="121.8">un</w> <w n="121.9">breuvage</w> ;</l>
						<l n="122" num="1.2"><w n="122.1">Mes</w> <w n="122.2">pores</w> <w n="122.3">la</w> <w n="122.4">buvaient</w> <w n="122.5">délicieusement</w> ;</l>
						<l n="123" num="1.3"><w n="123.1">Je</w> <w n="123.2">me</w> <w n="123.3">sentais</w> <w n="123.4">bercé</w> <w n="123.5">par</w> <w n="123.6">son</w> <w n="123.7">enivrement</w> ;</l>
						<l n="124" num="1.4"><w n="124.1">Et</w> <w n="124.2">toujours</w> <w n="124.3">j</w>’<w n="124.4">approchais</w> <w n="124.5">du</w> <w n="124.6">ténébreux</w> <w n="124.7">rivage</w></l>
						<l n="125" num="1.5"><w n="125.1">Où</w> <w n="125.2">l</w>’<w n="125.3">ombre</w> <w n="125.4">dans</w> <w n="125.5">les</w> <w n="125.6">corps</w> <w n="125.7">filtre</w> <w n="125.8">comme</w> <w n="125.9">un</w> <w n="125.10">breuvage</w>.</l>
						<l n="126" num="1.6"><w n="126.1">Le</w> <w n="126.2">Léthé</w> <w n="126.3">de</w> <w n="126.4">la</w> <w n="126.5">nuit</w> <w n="126.6">délicieusement</w></l>
						<l n="127" num="1.7"><w n="127.1">M</w>’<w n="127.2">imprégnait</w> <w n="127.3">d</w>’<w n="127.4">un</w> <w n="127.5">silence</w> <w n="127.6">ineffable</w> ; <w n="127.7">et</w> <w n="127.8">la</w> <w n="127.9">vie</w></l>
						<l n="128" num="1.8"><w n="128.1">Ne</w> <w n="128.2">comprendra</w> <w n="128.3">jamais</w> <w n="128.4">le</w> <w n="128.5">silence</w> <w n="128.6">et</w> <w n="128.7">la</w> <w n="128.8">nuit</w></l>
						<l n="129" num="1.9"><w n="129.1">Qui</w>, <w n="129.2">de</w> <w n="129.3">plus</w> <w n="129.4">en</w> <w n="129.5">plus</w> <w n="129.6">doux</w> <w n="129.7">pour</w> <w n="129.8">la</w> <w n="129.9">chair</w> <w n="129.10">asservie</w>,</l>
						<l n="130" num="1.10"><w n="130.1">Montaient</w> <w n="130.2">comme</w> <w n="130.3">le</w> <w n="130.4">jour</w>, <w n="130.5">croissaient</w> <w n="130.6">comme</w> <w n="130.7">le</w> <w n="130.8">bruit</w>.</l>
					</lg>
				</div>
				<div type="section" n="14">
					<head type="number">XIV</head>
					<lg n="1">
						<l n="131" num="1.1"><w n="131.1">Et</w> <w n="131.2">maintenant</w> <w n="131.3">au</w> <w n="131.4">bord</w> <w n="131.5">de</w> <w n="131.6">l</w>’<w n="131.7">érèbe</w> <w n="131.8">immobile</w>,</l>
						<l n="132" num="1.2"><w n="132.1">Sous</w> <w n="132.2">l</w>’<w n="132.3">œil</w> <w n="132.4">démesuré</w> <w n="132.5">d</w>’<w n="132.6">un</w> <w n="132.7">fixe</w> <w n="132.8">et</w> <w n="132.9">noir</w> <w n="132.10">soleil</w>,</l>
						<l n="133" num="1.3"><w n="133.1">Je</w> <w n="133.2">reposais</w> <w n="133.3">dissous</w> <w n="133.4">dans</w> <w n="133.5">l</w>’<w n="133.6">éternel</w> <w n="133.7">sommeil</w>,</l>
						<l n="134" num="1.4"><w n="134.1">Fécondant</w> <w n="134.2">sans</w> <w n="134.3">efforts</w> <w n="134.4">les</w> <w n="134.5">vaisseaux</w> <w n="134.6">de</w> <w n="134.7">l</w>’<w n="134.8">argile</w>.</l>
						<l n="135" num="1.5"><w n="135.1">Toujours</w> <w n="135.2">plus</w> <w n="135.3">obscurcis</w>, <w n="135.4">dans</w> <w n="135.5">l</w>’<w n="135.6">érèbe</w> <w n="135.7">immobile</w></l>
						<l n="136" num="1.6"><w n="136.1">Tombaient</w> <w n="136.2">les</w> <w n="136.3">longs</w> <w n="136.4">rayons</w> <w n="136.5">d</w>’<w n="136.6">un</w> <w n="136.7">fixe</w> <w n="136.8">et</w> <w n="136.9">noir</w> <w n="136.10">soleil</w> ;</l>
						<l n="137" num="1.7"><w n="137.1">Et</w> <w n="137.2">je</w> <w n="137.3">comptais</w> <w n="137.4">sans</w> <w n="137.5">fin</w>, <w n="137.6">ainsi</w> <w n="137.7">que</w> <w n="137.8">des</w> <w n="137.9">secondes</w>,</l>
						<l n="138" num="1.8"><w n="138.1">Les</w> <w n="138.2">siècles</w> <w n="138.3">un</w> <w n="138.4">par</w> <w n="138.5">un</w> <w n="138.6">tombés</w> <w n="138.7">des</w> <w n="138.8">mornes</w> <w n="138.9">cieux</w>,</l>
						<l n="139" num="1.9"><w n="139.1">Les</w> <w n="139.2">siècles</w> <w n="139.3">morts</w> <w n="139.4">tombés</w> <w n="139.5">de</w> <w n="139.6">l</w>’<w n="139.7">amas</w> <w n="139.8">des</w> <w n="139.9">vieux</w> <w n="139.10">mondes</w>,</l>
						<l n="140" num="1.10"><w n="140.1">Tombés</w> <w n="140.2">dans</w> <w n="140.3">le</w> <w n="140.4">néant</w> <w n="140.5">noir</w> <w n="140.6">et</w> <w n="140.7">silencieux</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>