<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES ET POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2449 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Et Poésies</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxpoemesetpoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1864">1864</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX1">
				<head type="main">La Vision d’Ève</head>
				<opener>
				<salute>À Leconte De Lisle.</salute>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">était</w> <w n="1.3">trois</w> <w n="1.4">ans</w> <w n="1.5">après</w> <w n="1.6">le</w> <w n="1.7">péché</w> <w n="1.8">dans</w> <w n="1.9">l</w>’<w n="1.10">Éden</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Adam</w> <w n="2.2">sous</w> <w n="2.3">les</w> <w n="2.4">grands</w> <w n="2.5">bois</w> <w n="2.6">chassait</w>, <w n="2.7">fier</w> <w n="2.8">et</w> <w n="2.9">superbe</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Luttant</w> <w n="3.2">contre</w> <w n="3.3">le</w> <w n="3.4">tigre</w> <w n="3.5">et</w> <w n="3.6">poursuivant</w> <w n="3.7">le</w> <w n="3.8">daim</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Tranquille</w>, <w n="4.2">il</w> <w n="4.3">aspirait</w> <w n="4.4">l</w>’<w n="4.5">âcre</w> <w n="4.6">senteur</w> <w n="4.7">de</w> <w n="4.8">l</w>’<w n="4.9">herbe</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ève</w>, <w n="5.2">sereine</w> <w n="5.3">aussi</w>, <w n="5.4">corps</w> <w n="5.5">vêtu</w> <w n="5.6">de</w> <w n="5.7">clartés</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Assise</w> <w n="6.2">aux</w> <w n="6.3">bords</w> <w n="6.4">ombreux</w> <w n="6.5">d</w>’<w n="6.6">une</w> <w n="6.7">vierge</w> <w n="6.8">fontaine</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Regardait</w> <w n="7.2">deux</w> <w n="7.3">enfants</w> <w n="7.4">s</w>’<w n="7.5">ébattre</w> <w n="7.6">à</w> <w n="7.7">ses</w> <w n="7.8">côtés</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Attentive</w> <w n="8.2">aux</w> <w n="8.3">échos</w> <w n="8.4">de</w> <w n="8.5">la</w> <w n="8.6">chasse</w> <w n="8.7">lointaine</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Adam</w> <w n="9.2">sous</w> <w n="9.3">la</w> <w n="9.4">forêt</w> <w n="9.5">parlait</w> <w n="9.6">d</w>’<w n="9.7">Ève</w> <w n="9.8">aux</w> <w n="9.9">oiseaux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">leur</w> <w n="10.3">disait</w> : « <w n="10.4">Chantez</w> ! <w n="10.5">Elle</w> <w n="10.6">est</w> <w n="10.7">belle</w>, <w n="10.8">et</w> <w n="10.9">je</w> <w n="10.10">l</w>’<w n="10.11">aime</w> ! »</l>
						<l n="11" num="3.3"><w n="11.1">Ève</w> <w n="11.2">disait</w> : « <w n="11.3">Répands</w>, <w n="11.4">source</w>, <w n="11.5">tes</w> <w n="11.6">fraîches</w> <w n="11.7">eaux</w> !</l>
						<l n="12" num="3.4"><w n="12.1">Mon</w> <w n="12.2">âme</w> <w n="12.3">vibre</w> <w n="12.4">en</w> <w n="12.5">lui</w>, <w n="12.6">mais</w> <w n="12.7">en</w> <w n="12.8">eux</w>, <w n="12.9">ma</w> <w n="12.10">chair</w> <w n="12.11">même</w> ! »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="13" num="1.1"><w n="13.1">Ève</w> <w n="13.2">pensait</w> : « <w n="13.3">Seigneur</w> ! <w n="13.4">Vous</w> <w n="13.5">nous</w> <w n="13.6">avez</w> <w n="13.7">chassés</w></l>
						<l n="14" num="1.2"><w n="14.1">Du</w> <w n="14.2">paradis</w> ; <w n="14.3">l</w>’<w n="14.4">archange</w> <w n="14.5">a</w> <w n="14.6">fait</w> <w n="14.7">luire</w> <w n="14.8">son</w> <w n="14.9">glaive</w>.</l>
						<l n="15" num="1.3"><w n="15.1">Mordus</w> <w n="15.2">par</w> <w n="15.3">la</w> <w n="15.4">douleur</w>, <w n="15.5">et</w> <w n="15.6">par</w> <w n="15.7">la</w> <w n="15.8">faim</w> <w n="15.9">pressés</w>,</l>
						<l n="16" num="1.4"><w n="16.1">Il</w> <w n="16.2">nous</w> <w n="16.3">faut</w> <w n="16.4">haleter</w> <w n="16.5">dès</w> <w n="16.6">que</w> <w n="16.7">le</w> <w n="16.8">jour</w> <w n="16.9">se</w> <w n="16.10">lève</w></l>
					</lg>
					<lg n="2">
						<l n="17" num="2.1">« <w n="17.1">Nous</w> <w n="17.2">n</w>’<w n="17.3">avons</w> <w n="17.4">plus</w>, <w n="17.5">errants</w> <w n="17.6">dans</w> <w n="17.7">ces</w> <w n="17.8">mornes</w> <w n="17.9">ravins</w>,</l>
						<l n="18" num="2.2"><w n="18.1">Maître</w> ! <w n="18.2">Comme</w> <w n="18.3">autrefois</w>, <w n="18.4">la</w> <w n="18.5">candeur</w> <w n="18.6">ni</w> <w n="18.7">l</w>’<w n="18.8">extase</w> ;</l>
						<l n="19" num="2.3"><w n="19.1">Et</w> <w n="19.2">nous</w> <w n="19.3">n</w>’<w n="19.4">entendons</w> <w n="19.5">plus</w> <w n="19.6">dans</w> <w n="19.7">les</w> <w n="19.8">buissons</w> <w n="19.9">divins</w></l>
						<l n="20" num="2.4"><w n="20.1">L</w>’<w n="20.2">hymne</w> <w n="20.3">des</w> <w n="20.4">anges</w> <w n="20.5">blancs</w> <w n="20.6">que</w> <w n="20.7">votre</w> <w n="20.8">gloire</w> <w n="20.9">embrase</w>.</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1">« <w n="21.1">Mais</w> <w n="21.2">qu</w>’<w n="21.3">importent</w> <w n="21.4">l</w>’<w n="21.5">embûche</w> <w n="21.6">et</w> <w n="21.7">la</w> <w n="21.8">nuit</w> <w n="21.9">sous</w> <w n="21.10">nos</w> <w n="21.11">pas</w>,</l>
						<l n="22" num="3.2"><w n="22.1">Si</w> <w n="22.2">toujours</w> <w n="22.3">dans</w> <w n="22.4">la</w> <w n="22.5">nuit</w> <w n="22.6">un</w> <w n="22.7">flambeau</w> <w n="22.8">nous</w> <w n="22.9">éclaire</w> ?</l>
						<l n="23" num="3.3"><w n="23.1">Ah</w> ! <w n="23.2">Si</w> <w n="23.3">l</w>’<w n="23.4">amour</w> <w n="23.5">nous</w> <w n="23.6">reste</w> <w n="23.7">et</w> <w n="23.8">nous</w> <w n="23.9">guide</w> <w n="23.10">ici</w>-<w n="23.11">bas</w>,</l>
						<l n="24" num="3.4"><w n="24.1">Soyez</w> <w n="24.2">béni</w> ! <w n="24.3">Dieu</w> <w n="24.4">fort</w> ! <w n="24.5">Dieu</w> <w n="24.6">bon</w> ! <w n="24.7">Dieu</w> <w n="24.8">tutélaire</w> !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">« <w n="25.1">Adam</w> <w n="25.2">a</w> <w n="25.3">la</w> <w n="25.4">vigueur</w> <w n="25.5">et</w> <w n="25.6">moi</w> <w n="25.7">j</w>’<w n="25.8">ai</w> <w n="25.9">la</w> <w n="25.10">beauté</w>.</l>
						<l n="26" num="4.2"><w n="26.1">Un</w> <w n="26.2">contraste</w> <w n="26.3">à</w> <w n="26.4">jamais</w> <w n="26.5">nous</w> <w n="26.6">lie</w> <w n="26.7">et</w> <w n="26.8">nous</w> <w n="26.9">console</w> ;</l>
						<l n="27" num="4.3"><w n="27.1">Ivres</w>, <w n="27.2">lui</w> <w n="27.3">de</w> <w n="27.4">ma</w> <w n="27.5">grâce</w> <w n="27.6">et</w> <w n="27.7">moi</w> <w n="27.8">de</w> <w n="27.9">sa</w> <w n="27.10">fierté</w>,</l>
						<l n="28" num="4.4"><w n="28.1">Pour</w> <w n="28.2">nous</w> <w n="28.3">chaque</w> <w n="28.4">fardeau</w> <w n="28.5">se</w> <w n="28.6">change</w> <w n="28.7">en</w> <w n="28.8">auréole</w>.</l>
					</lg>
					<lg n="5">
						<l n="29" num="5.1">« <w n="29.1">Et</w> <w n="29.2">maintenant</w>, <w n="29.3">voici</w> <w n="29.4">grandir</w> <w n="29.5">auprès</w> <w n="29.6">de</w> <w n="29.7">nous</w></l>
						<l n="30" num="5.2"><w n="30.1">Deux</w> <w n="30.2">êtres</w>, <w n="30.3">notre</w> <w n="30.4">espoir</w>, <w n="30.5">notre</w> <w n="30.6">orgueil</w>, <w n="30.7">notre</w> <w n="30.8">joie</w> ;</l>
						<l n="31" num="5.3"><w n="31.1">Quand</w> <w n="31.2">je</w> <w n="31.3">les</w> <w n="31.4">tiens</w> <w n="31.5">tous</w> <w n="31.6">deux</w> <w n="31.7">groupés</w> <w n="31.8">sur</w> <w n="31.9">mes</w> <w n="31.10">genoux</w>,</l>
						<l n="32" num="5.4"><w n="32.1">Je</w> <w n="32.2">sens</w> <w n="32.3">dans</w> <w n="32.4">ma</w> <w n="32.5">poitrine</w> <w n="32.6">un</w> <w n="32.7">soleil</w> <w n="32.8">qui</w> <w n="32.9">rougeoie</w> !</l>
					</lg>
					<lg n="6">
						<l n="33" num="6.1">« <w n="33.1">Vivant</w> <w n="33.2">encore</w> <w n="33.3">e</w> <w n="33.4">nous</w> <w n="33.5">qui</w> <w n="33.6">revivons</w> <w n="33.7">en</w> <w n="33.8">eux</w>,</l>
						<l n="34" num="6.2"><w n="34.1">Encor</w> <w n="34.2">pleins</w> <w n="34.3">de</w> <w n="34.4">mystère</w>, <w n="34.5">ils</w> <w n="34.6">sont</w> <w n="34.7">la</w> <w n="34.8">loi</w> <w n="34.9">nouvelle</w>.</l>
						<l n="35" num="6.3"><w n="35.1">Nés</w> <w n="35.2">de</w> <w n="35.3">nous</w>, <w n="35.4">sous</w> <w n="35.5">leurs</w> <w n="35.6">doigts</w> <w n="35.7">ils</w> <w n="35.8">resserrent</w> <w n="35.9">nos</w> <w n="35.10">nœuds</w> ;</l>
						<l n="36" num="6.4"><w n="36.1">Un</w> <w n="36.2">autre</w> <w n="36.3">amour</w> <w n="36.4">en</w> <w n="36.5">nous</w>, <w n="36.6">aussi</w> <w n="36.7">grand</w>, <w n="36.8">se</w> <w n="36.9">révèle</w>.</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1">« <w n="37.1">Leurs</w> <w n="37.2">yeux</w>, <w n="37.3">astres</w> <w n="37.4">plus</w> <w n="37.5">clairs</w> <w n="37.6">que</w> <w n="37.7">ceux</w> <w n="37.8">du</w> <w n="37.9">firmament</w>,</l>
						<l n="38" num="7.2"><w n="38.1">Ont</w> <w n="38.2">un</w> <w n="38.3">étrange</w> <w n="38.4">attrait</w> ; <w n="38.5">et</w> <w n="38.6">notre</w> <w n="38.7">âme</w> <w n="38.8">attirée</w>,</l>
						<l n="39" num="7.3"><w n="39.1">Qui</w> <w n="39.2">s</w>’<w n="39.3">étonne</w> <w n="39.4">et</w> <w n="39.5">s</w>’<w n="39.6">abîme</w> <w n="39.7">en</w> <w n="39.8">leur</w> <w n="39.9">regard</w> <w n="39.10">charmant</w>,</l>
						<l n="40" num="7.4"><w n="40.1">Y</w> <w n="40.2">cherche</w> <w n="40.3">le</w> <w n="40.4">secret</w> <w n="40.5">d</w>’<w n="40.6">une</w> <w n="40.7">enfance</w> <w n="40.8">ignorée</w>.</l>
					</lg>
					<lg n="8">
						<l n="41" num="8.1">« <w n="41.1">L</w>’<w n="41.2">amour</w> <w n="41.3">qui</w> <w n="41.4">les</w> <w n="41.5">créa</w> <w n="41.6">sommeille</w> <w n="41.7">en</w> <w n="41.8">eux</w>. <w n="41.9">Le</w> <w n="41.10">ciel</w></l>
						<l n="42" num="8.2"><w n="42.1">Peut</w> <w n="42.2">gronder</w> ; <w n="42.3">comme</w> <w n="42.4">nous</w>, <w n="42.5">dans</w> <w n="42.6">le</w> <w n="42.7">vent</w>, <w n="42.8">sous</w> <w n="42.9">l</w>’<w n="42.10">orage</w>,</l>
						<l n="43" num="8.3"><w n="43.1">Ils</w> <w n="43.2">se</w> <w n="43.3">tendront</w> <w n="43.4">la</w> <w n="43.5">main</w>, <w n="43.6">et</w> <w n="43.7">l</w>’<w n="43.8">éclair</w> <w n="43.9">d</w>’<w n="43.10">Azraël</w></l>
						<l n="44" num="8.4"><w n="44.1">Ne</w> <w n="44.2">pourra</w> <w n="44.3">faire</w> <w n="44.4">alors</w> <w n="44.5">chanceler</w> <w n="44.6">leur</w> <w n="44.7">courage</w>.</l>
					</lg>
					<lg n="9">
						<l n="45" num="9.1">« <w n="45.1">Gloire</w> <w n="45.2">et</w> <w n="45.3">louange</w> <w n="45.4">à</w> <w n="45.5">toi</w>, <w n="45.6">seigneur</w> ! <w n="45.7">à</w> <w n="45.8">toi</w> <w n="45.9">merci</w> !</l>
						<l n="46" num="9.2"><w n="46.1">Le</w> <w n="46.2">châtiment</w> <w n="46.3">est</w> <w n="46.4">doux</w>, <w n="46.5">si</w> <w n="46.6">malgré</w> <w n="46.7">l</w>’<w n="46.8">anathème</w></l>
						<l n="47" num="9.3"><w n="47.1">Le</w> <w n="47.2">baiser</w> <w n="47.3">de</w> <w n="47.4">l</w>’<w n="47.5">éden</w> <w n="47.6">se</w> <w n="47.7">perpétue</w> <w n="47.8">ici</w>.</l>
						<l n="48" num="9.4"><w n="48.1">Frappe</w> ! <w n="48.2">Regarde</w> <w n="48.3">croître</w> <w n="48.4">une</w> <w n="48.5">race</w> <w n="48.6">qui</w> <w n="48.7">t</w>’<w n="48.8">aime</w> ! »</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="49" num="1.1"><w n="49.1">Ainsi</w>, <w n="49.2">le</w> <w n="49.3">front</w> <w n="49.4">baigné</w> <w n="49.5">des</w> <w n="49.6">parfums</w> <w n="49.7">du</w> <w n="49.8">matin</w>,</l>
						<l n="50" num="1.2"><w n="50.1">Son</w> <w n="50.2">beau</w> <w n="50.3">sein</w> <w n="50.4">rayonnant</w> <w n="50.5">de</w> <w n="50.6">chaleurs</w> <w n="50.7">maternelles</w>,</l>
						<l n="51" num="1.3"><w n="51.1">Ève</w>, <w n="51.2">les</w> <w n="51.3">yeux</w> <w n="51.4">fixés</w> <w n="51.5">sur</w> <w n="51.6">Abel</w> <w n="51.7">et</w> <w n="51.8">Caïn</w>,</l>
						<l n="52" num="1.4"><w n="52.1">Sentait</w> <w n="52.2">l</w>’<w n="52.3">infini</w> <w n="52.4">bleu</w> <w n="52.5">noyé</w> <w n="52.6">dans</w> <w n="52.7">ses</w> <w n="52.8">prunelles</w>.</l>
					</lg>
				</div>
				<div type="section" n="4">
					<head type="number">IV</head>
					<lg n="1">
						<l n="53" num="1.1"><w n="53.1">Or</w> <w n="53.2">les</w> <w n="53.3">enfants</w> <w n="53.4">jouaient</w>. <w n="53.5">Soudain</w>, <w n="53.6">le</w> <w n="53.7">premier</w>-<w n="53.8">né</w>,</l>
						<l n="54" num="1.2"><w n="54.1">Debout</w>, <w n="54.2">l</w>’<w n="54.3">œil</w> <w n="54.4">plein</w> <w n="54.5">de</w> <w n="54.6">fauve</w> <w n="54.7">ardeur</w>, <w n="54.8">la</w> <w n="54.9">lèvre</w> <w n="54.10">amère</w>,</l>
						<l n="55" num="1.3"><w n="55.1">Frappa</w> <w n="55.2">l</w>’<w n="55.3">autre</w> <w n="55.4">éperdu</w> <w n="55.5">sous</w> <w n="55.6">un</w> <w n="55.7">poing</w> <w n="55.8">forcené</w></l>
						<l n="56" num="1.4"><w n="56.1">Et</w> <w n="56.2">qui</w> <w n="56.3">cria</w>, <w n="56.4">tendant</w> <w n="56.5">les</w> <w n="56.6">deux</w> <w n="56.7">mains</w> <w n="56.8">vers</w> <w n="56.9">la</w> <w n="56.10">mère</w>.</l>
					</lg>
					<lg n="2">
						<l n="57" num="2.1"><w n="57.1">Ève</w> <w n="57.2">accourut</w> <w n="57.3">tremblante</w> <w n="57.4">et</w> <w n="57.5">pâle</w> <w n="57.6">de</w> <w n="57.7">stupeur</w>,</l>
						<l n="58" num="2.2"><w n="58.1">Et</w> <w n="58.2">fermant</w> <w n="58.3">autour</w> <w n="58.4">d</w>’<w n="58.5">eux</w> <w n="58.6">ses</w> <w n="58.7">bras</w>, <w n="58.8">les</w> <w n="58.9">prit</w> <w n="58.10">sur</w> <w n="58.11">elle</w> ;</l>
						<l n="59" num="2.3"><w n="59.1">Et</w> <w n="59.2">comme</w> <w n="59.3">en</w> <w n="59.4">un</w> <w n="59.5">berceau</w> <w n="59.6">les</w> <w n="59.7">couchant</w> <w n="59.8">sur</w> <w n="59.9">son</w> <w n="59.10">cœur</w>,</l>
						<l n="60" num="2.4"><w n="60.1">Les</w> <w n="60.2">couvrit</w> <w n="60.3">de</w> <w n="60.4">baisers</w> <w n="60.5">pour</w> <w n="60.6">calmer</w> <w n="60.7">leur</w> <w n="60.8">querelle</w>.</l>
					</lg>
					<lg n="3">
						<l n="61" num="3.1"><w n="61.1">Bientôt</w> <w n="61.2">tout</w> <w n="61.3">s</w>’<w n="61.4">apaisa</w>, <w n="61.5">fureur</w>, <w n="61.6">plainte</w>, <w n="61.7">baisers</w> ;</l>
						<l n="62" num="3.2"><w n="62.1">Ils</w> <w n="62.2">dormaient</w> <w n="62.3">tous</w> <w n="62.4">les</w> <w n="62.5">deux</w> <w n="62.6">enlacés</w>, <w n="62.7">et</w> <w n="62.8">la</w> <w n="62.9">femme</w>,</l>
						<l n="63" num="3.3"><w n="63.1">Immobile</w>, <w n="63.2">ses</w> <w n="63.3">doigts</w> <w n="63.4">sous</w> <w n="63.5">un</w> <w n="63.6">genou</w> <w n="63.7">croisés</w>,</l>
						<l n="64" num="3.4"><w n="64.1">Sentit</w> <w n="64.2">les</w> <w n="64.3">jours</w> <w n="64.4">futurs</w> <w n="64.5">monter</w> <w n="64.6">noirs</w> <w n="64.7">dans</w> <w n="64.8">son</w> <w n="64.9">âme</w> !</l>
					</lg>
				</div>
				<div type="section" n="5">
					<head type="number">V</head>
					<lg n="1">
						<l n="65" num="1.1"><w n="65.1">Soleil</w> <w n="65.2">du</w> <w n="65.3">jardin</w> <w n="65.4">chaste</w> ! <w n="65.5">Ève</w> <w n="65.6">aux</w> <w n="65.7">longs</w> <w n="65.8">cheveux</w> <w n="65.9">d</w>’<w n="65.10">or</w> !</l>
						<l n="66" num="1.2"><w n="66.1">Toi</w> <w n="66.2">qui</w> <w n="66.3">fus</w> <w n="66.4">le</w> <w n="66.5">péché</w>, <w n="66.6">toi</w> <w n="66.7">qui</w> <w n="66.8">feras</w> <w n="66.9">la</w> <w n="66.10">gloire</w> !</l>
						<l n="67" num="1.3"><w n="67.1">Toi</w>, <w n="67.2">l</w>’<w n="67.3">éternel</w> <w n="67.4">soupir</w> <w n="67.5">que</w> <w n="67.6">nous</w> <w n="67.7">poussons</w> <w n="67.8">encor</w> !</l>
						<l n="68" num="1.4"><w n="68.1">Ineffable</w> <w n="68.2">calice</w> <w n="68.3">où</w> <w n="68.4">la</w> <w n="68.5">douleur</w> <w n="68.6">vient</w> <w n="68.7">boire</w> !</l>
					</lg>
					<lg n="2">
						<l n="69" num="2.1"><w n="69.1">O</w> <w n="69.2">femme</w> ! <w n="69.3">Qui</w> <w n="69.4">sachant</w> <w n="69.5">porter</w> <w n="69.6">un</w> <w n="69.7">ciel</w> <w n="69.8">en</w> <w n="69.9">toi</w>,</l>
						<l n="70" num="2.2"><w n="70.1">A</w> <w n="70.2">celui</w> <w n="70.3">qui</w> <w n="70.4">perdait</w> <w n="70.5">l</w>’<w n="70.6">autre</w> <w n="70.7">ciel</w>, <w n="70.8">en</w> <w n="70.9">échange</w></l>
						<l n="71" num="2.3"><w n="71.1">Offris</w> <w n="71.2">tout</w>, <w n="71.3">ta</w> <w n="71.4">splendeur</w>, <w n="71.5">ta</w> <w n="71.6">tendresse</w> <w n="71.7">et</w> <w n="71.8">ta</w> <w n="71.9">foi</w>,</l>
						<l n="72" num="2.4"><w n="72.1">Plus</w> <w n="72.2">belle</w> <w n="72.3">sous</w> <w n="72.4">le</w> <w n="72.5">geste</w> <w n="72.6">enflammé</w> <w n="72.7">de</w> <w n="72.8">l</w>’<w n="72.9">archange</w> !</l>
					</lg>
					<lg n="3">
						<l n="73" num="3.1"><w n="73.1">O</w> <w n="73.2">mère</w> <w n="73.3">aux</w> <w n="73.4">flancs</w> <w n="73.5">féconds</w> ! <w n="73.6">Par</w> <w n="73.7">quelle</w> <w n="73.8">brusque</w> <w n="73.9">horreur</w>,</l>
						<l n="74" num="3.2"><w n="74.1">Endormeuse</w> <w n="74.2">sans</w> <w n="74.3">voix</w>, <w n="74.4">étais</w>-<w n="74.5">tu</w> <w n="74.6">possédée</w> ?</l>
						<l n="75" num="3.3"><w n="75.1">Quel</w> <w n="75.2">si</w> <w n="75.3">livide</w> <w n="75.4">éclair</w> <w n="75.5">t</w>’<w n="75.6">en</w> <w n="75.7">fut</w> <w n="75.8">le</w> <w n="75.9">précurseur</w> ?</l>
						<l n="76" num="3.4"><w n="76.1">A</w> <w n="76.2">quoi</w> <w n="76.3">songeais</w>-<w n="76.4">tu</w> <w n="76.5">donc</w>, <w n="76.6">la</w> <w n="76.7">paupière</w> <w n="76.8">inondée</w> ?</l>
					</lg>
					<lg n="4">
						<l n="77" num="4.1"><w n="77.1">Ah</w> ! <w n="77.2">Dans</w> <w n="77.3">le</w> <w n="77.4">poing</w> <w n="77.5">crispé</w> <w n="77.6">de</w> <w n="77.7">Caïn</w> <w n="77.8">endormi</w></l>
						<l n="78" num="4.2"><w n="78.1">Lisais</w>-<w n="78.2">tu</w> <w n="78.3">la</w> <w n="78.4">réponse</w> <w n="78.5">à</w> <w n="78.6">ton</w> <w n="78.7">rêve</w> <w n="78.8">sublime</w> ?</l>
						<l n="79" num="4.3"><w n="79.1">Devinais</w>-<w n="79.2">tu</w> <w n="79.3">déjà</w> <w n="79.4">le</w> <w n="79.5">farouche</w> <w n="79.6">ennemi</w></l>
						<l n="80" num="4.4"><w n="80.1">Sur</w> <w n="80.2">Abel</w> <w n="80.3">faible</w> <w n="80.4">et</w> <w n="80.5">nu</w> <w n="80.6">s</w>’<w n="80.7">essayant</w> <w n="80.8">à</w> <w n="80.9">son</w> <w n="80.10">crime</w> ?</l>
					</lg>
					<lg n="5">
						<l n="81" num="5.1"><w n="81.1">Du</w> <w n="81.2">fond</w> <w n="81.3">de</w> <w n="81.4">l</w>’<w n="81.5">avenir</w>, <w n="81.6">Azraël</w>, <w n="81.7">menaçant</w>,</l>
						<l n="82" num="5.2"><w n="82.1">Te</w> <w n="82.2">montrait</w>-<w n="82.3">il</w> <w n="82.4">ce</w> <w n="82.5">fils</w>, <w n="82.6">ayant</w> <w n="82.7">fait</w> <w n="82.8">l</w>’<w n="82.9">œuvre</w> <w n="82.10">humaine</w>,</l>
						<l n="83" num="5.3"><w n="83.1">Qui</w> <w n="83.2">s</w>’<w n="83.3">enfuyait</w> <w n="83.4">sinistre</w> <w n="83.5">et</w> <w n="83.6">marqué</w> <w n="83.7">par</w> <w n="83.8">le</w> <w n="83.9">sang</w>,</l>
						<l n="84" num="5.4"><w n="84.1">Un</w> <w n="84.2">soir</w>, <w n="84.3">loin</w> <w n="84.4">d</w>’<w n="84.5">un</w> <w n="84.6">cadavre</w> <w n="84.7">étendu</w> <w n="84.8">dans</w> <w n="84.9">la</w> <w n="84.10">plaine</w> ?</l>
					</lg>
					<lg n="6">
						<l n="85" num="6.1"><w n="85.1">Le</w> <w n="85.2">voyais</w>-<w n="85.3">tu</w> <w n="85.4">mourir</w> <w n="85.5">longuement</w> <w n="85.6">dans</w> <w n="85.7">Énoch</w>,</l>
						<l n="86" num="6.2"><w n="86.1">Rempart</w> <w n="86.2">poussé</w> <w n="86.3">d</w>’<w n="86.4">un</w> <w n="86.5">jet</w> <w n="86.6">sous</w> <w n="86.7">le</w> <w n="86.8">puissant</w> <w n="86.9">blasphème</w></l>
						<l n="87" num="6.3"><w n="87.1">Des</w> <w n="87.2">maudits</w> <w n="87.3">qui</w> <w n="87.4">gravaient</w> <w n="87.5">leur</w> <w n="87.6">défi</w> <w n="87.7">sur</w> <w n="87.8">le</w> <w n="87.9">roc</w>,</l>
						<l n="88" num="6.4"><w n="88.1">Et</w> <w n="88.2">dont</w> <w n="88.3">la</w> <w n="88.4">race</w> <w n="88.5">immense</w> <w n="88.6">est</w> <w n="88.7">maudite</w> <w n="88.8">elle</w>-<w n="88.9">même</w> ?</l>
					</lg>
					<lg n="7">
						<l n="89" num="7.1"><w n="89.1">Ah</w> ! <w n="89.2">Voyais</w>-<w n="89.3">tu</w> <w n="89.4">l</w>’<w n="89.5">envie</w> <w n="89.6">armant</w> <w n="89.7">les</w> <w n="89.8">désaccords</w>,</l>
						<l n="90" num="7.2"><w n="90.1">Et</w> <w n="90.2">se</w> <w n="90.3">glissant</w> <w n="90.4">partout</w> <w n="90.5">comme</w> <w n="90.6">un</w> <w n="90.7">chacal</w> <w n="90.8">qui</w> <w n="90.9">rôde</w> ?</l>
						<l n="91" num="7.3"><w n="91.1">Le</w> <w n="91.2">fer</w> <w n="91.3">s</w>’<w n="91.4">ouvrant</w> <w n="91.5">sans</w> <w n="91.6">cesse</w> <w n="91.7">un</w> <w n="91.8">chemin</w> <w n="91.9">dans</w> <w n="91.10">les</w> <w n="91.11">corps</w>,</l>
						<l n="92" num="7.4"><w n="92.1">Le</w> <w n="92.2">sol</w> <w n="92.3">toujours</w> <w n="92.4">fumant</w> <w n="92.5">sous</w> <w n="92.6">une</w> <w n="92.7">pourpre</w> <w n="92.8">chaude</w> ?</l>
					</lg>
					<lg n="8">
						<l n="93" num="8.1"><w n="93.1">Et</w> <w n="93.2">les</w> <w n="93.3">peuples</w> <w n="93.4">Caïns</w> <w n="93.5">sur</w> <w n="93.6">les</w> <w n="93.7">peuples</w> <w n="93.8">Abels</w></l>
						<l n="94" num="8.2"><w n="94.1">Se</w> <w n="94.2">ruant</w> <w n="94.3">sans</w> <w n="94.4">pitié</w>, <w n="94.5">les</w> <w n="94.6">déchirant</w> <w n="94.7">sans</w> <w n="94.8">trêves</w> ;</l>
						<l n="95" num="8.3"><w n="95.1">Les</w> <w n="95.2">sanglots</w> <w n="95.3">éclatant</w> <w n="95.4">de</w> <w n="95.5">toutes</w> <w n="95.6">les</w> <w n="95.7">Babels</w>,</l>
						<l n="96" num="8.4"><w n="96.1">Les</w> <w n="96.2">râles</w> <w n="96.3">étouffés</w> <w n="96.4">par</w> <w n="96.5">la</w> <w n="96.6">clameur</w> <w n="96.7">des</w> <w n="96.8">grèves</w> ?</l>
					</lg>
					<lg n="9">
						<l n="97" num="9.1"><w n="97.1">Sous</w> <w n="97.2">l</w>’<w n="97.3">insoluble</w> <w n="97.4">brume</w> <w n="97.5">où</w> <w n="97.6">l</w>’<w n="97.7">homme</w> <w n="97.8">en</w> <w n="97.9">vils</w> <w n="97.10">troupeaux</w></l>
						<l n="98" num="9.2"><w n="98.1">S</w>’<w n="98.2">amoncelle</w>, <w n="98.3">effrayé</w> <w n="98.4">de</w> <w n="98.5">son</w> <w n="98.6">propre</w> <w n="98.7">héritage</w>,</l>
						<l n="99" num="9.3"><w n="99.1">Entendais</w>-<w n="99.2">tu</w> <w n="99.3">monter</w> <w n="99.4">dans</w> <w n="99.5">les</w> <w n="99.6">airs</w>, <w n="99.7">sans</w> <w n="99.8">repos</w>,</l>
						<l n="100" num="9.4"><w n="100.1">Le</w> <w n="100.2">hurlement</w> <w n="100.3">jaloux</w> <w n="100.4">des</w> <w n="100.5">foules</w>, <w n="100.6">d</w>’<w n="100.7">âge</w> <w n="100.8">en</w> <w n="100.9">âge</w> ?</l>
					</lg>
					<lg n="10">
						<l n="101" num="10.1"><w n="101.1">Compris</w>-<w n="101.2">tu</w> <w n="101.3">que</w> <w n="101.4">le</w> <w n="101.5">mal</w> <w n="101.6">était</w> <w n="101.7">né</w> ? <w n="101.8">Qu</w>’<w n="101.9">il</w> <w n="101.10">serait</w></l>
						<l n="102" num="10.2"><w n="102.1">Immortel</w> ? <w n="102.2">Que</w> <w n="102.3">l</w>’<w n="102.4">instinct</w> <w n="102.5">terrestre</w>, <w n="102.6">c</w>’<w n="102.7">est</w> <w n="102.8">la</w> <w n="102.9">haine</w></l>
						<l n="103" num="10.3"><w n="103.1">Qui</w>, <w n="103.2">dévouant</w> <w n="103.3">tes</w> <w n="103.4">fils</w> <w n="103.5">à</w> <w n="103.6">Satan</w> <w n="103.7">toujours</w> <w n="103.8">prêt</w>,</l>
						<l n="104" num="10.4"><w n="104.1">Lui</w> <w n="104.2">fera</w> <w n="104.3">sans</w> <w n="104.4">relâche</w> <w n="104.5">agrandir</w> <w n="104.6">la</w> <w n="104.7">géhenne</w> ?</l>
					</lg>
					<lg n="11">
						<l n="105" num="11.1"><w n="105.1">Compris</w>-<w n="105.2">tu</w> <w n="105.3">que</w> <w n="105.4">la</w> <w n="105.5">vie</w> <w n="105.6">était</w> <w n="105.7">le</w> <w n="105.8">don</w> <w n="105.9">cruel</w> ?</l>
						<l n="106" num="11.2"><w n="106.1">Que</w> <w n="106.2">l</w>’<w n="106.3">amour</w> <w n="106.4">périrait</w> <w n="106.5">avec</w> <w n="106.6">l</w>’<w n="106.7">aïeule</w> <w n="106.8">blonde</w> ?</l>
						<l n="107" num="11.3"><w n="107.1">Et</w> <w n="107.2">qu</w>’<w n="107.3">un</w> <w n="107.4">fleuve</w> <w n="107.5">infini</w> <w n="107.6">de</w> <w n="107.7">larmes</w> <w n="107.8">et</w> <w n="107.9">de</w> <w n="107.10">fiel</w></l>
						<l n="108" num="11.4"><w n="108.1">Né</w> <w n="108.2">du</w> <w n="108.3">premier</w> <w n="108.4">sourire</w> <w n="108.5">abreuverait</w> <w n="108.6">le</w> <w n="108.7">monde</w> ?</l>
					</lg>
				</div>
				<div type="section" n="6">
					<head type="number">VI</head>
					<lg n="1">
						<l n="109" num="1.1"><w n="109.1">Dieu</w> <w n="109.2">l</w>’<w n="109.3">a</w> <w n="109.4">su</w> ! — <w n="109.5">Jusqu</w>’<w n="109.6">au</w> <w n="109.7">soir</w> <w n="109.8">ainsi</w> <w n="109.9">tu</w> <w n="109.10">demeuras</w></l>
						<l n="110" num="1.2"><w n="110.1">Contemplant</w> <w n="110.2">ces</w> <w n="110.3">fronts</w> <w n="110.4">purs</w> <w n="110.5">où</w> <w n="110.6">le</w> <w n="110.7">soleil</w> <w n="110.8">se</w> <w n="110.9">joue</w> ;</l>
						<l n="111" num="1.3"><w n="111.1">Et</w> <w n="111.2">tandis</w> <w n="111.3">qu</w>’<w n="111.4">ils</w> <w n="111.5">dormaient</w> <w n="111.6">oublieux</w>, <w n="111.7">en</w> <w n="111.8">tes</w> <w n="111.9">bras</w>,</l>
						<l n="112" num="1.4"><w n="112.1">Deux</w> <w n="112.2">longs</w> <w n="112.3">ruisseaux</w> <w n="112.4">brûlants</w> <w n="112.5">descendaient</w> <w n="112.6">sur</w> <w n="112.7">ta</w> <w n="112.8">joue</w>.</l>
					</lg>
				</div>
			</div></body></text></TEI>