<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES ET POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2449 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Et Poésies</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxpoemesetpoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1864">1864</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX5">
				<head type="main">Après le bain</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Des</w> <w n="1.2">perles</w> <w n="1.3">encor</w> <w n="1.4">mouillent</w> <w n="1.5">son</w> <w n="1.6">bras</w> <w n="1.7">blanc</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Couchée</w> <w n="2.2">en</w> <w n="2.3">un</w> <w n="2.4">lit</w> <w n="2.5">de</w> <w n="2.6">joncs</w> <w n="2.7">verts</w> <w n="2.8">et</w> <w n="2.9">d</w>’<w n="2.10">herbes</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">sein</w> <w n="3.3">ombragé</w> <w n="3.4">d</w>’<w n="3.5">un</w> <w n="3.6">rameau</w> <w n="3.7">tremblant</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">bruissement</w> <w n="4.3">des</w> <w n="4.4">chênes</w> <w n="4.5">superbes</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Aux</w> <w n="5.2">molles</w> <w n="5.3">rumeurs</w> <w n="5.4">des</w> <w n="5.5">halliers</w> <w n="5.6">épais</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Non</w> <w n="6.2">loin</w> <w n="6.3">de</w> <w n="6.4">la</w> <w n="6.5">source</w> <w n="6.6">elle</w> <w n="6.7">rêve</w> <w n="6.8">en</w> <w n="6.9">paix</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Tandis</w> <w n="7.2">qu</w>’<w n="7.3">au</w> <w n="7.4">revers</w> <w n="7.5">des</w> <w n="7.6">souples</w> <w n="7.7">lianes</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Sur</w> <w n="8.2">son</w> <w n="8.3">reflet</w> <w n="8.4">nu</w> <w n="8.5">se</w> <w n="8.6">figent</w> <w n="8.7">pâmés</w></l>
					<l n="9" num="1.9"><w n="9.1">Les</w> <w n="9.2">flots</w> <w n="9.3">du</w> <w n="9.4">bassin</w>, <w n="9.5">lèvres</w> <w n="9.6">diaphanes</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Sous</w> <w n="10.2">les</w> <w n="10.3">noirs</w> <w n="10.4">treillis</w> <w n="10.5">au</w> <w n="10.6">ciel</w> <w n="10.7">bleu</w> <w n="10.8">fermés</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Les</w> <w n="11.2">yeux</w> <w n="11.3">demi</w>-<w n="11.4">clos</w>, <w n="11.5">chargés</w> <w n="11.6">de</w> <w n="11.7">paresse</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Elle</w> <w n="12.2">se</w> <w n="12.3">renverse</w>, <w n="12.4">écoute</w>, <w n="12.5">et</w> <w n="12.6">caresse</w></l>
					<l n="13" num="1.13"><w n="13.1">D</w>’<w n="13.2">un</w> <w n="13.3">baiser</w> <w n="13.4">brûlant</w> <w n="13.5">et</w> <w n="13.6">vague</w> <w n="13.7">à</w> <w n="13.8">la</w> <w n="13.9">fois</w></l>
					<l n="14" num="1.14"><w n="14.1">Le</w> <w n="14.2">souffle</w> <w n="14.3">lointain</w> <w n="14.4">qui</w> <w n="14.5">monte</w> <w n="14.6">et</w> <w n="14.7">qui</w> <w n="14.8">passe</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Immense</w> <w n="15.2">soupir</w> <w n="15.3">amoureux</w> <w n="15.4">des</w> <w n="15.5">bois</w>.</l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">tout</w> <w n="16.3">souvenir</w> <w n="16.4">en</w> <w n="16.5">son</w> <w n="16.6">cœur</w> <w n="16.7">s</w>’<w n="16.8">efface</w> ;</l>
					<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">sous</w> <w n="17.3">le</w> <w n="17.4">réseau</w> <w n="17.5">des</w> <w n="17.6">parfums</w> <w n="17.7">flottants</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Dans</w> <w n="18.2">l</w>’<w n="18.3">oubli</w> <w n="18.4">des</w> <w n="18.5">dieux</w>, <w n="18.6">du</w> <w n="18.7">monde</w> <w n="18.8">et</w> <w n="18.9">du</w> <w n="18.10">temps</w>,</l>
					<l n="19" num="1.19"><w n="19.1">Morte</w> <w n="19.2">au</w> <w n="19.3">vain</w> <w n="19.4">souci</w> <w n="19.5">du</w> <w n="19.6">désir</w> <w n="19.7">frivole</w>,</l>
					<l n="20" num="1.20"><w n="20.1">En</w> <w n="20.2">libres</w> <w n="20.3">essaims</w> <w n="20.4">de</w> <w n="20.5">songes</w> <w n="20.6">épars</w>,</l>
					<l n="21" num="1.21"><w n="21.1">Son</w> <w n="21.2">âme</w> <w n="21.3">à</w> <w n="21.4">travers</w> <w n="21.5">les</w> <w n="21.6">taillis</w> <w n="21.7">s</w>’<w n="21.8">envole</w>.</l>
					<l n="22" num="1.22"><w n="22.1">Autour</w> <w n="22.2">des</w> <w n="22.3">buissons</w>, <w n="22.4">sur</w> <w n="22.5">les</w> <w n="22.6">nénuphars</w>,</l>
					<l n="23" num="1.23"><w n="23.1">Ne</w> <w n="23.2">bourdonne</w> <w n="23.3">plus</w> <w n="23.4">l</w>’<w n="23.5">abeille</w> <w n="23.6">assouvie</w>,</l>
					<l n="24" num="1.24"><w n="24.1">Et</w> <w n="24.2">partout</w> <w n="24.3">s</w>’<w n="24.4">éloigne</w> <w n="24.5">ou</w> <w n="24.6">s</w>’<w n="24.7">endort</w> <w n="24.8">la</w> <w n="24.9">vie</w>.</l>
					<l n="25" num="1.25"><w n="25.1">Ils</w> <w n="25.2">ne</w> <w n="25.3">chantent</w> <w n="25.4">plus</w>, <w n="25.5">les</w> <w n="25.6">oiseaux</w> <w n="25.7">siffleurs</w> ;</l>
					<l n="26" num="1.26"><w n="26.1">Et</w> <w n="26.2">vers</w> <w n="26.3">ce</w> <w n="26.4">beau</w> <w n="26.5">corps</w> <w n="26.6">teint</w> <w n="26.7">de</w> <w n="26.8">flammes</w> <w n="26.9">roses</w>,</l>
					<l n="27" num="1.27"><w n="27.1">De</w> <w n="27.2">tous</w> <w n="27.3">les</w> <w n="27.4">côtés</w> <w n="27.5">se</w> <w n="27.6">penchent</w> <w n="27.7">les</w> <w n="27.8">fleurs</w>,</l>
					<l n="28" num="1.28"><w n="28.1">Semblables</w> <w n="28.2">aux</w> <w n="28.3">yeux</w> <w n="28.4">agrandis</w> <w n="28.5">des</w> <w n="28.6">choses</w>.</l>
				</lg>
			</div></body></text></TEI>