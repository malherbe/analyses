<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES ET POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2449 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Et Poésies</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxpoemesetpoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1864">1864</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX4">
				<head type="main">L’Image</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">terre</w> <w n="1.3">dans</w> <w n="1.4">le</w> <w n="1.5">ciel</w> <w n="1.6">promène</w></l>
					<l n="2" num="1.2"><w n="2.1">Sa</w> <w n="2.2">face</w> <w n="2.3">où</w> <w n="2.4">vit</w> <w n="2.5">l</w>’<w n="2.6">humanité</w>.</l>
					<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">terre</w> <w n="3.3">va</w> ; <w n="3.4">la</w> <w n="3.5">vie</w> <w n="3.6">humaine</w></l>
					<l n="4" num="1.4"><w n="4.1">Ronge</w> <w n="4.2">son</w> <w n="4.3">crâne</w> <w n="4.4">tourmenté</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">hommes</w> <w n="5.3">courent</w> <w n="5.4">à</w> <w n="5.5">leurs</w> <w n="5.6">quêtes</w></l>
					<l n="6" num="2.2"><w n="6.1">Sur</w> <w n="6.2">la</w> <w n="6.3">terre</w>, <w n="6.4">ardents</w> <w n="6.5">et</w> <w n="6.6">pressés</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Comme</w> <w n="7.2">aux</w> <w n="7.3">vieux</w> <w n="7.4">masques</w> <w n="7.5">des</w> <w n="7.6">coquettes</w></l>
					<l n="8" num="2.4"><w n="8.1">S</w>’<w n="8.2">obstinent</w> <w n="8.3">les</w> <w n="8.4">anciens</w> <w n="8.5">pensers</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">terre</w> <w n="9.3">est</w> <w n="9.4">vieille</w> <w n="9.5">et</w> <w n="9.6">décrépite</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">rêve</w> <w n="10.3">encor</w>, <w n="10.4">spectre</w> <w n="10.5">blafard</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">terre</w> <w n="11.3">croit</w> <w n="11.4">qu</w>’<w n="11.5">un</w> <w n="11.6">cœur</w> <w n="11.7">palpite</w></l>
					<l n="12" num="3.4"><w n="12.1">Entre</w> <w n="12.2">ses</w> <w n="12.3">os</w> <w n="12.4">couverts</w> <w n="12.5">de</w> <w n="12.6">fard</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Chaque</w> <w n="13.2">jour</w>, <w n="13.3">de</w> <w n="13.4">son</w> <w n="13.5">front</w> <w n="13.6">par</w> <w n="13.7">masse</w></l>
					<l n="14" num="4.2"><w n="14.1">Tombent</w> <w n="14.2">son</w> <w n="14.3">plâtre</w> <w n="14.4">et</w> <w n="14.5">ses</w> <w n="14.6">cheveux</w>.</l>
					<l n="15" num="4.3"><w n="15.1">La</w> <w n="15.2">vie</w> <w n="15.3">imbécile</w> <w n="15.4">grimace</w>,</l>
					<l n="16" num="4.4"><w n="16.1">S</w>’<w n="16.2">enivrant</w> <w n="16.3">des</w> <w n="16.4">plus</w> <w n="16.5">doux</w> <w n="16.6">aveux</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">quand</w> <w n="17.3">revient</w> <w n="17.4">le</w> <w n="17.5">crépuscule</w></l>
					<l n="18" num="5.2"><w n="18.1">Traînant</w> <w n="18.2">la</w> <w n="18.3">nuit</w>, <w n="18.4">parfait</w> <w n="18.5">miroir</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Jamais</w> <w n="19.2">sous</w> <w n="19.3">l</w>’<w n="19.4">horreur</w> <w n="19.5">ne</w> <w n="19.6">recule</w></l>
					<l n="20" num="5.4"><w n="20.1">La</w> <w n="20.2">terre</w> <w n="20.3">qui</w> <w n="20.4">ne</w> <w n="20.5">veut</w> <w n="20.6">pas</w> <w n="20.7">voir</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1">— <w n="21.1">Le</w> <w n="21.2">temps</w> <w n="21.3">d</w>’<w n="21.4">un</w> <w n="21.5">bras</w> <w n="21.6">robuste</w> <w n="21.7">enserre</w></l>
					<l n="22" num="6.2"><w n="22.1">Ta</w> <w n="22.2">carcasse</w>, <w n="22.3">et</w> <w n="22.4">la</w> <w n="22.5">fait</w> <w n="22.6">craquer</w> !</l>
					<l n="23" num="6.3"><w n="23.1">Regarde</w> <w n="23.2">enfin</w> <w n="23.3">d</w>’<w n="23.4">un</w> <w n="23.5">œil</w> <w n="23.6">sincère</w></l>
					<l n="24" num="6.4"><w n="24.1">Là</w>-<w n="24.2">haut</w> <w n="24.3">ton</w> <w n="24.4">corps</w> <w n="24.5">se</w> <w n="24.6">décalquer</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">C</w>’<w n="25.2">est</w> <w n="25.3">trop</w> <w n="25.4">longtemps</w> <w n="25.5">te</w> <w n="25.6">rendre</w> <w n="25.7">hommage</w></l>
					<l n="26" num="7.2"><w n="26.1">Sous</w> <w n="26.2">ton</w> <w n="26.3">reflet</w> <w n="26.4">morne</w> <w n="26.5">et</w> <w n="26.6">hideux</w>.</l>
					<l n="27" num="7.3"><w n="27.1">Reconnais</w>-<w n="27.2">toi</w> <w n="27.3">dans</w> <w n="27.4">ton</w> <w n="27.5">image</w> ;</l>
					<l n="28" num="7.4"><w n="28.1">Confrontez</w>-<w n="28.2">vous</w> <w n="28.3">toutes</w> <w n="28.4">les</w> <w n="28.5">deux</w> :</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">O</w> <w n="29.2">terre</w> <w n="29.3">lasse</w> ! <w n="29.4">ô</w> <w n="29.5">lune</w> <w n="29.6">inerte</w> !</l>
					<l n="30" num="8.2"><w n="30.1">Foyer</w> <w n="30.2">mourant</w> ! <w n="30.3">Cendre</w> <w n="30.4">des</w> <w n="30.5">morts</w> !</l>
					<l n="31" num="8.3"><w n="31.1">Toi</w>, <w n="31.2">que</w> <w n="31.3">partout</w> <w n="31.4">l</w>’<w n="31.5">espoir</w> <w n="31.6">déserte</w> !</l>
					<l n="32" num="8.4"><w n="32.1">Toi</w>, <w n="32.2">qui</w> <w n="32.3">n</w>’<w n="32.4">as</w> <w n="32.5">plus</w> <w n="32.6">même</w> <w n="32.7">un</w> <w n="32.8">remords</w> !</l>
				</lg>
			</div></body></text></TEI>