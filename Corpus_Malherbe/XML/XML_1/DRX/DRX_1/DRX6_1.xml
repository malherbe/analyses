<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÈMES ET POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DRX">
					<name>
						<forename>Léon</forename>
						<surname>DIERX</surname>
					</name>
					<date from="1838" to="1912">1838-1912</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement métrique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2449 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DRX_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poèmes Et Poésies</title>
						<author>Léon Dierx</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/leondierxpoemesetpoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Œuvres complètes, tome I</title>
						<author>Léon Dierx</author>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k2722190.r=l%C3%A9on+dierx.langFR</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ALPHONSE LEMERRE ÉDITEUR</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1864">1864</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les majuscules accentuées ont été vérifiées.</p>
				<p>Les majuscules en début de vers ont été restituées</p>
				<p>Le formatage strophique a été rétabli</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DRX6">
				<head type="main">Salvator Rosa</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Qu</w>’<w n="1.2">avais</w>-<w n="1.3">tu</w> <w n="1.4">dans</w> <w n="1.5">l</w>’<w n="1.6">esprit</w>, <w n="1.7">maître</w> <w n="1.8">à</w> <w n="1.9">la</w> <w n="1.10">brosse</w> <w n="1.11">ardente</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">que</w> <w n="2.3">sous</w> <w n="2.4">ton</w> <w n="2.5">pinceau</w> <w n="2.6">la</w> <w n="2.7">nature</w> <w n="2.8">en</w> <w n="2.9">fureur</w></l>
					<l n="3" num="1.3"><w n="3.1">Semble</w> <w n="3.2">jeter</w> <w n="3.3">au</w> <w n="3.4">ciel</w> <w n="3.5">une</w> <w n="3.6">insulte</w> <w n="3.7">stridente</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Ou</w> <w n="4.2">frémir</w> <w n="4.3">dans</w> <w n="4.4">l</w>’<w n="4.5">effroi</w> <w n="4.6">de</w> <w n="4.7">sa</w> <w n="4.8">sinistre</w> <w n="4.9">horreur</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pourquoi</w> <w n="5.2">dédaignais</w>-<w n="5.3">tu</w> <w n="5.4">les</w> <w n="5.5">calmes</w> <w n="5.6">paysages</w></l>
					<l n="6" num="2.2"><w n="6.1">Dans</w> <w n="6.2">la</w> <w n="6.3">lumière</w> <w n="6.4">au</w> <w n="6.5">loin</w> <w n="6.6">ourlant</w> <w n="6.7">leurs</w> <w n="6.8">horizons</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Les</w> <w n="7.2">lacs</w> <w n="7.3">d</w>’<w n="7.4">azur</w> <w n="7.5">limpide</w>, <w n="7.6">et</w> <w n="7.7">sur</w> <w n="7.8">de</w> <w n="7.9">frais</w> <w n="7.10">visages</w></l>
					<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">ombre</w> <w n="8.3">du</w> <w n="8.4">vert</w> <w n="8.5">printemps</w> <w n="8.6">qui</w> <w n="8.7">fleurit</w> <w n="8.8">les</w> <w n="8.9">gazons</w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">te</w> <w n="9.3">fallait</w> <w n="9.4">à</w> <w n="9.5">toi</w> <w n="9.6">l</w>’<w n="9.7">atmosphère</w> <w n="9.8">d</w>’<w n="9.9">orage</w> ;</l>
					<l n="10" num="3.2"><w n="10.1">Quelque</w> <w n="10.2">ravin</w> <w n="10.3">bien</w> <w n="10.4">noir</w> <w n="10.5">où</w> <w n="10.6">mugisse</w> <w n="10.7">un</w> <w n="10.8">torrent</w></l>
					<l n="11" num="3.3"><w n="11.1">Qui</w> <w n="11.2">boit</w> <w n="11.3">et</w> <w n="11.4">revomit</w> <w n="11.5">l</w>’<w n="11.6">écume</w> <w n="11.7">de</w> <w n="11.8">sa</w> <w n="11.9">rage</w> ;</l>
					<l n="12" num="3.4"><w n="12.1">Quelque</w> <w n="12.2">fauve</w> <w n="12.3">bandit</w> <w n="12.4">sur</w> <w n="12.5">des</w> <w n="12.6">rochers</w> <w n="12.7">errant</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L</w>’<w n="13.2">ouragan</w> <w n="13.3">qui</w> <w n="13.4">s</w>’<w n="13.5">abat</w> <w n="13.6">sur</w> <w n="13.7">tes</w> <w n="13.8">arbres</w> <w n="13.9">d</w>’<w n="13.10">automne</w></l>
					<l n="14" num="4.2"><w n="14.1">Rugissait</w>, <w n="14.2">n</w>’<w n="14.3">est</w>-<w n="14.4">ce</w> <w n="14.5">pas</w> ? <w n="14.6">Dans</w> <w n="14.7">ton</w> <w n="14.8">âme</w> <w n="14.9">de</w> <w n="14.10">fer</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Tu</w> <w n="15.2">ne</w> <w n="15.3">te</w> <w n="15.4">laissais</w> <w n="15.5">pas</w> <w n="15.6">au</w> <w n="15.7">bonheur</w> <w n="15.8">monotone</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Mais</w> <w n="16.2">aux</w> <w n="16.3">transports</w> <w n="16.4">fougueux</w> <w n="16.5">déchaînés</w> <w n="16.6">par</w> <w n="16.7">l</w>’<w n="16.8">enfer</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Ce</w> <w n="17.2">sont</w> <w n="17.3">tes</w> <w n="17.4">passions</w> <w n="17.5">qui</w> <w n="17.6">hurlent</w> <w n="17.7">sur</w> <w n="17.8">tes</w> <w n="17.9">toiles</w> ;</l>
					<l n="18" num="5.2"><w n="18.1">Toi</w>-<w n="18.2">même</w>, <w n="18.3">tu</w> <w n="18.4">t</w>’<w n="18.5">es</w> <w n="18.6">peint</w> <w n="18.7">dans</w> <w n="18.8">ces</w> <w n="18.9">lieux</w> <w n="18.10">dévastés</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Dans</w> <w n="19.2">ces</w> <w n="19.3">chênes</w> <w n="19.4">tordant</w>, <w n="19.5">sous</w> <w n="19.6">la</w> <w n="19.7">nuit</w> <w n="19.8">sans</w> <w n="19.9">étoiles</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Sur</w> <w n="20.2">l</w>’<w n="20.3">abîme</w> <w n="20.4">béant</w> <w n="20.5">leurs</w> <w n="20.6">troncs</w> <w n="20.7">décapités</w>.</l>
				</lg>
			</div></body></text></TEI>