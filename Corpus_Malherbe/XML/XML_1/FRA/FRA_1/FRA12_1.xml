<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les poèmes dorés</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>727 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Poèmes dorés</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesie-francaise.fr</publisher>
						<idno type="URL">http://www.poesie-francaise.fr/anatole-france-les-poemes-dores/</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les poèmes dorés</title>
						<author>Anatole France</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>EDOUARD-JOSEPH, EDITEUR</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1873">1873</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Remise en ordre des poèmes conforme à l’édition de référence.</p>
				<p>Les dates de fin de poème ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Les deux sections manquantes du poème "Le Désir" ont été ajoutées (édition : EDOUARD-JOSEPH, EDITEUR)</p>
				<p>Nombreuses corrections de ponctuation</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>

				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA12">
				<head type="main">Le désir</head>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">sais</w> <w n="1.3">la</w> <w n="1.4">vanité</w> <w n="1.5">de</w> <w n="1.6">tout</w> <w n="1.7">désir</w> <w n="1.8">profane</w>.</l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">peine</w> <w n="2.3">gardons</w>-<w n="2.4">nous</w> <w n="2.5">de</w> <w n="2.6">tes</w> <w n="2.7">amours</w> <w n="2.8">défunts</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Femme</w>, <w n="3.2">ce</w> <w n="3.3">que</w> <w n="3.4">la</w> <w n="3.5">fleur</w> <w n="3.6">qui</w> <w n="3.7">sur</w> <w n="3.8">ton</w> <w n="3.9">sein</w> <w n="3.10">se</w> <w n="3.11">fane</w></l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Y</w> <w n="4.2">laisse</w> <w n="4.3">d</w>’<w n="4.4">âme</w> <w n="4.5">et</w> <w n="4.6">de</w> <w n="4.7">parfums</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ils</w> <w n="5.2">n</w>’<w n="5.3">ont</w>, <w n="5.4">les</w> <w n="5.5">plus</w> <w n="5.6">beaux</w> <w n="5.7">bras</w>, <w n="5.8">que</w> <w n="5.9">des</w> <w n="5.10">chaînes</w> <w n="5.11">d</w>’<w n="5.12">argile</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Indolentes</w> <w n="6.2">autour</w> <w n="6.3">du</w> <w n="6.4">col</w> <w n="6.5">le</w> <w n="6.6">plus</w> <w n="6.7">aimé</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Avant</w> <w n="7.2">d</w>’<w n="7.3">être</w> <w n="7.4">rompu</w> <w n="7.5">leur</w> <w n="7.6">doux</w> <w n="7.7">cercle</w> <w n="7.8">fragile</w></l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">Ne</w> <w n="8.2">s</w>’<w n="8.3">était</w> <w n="8.4">pas</w> <w n="8.5">même</w> <w n="8.6">fermé</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mélancolique</w> <w n="9.2">nuit</w> <w n="9.3">des</w> <w n="9.4">chevelures</w> <w n="9.5">sombres</w>,</l>
						<l n="10" num="3.2"><w n="10.1">A</w> <w n="10.2">quoi</w> <w n="10.3">bon</w> <w n="10.4">s</w>’<w n="10.5">attarder</w> <w n="10.6">dans</w> <w n="10.7">ton</w> <w n="10.8">enivrement</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Si</w>, <w n="11.2">comme</w> <w n="11.3">dans</w> <w n="11.4">la</w> <w n="11.5">mort</w>, <w n="11.6">nul</w> <w n="11.7">ne</w> <w n="11.8">peut</w> <w n="11.9">sous</w> <w n="11.10">tes</w> <w n="11.11">ombres</w></l>
						<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">Se</w> <w n="12.2">plonger</w> <w n="12.3">éternellement</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Narines</w> <w n="13.2">qui</w> <w n="13.3">gonflez</w> <w n="13.4">vos</w> <w n="13.5">ailes</w> <w n="13.6">de</w> <w n="13.7">colombe</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Avec</w> <w n="14.2">les</w> <w n="14.3">longs</w> <w n="14.4">dédains</w> <w n="14.5">d</w>’<w n="14.6">une</w> <w n="14.7">belle</w> <w n="14.8">fierté</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Pour</w> <w n="15.2">la</w> <w n="15.3">dernière</w> <w n="15.4">fois</w>, <w n="15.5">à</w> <w n="15.6">l</w>’<w n="15.7">odeur</w> <w n="15.8">de</w> <w n="15.9">la</w> <w n="15.10">tombe</w>,</l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">Vous</w> <w n="16.2">aurez</w> <w n="16.3">déjà</w> <w n="16.4">palpité</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Lèvres</w>, <w n="17.2">vivantes</w> <w n="17.3">fleurs</w>, <w n="17.4">nobles</w> <w n="17.5">roses</w> <w n="17.6">sanglantes</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Vous</w> <w n="18.2">épanouissant</w> <w n="18.3">lorsque</w> <w n="18.4">nous</w> <w n="18.5">vous</w> <w n="18.6">baisons</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Quelques</w> <w n="19.2">feux</w> <w n="19.3">de</w> <w n="19.4">cristal</w> <w n="19.5">en</w> <w n="19.6">quelques</w> <w n="19.7">nuits</w> <w n="19.8">brûlantes</w></l>
						<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1">Sèchent</w> <w n="20.2">vos</w> <w n="20.3">brèves</w> <w n="20.4">floraisons</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Où</w> <w n="21.2">tend</w> <w n="21.3">le</w> <w n="21.4">vain</w> <w n="21.5">effort</w> <w n="21.6">de</w> <w n="21.7">deux</w> <w n="21.8">bouches</w> <w n="21.9">unies</w> ?</l>
						<l n="22" num="6.2"><w n="22.1">Le</w> <w n="22.2">plus</w> <w n="22.3">long</w> <w n="22.4">des</w> <w n="22.5">baisers</w> <w n="22.6">trompe</w> <w n="22.7">notre</w> <w n="22.8">dessein</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">comment</w> <w n="23.3">appuyer</w> <w n="23.4">nos</w> <w n="23.5">langueurs</w> <w n="23.6">infinies</w></l>
						<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">Sur</w> <w n="24.2">la</w> <w n="24.3">fragilité</w> <w n="24.4">d</w>’<w n="24.5">un</w> <w n="24.6">sein</w> ?</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="25" num="1.1"><w n="25.1">Mais</w> <w n="25.2">la</w> <w n="25.3">vague</w> <w n="25.4">beauté</w> <w n="25.5">des</w> <w n="25.6">regards</w>, <w n="25.7">d</w>’<w n="25.8">où</w> <w n="25.9">vient</w>-<w n="25.10">elle</w>,</l>
						<l n="26" num="1.2"><w n="26.1">Pour</w> <w n="26.2">nous</w> <w n="26.3">mettre</w> <w n="26.4">en</w> <w n="26.5">passant</w> <w n="26.6">tant</w> <w n="26.7">d</w>’<w n="26.8">espérance</w> <w n="26.9">au</w> <w n="26.10">front</w>.?</l>
						<l n="27" num="1.3"><w n="27.1">Et</w> <w n="27.2">pourquoi</w> <w n="27.3">rêvons</w>-<w n="27.4">nous</w> <w n="27.5">de</w> <w n="27.6">lumière</w> <w n="27.7">immortelle</w></l>
						<l n="28" num="1.4"><space quantity="8" unit="char"></space><w n="28.1">Devant</w> <w n="28.2">deux</w> <w n="28.3">yeux</w> <w n="28.4">qui</w> <w n="28.5">s</w>’<w n="28.6">éteindront</w>.</l>
					</lg>
					<lg n="2">
						<l n="29" num="2.1"><w n="29.1">Femme</w>, <w n="29.2">qui</w> <w n="29.3">vous</w> <w n="29.4">donna</w> <w n="29.5">cette</w> <w n="29.6">clarté</w> <w n="29.7">sacrée</w></l>
						<l n="30" num="2.2"><w n="30.1">Dont</w> <w n="30.2">vous</w> <w n="30.3">avez</w> <w n="30.4">béni</w> <w n="30.5">la</w> <w n="30.6">ferveur</w> <w n="30.7">de</w> <w n="30.8">mes</w> <w n="30.9">yeux</w> ?</l>
						<l n="31" num="2.3"><w n="31.1">Et</w> <w n="31.2">d</w>’<w n="31.3">où</w> <w n="31.4">vient</w> <w n="31.5">qu</w>’<w n="31.6">en</w> <w n="31.7">suivant</w> <w n="31.8">votre</w> <w n="31.9">trace</w> <w n="31.10">adorée</w></l>
						<l n="32" num="2.4"><space quantity="8" unit="char"></space><w n="32.1">Je</w> <w n="32.2">sens</w> <w n="32.3">un</w> <w n="32.4">dieu</w> <w n="32.5">mystérieux</w></l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="33" num="1.1"><space quantity="8" unit="char"></space><w n="33.1">Oh</w> ! <w n="33.2">montrez</w> <w n="33.3">un</w> <w n="33.4">moment</w> <w n="33.5">au</w> <w n="33.6">monde</w></l>
						<l n="34" num="1.2"><space quantity="8" unit="char"></space><w n="34.1">Votre</w> <w n="34.2">fragilité</w> <w n="34.3">féconde</w>,</l>
						<l n="35" num="1.3"><space quantity="8" unit="char"></space><w n="35.1">Et</w> <w n="35.2">semez</w> <w n="35.3">la</w> <w n="35.4">vie</w> <w n="35.5">à</w> <w n="35.6">vos</w> <w n="35.7">pieds</w> !</l>
						<l n="36" num="1.4"><space quantity="8" unit="char"></space><w n="36.1">Puis</w> <w n="36.2">passez</w>, <w n="36.3">formes</w> <w n="36.4">éphémères</w>;</l>
						<l n="37" num="1.5"><space quantity="8" unit="char"></space><w n="37.1">Femmes</w>, <w n="37.2">puisque</w> <w n="37.3">vous</w> <w n="37.4">êtes</w> <w n="37.5">mères</w>,</l>
						<l n="38" num="1.6"><space quantity="8" unit="char"></space><w n="38.1">C</w>’<w n="38.2">est</w> <w n="38.3">qu</w>’<w n="38.4">il</w> <w n="38.5">convient</w> <w n="38.6">que</w> <w n="38.7">vous</w> <w n="38.8">mouriez</w>.</l>
					</lg>
					<lg n="2">
						<l n="39" num="2.1"><space quantity="8" unit="char"></space><w n="39.1">Votre</w> <w n="39.2">divinité</w> <w n="39.3">ne</w> <w n="39.4">dure</w>,</l>
						<l n="40" num="2.2"><space quantity="8" unit="char"></space><w n="40.1">Douces</w> <w n="40.2">forces</w> <w n="40.3">de</w> <w n="40.4">la</w> <w n="40.5">Nature</w>,</l>
						<l n="41" num="2.3"><space quantity="8" unit="char"></space><w n="41.1">Que</w> <w n="41.2">ce</w> <w n="41.3">qu</w>’<w n="41.4">il</w> <w n="41.5">faut</w> <w n="41.6">pour</w> <w n="41.7">son</w> <w n="41.8">dessein</w>.</l>
						<l n="42" num="2.4"><space quantity="8" unit="char"></space><w n="42.1">La</w> <w n="42.2">race</w> <w n="42.3">impérissable</w> <w n="42.4">et</w> <w n="42.5">belle</w>,</l>
						<l n="43" num="2.5"><space quantity="8" unit="char"></space><w n="43.1">Voilà</w> <w n="43.2">cette</w> <w n="43.3">chose</w> <w n="43.4">immortelle</w></l>
						<l n="44" num="2.6"><space quantity="8" unit="char"></space><w n="44.1">Que</w> <w n="44.2">l</w>’<w n="44.3">on</w> <w n="44.4">rêve</w> <w n="44.5">sur</w> <w n="44.6">votre</w> <w n="44.7">sein</w> !</l>
					</lg>
					<lg n="3">
						<l n="45" num="3.1"><space quantity="8" unit="char"></space><w n="45.1">C</w>’<w n="45.2">est</w> <w n="45.3">par</w> <w n="45.4">vous</w> <w n="45.5">que</w> <w n="45.6">l</w>’<w n="45.7">heureuse</w> <w n="45.8">vie</w></l>
						<l n="46" num="3.2"><space quantity="8" unit="char"></space><w n="46.1">Tour</w> <w n="46.2">à</w> <w n="46.3">tour</w> <w n="46.4">en</w> <w n="46.5">la</w> <w n="46.6">chair</w> <w n="46.7">ravie</w></l>
						<l n="47" num="3.3"><space quantity="8" unit="char"></space><w n="47.1">S</w>’<w n="47.2">allume</w>, <w n="47.3">et</w> <w n="47.4">ne</w> <w n="47.5">s</w>’<w n="47.6">éteindra</w> <w n="47.7">pas</w>.</l>
						<l n="48" num="3.4"><space quantity="8" unit="char"></space><w n="48.1">En</w> <w n="48.2">vous</w> <w n="48.3">la</w> <w n="48.4">vie</w> <w n="48.5">universelle</w></l>
						<l n="49" num="3.5"><space quantity="8" unit="char"></space><w n="49.1">Éclate</w>, <w n="49.2">et</w> <w n="49.3">tout</w> <w n="49.4">homme</w> <w n="49.5">chancelle</w>,</l>
						<l n="50" num="3.6"><space quantity="8" unit="char"></space><w n="50.1">Ivre</w> <w n="50.2">de</w> <w n="50.3">beauté</w>, <w n="50.4">sur</w> <w n="50.5">vos</w> <w n="50.6">pas</w>.</l>
					</lg>
					<lg n="4">
						<l n="51" num="4.1"><space quantity="8" unit="char"></space><w n="51.1">Vivez</w>, <w n="51.2">mourez</w>, <w n="51.3">pleines</w> <w n="51.4">de</w> <w n="51.5">grâce</w>;</l>
						<l n="52" num="4.2"><space quantity="8" unit="char"></space><w n="52.1">Les</w> <w n="52.2">hommes</w> <w n="52.3">et</w> <w n="52.4">les</w> <w n="52.5">dieux</w>, <w n="52.6">tout</w> <w n="52.7">passe</w></l>
						<l n="53" num="4.3"><space quantity="8" unit="char"></space><w n="53.1">Mais</w> <w n="53.2">la</w> <w n="53.3">vie</w> <w n="53.4">existe</w> <w n="53.5">à</w> <w n="53.6">jamais</w>.</l>
						<l n="54" num="4.4"><space quantity="8" unit="char"></space><w n="54.1">Et</w> <w n="54.2">toi</w>, <w n="54.3">forme</w>, <w n="54.4">parfum</w>, <w n="54.5">lumière</w>,</l>
						<l n="55" num="4.5"><space quantity="8" unit="char"></space><w n="55.1">Qui</w> <w n="55.2">fleuris</w> <w n="55.3">ma</w> <w n="55.4">vertu</w> <w n="55.5">première</w>,</l>
						<l n="56" num="4.6"><space quantity="8" unit="char"></space><w n="56.1">Ah</w>! <w n="56.2">je</w> <w n="56.3">sais</w> <w n="56.4">pourquoi</w> <w n="56.5">je</w> <w n="56.6">t</w>’<w n="56.7">aimais</w>!</l>
					</lg>
				</div>
				<closer>
					<dateline>
						<date when="1869">Juin 1869</date>.
					</dateline>
				</closer>
			</div></body></text></TEI>