<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES ET LÉGENDES</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>860 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/anatolfrancepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Anatole_France_-_Po%C3%A9sies.djvu</idno>
					</publicationStmt>
					<sourceDesc>
					<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les pièces manquantes ont été ajoutées à partir de Wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA20">
				<head type="main">La Fille de Caïn</head>
				<opener>
					<epigraph>
						<cit>
							<quote>
								Hark, hark ! the sea-birds cry !… <lb></lb>
								In the sun’s place a pale and ghastly glare <lb></lb>
								Hath wound itself around the dying air.
							</quote>
							<bibl>
								<name>LORD BYRON</name>, <hi rend="ital"> Heaven and Earth</hi>.
							</bibl>
						</cit>
					</epigraph>
				</opener>
				<div type="section" n="1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">matin</w> <w n="1.3">de</w> <w n="1.4">ces</w> <w n="1.5">temps</w> <w n="1.6">où</w> <w n="1.7">des</w> <w n="1.8">hymens</w> <w n="1.9">étranges</w></l>
						<l n="2" num="1.2"><w n="2.1">Aux</w> <w n="2.2">filles</w> <w n="2.3">de</w> <w n="2.4">Caïn</w> <w n="2.5">mêlaient</w> <w n="2.6">les</w> <w n="2.7">pâles</w> <w n="2.8">Anges</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Azraël</w> <w n="3.2">quitta</w> <w n="3.3">Dieu</w> <w n="3.4">pour</w> <w n="3.5">Oholibama</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Elle</w> <w n="4.2">le</w> <w n="4.3">vit</w> <w n="4.4">pleurer</w> <w n="4.5">près</w> <w n="4.6">du</w> <w n="4.7">puits</w>, <w n="4.8">et</w> <w n="4.9">l</w>’<w n="4.10">aima</w>.</l>
						<l n="5" num="1.5">« <w n="5.1">Ô</w> <w n="5.2">toi</w> <w n="5.3">qui</w> <w n="5.4">souffres</w>, <w n="5.5">viens</w>, <w n="5.6">dit</w> <w n="5.7">la</w> <w n="5.8">fille</w> <w n="5.9">des</w> <w n="5.10">hommes</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">Qu</w>’<w n="6.2">importe</w>, <w n="6.3">ange</w> <w n="6.4">ou</w> <w n="6.5">démon</w>, <w n="6.6">le</w> <w n="6.7">nom</w> <w n="6.8">dont</w> <w n="6.9">tu</w> <w n="6.10">te</w> <w n="6.11">nommes</w> :</l>
						<l n="7" num="1.7"><w n="7.1">Ton</w> <w n="7.2">front</w> <w n="7.3">est</w> <w n="7.4">triste</w> <w n="7.5">et</w> <w n="7.6">fier</w> <w n="7.7">et</w> <w n="7.8">tes</w> <w n="7.9">yeux</w> <w n="7.10">sont</w> <w n="7.11">de</w> <w n="7.12">feu</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">En</w> <w n="8.2">te</w> <w n="8.3">voyant</w> <w n="8.4">si</w> <w n="8.5">beau</w>, <w n="8.6">je</w> <w n="8.7">te</w> <w n="8.8">préfère</w> <w n="8.9">à</w> <w n="8.10">Dieu</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Esprit</w>, <w n="9.2">puisqu</w>’<w n="9.3">il</w> <w n="9.4">te</w> <w n="9.5">plaît</w> <w n="9.6">d</w>’<w n="9.7">aimer</w> <w n="9.8">l</w>’<w n="9.9">argile</w> <w n="9.10">aimante</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Je</w> <w n="10.2">livre</w> <w n="10.3">à</w> <w n="10.4">ton</w> <w n="10.5">étreinte</w> <w n="10.6">effroyable</w> <w n="10.7">et</w> <w n="10.8">charmante</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Ô</w> <w n="11.2">ma</w> <w n="11.3">vie</w> <w n="11.4">et</w> <w n="11.5">ma</w> <w n="11.6">mort</w>, <w n="11.7">fils</w> <w n="11.8">révolté</w> <w n="11.9">du</w> <w n="11.10">jour</w> !</l>
						<l n="12" num="1.12"><w n="12.1">Tout</w> <w n="12.2">mon</w> <w n="12.3">être</w> <w n="12.4">qui</w> <w n="12.5">va</w> <w n="12.6">périr</w> <w n="12.7">de</w> <w n="12.8">ton</w> <w n="12.9">amour</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Ma</w> <w n="13.2">terrestre</w> <w n="13.3">beauté</w> <w n="13.4">dont</w> <w n="13.5">je</w> <w n="13.6">marchais</w> <w n="13.7">si</w> <w n="13.8">fière</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Ma</w> <w n="14.2">face</w> <w n="14.3">que</w> <w n="14.4">tes</w> <w n="14.5">yeux</w> <w n="14.6">inondent</w> <w n="14.7">de</w> <w n="14.8">lumière</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Mes</w> <w n="15.2">bras</w> <w n="15.3">et</w> <w n="15.4">leurs</w> <w n="15.5">anneaux</w>, <w n="15.6">mon</w> <w n="15.7">col</w> <w n="15.8">et</w> <w n="15.9">ses</w> <w n="15.10">colliers</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">ma</w> <w n="16.3">main</w> <w n="16.4">refusée</w> <w n="16.5">aux</w> <w n="16.6">fils</w> <w n="16.7">des</w> <w n="16.8">chameliers</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Tu</w> <w n="17.2">sauras</w>, <w n="17.3">loin</w> <w n="17.4">de</w> <w n="17.5">Dieu</w>, <w n="17.6">me</w> <w n="17.7">cacher</w> <w n="17.8">dans</w> <w n="17.9">tes</w> <w n="17.10">ailes</w>.</l>
						<l n="18" num="1.18"><w n="18.1">Nos</w> <w n="18.2">destins</w> <w n="18.3">seront</w> <w n="18.4">beaux</w> <w n="18.5">comme</w> <w n="18.6">les</w> <w n="18.7">nuits</w> <w n="18.8">sont</w> <w n="18.9">belles</w>. »</l>
					</lg>
				</div>
				<div type="section" n="2">
					<head type="number">II</head>
					<lg n="1">
						<l n="19" num="1.1"><w n="19.1">Le</w> <w n="19.2">lendemain</w>, <w n="19.3">la</w> <w n="19.4">race</w> <w n="19.5">humaine</w>, <w n="19.6">à</w> <w n="19.7">son</w> <w n="19.8">réveil</w>,</l>
						<l n="20" num="1.2"><w n="20.1">Vit</w> <w n="20.2">se</w> <w n="20.3">lever</w> <w n="20.4">la</w> <w n="20.5">mort</w> <w n="20.6">et</w> <w n="20.7">non</w> <w n="20.8">pas</w> <w n="20.9">le</w> <w n="20.10">soleil</w>.</l>
						<l n="21" num="1.3"><w n="21.1">La</w> <w n="21.2">fille</w> <w n="21.3">de</w> <w n="21.4">Caïn</w> <w n="21.5">dit</w>, <w n="21.6">près</w> <w n="21.7">de</w> <w n="21.8">la</w> <w n="21.9">fontaine</w> :</l>
						<l n="22" num="1.4">« <w n="22.1">Azraël</w>, <w n="22.2">connais</w>-<w n="22.3">tu</w> <w n="22.4">cette</w> <w n="22.5">brise</w> <w n="22.6">lointaine</w></l>
						<l n="23" num="1.5"><w n="23.1">Qui</w> <w n="23.2">vient</w> <w n="23.3">à</w> <w n="23.4">nos</w> <w n="23.5">baisers</w> <w n="23.6">mêler</w> <w n="23.7">un</w> <w n="23.8">sel</w> <w n="23.9">amer</w> ?</l>
						<l n="24" num="1.6"><w n="24.1">N</w>’<w n="24.2">entends</w>-<w n="24.3">tu</w> <w n="24.4">pas</w> <w n="24.5">crier</w> <w n="24.6">l</w>’<w n="24.7">hirondelle</w> <w n="24.8">de</w> <w n="24.9">mer</w> ?</l>
						<l n="25" num="1.7"><w n="25.1">La</w> <w n="25.2">mer</w> <w n="25.3">roule</w> <w n="25.4">vers</w> <w n="25.5">nous</w> <w n="25.6">et</w> <w n="25.7">c</w>’<w n="25.8">est</w> <w n="25.9">Dieu</w> <w n="25.10">qui</w> <w n="25.11">la</w> <w n="25.12">mène</w>.</l>
						<l n="26" num="1.8"><w n="26.1">Nous</w> <w n="26.2">redonnions</w> <w n="26.3">Éden</w> <w n="26.4">à</w> <w n="26.5">la</w> <w n="26.6">famille</w> <w n="26.7">humaine</w> !</l>
						<l n="27" num="1.9"><w n="27.1">Éden</w>, <w n="27.2">sous</w> <w n="27.3">nos</w> <w n="27.4">baisers</w>, <w n="27.5">refleurissait</w> <w n="27.6">plus</w> <w n="27.7">cher</w> !</l>
						<l n="28" num="1.10"><w n="28.1">Nous</w> <w n="28.2">avions</w> <w n="28.3">rétabli</w> <w n="28.4">la</w> <w n="28.5">gloire</w> <w n="28.6">de</w> <w n="28.7">la</w> <w n="28.8">chair</w> !</l>
						<l n="29" num="1.11"><w n="29.1">Mais</w> <w n="29.2">Dieu</w> !… <w n="29.3">Réjouis</w>-<w n="29.4">toi</w>, <w n="29.5">Caïn</w>, <w n="29.6">dans</w> <w n="29.7">ta</w> <w n="29.8">semence</w> :</l>
						<l n="30" num="1.12"><w n="30.1">Entre</w> <w n="30.2">la</w> <w n="30.3">femme</w> <w n="30.4">et</w> <w n="30.5">Dieu</w> <w n="30.6">la</w> <w n="30.7">lutte</w> <w n="30.8">recommence</w>…</l>
						<l n="31" num="1.13"><w n="31.1">Sur</w> <w n="31.2">la</w> <w n="31.3">terre</w> <w n="31.4">ébranlée</w> <w n="31.5">où</w> <w n="31.6">tendent</w> <w n="31.7">mes</w> <w n="31.8">genoux</w></l>
						<l n="32" num="1.14"><w n="32.1">Entends</w>-<w n="32.2">tu</w> <w n="32.3">les</w> <w n="32.4">démons</w> <w n="32.5">captifs</w> <w n="32.6">rire</w> <w n="32.7">de</w> <w n="32.8">nous</w> ?</l>
						<l n="33" num="1.15"><w n="33.1">Quelle</w> <w n="33.2">effroyable</w> <w n="33.3">nuit</w> <w n="33.4">roule</w> <w n="33.5">de</w> <w n="33.6">cime</w> <w n="33.7">en</w> <w n="33.8">cime</w> ! »</l>
					</lg>
					<lg n="2">
						<l n="34" num="2.1"><w n="34.1">Les</w> <w n="34.2">eaux</w> <w n="34.3">avaient</w> <w n="34.4">rompu</w> <w n="34.5">les</w> <w n="34.6">sources</w> <w n="34.7">de</w> <w n="34.8">l</w>’<w n="34.9">abîme</w> ;</l>
						<l n="35" num="2.2"><w n="35.1">Les</w> <w n="35.2">antiques</w> <w n="35.3">granits</w>, <w n="35.4">de</w> <w n="35.5">leurs</w> <w n="35.6">flancs</w> <w n="35.7">entr</w>’<w n="35.8">ouverts</w>,</l>
						<l n="36" num="2.3"><w n="36.1">Lançaient</w> <w n="36.2">des</w> <w n="36.3">gerbes</w> <w n="36.4">d</w>’<w n="36.5">eau</w>, <w n="36.6">de</w> <w n="36.7">fumée</w> <w n="36.8">et</w> <w n="36.9">d</w>’<w n="36.10">éclairs</w> ;</l>
						<l n="37" num="2.4"><w n="37.1">Et</w> <w n="37.2">bientôt</w>, <w n="37.3">dans</w> <w n="37.4">l</w>’<w n="37.5">horreur</w> <w n="37.6">des</w> <w n="37.7">ténèbres</w> <w n="37.8">compactes</w>,</l>
						<l n="38" num="2.5"><w n="38.1">Le</w> <w n="38.2">ciel</w> <w n="38.3">du</w> <w n="38.4">Dieu</w> <w n="38.5">jaloux</w> <w n="38.6">ouvrit</w> <w n="38.7">ses</w> <w n="38.8">cataractes</w>.</l>
						<l n="39" num="2.6"><w n="39.1">Sur</w> <w n="39.2">les</w> <w n="39.3">plaines</w> <w n="39.4">où</w> <w n="39.5">sont</w> <w n="39.6">les</w> <w n="39.7">tentes</w> <w n="39.8">des</w> <w n="39.9">pasteurs</w>,</l>
						<l n="40" num="2.7"><w n="40.1">Sur</w> <w n="40.2">les</w> <w n="40.3">sombres</w> <w n="40.4">forêts</w> <w n="40.5">et</w> <w n="40.6">les</w> <w n="40.7">pins</w> <w n="40.8">des</w> <w n="40.9">hauteurs</w>,</l>
						<l n="41" num="2.8"><w n="41.1">Sur</w> <w n="41.2">les</w> <w n="41.3">grandes</w> <w n="41.4">cités</w> <w n="41.5">aux</w> <w n="41.6">enceintes</w> <w n="41.7">de</w> <w n="41.8">brique</w></l>
						<l n="42" num="2.9"><w n="42.1">Où</w> <w n="42.2">l</w>’<w n="42.3">homme</w> <w n="42.4">rend</w> <w n="42.5">hommage</w> <w n="42.6">aux</w> <w n="42.7">Démons</w> <w n="42.8">et</w> <w n="42.9">fabrique</w>,</l>
						<l n="43" num="2.10"><w n="43.1">Près</w> <w n="43.2">des</w> <w n="43.3">fleuves</w> <w n="43.4">fangeux</w>, <w n="43.5">dans</w> <w n="43.6">de</w> <w n="43.7">noirs</w> <w n="43.8">ateliers</w>,</l>
						<l n="44" num="2.11"><w n="44.1">Les</w> <w n="44.2">étoffes</w> <w n="44.3">de</w> <w n="44.4">lin</w>, <w n="44.5">les</w> <w n="44.6">anneaux</w>, <w n="44.7">les</w> <w n="44.8">colliers</w>,</l>
						<l n="45" num="2.12"><w n="45.1">Les</w> <w n="45.2">grands</w> <w n="45.3">couteaux</w> <w n="45.4">de</w> <w n="45.5">bronze</w> <w n="45.6">et</w> <w n="45.7">les</w> <w n="45.8">flèches</w> <w n="45.9">de</w> <w n="45.10">pierre</w>,</l>
						<l n="46" num="2.13"><w n="46.1">Où</w> <w n="46.2">les</w> <w n="46.3">fils</w> <w n="46.4">de</w> <w n="46.5">Caïn</w>, <w n="46.6">race</w> <w n="46.7">maudite</w> <w n="46.8">et</w> <w n="46.9">fière</w>,</l>
						<l n="47" num="2.14"><w n="47.1">Lisent</w> <w n="47.2">au</w> <w n="47.3">ciel</w> <w n="47.4">changeant</w> <w n="47.5">sur</w> <w n="47.6">le</w> <w n="47.7">faîte</w> <w n="47.8">des</w> <w n="47.9">tours</w>,</l>
						<l n="48" num="2.15"><w n="48.1">L</w>’<w n="48.2">eau</w>, <w n="48.3">par</w> <w n="48.4">nappes</w>, <w n="48.5">tomba</w> <w n="48.6">durant</w> <w n="48.7">quarante</w> <w n="48.8">jours</w>,</l>
						<l n="49" num="2.16"><w n="49.1">Et</w> <w n="49.2">le</w> <w n="49.3">vent</w> <w n="49.4">souffla</w> <w n="49.5">tel</w> <w n="49.6">que</w> <w n="49.7">des</w> <w n="49.8">brisants</w> <w n="49.9">humides</w></l>
						<l n="50" num="2.17"><w n="50.1">Heurtaient</w> <w n="50.2">les</w> <w n="50.3">sept</w> <w n="50.4">degrés</w> <w n="50.5">des</w> <w n="50.6">hautes</w> <w n="50.7">pyramides</w>.</l>
						<l n="51" num="2.18"><w n="51.1">Les</w> <w n="51.2">fauves</w>, <w n="51.3">les</w> <w n="51.4">humains</w>, <w n="51.5">la</w> <w n="51.6">troupe</w> <w n="51.7">des</w> <w n="51.8">vivants</w></l>
						<l n="52" num="2.19"><w n="52.1">Gagna</w> <w n="52.2">les</w> <w n="52.3">pics</w> <w n="52.4">neigeux</w> <w n="52.5">sous</w> <w n="52.6">la</w> <w n="52.7">foudre</w> <w n="52.8">mouvants</w>.</l>
						<l n="53" num="2.20"><w n="53.1">Et</w> <w n="53.2">les</w> <w n="53.3">géants</w> <w n="53.4">debout</w> <w n="53.5">et</w> <w n="53.6">les</w> <w n="53.7">vierges</w> <w n="53.8">voilées</w>,</l>
						<l n="54" num="2.21"><w n="54.1">Les</w> <w n="54.2">mères</w> <w n="54.3">qui</w> <w n="54.4">tendaient</w> <w n="54.5">leurs</w> <w n="54.6">mamelles</w> <w n="54.7">gonflées</w></l>
						<l n="55" num="2.22"><w n="55.1">À</w> <w n="55.2">leurs</w> <w n="55.3">petits</w> <w n="55.4">enfants</w> <w n="55.5">aux</w> <w n="55.6">yeux</w> <w n="55.7">clos</w>, <w n="55.8">les</w> <w n="55.9">vieillards</w></l>
						<l n="56" num="2.23"><w n="56.1">Inertes</w>, <w n="56.2">et</w> <w n="56.3">du</w> <w n="56.4">fond</w> <w n="56.5">de</w> <w n="56.6">leurs</w> <w n="56.7">yeux</w> <w n="56.8">sans</w> <w n="56.9">regards</w></l>
						<l n="57" num="2.24"><w n="57.1">Pleurant</w> <w n="57.2">leurs</w> <w n="57.3">jours</w> <w n="57.4">de</w> <w n="57.5">paix</w> <w n="57.6">et</w> <w n="57.7">leurs</w> <w n="57.8">longues</w> <w n="57.9">mémoires</w>,</l>
						<l n="58" num="2.25"><w n="58.1">Les</w> <w n="58.2">chefs</w> <w n="58.3">portant</w> <w n="58.4">la</w> <w n="58.5">lance</w>, <w n="58.6">et</w> <w n="58.7">les</w> <w n="58.8">esclaves</w> <w n="58.9">noires</w>,</l>
						<l n="59" num="2.26"><w n="59.1">Les</w> <w n="59.2">marchands</w> <w n="59.3">étrangers</w> <w n="59.4">venus</w> <w n="59.5">sur</w> <w n="59.6">leurs</w> <w n="59.7">chameaux</w>,</l>
						<l n="60" num="2.27"><w n="60.1">Et</w> <w n="60.2">les</w> <w n="60.3">prêtres</w> <w n="60.4">savants</w> <w n="60.5">à</w> <w n="60.6">conjurer</w> <w n="60.7">les</w> <w n="60.8">maux</w>,</l>
						<l n="61" num="2.28"><w n="61.1">Sous</w> <w n="61.2">le</w> <w n="61.3">choc</w> <w n="61.4">écumant</w> <w n="61.5">de</w> <w n="61.6">la</w> <w n="61.7">vague</w> <w n="61.8">profonde</w>,</l>
						<l n="62" num="2.29"><w n="62.1">Priaient</w> <w n="62.2">ou</w> <w n="62.3">maudissaient</w> <w n="62.4">le</w> <w n="62.5">Destructeur</w> <w n="62.6">du</w> <w n="62.7">monde</w>.</l>
						<l n="63" num="2.30"><w n="63.1">Et</w>, <w n="63.2">quand</w> <w n="63.3">eurent</w> <w n="63.4">sombré</w> <w n="63.5">les</w> <w n="63.6">sommets</w> <w n="63.7">des</w> <w n="63.8">grands</w> <w n="63.9">monts</w>.</l>
						<l n="64" num="2.31"><w n="64.1">Quand</w> <w n="64.2">flotta</w> <w n="64.3">sur</w> <w n="64.4">les</w> <w n="64.5">eaux</w> <w n="64.6">le</w> <w n="64.7">rire</w> <w n="64.8">des</w> <w n="64.9">Démons</w>,</l>
						<l n="65" num="2.32"><w n="65.1">Le</w> <w n="65.2">mammouth</w>, <w n="65.3">exhalant</w> <w n="65.4">un</w> <w n="65.5">gémissement</w> <w n="65.6">rauque</w>,</l>
						<l n="66" num="2.33"><w n="66.1">Levait</w> <w n="66.2">sa</w> <w n="66.3">trompe</w> <w n="66.4">encor</w> <w n="66.5">sur</w> <w n="66.6">l</w>’<w n="66.7">immensité</w> <w n="66.8">glauque</w>.</l>
					</lg>
				</div>
				<div type="section" n="3">
					<head type="number">III</head>
					<lg n="1">
						<l n="67" num="1.1"><w n="67.1">Le</w> <w n="67.2">soleil</w> <w n="67.3">reparut</w>, <w n="67.4">rouge</w> <w n="67.5">et</w> <w n="67.6">froid</w> <w n="67.7">dans</w> <w n="67.8">les</w> <w n="67.9">cieux</w>.</l>
						<l n="68" num="1.2"><w n="68.1">Pressant</w> <w n="68.2">entre</w> <w n="68.3">ses</w> <w n="68.4">bras</w> <w n="68.5">le</w> <w n="68.6">corps</w> <w n="68.7">silencieux</w></l>
						<l n="69" num="1.3"><w n="69.1">Et</w> <w n="69.2">glacé</w> <w n="69.3">pour</w> <w n="69.4">jamais</w> <w n="69.5">dans</w> <w n="69.6">une</w> <w n="69.7">vive</w> <w n="69.8">étreinte</w>,</l>
						<l n="70" num="1.4"><w n="70.1">De</w> <w n="70.2">celle</w> <w n="70.3">qui</w> <w n="70.4">mourut</w> <w n="70.5">sans</w> <w n="70.6">regret</w> <w n="70.7">et</w> <w n="70.8">sans</w> <w n="70.9">crainte</w>,</l>
						<l n="71" num="1.5"><w n="71.1">L</w>’<w n="71.2">Ange</w> <w n="71.3">flottait</w>, <w n="71.4">splendide</w> <w n="71.5">et</w> <w n="71.6">triste</w>, <w n="71.7">dans</w> <w n="71.8">le</w> <w n="71.9">vent</w>,</l>
						<l n="72" num="1.6"><w n="72.1">Las</w> <w n="72.2">d</w>’<w n="72.3">offrir</w> <w n="72.4">à</w> <w n="72.5">la</w> <w n="72.6">foudre</w> <w n="72.7">un</w> <w n="72.8">front</w> <w n="72.9">toujours</w> <w n="72.10">vivant</w>.</l>
					</lg>
					<closer>
						<dateline>
							<date when="1864"> Juillet 1864.</date>
							</dateline>
					</closer>
				</div>
			</div></body></text></TEI>