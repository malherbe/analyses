<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES ET LÉGENDES</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>860 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/anatolfrancepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Anatole_France_-_Po%C3%A9sies.djvu</idno>
					</publicationStmt>
					<sourceDesc>
					<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les pièces manquantes ont été ajoutées à partir de Wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA34">
				<head type="main">L’Adieu</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">entrai</w> <w n="1.3">jusques</w> <w n="1.4">au</w> <w n="1.5">fond</w> <w n="1.6">d</w>’<w n="1.7">une</w> <w n="1.8">église</w>, <w n="1.9">le</w> <w n="1.10">soir</w></l>
					<l n="2" num="1.2"><w n="2.1">Du</w> <w n="2.2">jour</w> <w n="2.3">triste</w> <w n="2.4">où</w> <w n="2.5">le</w> <w n="2.6">prêtre</w> <w n="2.7">étend</w> <w n="2.8">un</w> <w n="2.9">voile</w> <w n="2.10">noir</w></l>
					<l n="3" num="1.3"><w n="3.1">Sur</w> <w n="3.2">les</w> <w n="3.3">images</w> <w n="3.4">d</w>’<w n="3.5">or</w> <w n="3.6">de</w> <w n="3.7">ce</w> <w n="3.8">bois</w> <w n="3.9">salutaire</w></l>
					<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">vint</w> <w n="4.3">s</w>’<w n="4.4">offrir</w> <w n="4.5">au</w> <w n="4.6">Ciel</w> <w n="4.7">la</w> <w n="4.8">rançon</w> <w n="4.9">de</w> <w n="4.10">la</w> <w n="4.11">terre</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Un</w> <w n="5.2">diacre</w> <w n="5.3">en</w> <w n="5.4">blanc</w> <w n="5.5">surplis</w> <w n="5.6">veillait</w> <w n="5.7">son</w> <w n="5.8">Dieu</w> <w n="5.9">mort</w>, <w n="5.10">seul</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Courbé</w> <w n="6.2">devant</w> <w n="6.3">l</w>’<w n="6.4">autel</w> <w n="6.5">que</w> <w n="6.6">couvrait</w> <w n="6.7">un</w> <w n="6.8">linceul</w>.</l>
					<l n="7" num="1.7"><w n="7.1">C</w>’<w n="7.2">était</w> <w n="7.3">le</w> <w n="7.4">vendredi</w> <w n="7.5">de</w> <w n="7.6">la</w> <w n="7.7">Semaine</w> <w n="7.8">sainte</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">les</w> <w n="8.3">femmes</w> <w n="8.4">glissaient</w> <w n="8.5">dans</w> <w n="8.6">la</w> <w n="8.7">lugubre</w> <w n="8.8">enceinte</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Sur</w> <w n="9.2">les</w> <w n="9.3">frissons</w> <w n="9.4">de</w> <w n="9.5">soie</w> <w n="9.6">et</w> <w n="9.7">les</w> <w n="9.8">bruits</w> <w n="9.9">argentins</w></l>
					<l n="10" num="1.10"><w n="10.1">Roulaient</w> <w n="10.2">les</w> <w n="10.3">voix</w> <w n="10.4">de</w> <w n="10.5">l</w>’<w n="10.6">orgue</w> <w n="10.7">et</w> <w n="10.8">les</w> <w n="10.9">versets</w> <w n="10.10">latins</w>.</l>
					<l n="11" num="1.11"><w n="11.1">Or</w> <w n="11.2">je</w> <w n="11.3">vis</w> <w n="11.4">celle</w>-<w n="11.5">là</w> <w n="11.6">qui</w> <w n="11.7">tient</w> <w n="11.8">ma</w> <w n="11.9">destinée</w>.</l>
					<l n="12" num="1.12"><w n="12.1">Elle</w> <w n="12.2">était</w> <w n="12.3">à</w> <w n="12.4">genoux</w>, <w n="12.5">mollement</w> <w n="12.6">inclinée</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Son</w> <w n="13.2">front</w> <w n="13.3">se</w> <w n="13.4">renversait</w> <w n="13.5">au</w> <w n="13.6">poids</w> <w n="13.7">des</w> <w n="13.8">cheveux</w> <w n="13.9">lourds</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Ses</w> <w n="14.2">mains</w> <w n="14.3">longues</w> <w n="14.4">pendaient</w> <w n="14.5">sur</w> <w n="14.6">les</w> <w n="14.7">plis</w> <w n="14.8">du</w> <w n="14.9">velours</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">les</w> <w n="15.3">lampes</w> <w n="15.4">tremblaient</w> <w n="15.5">dans</w> <w n="15.6">la</w> <w n="15.7">nef</w> <w n="15.8">ténébreuse</w></l>
					<l n="16" num="1.16"><w n="16.1">Sur</w> <w n="16.2">la</w> <w n="16.3">belle</w> <w n="16.4">pâleur</w> <w n="16.5">de</w> <w n="16.6">sa</w> <w n="16.7">joue</w> <w n="16.8">un</w> <w n="16.9">peu</w> <w n="16.10">creuse</w>.</l>
					<l n="17" num="1.17"><w n="17.1">Je</w> <w n="17.2">fus</w> <w n="17.3">d</w>’<w n="17.4">abord</w> <w n="17.5">surpris</w> <w n="17.6">de</w> <w n="17.7">la</w> <w n="17.8">voir</w> <w n="17.9">en</w> <w n="17.10">ce</w> <w n="17.11">lieu</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Car</w> <w n="18.2">elle</w> <w n="18.3">était</w> <w n="18.4">bien</w> <w n="18.5">loin</w> <w n="18.6">de</w> <w n="18.7">vivre</w> <w n="18.8">selon</w> <w n="18.9">Dieu</w>.</l>
				</lg>
				<lg n="2">
					<l n="19" num="2.1"><w n="19.1">J</w>’<w n="19.2">étais</w> <w n="19.3">à</w> <w n="19.4">son</w> <w n="19.5">côté</w>, <w n="19.6">frôlant</w> <w n="19.7">sa</w> <w n="19.8">jupe</w> <w n="19.9">sombre</w>.</l>
					<l n="20" num="2.2"><w n="20.1">Mais</w> <w n="20.2">rien</w> <w n="20.3">ne</w> <w n="20.4">l</w>’<w n="20.5">avertit</w> <w n="20.6">de</w> <w n="20.7">ce</w> <w n="20.8">qu</w>’<w n="20.9">était</w> <w n="20.10">cette</w> <w n="20.11">ombre</w>.</l>
					<l n="21" num="2.3"><w n="21.1">Et</w> <w n="21.2">ceci</w> <w n="21.3">me</w> <w n="21.4">frappa</w> <w n="21.5">que</w> <w n="21.6">dans</w> <w n="21.7">ses</w> <w n="21.8">grands</w> <w n="21.9">yeux</w> <w n="21.10">clairs</w></l>
					<l n="22" num="2.4"><w n="22.1">Je</w> <w n="22.2">n</w>’<w n="22.3">avais</w> <w n="22.4">jamais</w> <w n="22.5">vu</w> <w n="22.6">de</w> <w n="22.7">si</w> <w n="22.8">brillants</w> <w n="22.9">éclairs</w>,</l>
					<l n="23" num="2.5"><w n="23.1">Je</w> <w n="23.2">n</w>’<w n="23.3">avais</w> <w n="23.4">jamais</w> <w n="23.5">vu</w> <w n="23.6">de</w> <w n="23.7">larmes</w> <w n="23.8">si</w> <w n="23.9">brûlantes</w>.</l>
					<l n="24" num="2.6"><w n="24.1">Ni</w> <w n="24.2">de</w> <w n="24.3">regards</w> <w n="24.4">si</w> <w n="24.5">beaux</w>, <w n="24.6">ni</w> <w n="24.7">d</w>’<w n="24.8">extases</w> <w n="24.9">si</w> <w n="24.10">lentes</w>.</l>
					<l n="25" num="2.7"><w n="25.1">Tant</w> <w n="25.2">un</w> <w n="25.3">heureux</w> <w n="25.4">lien</w> <w n="25.5">de</w> <w n="25.6">célestes</w> <w n="25.7">effrois</w></l>
					<l n="26" num="2.8"><w n="26.1">L</w>’<w n="26.2">attachait</w> <w n="26.3">au</w> <w n="26.4">Dieu</w> <w n="26.5">pâle</w> <w n="26.6">étendu</w> <w n="26.7">sur</w> <w n="26.8">la</w> <w n="26.9">croix</w>,</l>
					<l n="27" num="2.9"><w n="27.1">Tant</w> <w n="27.2">sa</w> <w n="27.3">narine</w> <w n="27.4">ouverte</w> <w n="27.5">à</w> <w n="27.6">la</w> <w n="27.7">divine</w> <w n="27.8">haleine</w></l>
					<l n="28" num="2.10"><w n="28.1">S</w>’<w n="28.2">enivrait</w> <w n="28.3">de</w> <w n="28.4">l</w>’<w n="28.5">encens</w> <w n="28.6">dont</w> <w n="28.7">l</w>’<w n="28.8">église</w> <w n="28.9">était</w> <w n="28.10">pleine</w> !</l>
				</lg>
				<lg n="3">
					<l n="29" num="3.1"><w n="29.1">Que</w> <w n="29.2">l</w>’<w n="29.3">âme</w> <w n="29.4">de</w> <w n="29.5">la</w> <w n="29.6">femme</w> <w n="29.7">est</w> <w n="29.8">prompte</w> <w n="29.9">à</w> <w n="29.10">s</w>’<w n="29.11">embraser</w> !</l>
					<l n="30" num="3.2">— <w n="30.1">Sa</w> <w n="30.2">bouche</w> <w n="30.3">était</w> <w n="30.4">en</w> <w n="30.5">fleur</w> <w n="30.6">comme</w> <w n="30.7">pour</w> <w n="30.8">un</w> <w n="30.9">baiser</w>,</l>
					<l n="31" num="3.3"><w n="31.1">Son</w> <w n="31.2">être</w> <w n="31.3">palpitait</w> <w n="31.4">d</w>’<w n="31.5">une</w> <w n="31.6">invisible</w> <w n="31.7">étreinte</w>.</l>
					<l n="32" num="3.4"><w n="32.1">C</w>’<w n="32.2">est</w> <w n="32.3">pourquoi</w> <w n="32.4">je</w> <w n="32.5">fus</w> <w n="32.6">pris</w> <w n="32.7">de</w> <w n="32.8">tristesse</w> <w n="32.9">et</w> <w n="32.10">de</w> <w n="32.11">crainte</w> :</l>
					<l n="33" num="3.5"><w n="33.1">Je</w> <w n="33.2">vis</w> <w n="33.3">que</w> <w n="33.4">désormais</w> <w n="33.5">ce</w> <w n="33.6">cœur</w> <w n="33.7">m</w>’<w n="33.8">était</w> <w n="33.9">fermé</w></l>
					<l n="34" num="3.6"><w n="34.1">Et</w> <w n="34.2">qu</w>’<w n="34.3">il</w> <w n="34.4">se</w> <w n="34.5">repentait</w> <w n="34.6">de</w> <w n="34.7">m</w>’<w n="34.8">avoir</w> <w n="34.9">trop</w> <w n="34.10">aimé</w> ;</l>
					<l n="35" num="3.7"><w n="35.1">Que</w> <w n="35.2">ce</w> <w n="35.3">sein</w> <w n="35.4">inondé</w> <w n="35.5">par</w> <w n="35.6">la</w> <w n="35.7">Grâce</w> <w n="35.8">féconde</w></l>
					<l n="36" num="3.8"><w n="36.1">Se</w> <w n="36.2">haussait</w> <w n="36.3">du</w> <w n="36.4">dégoût</w> <w n="36.5">des</w> <w n="36.6">choses</w> <w n="36.7">de</w> <w n="36.8">ce</w> <w n="36.9">monde</w>.</l>
					<l n="37" num="3.9"><w n="37.1">Alors</w>, <w n="37.2">pleurant</w> <w n="37.3">sur</w> <w n="37.4">moi</w>, <w n="37.5">je</w> <w n="37.6">reconnus</w>, <w n="37.7">pensif</w>,</l>
					<l n="38" num="3.10"><w n="38.1">Que</w> <w n="38.2">tu</w> <w n="38.3">m</w>’<w n="38.4">avais</w> <w n="38.5">repris</w> <w n="38.6">cette</w> <w n="38.7">femme</w>, <w n="38.8">ô</w> <w n="38.9">beau</w> <w n="38.10">Juif</w>,</l>
					<l n="39" num="3.11"><w n="39.1">Roi</w>, <w n="39.2">dont</w> <w n="39.3">l</w>’<w n="39.4">épine</w> <w n="39.5">a</w> <w n="39.6">ceint</w> <w n="39.7">la</w> <w n="39.8">chevelure</w> <w n="39.9">rousse</w> !</l>
				</lg>
				<lg n="4">
					<l n="40" num="4.1"><w n="40.1">Ton</w> <w n="40.2">âme</w> <w n="40.3">était</w> <w n="40.4">profonde</w> <w n="40.5">et</w> <w n="40.6">ta</w> <w n="40.7">voix</w> <w n="40.8">était</w> <w n="40.9">douce</w> ;</l>
					<l n="41" num="4.2"><w n="41.1">Les</w> <w n="41.2">femmes</w> <w n="41.3">t</w>’<w n="41.4">écoutaient</w> <w n="41.5">parler</w> <w n="41.6">au</w> <w n="41.7">bord</w> <w n="41.8">des</w> <w n="41.9">puits</w>,</l>
					<l n="42" num="4.3"><w n="42.1">Les</w> <w n="42.2">femmes</w> <w n="42.3">parfumaient</w> <w n="42.4">tes</w> <w n="42.5">cheveux</w> ; <w n="42.6">et</w> <w n="42.7">depuis</w></l>
					<l n="43" num="4.4"><w n="43.1">Elles</w> <w n="43.2">ont</w> <w n="43.3">allumé</w> <w n="43.4">sur</w> <w n="43.5">ton</w> <w n="43.6">front</w> <w n="43.7">l</w>’<w n="43.8">auréole</w>.</l>
					<l n="44" num="4.5"><w n="44.1">Dieu</w> <w n="44.2">de</w> <w n="44.3">la</w> <w n="44.4">vierge</w> <w n="44.5">sage</w> <w n="44.6">et</w> <w n="44.7">de</w> <w n="44.8">la</w> <w n="44.9">vierge</w> <w n="44.10">folle</w> !</l>
					<l n="45" num="4.6"><w n="45.1">C</w>’<w n="45.2">est</w> <w n="45.3">écrit</w> : <w n="45.4">pour</w> <w n="45.5">jamais</w> <w n="45.6">toi</w> <w n="45.7">seul</w> <w n="45.8">achèveras</w></l>
					<l n="46" num="4.7"><w n="46.1">Les</w> <w n="46.2">plus</w> <w n="46.3">belles</w> <w n="46.4">amours</w> <w n="46.5">qu</w>’<w n="46.6">on</w> <w n="46.7">essaye</w> <w n="46.8">en</w> <w n="46.9">nos</w> <w n="46.10">bras</w> ;</l>
					<l n="47" num="4.8"><w n="47.1">Toute</w> <w n="47.2">femme</w> <w n="47.3">qui</w> <w n="47.4">pleure</w> <w n="47.5">est</w> <w n="47.6">déjà</w> <w n="47.7">ton</w> <w n="47.8">épouse</w> ;</l>
					<l n="48" num="4.9"><w n="48.1">Tous</w> <w n="48.2">les</w> <w n="48.3">cheveux</w> <w n="48.4">mordus</w> <w n="48.5">sous</w> <w n="48.6">notre</w> <w n="48.7">dent</w> <w n="48.8">jalouse</w></l>
					<l n="49" num="4.10"><w n="49.1">S</w>’<w n="49.2">en</w> <w n="49.3">iront</w> <w n="49.4">à</w> <w n="49.5">leur</w> <w n="49.6">tour</w> <w n="49.7">essuyer</w> <w n="49.8">tes</w> <w n="49.9">pieds</w> <w n="49.10">nus</w> ;</l>
					<l n="50" num="4.11"><w n="50.1">Dégageant</w> <w n="50.2">de</w> <w n="50.3">nos</w> <w n="50.4">bras</w> <w n="50.5">leurs</w> <w n="50.6">flancs</w> <w n="50.7">mal</w> <w n="50.8">retenus</w>,</l>
					<l n="51" num="4.12"><w n="51.1">Jusqu</w>’<w n="51.2">à</w> <w n="51.3">la</w> <w n="51.4">fin</w> <w n="51.5">des</w> <w n="51.6">temps</w> <w n="51.7">toutes</w> <w n="51.8">nos</w> <w n="51.9">Madeleines</w></l>
					<l n="52" num="4.13"><w n="52.1">Verseront</w> <w n="52.2">à</w> <w n="52.3">tes</w> <w n="52.4">pieds</w> <w n="52.5">leurs</w> <w n="52.6">urnes</w> <w n="52.7">encor</w> <w n="52.8">pleines</w>.</l>
					<l n="53" num="4.14"><w n="53.1">Christ</w> ! <w n="53.2">elle</w> <w n="53.3">a</w> <w n="53.4">délaissé</w> <w n="53.5">mon</w> <w n="53.6">âme</w> <w n="53.7">pour</w> <w n="53.8">ton</w> <w n="53.9">Ciel</w>,</l>
					<l n="54" num="4.15"><w n="54.1">Et</w> <w n="54.2">c</w>’<w n="54.3">est</w> <w n="54.4">pour</w> <w n="54.5">te</w> <w n="54.6">prier</w> <w n="54.7">que</w> <w n="54.8">ta</w> <w n="54.9">bouche</w> <w n="54.10">est</w> <w n="54.11">de</w> <w n="54.12">miel</w> !</l>
				</lg>
				<lg n="5">
					<l n="55" num="5.1"><w n="55.1">Adieu</w> ! <w n="55.2">coupe</w> <w n="55.3">sacrée</w> <w n="55.4">où</w> <w n="55.5">je</w> <w n="55.6">ne</w> <w n="55.7">dois</w> <w n="55.8">plus</w> <w n="55.9">boire</w>,</l>
					<l n="56" num="5.2"><w n="56.1">Rose</w> <w n="56.2">mystique</w> <w n="56.3">éclose</w> <w n="56.4">au</w> <w n="56.5">crucifix</w> <w n="56.6">d</w>’<w n="56.7">ivoire</w> !</l>
				</lg>
				<closer>
					<dateline>
						<date when="1866">Février 1866.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>