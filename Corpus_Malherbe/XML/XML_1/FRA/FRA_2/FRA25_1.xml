<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES ET LÉGENDES</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>860 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/anatolfrancepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Anatole_France_-_Po%C3%A9sies.djvu</idno>
					</publicationStmt>
					<sourceDesc>
					<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les pièces manquantes ont été ajoutées à partir de Wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA25">
				<head type="main">Souvenir</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Une</w> <w n="1.2">fois</w> <w n="1.3">seulement</w> <w n="1.4">elle</w> <w n="1.5">m</w>’<w n="1.6">est</w> <w n="1.7">apparue</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Sous</w> <w n="2.2">un</w> <w n="2.3">doux</w> <w n="2.4">ciel</w> <w n="2.5">d</w>’<w n="2.6">avril</w>, <w n="2.7">dans</w> <w n="2.8">une</w> <w n="2.9">calme</w> <w n="2.10">rue</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">l</w>’<w n="3.3">odeur</w> <w n="3.4">des</w> <w n="3.5">lilas</w> <w n="3.6">descendait</w> <w n="3.7">des</w> <w n="3.8">vieux</w> <w n="3.9">murs</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">jour</w>, <w n="4.3">en</w> <w n="4.4">la</w> <w n="4.5">touchant</w> <w n="4.6">prenait</w> <w n="4.7">des</w> <w n="4.8">tons</w> <w n="4.9">si</w> <w n="4.10">purs</w></l>
					<l n="5" num="1.5"><w n="5.1">Qu</w>’<w n="5.2">il</w> <w n="5.3">semblait</w> <w n="5.4">émaner</w> <w n="5.5">de</w> <w n="5.6">sa</w> <w n="5.7">propre</w> <w n="5.8">personne</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Sur</w> <w n="6.2">un</w> <w n="6.3">cheval</w> <w n="6.4">anglais</w>, <w n="6.5">pâle</w> <w n="6.6">et</w> <w n="6.7">svelte</w> <w n="6.8">amazone</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Elle</w> <w n="7.2">pliait</w> <w n="7.3">avec</w> <w n="7.4">une</w> <w n="7.5">fière</w> <w n="7.6">douceur</w></l>
					<l n="8" num="1.8"><w n="8.1">Sa</w> <w n="8.2">taille</w> <w n="8.3">au</w> <w n="8.4">rythme</w> <w n="8.5">égal</w> <w n="8.6">du</w> <w n="8.7">trot</w> <w n="8.8">lent</w> <w n="8.9">et</w> <w n="8.10">berceur</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Puis</w> <w n="9.2">elle</w> <w n="9.3">déroba</w> <w n="9.4">sa</w> <w n="9.5">forme</w> <w n="9.6">et</w> <w n="9.7">sa</w> <w n="9.8">lumière</w></l>
					<l n="10" num="1.10"><w n="10.1">Sous</w> <w n="10.2">la</w> <w n="10.3">porte</w> <w n="10.4">où</w> <w n="10.5">veillaient</w> <w n="10.6">deux</w> <w n="10.7">grands</w> <w n="10.8">lions</w> <w n="10.9">de</w> <w n="10.10">pierre</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1870">1er avril 1870.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>