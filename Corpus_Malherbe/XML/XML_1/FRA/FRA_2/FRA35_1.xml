<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES ET LÉGENDES</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>860 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/anatolfrancepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Anatole_France_-_Po%C3%A9sies.djvu</idno>
					</publicationStmt>
					<sourceDesc>
					<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les pièces manquantes ont été ajoutées à partir de Wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA35">
				<head type="main">Au Poète</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Gautier</w>, <w n="1.2">doux</w> <w n="1.3">enchanteur</w> <w n="1.4">à</w> <w n="1.5">la</w> <w n="1.6">parole</w> <w n="1.7">fière</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Habile</w> <w n="2.2">à</w> <w n="2.3">susciter</w> <w n="2.4">les</w> <w n="2.5">contours</w> <w n="2.6">précieux</w></l>
					<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">apparitions</w> <w n="3.3">qui</w> <w n="3.4">flottaient</w> <w n="3.5">dans</w> <w n="3.6">tes</w> <w n="3.7">yeux</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Tu</w> <w n="4.2">fis</w> <w n="4.3">avec</w> <w n="4.4">bonté</w> <w n="4.5">ton</w> <w n="4.6">œuvre</w> <w n="4.7">de</w> <w n="4.8">lumière</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">royal</w> <w n="5.3">talisman</w>, <w n="5.4">le</w> <w n="5.5">prompt</w> <w n="5.6">évocateur</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">Verbe</w> <w n="6.3">arma</w> <w n="6.4">ta</w> <w n="6.5">bouche</w> <w n="6.6">abondante</w> <w n="6.7">en</w> <w n="6.8">images</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Mieux</w> <w n="7.2">que</w> <w n="7.3">l</w>’<w n="7.4">anneau</w> <w n="7.5">mystique</w> <w n="7.6">et</w> <w n="7.7">la</w> <w n="7.8">verge</w> <w n="7.9">des</w> <w n="7.10">Mages</w></l>
					<l n="8" num="2.4"><w n="8.1">La</w> <w n="8.2">parole</w> <w n="8.3">servit</w> <w n="8.4">ton</w> <w n="8.5">vouloir</w> <w n="8.6">créateur</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">parole</w> <w n="9.3">est</w> <w n="9.4">divine</w> <w n="9.5">et</w> <w n="9.6">contient</w> <w n="9.7">toutes</w> <w n="9.8">choses</w>.</l>
					<l n="10" num="3.2"><w n="10.1">Heureux</w> <w n="10.2">qui</w>, <w n="10.3">pour</w> <w n="10.4">fixer</w> <w n="10.5">son</w> <w n="10.6">rêve</w> <w n="10.7">intérieur</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Employa</w> <w n="11.2">sans</w> <w n="11.3">faillir</w> <w n="11.4">la</w> <w n="11.5">forme</w> <w n="11.6">et</w> <w n="11.7">la</w> <w n="11.8">lueur</w></l>
					<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">le</w> <w n="12.3">cristal</w> <w n="12.4">des</w> <w n="12.5">sons</w> <w n="12.6">fatalement</w> <w n="12.7">encloses</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Heureux</w> <w n="13.2">qui</w> <w n="13.3">fit</w> <w n="13.4">couler</w>, <w n="13.5">à</w> <w n="13.6">flots</w>, <w n="13.7">de</w> <w n="13.8">son</w> <w n="13.9">pressoir</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Comme</w> <w n="14.2">un</w> <w n="14.3">vin</w> <w n="14.4">d</w>’<w n="14.5">Engaddi</w>, <w n="14.6">les</w> <w n="14.7">mots</w> <w n="14.8">dont</w> <w n="14.9">on</w> <w n="14.10">s</w>’<w n="14.11">enivre</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">qui</w>, <w n="15.3">pour</w> <w n="15.4">célébrer</w> <w n="15.5">le</w> <w n="15.6">triomphe</w> <w n="15.7">de</w> <w n="15.8">vivre</w>,</l>
					<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">rhythmes</w> <w n="16.3">parfumés</w> <w n="16.4">remplit</w> <w n="16.5">son</w> <w n="16.6">encensoir</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Heureux</w> <w n="17.2">qui</w>, <w n="17.3">comme</w> <w n="17.4">Adam</w>, <w n="17.5">entre</w> <w n="17.6">les</w> <w n="17.7">quatre</w> <w n="17.8">fleuves</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Sut</w> <w n="18.2">nommer</w> <w n="18.3">par</w> <w n="18.4">leur</w> <w n="18.5">nom</w> <w n="18.6">les</w> <w n="18.7">choses</w> <w n="18.8">qu</w>’<w n="18.9">il</w> <w n="18.10">sut</w> <w n="18.11">voir</w>.</l>
					<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">de</w> <w n="19.3">qui</w> <w n="19.4">l</w>’<w n="19.5">écriture</w> <w n="19.6">est</w> <w n="19.7">un</w> <w n="19.8">puissant</w> <w n="19.9">miroir</w></l>
					<l n="20" num="5.4"><w n="20.1">Fidèle</w> <w n="20.2">à</w> <w n="20.3">les</w> <w n="20.4">garder</w> <w n="20.5">immortellement</w> <w n="20.6">neuves</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Car</w> <w n="21.2">après</w> <w n="21.3">que</w> <w n="21.4">cet</w> <w n="21.5">homme</w> <w n="21.6">a</w> <w n="21.7">fini</w> <w n="21.8">ses</w> <w n="21.9">travaux</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">que</w> <w n="22.3">les</w> <w n="22.4">belles</w> <w n="22.5">mains</w> <w n="22.6">dé</w> <w n="22.7">la</w> <w n="22.8">Tristesse</w> <w n="22.9">calme</w></l>
					<l n="23" num="6.3"><w n="23.1">Ont</w> <w n="23.2">posé</w> <w n="23.3">fermement</w> <w n="23.4">la</w> <w n="23.5">couronne</w> <w n="23.6">et</w> <w n="23.7">la</w> <w n="23.8">palme</w></l>
					<l n="24" num="6.4"><w n="24.1">Sur</w> <w n="24.2">sa</w> <w n="24.3">bière</w> <w n="24.4">livrée</w> <w n="24.5">aux</w> <w n="24.6">lents</w> <w n="24.7">et</w> <w n="24.8">noirs</w> <w n="24.9">chevaux</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Il</w> <w n="25.2">vit</w> <w n="25.3">épars</w> <w n="25.4">en</w> <w n="25.5">nous</w> <w n="25.6">sur</w> <w n="25.7">la</w> <w n="25.8">terre</w> <w n="25.9">chérie</w> ;</l>
					<l n="26" num="7.2"><w n="26.1">Son</w> <w n="26.2">essence</w>, <w n="26.3">à</w> <w n="26.4">nos</w> <w n="26.5">yeux</w> <w n="26.6">charmés</w>, <w n="26.7">en</w> <w n="26.8">songes</w> <w n="26.9">clairs</w>.</l>
					<l n="27" num="7.3"><w n="27.1">En</w> <w n="27.2">chastes</w> <w n="27.3">visions</w>, <w n="27.4">dans</w> <w n="27.5">la</w> <w n="27.6">douceur</w> <w n="27.7">des</w> <w n="27.8">airs</w></l>
					<l n="28" num="7.4"><w n="28.1">Flotte</w>, <w n="28.2">et</w> <w n="28.3">l</w>’<w n="28.4">heure</w> <w n="28.5">présente</w> <w n="28.6">en</w> <w n="28.7">est</w> <w n="28.8">toute</w> <w n="28.9">fleurie</w>.</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Il</w> <w n="29.2">se</w> <w n="29.3">mêle</w>, <w n="29.4">subtil</w>, <w n="29.5">au</w> <w n="29.6">jour</w> <w n="29.7">que</w> <w n="29.8">nous</w> <w n="29.9">voyons</w></l>
					<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">vient</w> <w n="30.3">nous</w> <w n="30.4">affranchir</w> <w n="30.5">du</w> <w n="30.6">temps</w> <w n="30.7">et</w> <w n="30.8">de</w> <w n="30.9">l</w>’<w n="30.10">espace</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Un</w> <w n="31.2">frisson</w> <w n="31.3">glorieux</w> <w n="31.4">saisit</w> <w n="31.5">nos</w> <w n="31.6">cœurs</w> <w n="31.7">où</w> <w n="31.8">passe</w></l>
					<l n="32" num="8.4"><w n="32.1">Son</w> <w n="32.2">âme</w> <w n="32.3">dispersée</w> <w n="32.4">en</w> <w n="32.5">ses</w> <w n="32.6">créations</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Son</w> <w n="33.2">souffle</w> <w n="33.3">sibyllin</w> <w n="33.4">autour</w> <w n="33.5">de</w> <w n="33.6">nous</w> <w n="33.7">fait</w> <w n="33.8">naître</w></l>
					<l n="34" num="9.2"><w n="34.1">Un</w> <w n="34.2">astre</w> <w n="34.3">enchanté</w>, <w n="34.4">plein</w> <w n="34.5">de</w> <w n="34.6">suaves</w> <w n="34.7">couleurs</w>.</l>
					<l n="35" num="9.3"><w n="35.1">De</w> <w n="35.2">parfums</w>, <w n="35.3">de</w> <w n="35.4">regards</w>, <w n="35.5">de</w> <w n="35.6">sourires</w>, <w n="35.7">de</w> <w n="35.8">pleurs</w>,</l>
					<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">multiplie</w> <w n="36.3">en</w> <w n="36.4">nous</w> <w n="36.5">la</w> <w n="36.6">joie</w> <w n="36.7">immense</w> <w n="36.8">d</w>’<w n="36.9">être</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Que</w> <w n="37.2">pour</w> <w n="37.3">nous</w> <w n="37.4">l</w>’<w n="37.5">univers</w> <w n="37.6">se</w> <w n="37.7">baigne</w> <w n="37.8">tout</w> <w n="37.9">entier</w></l>
					<l n="38" num="10.2"><w n="38.1">Des</w> <w n="38.2">effluves</w> <w n="38.3">charmants</w> <w n="38.4">de</w> <w n="38.5">la</w> <w n="38.6">pensée</w> <w n="38.7">humaine</w> !</l>
					<l n="39" num="10.3"><w n="39.1">Que</w> <w n="39.2">sur</w> <w n="39.3">tous</w> <w n="39.4">les</w> <w n="39.5">chemins</w> <w n="39.6">où</w> <w n="39.7">le</w> <w n="39.8">destin</w> <w n="39.9">nous</w> <w n="39.10">mène</w></l>
					<l n="40" num="10.4"><w n="40.1">Tes</w> <w n="40.2">apparitions</w> <w n="40.3">se</w> <w n="40.4">lèvent</w>, <w n="40.5">ô</w> <w n="40.6">Gautier</w> !</l>
				</lg>
			</div></body></text></TEI>