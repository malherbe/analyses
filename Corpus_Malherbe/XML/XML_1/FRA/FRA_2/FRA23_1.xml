<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES ET LÉGENDES</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>860 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/anatolfrancepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Anatole_France_-_Po%C3%A9sies.djvu</idno>
					</publicationStmt>
					<sourceDesc>
					<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les pièces manquantes ont été ajoutées à partir de Wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA23">
				<head type="main">Un Sénateur romain</head>
				<opener>
					<salute>A Gérome, peintre.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">CÉSAR</w>, <w n="1.2">sur</w> <w n="1.3">le</w> <w n="1.4">pavé</w> <w n="1.5">de</w> <w n="1.6">la</w> <w n="1.7">salle</w> <w n="1.8">déserte</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Sous</w> <w n="2.2">sa</w> <w n="2.3">toge</w> <w n="2.4">aux</w> <w n="2.5">grands</w> <w n="2.6">plis</w>, <w n="2.7">gît</w> <w n="2.8">dans</w> <w n="2.9">sa</w> <w n="2.10">majesté</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">bronze</w> <w n="3.3">de</w> <w n="3.4">Pompée</w> <w n="3.5">avec</w> <w n="3.6">sa</w> <w n="3.7">lèvre</w> <w n="3.8">verte</w></l>
					<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">ce</w> <w n="4.3">cadavre</w> <w n="4.4">blanc</w> <w n="4.5">sourit</w> <w n="4.6">ensanglanté</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">âme</w>, <w n="5.3">qui</w> <w n="5.4">vient</w> <w n="5.5">de</w> <w n="5.6">fuir</w> <w n="5.7">par</w> <w n="5.8">une</w> <w n="5.9">route</w> <w n="5.10">ouverte</w></l>
					<l n="6" num="2.2"><w n="6.1">Sous</w> <w n="6.2">le</w> <w n="6.3">fer</w> <w n="6.4">de</w> <w n="6.5">Brutus</w> <w n="6.6">et</w> <w n="6.7">de</w> <w n="6.8">la</w> <w n="6.9">Liberté</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Triste</w>, <w n="7.2">voltige</w> <w n="7.3">autour</w> <w n="7.4">de</w> <w n="7.5">sa</w> <w n="7.6">dépouille</w> <w n="7.7">inerte</w></l>
					<l n="8" num="2.4"><w n="8.1">Où</w> <w n="8.2">l</w>’<w n="8.3">indulgente</w> <w n="8.4">Mort</w> <w n="8.5">mit</w> <w n="8.6">sa</w> <w n="8.7">pâle</w> <w n="8.8">beauté</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">sur</w> <w n="9.3">le</w> <w n="9.4">marbre</w> <w n="9.5">nu</w> <w n="9.6">des</w> <w n="9.7">bancs</w>, <w n="9.8">tout</w> <w n="9.9">seul</w>, <w n="9.10">au</w> <w n="9.11">centre</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Des</w> <w n="10.2">mouvements</w> <w n="10.3">égaux</w> <w n="10.4">de</w> <w n="10.5">son</w> <w n="10.6">énorme</w> <w n="10.7">ventre</w></l>
					<l n="11" num="3.3"><w n="11.1">Rythmant</w> <w n="11.2">ses</w> <w n="11.3">ronflements</w>, <w n="11.4">dort</w> <w n="11.5">un</w> <w n="11.6">vieux</w> <w n="11.7">Sénateur</w>.</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Le</w> <w n="12.2">silence</w> <w n="12.3">l</w>’<w n="12.4">éveille</w>, <w n="12.5">et</w>, <w n="12.6">l</w>’<w n="12.7">œil</w> <w n="12.8">trouble</w>, <w n="12.9">il</w> <w n="12.10">s</w>’<w n="12.11">écrie</w></l>
					<l n="13" num="4.2"><w n="13.1">D</w>’<w n="13.2">un</w> <w n="13.3">ton</w> <w n="13.4">rauque</w>, <w n="13.5">à</w> <w n="13.6">travers</w> <w n="13.7">l</w>’<w n="13.8">horreur</w> <w n="13.9">de</w> <w n="13.10">la</w> <w n="13.11">Curie</w></l>
					<l n="14" num="4.3">« <w n="14.1">Je</w> <w n="14.2">vote</w> <w n="14.3">la</w> <w n="14.4">couronne</w> <w n="14.5">à</w> <w n="14.6">César</w> <w n="14.7">dictateur</w> !»</l>
				</lg>
			</div></body></text></TEI>