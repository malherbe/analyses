<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES ET LÉGENDES</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>860 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/anatolfrancepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Anatole_France_-_Po%C3%A9sies.djvu</idno>
					</publicationStmt>
					<sourceDesc>
					<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les pièces manquantes ont été ajoutées à partir de Wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA29">
				<head type="main">Le Refus</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">fond</w> <w n="1.3">de</w> <w n="1.4">la</w> <w n="1.5">chambre</w> <w n="1.6">élégante</w></l>
					<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">parfuma</w> <w n="2.3">son</w> <w n="2.4">frôlement</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Seule</w>, <w n="3.2">immobile</w>, <w n="3.3">elle</w> <w n="3.4">dégante</w></l>
					<l n="4" num="1.4"><w n="4.1">Ses</w> <w n="4.2">longues</w> <w n="4.3">mains</w>, <w n="4.4">indolemment</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">globes</w> <w n="5.3">chauds</w> <w n="5.4">et</w> <w n="5.5">mats</w> <w n="5.6">des</w> <w n="5.7">lampes</w></l>
					<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">luisent</w> <w n="6.3">dans</w> <w n="6.4">l</w>’<w n="6.5">obscurité</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Sur</w> <w n="7.2">son</w> <w n="7.3">front</w> <w n="7.4">lisse</w> <w n="7.5">et</w> <w n="7.6">sur</w> <w n="7.7">ses</w> <w n="7.8">tempes</w></l>
					<l n="8" num="2.4"><w n="8.1">Versent</w> <w n="8.2">une</w> <w n="8.3">douce</w> <w n="8.4">clarté</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">torrent</w> <w n="9.3">de</w> <w n="9.4">sa</w> <w n="9.5">chevelure</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Où</w> <w n="10.2">l</w>’<w n="10.3">eau</w> <w n="10.4">des</w> <w n="10.5">diamants</w> <w n="10.6">reluit</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Roule</w> <w n="11.2">sur</w> <w n="11.3">sa</w> <w n="11.4">pâle</w> <w n="11.5">encolure</w></l>
					<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">va</w> <w n="12.3">se</w> <w n="12.4">perdre</w> <w n="12.5">dans</w> <w n="12.6">la</w> <w n="12.7">nuit</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">ses</w> <w n="13.3">épaules</w> <w n="13.4">sortent</w> <w n="13.5">nues</w></l>
					<l n="14" num="4.2"><w n="14.1">Du</w> <w n="14.2">noir</w> <w n="14.3">corsage</w> <w n="14.4">de</w> <w n="14.5">velours</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Comme</w> <w n="15.2">la</w> <w n="15.3">lune</w> <w n="15.4">sort</w> <w n="15.5">des</w> <w n="15.6">nues</w></l>
					<l n="16" num="4.4"><w n="16.1">Par</w> <w n="16.2">les</w> <w n="16.3">soirs</w> <w n="16.4">orageux</w> <w n="16.5">et</w> <w n="16.6">lourds</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Elle</w> <w n="17.2">croise</w> <w n="17.3">devant</w> <w n="17.4">la</w> <w n="17.5">glace</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Avec</w> <w n="18.2">un</w> <w n="18.3">tranquille</w> <w n="18.4">plaisir</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Ses</w> <w n="19.2">bras</w> <w n="19.3">blancs</w> <w n="19.4">que</w> <w n="19.5">l</w>’<w n="19.6">or</w> <w n="19.7">fin</w> <w n="19.8">enlace</w></l>
					<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">qui</w> <w n="20.3">ne</w> <w n="20.4">voudraient</w> <w n="20.5">plus</w> <w n="20.6">s</w>’<w n="20.7">ouvrir</w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Car</w> <w n="21.2">il</w> <w n="21.3">lui</w> <w n="21.4">suffit</w> <w n="21.5">d</w>’<w n="21.6">être</w> <w n="21.7">belle</w> :</l>
					<l n="22" num="6.2"><w n="22.1">Ses</w> <w n="22.2">yeux</w>, <w n="22.3">comme</w> <w n="22.4">ceux</w> <w n="22.5">d</w>’<w n="22.6">un</w> <w n="22.7">portrait</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Ont</w> <w n="23.2">une</w> <w n="23.3">fixité</w> <w n="23.4">cruelle</w>,</l>
					<l n="24" num="6.4"><w n="24.1">Pleine</w> <w n="24.2">de</w> <w n="24.3">calme</w> <w n="24.4">et</w> <w n="24.5">de</w> <w n="24.6">secret</w> ;</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Son</w> <w n="25.2">miroir</w> <w n="25.3">semble</w> <w n="25.4">une</w> <w n="25.5">peinture</w></l>
					<l n="26" num="7.2"><w n="26.1">Que</w> <w n="26.2">quelque</w> <w n="26.3">vieux</w> <w n="26.4">maître</w> <w n="26.5">amoureux</w></l>
					<l n="27" num="7.3"><w n="27.1">Offrit</w> <w n="27.2">à</w> <w n="27.3">la</w> <w n="27.4">race</w> <w n="27.5">future</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Claire</w> <w n="28.2">sur</w> <w n="28.3">un</w> <w n="28.4">fond</w> <w n="28.5">ténébreux</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Tant</w> <w n="29.2">la</w> <w n="29.3">beauté</w> <w n="29.4">qui</w> <w n="29.5">s</w>’<w n="29.6">y</w> <w n="29.7">reflète</w></l>
					<l n="30" num="8.2"><w n="30.1">A</w> <w n="30.2">d</w>’<w n="30.3">orgueil</w> <w n="30.4">et</w> <w n="30.5">d</w>’<w n="30.6">apaisement</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Tant</w> <w n="31.2">la</w> <w n="31.3">somptueuse</w> <w n="31.4">toilette</w></l>
					<l n="32" num="8.4"><w n="32.1">Endort</w> <w n="32.2">ses</w> <w n="32.3">plis</w> <w n="32.4">docilement</w>,</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Et</w> <w n="33.2">tant</w> <w n="33.3">cette</w> <w n="33.4">forme</w> <w n="33.5">savante</w></l>
					<l n="34" num="9.2"><w n="34.1">Paraît</w> <w n="34.2">d</w>’<w n="34.3">elle</w>-<w n="34.4">même</w> <w n="34.5">aspirer</w></l>
					<l n="35" num="9.3"><w n="35.1">A</w> <w n="35.2">l</w>’<w n="35.3">immobilité</w> <w n="35.4">vivante</w></l>
					<l n="36" num="9.4"><w n="36.1">Des</w> <w n="36.2">choses</w> <w n="36.3">qui</w> <w n="36.4">doivent</w> <w n="36.5">durer</w>.</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Pendant</w> <w n="37.2">que</w> <w n="37.3">cette</w> <w n="37.4">créature</w>,</l>
					<l n="38" num="10.2"><w n="38.1">Rebelle</w> <w n="38.2">aux</w> <w n="38.3">destins</w> <w n="38.4">familiers</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Divinise</w> <w n="39.2">ainsi</w> <w n="39.3">la</w> <w n="39.4">Nature</w></l>
					<l n="40" num="10.4"><w n="40.1">De</w> <w n="40.2">sa</w> <w n="40.3">chair</w> <w n="40.4">et</w> <w n="40.5">de</w> <w n="40.6">ses</w> <w n="40.7">colliers</w>,</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Le</w> <w n="41.2">miroir</w> <w n="41.3">lui</w> <w n="41.4">montre</w>, <w n="41.5">dans</w> <w n="41.6">l</w>’<w n="41.7">ombre</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Son</w> <w n="42.2">amant</w> <w n="42.3">doucement</w> <w n="42.4">venu</w>,</l>
					<l n="43" num="11.3"><w n="43.1">Au</w> <w n="43.2">bord</w> <w n="43.3">de</w> <w n="43.4">la</w> <w n="43.5">portière</w> <w n="43.6">sombre</w>,</l>
					<l n="44" num="11.4"><w n="44.1">Offrir</w> <w n="44.2">son</w> <w n="44.3">visage</w> <w n="44.4">connu</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Elle</w> <w n="45.2">se</w> <w n="45.3">retourne</w> <w n="45.4">sereine</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Dans</w> <w n="46.2">l</w>’<w n="46.3">amas</w> <w n="46.4">oblique</w> <w n="46.5">des</w> <w n="46.6">plis</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Qu</w>’<w n="47.2">en</w> <w n="47.3">soulevant</w> <w n="47.4">la</w> <w n="47.5">lourde</w> <w n="47.6">traîne</w></l>
					<l n="48" num="12.4"><w n="48.1">Son</w> <w n="48.2">talon</w> <w n="48.3">disperse</w>, <w n="48.4">assouplis</w>,</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><w n="49.1">Darde</w>, <w n="49.2">sans</w> <w n="49.3">pitié</w>, <w n="49.4">sans</w> <w n="49.5">colère</w>,</l>
					<l n="50" num="13.2"><w n="50.1">La</w> <w n="50.2">clarté</w> <w n="50.3">de</w> <w n="50.4">ses</w> <w n="50.5">grands</w> <w n="50.6">yeux</w> <w n="50.7">las</w>,</l>
					<l n="51" num="13.3"><w n="51.1">Et</w>, <w n="51.2">d</w>’<w n="51.3">une</w> <w n="51.4">voix</w> <w n="51.5">égale</w> <w n="51.6">et</w> <w n="51.7">claire</w>,</l>
					<l n="52" num="13.4"><w n="52.1">Dit</w> : « <w n="52.2">Non</w> ! <w n="52.3">je</w> <w n="52.4">ne</w> <w n="52.5">vous</w> <w n="52.6">aime</w> <w n="52.7">pas</w>. »</l>
				</lg>
			</div></body></text></TEI>