<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">IDYLLES ET LÉGENDES</title>
				<title type="medium">Une édition électronique</title>
				<author key="FRA">
					<name>
						<forename>Anatole</forename>
						<surname>FRANCE</surname>
					</name>
					<date from="1844" to="1924">1844-1924</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>860 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">FRA_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/anatolfrancepoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Anatole France</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">https://fr.wikisource.org/wiki/Livre:Anatole_France_-_Po%C3%A9sies.djvu</idno>
					</publicationStmt>
					<sourceDesc>
					<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Poésies</title>
						<author>Anatole France</author>
						<imprint>
							<publisher>Alphonse Lemerre, Éditeur</publisher>
							<date when="1904">1904</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1896">1896</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les pièces manquantes ont été ajoutées à partir de Wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-26" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="FRA33">
				<head type="main">La Danse des Morts</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">les</w> <w n="1.3">siècles</w> <w n="1.4">de</w> <w n="1.5">foi</w>, <w n="1.6">surtout</w> <w n="1.7">dans</w> <w n="1.8">les</w> <w n="1.9">derniers</w>,</l>
					<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">grand</w>’ <w n="2.3">danse</w> <w n="2.4">macabre</w> <w n="2.5">était</w> <w n="2.6">fréquemment</w> <w n="2.7">peinte</w></l>
					<l n="3" num="1.3"><w n="3.1">Au</w> <w n="3.2">vélin</w> <w n="3.3">des</w> <w n="3.4">missels</w> <w n="3.5">comme</w> <w n="3.6">aux</w> <w n="3.7">murs</w> <w n="3.8">des</w> <w n="3.9">charniers</w>.</l>
				</lg>
				<lg n="2">
					<l n="4" num="2.1"><w n="4.1">Je</w> <w n="4.2">crois</w> <w n="4.3">que</w> <w n="4.4">cette</w> <w n="4.5">image</w> <w n="4.6">édifiante</w> <w n="4.7">et</w> <w n="4.8">sainte</w></l>
					<l n="5" num="2.2"><w n="5.1">Mettait</w> <w n="5.2">un</w> <w n="5.3">peu</w> <w n="5.4">d</w>’<w n="5.5">espoir</w> <w n="5.6">au</w> <w n="5.7">fond</w> <w n="5.8">du</w> <w n="5.9">désespoir</w>,</l>
					<l n="6" num="2.3"><w n="6.1">Et</w> <w n="6.2">que</w> <w n="6.3">les</w> <w n="6.4">pauvres</w> <w n="6.5">gens</w> <w n="6.6">la</w> <w n="6.7">regardaient</w> <w n="6.8">sans</w> <w n="6.9">crainte</w>.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">Ce</w> <w n="7.2">n</w>’<w n="7.3">est</w> <w n="7.4">pas</w> <w n="7.5">que</w> <w n="7.6">la</w> <w n="7.7">mort</w> <w n="7.8">leur</w> <w n="7.9">fût</w> <w n="7.10">douce</w> <w n="7.11">à</w> <w n="7.12">prévoir</w> ;</l>
					<l n="8" num="3.2"><w n="8.1">Dieu</w> <w n="8.2">régnait</w> <w n="8.3">dans</w> <w n="8.4">le</w> <w n="8.5">ciel</w> <w n="8.6">et</w> <w n="8.7">le</w> <w n="8.8">roi</w> <w n="8.9">sur</w> <w n="8.10">la</w> <w n="8.11">terre</w> :</l>
					<l n="9" num="3.3"><w n="9.1">Pour</w> <w n="9.2">eux</w> <w n="9.3">mourir</w>, <w n="9.4">c</w>’<w n="9.5">était</w> <w n="9.6">passer</w> <w n="9.7">du</w> <w n="9.8">gris</w> <w n="9.9">au</w> <w n="9.10">noir</w>.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1"><w n="10.1">Mais</w> <w n="10.2">le</w> <w n="10.3">maître</w> <w n="10.4">imagier</w> <w n="10.5">qui</w>, <w n="10.6">d</w>’<w n="10.7">une</w> <w n="10.8">touche</w> <w n="10.9">austère</w>,</l>
					<l n="11" num="4.2"><w n="11.1">Peignait</w> <w n="11.2">ce</w> <w n="11.3">simulacre</w>, <w n="11.4">à</w> <w n="11.5">genoux</w> <w n="11.6">et</w> <w n="11.7">priant</w>,</l>
					<l n="12" num="4.3"><w n="12.1">Moine</w>, <w n="12.2">y</w> <w n="12.3">savait</w> <w n="12.4">souffler</w> <w n="12.5">la</w> <w n="12.6">paix</w> <w n="12.7">du</w> <w n="12.8">monastère</w>.</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1"><w n="13.1">Sous</w> <w n="13.2">les</w> <w n="13.3">pas</w> <w n="13.4">des</w> <w n="13.5">danseurs</w> <w n="13.6">on</w> <w n="13.7">voit</w> <w n="13.8">l</w>’<w n="13.9">enfer</w> <w n="13.10">béant</w> :</l>
					<l n="14" num="5.2"><w n="14.1">Le</w> <w n="14.2">branle</w> <w n="14.3">d</w>’<w n="14.4">un</w> <w n="14.5">squelette</w> <w n="14.6">et</w> <w n="14.7">d</w>’<w n="14.8">un</w> <w n="14.9">vif</w> <w n="14.10">sur</w> <w n="14.11">un</w> <w n="14.12">gouffre</w>,</l>
					<l n="15" num="5.3"><w n="15.1">C</w>’<w n="15.2">est</w> <w n="15.3">bien</w> <w n="15.4">affreux</w>, <w n="15.5">mais</w> <w n="15.6">moins</w> <w n="15.7">pourtant</w> <w n="15.8">que</w> <w n="15.9">le</w> <w n="15.10">néant</w>.</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1"><w n="16.1">On</w> <w n="16.2">croit</w> <w n="16.3">en</w> <w n="16.4">regardant</w> <w n="16.5">qu</w>’<w n="16.6">on</w> <w n="16.7">avale</w> <w n="16.8">du</w> <w n="16.9">soufre</w>,</l>
					<l n="17" num="6.2"><w n="17.1">Et</w> <w n="17.2">c</w>’<w n="17.3">est</w> <w n="17.4">pitié</w> <w n="17.5">de</w> <w n="17.6">voir</w> <w n="17.7">s</w>’<w n="17.8">abîmer</w> <w n="17.9">sans</w> <w n="17.10">retour</w></l>
					<l n="18" num="6.3"><w n="18.1">Sous</w> <w n="18.2">la</w> <w n="18.3">chair</w> <w n="18.4">qui</w> <w n="18.5">se</w> <w n="18.6">tord</w> <w n="18.7">la</w> <w n="18.8">pauvre</w> <w n="18.9">âme</w> <w n="18.10">qui</w> <w n="18.11">souffre</w>.</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1"><w n="19.1">Oui</w>, <w n="19.2">mais</w> <w n="19.3">dans</w> <w n="19.4">cette</w> <w n="19.5">nuit</w> <w n="19.6">étalée</w> <w n="19.7">au</w> <w n="19.8">grand</w> <w n="19.9">jour</w></l>
					<l n="20" num="7.2"><w n="20.1">On</w> <w n="20.2">sent</w> <w n="20.3">l</w>’<w n="20.4">élan</w> <w n="20.5">commun</w> <w n="20.6">de</w> <w n="20.7">la</w> <w n="20.8">pensée</w> <w n="20.9">humaine</w>,</l>
					<l n="21" num="7.3"><w n="21.1">On</w> <w n="21.2">sent</w> <w n="21.3">la</w> <w n="21.4">foi</w> <w n="21.5">profonde</w>. — <w n="21.6">Et</w> <w n="21.7">la</w> <w n="21.8">foi</w>, <w n="21.9">c</w>’<w n="21.10">est</w> <w n="21.11">l</w>’<w n="21.12">amour</w> !</l>
				</lg>
				<lg n="8">
					<l n="22" num="8.1"><w n="22.1">C</w>’<w n="22.2">est</w> <w n="22.3">là</w>, <w n="22.4">c</w>’<w n="22.5">est</w> <w n="22.6">cet</w> <w n="22.7">amour</w> <w n="22.8">triste</w> <w n="22.9">qui</w> <w n="22.10">rassérène</w>.</l>
					<l n="23" num="8.2"><w n="23.1">Les</w> <w n="23.2">mourants</w> <w n="23.3">sont</w> <w n="23.4">pensifs</w>, <w n="23.5">mais</w> <w n="23.6">ne</w> <w n="23.7">se</w> <w n="23.8">plaignent</w> <w n="23.9">pas</w>,</l>
					<l n="24" num="8.3"><w n="24.1">Et</w> <w n="24.2">la</w> <w n="24.3">troupe</w> <w n="24.4">est</w> <w n="24.5">très</w>-<w n="24.6">douce</w> <w n="24.7">à</w> <w n="24.8">la</w> <w n="24.9">Mort</w> <w n="24.10">qui</w> <w n="24.11">la</w> <w n="24.12">mène</w>.</l>
				</lg>
				<lg n="9">
					<l n="25" num="9.1"><w n="25.1">On</w> <w n="25.2">se</w> <w n="25.3">tient</w> <w n="25.4">en</w> <w n="25.5">bon</w> <w n="25.6">ordre</w> <w n="25.7">et</w> <w n="25.8">l</w>’<w n="25.9">on</w> <w n="25.10">marche</w> <w n="25.11">au</w> <w n="25.12">compas</w> ;</l>
					<l n="26" num="9.2"><w n="26.1">Une</w> <w n="26.2">musique</w> <w n="26.3">un</w> <w n="26.4">peu</w> <w n="26.5">faible</w> <w n="26.6">et</w> <w n="26.7">presque</w> <w n="26.8">câline</w></l>
					<l n="27" num="9.3"><w n="27.1">Marque</w> <w n="27.2">discrètement</w> <w n="27.3">et</w> <w n="27.4">dolemment</w> <w n="27.5">le</w> <w n="27.6">pas</w> :</l>
				</lg>
				<lg n="10">
					<l n="28" num="10.1"><w n="28.1">Un</w> <w n="28.2">squelette</w> <w n="28.3">est</w> <w n="28.4">debout</w> <w n="28.5">pinçant</w> <w n="28.6">la</w> <w n="28.7">mandoline</w>,</l>
					<l n="29" num="10.2"><w n="29.1">Et</w>, <w n="29.2">comme</w> <w n="29.3">un</w> <w n="29.4">amoureux</w>, <w n="29.5">sous</w> <w n="29.6">son</w> <w n="29.7">large</w> <w n="29.8">chapeau</w>,</l>
					<l n="30" num="10.3"><w n="30.1">Cache</w> <w n="30.2">son</w> <w n="30.3">front</w> <w n="30.4">de</w> <w n="30.5">vieil</w> <w n="30.6">ivoire</w> <w n="30.7">qu</w>’<w n="30.8">il</w> <w n="30.9">incline</w>.</l>
				</lg>
				<lg n="11">
					<l n="31" num="11.1"><w n="31.1">Son</w> <w n="31.2">compagnon</w> <w n="31.3">applique</w> <w n="31.4">un</w> <w n="31.5">rustique</w> <w n="31.6">pipeau</w></l>
					<l n="32" num="11.2"><w n="32.1">Contre</w> <w n="32.2">ses</w> <w n="32.3">belles</w> <w n="32.4">dents</w> <w n="32.5">blanches</w> <w n="32.6">et</w> <w n="32.7">toutes</w> <w n="32.8">nues</w>,</l>
					<l n="33" num="11.3"><w n="33.1">Ou</w> <w n="33.2">des</w> <w n="33.3">os</w> <w n="33.4">de</w> <w n="33.5">sa</w> <w n="33.6">main</w> <w n="33.7">frappe</w> <w n="33.8">un</w> <w n="33.9">disque</w> <w n="33.10">de</w> <w n="33.11">peau</w>.</l>
				</lg>
				<lg n="12">
					<l n="34" num="12.1"><w n="34.1">Un</w> <w n="34.2">squelette</w> <w n="34.3">de</w> <w n="34.4">femme</w> <w n="34.5">aux</w> <w n="34.6">mines</w> <w n="34.7">ingénues</w></l>
					<l n="35" num="12.2"><w n="35.1">Éveille</w> <w n="35.2">de</w> <w n="35.3">ses</w> <w n="35.4">doigts</w> <w n="35.5">les</w> <w n="35.6">touches</w> <w n="35.7">d</w>’<w n="35.8">un</w> <w n="35.9">clavier</w>,</l>
					<l n="36" num="12.3"><w n="36.1">Comme</w> <w n="36.2">sainte</w> <w n="36.3">Cécile</w> <w n="36.4">assise</w> <w n="36.5">sur</w> <w n="36.6">les</w> <w n="36.7">nues</w>.</l>
				</lg>
				<lg n="13">
					<l n="37" num="13.1"><w n="37.1">Cet</w> <w n="37.2">orchestre</w> <w n="37.3">si</w> <w n="37.4">doux</w> <w n="37.5">ne</w> <w n="37.6">saurait</w> <w n="37.7">convier</w></l>
					<l n="38" num="13.2"><w n="38.1">Les</w> <w n="38.2">vivants</w> <w n="38.3">au</w> <w n="38.4">Sabbat</w>, <w n="38.5">et</w>, <w n="38.6">pour</w> <w n="38.7">mener</w> <w n="38.8">la</w> <w n="38.9">ronde</w>,</l>
					<l n="39" num="13.3"><w n="39.1">Satan</w> <w n="39.2">aurait</w> <w n="39.3">vraiment</w> <w n="39.4">bien</w> <w n="39.5">tort</w> <w n="39.6">de</w> <w n="39.7">l</w>’<w n="39.8">envier</w>.</l>
				</lg>
				<lg n="14">
					<l n="40" num="14.1"><w n="40.1">C</w>’<w n="40.2">est</w> <w n="40.3">que</w> <w n="40.4">Dieu</w>, <w n="40.5">voyez</w>-<w n="40.6">vous</w>, <w n="40.7">tient</w> <w n="40.8">encor</w> <w n="40.9">le</w> <w n="40.10">vieux</w> <w n="40.11">monde</w>.</l>
					<l n="41" num="14.2"><w n="41.1">Voici</w> <w n="41.2">venir</w> <w n="41.3">d</w>’<w n="41.4">abord</w> <w n="41.5">le</w> <w n="41.6">Pape</w> <w n="41.7">et</w> <w n="41.8">l</w>’<w n="41.9">Empereur</w>,</l>
					<l n="42" num="14.3"><w n="42.1">Et</w> <w n="42.2">tout</w> <w n="42.3">le</w> <w n="42.4">peuple</w> <w n="42.5">suit</w> <w n="42.6">dans</w> <w n="42.7">une</w> <w n="42.8">paix</w> <w n="42.9">profonde</w>.</l>
				</lg>
				<lg n="15">
					<l n="43" num="15.1"><w n="43.1">Car</w> <w n="43.2">le</w> <w n="43.3">baron</w> <w n="43.4">a</w> <w n="43.5">foi</w>, <w n="43.6">comme</w> <w n="43.7">le</w> <w n="43.8">laboureur</w>,</l>
					<l n="44" num="15.2"><w n="44.1">En</w> <w n="44.2">tout</w> <w n="44.3">ce</w> <w n="44.4">qu</w>’<w n="44.5">ont</w> <w n="44.6">chanté</w> <w n="44.7">David</w> <w n="44.8">et</w> <w n="44.9">la</w> <w n="44.10">Sibylle</w>.</l>
					<l n="45" num="15.3"><w n="45.1">Leur</w> <w n="45.2">marche</w> <w n="45.3">est</w> <w n="45.4">sûre</w> : <w n="45.5">ils</w> <w n="45.6">vont</w> <w n="45.7">illuminés</w> <w n="45.8">d</w>’<w n="45.9">horreur</w>.</l>
				</lg>
				<lg n="16">
					<l n="46" num="16.1"><w n="46.1">Mais</w> <w n="46.2">la</w> <w n="46.3">vierge</w> <w n="46.4">s</w>’<w n="46.5">étonne</w>, <w n="46.6">et</w>, <w n="46.7">quand</w> <w n="46.8">la</w> <w n="46.9">main</w> <w n="46.10">habile</w></l>
					<l n="47" num="16.2"><w n="47.1">Du</w> <w n="47.2">squelette</w> <w n="47.3">lui</w> <w n="47.4">prend</w> <w n="47.5">la</w> <w n="47.6">taille</w> <w n="47.7">en</w> <w n="47.8">amoureux</w>,</l>
					<l n="48" num="16.3"><w n="48.1">Un</w> <w n="48.2">frisson</w> <w n="48.3">fait</w> <w n="48.4">bondir</w> <w n="48.5">sa</w> <w n="48.6">belle</w> <w n="48.7">chair</w> <w n="48.8">nubile</w> ;</l>
				</lg>
				<lg n="17">
					<l n="49" num="17.1"><w n="49.1">Puis</w>, <w n="49.2">les</w> <w n="49.3">cils</w> <w n="49.4">clos</w>, <w n="49.5">aux</w> <w n="49.6">bras</w> <w n="49.7">du</w> <w n="49.8">danseur</w> <w n="49.9">aux</w> <w n="49.10">yeux</w> <w n="49.11">creux</w></l>
					<l n="50" num="17.2"><w n="50.1">Elle</w> <w n="50.2">exhale</w> <w n="50.3">des</w> <w n="50.4">mots</w> <w n="50.5">charmants</w> <w n="50.6">d</w>’<w n="50.7">épithalame</w>,</l>
					<l n="51" num="17.3"><w n="51.1">Car</w> <w n="51.2">elle</w> <w n="51.3">est</w> <w n="51.4">fiancée</w> <w n="51.5">au</w> <w n="51.6">Christ</w>, <w n="51.7">le</w> <w n="51.8">divin</w> <w n="51.9">preux</w>.</l>
				</lg>
				<lg n="18">
					<l n="52" num="18.1"><w n="52.1">Le</w> <w n="52.2">chevalier</w> <w n="52.3">errant</w> <w n="52.4">trouve</w> <w n="52.5">une</w> <w n="52.6">étrange</w> <w n="52.7">dame</w> ;</l>
					<l n="53" num="18.2"><w n="53.1">Sur</w> <w n="53.2">ses</w> <w n="53.3">côtes</w> <w n="53.4">à</w> <w n="53.5">jour</w> <w n="53.6">pend</w>, <w n="53.7">comme</w> <w n="53.8">sur</w> <w n="53.9">un</w> <w n="53.10">gril</w>,</l>
					<l n="54" num="18.3"><w n="54.1">Un</w> <w n="54.2">reste</w> <w n="54.3">noir</w> <w n="54.4">de</w> <w n="54.5">peau</w> <w n="54.6">qui</w> <w n="54.7">fut</w> <w n="54.8">un</w> <w n="54.9">sein</w> <w n="54.10">de</w> <w n="54.11">femme</w> ;</l>
				</lg>
				<lg n="19">
					<l n="55" num="19.1"><w n="55.1">Mais</w> <w n="55.2">il</w> <w n="55.3">songe</w> <w n="55.4">avoir</w> <w n="55.5">vu</w> <w n="55.6">dans</w> <w n="55.7">un</w> <w n="55.8">bois</w>, <w n="55.9">en</w> <w n="55.10">avril</w>,</l>
					<l n="56" num="19.2"><w n="56.1">Une</w> <w n="56.2">belle</w> <w n="56.3">duchesse</w> <w n="56.4">avec</w> <w n="56.5">sa</w> <w n="56.6">haquenée</w> ;</l>
					<l n="57" num="19.3"><w n="57.1">Il</w> <w n="57.2">compte</w> <w n="57.3">la</w> <w n="57.4">revoir</w> <w n="57.5">au</w> <w n="57.6">ciel</w>. <w n="57.7">Ainsi</w> <w n="57.8">soit</w>-<w n="57.9">il</w> !</l>
				</lg>
				<lg n="20">
					<l n="58" num="20.1"><w n="58.1">Le</w> <w n="58.2">page</w>, <w n="58.3">dont</w> <w n="58.4">la</w> <w n="58.5">joue</w> <w n="58.6">est</w> <w n="58.7">une</w> <w n="58.8">fleur</w> <w n="58.9">fanée</w>,</l>
					<l n="59" num="20.2"><w n="59.1">Va</w> <w n="59.2">dansant</w> <w n="59.3">vers</w> <w n="59.4">l</w>’<w n="59.5">enfer</w> <w n="59.6">en</w> <w n="59.7">un</w> <w n="59.8">très</w>-<w n="59.9">doux</w> <w n="59.10">maintien</w>,</l>
					<l n="60" num="20.3"><w n="60.1">Car</w> <w n="60.2">il</w> <w n="60.3">sait</w> <w n="60.4">clairement</w> <w n="60.5">que</w> <w n="60.6">sa</w> <w n="60.7">dame</w> <w n="60.8">est</w> <w n="60.9">damnée</w>.</l>
				</lg>
				<lg n="21">
					<l n="61" num="21.1"><w n="61.1">L</w>’<w n="61.2">aveugle</w> <w n="61.3">besacier</w> <w n="61.4">ne</w> <w n="61.5">danserait</w> <w n="61.6">pas</w> <w n="61.7">bien</w>,</l>
					<l n="62" num="21.2"><w n="62.1">Mais</w>, <w n="62.2">sans</w> <w n="62.3">souffler</w>, <w n="62.4">la</w> <w n="62.5">Mort</w>, <w n="62.6">en</w> <w n="62.7">discrète</w> <w n="62.8">personne</w>,</l>
					<l n="63" num="21.3"><w n="63.1">Coupe</w> <w n="63.2">tout</w> <w n="63.3">simplement</w> <w n="63.4">la</w> <w n="63.5">corde</w> <w n="63.6">de</w> <w n="63.7">son</w> <w n="63.8">chien</w> :</l>
				</lg>
				<lg n="22">
					<l n="64" num="22.1"><w n="64.1">En</w> <w n="64.2">suivant</w> <w n="64.3">à</w> <w n="64.4">tâtons</w> <w n="64.5">quelque</w> <w n="64.6">grelot</w> <w n="64.7">qui</w> <w n="64.8">sonne</w>,</l>
					<l n="65" num="22.2"><w n="65.1">L</w>’<w n="65.2">aveugle</w> <w n="65.3">s</w>’<w n="65.4">en</w> <w n="65.5">va</w> <w n="65.6">seul</w> <w n="65.7">tout</w> <w n="65.8">droit</w> <w n="65.9">changer</w> <w n="65.10">de</w> <w n="65.11">nuit</w>,</l>
					<l n="66" num="22.3"><w n="66.1">Non</w> <w n="66.2">sans</w> <w n="66.3">avoir</w> <w n="66.4">beaucoup</w> <w n="66.5">juré</w>. <w n="66.6">Dieu</w> <w n="66.7">lui</w> <w n="66.8">pardonne</w></l>
				</lg>
				<lg n="23">
					<l n="67" num="23.1"><w n="67.1">Il</w> <w n="67.2">ferme</w> <w n="67.3">ainsi</w> <w n="67.4">le</w> <w n="67.5">bal</w> <w n="67.6">habilement</w> <w n="67.7">conduit</w> ;</l>
					<l n="68" num="23.2"><w n="68.1">Et</w> <w n="68.2">tous</w>, <w n="68.3">porteurs</w> <w n="68.4">de</w> <w n="68.5">sceptre</w> <w n="68.6">et</w> <w n="68.7">traîneurs</w> <w n="68.8">de</w> <w n="68.9">rapière</w>,</l>
					<l n="69" num="23.3"><w n="69.1">S</w>’<w n="69.2">en</w> <w n="69.3">sont</w> <w n="69.4">allés</w> <w n="69.5">dormir</w> <w n="69.6">sans</w> <w n="69.7">révolte</w> <w n="69.8">et</w> <w n="69.9">sans</w>-<w n="69.10">bruit</w>.</l>
				</lg>
				<lg n="24">
					<l n="70" num="24.1"><w n="70.1">Ils</w> <w n="70.2">comptent</w> <w n="70.3">bien</w> <w n="70.4">qu</w>’<w n="70.5">un</w> <w n="70.6">jour</w> <w n="70.7">le</w> <w n="70.8">lévrier</w> <w n="70.9">de</w> <w n="70.10">pierre</w>,</l>
					<l n="71" num="24.2"><w n="71.1">Sous</w> <w n="71.2">leurs</w> <w n="71.3">rigides</w> <w n="71.4">pieds</w> <w n="71.5">couché</w> <w n="71.6">fidèlement</w>,</l>
					<l n="72" num="24.3"><w n="72.1">Saura</w> <w n="72.2">se</w> <w n="72.3">réveiller</w> <w n="72.4">et</w> <w n="72.5">lécher</w> <w n="72.6">leur</w> <w n="72.7">paupière</w>.</l>
				</lg>
				<lg n="25">
					<l n="73" num="25.1"><w n="73.1">Ils</w> <w n="73.2">savent</w> <w n="73.3">que</w> <w n="73.4">les</w> <w n="73.5">noirs</w> <w n="73.6">clairons</w> <w n="73.7">du</w> <w n="73.8">jugement</w>,</l>
					<l n="74" num="25.2"><w n="74.1">Qu</w>’<w n="74.2">on</w> <w n="74.3">entendra</w> <w n="74.4">sonner</w> <w n="74.5">sur</w> <w n="74.6">chaque</w> <w n="74.7">sépulture</w>,</l>
					<l n="75" num="25.3"><w n="75.1">Agiteront</w> <w n="75.2">leurs</w> <w n="75.3">os</w> <w n="75.4">d</w>’<w n="75.5">un</w> <w n="75.6">grand</w> <w n="75.7">tressaillement</w>,</l>
				</lg>
				<lg n="26">
					<l n="76" num="26.1"><w n="76.1">Et</w> <w n="76.2">que</w> <w n="76.3">la</w> <w n="76.4">Mort</w> <w n="76.5">stupide</w> <w n="76.6">et</w> <w n="76.7">la</w> <w n="76.8">pâle</w> <w n="76.9">Nature</w></l>
					<l n="77" num="26.2"><w n="77.1">Verront</w> <w n="77.2">surgir</w> <w n="77.3">alors</w> <w n="77.4">sur</w> <w n="77.5">les</w> <w n="77.6">tombeaux</w> <w n="77.7">ouverts</w></l>
					<l n="78" num="26.3"><w n="78.1">Le</w> <w n="78.2">corps</w> <w n="78.3">ressuscité</w> <w n="78.4">de</w> <w n="78.5">toute</w> <w n="78.6">créature</w>.</l>
				</lg>
				<lg n="27">
					<l n="79" num="27.1"><w n="79.1">La</w> <w n="79.2">chair</w> <w n="79.3">des</w> <w n="79.4">fils</w> <w n="79.5">d</w>’<w n="79.6">Adam</w> <w n="79.7">sera</w> <w n="79.8">reprise</w> <w n="79.9">aux</w> <w n="79.10">vers</w> ;</l>
					<l n="80" num="27.2"><w n="80.1">La</w> <w n="80.2">Mort</w> <w n="80.3">mourra</w> : <w n="80.4">la</w> <w n="80.5">faim</w> <w n="80.6">détruira</w> <w n="80.7">l</w>’<w n="80.8">affamée</w>,</l>
					<l n="81" num="27.3"><w n="81.1">Lorsque</w> <w n="81.2">l</w>’<w n="81.3">éternité</w> <w n="81.4">prendra</w> <w n="81.5">tout</w> <w n="81.6">l</w>’<w n="81.7">univers</w>.</l>
				</lg>
				<lg n="28">
					<l n="82" num="28.1"><w n="82.1">Et</w>, <w n="82.2">mêlés</w> <w n="82.3">aux</w> <w n="82.4">martyrs</w>, <w n="82.5">belle</w> <w n="82.6">et</w> <w n="82.7">candide</w> <w n="82.8">armée</w>,</l>
					<l n="83" num="28.2"><w n="83.1">Les</w> <w n="83.2">époux</w> <w n="83.3">reverront</w>, <w n="83.4">ceinte</w> <w n="83.5">d</w>’<w n="83.6">un</w> <w n="83.7">nimbe</w> <w n="83.8">d</w>’<w n="83.9">or</w>,</l>
					<l n="84" num="28.3"><w n="84.1">Dans</w> <w n="84.2">les</w> <w n="84.3">longs</w> <w n="84.4">plis</w> <w n="84.5">du</w> <w n="84.6">lin</w> <w n="84.7">passer</w> <w n="84.8">la</w> <w n="84.9">bien</w>-<w n="84.10">aimée</w>.</l>
				</lg>
				<lg n="29">
					<l n="85" num="29.1"><w n="85.1">Mais</w> <w n="85.2">les</w> <w n="85.3">couples</w> <w n="85.4">dont</w> <w n="85.5">l</w>’<w n="85.6">Ange</w> <w n="85.7">aura</w> <w n="85.8">brisé</w> <w n="85.9">l</w>’<w n="85.10">essor</w>,</l>
					<l n="86" num="29.2"><w n="86.1">Sur</w> <w n="86.2">la</w> <w n="86.3">berge</w> <w n="86.4">où</w> <w n="86.5">le</w> <w n="86.6">souffre</w> <w n="86.7">ardent</w> <w n="86.8">roule</w> <w n="86.9">en</w> <w n="86.10">grands</w> <w n="86.11">fleuves</w>,</l>
					<l n="87" num="29.3"><w n="87.1">Oui</w>, <w n="87.2">ceux</w>-<w n="87.3">là</w> <w n="87.4">souffriront</w> : <w n="87.5">donc</w> <w n="87.6">ils</w> <w n="87.7">vivront</w> <w n="87.8">encor</w> !</l>
				</lg>
				<lg n="30">
					<l n="88" num="30.1"><w n="88.1">Les</w> <w n="88.2">tragiques</w> <w n="88.3">amants</w> <w n="88.4">et</w> <w n="88.5">les</w> <w n="88.6">sanglantes</w> <w n="88.7">veuves</w>,</l>
					<l n="89" num="30.2"><w n="89.1">Voltigeant</w> <w n="89.2">enlacés</w> <w n="89.3">dans</w> <w n="89.4">leur</w> <w n="89.5">cercle</w> <w n="89.6">de</w> <w n="89.7">fer</w>,</l>
					<l n="90" num="30.3"><w n="90.1">Soupireront</w> <w n="90.2">sans</w> <w n="90.3">fin</w> <w n="90.4">des</w> <w n="90.5">paroles</w> <w n="90.6">très</w>-<w n="90.7">neuves</w>.</l>
				</lg>
				<lg n="31">
					<l n="91" num="31.1"><w n="91.1">Oh</w> ! <w n="91.2">bienheureux</w> <w n="91.3">ceux</w>-<w n="91.4">là</w> <w n="91.5">qui</w> <w n="91.6">croyaient</w> <w n="91.7">à</w> <w n="91.8">l</w>’<w n="91.9">Enfer</w>.</l>
				</lg>
				<closer>
					<dateline>
						<date when="1869">Octobre 1869.</date>
						</dateline>
				</closer>
			</div></body></text></TEI>