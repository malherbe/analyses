<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">L’AUBE</head><div type="poem" key="HAR112">
						<head type="main">RENONCIATION</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">allais</w>, <w n="1.3">hautain</w> <w n="1.4">et</w> <w n="1.5">fort</w>, <w n="1.6">drapé</w> <w n="1.7">dans</w> <w n="1.8">ma</w> <w n="1.9">pensée</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Posant</w> <w n="2.2">mon</w> <w n="2.3">pied</w> <w n="2.4">vainqueur</w> <w n="2.5">sur</w> <w n="2.6">l</w>’<w n="2.7">avenir</w> <w n="2.8">vaincu</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Moi</w> <w n="3.2">seul</w> <w n="3.3">régnais</w> <w n="3.4">sur</w> <w n="3.5">moi</w> ; <w n="3.6">mais</w> <w n="3.7">vous</w> <w n="3.8">êtes</w> <w n="3.9">passée</w></l>
							<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">je</w> <w n="4.3">n</w>’<w n="4.4">ai</w> <w n="4.5">plus</w> <w n="4.6">compris</w> <w n="4.7">de</w> <w n="4.8">quoi</w> <w n="4.9">j</w>’<w n="4.10">avais</w> <w n="4.11">vécu</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Mon</w> <w n="5.2">art</w>, <w n="5.3">ma</w> <w n="5.4">volonté</w>, <w n="5.5">mes</w> <w n="5.6">passions</w>, <w n="5.7">mes</w> <w n="5.8">rêves</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Mon</w> <w n="6.2">triste</w> <w n="6.3">orgueil</w> <w n="6.4">assis</w> <w n="6.5">sur</w> <w n="6.6">les</w> <w n="6.7">néants</w> <w n="6.8">humains</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Mon</w> <w n="7.2">cœur</w> <w n="7.3">superbe</w> <w n="7.4">et</w> <w n="7.5">froid</w> <w n="7.6">comme</w> <w n="7.7">l</w>’<w n="7.8">acier</w> <w n="7.9">des</w> <w n="7.10">glaives</w>,</l>
							<l n="8" num="2.4"><w n="8.1">J</w>’<w n="8.2">ai</w> <w n="8.3">tout</w> <w n="8.4">mis</w> <w n="8.5">sans</w> <w n="8.6">remords</w> <w n="8.7">dans</w> <w n="8.8">vos</w> <w n="8.9">petites</w> <w n="8.10">mains</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2">ai</w> <w n="9.3">construit</w> <w n="9.4">un</w> <w n="9.5">vaisseau</w> <w n="9.6">géant</w>, <w n="9.7">noir</w> <w n="9.8">sous</w> <w n="9.9">la</w> <w n="9.10">flamme</w></l>
							<l n="10" num="3.2"><w n="10.1">Teinte</w> <w n="10.2">aux</w> <w n="10.3">couleurs</w> <w n="10.4">de</w> <w n="10.5">vos</w> <w n="10.6">cheveux</w> ; <w n="10.7">je</w> <w n="10.8">l</w>’<w n="10.9">ai</w> <w n="10.10">rempli</w></l>
							<l n="11" num="3.3"><w n="11.1">Du</w> <w n="11.2">reste</w> <w n="11.3">des</w> <w n="11.4">fiertés</w> <w n="11.5">qui</w> <w n="11.6">grandissaient</w> <w n="11.7">mon</w> <w n="11.8">âme</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">je</w> <w n="12.3">l</w>’<w n="12.4">ai</w> <w n="12.5">naufragé</w> <w n="12.6">dans</w> <w n="12.7">les</w> <w n="12.8">mers</w> <w n="12.9">de</w> <w n="12.10">l</w>’<w n="12.11">oubli</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Puis</w>, <w n="13.2">grave</w>, <w n="13.3">ayant</w> <w n="13.4">perdu</w> <w n="13.5">mon</w> <w n="13.6">trône</w> <w n="13.7">et</w> <w n="13.8">ma</w> <w n="13.9">patrie</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Mon</w> <w n="14.2">sceptre</w> <w n="14.3">et</w> <w n="14.4">mes</w> <w n="14.5">trésors</w>, <w n="14.6">mon</w> <w n="14.7">peuple</w> <w n="14.8">et</w> <w n="14.9">mes</w> <w n="14.10">palais</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">suis</w> <w n="15.3">venu</w> <w n="15.4">traîner</w> <w n="15.5">ma</w> <w n="15.6">noblesse</w> <w n="15.7">appauvrie</w></l>
							<l n="16" num="4.4"><w n="16.1">Près</w> <w n="16.2">du</w> <w n="16.3">porche</w> <w n="16.4">de</w> <w n="16.5">pierre</w> <w n="16.6">où</w> <w n="16.7">causent</w> <w n="16.8">vos</w> <w n="16.9">valets</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Reine</w> ! <w n="17.2">On</w> <w n="17.3">m</w>’<w n="17.4">a</w> <w n="17.5">vu</w> <w n="17.6">pleurer</w> <w n="17.7">au</w> <w n="17.8">seuil</w> <w n="17.9">de</w> <w n="17.10">votre</w> <w n="17.11">Louvre</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Pareil</w> <w n="18.2">aux</w> <w n="18.3">mendiants</w> <w n="18.4">accroupis</w> <w n="18.5">sur</w> <w n="18.6">les</w> <w n="18.7">quais</w> ;</l>
							<l n="19" num="5.3"><w n="19.1">Je</w> <w n="19.2">tremble</w> <w n="19.3">et</w> <w n="19.4">mon</w> <w n="19.5">sang</w> <w n="19.6">bat</w> <w n="19.7">quand</w> <w n="19.8">votre</w> <w n="19.9">porte</w> <w n="19.10">s</w>’<w n="19.11">ouvre</w>,</l>
							<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">mes</w> <w n="20.3">douleurs</w> <w n="20.4">de</w> <w n="20.5">roi</w> <w n="20.6">font</w> <w n="20.7">rire</w> <w n="20.8">vos</w> <w n="20.9">laquais</w> !</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">j</w>’<w n="21.3">accepte</w> <w n="21.4">en</w> <w n="21.5">dompté</w> <w n="21.6">leur</w> <w n="21.7">risée</w> <w n="21.8">infamante</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">je</w> <w n="22.3">boirais</w> <w n="22.4">encor</w> <w n="22.5">les</w> <w n="22.6">affronts</w> <w n="22.7">déjà</w> <w n="22.8">bus</w></l>
							<l n="23" num="6.3"><w n="23.1">Pour</w> <w n="23.2">voir</w> <w n="23.3">glisser</w> <w n="23.4">au</w> <w n="23.5">loin</w> <w n="23.6">les</w> <w n="23.7">plis</w> <w n="23.8">de</w> <w n="23.9">votre</w> <w n="23.10">mante</w></l>
							<l n="24" num="6.4"><w n="24.1">Ou</w> <w n="24.2">regarder</w> <w n="24.3">les</w> <w n="24.4">yeux</w> <w n="24.5">de</w> <w n="24.6">ceux</w> <w n="24.7">qui</w> <w n="24.8">les</w> <w n="24.9">ont</w> <w n="24.10">vus</w>.</l>
						</lg>
					</div></body></text></TEI>