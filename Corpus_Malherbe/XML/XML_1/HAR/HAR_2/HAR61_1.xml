<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR61">
						<head type="main">ARMA VIRUMQUE</head>
						<opener>
							<salute>À JULES BARBEY D’AUREVILLY</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Orgueil</w> ! <w n="1.2">Cuirasse</w> <w n="1.3">d</w>’<w n="1.4">or</w>, <w n="1.5">casque</w> <w n="1.6">d</w>’<w n="1.7">airain</w> <w n="1.8">poli</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">Armure</w> <w n="2.2">surhumaine</w> <w n="2.3">à</w> <w n="2.4">la</w> <w n="2.5">taille</w> <w n="2.6">de</w> <w n="2.7">l</w>’<w n="2.8">homme</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Heaume</w> <w n="3.2">fait</w> <w n="3.3">de</w> <w n="3.4">dédains</w>, <w n="3.5">de</w> <w n="3.6">pardons</w> <w n="3.7">et</w> <w n="3.8">d</w>’<w n="3.9">oubli</w> ;</l>
							<l n="4" num="1.4"><w n="4.1">Flamme</w> <w n="4.2">qui</w> <w n="4.3">luis</w> <w n="4.4">dans</w> <w n="4.5">l</w>’<w n="4.6">œil</w> <w n="4.7">des</w> <w n="4.8">Fiers</w>, <w n="4.9">dès</w> <w n="4.10">qu</w>’<w n="4.11">on</w> <w n="4.12">les</w> <w n="4.13">nomme</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Baudrier</w> <w n="5.2">de</w> <w n="5.3">la</w> <w n="5.4">foi</w> ! <w n="5.5">Virilité</w> <w n="5.6">du</w> <w n="5.7">cœur</w> !</l>
							<l n="6" num="2.2"><w n="6.1">Orgueil</w>, <w n="6.2">consolateur</w> <w n="6.3">fraternel</w> <w n="6.4">du</w> <w n="6.5">génie</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">fis</w> <w n="7.3">Satan</w> <w n="7.4">vaincu</w> <w n="7.5">plus</w> <w n="7.6">grand</w> <w n="7.7">que</w> <w n="7.8">Dieu</w> <w n="7.9">vainqueur</w> !</l>
							<l n="8" num="2.4"><w n="8.1">Baume</w> <w n="8.2">dans</w> <w n="8.3">le</w> <w n="8.4">combat</w>, <w n="8.5">chrême</w> <w n="8.6">dans</w> <w n="8.7">l</w>’<w n="8.8">agonie</w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Intime</w> <w n="9.2">avènement</w> <w n="9.3">des</w> <w n="9.4">gueux</w> <w n="9.5">qui</w> <w n="9.6">sont</w> <w n="9.7">nés</w> <w n="9.8">rois</w> !</l>
							<l n="10" num="3.2"><w n="10.1">Lumière</w> <w n="10.2">astrale</w>, <w n="10.3">aux</w> <w n="10.4">fronts</w> <w n="10.5">divins</w> <w n="10.6">souillés</w> <w n="10.7">d</w>’<w n="10.8">insultes</w> ;</l>
							<l n="11" num="3.3"><w n="11.1">Nimbe</w> <w n="11.2">étoilé</w> <w n="11.3">des</w> <w n="11.4">saints</w> <w n="11.5">et</w> <w n="11.6">des</w> <w n="11.7">martyrs</w> <w n="11.8">en</w> <w n="11.9">croix</w> ;</l>
							<l n="12" num="3.4"><w n="12.1">Orgueil</w>, <w n="12.2">bourreau</w> <w n="12.3">du</w> <w n="12.4">doute</w> <w n="12.5">et</w> <w n="12.6">réconfort</w> <w n="12.7">des</w> <w n="12.8">cultes</w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Béni</w> <w n="13.2">sois</w>-<w n="13.3">tu</w>, <w n="13.4">péché</w> <w n="13.5">plus</w> <w n="13.6">beau</w> <w n="13.7">que</w> <w n="13.8">la</w> <w n="13.9">vertu</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Toi</w> <w n="14.2">qui</w> <w n="14.3">venges</w> <w n="14.4">les</w> <w n="14.5">Forts</w> <w n="14.6">de</w> <w n="14.7">la</w> <w n="14.8">force</w> <w n="14.9">du</w> <w n="14.10">nombre</w> :</l>
							<l n="15" num="4.3"><w n="15.1">Géant</w> <w n="15.2">maudit</w> <w n="15.3">des</w> <w n="15.4">nains</w>, <w n="15.5">Orgueil</w>, <w n="15.6">béni</w> <w n="15.7">sois</w>-<w n="15.8">tu</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Toi</w> <w n="16.2">qui</w> <w n="16.3">pleus</w> <w n="16.4">des</w> <w n="16.5">soleils</w> <w n="16.6">sur</w> <w n="16.7">l</w>’<w n="16.8">envie</w> <w n="16.9">et</w> <w n="16.10">sur</w> <w n="16.11">l</w>’<w n="16.12">ombre</w> !</l>
						</lg>
					</div></body></text></TEI>