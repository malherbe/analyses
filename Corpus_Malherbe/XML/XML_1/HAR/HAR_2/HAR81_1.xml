<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR81">
						<head type="main">RÉSIGNATION</head>
						<opener>
							<salute>À LAURENT TAILHADE</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">Dimanche</w>. <w n="1.4">Le</w> <w n="1.5">vent</w> <w n="1.6">court</w> <w n="1.7">des</w> <w n="1.8">landes</w> <w n="1.9">aux</w> <w n="1.10">roches</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">douce</w> <w n="2.3">mer</w> <w n="2.4">d</w>’<w n="2.5">Arvor</w> <w n="2.6">joue</w> <w n="2.7">avec</w> <w n="2.8">les</w> <w n="2.9">écueils</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">les</w> <w n="3.3">horizons</w> <w n="3.4">gris</w> <w n="3.5">tremblent</w> <w n="3.6">du</w> <w n="3.7">son</w> <w n="3.8">des</w> <w n="3.9">cloches</w> :</l>
							<l n="4" num="1.4"><w n="4.1">Mais</w> <w n="4.2">le</w> <w n="4.3">village</w> <w n="4.4">est</w> <w n="4.5">noir</w> <w n="4.6">comme</w> <w n="4.7">un</w> <w n="4.8">drap</w> <w n="4.9">de</w> <w n="4.10">cercueils</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Tout</w> <w n="5.2">est</w> <w n="5.3">brûlé</w> : <w n="5.4">les</w> <w n="5.5">toits</w>, <w n="5.6">les</w> <w n="5.7">celliers</w>, <w n="5.8">les</w> <w n="5.9">étables</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">salles</w>, <w n="6.3">les</w> <w n="6.4">hauts</w> <w n="6.5">lits</w> <w n="6.6">enchâssés</w> <w n="6.7">dans</w> <w n="6.8">les</w> <w n="6.9">murs</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Les</w> <w n="7.2">vieux</w> <w n="7.3">dressoirs</w>, <w n="7.4">les</w> <w n="7.5">bancs</w> <w n="7.6">rangés</w> <w n="7.7">autour</w> <w n="7.8">des</w> <w n="7.9">tables</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">granges</w> <w n="8.3">qu</w>’<w n="8.4">embaumaient</w> <w n="8.5">les</w> <w n="8.6">foins</w> <w n="8.7">et</w> <w n="8.8">les</w> <w n="8.9">fruits</w> <w n="8.10">mûrs</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">a</w> <w n="9.3">crevé</w> <w n="9.4">les</w> <w n="9.5">fûts</w> <w n="9.6">et</w> <w n="9.7">bouilli</w> <w n="9.8">dans</w> <w n="9.9">les</w> <w n="9.10">outres</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Le</w> <w n="10.2">beau</w> <w n="10.3">cidre</w> <w n="10.4">écumant</w> <w n="10.5">de</w> <w n="10.6">rire</w> <w n="10.7">et</w> <w n="10.8">de</w> <w n="10.9">chansons</w> ;</l>
							<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">grands</w> <w n="11.3">bœufs</w> <w n="11.4">calcinés</w> <w n="11.5">sont</w> <w n="11.6">couchés</w> <w n="11.7">sous</w> <w n="11.8">les</w> <w n="11.9">poutres</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">les</w> <w n="12.3">vents</w> <w n="12.4">ont</w> <w n="12.5">vanné</w> <w n="12.6">la</w> <w n="12.7">cendre</w> <w n="12.8">des</w> <w n="12.9">moissons</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">flammes</w> <w n="13.3">ont</w> <w n="13.4">taillé</w> <w n="13.5">des</w> <w n="13.6">gibets</w> <w n="13.7">et</w> <w n="13.8">des</w> <w n="13.9">scies</w></l>
							<l n="14" num="4.2"><w n="14.1">Dans</w> <w n="14.2">les</w> <w n="14.3">arbres</w> <w n="14.4">joyeux</w> <w n="14.5">qui</w> <w n="14.6">bombaient</w> <w n="14.7">leurs</w> <w n="14.8">arceaux</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">les</w> <w n="15.3">petits</w> <w n="15.4">enfants</w>, <w n="15.5">avec</w> <w n="15.6">des</w> <w n="15.7">mains</w> <w n="15.8">noircies</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Sur</w> <w n="16.2">les</w> <w n="16.3">brasiers</w> <w n="16.4">éteints</w> <w n="16.5">se</w> <w n="16.6">creusent</w> <w n="16.7">des</w> <w n="16.8">berceaux</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Tels</w> <w n="17.2">les</w> <w n="17.3">champs</w> <w n="17.4">de</w> <w n="17.5">bataille</w> <w n="17.6">au</w> <w n="17.7">soir</w> <w n="17.8">des</w> <w n="17.9">grands</w> <w n="17.10">massacres</w> ;</l>
							<l n="18" num="5.2"><w n="18.1">On</w> <w n="18.2">croirait</w> <w n="18.3">que</w> <w n="18.4">tout</w> <w n="18.5">veut</w> <w n="18.6">mourir</w>, <w n="18.7">mourir</w> <w n="18.8">enfin</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">l</w>’<w n="19.3">on</w> <w n="19.4">sent</w> <w n="19.5">palpiter</w> <w n="19.6">sur</w> <w n="19.7">tout</w>, <w n="19.8">dans</w> <w n="19.9">les</w> <w n="19.10">airs</w> <w n="19.11">âcres</w>,</l>
							<l n="20" num="5.4"><w n="20.1">La</w> <w n="20.2">désolation</w>, <w n="20.3">la</w> <w n="20.4">misère</w> <w n="20.5">et</w> <w n="20.6">la</w> <w n="20.7">faim</w> !</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Seule</w>, <w n="21.2">l</w>’<w n="21.3">église</w> <w n="21.4">luit</w>, <w n="21.5">claire</w> <w n="21.6">entre</w> <w n="21.7">les</w> <w n="21.8">murs</w> <w n="21.9">sombres</w> :</l>
							<l n="22" num="6.2"><w n="22.1">Par</w>-<w n="22.2">dessus</w> <w n="22.3">le</w> <w n="22.4">hameau</w> <w n="22.5">funèbre</w>, <w n="22.6">au</w> <w n="22.7">plein</w> <w n="22.8">milieu</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Comme</w> <w n="23.2">un</w> <w n="23.3">ange</w> <w n="23.4">d</w>’<w n="23.5">espoir</w> <w n="23.6">qui</w> <w n="23.7">surgit</w> <w n="23.8">des</w> <w n="23.9">décombres</w>,</l>
							<l n="24" num="6.4"><w n="24.1">Rose</w> <w n="24.2">et</w> <w n="24.3">blanche</w>, <w n="24.4">elle</w> <w n="24.5">monte</w> <w n="24.6">et</w> <w n="24.7">rit</w> <w n="24.8">vers</w> <w n="24.9">le</w> <w n="24.10">ciel</w> <w n="24.11">bleu</w>.</l>
						</lg>
					</div></body></text></TEI>