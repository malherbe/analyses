<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR137">
						<head type="main">LES BÊTES</head>
						<opener>
							<salute>À MADAME M. DE LAMER</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">voudrais</w> <w n="1.3">être</w> <w n="1.4">calme</w> <w n="1.5">et</w> <w n="1.6">doux</w> <w n="1.7">comme</w> <w n="1.8">les</w> <w n="1.9">bêtes</w></l>
							<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">on</w> <w n="2.3">mène</w> <w n="2.4">par</w> <w n="2.5">troupeaux</w> <w n="2.6">brouter</w> <w n="2.7">à</w> <w n="2.8">travers</w> <w n="2.9">champs</w>.</l>
							<l n="3" num="1.3"><w n="3.1">Tout</w> <w n="3.2">les</w> <w n="3.3">aime</w> ; <w n="3.4">le</w> <w n="3.5">soir</w> <w n="3.6">mire</w> <w n="3.7">l</w>’<w n="3.8">or</w> <w n="3.9">des</w> <w n="3.10">couchants</w></l>
							<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">la</w> <w n="4.3">limpidité</w> <w n="4.4">de</w> <w n="4.5">leurs</w> <w n="4.6">grands</w> <w n="4.7">yeux</w> <w n="4.8">honnêtes</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Balançant</w> <w n="5.2">d</w>’<w n="5.3">un</w> <w n="5.4">air</w> <w n="5.5">las</w> <w n="5.6">le</w> <w n="5.7">bloc</w> <w n="5.8">lent</w> <w n="5.9">de</w> <w n="5.10">leurs</w> <w n="5.11">têtes</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Sur</w> <w n="6.2">les</w> <w n="6.3">pacages</w> <w n="6.4">plats</w> <w n="6.5">ou</w> <w n="6.6">les</w> <w n="6.7">ravins</w> <w n="6.8">penchants</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Dans</w> <w n="7.2">les</w> <w n="7.3">prés</w> <w n="7.4">pleins</w> <w n="7.5">de</w> <w n="7.6">fleurs</w>, <w n="7.7">sous</w> <w n="7.8">les</w> <w n="7.9">bois</w> <w n="7.10">pleins</w> <w n="7.11">de</w> <w n="7.12">chants</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Elles</w> <w n="8.2">vaguent</w>, <w n="8.3">rêvant</w> <w n="8.4">comme</w> <w n="8.5">font</w> <w n="8.6">les</w> <w n="8.7">poètes</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Quand</w> <w n="9.2">l</w>’<w n="9.3">herbe</w> <w n="9.4">rousse</w> <w n="9.5">fume</w> <w n="9.6">au</w> <w n="9.7">soleil</w> <w n="9.8">de</w> <w n="9.9">midi</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Elles</w> <w n="10.2">vont</w>, <w n="10.3">l</w>’<w n="10.4">œil</w> <w n="10.5">mi</w>-<w n="10.6">clos</w> <w n="10.7">et</w> <w n="10.8">le</w> <w n="10.9">pas</w> <w n="10.10">alourdi</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Loin</w> <w n="11.2">des</w> <w n="11.3">grillons</w> <w n="11.4">taquins</w> <w n="11.5">qui</w> <w n="11.6">craquent</w> <w n="11.7">autour</w> <w n="11.8">d</w>’<w n="11.9">elles</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Puis</w> <w n="12.2">graves</w>, <w n="12.3">étalant</w> <w n="12.4">leurs</w> <w n="12.5">gros</w> <w n="12.6">torses</w> <w n="12.7">velus</w>,</l>
							<l n="13" num="4.2"><w n="13.1">Elles</w> <w n="13.2">dorment</w> <w n="13.3">dans</w> <w n="13.4">l</w>’<w n="13.5">ombre</w> <w n="13.6">où</w> <w n="13.7">passent</w> <w n="13.8">des</w> <w n="13.9">bruits</w> <w n="13.10">d</w>’<w n="13.11">ailes</w>…</l>
							<l n="14" num="4.3">— <w n="14.1">Je</w> <w n="14.2">voudrais</w> <w n="14.3">être</w> <w n="14.4">calme</w> <w n="14.5">et</w> <w n="14.6">doux</w> : <w n="14.7">je</w> <w n="14.8">ne</w> <w n="14.9">sais</w> <w n="14.10">plus</w>.</l>
						</lg>
					</div></body></text></TEI>