<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR130">
						<head type="main">LA SAGESSE DE L’EUNUQUE</head>
						<opener>
							<salute>À BENJAMIN CONSTANT</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">« <w n="1.1">Certes</w>, <w n="1.2">dans</w> <w n="1.3">les</w> <w n="1.4">parfums</w> <w n="1.5">et</w> <w n="1.6">l</w>’<w n="1.7">ombre</w> <w n="1.8">du</w> <w n="1.9">sérail</w>,</l>
							<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2">ai</w> <w n="2.3">vu</w> <w n="2.4">des</w> <w n="2.5">corps</w> <w n="2.6">choisis</w> <w n="2.7">pour</w> <w n="2.8">le</w> <w n="2.9">baiser</w> <w n="2.10">des</w> <w n="2.11">princes</w> :</l>
							<l n="3" num="1.3"><w n="3.1">Flots</w> <w n="3.2">de</w> <w n="3.3">satin</w> <w n="3.4">vivant</w>, <w n="3.5">fleurs</w> <w n="3.6">d</w>’<w n="3.7">ambre</w> <w n="3.8">et</w> <w n="3.9">de</w> <w n="3.10">corail</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Moisson</w> <w n="4.2">d</w>’<w n="4.3">amour</w> <w n="4.4">fauchée</w> <w n="4.5">au</w> <w n="4.6">lointain</w> <w n="4.7">des</w> <w n="4.8">provinces</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">J</w>’<w n="5.2">ai</w> <w n="5.3">vu</w> <w n="5.4">l</w>’<w n="5.5">Indoue</w> <w n="5.6">aux</w> <w n="5.7">seins</w> <w n="5.8">bronzés</w> <w n="5.9">par</w> <w n="5.10">l</w>’<w n="5.11">air</w> <w n="5.12">du</w> <w n="5.13">ciel</w>,</l>
							<l n="6" num="2.2"><w n="6.1">La</w> <w n="6.2">Juive</w> <w n="6.3">aux</w> <w n="6.4">larges</w> <w n="6.5">yeux</w>, <w n="6.6">la</w> <w n="6.7">Mauresque</w> <w n="6.8">aux</w> <w n="6.9">reins</w> <w n="6.10">souples</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">la</w> <w n="7.3">vierge</w> <w n="7.4">du</w> <w n="7.5">Nord</w> <w n="7.6">blonde</w> <w n="7.7">comme</w> <w n="7.8">le</w> <w n="7.9">miel</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">les</w> <w n="8.3">filles</w> <w n="8.4">d</w>’<w n="8.5">Hellas</w> <w n="8.6">qui</w> <w n="8.7">s</w>’<w n="8.8">endorment</w> <w n="8.9">par</w> <w n="8.10">couples</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Mon</w> <w n="9.2">oreille</w> <w n="9.3">a</w> <w n="9.4">connu</w> <w n="9.5">le</w> <w n="9.6">bruit</w> <w n="9.7">de</w> <w n="9.8">leurs</w> <w n="9.9">sanglots</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">j</w>’<w n="10.3">ai</w> <w n="10.4">vu</w> <w n="10.5">se</w> <w n="10.6">mouiller</w> <w n="10.7">leurs</w> <w n="10.8">bouches</w> <w n="10.9">purpurines</w></l>
							<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">mourir</w> <w n="11.3">leurs</w> <w n="11.4">regards</w> <w n="11.5">sous</w> <w n="11.6">les</w> <w n="11.7">cils</w> <w n="11.8">demi</w>-<w n="11.9">clos</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Lorsque</w> <w n="12.2">le</w> <w n="12.3">vent</w> <w n="12.4">du</w> <w n="12.5">rut</w> <w n="12.6">secouait</w> <w n="12.7">leurs</w> <w n="12.8">poitrines</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Moi</w>, <w n="13.2">le</w> <w n="13.3">sage</w> <w n="13.4">aux</w> <w n="13.5">yeux</w> <w n="13.6">froids</w>, <w n="13.7">l</w>’<w n="13.8">affranchi</w> <w n="13.9">de</w> <w n="13.10">la</w> <w n="13.11">chair</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Berger</w> <w n="14.2">de</w> <w n="14.3">ce</w> <w n="14.4">troupeau</w> <w n="14.5">que</w> <w n="14.6">les</w> <w n="14.7">hontes</w> <w n="14.8">tourmentent</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">sais</w> <w n="15.3">par</w> <w n="15.4">quel</w> <w n="15.5">prestige</w> <w n="15.6">Allah</w> <w n="15.7">nous</w> <w n="15.8">le</w> <w n="15.9">rend</w> <w n="15.10">cher</w></l>
							<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">j</w>’<w n="16.3">ai</w> <w n="16.4">lu</w> <w n="16.5">le</w> <w n="16.6">secret</w> <w n="16.7">de</w> <w n="16.8">ses</w> <w n="16.9">beautés</w> <w n="16.10">qui</w> <w n="16.11">mentent</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Femme</w> ! <w n="17.2">C</w>’<w n="17.3">est</w> <w n="17.4">pour</w> <w n="17.5">un</w> <w n="17.6">but</w> <w n="17.7">que</w> <w n="17.8">tes</w> <w n="17.9">flancs</w> <w n="17.10">sont</w> <w n="17.11">pétris</w> :</l>
							<l n="18" num="5.2"><w n="18.1">Allah</w> <w n="18.2">t</w>’<w n="18.3">a</w> <w n="18.4">faite</w> <w n="18.5">utile</w>, <w n="18.6">Allah</w> <w n="18.7">te</w> <w n="18.8">veut</w> <w n="18.9">féconde</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Mais</w> <w n="19.2">il</w> <w n="19.3">a</w> <w n="19.4">refusé</w> <w n="19.5">la</w> <w n="19.6">splendeur</w> <w n="19.7">des</w> <w n="19.8">houris</w></l>
							<l n="20" num="5.4"><w n="20.1">À</w> <w n="20.2">ton</w> <w n="20.3">sein</w> <w n="20.4">déformé</w> <w n="20.5">par</w> <w n="20.6">le</w> <w n="20.7">berceau</w> <w n="20.8">du</w> <w n="20.9">monde</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Ton</w> <w n="21.2">corps</w> <w n="21.3">est</w> <w n="21.4">laid</w>, <w n="21.5">sans</w> <w n="21.6">force</w>, <w n="21.7">impur</w>, <w n="21.8">lent</w> <w n="21.9">au</w> <w n="21.10">plaisir</w> :</l>
							<l n="22" num="6.2"><w n="22.1">Mais</w> <w n="22.2">jusqu</w>’<w n="22.3">à</w> <w n="22.4">l</w>’<w n="22.5">heure</w> <w n="22.6">auguste</w> <w n="22.7">où</w> <w n="22.8">l</w>’<w n="22.9">œuvre</w> <w n="22.10">se</w> <w n="22.11">consomme</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Le</w> <w n="23.2">mâle</w> <w n="23.3">ne</w> <w n="23.4">te</w> <w n="23.5">voit</w> <w n="23.6">qu</w>’<w n="23.7">à</w> <w n="23.8">travers</w> <w n="23.9">son</w> <w n="23.10">désir</w> :</l>
							<l n="24" num="6.4"><w n="24.1">La</w> <w n="24.2">beauté</w> <w n="24.3">de</w> <w n="24.4">la</w> <w n="24.5">Femme</w> <w n="24.6">est</w> <w n="24.7">dans</w> <w n="24.8">les</w> <w n="24.9">nerfs</w> <w n="24.10">de</w> <w n="24.11">l</w>’<w n="24.12">Homme</w> !</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Parce</w> <w n="25.2">que</w> <w n="25.3">la</w> <w n="25.4">loi</w> <w n="25.5">sainte</w> <w n="25.6">a</w> <w n="25.7">dit</w> : « <w n="25.8">Reproduisez</w>, »</l>
							<l n="26" num="7.2"><w n="26.1">Ton</w> <w n="26.2">seigneur</w> <w n="26.3">croit</w> <w n="26.4">t</w>’<w n="26.5">aimer</w> <w n="26.6">et</w> <w n="26.7">tu</w> <w n="26.8">crois</w> <w n="26.9">être</w> <w n="26.10">aimée</w> ;</l>
							<l n="27" num="7.3"><w n="27.1">Mais</w> <w n="27.2">c</w>’<w n="27.3">est</w> <w n="27.4">le</w> <w n="27.5">vœu</w> <w n="27.6">d</w>’<w n="27.7">en</w> <w n="27.8">haut</w> <w n="27.9">qui</w> <w n="27.10">vous</w> <w n="27.11">jette</w> <w n="27.12">aux</w> <w n="27.13">baisers</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">ta</w> <w n="28.3">beauté</w> <w n="28.4">finit</w> <w n="28.5">quand</w> <w n="28.6">la</w> <w n="28.7">chair</w> <w n="28.8">s</w>’<w n="28.9">est</w> <w n="28.10">pâmée</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Qu</w>’<w n="29.2">importe</w> ? <w n="29.3">Ouvre</w> <w n="29.4">en</w> <w n="29.5">riant</w> <w n="29.6">tes</w> <w n="29.7">lèvres</w> <w n="29.8">et</w> <w n="29.9">tes</w> <w n="29.10">bras</w> :</l>
							<l n="30" num="8.2"><w n="30.1">Femme</w>, <w n="30.2">ce</w> <w n="30.3">n</w>’<w n="30.4">est</w> <w n="30.5">pas</w> <w n="30.6">toi</w>, <w n="30.7">c</w>’<w n="30.8">est</w> <w n="30.9">Dieu</w> <w n="30.10">qui</w> <w n="30.11">les</w> <w n="30.12">appelle</w>.</l>
							<l n="31" num="8.3"><w n="31.1">Tant</w> <w n="31.2">que</w> <w n="31.3">vivront</w> <w n="31.4">les</w> <w n="31.5">fils</w> <w n="31.6">des</w> <w n="31.7">fils</w>, <w n="31.8">tu</w> <w n="31.9">resteras</w></l>
							<l n="32" num="8.4"><w n="32.1">Par</w> <w n="32.2">l</w>’<w n="32.3">éternel</w> <w n="32.4">désir</w> <w n="32.5">éternellement</w> <w n="32.6">belle</w> ! »</l>
						</lg>
					</div></body></text></TEI>