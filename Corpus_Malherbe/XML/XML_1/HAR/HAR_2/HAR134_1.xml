<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR134">
						<head type="main">IVRE</head>
						<opener>
							<salute>À AMÉDÉE PATTE</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Tous</w> <w n="1.2">les</w> <w n="1.3">mensonges</w> <w n="1.4">dont</w> <w n="1.5">j</w>’<w n="1.6">ai</w> <w n="1.7">bu</w></l>
							<l n="2" num="1.2"><w n="2.1">M</w>’<w n="2.2">ont</w> <w n="2.3">grisé</w> <w n="2.4">jusqu</w>’<w n="2.5">à</w> <w n="2.6">la</w> <w n="2.7">folie</w>.</l>
							<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">avais</w> <w n="3.3">soif</w> <w n="3.4">de</w> <w n="3.5">croire</w> : <w n="3.6">Ils</w> <w n="3.7">l</w>’<w n="3.8">ont</w> <w n="3.9">vu</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">ma</w> <w n="4.3">coupe</w> <w n="4.4">fut</w> <w n="4.5">tôt</w> <w n="4.6">remplie</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Tous</w> <w n="5.2">se</w> <w n="5.3">sont</w> <w n="5.4">faits</w> <w n="5.5">mes</w> <w n="5.6">échansons</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">la</w> <w n="6.3">nuit</w> <w n="6.4">passa</w> <w n="6.5">comme</w> <w n="6.6">un</w> <w n="6.7">songe</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Jusqu</w>’<w n="7.2">au</w> <w n="7.3">matin</w> ! <w n="7.4">Versez</w> ! <w n="7.5">Versons</w> !</l>
							<l n="8" num="2.4"><w n="8.1">Chacun</w> <w n="8.2">m</w>’<w n="8.3">a</w> <w n="8.4">versé</w> <w n="8.5">son</w> <w n="8.6">mensonge</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">l</w>’<w n="9.3">on</w> <w n="9.4">versait</w> ! <w n="9.5">Et</w> <w n="9.6">je</w> <w n="9.7">buvais</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">je</w> <w n="10.3">suis</w> <w n="10.4">ivre</w>, <w n="10.5">je</w> <w n="10.6">suis</w> <w n="10.7">ivre</w>…</l>
							<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">ne</w> <w n="11.3">sais</w> <w n="11.4">plus</w> <w n="11.5">comment</w> <w n="11.6">je</w> <w n="11.7">vais</w> :</l>
							<l n="12" num="3.4"><w n="12.1">J</w>’<w n="12.2">ai</w> <w n="12.3">perdu</w> <w n="12.4">ma</w> <w n="12.5">route</w> <w n="12.6">de</w> <w n="12.7">vivre</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">n</w>’<w n="13.3">ai</w> <w n="13.4">plus</w> <w n="13.5">ni</w> <w n="13.6">foi</w> <w n="13.7">ni</w> <w n="13.8">remords</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Je</w> <w n="14.2">n</w>’<w n="14.3">aime</w> <w n="14.4">plus</w> <w n="14.5">rien</w> <w n="14.6">ni</w> <w n="14.7">personne</w> ;</l>
							<l n="15" num="4.3"><w n="15.1">Les</w> <w n="15.2">courages</w> <w n="15.3">jeunes</w> <w n="15.4">sont</w> <w n="15.5">morts</w></l>
							<l n="16" num="4.4"><w n="16.1">Dans</w> <w n="16.2">mon</w> <w n="16.3">âme</w> <w n="16.4">où</w> <w n="16.5">le</w> <w n="16.6">vide</w> <w n="16.7">sonne</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">J</w>’<w n="17.2">ai</w> <w n="17.3">trop</w> <w n="17.4">souffert</w> <w n="17.5">pour</w> <w n="17.6">être</w> <w n="17.7">bon</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">j</w>’<w n="18.3">ai</w> <w n="18.4">trop</w> <w n="18.5">cru</w> <w n="18.6">pour</w> <w n="18.7">croire</w> <w n="18.8">encore</w>.</l>
							<l n="19" num="5.3"><w n="19.1">J</w>’<w n="19.2">irai</w> <w n="19.3">seul</w>, <w n="19.4">j</w>’<w n="19.5">irai</w> <w n="19.6">vagabond</w></l>
							<l n="20" num="5.4"><w n="20.1">Par</w> <w n="20.2">les</w> <w n="20.3">grands</w> <w n="20.4">chemins</w> <w n="20.5">que</w> <w n="20.6">j</w>’<w n="20.7">ignore</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Traînant</w> <w n="21.2">les</w> <w n="21.3">pieds</w>, <w n="21.4">ployant</w> <w n="21.5">les</w> <w n="21.6">reins</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Le</w> <w n="22.2">regard</w> <w n="22.3">noyé</w> <w n="22.4">dans</w> <w n="22.5">mon</w> <w n="22.6">rêve</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Le</w> <w n="23.2">cœur</w> <w n="23.3">noyé</w> <w n="23.4">dans</w> <w n="23.5">mes</w> <w n="23.6">chagrins</w>,</l>
							<l n="24" num="6.4"><w n="24.1">J</w>’<w n="24.2">irai</w> <w n="24.3">sans</w> <w n="24.4">but</w>, <w n="24.5">j</w>’<w n="24.6">irai</w> <w n="24.7">sans</w> <w n="24.8">trêve</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Heurtant</w> <w n="25.2">les</w> <w n="25.3">murs</w> <w n="25.4">gras</w>, <w n="25.5">de</w> <w n="25.6">mon</w> <w n="25.7">front</w>,</l>
							<l n="26" num="7.2"><w n="26.1">J</w>’<w n="26.2">irai</w> <w n="26.3">jusqu</w>’<w n="26.4">à</w> <w n="26.5">l</w>’<w n="26.6">heure</w> <w n="26.7">dernière</w> :</l>
							<l n="27" num="7.3"><w n="27.1">Les</w> <w n="27.2">petits</w> <w n="27.3">enfants</w> <w n="27.4">blonds</w> <w n="27.5">suivront</w></l>
							<l n="28" num="7.4"><w n="28.1">Pour</w> <w n="28.2">me</w> <w n="28.3">voir</w> <w n="28.4">rouler</w> <w n="28.5">dans</w> <w n="28.6">l</w>’<w n="28.7">ornière</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">j</w>’<w n="29.3">y</w> <w n="29.4">roulerai</w>, <w n="29.5">las</w>, <w n="29.6">fourbu</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Lourd</w> <w n="30.2">du</w> <w n="30.3">sommeil</w> <w n="30.4">où</w> <w n="30.5">l</w>’<w n="30.6">on</w> <w n="30.7">oublie</w>…</l>
							<l n="31" num="8.3">— <w n="31.1">Tous</w> <w n="31.2">les</w> <w n="31.3">mensonges</w> <w n="31.4">dont</w> <w n="31.5">j</w>’<w n="31.6">ai</w> <w n="31.7">bu</w></l>
							<l n="32" num="8.4"><w n="32.1">M</w>’<w n="32.2">ont</w> <w n="32.3">grisé</w> <w n="32.4">jusqu</w>’<w n="32.5">à</w> <w n="32.6">la</w> <w n="32.7">folie</w> !</l>
						</lg>
					</div></body></text></TEI>