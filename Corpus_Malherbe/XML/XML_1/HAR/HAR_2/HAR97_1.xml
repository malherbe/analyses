<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES FORMES</head><div type="poem" key="HAR97">
						<head type="main">LES FAIBLES</head>
						<opener>
							<salute>À JOSEPH BERTHO</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">n</w>’<w n="1.3">ai</w> <w n="1.4">d</w>’<w n="1.5">amour</w> <w n="1.6">au</w> <w n="1.7">cœur</w> <w n="1.8">que</w> <w n="1.9">pour</w> <w n="1.10">ceux</w> <w n="1.11">qu</w>’<w n="1.12">on</w> <w n="1.13">torture</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">tout</w> <w n="2.3">petits</w> <w n="2.4">enfants</w> <w n="2.5">de</w> <w n="2.6">l</w>’<w n="2.7">immense</w> <w n="2.8">nature</w></l>
							<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">vivent</w> <w n="3.3">dans</w> <w n="3.4">l</w>’<w n="3.5">ennui</w>, <w n="3.6">la</w> <w n="3.7">tristesse</w> <w n="3.8">ou</w> <w n="3.9">l</w>’<w n="3.10">effroi</w> ;</l>
							<l n="4" num="1.4"><w n="4.1">Ceux</w> <w n="4.2">qui</w> <w n="4.3">n</w>’<w n="4.4">ont</w> <w n="4.5">pas</w> <w n="4.6">de</w> <w n="4.7">nid</w>, <w n="4.8">le</w> <w n="4.9">soir</w>, <w n="4.10">quand</w> <w n="4.11">il</w> <w n="4.12">fait</w> <w n="4.13">froid</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Qui</w> <w n="5.2">tremblent</w> <w n="5.3">dans</w> <w n="5.4">le</w> <w n="5.5">vent</w> <w n="5.6">et</w> <w n="5.7">gîtent</w> <w n="5.8">sous</w> <w n="5.9">la</w> <w n="5.10">neige</w> ;</l>
							<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">faibles</w>, <w n="6.3">ceux</w> <w n="6.4">qu</w>’<w n="6.5">on</w> <w n="6.6">tue</w> <w n="6.7">et</w> <w n="6.8">que</w> <w n="6.9">nul</w> <w n="6.10">ne</w> <w n="6.11">protège</w></l>
							<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">dont</w> <w n="7.3">le</w> <w n="7.4">bon</w> <w n="7.5">soleil</w> <w n="7.6">lui</w>-<w n="7.7">même</w> <w n="7.8">est</w> <w n="7.9">ennemi</w> ;</l>
							<l n="8" num="1.8"><w n="8.1">Qui</w> <w n="8.2">n</w>’<w n="8.3">ont</w> <w n="8.4">que</w> <w n="8.5">la</w> <w n="8.6">douceur</w> <w n="8.7">d</w>’<w n="8.8">avoir</w> <w n="8.9">un</w> <w n="8.10">peu</w> <w n="8.11">dormi</w></l>
							<l n="9" num="1.9"><w n="9.1">Lorsqu</w>’<w n="9.2">il</w> <w n="9.3">faut</w> <w n="9.4">s</w>’<w n="9.5">éveiller</w> <w n="9.6">encor</w> <w n="9.7">pour</w> <w n="9.8">vivre</w>, <w n="9.9">et</w> <w n="9.10">vivre</w>…</l>
							<l n="10" num="1.10"><w n="10.1">Aussi</w>, <w n="10.2">lorsque</w> <w n="10.3">l</w>’<w n="10.4">hiver</w> <w n="10.5">met</w> <w n="10.6">des</w> <w n="10.7">robes</w> <w n="10.8">de</w> <w n="10.9">givre</w></l>
							<l n="11" num="1.11"><w n="11.1">Sur</w> <w n="11.2">les</w> <w n="11.3">troncs</w> <w n="11.4">d</w>’<w n="11.5">arbres</w> <w n="11.6">noirs</w> <w n="11.7">et</w> <w n="11.8">les</w> <w n="11.9">brins</w> <w n="11.10">d</w>’<w n="11.11">herbe</w> <w n="11.12">roux</w>,</l>
							<l n="12" num="1.12"><w n="12.1">Je</w> <w n="12.2">rêve</w> <w n="12.3">d</w>’<w n="12.4">être</w> <w n="12.5">un</w> <w n="12.6">dieu</w> <w n="12.7">paternel</w>, <w n="12.8">grave</w> <w n="12.9">et</w> <w n="12.10">doux</w>,</l>
							<l n="13" num="1.13"><w n="13.1">Qui</w> <w n="13.2">pourrait</w>, <w n="13.3">en</w> <w n="13.4">faisant</w> <w n="13.5">refleurir</w> <w n="13.6">les</w> <w n="13.7">pervenches</w>,</l>
							<l n="14" num="1.14"><w n="14.1">Être</w> <w n="14.2">aimé</w> <w n="14.3">des</w> <w n="14.4">oiseaux</w> <w n="14.5">qui</w> <w n="14.6">glissent</w> <w n="14.7">sous</w> <w n="14.8">les</w> <w n="14.9">branches</w>.</l>
						</lg>
					</div></body></text></TEI>