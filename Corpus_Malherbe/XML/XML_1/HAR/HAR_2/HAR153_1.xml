<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR153">
						<head type="main">HYMENÆÉ</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">une</w> <w n="1.3">lavandière</w> <w n="1.4">agreste</w>, <w n="1.5">à</w> <w n="1.6">pleines</w> <w n="1.7">mains</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Tord</w> <w n="2.2">les</w> <w n="2.3">grands</w> <w n="2.4">linges</w> <w n="2.5">froids</w> <w n="2.6">qui</w> <w n="2.7">pleuvent</w> <w n="2.8">goutte</w> <w n="2.9">à</w> <w n="2.10">goutte</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Ô</w> <w n="3.2">femme</w>, <w n="3.3">si</w> <w n="3.4">la</w> <w n="3.5">vie</w> <w n="3.6">et</w> <w n="3.7">le</w> <w n="3.8">mensonge</w> <w n="3.9">humains</w></l>
							<l n="4" num="1.4"><w n="4.1">Ont</w> <w n="4.2">desséché</w> <w n="4.3">ton</w> <w n="4.4">cœur</w> <w n="4.5">jusqu</w>’<w n="4.6">au</w> <w n="4.7">mépris</w> <w n="4.8">du</w> <w n="4.9">doute</w> ;</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Si</w> <w n="5.2">tu</w> <w n="5.3">n</w>’<w n="5.4">as</w> <w n="5.5">plus</w> <w n="5.6">de</w> <w n="5.7">vœux</w> <w n="5.8">pour</w> <w n="5.9">les</w> <w n="5.10">bonheurs</w> <w n="5.11">d</w>’<w n="5.12">autrui</w>,</l>
							<l n="6" num="2.2"><w n="6.1">D</w>’<w n="6.2">effroi</w> <w n="6.3">pour</w> <w n="6.4">tes</w> <w n="6.5">dangers</w>, <w n="6.6">de</w> <w n="6.7">larmes</w> <w n="6.8">pour</w> <w n="6.9">tes</w> <w n="6.10">peines</w> ;</l>
							<l n="7" num="2.3"><w n="7.1">Si</w> <w n="7.2">le</w> <w n="7.3">mal</w> <w n="7.4">qu</w>’<w n="7.5">on</w> <w n="7.6">te</w> <w n="7.7">fait</w> <w n="7.8">glisse</w> <w n="7.9">sur</w> <w n="7.10">ton</w> <w n="7.11">ennui</w></l>
							<l n="8" num="2.4"><w n="8.1">Sans</w> <w n="8.2">pouvoir</w> <w n="8.3">secouer</w> <w n="8.4">le</w> <w n="8.5">sommeil</w> <w n="8.6">de</w> <w n="8.7">tes</w> <w n="8.8">haines</w> ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Si</w> <w n="9.2">ton</w> <w n="9.3">âme</w> <w n="9.4">embrumée</w>, <w n="9.5">ô</w> <w n="9.6">fille</w> <w n="9.7">d</w>’<w n="9.8">Ossian</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Par</w> <w n="10.2">un</w> <w n="10.3">dégoût</w> <w n="10.4">dont</w> <w n="10.5">rien</w> <w n="10.6">ne</w> <w n="10.7">doit</w> <w n="10.8">plus</w> <w n="10.9">la</w> <w n="10.10">distraire</w></l>
							<l n="11" num="3.3"><w n="11.1">S</w>’<w n="11.2">est</w> <w n="11.3">prise</w> <w n="11.4">pour</w> <w n="11.5">la</w> <w n="11.6">mort</w> <w n="11.7">d</w>’<w n="11.8">un</w> <w n="11.9">culte</w> <w n="11.10">patient</w> :</l>
							<l n="12" num="3.4"><w n="12.1">Pourquoi</w> <w n="12.2">donc</w> <w n="12.3">as</w>-<w n="12.4">tu</w> <w n="12.5">peur</w> <w n="12.6">de</w> <w n="12.7">moi</w> ? <w n="12.8">Je</w> <w n="12.9">suis</w> <w n="12.10">ton</w> <w n="12.11">frère</w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Hymen</w>, <w n="13.2">Hymenæé</w> ! <w n="13.3">C</w>’<w n="13.4">est</w> <w n="13.5">l</w>’<w n="13.6">heure</w> <w n="13.7">des</w> <w n="13.8">baisers</w>…</l>
							<l n="14" num="4.2"><w n="14.1">Viens</w>-<w n="14.2">nous</w>-<w n="14.3">en</w> : <w n="14.4">les</w> <w n="14.5">corbeaux</w> <w n="14.6">croassent</w> <w n="14.7">sur</w> <w n="14.8">les</w> <w n="14.9">tombes</w>.</l>
							<l n="15" num="4.3"><w n="15.1">Hymen</w> ! <w n="15.2">Et</w> <w n="15.3">que</w> <w n="15.4">l</w>’<w n="15.5">autel</w> <w n="15.6">ressemble</w> <w n="15.7">aux</w> <w n="15.8">épousés</w> !</l>
							<l n="16" num="4.4"><w n="16.1">Hymen</w> ! <w n="16.2">Et</w> <w n="16.3">que</w> <w n="16.4">le</w> <w n="16.5">nid</w> <w n="16.6">soit</w> <w n="16.7">digne</w> <w n="16.8">des</w> <w n="16.9">colombes</w> !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Au</w> <w n="17.2">fond</w> <w n="17.3">des</w> <w n="17.4">nuits</w> ! <w n="17.5">Je</w> <w n="17.6">sais</w> <w n="17.7">un</w> <w n="17.8">grand</w> <w n="17.9">bois</w> <w n="17.10">où</w> <w n="17.11">l</w>’<w n="17.12">hiver</w></l>
							<l n="18" num="5.2"><w n="18.1">Suspend</w> <w n="18.2">des</w> <w n="18.3">fruits</w> <w n="18.4">de</w> <w n="18.5">glace</w> <w n="18.6">autour</w> <w n="18.7">des</w> <w n="18.8">fleurs</w> <w n="18.9">de</w> <w n="18.10">givre</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Un</w> <w n="19.2">bois</w> <w n="19.3">désespéré</w>, <w n="19.4">paradis</w> <w n="19.5">de</w> <w n="19.6">l</w>’<w n="19.7">enfer</w>,</l>
							<l n="20" num="5.4"><w n="20.1">Où</w> <w n="20.2">noir</w>, <w n="20.3">nu</w>, <w n="20.4">chaque</w> <w n="20.5">tronc</w> <w n="20.6">dort</w> <w n="20.7">crispé</w> <w n="20.8">comme</w> <w n="20.9">un</w> <w n="20.10">guivre</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Nous</w> <w n="21.2">nous</w> <w n="21.3">en</w> <w n="21.4">irons</w> <w n="21.5">loin</w>, <w n="21.6">très</w> <w n="21.7">loin</w>, <w n="21.8">tous</w> <w n="21.9">deux</w>, <w n="21.10">tout</w> <w n="21.11">seuls</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Nous</w> <w n="22.2">accoupler</w> <w n="22.3">sans</w> <w n="22.4">bruit</w> <w n="22.5">sous</w> <w n="22.6">les</w> <w n="22.7">cieux</w> <w n="22.8">sans</w> <w n="22.9">étoiles</w> :</l>
							<l n="23" num="6.3"><w n="23.1">La</w> <w n="23.2">neige</w> <w n="23.3">déploiera</w> <w n="23.4">ses</w> <w n="23.5">tranquilles</w> <w n="23.6">linceuls</w></l>
							<l n="24" num="6.4"><w n="24.1">Pour</w> <w n="24.2">mettre</w> <w n="24.3">à</w> <w n="24.4">notre</w> <w n="24.5">lit</w> <w n="24.6">des</w> <w n="24.7">draps</w> <w n="24.8">blancs</w> <w n="24.9">et</w> <w n="24.10">des</w> <w n="24.11">voiles</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Nos</w> <w n="25.2">râles</w> <w n="25.3">attendris</w> <w n="25.4">effraieront</w> <w n="25.5">les</w> <w n="25.6">hiboux</w>,</l>
							<l n="26" num="7.2"><w n="26.1">Les</w> <w n="26.2">crapauds</w> <w n="26.3">gémiront</w> <w n="26.4">de</w> <w n="26.5">nous</w> <w n="26.6">voir</w> <w n="26.7">nous</w> <w n="26.8">étreindre</w>,</l>
							<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">la</w> <w n="27.3">lune</w>, <w n="27.4">veilleuse</w> <w n="27.5">extatique</w> <w n="27.6">des</w> <w n="27.7">fous</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Vers</w> <w n="28.2">le</w> <w n="28.3">vague</w> <w n="28.4">horizon</w> <w n="28.5">descendra</w> <w n="28.6">pour</w> <w n="28.7">nous</w> <w n="28.8">plaindre</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Alors</w>, <w n="29.2">peut</w>-<w n="29.3">être</w>, <w n="29.4">enfin</w>, <w n="29.5">pour</w> <w n="29.6">la</w> <w n="29.7">dernière</w> <w n="29.8">fois</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Envahis</w> <w n="30.2">par</w> <w n="30.3">l</w>’<w n="30.4">angoisse</w> <w n="30.5">et</w> <w n="30.6">l</w>’<w n="30.7">horreur</w> <w n="30.8">des</w> <w n="30.9">cieux</w> <w n="30.10">blêmes</w>,</l>
							<l n="31" num="8.3"><w n="31.1">Émus</w> <w n="31.2">de</w> <w n="31.3">la</w> <w n="31.4">tristesse</w> <w n="31.5">amicale</w> <w n="31.6">des</w> <w n="31.7">bois</w>,</l>
							<l n="32" num="8.4"><w n="32.1">Nous</w> <w n="32.2">trouverons</w> <w n="32.3">des</w> <w n="32.4">pleurs</w> <w n="32.5">à</w> <w n="32.6">verser</w> <w n="32.7">sur</w> <w n="32.8">nous</w>-<w n="32.9">mêmes</w> !</l>
						</lg>
						<ab type="star">⁂</ab>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">Oh</w> ! <w n="33.2">t</w>’<w n="33.3">avoir</w> <w n="33.4">rencontrée</w> <w n="33.5">aux</w> <w n="33.6">jours</w> <w n="33.7">de</w> <w n="33.8">nos</w> <w n="33.9">candeurs</w>,</l>
							<l n="34" num="9.2"><w n="34.1">Sauvage</w>, <w n="34.2">avec</w> <w n="34.3">des</w> <w n="34.4">yeux</w> <w n="34.5">aussi</w> <w n="34.6">clairs</w> <w n="34.7">que</w> <w n="34.8">tes</w> <w n="34.9">rêves</w>,</l>
							<l n="35" num="9.3"><w n="35.1">Ignorant</w> <w n="35.2">comme</w> <w n="35.3">moi</w> <w n="35.4">le</w> <w n="35.5">monde</w> <w n="35.6">et</w> <w n="35.7">ses</w> <w n="35.8">laideurs</w>,</l>
							<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">mêlant</w> <w n="36.3">tes</w> <w n="36.4">chansons</w> <w n="36.5">à</w> <w n="36.6">la</w> <w n="36.7">chanson</w> <w n="36.8">des</w> <w n="36.9">grèves</w> !</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">Avoir</w> <w n="37.2">sur</w> <w n="37.3">ton</w> <w n="37.4">front</w> <w n="37.5">brun</w> <w n="37.6">débrouillé</w> <w n="37.7">tes</w> <w n="37.8">cheveux</w>,</l>
							<l n="38" num="10.2"><w n="38.1">Baisé</w> <w n="38.2">tes</w> <w n="38.3">cils</w> <w n="38.4">câlins</w> <w n="38.5">et</w> <w n="38.6">tes</w> <w n="38.7">lèvres</w> <w n="38.8">dociles</w>,</l>
							<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">t</w>’<w n="39.3">avoir</w> <w n="39.4">dit</w> : « <w n="39.5">Veux</w>-<w n="39.6">tu</w> ? » <w n="39.7">Tu</w> <w n="39.8">m</w>’<w n="39.9">aurais</w> <w n="39.10">dit</w> : « <w n="39.11">Je</w> <w n="39.12">veux</w>. »</l>
							<l n="40" num="10.4"><w n="40.1">Et</w> <w n="40.2">nous</w> <w n="40.3">serions</w> <w n="40.4">partis</w> <w n="40.5">ensemble</w> <w n="40.6">pour</w> <w n="40.7">des</w> <w n="40.8">îles</w>.</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1"><w n="41.1">Par</w> <w n="41.2">delà</w> <w n="41.3">le</w> <w n="41.4">grand</w> <w n="41.5">champ</w> <w n="41.6">des</w> <w n="41.7">mers</w> <w n="41.8">aux</w> <w n="41.9">sillons</w> <w n="41.10">bleus</w>,</l>
							<l n="42" num="11.2"><w n="42.1">Nous</w> <w n="42.2">aurions</w>, <w n="42.3">sans</w> <w n="42.4">souci</w> <w n="42.5">du</w> <w n="42.6">temps</w> <w n="42.7">et</w> <w n="42.8">de</w> <w n="42.9">l</w>’<w n="42.10">espace</w>,</l>
							<l n="43" num="11.3"><w n="43.1">Cherché</w> <w n="43.2">le</w> <w n="43.3">dernier</w> <w n="43.4">coin</w> <w n="43.5">des</w> <w n="43.6">Édens</w> <w n="43.7">fabuleux</w></l>
							<l n="44" num="11.4"><w n="44.1">Où</w> <w n="44.2">les</w> <w n="44.3">petits</w> <w n="44.4">oiseaux</w> <w n="44.5">n</w>’<w n="44.6">ont</w> <w n="44.7">pas</w> <w n="44.8">peur</w> <w n="44.9">quand</w> <w n="44.10">on</w> <w n="44.11">passe</w>.</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1"><w n="45.1">Nous</w> <w n="45.2">aurions</w> <w n="45.3">empli</w> <w n="45.4">l</w>’<w n="45.5">air</w> <w n="45.6">d</w>’<w n="45.7">un</w> <w n="45.8">bonheur</w> <w n="45.9">sans</w> <w n="45.10">jaloux</w>,</l>
							<l n="46" num="12.2"><w n="46.1">Communiquant</w> <w n="46.2">la</w> <w n="46.3">joie</w> <w n="46.4">et</w> <w n="46.5">faisant</w> <w n="46.6">la</w> <w n="46.7">lumière</w> ;</l>
							<l n="47" num="12.3"><w n="47.1">La</w> <w n="47.2">terre</w>, <w n="47.3">autour</w> <w n="47.4">de</w> <w n="47.5">nous</w>, <w n="47.6">eût</w> <w n="47.7">gardé</w> <w n="47.8">comme</w> <w n="47.9">nous</w></l>
							<l n="48" num="12.4"><w n="48.1">L</w>’<w n="48.2">éternel</w> <w n="48.3">renouveau</w> <w n="48.4">de</w> <w n="48.5">sa</w> <w n="48.6">beauté</w> <w n="48.7">première</w>.</l>
						</lg>
						<lg n="13">
							<l n="49" num="13.1"><w n="49.1">Ah</w> ! <w n="49.2">la</w> <w n="49.3">douceur</w> <w n="49.4">de</w> <w n="49.5">vivre</w> <w n="49.6">indiciblement</w> <w n="49.7">pur</w> !</l>
							<l n="50" num="13.2"><w n="50.1">Faire</w> <w n="50.2">son</w> <w n="50.3">avenir</w> <w n="50.4">semblable</w> <w n="50.5">à</w> <w n="50.6">son</w> <w n="50.7">enfance</w>,</l>
							<l n="51" num="13.3"><w n="51.1">Rester</w> <w n="51.2">une</w> <w n="51.3">âme</w> <w n="51.4">en</w> <w n="51.5">fleur</w> <w n="51.6">quand</w> <w n="51.7">l</w>’<w n="51.8">esprit</w> <w n="51.9">devient</w> <w n="51.10">mûr</w>,</l>
							<l n="52" num="13.4"><w n="52.1">Et</w> <w n="52.2">vieillir</w> <w n="52.3">doucement</w> <w n="52.4">sans</w> <w n="52.5">crainte</w> <w n="52.6">et</w> <w n="52.7">sans</w> <w n="52.8">défense</w> :</l>
						</lg>
						<lg n="14">
							<l n="53" num="14.1"><w n="53.1">Vieillir</w> <w n="53.2">sans</w> <w n="53.3">comparer</w> <w n="53.4">les</w> <w n="53.5">temps</w> <w n="53.6">à</w> <w n="53.7">d</w>’<w n="53.8">autres</w> <w n="53.9">temps</w>,</l>
							<l n="54" num="14.2"><w n="54.1">Croire</w> <w n="54.2">en</w> <w n="54.3">Dieu</w>, <w n="54.4">croire</w> <w n="54.5">en</w> <w n="54.6">soi</w>, <w n="54.7">croire</w> <w n="54.8">en</w> <w n="54.9">tout</w> <w n="54.10">ce</w> <w n="54.11">qu</w>’<w n="54.12">on</w> <w n="54.13">aime</w> !</l>
							<l n="55" num="14.3">— <w n="55.1">Seigneur</w>, <w n="55.2">Seigneur</w> ! <w n="55.3">Prenez</w> <w n="55.4">pitié</w> <w n="55.5">des</w> <w n="55.6">pénitents</w></l>
							<l n="56" num="14.4"><w n="56.1">Et</w> <w n="56.2">versez</w> <w n="56.3">sur</w> <w n="56.4">nos</w> <w n="56.5">cœurs</w> <w n="56.6">le</w> <w n="56.7">pardon</w> <w n="56.8">du</w> <w n="56.9">blasphème</w> !</l>
						</lg>
					</div></body></text></TEI>