<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR120">
						<head type="main">EN CRÈTE</head>
						<opener>
							<salute>À VICTOR D’AURIAC</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Midi</w> ! <w n="1.2">Le</w> <w n="1.3">ciel</w> <w n="1.4">profond</w> <w n="1.5">est</w> <w n="1.6">d</w>’<w n="1.7">un</w> <w n="1.8">cobalt</w> <w n="1.9">intense</w>.</l>
							<l n="2" num="1.2"><w n="2.1">Comme</w> <w n="2.2">une</w> <w n="2.3">lampe</w> <w n="2.4">d</w>’<w n="2.5">or</w> <w n="2.6">pendue</w> <w n="2.7">au</w> <w n="2.8">zénith</w> <w n="2.9">bleu</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">soleil</w> <w n="3.3">qui</w> <w n="3.4">montait</w> <w n="3.5">s</w>’<w n="3.6">arrête</w> <w n="3.7">et</w> <w n="3.8">se</w> <w n="3.9">balance</w> :</l>
							<l n="4" num="1.4"><w n="4.1">Ses</w> <w n="4.2">rayons</w> <w n="4.3">verticaux</w> <w n="4.4">vibrent</w> <w n="4.5">dans</w> <w n="4.6">l</w>’<w n="4.7">air</w> <w n="4.8">en</w> <w n="4.9">feu</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">monts</w>, <w n="5.3">les</w> <w n="5.4">champs</w>, <w n="5.5">baignés</w> <w n="5.6">de</w> <w n="5.7">clartés</w> <w n="5.8">odorantes</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Rêvent</w> <w n="6.2">languissamment</w> <w n="6.3">dans</w> <w n="6.4">leur</w> <w n="6.5">vaste</w> <w n="6.6">sommeil</w>.</l>
							<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">île</w> <w n="7.3">nage</w> <w n="7.4">au</w> <w n="7.5">milieu</w> <w n="7.6">des</w> <w n="7.7">vagues</w> <w n="7.8">transparentes</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Dont</w> <w n="8.2">chacune</w> <w n="8.3">miroite</w> <w n="8.4">et</w> <w n="8.5">reflète</w> <w n="8.6">un</w> <w n="8.7">soleil</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">mer</w> <w n="9.3">chante</w> : <w n="9.4">le</w> <w n="9.5">flot</w> <w n="9.6">tiède</w> <w n="9.7">et</w> <w n="9.8">blanchi</w> <w n="9.9">d</w>’<w n="9.10">écume</w></l>
							<l n="10" num="3.2"><w n="10.1">Lèche</w> <w n="10.2">le</w> <w n="10.3">sable</w> <w n="10.4">ardent</w> <w n="10.5">qui</w> <w n="10.6">fume</w> <w n="10.7">dans</w> <w n="10.8">le</w> <w n="10.9">port</w>.</l>
							<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">parfum</w> <w n="11.3">lourd</w> <w n="11.4">des</w> <w n="11.5">fleurs</w> <w n="11.6">pèse</w>, <w n="11.7">comme</w> <w n="11.8">une</w> <w n="11.9">brume</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">l</w>’<w n="12.3">atmosphère</w> <w n="12.4">épaisse</w> <w n="12.5">où</w> <w n="12.6">la</w> <w n="12.7">brise</w> <w n="12.8">s</w>’<w n="12.9">endort</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">sève</w> <w n="13.3">bout</w> ; <w n="13.4">le</w> <w n="13.5">fruit</w> <w n="13.6">est</w> <w n="13.7">mûr</w> ; <w n="13.8">la</w> <w n="13.9">vie</w> <w n="13.10">éclate</w> :</l>
							<l n="14" num="4.2"><w n="14.1">Les</w> <w n="14.2">muscats</w> <w n="14.3">jaunissants</w> <w n="14.4">cuisent</w> <w n="14.5">sur</w> <w n="14.6">les</w> <w n="14.7">coteaux</w> ;</l>
							<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">pâtre</w>, <w n="15.3">désertant</w> <w n="15.4">la</w> <w n="15.5">lande</w> <w n="15.6">aride</w> <w n="15.7">et</w> <w n="15.8">plate</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Sous</w> <w n="16.2">les</w> <w n="16.3">blancs</w> <w n="16.4">oliviers</w> <w n="16.5">a</w> <w n="16.6">conduit</w> <w n="16.7">ses</w> <w n="16.8">troupeaux</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">dans</w> <w n="17.3">le</w> <w n="17.4">bois</w> <w n="17.5">sacré</w>, <w n="17.6">sa</w> <w n="17.7">royale</w> <w n="17.8">retraite</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Sous</w> <w n="18.2">les</w> <w n="18.3">myrtes</w> <w n="18.4">neigeux</w> <w n="18.5">du</w> <w n="18.6">temple</w> <w n="18.7">d</w>’<w n="18.8">Astarté</w>,</l>
							<l n="19" num="5.3"><w n="19.1">La</w> <w n="19.2">fille</w> <w n="19.3">du</w> <w n="19.4">Soleil</w>, <w n="19.5">Pasiphaë</w> <w n="19.6">de</w> <w n="19.7">Crète</w>,</l>
							<l n="20" num="5.4"><w n="20.1">Moule</w> <w n="20.2">dans</w> <w n="20.3">les</w> <w n="20.4">coussins</w> <w n="20.5">sa</w> <w n="20.6">brune</w> <w n="20.7">nudité</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Les</w> <w n="21.2">tons</w> <w n="21.3">mats</w> <w n="21.4">de</w> <w n="21.5">sa</w> <w n="21.6">chair</w> <w n="21.7">ont</w> <w n="21.8">des</w> <w n="21.9">reflets</w> <w n="21.10">d</w>’<w n="21.11">ivoire</w> ;</l>
							<l n="22" num="6.2"><w n="22.1">Ses</w> <w n="22.2">cheveux</w> <w n="22.3">sur</w> <w n="22.4">son</w> <w n="22.5">sein</w> <w n="22.6">roulent</w> <w n="22.7">comme</w> <w n="22.8">des</w> <w n="22.9">flots</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">l</w>’<w n="23.3">éclair</w> <w n="23.4">brille</w> <w n="23.5">au</w> <w n="23.6">fond</w> <w n="23.7">de</w> <w n="23.8">sa</w> <w n="23.9">prunelle</w> <w n="23.10">noire</w>,</l>
							<l n="24" num="6.4"><w n="24.1">Sous</w> <w n="24.2">le</w> <w n="24.3">voile</w> <w n="24.4">lascif</w> <w n="24.5">des</w> <w n="24.6">cils</w> <w n="24.7">à</w> <w n="24.8">demi</w>-<w n="24.9">clos</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">La</w> <w n="25.2">voilà</w> : <w n="25.3">c</w>’<w n="25.4">est</w> <w n="25.5">la</w> <w n="25.6">Reine</w> <w n="25.7">aux</w> <w n="25.8">fureurs</w> <w n="25.9">hystériques</w> !</l>
							<l n="26" num="7.2"><w n="26.1">Pour</w> <w n="26.2">éteindre</w> <w n="26.3">l</w>’<w n="26.4">ardeur</w> <w n="26.5">de</w> <w n="26.6">ses</w> <w n="26.7">sens</w> <w n="26.8">allumés</w>,</l>
							<l n="27" num="7.3"><w n="27.1">La</w> <w n="27.2">voilà</w> <w n="27.3">se</w> <w n="27.4">cabrant</w>, <w n="27.5">frottant</w> <w n="27.6">ses</w> <w n="27.7">chairs</w> <w n="27.8">lubriques</w></l>
							<l n="28" num="7.4"><w n="28.1">Sur</w> <w n="28.2">le</w> <w n="28.3">baiser</w> <w n="28.4">soyeux</w> <w n="28.5">des</w> <w n="28.6">tissus</w> <w n="28.7">parfumés</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Hélios</w> ! <w n="29.2">Tu</w> <w n="29.3">la</w> <w n="29.4">vois</w>, <w n="29.5">crispant</w> <w n="29.6">ses</w> <w n="29.7">membres</w> <w n="29.8">lisses</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Mordant</w> <w n="30.2">ses</w> <w n="30.3">propres</w> <w n="30.4">bras</w> <w n="30.5">et</w> <w n="30.6">tordant</w> <w n="30.7">ses</w> <w n="30.8">cheveux</w> ;</l>
							<l n="31" num="8.3"><w n="31.1">Une</w> <w n="31.2">peau</w> <w n="31.3">de</w> <w n="31.4">lion</w> <w n="31.5">serrée</w> <w n="31.6">entre</w> <w n="31.7">ses</w> <w n="31.8">cuisses</w>,</l>
							<l n="32" num="8.4"><w n="32.1">Elle</w> <w n="32.2">s</w>’<w n="32.3">arque</w>, <w n="32.4">du</w> <w n="32.5">cou</w> <w n="32.6">jusqu</w>’<w n="32.7">aux</w> <w n="32.8">jarrets</w> <w n="32.9">nerveux</w> !</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">En</w> <w n="33.2">vain</w> <w n="33.3">trente</w> <w n="33.4">guerriers</w>, <w n="33.5">les</w> <w n="33.6">plus</w> <w n="33.7">beaux</w> <w n="33.8">de</w> <w n="33.9">la</w> <w n="33.10">Grèce</w>,</l>
							<l n="34" num="9.2"><w n="34.1">Ont</w> <w n="34.2">sous</w> <w n="34.3">leurs</w> <w n="34.4">reins</w> <w n="34.5">musclés</w> <w n="34.6">pétri</w> <w n="34.7">son</w> <w n="34.8">torse</w> <w n="34.9">nu</w> :</l>
							<l n="35" num="9.3"><w n="35.1">Surexcités</w> <w n="35.2">par</w> <w n="35.3">leur</w> <w n="35.4">impuissante</w> <w n="35.5">caresse</w>,</l>
							<l n="36" num="9.4"><w n="36.1">Ses</w> <w n="36.2">flancs</w> <w n="36.3">inassouvis</w> <w n="36.4">ont</w> <w n="36.5">rêvé</w> <w n="36.6">d</w>’<w n="36.7">inconnu</w>.</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">En</w> <w n="37.2">vain</w>, <w n="37.3">pour</w> <w n="37.4">la</w> <w n="37.5">calmer</w>, <w n="37.6">Bacchantes</w> <w n="37.7">et</w> <w n="37.8">Tribades</w></l>
							<l n="38" num="10.2"><w n="38.1">De</w> <w n="38.2">leurs</w> <w n="38.3">touchers</w> <w n="38.4">savants</w> <w n="38.5">ont</w> <w n="38.6">énervé</w> <w n="38.7">son</w> <w n="38.8">corps</w> ;</l>
							<l n="39" num="10.3"><w n="39.1">Elle</w> <w n="39.2">a</w> <w n="39.3">pris</w> <w n="39.4">en</w> <w n="39.5">dégoût</w> <w n="39.6">ces</w> <w n="39.7">voluptés</w> <w n="39.8">trop</w> <w n="39.9">fades</w> :</l>
							<l n="40" num="10.4"><w n="40.1">La</w> <w n="40.2">Fille</w> <w n="40.3">du</w> <w n="40.4">Soleil</w> <w n="40.5">veut</w> <w n="40.6">des</w> <w n="40.7">muscles</w> <w n="40.8">plus</w> <w n="40.9">forts</w> !</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1"><w n="41.1">Or</w>, <w n="41.2">elle</w> <w n="41.3">a</w> <w n="41.4">vu</w> <w n="41.5">là</w>-<w n="41.6">bas</w>, <w n="41.7">sur</w> <w n="41.8">les</w> <w n="41.9">fauves</w> <w n="41.10">lagunes</w>,</l>
							<l n="42" num="11.2"><w n="42.1">Dans</w> <w n="42.2">la</w> <w n="42.3">fureur</w> <w n="42.4">du</w> <w n="42.5">rut</w> <w n="42.6">passer</w> <w n="42.7">un</w> <w n="42.8">taureau</w> <w n="42.9">blanc</w> :</l>
							<l n="43" num="11.3"><w n="43.1">Il</w> <w n="43.2">allait</w>, <w n="43.3">bondissant</w> <w n="43.4">près</w> <w n="43.5">des</w> <w n="43.6">génisses</w> <w n="43.7">brunes</w>,</l>
							<l n="44" num="11.4"><w n="44.1">Et</w> <w n="44.2">ses</w> <w n="44.3">rouges</w> <w n="44.4">naseaux</w> <w n="44.5">aspiraient</w> <w n="44.6">l</w>’<w n="44.7">air</w> <w n="44.8">brûlant</w>.</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1"><w n="45.1">Et</w> <w n="45.2">la</w> <w n="45.3">Reine</w> <w n="45.4">le</w> <w n="45.5">veut</w>, <w n="45.6">le</w> <w n="45.7">fier</w> <w n="45.8">taureau</w> <w n="45.9">de</w> <w n="45.10">Crète</w> !</l>
							<l n="46" num="12.2"><w n="46.1">Elle</w> <w n="46.2">veut</w> <w n="46.3">son</w> <w n="46.4">amour</w> <w n="46.5">superbe</w> <w n="46.6">et</w> <w n="46.7">vigoureux</w>…</l>
							<l n="47" num="12.3">— <w n="47.1">Dédale</w> <w n="47.2">l</w>’<w n="47.3">a</w> <w n="47.4">comprise</w> <w n="47.5">et</w> <w n="47.6">la</w> <w n="47.7">statue</w> <w n="47.8">est</w> <w n="47.9">prête</w> :</l>
							<l n="48" num="12.4"><w n="48.1">La</w> <w n="48.2">Génisse</w> <w n="48.3">de</w> <w n="48.4">bronze</w> <w n="48.5">entr</w>’<w n="48.6">ouvre</w> <w n="48.7">ses</w> <w n="48.8">flancs</w> <w n="48.9">creux</w>.</l>
						</lg>
					</div></body></text></TEI>