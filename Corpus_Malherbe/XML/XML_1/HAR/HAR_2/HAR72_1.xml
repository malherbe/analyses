<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR72">
						<head type="main">LE CHARRON</head>
						<opener>
							<salute>À CONSTANT COQUELIN</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Necker</w> <w n="1.2">est</w> <w n="1.3">expulsé</w> <w n="1.4">du</w> <w n="1.5">royaume</w>. <w n="1.6">À</w> <w n="1.7">Versailles</w>,</l>
							<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">Étrangère</w> <w n="2.3">et</w> <w n="2.4">la</w> <w n="2.5">cour</w> <w n="2.6">rêvent</w> <w n="2.7">de</w> <w n="2.8">représailles</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Besenval</w> <w n="3.2">a</w> <w n="3.3">les</w> <w n="3.4">murs</w> <w n="3.5">et</w> <w n="3.6">quatre</w> <w n="3.7">régiments</w>.</l>
							<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">vieux</w> <w n="4.3">Broglie</w>, <w n="4.4">avec</w> <w n="4.5">trente</w> <w n="4.6">mille</w> <w n="4.7">Allemands</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Tient</w> <w n="5.2">la</w> <w n="5.3">plaine</w>, <w n="5.4">et</w> <w n="5.5">la</w> <w n="5.6">tient</w> <w n="5.7">en</w> <w n="5.8">province</w> <w n="5.9">conquise</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Saccageant</w>, <w n="6.2">n</w>’<w n="6.3">attendant</w> <w n="6.4">qu</w>’<w n="6.5">un</w> <w n="6.6">vœu</w> <w n="6.7">de</w> <w n="6.8">la</w> <w n="6.9">marquise</w></l>
							<l n="7" num="1.7"><w n="7.1">Pour</w> <w n="7.2">étrangler</w> <w n="7.3">Paris</w> <w n="7.4">d</w>’<w n="7.5">un</w> <w n="7.6">seul</w> <w n="7.7">coup</w> <w n="7.8">de</w> <w n="7.9">lacet</w>.</l>
						</lg>
						<lg n="2">
							<l n="8" num="2.1"><w n="8.1">Donc</w>, <w n="8.2">la</w> <w n="8.3">ville</w>, <w n="8.4">on</w> <w n="8.5">l</w>’<w n="8.6">affame</w>, <w n="8.7">et</w> <w n="8.8">son</w> <w n="8.9">bon</w> <w n="8.10">roi</w> <w n="8.11">le</w> <w n="8.12">sait</w> ;</l>
							<l n="9" num="2.2"><w n="9.1">Le</w> <w n="9.2">peuple</w>, <w n="9.3">on</w> <w n="9.4">le</w> <w n="9.5">trahit</w> ; <w n="9.6">la</w> <w n="9.7">patrie</w>, <w n="9.8">on</w> <w n="9.9">la</w> <w n="9.10">pille</w>.</l>
							<l n="10" num="2.3"><w n="10.1">Alors</w> <w n="10.2">un</w> <w n="10.3">cri</w> <w n="10.4">tonna</w> <w n="10.5">dans</w> <w n="10.6">l</w>’<w n="10.7">air</w> : « <w n="10.8">À</w> <w n="10.9">la</w> <w n="10.10">Bastille</w> ! »</l>
							<l n="11" num="2.4"><w n="11.1">Et</w> <w n="11.2">formidablement</w> <w n="11.3">tout</w> <w n="11.4">Paris</w> <w n="11.5">se</w> <w n="11.6">leva</w>.</l>
							<l n="12" num="2.5"><w n="12.1">Point</w> <w n="12.2">de</w> <w n="12.3">canons</w>, <w n="12.4">point</w> <w n="12.5">de</w> <w n="12.6">fusils</w>. <w n="12.7">N</w>’<w n="12.8">importe</w> : <w n="12.9">on</w> <w n="12.10">va</w>.</l>
							<l n="13" num="2.6"><w n="13.1">On</w> <w n="13.2">veut</w>. <w n="13.3">Poussant</w> <w n="13.4">son</w> <w n="13.5">flux</w> <w n="13.6">et</w> <w n="13.7">remuant</w> <w n="13.8">sa</w> <w n="13.9">houle</w>,</l>
							<l n="14" num="2.7"><w n="14.1">Ce</w> <w n="14.2">flot</w> <w n="14.3">des</w> <w n="14.4">volontés</w>, <w n="14.5">cette</w> <w n="14.6">mer</w> <w n="14.7">d</w>’<w n="14.8">âmes</w>, <w n="14.9">roule</w>.</l>
							<l n="15" num="2.8"><w n="15.1">À</w> <w n="15.2">chaque</w> <w n="15.3">rue</w>, <w n="15.4">aux</w> <w n="15.5">quais</w>, <w n="15.6">aux</w> <w n="15.7">ponts</w>, <w n="15.8">aux</w> <w n="15.9">carrefours</w>,</l>
							<l n="16" num="2.9"><w n="16.1">Multipliant</w> <w n="16.2">sa</w> <w n="16.3">masse</w> <w n="16.4">écrasante</w>, <w n="16.5">et</w> <w n="16.6">toujours</w></l>
							<l n="17" num="2.10"><w n="17.1">Plus</w> <w n="17.2">profonde</w>, <w n="17.3">et</w> <w n="17.4">toujours</w> <w n="17.5">plus</w> <w n="17.6">dense</w> <w n="17.7">et</w> <w n="17.8">plus</w> <w n="17.9">serrée</w>,</l>
							<l n="18" num="2.11"><w n="18.1">Elle</w> <w n="18.2">élargit</w> <w n="18.3">l</w>’<w n="18.4">ampleur</w> <w n="18.5">de</w> <w n="18.6">sa</w> <w n="18.7">lourde</w> <w n="18.8">marée</w>.</l>
							<l n="19" num="2.12"><w n="19.1">L</w>’<w n="19.2">air</w> <w n="19.3">tremble</w> ; <w n="19.4">et</w> <w n="19.5">tout</w> <w n="19.6">au</w> <w n="19.7">fond</w> <w n="19.8">des</w> <w n="19.9">horizons</w>, <w n="19.10">là</w>-<w n="19.11">bas</w>,</l>
							<l n="20" num="2.13"><w n="20.1">Un</w> <w n="20.2">retentissement</w> <w n="20.3">effroyable</w> <w n="20.4">de</w> <w n="20.5">pas</w>,</l>
							<l n="21" num="2.14"><w n="21.1">Sous</w> <w n="21.2">la</w> <w n="21.3">clarté</w> <w n="21.4">des</w> <w n="21.5">cieux</w>, <w n="21.6">gronde</w> <w n="21.7">comme</w> <w n="21.8">un</w> <w n="21.9">tonnerre</w>.</l>
						</lg>
						<lg n="3">
							<l n="22" num="3.1"><w n="22.1">Il</w> <w n="22.2">peina</w> <w n="22.3">deux</w> <w n="22.4">mille</w> <w n="22.5">ans</w>, <w n="22.6">ce</w> <w n="22.7">Peuple</w> <w n="22.8">débonnaire</w> :</l>
							<l n="23" num="3.2"><w n="23.1">Il</w> <w n="23.2">en</w> <w n="23.3">est</w> <w n="23.4">las</w>, <w n="23.5">et</w> <w n="23.6">l</w>’<w n="23.7">heure</w> <w n="23.8">a</w> <w n="23.9">sonné</w> <w n="23.10">de</w> <w n="23.11">finir</w>.</l>
							<l n="24" num="3.3"><w n="24.1">C</w>’<w n="24.2">est</w> <w n="24.3">le</w> <w n="24.4">Passé</w>, <w n="24.5">c</w>’<w n="24.6">est</w> <w n="24.7">le</w> <w n="24.8">Présent</w>, <w n="24.9">c</w>’<w n="24.10">est</w> <w n="24.11">l</w>’<w n="24.12">Avenir</w></l>
							<l n="25" num="3.4"><w n="25.1">Qui</w> <w n="25.2">vont</w> : <w n="25.3">c</w>’<w n="25.4">est</w> <w n="25.5">l</w>’<w n="25.6">unanime</w> <w n="25.7">humanité</w> <w n="25.8">qui</w> <w n="25.9">marche</w> ;</l>
							<l n="26" num="3.5"><w n="26.1">Et</w> <w n="26.2">la</w> <w n="26.3">mer</w> <w n="26.4">de</w> <w n="26.5">vengeance</w> <w n="26.6">apporte</w> <w n="26.7">aussi</w> <w n="26.8">son</w> <w n="26.9">arche</w>,</l>
							<l n="27" num="3.6"><w n="27.1">Arche</w> <w n="27.2">sainte</w> <w n="27.3">arrachée</w> <w n="27.4">au</w> <w n="27.5">déluge</w> <w n="27.6">des</w> <w n="27.7">rois</w> :</l>
							<l part="I" n="28" num="3.7"><w n="28.1">La</w> <w n="28.2">Liberté</w> ! </l>
							<l part="F" n="28" num="3.7"><w n="28.3">Sinistre</w>, <w n="28.4">avec</w> <w n="28.5">ses</w> <w n="28.6">hauts</w> <w n="28.7">murs</w> <w n="28.8">droits</w>,</l>
							<l n="29" num="3.8"><w n="29.1">La</w> <w n="29.2">Bastille</w>, <w n="29.3">debout</w>, <w n="29.4">dans</w> <w n="29.5">sa</w> <w n="29.6">robe</w> <w n="29.7">de</w> <w n="29.8">pierre</w>,</l>
							<l n="30" num="3.9"><w n="30.1">Hausse</w> <w n="30.2">rigidement</w> <w n="30.3">sa</w> <w n="30.4">masse</w> <w n="30.5">calme</w> <w n="30.6">et</w> <w n="30.7">fière</w></l>
						</lg>
						<lg n="4">
							<l n="31" num="4.1"><w n="31.1">Sur</w> <w n="31.2">laquelle</w> <w n="31.3">Justice</w> <w n="31.4">et</w> <w n="31.5">Haine</w> <w n="31.6">n</w>’<w n="31.7">ont</w> <w n="31.8">rien</w> <w n="31.9">pu</w>.</l>
							<l n="32" num="4.2"><w n="32.1">Le</w> <w n="32.2">bloc</w> <w n="32.3">royal</w> <w n="32.4">attend</w> : <w n="32.5">tel</w> <w n="32.6">un</w> <w n="32.7">lion</w> <w n="32.8">repu</w>,</l>
							<l n="33" num="4.3"><w n="33.1">Superbe</w>, <w n="33.2">et</w> <w n="33.3">tout</w> <w n="33.4">entier</w> <w n="33.5">ramassé</w> <w n="33.6">sur</w> <w n="33.7">son</w> <w n="33.8">torse</w>,</l>
							<l n="34" num="4.4"><w n="34.1">Dort</w> <w n="34.2">dans</w> <w n="34.3">la</w> <w n="34.4">majesté</w> <w n="34.5">terrible</w> <w n="34.6">de</w> <w n="34.7">sa</w> <w n="34.8">force</w>.</l>
						</lg>
						<lg n="5">
							<l n="35" num="5.1"><w n="35.1">L</w>’<w n="35.2">Océan</w> <w n="35.3">d</w>’<w n="35.4">hommes</w> <w n="35.5">va</w>, <w n="35.6">déferle</w> <w n="35.7">au</w> <w n="35.8">pied</w> <w n="35.9">des</w> <w n="35.10">tours</w>,</l>
							<l n="36" num="5.2"><w n="36.1">Reflue</w>, <w n="36.2">et</w>, <w n="36.3">noircissant</w> <w n="36.4">au</w> <w n="36.5">loin</w> <w n="36.6">les</w> <w n="36.7">alentours</w>,</l>
							<l n="37" num="5.3"><w n="37.1">S</w>’<w n="37.2">étale</w> <w n="37.3">en</w> <w n="37.4">nappes</w>, <w n="37.5">chaud</w> <w n="37.6">comme</w> <w n="37.7">un</w> <w n="37.8">torrent</w> <w n="37.9">de</w> <w n="37.10">lave</w>.</l>
							<l n="38" num="5.4"><w n="38.1">Aux</w> <w n="38.2">créneaux</w>, <w n="38.3">les</w> <w n="38.4">canons</w> <w n="38.5">dardent</w> <w n="38.6">leur</w> <w n="38.7">grand</w> <w n="38.8">œil</w> <w n="38.9">cave</w> ;</l>
							<l n="39" num="5.5"><w n="39.1">Les</w> <w n="39.2">meurtrières</w> <w n="39.3">sont</w> <w n="39.4">luisantes</w> <w n="39.5">de</w> <w n="39.6">fusils</w>,</l>
							<l n="40" num="5.6"><w n="40.1">Et</w>, <w n="40.2">guettant</w> <w n="40.3">les</w> <w n="40.4">élus</w> <w n="40.5">qu</w>’<w n="40.6">elle</w> <w n="40.7">a</w> <w n="40.8">déjà</w> <w n="40.9">choisis</w>,</l>
							<l n="41" num="5.7"><w n="41.1">La</w> <w n="41.2">mort</w> <w n="41.3">veille</w>. <w n="41.4">Hurlant</w> <w n="41.5">de</w> <w n="41.6">rage</w> <w n="41.7">et</w> <w n="41.8">d</w>’<w n="41.9">impuissance</w>,</l>
							<l n="42" num="5.8"><w n="42.1">L</w>’<w n="42.2">orage</w> <w n="42.3">humain</w> <w n="42.4">se</w> <w n="42.5">jette</w>, <w n="42.6">et</w> <w n="42.7">recule</w>, <w n="42.8">et</w> <w n="42.9">s</w>’<w n="42.10">élance</w>,</l>
							<l n="43" num="5.9"><w n="43.1">Et</w> <w n="43.2">fait</w> <w n="43.3">tourbillonner</w> <w n="43.4">le</w> <w n="43.5">remous</w> <w n="43.6">de</w> <w n="43.7">ses</w> <w n="43.8">flots</w></l>
							<l n="44" num="5.10"><w n="44.1">Qu</w>’<w n="44.2">il</w> <w n="44.3">brise</w> <w n="44.4">au</w> <w n="44.5">choc</w> <w n="44.6">des</w> <w n="44.7">murs</w> <w n="44.8">invinciblement</w> <w n="44.9">clos</w>.</l>
						</lg>
						<lg n="6">
							<l n="45" num="6.1"><w n="45.1">Or</w>, <w n="45.2">dans</w> <w n="45.3">ce</w> <w n="45.4">grondement</w> <w n="45.5">de</w> <w n="45.6">fureur</w> <w n="45.7">populaire</w>,</l>
							<l n="46" num="6.2"><w n="46.1">Un</w> <w n="46.2">homme</w> <w n="46.3">s</w>’<w n="46.4">avança</w> ; <w n="46.5">sans</w> <w n="46.6">un</w> <w n="46.7">cri</w>, <w n="46.8">sans</w> <w n="46.9">colère</w>,</l>
							<l n="47" num="6.3"><w n="47.1">Calme</w>, <w n="47.2">s</w>’<w n="47.3">étant</w> <w n="47.4">frayé</w> <w n="47.5">doucement</w> <w n="47.6">un</w> <w n="47.7">chemin</w>.</l>
							<l n="48" num="6.4"><w n="48.1">Il</w> <w n="48.2">franchit</w> <w n="48.3">les</w> <w n="48.4">fossés</w>, <w n="48.5">une</w> <w n="48.6">hache</w> <w n="48.7">à</w> <w n="48.8">la</w> <w n="48.9">main</w>.</l>
							<l n="49" num="6.5"><w n="49.1">Et</w> <w n="49.2">seul</w>, <w n="49.3">les</w> <w n="49.4">deux</w> <w n="49.5">bras</w> <w n="49.6">nus</w>, <w n="49.7">vint</w> <w n="49.8">prendre</w> <w n="49.9">la</w> <w n="49.10">Bastille</w>.</l>
						</lg>
						<lg n="7">
							<l n="50" num="7.1"><w n="50.1">On</w> <w n="50.2">le</w> <w n="50.3">vit</w> <w n="50.4">sur</w> <w n="50.5">le</w> <w n="50.6">mur</w> <w n="50.7">et</w> <w n="50.8">les</w> <w n="50.9">pieds</w> <w n="50.10">dans</w> <w n="50.11">la</w> <w n="50.12">grille</w></l>
							<l n="51" num="7.2"><w n="51.1">Chercher</w> <w n="51.2">son</w> <w n="51.3">équilibre</w> <w n="51.4">au</w> <w n="51.5">haut</w> <w n="51.6">du</w> <w n="51.7">pont</w>-<w n="51.8">levis</w>.</l>
							<l n="52" num="7.3"><w n="52.1">Il</w> <w n="52.2">se</w> <w n="52.3">mit</w> <w n="52.4">à</w> <w n="52.5">son</w> <w n="52.6">œuvre</w> : <w n="52.7">et</w>, <w n="52.8">détournant</w> <w n="52.9">les</w> <w n="52.10">vis</w>,</l>
							<l n="53" num="7.4"><w n="53.1">Faisant</w> <w n="53.2">sauter</w> <w n="53.3">les</w> <w n="53.4">clous</w> <w n="53.5">hors</w> <w n="53.6">des</w> <w n="53.7">poutres</w> <w n="53.8">de</w> <w n="53.9">chênes</w>,</l>
							<l n="54" num="7.5"><w n="54.1">Broyant</w> <w n="54.2">les</w> <w n="54.3">gonds</w>, <w n="54.4">tranchant</w> <w n="54.5">l</w>’<w n="54.6">anneau</w> <w n="54.7">rouillé</w> <w n="54.8">des</w> <w n="54.9">chaînes</w>,</l>
							<l n="55" num="7.6"><w n="55.1">Il</w> <w n="55.2">travailla</w> <w n="55.3">longtemps</w>, <w n="55.4">car</w> <w n="55.5">l</w>’<w n="55.6">ouvrage</w> <w n="55.7">était</w> <w n="55.8">dur</w>.</l>
							<l part="I" n="56" num="7.7">— <w n="56.1">Feu</w> ! </l>
							<l part="F" n="56" num="7.7"><w n="56.2">Les</w> <w n="56.3">balles</w> <w n="56.4">heurtaient</w> <w n="56.5">et</w> <w n="56.6">déchiraient</w> <w n="56.7">le</w> <w n="56.8">mur</w></l>
							<l n="57" num="7.8"><w n="57.1">Et</w> <w n="57.2">faisaient</w> <w n="57.3">des</w> <w n="57.4">trous</w> <w n="57.5">ronds</w> <w n="57.6">dans</w> <w n="57.7">la</w> <w n="57.8">blouse</w> <w n="57.9">volante</w>.</l>
							<l part="I" n="58" num="7.9">— <w n="58.1">Feu</w> ! </l>
							<l part="F" n="58" num="7.9"><w n="58.2">Tout</w> <w n="58.3">autour</w> <w n="58.4">de</w> <w n="58.5">lui</w> <w n="58.6">la</w> <w n="58.7">mort</w> <w n="58.8">passait</w>, <w n="58.9">sifflante</w>,</l>
							<l n="59" num="7.10"><w n="59.1">Et</w> <w n="59.2">ses</w> <w n="59.3">souffles</w> <w n="59.4">vibrants</w> <w n="59.5">l</w>’<w n="59.6">effleuraient</w> <w n="59.7">tout</w> <w n="59.8">entier</w>.</l>
							<l n="60" num="7.11"><w n="60.1">Mais</w> <w n="60.2">le</w> <w n="60.3">charron</w>, <w n="60.4">sans</w> <w n="60.5">plus</w> <w n="60.6">frémir</w> <w n="60.7">qu</w>’<w n="60.8">à</w> <w n="60.9">son</w> <w n="60.10">chantier</w>,</l>
							<l n="61" num="7.12"><w n="61.1">Levait</w> <w n="61.2">et</w> <w n="61.3">rabaissait</w> <w n="61.4">sa</w> <w n="61.5">hache</w>, <w n="61.6">lent</w> <w n="61.7">et</w> <w n="61.8">grave</w>.</l>
						</lg>
						<lg n="8">
							<l n="62" num="8.1"><w n="62.1">Ô</w> <w n="62.2">jours</w> ! <w n="62.3">Race</w> <w n="62.4">des</w> <w n="62.5">forts</w> ! <w n="62.6">Siècle</w> <w n="62.7">où</w> <w n="62.8">l</w>’<w n="62.9">on</w> <w n="62.10">était</w> <w n="62.11">brave</w>,</l>
							<l n="63" num="8.2"><w n="63.1">Âge</w> <w n="63.2">auguste</w> <w n="63.3">où</w> <w n="63.4">le</w> <w n="63.5">sol</w> <w n="63.6">enfantait</w> <w n="63.7">des</w> <w n="63.8">Titans</w> !</l>
							<l n="64" num="8.3"><w n="64.1">Le</w> <w n="64.2">vil</w> <w n="64.3">Peuple</w>, <w n="64.4">oublié</w> <w n="64.5">dans</w> <w n="64.6">l</w>’<w n="64.7">abîme</w> <w n="64.8">des</w> <w n="64.9">temps</w>,</l>
							<l n="65" num="8.4"><w n="65.1">Se</w> <w n="65.2">dressait</w> <w n="65.3">tout</w> <w n="65.4">à</w> <w n="65.5">coup</w> <w n="65.6">de</w> <w n="65.7">sa</w> <w n="65.8">terre</w> <w n="65.9">féconde</w>,</l>
							<l n="66" num="8.5"><w n="66.1">Et</w>, <w n="66.2">la</w> <w n="66.3">justice</w> <w n="66.4">en</w> <w n="66.5">main</w>, <w n="66.6">balayait</w> <w n="66.7">le</w> <w n="66.8">vieux</w> <w n="66.9">monde</w> !</l>
							<l n="67" num="8.6"><w n="67.1">Salut</w> <w n="67.2">à</w> <w n="67.3">vous</w>, <w n="67.4">manants</w>, <w n="67.5">roturiers</w> <w n="67.6">et</w> <w n="67.7">vilains</w> !</l>
						</lg>
						<lg n="9">
							<l n="68" num="9.1"><w n="68.1">Inutiles</w> <w n="68.2">héros</w> <w n="68.3">dont</w> <w n="68.4">nos</w> <w n="68.5">champs</w> <w n="68.6">étaient</w> <w n="68.7">pleins</w>,</l>
							<l n="69" num="9.2"><w n="69.1">Salut</w> ! <w n="69.2">Athlètes</w> <w n="69.3">nés</w> <w n="69.4">et</w> <w n="69.5">conçus</w> <w n="69.6">dans</w> <w n="69.7">l</w>’<w n="69.8">épreuve</w>,</l>
							<l n="70" num="9.3"><w n="70.1">Vaillants</w> <w n="70.2">régénérés</w> <w n="70.3">de</w> <w n="70.4">l</w>’<w n="70.5">humanité</w> <w n="70.6">neuve</w> !</l>
							<l n="71" num="9.4">— <w n="71.1">Nous</w> <w n="71.2">partons</w>, <w n="71.3">nous</w>, <w n="71.4">les</w> <w n="71.5">fils</w> <w n="71.6">d</w>’<w n="71.7">un</w> <w n="71.8">monde</w> <w n="71.9">agonisant</w></l>
							<l n="72" num="9.5"><w n="72.1">Dont</w> <w n="72.2">les</w> <w n="72.3">siècles</w> <w n="72.4">vécus</w> <w n="72.5">ont</w> <w n="72.6">épuisé</w> <w n="72.7">le</w> <w n="72.8">sang</w>…</l>
							<l n="73" num="9.6"><w n="73.1">Peuple</w>, <w n="73.2">peuple</w> ! <w n="73.3">Sur</w> <w n="73.4">les</w> <w n="73.5">débris</w> <w n="73.6">des</w> <w n="73.7">nobles</w> <w n="73.8">races</w>,</l>
							<l n="74" num="9.7"><w n="74.1">Germez</w>, <w n="74.2">multipliez</w>, <w n="74.3">croissez</w>, <w n="74.4">rameaux</w> <w n="74.5">vivaces</w> !</l>
							<l n="75" num="9.8"><w n="75.1">Épanouissez</w>-<w n="75.2">vous</w> <w n="75.3">sous</w> <w n="75.4">le</w> <w n="75.5">ciel</w> <w n="75.6">libre</w> <w n="75.7">et</w> <w n="75.8">pur</w> !</l>
							<l n="76" num="9.9"><w n="76.1">Serfs</w> <w n="76.2">de</w> <w n="76.3">l</w>’<w n="76.4">ère</w> <w n="76.5">passée</w> <w n="76.6">et</w> <w n="76.7">rois</w> <w n="76.8">du</w> <w n="76.9">temps</w> <w n="76.10">futur</w>,</l>
							<l n="77" num="9.10"><w n="77.1">Voilà</w> <w n="77.2">que</w> <w n="77.3">ce</w> <w n="77.4">charron</w> <w n="77.5">a</w> <w n="77.6">commencé</w> <w n="77.7">la</w> <w n="77.8">tâche</w>,</l>
							<l n="78" num="9.11"><w n="78.1">Et</w> <w n="78.2">taille</w> <w n="78.3">l</w>’<w n="78.4">avenir</w> <w n="78.5">humain</w> <w n="78.6">à</w> <w n="78.7">coups</w> <w n="78.8">de</w> <w n="78.9">hache</w> !</l>
						</lg>
						<lg n="10">
							<l n="79" num="10.1"><w n="79.1">Le</w> <w n="79.2">pont</w>-<w n="79.3">levis</w> <w n="79.4">grinça</w> <w n="79.5">sur</w> <w n="79.6">ses</w> <w n="79.7">gonds</w>. <w n="79.8">Un</w> <w n="79.9">moment</w>,</l>
							<l n="80" num="10.2"><w n="80.1">Dans</w> <w n="80.2">l</w>’<w n="80.3">air</w>, <w n="80.4">il</w> <w n="80.5">hésita</w>, <w n="80.6">puis</w>, <w n="80.7">d</w>’<w n="80.8">un</w> <w n="80.9">bloc</w>, <w n="80.10">lourdement</w>,</l>
							<l n="81" num="10.3"><w n="81.1">Tomba</w>, <w n="81.2">dans</w> <w n="81.3">le</w> <w n="81.4">bruit</w> <w n="81.5">sourd</w> <w n="81.6">d</w>’<w n="81.7">un</w> <w n="81.8">monde</w> <w n="81.9">qui</w> <w n="81.10">se</w> <w n="81.11">brise</w>.</l>
							<l part="I" n="82" num="10.4">« <w n="82.1">En</w> <w n="82.2">avant</w> ! <w n="82.3">En</w> <w n="82.4">avant</w> ! » </l>
							<l part="F" n="82" num="10.4"><w n="82.5">Rois</w>, <w n="82.6">la</w> <w n="82.7">Bastille</w> <w n="82.8">est</w> <w n="82.9">prise</w>.</l>
						</lg>
						<lg n="11">
							<l n="83" num="11.1">— <w n="83.1">Le</w> <w n="83.2">charron</w> <w n="83.3">rabaissa</w> <w n="83.4">sa</w> <w n="83.5">manche</w>. <w n="83.6">Il</w> <w n="83.7">dit</w> : « <w n="83.8">Voilà</w>, »</l>
							<l n="84" num="11.2"><w n="84.1">Puis</w>, <w n="84.2">simple</w>, <w n="84.3">ayant</w> <w n="84.4">défait</w> <w n="84.5">vingt</w> <w n="84.6">siècles</w>, <w n="84.7">s</w>’<w n="84.8">en</w> <w n="84.9">alla</w>.</l>
						</lg>
					</div></body></text></TEI>