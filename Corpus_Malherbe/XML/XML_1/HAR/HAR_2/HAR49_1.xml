<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES LOIS</head><div type="poem" key="HAR49">
						<head type="main">L’OCÉAN</head>
						<opener>
							<salute>À FRANÇOIS COPPÉE</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1">— <w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">l</w>’<w n="1.4">Océan</w> ! <w n="1.5">Vois</w>-<w n="1.6">tu</w> <w n="1.7">déferler</w> <w n="1.8">les</w> <w n="1.9">étoiles</w>,</l>
							<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">mer</w>, <w n="2.3">la</w> <w n="2.4">vaste</w> <w n="2.5">mer</w> <w n="2.6">immobile</w> <w n="2.7">à</w> <w n="2.8">nos</w> <w n="2.9">yeux</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">vogue</w> <w n="3.3">avec</w> <w n="3.4">ennui</w>, <w n="3.5">comme</w> <w n="3.6">un</w> <w n="3.7">vaisseau</w> <w n="3.8">sans</w> <w n="3.9">voiles</w>,</l>
							<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">lune</w>, <w n="4.3">nef</w> <w n="4.4">d</w>’<w n="4.5">argent</w> <w n="4.6">qui</w> <w n="4.7">glisse</w> <w n="4.8">sur</w> <w n="4.9">les</w> <w n="4.10">cieux</w> ?</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Loin</w> ! <w n="5.2">Vois</w>-<w n="5.3">tu</w> <w n="5.4">poudroyer</w>, <w n="5.5">très</w> <w n="5.6">loin</w>, <w n="5.7">des</w> <w n="5.8">vapeurs</w> <w n="5.9">blondes</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Tourbillon</w> <w n="6.2">d</w>’<w n="6.3">astres</w> <w n="6.4">clairs</w> <w n="6.5">dans</w> <w n="6.6">les</w> <w n="6.7">gouffres</w> <w n="6.8">vermeils</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Flot</w> <w n="7.2">dont</w> <w n="7.3">l</w>’<w n="7.4">écume</w> <w n="7.5">ardente</w> <w n="7.6">est</w> <w n="7.7">faite</w> <w n="7.8">avec</w> <w n="7.9">des</w> <w n="7.10">mondes</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Houle</w> <w n="8.2">insondable</w> <w n="8.3">où</w> <w n="8.4">bout</w> <w n="8.5">la</w> <w n="8.6">mousse</w> <w n="8.7">des</w> <w n="8.8">soleils</w> ?</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Regarde</w> ! <w n="9.2">L</w>’<w n="9.3">ombre</w> <w n="9.4">bouge</w>… <w n="9.5">Entends</w>-<w n="9.6">tu</w> <w n="9.7">le</w> <w n="9.8">silence</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Le</w> <w n="10.2">silence</w> <w n="10.3">que</w> <w n="10.4">font</w> <w n="10.5">ces</w> <w n="10.6">milliards</w> <w n="10.7">de</w> <w n="10.8">bruits</w> ?</l>
							<l n="11" num="3.3"><w n="11.1">Tout</w> <w n="11.2">se</w> <w n="11.3">tait</w> : <w n="11.4">c</w>’<w n="11.5">est</w> <w n="11.6">la</w> <w n="11.7">mer</w> <w n="11.8">astrale</w> <w n="11.9">qui</w> <w n="11.10">s</w>’<w n="11.11">élance</w></l>
							<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">gronde</w> <w n="12.3">immensément</w> <w n="12.4">dans</w> <w n="12.5">l</w>’<w n="12.6">abîme</w> <w n="12.7">des</w> <w n="12.8">nuits</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Viens</w> ! <w n="13.2">Monte</w> ! <w n="13.3">Enlève</w>-<w n="13.4">toi</w> <w n="13.5">sur</w> <w n="13.6">la</w> <w n="13.7">crête</w> <w n="13.8">des</w> <w n="13.9">vagues</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Pareil</w> <w n="14.2">aux</w> <w n="14.3">alcyons</w> <w n="14.4">emportés</w> <w n="14.5">dans</w> <w n="14.6">leurs</w> <w n="14.7">nids</w> !</l>
							<l n="15" num="4.3">— <w n="15.1">Oh</w> ! <w n="15.2">là</w>-<w n="15.3">haut</w>, <w n="15.4">oh</w> ! <w n="15.5">là</w>-<w n="15.6">bas</w>, <w n="15.7">indéfiniment</w> <w n="15.8">vagues</w>.</l>
							<l n="16" num="4.4"><w n="16.1">Je</w> <w n="16.2">vois</w> <w n="16.3">des</w> <w n="16.4">golfes</w> <w n="16.5">d</w>’<w n="16.6">or</w> <w n="16.7">sourdre</w> <w n="16.8">des</w> <w n="16.9">infinis</w>…</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">— <w n="17.1">Viens</w> ! — <w n="17.2">J</w>’<w n="17.3">ai</w> <w n="17.4">peur</w> ! — <w n="17.5">Cette</w> <w n="17.6">lame</w>, <w n="17.7">ô</w> <w n="17.8">nain</w>, <w n="17.9">c</w>’<w n="17.10">est</w> <w n="17.11">la</w> <w n="17.12">première</w>,</l>
							<l n="18" num="5.2"><w n="18.1">La</w> <w n="18.2">seule</w> <w n="18.3">dont</w> <w n="18.4">notre</w> <w n="18.5">œil</w> <w n="18.6">devine</w> <w n="18.7">les</w> <w n="18.8">remous</w> :</l>
							<l n="19" num="5.3"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">là</w>, <w n="19.4">mais</w> <w n="19.5">c</w>’<w n="19.6">est</w> <w n="19.7">si</w> <w n="19.8">loin</w> <w n="19.9">qu</w>’<w n="19.10">un</w> <w n="19.11">rayon</w> <w n="19.12">de</w> <w n="19.13">lumière</w></l>
							<l n="20" num="5.4"><w n="20.1">Court</w> <w n="20.2">des</w> <w n="20.3">siècles</w>, <w n="20.4">avant</w> <w n="20.5">d</w>’<w n="20.6">arriver</w> <w n="20.7">jusqu</w>’<w n="20.8">à</w> <w n="20.9">nous</w> !</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">— <w n="21.1">Loin</w> ! <w n="21.2">Dans</w> <w n="21.3">la</w> <w n="21.4">perspective</w> <w n="21.5">innombrable</w> <w n="21.6">et</w> <w n="21.7">confuse</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Un</w> <w n="22.2">tranquille</w> <w n="22.3">brouillard</w> <w n="22.4">commence</w> <w n="22.5">à</w> <w n="22.6">s</w>’<w n="22.7">azurer</w>…</l>
							<l n="23" num="6.3">— <w n="23.1">Passe</w> : <w n="23.2">il</w> <w n="23.3">est</w> <w n="23.4">si</w> <w n="23.5">profond</w> <w n="23.6">que</w> <w n="23.7">l</w>’<w n="23.8">Éternité</w> <w n="23.9">s</w>’<w n="23.10">use</w></l>
							<l n="24" num="6.4"><w n="24.1">À</w> <w n="24.2">brûler</w> <w n="24.3">des</w> <w n="24.4">soleils</w> <w n="24.5">sans</w> <w n="24.6">pouvoir</w> <w n="24.7">l</w>’<w n="24.8">éclairer</w> !</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">— <w n="25.1">Des</w> <w n="25.2">mers</w> ! <w n="25.3">Des</w> <w n="25.4">mers</w> ! — <w n="25.5">Ce</w> <w n="25.6">n</w>’<w n="25.7">est</w> <w n="25.8">qu</w>’<w n="25.9">un</w> <w n="25.10">reflux</w> <w n="25.11">de</w> <w n="25.12">comètes</w> :</l>
							<l n="26" num="7.2"><w n="26.1">Passe</w>. — <w n="26.2">Nul</w> <w n="26.3">horizon</w> ! <w n="26.4">La</w> <w n="26.5">mer</w> <w n="26.6">moutonne</w> <w n="26.7">encor</w>…</l>
							<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">des</w> <w n="27.3">mers</w> ! <w n="27.4">Et</w> <w n="27.5">partout</w>, <w n="27.6">sous</w> <w n="27.7">nos</w> <w n="27.8">pieds</w>, <w n="27.9">sur</w> <w n="27.10">nos</w> <w n="27.11">têtes</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Tournoie</w> <w n="28.2">et</w> <w n="28.3">s</w>’<w n="28.4">élargit</w> <w n="28.5">le</w> <w n="28.6">monstrueux</w> <w n="28.7">décor</w> !</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">— <w n="29.1">Ce</w> <w n="29.2">n</w>’<w n="29.3">est</w> <w n="29.4">qu</w>’<w n="29.5">un</w> <w n="29.6">point</w> <w n="29.7">du</w> <w n="29.8">ciel</w> <w n="29.9">et</w> <w n="29.10">qu</w>’<w n="29.11">un</w> <w n="29.12">chiffre</w> <w n="29.13">du</w> <w n="29.14">nombre</w> !</l>
							<l n="30" num="8.2"><w n="30.1">Marche</w>, <w n="30.2">tu</w> <w n="30.3">n</w>’<w n="30.4">as</w> <w n="30.5">rien</w> <w n="30.6">vu</w>, <w n="30.7">marche</w>. — <w n="30.8">Je</w> <w n="30.9">suis</w> <w n="30.10">fourbu</w> :</l>
							<l n="31" num="8.3"><w n="31.1">Quand</w> <w n="31.2">donc</w> <w n="31.3">trouverons</w>-<w n="31.4">nous</w> <w n="31.5">la</w> <w n="31.6">fin</w>, <w n="31.7">les</w> <w n="31.8">murs</w> <w n="31.9">de</w> <w n="31.10">l</w>’<w n="31.11">ombre</w> ?</l>
							<l n="32" num="8.4">— <w n="32.1">Cours</w> <w n="32.2">mille</w> <w n="32.3">fois</w> <w n="32.4">mille</w> <w n="32.5">ans</w> <w n="32.6">et</w> <w n="32.7">tu</w> <w n="32.8">n</w>’<w n="32.9">auras</w> <w n="32.10">rien</w> <w n="32.11">vu</w> !</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">Va</w> <w n="33.2">l</w>’<w n="33.3">infini</w> <w n="33.4">de</w> <w n="33.5">temps</w> <w n="33.6">dans</w> <w n="33.7">l</w>’<w n="33.8">infini</w> <w n="33.9">d</w>’<w n="33.10">espace</w> !</l>
							<l n="34" num="9.2"><w n="34.1">Toujours</w> <w n="34.2">le</w> <w n="34.3">feu</w> <w n="34.4">des</w> <w n="34.5">feux</w> <w n="34.6">gronde</w> <w n="34.7">et</w> <w n="34.8">rugit</w> <w n="34.9">sur</w> <w n="34.10">toi</w> :</l>
							<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">tout</w> <w n="35.3">s</w>’<w n="35.4">entraîne</w>, <w n="35.5">fuit</w>, <w n="35.6">disparaît</w>, <w n="35.7">vient</w>, <w n="35.8">repasse</w></l>
							<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">roule</w> <w n="36.3">éperdument</w> <w n="36.4">dans</w> <w n="36.5">les</w> <w n="36.6">vents</w> <w n="36.7">de</w> <w n="36.8">la</w> <w n="36.9">Loi</w> !</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">tout</w> <w n="37.3">peuplé</w>, <w n="37.4">vivant</w>, <w n="37.5">pensant</w>, <w n="37.6">créant</w> <w n="37.7">des</w> <w n="37.8">rêves</w>,</l>
							<l n="38" num="10.2"><w n="38.1">Lançant</w> <w n="38.2">des</w> <w n="38.3">cris</w> <w n="38.4">d</w>’<w n="38.5">espoir</w> <w n="38.6">et</w> <w n="38.7">des</w> <w n="38.8">soupirs</w> <w n="38.9">de</w> <w n="38.10">deuil</w>,</l>
							<l n="39" num="10.3"><w n="39.1">Aimant</w>, <w n="39.2">souffrant</w>, <w n="39.3">croyant</w>, <w n="39.4">et</w> <w n="39.5">sans</w> <w n="39.6">buts</w>, <w n="39.7">et</w> <w n="39.8">sans</w> <w n="39.9">trêves</w>,</l>
							<l n="40" num="10.4"><w n="40.1">Sans</w> <w n="40.2">rien</w> <w n="40.3">savoir</w>, <w n="40.4">sans</w> <w n="40.5">rien</w> <w n="40.6">voir</w>… — <w n="40.7">Et</w> <w n="40.8">j</w>’<w n="40.9">ai</w> <w n="40.10">de</w> <w n="40.11">l</w>’<w n="40.12">orgueil</w> !</l>
						</lg>
					</div></body></text></TEI>