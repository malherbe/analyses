<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR117">
						<head type="main">ÆEA</head>
						<opener>
							<salute>À GEORGES CLAIRIN</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Oh</w> ! <w n="1.2">loin</w> <w n="1.3">du</w> <w n="1.4">monde</w> <w n="1.5">et</w> <w n="1.6">loin</w> <w n="1.7">du</w> <w n="1.8">bruit</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Sans</w> <w n="2.2">voir</w> <w n="2.3">le</w> <w n="2.4">jour</w>, <w n="2.5">sans</w> <w n="2.6">voir</w> <w n="2.7">la</w> <w n="2.8">nuit</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Vivre</w> <w n="3.2">sous</w> <w n="3.3">la</w> <w n="3.4">lueur</w> <w n="3.5">des</w> <w n="3.6">lampes</w> !</l>
							<l n="4" num="1.4"><w n="4.1">Marcher</w> <w n="4.2">à</w> <w n="4.3">pieds</w> <w n="4.4">nus</w> <w n="4.5">dans</w> <w n="4.6">les</w> <w n="4.7">peaux</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Suspendre</w> <w n="5.2">en</w> <w n="5.3">guise</w> <w n="5.4">de</w> <w n="5.5">drapeaux</w></l>
							<l n="6" num="1.6"><w n="6.1">Des</w> <w n="6.2">chevelures</w> <w n="6.3">à</w> <w n="6.4">des</w> <w n="6.5">hampes</w> !</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><w n="7.1">Mon</w> <w n="7.2">palais</w> <w n="7.3">est</w> <w n="7.4">de</w> <w n="7.5">marbre</w> <w n="7.6">gris</w> :</l>
							<l n="8" num="2.2"><w n="8.1">Dans</w> <w n="8.2">l</w>’<w n="8.3">or</w> <w n="8.4">ciselé</w> <w n="8.5">des</w> <w n="8.6">lambris</w></l>
							<l n="9" num="2.3"><w n="9.1">Glissent</w> <w n="9.2">des</w> <w n="9.3">serpents</w> <w n="9.4">d</w>’<w n="9.5">émeraude</w> ;</l>
							<l n="10" num="2.4"><w n="10.1">Le</w> <w n="10.2">parquet</w> <w n="10.3">lisse</w> <w n="10.4">est</w> <w n="10.5">de</w> <w n="10.6">santal</w> ;</l>
							<l n="11" num="2.5"><w n="11.1">Et</w> <w n="11.2">vers</w> <w n="11.3">les</w> <w n="11.4">plafonds</w> <w n="11.5">de</w> <w n="11.6">cristal</w></l>
							<l n="12" num="2.6"><w n="12.1">L</w>’<w n="12.2">encens</w> <w n="12.3">monte</w>, <w n="12.4">se</w> <w n="12.5">trouble</w>, <w n="12.6">et</w> <w n="12.7">rôde</w>…</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">Les</w> <w n="13.2">piliers</w> <w n="13.3">vêtus</w> <w n="13.4">de</w> <w n="13.5">brocart</w></l>
							<l n="14" num="3.2"><w n="14.1">Se</w> <w n="14.2">dressent</w>, <w n="14.3">drapés</w> <w n="14.4">avec</w> <w n="14.5">art</w>,</l>
							<l n="15" num="3.3"><w n="15.1">Comme</w> <w n="15.2">des</w> <w n="15.3">Grecs</w> <w n="15.4">dans</w> <w n="15.5">leurs</w> <w n="15.6">chlamydes</w> ;</l>
							<l n="16" num="3.4"><w n="16.1">Des</w> <w n="16.2">cataractes</w> <w n="16.3">de</w> <w n="16.4">velours</w></l>
							<l n="17" num="3.5"><w n="17.1">Tombent</w> <w n="17.2">des</w> <w n="17.3">frises</w> <w n="17.4">en</w> <w n="17.5">plis</w> <w n="17.6">lourds</w>,</l>
							<l n="18" num="3.6"><w n="18.1">Plus</w> <w n="18.2">lourds</w> <w n="18.3">que</w> <w n="18.4">des</w> <w n="18.5">toiles</w> <w n="18.6">humides</w>.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1"><w n="19.1">L</w>’<w n="19.2">air</w> <w n="19.3">rose</w>, <w n="19.4">en</w> <w n="19.5">courants</w> <w n="19.6">attiédis</w>,</l>
							<l n="20" num="4.2"><w n="20.1">Berce</w> <w n="20.2">des</w> <w n="20.3">fleurs</w> <w n="20.4">de</w> <w n="20.5">paradis</w></l>
							<l n="21" num="4.3"><w n="21.1">Qui</w> <w n="21.2">coquettent</w> <w n="21.3">du</w> <w n="21.4">bout</w> <w n="21.5">des</w> <w n="21.6">tiges</w>,</l>
							<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">charrie</w> <w n="22.3">un</w> <w n="22.4">flot</w> <w n="22.5">de</w> <w n="22.6">poison</w></l>
							<l n="23" num="4.5"><w n="23.1">Qui</w> <w n="23.2">fait</w> <w n="23.3">tournoyer</w> <w n="23.4">ma</w> <w n="23.5">raison</w></l>
							<l n="24" num="4.6"><w n="24.1">Au</w> <w n="24.2">bord</w> <w n="24.3">du</w> <w n="24.4">gouffre</w> <w n="24.5">des</w> <w n="24.6">vertiges</w>.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1"><w n="25.1">Je</w> <w n="25.2">ne</w> <w n="25.3">sais</w> <w n="25.4">rien</w>, <w n="25.5">je</w> <w n="25.6">ne</w> <w n="25.7">veux</w> <w n="25.8">rien</w> ;</l>
							<l n="26" num="5.2"><w n="26.1">J</w>’<w n="26.2">ai</w> <w n="26.3">tout</w> <w n="26.4">perdu</w>, <w n="26.5">l</w>’<w n="26.6">amour</w> <w n="26.7">du</w> <w n="26.8">bien</w>,</l>
							<l n="27" num="5.3"><w n="27.1">Le</w> <w n="27.2">sens</w> <w n="27.3">des</w> <w n="27.4">mots</w>, <w n="27.5">l</w>’<w n="27.6">orgueil</w> <w n="27.7">des</w> <w n="27.8">phrases</w> ;</l>
							<l n="28" num="5.4"><w n="28.1">J</w>’<w n="28.2">ignore</w> <w n="28.3">la</w> <w n="28.4">terre</w> <w n="28.5">et</w> <w n="28.6">le</w> <w n="28.7">ciel</w></l>
							<l n="29" num="5.5"><w n="29.1">Et</w> <w n="29.2">mon</w> <w n="29.3">rêve</w> <w n="29.4">perpétuel</w></l>
							<l n="30" num="5.6"><w n="30.1">Compte</w> <w n="30.2">ses</w> <w n="30.3">jours</w> <w n="30.4">par</w> <w n="30.5">ses</w> <w n="30.6">extases</w>.</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1"><w n="31.1">Moins</w> <w n="31.2">qu</w>’<w n="31.3">une</w> <w n="31.4">brute</w> <w n="31.5">et</w> <w n="31.6">presque</w> <w n="31.7">un</w> <w n="31.8">dieu</w>,</l>
							<l n="32" num="6.2"><w n="32.1">Je</w> <w n="32.2">m</w>’<w n="32.3">épuise</w> <w n="32.4">et</w> <w n="32.5">nage</w> <w n="32.6">au</w> <w n="32.7">milieu</w></l>
							<l n="33" num="6.3"><w n="33.1">Des</w> <w n="33.2">réconforts</w> <w n="33.3">et</w> <w n="33.4">des</w> <w n="33.5">dictames</w> ;</l>
							<l n="34" num="6.4"><w n="34.1">Puis</w>, <w n="34.2">dans</w> <w n="34.3">des</w> <w n="34.4">bras</w> <w n="34.5">entrecroisés</w>,</l>
							<l n="35" num="6.5"><w n="35.1">Frôlé</w> <w n="35.2">par</w> <w n="35.3">un</w> <w n="35.4">vol</w> <w n="35.5">de</w> <w n="35.6">baisers</w>,</l>
							<l n="36" num="6.6"><w n="36.1">Je</w> <w n="36.2">m</w>’<w n="36.3">endors</w> <w n="36.4">sur</w> <w n="36.5">des</w> <w n="36.6">corps</w> <w n="36.7">de</w> <w n="36.8">femmes</w> !</l>
						</lg>
					</div></body></text></TEI>