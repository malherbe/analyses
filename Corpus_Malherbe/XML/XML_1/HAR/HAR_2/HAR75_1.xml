<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR75">
						<head type="main">LES VERGES</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">enfant</w> <w n="1.3">qui</w> <w n="1.4">ne</w> <w n="1.5">sait</w> <w n="1.6">rien</w> <w n="1.7">sait</w> <w n="1.8">le</w> <w n="1.9">vrai</w> <w n="1.10">mieux</w> <w n="1.11">que</w> <w n="1.12">nous</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">dans</w> <w n="2.3">l</w>’<w n="2.4">étonnement</w> <w n="2.5">craintif</w> <w n="2.6">de</w> <w n="2.7">ses</w> <w n="2.8">yeux</w> <w n="2.9">doux</w>,</l>
							<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">voit</w> <w n="3.3">trop</w> <w n="3.4">qu</w>’<w n="3.5">il</w> <w n="3.6">comprend</w> <w n="3.7">déjà</w>, <w n="3.8">comme</w> <w n="3.9">les</w> <w n="3.10">bêtes</w>.</l>
							<l n="4" num="1.4"><w n="4.1">Combien</w> <w n="4.2">nos</w> <w n="4.3">cœurs</w> <w n="4.4">sont</w> <w n="4.5">durs</w> <w n="4.6">et</w> <w n="4.7">nos</w> <w n="4.8">lois</w> <w n="4.9">déshonnêtes</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">pense</w>, <w n="5.3">il</w> <w n="5.4">cherche</w>, <w n="5.5">il</w> <w n="5.6">erre</w>… <w n="5.7">Allons</w>, <w n="5.8">verges</w> <w n="5.9">en</w> <w n="5.10">main</w> !</l>
							<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">tape</w> <w n="6.3">sur</w> <w n="6.4">ton</w> <w n="6.5">fils</w> : <w n="6.6">il</w> <w n="6.7">comprendra</w> <w n="6.8">demain</w>.</l>
							<l n="7" num="1.7"><w n="7.1">Va</w> ! <w n="7.2">Ta</w> <w n="7.3">force</w> <w n="7.4">est</w> <w n="7.5">un</w> <w n="7.6">droit</w>, <w n="7.7">et</w> <w n="7.8">ta</w> <w n="7.9">rage</w> <w n="7.10">est</w> <w n="7.11">auguste</w>.</l>
							<l n="8" num="1.8"><w n="8.1">L</w>’<w n="8.2">enfant</w> <w n="8.3">vaut</w> <w n="8.4">mieux</w> <w n="8.5">que</w> <w n="8.6">toi</w> ? <w n="8.7">N</w>’<w n="8.8">importe</w> ! <w n="8.9">Au</w> <w n="8.10">nom</w> <w n="8.11">du</w> <w n="8.12">juste</w> !</l>
							<l n="9" num="1.9"><w n="9.1">N</w>’<w n="9.2">es</w>-<w n="9.3">tu</w> <w n="9.4">pas</w> <w n="9.5">le</w> <w n="9.6">devoir</w>, <w n="9.7">la</w> <w n="9.8">règle</w>, <w n="9.9">la</w> <w n="9.10">vertu</w>,</l>
							<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">ton</w> <w n="10.3">bras</w> <w n="10.4">n</w>’<w n="10.5">est</w>-<w n="10.6">il</w> <w n="10.7">pas</w> <w n="10.8">le</w> <w n="10.9">plus</w> <w n="10.10">fort</w> ? <w n="10.11">Que</w> <w n="10.12">crains</w>-<w n="10.13">tu</w> ?</l>
							<l n="11" num="1.11"><w n="11.1">S</w>’<w n="11.2">il</w> <w n="11.3">défendait</w> <w n="11.4">sa</w> <w n="11.5">chair</w>, <w n="11.6">ce</w> <w n="11.7">torturé</w> <w n="11.8">qui</w> <w n="11.9">t</w>’<w n="11.10">aime</w>,</l>
							<l n="12" num="1.12"><w n="12.1">Les</w> <w n="12.2">passants</w> <w n="12.3">indignés</w> <w n="12.4">crieraient</w> <w n="12.5">à</w> <w n="12.6">l</w>’<w n="12.7">anathème</w>.</l>
							<l n="13" num="1.13"><w n="13.1">Sus</w> ! <w n="13.2">Frappe</w> <w n="13.3">et</w> <w n="13.4">frappe</w>, <w n="13.5">afin</w> <w n="13.6">qu</w>’<w n="13.7">il</w> <w n="13.8">apprenne</w> <w n="13.9">par</w> <w n="13.10">toi</w></l>
							<l n="14" num="1.14"><w n="14.1">La</w> <w n="14.2">honte</w> <w n="14.3">d</w>’<w n="14.4">être</w> <w n="14.5">un</w> <w n="14.6">homme</w> <w n="14.7">et</w> <w n="14.8">l</w>’<w n="14.9">horreur</w> <w n="14.10">de</w> <w n="14.11">la</w> <w n="14.12">loi</w> !</l>
						</lg>
					</div></body></text></TEI>