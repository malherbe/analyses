<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR124">
						<head type="main">ADULTÈRE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">t</w>’<w n="1.3">apprendrai</w> <w n="1.4">l</w>’<w n="1.5">amour</w> <w n="1.6">stérile</w>, <w n="1.7">et</w> <w n="1.8">le</w> <w n="1.9">secret</w></l>
							<l n="2" num="1.2"><w n="2.1">Des</w> <w n="2.2">bonheurs</w> <w n="2.3">trop</w> <w n="2.4">savants</w> <w n="2.5">qu</w>’<w n="2.6">ignore</w> <w n="2.7">l</w>’<w n="2.8">hyménée</w> :</l>
							<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">veux</w> <w n="3.3">t</w>’<w n="3.4">ouvrir</w> <w n="3.5">un</w> <w n="3.6">monde</w> <w n="3.7">où</w> <w n="3.8">nul</w> <w n="3.9">ne</w> <w n="3.10">t</w>’<w n="3.11">a</w> <w n="3.12">menée</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Si</w> <w n="4.2">beau</w> <w n="4.3">qu</w>’<w n="4.4">on</w> <w n="4.5">n</w>’<w n="4.6">en</w> <w n="4.7">revient</w> <w n="4.8">qu</w>’<w n="4.9">en</w> <w n="4.10">pleurant</w> <w n="4.11">de</w> <w n="4.12">regret</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Oh</w> ! <w n="5.2">l</w>’<w n="5.3">art</w> <w n="5.4">du</w> <w n="5.5">long</w> <w n="5.6">baiser</w> <w n="5.7">qui</w> <w n="5.8">court</w>, <w n="5.9">profond</w>, <w n="5.10">discret</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Sur</w> <w n="6.2">le</w> <w n="6.3">ravissement</w> <w n="6.4">de</w> <w n="6.5">la</w> <w n="6.6">chair</w> <w n="6.7">étonnée</w> !</l>
							<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">art</w> <w n="7.3">que</w> <w n="7.4">ne</w> <w n="7.5">savaient</w> <w n="7.6">point</w> <w n="7.7">ceux</w> <w n="7.8">qui</w> <w n="7.9">t</w>’<w n="7.10">ont</w> <w n="7.11">profanée</w></l>
							<l n="8" num="2.4"><w n="8.1">Sur</w> <w n="8.2">la</w> <w n="8.3">couche</w> <w n="8.4">brutale</w> <w n="8.5">où</w> <w n="8.6">ton</w> <w n="8.7">cœur</w> <w n="8.8">s</w>’<w n="8.9">enivrait</w>…</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Viens</w> ! <w n="9.2">Ce</w> <w n="9.3">que</w> <w n="9.4">tu</w> <w n="9.5">rêvas</w> <w n="9.6">sans</w> <w n="9.7">le</w> <w n="9.8">pouvoir</w> <w n="9.9">connaître</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Je</w> <w n="10.2">te</w> <w n="10.3">le</w> <w n="10.4">donnerai</w> ! <w n="10.5">Tu</w> <w n="10.6">te</w> <w n="10.7">sentiras</w> <w n="10.8">naître</w> ;</l>
							<l n="11" num="3.3"><w n="11.1">Tes</w> <w n="11.2">grands</w> <w n="11.3">yeux</w> <w n="11.4">dessillés</w> <w n="11.5">verront</w> <w n="11.6">dans</w> <w n="11.7">l</w>’<w n="11.8">infini</w> :</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">tous</w> <w n="12.3">deux</w>, <w n="12.4">emportés</w> <w n="12.5">sur</w> <w n="12.6">un</w> <w n="12.7">rêve</w> <w n="12.8">sublime</w>,</l>
							<l n="13" num="4.2"><w n="13.1">Nous</w> <w n="13.2">aurons</w>, <w n="13.3">pour</w> <w n="13.4">bénir</w> <w n="13.5">encor</w> <w n="13.6">l</w>’<w n="13.7">amour</w> <w n="13.8">béni</w>,</l>
							<l n="14" num="4.3"><w n="14.1">L</w>’<w n="14.2">immense</w> <w n="14.3">volupté</w> <w n="14.4">qu</w>’<w n="14.5">on</w> <w n="14.6">appelle</w> <w n="14.7">le</w> <w n="14.8">crime</w> !</l>
						</lg>
					</div></body></text></TEI>