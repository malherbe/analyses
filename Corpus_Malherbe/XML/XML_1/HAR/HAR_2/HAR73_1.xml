<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR73">
						<head type="main">L’ÎLE VIERGE</head>
						<opener>
							<salute>À FRANCIS PITTIÉ</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">île</w>, <w n="1.3">en</w> <w n="1.4">son</w> <w n="1.5">nonchaloir</w> <w n="1.6">de</w> <w n="1.7">courtisane</w> <w n="1.8">hellène</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Dort</w> <w n="2.2">sur</w> <w n="2.3">son</w> <w n="2.4">lit</w> <w n="2.5">d</w>’<w n="2.6">azur</w> <w n="2.7">où</w> <w n="2.8">la</w> <w n="2.9">mer</w> <w n="2.10">a</w> <w n="2.11">mêlé</w></l>
							<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">éclats</w> <w n="3.3">de</w> <w n="3.4">satin</w> <w n="3.5">et</w> <w n="3.6">des</w> <w n="3.7">blancheurs</w> <w n="3.8">de</w> <w n="3.9">laine</w>.</l>
						</lg>
						<lg n="2">
							<l n="4" num="2.1"><w n="4.1">Son</w> <w n="4.2">lit</w> <w n="4.3">chante</w> <w n="4.4">et</w> <w n="4.5">l</w>’<w n="4.6">endort</w> <w n="4.7">dans</w> <w n="4.8">un</w> <w n="4.9">baiser</w> <w n="4.10">salé</w> ;</l>
							<l n="5" num="2.2"><w n="5.1">L</w>’<w n="5.2">arome</w> <w n="5.3">chaud</w> <w n="5.4">des</w> <w n="5.5">thyms</w> <w n="5.6">tremble</w> <w n="5.7">et</w> <w n="5.8">rôde</w> <w n="5.9">autour</w> <w n="5.10">d</w>’<w n="5.11">elle</w>,</l>
							<l n="6" num="2.3"><w n="6.1">Comme</w> <w n="6.2">le</w> <w n="6.3">filet</w> <w n="6.4">bleu</w> <w n="6.5">qui</w> <w n="6.6">sort</w> <w n="6.7">d</w>’<w n="6.8">un</w> <w n="6.9">narguilé</w>.</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1"><w n="7.1">Le</w> <w n="7.2">myrte</w>, <w n="7.3">le</w> <w n="7.4">jasmin</w>, <w n="7.5">la</w> <w n="7.6">rose</w> <w n="7.7">et</w> <w n="7.8">l</w>’<w n="7.9">asphodèle</w>,</l>
							<l n="8" num="3.2"><w n="8.1">Balançant</w> <w n="8.2">sous</w> <w n="8.3">l</w>’<w n="8.4">éther</w> <w n="8.5">leurs</w> <w n="8.6">frêles</w> <w n="8.7">encensoirs</w>,</l>
							<l n="9" num="3.3"><w n="9.1">Font</w> <w n="9.2">monter</w> <w n="9.3">des</w> <w n="9.4">parfums</w> <w n="9.5">vers</w> <w n="9.6">le</w> <w n="9.7">soleil</w> <w n="9.8">fidèle</w>.</l>
						</lg>
						<lg n="4">
							<l n="10" num="4.1"><w n="10.1">Et</w> <w n="10.2">librement</w>, <w n="10.3">dans</w> <w n="10.4">l</w>’<w n="10.5">air</w> <w n="10.6">des</w> <w n="10.7">matins</w> <w n="10.8">ou</w> <w n="10.9">des</w> <w n="10.10">soirs</w>,</l>
							<l n="11" num="4.2"><w n="11.1">Partout</w>, <w n="11.2">dans</w> <w n="11.3">tous</w> <w n="11.4">les</w> <w n="11.5">coins</w>, <w n="11.6">hasard</w>, <w n="11.7">où</w> <w n="11.8">tu</w> <w n="11.9">les</w> <w n="11.10">jettes</w>,</l>
							<l n="12" num="4.3"><w n="12.1">Les</w> <w n="12.2">fleurs</w> <w n="12.3">avec</w> <w n="12.4">les</w> <w n="12.5">fleurs</w> <w n="12.6">dressent</w> <w n="12.7">des</w> <w n="12.8">reposoirs</w>.</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1"><w n="13.1">Les</w> <w n="13.2">genêts</w>, <w n="13.3">sur</w> <w n="13.4">les</w> <w n="13.5">rocs</w>, <w n="13.6">agitent</w> <w n="13.7">leurs</w> <w n="13.8">vergettes</w> ;</l>
							<l n="14" num="5.2"><w n="14.1">Les</w> <w n="14.2">aloès</w>, <w n="14.3">autour</w> <w n="14.4">des</w> <w n="14.5">pins</w> <w n="14.6">aux</w> <w n="14.7">lourds</w> <w n="14.8">cimiers</w>,</l>
							<l n="15" num="5.3"><w n="15.1">Hérissent</w> <w n="15.2">leurs</w> <w n="15.3">poignards</w> <w n="15.4">et</w> <w n="15.5">dardent</w> <w n="15.6">leurs</w> <w n="15.7">sagettes</w>.</l>
						</lg>
						<lg n="6">
							<l n="16" num="6.1"><w n="16.1">Et</w> <w n="16.2">les</w> <w n="16.3">bois</w> <w n="16.4">d</w>’<w n="16.5">orangers</w> <w n="16.6">près</w> <w n="16.7">des</w> <w n="16.8">bois</w> <w n="16.9">de</w> <w n="16.10">palmiers</w>,</l>
							<l n="17" num="6.2"><w n="17.1">Pointillés</w> <w n="17.2">des</w> <w n="17.3">fruits</w> <w n="17.4">d</w>’<w n="17.5">or</w> <w n="17.6">dont</w> <w n="17.7">l</w>’<w n="17.8">hiver</w> <w n="17.9">les</w> <w n="17.10">parsème</w>,</l>
							<l n="18" num="6.3"><w n="18.1">Cachent</w> <w n="18.2">des</w> <w n="18.3">nids</w> <w n="18.4">où</w> <w n="18.5">vont</w> <w n="18.6">roucouler</w> <w n="18.7">les</w> <w n="18.8">ramiers</w>.</l>
						</lg>
						<lg n="7">
							<l n="19" num="7.1"><w n="19.1">Chaque</w> <w n="19.2">mois</w> <w n="19.3">est</w> <w n="19.4">le</w> <w n="19.5">mois</w> <w n="19.6">des</w> <w n="19.7">lis</w>. <w n="19.8">La</w> <w n="19.9">ruche</w> <w n="19.10">essaime</w>,</l>
							<l n="20" num="7.2"><w n="20.1">Et</w> <w n="20.2">le</w> <w n="20.3">miel</w> <w n="20.4">blond</w> <w n="20.5">déborde</w> <w n="20.6">à</w> <w n="20.7">chaque</w> <w n="20.8">lunaison</w> ;</l>
							<l n="21" num="7.3"><w n="21.1">Tout</w> <w n="21.2">rit</w>, <w n="21.3">tout</w> <w n="21.4">est</w> <w n="21.5">joyeux</w>, <w n="21.6">tout</w> <w n="21.7">est</w> <w n="21.8">pur</w> <w n="21.9">et</w> <w n="21.10">tout</w> <w n="21.11">s</w>’<w n="21.12">aime</w>…</l>
						</lg>
						<lg n="8">
							<l n="22" num="8.1">— <w n="22.1">L</w>’<w n="22.2">homme</w> <w n="22.3">étant</w> <w n="22.4">venu</w> <w n="22.5">là</w> <w n="22.6">bâtit</w> <w n="22.7">une</w> <w n="22.8">prison</w>.</l>
						</lg>
					</div></body></text></TEI>