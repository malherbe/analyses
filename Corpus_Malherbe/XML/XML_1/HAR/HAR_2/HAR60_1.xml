<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR60">
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">peu</w> <w n="1.3">de</w> <w n="1.4">foi</w> <w n="1.5">que</w> <w n="1.6">j</w>’<w n="1.7">ai</w>, <w n="1.8">ma</w> <w n="1.9">raison</w> <w n="1.10">me</w> <w n="1.11">l</w>’<w n="1.12">enlève</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Tout</w> <w n="2.2">ce</w> <w n="2.3">que</w> <w n="2.4">j</w>’<w n="2.5">ai</w> <w n="2.6">de</w> <w n="2.7">beau</w>, <w n="2.8">ma</w> <w n="2.9">raison</w> <w n="2.10">me</w> <w n="2.11">le</w> <w n="2.12">prend</w>…</l>
							<l n="3" num="1.3"><w n="3.1">Oh</w> ! <w n="3.2">sois</w> <w n="3.3">fou</w> <w n="3.4">si</w> <w n="3.5">tu</w> <w n="3.6">peux</w>, <w n="3.7">pauvre</w> <w n="3.8">être</w>, <w n="3.9">atome</w> <w n="3.10">errant</w> !</l>
							<l n="4" num="1.4"><w n="4.1">Tous</w> <w n="4.2">nos</w> <w n="4.3">paradis</w> <w n="4.4">morts</w>, <w n="4.5">l</w>’<w n="4.6">extase</w> <w n="4.7">nous</w> <w n="4.8">les</w> <w n="4.9">rend</w> :</l>
							<l n="5" num="1.5"><w n="5.1">Rêve</w> <w n="5.2">et</w> <w n="5.3">monte</w>, <w n="5.4">plus</w> <w n="5.5">haut</w> <w n="5.6">toujours</w>, <w n="5.7">plus</w> <w n="5.8">haut</w> <w n="5.9">sans</w> <w n="5.10">trêve</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">tu</w> <w n="6.3">reconnaîtras</w> <w n="6.4">que</w> <w n="6.5">ton</w> <w n="6.6">rêve</w> <w n="6.7">était</w> <w n="6.8">grand</w></l>
							<l n="7" num="1.7"><w n="7.1">Si</w> <w n="7.2">tu</w> <w n="7.3">te</w> <w n="7.4">sens</w> <w n="7.5">petit</w> <w n="7.6">au</w> <w n="7.7">sortir</w> <w n="7.8">de</w> <w n="7.9">ton</w> <w n="7.10">rêve</w> !</l>
						</lg>
					</div></body></text></TEI>