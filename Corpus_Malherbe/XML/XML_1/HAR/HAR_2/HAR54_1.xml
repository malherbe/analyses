<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR54">
						<head type="main">FUIR !</head>
						<opener>
							<salute>À LÉON CLADEL</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Assez</w> <w n="1.2">du</w> <w n="1.3">monde</w> ! <w n="1.4">Assez</w> <w n="1.5">de</w> <w n="1.6">nous</w> ! <w n="1.7">Assez</w> <w n="1.8">de</w> <w n="1.9">moi</w> !</l>
							<l n="2" num="1.2"><w n="2.1">Race</w> <w n="2.2">de</w> <w n="2.3">l</w>’<w n="2.4">impuissance</w> <w n="2.5">orgueilleuse</w> <w n="2.6">et</w> <w n="2.7">guindée</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Haine</w> <w n="3.2">à</w> <w n="3.3">ton</w> <w n="3.4">œuvre</w>, <w n="3.5">à</w> <w n="3.6">tes</w> <w n="3.7">justices</w>, <w n="3.8">à</w> <w n="3.9">ta</w> <w n="3.10">loi</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Haine</w> <w n="4.2">à</w> <w n="4.3">la</w> <w n="4.4">vanité</w> <w n="4.5">stupide</w> <w n="4.6">de</w> <w n="4.7">l</w>’<w n="4.8">idée</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Assez</w> <w n="5.2">du</w> <w n="5.3">moule</w> <w n="5.4">antique</w> <w n="5.5">où</w> <w n="5.6">l</w>’<w n="5.7">on</w> <w n="5.8">coule</w> <w n="5.9">les</w> <w n="5.10">dieux</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Assez</w> <w n="6.2">de</w> <w n="6.3">la</w> <w n="6.4">raison</w> <w n="6.5">qui</w> <w n="6.6">change</w> <w n="6.7">au</w> <w n="6.8">vent</w> <w n="6.9">qui</w> <w n="6.10">change</w> !</l>
							<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">plonge</w> <w n="7.3">dans</w> <w n="7.4">le</w> <w n="7.5">pire</w> <w n="7.6">à</w> <w n="7.7">chaque</w> <w n="7.8">espoir</w> <w n="7.9">du</w> <w n="7.10">mieux</w> ;</l>
							<l n="8" num="2.4"><w n="8.1">Chaque</w> <w n="8.2">effort</w> <w n="8.3">vers</w> <w n="8.4">le</w> <w n="8.5">ciel</w> <w n="8.6">m</w>’<w n="8.7">enlize</w> <w n="8.8">dans</w> <w n="8.9">la</w> <w n="8.10">fange</w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Déformer</w> <w n="9.2">la</w> <w n="9.3">nature</w>, <w n="9.4">inventer</w> <w n="9.5">des</w> <w n="9.6">vertus</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Penser</w>, <w n="10.2">chercher</w>, <w n="10.3">vouloir</w>, <w n="10.4">se</w> <w n="10.5">tordre</w> <w n="10.6">dans</w> <w n="10.7">un</w> <w n="10.8">rêve</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Battre</w> <w n="11.2">comme</w> <w n="11.3">des</w> <w n="11.4">flots</w> <w n="11.5">les</w> <w n="11.6">rocs</w> <w n="11.7">déjà</w> <w n="11.8">battus</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">ne</w> <w n="12.3">pas</w> <w n="12.4">déplacer</w> <w n="12.5">un</w> <w n="12.6">sable</w> <w n="12.7">de</w> <w n="12.8">la</w> <w n="12.9">grève</w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">toujours</w> <w n="13.3">des</w> <w n="13.4">essors</w>, <w n="13.5">des</w> <w n="13.6">vœux</w>, <w n="13.7">des</w> <w n="13.8">pleurs</w>, <w n="13.9">des</w> <w n="13.10">cris</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Des</w> <w n="14.2">douleurs</w> <w n="14.3">sans</w> <w n="14.4">motifs</w> <w n="14.5">et</w> <w n="14.6">des</w> <w n="14.7">rages</w> <w n="14.8">d</w>’<w n="14.9">homme</w> <w n="14.10">ivre</w> ;</l>
							<l n="15" num="4.3"><w n="15.1">Toujours</w> <w n="15.2">de</w> <w n="15.3">faux</w> <w n="15.4">espoirs</w> <w n="15.5">qu</w>’<w n="15.6">on</w> <w n="15.7">cloue</w> <w n="15.8">aux</w> <w n="15.9">piloris</w> !</l>
							<l n="16" num="4.4">— <w n="16.1">Je</w> <w n="16.2">suis</w> <w n="16.3">las</w> <w n="16.4">de</w> <w n="16.5">songer</w>, <w n="16.6">moi</w> <w n="16.7">qui</w> <w n="16.8">suis</w> <w n="16.9">né</w> <w n="16.10">pour</w> <w n="16.11">vivre</w> !</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">suis</w> <w n="17.3">las</w>. <w n="17.4">Je</w> <w n="17.5">voudrais</w> <w n="17.6">renaître</w> <w n="17.7">aux</w> <w n="17.8">temps</w> <w n="17.9">anciens</w> !</l>
							<l n="18" num="5.2"><w n="18.1">Où</w> <w n="18.2">sont</w> <w n="18.3">les</w> <w n="18.4">bois</w> <w n="18.5">touffus</w> <w n="18.6">qu</w>’<w n="18.7">on</w> <w n="18.8">peuple</w> <w n="18.9">autour</w> <w n="18.10">des</w> <w n="18.11">sources</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Les</w> <w n="19.2">antres</w> <w n="19.3">de</w> <w n="19.4">granit</w> <w n="19.5">sous</w> <w n="19.6">la</w> <w n="19.7">garde</w> <w n="19.8">des</w> <w n="19.9">chiens</w>,</l>
							<l n="20" num="5.4"><w n="20.1">Les</w> <w n="20.2">enfants</w> <w n="20.3">bruns</w> <w n="20.4">couchés</w> <w n="20.5">dans</w> <w n="20.6">le</w> <w n="20.7">poil</w> <w n="20.8">brun</w> <w n="20.9">des</w> <w n="20.10">ourses</w> ?</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Les</w> <w n="21.2">femmes</w> <w n="21.3">qui</w>, <w n="21.4">prenant</w> <w n="21.5">la</w> <w n="21.6">main</w> <w n="21.7">des</w> <w n="21.8">inconnus</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Graves</w>, <w n="22.2">sans</w> <w n="22.3">impudeur</w> <w n="22.4">ni</w> <w n="22.5">pudeur</w>, <w n="22.6">et</w> <w n="22.7">farouches</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Livrent</w> <w n="23.2">abondamment</w> <w n="23.3">leurs</w> <w n="23.4">flancs</w> <w n="23.5">et</w> <w n="23.6">leurs</w> <w n="23.7">seins</w> <w n="23.8">nus</w></l>
							<l n="24" num="6.4"><w n="24.1">Avec</w> <w n="24.2">de</w> <w n="24.3">grands</w> <w n="24.4">baisers</w> <w n="24.5">qui</w> <w n="24.6">font</w> <w n="24.7">saigner</w> <w n="24.8">les</w> <w n="24.9">bouches</w> ?</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Sur</w> <w n="25.2">quels</w> <w n="25.3">bords</w>, <w n="25.4">les</w> <w n="25.5">pays</w> <w n="25.6">de</w> <w n="25.7">l</w>’<w n="25.8">antique</w> <w n="25.9">bonheur</w></l>
							<l n="26" num="7.2"><w n="26.1">Où</w> <w n="26.2">l</w>’<w n="26.3">homme</w> <w n="26.4">en</w> <w n="26.5">souriant</w> <w n="26.6">suit</w> <w n="26.7">l</w>’<w n="26.8">instinct</w> <w n="26.9">qui</w> <w n="26.10">le</w> <w n="26.11">mène</w>,</l>
							<l n="27" num="7.3"><w n="27.1">Où</w> <w n="27.2">les</w> <w n="27.3">mots</w> <w n="27.4">criminels</w> <w n="27.5">de</w> <w n="27.6">justice</w> <w n="27.7">et</w> <w n="27.8">d</w>’<w n="27.9">honneur</w></l>
							<l n="28" num="7.4"><w n="28.1">N</w>’<w n="28.2">ont</w> <w n="28.3">pas</w> <w n="28.4">souillé</w> <w n="28.5">la</w> <w n="28.6">langue</w> <w n="28.7">et</w> <w n="28.8">meurtri</w> <w n="28.9">l</w>’<w n="28.10">âme</w> <w n="28.11">humaine</w> ?</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Ah</w> ! <w n="29.2">retrouver</w> <w n="29.3">les</w> <w n="29.4">beaux</w> <w n="29.5">Édens</w>, <w n="29.6">les</w> <w n="29.7">Édens</w> <w n="29.8">morts</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Courir</w> <w n="30.2">les</w> <w n="30.3">monts</w> <w n="30.4">avec</w> <w n="30.5">les</w> <w n="30.6">fils</w> <w n="30.7">des</w> <w n="30.8">races</w> <w n="30.9">mortes</w>,</l>
							<l n="31" num="8.3"><w n="31.1">Et</w>, <w n="31.2">nu</w> <w n="31.3">sous</w> <w n="31.4">le</w> <w n="31.5">plein</w> <w n="31.6">ciel</w>, <w n="31.7">n</w>’<w n="31.8">avoir</w> <w n="31.9">froid</w> <w n="31.10">qu</w>’<w n="31.11">à</w> <w n="31.12">son</w> <w n="31.13">corps</w>,</l>
							<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">brute</w>, <w n="32.3">n</w>’<w n="32.4">avoir</w> <w n="32.5">peur</w> <w n="32.6">que</w> <w n="32.7">des</w> <w n="32.8">brutes</w> <w n="32.9">plus</w> <w n="32.10">fortes</w> !</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">Encor</w>, <w n="33.2">toujours</w>, <w n="33.3">les</w> <w n="33.4">loups</w> <w n="33.5">errent</w> <w n="33.6">sous</w> <w n="33.7">les</w> <w n="33.8">forêts</w> ;</l>
							<l n="34" num="9.2"><w n="34.1">Les</w> <w n="34.2">tigres</w> <w n="34.3">accroupis</w> <w n="34.4">dans</w> <w n="34.5">les</w> <w n="34.6">roseaux</w> <w n="34.7">des</w> <w n="34.8">jungles</w></l>
							<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">les</w> <w n="35.3">lions</w> <w n="35.4">couchés</w> <w n="35.5">au</w> <w n="35.6">seuil</w> <w n="35.7">des</w> <w n="35.8">antres</w> <w n="35.9">frais</w></l>
							<l n="36" num="9.4"><w n="36.1">Lèchent</w> <w n="36.2">leur</w> <w n="36.3">large</w> <w n="36.4">patte</w> <w n="36.5">en</w> <w n="36.6">regardant</w> <w n="36.7">leurs</w> <w n="36.8">ongles</w>…</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">Ceux</w> <w n="37.2">que</w> <w n="37.3">nous</w> <w n="37.4">méprisons</w> <w n="37.5">dorment</w> <w n="37.6">au</w> <w n="37.7">grand</w> <w n="37.8">soleil</w> ;</l>
							<l n="38" num="10.2"><w n="38.1">Quand</w> <w n="38.2">leur</w> <w n="38.3">chair</w> <w n="38.4">n</w>’<w n="38.5">a</w> <w n="38.6">plus</w> <w n="38.7">faim</w> <w n="38.8">leur</w> <w n="38.9">âme</w> <w n="38.10">est</w> <w n="38.11">assouvie</w>.</l>
							<l n="39" num="10.3"><w n="39.1">Pas</w> <w n="39.2">de</w> <w n="39.3">chimère</w> ! <w n="39.4">Ils</w> <w n="39.5">ont</w> <w n="39.6">l</w>’<w n="39.7">amour</w> <w n="39.8">et</w> <w n="39.9">le</w> <w n="39.10">sommeil</w> :</l>
							<l n="40" num="10.4"><w n="40.1">Dors</w>, <w n="40.2">aime</w> ! <w n="40.3">Et</w> <w n="40.4">c</w>’<w n="40.5">est</w> <w n="40.6">assez</w> <w n="40.7">pour</w> <w n="40.8">leur</w> <w n="40.9">bénir</w> <w n="40.10">la</w> <w n="40.11">vie</w> !</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1"><w n="41.1">Ah</w>, <w n="41.2">malheur</w> <w n="41.3">sur</w> <w n="41.4">nos</w> <w n="41.5">lois</w>, <w n="41.6">malheur</w> <w n="41.7">sur</w> <w n="41.8">la</w> <w n="41.9">raison</w> !</l>
							<l n="42" num="11.2"><w n="42.1">Qui</w> <w n="42.2">donc</w> <w n="42.3">saura</w>, <w n="42.4">parmi</w> <w n="42.5">les</w> <w n="42.6">damnés</w> <w n="42.7">que</w> <w n="42.8">nous</w> <w n="42.9">sommes</w>,</l>
							<l n="43" num="11.3"><w n="43.1">Arracher</w> <w n="43.2">ses</w> <w n="43.3">barreaux</w> <w n="43.4">et</w> <w n="43.5">brûler</w> <w n="43.6">sa</w> <w n="43.7">prison</w> ?</l>
							<l n="44" num="11.4"><w n="44.1">Il</w> <w n="44.2">faudrait</w> <w n="44.3">être</w> <w n="44.4">brute</w> <w n="44.5">ou</w> <w n="44.6">dieu</w> ! <w n="44.7">Malheur</w> <w n="44.8">aux</w> <w n="44.9">hommes</w> !</l>
						</lg>
					</div></body></text></TEI>