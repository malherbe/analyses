<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURELE SOIR</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><div type="poem" key="HAR146">
						<head type="main">LA LUNE</head>
						<opener>
							<salute>À LOUIS BOUQUET</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Pleine</w> <w n="1.2">et</w> <w n="1.3">ronde</w>, <w n="1.4">du</w> <w n="1.5">bord</w> <w n="1.6">de</w> <w n="1.7">l</w>’<w n="1.8">horizon</w> <w n="1.9">dormant</w>,</l>
							<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">Lune</w>, <w n="2.3">avec</w> <w n="2.4">lenteur</w>, <w n="2.5">s</w>’<w n="2.6">enlève</w>, <w n="2.7">solitaire</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">son</w> <w n="3.3">disque</w> <w n="3.4">éclatant</w> <w n="3.5">monte</w> <w n="3.6">dans</w> <w n="3.7">le</w> <w n="3.8">mystère</w></l>
							<l n="4" num="1.4"><w n="4.1">Du</w> <w n="4.2">grand</w> <w n="4.3">ciel</w> <w n="4.4">nu</w> <w n="4.5">qui</w> <w n="4.6">fuit</w> <w n="4.7">silencieusement</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Sa</w> <w n="5.2">clarté</w> <w n="5.3">qui</w> <w n="5.4">rayonne</w> <w n="5.5">emplit</w> <w n="5.6">le</w> <w n="5.7">firmament</w> :</l>
							<l n="6" num="2.2"><w n="6.1">Or</w>, <w n="6.2">c</w>’<w n="6.3">est</w> <w n="6.4">un</w> <w n="6.5">globe</w> <w n="6.6">éteint</w>, <w n="6.7">un</w> <w n="6.8">astre</w> <w n="6.9">involontaire</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Un</w> <w n="7.2">miroir</w> <w n="7.3">sans</w> <w n="7.4">chaleur</w> <w n="7.5">esclave</w> <w n="7.6">de</w> <w n="7.7">la</w> <w n="7.8">terre</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">sa</w> <w n="8.3">calme</w> <w n="8.4">splendeur</w> <w n="8.5">n</w>’<w n="8.6">est</w> <w n="8.7">qu</w>’<w n="8.8">un</w> <w n="8.9">reflet</w> <w n="8.10">qui</w> <w n="8.11">ment</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">— <w n="9.1">Tel</w> <w n="9.2">est</w> <w n="9.3">mon</w> <w n="9.4">cœur</w>, <w n="9.5">hélas</w> : <w n="9.6">inerte</w>, <w n="9.7">froid</w> <w n="9.8">et</w> <w n="9.9">vide</w>,</l>
							<l n="10" num="3.2"><w n="10.1">N</w>’<w n="10.2">ayant</w> <w n="10.3">pour</w> <w n="10.4">s</w>’<w n="10.5">éclairer</w> <w n="10.6">que</w> <w n="10.7">la</w> <w n="10.8">lueur</w> <w n="10.9">livide</w></l>
							<l n="11" num="3.3"><w n="11.1">D</w>’<w n="11.2">une</w> <w n="11.3">vie</w> <w n="11.4">et</w> <w n="11.5">d</w>’<w n="11.6">un</w> <w n="11.7">monde</w> <w n="11.8">où</w> <w n="11.9">tout</w> <w n="11.10">est</w> <w n="11.11">mort</w> <w n="11.12">pour</w> <w n="11.13">lui</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Mon</w> <w n="12.2">cœur</w>, <w n="12.3">qui</w>, <w n="12.4">seul</w>, <w n="12.5">perdu</w> <w n="12.6">dans</w> <w n="12.7">l</w>’<w n="12.8">immensité</w> <w n="12.9">noire</w>,</l>
							<l n="13" num="4.2"><w n="13.1">Suit</w> <w n="13.2">l</w>’<w n="13.3">orbe</w> <w n="13.4">irrésistible</w> <w n="13.5">où</w> <w n="13.6">m</w>’<w n="13.7">entraîne</w> <w n="13.8">l</w>’<w n="13.9">ennui</w>,</l>
							<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">roule</w> <w n="14.3">en</w> <w n="14.4">plein</w> <w n="14.5">néant</w>, <w n="14.6">sans</w> <w n="14.7">force</w> <w n="14.8">et</w> <w n="14.9">sans</w> <w n="14.10">mémoire</w>.</l>
						</lg>
					</div></body></text></TEI>