<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES LOIS</head><div type="poem" key="HAR46">
						<head type="main">LES ATOMES</head>
						<opener>
							<salute>À FERNAND ICRES</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Rien</w> <w n="1.2">n</w>’<w n="1.3">était</w>. <w n="1.4">Le</w> <w n="1.5">Néant</w> <w n="1.6">s</w>’<w n="1.7">étalait</w> <w n="1.8">dans</w> <w n="1.9">la</w> <w n="1.10">nuit</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">Nul</w> <w n="2.2">frisson</w> <w n="2.3">n</w>’<w n="2.4">annonçait</w> <w n="2.5">un</w> <w n="2.6">monde</w> <w n="2.7">qui</w> <w n="2.8">commence</w> :</l>
							<l n="3" num="1.3"><w n="3.1">Sans</w> <w n="3.2">forme</w>, <w n="3.3">sans</w> <w n="3.4">couleur</w>, <w n="3.5">sans</w> <w n="3.6">mouvement</w>, <w n="3.7">sans</w> <w n="3.8">bruit</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">germes</w> <w n="4.3">confondus</w> <w n="4.4">flottaient</w> <w n="4.5">dans</w> <w n="4.6">l</w>’<w n="4.7">ombre</w> <w n="4.8">immense</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">froid</w> <w n="5.3">stérilisait</w> <w n="5.4">les</w> <w n="5.5">espaces</w> <w n="5.6">sans</w> <w n="5.7">fin</w> ;</l>
							<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2">essence</w> <w n="6.3">de</w> <w n="6.4">la</w> <w n="6.5">vie</w> <w n="6.6">et</w> <w n="6.7">la</w> <w n="6.8">source</w> <w n="6.9">des</w> <w n="6.10">causes</w></l>
							<l n="7" num="2.3"><w n="7.1">Sommeillaient</w> <w n="7.2">lourdement</w> <w n="7.3">dans</w> <w n="7.4">le</w> <w n="7.5">chaos</w> <w n="7.6">divin</w>.</l>
							<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">âme</w> <w n="8.3">de</w> <w n="8.4">Pan</w> <w n="8.5">nageait</w> <w n="8.6">dans</w> <w n="8.7">la</w> <w n="8.8">vapeur</w> <w n="8.9">des</w> <w n="8.10">choses</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">L</w>’<w n="9.2">originelle</w> <w n="9.3">Mort</w>, <w n="9.4">d</w>’<w n="9.5">où</w> <w n="9.6">l</w>’<w n="9.7">univers</w> <w n="9.8">est</w> <w n="9.9">né</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Engourdissait</w> <w n="10.2">dans</w> <w n="10.3">l</w>’<w n="10.4">œuf</w> <w n="10.5">l</w>’<w n="10.6">innomable</w> <w n="10.7">matière</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Et</w>, <w n="11.2">sans</w> <w n="11.3">force</w>, <w n="11.4">impuissant</w>, <w n="11.5">le</w> <w n="11.6">Verbe</w> <w n="11.7">consterné</w></l>
							<l n="12" num="3.4"><w n="12.1">Pesait</w> <w n="12.2">dans</w> <w n="12.3">l</w>’<w n="12.4">infini</w> <w n="12.5">son</w> <w n="12.6">œuvre</w> <w n="12.7">tout</w> <w n="12.8">entière</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Soudain</w>, <w n="13.2">sous</w> <w n="13.3">l</w>’<w n="13.4">œil</w> <w n="13.5">de</w> <w n="13.6">Dieu</w> <w n="13.7">qui</w> <w n="13.8">regardait</w> <w n="13.9">sans</w> <w n="13.10">but</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Frémit</w> <w n="14.2">une</w> <w n="14.3">lueur</w> <w n="14.4">vague</w> <w n="14.5">de</w> <w n="14.6">crépuscule</w>.</l>
							<l n="15" num="4.3"><w n="15.1">L</w>’<w n="15.2">atome</w> <w n="15.3">vit</w> <w n="15.4">l</w>’<w n="15.5">atome</w> ; <w n="15.6">il</w> <w n="15.7">bougea</w> : <w n="15.8">l</w>’<w n="15.9">Amour</w> <w n="15.10">fut</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">du</w> <w n="16.3">premier</w> <w n="16.4">baiser</w> <w n="16.5">naquit</w> <w n="16.6">la</w> <w n="16.7">molécule</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Or</w>, <w n="17.2">l</w>’<w n="17.3">Esprit</w>, <w n="17.4">stupéfait</w> <w n="17.5">de</w> <w n="17.6">ces</w> <w n="17.7">accouplements</w></l>
							<l n="18" num="5.2"><w n="18.1">Qui</w> <w n="18.2">grouillaient</w> <w n="18.3">dans</w> <w n="18.4">l</w>’<w n="18.5">abîme</w> <w n="18.6">insondé</w> <w n="18.7">du</w> <w n="18.8">désordre</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Vit</w>, <w n="19.2">dans</w> <w n="19.3">la</w> <w n="19.4">profondeur</w> <w n="19.5">des</w> <w n="19.6">nouveaux</w> <w n="19.7">firmaments</w>,</l>
							<l n="20" num="5.4"><w n="20.1">D</w>’<w n="20.2">infimes</w> <w n="20.3">embryons</w> <w n="20.4">se</w> <w n="20.5">chercher</w> <w n="20.6">et</w> <w n="20.7">se</w> <w n="20.8">mordre</w>.</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Pleins</w> <w n="21.2">de</w> <w n="21.3">lenteur</w> <w n="21.4">pénible</w> <w n="21.5">et</w> <w n="21.6">d</w>’<w n="21.7">efforts</w> <w n="21.8">caressants</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Les</w> <w n="22.2">corps</w> <w n="22.3">erraient</w>, <w n="22.4">tournaient</w> <w n="22.5">et</w> <w n="22.6">s</w>’<w n="22.7">accrochaient</w>, <w n="22.8">sans</w> <w n="22.9">nombre</w>.</l>
							<l n="23" num="6.3"><w n="23.1">L</w>’<w n="23.2">Amour</w> <w n="23.3">inespéré</w> <w n="23.4">subtilisait</w> <w n="23.5">leurs</w> <w n="23.6">sens</w> ;</l>
							<l n="24" num="6.4"><w n="24.1">La</w> <w n="24.2">lumière</w> <w n="24.3">naissait</w> <w n="24.4">des</w> <w n="24.5">frottements</w> <w n="24.6">de</w> <w n="24.7">l</w>’<w n="24.8">ombre</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">les</w> <w n="25.3">astres</w> <w n="25.4">germaient</w>. <w n="25.5">O</w> <w n="25.6">splendeurs</w> ! <w n="25.7">O</w> <w n="25.8">matins</w> !</l>
							<l n="26" num="7.2"><w n="26.1">Chaudes</w> <w n="26.2">affinités</w> <w n="26.3">des</w> <w n="26.4">êtres</w> <w n="26.5">et</w> <w n="26.6">des</w> <w n="26.7">formes</w> !</l>
							<l n="27" num="7.3"><w n="27.1">Les</w> <w n="27.2">soleils</w> <w n="27.3">s</w>’<w n="27.4">envolaient</w> <w n="27.5">sur</w> <w n="27.6">les</w> <w n="27.7">orbes</w> <w n="27.8">lointains</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Entraînant</w> <w n="28.2">par</w> <w n="28.3">troupeaux</w> <w n="28.4">les</w> <w n="28.5">planètes</w> <w n="28.6">énormes</w> !</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Des</w> <w n="29.2">feux</w> <w n="29.3">tourbillonnants</w> <w n="29.4">fendaient</w> <w n="29.5">l</w>’<w n="29.6">immensité</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">les</w> <w n="30.3">sphères</w> <w n="30.4">en</w> <w n="30.5">rut</w> <w n="30.6">roulaient</w> <w n="30.7">leurs</w> <w n="30.8">masses</w> <w n="30.9">rondes</w> :</l>
							<l n="31" num="8.3"><w n="31.1">Leurs</w> <w n="31.2">flancs</w>, <w n="31.3">brûlés</w> <w n="31.4">d</w>’<w n="31.5">amour</w> <w n="31.6">et</w> <w n="31.7">de</w> <w n="31.8">fécondité</w>,</l>
							<l n="32" num="8.4"><w n="32.1">Crachaient</w> <w n="32.2">à</w> <w n="32.3">pleins</w> <w n="32.4">volcans</w> <w n="32.5">la</w> <w n="32.6">semence</w> <w n="32.7">des</w> <w n="32.8">mondes</w>.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">Puis</w>, <w n="33.2">les</w> <w n="33.3">éléments</w> <w n="33.4">lourds</w> <w n="33.5">s</w>’<w n="33.6">ordonnaient</w>, <w n="33.7">divisés</w> :</l>
							<l n="34" num="9.2"><w n="34.1">Les</w> <w n="34.2">terres</w> <w n="34.3">s</w>’<w n="34.4">habillaient</w> <w n="34.5">de</w> <w n="34.6">roches</w> <w n="34.7">et</w> <w n="34.8">de</w> <w n="34.9">plantes</w> ;</l>
							<l n="35" num="9.3"><w n="35.1">L</w>’<w n="35.2">air</w> <w n="35.3">tiède</w> <w n="35.4">enveloppait</w> <w n="35.5">les</w> <w n="35.6">globes</w> <w n="35.7">de</w> <w n="35.8">baisers</w>,</l>
							<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">les</w> <w n="36.3">mers</w> <w n="36.4">aux</w> <w n="36.5">flots</w> <w n="36.6">bleus</w> <w n="36.7">chantaient</w> <w n="36.8">leurs</w> <w n="36.9">hymnes</w> <w n="36.10">lentes</w>.</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">C</w>’<w n="37.2">est</w> <w n="37.3">alors</w> <w n="37.4">qu</w>’<w n="37.5">au</w> <w n="37.6">milieu</w> <w n="37.7">du</w> <w n="37.8">monde</w> <w n="37.9">épais</w> <w n="37.10">et</w> <w n="37.11">brut</w>,</l>
							<l n="38" num="10.2"><w n="38.1">Debout</w>, <w n="38.2">fier</w>, <w n="38.3">et</w> <w n="38.4">criant</w> <w n="38.5">l</w>’<w n="38.6">éternelle</w> <w n="38.7">victoire</w>,</l>
							<l n="39" num="10.3"><w n="39.1">Chef</w>-<w n="39.2">d</w>’<w n="39.3">œuvre</w> <w n="39.4">de</w> <w n="39.5">l</w>’<w n="39.6">Amour</w>, <w n="39.7">l</w>’<w n="39.8">être</w> <w n="39.9">vivant</w> <w n="39.10">parut</w> !</l>
							<l n="40" num="10.4">— <w n="40.1">Et</w> <w n="40.2">Dieu</w> <w n="40.3">sentit</w> <w n="40.4">l</w>’<w n="40.5">horreur</w> <w n="40.6">d</w>’<w n="40.7">être</w> <w n="40.8">seul</w> <w n="40.9">dans</w> <w n="40.10">sa</w> <w n="40.11">gloire</w>.</l>
						</lg>
					</div></body></text></TEI>