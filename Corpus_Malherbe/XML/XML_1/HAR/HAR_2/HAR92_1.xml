<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES FORMES</head><div type="poem" key="HAR92">
						<head type="main">DEMI-DEUIL</head>
						<opener>
							<salute>À ALFRED GUESVILLER</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">nuit</w> <w n="1.3">voluptueuse</w> <w n="1.4">et</w> <w n="1.5">triste</w>, <w n="1.6">par</w> <w n="1.7">degrés</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Descend</w> <w n="2.2">les</w> <w n="2.3">escaliers</w> <w n="2.4">du</w> <w n="2.5">ciel</w>. <w n="2.6">Le</w> <w n="2.7">jour</w> <w n="2.8">se</w> <w n="2.9">sauve</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Posant</w> <w n="3.2">son</w> <w n="3.3">pied</w> <w n="3.4">furtif</w> <w n="3.5">sur</w> <w n="3.6">les</w> <w n="3.7">toits</w> <w n="3.8">mordorés</w>.</l>
						</lg>
						<lg n="2">
							<l n="4" num="2.1"><w n="4.1">Des</w> <w n="4.2">nuages</w> <w n="4.3">lilas</w> <w n="4.4">fleurissent</w> <w n="4.5">dans</w> <w n="4.6">l</w>’<w n="4.7">air</w> <w n="4.8">mauve</w></l>
							<l n="5" num="2.2"><w n="5.1">Qui</w> <w n="5.2">palpite</w> <w n="5.3">au</w>-<w n="5.4">dessus</w> <w n="5.5">des</w> <w n="5.6">coteaux</w> <w n="5.7">violets</w>,</l>
							<l n="6" num="2.3"><w n="6.1">Et</w> <w n="6.2">le</w> <w n="6.3">vent</w> <w n="6.4">recueilli</w> <w n="6.5">prend</w> <w n="6.6">des</w> <w n="6.7">senteurs</w> <w n="6.8">d</w>’<w n="6.9">alcôve</w>.</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1"><w n="7.1">La</w> <w n="7.2">glycine</w> <w n="7.3">en</w> <w n="7.4">festons</w> <w n="7.5">grimpe</w> <w n="7.6">autour</w> <w n="7.7">des</w> <w n="7.8">volets</w> ;</l>
							<l n="8" num="3.2"><w n="8.1">Les</w> <w n="8.2">saules</w> <w n="8.3">prosternés</w> <w n="8.4">sous</w> <w n="8.5">leurs</w> <w n="8.6">branches</w> <w n="8.7">pieuses</w>,</l>
							<l n="9" num="3.3"><w n="9.1">Sont</w> <w n="9.2">des</w> <w n="9.3">bouquets</w> <w n="9.4">vivants</w> <w n="9.5">de</w> <w n="9.6">cris</w> <w n="9.7">et</w> <w n="9.8">de</w> <w n="9.9">sifflets</w>.</l>
						</lg>
						<lg n="4">
							<l n="10" num="4.1"><w n="10.1">La</w> <w n="10.2">jacinthe</w>, <w n="10.3">l</w>’<w n="10.4">iris</w>, <w n="10.5">les</w> <w n="10.6">pâles</w> <w n="10.7">scabieuses</w></l>
							<l n="11" num="4.2"><w n="11.1">Mêlent</w> <w n="11.2">leurs</w> <w n="11.3">tons</w> <w n="11.4">mêlés</w> <w n="11.5">de</w> <w n="11.6">joie</w> <w n="11.7">et</w> <w n="11.8">de</w> <w n="11.9">chagrin</w> ;</l>
							<l n="12" num="4.3"><w n="12.1">Les</w> <w n="12.2">cyprès</w> <w n="12.3">sont</w> <w n="12.4">des</w> <w n="12.5">nids</w> <w n="12.6">pleins</w> <w n="12.7">de</w> <w n="12.8">chansons</w> <w n="12.9">rieuses</w>.</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1"><w n="13.1">Et</w> <w n="13.2">svelte</w>, <w n="13.3">toute</w> <w n="13.4">droite</w>, <w n="13.5">au</w> <w n="13.6">bord</w> <w n="13.7">du</w> <w n="13.8">boulingrin</w>,</l>
							<l n="14" num="5.2"><w n="14.1">Regardant</w> <w n="14.2">d</w>’<w n="14.3">un</w> <w n="14.4">œil</w> <w n="14.5">doux</w> <w n="14.6">les</w> <w n="14.7">lilas</w> <w n="14.8">et</w> <w n="14.9">les</w> <w n="14.10">chênes</w></l>
							<l n="15" num="5.3"><w n="15.1">Bleuir</w> <w n="15.2">et</w> <w n="15.3">rougeoyer</w> <w n="15.4">dans</w> <w n="15.5">un</w> <w n="15.6">brouillard</w> <w n="15.7">serein</w>,</l>
						</lg>
						<lg n="6">
							<l n="16" num="6.1"><w n="16.1">La</w> <w n="16.2">veuve</w> <w n="16.3">en</w> <w n="16.4">demi</w>-<w n="16.5">deuil</w> <w n="16.6">rêve</w> <w n="16.7">aux</w> <w n="16.8">amours</w> <w n="16.9">prochaines</w>.</l>
						</lg>
					</div></body></text></TEI>