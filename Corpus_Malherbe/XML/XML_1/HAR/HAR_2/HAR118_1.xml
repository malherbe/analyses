<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR118">
						<head type="main">PARISIENNE</head>
						<opener>
							<salute>À ACHILLE MÉLANDRI</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Redressant</w> <w n="1.2">les</w> <w n="1.3">rondeurs</w> <w n="1.4">de</w> <w n="1.5">son</w> <w n="1.6">buste</w> <w n="1.7">replet</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Droite</w>, <w n="2.2">le</w> <w n="2.3">nez</w> <w n="2.4">railleur</w> <w n="2.5">et</w> <w n="2.6">la</w> <w n="2.7">lèvre</w> <w n="2.8">mutine</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Elle</w> <w n="3.2">va</w> : <w n="3.3">son</w> <w n="3.4">regard</w> <w n="3.5">qui</w> <w n="3.6">voltige</w> <w n="3.7">et</w> <w n="3.8">butine</w></l>
							<l n="4" num="1.4"><w n="4.1">Se</w> <w n="4.2">pose</w> <w n="4.3">au</w> <w n="4.4">bord</w> <w n="4.5">de</w> <w n="4.6">tout</w>, <w n="4.7">prend</w> <w n="4.8">à</w> <w n="4.9">tout</w> <w n="4.10">un</w> <w n="4.11">reflet</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Elle</w> <w n="5.2">va</w>. <w n="5.3">Sous</w> <w n="5.4">les</w> <w n="5.5">plis</w> <w n="5.6">susurrants</w> <w n="5.7">de</w> <w n="5.8">l</w>’<w n="5.9">ourlet</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">pied</w> <w n="6.3">vif</w>, <w n="6.4">provocant</w> <w n="6.5">et</w> <w n="6.6">plein</w> <w n="6.7">d</w>’<w n="6.8">esprit</w>, <w n="6.9">trottine</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Rase</w> <w n="7.2">l</w>’<w n="7.3">asphalte</w>, <w n="7.4">et</w> <w n="7.5">rit</w>, <w n="7.6">au</w> <w n="7.7">fond</w> <w n="7.8">de</w> <w n="7.9">la</w> <w n="7.10">bottine</w>.</l>
							<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">se</w> <w n="8.3">voir</w> <w n="8.4">si</w> <w n="8.5">petit</w> <w n="8.6">et</w> <w n="8.7">si</w> <w n="8.8">près</w> <w n="8.9">du</w> <w n="8.10">mollet</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">main</w> <w n="9.3">gantée</w>, <w n="9.4">au</w> <w n="9.5">bord</w> <w n="9.6">de</w> <w n="9.7">la</w> <w n="9.8">manchette</w> <w n="9.9">blanche</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Berce</w> <w n="10.2">l</w>’<w n="10.3">éventail</w> <w n="10.4">noir</w> <w n="10.5">qui</w> <w n="10.6">caresse</w> <w n="10.7">la</w> <w n="10.8">hanche</w></l>
							<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">rythme</w> <w n="11.3">au</w> <w n="11.4">bruit</w> <w n="11.5">des</w> <w n="11.6">pas</w> <w n="11.7">son</w> <w n="11.8">doux</w> <w n="11.9">balancement</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Un</w> <w n="12.2">sillage</w> <w n="12.3">odorant</w> <w n="12.4">la</w> <w n="12.5">suit</w>, <w n="12.6">senteurs</w> <w n="12.7">de</w> <w n="12.8">femmes</w>,</l>
							<l n="13" num="4.2"><w n="13.1">Parfums</w> <w n="13.2">de</w> <w n="13.3">fleurs</w>… <w n="13.4">Et</w> <w n="13.5">souple</w>, <w n="13.6">elle</w> <w n="13.7">marche</w>, <w n="13.8">semant</w></l>
							<l n="14" num="4.3"><w n="14.1">Le</w> <w n="14.2">germe</w> <w n="14.3">des</w> <w n="14.4">désirs</w> <w n="14.5">chauds</w> <w n="14.6">et</w> <w n="14.7">virils</w> <w n="14.8">dans</w> <w n="14.9">l</w>’<w n="14.10">âme</w> !</l>
						</lg>
					</div></body></text></TEI>