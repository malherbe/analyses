<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES LOIS</head><div type="poem" key="HAR44">
						<head type="main">LA RÉPONSE DE LA TERRE</head>
						<opener>
							<salute>À HENRI BOULEY</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">crié</w> <w n="1.4">vers</w> <w n="1.5">la</w> <w n="1.6">Terre</w> : « <w n="1.7">Aïeule</w>, <w n="1.8">ô</w> <w n="1.9">bonne</w> <w n="1.10">aïeule</w> !</l>
							<l n="2" num="1.2"><w n="2.1">Déesse</w> <w n="2.2">de</w> <w n="2.3">nos</w> <w n="2.4">dieux</w>, <w n="2.5">toi</w> <w n="2.6">la</w> <w n="2.7">Rhée</w> <w n="2.8">et</w> <w n="2.9">l</w>’<w n="2.10">Isis</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Toi</w> <w n="3.2">qui</w> <w n="3.3">fais</w> <w n="3.4">refleurir</w> <w n="3.5">les</w> <w n="3.6">bleuets</w> <w n="3.7">dans</w> <w n="3.8">l</w>’<w n="3.9">éteule</w></l>
							<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">susurrer</w> <w n="4.3">la</w> <w n="4.4">source</w> <w n="4.5">au</w> <w n="4.6">fond</w> <w n="4.7">des</w> <w n="4.8">oasis</w> ;</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">« <w n="5.1">Toi</w> <w n="5.2">qui</w> <w n="5.3">donnes</w> <w n="5.4">aux</w> <w n="5.5">nids</w> <w n="5.6">le</w> <w n="5.7">dais</w> <w n="5.8">mouvant</w> <w n="5.9">des</w> <w n="5.10">feuilles</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">qui</w> <w n="6.3">verses</w> <w n="6.4">ta</w> <w n="6.5">sève</w> <w n="6.6">aux</w> <w n="6.7">arbres</w> <w n="6.8">jaunissants</w> ;</l>
							<l n="7" num="2.3"><w n="7.1">Qui</w> <w n="7.2">nourris</w> <w n="7.3">les</w> <w n="7.4">oiseaux</w> <w n="7.5">des</w> <w n="7.6">graines</w> <w n="7.7">que</w> <w n="7.8">tu</w> <w n="7.9">cueilles</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">qui</w> <w n="8.3">berces</w> <w n="8.4">les</w> <w n="8.5">mers</w> <w n="8.6">entre</w> <w n="8.7">tes</w> <w n="8.8">seins</w> <w n="8.9">puissants</w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1">« <w n="9.1">Pitié</w>, <w n="9.2">prends</w> <w n="9.3">en</w> <w n="9.4">pitié</w> <w n="9.5">les</w> <w n="9.6">martyrs</w> <w n="9.7">que</w> <w n="9.8">nous</w> <w n="9.9">sommes</w> :</l>
							<l n="10" num="3.2"><w n="10.1">Notre</w> <w n="10.2">effort</w> <w n="10.3">épuisé</w> <w n="10.4">trébuche</w> <w n="10.5">à</w> <w n="10.6">chaque</w> <w n="10.7">pas</w>.</l>
							<l n="11" num="3.3"><w n="11.1">Aïeule</w>, <w n="11.2">est</w>-<w n="11.3">ce</w> <w n="11.4">que</w> <w n="11.5">tout</w> <w n="11.6">souffre</w> <w n="11.7">autant</w> <w n="11.8">que</w> <w n="11.9">les</w> <w n="11.10">hommes</w> ? »</l>
							<l n="12" num="3.4">— <w n="12.1">Mais</w> <w n="12.2">la</w> <w n="12.3">Terre</w> <w n="12.4">m</w>’<w n="12.5">a</w> <w n="12.6">dit</w> : « <w n="12.7">Je</w> <w n="12.8">ne</w> <w n="12.9">te</w> <w n="12.10">connais</w> <w n="12.11">pas</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1">« <w n="13.1">Ai</w>-<w n="13.2">je</w> <w n="13.3">compté</w> <w n="13.4">les</w> <w n="13.5">fleurs</w>, <w n="13.6">les</w> <w n="13.7">mouches</w>, <w n="13.8">les</w> <w n="13.9">nuages</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Les</w> <w n="14.2">formes</w> <w n="14.3">de</w> <w n="14.4">la</w> <w n="14.5">chair</w>, <w n="14.6">des</w> <w n="14.7">plantes</w>, <w n="14.8">du</w> <w n="14.9">métal</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Les</w> <w n="15.2">cris</w> <w n="15.3">du</w> <w n="15.4">vent</w>, <w n="15.5">l</w>’<w n="15.6">écume</w> <w n="15.7">ou</w> <w n="15.8">le</w> <w n="15.9">sable</w> <w n="15.10">des</w> <w n="15.11">plages</w> ?</l>
							<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">qu</w>’<w n="16.3">es</w>-<w n="16.4">tu</w> <w n="16.5">donc</w>, <w n="16.6">sinon</w> <w n="16.7">leur</w> <w n="16.8">frère</w> <w n="16.9">et</w> <w n="16.10">leur</w> <w n="16.11">égal</w> ?</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1">« <w n="17.1">Rien</w> ! <w n="17.2">Et</w> <w n="17.3">je</w> <w n="17.4">ne</w> <w n="17.5">sais</w> <w n="17.6">rien</w> <w n="17.7">de</w> <w n="17.8">ceux</w> <w n="17.9">que</w> <w n="17.10">je</w> <w n="17.11">renferme</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Pas</w> <w n="18.2">plus</w> <w n="18.3">que</w> <w n="18.4">tu</w> <w n="18.5">ne</w> <w n="18.6">sais</w> <w n="18.7">l</w>’<w n="18.8">angoisse</w> <w n="18.9">ou</w> <w n="18.10">les</w> <w n="18.11">gaîtés</w></l>
							<l n="19" num="5.3"><w n="19.1">Des</w> <w n="19.2">millions</w> <w n="19.3">de</w> <w n="19.4">corps</w> <w n="19.5">qui</w> <w n="19.6">vibrent</w> <w n="19.7">sous</w> <w n="19.8">ton</w> <w n="19.9">derme</w>,</l>
							<l n="20" num="5.4"><w n="20.1">Infiniment</w> <w n="20.2">petits</w> <w n="20.3">et</w> <w n="20.4">toujours</w> <w n="20.5">habités</w> !</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1">« <w n="21.1">J</w>’<w n="21.2">ignore</w> <w n="21.3">tout</w>, <w n="21.4">les</w> <w n="21.5">noms</w> <w n="21.6">et</w> <w n="21.7">le</w> <w n="21.8">nombre</w> <w n="21.9">des</w> <w n="21.10">races</w></l>
							<l n="22" num="6.2"><w n="22.1">Qui</w> <w n="22.2">pullulent</w> <w n="22.3">de</w> <w n="22.4">moi</w> <w n="22.5">pour</w> <w n="22.6">courir</w> <w n="22.7">sur</w> <w n="22.8">mes</w> <w n="22.9">flancs</w>,</l>
							<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">mon</w> <w n="23.3">indifférence</w> <w n="23.4">efface</w> <w n="23.5">jusqu</w>’<w n="23.6">aux</w> <w n="23.7">traces</w></l>
							<l n="24" num="6.4"><w n="24.1">De</w> <w n="24.2">ceux</w> <w n="24.3">qui</w> <w n="24.4">sont</w> <w n="24.5">passés</w> <w n="24.6">depuis</w> <w n="24.7">cent</w> <w n="24.8">fois</w> <w n="24.9">mille</w> <w n="24.10">ans</w> ! »</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1">— <w n="25.1">J</w>’<w n="25.2">ai</w> <w n="25.3">crié</w> <w n="25.4">vers</w> <w n="25.5">la</w> <w n="25.6">Terre</w> : « <w n="25.7">Aïeule</w>, <w n="25.8">ô</w> <w n="25.9">dure</w> <w n="25.10">aïeule</w> !</l>
							<l n="26" num="7.2"><w n="26.1">Ô</w> <w n="26.2">marâtre</w> ! <w n="26.3">Du</w> <w n="26.4">moins</w>, <w n="26.5">si</w> <w n="26.6">ton</w> <w n="26.7">cœur</w> <w n="26.8">reste</w> <w n="26.9">clos</w></l>
							<l n="27" num="7.3"><w n="27.1">Au</w> <w n="27.2">râle</w> <w n="27.3">de</w> <w n="27.4">tes</w> <w n="27.5">fils</w> <w n="27.6">écrasés</w> <w n="27.7">sous</w> <w n="27.8">la</w> <w n="27.9">meule</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">si</w> <w n="28.3">ton</w> <w n="28.4">vieux</w> <w n="28.5">mépris</w> <w n="28.6">n</w>’<w n="28.7">entend</w> <w n="28.8">pas</w> <w n="28.9">nos</w> <w n="28.10">sanglots</w> :</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1">« <w n="29.1">Nous</w> <w n="29.2">diras</w>-<w n="29.3">tu</w> <w n="29.4">quel</w> <w n="29.5">est</w> <w n="29.6">le</w> <w n="29.7">terme</w> <w n="29.8">de</w> <w n="29.9">la</w> <w n="29.10">route</w>,</l>
							<l n="30" num="8.2"><w n="30.1">Quel</w> <w n="30.2">mystère</w> <w n="30.3">est</w> <w n="30.4">caché</w> <w n="30.5">dans</w> <w n="30.6">la</w> <w n="30.7">nuit</w> <w n="30.8">du</w> <w n="30.9">trépas</w>,</l>
							<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">si</w> <w n="31.3">c</w>’<w n="31.4">est</w> <w n="31.5">bien</w> <w n="31.6">fini</w> <w n="31.7">quand</w> <w n="31.8">la</w> <w n="31.9">chair</w> <w n="31.10">est</w> <w n="31.11">dissoute</w> ? »</l>
							<l n="32" num="8.4">— <w n="32.1">Mais</w> <w n="32.2">la</w> <w n="32.3">Terre</w> <w n="32.4">m</w>’<w n="32.5">a</w> <w n="32.6">dit</w> : « <w n="32.7">Ta</w> <w n="32.8">mort</w> <w n="32.9">n</w>’<w n="32.10">existe</w> <w n="32.11">pas</w>.</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1">« <w n="33.1">N</w>’<w n="33.2">être</w> <w n="33.3">plus</w> ! <w n="33.4">Vanité</w> <w n="33.5">d</w>’<w n="33.6">un</w> <w n="33.7">germe</w> <w n="33.8">qui</w> <w n="33.9">croit</w> <w n="33.10">vivre</w> !</l>
							<l n="34" num="9.2"><w n="34.1">Présomption</w> <w n="34.2">d</w>’<w n="34.3">atome</w> <w n="34.4">errant</w> <w n="34.5">dans</w> <w n="34.6">le</w> <w n="34.7">plein</w> <w n="34.8">ciel</w> !</l>
							<l n="35" num="9.3"><w n="35.1">Orgueil</w> <w n="35.2">stupide</w> <w n="35.3">et</w> <w n="35.4">fou</w> ! <w n="35.5">Rêve</w> <w n="35.6">de</w> <w n="35.7">frelon</w> <w n="35.8">ivre</w> !</l>
							<l n="36" num="9.4"><w n="36.1">Ce</w> <w n="36.2">n</w>’<w n="36.3">est</w> <w n="36.4">pas</w> <w n="36.5">toi</w> <w n="36.6">qui</w> <w n="36.7">vis</w>, <w n="36.8">c</w>’<w n="36.9">est</w> <w n="36.10">l</w>’<w n="36.11">Être</w> <w n="36.12">universel</w>.</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1">« <w n="37.1">L</w>’<w n="37.2">Être</w> <w n="37.3">total</w>, <w n="37.4">matière</w> <w n="37.5">et</w> <w n="37.6">force</w>, <w n="37.7">esclave</w> <w n="37.8">et</w> <w n="37.9">maître</w>,</l>
							<l n="38" num="10.2"><w n="38.1">L</w>’<w n="38.2">immortel</w> <w n="38.3">incréé</w>, <w n="38.4">le</w> <w n="38.5">Dieu</w>, <w n="38.6">le</w> <w n="38.7">seul</w> <w n="38.8">vrai</w> <w n="38.9">Dieu</w>,</l>
							<l n="39" num="10.3"><w n="39.1">En</w> <w n="39.2">qui</w> <w n="39.3">rien</w> <w n="39.4">ne</w> <w n="39.5">saurait</w> <w n="39.6">venir</w> <w n="39.7">ou</w> <w n="39.8">disparaître</w></l>
							<l n="40" num="10.4"><w n="40.1">Car</w> <w n="40.2">il</w> <w n="40.3">est</w> <w n="40.4">infini</w> <w n="40.5">dans</w> <w n="40.6">le</w> <w n="40.7">temps</w> <w n="40.8">et</w> <w n="40.9">le</w> <w n="40.10">lieu</w> !</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1">« <w n="41.1">Seul</w>, <w n="41.2">il</w> <w n="41.3">vit</w>. <w n="41.4">Et</w> <w n="41.5">que</w> <w n="41.6">font</w> <w n="41.7">un</w> <w n="41.8">brin</w> <w n="41.9">d</w>’<w n="41.10">herbe</w> <w n="41.11">qu</w>’<w n="41.12">on</w> <w n="41.13">mange</w>,</l>
							<l n="42" num="11.2"><w n="42.1">Un</w> <w n="42.2">soleil</w> <w n="42.3">qui</w> <w n="42.4">s</w>’<w n="42.5">effrite</w>, <w n="42.6">un</w> <w n="42.7">homme</w> <w n="42.8">qui</w> <w n="42.9">s</w>’<w n="42.10">endort</w> ?</l>
							<l n="43" num="11.3"><w n="43.1">Le</w> <w n="43.2">Dieu</w> <w n="43.3">sent</w> <w n="43.4">palpiter</w> <w n="43.5">sa</w> <w n="43.6">vie</w> <w n="43.7">énorme</w> : <w n="43.8">il</w> <w n="43.9">change</w>,</l>
							<l n="44" num="11.4"><w n="44.1">Il</w> <w n="44.2">respire</w>, <w n="44.3">et</w> <w n="44.4">son</w> <w n="44.5">souffle</w> <w n="44.6">est</w> <w n="44.7">fait</w> <w n="44.8">avec</w> <w n="44.9">la</w> <w n="44.10">Mort</w>.</l>
						</lg>
						<lg n="12">
							<l n="45" num="12.1">« <w n="45.1">La</w> <w n="45.2">Mort</w>, <w n="45.3">c</w>’<w n="45.4">est</w> <w n="45.5">la</w> <w n="45.6">formule</w> <w n="45.7">unique</w> <w n="45.8">de</w> <w n="45.9">la</w> <w n="45.10">Vie</w>,</l>
							<l n="46" num="12.2"><w n="46.1">Le</w> <w n="46.2">passage</w> <w n="46.3">alterné</w> <w n="46.4">des</w> <w n="46.5">corps</w> <w n="46.6">dans</w> <w n="46.7">d</w>’<w n="46.8">autres</w> <w n="46.9">corps</w>,</l>
							<l n="47" num="12.3"><w n="47.1">C</w>’<w n="47.2">est</w> <w n="47.3">le</w> <w n="47.4">mouvement</w> <w n="47.5">calme</w> <w n="47.6">et</w> <w n="47.7">dont</w> <w n="47.8">rien</w> <w n="47.9">ne</w> <w n="47.10">dévie</w>,</l>
							<l n="48" num="12.4"><w n="48.1">La</w> <w n="48.2">résurrection</w> <w n="48.3">des</w> <w n="48.4">faibles</w> <w n="48.5">dans</w> <w n="48.6">les</w> <w n="48.7">forts</w>.</l>
						</lg>
						<lg n="13">
							<l n="49" num="13.1">« <w n="49.1">C</w>’<w n="49.2">est</w> <w n="49.3">la</w> <w n="49.4">rajeunissante</w> <w n="49.5">et</w> <w n="49.6">la</w> <w n="49.7">réparatrice</w>,</l>
							<l n="50" num="13.2"><w n="50.1">Aurore</w> <w n="50.2">après</w> <w n="50.3">le</w> <w n="50.4">jour</w>, <w n="50.5">printemps</w> <w n="50.6">après</w> <w n="50.7">l</w>’<w n="50.8">été</w>,</l>
							<l n="51" num="13.3"><w n="51.1">La</w> <w n="51.2">mère</w> <w n="51.3">inépuisable</w> <w n="51.4">et</w> <w n="51.5">l</w>’<w n="51.6">auguste</w> <w n="51.7">nourrice</w></l>
							<l n="52" num="13.4"><w n="52.1">Dont</w> <w n="52.2">le</w> <w n="52.3">travail</w> <w n="52.4">fécond</w> <w n="52.5">peuple</w> <w n="52.6">l</w>’<w n="52.7">éternité</w>.</l>
						</lg>
						<lg n="14">
							<l n="53" num="14.1">« <w n="53.1">C</w>’<w n="53.2">est</w> <w n="53.3">la</w> <w n="53.4">chaîne</w> <w n="53.5">d</w>’<w n="53.6">amour</w> <w n="53.7">et</w> <w n="53.8">d</w>’<w n="53.9">hymen</w> <w n="53.10">qui</w> <w n="53.11">nous</w> <w n="53.12">lie</w> :</l>
							<l n="54" num="14.2"><w n="54.1">C</w>’<w n="54.2">est</w> <w n="54.3">par</w> <w n="54.4">elle</w> <w n="54.5">que</w> <w n="54.6">tout</w> <w n="54.7">se</w> <w n="54.8">fond</w> <w n="54.9">et</w> <w n="54.10">se</w> <w n="54.11">confond</w>,</l>
							<l n="55" num="14.3"><w n="55.1">Naît</w>, <w n="55.2">se</w> <w n="55.3">croise</w>, <w n="55.4">renaît</w>, <w n="55.5">court</w> <w n="55.6">et</w> <w n="55.7">se</w> <w n="55.8">multiplie</w>,</l>
							<l n="56" num="14.4"><w n="56.1">Dans</w> <w n="56.2">les</w> <w n="56.3">bouillonnements</w> <w n="56.4">de</w> <w n="56.5">l</w>’<w n="56.6">espace</w> <w n="56.7">sans</w> <w n="56.8">fond</w>.</l>
						</lg>
						<lg n="15">
							<l n="57" num="15.1">« <w n="57.1">Elle</w> <w n="57.2">accouple</w>, <w n="57.3">elle</w> <w n="57.4">brise</w>, <w n="57.5">elle</w> <w n="57.6">épure</w>, <w n="57.7">elle</w> <w n="57.8">émonde</w> ;</l>
							<l n="58" num="15.2"><w n="58.1">Nous</w> <w n="58.2">sommes</w> <w n="58.3">tous</w> <w n="58.4">égaux</w> <w n="58.5">pour</w> <w n="58.6">elle</w>, <w n="58.7">et</w> <w n="58.8">je</w> <w n="58.9">ne</w> <w n="58.10">suis</w></l>
							<l n="59" num="15.3"><w n="59.1">Qu</w>’<w n="59.2">un</w> <w n="59.3">globule</w> <w n="59.4">de</w> <w n="59.5">sang</w> <w n="59.6">dans</w> <w n="59.7">les</w> <w n="59.8">veines</w> <w n="59.9">du</w> <w n="59.10">monde</w>,</l>
							<l n="60" num="15.4"><w n="60.1">Un</w> <w n="60.2">point</w> <w n="60.3">d</w>’<w n="60.4">ombre</w> <w n="60.5">dans</w> <w n="60.6">l</w>’<w n="60.7">ombre</w> <w n="60.8">insondable</w> <w n="60.9">des</w> <w n="60.10">nuits</w>.</l>
						</lg>
						<lg n="16">
							<l n="61" num="16.1">« <w n="61.1">Je</w> <w n="61.2">m</w>’<w n="61.3">éparpillerais</w> <w n="61.4">dans</w> <w n="61.5">la</w> <w n="61.6">poussière</w> <w n="61.7">immense</w>,</l>
							<l n="62" num="16.2"><w n="62.1">Sans</w> <w n="62.2">troubler</w> <w n="62.3">un</w> <w n="62.4">instant</w> <w n="62.5">la</w> <w n="62.6">paix</w> <w n="62.7">de</w> <w n="62.8">l</w>’<w n="62.9">ordre</w> <w n="62.10">ancien</w>,</l>
							<l n="63" num="16.3"><w n="63.1">Et</w> <w n="63.2">l</w>’<w n="63.3">astre</w> <w n="63.4">dont</w> <w n="63.5">je</w> <w n="63.6">suis</w> <w n="63.7">la</w> <w n="63.8">quinzième</w> <w n="63.9">semence</w>,</l>
							<l n="64" num="16.4"><w n="64.1">Le</w> <w n="64.2">Soleil</w> <w n="64.3">s</w>’<w n="64.4">éteindrait</w> <w n="64.5">sans</w> <w n="64.6">que</w> <w n="64.7">rien</w> <w n="64.8">en</w> <w n="64.9">sût</w> <w n="64.10">rien</w> ! »</l>
						</lg>
					</div></body></text></TEI>