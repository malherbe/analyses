<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR67">
						<head type="main">SONNETS DE SANG</head>
						<head type="number">III</head>
						<head type="sub">SOCIÉTÉ</head>
						<opener>
							<salute>À JOSÉ MARIA DE HEREDIA</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">foule</w>, <w n="1.3">ivre</w> <w n="1.4">du</w> <w n="1.5">sang</w> <w n="1.6">promis</w>, <w n="1.7">trépigne</w> <w n="1.8">et</w> <w n="1.9">hue</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Battant</w> <w n="2.2">des</w> <w n="2.3">mains</w>, <w n="2.4">jetant</w> <w n="2.5">des</w> <w n="2.6">cris</w> <w n="2.7">et</w> <w n="2.8">des</w> <w n="2.9">chansons</w>.</l>
							<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">air</w> <w n="3.3">flambe</w> ; <w n="3.4">l</w>’<w n="3.5">échafaud</w>, <w n="3.6">droit</w> <w n="3.7">sur</w> <w n="3.8">ses</w> <w n="3.9">étançons</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Se</w> <w n="4.2">hausse</w> <w n="4.3">comme</w> <w n="4.4">un</w> <w n="4.5">phare</w> <w n="4.6">et</w> <w n="4.7">luit</w> <w n="4.8">sur</w> <w n="4.9">la</w> <w n="4.10">cohue</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Voici</w> <w n="5.2">qu</w>’<w n="5.3">un</w> <w n="5.4">flot</w> <w n="5.5">d</w>’<w n="5.6">enfants</w> <w n="5.7">et</w> <w n="5.8">de</w> <w n="5.9">femmes</w> <w n="5.10">se</w> <w n="5.11">rue</w> :</l>
							<l n="6" num="2.2"><w n="6.1">Fusils</w>, <w n="6.2">faux</w>, <w n="6.3">sabres</w> <w n="6.4">nus</w>, <w n="6.5">piques</w>, <w n="6.6">estramaçons</w>.</l>
							<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">acier</w> <w n="7.3">brille</w> ; <w n="7.4">les</w> <w n="7.5">gueux</w> <w n="7.6">trônent</w> <w n="7.7">sur</w> <w n="7.8">les</w> <w n="7.9">arçons</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">le</w> <w n="8.3">chariot</w> <w n="8.4">tourne</w> <w n="8.5">à</w> <w n="8.6">l</w>’<w n="8.7">angle</w> <w n="8.8">de</w> <w n="8.9">la</w> <w n="8.10">rue</w>.</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">vient</w>, <w n="9.3">pesant</w>… <w n="9.4">Chénier</w> <w n="9.5">monte</w> <w n="9.6">sur</w> <w n="9.7">l</w>’<w n="9.8">échafaud</w>.</l>
							<l n="10" num="3.2">« <w n="10.1">À</w> <w n="10.2">mort</w> ! <w n="10.3">À</w> <w n="10.4">mort</w> ! » <w n="10.5">Superbe</w>, <w n="10.6">il</w> <w n="10.7">attend</w>, <w n="10.8">le</w> <w n="10.9">front</w> <w n="10.10">haut</w> :</l>
							<l n="11" num="3.3"><w n="11.1">Un</w> <w n="11.2">vers</w> <w n="11.3">harmonieux</w> <w n="11.4">chante</w> <w n="11.5">dans</w> <w n="11.6">sa</w> <w n="11.7">pensée</w>.</l>
						</lg>
						<lg n="4">
							<l n="12" num="4.1"><w n="12.1">Il</w> <w n="12.2">passe</w> <w n="12.3">avec</w> <w n="12.4">lenteur</w> <w n="12.5">ses</w> <w n="12.6">doigts</w> <w n="12.7">fins</w> <w n="12.8">à</w> <w n="12.9">son</w> <w n="12.10">cou</w>,</l>
							<l n="13" num="4.2"><w n="13.1">Rêve</w> <w n="13.2">sur</w> <w n="13.3">l</w>’<w n="13.4">homme</w>, <w n="13.5">songe</w> <w n="13.6">à</w> <w n="13.7">l</w>’<w n="13.8">œuvre</w> <w n="13.9">commencée</w>,</l>
							<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">sa</w> <w n="14.3">tête</w> <w n="14.4">aux</w> <w n="14.5">yeux</w> <w n="14.6">clairs</w> <w n="14.7">tombe</w> <w n="14.8">au</w> <w n="14.9">panier</w>, <w n="14.10">d</w>’<w n="14.11">un</w> <w n="14.12">coup</w>.</l>
						</lg>
					</div></body></text></TEI>