<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES CULTES</head><div type="poem" key="HAR53">
						<head type="main">LE VASE</head>
						<opener>
							<salute>À JOSEPH VILLENEUVE</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Superbe</w> <w n="1.2">et</w> <w n="1.3">pur</w>, <w n="1.4">le</w> <w n="1.5">Monde</w> <w n="1.6">est</w> <w n="1.7">un</w> <w n="1.8">vase</w> <w n="1.9">d</w>’<w n="1.10">argent</w></l>
							<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">l</w>’<w n="2.3">art</w> <w n="2.4">a</w> <w n="2.5">ciselé</w> <w n="2.6">des</w> <w n="2.7">formes</w> <w n="2.8">long</w> <w n="2.9">vêtues</w></l>
							<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">dansent</w> <w n="3.3">deux</w> <w n="3.4">à</w> <w n="3.5">deux</w> <w n="3.6">sur</w> <w n="3.7">un</w> <w n="3.8">rythme</w> <w n="3.9">changeant</w>.</l>
						</lg>
						<lg n="2">
							<l n="4" num="2.1"><w n="4.1">Les</w> <w n="4.2">aèdes</w>, <w n="4.3">autour</w> <w n="4.4">des</w> <w n="4.5">antiques</w> <w n="4.6">statues</w>,</l>
							<l n="5" num="2.2"><w n="5.1">Chantent</w> <w n="5.2">en</w> <w n="5.3">vers</w> <w n="5.4">pieux</w> <w n="5.5">la</w> <w n="5.6">gloire</w> <w n="5.7">des</w> <w n="5.8">guerriers</w>,</l>
							<l n="6" num="2.3"><w n="6.1">Et</w> <w n="6.2">font</w> <w n="6.3">sauter</w> <w n="6.4">le</w> <w n="6.5">plectre</w> <w n="6.6">aux</w> <w n="6.7">cordes</w> <w n="6.8">des</w> <w n="6.9">tortues</w>.</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1"><w n="7.1">Les</w> <w n="7.2">vierges</w> <w n="7.3">aux</w> <w n="7.4">bras</w> <w n="7.5">fins</w> <w n="7.6">glissent</w> <w n="7.7">sous</w> <w n="7.8">les</w> <w n="7.9">mûriers</w>,</l>
							<l n="8" num="3.2"><w n="8.1">Et</w>, <w n="8.2">remplissant</w> <w n="8.3">de</w> <w n="8.4">fleurs</w> <w n="8.5">le</w> <w n="8.6">treillis</w> <w n="8.7">d</w>’<w n="8.8">or</w> <w n="8.9">des</w> <w n="8.10">cistes</w>,</l>
							<l n="9" num="3.3"><w n="9.1">Mêlent</w> <w n="9.2">le</w> <w n="9.3">myrte</w> <w n="9.4">pâle</w> <w n="9.5">aux</w> <w n="9.6">roses</w> <w n="9.7">des</w> <w n="9.8">lauriers</w>.</l>
						</lg>
						<lg n="4">
							<l n="10" num="4.1"><w n="10.1">Les</w> <w n="10.2">éphèbes</w>, <w n="10.3">debout</w> <w n="10.4">près</w> <w n="10.5">des</w> <w n="10.6">fûts</w> <w n="10.7">ronds</w> <w n="10.8">des</w> <w n="10.9">xystes</w>,</l>
							<l n="11" num="4.2"><w n="11.1">S</w>’<w n="11.2">écartent</w> <w n="11.3">pour</w> <w n="11.4">livrer</w> <w n="11.5">passage</w> <w n="11.6">aux</w> <w n="11.7">vieillards</w> <w n="11.8">lents</w>,</l>
							<l n="12" num="4.3"><w n="12.1">Qui</w> <w n="12.2">s</w>’<w n="12.3">en</w> <w n="12.4">vont</w>, <w n="12.5">les</w> <w n="12.6">pieds</w> <w n="12.7">lourds</w>, <w n="12.8">le</w> <w n="12.9">front</w> <w n="12.10">bas</w>, <w n="12.11">les</w> <w n="12.12">yeux</w> <w n="12.13">tristes</w>.</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1"><w n="13.1">Sous</w> <w n="13.2">l</w>’<w n="13.3">autel</w> <w n="13.4">de</w> <w n="13.5">Vénus</w>, <w n="13.6">les</w> <w n="13.7">femmes</w> <w n="13.8">aux</w> <w n="13.9">beaux</w> <w n="13.10">flancs</w></l>
							<l n="14" num="5.2"><w n="14.1">S</w>’<w n="14.2">arrêtent</w>, <w n="14.3">présentant</w> <w n="14.4">les</w> <w n="14.5">couples</w> <w n="14.6">de</w> <w n="14.7">colombes</w></l>
							<l n="15" num="5.3"><w n="15.1">Qui</w> <w n="15.2">palpitent</w>, <w n="15.3">frileux</w>, <w n="15.4">dans</w> <w n="15.5">le</w> <w n="15.6">nid</w> <w n="15.7">des</w> <w n="15.8">seins</w> <w n="15.9">blancs</w>.</l>
						</lg>
						<lg n="6">
							<l n="16" num="6.1"><w n="16.1">Les</w> <w n="16.2">prêtres</w> <w n="16.3">vénérés</w> <w n="16.4">parent</w> <w n="16.5">les</w> <w n="16.6">hécatombes</w>,</l>
							<l n="17" num="6.2"><w n="17.1">Tandis</w> <w n="17.2">que</w>, <w n="17.3">prosternant</w> <w n="17.4">au</w> <w n="17.5">loin</w> <w n="17.6">leurs</w> <w n="17.7">chers</w> <w n="17.8">regrets</w>,</l>
							<l n="18" num="6.3"><w n="18.1">Les</w> <w n="18.2">veuves</w> <w n="18.3">en</w> <w n="18.4">longs</w> <w n="18.5">deuils</w> <w n="18.6">pleurent</w> <w n="18.7">au</w> <w n="18.8">bord</w> <w n="18.9">des</w> <w n="18.10">tombes</w>.</l>
						</lg>
						<lg n="7">
							<l n="19" num="7.1"><w n="19.1">Des</w> <w n="19.2">groupes</w> <w n="19.3">enlacés</w> <w n="19.4">montent</w> <w n="19.5">vers</w> <w n="19.6">les</w> <w n="19.7">forêts</w> :</l>
							<l n="20" num="7.2"><w n="20.1">Les</w> <w n="20.2">lèvres</w> <w n="20.3">des</w> <w n="20.4">amants</w> <w n="20.5">ont</w> <w n="20.6">des</w> <w n="20.7">rires</w> <w n="20.8">d</w>’<w n="20.9">extase</w></l>
							<l n="21" num="7.3"><w n="21.1">Qui</w> <w n="21.2">font</w> <w n="21.3">fuir</w> <w n="21.4">l</w>’<w n="21.5">albe</w> <w n="21.6">lune</w> <w n="21.7">au</w> <w n="21.8">fond</w> <w n="21.9">des</w> <w n="21.10">cieux</w> <w n="21.11">discrets</w>.</l>
						</lg>
						<lg n="8">
							<l n="22" num="8.1">— <w n="22.1">Mais</w> <w n="22.2">le</w> <w n="22.3">cœur</w> <w n="22.4">ténébreux</w> <w n="22.5">de</w> <w n="22.6">l</w>’<w n="22.7">urne</w> <w n="22.8">est</w> <w n="22.9">plein</w> <w n="22.10">de</w> <w n="22.11">vase</w>,</l>
							<l n="23" num="8.2"><w n="23.1">Et</w> <w n="23.2">par</w>-<w n="23.3">dessus</w> <w n="23.4">le</w> <w n="23.5">pampre</w> <w n="23.6">et</w> <w n="23.7">les</w> <w n="23.8">volubilis</w></l>
							<l n="24" num="8.3"><w n="24.1">Qui</w> <w n="24.2">rampent</w> <w n="24.3">sveltement</w> <w n="24.4">sur</w> <w n="24.5">les</w> <w n="24.6">marges</w> <w n="24.7">du</w> <w n="24.8">vase</w>,</l>
						</lg>
						<lg n="9">
							<l n="25" num="9.1"><w n="25.1">Le</w> <w n="25.2">Mensonge</w> <w n="25.3">fleurit</w>, <w n="25.4">calme</w> <w n="25.5">et</w> <w n="25.6">blanc</w>, <w n="25.7">comme</w> <w n="25.8">un</w> <w n="25.9">lis</w>.</l>
						</lg>
					</div></body></text></TEI>