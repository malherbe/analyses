<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><head type="main_part">LA VIE EXTÉRIEURE</head><head type="sub_part">LES LOIS — LES CULTES — LES FORMES</head><head type="main_subpart">LES LOIS</head><div type="poem" key="HAR47">
						<head type="main">CLAIR DE LUNE</head>
						<opener>
							<salute>À ÉMILE GITER</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Jadis</w>, <w n="1.2">aux</w> <w n="1.3">jours</w> <w n="1.4">du</w> <w n="1.5">Feu</w>, <w n="1.6">quand</w> <w n="1.7">la</w> <w n="1.8">Terre</w>, <w n="1.9">en</w> <w n="1.10">hurlant</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Roulait</w> <w n="2.2">son</w> <w n="2.3">bloc</w> <w n="2.4">fluide</w> <w n="2.5">à</w> <w n="2.6">travers</w> <w n="2.7">le</w> <w n="2.8">ciel</w> <w n="2.9">blanc</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Elle</w> <w n="3.2">enfla</w> <w n="3.3">par</w> <w n="3.4">degrés</w> <w n="3.5">sa</w> <w n="3.6">courbe</w> <w n="3.7">originelle</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Puis</w>, <w n="4.2">dans</w> <w n="4.3">un</w> <w n="4.4">vaste</w> <w n="4.5">effort</w>, <w n="4.6">creva</w> <w n="4.7">ses</w> <w n="4.8">flancs</w> <w n="4.9">ignés</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">lança</w>, <w n="5.3">vers</w> <w n="5.4">le</w> <w n="5.5">flux</w> <w n="5.6">des</w> <w n="5.7">mondes</w> <w n="5.8">déjà</w> <w n="5.9">nés</w>,</l>
							<l n="6" num="1.6"><space unit="char" quantity="8"></space> <w n="6.1">La</w> <w n="6.2">Lune</w> <w n="6.3">qui</w> <w n="6.4">germait</w> <w n="6.5">en</w> <w n="6.6">elle</w>.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><w n="7.1">Alors</w>, <w n="7.2">dans</w> <w n="7.3">la</w> <w n="7.4">splendeur</w> <w n="7.5">des</w> <w n="7.6">siècles</w> <w n="7.7">éclatants</w>,</l>
							<l n="8" num="2.2"><w n="8.1">Sans</w> <w n="8.2">relâche</w>, <w n="8.3">sans</w> <w n="8.4">fin</w>, <w n="8.5">à</w> <w n="8.6">toute</w> <w n="8.7">heure</w> <w n="8.8">du</w> <w n="8.9">temps</w>,</l>
							<l n="9" num="2.3"><w n="9.1">La</w> <w n="9.2">mère</w>, <w n="9.3">ivre</w> <w n="9.4">d</w>’<w n="9.5">amour</w>, <w n="9.6">contemplait</w> <w n="9.7">dans</w> <w n="9.8">sa</w> <w n="9.9">force</w></l>
							<l n="10" num="2.4"><w n="10.1">L</w>’<w n="10.2">astre</w> <w n="10.3">enfant</w> <w n="10.4">qui</w> <w n="10.5">courait</w> <w n="10.6">comme</w> <w n="10.7">un</w> <w n="10.8">jeune</w> <w n="10.9">soleil</w> :</l>
							<l n="11" num="2.5"><w n="11.1">Il</w> <w n="11.2">flambait</w>. <w n="11.3">Un</w> <w n="11.4">froid</w> <w n="11.5">vint</w> <w n="11.6">l</w>’<w n="11.7">engourdir</w> <w n="11.8">de</w> <w n="11.9">sommeil</w></l>
							<l n="12" num="2.6"><space unit="char" quantity="8"></space>  <w n="12.1">Et</w> <w n="12.2">pétrifia</w> <w n="12.3">son</w> <w n="12.4">écorce</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">Puis</w>, <w n="13.2">ce</w> <w n="13.3">fut</w> <w n="13.4">l</w>’<w n="13.5">âge</w> <w n="13.6">blond</w> <w n="13.7">des</w> <w n="13.8">tiédeurs</w> <w n="13.9">et</w> <w n="13.10">des</w> <w n="13.11">vents</w> :</l>
							<l n="14" num="3.2"><w n="14.1">La</w> <w n="14.2">Lune</w> <w n="14.3">se</w> <w n="14.4">peupla</w> <w n="14.5">de</w> <w n="14.6">murmures</w> <w n="14.7">vivants</w> ;</l>
							<l n="15" num="3.3"><w n="15.1">Elle</w> <w n="15.2">eut</w> <w n="15.3">des</w> <w n="15.4">mers</w> <w n="15.5">sans</w> <w n="15.6">fond</w> <w n="15.7">et</w> <w n="15.8">des</w> <w n="15.9">fleuves</w> <w n="15.10">sans</w> <w n="15.11">nombre</w>,</l>
							<l n="16" num="3.4"><w n="16.1">Des</w> <w n="16.2">troupeaux</w>, <w n="16.3">des</w> <w n="16.4">cités</w>, <w n="16.5">des</w> <w n="16.6">pleurs</w>, <w n="16.7">des</w> <w n="16.8">cris</w> <w n="16.9">joyeux</w> ;</l>
							<l n="17" num="3.5"><w n="17.1">Elle</w> <w n="17.2">eut</w> <w n="17.3">l</w>’<w n="17.4">amour</w> ; <w n="17.5">elle</w> <w n="17.6">eut</w> <w n="17.7">ses</w> <w n="17.8">arts</w>, <w n="17.9">ses</w> <w n="17.10">lois</w>, <w n="17.11">ses</w> <w n="17.12">dieux</w>,</l>
							<l n="18" num="3.6"><space unit="char" quantity="8"></space> <w n="18.1">Et</w>, <w n="18.2">lentement</w>, <w n="18.3">rentra</w> <w n="18.4">dans</w> <w n="18.5">l</w>’<w n="18.6">ombre</w>.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1"><w n="19.1">Depuis</w>, <w n="19.2">rien</w> <w n="19.3">ne</w> <w n="19.4">sent</w> <w n="19.5">plus</w> <w n="19.6">son</w> <w n="19.7">baiser</w> <w n="19.8">jeune</w> <w n="19.9">et</w> <w n="19.10">chaud</w> ;</l>
							<l n="20" num="4.2"><w n="20.1">La</w> <w n="20.2">Terre</w> <w n="20.3">qui</w> <w n="20.4">vieillit</w> <w n="20.5">la</w> <w n="20.6">cherche</w> <w n="20.7">encor</w> <w n="20.8">là</w>-<w n="20.9">haut</w> :</l>
							<l n="21" num="4.3"><w n="21.1">Tout</w> <w n="21.2">est</w> <w n="21.3">nu</w>. <w n="21.4">Mais</w>, <w n="21.5">le</w> <w n="21.6">soir</w>, <w n="21.7">passe</w> <w n="21.8">un</w> <w n="21.9">globe</w> <w n="21.10">éphémère</w>,</l>
							<l n="22" num="4.4"><w n="22.1">Et</w> <w n="22.2">l</w>’<w n="22.3">on</w> <w n="22.4">dirait</w>, <w n="22.5">à</w> <w n="22.6">voir</w> <w n="22.7">sa</w> <w n="22.8">forme</w> <w n="22.9">errer</w> <w n="22.10">sans</w> <w n="22.11">bruit</w>,</l>
							<l n="23" num="4.5"><w n="23.1">L</w>’<w n="23.2">âme</w> <w n="23.3">d</w>’<w n="23.4">un</w> <w n="23.5">enfant</w> <w n="23.6">mort</w> <w n="23.7">qui</w> <w n="23.8">reviendrait</w> <w n="23.9">la</w> <w n="23.10">nuit</w></l>
							<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">Pour</w> <w n="24.2">regarder</w> <w n="24.3">dormir</w> <w n="24.4">sa</w> <w n="24.5">mère</w>.</l>
						</lg>
					</div></body></text></TEI>