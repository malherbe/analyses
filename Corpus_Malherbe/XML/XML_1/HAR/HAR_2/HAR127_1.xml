<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">L’Âme nue</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2991 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">L’Âme nue</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/L%E2%80%99%C3%82me_nue</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>L’Âme nue</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https://fr.wikisource.org/w/index.php?title=Fichier:Haraucourt_-_L%E2%80%99%C3%82me_nue,_1885.djvu</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>G. CHARPENTIER ET Cie, ÉDITEURS</publisher>
									<date when="1885">1885</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1885">1885</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
				<p>Les poèmes manquants ont été ajoutés à partir de la page d’édition du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-10-29" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><head type="main_part">LA VIE INTÉRIEURE</head><head type="sub_part">L’AUBE — MIDI — LE SOIR</head><head type="main_subpart">MIDI</head><div type="poem" key="HAR127">
						<head type="main">CHANAAN</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Ils</w> <w n="1.2">sont</w> <w n="1.3">vécus</w>, <w n="1.4">les</w> <w n="1.5">jours</w> <w n="1.6">où</w> <w n="1.7">mes</w> <w n="1.8">désirs</w> <w n="1.9">nomades</w></l>
							<l n="2" num="1.2"><w n="2.1">Couraient</w> <w n="2.2">allègrement</w> <w n="2.3">de</w> <w n="2.4">lointain</w> <w n="2.5">en</w> <w n="2.6">lointain</w></l>
							<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">s</w>’<w n="3.3">arrêtaient</w> <w n="3.4">le</w> <w n="3.5">soir</w> <w n="3.6">pour</w> <w n="3.7">cueillir</w> <w n="3.8">des</w> <w n="3.9">grenades</w></l>
							<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">fond</w> <w n="4.3">des</w> <w n="4.4">oasis</w> <w n="4.5">qu</w>’<w n="4.6">ils</w> <w n="4.7">rêvaient</w> <w n="4.8">au</w> <w n="4.9">matin</w>…</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Ah</w> ! <w n="5.2">les</w> <w n="5.3">fruits</w> <w n="5.4">savoureux</w> <w n="5.5">qu</w>’<w n="5.6">on</w> <w n="5.7">prend</w> <w n="5.8">à</w> <w n="5.9">mains</w> <w n="5.10">câlines</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">haltes</w> <w n="6.3">de</w> <w n="6.4">hasard</w> <w n="6.5">sur</w> <w n="6.6">la</w> <w n="6.7">croupe</w> <w n="6.8">des</w> <w n="6.9">monts</w> !</l>
							<l n="7" num="2.3"><w n="7.1">Ah</w> ! <w n="7.2">les</w> <w n="7.3">côtes</w>, <w n="7.4">les</w> <w n="7.5">cols</w>, <w n="7.6">les</w> <w n="7.7">flancs</w> <w n="7.8">ronds</w> <w n="7.9">des</w> <w n="7.10">collines</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">les</w> <w n="8.3">grèves</w>, <w n="8.4">parmi</w> <w n="8.5">l</w>’<w n="8.6">odeur</w> <w n="8.7">des</w> <w n="8.8">goémons</w> !</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Plus</w> <w n="9.2">de</w> <w n="9.3">sommeil</w> <w n="9.4">conquis</w> <w n="9.5">près</w> <w n="9.6">des</w> <w n="9.7">gorges</w> <w n="9.8">conquises</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Plus</w> <w n="10.2">d</w>’<w n="10.3">alanguissement</w> <w n="10.4">sous</w> <w n="10.5">l</w>’<w n="10.6">ombre</w> <w n="10.7">des</w> <w n="10.8">forêts</w> ;</l>
							<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">c</w>’<w n="11.3">est</w> <w n="11.4">fini</w> <w n="11.5">du</w> <w n="11.6">rire</w> <w n="11.7">et</w> <w n="11.8">des</w> <w n="11.9">chansons</w> <w n="11.10">exquises</w></l>
							<l n="12" num="3.4"><w n="12.1">Qu</w>’<w n="12.2">entonnaient</w> <w n="12.3">au</w> <w n="12.4">réveil</w> <w n="12.5">les</w> <w n="12.6">départs</w> <w n="12.7">sans</w> <w n="12.8">regrets</w>…</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">caravane</w> <w n="13.3">errante</w> <w n="13.4">a</w> <w n="13.5">trouvé</w> <w n="13.6">sa</w> <w n="13.7">patrie</w> :</l>
							<l n="14" num="4.2"><w n="14.1">Éden</w> <w n="14.2">des</w> <w n="14.3">derniers</w> <w n="14.4">jours</w> <w n="14.5">et</w> <w n="14.6">des</w> <w n="14.7">premiers</w> <w n="14.8">repos</w>,</l>
							<l n="15" num="4.3"><w n="15.1">Où</w>, <w n="15.2">quand</w> <w n="15.3">tombe</w> <w n="15.4">le</w> <w n="15.5">soir</w> <w n="15.6">sur</w> <w n="15.7">la</w> <w n="15.8">mousse</w> <w n="15.9">fleurie</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Les</w> <w n="16.2">grands</w> <w n="16.3">baisers</w> <w n="16.4">puissants</w> <w n="16.5">se</w> <w n="16.6">couchent</w> <w n="16.7">par</w> <w n="16.8">troupeaux</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Vallon</w> <w n="17.2">tiède</w> <w n="17.3">et</w> <w n="17.4">lascif</w>, <w n="17.5">parc</w> <w n="17.6">et</w> <w n="17.7">berceaux</w> <w n="17.8">d</w>’<w n="17.9">Armide</w> ;</l>
							<l n="18" num="5.2"><w n="18.1">Empire</w> <w n="18.2">extasié</w> <w n="18.3">des</w> <w n="18.4">fleurs</w> <w n="18.5">et</w> <w n="18.6">des</w> <w n="18.7">oiseaux</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Où</w> <w n="19.2">l</w>’<w n="19.3">on</w> <w n="19.4">entend</w>, <w n="19.5">la</w> <w n="19.6">nuit</w>, <w n="19.7">glisser</w> <w n="19.8">dans</w> <w n="19.9">l</w>’<w n="19.10">air</w> <w n="19.11">humide</w></l>
							<l n="20" num="5.4"><w n="20.1">Le</w> <w n="20.2">gazouillis</w> <w n="20.3">léger</w> <w n="20.4">des</w> <w n="20.5">eaux</w> <w n="20.6">sous</w> <w n="20.7">les</w> <w n="20.8">roseaux</w>…</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Jardin</w> <w n="21.2">universel</w>, <w n="21.3">charme</w> <w n="21.4">total</w> <w n="21.5">du</w> <w n="21.6">monde</w> ;</l>
							<l n="22" num="6.2"><w n="22.1">Sérail</w> <w n="22.2">de</w> <w n="22.3">voluptés</w> <w n="22.4">changeantes</w>, <w n="22.5">où</w> <w n="22.6">l</w>’<w n="22.7">amour</w></l>
							<l n="23" num="6.3"><w n="23.1">Mêle</w> <w n="23.2">le</w> <w n="23.3">baiser</w> <w n="23.4">brun</w> <w n="23.5">et</w> <w n="23.6">la</w> <w n="23.7">caresse</w> <w n="23.8">blonde</w>,</l>
							<l n="24" num="6.4"><w n="24.1">Tour</w> <w n="24.2">à</w> <w n="24.3">tour</w>, <w n="24.4">et</w> <w n="24.5">sans</w> <w n="24.6">fin</w> <w n="24.7">ni</w> <w n="24.8">trêve</w>, <w n="24.9">et</w> <w n="24.10">tour</w> <w n="24.11">à</w> <w n="24.12">tour</w>.</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Paysage</w> <w n="25.2">alterné</w> <w n="25.3">du</w> <w n="25.4">Pôle</w> <w n="25.5">et</w> <w n="25.6">du</w> <w n="25.7">Tropique</w></l>
							<l n="26" num="7.2"><w n="26.1">Où</w> <w n="26.2">toutes</w> <w n="26.3">les</w> <w n="26.4">ardeurs</w> <w n="26.5">suivent</w> <w n="26.6">tous</w> <w n="26.7">les</w> <w n="26.8">frimas</w> ;</l>
							<l n="27" num="7.3"><w n="27.1">Golfe</w> <w n="27.2">où</w> <w n="27.3">l</w>’<w n="27.4">âme</w> <w n="27.5">s</w>’<w n="27.6">endort</w>, <w n="27.7">sous</w> <w n="27.8">un</w> <w n="27.9">vent</w> <w n="27.10">balsamique</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Dans</w> <w n="28.2">la</w> <w n="28.3">chanson</w> <w n="28.4">des</w> <w n="28.5">flots</w> <w n="28.6">et</w> <w n="28.7">le</w> <w n="28.8">roulis</w> <w n="28.9">des</w> <w n="28.10">mâts</w>.</l>
						</lg>
						<lg n="8">
							<l n="29" num="8.1"><w n="29.1">Des</w> <w n="29.2">piments</w> <w n="29.3">et</w> <w n="29.4">des</w> <w n="29.5">lis</w>, <w n="29.6">des</w> <w n="29.7">menthes</w> <w n="29.8">et</w> <w n="29.9">des</w> <w n="29.10">mauves</w> ;</l>
							<l n="30" num="8.2"><w n="30.1">La</w> <w n="30.2">ferveur</w> <w n="30.3">des</w> <w n="30.4">Simouns</w>, <w n="30.5">la</w> <w n="30.6">fraîcheur</w> <w n="30.7">des</w> <w n="30.8">caveaux</w> ;</l>
							<l n="31" num="8.3"><w n="31.1">La</w> <w n="31.2">grâce</w> <w n="31.3">des</w> <w n="31.4">serpents</w> <w n="31.5">et</w> <w n="31.6">la</w> <w n="31.7">fierté</w> <w n="31.8">des</w> <w n="31.9">fauves</w>,</l>
							<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">le</w> <w n="32.3">rire</w> <w n="32.4">incessant</w> <w n="32.5">des</w> <w n="32.6">ciels</w> <w n="32.7">toujours</w> <w n="32.8">nouveaux</w>…</l>
						</lg>
						<lg n="9">
							<l n="33" num="9.1"><w n="33.1">Ô</w> <w n="33.2">Seule</w> ! <w n="33.3">Ô</w> <w n="33.4">Chanaan</w> ! <w n="33.5">Terre</w> <w n="33.6">des</w> <w n="33.7">aromates</w> !</l>
							<l n="34" num="9.2"><w n="34.1">J</w>’<w n="34.2">allais</w> : <w n="34.3">tu</w> <w n="34.4">m</w>’<w n="34.5">apparus</w> <w n="34.6">dans</w> <w n="34.7">le</w> <w n="34.8">reflet</w> <w n="34.9">vermeil</w></l>
							<l n="35" num="9.3"><w n="35.1">Dont</w> <w n="35.2">tes</w> <w n="35.3">contours</w> <w n="35.4">vibrants</w> <w n="35.5">doraient</w> <w n="35.6">leurs</w> <w n="35.7">splendeurs</w> <w n="35.8">mates</w>,</l>
							<l n="36" num="9.4"><w n="36.1">Comme</w> <w n="36.2">une</w> <w n="36.3">île</w> <w n="36.4">de</w> <w n="36.5">marbre</w> <w n="36.6">au</w> <w n="36.7">coucher</w> <w n="36.8">du</w> <w n="36.9">soleil</w>.</l>
						</lg>
						<lg n="10">
							<l n="37" num="10.1"><w n="37.1">Et</w> <w n="37.2">je</w> <w n="37.3">te</w> <w n="37.4">reconnus</w>, <w n="37.5">Femme</w>, <w n="37.6">sans</w> <w n="37.7">t</w>’<w n="37.8">avoir</w> <w n="37.9">vue</w>,</l>
							<l n="38" num="10.2"><w n="38.1">Toi</w> <w n="38.2">qui</w> <w n="38.3">devais</w> <w n="38.4">courber</w> <w n="38.5">mes</w> <w n="38.6">rêves</w> <w n="38.7">sous</w> <w n="38.8">ta</w> <w n="38.9">loi</w>,</l>
							<l n="39" num="10.3"><w n="39.1">Verser</w> <w n="39.2">à</w> <w n="39.3">tous</w> <w n="39.4">mes</w> <w n="39.5">sens</w> <w n="39.6">l</w>’<w n="39.7">ivresse</w> <w n="39.8">jamais</w> <w n="39.9">bue</w>,</l>
							<l n="40" num="10.4"><w n="40.1">Et</w> <w n="40.2">dépeupler</w> <w n="40.3">mes</w> <w n="40.4">cieux</w> <w n="40.5">pour</w> <w n="40.6">les</w> <w n="40.7">peupler</w> <w n="40.8">de</w> <w n="40.9">toi</w> !</l>
						</lg>
						<lg n="11">
							<l n="41" num="11.1"><w n="41.1">Passent</w> <w n="41.2">les</w> <w n="41.3">jours</w> ! <w n="41.4">À</w> <w n="41.5">tous</w> <w n="41.6">les</w> <w n="41.7">temps</w>, <w n="41.8">je</w> <w n="41.9">suis</w> <w n="41.10">ta</w> <w n="41.11">chose</w> ;</l>
							<l n="42" num="11.2"><w n="42.1">Et</w> <w n="42.2">partout</w> <w n="42.3">je</w> <w n="42.4">vois</w> <w n="42.5">luire</w>, <w n="42.6">à</w> <w n="42.7">travers</w> <w n="42.8">mes</w> <w n="42.9">ennuis</w>,</l>
							<l n="43" num="11.3"><w n="43.1">Ton</w> <w n="43.2">œil</w> <w n="43.3">noir</w> <w n="43.4">au</w> <w n="43.5">feu</w> <w n="43.6">rouge</w> <w n="43.7">et</w> <w n="43.8">ta</w> <w n="43.9">lèvre</w> <w n="43.10">au</w> <w n="43.11">feu</w> <w n="43.12">rose</w>,</l>
							<l n="44" num="11.4"><w n="44.1">Comme</w> <w n="44.2">un</w> <w n="44.3">phare</w> <w n="44.4">allumé</w> <w n="44.5">sur</w> <w n="44.6">l</w>’<w n="44.7">océan</w> <w n="44.8">des</w> <w n="44.9">nuits</w> !</l>
						</lg>
					</div></body></text></TEI>