<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La Légende des Sexes</title>
				<title type="sub">Poëmes Hystériques</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https ://fr.wikisource.org/wiki/La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Légende des sexes, poëmes hystériques</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https ://fr.wikisource.org/wiki/Fichier :Haraucourt_-_La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques,_1882.djvu</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimé à Bruxelles pour l’auteur</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/btv1b8618380c</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>ÉDITION PRIVILE, REVUE PAR L’AUTEUR</publisher>
							<date when="1893">1893</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1882">1882</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-28" who="RR">Une correction introduite à partir de l’édition imprimée de 1893</change>
				<change when="2017-10-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La légende des sexes</head><div type="poem" key="HAR40">
					<head type="main">ENVOI</head>
					<opener>
						<salute>A Gaston Béthune.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Frère</w>, <w n="1.2">le</w> <w n="1.3">plus</w> <w n="1.4">aimé</w> <w n="1.5">de</w> <w n="1.6">mes</w> <w n="1.7">plus</w> <w n="1.8">chers</w> <w n="1.9">amis</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Esprit</w> <w n="2.2">vibrant</w> <w n="2.3">et</w> <w n="2.4">souple</w> <w n="2.5">où</w> <w n="2.6">la</w> <w n="2.7">nature</w> <w n="2.8">a</w> <w n="2.9">mis</w></l>
						<l n="3" num="1.3"><w n="3.1">Des</w> <w n="3.2">grandeurs</w> <w n="3.3">de</w> <w n="3.4">poëte</w> <w n="3.5">et</w> <w n="3.6">des</w> <w n="3.7">douceurs</w> <w n="3.8">de</w> <w n="3.9">femme</w> ;</l>
					</lg>
					<lg n="2">
						<l n="4" num="2.1"><w n="4.1">Toi</w> <w n="4.2">qui</w> <w n="4.3">me</w> <w n="4.4">consolais</w> <w n="4.5">dans</w> <w n="4.6">mes</w> <w n="4.7">jours</w> <w n="4.8">de</w> <w n="4.9">rancœur</w> ;</l>
						<l n="5" num="2.2"><w n="5.1">Qui</w> <w n="5.2">réchauffais</w> <w n="5.3">mon</w> <w n="5.4">âme</w> <w n="5.5">aux</w> <w n="5.6">chaleurs</w> <w n="5.7">de</w> <w n="5.8">ton</w> <w n="5.9">âme</w></l>
						<l n="6" num="2.3"><w n="6.1">Quand</w> <w n="6.2">le</w> <w n="6.3">dégoût</w> <w n="6.4">d</w>’<w n="6.5">être</w> <w n="6.6">homme</w> <w n="6.7">humiliait</w> <w n="6.8">mon</w> <w n="6.9">cœur</w> ;</l>
					</lg>
					<lg n="3">
						<l n="7" num="3.1"><w n="7.1">Toi</w> <w n="7.2">qui</w> <w n="7.3">lis</w> <w n="7.4">dans</w> <w n="7.5">ma</w> <w n="7.6">vie</w>, <w n="7.7">et</w> <w n="7.8">qui</w> <w n="7.9">sauras</w> <w n="7.10">peut</w>-<w n="7.11">être</w></l>
						<l n="8" num="3.2"><w n="8.1">Le</w> <w n="8.2">lourd</w> <w n="8.3">secret</w> <w n="8.4">que</w> <w n="8.5">nul</w> <w n="8.6">ne</w> <w n="8.7">doit</w> <w n="8.8">jamais</w> <w n="8.9">connaître</w></l>
						<l n="9" num="3.3"><w n="9.1">Et</w> <w n="9.2">qui</w> <w n="9.3">me</w> <w n="9.4">fait</w> <w n="9.5">pleurer</w>, <w n="9.6">le</w> <w n="9.7">soir</w>, <w n="9.8">comme</w> <w n="9.9">un</w> <w n="9.10">enfant</w> :</l>
					</lg>
					<lg n="4">
						<l n="10" num="4.1"><w n="10.1">En</w> <w n="10.2">souvenir</w> <w n="10.3">de</w> <w n="10.4">moi</w>, <w n="10.5">je</w> <w n="10.6">te</w> <w n="10.7">donne</w> <w n="10.8">ce</w> <w n="10.9">livre</w></l>
						<l n="11" num="4.2"><w n="11.1">Où</w> <w n="11.2">mon</w> <w n="11.3">rut</w> <w n="11.4">exalté</w> <w n="11.5">se</w> <w n="11.6">dresse</w>, <w n="11.7">triomphant</w> ;</l>
						<l n="12" num="4.3"><w n="12.1">Ceux</w> <w n="12.2">qui</w> <w n="12.3">passeront</w> <w n="12.4">là</w> <w n="12.5">pourront</w> <w n="12.6">m</w>’<w n="12.7">entendre</w> <w n="12.8">vivre</w>.</l>
					</lg>
					<lg n="5">
						<l n="13" num="5.1"><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">tiré</w> <w n="13.4">les</w> <w n="13.5">rideaux</w> <w n="13.6">de</w> <w n="13.7">mon</w> <w n="13.8">lit</w>, <w n="13.9">grands</w> <w n="13.10">ouverts</w> ;</l>
						<l n="14" num="5.2"><w n="14.1">Je</w> <w n="14.2">n</w>’<w n="14.3">ai</w> <w n="14.4">honte</w> <w n="14.5">de</w> <w n="14.6">rien</w>, <w n="14.7">et</w> <w n="14.8">je</w> <w n="14.9">crie</w> <w n="14.10">à</w> <w n="14.11">pleins</w> <w n="14.12">vers</w></l>
						<l n="15" num="5.3"><w n="15.1">Quand</w> <w n="15.2">l</w>’<w n="15.3">amour</w> <w n="15.4">bienfaisant</w> <w n="15.5">descend</w> <w n="15.6">sur</w> <w n="15.7">ma</w> <w n="15.8">torture</w>.</l>
					</lg>
					<lg n="6">
						<l n="16" num="6.1"><w n="16.1">Plus</w> <w n="16.2">corrompu</w> <w n="16.3">que</w> <w n="16.4">nous</w>, <w n="16.5">le</w> <w n="16.6">siècle</w> <w n="16.7">n</w>’<w n="16.8">aime</w> <w n="16.9">pas</w></l>
						<l n="17" num="6.2"><w n="17.1">Qu</w>’<w n="17.2">on</w> <w n="17.3">se</w> <w n="17.4">souvienne</w> <w n="17.5">d</w>’<w n="17.6">être</w> <w n="17.7">un</w> <w n="17.8">fils</w> <w n="17.9">de</w> <w n="17.10">la</w> <w n="17.11">nature</w>,</l>
						<l n="18" num="6.3"><w n="18.1">Et</w> <w n="18.2">qu</w>’<w n="18.3">on</w> <w n="18.4">dise</w> <w n="18.5">tout</w> <w n="18.6">haut</w> <w n="18.7">ce</w> <w n="18.8">qu</w>’<w n="18.9">il</w> <w n="18.10">pense</w> <w n="18.11">tout</w> <w n="18.12">bas</w> ;</l>
					</lg>
					<lg n="7">
						<l n="19" num="7.1"><w n="19.1">Il</w> <w n="19.2">veut</w> <w n="19.3">qu</w>’<w n="19.4">on</w> <w n="19.5">soit</w> <w n="19.6">poncif</w> <w n="19.7">et</w> <w n="19.8">qu</w>’<w n="19.9">on</w> <w n="19.10">chante</w> <w n="19.11">les</w> <w n="19.12">roses</w>,</l>
						<l n="20" num="7.2"><w n="20.1">Les</w> <w n="20.2">bois</w>, <w n="20.3">les</w> <w n="20.4">vingt</w> <w n="20.5">printemps</w> <w n="20.6">et</w> <w n="20.7">les</w> <w n="20.8">hivers</w> <w n="20.9">moroses</w> ;</l>
						<l n="21" num="7.3"><w n="21.1">Il</w> <w n="21.2">faut</w> <w n="21.3">rougir</w> <w n="21.4">d</w>’<w n="21.5">être</w> <w n="21.6">homme</w> <w n="21.7">et</w> <w n="21.8">renier</w> <w n="21.9">sa</w> <w n="21.10">chair</w>.</l>
					</lg>
					<lg n="8">
						<l n="22" num="8.1"><w n="22.1">Ah</w>, <w n="22.2">qui</w> <w n="22.3">nous</w> <w n="22.4">rendra</w> <w n="22.5">l</w>’<w n="22.6">âge</w> <w n="22.7">où</w> <w n="22.8">la</w> <w n="22.9">grâce</w> <w n="22.10">était</w> <w n="22.11">nue</w> ?</l>
						<l n="23" num="8.2"><w n="23.1">L</w>’<w n="23.2">âpre</w> <w n="23.3">splendeur</w> <w n="23.4">du</w> <w n="23.5">vrai</w> <w n="23.6">rendait</w> <w n="23.7">le</w> <w n="23.8">beau</w> <w n="23.9">plus</w> <w n="23.10">cher</w>,</l>
						<l n="24" num="8.3"><w n="24.1">Et</w> <w n="24.2">la</w> <w n="24.3">pudeur</w> <w n="24.4">dormait</w>, <w n="24.5">hérésie</w> <w n="24.6">inconnue</w> ;</l>
					</lg>
					<lg n="9">
						<l n="25" num="9.1"><w n="25.1">Tous</w> <w n="25.2">les</w> <w n="25.3">bonheurs</w> <w n="25.4">humains</w> <w n="25.5">s</w>’<w n="25.6">appelaient</w> <w n="25.7">par</w> <w n="25.8">leur</w> <w n="25.9">nom</w>,</l>
						<l n="26" num="9.2"><w n="26.1">Et</w> <w n="26.2">nul</w> <w n="26.3">n</w>’<w n="26.4">aurait</w> <w n="26.5">osé</w> <w n="26.6">trouver</w> <w n="26.7">leur</w> <w n="26.8">culte</w> <w n="26.9">immonde</w>…</l>
						<l n="27" num="9.3">— « <w n="27.1">Tu</w> <w n="27.2">vas</w> <w n="27.3">châtrer</w> <w n="27.4">ton</w> <w n="27.5">art</w>, <w n="27.6">et</w> <w n="27.7">mentir</w>»— <w n="27.8">Eh</w> <w n="27.9">bien</w>, <w n="27.10">non</w> !</l>
					</lg>
					<lg n="10">
						<l n="28" num="10.1"><w n="28.1">Le</w> <w n="28.2">monde</w> <w n="28.3">en</w> <w n="28.4">rugira</w> : <w n="28.5">nous</w> <w n="28.6">méprisons</w> <w n="28.7">le</w> <w n="28.8">monde</w> !</l>
					</lg>
				</div></body></text></TEI>