<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La Légende des Sexes</title>
				<title type="sub">Poëmes Hystériques</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https ://fr.wikisource.org/wiki/La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Légende des sexes, poëmes hystériques</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https ://fr.wikisource.org/wiki/Fichier :Haraucourt_-_La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques,_1882.djvu</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimé à Bruxelles pour l’auteur</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/btv1b8618380c</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>ÉDITION PRIVILE, REVUE PAR L’AUTEUR</publisher>
							<date when="1893">1893</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1882">1882</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-28" who="RR">Une correction introduite à partir de l’édition imprimée de 1893</change>
				<change when="2017-10-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La légende des sexes</head><div type="poem" key="HAR21">
					<head type="main">LE CRÂNE</head>
					<head type="sub_2">Envoi d’une teste de mort à une jeune artiste</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Cy</w>, <w n="1.2">dure</w> <w n="1.3">boëte</w> <w n="1.4">à</w> <w n="1.5">cervelle</w>.</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">ame</w> <w n="2.3">par</w> <w n="2.4">perthuis</w> <w n="2.5">des</w> <w n="2.6">yeux</w> <w n="2.7">ronds</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Dévalant</w> <w n="3.2">vers</w> <w n="3.3">sphère</w> <w n="3.4">nouvelle</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Yssit</w> <w n="4.2">du</w> <w n="4.3">monde</w> <w n="4.4">où</w> <w n="4.5">nous</w> <w n="4.6">entrons</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Se</w> <w n="5.2">fut</w> <w n="5.3">maulvaise</w> <w n="5.4">ou</w> <w n="5.5">se</w> <w n="5.6">fut</w> <w n="5.7">bonne</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Point</w> <w n="6.2">ne</w> <w n="6.3">le</w> <w n="6.4">sçais</w> <w n="6.5">et</w> <w n="6.6">rien</w> <w n="6.7">n</w>’<w n="6.8">en</w> <w n="6.9">dis</w> :</l>
						<l n="7" num="2.3"><w n="7.1">Maulvaise</w>, <w n="7.2">que</w> <w n="7.3">Dieu</w> <w n="7.4">lui</w> <w n="7.5">pardonne</w> ;</l>
						<l n="8" num="2.4"><w n="8.1">Bonne</w>, <w n="8.2">la</w> <w n="8.3">boute</w> <w n="8.4">ès</w> <w n="8.5">paradis</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">seur</w>, <w n="9.3">chez</w> <w n="9.4">vous</w>, <w n="9.5">aura</w> <w n="9.6">martyre</w></l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">chauldes</w> <w n="10.3">tortures</w> <w n="10.4">d</w>’<w n="10.5">Enfer</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Ce</w> <w n="11.2">dit</w> <w n="11.3">chief</w> <w n="11.4">viril</w> <w n="11.5">de</w> <w n="11.6">Satyre</w></l>
						<l n="12" num="3.4"><w n="12.1">Qu</w>’<w n="12.2">esmorcha</w> <w n="12.3">l</w>’<w n="12.4">appétit</w> <w n="12.5">du</w> <w n="12.6">ver</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Crasne</w> <w n="13.2">de</w> <w n="13.3">masle</w> ! <w n="13.4">Vuide</w> <w n="13.5">teste</w> !</l>
						<l n="14" num="4.2"><w n="14.1">Las</w> ! <w n="14.2">Mains</w> <w n="14.3">souëfes</w> <w n="14.4">t</w>’<w n="14.5">arresseront</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Ongles</w> <w n="15.2">rondis</w> <w n="15.3">se</w> <w n="15.4">feront</w> <w n="15.5">feste</w></l>
						<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">s</w>’<w n="16.3">esbaudir</w> <w n="16.4">emmi</w> <w n="16.5">ton</w> <w n="16.6">front</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Ardera</w> <w n="17.2">ta</w> <w n="17.3">cervelle</w> <w n="17.4">absente</w></l>
						<l n="18" num="5.2"><w n="18.1">Au</w> <w n="18.2">tact</w> <w n="18.3">errant</w> <w n="18.4">des</w> <w n="18.5">detz</w> <w n="18.6">rosés</w>,</l>
						<l n="19" num="5.3"><w n="19.1">De</w> <w n="19.2">ne</w> <w n="19.3">pouvoir</w>, <w n="19.4">sans</w> <w n="19.5">qu</w>’<w n="19.6">elle</w> <w n="19.7">y</w> <w n="19.8">sente</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Lui</w> <w n="20.2">vestir</w> <w n="20.3">les</w> <w n="20.4">bras</w> <w n="20.5">de</w> <w n="20.6">baisers</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">la</w> <w n="21.3">nuyct</w>, <w n="21.4">la</w> <w n="21.5">véant</w> <w n="21.6">ès</w> <w n="21.7">plumes</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Sise</w> <w n="22.2">en</w> <w n="22.3">chair</w> <w n="22.4">et</w> <w n="22.5">blanche</w> <w n="22.6">de</w> <w n="22.7">pel</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Tes</w> <w n="23.2">vielz</w> <w n="23.3">os</w>, <w n="23.4">prins</w> <w n="23.5">de</w> <w n="23.6">ruyts</w> <w n="23.7">posthumes</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Pourbondiront</w> <w n="24.2">dans</w> <w n="24.3">leur</w> <w n="24.4">tumbel</w>.</l>
					</lg>
				</div></body></text></TEI>