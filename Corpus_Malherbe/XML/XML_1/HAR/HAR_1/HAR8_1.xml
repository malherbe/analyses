<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La Légende des Sexes</title>
				<title type="sub">Poëmes Hystériques</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https ://fr.wikisource.org/wiki/La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Légende des sexes, poëmes hystériques</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https ://fr.wikisource.org/wiki/Fichier :Haraucourt_-_La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques,_1882.djvu</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimé à Bruxelles pour l’auteur</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/btv1b8618380c</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>ÉDITION PRIVILE, REVUE PAR L’AUTEUR</publisher>
							<date when="1893">1893</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1882">1882</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-28" who="RR">Une correction introduite à partir de l’édition imprimée de 1893</change>
				<change when="2017-10-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La légende des sexes</head><div type="poem" key="HAR8">
					<head type="main">LA FLÛTE</head>
					<head type="form">TRIOLETS</head>
					<opener>
						<salute>À Léopold Allard.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">les</w> <w n="1.3">vers</w> <w n="1.4">m</w>’<w n="1.5">auront</w> <w n="1.6">désossé</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Tout</w> <w n="2.2">nu</w>, <w n="2.3">tout</w> <w n="2.4">sec</w>, <w n="2.5">dans</w> <w n="2.6">mes</w> <w n="2.7">six</w> <w n="2.8">planches</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Fait</w> <w n="3.2">de</w> <w n="3.3">trous</w> <w n="3.4">comme</w> <w n="3.5">un</w> <w n="3.6">bas</w> <w n="3.7">percé</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Quand</w> <w n="4.2">les</w> <w n="4.3">vers</w> <w n="4.4">m</w>’<w n="4.5">auront</w> <w n="4.6">désossé</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Quand</w> <w n="5.2">le</w> <w n="5.3">Temps</w> <w n="5.4">grave</w> <w n="5.5">aura</w> <w n="5.6">lissé</w></l>
						<l n="6" num="1.6"><w n="6.1">Mon</w> <w n="6.2">vieux</w> <w n="6.3">squelette</w> <w n="6.4">aux</w> <w n="6.5">maigreurs</w> <w n="6.6">blanches</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Quand</w> <w n="7.2">les</w> <w n="7.3">vers</w> <w n="7.4">m</w>’<w n="7.5">auront</w> <w n="7.6">désossé</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Tout</w> <w n="8.2">nu</w>, <w n="8.3">tout</w> <w n="8.4">sec</w>, <w n="8.5">dans</w> <w n="8.6">mes</w> <w n="8.7">six</w> <w n="8.8">planches</w> :</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Alors</w>, <w n="9.2">gaiement</w>, <w n="9.3">venez</w> <w n="9.4">me</w> <w n="9.5">voir</w>,</l>
						<l n="10" num="2.2"><w n="10.1">Chœur</w> <w n="10.2">lascif</w> <w n="10.3">des</w> <w n="10.4">vierges</w> <w n="10.5">à</w> <w n="10.6">naître</w></l>
						<l n="11" num="2.3"><w n="11.1">Qui</w> <w n="11.2">vivez</w> <w n="11.3">trop</w> <w n="11.4">tard</w> <w n="11.5">pour</w> <w n="11.6">m</w>’<w n="11.7">avoir</w>…</l>
						<l n="12" num="2.4"><w n="12.1">Alors</w>, <w n="12.2">gaiement</w>, <w n="12.3">venez</w> <w n="12.4">me</w> <w n="12.5">voir</w> !</l>
						<l n="13" num="2.5"><w n="13.1">Vous</w> <w n="13.2">lèverez</w> <w n="13.3">le</w> <w n="13.4">marbre</w> <w n="13.5">noir</w>,</l>
						<l n="14" num="2.6"><w n="14.1">Et</w> <w n="14.2">me</w> <w n="14.3">creusant</w> <w n="14.4">une</w> <w n="14.5">fenêtre</w>,</l>
						<l n="15" num="2.7"><w n="15.1">Alors</w>, <w n="15.2">gaiement</w>, <w n="15.3">venez</w> <w n="15.4">me</w> <w n="15.5">voir</w>,</l>
						<l n="16" num="2.8"><w n="16.1">Chœur</w> <w n="16.2">lascif</w> <w n="16.3">des</w> <w n="16.4">vierges</w> <w n="16.5">à</w> <w n="16.6">naître</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Vous</w> <w n="17.2">chercherez</w> <w n="17.3">parmi</w> <w n="17.4">mes</w> <w n="17.5">os</w></l>
						<l n="18" num="3.2"><w n="18.1">Cet</w> <w n="18.2">os</w> <w n="18.3">viril</w> <w n="18.4">qui</w> <w n="18.5">fut</w> <w n="18.6">mon</w> <w n="18.7">membre</w> ;</l>
						<l n="19" num="3.3"><w n="19.1">Près</w> <w n="19.2">des</w> <w n="19.3">fémurs</w>, <w n="19.4">au</w> <w n="19.5">bas</w> <w n="19.6">du</w> <w n="19.7">dos</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Vous</w> <w n="20.2">chercherez</w> <w n="20.3">parmi</w> <w n="20.4">mes</w> <w n="20.5">os</w>.</l>
						<l n="21" num="3.5"><w n="21.1">Roide</w> <w n="21.2">encore</w> <w n="21.3">dans</w> <w n="21.4">son</w> <w n="21.5">repos</w></l>
						<l n="22" num="3.6"><w n="22.1">Comme</w> <w n="22.2">un</w> <w n="22.3">athlète</w> <w n="22.4">qui</w> <w n="22.5">se</w> <w n="22.6">cambre</w>,</l>
						<l n="23" num="3.7"><w n="23.1">Vous</w> <w n="23.2">chercherez</w> <w n="23.3">parmi</w> <w n="23.4">mes</w> <w n="23.5">os</w></l>
						<l n="24" num="3.8"><w n="24.1">Cet</w> <w n="24.2">os</w> <w n="24.3">viril</w> <w n="24.4">qui</w> <w n="24.5">fut</w> <w n="24.6">mon</w> <w n="24.7">membre</w>.</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Vous</w> <w n="25.2">le</w> <w n="25.3">verrez</w> <w n="25.4">très</w> <w n="25.5">long</w>, <w n="25.6">très</w> <w n="25.7">fort</w>,</l>
						<l n="26" num="4.2"><w n="26.1">Dur</w> <w n="26.2">aux</w> <w n="26.3">contours</w>, <w n="26.4">et</w> <w n="26.5">creux</w> <w n="26.6">au</w> <w n="26.7">centre</w> ;</l>
						<l n="27" num="4.3"><w n="27.1">Veuf</w> <w n="27.2">de</w> <w n="27.3">son</w> <w n="27.4">double</w> <w n="27.5">contrefort</w>,</l>
						<l n="28" num="4.4"><w n="28.1">Vous</w> <w n="28.2">le</w> <w n="28.3">verrez</w> <w n="28.4">très</w> <w n="28.5">long</w>, <w n="28.6">très</w> <w n="28.7">fort</w>,</l>
						<l n="29" num="4.5"><w n="29.1">L</w>’<w n="29.2">os</w> <w n="29.3">vaillant</w> <w n="29.4">qui</w> <w n="29.5">sous</w> <w n="29.6">son</w> <w n="29.7">effort</w></l>
						<l n="30" num="4.6"><w n="30.1">Fora</w> <w n="30.2">tant</w> <w n="30.3">d</w>’<w n="30.4">isthmes</w> <w n="30.5">au</w> <w n="30.6">bas</w> <w n="30.7">ventre</w> :</l>
						<l n="31" num="4.7"><w n="31.1">Vous</w> <w n="31.2">le</w> <w n="31.3">verrez</w> <w n="31.4">très</w> <w n="31.5">long</w>, <w n="31.6">très</w> <w n="31.7">fort</w>,</l>
						<l n="32" num="4.8"><w n="32.1">Dur</w> <w n="32.2">aux</w> <w n="32.3">contours</w>, <w n="32.4">et</w> <w n="32.5">creux</w> <w n="32.6">au</w> <w n="32.7">centre</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Hélas</w>, <w n="33.2">j</w>’<w n="33.3">en</w> <w n="33.4">aurai</w> <w n="33.5">fait</w> <w n="33.6">mon</w> <w n="33.7">deuil</w> :</l>
						<l n="34" num="5.2"><w n="34.1">Emportez</w>-<w n="34.2">le</w>, <w n="34.3">je</w> <w n="34.4">vous</w> <w n="34.5">le</w> <w n="34.6">donne</w>.</l>
						<l n="35" num="5.3"><w n="35.1">Il</w> <w n="35.2">fut</w> <w n="35.3">ma</w> <w n="35.4">force</w> <w n="35.5">et</w> <w n="35.6">mon</w> <w n="35.7">orgueil</w>,</l>
						<l n="36" num="5.4"><w n="36.1">Hélas</w>, <w n="36.2">j</w>’<w n="36.3">en</w> <w n="36.4">aurai</w> <w n="36.5">fait</w> <w n="36.6">mon</w> <w n="36.7">deuil</w> !</l>
						<l n="37" num="5.5"><w n="37.1">Dans</w> <w n="37.2">le</w> <w n="37.3">célibat</w> <w n="37.4">du</w> <w n="37.5">cercueil</w>,</l>
						<l n="38" num="5.6"><w n="38.1">On</w> <w n="38.2">dort</w> <w n="38.3">seul</w>, <w n="38.4">et</w> <w n="38.5">la</w> <w n="38.6">mort</w> <w n="38.7">chaponne</w>…</l>
						<l n="39" num="5.7"><w n="39.1">Hélas</w>, <w n="39.2">j</w>’<w n="39.3">en</w> <w n="39.4">aurai</w> <w n="39.5">fait</w> <w n="39.6">mon</w> <w n="39.7">deuil</w> :</l>
						<l n="40" num="5.8"><w n="40.1">Emportez</w>-<w n="40.2">le</w>, <w n="40.3">je</w> <w n="40.4">vous</w> <w n="40.5">le</w> <w n="40.6">donne</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">Vous</w> <w n="41.2">percerez</w> <w n="41.3">sept</w> <w n="41.4">trous</w>, <w n="41.5">sept</w> <w n="41.6">trous</w>,</l>
						<l n="42" num="6.2"><w n="42.1">Et</w> <w n="42.2">le</w> <w n="42.3">canal</w> <w n="42.4">deviendra</w> <w n="42.5">flûte</w> :</l>
						<l n="43" num="6.3"><w n="43.1">Dans</w> <w n="43.2">l</w>’<w n="43.3">os</w> <w n="43.4">sonore</w> <w n="43.5">aux</w> <w n="43.6">reflets</w> <w n="43.7">roux</w>,</l>
						<l n="44" num="6.4"><w n="44.1">Vous</w> <w n="44.2">percerez</w> <w n="44.3">sept</w> <w n="44.4">trous</w>, <w n="44.5">sept</w> <w n="44.6">trous</w>,</l>
						<l n="45" num="6.5"><w n="45.1">Pour</w> <w n="45.2">accompagner</w> <w n="45.3">les</w> <w n="45.4">froufrous</w></l>
						<l n="46" num="6.6"><w n="46.1">Des</w> <w n="46.2">jupons</w> <w n="46.3">que</w> <w n="46.4">froisse</w> <w n="46.5">la</w> <w n="46.6">lutte</w> :</l>
						<l n="47" num="6.7"><w n="47.1">Vous</w> <w n="47.2">percerez</w> <w n="47.3">sept</w> <w n="47.4">trous</w>, <w n="47.5">sept</w> <w n="47.6">trous</w>,</l>
						<l n="48" num="6.8"><w n="48.1">Et</w> <w n="48.2">le</w> <w n="48.3">canal</w> <w n="48.4">deviendra</w> <w n="48.5">flûte</w>.</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1"><w n="49.1">Sur</w> <w n="49.2">la</w> <w n="49.3">gamme</w> <w n="49.4">des</w> <w n="49.5">baisers</w> <w n="49.6">nus</w></l>
						<l n="50" num="7.2"><w n="50.1">L</w>’<w n="50.2">Amour</w> <w n="50.3">va</w> <w n="50.4">chanter</w> <w n="50.5">sa</w> <w n="50.6">romance</w> :</l>
						<l n="51" num="7.3"><w n="51.1">Souffle</w> <w n="51.2">dans</w> <w n="51.3">ma</w> <w n="51.4">flûte</w>, <w n="51.5">ô</w> <w n="51.6">Vénus</w> !</l>
						<l n="52" num="7.4"><w n="52.1">Sur</w> <w n="52.2">la</w> <w n="52.3">gamme</w> <w n="52.4">des</w> <w n="52.5">baisers</w> <w n="52.6">nus</w></l>
						<l n="53" num="7.5"><w n="53.1">Souffle</w> <w n="53.2">tes</w> <w n="53.3">airs</w> <w n="53.4">les</w> <w n="53.5">plus</w> <w n="53.6">connus</w> :</l>
						<l n="54" num="7.6"><w n="54.1">Voici</w> <w n="54.2">le</w> <w n="54.3">bal</w> <w n="54.4">qui</w> <w n="54.5">recommence</w> :</l>
						<l n="55" num="7.7"><w n="55.1">Sur</w> <w n="55.2">la</w> <w n="55.3">gamme</w> <w n="55.4">des</w> <w n="55.5">baisers</w> <w n="55.6">nus</w></l>
						<l n="56" num="7.8"><w n="56.1">L</w>’<w n="56.2">Amour</w> <w n="56.3">va</w> <w n="56.4">chanter</w> <w n="56.5">sa</w> <w n="56.6">romance</w>.</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1"><w n="57.1">Do</w>, <w n="57.2">ré</w>, <w n="57.3">mi</w>, <w n="57.4">fa</w>, <w n="57.5">sol</w>, <w n="57.6">la</w>, <w n="57.7">si</w>, <w n="57.8">do</w> :</l>
						<l n="58" num="8.2"><w n="58.1">La</w> <w n="58.2">valse</w> <w n="58.3">horizontale</w> <w n="58.4">danse</w>,</l>
						<l n="59" num="8.3"><w n="59.1">Tourne</w>, <w n="59.2">ondule</w> <w n="59.3">sous</w> <w n="59.4">le</w> <w n="59.5">rideau</w> ;</l>
						<l n="60" num="8.4"><w n="60.1">Do</w>, <w n="60.2">ré</w>, <w n="60.3">mi</w>, <w n="60.4">fa</w>, <w n="60.5">sol</w>, <w n="60.6">la</w>, <w n="60.7">si</w>, <w n="60.8">do</w>…</l>
						<l n="61" num="8.5"><w n="61.1">La</w> <w n="61.2">flûte</w> <w n="61.3">suit</w> <w n="61.4">le</w> <w n="61.5">crescendo</w></l>
						<l n="62" num="8.6"><w n="62.1">Et</w> <w n="62.2">rythme</w> <w n="62.3">l</w>’<w n="62.4">amour</w> <w n="62.5">en</w> <w n="62.6">cadence</w>.</l>
						<l n="63" num="8.7"><w n="63.1">Do</w>, <w n="63.2">ré</w>, <w n="63.3">mi</w>, <w n="63.4">fa</w>, <w n="63.5">sol</w>, <w n="63.6">la</w>, <w n="63.7">si</w>, <w n="63.8">do</w> :</l>
						<l n="64" num="8.8"><w n="64.1">La</w> <w n="64.2">valse</w> <w n="64.3">horizontale</w> <w n="64.4">danse</w> !</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1"><w n="65.1">Et</w> <w n="65.2">l</w>’<w n="65.3">os</w> <w n="65.4">vibre</w> <w n="65.5">sous</w> <w n="65.6">le</w> <w n="65.7">baiser</w>,</l>
						<l n="66" num="9.2"><w n="66.1">Au</w> <w n="66.2">souffle</w> <w n="66.3">de</w> <w n="66.4">la</w> <w n="66.5">lèvre</w> <w n="66.6">rose</w> ;</l>
						<l n="67" num="9.3"><w n="67.1">L</w>’<w n="67.2">air</w> <w n="67.3">chaud</w> <w n="67.4">le</w> <w n="67.5">gonfle</w> <w n="67.6">à</w> <w n="67.7">le</w> <w n="67.8">briser</w> !</l>
						<l n="68" num="9.4"><w n="68.1">Et</w> <w n="68.2">l</w>’<w n="68.3">os</w> <w n="68.4">vibre</w> <w n="68.5">sous</w> <w n="68.6">le</w> <w n="68.7">baiser</w></l>
						<l n="69" num="9.5"><w n="69.1">Du</w> <w n="69.2">doigt</w> <w n="69.3">blanc</w> <w n="69.4">qui</w> <w n="69.5">court</w> <w n="69.6">se</w> <w n="69.7">poser</w>,</l>
						<l n="70" num="9.6"><w n="70.1">Va</w>, <w n="70.2">revient</w>, <w n="70.3">remonte</w>, <w n="70.4">et</w> <w n="70.5">se</w> <w n="70.6">pose</w>…</l>
						<l n="71" num="9.7"><w n="71.1">Et</w> <w n="71.2">l</w>’<w n="71.3">os</w> <w n="71.4">vibre</w> <w n="71.5">sous</w> <w n="71.6">le</w> <w n="71.7">baiser</w></l>
						<l n="72" num="9.8"><w n="72.1">Au</w> <w n="72.2">souffle</w> <w n="72.3">de</w> <w n="72.4">la</w> <w n="72.5">lèvre</w> <w n="72.6">rose</w> !</l>
					</lg>
					<lg n="10">
						<l n="73" num="10.1"><w n="73.1">Ainsi</w> <w n="73.2">j</w>’<w n="73.3">attendrai</w> <w n="73.4">doucement</w></l>
						<l n="74" num="10.2"><w n="74.1">Sur</w> <w n="74.2">la</w> <w n="74.3">bouche</w> <w n="74.4">des</w> <w n="74.5">belles</w> <w n="74.6">filles</w>,</l>
						<l n="75" num="10.3"><w n="75.1">L</w>’<w n="75.2">heure</w> <w n="75.3">auguste</w> <w n="75.4">du</w> <w n="75.5">Jugement</w>.</l>
						<l n="76" num="10.4"><w n="76.1">Ainsi</w> <w n="76.2">j</w>’<w n="76.3">attendrais</w> <w n="76.4">doucement</w>,</l>
						<l n="77" num="10.5"><w n="77.1">Joyeux</w> <w n="77.2">de</w> <w n="77.3">pouvoir</w> <w n="77.4">en</w> <w n="77.5">dormant</w></l>
						<l n="78" num="10.6"><w n="78.1">Conduire</w> <w n="78.2">encor</w> <w n="78.3">les</w> <w n="78.4">chauds</w> <w n="78.5">quadrilles</w> :</l>
						<l n="79" num="10.7"><w n="79.1">Ainsi</w> <w n="79.2">j</w>’<w n="79.3">attendrai</w> <w n="79.4">doucement</w>,</l>
						<l n="80" num="10.8"><w n="80.1">Sur</w> <w n="80.2">la</w> <w n="80.3">bouche</w> <w n="80.4">des</w> <w n="80.5">belles</w> <w n="80.6">filles</w>.</l>
					</lg>
				</div></body></text></TEI>