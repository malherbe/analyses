<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">La Légende des Sexes</title>
				<title type="sub">Poëmes Hystériques</title>
				<title type="medium">Édition électronique</title>
				<author key="HAR">
					<name>
						<forename>Edmond</forename>
						<surname>HARAUCOURT</surname>
					</name>
					<date from="1856" to="1941">1856-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1404 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">HAR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https ://fr.wikisource.org/wiki/La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>La Légende des sexes, poëmes hystériques</title>
								<author>Edmond Haraucourt</author>
								<idno type="URI">https ://fr.wikisource.org/wiki/Fichier :Haraucourt_-_La_L%C3%A9gende_des_sexes,_po%C3%ABmes_hyst%C3%A9riques,_1882.djvu</idno>
								<imprint>
									<pubPlace>Bruxelles</pubPlace>
									<publisher>Imprimé à Bruxelles pour l’auteur</publisher>
									<date when="1882">1882</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>La Légende des sexes, poëmes hystériques</title>
						<author>Edmond Haraucourt</author>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/btv1b8618380c</idno>
						<imprint>
							<pubPlace>Bruxelles</pubPlace>
							<publisher>ÉDITION PRIVILE, REVUE PAR L’AUTEUR</publisher>
							<date when="1893">1893</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1882">1882</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2022-05-28" who="RR">Une correction introduite à partir de l’édition imprimée de 1893</change>
				<change when="2017-10-30" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-10-30" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">La légende des sexes</head><div type="poem" key="HAR9">
					<head type="main">SONNET POINTU</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Reviens</w> <w n="1.2">sur</w> <w n="1.3">moi</w> ! <w n="1.4">Je</w> <w n="1.5">sens</w> <w n="1.6">ton</w> <w n="1.7">amour</w> <w n="1.8">qui</w> <w n="1.9">se</w> <w n="1.10">dresse</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Viens</w>, <w n="2.2">j</w>’<w n="2.3">ouvre</w> <w n="2.4">mon</w> <w n="2.5">désir</w> <w n="2.6">au</w> <w n="2.7">tien</w>, <w n="2.8">mon</w> <w n="2.9">jeune</w> <w n="2.10">amant</w>.</l>
						<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Là</w>… <w n="3.2">Tiens</w>… <w n="3.3">Doucement</w>… <w n="3.4">Va</w> <w n="3.5">plus</w> <w n="3.6">doucement</w>…</l>
						<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Je</w> <w n="4.2">sens</w>, <w n="4.3">tout</w> <w n="4.4">au</w> <w n="4.5">fond</w>, <w n="4.6">ta</w> <w n="4.7">chair</w> <w n="4.8">qui</w> <w n="4.9">me</w> <w n="4.10">presse</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><space unit="char" quantity="8"></space><w n="5.1">Rhythme</w> <w n="5.2">ton</w> <w n="5.3">ardente</w> <w n="5.4">caresse</w></l>
						<l n="6" num="2.2"><space unit="char" quantity="8"></space><w n="6.1">Au</w> <w n="6.2">gré</w> <w n="6.3">de</w> <w n="6.4">mon</w> <w n="6.5">balancement</w>,</l>
						<l n="7" num="2.3"><space unit="char" quantity="10"></space><w n="7.1">Ô</w> <w n="7.2">mon</w> <w n="7.3">âme</w>… <w n="7.4">Lentement</w>,</l>
						<l n="8" num="2.4"><space unit="char" quantity="10"></space><w n="8.1">Prolongeons</w> <w n="8.2">l</w>’<w n="8.3">instant</w> <w n="8.4">d</w>’<w n="8.5">ivresse</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><space unit="char" quantity="12"></space><w n="9.1">Là</w>… <w n="9.2">Vite</w> ! <w n="9.3">Plus</w> <w n="9.4">longtemps</w> !</l>
						<l n="10" num="3.2"><space unit="char" quantity="16"></space><w n="10.1">Je</w> <w n="10.2">fonds</w> ! <w n="10.3">Attends</w>,</l>
						<l n="11" num="3.3"><space unit="char" quantity="16"></space><w n="11.1">Oui</w>, <w n="11.2">je</w> <w n="11.3">t</w>’<w n="11.4">adore</w>…</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><space unit="char" quantity="18"></space><w n="12.1">Va</w> ! <w n="12.2">va</w> ! <w n="12.3">va</w> !</l>
						<l n="13" num="4.2"><space unit="char" quantity="20"></space><w n="13.1">Encore</w>.</l>
						<l n="14" num="4.3"><space unit="char" quantity="22"></space><w n="14.1">Ha</w> !</l>
					</lg>
				</div></body></text></TEI>