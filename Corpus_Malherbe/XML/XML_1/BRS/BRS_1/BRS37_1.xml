<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA LAMPE</head><div type="poem" key="BRS37">
					<head type="main">LA CONSOLATRICE QUI NE SAVAIT PAS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">avez</w> <w n="1.3">guéri</w> <w n="1.4">ma</w> <w n="1.5">souffrance</w></l>
						<l n="2" num="1.2"><w n="2.1">Sans</w> <w n="2.2">savoir</w> <w n="2.3">ce</w> <w n="2.4">qu</w>’<w n="2.5">elle</w> <w n="2.6">pleurait</w>.</l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">amitié</w> <w n="3.3">de</w> <w n="3.4">votre</w> <w n="3.5">présence</w></l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">tant</w> <w n="4.3">caressé</w> <w n="4.4">mon</w> <w n="4.5">secret</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Impassible</w>, <w n="5.2">et</w> <w n="5.3">pourtant</w> <w n="5.4">si</w> <w n="5.5">tendre</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Vous</w> <w n="6.2">parfumiez</w> <w n="6.3">la</w> <w n="6.4">vérité</w>,</l>
						<l n="7" num="2.3"><w n="7.1">M</w>’<w n="7.2">offrant</w> <w n="7.3">tout</w> <w n="7.4">l</w>’<w n="7.5">espoir</w> <w n="7.6">sans</w> <w n="7.7">m</w>’<w n="7.8">entendre</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Comme</w> <w n="8.2">une</w> <w n="8.3">fleur</w> <w n="8.4">de</w> <w n="8.5">charité</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mon</w> <w n="9.2">mal</w>, <w n="9.3">rien</w> <w n="9.4">n</w>’<w n="9.5">a</w> <w n="9.6">pu</w> <w n="9.7">vous</w> <w n="9.8">le</w> <w n="9.9">dire</w></l>
						<l n="10" num="3.2"><w n="10.1">Mais</w> <w n="10.2">vos</w> <w n="10.3">yeux</w> <w n="10.4">étaient</w> <w n="10.5">réchauffants</w></l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">malgré</w> <w n="11.3">vous</w> <w n="11.4">votre</w> <w n="11.5">sourire</w></l>
						<l n="12" num="3.4"><w n="12.1">Me</w> <w n="12.2">donnait</w> <w n="12.3">son</w> <w n="12.4">baiser</w> <w n="12.5">d</w>’<w n="12.6">enfant</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Sur</w> <w n="13.2">votre</w> <w n="13.3">seuil</w> <w n="13.4">plein</w> <w n="13.5">de</w> <w n="13.6">corolles</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Le</w> <w n="14.2">soir</w> <w n="14.3">je</w> <w n="14.4">suis</w> <w n="14.5">resté</w> <w n="14.6">parfois</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Abandonné</w> <w n="15.2">par</w> <w n="15.3">vos</w> <w n="15.4">paroles</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Mais</w> <w n="16.2">secouru</w> <w n="16.3">par</w> <w n="16.4">votre</w> <w n="16.5">voix</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Oh</w> ! <w n="17.2">quel</w> <w n="17.3">destin</w> <w n="17.4">sacré</w> <w n="17.5">te</w> <w n="17.6">pousse</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Petit</w> <w n="18.2">ange</w> <w n="18.3">qui</w> <w n="18.4">m</w>’<w n="18.5">est</w> <w n="18.6">venu</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Toi</w> <w n="19.2">dont</w> <w n="19.3">la</w> <w n="19.4">douceur</w> <w n="19.5">est</w> <w n="19.6">si</w> <w n="19.7">douce</w></l>
						<l n="20" num="5.4"><w n="20.1">Qu</w>’<w n="20.2">elle</w> <w n="20.3">console</w> <w n="20.4">l</w>’<w n="20.5">Inconnu</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Toi</w> <w n="21.2">qui</w> <w n="21.3">passas</w>, <w n="21.4">penchée</w> <w n="21.5">à</w> <w n="21.6">peine</w></l>
						<l n="22" num="6.2"><w n="22.1">Prés</w> <w n="22.2">du</w> <w n="22.3">pauvre</w>, <w n="22.4">prés</w> <w n="22.5">du</w> <w n="22.6">pécheur</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">qui</w> <w n="23.3">te</w> <w n="23.4">mêlas</w> <w n="23.5">à</w> <w n="23.6">ma</w> <w n="23.7">peine</w></l>
						<l n="24" num="6.4"><w n="24.1">En</w> <w n="24.2">gardant</w> <w n="24.3">toute</w> <w n="24.4">ta</w> <w n="24.5">blancheur</w>.</l>
					</lg>
				</div></body></text></TEI>