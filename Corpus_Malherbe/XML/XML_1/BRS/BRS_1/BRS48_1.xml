<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA HAINE</head><div type="poem" key="BRS48">
					<head type="main">LA HAINE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Malgré toi la beauté me brave.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">sommes</w> <w n="1.3">tous</w> <w n="1.4">les</w> <w n="1.5">deux</w> <w n="1.6">ensemble</w></l>
						<l n="2" num="1.2"><w n="2.1">Nous</w>, <w n="2.2">les</w> <w n="2.3">amants</w> <w n="2.4">à</w> <w n="2.5">l</w>’<w n="2.6">infini</w>,</l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">ouragan</w> <w n="3.3">pleure</w> <w n="3.4">et</w> <w n="3.5">le</w> <w n="3.6">ciel</w> <w n="3.7">tremble</w>…</l>
						<l n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">n</w>’<w n="4.3">avons</w> <w n="4.4">rien</w> <w n="4.5">qui</w> <w n="4.6">nous</w> <w n="4.7">unit</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Nous</w> <w n="5.2">regardons</w> <w n="5.3">le</w> <w n="5.4">soir</w> <w n="5.5">céleste</w></l>
						<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">se</w> <w n="6.3">plombe</w> <w n="6.4">et</w> <w n="6.5">tombe</w> <w n="6.6">sans</w> <w n="6.7">fin</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">le</w> <w n="7.3">silence</w> <w n="7.4">nous</w> <w n="7.5">déteste</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">notre</w> <w n="8.3">amour</w> <w n="8.4">a</w> <w n="8.5">toujours</w> <w n="8.6">faim</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Tandis</w> <w n="9.2">que</w> <w n="9.3">l</w>’<w n="9.4">ombre</w> <w n="9.5">nous</w> <w n="9.6">azure</w></l>
						<l n="10" num="3.2"><w n="10.1">Ainsi</w> <w n="10.2">qu</w>’<w n="10.3">un</w> <w n="10.4">grand</w> <w n="10.5">couple</w> <w n="10.6">éternel</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Le</w> <w n="11.2">silence</w> <w n="11.3">comme</w> <w n="11.4">un</w> <w n="11.5">murmure</w></l>
						<l n="12" num="3.4"><w n="12.1">Remplit</w> <w n="12.2">la</w> <w n="12.3">chambre</w> <w n="12.4">jusqu</w>’<w n="12.5">au</w> <w n="12.6">ciel</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">lorsque</w> <w n="13.3">la</w> <w n="13.4">nuit</w> <w n="13.5">souveraine</w></l>
						<l n="14" num="4.2"><w n="14.1">T</w>’<w n="14.2">étoile</w> <w n="14.3">de</w> <w n="14.4">son</w> <w n="14.5">vieux</w> <w n="14.6">reflet</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">sens</w> <w n="15.3">comme</w> <w n="15.4">une</w> <w n="15.5">grande</w> <w n="15.6">haine</w></l>
						<l n="16" num="4.4"><w n="16.1">Qui</w> <w n="16.2">nous</w> <w n="16.3">sépare</w> <w n="16.4">et</w> <w n="16.5">qui</w> <w n="16.6">se</w> <w n="16.7">tait</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">t</w>’<w n="17.3">aime</w> <w n="17.4">pourtant</w>, <w n="17.5">oh</w> <w n="17.6">je</w> <w n="17.7">t</w>’<w n="17.8">aime</w></l>
						<l n="18" num="5.2"><w n="18.1">Demi</w>-<w n="18.2">pleurante</w> <w n="18.3">en</w> <w n="18.4">tes</w> <w n="18.5">attraits</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">vague</w>, <w n="19.3">avec</w> <w n="19.4">ton</w> <w n="19.5">diadème</w></l>
						<l n="20" num="5.4"><w n="20.1">Où</w> <w n="20.2">frissonnent</w> <w n="20.3">les</w> <w n="20.4">astres</w> <w n="20.5">vrais</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Presque</w> <w n="21.2">cachés</w> <w n="21.3">par</w> <w n="21.4">l</w>’<w n="21.5">heure</w> <w n="21.6">sombre</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Je</w> <w n="22.2">vois</w> <w n="22.3">surgir</w> <w n="22.4">blanches</w>, <w n="22.5">sans</w> <w n="22.6">bruit</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Les</w> <w n="23.2">mains</w> <w n="23.3">que</w> <w n="23.4">tu</w> <w n="23.5">tends</w> <w n="23.6">à</w> <w n="23.7">mon</w> <w n="23.8">ombre</w></l>
						<l n="24" num="6.4"><w n="24.1">Dans</w> <w n="24.2">les</w> <w n="24.3">abîmes</w> <w n="24.4">de</w> <w n="24.5">la</w> <w n="24.6">nuit</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">lorsqu</w>’<w n="25.3">un</w> <w n="25.4">grand</w> <w n="25.5">rayon</w> <w n="25.6">t</w>’<w n="25.7">éclaire</w></l>
						<l n="26" num="7.2"><w n="26.1">Je</w> <w n="26.2">devine</w> <w n="26.3">invinciblement</w></l>
						<l n="27" num="7.3"><w n="27.1">Que</w> <w n="27.2">je</w> <w n="27.3">ne</w> <w n="27.4">sais</w> <w n="27.5">pas</w> <w n="27.6">la</w> <w n="27.7">lumière</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Que</w> <w n="28.2">l</w>’<w n="28.3">on</w> <w n="28.4">s</w>’<w n="28.5">ignore</w>, <w n="28.6">et</w> <w n="28.7">que</w> <w n="28.8">l</w>’<w n="28.9">on</w> <w n="28.10">ment</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">J</w>’<w n="29.2">avais</w> <w n="29.3">rêvé</w> <w n="29.4">comme</w> <w n="29.5">un</w> <w n="29.6">apôtre</w></l>
						<l n="30" num="8.2"><w n="30.1">D</w>’<w n="30.2">inaccessibles</w> <w n="30.3">unions</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">Nous</w> <w n="31.2">sommes</w> <w n="31.3">l</w>’<w n="31.4">un</w> <w n="31.5">auprès</w> <w n="31.6">de</w> <w n="31.7">l</w>’<w n="31.8">autre</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Il</w> <w n="32.2">faut</w> <w n="32.3">que</w> <w n="32.4">nous</w> <w n="32.5">nous</w> <w n="32.6">haïssions</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Hélas</w>, <w n="33.2">lorsque</w> <w n="33.3">mon</w> <w n="33.4">âme</w> <w n="33.5">est</w> <w n="33.6">pleine</w></l>
						<l n="34" num="9.2"><w n="34.1">De</w> <w n="34.2">tant</w> <w n="34.3">d</w>’<w n="34.4">impuissance</w> <w n="34.5">et</w> <w n="34.6">d</w>’<w n="34.7">adieu</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Je</w> <w n="35.2">souffre</w> <w n="35.3">d</w>’<w n="35.4">avoir</w> <w n="35.5">tant</w> <w n="35.6">de</w> <w n="35.7">haine</w></l>
						<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">je</w> <w n="36.3">voudrais</w> <w n="36.4">t</w>’<w n="36.5">aimer</w> <w n="36.6">un</w> <w n="36.7">peu</w>…</l>
					</lg>
				</div></body></text></TEI>