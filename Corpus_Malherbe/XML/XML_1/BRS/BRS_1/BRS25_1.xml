<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES CHOSES</head><div type="poem" key="BRS25">
					<head type="main">LES CHOSES</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Dans le sourire et l’habitude…
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">yeux</w> <w n="1.3">dans</w> <w n="1.4">tes</w> <w n="1.5">yeux</w> <w n="1.6">de</w> <w n="1.7">beauté</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">je</w> <w n="2.3">mets</w> <w n="2.4">ma</w> <w n="2.5">main</w> <w n="2.6">dans</w> <w n="2.7">la</w> <w n="2.8">tienne</w>,</l>
						<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">évoque</w> <w n="3.3">les</w> <w n="3.4">choses</w> <w n="3.5">anciennes</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">toute</w> <w n="4.3">leur</w> <w n="4.4">tranquillité</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tous</w> <w n="5.2">nos</w> <w n="5.3">rêves</w> <w n="5.4">les</w> <w n="5.5">plus</w> <w n="5.6">rapides</w></l>
						<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">aiment</w> <w n="6.3">un</w> <w n="6.4">peu</w> <w n="6.5">tour</w> <w n="6.6">à</w> <w n="6.7">tour</w>,</l>
						<l n="7" num="2.3"><w n="7.1">C</w>’<w n="7.2">est</w> <w n="7.3">pourquoi</w> <w n="7.4">leur</w> <w n="7.5">confus</w> <w n="7.6">amour</w></l>
						<l n="8" num="2.4"><w n="8.1">Nous</w> <w n="8.2">suit</w> <w n="8.3">avec</w> <w n="8.4">ses</w> <w n="8.5">yeux</w> <w n="8.6">placides</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Nous</w> <w n="9.2">leur</w> <w n="9.3">donnons</w> <w n="9.4">un</w> <w n="9.5">peu</w> <w n="9.6">d</w>’<w n="9.7">été</w></l>
						<l n="10" num="3.2"><w n="10.1">Lorsque</w> <w n="10.2">les</w> <w n="10.3">rayons</w> <w n="10.4">nous</w> <w n="10.5">regardent</w></l>
						<l n="11" num="3.3"><w n="11.1">Un</w> <w n="11.2">peu</w> <w n="11.3">de</w> <w n="11.4">gloire</w> <w n="11.5">qu</w>’<w n="11.6">elles</w> <w n="11.7">gardent</w></l>
						<l n="12" num="3.4"><w n="12.1">Avec</w> <w n="12.2">leur</w> <w n="12.3">immobilité</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Les</w> <w n="13.2">souvenirs</w> <w n="13.3">seront</w> <w n="13.4">fidèles</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Car</w> <w n="14.2">ils</w> <w n="14.3">ont</w> <w n="14.4">leur</w> <w n="14.5">recueillement</w> :</l>
						<l n="15" num="4.3"><w n="15.1">Nous</w> <w n="15.2">les</w> <w n="15.3">trouvons</w> <w n="15.4">exactement</w></l>
						<l n="16" num="4.4"><w n="16.1">Quand</w> <w n="16.2">nous</w> <w n="16.3">revenons</w> <w n="16.4">auprès</w> <w n="16.5">d</w>’<w n="16.6">elles</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Forts</w>, <w n="17.2">riants</w>, <w n="17.3">rêvant</w> <w n="17.4">d</w>’<w n="17.5">avenir</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Nous</w> <w n="18.2">semions</w> <w n="18.3">notre</w> <w n="18.4">âme</w> <w n="18.5">ravie</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Elles</w> <w n="19.2">nous</w> <w n="19.3">ont</w> <w n="19.4">rendu</w> <w n="19.5">la</w> <w n="19.6">vie</w></l>
						<l n="20" num="5.4"><w n="20.1">Pacifique</w> <w n="20.2">du</w> <w n="20.3">souvenir</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">ce</w> <w n="21.3">soir</w>, <w n="21.4">lassé</w> <w n="21.5">des</w> <w n="21.6">paroles</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Sans</w> <w n="22.2">le</w> <w n="22.3">savoir</w> <w n="22.4">sage</w> <w n="22.5">et</w> <w n="22.6">pieux</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Je</w> <w n="23.2">sens</w> <w n="23.3">ma</w> <w n="23.4">chair</w> <w n="23.5">lever</w> <w n="23.6">les</w> <w n="23.7">yeux</w></l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">prier</w>, <w n="24.3">ô</w> <w n="24.4">toi</w> <w n="24.5">qui</w> <w n="24.6">t</w>’<w n="24.7">envoles</w>…</l>
					</lg>
				</div></body></text></TEI>