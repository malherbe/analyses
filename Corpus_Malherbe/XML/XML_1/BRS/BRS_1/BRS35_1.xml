<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA LAMPE</head><div type="poem" key="BRS35">
					<head type="main">LA LETTRE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Doucement,
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">t</w>’<w n="1.3">écris</w>, <w n="1.4">et</w> <w n="1.5">la</w> <w n="1.6">lampe</w> <w n="1.7">écoute</w>.</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">horloge</w> <w n="2.3">attend</w> <w n="2.4">à</w> <w n="2.5">petits</w> <w n="2.6">coups</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">vais</w> <w n="3.3">fermer</w> <w n="3.4">les</w> <w n="3.5">yeux</w> <w n="3.6">sans</w> <w n="3.7">doute</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">je</w> <w n="4.3">vais</w> <w n="4.4">m</w>’<w n="4.5">endormir</w> <w n="4.6">en</w> <w n="4.7">nous</w>…</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">lampe</w> <w n="5.3">est</w> <w n="5.4">douce</w> <w n="5.5">et</w> <w n="5.6">j</w>’<w n="5.7">ai</w> <w n="5.8">la</w> <w n="5.9">fièvre</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">On</w> <w n="6.2">n</w>’<w n="6.3">entend</w> <w n="6.4">que</w> <w n="6.5">ta</w> <w n="6.6">voix</w>, <w n="6.7">ta</w> <w n="6.8">voix</w>…</l>
						<l n="7" num="2.3"><w n="7.1">J</w>’<w n="7.2">ai</w> <w n="7.3">ton</w> <w n="7.4">nom</w> <w n="7.5">qui</w> <w n="7.6">rit</w> <w n="7.7">sur</w> <w n="7.8">ma</w> <w n="7.9">lèvre</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">ta</w> <w n="8.3">caresse</w> <w n="8.4">est</w> <w n="8.5">dans</w> <w n="8.6">mes</w> <w n="8.7">doigts</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2">ai</w> <w n="9.3">notre</w> <w n="9.4">douceur</w> <w n="9.5">de</w> <w n="9.6">naguère</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Ton</w> <w n="10.2">pauvre</w> <w n="10.3">cœur</w> <w n="10.4">sanglote</w> <w n="10.5">en</w> <w n="10.6">moi</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">mi</w>-<w n="11.3">rêvant</w>, <w n="11.4">je</w> <w n="11.5">ne</w> <w n="11.6">sais</w> <w n="11.7">guère</w></l>
						<l n="12" num="3.4"><w n="12.1">Si</w> <w n="12.2">c</w>’<w n="12.3">est</w> <w n="12.4">moi</w> <w n="12.5">qui</w> <w n="12.6">t</w>’<w n="12.7">écris</w>, <w n="12.8">ou</w> <w n="12.9">toi</w>…</l>
					</lg>
				</div></body></text></TEI>