<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES CHOSES</head><div type="poem" key="BRS26">
					<head type="main">DEUX VIEILLES CHOSES</head>

					<div type="section" n="1">
						<head type="number">I</head>
						<head type="main">Le Poisson Sec</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Parmi</w> <w n="1.2">la</w> <w n="1.3">boutique</w> <w n="1.4">un</w> <w n="1.5">peu</w> <w n="1.6">noire</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Reflet</w> <w n="2.2">morne</w> <w n="2.3">demi</w>-<w n="2.4">caché</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Tu</w> <w n="3.2">n</w>’<w n="3.3">es</w>, <w n="3.4">pauvre</w> <w n="3.5">poisson</w> <w n="3.6">séché</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">les</w> <w n="4.3">lettres</w> <w n="4.4">de</w> <w n="4.5">ton</w> <w n="4.6">histoire</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Te</w> <w n="5.2">rendrait</w>-<w n="5.3">on</w> <w n="5.4">ton</w> <w n="5.5">cœur</w> <w n="5.6">amer</w></l>
							<l n="6" num="2.2"><w n="6.1">Ta</w> <w n="6.2">vie</w> <w n="6.3">âpre</w> <w n="6.4">et</w> <w n="6.5">dévoratrice</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Quand</w> <w n="7.2">tu</w> <w n="7.3">sombrais</w> <w n="7.4">avec</w> <w n="7.5">délice</w></l>
							<l n="8" num="2.4"><w n="8.1">Dans</w> <w n="8.2">la</w> <w n="8.3">caresse</w> <w n="8.4">de</w> <w n="8.5">la</w> <w n="8.6">mer</w> ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Te</w> <w n="9.2">rendrait</w>-<w n="9.3">on</w> <w n="9.4">ton</w> <w n="9.5">doux</w> <w n="9.6">sillage</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Monarque</w> <w n="10.2">fluide</w> <w n="10.3">aux</w> <w n="10.4">yeux</w> <w n="10.5">d</w>’<w n="10.6">or</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Ton</w> <w n="11.2">rêve</w> <w n="11.3">assiégeant</w> <w n="11.4">et</w> <w n="11.5">sans</w> <w n="11.6">bord</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Ta</w> <w n="12.2">vie</w>, <w n="12.3">étroit</w> <w n="12.4">et</w> <w n="12.5">grand</w> <w n="12.6">voyage</w>,</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Quand</w> <w n="13.2">même</w> <w n="13.3">entre</w> <w n="13.4">tes</w> <w n="13.5">petits</w> <w n="13.6">os</w></l>
							<l n="14" num="4.2"><w n="14.1">Tandis</w> <w n="14.2">que</w> <w n="14.3">tu</w> <w n="14.4">gis</w> <w n="14.5">sur</w> <w n="14.6">la</w> <w n="14.7">planche</w>,</l>
							<l n="15" num="4.3"><w n="15.1">On</w> <w n="15.2">mettrait</w> <w n="15.3">en</w> <w n="15.4">poussière</w> <w n="15.5">blanche</w></l>
							<l n="16" num="4.4"><w n="16.1">La</w> <w n="16.2">grande</w> <w n="16.3">amertume</w> <w n="16.4">des</w> <w n="16.5">eaux</w> !…</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Ce</w> <w n="17.2">matin</w>, <w n="17.3">j</w>’<w n="17.4">ai</w> <w n="17.5">jeté</w> <w n="17.6">nos</w> <w n="17.7">lettres</w></l>
							<l n="18" num="5.2"><w n="18.1">Dans</w> <w n="18.2">le</w> <w n="18.3">feu</w>, <w n="18.4">neuf</w> <w n="18.5">et</w> <w n="18.6">clair</w> <w n="18.7">frisson</w>…</l>
							<l n="19" num="5.3"><w n="19.1">Elle</w> <w n="19.2">n</w>’<w n="19.3">a</w> <w n="19.4">rien</w> <w n="19.5">dit</w>, <w n="19.6">la</w> <w n="19.7">chanson</w></l>
							<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">chantonnait</w> <w n="20.3">auprès</w> <w n="20.4">des</w> <w n="20.5">lettres</w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<head type="main">Loque</head>
						<opener>
							<epigraph>
								<cit>
									<quote>
										Ta belle âme de ballon…
									</quote>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="21" num="1.1"><w n="21.1">La</w> <w n="21.2">félicité</w> <w n="21.3">n</w>’<w n="21.4">est</w> <w n="21.5">qu</w>’<w n="21.6">un</w> <w n="21.7">songe</w></l>
							<l n="22" num="1.2"><w n="22.1">Qui</w> <w n="22.2">s</w>’<w n="22.3">en</w> <w n="22.4">va</w> <w n="22.5">comme</w> <w n="22.6">un</w> <w n="22.7">chenapan</w>.</l>
							<l n="23" num="1.3"><w n="23.1">On</w> <w n="23.2">dirait</w> <w n="23.3">un</w> <w n="23.4">peu</w> <w n="23.5">qu</w>’<w n="23.6">il</w> <w n="23.7">y</w> <w n="23.8">songe</w>,</l>
							<l n="24" num="1.4"><w n="24.1">Lorsque</w>, <w n="24.2">mélancolique</w>, <w n="24.3">il</w> <w n="24.4">pend</w>.</l>
							<l n="25" num="1.5"><w n="25.1">Les</w> <w n="25.2">heures</w> <w n="25.3">d</w>’<w n="25.4">oubli</w> <w n="25.5">sont</w> <w n="25.6">rapides</w> :</l>
							<l n="26" num="1.6"><w n="26.1">Ivre</w> <w n="26.2">et</w> <w n="26.3">tout</w> <w n="26.4">vague</w>, <w n="26.5">l</w>’<w n="26.6">aquilon</w></l>
							<l n="27" num="1.7"><w n="27.1">Touche</w> <w n="27.2">du</w> <w n="27.3">doigt</w> <w n="27.4">ses</w> <w n="27.5">jambes</w> <w n="27.6">vides</w>.</l>
							<l n="28" num="1.8"><w n="28.1">Le</w> <w n="28.2">jour</w> <w n="28.3">est</w> <w n="28.4">mort</w>, <w n="28.5">le</w> <w n="28.6">soir</w> <w n="28.7">est</w> <w n="28.8">long</w>.</l>
						</lg>
						<lg n="2">
							<l n="29" num="2.1"><w n="29.1">Le</w> <w n="29.2">vent</w> <w n="29.3">sans</w> <w n="29.4">pitié</w> <w n="29.5">pour</w> <w n="29.6">son</w> <w n="29.7">âge</w></l>
							<l n="30" num="2.2"><w n="30.1">Mêle</w> <w n="30.2">ses</w> <w n="30.3">membres</w> <w n="30.4">ramollis</w>,</l>
							<l n="31" num="2.3"><w n="31.1">C</w>’<w n="31.2">est</w> <w n="31.3">corme</w> <w n="31.4">un</w> <w n="31.5">mince</w> <w n="31.6">personnage</w></l>
							<l n="32" num="2.4"><w n="32.1">Qui</w> <w n="32.2">se</w> <w n="32.3">glisse</w> <w n="32.4">dans</w> <w n="32.5">les</w> <w n="32.6">vieux</w> <w n="32.7">plis</w>.</l>
							<l n="33" num="2.5"><w n="33.1">Et</w> <w n="33.2">lui</w>, <w n="33.3">s</w>’<w n="33.4">éveillant</w> <w n="33.5">triste</w> <w n="33.6">et</w> <w n="33.7">gauche</w>,</l>
							<l n="34" num="2.6"><w n="34.1">Voudrait</w> <w n="34.2">rire</w>, <w n="34.3">malgré</w> <w n="34.4">son</w> <w n="34.5">plomb</w> ;</l>
							<l n="35" num="2.7"><w n="35.1">Il</w> <w n="35.2">essaye</w> <w n="35.3">une</w> <w n="35.4">vague</w> <w n="35.5">ébauche</w>…</l>
							<l n="36" num="2.8"><w n="36.1">Le</w> <w n="36.2">jour</w> <w n="36.3">est</w> <w n="36.4">mort</w>, <w n="36.5">le</w> <w n="36.6">soir</w> <w n="36.7">est</w> <w n="36.8">long</w>.</l>
						</lg>
						<lg n="3">
							<l n="37" num="3.1"><w n="37.1">Près</w> <w n="37.2">d</w>’<w n="37.3">un</w> <w n="37.4">habit</w> <w n="37.5">à</w> <w n="37.6">longues</w> <w n="37.7">basques</w>,</l>
							<l n="38" num="3.2"><w n="38.1">Il</w> <w n="38.2">esquisse</w> <w n="38.3">en</w> <w n="38.4">l</w>’<w n="38.5">air</w>, <w n="38.6">accroché</w>,</l>
							<l n="39" num="3.3"><w n="39.1">Ses</w> <w n="39.2">pas</w> <w n="39.3">incohérents</w> <w n="39.4">et</w> <w n="39.5">flasques</w>,</l>
							<l n="40" num="3.4"><w n="40.1">Ce</w> <w n="40.2">vieux</w> <w n="40.3">qui</w> <w n="40.4">sait</w> <w n="40.5">qu</w>’<w n="40.6">il</w> <w n="40.7">a</w> <w n="40.8">marché</w>.</l>
							<l n="41" num="3.5"><w n="41.1">Le</w> <w n="41.2">dolman</w> <w n="41.3">à</w> <w n="41.4">large</w> <w n="41.5">carrure</w></l>
							<l n="42" num="3.6"><w n="42.1">Dont</w> <w n="42.2">il</w> <w n="42.3">bat</w> <w n="42.4">le</w> <w n="42.5">triple</w> <w n="42.6">galon</w></l>
							<l n="43" num="3.7"><w n="43.1">Grince</w> <w n="43.2">avec</w> <w n="43.3">un</w> <w n="43.4">bruit</w> <w n="43.5">de</w> <w n="43.6">serrure</w>…</l>
							<l n="44" num="3.8"><w n="44.1">Le</w> <w n="44.2">jour</w> <w n="44.3">est</w> <w n="44.4">mort</w>, <w n="44.5">le</w> <w n="44.6">soir</w> <w n="44.7">est</w> <w n="44.8">long</w>.</l>
						</lg>
						<lg n="4">
							<l n="45" num="4.1"><w n="45.1">Tu</w> <w n="45.2">danses</w> <w n="45.3">dans</w> <w n="45.4">l</w>’<w n="45.5">or</w> <w n="45.6">poétique</w>,</l>
							<l n="46" num="4.2"><w n="46.1">Pauvre</w> <w n="46.2">orateur</w> <w n="46.3">tenace</w> <w n="46.4">et</w> <w n="46.5">laid</w>,</l>
							<l n="47" num="4.3"><w n="47.1">Avec</w> <w n="47.2">ton</w> <w n="47.3">destin</w> <w n="47.4">de</w> <w n="47.5">boutique</w></l>
							<l n="48" num="4.4"><w n="48.1">Et</w> <w n="48.2">tes</w> <w n="48.3">cauchemars</w> <w n="48.4">de</w> <w n="48.5">balai</w>.</l>
							<l n="49" num="4.5"><w n="49.1">Qu</w>’<w n="49.2">un</w> <w n="49.3">jeune</w>, <w n="49.4">auquel</w> <w n="49.5">rien</w> <w n="49.6">ne</w> <w n="49.7">résiste</w>,</l>
							<l n="50" num="4.6"><w n="50.1">Pince</w> <w n="50.2">la</w> <w n="50.3">lyre</w> <w n="50.4">d</w>’<w n="50.5">Apollon</w> ;</l>
							<l n="51" num="4.7"><w n="51.1">Je</w> <w n="51.2">le</w> <w n="51.3">regarde</w> <w n="51.4">d</w>’<w n="51.5">un</w> <w n="51.6">air</w> <w n="51.7">triste</w>.</l>
							<l n="52" num="4.8"><w n="52.1">Le</w> <w n="52.2">jour</w> <w n="52.3">est</w> <w n="52.4">mort</w>, <w n="52.5">le</w> <w n="52.6">soir</w> <w n="52.7">est</w> <w n="52.8">long</w>.</l>
						</lg>
						<lg n="5">
							<l n="53" num="5.1"><w n="53.1">Nous</w> <w n="53.2">nous</w> <w n="53.3">en</w> <w n="53.4">irons</w>, <w n="53.5">pauvres</w> <w n="53.6">princes</w>,</l>
							<l n="54" num="5.2"><w n="54.1">Avec</w> <w n="54.2">notre</w> <w n="54.3">tranquillité</w> ;</l>
							<l n="55" num="5.3"><w n="55.1">Je</w> <w n="55.2">te</w> <w n="55.3">prendrai</w> <w n="55.4">dans</w> <w n="55.5">mes</w> <w n="55.6">bras</w> <w n="55.7">minces</w>,</l>
							<l n="56" num="5.4"><w n="56.1">Ô</w> <w n="56.2">le</w> <w n="56.3">seul</w> <w n="56.4">qui</w> <w n="56.5">me</w> <w n="56.6">soit</w> <w n="56.7">resté</w> !</l>
							<l n="57" num="5.5"><w n="57.1">Automne</w> <w n="57.2">gris</w> <w n="57.3">qui</w> <w n="57.4">te</w> <w n="57.5">recueilles</w>,</l>
							<l n="58" num="5.6"><w n="58.1">J</w>’<w n="58.2">entends</w> <w n="58.3">gémir</w> <w n="58.4">dans</w> <w n="58.5">le</w> <w n="58.6">vallon</w></l>
							<l n="59" num="5.7"><w n="59.1">Des</w> <w n="59.2">souvenirs</w> <w n="59.3">de</w> <w n="59.4">vieilles</w> <w n="59.5">feuilles</w>.</l>
							<l n="60" num="5.8"><w n="60.1">Le</w> <w n="60.2">jour</w> <w n="60.3">est</w> <w n="60.4">mort</w>, <w n="60.5">le</w> <w n="60.6">soir</w> <w n="60.7">est</w> <w n="60.8">long</w>.</l>
						</lg>
					</div>
				</div></body></text></TEI>