<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LES CHOSES</head><div type="poem" key="BRS29">
					<head type="main">RETOUR</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Dans la solitude qu’on voit.
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">visiterons</w> <w n="1.3">lentement</w></l>
						<l n="2" num="1.2"><w n="2.1">Notre</w> <w n="2.2">existence</w> <w n="2.3">douce</w> <w n="2.4">et</w> <w n="2.5">lasse</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">un</w> <w n="3.3">vieux</w> <w n="3.4">voyageur</w> <w n="3.5">qui</w> <w n="3.6">passe</w></l>
						<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">un</w> <w n="4.3">très</w> <w n="4.4">vieil</w> <w n="4.5">appartement</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Pleins</w> <w n="5.2">de</w> <w n="5.3">rêves</w> <w n="5.4">mélancoliques</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Éveillons</w> <w n="6.2">les</w> <w n="6.3">espoirs</w> <w n="6.4">tremblants</w></l>
						<l n="7" num="2.3"><w n="7.1">En</w> <w n="7.2">nous</w> <w n="7.3">promenant</w> <w n="7.4">à</w> <w n="7.5">pas</w> <w n="7.6">lents</w></l>
						<l n="8" num="2.4"><w n="8.1">Parmi</w> <w n="8.2">les</w> <w n="8.3">chambres</w> <w n="8.4">pacifiques</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Passons</w> <w n="9.2">où</w> <w n="9.3">nous</w> <w n="9.4">avons</w> <w n="9.5">passé</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Par</w> <w n="10.2">la</w> <w n="10.3">large</w> <w n="10.4">et</w> <w n="10.5">pâle</w> <w n="10.6">fenêtre</w></l>
						<l n="11" num="3.3"><w n="11.1">Un</w> <w n="11.2">peu</w> <w n="11.3">de</w> <w n="11.4">lumière</w> <w n="11.5">pénètre</w></l>
						<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">la</w> <w n="12.3">fatigue</w> <w n="12.4">du</w> <w n="12.5">passé</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Nous</w> <w n="13.2">aurons</w> <w n="13.3">des</w> <w n="13.4">caresses</w> <w n="13.5">d</w>’<w n="13.6">ombres</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">des</w> <w n="14.3">appels</w> <w n="14.4">silencieux</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">nous</w> <w n="15.3">sentirons</w> <w n="15.4">sur</w> <w n="15.5">nos</w> <w n="15.6">yeux</w></l>
						<l n="16" num="4.4"><w n="16.1">Le</w> <w n="16.2">regard</w> <w n="16.3">triste</w> <w n="16.4">des</w> <w n="16.5">coins</w> <w n="16.6">sombres</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">La</w> <w n="17.2">petite</w> <w n="17.3">chambre</w> <w n="17.4">est</w> <w n="17.5">bien</w> <w n="17.6">vide</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Elle</w> <w n="18.2">nous</w> <w n="18.3">reconnaît</w> <w n="18.4">un</w> <w n="18.5">peu</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Elle</w> <w n="19.2">est</w> <w n="19.3">demi</w>-<w n="19.4">morte</w> <w n="19.5">d</w>’<w n="19.6">adieu</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Demi</w>-<w n="20.2">morte</w> <w n="20.3">et</w> <w n="20.4">demi</w>-<w n="20.5">timide</w>…</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">La</w> <w n="21.2">douceur</w> <w n="21.3">de</w> <w n="21.4">ce</w> <w n="21.5">jour</w> <w n="21.6">d</w>’<w n="21.7">été</w></l>
						<l n="22" num="6.2"><w n="22.1">Erre</w> <w n="22.2">dans</w> <w n="22.3">l</w>’<w n="22.4">antique</w> <w n="22.5">silence</w>…</l>
						<l n="23" num="6.3"><w n="23.1">Elle</w> <w n="23.2">exauce</w> <w n="23.3">ma</w> <w n="23.4">pauvre</w> <w n="23.5">enfance</w></l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">la</w> <w n="24.3">bénit</w> <w n="24.4">de</w> <w n="24.5">vérité</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Je</w> <w n="25.2">pleure</w> <w n="25.3">l</w>’<w n="25.4">âme</w> <w n="25.5">répandue</w>,</l>
						<l n="26" num="7.2"><w n="26.1">La</w> <w n="26.2">foi</w>, <w n="26.3">le</w> <w n="26.4">rêve</w> <w n="26.5">abandonné</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">le</w> <w n="27.3">mur</w> <w n="27.4">est</w> <w n="27.5">illuminé</w></l>
						<l n="28" num="7.4"><w n="28.1">De</w> <w n="28.2">toute</w> <w n="28.3">la</w> <w n="28.4">fête</w> <w n="28.5">perdue</w>…</l>
					</lg>
				</div></body></text></TEI>