<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA LAMPE</head><div type="poem" key="BRS32">
					<head type="main">LA ROMANCE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Sur</w> <w n="1.2">la</w> <w n="1.3">pluie</w> <w n="1.4">un</w> <w n="1.5">peu</w> <w n="1.6">de</w> <w n="1.7">jour</w>…</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">soleil</w> <w n="2.3">jaune</w> <w n="2.4">et</w> <w n="2.5">bleu</w> <w n="2.6">verse</w></l>
						<l n="3" num="1.3"><w n="3.1">Un</w> <w n="3.2">rayon</w> <w n="3.3">perlé</w> <w n="3.4">d</w>’<w n="3.5">averse</w></l>
						<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">les</w> <w n="4.3">maisons</w> <w n="4.4">du</w> <w n="4.5">faubourg</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Parmi</w> <w n="5.2">l</w>’<w n="5.3">atelier</w> <w n="5.4">avare</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Sombre</w> <w n="6.2">et</w> <w n="6.3">courbée</w>, <w n="6.4">elle</w> <w n="6.5">coud</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">sent</w> <w n="7.3">doucement</w> <w n="7.4">sur</w> <w n="7.5">tout</w></l>
						<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">arc</w>-<w n="8.3">en</w>-<w n="8.4">ciel</w> <w n="8.5">qui</w> <w n="8.6">se</w> <w n="8.7">prépare</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Quand</w> <w n="9.2">il</w> <w n="9.3">luit</w> <w n="9.4">illimité</w></l>
						<l n="10" num="3.2"><w n="10.1">Sur</w> <w n="10.2">les</w> <w n="10.3">maisons</w> <w n="10.4">éblouies</w></l>
						<l n="11" num="3.3"><w n="11.1">Des</w> <w n="11.2">longs</w> <w n="11.3">rayons</w> <w n="11.4">de</w> <w n="11.5">la</w> <w n="11.6">pluie</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Comme</w> <w n="12.2">un</w> <w n="12.3">ange</w> <w n="12.4">elle</w> <w n="12.5">a</w> <w n="12.6">chanté</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Chanté</w> <w n="13.2">l</w>’<w n="13.3">étendue</w> <w n="13.4">immense</w>,</l>
						<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2">avenir</w> <w n="14.3">vague</w> <w n="14.4">et</w> <w n="14.5">fleuri</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Les</w> <w n="15.2">yeux</w> <w n="15.3">sur</w> <w n="15.4">ses</w> <w n="15.5">mains</w> <w n="15.6">sourient</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Elle</w> <w n="16.2">croit</w> <w n="16.3">à</w> <w n="16.4">sa</w> <w n="16.5">romance</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Elle</w> <w n="17.2">croit</w> <w n="17.3">à</w> <w n="17.4">la</w> <w n="17.5">beauté</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Elle</w> <w n="18.2">croit</w> <w n="18.3">à</w> <w n="18.4">l</w>’<w n="18.5">harmonie</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Elle</w> <w n="19.2">se</w> <w n="19.3">sent</w> <w n="19.4">infinie</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Les</w> <w n="20.2">lèvres</w> <w n="20.3">dans</w> <w n="20.4">la</w> <w n="20.5">clarté</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">plus</w> <w n="21.3">tard</w>, <w n="21.4">grise</w> <w n="21.5">et</w> <w n="21.6">fidèle</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Murmurant</w> <w n="22.2">les</w> <w n="22.3">airs</w> <w n="22.4">anciens</w></l>
						<l n="23" num="6.3"><w n="23.1">Elle</w> <w n="23.2">revient</w> <w n="23.3">vers</w> <w n="23.4">les</w> <w n="23.5">siens</w></l>
						<l n="24" num="6.4"><w n="24.1">Avec</w> <w n="24.2">le</w> <w n="24.3">soir</w> <w n="24.4">autour</w> <w n="24.5">d</w>’<w n="24.6">elle</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Au</w> <w n="25.2">milieu</w> <w n="25.3">du</w> <w n="25.4">grand</w> <w n="25.5">frisson</w></l>
						<l n="26" num="7.2"><w n="26.1">Indifférent</w> <w n="26.2">qui</w> <w n="26.3">la</w> <w n="26.4">foule</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Elle</w> <w n="27.2">est</w> <w n="27.3">seule</w> <w n="27.4">dans</w> <w n="27.5">la</w> <w n="27.6">foule</w></l>
						<l n="28" num="7.4"><w n="28.1">À</w> <w n="28.2">cause</w> <w n="28.3">de</w> <w n="28.4">sa</w> <w n="28.5">chanson</w></l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Toute</w> <w n="29.2">sainte</w> <w n="29.3">d</w>’<w n="29.4">impossible</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Elle</w> <w n="30.2">rentre</w> <w n="30.3">du</w> <w n="30.4">labeur</w></l>
						<l n="31" num="8.3"><w n="31.1">Égarée</w> <w n="31.2">et</w> <w n="31.3">l</w>’<w n="31.4">air</w> <w n="31.5">rêveur</w></l>
						<l n="32" num="8.4"><w n="32.1">Dans</w> <w n="32.2">la</w> <w n="32.3">musique</w> <w n="32.4">invisible</w>.</l>
					</lg>
				</div></body></text></TEI>