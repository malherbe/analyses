<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Pleureuses</title>
				<title type="medium">Édition électronique</title>
				<author key="BRS">
					<name>
						<forename>Henri</forename>
						<surname>BARBUSSE</surname>
					</name>
					<date from="1873" to="1935">1873-1935</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2269 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">BRS_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URI">https ://www.poesies.net/henribarbussepleureuses.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">https://archive.org/details/pleureusesposi00barbuoft</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Ernest Flammarion, éditeur</publisher>
							<date when="1920">1920</date>
						</imprint>
					</monogr>
					<note>
						l’édition de 1920 comporte une citation dans le poème "Dans le Passé", absente dans l’édition de 1895.
					</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">Les Pleureuses</title>
						<author>Henri Barbusse</author>
						<idno type="URI">http://gallica.bnf.fr/ark :/12148/bpt6k5719046r.r=henri%20barbusse%20les%20pleureuses ?rk=21459 ;2</idno>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>Bibliothèque Charpentier</publisher>
							<date when="1895">1895</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1895">1895</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-07" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-07" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE SILENCE DES PAUVRES</head><div type="poem" key="BRS52">
					<head type="main">LA COLÈRE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Sur le chemin…
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ton</w> <w n="1.2">droit</w> <w n="1.3">t</w>’<w n="1.4">éblouit</w> <w n="1.5">et</w> <w n="1.6">flamboie</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">cri</w> <w n="2.3">muet</w> <w n="2.4">gonfle</w> <w n="2.5">ton</w> <w n="2.6">cou</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">un</w> <w n="3.3">dieu</w> <w n="3.4">tu</w> <w n="3.5">vas</w> <w n="3.6">n</w>’<w n="3.7">importe</w> <w n="3.8">où</w></l>
						<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">ta</w> <w n="4.3">colère</w> <w n="4.4">et</w> <w n="4.5">ta</w> <w n="4.6">joie</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">le</w> <w n="5.4">calme</w> <w n="5.5">artificiel</w></l>
						<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">se</w> <w n="6.3">rompt</w> <w n="6.4">comme</w> <w n="6.5">un</w> <w n="6.6">pauvre</w> <w n="6.7">gage</w>,</l>
						<l n="7" num="2.3"><w n="7.1">C</w>’<w n="7.2">est</w> <w n="7.3">ta</w> <w n="7.4">haine</w> <w n="7.5">qui</w> <w n="7.6">se</w> <w n="7.7">dégage</w>,</l>
						<l n="8" num="2.4"><w n="8.1">C</w>’<w n="8.2">est</w> <w n="8.3">ta</w> <w n="8.4">haine</w> <w n="8.5">qui</w> <w n="8.6">monte</w> <w n="8.7">au</w> <w n="8.8">ciel</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Tes</w> <w n="9.2">pas</w> <w n="9.3">font</w> <w n="9.4">vaciller</w> <w n="9.5">le</w> <w n="9.6">monde</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Tes</w> <w n="10.2">raisons</w> <w n="10.3">t</w>’<w n="10.4">assaillent</w> <w n="10.5">en</w> <w n="10.6">chœur</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">tout</w> <w n="11.3">ton</w> <w n="11.4">sang</w> <w n="11.5">te</w> <w n="11.6">monte</w> <w n="11.7">au</w> <w n="11.8">cœur</w></l>
						<l n="12" num="3.4"><w n="12.1">Avec</w> <w n="12.2">sa</w> <w n="12.3">vérité</w> <w n="12.4">qui</w> <w n="12.5">gronde</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Le</w> <w n="13.2">vent</w> <w n="13.3">effare</w> <w n="13.4">tes</w> <w n="13.5">cheveux</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Tes</w> <w n="14.2">mains</w> <w n="14.3">tremblent</w> <w n="14.4">et</w> <w n="14.5">ta</w> <w n="14.6">voix</w> <w n="14.7">crie</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Ta</w> <w n="15.2">souffrance</w> <w n="15.3">devient</w> <w n="15.4">féerie</w>…</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">tu</w> <w n="16.3">ne</w> <w n="16.4">sais</w> <w n="16.5">plus</w>, <w n="16.6">et</w> <w n="16.7">tu</w> <w n="16.8">veux</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Perdu</w> <w n="17.2">dans</w> <w n="17.3">un</w> <w n="17.4">essor</w> <w n="17.5">d</w>’<w n="17.6">envie</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Sans</w> <w n="18.2">souvenir</w> <w n="18.3">et</w> <w n="18.4">sans</w> <w n="18.5">pitié</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Tu</w> <w n="19.2">te</w> <w n="19.3">redresses</w> <w n="19.4">tout</w> <w n="19.5">entier</w></l>
						<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">tu</w> <w n="20.3">ne</w> <w n="20.4">penses</w> <w n="20.5">qu</w>’<w n="20.6">à</w> <w n="20.7">la</w> <w n="20.8">vie</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Tout</w> <w n="21.2">t</w>’<w n="21.3">apparaît</w> <w n="21.4">dans</w> <w n="21.5">un</w> <w n="21.6">réveil</w> ;</l>
						<l n="22" num="6.2"><w n="22.1">Ton</w> <w n="22.2">cri</w> <w n="22.3">prolonge</w> <w n="22.4">l</w>’<w n="22.5">étendue</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Tu</w> <w n="23.2">sens</w> <w n="23.3">une</w> <w n="23.4">larme</w> <w n="23.5">éperdue</w></l>
						<l n="24" num="6.4"><w n="24.1">Qui</w> <w n="24.2">t</w>’<w n="24.3">illumine</w> <w n="24.4">le</w> <w n="24.5">soleil</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Ta</w> <w n="25.2">gloire</w> <w n="25.3">divague</w> <w n="25.4">et</w> <w n="25.5">se</w> <w n="25.6">creuse</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Ta</w> <w n="26.2">chair</w> <w n="26.3">t</w>’<w n="26.4">admire</w> <w n="26.5">en</w> <w n="26.6">frémissant</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Tu</w> <w n="27.2">n</w>’<w n="27.3">es</w> <w n="27.4">que</w> <w n="27.5">l</w>’<w n="27.6">hymne</w> <w n="27.7">de</w> <w n="27.8">ton</w> <w n="27.9">sang</w></l>
						<l n="28" num="7.4"><w n="28.1">Vers</w> <w n="28.2">la</w> <w n="28.3">lumière</w> <w n="28.4">bienheureuse</w> !</l>
					</lg>
				</div></body></text></TEI>