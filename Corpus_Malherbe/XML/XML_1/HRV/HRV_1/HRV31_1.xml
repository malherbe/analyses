<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV31">
					<head type="main">JUILLET</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ah</w> <w n="1.2">Dieu</w> !… <w n="1.3">combien</w> <w n="1.4">y</w> <w n="1.5">a</w>-<w n="1.6">t</w>-<w n="1.7">il</w> <w n="1.8">de</w> <w n="1.9">jours</w> <w n="1.10">en</w> <w n="1.11">juillet</w> ?</l>
						<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2">est</w> <w n="2.3">infini</w> ! <w n="2.4">Jour</w> <w n="2.5">après</w> <w n="2.6">jour</w>, <w n="2.7">jour</w> <w n="2.8">sur</w> <w n="2.9">journée</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Du</w> <w n="3.2">soleil</w> <w n="3.3">tous</w> <w n="3.4">les</w> <w n="3.5">jours</w> ! <w n="3.6">Et</w> <w n="3.7">la</w> <w n="3.8">fête</w> <w n="3.9">avinée</w></l>
						<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">bat</w> <w n="4.3">son</w> <w n="4.4">plein</w> : <w n="4.5">coups</w> <w n="4.6">de</w> <w n="4.7">tambour</w>, <w n="4.8">coups</w> <w n="4.9">de</w> <w n="4.10">maillet</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Jusqu</w>’<w n="5.2">à</w> <w n="5.3">l</w>’<w n="5.4">aube</w> <w n="5.5">aujourd</w>’<w n="5.6">hui</w>, <w n="5.7">tout</w> <w n="5.8">le</w> <w n="5.9">ciel</w> <w n="5.10">flamboyait</w></l>
						<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">lumières</w>, <w n="6.3">de</w> <w n="6.4">bruits</w>, <w n="6.5">de</w> <w n="6.6">chaleur</w> <w n="6.7">effrénée</w>…</l>
						<l n="7" num="2.3"><w n="7.1">Leur</w> <w n="7.2">spirale</w> <w n="7.3">diézée</w>, <w n="7.4">en</w> <w n="7.5">anneaux</w> <w n="7.6">contournés</w></l>
						<l n="8" num="2.4"><w n="8.1">Me</w> <w n="8.2">taraudait</w> <w n="8.3">le</w> <w n="8.4">cœur</w> <w n="8.5">de</w> <w n="8.6">son</w> <w n="8.7">rythme</w> <w n="8.8">inquiet</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">Tu</w> <w n="9.2">voudrais</w> <w n="9.3">oublier</w> <w n="9.4">ta</w> <w n="9.5">souffrance</w>, <w n="9.6">au</w> <w n="9.7">loin</w>, <w n="9.8">seule</w> ? »…</l>
						<l n="10" num="3.2">…<w n="10.1">Non</w> ! <w n="10.2">Non</w> !… <w n="10.3">Je</w> <w n="10.4">veux</w> <w n="10.5">pouvoir</w> <w n="10.6">m</w>’<w n="10.7">écraser</w> <w n="10.8">sous</w> <w n="10.9">sa</w> <w n="10.10">meule</w></l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">veux</w> <w n="11.3">pouvoir</w> <w n="11.4">saigner</w> <w n="11.5">de</w> <w n="11.6">son</w> <w n="11.7">poids</w> <w n="11.8">déchirant</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Sentir</w> <w n="12.2">crier</w> <w n="12.3">mes</w> <w n="12.4">os</w>… <w n="12.5">Jusqu</w>’<w n="12.6">à</w> <w n="12.7">la</w> <w n="12.8">plénitude</w></l>
						<l n="13" num="4.2"><w n="13.1">Souffrir</w> <w n="13.2">du</w> <w n="13.3">mal</w> <w n="13.4">qu</w>’<w n="13.5">il</w> <w n="13.6">m</w>’<w n="13.7">a</w> <w n="13.8">fait</w> !… <w n="13.9">Mais</w> <w n="13.10">lorsque</w> <w n="13.11">descend</w></l>
						<l n="14" num="4.3"><w n="14.1">La</w> <w n="14.2">pudeur</w> <w n="14.3">du</w> <w n="14.4">silence</w> <w n="14.5">et</w> <w n="14.6">de</w> <w n="14.7">la</w> <w n="14.8">solitude</w> !</l>
					</lg>
					<closer>
						<note id="" type="footnote">A la lecture, les deux sonnets, juillet, Août doivent s’enchaîner, Juillet,— Tous les diètes sans répit, rouge canaille, La sonorité "é", qui domine, pince le son, le remonte, analogue à l’altération dièze.</note>
					</closer>
				</div></body></text></TEI>