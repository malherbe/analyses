<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUIVANT L’HUMEUR DES JOURS</head><div type="poem" key="HRV21">
				<head type="main">RÉPLIQUE D’UNE HÉLÈNE</head>
				<opener>
					<epigraph>
						<cit>
							<quote>Quand yous serez bien vieille, au soir, à la chandelle, </quote>
							<bibl>
								<name>RONSARD</name> (Sonnet à Hélène).
							</bibl>
						</cit>
					</epigraph>
				</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">vous</w> <w n="1.3">serez</w> <w n="1.4">bien</w> <w n="1.5">vieux</w>, <w n="1.6">le</w> <w n="1.7">soir</w>, <w n="1.8">à</w> <w n="1.9">la</w> <w n="1.10">chandelle</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Assis</w> <w n="2.2">auprès</w> <w n="2.3">dit</w> <w n="2.4">feu</w>, <w n="2.5">tisonnant</w> <w n="2.6">et</w> <w n="2.7">rêvant</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Peut</w>-<w n="3.2">être</w> <w n="3.3">lirez</w>-<w n="3.4">vous</w> <w n="3.5">les</w> <w n="3.6">vers</w>, <w n="3.7">où</w> <w n="3.8">trop</w> <w n="3.9">souvent</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">vous</w> <w n="4.3">ai</w> <w n="4.4">célébré</w>, <w n="4.5">du</w> <w n="4.6">temps</w> <w n="4.7">que</w> <w n="4.8">j</w>’<w n="4.9">étois</w> <w n="4.10">belle</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Alors</w> <w n="5.2">vous</w> <w n="5.3">apprendrez</w>, <w n="5.4">ainsi</w> <w n="5.5">qu</w>’<w n="5.6">une</w> <w n="5.7">nouvelle</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Combien</w> <w n="6.2">je</w> <w n="6.3">vous</w> <w n="6.4">aimois</w>, <w n="6.5">et</w> <w n="6.6">que</w> <w n="6.7">mon</w>, <w n="6.8">cœur</w> <w n="6.9">fervent</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Malgré</w> <w n="7.2">vostre</w> <w n="7.3">rigueur</w> <w n="7.4">qui</w> <w n="7.5">l</w>’<w n="7.6">alloit</w> <w n="7.7">décevant</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Bénissoit</w> <w n="8.2">vostre</w> <w n="8.3">nom</w> <w n="8.4">de</w> <w n="8.5">louange</w> <w n="8.6">immortelle</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">il</w> <w n="9.3">sera</w> <w n="9.4">trop</w> <w n="9.5">tard</w> <w n="9.6">pour</w> <w n="9.7">respondre</w> <w n="9.8">aux</w> <w n="9.9">aveux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Vous</w> <w n="10.2">aurez</w> <w n="10.3">presque</w> <w n="10.4">atteint</w> <w n="10.5">les</w> <w n="10.6">ombrages</w> <w n="10.7">myrteux</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">ne</w> <w n="11.3">pourray</w> <w n="11.4">donner</w> <w n="11.5">de</w> <w n="11.6">désir</w> <w n="11.7">ni</w> <w n="11.8">d</w>’<w n="11.9">envie</w>…</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Ce</w> <w n="12.2">soir</w>-<w n="12.3">là</w> <w n="12.4">pleurerez</w>, <w n="12.5">le</w> <w n="12.6">front</w> <w n="12.7">dessus</w> <w n="12.8">la</w> <w n="12.9">main</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Regrettant</w> <w n="13.2">mon</w> <w n="13.3">amour</w> <w n="13.4">et</w> <w n="13.5">vostre</w> <w n="13.6">fier</w> <w n="13.7">desdain</w>,</l>
						<l n="14" num="4.3"><w n="14.1">De</w> <w n="14.2">n</w>’<w n="14.3">avoir</w> <w n="14.4">pas</w> <w n="14.5">cueilli</w> <w n="14.6">les</w> <w n="14.7">roses</w> <w n="14.8">de</w> <w n="14.9">ma</w> <w n="14.10">vie</w> !</l>
					</lg>
				</div></body></text></TEI>