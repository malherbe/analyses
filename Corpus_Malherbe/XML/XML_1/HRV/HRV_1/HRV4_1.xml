<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUIVANT L’HUMEUR DES JOURS</head><div type="poem" key="HRV4">
				<head type="main">LES LYS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Vous</w> <w n="1.2">vous</w> <w n="1.3">souvenez</w> <w n="1.4">bien</w> <w n="1.5">de</w> <w n="1.6">ce</w> <w n="1.7">chaud</w> <w n="1.8">jour</w> <w n="1.9">d</w>’<w n="1.10">été</w> ?…</l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">grands</w> <w n="2.3">bouquets</w> <w n="2.4">de</w> <w n="2.5">lys</w> <w n="2.6">illuminaient</w> <w n="2.7">la</w> <w n="2.8">chambre</w></l>
						<l n="3" num="1.3"><w n="3.1">Obscure</w> <w n="3.2">et</w> <w n="3.3">fraîche</w> <w n="3.4">un</w> <w n="3.5">peu</w>… <w n="3.6">J</w>’<w n="3.7">avais</w> <w n="3.8">mon</w> <w n="3.9">collier</w> <w n="3.10">d</w>’<w n="3.11">ambre</w>…</l>
						<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">y</w> <w n="4.3">a</w> <w n="4.4">presque</w> <w n="4.5">un</w> <w n="4.6">an</w>… <w n="4.7">mon</w> <w n="4.8">cœur</w> <w n="4.9">est</w> <w n="4.10">arrêté</w></l>
						<l n="5" num="1.5"><w n="5.1">Depuis</w>… <w n="5.2">Des</w> <w n="5.3">arbres</w> <w n="5.4">en</w> <w n="5.5">fleur</w> <w n="5.6">l</w>’<w n="5.7">odeur</w> <w n="5.8">pénétrante</w></l>
						<l n="6" num="1.6"><w n="6.1">Montait</w> <w n="6.2">par</w> <w n="6.3">la</w> <w n="6.4">fenêtre</w>, <w n="6.5">avec</w> <w n="6.6">la</w> <w n="6.7">plainte</w> <w n="6.8">lente</w></l>
						<l n="7" num="1.7"><w n="7.1">Des</w> <w n="7.2">pigeons</w> <w n="7.3">du</w> <w n="7.4">jardin</w>… <w n="7.5">L</w>’<w n="7.6">orage</w> <w n="7.7">menaçait</w>…</l>
						<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">distraits</w>, <w n="8.3">nous</w> <w n="8.4">parlions</w>… <w n="8.5">De</w> <w n="8.6">quoi</w> ?… <w n="8.7">Est</w>-<w n="8.8">ce</w> <w n="8.9">qu</w>’<w n="8.10">on</w> <w n="8.11">sait</w> !</l>
						<l n="9" num="1.9"><w n="9.1">De</w> <w n="9.2">tout</w>, <w n="9.3">et</w> <w n="9.4">puis</w> <w n="9.5">de</w> <w n="9.6">rien</w>… <w n="9.7">Du</w> <w n="9.8">soleil</w>… <w n="9.9">de</w> <w n="9.10">l</w>’<w n="9.11">ondée</w>…</l>
						<l n="10" num="1.10"><w n="10.1">Je</w> <w n="10.2">crois</w> <w n="10.3">vous</w> <w n="10.4">avoir</w> <w n="10.5">dit</w> : « <w n="10.6">Ces</w> <w n="10.7">lys</w> <w n="10.8">sentent</w> <w n="10.9">bien</w> <w n="10.10">fort</w> »…</l>
						<l n="11" num="1.11"><w n="11.1">Sans</w> <w n="11.2">répondre</w> <w n="11.3">à</w> <w n="11.4">cela</w> <w n="11.5">vous</w> <w n="11.6">m</w>’<w n="11.7">avez</w> <w n="11.8">regardée</w></l>
						<l n="12" num="1.12"><w n="12.1">D</w>’<w n="12.2">un</w> <w n="12.3">regard</w> <w n="12.4">si</w> <w n="12.5">pesant</w>… <w n="12.6">que</w>, <w n="12.7">presque</w> <w n="12.8">avec</w> <w n="12.9">effort</w>,</l>
						<l n="13" num="1.13"><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">détourné</w> <w n="13.4">les</w> <w n="13.5">yeux</w>… <w n="13.6">Il</w> <w n="13.7">y</w> <w n="13.8">eût</w> <w n="13.9">un</w> <w n="13.10">silence</w></l>
						<l n="14" num="1.14"><w n="14.1">De</w> <w n="14.2">mortelle</w> <w n="14.3">douceur</w>, <w n="14.4">glissant</w> <w n="14.5">entre</w> <w n="14.6">nous</w> <w n="14.7">deux</w></l>
						<l n="15" num="1.15"><w n="15.1">Avec</w> <w n="15.2">l</w>’<w n="15.3">odeur</w> <w n="15.4">des</w> <w n="15.5">lys</w>, <w n="15.6">et</w> <w n="15.7">comme</w> <w n="15.8">émané</w> <w n="15.9">d</w>’<w n="15.10">eux</w>…</l>
						<l n="16" num="1.16"><w n="16.1">Le</w> <w n="16.2">temps</w> <w n="16.3">ne</w> <w n="16.4">fuyait</w> <w n="16.5">plus</w>, <w n="16.6">car</w> <w n="16.7">la</w> <w n="16.8">tendre</w> <w n="16.9">assonance</w></l>
						<l n="17" num="1.17"><w n="17.1">Des</w> <w n="17.2">mots</w> <w n="17.3">inexprimés</w> <w n="17.4">suspendait</w> <w n="17.5">sort</w> <w n="17.6">essor</w>…</l>
						<l n="18" num="1.18"><w n="18.1">Je</w> <w n="18.2">défaillais</w>… <w n="18.3">Pourquoi</w> ?… <w n="18.4">Nous</w> <w n="18.5">n</w>’<w n="18.6">avions</w> <w n="18.7">aucun</w> <w n="18.8">tort</w>…</l>
						<l n="19" num="1.19"><w n="19.1">Je</w> <w n="19.2">n</w>’<w n="19.3">oublierai</w> <w n="19.4">jamais</w> <w n="19.5">cet</w> <w n="19.6">instant</w> <w n="19.7">de</w> <w n="19.8">silence</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Où</w>, <w n="20.2">sans</w> <w n="20.3">m</w>’<w n="20.4">en</w> <w n="20.5">rendre</w> <w n="20.6">compte</w>, <w n="20.7">en</w> <w n="20.8">complète</w> <w n="20.9">innocence</w>,</l>
						<l n="21" num="1.21"><w n="21.1">J</w>’<w n="21.2">ai</w> <w n="21.3">ressenti</w> <w n="21.4">l</w>’<w n="21.5">amour</w>… <w n="21.6">et</w> <w n="21.7">vous</w>, <w n="21.8">vous</w> <w n="21.9">le</w> <w n="21.10">saviez</w>…</l>
						<l n="22" num="1.22"><w n="22.1">Trouble</w> <w n="22.2">délicieux</w> <w n="22.3">dont</w> <w n="22.4">nous</w> <w n="22.5">fûmes</w> <w n="22.6">liés</w></l>
						<l n="23" num="1.23"><w n="23.1">Sans</w> <w n="23.2">nous</w> <w n="23.3">être</w> <w n="23.4">rien</w> <w n="23.5">dit</w> ! <w n="23.6">Emprise</w> <w n="23.7">ineffaçable</w></l>
						<l n="24" num="1.24"><w n="24.1">D</w>’<w n="24.2">avoir</w> <w n="24.3">été</w> <w n="24.4">muette</w> !… <w n="24.5">Ah</w> ! <w n="24.6">béni</w> <w n="24.7">soit</w> <w n="24.8">le</w> <w n="24.9">dieu</w></l>
						<l n="25" num="1.25"><w n="25.1">Qui</w> <w n="25.2">fit</w>, <w n="25.3">dans</w> <w n="25.4">un</w> <w n="25.5">instant</w> <w n="25.6">de</w> <w n="25.7">notre</w> <w n="25.8">vie</w> <w n="25.9">instable</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Tenir</w> <w n="26.2">tout</w> <w n="26.3">l</w>’<w n="26.4">univers</w>, <w n="26.5">pour</w> <w n="26.6">nos</w> <w n="26.7">cœurs</w>, <w n="26.8">dans</w> <w n="26.9">ce</w> <w n="26.10">lieu</w> !</l>
						<l n="27" num="1.27"><w n="27.1">Rien</w> <w n="27.2">ne</w> <w n="27.3">s</w>’<w n="27.4">était</w> <w n="27.5">passé</w>… <w n="27.6">du</w> <w n="27.7">moins</w>, <w n="27.8">si</w> <w n="27.9">peu</w> <w n="27.10">de</w> <w n="27.11">chose</w>…</l>
						<l n="28" num="1.28"><w n="28.1">Ce</w> <w n="28.2">fut</w>, <w n="28.3">dans</w> <w n="28.4">le</w> <w n="28.5">tourment</w> <w n="28.6">des</w> <w n="28.7">jours</w>, <w n="28.8">comme</w> <w n="28.9">une</w> <w n="28.10">pause</w>.</l>
						<l n="29" num="1.29"><w n="29.1">Depuis</w>, <w n="29.2">l</w>’<w n="29.3">amour</w> <w n="29.4">a</w> <w n="29.5">toujours</w> <w n="29.6">eu</w>, <w n="29.7">pour</w> <w n="29.8">moi</w>, <w n="29.9">l</w>’<w n="29.10">odeur</w></l>
						<l n="30" num="1.30"><w n="30.1">D</w>’<w n="30.2">un</w> <w n="30.3">jour</w> <w n="30.4">d</w>’<w n="30.5">été</w>, <w n="30.6">d</w>’<w n="30.7">orage</w>, <w n="30.8">avec</w> <w n="30.9">des</w> <w n="30.10">lys</w> <w n="30.11">en</w> <w n="30.12">fleur</w>…</l>
					</lg>
				</div></body></text></TEI>