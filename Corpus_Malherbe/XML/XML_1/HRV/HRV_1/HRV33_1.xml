<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV33">
					<head type="main">SEPTEMBRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Regarde</w> !… <w n="1.2">Voici</w> <w n="1.3">Septembre</w> <w n="1.4">vibrant</w> <w n="1.5">d</w>’<w n="1.6">émoi</w></l>
						<l n="2" num="1.2"><w n="2.1">Sur</w> <w n="2.2">ces</w> <w n="2.3">sombres</w> <w n="2.4">cheveux</w> <w n="2.5">un</w> <w n="2.6">plateau</w> <w n="2.7">d</w>’<w n="2.8">or</w> <w n="2.9">flamboie</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Chargé</w> <w n="3.2">des</w> <w n="3.3">fruits</w> <w n="3.4">de</w> <w n="3.5">son</w> <w n="3.6">été</w> ! <w n="3.7">Toute</w> <w n="3.8">la</w> <w n="3.9">joie</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">son</w> <w n="4.3">pas</w> <w n="4.4">rythmique</w> <w n="4.5">et</w> <w n="4.6">dansant</w> <w n="4.7">vient</w> <w n="4.8">jusqu</w>’<w n="4.9">à</w> <w n="4.10">toi</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ses</w> <w n="5.2">yeux</w> <w n="5.3">et</w> <w n="5.4">le</w> <w n="5.5">sourire</w> <w n="5.6">en</w> <w n="5.7">son</w> <w n="5.8">visage</w> <w n="5.9">étroit</w></l>
						<l n="6" num="2.2"><w n="6.1">Disent</w> <w n="6.2">l</w>’<w n="6.3">amour</w>, <w n="6.4">avant</w> <w n="6.5">que</w> <w n="6.6">son</w> <w n="6.7">genou</w> <w n="6.8">ne</w> <w n="6.9">ploie</w></l>
						<l n="7" num="2.3"><w n="7.1">Pour</w> <w n="7.2">mieux</w> <w n="7.3">t</w>’<w n="7.4">offrir</w> <w n="7.5">sa</w> <w n="7.6">charge</w>… <w n="7.7">elle</w>-<w n="7.8">même</w> <w n="7.9">une</w> <w n="7.10">proie</w></l>
						<l n="8" num="2.4"><w n="8.1">Sauvagement</w> <w n="8.2">soumise</w> <w n="8.3">au</w> <w n="8.4">maître</w> <w n="8.5">de</w> <w n="8.6">son</w> <w n="8.7">choix</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Entends</w>-<w n="9.2">tu</w> <w n="9.3">résonner</w> <w n="9.4">les</w> <w n="9.5">couleurs</w>, <w n="9.6">les</w> <w n="9.7">aromes</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">larmes</w> <w n="10.3">des</w> <w n="10.4">raisins</w> <w n="10.5">et</w> <w n="10.6">le</w> <w n="10.7">rire</w> <w n="10.8">des</w> <w n="10.9">pommes</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">cent</w> <w n="11.3">grains</w> <w n="11.4">de</w> <w n="11.5">grenade</w> <w n="11.6">et</w> <w n="11.7">le</w> <w n="11.8">grand</w> <w n="11.9">plateau</w> <w n="11.10">d</w>’<w n="11.11">or</w> ?</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Ils</w> <w n="12.2">roulent</w> <w n="12.3">à</w> <w n="12.4">nos</w> <w n="12.5">pieds</w>… <w n="12.6">la</w> <w n="12.7">terre</w> <w n="12.8">en</w> <w n="12.9">est</w> <w n="12.10">jonchée</w>…</l>
						<l n="13" num="4.2">…<w n="13.1">Je</w> <w n="13.2">suis</w> <w n="13.3">entre</w> <w n="13.4">tes</w> <w n="13.5">bras</w>, <w n="13.6">qui</w> <w n="13.7">meurtrissent</w> <w n="13.8">encor</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Le</w> <w n="14.2">verger</w> <w n="14.3">qu</w>’<w n="14.4">on</w> <w n="14.5">saccage</w> <w n="14.6">et</w> <w n="14.7">la</w> <w n="14.8">vigne</w> <w n="14.9">arrachée</w>…</l>
					</lg>
					<closer>
						<note type="footnote" id="">La bémol majeur qui possède comme une sorte de motricité spéciale par ses quatre bémols bien équilibrés dans une large pulsation vitale.</note>
					</closer>
				</div></body></text></TEI>