<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV38">
				<head type="main">FÉVRIER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">t</w>’<w n="1.3">attends</w> <w n="1.4">depuis</w> <w n="1.5">si</w> <w n="1.6">longtemps</w> <w n="1.7">que</w> <w n="1.8">Février</w></l>
						<l n="2" num="1.2"><w n="2.1">Est</w> <w n="2.2">de</w> <w n="2.3">retour</w>… <w n="2.4">Par</w> <w n="2.5">ce</w> <w n="2.6">mois</w> <w n="2.7">si</w> <w n="2.8">tendre</w> <w n="2.9">et</w> <w n="2.10">si</w> <w n="2.11">tiède</w></l>
						<l n="3" num="1.3"><w n="3.1">Ne</w> <w n="3.2">nous</w> <w n="3.3">défendons</w> <w n="3.4">plus</w>… <w n="3.5">Que</w> <w n="3.6">ta</w> <w n="3.7">volonté</w> <w n="3.8">cède</w></l>
						<l n="4" num="1.4"><w n="4.1">A</w> <w n="4.2">la</w> <w n="4.3">caresse</w> <w n="4.4">adorable</w> <w n="4.5">du</w> <w n="4.6">vent</w> <w n="4.7">mouillé</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Ce</w> <w n="5.2">vent</w> <w n="5.3">d</w>’<w n="5.4">ouest</w>, <w n="5.5">en</w> <w n="5.6">sol</w> <w n="5.7">mineur</w>, <w n="5.8">plein</w> <w n="5.9">de</w> <w n="5.10">pitié</w></l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">d</w>’<w n="6.3">abandon</w>, <w n="6.4">le</w> <w n="6.5">sens</w>-<w n="6.6">tu</w> <w n="6.7">m</w>’<w n="6.8">apporter</w> <w n="6.9">en</w> <w n="6.10">aide</w></l>
						<l n="7" num="2.3"><w n="7.1">L</w>’<w n="7.2">odeur</w> <w n="7.3">d</w>’<w n="7.4">algue</w> <w n="7.5">et</w> <w n="7.6">de</w> <w n="7.7">violette</w> <w n="7.8">qui</w> <w n="7.9">t</w>’<w n="7.10">obsède</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Qui</w> <w n="8.2">te</w> <w n="8.3">poursuit</w>, <w n="8.4">t</w>’<w n="8.5">enivre</w>… <w n="8.6">et</w> <w n="8.7">qui</w> <w n="8.8">va</w> <w n="8.9">nous</w> <w n="8.10">lier</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Non</w> ! <w n="9.2">Ne</w> <w n="9.3">résiste</w> <w n="9.4">plus</w> ! <w n="9.5">Fais</w> <w n="9.6">l</w>’<w n="9.7">effort</w> <w n="9.8">de</w> <w n="9.9">ton</w> <w n="9.10">geste</w></l>
						<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">me</w> <w n="10.3">repousser</w> <w n="10.4">loin</w> <w n="10.5">de</w> <w n="10.6">toi</w>… <w n="10.7">mais</w>, <w n="10.8">dis</w>-<w n="10.9">moi</w> « <w n="10.10">Reste</w> !»</l>
						<l n="11" num="3.3">…<w n="11.1">Ton</w> <w n="11.2">bras</w> <w n="11.3">tendu</w> <w n="11.4">suspendra</w> <w n="11.5">l</w>’<w n="11.6">instant</w> <w n="11.7">d</w>’<w n="11.8">être</w> <w n="11.9">heureux</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Comme</w> <w n="12.2">l</w>’<w n="12.3">écart</w> <w n="12.4">sensible</w> <w n="12.5">et</w> <w n="12.6">tremblant</w> <w n="12.7">du</w> <w n="12.8">fa</w> <w n="12.9">dièze</w>…</l>
						<l n="13" num="4.2"><w n="13.1">Puis</w>… <w n="13.2">sol</w>… <w n="13.3">à</w> <w n="13.4">l</w>’<w n="13.5">unisson</w> <w n="13.6">de</w> <w n="13.7">mon</w> <w n="13.8">cœur</w> <w n="13.9">à</w> <w n="13.10">tes</w> <w n="13.11">yeux</w>…</l>
						<l n="14" num="4.3"><w n="14.1">La</w> <w n="14.2">proche</w> <w n="14.3">résolution</w> <w n="14.4">qui</w> <w n="14.5">nous</w> <w n="14.6">apaise</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date to="1922" from="1921">Mars 1921 ‒ Février 1922</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>