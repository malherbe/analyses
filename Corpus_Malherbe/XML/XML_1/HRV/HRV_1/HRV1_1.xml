<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="HRV1">
				<head type="main">DÉDICACE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Pourquoi</w> <w n="1.2">tracer</w> <w n="1.3">ton</w> <w n="1.4">nom</w> <w n="1.5">ici</w>, <w n="1.6">puisqu</w>’<w n="1.7">à</w> <w n="1.8">voix</w> <w n="1.9">basse</w></l>
					<l n="2" num="1.2"><w n="2.1">Tu</w> <w n="2.2">m</w>’<w n="2.3">entendras</w>, <w n="2.4">à</w> <w n="2.5">chaque</w> <w n="2.6">page</w>, <w n="2.7">le</w> <w n="2.8">crier</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">Qu</w>’<w n="3.2">est</w>-<w n="3.3">ce</w> <w n="3.4">donc</w> <w n="3.5">que</w> <w n="3.6">l</w>’<w n="3.7">amour</w> <w n="3.8">sinon</w> <w n="3.9">la</w> <w n="3.10">dédicace</w></l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">consacre</w> <w n="4.3">à</w> <w n="4.4">l</w>’<w n="4.5">élu</w> <w n="4.6">mon</w> <w n="4.7">être</w> <w n="4.8">tout</w> <w n="4.9">entier</w> ?</l>
					<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">cet</w> <w n="5.3">amour</w> <w n="5.4">c</w>’<w n="5.5">est</w> <w n="5.6">le</w> <w n="5.7">chef</w> -<w n="5.8">d</w>’<w n="5.9">œuvre</w> <w n="5.10">de</w> <w n="5.11">mon</w> <w n="5.12">âme</w> !</l>
					<l n="6" num="1.6"><w n="6.1">Elle</w> <w n="6.2">y</w> <w n="6.3">est</w> <w n="6.4">tout</w> <w n="6.5">entière</w> <w n="6.6">et</w> <w n="6.7">donne</w> <w n="6.8">son</w> <w n="6.9">accent</w></l>
					<l n="7" num="1.7"><w n="7.1">A</w> <w n="7.2">la</w> <w n="7.3">forme</w> <w n="7.4">multiple</w>, <w n="7.5">aux</w> <w n="7.6">rythmes</w> <w n="7.7">différents</w></l>
					<l n="8" num="1.8"><w n="8.1">Qu</w>’<w n="8.2">a</w> <w n="8.3">pu</w> <w n="8.4">prendre</w> <w n="8.5">mon</w> <w n="8.6">rêve</w> ! <w n="8.7">Elle</w> <w n="8.8">prête</w> <w n="8.9">sa</w> <w n="8.10">trame</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Vibrante</w> <w n="9.2">et</w> <w n="9.3">douloureuse</w>, <w n="9.4">à</w> <w n="9.5">l</w>’<w n="9.6">ondoyant</w> <w n="9.7">tissu</w></l>
					<l n="10" num="1.10"><w n="10.1">Que</w> <w n="10.2">j</w>’<w n="10.3">ai</w> <w n="10.4">tenté</w> <w n="10.5">d</w>’<w n="10.6">ourdir</w> <w n="10.7">avec</w> <w n="10.8">l</w>’<w n="10.9">eau</w> <w n="10.10">des</w> <w n="10.11">fontaines</w></l>
					<l n="11" num="1.11"><w n="11.1">Dont</w> <w n="11.2">la</w> <w n="11.3">verte</w> <w n="11.4">fraîcheur</w>, <w n="11.5">au</w> <w n="11.6">fond</w> <w n="11.7">des</w> <w n="11.8">bois</w> <w n="11.9">moussus</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Pouvait</w> <w n="12.2">désaltérer</w> <w n="12.3">la</w> <w n="12.4">fièvre</w> <w n="12.5">de</w> <w n="12.6">mes</w> <w n="12.7">veines</w> ;</l>
					<l n="13" num="1.13"><w n="13.1">Avec</w> <w n="13.2">la</w> <w n="13.3">cruauté</w> <w n="13.4">sans</w> <w n="13.5">répit</w> <w n="13.6">du</w> <w n="13.7">soleil</w></l>
					<l n="14" num="1.14"><w n="14.1">Un</w> <w n="14.2">jour</w> ’<w n="14.3">de</w> <w n="14.4">peine</w> ; <w n="14.5">avec</w>, <w n="14.6">angoissante</w> <w n="14.7">au</w> <w n="14.8">sommeil</w>,</l>
					<l n="15" num="1.15"><w n="15.1">La</w> <w n="15.2">douceur</w> <w n="15.3">d</w>’<w n="15.4">un</w> <w n="15.5">tilleul</w>, <w n="15.6">la</w> <w n="15.7">volupté</w> <w n="15.8">des</w> <w n="15.9">roses</w></l>
					<l n="16" num="1.16"><w n="16.1">Trop</w> <w n="16.2">ouvertes</w> <w n="16.3">et</w> <w n="16.4">que</w> <w n="16.5">la</w> <w n="16.6">nuit</w> <w n="16.7">va</w> <w n="16.8">défleurir</w>…</l>
					<l n="17" num="1.17"><w n="17.1">Car</w> <w n="17.2">ce</w> <w n="17.3">n</w>’<w n="17.4">est</w> <w n="17.5">qu</w>’<w n="17.6">à</w> <w n="17.7">travers</w> <w n="17.8">le</w> <w n="17.9">prisme</w> <w n="17.10">du</w> <w n="17.11">désir</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Ou</w> <w n="18.2">celui</w> <w n="18.3">du</w> <w n="18.4">regret</w>, <w n="18.5">que</w> <w n="18.6">j</w>’<w n="18.7">ai</w> <w n="18.8">vu</w> <w n="18.9">toutes</w> <w n="18.10">choses</w> !</l>
					<l n="19" num="1.19"><w n="19.1">Je</w> <w n="19.2">n</w>’<w n="19.3">ai</w> <w n="19.4">parlé</w> <w n="19.5">d</w>’<w n="19.6">amour</w> <w n="19.7">qu</w>’<w n="19.8">à</w> <w n="19.9">l</w>’<w n="19.10">appel</w> <w n="19.11">de</w> <w n="19.12">ta</w> <w n="19.13">voix</w>,</l>
					<l n="20" num="1.20"><w n="20.1">Je</w> <w n="20.2">n</w>’<w n="20.3">ai</w> <w n="20.4">dit</w> <w n="20.5">ma</w> <w n="20.6">douleur</w> <w n="20.7">qu</w>’<w n="20.8">afin</w> <w n="20.9">qu</w>’<w n="20.10">il</w> <w n="20.11">t</w>’<w n="20.12">en</w> <w n="20.13">souvienne</w></l>
					<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">ces</w> <w n="21.3">vers</w> <w n="21.4">sont</w> <w n="21.5">ton</w> <w n="21.6">œuvre</w> <w n="21.7">encor</w> <w n="21.8">plus</w> <w n="21.9">que</w> <w n="21.10">la</w> <w n="21.11">mienne</w> !</l>
					<l n="22" num="1.22"><w n="22.1">Qu</w>’<w n="22.2">ont</w>-<w n="22.3">ils</w> <w n="22.4">besoin</w> <w n="22.5">de</w> <w n="22.6">dédicace</w> ?… <w n="22.7">Ils</w> <w n="22.8">sont</w> <w n="22.9">à</w> <w n="22.10">toi</w></l>
					<l n="23" num="1.23"><w n="23.1">Ainsi</w> <w n="23.2">qu</w>’<w n="23.3">un</w> <w n="23.4">fruit</w> <w n="23.5">sauvage</w> <w n="23.6">ou</w> <w n="23.7">qu</w>’<w n="23.8">une</w> <w n="23.9">tendre</w> <w n="23.10">feuille</w></l>
					<l n="24" num="1.24"><w n="24.1">N</w>’<w n="24.2">appartiennent</w> <w n="24.3">vraiment</w> <w n="24.4">qu</w>’<w n="24.5">à</w> <w n="24.6">celui</w> <w n="24.7">qui</w> <w n="24.8">les</w> <w n="24.9">cueille</w></l>
					<l n="25" num="1.25"><w n="25.1">Et</w> <w n="25.2">n</w>’<w n="25.3">embaument</w> <w n="25.4">la</w> <w n="25.5">main</w> <w n="25.6">que</w> <w n="25.7">lorsqu</w>’<w n="25.8">ils</w> <w n="25.9">sont</w> <w n="25.10">meurtris</w> !</l>
				</lg>
				<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . </ab>
				<lg n="2">
					<l n="26" num="2.1"><w n="26.1">Ton</w> <w n="26.2">nom</w> ?… <w n="26.3">Pourquoi</w> ?… <w n="26.4">Je</w> <w n="26.5">le</w> <w n="26.6">sais</w>, <w n="26.7">et</w> <w n="26.8">tu</w> <w n="26.9">l</w>’<w n="26.10">as</w> <w n="26.11">compris</w>…</l>
				</lg>
			</div></body></text></TEI>