<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV30">
					<head type="main">JUIN</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">éprouvions</w> <w n="1.3">tous</w> <w n="1.4">deux</w> <w n="1.5">un</w> <w n="1.6">sentiment</w> <w n="1.7">d</w>’<w n="1.8">attente</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Par</w> <w n="2.2">ce</w> <w n="2.3">ciel</w> <w n="2.4">lourd</w> <w n="2.5">et</w> <w n="2.6">chaud</w>, <w n="2.7">c</w>’<w n="2.8">était</w> <w n="2.9">au</w> <w n="2.10">mois</w> <w n="2.11">de</w> <w n="2.12">Juin</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">j</w>’<w n="3.3">écoutais</w>, <w n="3.4">à</w> <w n="3.5">travers</w> <w n="3.6">le</w> <w n="3.7">sens</w> <w n="3.8">incertain</w></l>
						<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">mots</w> <w n="4.3">que</w> <w n="4.4">nous</w> <w n="4.5">disions</w>, <w n="4.6">la</w> <w n="4.7">pédale</w> <w n="4.8">constante</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Que</w> <w n="5.2">tenaient</w> <w n="5.3">nos</w> <w n="5.4">deux</w> <w n="5.5">cœurs</w>… <w n="5.6">Ah</w> ! <w n="5.7">je</w> <w n="5.8">sais</w> <w n="5.9">qu</w>’<w n="5.10">il</w> <w n="5.11">te</w> <w n="5.12">hante</w></l>
						<l n="6" num="2.2"><w n="6.1">Aussi</w>, <w n="6.2">l</w>’<w n="6.3">appel</w> <w n="6.4">réitéré</w> <w n="6.5">comme</w> <w n="6.6">un</w> <w n="6.7">tocsin</w></l>
						<l n="7" num="2.3"><w n="7.1">Du</w> <w n="7.2">do</w> <w n="7.3">bémol</w> <w n="7.4">si</w> <w n="7.5">doux</w>, <w n="7.6">qui</w> <w n="7.7">cherche</w>, <w n="7.8">et</w> <w n="7.9">cherche</w> <w n="7.10">en</w> <w n="7.11">vain</w>,</l>
						<l n="8" num="2.4"><w n="8.1">La</w> <w n="8.2">fusion</w> <w n="8.3">de</w> <w n="8.4">son</w> <w n="8.5">accord</w> <w n="8.6">de</w> <w n="8.7">dominante</w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Mais</w> <w n="9.2">ta</w> <w n="9.3">voix</w> <w n="9.4">s</w>’<w n="9.5">est</w> <w n="9.6">brisée</w> <w n="9.7">au</w> <w n="9.8">bord</w> <w n="9.9">de</w> <w n="9.10">tes</w> <w n="9.11">aveux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">voilà</w> <w n="10.3">que</w> <w n="10.4">moi</w>, <w n="10.5">j</w>’<w n="10.6">ai</w> <w n="10.7">des</w> <w n="10.8">larmes</w> <w n="10.9">dans</w> <w n="10.10">les</w> <w n="10.11">yeux</w>…</l>
						<l n="11" num="3.3"><w n="11.1">Minute</w> <w n="11.2">étrange</w> <w n="11.3">où</w> <w n="11.4">j</w>’<w n="11.5">ai</w> <w n="11.6">peur</w> <w n="11.7">de</w> <w n="11.8">ce</w> <w n="11.9">que</w> <w n="11.10">j</w>’<w n="11.11">inspire</w>…</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">De</w> <w n="12.2">ce</w> <w n="12.3">que</w> <w n="12.4">j</w>’<w n="12.5">éprouve</w> <w n="12.6">aussi</w>. <w n="12.7">Mais</w> <w n="12.8">rien</w> <w n="12.9">ne</w> <w n="12.10">traduit</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Qu</w>’<w n="13.2">un</w> <w n="13.3">long</w> <w n="13.4">regard</w>, <w n="13.5">tout</w> <w n="13.6">ce</w> <w n="13.7">combat</w> <w n="13.8">qui</w> <w n="13.9">nous</w> <w n="13.10">déchire</w>…</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">brusquement</w>… <w n="14.3">je</w> <w n="14.4">t</w>’<w n="14.5">ai</w> <w n="14.6">quitté</w> <w n="14.7">comme</w> <w n="14.8">on</w> <w n="14.9">s</w>’<w n="14.10">enfuit</w>…</l>
					</lg>
					<closer>
						<note type="footnote" id="">La gamme descendante de mi bémol mineur, avec l’attirance du sixième degré vers la dominante.</note>
					</closer>
				</div></body></text></TEI>