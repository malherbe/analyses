<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
				<change when="2023-06-20" who="RR" type="analyse">Étape 7 de l'analyse automatique du corpus : Traitement des rimes, des strophes et de la forme globale.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">TRIPTYQUE</head><div type="poem" key="HRV55" rhyme="none">
					<head type="number">III</head>
					<head type="main">LA CATHÉDRALE</head>
					<head type="sub_1">(Amiens)</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									"Seigneur j’ai aimé la beauté de votre maison <lb></lb>
									et le lieu où habite votre gloire"
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Aussi</w> <w n="1.2">loin</w> <w n="1.3">que</w> <w n="1.4">tu</w> <w n="1.5">sois</w>, <w n="1.6">c</w>’<w n="1.7">est</w> <w n="1.8">ici</w> <w n="1.9">ta</w> <w n="1.10">maison</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Et</w> <w n="2.2">le</w> <w n="2.3">lieu</w> <w n="2.4">qu</w>’<w n="2.5">habité</w> <w n="2.6">ta</w> <w n="2.7">gloire</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Car</w> <w n="3.2">j</w>’<w n="3.3">ai</w> <w n="3.4">su</w> <w n="3.5">te</w> <w n="3.6">contraindre</w>, <w n="3.7">à</w> <w n="3.8">force</w> <w n="3.9">d</w>’<w n="3.10">oraisons</w>,</l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">A</w> <w n="4.2">t</w>’<w n="4.3">intégrer</w> <w n="4.4">à</w> <w n="4.5">ma</w> <w n="4.6">mémoire</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Pleur</w> <w n="5.2">à</w> <w n="5.3">pleur</w>, <w n="5.4">peine</w> <w n="5.5">à</w> <w n="5.6">peine</w>, <w n="5.7">et</w> <w n="5.8">combat</w> <w n="5.9">a</w> <w n="5.10">combat</w>,</l>
						<l n="6" num="1.6"><w n="6.1">J</w>’<w n="6.2">ai</w> <w n="6.3">créé</w> <w n="6.4">dans</w> <w n="6.5">mon</w> <w n="6.6">âme</w> <w n="6.7">une</w> <w n="6.8">église</w> <w n="6.9">idéale</w></l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Où</w> <w n="7.2">tu</w> <w n="7.3">reçois</w>, <w n="7.4">sans</w> <w n="7.5">intervalles</w>,</l>
						<l n="8" num="1.8"><w n="8.1">L</w>’<w n="8.2">hommage</w> <w n="8.3">de</w> <w n="8.4">deux</w> <w n="8.5">chants</w> <w n="8.6">dont</w> <w n="8.7">la</w> <w n="8.8">force</w> <w n="8.9">est</w> <w n="8.10">égale</w></l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Dies</w> <w n="9.2">iræ</w> ! <w n="9.3">Alléluia</w> !</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1"><w n="10.1">Chant</w> <w n="10.2">de</w> <w n="10.3">mort</w>, <w n="10.4">chant</w> <w n="10.5">de</w> <w n="10.6">vie</w>, <w n="10.7">hymnes</w> <w n="10.8">d</w>’<w n="10.9">amour</w> <w n="10.10">tous</w> <w n="10.11">deux</w>,</l>
						<l n="11" num="2.2"><space unit="char" quantity="8"></space><w n="11.1">Qui</w> <w n="11.2">maintenant</w> <w n="11.3">se</w> <w n="11.4">réunissent</w></l>
						<l n="12" num="2.3"><w n="12.1">Sous</w> <w n="12.2">la</w> <w n="12.3">voûte</w> <w n="12.4">achevée</w> <w n="12.5">où</w> <w n="12.6">tu</w> <w n="12.7">règnes</w>, <w n="12.8">Beau</w> <w n="12.9">Dieu</w> !</l>
						<l n="13" num="2.4"><space unit="char" quantity="8"></space><w n="13.1">Mais</w> <w n="13.2">dont</w> <w n="13.3">un</w> <w n="13.4">seul</w> <w n="13.5">dit</w> <w n="13.6">mon</w> <w n="13.7">supplice</w></l>
						<l n="14" num="2.5"><w n="14.1">Aux</w> <w n="14.2">temps</w> <w n="14.3">déjà</w> <w n="14.4">lointains</w> <w n="14.5">où</w> <w n="14.6">mon</w> <w n="14.7">cœur</w> <w n="14.8">atterré</w>,</l>
						<l n="15" num="2.6"><w n="15.1">Méconnaissant</w> <w n="15.2">en</w> <w n="15.3">soi</w> <w n="15.4">de</w> <w n="15.5">fécondes</w> <w n="15.6">richesses</w>,</l>
						<l n="16" num="2.7"><space unit="char" quantity="8"></space><w n="16.1">Sans</w> <w n="16.2">tenter</w> <w n="16.3">l</w>’<w n="16.4">effort</w> <w n="16.5">qui</w> <w n="16.6">redresse</w></l>
						<l n="17" num="2.8"><w n="17.1">N</w>’<w n="17.2">aspirait</w> <w n="17.3">qu</w>’<w n="17.4">au</w> <w n="17.5">néant</w> <w n="17.6">et</w> <w n="17.7">répétait</w> <w n="17.8">sans</w> <w n="17.9">cesse</w></l>
						<l n="18" num="2.9"><space unit="char" quantity="8"></space><w n="18.1">Dies</w> <w n="18.2">iræ</w> ! <w n="18.3">Alléluia</w> !</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">ce</w> <w n="19.4">rythme</w> <w n="19.5">pesant</w> <w n="19.6">de</w> <w n="19.7">désolation</w></l>
						<l n="20" num="3.2"><space unit="char" quantity="8"></space><w n="20.1">Qui</w> <w n="20.2">souleva</w> <w n="20.3">mon</w> <w n="20.4">premier</w> <w n="20.5">geste</w>,</l>
						<l n="21" num="3.3"><w n="21.1">Qui</w> <w n="21.2">vint</w> <w n="21.3">frapper</w> <w n="21.4">le</w> <w n="21.5">roc</w> <w n="21.6">et</w> <w n="21.7">creuser</w> <w n="21.8">le</w> <w n="21.9">sillon</w></l>
						<l n="22" num="3.4"><space unit="char" quantity="8"></space><w n="22.1">D</w>’<w n="22.2">où</w> <w n="22.3">la</w> <w n="22.4">basilique</w> <w n="22.5">céleste</w></l>
						<l n="23" num="3.5"><w n="23.1">Devait</w> <w n="23.2">plus</w> <w n="23.3">tard</w> <w n="23.4">lancer</w> <w n="23.5">ses</w> <w n="23.6">clochers</w> <w n="23.7">ajourés</w> !</l>
						<l n="24" num="3.6"><w n="24.1">C</w>’<w n="24.2">est</w> <w n="24.3">lui</w> <w n="24.4">qui</w> <w n="24.5">stimulait</w> <w n="24.6">et</w> <w n="24.7">ma</w> <w n="24.8">peine</w> <w n="24.9">et</w> <w n="24.10">mon</w> <w n="24.11">zèle</w>,</l>
						<l n="25" num="3.7"><space unit="char" quantity="8"></space><w n="25.1">Quand</w>, <w n="25.2">déployant</w> <w n="25.3">ses</w> <w n="25.4">lourdes</w> <w n="25.5">ailes</w>,</l>
						<l n="26" num="3.8"><w n="26.1">Il</w> <w n="26.2">flagellait</w> <w n="26.3">sans</w> <w n="26.4">fin</w> <w n="26.5">tout</w> <w n="26.6">mon</w> <w n="26.7">être</w> <w n="26.8">rebelle</w> !</l>
						<l n="27" num="3.9"><space unit="char" quantity="8"></space><w n="27.1">Dies</w> <w n="27.2">iræ</w> ! <w n="27.3">Alléluia</w> !</l>
					</lg>
					<lg n="4">
						<l n="28" num="4.1">« <w n="28.1">Je</w> <w n="28.2">ne</w> <w n="28.3">veux</w> <w n="28.4">que</w> <w n="28.5">mourir</w> !… <w n="28.6">La</w> <w n="28.7">mort</w> <w n="28.8">ne</w> <w n="28.9">suffit</w> <w n="28.10">point</w> ! »</l>
						<l n="29" num="4.2"><space unit="char" quantity="8"></space><w n="29.1">Et</w> <w n="29.2">le</w> <w n="29.3">glas</w> <w n="29.4">suscitait</w> <w n="29.5">la</w> <w n="29.6">tâche</w> !</l>
						<l n="30" num="4.3">…<w n="30.1">Voici</w> <w n="30.2">la</w> <w n="30.3">pierre</w> <w n="30.4">vive</w> <w n="30.5">et</w> <w n="30.6">que</w> <w n="30.7">rien</w> <w n="30.8">ne</w> <w n="30.9">disjoint</w> !</l>
						<l n="31" num="4.4"><space unit="char" quantity="8"></space><w n="31.1">Façonnée</w> <w n="31.2">à</w> <w n="31.3">grands</w> <w n="31.4">coups</w> <w n="31.5">de</w> <w n="31.6">hache</w></l>
						<l n="32" num="4.5"><w n="32.1">Voici</w> <w n="32.2">l</w>’<w n="32.3">assise</w> <w n="32.4">des</w> <w n="32.5">douleurs</w>, <w n="32.6">portant</w> <w n="32.7">serrés</w></l>
						<l n="33" num="4.6"><w n="33.1">Ses</w> <w n="33.2">arcs</w>-<w n="33.3">boutants</w> <w n="33.4">de</w> <w n="33.5">foi</w>, <w n="33.6">de</w> <w n="33.7">rêve</w>, <w n="33.8">et</w> <w n="33.9">d</w>’<w n="33.10">humble</w> <w n="33.11">audace</w>,</l>
						<l n="34" num="4.7"><space unit="char" quantity="8"></space><w n="34.1">Voici</w> <w n="34.2">le</w> <w n="34.3">parvis</w> <w n="34.4">de</w> <w n="34.5">disgrâce</w>,</l>
						<l n="35" num="4.8"><w n="35.1">Voici</w> <w n="35.2">l</w>’<w n="35.3">oblation</w> <w n="35.4">de</w> <w n="35.5">mon</w> <w n="35.6">âme</w> <w n="35.7">trop</w> <w n="35.8">lasse</w> !</l>
						<l n="36" num="4.9"><space unit="char" quantity="8"></space><w n="36.1">Dies</w> <w n="36.2">iræ</w> ! <w n="36.3">Dies</w> <w n="36.4">iræ</w> !</l>
					</lg>
					<lg n="5">
						<l n="37" num="5.1"><w n="37.1">Voici</w> <w n="37.2">pourtant</w> <w n="37.3">encor</w> <w n="37.4">la</w> <w n="37.5">force</w> <w n="37.6">des</w> <w n="37.7">piliers</w></l>
						<l n="38" num="5.2"><space unit="char" quantity="8"></space><w n="38.1">Dans</w> <w n="38.2">leur</w> <w n="38.3">persévérance</w> <w n="38.4">grise</w> !</l>
						<l n="39" num="5.3"><w n="39.1">Voici</w> <w n="39.2">l</w>’<w n="39.3">image</w> <w n="39.4">et</w> <w n="39.5">le</w> <w n="39.6">symbole</w> <w n="39.7">des</w> <w n="39.8">pitiés</w></l>
						<l n="40" num="5.4"><space unit="char" quantity="8"></space><w n="40.1">Voici</w> <w n="40.2">les</w> <w n="40.3">saints</w> <w n="40.4">qu</w>’<w n="40.5">on</w> <w n="40.6">martyrise</w>,</l>
						<l n="41" num="5.5"><w n="41.1">Et</w> <w n="41.2">de</w> <w n="41.3">l</w>’<w n="41.4">amour</w> <w n="41.5">souffrant</w> <w n="41.6">voici</w> <w n="41.7">les</w> <w n="41.8">sept</w> <w n="41.9">degrés</w></l>
						<l n="42" num="5.6"><w n="42.1">Et</w> <w n="42.2">les</w> <w n="42.3">trois</w> <w n="42.4">porches</w> <w n="42.5">d</w>’<w n="42.6">ombre</w> <w n="42.7">avec</w> <w n="42.8">leurs</w> <w n="42.9">défaillances</w>,</l>
						<l n="43" num="5.7"><space unit="char" quantity="8"></space><w n="43.1">Voici</w> <w n="43.2">les</w> <w n="43.3">narthex</w> <w n="43.4">du</w> <w n="43.5">silence</w>,</l>
						<l n="44" num="5.8"><w n="44.1">Voici</w> <w n="44.2">le</w> <w n="44.3">Roi</w> <w n="44.4">des</w> <w n="44.5">rois</w> <w n="44.6">dans</w> <w n="44.7">sa</w> <w n="44.8">magnificence</w> !</l>
						<l n="45" num="5.9"><space unit="char" quantity="8"></space><w n="45.1">Dies</w> <w n="45.2">iræ</w> ! <w n="45.3">Dies</w> <w n="45.4">iræ</w> !</l>
					</lg>
					<lg n="6">
						<l n="46" num="6.1"><w n="46.1">Voici</w> <w n="46.2">la</w> <w n="46.3">flamme</w> <w n="46.4">haute</w> <w n="46.5">et</w> <w n="46.6">les</w> <w n="46.7">brasiers</w> <w n="46.8">d</w>’<w n="46.9">ardeur</w>,</l>
						<l n="47" num="6.2"><space unit="char" quantity="8"></space><w n="47.1">Voici</w> <w n="47.2">la</w> <w n="47.3">torche</w> <w n="47.4">et</w> <w n="47.5">l</w>’<w n="47.6">auréole</w> !</l>
						<l n="48" num="6.3"><w n="48.1">Voici</w> <w n="48.2">la</w> <w n="48.3">lampe</w> <w n="48.4">éteinte</w> <w n="48.5">et</w> <w n="48.6">le</w> <w n="48.7">feu</w> <w n="48.8">qui</w> <w n="48.9">se</w> <w n="48.10">meurt</w>,</l>
						<l n="49" num="6.4"><space unit="char" quantity="8"></space><w n="49.1">Offrande</w> <w n="49.2">sage</w>, <w n="49.3">offrande</w> <w n="49.4">folle</w> !</l>
						<l n="50" num="6.5"><w n="50.1">Voici</w> <w n="50.2">le</w> <w n="50.3">mal</w> <w n="50.4">vivant</w>, <w n="50.5">voici</w> <w n="50.6">le</w> <w n="50.7">verbe</w> <w n="50.8">vrai</w>,</l>
						<l n="51" num="6.6"><w n="51.1">La</w> <w n="51.2">moisson</w> <w n="51.3">de</w> <w n="51.4">vertus</w>, <w n="51.5">la</w> <w n="51.6">floraison</w> <w n="51.7">de</w> <w n="51.8">vices</w>,</l>
						<l n="52" num="6.7"><space unit="char" quantity="8"></space><w n="52.1">Et</w> <w n="52.2">les</w> <w n="52.3">dévastantes</w> <w n="52.4">blandices</w></l>
						<l n="53" num="6.8"><w n="53.1">Des</w> <w n="53.2">renonciations</w> <w n="53.3">pleines</w> <w n="53.4">de</w> <w n="53.5">paix</w> <w n="53.6">factice</w> !</l>
						<l n="54" num="6.9"><space unit="char" quantity="8"></space><w n="54.1">Dies</w> <w n="54.2">iræ</w> ! <w n="54.3">Dies</w> <w n="54.4">iræ</w> !</l>
					</lg>
					<lg n="7">
						<l n="55" num="7.1"><w n="55.1">Voici</w> <w n="55.2">l</w>’<w n="55.3">âme</w> <w n="55.4">élargie</w> <w n="55.5">exhaussant</w> <w n="55.6">en</w> <w n="55.7">plein</w> <w n="55.8">ciel</w></l>
						<l n="56" num="7.2"><space unit="char" quantity="8"></space><w n="56.1">Ses</w> <w n="56.2">amples</w> <w n="56.3">toits</w> <w n="56.4">de</w> <w n="56.5">patience</w>,</l>
						<l n="57" num="7.3"><w n="57.1">Ses</w> <w n="57.2">clochers</w> <w n="57.3">de</w> <w n="57.4">prière</w> <w n="57.5">et</w> <w n="57.6">sa</w> <w n="57.7">flèche</w> <w n="57.8">d</w>’<w n="57.9">appel</w> !</l>
						<l n="58" num="7.4"><space unit="char" quantity="8"></space><w n="58.1">Voici</w> <w n="58.2">surgir</w> <w n="58.3">sa</w> <w n="58.4">triple</w> <w n="58.5">instance</w></l>
						<l n="59" num="7.5"><w n="59.1">L</w>’<w n="59.2">abside</w> <w n="59.3">agenouillée</w> <w n="59.4">et</w> <w n="59.5">murmurant</w> <w n="59.6">tout</w> <w n="59.7">bas</w>,</l>
						<l n="60" num="7.6"><w n="60.1">Le</w> <w n="60.2">transept</w> <w n="60.3">redressé</w> <w n="60.4">sous</w> <w n="60.5">un</w> <w n="60.6">vent</w> <w n="60.7">de</w> <w n="60.8">miracle</w>,</l>
						<l n="61" num="7.7"><space unit="char" quantity="8"></space><w n="61.1">Et</w>, <w n="61.2">libre</w> <w n="61.3">enfin</w> <w n="61.4">de</w> <w n="61.5">tout</w> <w n="61.6">obstacle</w>,</l>
						<l n="62" num="7.8"><w n="62.1">L</w>’<w n="62.2">immense</w> <w n="62.3">nef</w>, <w n="62.4">debout</w>, <w n="62.5">jetant</w> <w n="62.6">tours</w> <w n="62.7">et</w> <w n="62.8">pinacles</w> !</l>
						<l n="63" num="7.9"><space unit="char" quantity="8"></space><w n="63.1">Dies</w> <w n="63.2">iræ</w> ! <w n="63.3">Dies</w> <w n="63.4">iræ</w> !</l>
					</lg>
					<lg n="8">
						<l n="64" num="8.1"><w n="64.1">Car</w> <w n="64.2">mon</w> <w n="64.3">âme</w>, <w n="64.4">étouffant</w> <w n="64.5">sous</w> <w n="64.6">l</w>’<w n="64.7">afflux</w> <w n="64.8">de</w> <w n="64.9">l</w>’<w n="64.10">amour</w>,</l>
						<l n="65" num="8.2"><space unit="char" quantity="8"></space><w n="65.1">S</w>’<w n="65.2">est</w> <w n="65.3">réalisée</w> <w n="65.4">elle</w>-<w n="65.5">même</w></l>
						<l n="66" num="8.3"><w n="66.1">En</w> <w n="66.2">se</w> <w n="66.3">perdant</w> <w n="66.4">en</w> <w n="66.5">toi</w> ! <w n="66.6">Désormais</w>, <w n="66.7">sans</w> <w n="66.8">recours</w></l>
						<l n="67" num="8.4"><space unit="char" quantity="8"></space><w n="67.1">Au</w> <w n="67.2">monde</w> <w n="67.3">extérieur</w>, <w n="67.4">elle</w> <w n="67.5">aime</w> !</l>
						<l n="68" num="8.5"><w n="68.1">Et</w>, <w n="68.2">laissant</w> <w n="68.3">autour</w> <w n="68.4">d</w>’<w n="68.5">elle</w>, <w n="68.6">en</w> <w n="68.7">fabuleux</w> <w n="68.8">amas</w>,</l>
						<l n="69" num="8.6"><w n="69.1">Le</w> <w n="69.2">labeur</w> <w n="69.3">apparent</w> <w n="69.4">de</w> <w n="69.5">ses</w> <w n="69.6">trois</w> <w n="69.7">frontispices</w></l>
						<l n="70" num="8.7"><space unit="char" quantity="8"></space><w n="70.1">Aux</w> <w n="70.2">images</w> <w n="70.3">médiatrices</w>,</l>
						<l n="71" num="8.8"><w n="71.1">Elle</w> <w n="71.2">rejoint</w> <w n="71.3">l</w>’<w n="71.4">Esprit</w> <w n="71.5">au</w> <w n="71.6">sein</w> <w n="71.7">de</w> <w n="71.8">l</w>’<w n="71.9">édifice</w> !</l>
						<l n="72" num="8.9"><space unit="char" quantity="8"></space><w n="72.1">Alléluia</w> ! <w n="72.2">Alléluia</w> !</l>
					</lg>
					<lg n="9">
						<l n="73" num="9.1"><w n="73.1">Elle</w> <w n="73.2">possède</w> <w n="73.3">enfin</w>, <w n="73.4">dans</w> <w n="73.5">sa</w> <w n="73.6">simplicité</w>,</l>
						<l n="74" num="9.2"><space unit="char" quantity="8"></space><w n="74.1">Sans</w> <w n="74.2">figuration</w> <w n="74.3">sensible</w>,</l>
						<l n="75" num="9.3"><w n="75.1">Ta</w> <w n="75.2">pure</w> <w n="75.3">pensée</w>, <w n="75.4">Amour</w>, <w n="75.5">ta</w> <w n="75.6">divinité</w> !</l>
						<l n="76" num="9.4"><space unit="char" quantity="8"></space><w n="76.1">C</w>’<w n="76.2">est</w> <w n="76.3">pourquoi</w> <w n="76.4">ton</w> <w n="76.5">temple</w> <w n="76.6">invisible</w></l>
						<l n="77" num="9.5"><w n="77.1">N</w>’<w n="77.2">est</w>, <w n="77.3">dans</w> <w n="77.4">sa</w> <w n="77.5">profondeur</w>, <w n="77.6">qu</w>’<w n="77.7">un</w> <w n="77.8">élan</w>, <w n="77.9">qu</w>’<w n="77.10">un</w> <w n="77.11">éclat</w>,</l>
						<l n="78" num="9.6"><w n="78.1">Qu</w>’<w n="78.2">un</w> <w n="78.3">long</w> <w n="78.4">effort</w> <w n="78.5">votif</w>, <w n="78.6">qu</w>’<w n="78.7">un</w> <w n="78.8">vibrant</w> <w n="78.9">témoignage</w>,</l>
						<l n="79" num="9.7"><space unit="char" quantity="8"></space><w n="79.1">Et</w> <w n="79.2">sa</w> <w n="79.3">nudité</w> <w n="79.4">sans</w> <w n="79.5">image</w></l>
						<l n="80" num="9.8"><w n="80.1">S</w>’<w n="80.2">imprègne</w> <w n="80.3">de</w> <w n="80.4">ton</w> <w n="80.5">feu</w> <w n="80.6">chaque</w> <w n="80.7">jour</w> <w n="80.8">davantage</w> !</l>
						<l n="81" num="9.9"><space unit="char" quantity="8"></space><w n="81.1">Alléluia</w> ! <w n="81.2">Alléluia</w> !</l>
					</lg>
					<lg n="10">
						<l n="82" num="10.1"><w n="82.1">Mon</w> <w n="82.2">âme</w> <w n="82.3">te</w> <w n="82.4">contient</w>, <w n="82.5">Raison</w> <w n="82.6">de</w> <w n="82.7">ma</w> <w n="82.8">raison</w>,</l>
						<l n="83" num="10.2"><space unit="char" quantity="8"></space><w n="83.1">Fixée</w> <w n="83.2">au</w> <w n="83.3">centre</w> <w n="83.4">de</w> <w n="83.5">ses</w> <w n="83.6">voûtes</w> !</l>
						<l n="84" num="10.3"><w n="84.1">Pensée</w> <w n="84.2">et</w> <w n="84.3">sentiment</w>, <w n="84.4">voilà</w> <w n="84.5">tes</w> <w n="84.6">étançons</w></l>
						<l n="85" num="10.4"><space unit="char" quantity="8"></space><w n="85.1">Leur</w> <w n="85.2">force</w> <w n="85.3">à</w> <w n="85.4">ta</w> <w n="85.5">force</w> <w n="85.6">s</w>’<w n="85.7">ajoute</w></l>
						<l n="86" num="10.5"><w n="86.1">Et</w> <w n="86.2">vous</w> <w n="86.3">formez</w> <w n="86.4">ensemble</w> <w n="86.5">un</w> <w n="86.6">triangle</w> <w n="86.7">adéquat</w>,</l>
						<l n="87" num="10.6"><w n="87.1">Un</w> <w n="87.2">tiers</w>-<w n="87.3">point</w> <w n="87.4">sans</w> <w n="87.5">défaut</w>, <w n="87.6">une</w> <w n="87.7">ogive</w> <w n="87.8">parfaite</w>,</l>
						<l n="88" num="10.7"><space unit="char" quantity="8"></space><w n="88.1">A</w> <w n="88.2">travers</w> <w n="88.3">quoi</w>, <w n="88.4">du</w> <w n="88.5">sol</w> <w n="88.6">au</w> <w n="88.7">faite</w>,</l>
						<l n="89" num="10.8"><w n="89.1">S</w>’<w n="89.2">irradiera</w> <w n="89.3">la</w> <w n="89.4">cathédrale</w> <w n="89.5">un</w> <w n="89.6">jour</w> <w n="89.7">de</w> <w n="89.8">fête</w> !</l>
						<l n="90" num="10.9"><space unit="char" quantity="8"></space><w n="90.1">Alléluia</w> ! <w n="90.2">Alléluia</w> !</l>
					</lg>
					<lg n="11">
						<l n="91" num="11.1"><w n="91.1">C</w>’<w n="91.2">est</w> <w n="91.3">ta</w> <w n="91.4">maison</w>… <w n="91.5">Elle</w> <w n="91.6">est</w> <w n="91.7">sans</w> <w n="91.8">calme</w> <w n="91.9">et</w> <w n="91.10">sans</w> <w n="91.11">repos</w>,</l>
						<l n="92" num="11.2"><space unit="char" quantity="8"></space><w n="92.1">Ses</w> <w n="92.2">moindres</w> <w n="92.3">pierres</w> <w n="92.4">te</w> <w n="92.5">célèbrent</w> !</l>
						<l n="93" num="11.3"><w n="93.1">Elle</w> <w n="93.2">est</w> <w n="93.3">sans</w> <w n="93.4">ornements</w>, <w n="93.5">sans</w> <w n="93.6">somptueux</w> <w n="93.7">vitraux</w></l>
						<l n="94" num="11.4"><space unit="char" quantity="8"></space><w n="94.1">Comme</w> <w n="94.2">elle</w> <w n="94.3">est</w> <w n="94.4">aussi</w> <w n="94.5">sans</w> <w n="94.6">ténèbres</w> !</l>
						<l n="95" num="11.5"><w n="95.1">L</w>’<w n="95.2">élan</w> <w n="95.3">de</w> <w n="95.4">ses</w> <w n="95.5">piliers</w> <w n="95.6">se</w> <w n="95.7">perd</w> <w n="95.8">dans</w> <w n="95.9">l</w>’<w n="95.10">Au</w>-<w n="95.11">delà</w></l>
						<l n="96" num="11.6"><w n="96.1">Car</w> <w n="96.2">leur</w> <w n="96.3">effort</w> <w n="96.4">a</w> <w n="96.5">dépassé</w> <w n="96.6">mon</w> <w n="96.7">sacrifice</w>,</l>
						<l n="97" num="11.7"><space unit="char" quantity="8"></space><w n="97.1">Et</w> <w n="97.2">sa</w> <w n="97.3">lumière</w> <w n="97.4">excitatrice</w></l>
						<l n="98" num="11.8"><w n="98.1">Fait</w> <w n="98.2">tendre</w> <w n="98.3">encor</w> <w n="98.4">plus</w> <w n="98.5">haut</w> <w n="98.6">les</w> <w n="98.7">faisceaux</w> <w n="98.8">qu</w>’<w n="98.9">ils</w> <w n="98.10">déplissent</w> !</l>
						<l n="99" num="11.9"><space unit="char" quantity="8"></space><w n="99.1">Alléluia</w> ! <w n="99.2">Alléluia</w></l>
					</lg>
					<lg n="12">
						<l n="100" num="12.1"><w n="100.1">Mon</w> <w n="100.2">appel</w> <w n="100.3">t</w>’<w n="100.4">y</w> <w n="100.5">retient</w> <w n="100.6">et</w> <w n="100.7">j</w>’<w n="100.8">y</w> <w n="100.9">reçois</w> <w n="100.10">sans</w> <w n="100.11">fin</w></l>
						<l n="101" num="12.2"><space unit="char" quantity="8"></space><w n="101.1">Les</w> <w n="101.2">dictamen</w> <w n="101.3">de</w> <w n="101.4">ta</w> <w n="101.5">présence</w> !</l>
						<l n="102" num="12.3"><w n="102.1">Je</w> <w n="102.2">n</w>’<w n="102.3">ai</w> <w n="102.4">plus</w> <w n="102.5">qu</w>’<w n="102.6">à</w> <w n="102.7">suspendre</w> <w n="102.8">un</w> <w n="102.9">mystique</w> <w n="102.10">lien</w>,</l>
						<l n="103" num="12.4"><space unit="char" quantity="8"></space><w n="103.1">Emblème</w> <w n="103.2">de</w> <w n="103.3">cette</w> <w n="103.4">alliance</w>,</l>
						<l n="104" num="12.5"><w n="104.1">Autour</w> <w n="104.2">du</w> <w n="104.3">sanctuaire</w>… <w n="104.4">Et</w>, <w n="104.5">dans</w> <w n="104.6">tout</w> <w n="104.7">son</w> <w n="104.8">éclat</w>,</l>
						<l n="105" num="12.6"><w n="105.1">Voici</w>, <w n="105.2">Beau</w> <w n="105.3">Dieu</w>, <w n="105.4">la</w> <w n="105.5">guirlande</w> <w n="105.6">de</w> <w n="105.7">feuilles</w> <w n="105.8">vives</w>,</l>
						<l n="106" num="12.7"><space unit="char" quantity="8"></space><w n="106.1">Où</w>, <w n="106.2">d</w>’<w n="106.3">arceaux</w> <w n="106.4">en</w> <w n="106.5">arceaux</w>, <w n="106.6">s</w>’<w n="106.7">avive</w></l>
						<l n="107" num="12.8"><w n="107.1">L</w>’<w n="107.2">immarcescible</w> <w n="107.3">fleur</w> <w n="107.4">de</w> <w n="107.5">la</w> <w n="107.6">vie</w> <w n="107.7">unitive</w> !</l>
						<l n="108" num="12.9"><space unit="char" quantity="8"></space><w n="108.1">Alléluia</w> ! <w n="108.2">Alléluia</w> !</l>
					</lg>
					<lg n="13">
						<l n="109" num="13.1"><w n="109.1">Mais</w> <w n="109.2">ce</w> <w n="109.3">chant</w> <w n="109.4">qui</w> <w n="109.5">poursuit</w> <w n="109.6">son</w> <w n="109.7">tumulte</w> <w n="109.8">obstiné</w> ,</l>
						<l n="110" num="13.2"><space unit="char" quantity="8"></space><w n="110.1">Sauras</w>-<w n="110.2">tu</w> <w n="110.3">jamais</w> <w n="110.4">le</w> <w n="110.5">comprendre</w> ?</l>
						<l n="111" num="13.3"><w n="111.1">Il</w> <w n="111.2">traduit</w> <w n="111.3">un</w> <w n="111.4">orgueil</w> <w n="111.5">aussitôt</w> <w n="111.6">mort</w> <w n="111.7">que</w> <w n="111.8">né</w>,</l>
						<l n="112" num="13.4"><space unit="char" quantity="8"></space><w n="112.1">Des</w> <w n="112.2">pleurs</w> <w n="112.3">qu</w>’<w n="112.4">on</w> <w n="112.5">ne</w> <w n="112.6">veut</w> <w n="112.7">plus</w> <w n="112.8">répandre</w>,</l>
						<l n="113" num="13.5"><w n="113.1">Un</w> <w n="113.2">travail</w> <w n="113.3">que</w> <w n="113.4">chaque</w> <w n="113.5">heure</w> <w n="113.6">ébauche</w> <w n="113.7">et</w> <w n="113.8">brise</w>, <w n="113.9">hélas</w> !</l>
						<l n="114" num="13.6"><w n="114.1">Il</w> <w n="114.2">est</w> <w n="114.3">ma</w> <w n="114.4">volonté</w> <w n="114.5">mais</w> <w n="114.6">n</w>’<w n="114.7">est</w> <w n="114.8">pas</w> <w n="114.9">mon</w> <w n="114.10">courage</w>,</l>
						<l n="115" num="13.7"><space unit="char" quantity="8"></space><w n="115.1">Et</w> <w n="115.2">quand</w> <w n="115.3">vient</w> <w n="115.4">alterner</w> <w n="115.5">l</w>’<w n="115.6">hommage</w></l>
						<l n="116" num="13.8"><w n="116.1">C</w>’<w n="116.2">est</w> <w n="116.3">son</w> <w n="116.4">cri</w> <w n="116.5">triomphal</w>, <w n="116.6">crois</w>-<w n="116.7">moi</w>, <w n="116.8">le</w> <w n="116.9">plus</w> <w n="116.10">sauvage</w> !</l>
						<l n="117" num="13.9"><space unit="char" quantity="8"></space><w n="117.1">Dies</w> <w n="117.2">iræ</w> ! <w n="117.3">Alléluia</w> !</l>
					</lg>
				</div></body></text></TEI>