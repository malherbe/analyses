<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA SAINT JEAN D’ÉTÉ</head><head type="main_subpart">SONNETS POUR UNE ANNÉE</head><div type="poem" key="HRV35">
				<head type="main">NOVEMBRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Accueille</w>-<w n="1.2">moi</w> <w n="1.3">dans</w> <w n="1.4">ton</w> <w n="1.5">expirante</w> <w n="1.6">beauté</w></l>
						<l n="2" num="1.2"><w n="2.1">Forêt</w> !… <w n="2.2">endors</w> <w n="2.3">par</w> <w n="2.4">ta</w> <w n="2.5">multiple</w> <w n="2.6">symphonie</w></l>
						<l n="3" num="1.3"><w n="3.1">Cette</w> <w n="3.2">voix</w> <w n="3.3">de</w> <w n="3.4">mon</w> <w n="3.5">cœur</w> <w n="3.6">chantant</w> <w n="3.7">sa</w> <w n="3.8">litanie</w></l>
						<l n="4" num="1.4"><w n="4.1">Fais</w> <w n="4.2">jouer</w> <w n="4.3">ta</w> <w n="4.4">lumineuse</w> <w n="4.5">mobilité</w></l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">De</w> <w n="5.2">pourpres</w> <w n="5.3">opalins</w>, <w n="5.4">de</w> <w n="5.5">bronze</w> <w n="5.6">velouté</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Souple</w> <w n="6.2">et</w> <w n="6.3">changeant</w> <w n="6.4">brocart</w>, <w n="6.5">manteau</w> <w n="6.6">de</w> <w n="6.7">Bithynie</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Dont</w> <w n="7.2">s</w>’<w n="7.3">entoure</w> <w n="7.4">à</w> <w n="7.5">l</w>’<w n="7.6">instant</w> <w n="7.7">suprême</w>, <w n="7.8">l</w>’<w n="7.9">agonie</w></l>
						<l n="8" num="2.4"><w n="8.1">Émouvante</w> <w n="8.2">et</w> <w n="8.3">délicieuse</w> <w n="8.4">de</w> <w n="8.5">l</w>’<w n="8.6">été</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ah</w> ! <w n="9.2">donne</w>-<w n="9.3">moi</w> <w n="9.4">par</w> <w n="9.5">ta</w> <w n="9.6">solitude</w> <w n="9.7">odorante</w>,</l>
						<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">paix</w>… <w n="10.3">l</w>’<w n="10.4">oubli</w> !… <w n="10.5">Mais</w> <w n="10.6">ta</w> <w n="10.7">douceur</w> <w n="10.8">est</w> <w n="10.9">trop</w> <w n="10.10">poignante</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Mon</w> <w n="11.2">amour</w> <w n="11.3">est</w> <w n="11.4">là</w>, <w n="11.5">trop</w> <w n="11.6">vivant</w> ! <w n="11.7">L</w>’<w n="11.8">obsession</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">De</w> <w n="12.2">son</w> <w n="12.3">ardente</w> <w n="12.4">voix</w> <w n="12.5">traverse</w> <w n="12.6">ton</w> <w n="12.7">silence</w></l>
						<l n="13" num="4.2"><w n="13.1">Et</w> <w n="13.2">sur</w> <w n="13.3">tes</w> <w n="13.4">feuilles</w> <w n="13.5">j</w>’<w n="13.6">aperçois</w> <w n="13.7">en</w> <w n="13.8">transparence</w></l>
						<l n="14" num="4.3"><w n="14.1">Le</w> <w n="14.2">filigrane</w> <w n="14.3">ineffaçable</w> <w n="14.4">de</w> <w n="14.5">son</w> <w n="14.6">nom</w> !…</l>
					</lg>
					<closer>
						<note id="" type="footnote">Mi bémol mineur ‒ en rapport tonal, dans un état émotif différent avec juin.‒ Même touffeur énervante.</note>
					</closer>
				</div></body></text></TEI>