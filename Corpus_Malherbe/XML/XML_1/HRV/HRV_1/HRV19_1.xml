<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUIVANT L’HUMEUR DES JOURS</head><div type="poem" key="HRV19">
				<head type="main">PRINTEMPS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Une</w> <w n="1.2">vaste</w> <w n="1.3">rumeur</w> <w n="1.4">d</w>’<w n="1.5">attente</w> <w n="1.6">et</w> <w n="1.7">de</w> <w n="1.8">désir</w></l>
						<l n="2" num="1.2"><w n="2.1">Émeut</w> <w n="2.2">les</w> <w n="2.3">bois</w>, <w n="2.4">crispés</w> <w n="2.5">sous</w> <w n="2.6">la</w> <w n="2.7">caresse</w> <w n="2.8">acide</w></l>
						<l n="3" num="1.3"><w n="3.1">D</w>’<w n="3.2">un</w> <w n="3.3">vent</w> <w n="3.4">trop</w> <w n="3.5">frais</w>, <w n="3.6">d</w>’<w n="3.7">un</w> <w n="3.8">vent</w> <w n="3.9">aigu</w> <w n="3.10">qui</w> <w n="3.11">fait</w> <w n="3.12">frémir</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">les</w> <w n="4.3">vergers</w>, <w n="4.4">les</w> <w n="4.5">frissonnantes</w> <w n="4.6">fleurs</w> <w n="4.7">candides</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">je</w> <w n="5.3">vais</w> <w n="5.4">en</w> <w n="5.5">tenant</w> <w n="5.6">mes</w> <w n="5.7">rêves</w> <w n="5.8">par</w> <w n="5.9">la</w> <w n="5.10">main</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">ciel</w> <w n="6.3">est</w> <w n="6.4">agrandi</w> <w n="6.5">de</w> <w n="6.6">leur</w> <w n="6.7">aile</w> <w n="6.8">éployée</w> .</l>
						<l n="7" num="1.7"><w n="7.1">Qui</w> <w n="7.2">jette</w> <w n="7.3">par</w> <w n="7.4">instants</w> <w n="7.5">de</w> <w n="7.6">l</w>’<w n="7.7">ombre</w> <w n="7.8">en</w> <w n="7.9">mon</w> <w n="7.10">chemin</w>.</l>
						<l n="8" num="1.8">…<w n="8.1">Oui</w>, <w n="8.2">je</w> <w n="8.3">vais</w> <w n="8.4">au</w> <w n="8.5">hasard</w> <w n="8.6">sur</w> <w n="8.7">ta</w> <w n="8.8">trace</w> <w n="8.9">mouillée</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Printemps</w> <w n="9.2">au</w> <w n="9.3">visage</w> <w n="9.4">incertain</w>, <w n="9.5">aux</w> <w n="9.6">jeunes</w> <w n="9.7">yeux</w></l>
						<l n="10" num="1.10"><w n="10.1">Tournés</w> <w n="10.2">vers</w> <w n="10.3">l</w>’<w n="10.4">inconnu</w> <w n="10.5">d</w>’<w n="10.6">une</w> <w n="10.7">saison</w> <w n="10.8">nouvelle</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Car</w> <w n="11.2">je</w> <w n="11.3">veux</w> <w n="11.4">lire</w> <w n="11.5">encor</w> <w n="11.6">dans</w> <w n="11.7">tes</w> <w n="11.8">vertes</w> <w n="11.9">prunelles</w></l>
						<l n="12" num="1.12"><w n="12.1">Ton</w> <w n="12.2">mensonge</w> <w n="12.3">périssable</w> <w n="12.4">mais</w> <w n="12.5">radieux</w> !</l>
						<l n="13" num="1.13"><w n="13.1">Je</w> <w n="13.2">t</w>’<w n="13.3">aperçois</w>… <w n="13.4">je</w> <w n="13.5">te</w> <w n="13.6">poursuis</w>… <w n="13.7">mais</w> <w n="13.8">tu</w> <w n="13.9">t</w>’<w n="13.10">élances</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Et</w> <w n="14.2">tourbillonnant</w>, <w n="14.3">fou</w>, <w n="14.4">jeune</w> <w n="14.5">et</w> <w n="14.6">divin</w>, <w n="14.7">tu</w> <w n="14.8">danses</w> !</l>
						<l n="15" num="1.15"><w n="15.1">Sous</w> <w n="15.2">le</w> <w n="15.3">grand</w> <w n="15.4">soleil</w> <w n="15.5">blond</w> <w n="15.6">comme</w> <w n="15.7">un</w> <w n="15.8">rayon</w> <w n="15.9">de</w> <w n="15.10">miel</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Tu</w> <w n="16.2">danses</w> ! <w n="16.3">Sous</w> <w n="16.4">la</w> <w n="16.5">soie</w> <w n="16.6">opaline</w> <w n="16.7">du</w> <w n="16.8">ciel</w>,</l>
						<l n="17" num="1.17"><w n="17.1">Dans</w> <w n="17.2">le</w> <w n="17.3">ruissellement</w> <w n="17.4">aérien</w> <w n="17.5">des</w> <w n="17.6">écharpes</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Je</w> <w n="18.2">vois</w> <w n="18.3">jouer</w> <w n="18.4">sur</w> <w n="18.5">toi</w> <w n="18.6">le</w> <w n="18.7">prisme</w> <w n="18.8">aux</w> <w n="18.9">sept</w> <w n="18.10">couleurs</w>…</l>
						<l n="19" num="1.19">… <w n="19.1">Et</w> <w n="19.2">je</w> <w n="19.3">songe</w> <w n="19.4">à</w> <w n="19.5">l</w>’<w n="19.6">amour</w> <w n="19.7">dont</w> <w n="19.8">toute</w> <w n="19.9">la</w> <w n="19.10">douceur</w></l>
						<l n="20" num="1.20"><w n="20.1">N</w>’<w n="20.2">est</w> <w n="20.3">peut</w>-<w n="20.4">être</w> <w n="20.5">qu</w>’<w n="20.6">un</w> <w n="20.7">chant</w> <w n="20.8">merveilleux</w> <w n="20.9">de</w> <w n="20.10">ta</w> <w n="20.11">harpe</w>,</l>
						<l n="21" num="1.21"><w n="21.1">O</w> <w n="21.2">Printemps</w> !… <w n="21.3">Toi</w>, <w n="21.4">tu</w> <w n="21.5">ris</w> <w n="21.6">en</w> <w n="21.7">dansant</w>, <w n="21.8">les</w> <w n="21.9">yeux</w> <w n="21.10">clos</w>…</l>
						<l n="22" num="1.22"><w n="22.1">Tu</w> <w n="22.2">foules</w> <w n="22.3">de</w> <w n="22.4">tes</w> <w n="22.5">pas</w> <w n="22.6">l</w>’<w n="22.7">herbe</w> <w n="22.8">odorante</w> <w n="22.9">et</w> <w n="22.10">molle</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Sous</w> <w n="23.2">les</w> <w n="23.3">branches</w> <w n="23.4">retombantes</w> <w n="23.5">des</w> <w n="23.6">grands</w> <w n="23.7">bouleaux</w> ;</l>
						<l n="24" num="1.24"><w n="24.1">Tu</w> <w n="24.2">fais</w> <w n="24.3">naître</w> <w n="24.4">en</w> <w n="24.5">dansant</w> <w n="24.6">ce</w> <w n="24.7">chant</w> <w n="24.8">qui</w> <w n="24.9">nous</w> <w n="24.10">affole</w>,</l>
						<l n="25" num="1.25"><w n="25.1">Tu</w> <w n="25.2">l</w>’<w n="25.3">inscris</w> <w n="25.4">sur</w> <w n="25.5">la</w> <w n="25.6">terre</w> <w n="25.7">en</w> <w n="25.8">bouquets</w> <w n="25.9">dénoués</w>,.</l>
						<l n="26" num="1.26"><w n="26.1">Et</w>, <w n="26.2">par</w> <w n="26.3">tes</w> <w n="26.4">traits</w> <w n="26.5">d</w>’<w n="26.6">anémone</w> <w n="26.7">et</w> <w n="26.8">de</w> <w n="26.9">violette</w>,</l>
						<l n="27" num="1.27"><w n="27.1">La</w> <w n="27.2">forme</w> <w n="27.3">et</w> <w n="27.4">le</w> <w n="27.5">parfum</w> <w n="27.6">d</w>’<w n="27.7">une</w> <w n="27.8">fugue</w> <w n="27.9">muette</w></l>
						<l n="28" num="1.28"><w n="28.1">Suit</w> <w n="28.2">ta</w> <w n="28.3">danse</w> <w n="28.4">éperdue</w>, <w n="28.5">en</w> <w n="28.6">motifs</w> <w n="28.7">alternés</w> !</l>
						<l n="29" num="1.29"><w n="29.1">Tout</w> <w n="29.2">autour</w> <w n="29.3">de</w> <w n="29.4">ta</w> <w n="29.5">grâce</w> <w n="29.6">ondoient</w> <w n="29.7">les</w> <w n="29.8">frais</w> <w n="29.9">pétales</w> !</l>
						<l n="30" num="1.30"><w n="30.1">Tu</w> <w n="30.2">leur</w> <w n="30.3">dictes</w> <w n="30.4">la</w> <w n="30.5">joie</w> <w n="30.6">et</w> <w n="30.7">la</w> <w n="30.8">vie</w>, <w n="30.9">et</w> <w n="30.10">l</w>’<w n="30.11">instinct</w>,</l>
						<l n="31" num="1.31"><w n="31.1">La</w> <w n="31.2">danse</w> <w n="31.3">aveugle</w> <w n="31.4">du</w> <w n="31.5">désir</w> <w n="31.6">et</w> <w n="31.7">du</w> <w n="31.8">destin</w>…</l>
						<l n="32" num="1.32"><w n="32.1">La</w> <w n="32.2">forêt</w> <w n="32.3">tout</w> <w n="32.4">entière</w> <w n="32.5">en</w> <w n="32.6">un</w> <w n="32.7">soupir</w> <w n="32.8">exhale</w></l>
						<l n="33" num="1.33"><w n="33.1">La</w> <w n="33.2">réponse</w> <w n="33.3">à</w> <w n="33.4">ton</w> <w n="33.5">chant</w>, <w n="33.6">et</w> <w n="33.7">semble</w> <w n="33.8">s</w>’<w n="33.9">abreuver</w></l>
						<l n="34" num="1.34"><w n="34.1">Au</w> <w n="34.2">geste</w> <w n="34.3">fécondant</w> <w n="34.4">de</w> <w n="34.5">tes</w> <w n="34.6">deux</w> <w n="34.7">bras</w> <w n="34.8">levés</w></l>
						<l n="35" num="1.35"><w n="35.1">Son</w> <w n="35.2">ciel</w> <w n="35.3">a</w> <w n="35.4">reflété</w> <w n="35.5">ta</w> <w n="35.6">clarté</w> <w n="35.7">juvénile</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Ses</w> <w n="36.2">arbres</w> <w n="36.3">garderont</w> <w n="36.4">la</w> <w n="36.5">tendre</w> <w n="36.6">flexion</w></l>
						<l n="37" num="1.37"><w n="37.1">Et</w> <w n="37.2">le</w> <w n="37.3">rythme</w> <w n="37.4">émouvant</w> <w n="37.5">de</w> <w n="37.6">ton</w> <w n="37.7">beau</w> <w n="37.8">corps</w> <w n="37.9">gracile</w></l>
						<l n="38" num="1.38"><w n="38.1">Tu</w> <w n="38.2">la</w> <w n="38.3">feras</w> <w n="38.4">renaître</w> <w n="38.5">après</w> <w n="38.6">chaque</w> <w n="38.7">abandon</w></l>
						<l n="39" num="1.39"><w n="39.1">Plus</w> <w n="39.2">docile</w> <w n="39.3">à</w> <w n="39.4">ta</w> <w n="39.5">voix</w> <w n="39.6">sous</w> <w n="39.7">le</w> <w n="39.8">joug</w> <w n="39.9">des</w> <w n="39.10">années</w></l>
						<l n="40" num="1.40"><w n="40.1">Mais</w> <w n="40.2">un</w> <w n="40.3">joug</w> <w n="40.4">bien</w> <w n="40.5">plus</w> <w n="40.6">lourd</w> <w n="40.7">vient</w> <w n="40.8">entraver</w> <w n="40.9">l</w>’<w n="40.10">élan</w></l>
						<l n="41" num="1.41"><w n="41.1">Dont</w> <w n="41.2">j</w>’<w n="41.3">accueillais</w> <w n="41.4">ton</w> <w n="41.5">premier</w> <w n="41.6">souffle</w> <w n="41.7">étincelant</w></l>
						<l n="42" num="1.42"><w n="42.1">Tu</w> <w n="42.2">ne</w> <w n="42.3">peux</w> <w n="42.4">refleurir</w> <w n="42.5">toutes</w> <w n="42.6">les</w> <w n="42.7">destinées</w>…</l>
						<l n="43" num="1.43"><w n="43.1">Toi</w> <w n="43.2">que</w> <w n="43.3">j</w>’<w n="43.4">ai</w> <w n="43.5">tant</w> <w n="43.6">aimé</w>, <w n="43.7">toi</w> <w n="43.8">qui</w> <w n="43.9">me</w> <w n="43.10">répondais</w>,</l>
						<l n="44" num="1.44"><w n="44.1">Tu</w> <w n="44.2">ne</w> <w n="44.3">m</w>’<w n="44.4">apparais</w> <w n="44.5">plus</w>, <w n="44.6">Printemps</w>, <w n="44.7">quand</w> <w n="44.8">je</w> <w n="44.9">t</w>’<w n="44.10">implore</w></l>
						<l n="45" num="1.45"><w n="45.1">Et</w> <w n="45.2">le</w> <w n="45.3">ramier</w> <w n="45.4">donne</w> <w n="45.5">une</w> <w n="45.6">voix</w> <w n="45.7">à</w> <w n="45.8">mes</w> <w n="45.9">regrets</w></l>
						<l n="46" num="1.46"><w n="46.1">En</w> <w n="46.2">roucoulant</w> : « <w n="46.3">Erôs</w>, <w n="46.4">Erôs</w> »… <w n="46.5">il</w> <w n="46.6">pleure</w> <w n="46.7">encore</w>…</l>
					</lg>
				</div></body></text></TEI>