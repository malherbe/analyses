<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUIVANT L’HUMEUR DES JOURS</head><div type="poem" key="HRV11">
				<head type="main">LA BERCEUSE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">moins</w> <w n="1.4">mal</w> <w n="1.5">ce</w> <w n="1.6">soir</w>… <w n="1.7">Pourquoi</w> ?… <w n="1.8">Devine</w></l>
						<l n="2" num="1.2"><w n="2.1">C</w>’<w n="2.2">est</w> <w n="2.3">bien</w> <w n="2.4">toujours</w> <w n="2.5">le</w> <w n="2.6">même</w> <w n="2.7">tourment</w></l>
						<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">il</w> <w n="3.3">me</w> <w n="3.4">chante</w> <w n="3.5">pour</w> <w n="3.6">le</w> <w n="3.7">moment</w></l>
						<l n="4" num="1.4"><w n="4.1">Une</w> <w n="4.2">tendre</w> <w n="4.3">berceuse</w> <w n="4.4">en</w> <w n="4.5">sourdine</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tendre</w>, <w n="5.2">douce</w> <w n="5.3">à</w> <w n="5.4">mourir</w>, <w n="5.5">et</w> <w n="5.6">câline</w>,</l>
						<l n="6" num="2.2"><w n="6.1">La</w> <w n="6.2">berceuse</w> <w n="6.3">dit</w> <w n="6.4">le</w> <w n="6.5">changement</w></l>
						<l n="7" num="2.3"><w n="7.1">Du</w> <w n="7.2">visage</w> <w n="7.3">vieilli</w> <w n="7.4">de</w> <w n="7.5">l</w>’<w n="7.6">amant</w></l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">sa</w> <w n="8.3">jeunesse</w> <w n="8.4">qui</w> <w n="8.5">se</w> <w n="8.6">termine</w>…</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">En</w> <w n="9.2">sourdine</w> <w n="9.3">elle</w> <w n="9.4">m</w>’<w n="9.5">ôte</w> <w n="9.6">un</w> <w n="9.7">effroi</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Car</w> <w n="10.2">je</w> <w n="10.3">sais</w> <w n="10.4">que</w> <w n="10.5">pour</w> <w n="10.6">d</w>’<w n="10.7">autres</w> <w n="10.8">que</w> <w n="10.9">moi</w></l>
						<l n="11" num="3.3"><w n="11.1">Tes</w> <w n="11.2">lèvres</w> <w n="11.3">enfin</w> <w n="11.4">seront</w> <w n="11.5">sans</w> <w n="11.6">charmes</w></l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Et</w> <w n="12.2">tes</w> <w n="12.3">regards</w> <w n="12.4">dépourvus</w> <w n="12.5">d</w>’<w n="12.6">appas</w> !</l>
						<l n="13" num="4.2"><w n="13.1">Mon</w> <w n="13.2">tourment</w> <w n="13.3">chante</w>, <w n="13.4">chante</w> <w n="13.5">tout</w> <w n="13.6">bas</w>…</l>
						<l n="14" num="4.3"><w n="14.1">J</w>’<w n="14.2">ai</w> <w n="14.3">moins</w> <w n="14.4">mal</w>… <w n="14.5">Et</w> <w n="14.6">cependant</w> <w n="14.7">mes</w> <w n="14.8">larmes</w>…</l>
					</lg>
				</div></body></text></TEI>