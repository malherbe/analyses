<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">DILECTION</title>
				<title type="medium">Édition électronique</title>
				<author key="HRV">
					<name>
						<forename>Henriette</forename>
						<surname>HERVÉ</surname>
					</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>OCR, encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>3324 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">HRV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title type="main">DILECTION</title>
						<author>Henriette Hervé</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS MONTAIGNE</publisher>
							<date when="1925">1925</date>
						</imprint>
					</monogr>
					<note>Édition numérisée sur place</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1925">1925</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>.....</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les notes de bas de page ont été reportées en fin de poème.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-05-22" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-05-22" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">CHANTS DE PALESTINE</head><div type="poem" key="HRV40">
				<head type="main">LE PRESSOIR DES OLIVES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">faisait</w> <w n="1.3">presque</w> <w n="1.4">obscur</w> <w n="1.5">dans</w> <w n="1.6">l</w>’<w n="1.7">antique</w> <w n="1.8">pressoir</w></l>
						<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">je</w> <w n="2.3">me</w> <w n="2.4">suis</w> <w n="2.5">penchée</w> <w n="2.6">à</w> <w n="2.7">la</w> <w n="2.8">porte</w> <w n="2.9">entr</w>’<w n="2.10">ouverte</w>…</l>
						<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">dans</w> <w n="3.3">l</w>’<w n="3.4">ombre</w> <w n="3.5">odorante</w> <w n="3.6">et</w> <w n="3.7">chaude</w> <w n="3.8">ou</w> <w n="3.9">pouvait</w> <w n="3.10">voir</w></l>
						<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">lourde</w> <w n="4.3">roue</w> <w n="4.4">en</w> <w n="4.5">bois</w> <w n="4.6">presser</w> <w n="4.7">la</w> <w n="4.8">pulpe</w> <w n="4.9">verte</w></l>
						<l n="5" num="1.5"><w n="5.1">Des</w> <w n="5.2">olives</w> <w n="5.3">dans</w> <w n="5.4">le</w> <w n="5.5">cuvier</w> ; <w n="5.6">et</w> <w n="5.7">lentement</w></l>
						<l n="6" num="1.6"><w n="6.1">Elles</w> <w n="6.2">rendaient</w> <w n="6.3">leur</w> <w n="6.4">suc</w>, <w n="6.5">ainsi</w> <w n="6.6">qu</w>’<w n="6.7">un</w> <w n="6.8">clair</w> <w n="6.9">flot</w> <w n="6.10">d</w>’<w n="6.11">ambre</w></l>
						<l n="7" num="1.7"><w n="7.1">Qui</w> <w n="7.2">venait</w> <w n="7.3">ruisseler</w> <w n="7.4">jusqu</w>’<w n="7.5">au</w> <w n="7.6">sol</w> <w n="7.7">par</w> <w n="7.8">instant</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Sous</w> <w n="8.2">l</w>’<w n="8.3">effort</w> <w n="8.4">continu</w> <w n="8.5">de</w> <w n="8.6">tes</w> <w n="8.7">reins</w> <w n="8.8">qui</w> <w n="8.9">se</w> <w n="8.10">cambrent</w>…</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">tu</w> <w n="9.3">marchais</w> <w n="9.4">le</w> <w n="9.5">front</w> <w n="9.6">baissé</w>,. <w n="9.7">les</w> <w n="9.8">bras</w> <w n="9.9">tendus</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Pressant</w> <w n="10.2">à</w> <w n="10.3">chaque</w> <w n="10.4">tour</w> <w n="10.5">l</w>’<w n="10.6">onctueux</w> <w n="10.7">résidu</w>.</l>
						<l n="11" num="1.11">… <w n="11.1">Je</w> <w n="11.2">ne</w> <w n="11.3">t</w>’<w n="11.4">ai</w> <w n="11.5">pas</w> <w n="11.6">parlé</w>… <w n="11.7">mais_</w> <w n="11.8">j</w>’<w n="11.9">ai</w> <w n="11.10">vu</w> <w n="11.11">ton</w> <w n="11.12">visage</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Qu</w>’<w n="12.2">une</w> <w n="12.3">lampe</w> <w n="12.4">de</w> <w n="12.5">cuivre</w> <w n="12.6">éclairait</w> <w n="12.7">faiblement</w></l>
						<l n="13" num="1.13"><w n="13.1">Tu</w> <w n="13.2">souriais</w>… <w n="13.3">Moi</w>, <w n="13.4">je</w> <w n="13.5">rêvais</w>, <w n="13.6">en</w> <w n="13.7">m</w>’<w n="13.8">en</w> <w n="13.9">allant</w>…</l>
						<l n="14" num="1.14"><w n="14.1">J</w>’<w n="14.2">aperçus</w> <w n="14.3">dans</w> <w n="14.4">la</w> <w n="14.5">nuit</w> <w n="14.6">un</w> <w n="14.7">nouveau</w> <w n="14.8">paysage</w></l>
						<l n="15" num="1.15"><w n="15.1">Où</w> <w n="15.2">je</w> <w n="15.3">ne</w> <w n="15.4">trouvais</w> <w n="15.5">plus</w> <w n="15.6">les</w> <w n="15.7">contours</w> <w n="15.8">familiers</w></l>
						<l n="16" num="1.16"><w n="16.1">Des</w> <w n="16.2">champs</w> <w n="16.3">et</w> <w n="16.4">des</w> <w n="16.5">vallons</w>… <w n="16.6">Et</w> <w n="16.7">du</w> <w n="16.8">flanc</w> <w n="16.9">des</w> <w n="16.10">collines</w>.</l>
						<l n="17" num="1.17"><w n="17.1">S</w>’<w n="17.2">exhalaient</w> <w n="17.3">des</w>, <w n="17.4">parfums</w> <w n="17.5">plus</w> <w n="17.6">doux</w>, <w n="17.7">et</w> <w n="17.8">les</w> <w n="17.9">palmiers</w></l>
						<l n="18" num="1.18"><w n="18.1">Chantaient</w> <w n="18.2">plus</w> <w n="18.3">tendrement</w> <w n="18.4">au</w> <w n="18.5">vent</w> <w n="18.6">qui</w> <w n="18.7">les</w> <w n="18.8">incline</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">les</w> <w n="19.3">rossignols</w> <w n="19.4">répondaient</w>… <w n="19.5">Et</w> <w n="19.6">l</w>’<w n="19.7">univers</w></l>
						<l n="20" num="1.20"><w n="20.1">Était</w> <w n="20.2">fertile</w> <w n="20.3">et</w> <w n="20.4">beau</w> <w n="20.5">sous</w> <w n="20.6">un</w> <w n="20.7">grand</w> <w n="20.8">ciel</w> <w n="20.9">très</w> <w n="20.10">clair</w></l>
						<l n="21" num="1.21"><w n="21.1">Où</w> <w n="21.2">le</w> <w n="21.3">char</w> <w n="21.4">de</w> <w n="21.5">David</w> <w n="21.6">faisait</w> <w n="21.7">briller</w> <w n="21.8">ses</w> <w n="21.9">roues</w></l>
						<l n="22" num="1.22">…<w n="22.1">J</w>’<w n="22.2">ai</w> <w n="22.3">croisé</w> <w n="22.4">plus</w> <w n="22.5">d</w>’<w n="22.6">un</w> <w n="22.7">couple</w>, <w n="22.8">amis</w> <w n="22.9">ou</w> <w n="22.10">fiancés</w>…</l>
						<l n="23" num="1.23"><w n="23.1">Mais</w> <w n="23.2">leurs</w> <w n="23.3">yeux</w> <w n="23.4">éperdus</w> <w n="23.5">et</w> <w n="23.6">leurs</w> <w n="23.7">mains</w> <w n="23.8">qui</w> <w n="23.9">se</w> <w n="23.10">nouent</w></l>
						<l n="24" num="1.24"><w n="24.1">Tenaient</w> <w n="24.2">moins</w> <w n="24.3">de</w> <w n="24.4">bonheur</w> <w n="24.5">que</w> <w n="24.6">mes</w> <w n="24.7">doigts</w> <w n="24.8">enlacés</w></l>
						<l n="25" num="1.25"><w n="25.1">Qui</w> <w n="25.2">ne</w> <w n="25.3">portaient</w> <w n="25.4">qu</w>’<w n="25.5">un</w> <w n="25.6">souvenir</w> ! <w n="25.7">Sous</w> <w n="25.8">les</w> <w n="25.9">yeuses</w></l>
						<l n="26" num="1.26"><w n="26.1">J</w>’<w n="26.2">ai</w> <w n="26.3">pu</w> <w n="26.4">passer</w> <w n="26.5">près</w> <w n="26.6">des</w> <w n="26.7">amants</w>, <w n="26.8">sans</w> <w n="26.9">un</w> <w n="26.10">regret</w>,</l>
						<l n="27" num="1.27">‒ <w n="27.1">Ils</w> <w n="27.2">étaient</w> <w n="27.3">réunis</w>, <w n="27.4">j</w>’<w n="27.5">étais</w> <w n="27.6">seule</w>, <w n="27.7">c</w>’<w n="27.8">est</w> <w n="27.9">vrai</w>, ‒</l>
						<l n="28" num="1.28"><w n="28.1">Mais</w> <w n="28.2">je</w> <w n="28.3">t</w>’<w n="28.4">aimais</w>, <w n="28.5">je</w> <w n="28.6">t</w>’<w n="28.7">avais</w> <w n="28.8">vu</w>, <w n="28.9">j</w>’<w n="28.10">étais</w> <w n="28.11">heureuse</w>…</l>
					</lg>
				</div></body></text></TEI>