<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR3">
						<head type="main">Le Vieux Capitaine</head>
						<opener>
							<salute>À E. D***, peintre.</salute>
							<epigraph>
								<cit>
									<quote>
										Mais enfin le matelot crie : <lb></lb>
										Terre ! terre ! là-bas, voyez !…
									</quote>
									<bibl>
										<name>Béranger</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<div type="section" n="1">
							<head type="number">I</head>
							<lg n="1">
								<l n="1" num="1.1"><w n="1.1">Jean</w>, <w n="1.2">mon</w> <w n="1.3">vieux</w> <w n="1.4">matelot</w>, <w n="1.5">nous</w> <w n="1.6">touchons</w> : <w n="1.7">France</w> ! <w n="1.8">France</w> !</l>
								<l n="2" num="1.2"><w n="2.1">Cet</w> <w n="2.2">air</w>, <w n="2.3">de</w> <w n="2.4">nos</w> <w n="2.5">longs</w> <w n="2.6">cours</w>, <w n="2.7">emporte</w> <w n="2.8">la</w> <w n="2.9">souffrance</w>.</l>
								<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">Jean</w>, <w n="3.2">serait</w>-<w n="3.3">ce</w> <w n="3.4">une</w> <w n="3.5">erreur</w> ?</l>
								<l n="4" num="1.4"><w n="4.1">Vois</w>-<w n="4.2">tu</w>, <w n="4.3">dans</w> <w n="4.4">la</w> <w n="4.5">vapeur</w> <w n="4.6">qui</w> <w n="4.7">nous</w> <w n="4.8">cache</w> <w n="4.9">la</w> <w n="4.10">grève</w>,</l>
								<l n="5" num="1.5"><w n="5.1">Vois</w>-<w n="5.2">tu</w> <w n="5.3">là</w>-<w n="5.4">bas</w> <w n="5.5">flotter</w> ?… <w n="5.6">Non</w>, <w n="5.7">ce</w> <w n="5.8">n</w>’<w n="5.9">est</w> <w n="5.10">point</w> <w n="5.11">un</w> <w n="5.12">rêve</w> :</l>
								<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Il</w> <w n="6.2">vit</w>, <w n="6.3">notre</w> <w n="6.4">Empereur</w> !</l>
							</lg>
							<lg n="2">
								<l n="7" num="2.1"><w n="7.1">Jean</w>, <w n="7.2">embrasse</w>-<w n="7.3">moi</w> <w n="7.4">donc</w> !…. <w n="7.5">Tu</w> <w n="7.6">ris</w> <w n="7.7">et</w> <w n="7.8">tu</w> <w n="7.9">m</w>’<w n="7.10">assures</w></l>
								<l n="8" num="2.2"><w n="8.1">Par</w> <w n="8.2">tes</w> <w n="8.3">gros</w> <w n="8.4">pleurs</w> <w n="8.5">joyeux</w>, <w n="8.6">serre</w> <w n="8.7">moins</w> <w n="8.8">mes</w> <w n="8.9">blessures</w> ;</l>
								<l n="9" num="2.3"><space unit="char" quantity="12"></space><w n="9.1">Sens</w>-<w n="9.2">tu</w> <w n="9.3">battre</w> <w n="9.4">ce</w> <w n="9.5">cœur</w> ?</l>
								<l n="10" num="2.4"><w n="10.1">Heureux</w> ! <w n="10.2">le</w> <w n="10.3">serviteur</w> <w n="10.4">à</w> <w n="10.5">qui</w> <w n="10.6">Dieu</w> <w n="10.7">peut</w> <w n="10.8">permettre</w>,</l>
								<l n="11" num="2.5"><w n="11.1">Après</w> <w n="11.2">quinze</w> <w n="11.3">ans</w> <w n="11.4">d</w>’<w n="11.5">exil</w>, <w n="11.6">de</w> <w n="11.7">revoir</w> <w n="11.8">son</w> <w n="11.9">vieux</w> <w n="11.10">maître</w> :</l>
								<l n="12" num="2.6"><space unit="char" quantity="12"></space><w n="12.1">Il</w> <w n="12.2">vit</w>, <w n="12.3">notre</w> <w n="12.4">Empereur</w> !</l>
							</lg>
							<lg n="3">
								<l n="13" num="3.1"><w n="13.1">Jean</w>, <w n="13.2">que</w> <w n="13.3">simple</w> <w n="13.4">on</w> <w n="13.5">était</w> <w n="13.6">de</w> <w n="13.7">croire</w> <w n="13.8">à</w> <w n="13.9">cette</w> <w n="13.10">perte</w> :</l>
								<l n="14" num="3.2"><w n="14.1">J</w>’<w n="14.2">étais</w> <w n="14.3">bien</w> <w n="14.4">sûr</w> <w n="14.5">qu</w>’<w n="14.6">enfin</w>, <w n="14.7">de</w> <w n="14.8">son</w> <w n="14.9">île</w> <w n="14.10">déserte</w>,</l>
								<l n="15" num="3.3"><space unit="char" quantity="12"></space><w n="15.1">Loin</w> <w n="15.2">des</w> <w n="15.3">rois</w> <w n="15.4">la</w> <w n="15.5">terreur</w> !</l>
								<l n="16" num="3.4"><w n="16.1">Un</w> <w n="16.2">jour</w> <w n="16.3">il</w> <w n="16.4">reviendrait</w> <w n="16.5">debout</w>, <w n="16.6">la</w> <w n="16.7">lame</w> <w n="16.8">nue</w>,</l>
								<l n="17" num="3.5"><w n="17.1">Éveiller</w> <w n="17.2">ses</w> <w n="17.3">Français</w> <w n="17.4">avec</w> <w n="17.5">sa</w> <w n="17.6">voix</w> <w n="17.7">connue</w> :</l>
								<l n="18" num="3.6"><space unit="char" quantity="12"></space><w n="18.1">Il</w> <w n="18.2">vit</w>, <w n="18.3">notre</w> <w n="18.4">Empereur</w> !</l>
							</lg>
							<lg n="4">
								<l n="19" num="4.1"><w n="19.1">Jean</w>, <w n="19.2">que</w> <w n="19.3">simple</w> <w n="19.4">on</w> <w n="19.5">était</w> <w n="19.6">de</w> <w n="19.7">croire</w> <w n="19.8">que</w> <w n="19.9">cet</w> <w n="19.10">homme</w></l>
								<l n="20" num="4.2"><w n="20.1">Qui</w> <w n="20.2">se</w> <w n="20.3">sacra</w> <w n="20.4">lui</w>-<w n="20.5">même</w> <w n="20.6">avec</w> <w n="20.7">la</w> <w n="20.8">main</w> <w n="20.9">de</w> <w n="20.10">Rome</w>,</l>
								<l n="21" num="4.3"><space unit="char" quantity="12"></space><w n="21.1">Et</w> <w n="21.2">qui</w> <w n="21.3">s</w>’<w n="21.4">assit</w>, <w n="21.5">vainqueur</w>,</l>
								<l n="22" num="4.4"><w n="22.1">Déjouant</w> <w n="22.2">le</w> <w n="22.3">poignard</w>, <w n="22.4">riant</w> <w n="22.5">aux</w> <w n="22.6">anarchies</w>,</l>
								<l n="23" num="4.5"><w n="23.1">Sur</w> <w n="23.2">le</w> <w n="23.3">trône</w> <w n="23.4">détruit</w> <w n="23.5">des</w> <w n="23.6">vieilles</w> <w n="23.7">monarchies</w> :</l>
								<l n="24" num="4.6"><space unit="char" quantity="12"></space><w n="24.1">Il</w> <w n="24.2">vit</w>, <w n="24.3">notre</w> <w n="24.4">Empereur</w> !</l>
							</lg>
							<lg n="5">
								<l n="25" num="5.1"><w n="25.1">Jean</w>, <w n="25.2">que</w> <w n="25.3">simple</w> <w n="25.4">on</w> <w n="25.5">était</w> ! <w n="25.6">croire</w> <w n="25.7">que</w> <w n="25.8">l</w>’<w n="25.9">homme</w> <w n="25.10">austère</w></l>
								<l n="26" num="5.2"><w n="26.1">Qui</w> <w n="26.2">d</w>’<w n="26.3">un</w> <w n="26.4">geste</w>, <w n="26.5">dix</w> <w n="26.6">ans</w>, <w n="26.7">a</w> <w n="26.8">foudroyé</w> <w n="26.9">la</w> <w n="26.10">terre</w>,</l>
								<l n="27" num="5.3"><space unit="char" quantity="12"></space><w n="27.1">Mourrait</w> <w n="27.2">comme</w> <w n="27.3">un</w> <w n="27.4">pasteur</w> ;</l>
								<l n="28" num="5.4"><w n="28.1">N</w>’<w n="28.2">entend</w>-<w n="28.3">on</w> <w n="28.4">pas</w> <w n="28.5">le</w> <w n="28.6">brick</w> <w n="28.7">qui</w> <w n="28.8">s</w>’<w n="28.9">entr</w>’<w n="28.10">ouvre</w> <w n="28.11">et</w> <w n="28.12">qui</w> <w n="28.13">lutte</w>,</l>
								<l n="29" num="5.5"><w n="29.1">Ou</w> <w n="29.2">le</w> <w n="29.3">cri</w> <w n="29.4">du</w> <w n="29.5">rocher</w> <w n="29.6">qui</w> <w n="29.7">s</w>’<w n="29.8">écrase</w> <w n="29.9">en</w> <w n="29.10">sa</w> <w n="29.11">chute</w> ?…</l>
								<l n="30" num="5.6"><space unit="char" quantity="12"></space><w n="30.1">Il</w> <w n="30.2">vit</w>, <w n="30.3">notre</w> <w n="30.4">Empereur</w> !</l>
							</lg>
							<lg n="6">
								<l n="31" num="6.1"><w n="31.1">Jean</w>, <w n="31.2">comme</w> <w n="31.3">nous</w> <w n="31.4">un</w> <w n="31.5">jour</w>, <w n="31.6">s</w>’<w n="31.7">il</w> <w n="31.8">doit</w> <w n="31.9">quitter</w> <w n="31.10">ce</w> <w n="31.11">monde</w>,</l>
								<l n="32" num="6.2"><w n="32.1">Le</w> <w n="32.2">globe</w> <w n="32.3">sentira</w> <w n="32.4">la</w> <w n="32.5">secousse</w> <w n="32.6">profonde</w>,</l>
								<l n="33" num="6.3"><space unit="char" quantity="12"></space><w n="33.1">Jetant</w> <w n="33.2">une</w> <w n="33.3">clameur</w> :</l>
								<l n="34" num="6.4"><w n="34.1">Comme</w> <w n="34.2">à</w> <w n="34.3">la</w> <w n="34.4">mort</w> <w n="34.5">du</w> <w n="34.6">Christ</w>, <w n="34.7">prodiges</w> <w n="34.8">sans</w> <w n="34.9">exemple</w>,</l>
								<l n="35" num="6.5"><w n="35.1">Déchireront</w> <w n="35.2">la</w> <w n="35.3">terre</w> <w n="35.4">et</w> <w n="35.5">le</w> <w n="35.6">voile</w> <w n="35.7">du</w> <w n="35.8">Temple</w> !</l>
								<l n="36" num="6.6"><space unit="char" quantity="12"></space><w n="36.1">Il</w> <w n="36.2">vit</w>, <w n="36.3">notre</w> <w n="36.4">Empereur</w> !</l>
							</lg>
							<lg n="7">
								<l n="37" num="7.1"><w n="37.1">Jean</w>, <w n="37.2">cargue</w> <w n="37.3">le</w> <w n="37.4">pennon</w>, <w n="37.5">pavillon</w> <w n="37.6">qu</w>’<w n="37.7">on</w> <w n="37.8">abhorre</w>,</l>
								<l n="38" num="7.2"><w n="38.1">Attachons</w> <w n="38.2">à</w> <w n="38.3">ces</w> <w n="38.4">mâts</w> <w n="38.5">ce</w> <w n="38.6">flottant</w> <w n="38.7">météore</w></l>
								<l n="39" num="7.3"><space unit="char" quantity="12"></space><w n="39.1">Qu</w>’<w n="39.2">envoie</w> <w n="39.3">un</w> <w n="39.4">ciel</w> <w n="39.5">vengeur</w> !</l>
								<l n="40" num="7.4"><w n="40.1">A</w> <w n="40.2">sa</w> <w n="40.3">vue</w>, <w n="40.4">ébloui</w>, <w n="40.5">l</w>’<w n="40.6">ossifrague</w> <w n="40.7">s</w>’<w n="40.8">arrête</w> ;</l>
								<l n="41" num="7.5"><w n="41.1">Et</w> <w n="41.2">la</w> <w n="41.3">vague</w> <w n="41.4">en</w> <w n="41.5">respect</w> <w n="41.6">semble</w> <w n="41.7">incliner</w> <w n="41.8">sa</w> <w n="41.9">tête</w> :</l>
								<l n="42" num="7.6"><space unit="char" quantity="12"></space><w n="42.1">Il</w> <w n="42.2">vit</w>, <w n="42.3">notre</w> <w n="42.4">Empereur</w> !…</l>
							</lg>
							<lg n="8">
								<l n="43" num="8.1"><w n="43.1">Jean</w>, <w n="43.2">tout</w> <w n="43.3">comme</w> <w n="43.4">un</w> <w n="43.5">obus</w> <w n="43.6">mon</w> <w n="43.7">cœur</w> <w n="43.8">en</w> <w n="43.9">joie</w> <w n="43.10">éclate</w>.</l>
								<l n="44" num="8.2"><w n="44.1">Qu</w>’<w n="44.2">il</w> <w n="44.3">est</w> <w n="44.4">beau</w>, <w n="44.5">comme</w> <w n="44.6">il</w> <w n="44.7">flotte</w>, <w n="44.8">azur</w> ! <w n="44.9">blanc</w> ! <w n="44.10">écarlate</w> !</l>
								<l n="45" num="8.3"><space unit="char" quantity="12"></space><w n="45.1">Le</w> <w n="45.2">drapeau</w> <w n="45.3">rédempteur</w>,</l>
								<l n="46" num="8.4"><w n="46.1">Qui</w> <w n="46.2">de</w> <w n="46.3">son</w> <w n="46.4">long</w> <w n="46.5">tissu</w>, <w n="46.6">mortuaire</w> <w n="46.7">enveloppe</w>,</l>
								<l n="47" num="8.5"><w n="47.1">Emmaillota</w> <w n="47.2">les</w> <w n="47.3">rois</w>, <w n="47.4">emmantela</w> <w n="47.5">l</w>’<w n="47.6">Europe</w> !</l>
								<l n="48" num="8.6"><space unit="char" quantity="12"></space><w n="48.1">Il</w> <w n="48.2">vit</w>, <w n="48.3">notre</w> <w n="48.4">Empereur</w> !…</l>
							</lg>
							<lg n="9">
								<l n="49" num="9.1"><w n="49.1">Jean</w>, <w n="49.2">cours</w> <w n="49.3">aux</w> <w n="49.4">canonniers</w>, <w n="49.5">dis</w>-<w n="49.6">leur</w> <w n="49.7">que</w> <w n="49.8">la</w> <w n="49.9">patrie</w></l>
								<l n="50" num="9.2"><w n="50.1">A</w> <w n="50.2">secoué</w> <w n="50.3">le</w> <w n="50.4">joug</w>, <w n="50.5">que</w> <w n="50.6">notre</w> <w n="50.7">artillerie</w></l>
								<l n="51" num="9.3"><space unit="char" quantity="12"></space><w n="51.1">Doit</w> <w n="51.2">tonner</w> <w n="51.3">ce</w> <w n="51.4">bonheur</w> !</l>
								<l n="52" num="9.4"><w n="52.1">Que</w> <w n="52.2">tribord</w> <w n="52.3">et</w> <w n="52.4">bâbord</w> <w n="52.5">lancent</w> <w n="52.6">vingt</w> <w n="52.7">fois</w> <w n="52.8">leur</w> <w n="52.9">foudre</w> !</l>
								<l n="53" num="9.5"><w n="53.1">Dieu</w> ! <w n="53.2">que</w> <w n="53.3">de</w> <w n="53.4">patients</w> <w n="53.5">ce</w> <w n="53.6">jour</w>-<w n="53.7">là</w> <w n="53.8">doit</w> <w n="53.9">absoudre</w> !…</l>
								<l n="54" num="9.6"><space unit="char" quantity="12"></space><w n="54.1">Il</w> <w n="54.2">vit</w>, <w n="54.3">notre</w> <w n="54.4">Empereur</w> !</l>
							</lg>
						</div>
						<div type="section" n="2">
							<head type="number">II</head>
							<lg n="1">
								<l n="55" num="1.1"><w n="55.1">Jean</w>, <w n="55.2">quel</w> <w n="55.3">est</w> <w n="55.4">donc</w> <w n="55.5">ce</w> <w n="55.6">cri</w> <w n="55.7">que</w>, <w n="55.8">là</w>-<w n="55.9">bas</w> <w n="55.10">sur</w> <w n="55.11">la</w> <w n="55.12">plage</w>,</l>
								<l n="56" num="1.2"><space unit="char" quantity="8"></space><w n="56.1">La</w> <w n="56.2">foule</w> <w n="56.3">a</w> <w n="56.4">cent</w> <w n="56.5">fois</w> <w n="56.6">répété</w> ?</l>
								<l n="57" num="1.3"><w n="57.1">Est</w>-<w n="57.2">ce</w> <w n="57.3">Napoléon</w> ? — <w n="57.4">Non</w>, <w n="57.5">dans</w> <w n="57.6">ces</w> <w n="57.7">cris</w> <w n="57.8">de</w> <w n="57.9">rage</w>,</l>
								<l n="58" num="1.4"><space unit="char" quantity="8"></space><w n="58.1">Je</w> <w n="58.2">n</w>’<w n="58.3">entends</w> <w n="58.4">rien</w> <w n="58.5">que</w> : <w n="58.6">Liberté</w>.</l>
								<l n="59" num="1.5"><w n="59.1">Cependant</w>, <w n="59.2">couronnant</w> <w n="59.3">le</w> <w n="59.4">chef</w> <w n="59.5">du</w> <w n="59.6">la</w> <w n="59.7">bannière</w>,</l>
								<l n="60" num="1.6"><space unit="char" quantity="8"></space><w n="60.1">C</w>’<w n="60.2">est</w> <w n="60.3">bien</w> <w n="60.4">un</w> <w n="60.5">aigle</w> <w n="60.6">que</w> <w n="60.7">je</w> <w n="60.8">vois</w> ?</l>
								<l n="61" num="1.7"><w n="61.1">Oui</w> ! <w n="61.2">l</w>’<w n="61.3">aigle</w> <w n="61.4">impérial</w> <w n="61.5">enserrant</w> <w n="61.6">le</w> <w n="61.7">tonnerre</w> !…</l>
								<l n="62" num="1.8">— <w n="62.1">Pardon</w>, <w n="62.2">mon</w> <w n="62.3">commandant</w>, <w n="62.4">c</w>’<w n="62.5">est</w> <w n="62.6">le</w> <w n="62.7">vieux</w> <w n="62.8">coq</w> <w n="62.9">gaulois</w> !</l>
							</lg>
							<lg n="2">
								<l n="63" num="2.1"><w n="63.1">A</w> <w n="63.2">ces</w> <w n="63.3">mots</w>, <w n="63.4">sur</w> <w n="63.5">le</w> <w n="63.6">pont</w>, <w n="63.7">on</w> <w n="63.8">voit</w> <w n="63.9">le</w> <w n="63.10">capitaine</w></l>
								<l n="64" num="2.2"><space unit="char" quantity="12"></space><w n="64.1">Pâlir</w> <w n="64.2">et</w> <w n="64.3">reculer</w> ;</l>
								<l n="65" num="2.3"><w n="65.1">Et</w> <w n="65.2">les</w> <w n="65.3">deux</w> <w n="65.4">vétérans</w>, <w n="65.5">la</w> <w n="65.6">mine</w> <w n="65.7">moins</w> <w n="65.8">hautaine</w>,</l>
								<l n="66" num="2.4"><space unit="char" quantity="8"></space><w n="66.1">Se</w> <w n="66.2">regardent</w> <w n="66.3">sans</w> <w n="66.4">se</w> <w n="66.5">parler</w>.</l>
								<l n="67" num="2.5"><w n="67.1">Plus</w> <w n="67.2">surpris</w> <w n="67.3">et</w> <w n="67.4">défaits</w> <w n="67.5">que</w> <w n="67.6">dans</w> <w n="67.7">la</w> <w n="67.8">nuit</w> <w n="67.9">fatale</w>,</l>
								<l n="68" num="2.6"><space unit="char" quantity="8"></space><w n="68.1">Et</w>, <w n="68.2">dans</w> <w n="68.3">son</w> <w n="68.4">fol</w> <w n="68.5">enivrement</w>,</l>
								<l n="69" num="2.7"><w n="69.1">Une</w> <w n="69.2">fille</w> <w n="69.3">qui</w> <w n="69.4">croit</w> <w n="69.5">accoler</w> <w n="69.6">son</w> <w n="69.7">amant</w>,</l>
								<l n="70" num="2.8"><space unit="char" quantity="8"></space><w n="70.1">Et</w> <w n="70.2">qui</w> <w n="70.3">baise</w> <w n="70.4">au</w> <w n="70.5">front</w>, <w n="70.6">sa</w> <w n="70.7">rivale</w>.</l>
							</lg>
						</div>
					</div></body></text></TEI>