<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR23">
						<head type="main">À Jules Vabre</head>
						<head type="sub_2">Architecte</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">bonne</w> <w n="1.3">foi</w>, <w n="1.4">Jules</w> <w n="1.5">Vabre</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Compagnon</w> <w n="2.2">miraculeux</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Aux</w> <w n="3.2">regards</w> <w n="3.3">méticuleux</w></l>
							<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">bourgeois</w> <w n="4.3">à</w> <w n="4.4">menton</w> <w n="4.5">glabre</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Devons</w>-<w n="5.2">nous</w> <w n="5.3">sembler</w> <w n="5.4">follet</w></l>
							<l n="6" num="1.6"><w n="6.1">Dans</w> <w n="6.2">ce</w> <w n="6.3">monde</w> <w n="6.4">où</w> <w n="6.5">tout</w> <w n="6.6">se</w> <w n="6.7">range</w> !</l>
							<l n="7" num="1.7"><w n="7.1">Devons</w>-<w n="7.2">nous</w> <w n="7.3">sembler</w> <w n="7.4">étrange</w>,</l>
							<l n="8" num="1.8"><w n="8.1">Nous</w>, <w n="8.2">faisant</w> <w n="8.3">ce</w> <w n="8.4">qu</w>’<w n="8.5">il</w> <w n="8.6">nous</w> <w n="8.7">plaît</w> !</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Dans</w> <w n="9.2">Paris</w>, <w n="9.3">ville</w> <w n="9.4">accroupie</w>,</l>
							<l n="10" num="2.2"><w n="10.1">Passant</w> <w n="10.2">comme</w> <w n="10.3">un</w> <w n="10.4">brin</w> <w n="10.5">sur</w> <w n="10.6">l</w>’<w n="10.7">eau</w>,</l>
							<l n="11" num="2.3"><w n="11.1">Comme</w> <w n="11.2">un</w> <w n="11.3">vagabond</w> <w n="11.4">ruisseau</w></l>
							<l n="12" num="2.4"><w n="12.1">Dans</w> <w n="12.2">une</w> <w n="12.3">mare</w> <w n="12.4">croupie</w>.</l>
							<l n="13" num="2.5"><w n="13.1">Bohémiens</w>, <w n="13.2">sans</w> <w n="13.3">toits</w>, <w n="13.4">sans</w> <w n="13.5">bancs</w>,</l>
							<l n="14" num="2.6"><w n="14.1">Sans</w> <w n="14.2">existence</w> <w n="14.3">engaînée</w>,</l>
							<l n="15" num="2.7"><w n="15.1">Menant</w> <w n="15.2">vie</w> <w n="15.3">abandonnée</w>,</l>
							<l n="16" num="2.8"><w n="16.1">Ainsi</w> <w n="16.2">que</w> <w n="16.3">des</w> <w n="16.4">moineaux</w> <w n="16.5">francs</w></l>
							<l n="17" num="2.9"><w n="17.1">Au</w> <w n="17.2">chef</w> <w n="17.3">d</w>’<w n="17.4">une</w> <w n="17.5">cheminée</w> !</l>
						</lg>
						<lg n="3">
							<l n="18" num="3.1"><w n="18.1">Chats</w> <w n="18.2">de</w> <w n="18.3">coulisse</w>, <w n="18.4">endêvés</w> !</l>
							<l n="19" num="3.2"><w n="19.1">Devant</w> <w n="19.2">la</w> <w n="19.3">salle</w> <w n="19.4">ébahie</w></l>
							<l n="20" num="3.3"><w n="20.1">Traversant</w>, <w n="20.2">rideaux</w> <w n="20.3">levés</w>,</l>
							<l n="21" num="3.4"><w n="21.1">Le</w> <w n="21.2">théâtre</w> <w n="21.3">de</w> <w n="21.4">la</w> <w n="21.5">vie</w>.</l>
						</lg>
					</div></body></text></TEI>