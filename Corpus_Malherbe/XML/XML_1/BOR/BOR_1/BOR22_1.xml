<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR22">
						<head type="main">Sur le refus du tableau, <lb></lb>La Mort de Bailli, par le jury</head>
						<opener>
							<epigraph>
								<cit>
									<quote>
										E dolce il pianto piu ch’altri non crede.
									</quote>
									<bibl>
										<name>PETRARCA</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Laisse</w>-<w n="1.2">moi</w>, <w n="1.3">Boulanger</w>, <w n="1.4">dans</w> <w n="1.5">ta</w> <w n="1.6">douleur</w> <w n="1.7">profonde</w></l>
							<l n="2" num="1.2"><w n="2.1">Descendre</w> <w n="2.2">tout</w> <w n="2.3">entier</w> <w n="2.4">par</w> <w n="2.5">ses</w> <w n="2.6">noirs</w> <w n="2.7">soupiraux</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Laisse</w> <w n="3.2">immiscer</w> <w n="3.3">ma</w> <w n="3.4">rage</w> <w n="3.5">à</w> <w n="3.6">ta</w> <w n="3.7">plainte</w> <w n="3.8">qui</w> <w n="3.9">gronde</w> ;</l>
							<l n="4" num="1.4"><w n="4.1">Laisse</w> <w n="4.2">pilorier</w> <w n="4.3">tes</w> <w n="4.4">iniques</w> <w n="4.5">bourreaux</w> ;</l>
							<l n="5" num="1.5"><w n="5.1">Laisse</w>-<w n="5.2">moi</w> <w n="5.3">sur</w> <w n="5.4">leur</w> <w n="5.5">front</w> <w n="5.6">clouer</w> <w n="5.7">l</w>’<w n="5.8">ignominie</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">traîner</w> <w n="6.3">sur</w> <w n="6.4">la</w> <w n="6.5">claie</w> <w n="6.6">au</w> <w n="6.7">banc</w> <w n="6.8">du</w> <w n="6.9">carrefour</w>,</l>
							<l n="7" num="1.7"><w n="7.1">Tribunal</w> <w n="7.2">plébéien</w>, <w n="7.3">d</w>’<w n="7.4">où</w> <w n="7.5">la</w> <w n="7.6">fourbe</w> <w n="7.7">est</w> <w n="7.8">bannie</w>,</l>
							<l n="8" num="1.8"><w n="8.1">Où</w> <w n="8.2">l</w>’<w n="8.3">on</w> <w n="8.4">jette</w> <w n="8.5">pourvoi</w> <w n="8.6">des</w> <w n="8.7">arrêtés</w> <w n="8.8">de</w> <w n="8.9">cour</w> !</l>
							<l n="9" num="1.9"><w n="9.1">Car</w> <w n="9.2">il</w> <w n="9.3">est</w> <w n="9.4">temps</w> <w n="9.5">enfin</w> <w n="9.6">qu</w>’<w n="9.7">au</w> <w n="9.8">soleil</w> <w n="9.9">on</w> <w n="9.10">flétrisse</w></l>
							<l n="10" num="1.10"><w n="10.1">Ces</w> <w n="10.2">courtisans</w> <w n="10.3">flairant</w> <w n="10.4">au</w> <w n="10.5">cul</w> <w n="10.6">de</w> <w n="10.7">tout</w> <w n="10.8">pouvoir</w>,</l>
							<l n="11" num="1.11"><w n="11.1">Ces</w> <w n="11.2">ouvriers</w> <w n="11.3">déclins</w>, <w n="11.4">à</w> <w n="11.5">la</w> <w n="11.6">figure</w> <w n="11.7">actrice</w>,</l>
							<l n="12" num="1.12"><w n="12.1">Que</w> <w n="12.2">Urbaine</w> <w n="12.3">et</w> <w n="12.4">les</w> <w n="12.5">sous</w> <w n="12.6">peuvent</w> <w n="12.7">seuls</w> <w n="12.8">émouvoir</w> !</l>
							<l n="13" num="1.13"><w n="13.1">Détriments</w> <w n="13.2">de</w> <w n="13.3">l</w>’<w n="13.4">Empire</w>, <w n="13.5">étreignant</w> <w n="13.6">notre</w> <w n="13.7">époque</w>,</l>
						</lg>
						<lg n="2">
							<l n="14" num="2.1"><w n="14.1">Qui</w> <w n="14.2">triture</w> <w n="14.3">du</w> <w n="14.4">pied</w> <w n="14.5">leurs</w> <w n="14.6">cœurs</w> <w n="14.7">étroits</w> <w n="14.8">et</w> <w n="14.9">secs</w> ;</l>
							<l n="15" num="2.2"><w n="15.1">Détriments</w> <w n="15.2">du</w> <w n="15.3">passé</w> <w n="15.4">que</w> <w n="15.5">le</w> <w n="15.6">siècle</w> <w n="15.7">révoque</w>,</l>
							<l n="16" num="2.3"><w n="16.1">Fabricateurs</w> <w n="16.2">à</w> <w n="16.3">plats</w> <w n="16.4">de</w> <w n="16.5">Romains</w> <w n="16.6">et</w> <w n="16.7">de</w> <w n="16.8">Grecs</w> ;</l>
							<l n="17" num="2.4"><w n="17.1">Lauréats</w>, <w n="17.2">à</w> <w n="17.3">deux</w> <w n="17.4">mains</w>, <w n="17.5">retenant</w> <w n="17.6">leur</w>’ <w n="17.7">couronne</w>,</l>
							<l n="18" num="2.5"><w n="18.1">Qui</w>, <w n="18.2">caduque</w>, <w n="18.3">déchoit</w> <w n="18.4">de</w> <w n="18.5">leur</w> <w n="18.6">front</w> <w n="18.7">conspué</w> ;</l>
							<l n="19" num="2.6"><w n="19.1">Gauchement</w> <w n="19.2">ameutés</w>, <w n="19.3">et</w> <w n="19.4">grinçant</w> <w n="19.5">sur</w> <w n="19.6">leur</w> <w n="19.7">trône</w></l>
							<l n="20" num="2.7"><w n="20.1">Contre</w> <w n="20.2">un</w> <w n="20.3">âge</w> <w n="20.4">puissant</w> <w n="20.5">qui</w> <w n="20.6">sur</w> <w n="20.7">eux</w> <w n="20.8">a</w> <w n="20.9">rué</w>.</l>
							<l n="21" num="2.8"><w n="21.1">Comme</w> <w n="21.2">un</w> <w n="21.3">ours</w> <w n="21.4">montagnard</w> <w n="21.5">qui</w> <w n="21.6">sur</w> <w n="21.7">les</w> <w n="21.8">rocs</w> <w n="21.9">se</w> <w n="21.10">traîne</w>,</l>
							<l n="22" num="2.9"><w n="22.1">Saigneux</w>, <w n="22.2">frappé</w> <w n="22.3">de</w> <w n="22.4">mort</w>, <w n="22.5">ils</w> <w n="22.6">voudraient</w> <w n="22.7">dévorer</w>,</l>
							<l n="23" num="2.10"><w n="23.1">Étouffer</w> <w n="23.2">sous</w> <w n="23.3">leurs</w> <w n="23.4">brus</w>, <w n="23.5">et</w> <w n="23.6">broyer</w> <w n="23.7">sur</w> <w n="23.8">l</w>’<w n="23.9">arène</w>,</l>
							<l n="24" num="2.11"><w n="24.1">Tout</w> <w n="24.2">ce</w> <w n="24.3">qui</w> <w n="24.4">gît</w> <w n="24.5">debout</w> <w n="24.6">avant</w> <w n="24.7">que</w> <w n="24.8">d</w>’<w n="24.9">expirer</w>.</l>
						</lg>
						<lg n="3">
							<l n="25" num="3.1"><w n="25.1">Voilà</w> <w n="25.2">donc</w> <w n="25.3">ce</w> <w n="25.4">qu</w>’<w n="25.5">étaient</w> <w n="25.6">tes</w> <w n="25.7">jugeurs</w> ! <w n="25.8">Le</w> <w n="25.9">prétoire</w>,</l>
							<l n="26" num="3.2"><w n="26.1">A</w> <w n="26.2">l</w>’<w n="26.3">aspect</w> <w n="26.4">du</w> <w n="26.5">tableau</w> <w n="26.6">par</w> <w n="26.7">la</w> <w n="26.8">peur</w> <w n="26.9">assailli</w>,</l>
							<l n="27" num="3.3"><w n="27.1">Qui</w> <w n="27.2">se</w> <w n="27.3">plaignit</w>-<w n="27.4">pourquoi</w> <w n="27.5">la</w> <w n="27.6">désolante</w> <w n="27.7">histoire</w></l>
							<l n="28" num="3.4"><w n="28.1">Fait</w> <w n="28.2">le</w> <w n="28.3">peuple</w> <w n="28.4">si</w> <w n="28.5">laid</w> <w n="28.6">et</w> <w n="28.7">si</w> <w n="28.8">beau</w> <w n="28.9">le</w> <w n="28.10">Bailli</w> :</l>
							<l n="29" num="3.5"><w n="29.1">Prétexte</w> <w n="29.2">captieux</w> <w n="29.3">et</w> <w n="29.4">couvrant</w> <w n="29.5">une</w> <w n="29.6">trame</w>.</l>
							<l n="30" num="3.6"><w n="30.1">Depuis</w> <w n="30.2">quand</w>, <w n="30.3">pour</w> <w n="30.4">la</w> <w n="30.5">plèbe</w>, <w n="30.6">êtes</w>-<w n="30.7">vous</w> <w n="30.8">si</w> <w n="30.9">courtois</w>,</l>
							<l n="31" num="3.7"><w n="31.1">A</w> <w n="31.2">propos</w> <w n="31.3">Jacobins</w>, <w n="31.4">chiens</w> <w n="31.5">de</w> <w n="31.6">palais</w> <w n="31.7">dans</w> <w n="31.8">l</w>’<w n="31.9">âme</w>,</l>
							<l n="32" num="3.8"><w n="32.1">Léchant</w> <w n="32.2">la</w> <w n="32.3">populace</w> ?… <w n="32.4">O</w> <w n="32.5">vous</w> <w n="32.6">êtes</w> <w n="32.7">matois</w> !…</l>
							<l n="33" num="3.9"><w n="33.1">Alors</w>, <w n="33.2">en</w> <w n="33.3">ricanant</w>, <w n="33.4">ils</w> <w n="33.5">ont</w> <w n="33.6">dit</w> <w n="33.7">anathème</w></l>
							<l n="34" num="3.10"><w n="34.1">Sur</w> <w n="34.2">ton</w> <w n="34.3">hardi</w> <w n="34.4">labeur</w> ; <w n="34.5">puis</w> <w n="34.6">chassé</w> <w n="34.7">du</w> <w n="34.8">salon</w></l>
							<l n="35" num="3.11"><w n="35.1">Cette</w> <w n="35.2">page</w> <w n="35.3">d</w>’<w n="35.4">espoir</w>, <w n="35.5">ce</w> <w n="35.6">taciturne</w> <w n="35.7">thème</w></l>
							<l n="36" num="3.12"><w n="36.1">De</w> <w n="36.2">ton</w> <w n="36.3">pinceau</w> <w n="36.4">si</w> <w n="36.5">vrai</w> <w n="36.6">qu</w>’<w n="36.7">ils</w> <w n="36.8">appellent</w> <w n="36.9">félon</w>.</l>
							<l n="37" num="3.13"><w n="37.1">Quoi</w> ! <w n="37.2">le</w> <w n="37.3">Christ</w> <w n="37.4">est</w> <w n="37.5">chassé</w> <w n="37.6">par</w> <w n="37.7">les</w> <w n="37.8">vendeurs</w> <w n="37.9">du</w> <w n="37.10">Temple</w> !</l>
							<l n="38" num="3.14"><w n="38.1">Horreur</w> !… <w n="38.2">le</w> <w n="38.3">jour</w> <w n="38.4">vengeur</w>, <w n="38.5">pour</w> <w n="38.6">nous</w>, <w n="38.7">bientôt</w> <w n="38.8">poindra</w>.</l>
							<l n="39" num="3.15"><w n="39.1">Leur</w> <w n="39.2">tombe</w> <w n="39.3">est</w> <w n="39.4">entr</w>’<w n="39.5">ouverte</w> <w n="39.6">et</w> <w n="39.7">la</w> <w n="39.8">porte</w> <w n="39.9">en</w> <w n="39.10">est</w> <w n="39.11">ample</w>,</l>
							<l n="40" num="3.16"><w n="40.1">Et</w> <w n="40.2">leur</w> <w n="40.3">œuvre</w> <w n="40.4">avec</w> <w n="40.5">eux</w> <w n="40.6">au</w> <w n="40.7">néant</w> <w n="40.8">descendra</w> !…</l>
						</lg>
						<closer>
							<dateline>
								<date when="1831">Août 1831.</date>
								</dateline>
						</closer>
					</div></body></text></TEI>