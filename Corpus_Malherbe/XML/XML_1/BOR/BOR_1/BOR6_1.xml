<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR6">
						<head type="main">Le Rendez-vous</head>
						<opener>
							<salute>À EUGÈNE BION, statuaire.</salute>
							<epigraph>
								<cit>
									<quote>
										Au luisant de la moucharde…
									</quote>
									<bibl>
										<name>ARGOT</name>.
									</bibl>
								</cit>
								<cit>
									<quote>
										…Enfin au cimetière, <lb></lb>
										Un soir d’automne, sombre et grisâtre, une bière <lb></lb>
										Fut apportée !…
									</quote>
									<bibl>
										<name>Théophile GAUTIER</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">m</w>’<w n="1.3">avais</w> <w n="1.4">dit</w> : <w n="1.5">Au</w> <w n="1.6">soir</w> <w n="1.7">fidèle</w></l>
							<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">reparaît</w> <w n="2.3">le</w> <w n="2.4">bûcheron</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Quand</w>, <w n="3.2">penché</w> <w n="3.3">sur</w> <w n="3.4">son</w> <w n="3.5">escabelle</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">sein</w> <w n="4.3">de</w> <w n="4.4">sa</w> <w n="4.5">famille</w> <w n="4.6">en</w> <w n="4.7">rond</w>,</l>
							<l n="5" num="1.5"><w n="5.1">Il</w> <w n="5.2">partage</w> <w n="5.3">dans</w> <w n="5.4">sa</w> <w n="5.5">misère</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Triste</w> <w n="6.2">gain</w> <w n="6.3">de</w> <w n="6.4">sa</w> <w n="6.5">peine</w> <w n="6.6">amère</w>,</l>
							<l n="7" num="1.7"><w n="7.1">Un</w> <w n="7.2">peu</w> <w n="7.3">de</w>’ <w n="7.4">pain</w> <w n="7.5">à</w> <w n="7.6">ses</w> <w n="7.7">enfants</w>,</l>
							<l n="8" num="1.8"><w n="8.1">Qu</w>’<w n="8.2">au</w> <w n="8.3">loin</w> <w n="8.4">l</w>’<w n="8.5">ambition</w> <w n="8.6">n</w>’<w n="8.7">entraîne</w>,</l>
							<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">dont</w> <w n="9.3">nul</w> <w n="9.4">proscrit</w> <w n="9.5">par</w> <w n="9.6">la</w> <w n="9.7">haine</w>,</l>
							<l n="10" num="1.10"><w n="10.1">Ne</w> <w n="10.2">manque</w> <w n="10.3">à</w> <w n="10.4">ses</w> <w n="10.5">embrassements</w>.</l>
						</lg>
						<lg n="2">
							<l n="11" num="2.1"><w n="11.1">Tu</w> <w n="11.2">m</w>’<w n="11.3">avais</w> <w n="11.4">dit</w> : <w n="11.5">Toi</w>, <w n="11.6">que</w> <w n="11.7">j</w>’<w n="11.8">adore</w> !</l>
							<l n="12" num="2.2"><w n="12.1">Tout</w> <w n="12.2">bas</w> <w n="12.3">avec</w> <w n="12.4">ta</w> <w n="12.5">douce</w> <w n="12.6">voix</w>.</l>
							<l n="13" num="2.3"><w n="13.1">Du</w> <w n="13.2">beffroi</w>, <w n="13.3">quand</w> <w n="13.4">l</w>’<w n="13.5">airain</w> <w n="13.6">sonore</w></l>
							<l n="14" num="2.4"><w n="14.1">Dans</w> <w n="14.2">l</w>’<w n="14.3">air</w> <w n="14.4">bourdonnera</w> <w n="14.5">sept</w> <w n="14.6">fois</w> ;</l>
							<l n="15" num="2.5"><w n="15.1">Quand</w> <w n="15.2">sous</w> <w n="15.3">l</w>’<w n="15.4">arc</w> <w n="15.5">du</w> <w n="15.6">jubé</w> <w n="15.7">gothique</w>,</l>
							<l n="16" num="2.6"><w n="16.1">Le</w> <w n="16.2">curé</w> <w n="16.3">d</w>’<w n="16.4">une</w> <w n="16.5">main</w> <w n="16.6">rustique</w></l>
							<l n="17" num="2.7"><w n="17.1">Aura</w> <w n="17.2">balancé</w> <w n="17.3">l</w>’<w n="17.4">encensoir</w> ;</l>
							<l n="18" num="2.8"><w n="18.1">Quand</w>, <w n="18.2">sous</w> <w n="18.3">la</w> <w n="18.4">lampe</w> <w n="18.5">vacillante</w>,</l>
							<l n="19" num="2.9"><w n="19.1">Des</w> <w n="19.2">vieilles</w> <w n="19.3">la</w> <w n="19.4">voix</w> <w n="19.5">chevrottante</w></l>
							<l n="20" num="2.10"><w n="20.1">Tremblottera</w> <w n="20.2">l</w>’<w n="20.3">hymne</w> <w n="20.4">du</w> <w n="20.5">soir</w>.</l>
						</lg>
						<lg n="3">
							<l n="21" num="3.1"><w n="21.1">Tu</w> <w n="21.2">m</w>’<w n="21.3">avais</w> <w n="21.4">dit</w> : <w n="21.5">Viens</w> <w n="21.6">à</w> <w n="21.7">cette</w> <w n="21.8">heure</w> ;</l>
							<l n="22" num="3.2"><w n="22.1">Longe</w> <w n="22.2">le</w> <w n="22.3">mur</w> <w n="22.4">des</w> <w n="22.5">templiers</w>,</l>
							<l n="23" num="3.3"><w n="23.1">Longe</w> <w n="23.2">encor</w> <w n="23.3">la</w> <w n="23.4">sombre</w> <w n="23.5">demeure</w></l>
							<l n="24" num="3.4"><w n="24.1">Assise</w> <w n="24.2">sous</w> <w n="24.3">les</w> <w n="24.4">peupliers</w> ;</l>
							<l n="25" num="3.5"><w n="25.1">Puis</w>, <w n="25.2">glisse</w>-<w n="25.3">toi</w> <w n="25.4">dans</w> <w n="25.5">la</w> <w n="25.6">presqu</w>’<w n="25.7">île</w></l>
							<l n="26" num="3.6"><w n="26.1">Qui</w> <w n="26.2">penche</w> <w n="26.3">sur</w> <w n="26.4">le</w> <w n="26.5">lac</w> <w n="26.6">mobile</w>,</l>
							<l n="27" num="3.7"><w n="27.1">Son</w> <w n="27.2">front</w> <w n="27.3">vert</w>, <w n="27.4">battu</w> <w n="27.5">des</w> <w n="27.6">autans</w>,</l>
							<l n="28" num="3.8"><w n="28.1">Vers</w> <w n="28.2">ce</w> <w n="28.3">saule</w>, <w n="28.4">pâle</w> <w n="28.5">fantôme</w>,</l>
							<l n="29" num="3.9"><w n="29.1">Sortant</w> <w n="29.2">du</w> <w n="29.3">rocher</w> <w n="29.4">comme</w> <w n="29.5">un</w> <w n="29.6">gnome</w>.</l>
							<l n="30" num="3.10"><w n="30.1">Courbé</w> <w n="30.2">sous</w> <w n="30.3">de</w> <w n="30.4">longs</w> <w n="30.5">cheveux</w> <w n="30.6">blancs</w>.</l>
						</lg>
						<lg n="4">
							<l n="31" num="4.1"><w n="31.1">Tu</w> <w n="31.2">m</w>’<w n="31.3">avais</w> <w n="31.4">dit</w>… <w n="31.5">Mais</w> <w n="31.6">qui</w> <w n="31.7">t</w>’<w n="31.8">enchaîne</w> ?…</l>
							<l n="32" num="4.2"><w n="32.1">Fatal</w> <w n="32.2">penser</w> <w n="32.3">qui</w> <w n="32.4">vient</w> <w n="32.5">s</w>’<w n="32.6">offrir</w> !…</l>
							<l n="33" num="4.3"><w n="33.1">Enfer</w> ! <w n="33.2">si</w> <w n="33.3">ta</w> <w n="33.4">peine</w> <w n="33.5">est</w> <w n="33.6">ma</w> <w n="33.7">peine</w>,</l>
							<l n="34" num="4.4"><w n="34.1">Qu</w>’<w n="34.2">en</w> <w n="34.3">ce</w> <w n="34.4">moment</w> <w n="34.5">tu</w> <w n="34.6">dois</w> <w n="34.7">souffrir</w> !</l>
							<l n="35" num="4.5"><w n="35.1">Pour</w> <w n="35.2">chasser</w> <w n="35.3">l</w>’<w n="35.4">ennui</w> <w n="35.5">de</w> <w n="35.6">l</w>’<w n="35.7">attente</w>,</l>
							<l n="36" num="4.6"><w n="36.1">Pour</w> <w n="36.2">endormir</w> <w n="36.3">mon</w> <w n="36.4">âme</w> <w n="36.5">ardente</w>.</l>
							<l n="37" num="4.7"><w n="37.1">Et</w> <w n="37.2">pour</w> <w n="37.3">recevoir</w> <w n="37.4">tes</w> <w n="37.5">attraits</w> ;</l>
							<l n="38" num="4.8"><w n="38.1">Je</w> <w n="38.2">fais</w> <w n="38.3">de</w> <w n="38.4">ces</w> <w n="38.5">fleurs</w> <w n="38.6">que</w> <w n="38.7">tu</w> <w n="38.8">cueilles</w>,</l>
							<l n="39" num="4.9"><w n="39.1">Du</w> <w n="39.2">martagon</w> <w n="39.3">aux</w> <w n="39.4">larges</w> <w n="39.5">feuilles</w>,</l>
							<l n="40" num="4.10"><w n="40.1">Un</w> <w n="40.2">lit</w> <w n="40.3">de</w> <w n="40.4">repos</w> <w n="40.5">sous</w> <w n="40.6">ce</w> <w n="40.7">dais</w>.</l>
						</lg>
						<lg n="5">
							<l n="41" num="5.1"><w n="41.1">Tu</w> <w n="41.2">m</w>’<w n="41.3">avais</w> <w n="41.4">dit</w>… <w n="41.5">le</w> <w n="41.6">temps</w> <w n="41.7">se</w> <w n="41.8">passe</w>,</l>
							<l n="42" num="5.2"><w n="42.1">En</w> <w n="42.2">vain</w> <w n="42.3">j</w>’<w n="42.4">attends</w>, <w n="42.5">tu</w> <w n="42.6">ne</w> <w n="42.7">viens</w> <w n="42.8">pas</w> ;</l>
							<l n="43" num="5.3"><w n="43.1">Et</w> <w n="43.2">la</w> <w n="43.3">lune</w> <w n="43.4">sur</w> <w n="43.5">ma</w> <w n="43.6">cuirasse</w></l>
							<l n="44" num="5.4"><w n="44.1">Brille</w> <w n="44.2">et</w> <w n="44.3">pourrait</w> <w n="44.4">guider</w> <w n="44.5">tes</w> <w n="44.6">pas</w> ;</l>
							<l n="45" num="5.5"><w n="45.1">Peut</w>-<w n="45.2">être</w> <w n="45.3">un</w> <w n="45.4">rival</w> ?… <w n="45.5">Infidelle</w> !</l>
							<l n="46" num="5.6"><w n="46.1">Il</w> <w n="46.2">dit</w> : <w n="46.3">S</w>’<w n="46.4">éloigne</w>, <w n="46.5">vient</w>, <w n="46.6">chancelle</w>,</l>
							<l n="47" num="5.7"><w n="47.1">Faisant</w> <w n="47.2">sonner</w> <w n="47.3">ses</w> <w n="47.4">éperons</w> ;</l>
							<l n="48" num="5.8"><w n="48.1">Et</w> <w n="48.2">de</w> <w n="48.3">rage</w> <w n="48.4">et</w> <w n="48.5">d</w>’<w n="48.6">impatience</w></l>
							<l n="49" num="5.9"><w n="49.1">Il</w> <w n="49.2">fouille</w> <w n="49.3">le</w> <w n="49.4">sol</w> <w n="49.5">de</w> <w n="49.6">sa</w> <w n="49.7">lance</w>,</l>
							<l n="50" num="5.10"><w n="50.1">Et</w> <w n="50.2">va</w>, <w n="50.3">poignardant</w> <w n="50.4">de</w> <w n="50.5">vieux</w> <w n="50.6">troncs</w>.</l>
						</lg>
						<lg n="6">
							<l n="51" num="6.1"><w n="51.1">Soudain</w>, <w n="51.2">il</w> <w n="51.3">voit</w> <w n="51.4">une</w> <w n="51.5">lumière</w></l>
							<l n="52" num="6.2"><w n="52.1">Qui</w> <w n="52.2">vers</w> <w n="52.3">le</w> <w n="52.4">manoir</w> <w n="52.5">passe</w> <w n="52.6">et</w> <w n="52.7">fuit</w> ;</l>
							<l n="53" num="6.3"><w n="53.1">Un</w> <w n="53.2">cercueil</w> <w n="53.3">entre</w> <w n="53.4">au</w> <w n="53.5">cimetière</w>,</l>
							<l n="54" num="6.4"><w n="54.1">Un</w> <w n="54.2">blanc</w> <w n="54.3">cercueil</w>. — <w n="54.4">Eh</w> ! <w n="54.5">qui</w> <w n="54.6">le</w> <w n="54.7">suit</w> ?</l>
							<l n="55" num="6.5"><w n="55.1">Horreur</w> ! <w n="55.2">eh</w> ! <w n="55.3">n</w>’<w n="55.4">est</w>-<w n="55.5">ce</w> <w n="55.6">pas</w> <w n="55.7">ton</w> <w n="55.8">père</w></l>
							<l n="56" num="6.6"><w n="56.1">Qui</w> <w n="56.2">hurle</w> <w n="56.3">ainsi</w>, <w n="56.4">se</w> <w n="56.5">traîne</w> <w n="56.6">à</w> <w n="56.7">terre</w> ?…</l>
							<l n="57" num="6.7"><w n="57.1">Je</w> <w n="57.2">t</w>’<w n="57.3">accusais</w> !… <w n="57.4">tiens</w>, <w n="57.5">à</w> <w n="57.6">genoux</w> :</l>
							<l n="58" num="6.8"><w n="58.1">Poignard</w> <w n="58.2">que</w> <w n="58.3">mon</w> <w n="58.4">sang</w> <w n="58.5">damasquine</w></l>
							<l n="59" num="6.9"><w n="59.1">Frappe</w>, <w n="59.2">déchire</w> <w n="59.3">ma</w> <w n="59.4">poitrine</w> !…</l>
							<l n="60" num="6.10"><w n="60.1">Je</w> <w n="60.2">te</w> <w n="60.3">rejoins</w> <w n="60.4">au</w> <w n="60.5">rendez</w>-<w n="60.6">vous</w> !</l>
						</lg>
					</div></body></text></TEI>