<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR1">
						<head type="main">Prologue</head>
						<opener>
							<salute>À Léon Clopet, architecte.</salute>
							<epigraph>
								<cit>
									<quote>
									« Voici, je m’en vais faire une chose nouvelle <lb></lb>
									qui viendra en avant ; et les bêtes des champs, <lb></lb>
									les dragons et les chats-huants me glorifieront.»
									</quote>
									<bibl>
										<name>La Bible</name>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">ton</w> <w n="1.3">Petrus</w> <w n="1.4">ou</w> <w n="1.5">ton</w> <w n="1.6">Pierre</w></l>
							<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2">avait</w> <w n="2.3">pas</w> <w n="2.4">même</w> <w n="2.5">une</w> <w n="2.6">pierre</w></l>
							<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">se</w> <w n="3.3">poser</w>, <w n="3.4">l</w>’<w n="3.5">œil</w> <w n="3.6">tari</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">clou</w> <w n="4.3">sur</w> <w n="4.4">un</w> <w n="4.5">mur</w> <w n="4.6">avare</w></l>
							<l n="5" num="1.5"><w n="5.1">Pour</w> <w n="5.2">suspendre</w> <w n="5.3">sa</w> <w n="5.4">guitare</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Tu</w> <w n="6.2">me</w> <w n="6.3">donnas</w> <w n="6.4">un</w> <w n="6.5">abri</w>.</l>
						</lg>
						<lg n="2">
							<l n="7" num="2.1"><w n="7.1">Tu</w> <w n="7.2">me</w> <w n="7.3">dis</w> : — <w n="7.4">Viens</w>, <w n="7.5">mon</w> <w n="7.6">rhapsode</w>,</l>
							<l n="8" num="2.2"><w n="8.1">Viens</w> <w n="8.2">chez</w> <w n="8.3">moi</w> <w n="8.4">finir</w> <w n="8.5">ton</w> <w n="8.6">ode</w> ;</l>
							<l n="9" num="2.3"><w n="9.1">Car</w> <w n="9.2">ton</w> <w n="9.3">ciel</w> <w n="9.4">n</w>’<w n="9.5">est</w> <w n="9.6">pas</w> <w n="9.7">d</w>’<w n="9.8">azur</w>,</l>
							<l n="10" num="2.4"><w n="10.1">Ainsi</w> <w n="10.2">que</w> <w n="10.3">le</w> <w n="10.4">ciel</w> <w n="10.5">d</w>’<w n="10.6">Homère</w>,</l>
							<l n="11" num="2.5"><w n="11.1">Ou</w> <w n="11.2">du</w> <w n="11.3">provençal</w> <w n="11.4">trouvère</w> ;</l>
							<l n="12" num="2.6"><w n="12.1">L</w>’<w n="12.2">air</w> <w n="12.3">est</w> <w n="12.4">froid</w>, <w n="12.5">le</w> <w n="12.6">sol</w> <w n="12.7">est</w> <w n="12.8">dur</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">Paris</w> <w n="13.2">n</w>’<w n="13.3">a</w> <w n="13.4">point</w> <w n="13.5">de</w> <w n="13.6">bocage</w>,</l>
							<l n="14" num="3.2"><w n="14.1">Viens</w> <w n="14.2">donc</w>, <w n="14.3">je</w> <w n="14.4">t</w>’<w n="14.5">ouvre</w> <w n="14.6">ma</w> <w n="14.7">cage</w>,</l>
							<l n="15" num="3.3"><w n="15.1">Où</w>, <w n="15.2">pauvre</w>, <w n="15.3">gaiement</w> <w n="15.4">je</w> <w n="15.5">vis</w> ;</l>
							<l n="16" num="3.4"><w n="16.1">Viens</w>, <w n="16.2">l</w>’<w n="16.3">amitié</w> <w n="16.4">nous</w> <w n="16.5">rassemble</w>,</l>
							<l n="17" num="3.5"><w n="17.1">Nous</w> <w n="17.2">partagerons</w>, <w n="17.3">ensemble</w>,</l>
							<l n="18" num="3.6"><w n="18.1">Quelques</w> <w n="18.2">grains</w> <w n="18.3">de</w> <w n="18.4">chenevis</w>.</l>
						</lg>
						<lg n="4">
							<l n="19" num="4.1"><w n="19.1">Tout</w> <w n="19.2">bas</w>, <w n="19.3">mon</w> <w n="19.4">âme</w> <w n="19.5">honteuse</w></l>
							<l n="20" num="4.2"><w n="20.1">Bénissait</w> <w n="20.2">ta</w> <w n="20.3">voix</w> <w n="20.4">flatteuse</w></l>
							<l n="21" num="4.3"><w n="21.1">Qui</w> <w n="21.2">caressait</w> <w n="21.3">son</w> <w n="21.4">malheur</w> ;</l>
							<l n="22" num="4.4"><w n="22.1">Car</w> <w n="22.2">toi</w> <w n="22.3">seul</w>, <w n="22.4">au</w> <w n="22.5">sort</w> <w n="22.6">austère</w></l>
							<l n="23" num="4.5"><w n="23.1">Qui</w> <w n="23.2">m</w>’<w n="23.3">accablait</w> <w n="23.4">solitaire</w>,</l>
							<l n="24" num="4.6"><w n="24.1">Léon</w>, <w n="24.2">tu</w> <w n="24.3">donnas</w> <w n="24.4">un</w> <w n="24.5">pleur</w>.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1"><w n="25.1">Quoi</w> ! <w n="25.2">ma</w> <w n="25.3">franchise</w> <w n="25.4">te</w> <w n="25.5">blesse</w> ?</l>
							<l n="26" num="5.2"><w n="26.1">Voudrais</w>-<w n="26.2">tu</w> <w n="26.3">que</w>, <w n="26.4">par</w> <w n="26.5">faiblesse</w>,</l>
							<l n="27" num="5.3"><w n="27.1">On</w> <w n="27.2">voilât</w> <w n="27.3">sa</w> <w n="27.4">pauvreté</w> ?</l>
							<l n="28" num="5.4"><w n="28.1">Non</w>, <w n="28.2">non</w>, <w n="28.3">nouveau</w> <w n="28.4">Malfilâtre</w>,</l>
							<l n="29" num="5.5"><w n="29.1">Je</w> <w n="29.2">veux</w>, <w n="29.3">au</w> <w n="29.4">siècle</w> <w n="29.5">parâtre</w>,</l>
							<l n="30" num="5.6"><w n="30.1">Étaler</w> <w n="30.2">ma</w> <w n="30.3">nudité</w> !</l>
						</lg>
						<lg n="6">
							<l n="31" num="6.1"><w n="31.1">Je</w> <w n="31.2">le</w> <w n="31.3">veux</w>, <w n="31.4">afin</w> <w n="31.5">qu</w>’<w n="31.6">on</w> <w n="31.7">sache</w></l>
							<l n="32" num="6.2"><w n="32.1">Que</w> <w n="32.2">je</w> <w n="32.3">ne</w> <w n="32.4">suis</w> <w n="32.5">point</w> <w n="32.6">un</w> <w n="32.7">lâche</w>,</l>
							<l n="33" num="6.3"><w n="33.1">Car</w> <w n="33.2">j</w>’<w n="33.3">ai</w> <w n="33.4">deux</w> <w n="33.5">parts</w> <w n="33.6">de</w> <w n="33.7">douleur</w></l>
							<l n="34" num="6.4"><w n="34.1">À</w> <w n="34.2">ce</w> <w n="34.3">banquet</w> <w n="34.4">de</w> <w n="34.5">la</w> <w n="34.6">terre</w> ;</l>
							<l n="35" num="6.5"><w n="35.1">Car</w>, <w n="35.2">bien</w> <w n="35.3">jeune</w>, <w n="35.4">la</w> <w n="35.5">misère</w></l>
							<l n="36" num="6.6"><w n="36.1">N</w>’<w n="36.2">a</w> <w n="36.3">pu</w> <w n="36.4">briser</w> <w n="36.5">ma</w> <w n="36.6">verdeur</w>.</l>
						</lg>
						<lg n="7">
							<l n="37" num="7.1"><w n="37.1">Je</w> <w n="37.2">le</w> <w n="37.3">veux</w>, <w n="37.4">afin</w> <w n="37.5">qu</w>’<w n="37.6">on</w> <w n="37.7">sache</w></l>
							<l n="38" num="7.2"><w n="38.1">Que</w> <w n="38.2">je</w> <w n="38.3">n</w>’<w n="38.4">ai</w> <w n="38.5">que</w> <w n="38.6">ma</w> <w n="38.7">moustache</w>,</l>
							<l n="39" num="7.3"><w n="39.1">Ma</w> <w n="39.2">chanson</w> <w n="39.3">et</w> <w n="39.4">puis</w> <w n="39.5">mon</w> <w n="39.6">cœur</w>,</l>
							<l n="40" num="7.4"><w n="40.1">Qui</w> <w n="40.2">se</w> <w n="40.3">rit</w> <w n="40.4">de</w> <w n="40.5">la</w> <w n="40.6">détresse</w> ;</l>
							<l n="41" num="7.5"><w n="41.1">Et</w> <w n="41.2">que</w> <w n="41.3">mon</w> <w n="41.4">âme</w> <w n="41.5">maîtresse</w></l>
							<l n="42" num="7.6"><w n="42.1">Contre</w> <w n="42.2">tout</w> <w n="42.3">surgit</w> <w n="42.4">vainqueur</w>.</l>
						</lg>
						<lg n="8">
							<l n="43" num="8.1"><w n="43.1">Je</w> <w n="43.2">le</w> <w n="43.3">veux</w>, <w n="43.4">afin</w> <w n="43.5">qu</w>’<w n="43.6">on</w> <w n="43.7">sache</w>,</l>
							<l n="44" num="8.2"><w n="44.1">Que</w>, <w n="44.2">sans</w> <w n="44.3">toge</w> <w n="44.4">et</w> <w n="44.5">sans</w> <w n="44.6">rondache</w>,</l>
							<l n="45" num="8.3"><w n="45.1">Ni</w> <w n="45.2">chancelier</w>, <w n="45.3">ni</w> <w n="45.4">baron</w>,</l>
							<l n="46" num="8.4"><w n="46.1">Je</w> <w n="46.2">ne</w> <w n="46.3">suis</w> <w n="46.4">point</w> <w n="46.5">gentilhomme</w>,</l>
							<l n="47" num="8.5"><w n="47.1">Ni</w> <w n="47.2">commis</w> <w n="47.3">à</w> <w n="47.4">maigre</w> <w n="47.5">somme</w></l>
							<l n="48" num="8.6"><w n="48.1">Parodiant</w> <w n="48.2">lord</w> <w n="48.3">Byron</w>.</l>
						</lg>
						<lg n="9">
							<l n="49" num="9.1"><w n="49.1">À</w> <w n="49.2">la</w> <w n="49.3">cour</w>, <w n="49.4">dans</w> <w n="49.5">ses</w> <w n="49.6">orgies</w>,</l>
							<l n="50" num="9.2"><w n="50.1">Je</w> <w n="50.2">n</w>’<w n="50.3">ai</w> <w n="50.4">point</w> <w n="50.5">fait</w> <w n="50.6">d</w>’<w n="50.7">élégies</w>,</l>
							<l n="51" num="9.3"><w n="51.1">Point</w> <w n="51.2">d</w>’<w n="51.3">hymne</w> <w n="51.4">à</w> <w n="51.5">la</w> <w n="51.6">déité</w> ;</l>
							<l n="52" num="9.4"><w n="52.1">Sur</w> <w n="52.2">le</w> <w n="52.3">flanc</w> <w n="52.4">d</w>’<w n="52.5">une</w> <w n="52.6">duchesse</w>,</l>
							<l n="53" num="9.5"><w n="53.1">Barbotant</w> <w n="53.2">dans</w> <w n="53.3">la</w> <w n="53.4">richesse</w></l>
							<l n="54" num="9.6"><w n="54.1">De</w> <w n="54.2">lai</w> <w n="54.3">sur</w> <w n="54.4">ma</w> <w n="54.5">pauvreté</w>.</l>
						</lg>
					</div></body></text></TEI>