<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR14">
						<head type="main">La Fille du Baron</head>
						<opener>
							<salute>À THÉOPHILE GAUTIER, poète.</salute>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Non</w> ! <w n="1.2">rendez</w>-<w n="1.3">moi</w> <w n="1.4">mon</w> <w n="1.5">bachelet</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">Mon</w> <w n="2.2">humble</w> <w n="2.3">cœur</w> <w n="2.4">est</w> <w n="2.5">son</w> <w n="2.6">varlet</w> !</l>
						</lg>
						<lg n="2">
							<l n="3" num="2.1"><w n="3.1">Sèche</w> <w n="3.2">tes</w> <w n="3.3">pleurs</w>, <w n="3.4">fille</w> <w n="3.5">adorée</w>.</l>
							<l n="4" num="2.2"><w n="4.1">Tu</w> <w n="4.2">peux</w> <w n="4.3">puiser</w> <w n="4.4">dans</w> <w n="4.5">mon</w> <w n="4.6">trésor</w> ;</l>
							<l n="5" num="2.3"><w n="5.1">Veux</w>-<w n="5.2">tu</w> <w n="5.3">brillera</w> <w n="5.4">la</w> <w n="5.5">vesprée</w> ?</l>
							<l n="6" num="2.4"><w n="6.1">Prends</w> <w n="6.2">tous</w> <w n="6.3">ces</w> <w n="6.4">velours</w> <w n="6.5">et</w> <w n="6.6">cet</w> <w n="6.7">or</w> !</l>
						</lg>
						<lg n="3">
							<l n="7" num="3.1"><w n="7.1">Non</w> ! <w n="7.2">rendez</w>-<w n="7.3">moi</w> <w n="7.4">mon</w> <w n="7.5">bachelet</w> ;</l>
							<l n="8" num="3.2"><w n="8.1">Mon</w> <w n="8.2">humble</w> <w n="8.3">cœur</w> <w n="8.4">est</w> <w n="8.5">son</w> <w n="8.6">varlet</w> !</l>
						</lg>
						<lg n="4">
							<l n="9" num="4.1"><w n="9.1">Peux</w>-<w n="9.2">tu</w> <w n="9.3">préférer</w>, <w n="9.4">ô</w> <w n="9.5">ma</w> <w n="9.6">fille</w> !</l>
							<l n="10" num="4.2"><w n="10.1">Ce</w> <w n="10.2">tant</w> <w n="10.3">pauvret</w> <w n="10.4">à</w> <w n="10.5">d</w>’<w n="10.6">Archambault</w>,</l>
							<l n="11" num="4.3"><w n="11.1">Dont</w> <w n="11.2">l</w>’<w n="11.3">estoc</w> <w n="11.4">près</w> <w n="11.5">du</w> <w n="11.6">trône</w> <w n="11.7">brille</w>,</l>
							<l n="12" num="4.4"><w n="12.1">Et</w> <w n="12.2">qui</w> <w n="12.3">même</w> <w n="12.4">au</w> <w n="12.5">roi</w> <w n="12.6">parle</w> <w n="12.7">haut</w>.</l>
						</lg>
						<lg n="5">
							<l n="13" num="5.1"><w n="13.1">Non</w> ! <w n="13.2">rendez</w>-<w n="13.3">moi</w> <w n="13.4">mon</w> <w n="13.5">bachelet</w> ;</l>
							<l n="14" num="5.2"><w n="14.1">Mon</w> <w n="14.2">humble</w> <w n="14.3">cœur</w> <w n="14.4">est</w> <w n="14.5">son</w> <w n="14.6">varlet</w> !</l>
						</lg>
						<lg n="6">
							<l n="15" num="6.1"><w n="15.1">Il</w> <w n="15.2">a</w> <w n="15.3">trois</w> <w n="15.4">châteaux</w> <w n="15.5">en</w> <w n="15.6">Touraine</w>,</l>
							<l n="16" num="6.2"><w n="16.1">Deux</w> <w n="16.2">dans</w> <w n="16.3">le</w> <w n="16.4">Rhône</w> <w n="16.5">se</w> <w n="16.6">mirant</w>.</l>
							<l n="17" num="6.3"><w n="17.1">Tu</w> <w n="17.2">serais</w> <w n="17.3">grande</w> <w n="17.4">suzeraine</w>,</l>
							<l n="18" num="6.4"><w n="18.1">Tu</w> <w n="18.2">brillerais</w> <w n="18.3">nu</w> <w n="18.4">premier</w> <w n="18.5">rang</w>.</l>
						</lg>
						<lg n="7">
							<l n="19" num="7.1"><w n="19.1">Non</w> ! <w n="19.2">rendez</w>-<w n="19.3">moi</w> <w n="19.4">mon</w> <w n="19.5">bachelet</w> ;</l>
							<l n="20" num="7.2"><w n="20.1">Mon</w> <w n="20.2">humble</w> <w n="20.3">cœur</w> <w n="20.4">est</w> <w n="20.5">son</w> <w n="20.6">varlet</w> !</l>
						</lg>
						<lg n="8">
							<l n="21" num="8.1"><w n="21.1">On</w> <w n="21.2">te</w> <w n="21.3">rendra</w> <w n="21.4">partout</w> <w n="21.5">hommage</w>,</l>
							<l n="22" num="8.2"><w n="22.1">Partout</w> ! <w n="22.2">comme</w> <w n="22.3">on</w> <w n="22.4">le</w> <w n="22.5">fait</w> <w n="22.6">au</w> <w n="22.7">roi</w>,</l>
							<l n="23" num="8.3"><w n="23.1">Les</w> <w n="23.2">vassaux</w> <w n="23.3">baiseront</w> <w n="23.4">la</w> <w n="23.5">plage</w></l>
							<l n="24" num="8.4"><w n="24.1">Où</w> <w n="24.2">passera</w> <w n="24.3">ton</w> <w n="24.4">palefroi</w>.</l>
						</lg>
						<lg n="9">
							<l n="25" num="9.1"><w n="25.1">Non</w> ! <w n="25.2">rendez</w>-<w n="25.3">moi</w> <w n="25.4">mon</w> <w n="25.5">bachelet</w> ;</l>
							<l n="26" num="9.2"><w n="26.1">Mou</w> <w n="26.2">humble</w> <w n="26.3">cœur</w> <w n="26.4">est</w> <w n="26.5">son</w> <w n="26.6">varlet</w> !</l>
						</lg>
						<lg n="10">
							<l n="27" num="10.1"><w n="27.1">Ainsi</w>, <w n="27.2">tu</w> <w n="27.3">brave</w> <w n="27.4">honneurs</w>, <w n="27.5">famille</w>,</l>
							<l n="28" num="10.2"><w n="28.1">D</w>’<w n="28.2">Archambault</w>, <w n="28.3">mes</w> <w n="28.4">vœux</w> !… <w n="28.5">sans</w> <w n="28.6">détour</w>,</l>
							<l n="29" num="10.3"><w n="29.1">Écuyers</w> ! <w n="29.2">qu</w>’<w n="29.3">on</w> <w n="29.4">traîne</w> <w n="29.5">ma</w> <w n="29.6">fille</w></l>
							<l n="30" num="10.4"><w n="30.1">Aux</w> <w n="30.2">oubliettes</w> <w n="30.3">de</w> <w n="30.4">la</w> <w n="30.5">tour</w> !</l>
						</lg>
						<lg n="11">
							<l n="31" num="11.1"><w n="31.1">Non</w> ! <w n="31.2">rendez</w>-<w n="31.3">moi</w> <w n="31.4">mon</w> <w n="31.5">bachelet</w> ;</l>
							<l n="32" num="11.2"><w n="32.1">Mon</w> <w n="32.2">humble</w> <w n="32.3">cœur</w> <w n="32.4">est</w> <w n="32.5">son</w> <w n="32.6">varlet</w> !</l>
						</lg>
					</div></body></text></TEI>