<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Rapsodies</title>
				<title type="medium">Une édition électronique</title>
				<author key="BOR">
					<name>
						<forename>Pétrus</forename>
						<surname>BOREL</surname>
					</name>
					<date from="1809" to="1859">1809-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>1481 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BOR_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Rapsodies</title>
						<author>Pétrus Borel</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/petruborelrhapsodies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Rapsodies</title>
						<author>Pétrus Borel</author>
						<imprint>
							<publisher>M. Levy,Paris</publisher>
							<date when="1868">1868</date>
						</imprint>
					</monogr>
				<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1832">1832</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>Les balises de pagination ont été supprimées.</p>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème.
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">RAPSODIES</head><div type="poem" key="BOR11">
						<head type="main">Doléance</head>
						<opener>
							<salute>À Francisque Borel.</salute>
							<epigraph>
								<cit>
									<quote>Moerore conficior</quote>
									<bibl>
										<hi rend="ital">Rudiment</hi>.
									</bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Son</w> <w n="1.2">joyeux</w>, <w n="1.3">importun</w>, <w n="1.4">d</w>’<w n="1.5">un</w> <w n="1.6">clavecin</w> <w n="1.7">sonore</w>,</l>
							<l n="2" num="1.2"><space unit="char" quantity="12"></space><w n="2.1">Parle</w>, <w n="2.2">que</w> <w n="2.3">me</w> <w n="2.4">veux</w>-<w n="2.5">tu</w> ?</l>
							<l n="3" num="1.3"><w n="3.1">Viens</w>-<w n="3.2">tu</w>, <w n="3.3">dans</w> <w n="3.4">mon</w> <w n="3.5">grenier</w>, <w n="3.6">pour</w> <w n="3.7">insulter</w> <w n="3.8">encore</w></l>
							<l n="4" num="1.4"><space unit="char" quantity="12"></space><w n="4.1">A</w> <w n="4.2">ce</w> <w n="4.3">cœur</w> <w n="4.4">abattu</w> ?</l>
							<l n="5" num="1.5"><w n="5.1">Son</w> <w n="5.2">joyeux</w>, <w n="5.3">ne</w> <w n="5.4">viens</w> <w n="5.5">plus</w> ; <w n="5.6">verse</w> <w n="5.7">à</w> <w n="5.8">d</w>’<w n="5.9">autres</w> <w n="5.10">l</w>’<w n="5.11">ivresse</w> ;</l>
							<l n="6" num="1.6"><space unit="char" quantity="12"></space><w n="6.1">Leur</w> <w n="6.2">vie</w> <w n="6.3">est</w> <w n="6.4">un</w> <w n="6.5">festin</w></l>
							<l n="7" num="1.7"><w n="7.1">Que</w> <w n="7.2">je</w> <w n="7.3">n</w>’ <w n="7.4">ai</w> <w n="7.5">point</w> <w n="7.6">troublé</w> ; <w n="7.7">tu</w> <w n="7.8">troubles</w> <w n="7.9">ma</w> <w n="7.10">détresse</w>,</l>
							<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1">Mon</w> <w n="8.2">râle</w> <w n="8.3">clandestin</w> !</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Indiscret</w>, <w n="9.2">d</w>’<w n="9.3">où</w> <w n="9.4">viens</w>-<w n="9.5">tu</w> ? <w n="9.6">Sans</w> <w n="9.7">doute</w> <w n="9.8">une</w> <w n="9.9">main</w> <w n="9.10">blanche</w>,</l>
							<l n="10" num="2.2"><space unit="char" quantity="12"></space><w n="10.1">Un</w> <w n="10.2">beau</w> <w n="10.3">doigt</w> <w n="10.4">prisonnier</w></l>
							<l n="11" num="2.3"><w n="11.1">Dans</w> <w n="11.2">de</w> <w n="11.3">riches</w> <w n="11.4">joyaux</w> <w n="11.5">a</w> <w n="11.6">frappé</w> <w n="11.7">sur</w> <w n="11.8">ton</w> <w n="11.9">anche</w></l>
							<l n="12" num="2.4"><space unit="char" quantity="12"></space><w n="12.1">D</w>’<w n="12.2">ivoire</w> <w n="12.3">et</w> <w n="12.4">d</w>’<w n="12.5">ébénier</w>.</l>
							<l n="13" num="2.5"><w n="13.1">Accompagnerais</w>-<w n="13.2">tu</w> <w n="13.3">d</w>’<w n="13.4">une</w> <w n="13.5">enfant</w> <w n="13.6">angélique</w></l>
							<l n="14" num="2.6"><space unit="char" quantity="12"></space><w n="14.1">La</w> <w n="14.2">timide</w> <w n="14.3">leçon</w> ?</l>
							<l n="15" num="2.7"><w n="15.1">Si</w> <w n="15.2">le</w> <w n="15.3">rhythme</w> <w n="15.4">est</w> <w n="15.5">bien</w> <w n="15.6">sombre</w> <w n="15.7">et</w> <w n="15.8">l</w>’<w n="15.9">air</w> <w n="15.10">mélancolique</w>,</l>
							<l n="16" num="2.8"><space unit="char" quantity="12"></space><w n="16.1">Trahis</w>-<w n="16.2">moi</w> <w n="16.3">sa</w> <w n="16.4">chanson</w>.</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">Non</w> : <w n="17.2">j</w>’<w n="17.3">entends</w> <w n="17.4">les</w> <w n="17.5">pas</w> <w n="17.6">sourds</w> <w n="17.7">d</w>’<w n="17.8">une</w> <w n="17.9">foule</w> <w n="17.10">ameutée</w>,</l>
							<l n="18" num="3.2"><space unit="char" quantity="12"></space><w n="18.1">Dans</w> <w n="18.2">un</w> <w n="18.3">salon</w> <w n="18.4">étroit</w></l>
							<l n="19" num="3.3"><w n="19.1">Elle</w> <w n="19.2">vogue</w> <w n="19.3">en</w> <w n="19.4">tournant</w> <w n="19.5">par</w> <w n="19.6">la</w> <w n="19.7">valse</w> <w n="19.8">exaltée</w></l>
							<l n="20" num="3.4"><space unit="char" quantity="12"></space><w n="20.1">Ébranlant</w> <w n="20.2">mur</w> <w n="20.3">et</w> <w n="20.4">toit</w>.</l>
							<l n="21" num="3.5"><w n="21.1">Au</w> <w n="21.2">dehors</w> <w n="21.3">bruits</w> <w n="21.4">confus</w>, <w n="21.5">cris</w>, <w n="21.6">chevaux</w> <w n="21.7">qui</w> <w n="21.8">hennissent</w>,</l>
							<l n="22" num="3.6"><space unit="char" quantity="12"></space><w n="22.1">Fleurs</w>, <w n="22.2">esclaves</w>, <w n="22.3">flambeaux</w>.</l>
							<l n="23" num="3.7"><w n="23.1">Le</w> <w n="23.2">riche</w> <w n="23.3">épand</w> <w n="23.4">sa</w> <w n="23.5">joie</w>, <w n="23.6">et</w> <w n="23.7">les</w> <w n="23.8">pauvres</w> <w n="23.9">gémissent</w>,</l>
							<l n="24" num="3.8"><space unit="char" quantity="12"></space><w n="24.1">Honteux</w> <w n="24.2">sous</w> <w n="24.3">leurs</w> <w n="24.4">lambeaux</w> !</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1"><w n="25.1">Autour</w> <w n="25.2">de</w> <w n="25.3">moi</w> <w n="25.4">ce</w> <w n="25.5">n</w>’<w n="25.6">est</w> <w n="25.7">que</w> <w n="25.8">palais</w>, <w n="25.9">joie</w> <w n="25.10">immonde</w>,</l>
							<l n="26" num="4.2"><space unit="char" quantity="12"></space><w n="26.1">Biens</w>, <w n="26.2">somptueuses</w> <w n="26.3">nuits</w>.</l>
							<l n="27" num="4.3"><w n="27.1">Avenir</w>, <w n="27.2">gloire</w>, <w n="27.3">honneurs</w> : <w n="27.4">au</w> <w n="27.5">milieu</w> <w n="27.6">de</w> <w n="27.7">ce</w> <w n="27.8">monde</w></l>
							<l n="28" num="4.4"><space unit="char" quantity="12"></space><w n="28.1">Pauvre</w> <w n="28.2">et</w> <w n="28.3">souffrant</w> <w n="28.4">je</w> <w n="28.5">suis</w>,</l>
							<l n="29" num="4.5"><w n="29.1">Comme</w> <w n="29.2">entouré</w> <w n="29.3">des</w> <w n="29.4">grands</w>, <w n="29.5">du</w> <w n="29.6">roi</w>, <w n="29.7">du</w> <w n="29.8">saint</w> <w n="29.9">office</w>,</l>
							<l n="30" num="4.6"><space unit="char" quantity="12"></space><w n="30.1">Sur</w> <w n="30.2">le</w> <w n="30.3">quémadero</w>,</l>
							<l n="31" num="4.7"><w n="31.1">Tous</w> <w n="31.2">en</w> <w n="31.3">pompe</w> <w n="31.4">assemblés</w> <w n="31.5">pour</w> <w n="31.6">humer</w> <w n="31.7">un</w> <w n="31.8">supplice</w>,</l>
							<l n="32" num="4.8"><space unit="char" quantity="12"></space><w n="32.1">Un</w> <w n="32.2">juif</w> <w n="32.3">au</w> <w n="32.4">brazero</w> !</l>
						</lg>
						<lg n="5">
							<l n="33" num="5.1"><w n="33.1">Car</w> <w n="33.2">tout</w> <w n="33.3">m</w>’<w n="33.4">accable</w> <w n="33.5">enfin</w> ; <w n="33.6">néant</w>, <w n="33.7">misère</w>, <w n="33.8">envie</w></l>
							<l n="34" num="5.2"><space unit="char" quantity="12"></space><w n="34.1">Vont</w> <w n="34.2">morcelant</w> <w n="34.3">mes</w> <w n="34.4">jours</w> !</l>
							<l n="35" num="5.3"><w n="35.1">Mes</w> <w n="35.2">amours</w> <w n="35.3">brochaient</w> <w n="35.4">d</w>’<w n="35.5">or</w> <w n="35.6">le</w> <w n="35.7">crêpe</w> <w n="35.8">de</w> <w n="35.9">ma</w> <w n="35.10">vie</w> ;</l>
							<l n="36" num="5.4"><space unit="char" quantity="12"></space><w n="36.1">Désormais</w> <w n="36.2">plus</w> <w n="36.3">d</w>’<w n="36.4">amours</w>.</l>
							<l n="37" num="5.5"><w n="37.1">Pauvre</w> <w n="37.2">fille</w> ! <w n="37.3">C</w>’<w n="37.4">est</w> <w n="37.5">moi</w> <w n="37.6">qui</w> <w n="37.7">t</w>’<w n="37.8">avais</w> <w n="37.9">entraînée</w></l>
							<l n="38" num="5.6"><space unit="char" quantity="12"></space><w n="38.1">Au</w> <w n="38.2">sentier</w> <w n="38.3">de</w> <w n="38.4">douleur</w> ;</l>
							<l n="39" num="5.7"><w n="39.1">Mais</w> <w n="39.2">d</w>’<w n="39.3">un</w> <w n="39.4">poison</w> <w n="39.5">plus</w> <w n="39.6">fort</w> <w n="39.7">avant</w> <w n="39.8">qu</w>’<w n="39.9">il</w> <w n="39.10">t</w>’<w n="39.11">ait</w> <w n="39.12">fanée</w></l>
							<l n="40" num="5.8"><space unit="char" quantity="12"></space><w n="40.1">Tu</w> <w n="40.2">tuas</w> <w n="40.3">le</w> <w n="40.4">malheur</w> !</l>
						</lg>
						<lg n="6">
							<l n="41" num="6.1"><w n="41.1">Eh</w> ! <w n="41.2">Moi</w>, <w n="41.3">plus</w> <w n="41.4">qu</w>’<w n="41.5">un</w> <w n="41.6">enfant</w>, <w n="41.7">capon</w>, <w n="41.8">flasque</w>, <w n="41.9">gavache</w>,</l>
							<l n="42" num="6.2"><space unit="char" quantity="12"></space><w n="42.1">De</w> <w n="42.2">ce</w> <w n="42.3">fer</w> <w n="42.4">acéré</w></l>
							<l n="43" num="6.3"><w n="43.1">Je</w> <w n="43.2">ne</w> <w n="43.3">déchire</w> <w n="43.4">pas</w> <w n="43.5">avec</w> <w n="43.6">ce</w> <w n="43.7">bras</w> <w n="43.8">trop</w> <w n="43.9">lâche</w></l>
							<l n="44" num="6.4"><space unit="char" quantity="12"></space><w n="44.1">Mon</w> <w n="44.2">poitrail</w> <w n="44.3">ulcéré</w> !</l>
							<l n="45" num="6.5"><w n="45.1">Je</w> <w n="45.2">rumine</w> <w n="45.3">mes</w> <w n="45.4">maux</w> : <w n="45.5">son</w> <w n="45.6">ombre</w> <w n="45.7">est</w> <w n="45.8">poursuivie</w></l>
							<l n="46" num="6.6"><space unit="char" quantity="12"></space><w n="46.1">D</w>’<w n="46.2">un</w> <w n="46.3">geindre</w> <w n="46.4">coutumier</w>.</l>
							<l n="47" num="6.7"><w n="47.1">Qui</w> <w n="47.2">donc</w> <w n="47.3">me</w> <w n="47.4">rend</w> <w n="47.5">si</w> <w n="47.6">veule</w> <w n="47.7">et</w> <w n="47.8">m</w>’<w n="47.9">enchaîne</w> <w n="47.10">à</w> <w n="47.11">la</w> <w n="47.12">vie</w> ?…</l>
							<l n="48" num="6.8"><space unit="char" quantity="12"></space><w n="48.1">Pauvre</w> <w n="48.2">Job</w> <w n="48.3">au</w> <w n="48.4">fumier</w> !</l>
						</lg>
					</div></body></text></TEI>