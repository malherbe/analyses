<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA COMÉDIE DU MONDE</title>
				<title type="medium">Édition électronique</title>
				<author key="DSA">
					<name>
						<forename>Alfred</forename>
						<nameLink>de</nameLink>
						<surname>ESSARTS</surname>
					</name>
					<date from="1811" to="1893">1811-1893</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>5551 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">DSA_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA COMÉDIE DU MONDE</title>
						<author>Alfred des Essarts</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URI">https://gallica.bnf.fr/ark:/12148/bpt6k5446283w?rk=429186</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title type="main">LA COMÉDIE DU MONDE</title>
								<author>Alfred des Essarts</author>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher> Comptoir des imprimeurs-unis</publisher>
									<date when="1851">1851</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1851">1851</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension(…).</p>
					<p>Les tirets simples précédés et/ou suivis d’une espace ont été remplacés par des tirets demi-cadratin</p>
					<p>
						Le signe "_" (_) est utilisé à la place du trait d’union pour séparer les syllabes
						du mot "so-ci-a-lis-tes" afin de préserver l’unité du mot dans le traitement des rimes.
					</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-04-25" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2021-04-25" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DSA21">
				<head type="main">CONCLUSION</head>
				<head type="sub">HUIT ANS APRÈS</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">A</w> <w n="1.2">la</w> <w n="1.3">fin</w> <w n="1.4">du</w> <w n="1.5">récit</w> — <w n="1.6">ou</w> <w n="1.7">bien</w> <w n="1.8">de</w> <w n="1.9">mon</w> <w n="1.10">histoire</w>, —</l>
					<l n="2" num="1.2"><w n="2.1">Car</w> <w n="2.2">elle</w> <w n="2.3">est</w> <w n="2.4">vraisemblable</w> <w n="2.5">et</w> <w n="2.6">vous</w> <w n="2.7">pouvez</w> <w n="2.8">y</w> <w n="2.9">croire</w>, —</l>
					<l n="3" num="1.3"><w n="3.1">Me</w> <w n="3.2">voici</w> <w n="3.3">parvenu</w>. <w n="3.4">Reposons</w>-<w n="3.5">nous</w>, <w n="3.6">lecteur</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">daignez</w> <w n="4.3">excuser</w> <w n="4.4">les</w> <w n="4.5">fautes</w> <w n="4.6">de</w> <w n="4.7">l</w>’<w n="4.8">auteur</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Rimant</w> <w n="5.2">tant</w> <w n="5.3">bien</w> <w n="5.4">que</w> <w n="5.5">mal</w>, <w n="5.6">j</w>’<w n="5.7">ai</w> <w n="5.8">traversé</w> <w n="5.9">l</w>’<w n="5.10">épreuve</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">D</w>’<w n="6.2">autres</w> <w n="6.3">feront</w> <w n="6.4">ainsi</w> : <w n="6.5">la</w> <w n="6.6">route</w> <w n="6.7">n</w>’<w n="6.8">est</w> <w n="6.9">pas</w> <w n="6.10">neuve</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">J</w>’<w n="7.2">y</w> <w n="7.3">vois</w> <w n="7.4">briller</w> <w n="7.5">Boileau</w>, <w n="7.6">La</w> <w n="7.7">Fontaine</w>, <w n="7.8">Gresset</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Du</w> <w n="8.2">roman</w> <w n="8.3">de</w> <w n="8.4">la</w> <hi rend="ital"><w n="8.5">Rose</w></hi> <w n="8.6">aux</w> <w n="8.7">Contes</w> <w n="8.8">de</w> <w n="8.9">Musset</w>,</l>
					<l n="9" num="1.9"><w n="9.1">Le</w> <w n="9.2">récit</w> <w n="9.3">familier</w> <w n="9.4">eut</w> <w n="9.5">plus</w> <w n="9.6">d</w>’<w n="9.7">une</w> <w n="9.8">épopée</w>.</l>
					<l n="10" num="1.10"><w n="10.1">La</w> <w n="10.2">Muse</w> <w n="10.3">ne</w> <w n="10.4">vit</w> <w n="10.5">pas</w> <w n="10.6">que</w> <w n="10.7">de</w> <w n="10.8">grands</w> <w n="10.9">coups</w> <w n="10.10">d</w>’<w n="10.11">épée</w>.</l>
					<l n="11" num="1.11"><w n="11.1">Si</w> <w n="11.2">je</w> <w n="11.3">vous</w> <w n="11.4">ai</w> <w n="11.5">distrait</w>, <w n="11.6">j</w>’<w n="11.7">ai</w> <w n="11.8">ma</w> <w n="11.9">grâce</w> <w n="11.10">à</w> <w n="11.11">vos</w> <w n="11.12">yeux</w> :</l>
					<l n="12" num="1.12"><hi rend="ital"><w n="12.1">Tous</w> <w n="12.2">les</w> <w n="12.3">genres</w> <w n="12.4">sont</w> <w n="12.5">bons</w>, <w n="12.6">hors</w> <w n="12.7">le</w> <w n="12.8">genre</w> <w n="12.9">ennuyeux</w>.</hi></l>
				</lg>
				<lg n="2">
					<l n="13" num="2.1"><w n="13.1">Quelques</w> <w n="13.2">détails</w> <w n="13.3">encore</w>, <w n="13.4">avant</w> <w n="13.5">que</w> <w n="13.6">je</w> <w n="13.7">termine</w>.</l>
				</lg>
				<lg n="3">
					<l n="14" num="3.1"><space unit="char" quantity="2"></space><w n="14.1">La</w> <w n="14.2">baronne</w> <w n="14.3">Wolgande</w>-<w n="14.4">Ulrique</w>-<w n="14.5">Wilhelmine</w>,</l>
					<l n="15" num="3.2"><w n="15.1">Très</w>-<w n="15.2">riche</w> <w n="15.3">Hollandaise</w>, <w n="15.4">a</w> <w n="15.5">pris</w> <w n="15.6">pour</w> <w n="15.7">son</w> <w n="15.8">époux</w></l>
					<l n="16" num="3.3"><w n="16.1">Un</w> <w n="16.2">monsieur</w> <hi rend="ital"><w n="16.3">de</w></hi> <w n="16.4">Caron</w>, <w n="16.5">assez</w> <w n="16.6">connu</w> <w n="16.7">de</w> <w n="16.8">vous</w>.</l>
					<l n="17" num="3.4"><w n="17.1">De</w> <w n="17.2">son</w> <w n="17.3">charmant</w> <w n="17.4">Français</w> <w n="17.5">cette</w> <w n="17.6">veuve</w> <w n="17.7">raffole</w> ;</l>
					<l n="18" num="3.5"><w n="18.1">Amour</w> <w n="18.2">dont</w> <w n="18.3">quatre</w> <w n="18.4">enfants</w> <w n="18.5">sont</w> <w n="18.6">le</w> <w n="18.7">vivant</w> <w n="18.8">symbole</w>.</l>
					<l n="19" num="3.6"><w n="19.1">Caron</w>, <w n="19.2">content</w> <w n="19.3">du</w> <w n="19.4">sort</w>, <w n="19.5">ne</w> <w n="19.6">demande</w> <w n="19.7">plus</w> <w n="19.8">rien</w> ;</l>
					<l n="20" num="3.7"><w n="20.1">Il</w> <w n="20.2">trouve</w> <w n="20.3">qu</w>’<w n="20.4">ici</w>-<w n="20.5">bas</w> <w n="20.6">tout</w> <w n="20.7">est</w> <w n="20.8">réglé</w> <w n="20.9">très</w>-<w n="20.10">bien</w>,</l>
					<l n="21" num="3.8"><w n="21.1">Et</w> <w n="21.2">que</w> <w n="21.3">les</w> <w n="21.4">mécontents</w>, <w n="21.5">qui</w> <w n="21.6">n</w>’<w n="21.7">ont</w> <w n="21.8">pas</w> <w n="21.9">de</w> <w n="21.10">quoi</w> <w n="21.11">vivre</w>,</l>
					<l n="22" num="3.9"><w n="22.1">Sont</w> <w n="22.2">des</w> <w n="22.3">gueux</w> <w n="22.4">dont</w> <w n="22.5">il</w> <w n="22.6">faut</w> <w n="22.7">que</w> <w n="22.8">l</w>’<w n="22.9">État</w> <w n="22.10">se</w> <w n="22.11">délivre</w>.</l>
					<l n="23" num="3.10"><w n="23.1">Fort</w> <w n="23.2">tranquille</w> <w n="23.3">à</w> <w n="23.4">La</w> <w n="23.5">Haye</w>, <w n="23.6">il</w> <w n="23.7">est</w> <w n="23.8">très</w>-<w n="23.9">gros</w>, <w n="23.10">très</w>-<w n="23.11">gras</w>.</l>
					<l n="24" num="3.11"><space unit="char" quantity="2"></space><w n="24.1">Le</w> <w n="24.2">duc</w> <w n="24.3">s</w>’<w n="24.4">est</w> <w n="24.5">retiré</w> <w n="24.6">dans</w> <w n="24.7">son</w> <w n="24.8">château</w>… <w n="24.9">là</w>-<w n="24.10">bas</w>…</l>
					<l n="25" num="3.12"><w n="25.1">Vous</w> <w n="25.2">savez</w> ? — <w n="25.3">Non</w>. — <w n="25.4">La</w> <w n="25.5">chose</w> <w n="25.6">est</w> <w n="25.7">fort</w> <w n="25.8">indifférente</w> :</l>
					<l n="26" num="3.13"><w n="26.1">Le</w> <w n="26.2">bonheur</w> <w n="26.3">suit</w> <w n="26.4">partout</w> <w n="26.5">cent</w> <w n="26.6">mille</w> <w n="26.7">écus</w> <w n="26.8">de</w> <w n="26.9">rente</w>,</l>
					<l n="27" num="3.14"><space unit="char" quantity="2"></space><w n="27.1">Enguerrand</w> <w n="27.2">joue</w> <w n="27.3">au</w> <w n="27.4">whist</w> ; <w n="27.5">on</w> <w n="27.6">peut</w> <w n="27.7">vivre</w> <w n="27.8">du</w> <w n="27.9">jeu</w>.</l>
					<l n="28" num="3.15"><space unit="char" quantity="2"></space><w n="28.1">Fortunat</w>, <w n="28.2">journaliste</w>, <w n="28.3">est</w> <w n="28.4">rouge</w> — <w n="28.5">blanc</w> — <w n="28.6">ou</w> <w n="28.7">bleu</w>.</l>
					<l n="29" num="3.16"><space unit="char" quantity="2"></space><w n="29.1">Florida</w> ? — <w n="29.2">Tous</w> <w n="29.3">les</w> <w n="29.4">soirs</w> <w n="29.5">la</w> <w n="29.6">belle</w> <w n="29.7">se</w> <w n="29.8">promène</w></l>
					<l n="30" num="3.17"><w n="30.1">Du</w> <w n="30.2">passage</w> <w n="30.3">Jouffroy</w> <w n="30.4">jusqu</w>’<w n="30.5">à</w> <w n="30.6">la</w> <w n="30.7">Madeleine</w>.</l>
					<l n="31" num="3.18"><space unit="char" quantity="2"></space><w n="31.1">Forbain</w> ? — <w n="31.2">Il</w> <w n="31.3">est</w> <w n="31.4">préfet</w>. <w n="31.5">Mais</w> <w n="31.6">jetant</w> <w n="31.7">le</w> <w n="31.8">poignard</w>,</l>
					<l n="32" num="3.19"><w n="32.1">Il</w> <w n="32.2">a</w> <w n="32.3">voué</w> <w n="32.4">sa</w> <w n="32.5">haine</w> <w n="32.6">au</w> <w n="32.7">parti</w> <w n="32.8">montagnard</w>.</l>
					<l n="33" num="3.20"><space unit="char" quantity="2"></space><w n="33.1">Et</w> <w n="33.2">Gospur</w> ? — <w n="33.3">Le</w> <w n="33.4">profond</w> <w n="33.5">Gospur</w> <w n="33.6">fut</w> <w n="33.7">aux</w> <w n="33.8">Finances</w>,</l>
					<l n="34" num="3.21"><w n="34.1">Et</w> <w n="34.2">nous</w> <w n="34.3">reconnaissions</w> <w n="34.4">ses</w> <w n="34.5">belles</w> <w n="34.6">ordonnances</w>.</l>
					<l n="35" num="3.22"><w n="35.1">Pour</w> <w n="35.2">mieux</w> <w n="35.3">nous</w> <w n="35.4">dégrever</w> <w n="35.5">il</w> <w n="35.6">quadruplait</w> <w n="35.7">l</w>’<w n="35.8">impôt</w>…</l>
					<l n="36" num="3.23"><w n="36.1">Nous</w> <w n="36.2">n</w>’<w n="36.3">eûmes</w> <w n="36.4">pas</w> <w n="36.5">encor</w> <w n="36.6">par</w> <w n="36.7">lui</w> <w n="36.8">la</w> <hi rend="ital"><w n="36.9">poule</w> <w n="36.10">au</w> <w n="36.11">pot</w></hi>.</l>
					<l n="37" num="3.24"><space unit="char" quantity="2"></space><w n="37.1">Rétho</w> ? — <w n="37.2">De</w> <w n="37.3">ses</w> <hi rend="ital"><w n="37.4">canards</w></hi> <w n="37.5">nous</w> <w n="37.6">sommes</w> <w n="37.7">tous</w> <w n="37.8">victimes</w> ;</l>
					<l n="38" num="3.25"><w n="38.1">Il</w> <w n="38.2">les</w> <w n="38.3">sème</w> <w n="38.4">partout</w>, <w n="38.5">au</w> <w n="38.6">prix</w> <w n="38.7">de</w> <w n="38.8">cinq</w> <w n="38.9">centimes</w>.</l>
					<l n="39" num="3.26"><w n="39.1">Pour</w> <w n="39.2">toi</w>, <w n="39.3">Peuple</w>, <w n="39.4">il</w> <w n="39.5">se</w> <w n="39.6">fait</w> <w n="39.7">un</w> <w n="39.8">Brutus</w>, <w n="39.9">un</w> <w n="39.10">Caton</w> :</l>
					<l n="40" num="3.27"><w n="40.1">S</w>’<w n="40.2">il</w> <w n="40.3">était</w> <w n="40.4">au</w> <w n="40.5">pouvoir</w>, <w n="40.6">il</w> <w n="40.7">changerait</w> <w n="40.8">dé</w> <w n="40.9">ton</w>.</l>
					<l n="41" num="3.28"><space unit="char" quantity="2"></space><w n="41.1">Et</w> <w n="41.2">Bennotti</w> ? — <w n="41.3">Dans</w> <w n="41.4">Rome</w> <w n="41.5">à</w> <w n="41.6">son</w> <w n="41.7">aise</w> <w n="41.8">il</w> <w n="41.9">pérore</w> ;</l>
					<l n="42" num="3.29"><w n="42.1">Il</w> <w n="42.2">cite</w> <w n="42.3">Régulus</w> <w n="42.4">et</w> <w n="42.5">vingt</w> <w n="42.6">héros</w> <w n="42.7">encore</w> ;</l>
					<l n="43" num="3.30"><w n="43.1">A</w> <w n="43.2">propos</w> <w n="43.3">d</w>’<w n="43.4">un</w> <w n="43.5">bon</w> <w n="43.6">Pape</w>, <w n="43.7">il</w> <w n="43.8">crie</w> <w n="43.9">aux</w> <w n="43.10">Borgias</w>,</l>
					<l n="44" num="3.31"><w n="44.1">Et</w> <w n="44.2">sur</w> <w n="44.3">les</w> <hi rend="ital"><w n="44.4">Raphaël</w></hi> <w n="44.5">il</w> <w n="44.6">fait</w> <w n="44.7">des</w> <hi rend="ital"><w n="44.8">razzias</w></hi> <ref target="1" type="noteAnchor">(1)</ref>.</l>
					<l n="45" num="3.32"><space unit="char" quantity="2"></space><w n="45.1">Crampon</w> ? — <w n="45.2">Il</w> <w n="45.3">combattit</w> <w n="45.4">pour</w> <w n="45.5">une</w> <w n="45.6">triste</w> <w n="45.7">cause</w>.</l>
					<l n="46" num="3.33"><w n="46.1">Du</w> <w n="46.2">moins</w> <w n="46.3">il</w> <w n="46.4">était</w> <w n="46.5">brave</w>… <w n="46.6">A</w> <w n="46.7">présent</w>, <w n="46.8">il</w> <w n="46.9">repose</w>…</l>
					<l n="47" num="3.34"><w n="47.1">Sur</w> <w n="47.2">les</w> <w n="47.3">pavés</w> <w n="47.4">de</w> <w n="47.5">Juin</w> <w n="47.6">il</w> <w n="47.7">reçut</w> <w n="47.8">le</w> <w n="47.9">trépas</w></l>
					<l n="48" num="3.35"><w n="48.1">Pour</w> <w n="48.2">ceux</w> <w n="48.3">qui</w> <w n="48.4">le</w> <w n="48.5">poussaient</w> <w n="48.6">et</w> <w n="48.7">ne</w> <w n="48.8">se</w> <w n="48.9">battaient</w> <w n="48.10">pas</w>.</l>
					<l n="49" num="3.36"><space unit="char" quantity="2"></space><w n="49.1">Rosencrantz</w> <w n="49.2">est</w> <w n="49.3">en</w> <w n="49.4">Suisse</w> <w n="49.5">avec</w> <w n="49.6">notre</w> <w n="49.7">Montagne</w>.</l>
					<l n="50" num="3.37"><w n="50.1">Contre</w> <w n="50.2">le</w> <w n="50.3">Sonderbund</w> <w n="50.4">il</w> <w n="50.5">s</w>’<w n="50.6">est</w> <w n="50.7">mis</w> <w n="50.8">en</w> <w n="50.9">campagne</w>.</l>
					<l n="51" num="3.38"><w n="51.1">Monsieur</w> <w n="51.2">Druey</w> <w n="51.3">l</w>’<w n="51.4">estime</w>… <w n="51.5">Ils</w> <w n="51.6">s</w>’<w n="51.7">honorent</w> <w n="51.8">tous</w> <w n="51.9">deux</w>…</l>
					<l n="52" num="3.39"><w n="52.1">L</w>’<w n="52.2">amitié</w> <w n="52.3">d</w>’<w n="52.4">un</w> <w n="52.5">grand</w> <w n="52.6">homme</w> <w n="52.7">est</w> <w n="52.8">un</w> <w n="52.9">bienfait</w> <w n="52.10">des</w> <w n="52.11">dieux</w>.</l>
					<l n="53" num="3.40"><space unit="char" quantity="2"></space><w n="53.1">Revenu</w> <w n="53.2">des</w> <w n="53.3">honneurs</w>, <w n="53.4">Jacob</w>, <w n="53.5">dans</w> <w n="53.6">sa</w> <w n="53.7">boutique</w>,</l>
					<l n="54" num="3.41"><w n="54.1">Songe</w> <w n="54.2">au</w> <w n="54.3">temps</w> <w n="54.4">où</w> <w n="54.5">sa</w> <w n="54.6">main</w> <w n="54.7">tenait</w> <w n="54.8">la</w> <w n="54.9">République</w>.</l>
					<l n="55" num="3.42"><w n="55.1">A</w> <w n="55.2">Denys</w> <w n="55.3">le</w> <w n="55.4">tyran</w> <w n="55.5">s</w>’<w n="55.6">assimilant</w> <w n="55.7">parfois</w>,</l>
					<l n="56" num="3.43"><w n="56.1">Jacob</w> <w n="56.2">s</w>’<w n="56.3">est</w> <w n="56.4">consolé</w> <w n="56.5">par</w> <w n="56.6">l</w>’<w n="56.7">exemple</w> <w n="56.8">des</w> <w n="56.9">rois</w>.</l>
					<l n="57" num="3.44"><space unit="char" quantity="2"></space><w n="57.1">Vendant</w> <w n="57.2">la</w> <hi rend="ital"><w n="57.3">Voix</w> <w n="57.4">du</w> <w n="57.5">Peuple</w></hi>, <w n="57.6">Aristide</w> <w n="57.7">s</w>’<w n="57.8">exerce</w></l>
					<l n="58" num="3.45"><w n="58.1">A</w> <w n="58.2">hurler</w> <w n="58.3">dans</w> <w n="58.4">la</w> <w n="58.5">rue</w>… — <w n="58.6">Un</w> <w n="58.7">utile</w> <w n="58.8">commerce</w> !</l>
					<l n="59" num="3.46"><space unit="char" quantity="2"></space><w n="59.1">Rasant</w> <w n="59.2">civiquement</w> <w n="59.3">les</w> <w n="59.4">mentons</w> <w n="59.5">trop</w> <w n="59.6">garnis</w>,</l>
					<l n="60" num="3.47"><w n="60.1">Boudet</w> <w n="60.2">tient</w> <w n="60.3">magasin</w> <hi rend="ital"><w n="60.4">aux</w> <w n="60.5">Coiffeurs</w> <w n="60.6">réunis</w></hi>.</l>
					<l n="61" num="3.48"><space unit="char" quantity="2"></space><w n="61.1">Attendant</w> <w n="61.2">que</w> <w n="61.3">la</w> <w n="61.4">France</w> <w n="61.5">encore</w> <w n="61.6">s</w>’<w n="61.7">émancipe</w>,</l>
					<l n="62" num="3.49"><w n="62.1">Flacon</w> <w n="62.2">boit</w> <w n="62.3">de</w> <w n="62.4">la</w> <w n="62.5">bière</w> <w n="62.6">et</w> <w n="62.7">culotte</w> <w n="62.8">sa</w> <w n="62.9">pipe</w>.</l>
					<l n="63" num="3.50"><space unit="char" quantity="2"></space><w n="63.1">Pierrefitte</w> <w n="63.2">s</w>’<w n="63.3">est</w> <w n="63.4">fait</w> <w n="63.5">courtier</w> <w n="63.6">d</w>’<w n="63.7">élections</w>,</l>
					<l n="64" num="3.51"><w n="64.1">Et</w> <w n="64.2">de</w> <w n="64.3">tous</w> <w n="64.4">les</w> <w n="64.5">drapeaux</w> <w n="64.6">a</w> <w n="64.7">des</w> <w n="64.8">échantillons</w>.</l>
					<l n="65" num="3.52"><space unit="char" quantity="2"></space><w n="65.1">Et</w> <w n="65.2">le</w> <w n="65.3">reste</w> <w n="65.4">du</w> <w n="65.5">club</w> ? — <w n="65.6">Hélas</w> ! <w n="65.7">les</w> <w n="65.8">pauvres</w> <w n="65.9">diables</w></l>
					<l n="66" num="3.53"><w n="66.1">Ont</w> <w n="66.2">eu</w>, <w n="66.3">pour</w> <w n="66.4">la</w> <w n="66.5">plupart</w>, <w n="66.6">des</w> <w n="66.7">destins</w> <w n="66.8">misérables</w>.</l>
					<l n="67" num="3.54"><w n="67.1">C</w>’<w n="67.2">est</w> <w n="67.3">un</w> <w n="67.4">triste</w> <w n="67.5">métier</w> <w n="67.6">que</w> <w n="67.7">de</w> <w n="67.8">passer</w> <w n="67.9">le</w> <w n="67.10">temps</w>.</l>
					<l n="68" num="3.55"><w n="68.1">A</w> <w n="68.2">nourrir</w> <w n="68.3">des</w> <w n="68.4">complots</w> ; <w n="68.5">clubistes</w> <w n="68.6">mécontents</w>,</l>
					<l n="69" num="3.56"><w n="69.1">Comprenez</w> <w n="69.2">donc</w> <w n="69.3">enfin</w> <w n="69.4">que</w> <w n="69.5">de</w> <w n="69.6">la</w> <w n="69.7">barricade</w></l>
					<l n="70" num="3.57"><w n="70.1">Il</w> <w n="70.2">ne</w> <w n="70.3">peut</w> <w n="70.4">s</w>’<w n="70.5">élever</w> <w n="70.6">qu</w>’<w n="70.7">un</w> <w n="70.8">pouvoir</w> <w n="70.9">bien</w> <w n="70.10">malade</w>.</l>
					<l n="71" num="3.58"><space unit="char" quantity="2"></space><w n="71.1">Et</w> <w n="71.2">que</w> <w n="71.3">fait</w> <w n="71.4">à</w> <w n="71.5">présent</w> <w n="71.6">notre</w> <w n="71.7">ami</w> <w n="71.8">Duriveau</w></l>
					<l n="72" num="3.59"><w n="72.1">A</w> <w n="72.2">qui</w>, <w n="72.3">n</w>’<w n="72.4">en</w> <w n="72.5">fût</w>-<w n="72.6">il</w> <w n="72.7">plus</w>, <w n="72.8">il</w> <w n="72.9">fallait</w> <w n="72.10">du</w> <w n="72.11">nouveau</w></l>
					<l n="73" num="3.60"><w n="73.1">Ce</w> <w n="73.2">statuaire</w> <hi rend="ital"><w n="73.3">oscur</w></hi>, <w n="73.4">qui</w> <w n="73.5">taillait</w> <w n="73.6">des</w> <w n="73.7">colosses</w>,</l>
					<l n="74" num="3.61"><w n="74.1">A</w> <w n="74.2">quelques</w> <w n="74.3">écoliers</w> <w n="74.4">fait</w> <w n="74.5">copier</w> <w n="74.6">des</w> <w n="74.7">bosses</w>,</l>
					<l n="75" num="3.62"><w n="75.1">S</w>’<w n="75.2">estimant</w> <w n="75.3">fort</w> <w n="75.4">heureux</w> <w n="75.5">lorsque</w> <w n="75.6">Susse</w> <w n="75.7">et</w> <w n="75.8">Giroux</w></l>
					<l n="76" num="3.63"><w n="76.1">Lui</w> <w n="76.2">donnent</w> <w n="76.3">à</w> <w n="76.4">gagner</w> <w n="76.5">deux</w> <w n="76.6">pièces</w> <w n="76.7">de</w> <w n="76.8">cent</w> <w n="76.9">sous</w>.</l>
					<l n="77" num="3.64"><space unit="char" quantity="2"></space><w n="77.1">Et</w> <w n="77.2">Bardoche</w> ? — <w n="77.3">On</w> <w n="77.4">l</w>’<w n="77.5">a</w> <w n="77.6">pris</w> <w n="77.7">pour</w> <w n="77.8">arranger</w> <w n="77.9">le</w> <w n="77.10">Louvre</w>.</l>
					<l n="78" num="3.65"><w n="78.1">De</w> <w n="78.2">chefs</w>-<w n="78.3">d</w>’<w n="78.4">œuvre</w> <w n="78.5">inconnus</w> <w n="78.6">Dieu</w> <w n="78.7">sait</w> <w n="78.8">ce</w> <w n="78.9">qu</w>’<w n="78.10">il</w> <w n="78.11">découvre</w> ;</l>
					<l n="79" num="3.66"><w n="79.1">Mais</w>, <w n="79.2">en</w> <w n="79.3">revanche</w>,<w n="79.4">il</w> <w n="79.5">met</w> <w n="79.6">souvent</w> <w n="79.7">à</w> <w n="79.8">contre</w>-<w n="79.9">jour</w></l>
					<l n="80" num="3.67"><w n="80.1">Les</w> <w n="80.2">chefs</w>-<w n="80.3">d</w>’<w n="80.4">œuvre</w> <w n="80.5">connus</w>, <w n="80.6">et</w> <w n="80.7">c</w>’<w n="80.8">est</w> <w n="80.9">un</w> <w n="80.10">mauvais</w> <w n="80.11">tour</w></l>
					<l n="81" num="3.68"><w n="81.1">Pour</w> <w n="81.2">les</w> <w n="81.3">Maîtres</w> : <w n="81.4">ils</w> <w n="81.5">ont</w>, <w n="81.6">trop</w> <w n="81.7">brillants</w> <w n="81.8">ou</w> <w n="81.9">trop</w> <w n="81.10">sombres</w>,</l>
					<l n="82" num="3.69"><w n="82.1">De</w> <w n="82.2">l</w>’<w n="82.3">ombre</w> <w n="82.4">sur</w> <w n="82.5">leurs</w> <w n="82.6">clairs</w> <w n="82.7">et</w> <w n="82.8">du</w> <w n="82.9">clair</w> <w n="82.10">sur</w> <w n="82.11">leurs</w> <w n="82.12">ombres</w>.</l>
					<l n="83" num="3.70"><space unit="char" quantity="2"></space><w n="83.1">Vouzin</w> ? — <w n="83.2">Il</w> <w n="83.3">est</w> <w n="83.4">pédant</w> <w n="83.5">comme</w> <w n="83.6">par</w> <w n="83.7">le</w> <w n="83.8">passé</w> ;</l>
					<l n="84" num="3.71"><w n="84.1">Il</w> <w n="84.2">restera</w> <w n="84.3">pédant</w>, <w n="84.4">même</w> <w n="84.5">étant</w> <w n="84.6">trépassé</w>.</l>
					<l n="85" num="3.72"><space unit="char" quantity="2"></space><w n="85.1">Zacharie</w> <w n="85.2">à</w> <w n="85.3">l</w>’<w n="85.4">État</w> <w n="85.5">prépare</w> <w n="85.6">un</w> <w n="85.7">héritage</w>,</l>
					<l n="86" num="3.73"><w n="86.1">Et</w> <w n="86.2">sa</w> <w n="86.3">Californie</w> <w n="86.4">est</w> <w n="86.5">au</w> <w n="86.6">sixième</w> <w n="86.7">étage</w>.</l>
					<l n="87" num="3.74"><space unit="char" quantity="2"></space><w n="87.1">Saint</w>-<w n="87.2">Makaire</w> ? — <w n="87.3">On</w> <w n="87.4">le</w> <w n="87.5">dit</w> <w n="87.6">un</w> <w n="87.7">très</w>-<w n="87.8">fort</w> <hi rend="ital"><w n="87.9">coupletier</w></hi> ;</l>
					<l n="88" num="3.75"><w n="88.1">Il</w> <w n="88.2">mettrait</w> <w n="88.3">en</w> <w n="88.4">chansons</w> <w n="88.5">le</w> <w n="88.6">Jugement</w> <w n="88.7">dernier</w>.</l>
					<l n="89" num="3.76"><w n="89.1">Du</w> <w n="89.2">reste</w>, <w n="89.3">avec</w> <w n="89.4">des</w> <w n="89.5">tiers</w>, <w n="89.6">des</w> <w n="89.7">quarts</w> <w n="89.8">de</w> <w n="89.9">vaudeville</w></l>
					<l n="90" num="3.77"><w n="90.1">Notre</w> <w n="90.2">homme</w> <w n="90.3">a</w> <w n="90.4">maintenant</w> <w n="90.5">une</w> <w n="90.6">maison</w> <w n="90.7">en</w> <w n="90.8">ville</w> ;</l>
					<l n="91" num="3.78"><w n="91.1">Ce</w> <w n="91.2">qu</w>’<w n="91.3">il</w> <w n="91.4">fait</w> <w n="91.5">ne</w> <w n="91.6">vaut</w> <w n="91.7">rien</w>, <w n="91.8">il</w> <w n="91.9">en</w> <w n="91.10">est</w> <w n="91.11">convaincu</w> :</l>
					<l n="92" num="3.79"><w n="92.1">Mais</w> <w n="92.2">il</w> <w n="92.3">voulait</w> <w n="92.4">bien</w> <w n="92.5">vivre</w>… <w n="92.6">Il</w> <w n="92.7">aura</w> <w n="92.8">bien</w> <w n="92.9">vécu</w>.</l>
				</lg>
				<lg n="4">
					<l part="I" n="93" num="4.1"><space unit="char" quantity="2"></space><w n="93.1">Et</w> <w n="93.2">Firmin</w> ? </l>
					<l part="F" n="93" num="4.1"><w n="93.3">Dans</w> <w n="93.4">un</w> <w n="93.5">coin</w> <w n="93.6">de</w> <w n="93.7">l</w>’<w n="93.8">Archipel</w>, <w n="93.9">une</w> <w n="93.10">île</w></l>
					<l n="94" num="4.2"><w n="94.1">Pour</w> <w n="94.2">le</w> <w n="94.3">triste</w> <w n="94.4">exilé</w> <w n="94.5">fut</w> <w n="94.6">un</w> <w n="94.7">dernier</w> <w n="94.8">asile</w>.</l>
					<l n="95" num="4.3"><w n="95.1">Là</w>, <w n="95.2">devant</w> <w n="95.3">le</w> <w n="95.4">flot</w> <w n="95.5">calme</w> <w n="95.6">et</w> <w n="95.7">le</w> <w n="95.8">ciel</w> <w n="95.9">azuré</w>,</l>
					<l n="96" num="4.4"><w n="96.1">Paul</w> <w n="96.2">évoque</w> <w n="96.3">un</w> <w n="96.4">passé</w> <w n="96.5">pénible</w>, <w n="96.6">mais</w> <w n="96.7">pleuré</w> ;</l>
					<l n="97" num="4.5"><w n="97.1">Il</w> <w n="97.2">n</w>’<w n="97.3">a</w> <w n="97.4">plus</w> <w n="97.5">de</w> <w n="97.6">patrie</w>, <w n="97.7">il</w> <w n="97.8">dédaigne</w> <w n="97.9">la</w> <w n="97.10">gloire</w>…</l>
					<l n="98" num="4.6"><w n="98.1">Sa</w> <w n="98.2">vie</w> <w n="98.3">est</w> <w n="98.4">tout</w> <w n="98.5">entière</w> <w n="98.6">au</w> <w n="98.7">fond</w> <w n="98.8">de</w> <w n="98.9">sa</w> <w n="98.10">mémoire</w>.</l>
				</lg>
				<lg n="5">
					<l n="99" num="5.1"><space unit="char" quantity="2"></space><w n="99.1">Aux</w> <w n="99.2">pauvres</w>, <w n="99.3">Amélie</w> <w n="99.4">a</w> <w n="99.5">consacré</w> <w n="99.6">ses</w> <w n="99.7">soins</w>.</l>
					<l n="100" num="5.2"><w n="100.1">Sans</w> <w n="100.2">espoir</w> <w n="100.3">de</w> <w n="100.4">bonheur</w> <w n="100.5">elle</w> <w n="100.6">est</w> <w n="100.7">calme</w> <w n="100.8">du</w> <w n="100.9">moins</w>.</l>
					<l n="101" num="5.3"><w n="101.1">Dieu</w> <w n="101.2">lui</w> <w n="101.3">pardonnera</w> <w n="101.4">sans</w> <w n="101.5">doute</w>. <w n="101.6">Chaque</w> <w n="101.7">année</w>,</l>
					<l n="102" num="5.4"><w n="102.1">Dans</w> <w n="102.2">le</w> <w n="102.3">champ</w> <w n="102.4">de</w> <w n="102.5">la</w> <w n="102.6">mort</w> <w n="102.7">on</w> <w n="102.8">la</w> <w n="102.9">voit</w> <w n="102.10">prosternée</w></l>
					<l n="103" num="5.5"><w n="103.1">Et</w>, <w n="103.2">des</w> <w n="103.3">fleurs</w> <w n="103.4">dans</w> <w n="103.5">la</w> <w n="103.6">main</w>, <w n="103.7">contemplant</w> <w n="103.8">à</w> <w n="103.9">genoux</w></l>
					<l n="104" num="5.6"><w n="104.1">La</w> <w n="104.2">tombe</w> <w n="104.3">où</w> <w n="104.4">la</w> <w n="104.5">douleur</w> <w n="104.6">a</w> <w n="104.7">plongé</w> <w n="104.8">son</w> <w n="104.9">époux</w>.</l>
				</lg>
				<closer>
					<note id="(1)" type="footnote">Ces vers datent de 1848.</note>
				</closer>
			</div></body></text></TEI>