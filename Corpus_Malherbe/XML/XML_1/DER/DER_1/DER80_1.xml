<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER80">
				<head type="number">LXXX</head>
				<opener>
					<salute>A Francis Carco.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">visage</w>, <w n="1.3">une</w> <w n="1.4">phrase</w>, <w n="1.5">un</w> <w n="1.6">merle</w>, <w n="1.7">ce</w> <w n="1.8">fruit</w> <w n="1.9">d</w>’<w n="1.10">if</w></l>
					<l n="2" num="1.2"><w n="2.1">Jaune</w>, <w n="2.2">j</w>’<w n="2.3">ai</w> <w n="2.4">tout</w> <w n="2.5">aimé</w> <w n="2.6">d</w>’<w n="2.7">un</w> <w n="2.8">amour</w> <w n="2.9">maladif</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Car</w> <w n="3.2">en</w> <w n="3.3">tout</w> <w n="3.4">je</w> <w n="3.5">trouvais</w> <w n="3.6">la</w> <w n="3.7">marque</w> <w n="3.8">du</w> <w n="3.9">mystère</w></l>
					<l n="4" num="1.4"><w n="4.1">Universel</w> ; <w n="4.2">et</w> <w n="4.3">sous</w> <w n="4.4">les</w> <w n="4.5">branches</w>, <w n="4.6">solitaire</w></l>
					<l n="5" num="1.5"><w n="5.1">Dans</w> <w n="5.2">l</w>’<w n="5.3">herbe</w> <w n="5.4">et</w> <w n="5.5">la</w> <w n="5.6">chaleur</w> <w n="5.7">que</w> <w n="5.8">de</w> <w n="5.9">fois</w> <w n="5.10">j</w>’<w n="5.11">ai</w> <w n="5.12">compté</w></l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">anneaux</w> <w n="6.3">éclatants</w> <w n="6.4">des</w> <w n="6.5">guêpes</w> <w n="6.6">de</w> <w n="6.7">l</w>’<w n="6.8">été</w>.</l>
					<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">ombre</w> <w n="7.3">émouvante</w> <w n="7.4">est</w> <w n="7.5">dans</w> <w n="7.6">les</w> <w n="7.7">choses</w> <w n="7.8">minuscules</w></l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">je</w> <w n="8.3">me</w> <w n="8.4">tais</w> <w n="8.5">pour</w> <w n="8.6">écouter</w> <w n="8.7">aux</w> <w n="8.8">crépuscules</w></l>
					<l n="9" num="1.9"><w n="9.1">Le</w> <w n="9.2">grillon</w> <w n="9.3">dont</w> <w n="9.4">la</w> <w n="9.5">voix</w> <w n="9.6">déferle</w> <w n="9.7">comme</w> <w n="9.8">un</w> <w n="9.9">flot</w></l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">renaît</w> <w n="10.3">et</w> <w n="10.4">se</w> <w n="10.5">brise</w> ; <w n="10.6">et</w> <w n="10.7">dans</w> <w n="10.8">l</w>’<w n="10.9">œil</w> <w n="10.10">d</w>’<w n="10.11">un</w> <w n="10.12">mulot</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Ainsi</w> <w n="11.2">que</w> <w n="11.3">dans</w> <w n="11.4">la</w> <w n="11.5">mer</w> <w n="11.6">où</w> <w n="11.7">se</w> <w n="11.8">perdent</w> <w n="11.9">les</w> <w n="11.10">voiles</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Se</w> <w n="12.2">reflète</w> <w n="12.3">l</w>’<w n="12.4">azur</w>, <w n="12.5">la</w> <w n="12.6">lune</w> <w n="12.7">et</w> <w n="12.8">les</w> <w n="12.9">étoiles</w>.</l>
				</lg>
			</div></body></text></TEI>