<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER121">
				<head type="number">CXXI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">vie</w> <w n="1.3">est</w> <w n="1.4">douce</w> <w n="1.5">encore</w> <w n="1.6">à</w> <w n="1.7">ceux</w> <w n="1.8">qui</w> <w n="1.9">savent</w> <w n="1.10">vivre</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">tirent</w> <w n="2.3">de</w> <w n="2.4">leurs</w> <w n="2.5">maux</w> <w n="2.6">de</w> <w n="2.7">puissantes</w> <w n="2.8">liqueurs</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Suspendez</w> <w n="3.2">ce</w> <w n="3.3">fracas</w>, <w n="3.4">ce</w> <w n="3.5">tambour</w> <w n="3.6">et</w> <w n="3.7">ce</w> <w n="3.8">cuivre</w> :</l>
					<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">n</w>’<w n="4.3">est</w> <w n="4.4">besoin</w> <w n="4.5">de</w> <w n="4.6">cris</w> <w n="4.7">pour</w> <w n="4.8">émouvoir</w> <w n="4.9">nos</w> <w n="4.10">cœurs</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ne</w> <w n="5.2">me</w> <w n="5.3">reprochez</w> <w n="5.4">pas</w> <w n="5.5">de</w> <w n="5.6">vivre</w> <w n="5.7">solitaire</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">Mais</w> <w n="6.2">dans</w> <w n="6.3">ce</w> <w n="6.4">bleu</w> <w n="6.5">jardin</w> <w n="6.6">au</w> <w n="6.7">feuillage</w> <w n="6.8">léger</w></l>
					<l n="7" num="2.3"><w n="7.1">Où</w> <w n="7.2">la</w> <w n="7.3">rose</w> <w n="7.4">fleurit</w> <w n="7.5">près</w> <w n="7.6">de</w> <w n="7.7">la</w> <w n="7.8">serpentaire</w></l>
					<l n="8" num="2.4"><w n="8.1">Pour</w> <w n="8.2">un</w> <w n="8.3">songe</w> <w n="8.4">amical</w> <w n="8.5">j</w>’<w n="8.6">ai</w> <w n="8.7">de</w> <w n="8.8">quoi</w> <w n="8.9">vendanger</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">fume</w> <w n="9.3">sagement</w> <w n="9.4">ma</w> <w n="9.5">vieille</w> <w n="9.6">pipe</w> <w n="9.7">à</w> <w n="9.8">l</w>’<w n="9.9">ombre</w></l>
					<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2">un</w> <w n="10.3">arbre</w> <w n="10.4">blanc</w> <w n="10.5">et</w> <w n="10.6">vert</w>, <w n="10.7">sonore</w> <w n="10.8">et</w> <w n="10.9">japonais</w> ;</l>
					<l n="11" num="3.3"><w n="11.1">Eh</w> ! <w n="11.2">pourquoi</w> <w n="11.3">penserais</w>-<w n="11.4">je</w> <w n="11.5">à</w> <w n="11.6">quelque</w> <w n="11.7">heure</w> <w n="11.8">plus</w> <w n="11.9">sombre</w>,</l>
					<l n="12" num="3.4"><w n="12.1">À</w> <w n="12.2">d</w>’<w n="12.3">anciens</w> <w n="12.4">printemps</w> <w n="12.5">qui</w> <w n="12.6">sont</w> <w n="12.7">déjà</w> <w n="12.8">fanés</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Celui</w>-<w n="13.2">ci</w> <w n="13.3">me</w> <w n="13.4">déchire</w> <w n="13.5">et</w> <w n="13.6">cet</w> <w n="13.7">autre</w> <w n="13.8">me</w> <w n="13.9">loue</w> ;</l>
					<l n="14" num="4.2"><w n="14.1">Mais</w> <w n="14.2">qu</w>’<w n="14.3">importe</w> ? <w n="14.4">Demain</w>, <w n="14.5">les</w> <w n="14.6">grappes</w> <w n="14.7">mûriront</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Laissez</w>-<w n="15.2">moi</w> <w n="15.3">dans</w> <w n="15.4">ces</w> <w n="15.5">jours</w> <w n="15.6">que</w> <w n="15.7">le</w> <w n="15.8">destin</w> <w n="15.9">m</w>’<w n="15.10">alloue</w></l>
					<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">funèbres</w> <w n="16.3">rameaux</w> <w n="16.4">ne</w> <w n="16.5">pas</w> <w n="16.6">ceindre</w> <w n="16.7">mon</w> <w n="16.8">front</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Dois</w>-<w n="17.2">je</w> <w n="17.3">encore</w> <w n="17.4">pleurer</w> ? <w n="17.5">Qui</w> <w n="17.6">faut</w>-<w n="17.7">il</w> <w n="17.8">que</w> <w n="17.9">j</w>’<w n="17.10">envie</w> ?</l>
					<l n="18" num="5.2"><w n="18.1">Cette</w> <w n="18.2">glycine</w> <w n="18.3">en</w> <w n="18.4">fleur</w> <w n="18.5">s</w>’<w n="18.6">enroule</w> <w n="18.7">au</w> <w n="18.8">cyprès</w> <w n="18.9">noir</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">Amie</w> <w n="19.2">aux</w> <w n="19.3">beaux</w> <w n="19.4">cheveux</w> <w n="19.5">dont</w> <w n="19.6">l</w>’<w n="19.7">amour</w> <w n="19.8">est</w> <w n="19.9">ma</w> <w n="19.10">vie</w>,</l>
					<l n="20" num="5.4"><w n="20.1">N</w>’<w n="20.2">ai</w>-<w n="20.3">je</w> <w n="20.4">pas</w> <w n="20.5">les</w> <w n="20.6">bras</w> <w n="20.7">nus</w> <w n="20.8">qui</w> <w n="20.9">m</w>’<w n="20.10">enivrent</w> <w n="20.11">le</w> <w n="20.12">soir</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Bientôt</w>, <w n="21.2">les</w> <w n="21.3">escargots</w> <w n="21.4">endormis</w> <w n="21.5">sous</w> <w n="21.6">les</w> <w n="21.7">fraises</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Un</w> <w n="22.2">chœur</w> <w n="22.3">de</w> <w n="22.4">rossignols</w> <w n="22.5">charmera</w> <w n="22.6">mon</w> <w n="22.7">loisir</w> ;</l>
					<l n="23" num="6.3"><w n="23.1">Mais</w> <w n="23.2">déjà</w> <w n="23.3">renversée</w> <w n="23.4">et</w> <w n="23.5">dans</w> <w n="23.6">l</w>’<w n="23.7">ombre</w> <w n="23.8">tu</w> <w n="23.9">baises</w></l>
					<l n="24" num="6.4"><w n="24.1">Les</w> <w n="24.2">roses</w> <w n="24.3">de</w> <w n="24.4">juillet</w> <w n="24.5">en</w> <w n="24.6">riant</w> <w n="24.7">de</w> <w n="24.8">plaisir</w>.</l>
				</lg>
			</div></body></text></TEI>