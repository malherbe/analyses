<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER108">
				<head type="number">CVIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">nous</w> <w n="1.3">taisons</w>. <w n="1.4">Le</w> <w n="1.5">vent</w> <w n="1.6">balance</w></l>
					<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">deux</w> <w n="2.3">saules</w> <w n="2.4">sur</w> <w n="2.5">l</w>’<w n="2.6">abreuvoir</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">je</w> <w n="3.3">sais</w> <w n="3.4">malgré</w> <w n="3.5">ton</w> <w n="3.6">silence</w></l>
					<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">ce</w> <w n="4.3">soir</w> <w n="4.4">est</w> <w n="4.5">le</w> <w n="4.6">dernier</w> <w n="4.7">soir</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Adieu</w>. <w n="5.2">Les</w> <w n="5.3">feuilles</w> <w n="5.4">tombent</w>. <w n="5.5">Lune</w></l>
					<l n="6" num="2.2"><w n="6.1">Coutumière</w>. <w n="6.2">Décor</w> <w n="6.3">banal</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Tourterelles</w>, <w n="7.2">crépuscule</w>. <w n="7.3">Une</w></l>
					<l n="8" num="2.4"><w n="8.1">Étoile</w>, <w n="8.2">comme</w> <w n="8.3">un</w> <w n="8.4">point</w> <w n="8.5">final</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Tu</w> <w n="9.2">as</w> <w n="9.3">la</w> <w n="9.4">force</w> <w n="9.5">de</w> <w n="9.6">sourire</w></l>
					<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">dans</w> <w n="10.3">mon</w> <w n="10.4">cœur</w> <w n="10.5">je</w> <w n="10.6">reconnais</w></l>
					<l n="11" num="3.3"><w n="11.1">L</w>’<w n="11.2">odeur</w> <w n="11.3">des</w> <w n="11.4">buis</w> <w n="11.5">que</w> <w n="11.6">l</w>’<w n="11.7">on</w> <w n="11.8">respire</w></l>
					<l n="12" num="3.4"><w n="12.1">Dans</w> <w n="12.2">les</w> <w n="12.3">jardins</w> <w n="12.4">abandonnés</w>.</l>
				</lg>
			</div></body></text></TEI>