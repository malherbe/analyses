<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER88">
				<head type="number">LXXXVIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Oui</w>, <w n="1.2">je</w> <w n="1.3">chante</w> <w n="1.4">la</w> <w n="1.5">joie</w> <w n="1.6">ivre</w> <w n="1.7">et</w> <w n="1.8">passionnée</w></l>
					<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">je</w> <w n="2.3">noue</w> <w n="2.4">à</w> <w n="2.5">ma</w> <w n="2.6">barbe</w> <w n="2.7">une</w> <w n="2.8">rose</w> <w n="2.9">fanée</w></l>
					<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">songer</w> <w n="3.3">nuit</w> <w n="3.4">et</w> <w n="3.5">jour</w> <w n="3.6">qu</w>’<w n="3.7">il</w> <w n="3.8">faudra</w> <w n="3.9">que</w> <w n="3.10">mon</w> <w n="3.11">corps</w></l>
					<l n="4" num="1.4"><w n="4.1">Se</w> <w n="4.2">dissolve</w> <w n="4.3">comme</w> <w n="4.4">elle</w> <w n="4.5">et</w> <w n="4.6">quitte</w> <w n="4.7">les</w> <w n="4.8">décors</w></l>
					<l n="5" num="1.5"><w n="5.1">Fastueux</w> <w n="5.2">où</w> <w n="5.3">le</w> <w n="5.4">monde</w> <w n="5.5">épanouit</w> <w n="5.6">sa</w> <w n="5.7">force</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Je</w> <w n="6.2">m</w>’<w n="6.3">en</w> <w n="6.4">irai</w>. <w n="6.5">Je</w> <w n="6.6">tomberai</w> <w n="6.7">comme</w> <w n="6.8">l</w>’<w n="6.9">écorce</w></l>
					<l n="7" num="1.7"><w n="7.1">Des</w> <w n="7.2">platanes</w>, <w n="7.3">comme</w> <w n="7.4">les</w> <w n="7.5">feuilles</w>, <w n="7.6">comme</w> <w n="7.7">les</w></l>
					<l n="8" num="1.8"><w n="8.1">Roses</w> ! <w n="8.2">Je</w> <w n="8.3">suis</w> <w n="8.4">vivant</w> ! <w n="8.5">Ciel</w>, <w n="8.6">nuages</w> <w n="8.7">gonflés</w></l>
					<l n="9" num="1.9"><w n="9.1">D</w>’<w n="9.2">eau</w> <w n="9.3">lourde</w>, <w n="9.4">bois</w> <w n="9.5">roussis</w> <w n="9.6">par</w> <w n="9.7">les</w> <w n="9.8">torches</w> <w n="9.9">d</w>’<w n="9.10">automne</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Vergers</w> <w n="10.2">où</w> <w n="10.3">l</w>’<w n="10.4">or</w> <w n="10.5">vivant</w> <w n="10.6">des</w> <w n="10.7">abeilles</w> <w n="10.8">bourdonne</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Fruits</w> <w n="11.2">riches</w>, <w n="11.3">souvenirs</w> <w n="11.4">d</w>’<w n="11.5">un</w> <w n="11.6">magnifique</w> <w n="11.7">été</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Moissons</w>, <w n="12.2">je</w> <w n="12.3">vous</w> <w n="12.4">respire</w> <w n="12.5">avec</w> <w n="12.6">avidité</w></l>
					<l n="13" num="1.13"><w n="13.1">Et</w> <w n="13.2">je</w> <w n="13.3">mêle</w> <w n="13.4">ma</w> <w n="13.5">vie</w> <w n="13.6">au</w> <w n="13.7">triomphe</w> <w n="13.8">des</w> <w n="13.9">choses</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Éperdu</w> <w n="14.2">comme</w> <w n="14.3">les</w> <w n="14.4">feuilles</w>, <w n="14.5">comme</w> <w n="14.6">les</w> <w n="14.7">roses</w> !</l>
				</lg>
			</div></body></text></TEI>