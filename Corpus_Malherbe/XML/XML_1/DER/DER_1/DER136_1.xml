<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER136">
				<head type="number">CXXXVI</head>
				<opener>
					<salute>A Armand Praviel.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Sur</w> <w n="1.2">le</w> <w n="1.3">toit</w> <w n="1.4">noir</w> <w n="1.5">et</w> <w n="1.6">bleu</w> <w n="1.7">que</w> <w n="1.8">mon</w> <w n="1.9">exil</w> <w n="1.10">habite</w></l>
					<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">grêle</w> <w n="2.3">blanche</w> <w n="2.4">et</w> <w n="2.5">dure</w> <w n="2.6">aux</w> <w n="2.7">ardoises</w> <w n="2.8">crépite</w></l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">rebondit</w>, <w n="3.3">tintant</w> <w n="3.4">aux</w> <w n="3.5">vitres</w>, <w n="3.6">parmi</w> <w n="3.7">les</w></l>
					<l n="4" num="1.4"><w n="4.1">Plumes</w> <w n="4.2">qui</w> <w n="4.3">volent</w> <w n="4.4">dans</w> <w n="4.5">la</w> <w n="4.6">fuite</w> <w n="4.7">des</w> <w n="4.8">poulets</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Grêle</w>, <w n="5.2">boules</w> <w n="5.3">de</w> <w n="5.4">gui</w>, <w n="5.5">cristal</w>, <w n="5.6">œufs</w> <w n="5.7">des</w> <w n="5.8">colombes</w></l>
					<l n="6" num="1.6"><w n="6.1">Fabuleuses</w>, <w n="6.2">dans</w> <w n="6.3">un</w> <w n="6.4">fracas</w> <w n="6.5">rauque</w> <w n="6.6">tu</w> <w n="6.7">tombes</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Fauchant</w> <w n="7.2">les</w> <w n="7.3">roses</w> <w n="7.4">des</w> <w n="7.5">rosiers</w>, <w n="7.6">et</w> <w n="7.7">tu</w> <w n="7.8">détruis</w></l>
					<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">verdure</w> <w n="8.3">des</w> <w n="8.4">cerisiers</w> <w n="8.5">rouges</w> <w n="8.6">de</w> <w n="8.7">fruits</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Dans</w> <w n="9.2">l</w>’<w n="9.3">herbe</w>, <w n="9.4">les</w> <w n="9.5">rubis</w> <w n="9.6">roulent</w> <w n="9.7">avec</w> <w n="9.8">les</w> <w n="9.9">perles</w>.</l>
					<l n="10" num="1.10"><w n="10.1">Ô</w> <w n="10.2">nuage</w>, <w n="10.3">au</w> <w n="10.4">coteau</w> <w n="10.5">bleuâtre</w> <w n="10.6">tu</w> <w n="10.7">déferles</w></l>
					<l n="11" num="1.11"><w n="11.1">Comme</w> <w n="11.2">un</w> <w n="11.3">océan</w> <w n="11.4">gris</w> <w n="11.5">qui</w> <w n="11.6">submerge</w> <w n="11.7">l</w>’<w n="11.8">azur</w></l>
					<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">la</w> <w n="12.3">terre</w> <w n="12.4">avec</w> <w n="12.5">ses</w> <w n="12.6">vignes</w> <w n="12.7">tristes</w> ; <w n="12.8">et</w> <w n="12.9">sur</w></l>
					<l n="13" num="1.13"><w n="13.1">Le</w> <w n="13.2">paysage</w> <w n="13.3">éteint</w> <w n="13.4">cette</w> <w n="13.5">mélancolie</w>,</l>
					<l n="14" num="1.14"><w n="14.1">N</w>’<w n="14.2">est</w>-<w n="14.3">ce</w> <w n="14.4">la</w> <w n="14.5">mienne</w> <w n="14.6">où</w> <w n="14.7">tout</w> <w n="14.8">soleil</w> <w n="14.9">baisse</w> <w n="14.10">et</w> <w n="14.11">s</w>’<w n="14.12">oublie</w>,</l>
					<l n="15" num="1.15"><w n="15.1">Qui</w> <w n="15.2">déchire</w> <w n="15.3">et</w> <w n="15.4">ternit</w> <w n="15.5">les</w> <w n="15.6">rameaux</w> <w n="15.7">les</w> <w n="15.8">plus</w> <w n="15.9">verts</w></l>
					<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">de</w> <w n="16.3">cendre</w> <w n="16.4">et</w> <w n="16.5">de</w> <w n="16.6">nuit</w> <w n="16.7">imprègne</w> <w n="16.8">l</w>’<w n="16.9">univers</w> ?</l>
				</lg>
			</div></body></text></TEI>