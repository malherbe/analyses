<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" rhyme="none" key="DER117">
				<head type="number">CXVII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">lune</w> <w n="1.3">se</w> <w n="1.4">répand</w> <w n="1.5">sur</w> <w n="1.6">les</w> <w n="1.7">blanches</w> <w n="1.8">prairies</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Mais</w> <w n="2.2">je</w> <w n="2.3">veux</w> <w n="2.4">dans</w> <w n="2.5">mes</w> <w n="2.6">bras</w> <w n="2.7">ce</w> <w n="2.8">soir</w> <w n="2.9">que</w> <w n="2.10">tu</w> <w n="2.11">souries</w></l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">que</w> <w n="3.3">d</w>’<w n="3.4">un</w> <w n="3.5">cœur</w> <w n="3.6">gonflé</w> <w n="3.7">de</w> <w n="3.8">jeunesse</w> <w n="3.9">et</w> <w n="3.10">d</w>’<w n="3.11">amour</w></l>
					<l n="4" num="1.4"><w n="4.1">Tu</w> <w n="4.2">goûtes</w> <w n="4.3">la</w> <w n="4.4">beauté</w> <w n="4.5">de</w> <w n="4.6">l</w>’<w n="4.7">ombre</w> <w n="4.8">après</w> <w n="4.9">le</w> <w n="4.10">jour</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Car</w> <w n="5.2">nous</w> <w n="5.3">remémorant</w> <w n="5.4">l</w>’<w n="5.5">azur</w>, <w n="5.6">ô</w> <w n="5.7">toi</w> <w n="5.8">qui</w> <w n="5.9">n</w>’<w n="5.10">aimes</w></l>
					<l n="6" num="1.6"><w n="6.1">Que</w> <w n="6.2">les</w> <w n="6.3">prés</w> <w n="6.4">attiédis</w> <w n="6.5">et</w> <w n="6.6">les</w> <w n="6.7">fraîches</w> <w n="6.8">tonnelles</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Dans</w> <w n="7.2">la</w> <w n="7.3">lueur</w> <w n="7.4">lunaire</w> <w n="7.5">encore</w> <w n="7.6">nous</w> <w n="7.7">verrons</w></l>
					<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">lumière</w>, <w n="8.3">le</w> <w n="8.4">chœur</w> <w n="8.5">ivre</w> <w n="8.6">des</w> <w n="8.7">moucherons</w>,</l>
					<l n="9" num="1.9"><w n="9.1">La</w> <w n="9.2">cage</w> <w n="9.3">de</w> <w n="9.4">roseaux</w> <w n="9.5">suspendue</w> <w n="9.6">au</w> <w n="9.7">platane</w></l>
					<l n="10" num="1.10"><w n="10.1">Et</w> <w n="10.2">l</w>’<w n="10.3">ombre</w> <w n="10.4">violette</w> <w n="10.5">où</w> <w n="10.6">le</w> <w n="10.7">paon</w> <w n="10.8">vert</w> <w n="10.9">étale</w></l>
					<l n="11" num="1.11"><w n="11.1">Rouge</w>, <w n="11.2">bleue</w> <w n="11.3">et</w> <w n="11.4">dorée</w> <w n="11.5">une</w> <w n="11.6">roue</w> <w n="11.7">en</w> <w n="11.8">émail</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Le</w> <w n="12.2">bouc</w> <w n="12.3">apprivoisé</w> <w n="12.4">sous</w> <w n="12.5">les</w> <w n="12.6">branches</w> <w n="12.7">du</w> <w n="12.8">mail</w>,</l>
					<l n="13" num="1.13"><w n="13.1">La</w> <w n="13.2">neige</w> <w n="13.3">des</w> <w n="13.4">brebis</w> <w n="13.5">et</w> <w n="13.6">les</w> <w n="13.7">genêts</w> <w n="13.8">de</w> <w n="13.9">soufre</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Les</w> <w n="14.2">feuillages</w> <w n="14.3">légers</w> <w n="14.4">et</w> <w n="14.5">l</w>’<w n="14.6">air</w> <w n="14.7">tendre</w> <w n="14.8">qui</w> <w n="14.9">souffle</w></l>
					<l n="15" num="1.15"><w n="15.1">Dans</w> <w n="15.2">l</w>’<w n="15.3">herbe</w> <w n="15.4">d</w>’<w n="15.5">émeraude</w> <w n="15.6">et</w> <w n="15.7">le</w> <w n="15.8">trèfle</w> <w n="15.9">incarnat</w>.</l>
					<l n="16" num="1.16"><w n="16.1">Angélus</w>. <w n="16.2">Souvenirs</w>. <w n="16.3">Cloche</w> <w n="16.4">et</w> <w n="16.5">pensionnat</w>.</l>
					<l n="17" num="1.17"><w n="17.1">La</w> <w n="17.2">lune</w> <w n="17.3">sur</w> <w n="17.4">le</w> <w n="17.5">toit</w> <w n="17.6">glissait</w> <w n="17.7">sa</w> <w n="17.8">blanche</w> <w n="17.9">corne</w>,</l>
					<l n="18" num="1.18"><w n="18.1">Sous</w> <w n="18.2">les</w> <w n="18.3">tilleuls</w> <w n="18.4">nouveaux</w> <w n="18.5">tu</w> <w n="18.6">sautais</w> <w n="18.7">à</w> <w n="18.8">la</w> <w n="18.9">corde</w></l>
					<l n="19" num="1.19"><w n="19.1">En</w> <w n="19.2">écoutant</w> <w n="19.3">gémir</w> <w n="19.4">de</w> <w n="19.5">vagues</w> <w n="19.6">pianos</w></l>
					<l n="20" num="1.20"><w n="20.1">Dans</w> <w n="20.2">la</w> <w n="20.3">cour</w> <w n="20.4">où</w> <w n="20.5">déjà</w> <w n="20.6">se</w> <w n="20.7">taisaient</w> <w n="20.8">les</w> <w n="20.9">moineaux</w>.</l>
					<l n="21" num="1.21"><w n="21.1">Tu</w> <w n="21.2">cousais</w> <w n="21.3">mes</w> <w n="21.4">billets</w> <w n="21.5">aux</w> <w n="21.6">volants</w> <w n="21.7">de</w> <w n="21.8">tes</w> <w n="21.9">robes</w>…</l>
					<l n="22" num="1.22"><w n="22.1">Les</w> <w n="22.2">mésanges</w> <w n="22.3">de</w> <w n="22.4">juin</w> <w n="22.5">s</w>’<w n="22.6">endorment</w> <w n="22.7">sur</w> <w n="22.8">les</w> <w n="22.9">roses</w></l>
					<l n="23" num="1.23"><w n="23.1">Et</w> <w n="23.2">près</w> <w n="23.3">de</w> <w n="23.4">toi</w> <w n="23.5">je</w> <w n="23.6">songe</w> <w n="23.7">à</w> <w n="23.8">tous</w> <w n="23.9">les</w> <w n="23.10">rossignols</w></l>
					<l n="24" num="1.24"><w n="24.1">Dont</w> <w n="24.2">la</w> <w n="24.3">voix</w> <w n="24.4">enivrait</w> <w n="24.5">les</w> <w n="24.6">jasmins</w> <w n="24.7">espagnols</w></l>
					<l n="25" num="1.25"><w n="25.1">Aux</w> <w n="25.2">jardins</w> <w n="25.3">de</w> <w n="25.4">jadis</w> <w n="25.5">dans</w> <w n="25.6">la</w> <w n="25.7">nuit</w> <w n="25.8">odorante</w></l>
					<l n="26" num="1.26"><w n="26.1">Où</w> <w n="26.2">la</w> <w n="26.3">lune</w> <w n="26.4">roulait</w> <w n="26.5">comme</w> <w n="26.6">une</w> <w n="26.7">rouge</w> <w n="26.8">orange</w>.</l>
				</lg>
			</div></body></text></TEI>