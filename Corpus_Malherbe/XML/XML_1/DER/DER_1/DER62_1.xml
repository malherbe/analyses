<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER62">
				<head type="number">LXII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">enthousiasme</w>, <w n="1.3">comme</w> <w n="1.4">un</w> <w n="1.5">peuple</w> <w n="1.6">de</w> <w n="1.7">frelons</w>,</l>
					<l n="2" num="1.2"><space quantity="16" unit="char"></space><w n="2.1">Vibre</w> <w n="2.2">dans</w> <w n="2.3">l</w>’<w n="2.4">heure</w> <w n="2.5">noire</w>.</l>
					<l n="3" num="1.3"><w n="3.1">Debout</w>, <w n="3.2">et</w> <w n="3.3">déchirons</w> <w n="3.4">la</w> <w n="3.5">nuit</w> <w n="3.6">où</w> <w n="3.7">nous</w> <w n="3.8">râlons</w>,</l>
					<l n="4" num="1.4"><space quantity="16" unit="char"></space><w n="4.1">Pour</w> <w n="4.2">un</w> <w n="4.3">ciel</w> <w n="4.4">de</w> <w n="4.5">victoire</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Nos</w> <w n="5.2">marteaux</w> <w n="5.3">font</w> <w n="5.4">le</w> <w n="5.5">bruit</w> <w n="5.6">crépitant</w> <w n="5.7">des</w> <w n="5.8">grêlons</w> ;</l>
					<l n="6" num="2.2"><space quantity="16" unit="char"></space><w n="6.1">Et</w> <w n="6.2">pour</w> <w n="6.3">le</w> <w n="6.4">char</w> <w n="6.5">d</w>’<w n="6.6">ivoire</w></l>
					<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">dompterai</w> <w n="7.3">les</w> <w n="7.4">mots</w> <w n="7.5">comme</w> <w n="7.6">des</w> <w n="7.7">étalons</w></l>
					<l n="8" num="2.4"><space quantity="16" unit="char"></space><w n="8.1">Qui</w> <w n="8.2">traîneront</w> <w n="8.3">ta</w> <w n="8.4">gloire</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Se</w> <w n="9.2">roidissent</w> <w n="9.3">leur</w> <w n="9.4">flanc</w> <w n="9.5">sur</w> <w n="9.6">le</w> <w n="9.7">timon</w> <w n="9.8">d</w>’<w n="9.9">airain</w>,</l>
					<l n="10" num="3.2"><space quantity="16" unit="char"></space><w n="10.1">Et</w> <w n="10.2">d</w>’<w n="10.3">un</w> <w n="10.4">vol</w> <w n="10.5">souverain</w></l>
					<l n="11" num="3.3"><w n="11.1">Que</w> <w n="11.2">dans</w> <w n="11.3">mon</w> <w n="11.4">bras</w>, <w n="11.5">ton</w> <w n="11.6">corps</w> <w n="11.7">qui</w> <w n="11.8">tremble</w> <w n="11.9">et</w> <w n="11.10">s</w>’<w n="11.11">abandonne</w>,</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><space quantity="16" unit="char"></space><w n="12.1">Au</w> <w n="12.2">tumulte</w> <w n="12.3">du</w> <w n="12.4">vent</w>,</l>
					<l n="13" num="4.2"><w n="13.1">Sur</w> <w n="13.2">la</w> <w n="13.3">rouge</w> <w n="13.4">splendeur</w> <w n="13.5">de</w> <w n="13.6">ce</w> <w n="13.7">couchant</w> <w n="13.8">d</w>’<w n="13.9">automne</w>,</l>
					<l n="14" num="4.3"><space quantity="16" unit="char"></space><w n="14.1">S</w>’<w n="14.2">élève</w> <w n="14.3">triomphant</w> !</l>
				</lg>
			</div></body></text></TEI>