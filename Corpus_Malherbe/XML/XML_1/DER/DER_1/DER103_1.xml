<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" rhyme="none" key="DER103">
				<head type="number">CIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">T</w>’<w n="1.2">en</w> <w n="1.3">souviens</w>-<w n="1.4">tu</w> (<w n="1.5">comme</w> <w n="1.6">on</w> <w n="1.7">écrit</w> <w n="1.8">dans</w> <w n="1.9">les</w> <w n="1.10">romances</w>)</l>
					<l n="2" num="1.2"><w n="2.1">T</w>’<w n="2.2">en</w> <w n="2.3">souviens</w>-<w n="2.4">tu</w> <w n="2.5">de</w> <w n="2.6">ce</w> <w n="2.7">dimanche</w> <w n="2.8">des</w> <w n="2.9">dimanches</w></l>
					<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">nous</w> <w n="3.3">avons</w> <w n="3.4">erré</w> <w n="3.5">sous</w> <w n="3.6">les</w> <w n="3.7">mornes</w> <w n="3.8">platanes</w></l>
					<l n="4" num="1.4"><w n="4.1">Après</w> <w n="4.2">l</w>’<w n="4.3">azur</w> <w n="4.4">et</w> <w n="4.5">la</w> <w n="4.6">poussière</w> <w n="4.7">et</w> <w n="4.8">la</w> <w n="4.9">chaleur</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Souvenirs</w>, <w n="5.2">souvenirs</w>, <w n="5.3">venez</w> <w n="5.4">qu</w>’<w n="5.5">on</w> <w n="5.6">vous</w> <w n="5.7">rétame</w>,</l>
					<l n="6" num="2.2"><space quantity="12" unit="char"></space><w n="6.1">C</w>’<w n="6.2">est</w> <w n="6.3">moi</w> <w n="6.4">qui</w> <w n="6.5">suis</w> <w n="6.6">le</w> <w n="6.7">rétameur</w> !</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><space quantity="12" unit="char"></space><w n="7.1">Ah</w> ! <w n="7.2">malgré</w> <w n="7.3">qu</w>’<w n="7.4">on</w> <w n="7.5">veuille</w> <w n="7.6">sourire</w>,</l>
					<l n="8" num="3.2"><space quantity="12" unit="char"></space><w n="8.1">Moi</w>, <w n="8.2">j</w>’<w n="8.3">ai</w> <w n="8.4">des</w> <w n="8.5">larmes</w> <w n="8.6">plein</w> <w n="8.7">le</w> <w n="8.8">cœur</w></l>
					<l n="9" num="3.3"><space quantity="12" unit="char"></space><w n="9.1">Et</w> <w n="9.2">je</w> <w n="9.3">m</w>’<w n="9.4">en</w> <w n="9.5">vais</w> <w n="9.6">à</w> <w n="9.7">la</w> <w n="9.8">dérive</w>.</l>
				</lg>
				<lg n="4">
					<l n="10" num="4.1"><w n="10.1">Cette</w> <w n="10.2">musique</w> <w n="10.3">au</w> <w n="10.4">loin</w> <w n="10.5">et</w> <w n="10.6">ces</w> <w n="10.7">bouffées</w> <w n="10.8">de</w> <w n="10.9">cuivre</w>,</l>
					<l n="11" num="4.2"><w n="11.1">Polka</w> <w n="11.2">pour</w> <w n="11.3">deux</w> <w n="11.4">pistons</w> <w n="11.5">et</w> <w n="11.6">grands</w> <w n="11.7">airs</w> <w n="11.8">d</w>’<w n="11.9">opéra</w></l>
					<l n="12" num="4.3"><hi rend="ital"><w n="12.1">La</w> <w n="12.2">Favorite</w>, <w n="12.3">l</w>’<w n="12.4">Africaine</w>, <subst hand="RR" reason="analysis" type="phonemization"><del>etc</del><add rend="hidden"><w n="12.5">et</w> <w n="12.6">cétéra</w></add></subst></hi>…</l>
				</lg>
				<lg n="5">
					<l n="13" num="5.1"><space quantity="12" unit="char"></space><w n="13.1">La</w> <w n="13.2">même</w> <w n="13.3">lune</w> <w n="13.4">va</w> <w n="13.5">reluire</w></l>
					<l n="14" num="5.2"><space quantity="6" unit="char"></space><w n="14.1">Et</w> <w n="14.2">refléter</w> <w n="14.3">son</w> <w n="14.4">cristal</w> <w n="14.5">nacarat</w></l>
					<l n="15" num="5.3"><space quantity="18" unit="char"></space><w n="15.1">Dans</w> <w n="15.2">l</w>’<w n="15.3">eau</w> <w n="15.4">chaude</w> <w n="15.5">du</w> <w n="15.6">fleuve</w>.</l>
				</lg>
				<lg n="6">
					<l n="16" num="6.1"><w n="16.1">Un</w> <w n="16.2">vent</w> <w n="16.3">tiède</w> <w n="16.4">se</w> <w n="16.5">prit</w> <w n="16.6">à</w> <w n="16.7">remuer</w> <w n="16.8">les</w> <w n="16.9">feuilles</w>.</l>
					<l n="17" num="6.2"><space quantity="12" unit="char"></space><w n="17.1">Tes</w> <w n="17.2">mains</w> <w n="17.3">étaient</w> <w n="17.4">pleines</w> <w n="17.5">de</w> <w n="17.6">larmes</w>.</l>
					<l n="18" num="6.3"><w n="18.1">Les</w> <w n="18.2">tramways</w> <w n="18.3">en</w> <w n="18.4">passant</w> <w n="18.5">t</w>’<w n="18.6">éclairaient</w> <w n="18.7">le</w> <w n="18.8">visage</w>.</l>
				</lg>
				<lg n="7">
					<l n="19" num="7.1"><w n="19.1">Près</w> <w n="19.2">d</w>’<w n="19.3">un</w> <w n="19.4">café</w> <w n="19.5">pleurait</w> <w n="19.6">une</w> <w n="19.7">aigre</w> <w n="19.8">clarinette</w>.</l>
					<l n="20" num="7.2"><w n="20.1">Un</w> <w n="20.2">grand</w> <w n="20.3">magnolia</w> <w n="20.4">balançait</w> <w n="20.5">ses</w> <w n="20.6">fleurs</w> <w n="20.7">blanches</w>,</l>
					<l n="21" num="7.3"><space quantity="12" unit="char"></space><w n="21.1">Et</w> <w n="21.2">la</w> <w n="21.3">lune</w> <w n="21.4">pendait</w> <w n="21.5">aux</w> <w n="21.6">branches</w>,</l>
					<l n="22" num="7.4"><space quantity="12" unit="char"></space><w n="22.1">Douce</w> <w n="22.2">lanterne</w> <w n="22.3">japonaise</w>.</l>
				</lg>
			</div></body></text></TEI>