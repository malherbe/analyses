<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER84">
				<head type="number">LXXXIV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Vers</w> <w n="1.2">la</w> <w n="1.3">croisée</w> <w n="1.4">et</w> <w n="1.5">vers</w> <w n="1.6">les</w> <w n="1.7">roses</w> <w n="1.8">du</w> <w n="1.9">plafond</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Souffle</w> <w n="2.2">au</w> <w n="2.3">bout</w> <w n="2.4">de</w> <w n="2.5">tes</w> <w n="2.6">doigts</w> <w n="2.7">les</w> <w n="2.8">bulles</w> <w n="2.9">de</w> <w n="2.10">savon</w></l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">souris</w> <w n="3.3">tendrement</w> <w n="3.4">à</w> <w n="3.5">mon</w> <w n="3.6">rêve</w> <w n="3.7">crédule</w>.</l>
					<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">lune</w> <w n="4.3">de</w> <w n="4.4">cristal</w> <w n="4.5">flotte</w> <w n="4.6">comme</w> <w n="4.7">une</w> <w n="4.8">bulle</w></l>
					<l n="5" num="1.5"><w n="5.1">Aux</w> <w n="5.2">cieux</w> <w n="5.3">tendres</w> <w n="5.4">et</w> <w n="5.5">verts</w> ; <w n="5.6">les</w> <w n="5.7">constellations</w></l>
					<l n="6" num="1.6"><w n="6.1">Éternelles</w>, <w n="6.2">nos</w> <w n="6.3">cris</w>, <w n="6.4">nos</w> <w n="6.5">pleurs</w>, <w n="6.6">nos</w> <w n="6.7">passions</w></l>
					<l n="7" num="1.7"><w n="7.1">Se</w> <w n="7.2">vont</w> <w n="7.3">faner</w> <w n="7.4">comme</w> <w n="7.5">de</w> <w n="7.6">blanches</w> <w n="7.7">marguerites</w>.</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">toi</w>, <w n="8.3">ma</w> <w n="8.4">pure</w> <w n="8.5">amie</w> <w n="8.6">et</w> <w n="8.7">claire</w>, <w n="8.8">qui</w> <w n="8.9">abrites</w></l>
					<l n="9" num="1.9"><w n="9.1">Les</w> <w n="9.2">bleus</w> <w n="9.3">ramiers</w> <w n="9.4">de</w> <w n="9.5">mon</w> <w n="9.6">amour</w>, <w n="9.7">tu</w> <w n="9.8">souriras</w></l>
					<l n="10" num="1.10"><w n="10.1">À</w> <w n="10.2">quelque</w> <w n="10.3">autre</w> <w n="10.4">baiser</w> <w n="10.5">qui</w> <w n="10.6">brûlera</w> <w n="10.7">tes</w> <w n="10.8">bras</w></l>
					<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">tu</w> <w n="11.3">ne</w> <w n="11.4">seras</w> <w n="11.5">plus</w> <w n="11.6">qu</w>’<w n="11.7">un</w> <w n="11.8">tumulte</w> <w n="11.9">de</w> <w n="11.10">fête</w>.</l>
					<l n="12" num="1.12"><w n="12.1">L</w>’<w n="12.2">eau</w> <w n="12.3">glace</w> <w n="12.4">tes</w> <w n="12.5">bras</w> <w n="12.6">nus</w> <w n="12.7">dans</w> <w n="12.8">la</w> <w n="12.9">blanche</w> <w n="12.10">cuvette</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Souffle</w> <w n="13.2">des</w> <w n="13.3">bulles</w> <w n="13.4">qui</w> <w n="13.5">reflètent</w> <w n="13.6">l</w>’<w n="13.7">Univers</w>.</l>
					<l n="14" num="1.14"><w n="14.1">Mais</w> <w n="14.2">vois</w> ! <w n="14.3">plus</w> <w n="14.4">rien</w> <w n="14.5">ne</w> <w n="14.6">flotte</w> <w n="14.7">aux</w> <w n="14.8">cieux</w> <w n="14.9">tendres</w> <w n="14.10">et</w> <w n="14.11">verts</w> ;</l>
					<l n="15" num="1.15"><w n="15.1">La</w> <w n="15.2">lune</w> <w n="15.3">de</w> <w n="15.4">cristal</w> <w n="15.5">sur</w> <w n="15.6">un</w> <w n="15.7">toit</w> <w n="15.8">s</w>’<w n="15.9">est</w> <w n="15.10">brisée</w> ;</l>
					<l n="16" num="1.16"><w n="16.1">Baisse</w> <w n="16.2">les</w> <w n="16.3">yeux</w> : <w n="16.4">le</w> <w n="16.5">pré</w> <w n="16.6">scintille</w> <w n="16.7">de</w> <w n="16.8">rosée</w>.</l>
				</lg>
			</div></body></text></TEI>