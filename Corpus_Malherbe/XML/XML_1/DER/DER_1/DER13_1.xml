<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER13">
				<head type="number">XIII</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">temps</w> <w n="1.3">est</w> <w n="1.4">achevé</w> <w n="1.5">des</w> <w n="1.6">cris</w> <w n="1.7">et</w> <w n="1.8">des</w> <w n="1.9">tempêtes</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Aimons</w>-<w n="2.2">nous</w> <w n="2.3">aujourd</w>’<w n="2.4">hui</w> <w n="2.5">sans</w> <w n="2.6">tambours</w> <w n="2.7">ni</w> <w n="2.8">trompettes</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">les</w> <w n="3.3">étalons</w> <w n="3.4">blancs</w> <w n="3.5">qui</w> <w n="3.6">piaffent</w> <w n="3.7">dans</w> <w n="3.8">la</w> <w n="3.9">cour</w></l>
					<l n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">les</w> <w n="4.3">mettrons</w> <w n="4.4">à</w> <w n="4.5">l</w>’<w n="4.6">écurie</w>. <w n="4.7">Ô</w> <w n="4.8">mon</w> <w n="4.9">amour</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Suis</w>-<w n="5.2">moi</w> ; <w n="5.3">nous</w> <w n="5.4">mènerons</w> <w n="5.5">le</w> <w n="5.6">troupeau</w> <w n="5.7">noir</w> <w n="5.8">des</w> <w n="5.9">chèvres</w>.</l>
					<l n="6" num="1.6"><w n="6.1">Les</w> <w n="6.2">mots</w> <w n="6.3">ambitieux</w> <w n="6.4">déserteront</w> <w n="6.5">nos</w> <w n="6.6">lèvres</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Nous</w> <w n="7.2">raillerons</w> <w n="7.3">la</w> <w n="7.4">gloire</w> <w n="7.5">et</w> <w n="7.6">nous</w> <w n="7.7">nous</w> <w n="7.8">étendrons</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">soir</w>, <w n="8.3">pour</w> <w n="8.4">bavarder</w>, <w n="8.5">sous</w> <w n="8.6">les</w> <w n="8.7">rhododendrons</w>.</l>
				</lg>
			</div></body></text></TEI>