<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER115">
				<head type="number">CXV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">C</w>’<w n="1.2">est</w> <w n="1.3">février</w>, <w n="1.4">le</w> <w n="1.5">mois</w> <w n="1.6">des</w> <w n="1.7">chattes</w> <w n="1.8">et</w> <w n="1.9">du</w> <w n="1.10">givre</w></l>
					<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">les</w> <w n="2.3">ténèbres</w>. <w n="2.4">Tu</w> <w n="2.5">reviens</w>. <w n="2.6">Je</w> <w n="2.7">vais</w> <w n="2.8">revivre</w>.</l>
					<l n="3" num="1.3"><w n="3.1">C</w>’<w n="3.2">est</w> <w n="3.3">toi</w>. <w n="3.4">Je</w> <w n="3.5">ne</w> <w n="3.6">sais</w> <w n="3.7">plus</w> <w n="3.8">si</w> <w n="3.9">j</w>’<w n="3.10">ai</w> <w n="3.11">pleuré</w>. <w n="3.12">C</w>’<w n="3.13">est</w> <w n="3.14">toi</w>.</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">tous</w> <w n="4.3">les</w> <w n="4.4">rossignols</w> <w n="4.5">gazouillent</w> <w n="4.6">sous</w> <w n="4.7">mon</w> <w n="4.8">toit</w>.</l>
					<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">toi</w>. <w n="5.4">Voici</w> <w n="5.5">un</w> <w n="5.6">an</w> <w n="5.7">que</w> <w n="5.8">tu</w> <w n="5.9">n</w>’<w n="5.10">es</w> <w n="5.11">revenue</w></l>
					<l n="6" num="1.6"><w n="6.1">Dans</w> <w n="6.2">cette</w> <w n="6.3">chambre</w> <w n="6.4">où</w> <w n="6.5">tu</w> <w n="6.6">as</w> <w n="6.7">dansé</w> <w n="6.8">toute</w> <w n="6.9">nue</w></l>
					<l n="7" num="1.7"><w n="7.1">En</w> <w n="7.2">ouvrant</w> <w n="7.3">ton</w> <w n="7.4">ombrelle</w> <w n="7.5">afin</w> <w n="7.6">de</w> <w n="7.7">ne</w> <w n="7.8">pas</w> <w n="7.9">voir</w></l>
					<l n="8" num="1.8"><w n="8.1">Ton</w> <w n="8.2">corps</w> <w n="8.3">souple</w> <w n="8.4">se</w> <w n="8.5">refléter</w> <w n="8.6">dans</w> <w n="8.7">le</w> <w n="8.8">miroir</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Tu</w> <w n="9.2">t</w>’<w n="9.3">en</w> <w n="9.4">allas</w>. <w n="9.5">Le</w> <w n="9.6">soir</w>, <w n="9.7">j</w>’<w n="9.8">alignais</w> <w n="9.9">des</w> <w n="9.10">distiques</w></l>
					<l n="10" num="1.10"><w n="10.1">Mélancoliques</w>. <w n="10.2">Souvenirs</w>, <w n="10.3">vol</w> <w n="10.4">de</w> <w n="10.5">moustiques</w> !</l>
					<l n="11" num="1.11"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">toi</w>. <w n="11.4">Je</w> <w n="11.5">veux</w> <w n="11.6">chanter</w> <w n="11.7">ton</w> <w n="11.8">rire</w> <w n="11.9">et</w> <w n="11.10">février</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">piquer</w> <w n="12.3">un</w> <w n="12.4">lilas</w> <w n="12.5">sur</w> <w n="12.6">le</w> <w n="12.7">calendrier</w>.</l>
				</lg>
			</div></body></text></TEI>