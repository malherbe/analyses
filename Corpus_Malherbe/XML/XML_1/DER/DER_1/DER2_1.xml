<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER2">
				<head type="number">II</head>
				<opener>
					<salute>A Lucien Corpechot.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quelle</w> <w n="1.2">bataille</w> <w n="1.3">se</w> <w n="1.4">livre</w></l>
					<l n="2" num="1.2"><w n="2.1">Sous</w> <w n="2.2">les</w> <w n="2.3">constellations</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">Fatigué</w> <w n="3.2">de</w> <w n="3.3">mon</w> <w n="3.4">cœur</w> <w n="3.5">ivre</w></l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">du</w> <w n="4.3">cri</w> <w n="4.4">des</w> <w n="4.5">passions</w>,</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Lassé</w> <w n="5.2">de</w> <w n="5.3">lutter</w>, <w n="5.4">de</w> <w n="5.5">mordre</w></l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">de</w> <w n="6.3">vaincre</w>, <w n="6.4">j</w>’<w n="6.5">aspirais</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Dans</w> <w n="7.2">l</w>’<w n="7.3">apaisement</w> <w n="7.4">de</w> <w n="7.5">l</w>’<w n="7.6">ordre</w>,</l>
					<l n="8" num="2.4"><w n="8.1">À</w> <w n="8.2">des</w> <w n="8.3">songes</w> <w n="8.4">mesurés</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Sage</w> <w n="9.2">et</w> <w n="9.3">pur</w>, <w n="9.4">oubliant</w> <w n="9.5">celle</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Dont</w> <w n="10.2">la</w> <w n="10.3">chevelure</w> <w n="10.4">ainsi</w></l>
					<l n="11" num="3.3"><w n="11.1">Qu</w>’<w n="11.2">une</w> <w n="11.3">eau</w> <w n="11.4">vivante</w> <w n="11.5">ruisselle</w></l>
					<l n="12" num="3.4"><w n="12.1">Sur</w> <w n="12.2">ma</w> <w n="12.3">joie</w> <w n="12.4">et</w> <w n="12.5">mon</w> <w n="12.6">souci</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Onde</w> <w n="13.2">magnifique</w> <w n="13.3">et</w> <w n="13.4">noire</w></l>
					<l n="14" num="4.2"><w n="14.1">Où</w> <w n="14.2">le</w> <w n="14.3">poète</w> <w n="14.4">noierait</w></l>
					<l n="15" num="4.3"><w n="15.1">Sa</w> <w n="15.2">passion</w> <w n="15.3">de</w> <w n="15.4">la</w> <w n="15.5">gloire</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Son</w> <w n="16.2">espoir</w> <w n="16.3">et</w> <w n="16.4">son</w> <w n="16.5">regret</w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Je</w> <w n="17.2">voulais</w> <w n="17.3">sous</w> <w n="17.4">le</w> <w n="17.5">feuillage</w></l>
					<l n="18" num="5.2"><w n="18.1">De</w> <w n="18.2">ce</w> <w n="18.3">fabuleux</w> <w n="18.4">été</w></l>
					<l n="19" num="5.3"><w n="19.1">Écraser</w> <w n="19.2">sur</w> <w n="19.3">chaque</w> <w n="19.4">page</w></l>
					<l n="20" num="5.4"><w n="20.1">L</w>’<w n="20.2">ombre</w> <w n="20.3">chaude</w> <w n="20.4">et</w> <w n="20.5">la</w> <w n="20.6">clarté</w>,</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Le</w> <w n="21.2">trèfle</w> <w n="21.3">rouge</w> <w n="21.4">qui</w> <w n="21.5">brûle</w>,</l>
					<l n="22" num="6.2"><w n="22.1">L</w>’<w n="22.2">air</w> <w n="22.3">qui</w> <w n="22.4">dort</w>, <w n="22.5">le</w> <w n="22.6">bruit</w> <w n="22.7">des</w> <w n="22.8">eaux</w></l>
					<l n="23" num="6.3"><w n="23.1">Aux</w> <w n="23.2">rameaux</w> <w n="23.3">du</w> <w n="23.4">crépuscule</w></l>
					<l n="24" num="6.4"><w n="24.1">Le</w> <w n="24.2">tumulte</w> <w n="24.3">des</w> <w n="24.4">oiseaux</w>,</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Dans</w> <w n="25.2">les</w> <w n="25.3">ténèbres</w> <w n="25.4">fleuries</w></l>
					<l n="26" num="7.2"><w n="26.1">La</w> <w n="26.2">lune</w>, <w n="26.3">fruit</w> <w n="26.4">d</w>’<w n="26.5">un</w> <w n="26.6">beau</w> <w n="26.7">soir</w></l>
					<l n="27" num="7.3"><w n="27.1">L</w>’<w n="27.2">herbe</w> <w n="27.3">humide</w> <w n="27.4">des</w> <w n="27.5">prairies</w></l>
					<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">l</w>’<w n="28.3">azur</w> <w n="28.4">sonore</w>. — <w n="28.5">Espoir</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">S</w>’<w n="29.2">attaquant</w> <w n="29.3">à</w> <w n="29.4">la</w> <w n="29.5">nature</w></l>
					<l n="30" num="8.2"><w n="30.1">Le</w> <w n="30.2">poète</w> <w n="30.3">la</w> <w n="30.4">pétrit</w></l>
					<l n="31" num="8.3"><w n="31.1">Pour</w> <w n="31.2">en</w> <w n="31.3">faire</w> <w n="31.4">l</w>’<w n="31.5">œuvre</w> <w n="31.6">où</w> <w n="31.7">dure</w></l>
					<l n="32" num="8.4"><w n="32.1">Le</w> <w n="32.2">triomphe</w> <w n="32.3">de</w> <w n="32.4">l</w>’<w n="32.5">esprit</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Mais</w> <w n="33.2">quoi</w> ! <w n="33.3">langoureuse</w> <w n="33.4">celle</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Dont</w> <w n="34.2">la</w> <w n="34.3">chevelure</w> <w n="34.4">ainsi</w></l>
					<l n="35" num="9.3"><w n="35.1">Qu</w>’<w n="35.2">une</w> <w n="35.3">eau</w> <w n="35.4">vivante</w> <w n="35.5">ruisselle</w></l>
					<l n="36" num="9.4"><w n="36.1">Sur</w> <w n="36.2">ma</w> <w n="36.3">joie</w> <w n="36.4">et</w> <w n="36.5">mon</w> <w n="36.6">souci</w>,</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Découvre</w> <w n="37.2">sa</w> <w n="37.3">gorge</w> <w n="37.4">blanche</w></l>
					<l n="38" num="10.2"><w n="38.1">Et</w> <w n="38.2">féconde</w> <w n="38.3">en</w> <w n="38.4">voluptés</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Sourit</w> <w n="39.2">et</w> <w n="39.3">vers</w> <w n="39.4">moi</w> <w n="39.5">se</w> <w n="39.6">penche</w></l>
					<l n="40" num="10.4"><w n="40.1">Dans</w> <w n="40.2">l</w>’<w n="40.3">ombre</w> <w n="40.4">où</w> <w n="40.5">je</w> <w n="40.6">méditais</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Qu</w>’<w n="41.2">est</w>-<w n="41.3">ce</w> ? <w n="41.4">Le</w> <w n="41.5">monde</w> <w n="41.6">chavire</w></l>
					<l n="42" num="11.2"><w n="42.1">Comme</w> <w n="42.2">un</w> <w n="42.3">jeu</w> <w n="42.4">de</w> <w n="42.5">vains</w> <w n="42.6">décors</w> ;</l>
					<l n="43" num="11.3"><w n="43.1">Elle</w> <w n="43.2">est</w> <w n="43.3">belle</w> <w n="43.4">et</w> <w n="43.5">je</w> <w n="43.6">respire</w></l>
					<l n="44" num="11.4"><w n="44.1">L</w>’<w n="44.2">odeur</w> <w n="44.3">lourde</w> <w n="44.4">de</w> <w n="44.5">son</w> <w n="44.6">corps</w>.</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">Vignes</w> <w n="45.2">blanches</w> <w n="45.3">de</w> <w n="45.4">rosée</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Peupliers</w> <w n="46.2">jaunes</w> <w n="46.3">et</w> <w n="46.4">verts</w>,</l>
					<l n="47" num="12.3"><w n="47.1">Sa</w> <w n="47.2">main</w> <w n="47.3">sur</w> <w n="47.4">mes</w> <w n="47.5">yeux</w> <w n="47.6">posée</w></l>
					<l n="48" num="12.4"><w n="48.1">Me</w> <w n="48.2">dérobe</w> <w n="48.3">l</w>’<w n="48.4">univers</w>.</l>
				</lg>
			</div></body></text></TEI>