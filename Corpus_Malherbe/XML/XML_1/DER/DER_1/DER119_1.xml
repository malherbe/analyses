<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" rhyme="none" key="DER119">
				<head type="number">CXIX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">n</w>’<w n="1.3">aimes</w> <w n="1.4">pas</w> <w n="1.5">les</w> <w n="1.6">vers</w>, <w n="1.7">car</w> <w n="1.8">tu</w> <w n="1.9">es</w> <w n="1.10">belle</w> <w n="1.11">et</w> <w n="1.12">dis</w></l>
					<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">il</w> <w n="2.3">faut</w> <w n="2.4">saisir</w> <w n="2.5">le</w> <w n="2.6">temps</w> <w n="2.7">sous</w> <w n="2.8">des</w> <w n="2.9">ongles</w> <w n="2.10">hardis</w></l>
					<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">tenaces</w>, <w n="3.3">le</w> <w n="3.4">déchirer</w>, <w n="3.5">rouge</w> <w n="3.6">grenade</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">mâcher</w> <w n="4.3">et</w> <w n="4.4">jeter</w> <w n="4.5">l</w>’<w n="4.6">écorce</w> <w n="4.7">vaine</w>. <w n="4.8">Une</w> <w n="4.9">ode</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Son</w> <w n="5.2">ampleur</w> <w n="5.3">magnifique</w> <w n="5.4">et</w> <w n="5.5">son</w> <w n="5.6">rythme</w> <w n="5.7">pareil</w></l>
					<l n="6" num="1.6"><w n="6.1">Aux</w> <w n="6.2">respirations</w> <w n="6.3">des</w> <w n="6.4">flots</w> <w n="6.5">sous</w> <w n="6.6">le</w> <w n="6.7">soleil</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Sa</w> <w n="7.2">splendeur</w>, <w n="7.3">son</w> <w n="7.4">tumulte</w> <w n="7.5">et</w> <w n="7.6">ses</w> <w n="7.7">tempêtes</w> <w n="7.8">sourdes</w></l>
					<l n="8" num="1.8"><w n="8.1">Qu</w>’<w n="8.2">importent</w>, <w n="8.3">et</w> <w n="8.4">tout</w> <w n="8.5">l</w>’<w n="8.6">art</w>, <w n="8.7">puisqu</w>’<w n="8.8">il</w> <w n="8.9">faut</w> <w n="8.10">que</w> <w n="8.11">tu</w> <w n="8.12">mordes</w></l>
					<l n="9" num="1.9"><w n="9.1">Ivre</w> <w n="9.2">et</w> <w n="9.3">pour</w> <w n="9.4">en</w> <w n="9.5">jouir</w> <w n="9.6">la</w> <w n="9.7">vie</w> <w n="9.8">à</w> <w n="9.9">pleines</w> <w n="9.10">dents</w> !</l>
					<l n="10" num="1.10">« <w n="10.1">Les</w> <w n="10.2">poètes</w>, <w n="10.3">dis</w>-<w n="10.4">tu</w>, <w n="10.5">qui</w> <w n="10.6">contemplent</w>, <w n="10.7">qui</w> <w n="10.8">dans</w></l>
					<l n="11" num="1.11"><w n="11.1">Le</w> <w n="11.2">secret</w> <w n="11.3">de</w> <w n="11.4">leur</w> <w n="11.5">cœur</w> <w n="11.6">reconstruisent</w> <w n="11.7">le</w> <w n="11.8">monde</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Peignent</w> <w n="12.2">de</w> <w n="12.3">vains</w> <w n="12.4">décors</w> <w n="12.5">sur</w> <w n="12.6">des</w> <w n="12.7">coques</w> <w n="12.8">d</w>’<w n="12.9">amande</w>.</l>
					<l n="13" num="1.13"><w n="13.1">Je</w> <w n="13.2">tressaille</w>, <w n="13.3">je</w> <w n="13.4">plonge</w> <w n="13.5">et</w> <w n="13.6">je</w> <w n="13.7">m</w>’<w n="13.8">évanouis</w></l>
					<l n="14" num="1.14"><w n="14.1">Aux</w> <w n="14.2">durs</w> <w n="14.3">baisers</w> <w n="14.4">du</w> <w n="14.5">fleuve</w>, <w n="14.6">à</w> <w n="14.7">ses</w> <w n="14.8">cris</w> <w n="14.9">inouïs</w>,</l>
					<l n="15" num="1.15"><w n="15.1">L</w>’<w n="15.2">eau</w> <w n="15.3">m</w>’<w n="15.4">emporte</w>, <w n="15.5">me</w> <w n="15.6">bat</w>, <w n="15.7">m</w>’<w n="15.8">enivre</w> <w n="15.9">et</w> <w n="15.10">quand</w> <w n="15.11">j</w>’<w n="15.12">émerge</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Poètes</w>, <w n="16.2">je</w> <w n="16.3">vous</w> <w n="16.4">vois</w> <w n="16.5">qui</w> <w n="16.6">rêvez</w> <w n="16.7">sur</w> <w n="16.8">la</w> <w n="16.9">berge</w>. »</l>
				</lg>
			</div></body></text></TEI>