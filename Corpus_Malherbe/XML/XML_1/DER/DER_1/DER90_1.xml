<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" rhyme="none" key="DER90">
				<head type="number">XC</head>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="8" unit="char"></space><w n="1.1">Une</w> <w n="1.2">pie</w> <w n="1.3">de</w> <w n="1.4">neige</w> <w n="1.5">et</w> <w n="1.6">d</w>’<w n="1.7">ébène</w></l>
					<l n="2" num="1.2"><w n="2.1">Ou</w>, <w n="2.2">si</w> <w n="2.3">l</w>’<w n="2.4">on</w> <w n="2.5">veut</w>, <w n="2.6">de</w> <w n="2.7">craie</w> <w n="2.8">et</w> <w n="2.9">de</w> <w n="2.10">charbon</w>,</l>
					<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">Tandis</w> <w n="3.2">que</w> <w n="3.3">je</w> <w n="3.4">songe</w> <w n="3.5">à</w> <w n="3.6">ma</w> <w n="3.7">peine</w></l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">S</w>’<w n="4.2">envole</w> <w n="4.3">et</w> <w n="4.4">vole</w> <w n="4.5">au</w> <w n="4.6">vert</w> <w n="4.7">vallon</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><space quantity="8" unit="char"></space><w n="5.1">L</w>’<w n="5.2">eau</w> <w n="5.3">fredonne</w> <w n="5.4">sous</w> <w n="5.5">mes</w> <w n="5.6">semelles</w></l>
					<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">je</w> <w n="6.3">m</w>’<w n="6.4">endors</w> <w n="6.5">dans</w> <w n="6.6">l</w>’<w n="6.7">herbe</w> <w n="6.8">du</w> <w n="6.9">talus</w>.</l>
					<l n="7" num="2.3"><space quantity="8" unit="char"></space><w n="7.1">Voici</w> <w n="7.2">des</w> <w n="7.3">jours</w> <w n="7.4">et</w> <w n="7.5">des</w> <w n="7.6">semaines</w></l>
					<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">Que</w> <w n="8.2">pour</w> <w n="8.3">moi</w> <w n="8.4">tu</w> <w n="8.5">ne</w> <w n="8.6">souris</w> <w n="8.7">plus</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><space quantity="8" unit="char"></space><w n="9.1">Tu</w> <w n="9.2">dois</w> <w n="9.3">pleurer</w> <w n="9.4">dans</w> <w n="9.5">cette</w> <w n="9.6">ville</w></l>
					<l n="10" num="3.2"><w n="10.1">Comme</w>, <w n="10.2">la</w> <w n="10.3">nuit</w>, <w n="10.4">je</w> <w n="10.5">pleure</w> <w n="10.6">loin</w> <w n="10.7">de</w> <w n="10.8">loi</w>…</l>
					<l n="11" num="3.3"><space quantity="8" unit="char"></space><w n="11.1">Le</w> <w n="11.2">vent</w> (<w n="11.3">qui</w> <w n="11.4">sait</w> ?) <w n="11.5">souffle</w>, <w n="11.6">et</w> <w n="11.7">ravive</w></l>
					<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">Les</w> <w n="12.2">girouettes</w> <w n="12.3">sur</w> <w n="12.4">le</w> <w n="12.5">toit</w></l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><space quantity="8" unit="char"></space><w n="13.1">Rouge</w> <w n="13.2">de</w> <w n="13.3">cette</w> <w n="13.4">maisonnette</w></l>
					<l n="14" num="4.2"><w n="14.1">En</w> <w n="14.2">fleurs</w> <w n="14.3">où</w> <w n="14.4">nous</w> <w n="14.5">nous</w> <w n="14.6">sommes</w> <w n="14.7">tant</w> <w n="14.8">aimés</w> ;</l>
					<l n="15" num="4.3"><space quantity="8" unit="char"></space><w n="15.1">Mais</w> <w n="15.2">les</w> <w n="15.3">roses</w> <w n="15.4">de</w> <w n="15.5">la</w> <w n="15.6">tonnelle</w></l>
					<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">Refleuriront</w>-<w n="16.2">elles</w> <w n="16.3">jamais</w></l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><space quantity="8" unit="char"></space><w n="17.1">Pour</w> <w n="17.2">moi</w> <w n="17.3">qui</w>, <w n="17.4">près</w> <w n="17.5">de</w> <w n="17.6">la</w> <w n="17.7">rivière</w></l>
					<l n="18" num="5.2"><w n="18.1">Que</w> <w n="18.2">frôle</w> <w n="18.3">et</w> <w n="18.4">griffe</w> <w n="18.5">un</w> <w n="18.6">bleu</w> <w n="18.7">martin</w>-<w n="18.8">pêcheur</w>,</l>
					<l n="19" num="5.3"><space quantity="8" unit="char"></space><w n="19.1">En</w> <w n="19.2">attendant</w> <w n="19.3">que</w> <w n="19.4">l</w>’<w n="19.5">oubli</w> <w n="19.6">vienne</w></l>
					<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1">Avec</w> <w n="20.2">son</w> <w n="20.3">calme</w> <w n="20.4">et</w> <w n="20.5">sa</w> <w n="20.6">fraîcheur</w></l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><space quantity="8" unit="char"></space><w n="21.1">Compose</w> <w n="21.2">un</w> <w n="21.3">poème</w> <w n="21.4">inutile</w></l>
					<l n="22" num="6.2"><w n="22.1">En</w> <w n="22.2">écoutant</w> <w n="22.3">les</w> <w n="22.4">blancs</w> <w n="22.5">et</w> <w n="22.6">bleus</w> <w n="22.7">remous</w></l>
					<l n="23" num="6.3"><space quantity="8" unit="char"></space><w n="23.1">De</w> <w n="23.2">l</w>’<w n="23.3">eau</w> <w n="23.4">qui</w> <w n="23.5">chante</w> <w n="23.6">sous</w> <w n="23.7">la</w> <w n="23.8">digue</w></l>
					<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">Et</w> <w n="24.2">qui</w> <w n="24.3">caresse</w> <w n="24.4">les</w> <w n="24.5">cailloux</w>.</l>
				</lg>
			</div></body></text></TEI>