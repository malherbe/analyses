<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER125">
				<head type="number">CXXV</head>
				<opener>
					<salute>A Lucien Dubech.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="12" unit="char"></space><w n="1.1">L</w>’<w n="1.2">acacia</w> <w n="1.3">blanc</w> <w n="1.4">sur</w> <w n="1.5">la</w> <w n="1.6">berge</w></l>
					<l n="2" num="1.2"><space quantity="20" unit="char"></space><w n="2.1">Remue</w> <w n="2.2">au</w> <w n="2.3">vent</w> <w n="2.4">du</w> <w n="2.5">soir</w> ;</l>
					<l n="3" num="1.3"><space quantity="12" unit="char"></space><w n="3.1">Les</w> <w n="3.2">rouliers</w> <w n="3.3">boivent</w> <w n="3.4">du</w> <w n="3.5">vin</w> <w n="3.6">noir</w></l>
					<l n="4" num="1.4"><w n="4.1">Sous</w> <w n="4.2">la</w> <w n="4.3">glycine</w> <w n="4.4">bleue</w> <w n="4.5">et</w> <w n="4.6">fraîche</w> <w n="4.7">de</w> <w n="4.8">l</w>’<w n="4.9">auberge</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><space quantity="12" unit="char"></space><w n="5.1">Mes</w> <w n="5.2">beaux</w> <w n="5.3">rêves</w> <w n="5.4">s</w>’<w n="5.5">en</w> <w n="5.6">sont</w> <w n="5.7">allés</w>,</l>
					<l n="6" num="2.2"><space quantity="20" unit="char"></space><w n="6.1">Rouliers</w>, <w n="6.2">dans</w> <w n="6.3">vos</w> <w n="6.4">charrettes</w> ;</l>
					<l n="7" num="2.3"><space quantity="12" unit="char"></space><w n="7.1">Mon</w> <w n="7.2">cœur</w> <w n="7.3">plein</w> <w n="7.4">de</w> <w n="7.5">larmes</w> <w n="7.6">secrètes</w></l>
					<l n="8" num="2.4"><w n="8.1">Songe</w> <w n="8.2">à</w> <w n="8.3">des</w> <w n="8.4">rosiers</w> <w n="8.5">verts</w> <w n="8.6">que</w> <w n="8.7">la</w> <w n="8.8">foudre</w> <w n="8.9">a</w> <w n="8.10">brûlés</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><space quantity="8" unit="char"></space><w n="9.1">Pourquoi</w> <w n="9.2">faut</w>-<w n="9.3">il</w> <w n="9.4">qu</w>’<w n="9.5">à</w> <w n="9.6">vos</w> <w n="9.7">voix</w> <w n="9.8">dures</w></l>
					<l n="10" num="3.2"><space quantity="20" unit="char"></space><w n="10.1">Renaissent</w> <w n="10.2">mes</w> <w n="10.3">beaux</w> <w n="10.4">jours</w></l>
					<l n="11" num="3.3"><space quantity="12" unit="char"></space><w n="11.1">Et</w> <w n="11.2">ma</w> <w n="11.3">jeunesse</w> <w n="11.4">et</w> <w n="11.5">mes</w> <w n="11.6">amours</w></l>
					<l n="12" num="3.4"><w n="12.1">Avec</w> <w n="12.2">tous</w> <w n="12.3">les</w> <w n="12.4">oiseaux</w> <w n="12.5">et</w> <w n="12.6">toutes</w> <w n="12.7">les</w> <w n="12.8">verdures</w>,</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><space quantity="12" unit="char"></space><w n="13.1">Alors</w> <w n="13.2">qu</w>’<w n="13.3">un</w> <w n="13.4">âpre</w> <w n="13.5">désespoir</w></l>
					<l n="14" num="4.2"><space quantity="20" unit="char"></space><w n="14.1">Casse</w> <w n="14.2">toutes</w> <w n="14.3">les</w> <w n="14.4">branches</w></l>
					<l n="15" num="4.3"><space quantity="12" unit="char"></space><w n="15.1">Et</w> <w n="15.2">que</w> <w n="15.3">la</w> <w n="15.4">berge</w> <w n="15.5">et</w> <w n="15.6">l</w>’<w n="15.7">eau</w> <w n="15.8">sont</w> <w n="15.9">blanches</w></l>
					<l n="16" num="4.4"><w n="16.1">Acacia</w>, <w n="16.2">des</w> <w n="16.3">fleurs</w> <w n="16.4">que</w> <w n="16.5">t</w>’<w n="16.6">arrache</w> <w n="16.7">le</w> <w n="16.8">soir</w> ?</l>
				</lg>
			</div></body></text></TEI>