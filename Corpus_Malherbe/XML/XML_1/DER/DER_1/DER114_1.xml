<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER114">
				<head type="number">CXIV</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Pour</w> <w n="1.2">goûter</w> <w n="1.3">au</w> <w n="1.4">charme</w> <w n="1.5">unique</w></l>
					<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">jaillit</w> <w n="2.3">de</w> <w n="2.4">ton</w> <w n="2.5">baiser</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Diogène</w> <w n="3.2">le</w> <w n="3.3">Cynique</w></l>
					<l n="4" num="1.4"><w n="4.1">Courrait</w> <w n="4.2">se</w> <w n="4.3">faire</w> <w n="4.4">raser</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">S</w>’<w n="5.2">ils</w> <w n="5.3">savaient</w> <w n="5.4">ton</w> <w n="5.5">regard</w>, <w n="5.6">ivres</w></l>
					<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">sages</w> <w n="6.3">silencieux</w></l>
					<l n="7" num="2.3"><w n="7.1">Verraient</w> <w n="7.2">s</w>’<w n="7.3">ouvrir</w> <w n="7.4">dans</w> <w n="7.5">leurs</w> <w n="7.6">livres</w></l>
					<l n="8" num="2.4"><w n="8.1">Des</w> <w n="8.2">pivoines</w> <w n="8.3">et</w> <w n="8.4">des</w> <w n="8.5">yeux</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Et</w>, <w n="9.2">mordus</w> <w n="9.3">de</w> <w n="9.4">quelles</w> <w n="9.5">fièvres</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Secouant</w> <w n="10.2">toge</w> <w n="10.3">ou</w> <w n="10.4">veston</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">sautant</w> <w n="11.3">comme</w> <w n="11.4">des</w> <w n="11.5">lièvres</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Ils</w> <w n="12.2">crieraient</w> : <w n="12.3">Où</w> <w n="12.4">la</w> <w n="12.5">voit</w>-<w n="12.6">on</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Dans</w> <w n="13.2">leurs</w> <w n="13.3">veines</w> : <w n="13.4">escarboucles</w></l>
					<l n="14" num="4.2"><w n="14.1">Liquides</w> <w n="14.2">et</w> <w n="14.3">plomb</w> <w n="14.4">fondu</w>,</l>
					<l n="15" num="4.3"><w n="15.1">Tu</w> <w n="15.2">les</w> <w n="15.3">verrais</w> <w n="15.4">pour</w> <w n="15.5">tes</w> <w n="15.6">boucles</w></l>
					<l n="16" num="4.4"><w n="16.1">Poussant</w> <w n="16.2">un</w> <w n="16.3">cri</w> <w n="16.4">éperdu</w>,</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Tous</w>, <w n="17.2">contemporains</w> <w n="17.3">d</w>’<w n="17.4">Ulysse</w></l>
					<l n="18" num="5.2"><w n="18.1">Et</w> <w n="18.2">disciples</w> <w n="18.3">de</w> <w n="18.4">Bergson</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Se</w> <w n="19.2">rouler</w> <w n="19.3">nus</w>, <w n="19.4">ô</w> <w n="19.5">délice</w> !</l>
					<l n="20" num="5.4"><w n="20.1">Sur</w> <w n="20.2">des</w> <w n="20.3">peaux</w> <w n="20.4">de</w> <w n="20.5">hérisson</w></l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Car</w> <w n="21.2">pour</w> <w n="21.3">mordre</w> <w n="21.4">à</w> <w n="21.5">la</w> <w n="21.6">grenade</w></l>
					<l n="22" num="6.2"><w n="22.1">Rouge</w> <w n="22.2">des</w> <w n="22.3">désirs</w> <w n="22.4">ouverts</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Ils</w> <w n="23.2">oublieraient</w> <w n="23.3">la</w> <w n="23.4">monade</w>.</l>
					<l n="24" num="6.4"><w n="24.1">Le</w> <w n="24.2">noumène</w> <w n="24.3">et</w> <w n="24.4">l</w>’<w n="24.5">Univers</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Ah</w> ! <w n="25.2">Vivons</w> ! <w n="25.3">La</w> <w n="25.4">page</w> <w n="25.5">écrite</w></l>
					<l n="26" num="7.2"><w n="26.1">Ne</w> <w n="26.2">vaut</w> <w n="26.3">pas</w> <w n="26.4">les</w> <w n="26.5">lèvres</w> <w n="26.6">qu</w>’<w n="26.7">on</w></l>
					<l n="27" num="7.3"><w n="27.1">Mord</w> ! <w n="27.2">Vers</w> <w n="27.3">l</w>’<w n="27.4">amour</w> ! <w n="27.5">Démocrite</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Spinoza</w>, <w n="28.2">Hegel</w>, <w n="28.3">Bacon</w>,</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><subst hand="RR" reason="analysis" type="phonemization"><del>M</del><add rend="hidden"><w n="29.1">Monsieur</w></add></subst>. <w n="29.2">Durkheim</w>, <w n="29.3">Xénophane</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Vers</w> <w n="30.2">l</w>’<w n="30.3">amour</w>, <w n="30.4">vous</w> <w n="30.5">chanteriez</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Sans</w> <w n="31.2">archet</w> <w n="31.3">ni</w> <w n="31.4">colophane</w></l>
					<l n="32" num="8.4"><w n="32.1">Violons</w> <w n="32.2">extasiés</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">Ah</w> ! <w n="33.2">que</w> <w n="33.3">pèsent</w> <w n="33.4">hypothèses</w>,</l>
					<l n="34" num="9.2"><w n="34.1">Postulats</w>, <w n="34.2">systèmes</w>, <w n="34.3">lois</w> ?</l>
					<l n="35" num="9.3"><w n="35.1">Ne</w> <w n="35.2">faut</w>-<w n="35.3">il</w> <w n="35.4">que</w> <w n="35.5">tu</w> <w n="35.6">te</w> <w n="35.7">taises</w></l>
					<l n="36" num="9.4"><w n="36.1">Ou</w> <w n="36.2">que</w> <w n="36.3">tu</w> <w n="36.4">joignes</w> <w n="36.5">ta</w> <w n="36.6">voix</w></l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Au</w> <w n="37.2">grand</w> <w n="37.3">tumulte</w> <w n="37.4">des</w> <w n="37.5">choses</w>,</l>
					<l n="38" num="10.2"><w n="38.1">À</w> <w n="38.2">l</w>’<w n="38.3">amour</w> <w n="38.4">qui</w> <w n="38.5">fait</w> <w n="38.6">ployer</w></l>
					<l n="39" num="10.3"><w n="39.1">Les</w> <w n="39.2">cigognes</w> <w n="39.3">et</w> <w n="39.4">les</w> <w n="39.5">roses</w>,</l>
					<l n="40" num="10.4"><w n="40.1">La</w> <w n="40.2">tulipe</w> <w n="40.3">et</w> <w n="40.4">l</w>’<w n="40.5">épervier</w> ?</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Viens</w>, <w n="41.2">et</w> <w n="41.3">contre</w> <w n="41.4">moi</w> <w n="41.5">pressée</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Aux</w> <w n="42.2">brutes</w> <w n="42.3">fais</w>-<w n="42.4">moi</w> <w n="42.5">pareil</w></l>
					<l n="43" num="11.3"><w n="43.1">En</w> <w n="43.2">écrasant</w> <w n="43.3">ma</w> <w n="43.4">pensée</w></l>
					<l n="44" num="11.4"><w n="44.1">Sous</w> <w n="44.2">la</w> <w n="44.3">grâce</w> <w n="44.4">d</w>’<w n="44.5">un</w> <w n="44.6">orteil</w>.</l>
				</lg>
			</div></body></text></TEI>