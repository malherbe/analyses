<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LA VERDURE DORÉE</title>
				<title type="medium">Édition électronique</title>
				<author key="DER">
					<name>
						<forename>Tristan</forename>
						<surname>DERÈME</surname>
					</name>
					<date from="1889" to="1941">1889-1941</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2644 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2013">2013</date>
				<idno type="local">DER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://poesies.net/tristanderemelaverduredoree.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title type="main">LA VERDURE DORÉE</title>
						<author>Tristan Derème</author>
						<imprint>
							<pubPlace>Paris</pubPlace>
							<publisher>ÉDITIONS ÉMILE-PAUL FRÈRES</publisher>
							<date when="1908">1908</date>
						</imprint>
					</monogr>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1922">1922</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les poèmes en prose ne sont pas inclus.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique
					après application du programme de calcul de la longueur métrique des vers).</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DER6">
				<head type="number">VI</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">Passé</w> <w n="1.3">maugréait</w> <w n="1.4">et</w> <w n="1.5">frappait</w> <w n="1.6">à</w> <w n="1.7">la</w> <w n="1.8">porte</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">me</w> <w n="2.3">taisais</w>. <w n="2.4">Il</w> <w n="2.5">m</w>’<w n="2.6">appela</w> <w n="2.7">d</w>’<w n="2.8">une</w> <w n="2.9">voix</w> <w n="2.10">forte</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">je</w> <w n="3.3">continuai</w> <w n="3.4">de</w> <w n="3.5">songer</w> <w n="3.6">à</w> <w n="3.7">tes</w> <w n="3.8">yeux</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">j</w>’<w n="4.3">entendais</w> <w n="4.4">crier</w> <w n="4.5">le</w> <w n="4.6">vieillard</w> <w n="4.7">furieux</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Grelottant</w> <w n="5.2">dans</w> <w n="5.3">la</w> <w n="5.4">nuit</w> <w n="5.5">sous</w> <w n="5.6">sa</w> <w n="5.7">mante</w> <w n="5.8">à</w> <w n="5.9">ramages</w>,</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Il</w> <w n="6.2">est</w> <w n="6.3">entré</w> <w n="6.4">portant</w> <w n="6.5">un</w> <w n="6.6">vieux</w> <w n="6.7">livre</w> <w n="6.8">d</w>’<w n="6.9">images</w>.</l>
				</lg>
				<lg n="3">
					<l n="7" num="3.1"><w n="7.1">Laure</w>, <w n="7.2">dans</w> <w n="7.3">la</w> <w n="7.4">maison</w> <w n="7.5">à</w> <w n="7.6">l</w>’<w n="7.7">ombre</w> <w n="7.8">des</w> <w n="7.9">sureaux</w>,</l>
					<l n="8" num="3.2"><w n="8.1">Songeuse</w>, <w n="8.2">tu</w> <w n="8.3">brodais</w> <w n="8.4">derrière</w> <w n="8.5">les</w> <w n="8.6">carreaux</w>,</l>
					<l n="9" num="3.3"><w n="9.1">Et</w>, <w n="9.2">si</w> <w n="9.3">j</w>’<w n="9.4">apercevais</w> <w n="9.5">un</w> <w n="9.6">livre</w> <w n="9.7">à</w> <w n="9.8">ta</w> <w n="9.9">fenêtre</w>,</l>
					<l n="10" num="3.4"><w n="10.1">Je</w> <w n="10.2">sonnais</w> <w n="10.3">à</w> <w n="10.4">la</w> <w n="10.5">grille</w> <w n="10.6">et</w> <w n="10.7">tu</w> <w n="10.8">voyais</w> <w n="10.9">paraître</w>,</l>
					<l n="11" num="3.5"><w n="11.1">Au</w> <w n="11.2">jardin</w> <w n="11.3">envahi</w> <w n="11.4">d</w>’<w n="11.5">herbe</w> <w n="11.6">et</w> <w n="11.7">de</w> <w n="11.8">serpolet</w>,</l>
					<l n="12" num="3.6"><w n="12.1">Celui</w> <w n="12.2">qui</w> <w n="12.3">dans</w> <w n="12.4">les</w> <w n="12.5">soirs</w> <w n="12.6">longuement</w> <w n="12.7">te</w> <w n="12.8">parlait</w></l>
					<l n="13" num="3.7"><w n="13.1">Et</w> <w n="13.2">déroulait</w> <w n="13.3">son</w> <w n="13.4">rêve</w> <w n="13.5">ainsi</w> <w n="13.6">qu</w>’<w n="13.7">un</w> <w n="13.8">paysage</w>…</l>
					<l n="14" num="3.8"><w n="14.1">Laure</w>, <w n="14.2">où</w> <w n="14.3">sont</w> <w n="14.4">tes</w> <w n="14.5">cheveux</w>, <w n="14.6">tes</w> <w n="14.7">mains</w> <w n="14.8">et</w> <w n="14.9">ton</w> <w n="14.10">visage</w> ?…</l>
				</lg>
				<lg n="4">
					<l n="15" num="4.1"><w n="15.1">Vous</w> <w n="15.2">qui</w> <w n="15.3">pleuriez</w>, <w n="15.4">mélancolique</w>, <w n="15.5">au</w> <w n="15.6">soir</w> <w n="15.7">tombant</w> ;</l>
					<l n="16" num="4.2"><w n="16.1">Toi</w> <w n="16.2">qui</w> <w n="16.3">sur</w> <w n="16.4">ton</w> <w n="16.5">épaule</w> <w n="16.6">attachais</w> <w n="16.7">un</w> <w n="16.8">ruban</w></l>
					<l n="17" num="4.3"><w n="17.1">Mauve</w> ; <w n="17.2">toi</w> <w n="17.3">qui</w> <w n="17.4">jouais</w> <w n="17.5">Manon</w> <w n="17.6">et</w> <w n="17.7">l</w>’<w n="17.8">ouverture</w></l>
					<l n="18" num="4.4"><w n="18.1">De</w> <hi rend="ital"><w n="18.2">Tannhäuser</w></hi> ; <w n="18.3">toi</w> <w n="18.4">qui</w> <w n="18.5">riais</w> <w n="18.6">dans</w> <w n="18.7">ta</w> <w n="18.8">voiture</w>…</l>
					<l n="19" num="4.5"><w n="19.1">Ô</w> <w n="19.2">passé</w>, <w n="19.3">plein</w> <w n="19.4">de</w> <w n="19.5">fleurs</w> <w n="19.6">et</w> <w n="19.7">de</w> <w n="19.8">chardonnerets</w> !</l>
					<l n="20" num="4.6"><w n="20.1">Rires</w> ! <w n="20.2">Passé</w> <w n="20.3">léger</w> ! <w n="20.4">Passé</w> <w n="20.5">tendre</w> ! <w n="20.6">Regrets</w> !</l>
					<l n="21" num="4.7"><w n="21.1">Mésanges</w>, <w n="21.2">accourez</w>, <w n="21.3">mes</w> <w n="21.4">lointaines</w> <w n="21.5">pensées</w> !</l>
					<l n="22" num="4.8"><w n="22.1">Ô</w> <w n="22.2">souvenirs</w>, <w n="22.3">rameaux</w> <w n="22.4">flétris</w>, <w n="22.5">branches</w> <w n="22.6">cassées</w>…</l>
				</lg>
				<lg n="5">
					<l n="23" num="5.1"><w n="23.1">Oui</w>, <w n="23.2">j</w>’<w n="23.3">aurais</w> <w n="23.4">dû</w>, <w n="23.5">ce</w> <w n="23.6">soir</w>, <w n="23.7">te</w> <w n="23.8">dire</w> <w n="23.9">tout</w> <w n="23.10">cela</w>,</l>
					<l n="24" num="5.2"><w n="24.1">T</w>’<w n="24.2">avouer</w> <w n="24.3">les</w> <w n="24.4">penchants</w> <w n="24.5">où</w> <w n="24.6">mon</w> <w n="24.7">cœur</w> <w n="24.8">s</w>’<w n="24.9">écoula</w></l>
					<l n="25" num="5.3"><w n="25.1">Et</w> <w n="25.2">te</w> <w n="25.3">montrer</w> <w n="25.4">au</w> <w n="25.5">loin</w> <w n="25.6">ces</w> <w n="25.7">figures</w> <w n="25.8">d</w>’<w n="25.9">argile</w>,</l>
					<l n="26" num="5.4"><w n="26.1">Et</w> <w n="26.2">nous</w> <w n="26.3">aurions</w> <w n="26.4">pleuré</w> <w n="26.5">de</w> <w n="26.6">sentir</w> <w n="26.7">si</w> <w n="26.8">fragile</w></l>
					<l n="27" num="5.5"><w n="27.1">Notre</w> <w n="27.2">amour</w> <w n="27.3">qui</w> <w n="27.4">s</w>’<w n="27.5">éveille</w> <w n="27.6">et</w> <w n="27.7">frissonne</w> <w n="27.8">au</w> <w n="27.9">soleil</w></l>
					<l n="28" num="5.6"><w n="28.1">D</w>’<w n="28.2">automne</w>, <w n="28.3">notre</w> <w n="28.4">amour</w> <w n="28.5">incassable</w> <w n="28.6">et</w> <w n="28.7">pareil</w></l>
					<l n="29" num="5.7"><w n="29.1">Aux</w> <w n="29.2">beaux</w> <w n="29.3">jouets</w> <w n="29.4">de</w> <w n="29.5">notre</w> <w n="29.6">enfance</w>. <w n="29.7">Mais</w> <w n="29.8">qu</w>’<w n="29.9">importe</w>,</l>
					<l n="30" num="5.8"><w n="30.1">Si</w> <w n="30.2">l</w>’<w n="30.3">espérance</w> <w n="30.4">encore</w> <w n="30.5">ouvre</w> <w n="30.6">la</w> <w n="30.7">vieille</w> <w n="30.8">porte</w> ?</l>
					<l n="31" num="5.9"><w n="31.1">Elle</w> <w n="31.2">parle</w> ; <w n="31.3">sa</w> <w n="31.4">voix</w> <w n="31.5">illumine</w> <w n="31.6">tes</w> <w n="31.7">yeux</w> ;</l>
					<l n="32" num="5.10"><w n="32.1">Son</w> <w n="32.2">regard</w> <w n="32.3">verse</w> <w n="32.4">en</w> <w n="32.5">nous</w> <w n="32.6">la</w> <w n="32.7">lumière</w> <w n="32.8">des</w> <w n="32.9">cieux</w>.</l>
					<l n="33" num="5.11"><w n="33.1">Sous</w> <w n="33.2">le</w> <w n="33.3">manteau</w> <w n="33.4">de</w> <w n="33.5">pourpre</w> <w n="33.6">et</w> <w n="33.7">la</w> <w n="33.8">cuirasse</w> <w n="33.9">triple</w>,</l>
					<l n="34" num="5.12"><w n="34.1">Cheveux</w> <w n="34.2">au</w> <w n="34.3">vent</w>, <w n="34.4">partons</w> <w n="34.5">pour</w> <w n="34.6">le</w> <w n="34.7">vaste</w> <w n="34.8">périple</w>.</l>
					<l n="35" num="5.13"><w n="35.1">Les</w> <w n="35.2">merles</w> <w n="35.3">se</w> <w n="35.4">sont</w> <w n="35.5">tus</w> <w n="35.6">devant</w> <w n="35.7">l</w>’<w n="35.8">astre</w> <w n="35.9">éclatant</w> ;</l>
					<l n="36" num="5.14"><w n="36.1">Et</w> <w n="36.2">le</w> <w n="36.3">navire</w> <w n="36.4">aux</w> <w n="36.5">voiles</w> <w n="36.6">blanches</w> <w n="36.7">nous</w> <w n="36.8">attend</w></l>
					<l n="37" num="5.15"><w n="37.1">Au</w> <w n="37.2">port</w>, <w n="37.3">prêt</w> <w n="37.4">à</w> <w n="37.5">cingler</w> <w n="37.6">vers</w> <w n="37.7">les</w> <w n="37.8">îles</w> <w n="37.9">lointaines</w></l>
					<l n="38" num="5.16"><w n="38.1">Où</w> <w n="38.2">le</w> <w n="38.3">bonheur</w> <w n="38.4">fleurit</w> <w n="38.5">aux</w> <w n="38.6">rives</w> <w n="38.7">des</w> <w n="38.8">fontaines</w>.</l>
					<l n="39" num="5.17"><w n="39.1">Je</w> <w n="39.2">ne</w> <w n="39.3">sais</w> <w n="39.4">quelle</w> <w n="39.5">main</w> <w n="39.6">nous</w> <w n="39.7">pousse</w>. <w n="39.8">Nous</w> <w n="39.9">rirons</w></l>
					<l n="40" num="5.18"><w n="40.1">Des</w> <w n="40.2">rafales</w> <w n="40.3">soufflant</w> <w n="40.4">dans</w> <w n="40.5">leurs</w> <w n="40.6">rauques</w> <w n="40.7">clairons</w> ;</l>
					<l n="41" num="5.19"><w n="41.1">Et</w>, <w n="41.2">comme</w> <w n="41.3">ivres</w>, <w n="41.4">car</w> <w n="41.5">l</w>’<w n="41.6">Univers</w> <w n="41.7">nous</w> <w n="41.8">est</w> <w n="41.9">complice</w>,</l>
					<l n="42" num="5.20"><w n="42.1">Les</w> <w n="42.2">flots</w> <w n="42.3">noirs</w> <w n="42.4">et</w> <w n="42.5">cabrés</w> <w n="42.6">nous</w> <w n="42.7">seront</w> <w n="42.8">un</w> <w n="42.9">délice</w>.</l>
				</lg>
				<lg n="6">
					<l n="43" num="6.1"><w n="43.1">Ainsi</w> <w n="43.2">nous</w> <w n="43.3">voguerons</w> <w n="43.4">sur</w> <w n="43.5">l</w>’<w n="43.6">eau</w> <w n="43.7">cruelle</w> <w n="43.8">ou</w> <w n="43.9">sur</w></l>
					<l n="44" num="6.2"><w n="44.1">L</w>’<w n="44.2">eau</w> <w n="44.3">calme</w>, <w n="44.4">sous</w> <w n="44.5">tes</w> <w n="44.6">coups</w>, <w n="44.7">tonnerre</w>, <w n="44.8">ou</w> <w n="44.9">sous</w> <w n="44.10">l</w>’<w n="44.11">azur</w>,</l>
					<l n="45" num="6.3"><w n="45.1">Sous</w> <w n="45.2">la</w> <w n="45.3">lune</w> <w n="45.4">indulgente</w> <w n="45.5">ou</w> <w n="45.6">dans</w> <w n="45.7">l</w>’<w n="45.8">ombre</w> <w n="45.9">sauvage</w>.</l>
					<l n="46" num="6.4"><w n="46.1">Et</w> <w n="46.2">plus</w> <w n="46.3">tard</w> <w n="46.4">n</w>’<w n="46.5">ayant</w> <w n="46.6">vu</w> <w n="46.7">briller</w> <w n="46.8">aucun</w> <w n="46.9">rivage</w>,</l>
					<l n="47" num="6.5"><w n="47.1">Revenus</w>, <w n="47.2">mais</w> <w n="47.3">encor</w>, <w n="47.4">les</w> <w n="47.5">doigts</w> <w n="47.6">ensanglantés</w>,</l>
					<l n="48" num="6.6"><w n="48.1">Rêvant</w> <w n="48.2">que</w> <w n="48.3">sur</w> <w n="48.4">la</w> <w n="48.5">mer</w> <w n="48.6">âpre</w> <w n="48.7">des</w> <w n="48.8">voluptés</w></l>
					<l n="49" num="6.7"><w n="49.1">Il</w> <w n="49.2">est</w> <w n="49.3">pourtant</w> <w n="49.4">après</w> <w n="49.5">les</w> <w n="49.6">tempêtes</w> <w n="49.7">quelque</w> <w n="49.8">île</w></l>
					<l n="50" num="6.8"><w n="50.1">Où</w> <w n="50.2">boire</w> <w n="50.3">le</w> <w n="50.4">bonheur</w> <w n="50.5">d</w>’<w n="50.6">une</w> <w n="50.7">âme</w> <w n="50.8">enfin</w> <w n="50.9">tranquille</w>,</l>
					<l n="51" num="6.9"><w n="51.1">Fourbus</w>, <w n="51.2">endoloris</w>, <w n="51.3">meurtris</w>, <w n="51.4">nous</w> <w n="51.5">changerons</w></l>
					<l n="52" num="6.10"><w n="52.1">La</w> <w n="52.2">voile</w> <w n="52.3">blanche</w> <w n="52.4">ou</w> <w n="52.5">nous</w> <w n="52.6">prendrons</w> <w n="52.7">les</w> <w n="52.8">avirons</w>,</l>
					<l n="53" num="6.11"><w n="53.1">Sur</w> <w n="53.2">l</w>’<w n="53.3">eau</w> <w n="53.4">vaine</w> <w n="53.5">luttant</w>, <w n="53.6">mangeant</w> <w n="53.7">notre</w> <w n="53.8">colère</w>,</l>
					<l n="54" num="6.12"><w n="54.1">Pauvres</w> <w n="54.2">rameurs</w> <w n="54.3">perdus</w> <w n="54.4">sur</w> <w n="54.5">la</w> <w n="54.6">vieille</w> <w n="54.7">galère</w>.</l>
				</lg>
			</div></body></text></TEI>