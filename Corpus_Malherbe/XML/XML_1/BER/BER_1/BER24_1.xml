<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER24">
				<head type="main">LES GAULOIS ET LES FRANCS</head>
				<opener>
					<dateline>
						<date when="1814">Janvier 1814</date>
					</dateline>
				</opener>
				<head type="tune">AIR : Gai ! gai ! marions-nous</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1">Gai</w> ! <w n="1.2">Gai</w> ! <w n="1.3">Serrons</w> <w n="1.4">nos</w> <w n="1.5">rangs</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Espérance</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">De</w> <w n="3.2">la</w> <w n="3.3">France</w> ;</l>
					<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">Gai</w> ! <w n="4.2">Gai</w> ! <w n="4.3">Serrons</w> <w n="4.4">nos</w> <w n="4.5">rangs</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">En</w> <w n="5.2">avant</w>, <w n="5.3">gaulois</w> <w n="5.4">et</w> <w n="5.5">francs</w> !</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">D</w>’<w n="6.2">Attila</w> <w n="6.3">suivant</w> <w n="6.4">la</w> <w n="6.5">voix</w>,</l>
					<l n="7" num="2.2"><space unit="char" quantity="8"></space><w n="7.1">Le</w> <w n="7.2">barbare</w></l>
					<l n="8" num="2.3"><space unit="char" quantity="8"></space><w n="8.1">Qu</w>’<w n="8.2">elle</w> <w n="8.3">égare</w></l>
					<l n="9" num="2.4"><w n="9.1">Vient</w> <w n="9.2">une</w> <w n="9.3">seconde</w> <w n="9.4">fois</w></l>
					<l n="10" num="2.5"><w n="10.1">Périr</w> <w n="10.2">dans</w> <w n="10.3">les</w> <w n="10.4">champs</w> <w n="10.5">gaulois</w>.</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><space unit="char" quantity="2"></space><w n="11.1">Gai</w> ! <w n="11.2">Gai</w> ! <w n="11.3">Serrons</w> <w n="11.4">nos</w> <w n="11.5">rangs</w>,</l>
					<l n="12" num="3.2"><space unit="char" quantity="8"></space><w n="12.1">Espérance</w></l>
					<l n="13" num="3.3"><space unit="char" quantity="8"></space><w n="13.1">De</w> <w n="13.2">la</w> <w n="13.3">France</w> ;</l>
					<l n="14" num="3.4"><space unit="char" quantity="2"></space><w n="14.1">Gai</w> ! <w n="14.2">Gai</w> ! <w n="14.3">Serrons</w> <w n="14.4">nos</w> <w n="14.5">rangs</w> ;</l>
					<l n="15" num="3.5"><w n="15.1">En</w> <w n="15.2">avant</w>, <w n="15.3">gaulois</w> <w n="15.4">et</w> <w n="15.5">francs</w> !</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">Renonçant</w> <w n="16.2">à</w> <w n="16.3">ses</w> <w n="16.4">marais</w>,</l>
					<l n="17" num="4.2"><space unit="char" quantity="8"></space><w n="17.1">Le</w> <w n="17.2">cosaque</w></l>
					<l n="18" num="4.3"><space unit="char" quantity="8"></space><w n="18.1">Qui</w> <w n="18.2">bivouaque</w>,</l>
					<l n="19" num="4.4"><w n="19.1">Croit</w>, <w n="19.2">sur</w> <w n="19.3">la</w> <w n="19.4">foi</w> <w n="19.5">des</w> <w n="19.6">anglais</w>,</l>
					<l n="20" num="4.5"><w n="20.1">Se</w> <w n="20.2">loger</w> <w n="20.3">dans</w> <w n="20.4">nos</w> <w n="20.5">palais</w>.</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><space unit="char" quantity="2"></space><w n="21.1">Gai</w> ! <w n="21.2">Gai</w> ! <w n="21.3">Serrons</w> <w n="21.4">nos</w> <w n="21.5">rangs</w>,</l>
					<l n="22" num="5.2"><space unit="char" quantity="8"></space><w n="22.1">Espérance</w></l>
					<l n="23" num="5.3"><space unit="char" quantity="8"></space><w n="23.1">De</w> <w n="23.2">la</w> <w n="23.3">France</w> ;</l>
					<l n="24" num="5.4"><space unit="char" quantity="2"></space><w n="24.1">Gai</w> ! <w n="24.2">Gai</w> ! <w n="24.3">Serrons</w> <w n="24.4">nos</w> <w n="24.5">rangs</w> ;</l>
					<l n="25" num="5.5"><w n="25.1">En</w> <w n="25.2">avant</w>, <w n="25.3">gaulois</w> <w n="25.4">et</w> <w n="25.5">francs</w> !</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">Le</w> <w n="26.2">russe</w>, <w n="26.3">toujours</w> <w n="26.4">tremblant</w></l>
					<l n="27" num="6.2"><space unit="char" quantity="8"></space><w n="27.1">Sous</w> <w n="27.2">la</w> <w n="27.3">neige</w></l>
					<l n="28" num="6.3"><space unit="char" quantity="8"></space><w n="28.1">Qui</w> <w n="28.2">l</w>’<w n="28.3">assiège</w>,</l>
					<l n="29" num="6.4"><w n="29.1">Las</w> <w n="29.2">de</w> <w n="29.3">pain</w> <w n="29.4">noir</w> <w n="29.5">et</w> <w n="29.6">de</w> <w n="29.7">gland</w>,</l>
					<l n="30" num="6.5"><w n="30.1">Veut</w> <w n="30.2">manger</w> <w n="30.3">notre</w> <w n="30.4">pain</w> <w n="30.5">blanc</w>.</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><space unit="char" quantity="2"></space><w n="31.1">Gai</w> ! <w n="31.2">Gai</w> ! <w n="31.3">Serrons</w> <w n="31.4">nos</w> <w n="31.5">rangs</w>,</l>
					<l n="32" num="7.2"><space unit="char" quantity="8"></space><w n="32.1">Espérance</w></l>
					<l n="33" num="7.3"><space unit="char" quantity="8"></space><w n="33.1">De</w> <w n="33.2">la</w> <w n="33.3">France</w> ;</l>
					<l n="34" num="7.4"><space unit="char" quantity="2"></space><w n="34.1">Gai</w> ! <w n="34.2">Gai</w> ! <w n="34.3">Serrons</w> <w n="34.4">nos</w> <w n="34.5">rangs</w> ;</l>
					<l n="35" num="7.5"><w n="35.1">En</w> <w n="35.2">avant</w>, <w n="35.3">gaulois</w> <w n="35.4">et</w> <w n="35.5">francs</w> !</l>
				</lg>
				<lg n="8">
					<l n="36" num="8.1"><w n="36.1">Ces</w> <w n="36.2">vins</w> <w n="36.3">que</w> <w n="36.4">nous</w> <w n="36.5">amassons</w></l>
					<l n="37" num="8.2"><space unit="char" quantity="8"></space><w n="37.1">Pour</w> <w n="37.2">les</w> <w n="37.3">boire</w></l>
					<l n="38" num="8.3"><space unit="char" quantity="6"></space><w n="38.1">À</w> <w n="38.2">la</w> <w n="38.3">victoire</w>,</l>
					<l n="39" num="8.4"><w n="39.1">Seraient</w> <w n="39.2">bus</w> <w n="39.3">par</w> <w n="39.4">des</w> <w n="39.5">saxons</w> !</l>
					<l n="40" num="8.5"><w n="40.1">Plus</w> <w n="40.2">de</w> <w n="40.3">vin</w>, <w n="40.4">plus</w> <w n="40.5">de</w> <w n="40.6">chansons</w> !</l>
				</lg>
				<lg n="9">
					<l n="41" num="9.1"><space unit="char" quantity="2"></space><w n="41.1">Gai</w> ! <w n="41.2">Gai</w> ! <w n="41.3">Serrons</w> <w n="41.4">nos</w> <w n="41.5">rangs</w>,</l>
					<l n="42" num="9.2"><space unit="char" quantity="8"></space><w n="42.1">Espérance</w></l>
					<l n="43" num="9.3"><space unit="char" quantity="8"></space><w n="43.1">De</w> <w n="43.2">la</w> <w n="43.3">France</w> ;</l>
					<l n="44" num="9.4"><space unit="char" quantity="2"></space><w n="44.1">Gai</w> ! <w n="44.2">Gai</w> ! <w n="44.3">Serrons</w> <w n="44.4">nos</w> <w n="44.5">rangs</w> ;</l>
					<l n="45" num="9.5"><w n="45.1">En</w> <w n="45.2">avant</w>, <w n="45.3">gaulois</w> <w n="45.4">et</w> <w n="45.5">francs</w> !</l>
				</lg>
				<lg n="10">
					<l n="46" num="10.1"><w n="46.1">Pour</w> <w n="46.2">des</w> <w n="46.3">calmouks</w> <w n="46.4">durs</w> <w n="46.5">et</w> <w n="46.6">laids</w></l>
					<l n="47" num="10.2"><space unit="char" quantity="10"></space><w n="47.1">Nos</w> <w n="47.2">filles</w></l>
					<l n="48" num="10.3"><space unit="char" quantity="6"></space><w n="48.1">Sont</w> <w n="48.2">trop</w> <w n="48.3">gentilles</w>,</l>
					<l n="49" num="10.4"><w n="49.1">Nos</w> <w n="49.2">femmes</w> <w n="49.3">ont</w> <w n="49.4">trop</w> <w n="49.5">d</w>’<w n="49.6">attraits</w>.</l>
					<l n="50" num="10.5"><w n="50.1">Ah</w> ! <w n="50.2">Que</w> <w n="50.3">leurs</w> <w n="50.4">fils</w> <w n="50.5">soient</w> <w n="50.6">français</w> !</l>
				</lg>
				<lg n="11">
					<l n="51" num="11.1"><space unit="char" quantity="2"></space><w n="51.1">Gai</w> ! <w n="51.2">Gai</w> ! <w n="51.3">Serrons</w> <w n="51.4">nos</w> <w n="51.5">rangs</w>,</l>
					<l n="52" num="11.2"><space unit="char" quantity="8"></space><w n="52.1">Espérance</w></l>
					<l n="53" num="11.3"><space unit="char" quantity="8"></space><w n="53.1">De</w> <w n="53.2">la</w> <w n="53.3">France</w> ;</l>
					<l n="54" num="11.4"><space unit="char" quantity="2"></space><w n="54.1">Gai</w> ! <w n="54.2">Gai</w> ! <w n="54.3">Serrons</w> <w n="54.4">nos</w> <w n="54.5">rangs</w> ;</l>
					<l n="55" num="11.5"><w n="55.1">En</w> <w n="55.2">avant</w>, <w n="55.3">gaulois</w> <w n="55.4">et</w> <w n="55.5">francs</w> !</l>
				</lg>
				<lg n="12">
					<l n="56" num="12.1"><w n="56.1">Quoi</w> ! <w n="56.2">Ces</w> <w n="56.3">monuments</w> <w n="56.4">chéris</w>,</l>
					<l n="57" num="12.2"><space unit="char" quantity="10"></space><w n="57.1">Histoire</w></l>
					<l n="58" num="12.3"><space unit="char" quantity="6"></space><w n="58.1">De</w> <w n="58.2">notre</w> <w n="58.3">gloire</w>,</l>
					<l n="59" num="12.4"><w n="59.1">S</w>’<w n="59.2">écrouleraient</w> <w n="59.3">en</w> <w n="59.4">débris</w> !</l>
					<l n="60" num="12.5"><w n="60.1">Quoi</w> ! <w n="60.2">Les</w> <w n="60.3">prussiens</w> <w n="60.4">à</w> <w n="60.5">Paris</w> !</l>
				</lg>
				<lg n="13">
					<l n="61" num="13.1"><space unit="char" quantity="2"></space><w n="61.1">Gai</w> ! <w n="61.2">Gai</w> ! <w n="61.3">Serrons</w> <w n="61.4">nos</w> <w n="61.5">rangs</w>,</l>
					<l n="62" num="13.2"><space unit="char" quantity="8"></space><w n="62.1">Espérance</w></l>
					<l n="63" num="13.3"><space unit="char" quantity="8"></space><w n="63.1">De</w> <w n="63.2">la</w> <w n="63.3">France</w> ;</l>
					<l n="64" num="13.4"><space unit="char" quantity="2"></space><w n="64.1">Gai</w> ! <w n="64.2">Gai</w> ! <w n="64.3">Serrons</w> <w n="64.4">nos</w> <w n="64.5">rangs</w> ;</l>
					<l n="65" num="13.5"><w n="65.1">En</w> <w n="65.2">avant</w>, <w n="65.3">gaulois</w> <w n="65.4">et</w> <w n="65.5">francs</w> !</l>
				</lg>
				<lg n="14">
					<l n="66" num="14.1"><w n="66.1">Nobles</w> <w n="66.2">francs</w> <w n="66.3">et</w> <w n="66.4">bons</w> <w n="66.5">gaulois</w>,</l>
					<l n="67" num="14.2"><space unit="char" quantity="6"></space><w n="67.1">La</w> <w n="67.2">paix</w> <w n="67.3">si</w> <w n="67.4">chère</w></l>
					<l n="68" num="14.3"><space unit="char" quantity="8"></space><w n="68.1">À</w> <w n="68.2">la</w> <w n="68.3">terre</w></l>
					<l n="69" num="14.4"><w n="69.1">Dans</w> <w n="69.2">peu</w> <w n="69.3">viendra</w> <w n="69.4">sous</w> <w n="69.5">vos</w> <w n="69.6">toits</w></l>
					<l n="70" num="14.5"><w n="70.1">Vous</w> <w n="70.2">payer</w> <w n="70.3">de</w> <w n="70.4">tant</w> <w n="70.5">d</w>’<w n="70.6">exploits</w>.</l>
				</lg>
				<lg n="15">
					<l n="71" num="15.1"><space unit="char" quantity="2"></space><w n="71.1">Gai</w> ! <w n="71.2">Gai</w> ! <w n="71.3">Serrons</w> <w n="71.4">nos</w> <w n="71.5">rangs</w>,</l>
					<l n="72" num="15.2"><space unit="char" quantity="8"></space><w n="72.1">Espérance</w></l>
					<l n="73" num="15.3"><space unit="char" quantity="8"></space><w n="73.1">De</w> <w n="73.2">la</w> <w n="73.3">France</w> ;</l>
					<l n="74" num="15.4"><space unit="char" quantity="2"></space><w n="74.1">Gai</w> ! <w n="74.2">Gai</w> ! <w n="74.3">Serrons</w> <w n="74.4">nos</w> <w n="74.5">rangs</w> ;</l>
					<l n="75" num="15.5"><w n="75.1">En</w> <w n="75.2">avant</w>, <w n="75.3">gaulois</w> <w n="75.4">et</w> <w n="75.5">francs</w> !</l>
				</lg>
			</div></body></text></TEI>