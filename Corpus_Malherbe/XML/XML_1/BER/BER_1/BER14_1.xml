<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER14">
				<head type="main">AINSI SOIT-IL !</head>
				<opener>
					<dateline>
						<date when="1812">1812</date>
					</dateline>
				</opener>
				<head type="tune">AIR : Alleluia</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">suis</w> <w n="1.3">devin</w>, <w n="1.4">mes</w> <w n="1.5">chers</w> <w n="1.6">amis</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">avenir</w> <w n="2.3">qui</w> <w n="2.4">nous</w> <w n="2.5">est</w> <w n="2.6">promis</w></l>
					<l n="3" num="1.3"><w n="3.1">Se</w> <w n="3.2">découvre</w> <w n="3.3">à</w> <w n="3.4">mon</w> <w n="3.5">art</w> <w n="3.6">subtil</w>.</l>
					<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">Ainsi</w> <w n="4.2">soit</w>-<w n="4.3">il</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Plus</w> <w n="5.2">de</w> <w n="5.3">poëte</w> <w n="5.4">adulateur</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">puissant</w> <w n="6.3">craindra</w> <w n="6.4">le</w> <w n="6.5">flatteur</w> ;</l>
					<l n="7" num="2.3"><w n="7.1">Nul</w> <w n="7.2">courtisan</w> <w n="7.3">ne</w> <w n="7.4">sera</w> <w n="7.5">vil</w>.</l>
					<l n="8" num="2.4"><space unit="char" quantity="8"></space><w n="8.1">Ainsi</w> <w n="8.2">soit</w>-<w n="8.3">il</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Plus</w> <w n="9.2">d</w>’<w n="9.3">usuriers</w>, <w n="9.4">plus</w> <w n="9.5">de</w> <w n="9.6">joueurs</w>,</l>
					<l n="10" num="3.2"><w n="10.1">De</w> <w n="10.2">petits</w> <w n="10.3">banquiers</w> <w n="10.4">grands</w> <w n="10.5">seigneurs</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">pas</w> <w n="11.3">un</w> <w n="11.4">commis</w> <w n="11.5">incivil</w>.</l>
					<l n="12" num="3.4"><space unit="char" quantity="8"></space><w n="12.1">Ainsi</w> <w n="12.2">soit</w>-<w n="12.3">il</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">L</w>’<w n="13.2">amitié</w>, <w n="13.3">charme</w> <w n="13.4">de</w> <w n="13.5">nos</w> <w n="13.6">jours</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Ne</w> <w n="14.2">sera</w> <w n="14.3">plus</w> <w n="14.4">un</w> <w n="14.5">froid</w> <w n="14.6">discours</w></l>
					<l n="15" num="4.3"><w n="15.1">Dont</w> <w n="15.2">l</w>’<w n="15.3">infortune</w> <w n="15.4">rompt</w> <w n="15.5">le</w> <w n="15.6">fil</w>.</l>
					<l n="16" num="4.4"><space unit="char" quantity="8"></space><w n="16.1">Ainsi</w> <w n="16.2">soit</w>-<w n="16.3">il</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">La</w> <w n="17.2">fille</w>, <w n="17.3">novice</w> <w n="17.4">à</w> <w n="17.5">quinze</w> <w n="17.6">ans</w>,</l>
					<l n="18" num="5.2"><w n="18.1">À</w> <w n="18.2">dix</w>-<w n="18.3">huit</w> <w n="18.4">avec</w> <w n="18.5">ses</w> <w n="18.6">amants</w></l>
					<l n="19" num="5.3"><w n="19.1">N</w>’<w n="19.2">exercera</w> <w n="19.3">que</w> <w n="19.4">son</w> <w n="19.5">babil</w>.</l>
					<l n="20" num="5.4"><space unit="char" quantity="8"></space><w n="20.1">Ainsi</w> <w n="20.2">soit</w>-<w n="20.3">il</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Femme</w> <w n="21.2">fuira</w> <w n="21.3">les</w> <w n="21.4">vains</w> <w n="21.5">atours</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">son</w> <w n="22.3">mari</w> <w n="22.4">pendant</w> <w n="22.5">huit</w> <w n="22.6">jours</w></l>
					<l n="23" num="6.3"><w n="23.1">Pourra</w> <w n="23.2">s</w>’<w n="23.3">absenter</w> <w n="23.4">sans</w> <w n="23.5">péril</w>.</l>
					<l n="24" num="6.4"><space unit="char" quantity="8"></space><w n="24.1">Ainsi</w> <w n="24.2">soit</w>-<w n="24.3">il</w> !</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">L</w>’<w n="25.2">on</w> <w n="25.3">montrera</w> <w n="25.4">dans</w> <w n="25.5">chaque</w> <w n="25.6">écrit</w></l>
					<l n="26" num="7.2"><w n="26.1">Plus</w> <w n="26.2">de</w> <w n="26.3">génie</w> <w n="26.4">et</w> <w n="26.5">moins</w> <w n="26.6">d</w>’<w n="26.7">esprit</w>,</l>
					<l n="27" num="7.3"><w n="27.1">Laissant</w> <w n="27.2">tout</w> <w n="27.3">jargon</w> <w n="27.4">puéril</w>.</l>
					<l n="28" num="7.4"><space unit="char" quantity="8"></space><w n="28.1">Ainsi</w> <w n="28.2">soit</w>-<w n="28.3">il</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">L</w>’<w n="29.2">auteur</w> <w n="29.3">aura</w> <w n="29.4">plus</w> <w n="29.5">de</w> <w n="29.6">fierté</w>,</l>
					<l n="30" num="8.2"><w n="30.1">L</w>’<w n="30.2">acteur</w> <w n="30.3">moins</w> <w n="30.4">de</w> <w n="30.5">fatuité</w> ;</l>
					<l n="31" num="8.3"><w n="31.1">Le</w> <w n="31.2">critique</w> <w n="31.3">sera</w> <w n="31.4">civil</w>.</l>
					<l n="32" num="8.4"><space unit="char" quantity="8"></space><w n="32.1">Ainsi</w> <w n="32.2">soit</w>-<w n="32.3">il</w> !</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><w n="33.1">On</w> <w n="33.2">rira</w> <w n="33.3">des</w> <w n="33.4">erreurs</w> <w n="33.5">des</w> <w n="33.6">grands</w>,</l>
					<l n="34" num="9.2"><w n="34.1">On</w> <w n="34.2">chansonnera</w> <w n="34.3">leurs</w> <w n="34.4">agents</w>,</l>
					<l n="35" num="9.3"><w n="35.1">Sans</w> <w n="35.2">voir</w> <w n="35.3">arriver</w> <w n="35.4">l</w>’<w n="35.5">alguazil</w>.</l>
					<l n="36" num="9.4"><space unit="char" quantity="8"></space><w n="36.1">Ainsi</w> <w n="36.2">soit</w>-<w n="36.3">il</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">En</w> <w n="37.2">France</w> <w n="37.3">enfin</w> <w n="37.4">renaît</w> <w n="37.5">le</w> <w n="37.6">goût</w> ;</l>
					<l n="38" num="10.2"><w n="38.1">La</w> <w n="38.2">justice</w> <w n="38.3">règne</w> <w n="38.4">par</w>-<w n="38.5">tout</w>,</l>
					<l n="39" num="10.3"><w n="39.1">Et</w> <w n="39.2">la</w> <w n="39.3">vérité</w> <w n="39.4">sort</w> <w n="39.5">d</w>’<w n="39.6">exil</w>.</l>
					<l n="40" num="10.4"><space unit="char" quantity="8"></space><w n="40.1">Ainsi</w> <w n="40.2">soit</w>-<w n="40.3">il</w> !</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><w n="41.1">Or</w>, <w n="41.2">mes</w> <w n="41.3">amis</w>, <w n="41.4">bénissons</w> <w n="41.5">Dieu</w>,</l>
					<l n="42" num="11.2"><w n="42.1">Qui</w> <w n="42.2">met</w> <w n="42.3">chaque</w> <w n="42.4">chose</w> <w n="42.5">en</w> <w n="42.6">son</w> <w n="42.7">lieu</w> :</l>
					<l n="43" num="11.3"><w n="43.1">Celles</w>-<w n="43.2">ci</w> <w n="43.3">sont</w> <w n="43.4">pour</w> <w n="43.5">l</w>’<w n="43.6">an</w> <w n="43.7">trois</w> <w n="43.8">mil</w>.</l>
					<l n="44" num="11.4"><space unit="char" quantity="8"></space><w n="44.1">Ainsi</w> <w n="44.2">soit</w>-<w n="44.3">il</w> !</l>
				</lg>
			</div></body></text></TEI>