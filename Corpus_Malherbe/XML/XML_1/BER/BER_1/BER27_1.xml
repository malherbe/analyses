<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER27">
				<head type="main">LA DOUBLE IVRESSE</head>
				<head type="tune">AIR : Que ne suis-je la fougère !</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">reposais</w> <w n="1.3">sous</w> <w n="1.4">l</w>’<w n="1.5">ombrage</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Quand</w> <w n="2.2">Nœris</w> <w n="2.3">vint</w> <w n="2.4">m</w>’<w n="2.5">éveiller</w> :</l>
					<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">crus</w> <w n="3.3">voir</w> <w n="3.4">sur</w> <w n="3.5">son</w> <w n="3.6">visage</w></l>
					<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">feu</w> <w n="4.3">du</w> <w n="4.4">désir</w> <w n="4.5">briller</w> :</l>
					<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">son</w> <w n="5.3">front</w> <w n="5.4">Zéphire</w> <w n="5.5">agite</w></l>
					<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">rose</w> <w n="6.3">et</w> <w n="6.4">le</w> <w n="6.5">pampre</w> <w n="6.6">vert</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">de</w> <w n="7.3">son</w> <w n="7.4">sein</w> <w n="7.5">qui</w> <w n="7.6">palpite</w></l>
					<l n="8" num="1.8"><w n="8.1">Flotte</w> <w n="8.2">le</w> <w n="8.3">voile</w> <w n="8.4">entr</w>’<w n="8.5">ouvert</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Un</w> <w n="9.2">enfant</w> <w n="9.3">qui</w> <w n="9.4">suit</w> <w n="9.5">sa</w> <w n="9.6">trace</w></l>
					<l n="10" num="2.2">(<w n="10.1">son</w> <w n="10.2">frère</w>, <w n="10.3">si</w> <w n="10.4">je</w> <w n="10.5">l</w>’<w n="10.6">en</w> <w n="10.7">crois</w>)</l>
					<l n="11" num="2.3"><w n="11.1">Presse</w> <w n="11.2">pour</w> <w n="11.3">remplir</w> <w n="11.4">sa</w> <w n="11.5">tasse</w></l>
					<l n="12" num="2.4"><w n="12.1">Des</w> <w n="12.2">raisins</w> <w n="12.3">entre</w> <w n="12.4">ses</w> <w n="12.5">doigts</w>.</l>
					<l n="13" num="2.5"><w n="13.1">Tandis</w> <w n="13.2">qu</w>’<w n="13.3">à</w> <w n="13.4">mes</w> <w n="13.5">yeux</w> <w n="13.6">la</w> <w n="13.7">belle</w></l>
					<l n="14" num="2.6"><w n="14.1">Chante</w> <w n="14.2">et</w> <w n="14.3">danse</w> <w n="14.4">à</w> <w n="14.5">ses</w> <w n="14.6">chansons</w>,</l>
					<l n="15" num="2.7"><w n="15.1">L</w>’<w n="15.2">enfant</w>, <w n="15.3">caché</w> <w n="15.4">derrière</w> <w n="15.5">elle</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Mêle</w> <w n="16.2">au</w> <w n="16.3">vin</w> <w n="16.4">d</w>’<w n="16.5">affreux</w> <w n="16.6">poisons</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Nœris</w> <w n="17.2">prend</w> <w n="17.3">la</w> <w n="17.4">tasse</w> <w n="17.5">pleine</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Y</w> <w n="18.2">goûte</w>, <w n="18.3">et</w> <w n="18.4">vient</w> <w n="18.5">me</w> <w n="18.6">l</w>’<w n="18.7">offrir</w>.</l>
					<l n="19" num="3.3"><w n="19.1">Ah</w> ! <w n="19.2">Dis</w>-<w n="19.3">je</w>, <w n="19.4">la</w> <w n="19.5">ruse</w> <w n="19.6">est</w> <w n="19.7">vaine</w> :</l>
					<l n="20" num="3.4"><w n="20.1">Je</w> <w n="20.2">sais</w> <w n="20.3">qu</w>’<w n="20.4">on</w> <w n="20.5">peut</w> <w n="20.6">en</w> <w n="20.7">mourir</w>.</l>
					<l n="21" num="3.5"><w n="21.1">Tu</w> <w n="21.2">le</w> <w n="21.3">veux</w>, <w n="21.4">enchanteresse</w> ;</l>
					<l n="22" num="3.6"><w n="22.1">Je</w> <w n="22.2">bois</w>, <w n="22.3">dussé</w>-<w n="22.4">je</w> <w n="22.5">en</w> <w n="22.6">ce</w> <w n="22.7">jour</w></l>
					<l n="23" num="3.7"><w n="23.1">Du</w> <w n="23.2">vin</w> <w n="23.3">expier</w> <w n="23.4">l</w>’<w n="23.5">ivresse</w></l>
					<l n="24" num="3.8"><w n="24.1">Par</w> <w n="24.2">l</w>’<w n="24.3">ivresse</w> <w n="24.4">de</w> <w n="24.5">l</w>’<w n="24.6">amour</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Mon</w> <w n="25.2">délire</w> <w n="25.3">fut</w> <w n="25.4">extrême</w> :</l>
					<l n="26" num="4.2"><w n="26.1">Mais</w> <w n="26.2">aussi</w> <w n="26.3">qu</w>’<w n="26.4">il</w> <w n="26.5">dura</w> <w n="26.6">peu</w> !</l>
					<l n="27" num="4.3"><w n="27.1">Ce</w> <w n="27.2">n</w>’<w n="27.3">est</w> <w n="27.4">plus</w> <w n="27.5">Nœris</w> <w n="27.6">que</w> <w n="27.7">j</w>’<w n="27.8">aime</w>,</l>
					<l n="28" num="4.4"><w n="28.1">Et</w> <w n="28.2">Nœris</w> <w n="28.3">s</w>’<w n="28.4">en</w> <w n="28.5">fait</w> <w n="28.6">un</w> <w n="28.7">jeu</w>.</l>
					<l n="29" num="4.5"><w n="29.1">De</w> <w n="29.2">ces</w> <w n="29.3">ardeurs</w> <w n="29.4">infidèles</w></l>
					<l n="30" num="4.6"><w n="30.1">Ce</w> <w n="30.2">qui</w> <w n="30.3">reste</w> <w n="30.4">c</w>’<w n="30.5">est</w> <w n="30.6">qu</w>’<w n="30.7">enfin</w>,</l>
					<l n="31" num="4.7"><w n="31.1">Depuis</w>, <w n="31.2">à</w> <w n="31.3">l</w>’<w n="31.4">amour</w> <w n="31.5">des</w> <w n="31.6">belles</w></l>
					<l n="32" num="4.8"><w n="32.1">J</w>’<w n="32.2">ai</w> <w n="32.3">mêlé</w> <w n="32.4">le</w> <w n="32.5">goût</w> <w n="32.6">du</w> <w n="32.7">vin</w>.</l>
				</lg>
			</div></body></text></TEI>