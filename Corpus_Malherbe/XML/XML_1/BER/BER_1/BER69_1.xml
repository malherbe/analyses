<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER69">
				<head type="main">ON S’EN FICHE !</head>
				<head type="tune">AIR : Le fleuve d’oubli</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1">De</w> <w n="1.2">traverse</w> <w n="1.3">en</w> <w n="1.4">traverse</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">Tout</w> <w n="2.2">va</w> <w n="2.3">dans</w> <w n="2.4">l</w>’<w n="2.5">univers</w></l>
					<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">De</w> <w n="3.2">travers</w>.</l>
					<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">Toute</w> <w n="4.2">femme</w> <w n="4.3">est</w> <w n="4.4">perverse</w>,</l>
					<l n="5" num="1.5"><space unit="char" quantity="2"></space><w n="5.1">Tout</w> <w n="5.2">traiteur</w> <w n="5.3">exigeant</w></l>
					<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Pour</w> <w n="6.2">l</w>’<w n="6.3">argent</w>.</l>
					<l n="7" num="1.7"><w n="7.1">À</w> <w n="7.2">tout</w> <w n="7.3">jeu</w> <w n="7.4">le</w> <w n="7.5">sort</w> <w n="7.6">nous</w> <w n="7.7">triche</w> ;</l>
					<l n="8" num="1.8"><space unit="char" quantity="2"></space><w n="8.1">Mais</w> <w n="8.2">enfin</w> <w n="8.3">est</w>-<w n="8.4">on</w> <w n="8.5">gris</w>,</l>
					<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Biribi</w>,</l>
					<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">On</w> <w n="10.2">s</w>’<w n="10.3">en</w> <w n="10.4">fiche</w> !</l>
				</lg>
				<lg n="2">
					<l n="11" num="2.1"><space unit="char" quantity="2"></space><w n="11.1">Désespoir</w> <w n="11.2">d</w>’<w n="11.3">un</w> <w n="11.4">ivrogne</w>,</l>
					<l n="12" num="2.2"><space unit="char" quantity="2"></space><w n="12.1">Vient</w> <w n="12.2">un</w> <w n="12.3">marchand</w> <w n="12.4">maudit</w></l>
					<l n="13" num="2.3"><space unit="char" quantity="8"></space><w n="13.1">Qui</w> <w n="13.2">vous</w> <w n="13.3">dit</w></l>
					<l n="14" num="2.4"><space unit="char" quantity="2"></space><w n="14.1">Qu</w>’<w n="14.2">en</w> <w n="14.3">Champagne</w>, <w n="14.4">en</w> <w n="14.5">Bourgogne</w>,</l>
					<l n="15" num="2.5"><space unit="char" quantity="2"></space><w n="15.1">Les</w> <w n="15.2">coteaux</w> <w n="15.3">sont</w> <w n="15.4">grêlés</w></l>
					<l n="16" num="2.6"><space unit="char" quantity="8"></space><w n="16.1">Et</w> <w n="16.2">gelés</w>.</l>
					<l n="17" num="2.7"><w n="17.1">À</w> <w n="17.2">tout</w> <w n="17.3">jeu</w> <w n="17.4">le</w> <w n="17.5">sort</w> <w n="17.6">nous</w> <w n="17.7">triche</w> ;</l>
					<l n="18" num="2.8"><space unit="char" quantity="2"></space><w n="18.1">Mais</w> <w n="18.2">enfin</w> <w n="18.3">est</w>-<w n="18.4">on</w> <w n="18.5">gris</w>,</l>
					<l n="19" num="2.9"><space unit="char" quantity="8"></space><w n="19.1">Biribi</w>,</l>
					<l n="20" num="2.10"><space unit="char" quantity="8"></space><w n="20.1">On</w> <w n="20.2">s</w>’<w n="20.3">en</w> <w n="20.4">fiche</w> !</l>
				</lg>
				<lg n="3">
					<l n="21" num="3.1"><space unit="char" quantity="2"></space><w n="21.1">Oubliez</w> <w n="21.2">une</w> <w n="21.3">dette</w>,</l>
					<l n="22" num="3.2"><space unit="char" quantity="2"></space><w n="22.1">Chez</w> <w n="22.2">vous</w> <w n="22.3">entre</w> <w n="22.4">un</w> <w n="22.5">huissier</w></l>
					<l n="23" num="3.3"><space unit="char" quantity="8"></space><w n="23.1">Bien</w> <w n="23.2">grossier</w></l>
					<l n="24" num="3.4"><space unit="char" quantity="2"></space><w n="24.1">Qui</w> <w n="24.2">vend</w> <w n="24.3">table</w> <w n="24.4">et</w> <w n="24.5">couchette</w>,</l>
					<l n="25" num="3.5"><space unit="char" quantity="2"></space><w n="25.1">Et</w> <w n="25.2">trouve</w> <w n="25.3">encor</w> <w n="25.4">de</w> <w n="25.5">quoi</w></l>
					<l n="26" num="3.6"><space unit="char" quantity="8"></space><w n="26.1">Pour</w> <w n="26.2">le</w> <w n="26.3">roi</w>.</l>
					<l n="27" num="3.7"><w n="27.1">À</w> <w n="27.2">tout</w> <w n="27.3">jeu</w> <w n="27.4">le</w> <w n="27.5">sort</w> <w n="27.6">nous</w> <w n="27.7">triche</w> ;</l>
					<l n="28" num="3.8"><space unit="char" quantity="2"></space><w n="28.1">Mais</w> <w n="28.2">enfin</w> <w n="28.3">est</w>-<w n="28.4">on</w> <w n="28.5">gris</w>,</l>
					<l n="29" num="3.9"><space unit="char" quantity="8"></space><w n="29.1">Biribi</w>,</l>
					<l n="30" num="3.10"><space unit="char" quantity="8"></space><w n="30.1">On</w> <w n="30.2">s</w>’<w n="30.3">en</w> <w n="30.4">fiche</w> !</l>
				</lg>
				<lg n="4">
					<l n="31" num="4.1"><space unit="char" quantity="2"></space><w n="31.1">Aucun</w> <w n="31.2">plaisir</w> <w n="31.3">n</w>’<w n="31.4">est</w> <w n="31.5">stable</w> :</l>
					<l n="32" num="4.2"><space unit="char" quantity="2"></space><w n="32.1">Pour</w> <w n="32.2">boire</w> <w n="32.3">est</w>-<w n="32.4">on</w> <w n="32.5">assis</w></l>
					<l n="33" num="4.3"><space unit="char" quantity="8"></space><w n="33.1">Cinq</w> <w n="33.2">ou</w> <w n="33.3">six</w>,</l>
					<l n="34" num="4.4"><space unit="char" quantity="2"></space><w n="34.1">Avant</w> <w n="34.2">vous</w> <w n="34.3">sous</w> <w n="34.4">la</w> <w n="34.5">table</w></l>
					<l n="35" num="4.5"><space unit="char" quantity="2"></space><w n="35.1">Tombent</w> <w n="35.2">deux</w>, <w n="35.3">trois</w> <w n="35.4">amis</w></l>
					<l n="36" num="4.6"><space unit="char" quantity="8"></space><w n="36.1">Endormis</w>.</l>
					<l n="37" num="4.7"><w n="37.1">À</w> <w n="37.2">tout</w> <w n="37.3">jeu</w> <w n="37.4">le</w> <w n="37.5">sort</w> <w n="37.6">nous</w> <w n="37.7">triche</w> ;</l>
					<l n="38" num="4.8"><space unit="char" quantity="2"></space><w n="38.1">Mais</w> <w n="38.2">enfin</w> <w n="38.3">est</w>-<w n="38.4">on</w> <w n="38.5">gris</w>,</l>
					<l n="39" num="4.9"><space unit="char" quantity="8"></space><w n="39.1">Biribi</w>,</l>
					<l n="40" num="4.10"><space unit="char" quantity="8"></space><w n="40.1">On</w> <w n="40.2">s</w>’<w n="40.3">en</w> <w n="40.4">fiche</w> !</l>
				</lg>
				<lg n="5">
					<l n="41" num="5.1"><space unit="char" quantity="2"></space><w n="41.1">C</w>’<w n="41.2">est</w> <w n="41.3">trop</w> <w n="41.4">d</w>’<w n="41.5">une</w> <w n="41.6">maîtresse</w> :</l>
					<l n="42" num="5.2"><space unit="char" quantity="2"></space><w n="42.1">Que</w> <w n="42.2">je</w> <w n="42.3">fus</w> <w n="42.4">malheureux</w></l>
					<l n="43" num="5.3"><space unit="char" quantity="8"></space><w n="43.1">Avec</w> <w n="43.2">deux</w> !</l>
					<l n="44" num="5.4"><space unit="char" quantity="2"></space><w n="44.1">Que</w> <w n="44.2">j</w>’<w n="44.3">eus</w> <w n="44.4">peu</w> <w n="44.5">de</w> <w n="44.6">sagesse</w></l>
					<l n="45" num="5.5"><space unit="char" quantity="2"></space><w n="45.1">D</w>’<w n="45.2">en</w> <w n="45.3">avoir</w> <w n="45.4">jusqu</w>’<w n="45.5">à</w> <w n="45.6">trois</w></l>
					<l n="46" num="5.6"><space unit="char" quantity="8"></space><w n="46.1">À</w>-<w n="46.2">la</w>-<w n="46.3">fois</w> !</l>
					<l n="47" num="5.7"><w n="47.1">À</w> <w n="47.2">tout</w> <w n="47.3">jeu</w> <w n="47.4">le</w> <w n="47.5">sort</w> <w n="47.6">nous</w> <w n="47.7">triche</w> ;</l>
					<l n="48" num="5.8"><space unit="char" quantity="2"></space><w n="48.1">Mais</w> <w n="48.2">enfin</w> <w n="48.3">est</w>-<w n="48.4">on</w> <w n="48.5">gris</w>,</l>
					<l n="49" num="5.9"><space unit="char" quantity="8"></space><w n="49.1">Biribi</w>,</l>
					<l n="50" num="5.10"><space unit="char" quantity="8"></space><w n="50.1">On</w> <w n="50.2">s</w>’<w n="50.3">en</w> <w n="50.4">fiche</w> !</l>
				</lg>
				<lg n="6">
					<l n="51" num="6.1"><space unit="char" quantity="2"></space><w n="51.1">De</w> <w n="51.2">ma</w> <w n="51.3">misanthropie</w></l>
					<l n="52" num="6.2"><space unit="char" quantity="2"></space><w n="52.1">Pardonnez</w> <w n="52.2">les</w> <w n="52.3">accès</w></l>
					<l n="53" num="6.3"><space unit="char" quantity="8"></space><w n="53.1">Et</w> <w n="53.2">l</w>’<w n="53.3">excès</w> ;</l>
					<l n="54" num="6.4"><space unit="char" quantity="2"></space><w n="54.1">Car</w> <w n="54.2">je</w> <w n="54.3">crains</w> <w n="54.4">la</w> <w n="54.5">pépie</w>,</l>
					<l n="55" num="6.5"><space unit="char" quantity="2"></space><w n="55.1">Et</w> <w n="55.2">je</w> <w n="55.3">ne</w> <w n="55.4">vois</w> <w n="55.5">qu</w>’<w n="55.6">abus</w></l>
					<l n="56" num="6.6"><space unit="char" quantity="8"></space><w n="56.1">Et</w> <w n="56.2">vins</w> <w n="56.3">bus</w>.</l>
					<l n="57" num="6.7"><w n="57.1">À</w> <w n="57.2">tout</w> <w n="57.3">jeu</w> <w n="57.4">le</w> <w n="57.5">sort</w> <w n="57.6">nous</w> <w n="57.7">triche</w> ;</l>
					<l n="58" num="6.8"><space unit="char" quantity="2"></space><w n="58.1">Mais</w> <w n="58.2">enfin</w> <w n="58.3">est</w>-<w n="58.4">on</w> <w n="58.5">gris</w>,</l>
					<l n="59" num="6.9"><space unit="char" quantity="8"></space><w n="59.1">Biribi</w>,</l>
					<l n="60" num="6.10"><space unit="char" quantity="8"></space><w n="60.1">On</w> <w n="60.2">s</w>’<w n="60.3">en</w> <w n="60.4">fiche</w> !</l>
				</lg>
			</div></body></text></TEI>