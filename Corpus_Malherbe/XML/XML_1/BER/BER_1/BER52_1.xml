<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER52">
				<head type="main">LA BOUTEILLE VOLÉE</head>
				<head type="tune">AIR : La fête des bonnes gens</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1">Sans</w> <w n="1.2">bruit</w>, <w n="1.3">dans</w> <w n="1.4">ma</w> <w n="1.5">retraite</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Hier</w> <w n="2.2">l</w>’<w n="2.3">amour</w> <w n="2.4">pénétra</w>,</l>
					<l n="3" num="1.3"><space unit="char" quantity="2"></space><w n="3.1">Courut</w> <w n="3.2">à</w> <w n="3.3">ma</w> <w n="3.4">cachette</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">de</w> <w n="4.3">mon</w> <w n="4.4">vin</w> <w n="4.5">s</w>’<w n="4.6">empara</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Depuis</w> <w n="5.2">lors</w> <w n="5.3">ma</w> <w n="5.4">voix</w> <w n="5.5">sommeille</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">Adieu</w> <w n="6.2">tous</w> <w n="6.3">mes</w> <w n="6.4">joyeux</w> <w n="6.5">sons</w>.</l>
					<l n="7" num="1.7"><w n="7.1">Amour</w>, <w n="7.2">rends</w>-<w n="7.3">moi</w> <w n="7.4">ma</w> <w n="7.5">bouteille</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Ma</w> <w n="8.2">bouteille</w> <w n="8.3">et</w> <w n="8.4">mes</w> <w n="8.5">chansons</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="2"></space><w n="9.1">Iris</w>, <w n="9.2">dame</w> <w n="9.3">et</w> <w n="9.4">coquette</w>,</l>
					<l n="10" num="2.2"><w n="10.1">À</w> <w n="10.2">ce</w> <w n="10.3">larcin</w> <w n="10.4">l</w>’<w n="10.5">a</w> <w n="10.6">poussé</w>.</l>
					<l n="11" num="2.3"><space unit="char" quantity="2"></space><w n="11.1">Je</w> <w n="11.2">n</w>’<w n="11.3">ai</w> <w n="11.4">plus</w> <w n="11.5">la</w> <w n="11.6">recette</w></l>
					<l n="12" num="2.4"><w n="12.1">Qui</w> <w n="12.2">soulage</w> <w n="12.3">un</w> <w n="12.4">cœur</w> <w n="12.5">blessé</w>.</l>
					<l n="13" num="2.5"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">pour</w> <w n="13.4">gémir</w> <w n="13.5">que</w> <w n="13.6">je</w> <w n="13.7">veille</w>,</l>
					<l n="14" num="2.6"><w n="14.1">En</w> <w n="14.2">proie</w> <w n="14.3">aux</w> <w n="14.4">jaloux</w> <w n="14.5">soupçons</w>.</l>
					<l n="15" num="2.7"><w n="15.1">Amour</w>, <w n="15.2">rends</w>-<w n="15.3">moi</w> <w n="15.4">ma</w> <w n="15.5">bouteille</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Ma</w> <w n="16.2">bouteille</w> <w n="16.3">et</w> <w n="16.4">mes</w> <w n="16.5">chansons</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space unit="char" quantity="2"></space><w n="17.1">Épicurien</w> <w n="17.2">aimable</w>,</l>
					<l n="18" num="3.2"><w n="18.1">À</w> <w n="18.2">verser</w> <w n="18.3">frais</w> <w n="18.4">m</w>’<w n="18.5">invitant</w>,</l>
					<l n="19" num="3.3"><space unit="char" quantity="2"></space><w n="19.1">Un</w> <w n="19.2">vieil</w> <w n="19.3">ami</w> <w n="19.4">de</w> <w n="19.5">table</w></l>
					<l n="20" num="3.4"><w n="20.1">Me</w> <w n="20.2">tend</w> <w n="20.3">son</w> <w n="20.4">verre</w> <w n="20.5">en</w> <w n="20.6">chantant</w> ;</l>
					<l n="21" num="3.5"><w n="21.1">Un</w> <w n="21.2">autre</w> <w n="21.3">vient</w> <w n="21.4">à</w> <w n="21.5">l</w>’<w n="21.6">oreille</w></l>
					<l n="22" num="3.6"><w n="22.1">Me</w> <w n="22.2">demander</w> <w n="22.3">des</w> <w n="22.4">leçons</w>.</l>
					<l n="23" num="3.7"><w n="23.1">Amour</w>, <w n="23.2">rends</w>-<w n="23.3">moi</w> <w n="23.4">ma</w> <w n="23.5">bouteille</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Ma</w> <w n="24.2">bouteille</w> <w n="24.3">et</w> <w n="24.4">mes</w> <w n="24.5">chansons</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="2"></space><w n="25.1">Tant</w> <w n="25.2">qu</w>’<w n="25.3">Iris</w> <w n="25.4">eut</w> <w n="25.5">contre</w> <w n="25.6">elle</w></l>
					<l n="26" num="4.2"><w n="26.1">Ce</w> <w n="26.2">bon</w> <w n="26.3">vin</w> <w n="26.4">si</w> <w n="26.5">regretté</w>,</l>
					<l n="27" num="4.3"><space unit="char" quantity="2"></space><w n="27.1">Grisette</w> <w n="27.2">folle</w> <w n="27.3">et</w> <w n="27.4">belle</w></l>
					<l n="28" num="4.4"><w n="28.1">Tenait</w> <w n="28.2">mon</w> <w n="28.3">cœur</w> <w n="28.4">en</w> <w n="28.5">gaîté</w>.</l>
					<l n="29" num="4.5"><w n="29.1">Lison</w> <w n="29.2">n</w>’<w n="29.3">a</w> <w n="29.4">point</w> <w n="29.5">sa</w> <w n="29.6">pareille</w></l>
					<l n="30" num="4.6"><w n="30.1">Pour</w> <w n="30.2">vivre</w> <w n="30.3">avec</w> <w n="30.4">des</w> <w n="30.5">garçons</w>.</l>
					<l n="31" num="4.7"><w n="31.1">Amour</w>, <w n="31.2">rends</w>-<w n="31.3">moi</w> <w n="31.4">ma</w> <w n="31.5">bouteille</w>,</l>
					<l n="32" num="4.8"><w n="32.1">Ma</w> <w n="32.2">bouteille</w> <w n="32.3">et</w> <w n="32.4">mes</w> <w n="32.5">chansons</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><space unit="char" quantity="2"></space><w n="33.1">Mais</w> <w n="33.2">le</w> <w n="33.3">filou</w> <w n="33.4">se</w> <w n="33.5">livre</w> :</l>
					<l n="34" num="5.2"><w n="34.1">Joyeux</w>, <w n="34.2">il</w> <w n="34.3">vient</w> <w n="34.4">à</w> <w n="34.5">ma</w> <w n="34.6">voix</w> ;</l>
					<l n="35" num="5.3"><space unit="char" quantity="2"></space><w n="35.1">De</w> <w n="35.2">mon</w> <w n="35.3">vin</w> <w n="35.4">il</w> <w n="35.5">est</w> <w n="35.6">ivre</w>,</l>
					<l n="36" num="5.4"><w n="36.1">Et</w> <w n="36.2">n</w>’<w n="36.3">en</w> <w n="36.4">a</w> <w n="36.5">bu</w> <w n="36.6">que</w> <w n="36.7">deux</w> <w n="36.8">doigts</w>.</l>
					<l n="37" num="5.5"><w n="37.1">Qu</w>’<w n="37.2">Iris</w> <w n="37.3">soit</w> <w n="37.4">une</w> <w n="37.5">merveille</w>,</l>
					<l n="38" num="5.6"><w n="38.1">Je</w> <w n="38.2">me</w> <w n="38.3">ris</w> <w n="38.4">de</w> <w n="38.5">ses</w> <w n="38.6">façons</w> :</l>
					<l n="39" num="5.7"><w n="39.1">Amour</w> <w n="39.2">me</w> <w n="39.3">rend</w> <w n="39.4">ma</w> <w n="39.5">bouteille</w>,</l>
					<l n="40" num="5.8"><w n="40.1">Ma</w> <w n="40.2">bouteille</w> <w n="40.3">et</w> <w n="40.4">mes</w> <w n="40.5">chansons</w>.</l>
				</lg>
			</div></body></text></TEI>