<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER19">
				<head type="main">LES GUEUX</head>
				<opener>
					<dateline>
						<date when="1812">1812</date>
					</dateline>
				</opener>
				<head type="tune">AIR : Première ronde du Départ pour Saint-Malo</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="6"></space><w n="1.1">Les</w> <w n="1.2">gueux</w>, <w n="1.3">les</w> <w n="1.4">gueux</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Sont</w> <w n="2.2">les</w> <w n="2.3">gens</w> <w n="2.4">heureux</w> ;</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Ils</w> <w n="3.2">s</w>’<w n="3.3">aiment</w> <w n="3.4">entre</w> <w n="3.5">eux</w>.</l>
					<l n="4" num="1.4"><space unit="char" quantity="6"></space><w n="4.1">Vivent</w> <w n="4.2">les</w> <w n="4.3">gueux</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Des</w> <w n="5.2">gueux</w> <w n="5.3">chantons</w> <w n="5.4">la</w> <w n="5.5">louange</w>.</l>
					<l n="6" num="2.2"><w n="6.1">Que</w> <w n="6.2">de</w> <w n="6.3">gueux</w> <w n="6.4">hommes</w> <w n="6.5">de</w> <w n="6.6">bien</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Il</w> <w n="7.2">faut</w> <w n="7.3">qu</w>’<w n="7.4">enfin</w> <w n="7.5">l</w>’<w n="7.6">esprit</w> <w n="7.7">venge</w></l>
					<l n="8" num="2.4"><w n="8.1">L</w>’<w n="8.2">honnête</w> <w n="8.3">homme</w> <w n="8.4">qui</w> <w n="8.5">n</w>’<w n="8.6">a</w> <w n="8.7">rien</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><space unit="char" quantity="6"></space><w n="9.1">Les</w> <w n="9.2">gueux</w>, <w n="9.3">les</w> <w n="9.4">gueux</w>,</l>
					<l n="10" num="3.2"><space unit="char" quantity="4"></space><w n="10.1">Sont</w> <w n="10.2">les</w> <w n="10.3">gens</w> <w n="10.4">heureux</w> ;</l>
					<l n="11" num="3.3"><space unit="char" quantity="4"></space><w n="11.1">Ils</w> <w n="11.2">s</w>’<w n="11.3">aiment</w> <w n="11.4">entre</w> <w n="11.5">eux</w>.</l>
					<l n="12" num="3.4"><space unit="char" quantity="6"></space><w n="12.1">Vivent</w> <w n="12.2">les</w> <w n="12.3">gueux</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Oui</w>, <w n="13.2">le</w> <w n="13.3">bonheur</w> <w n="13.4">est</w> <w n="13.5">facile</w></l>
					<l n="14" num="4.2"><w n="14.1">Au</w> <w n="14.2">sein</w> <w n="14.3">de</w> <w n="14.4">la</w> <w n="14.5">pauvreté</w> :</l>
					<l n="15" num="4.3"><w n="15.1">J</w>’<w n="15.2">en</w> <w n="15.3">atteste</w> <w n="15.4">l</w>’<w n="15.5">évangile</w> ;</l>
					<l n="16" num="4.4"><w n="16.1">J</w>’<w n="16.2">en</w> <w n="16.3">atteste</w> <w n="16.4">ma</w> <w n="16.5">gaîté</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><space unit="char" quantity="6"></space><w n="17.1">Les</w> <w n="17.2">gueux</w>, <w n="17.3">les</w> <w n="17.4">gueux</w>,</l>
					<l n="18" num="5.2"><space unit="char" quantity="4"></space><w n="18.1">Sont</w> <w n="18.2">les</w> <w n="18.3">gens</w> <w n="18.4">heureux</w> ;</l>
					<l n="19" num="5.3"><space unit="char" quantity="4"></space><w n="19.1">Ils</w> <w n="19.2">s</w>’<w n="19.3">aiment</w> <w n="19.4">entre</w> <w n="19.5">eux</w>.</l>
					<l n="20" num="5.4"><space unit="char" quantity="6"></space><w n="20.1">Vivent</w> <w n="20.2">les</w> <w n="20.3">gueux</w> !</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Au</w> <w n="21.2">Parnasse</w> <w n="21.3">la</w> <w n="21.4">misère</w></l>
					<l n="22" num="6.2"><w n="22.1">Long</w>-<w n="22.2">temps</w> <w n="22.3">a</w> <w n="22.4">régné</w>, <w n="22.5">dit</w>-<w n="22.6">on</w>.</l>
					<l n="23" num="6.3"><w n="23.1">Quels</w> <w n="23.2">biens</w> <w n="23.3">possédait</w> <w n="23.4">Homère</w> ?</l>
					<l n="24" num="6.4"><w n="24.1">Une</w> <w n="24.2">besace</w>, <w n="24.3">un</w> <w n="24.4">bâton</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><space unit="char" quantity="6"></space><w n="25.1">Les</w> <w n="25.2">gueux</w>, <w n="25.3">les</w> <w n="25.4">gueux</w>,</l>
					<l n="26" num="7.2"><space unit="char" quantity="4"></space><w n="26.1">Sont</w> <w n="26.2">les</w> <w n="26.3">gens</w> <w n="26.4">heureux</w> ;</l>
					<l n="27" num="7.3"><space unit="char" quantity="4"></space><w n="27.1">Ils</w> <w n="27.2">s</w>’<w n="27.3">aiment</w> <w n="27.4">entre</w> <w n="27.5">eux</w>.</l>
					<l n="28" num="7.4"><space unit="char" quantity="6"></space><w n="28.1">Vivent</w> <w n="28.2">les</w> <w n="28.3">gueux</w> !</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Vous</w> <w n="29.2">qu</w>’<w n="29.3">afflige</w> <w n="29.4">la</w> <w n="29.5">détresse</w>,</l>
					<l n="30" num="8.2"><w n="30.1">Croyez</w> <w n="30.2">que</w> <w n="30.3">plus</w> <w n="30.4">d</w>’<w n="30.5">un</w> <w n="30.6">héros</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Dans</w> <w n="31.2">le</w> <w n="31.3">soulier</w> <w n="31.4">qui</w> <w n="31.5">le</w> <w n="31.6">blesse</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Peut</w> <w n="32.2">regretter</w> <w n="32.3">ses</w> <w n="32.4">sabots</w>.</l>
				</lg>
				<lg n="9">
					<l n="33" num="9.1"><space unit="char" quantity="6"></space><w n="33.1">Les</w> <w n="33.2">gueux</w>, <w n="33.3">les</w> <w n="33.4">gueux</w>,</l>
					<l n="34" num="9.2"><space unit="char" quantity="4"></space><w n="34.1">Sont</w> <w n="34.2">les</w> <w n="34.3">gens</w> <w n="34.4">heureux</w> ;</l>
					<l n="35" num="9.3"><space unit="char" quantity="4"></space><w n="35.1">Ils</w> <w n="35.2">s</w>’<w n="35.3">aiment</w> <w n="35.4">entre</w> <w n="35.5">eux</w>.</l>
					<l n="36" num="9.4"><space unit="char" quantity="6"></space><w n="36.1">Vivent</w> <w n="36.2">les</w> <w n="36.3">gueux</w> !</l>
				</lg>
				<lg n="10">
					<l n="37" num="10.1"><w n="37.1">Du</w> <w n="37.2">faste</w> <w n="37.3">qui</w> <w n="37.4">vous</w> <w n="37.5">étonne</w></l>
					<l n="38" num="10.2"><w n="38.1">L</w>’<w n="38.2">exil</w> <w n="38.3">punit</w> <w n="38.4">plus</w> <w n="38.5">d</w>’<w n="38.6">un</w> <w n="38.7">grand</w> ;</l>
					<l n="39" num="10.3"><space unit="char" quantity="2"></space><w n="39.1">Diogène</w>, <w n="39.2">dans</w> <w n="39.3">sa</w> <w n="39.4">tonne</w>,</l>
					<l n="40" num="10.4"><w n="40.1">Brave</w> <w n="40.2">en</w> <w n="40.3">paix</w> <w n="40.4">un</w> <w n="40.5">conquérant</w>.</l>
				</lg>
				<lg n="11">
					<l n="41" num="11.1"><space unit="char" quantity="6"></space><w n="41.1">Les</w> <w n="41.2">gueux</w>, <w n="41.3">les</w> <w n="41.4">gueux</w>,</l>
					<l n="42" num="11.2"><space unit="char" quantity="4"></space><w n="42.1">Sont</w> <w n="42.2">les</w> <w n="42.3">gens</w> <w n="42.4">heureux</w> ;</l>
					<l n="43" num="11.3"><space unit="char" quantity="4"></space><w n="43.1">Ils</w> <w n="43.2">s</w>’<w n="43.3">aiment</w> <w n="43.4">entre</w> <w n="43.5">eux</w>.</l>
					<l n="44" num="11.4"><space unit="char" quantity="6"></space><w n="44.1">Vivent</w> <w n="44.2">les</w> <w n="44.3">gueux</w> !</l>
				</lg>
				<lg n="12">
					<l n="45" num="12.1"><w n="45.1">D</w>’<w n="45.2">un</w> <w n="45.3">palais</w> <w n="45.4">l</w>’<w n="45.5">éclat</w> <w n="45.6">vous</w> <w n="45.7">frappe</w>,</l>
					<l n="46" num="12.2"><w n="46.1">Mais</w> <w n="46.2">l</w>’<w n="46.3">ennui</w> <w n="46.4">vient</w> <w n="46.5">y</w> <w n="46.6">gémir</w>.</l>
					<l n="47" num="12.3"><w n="47.1">On</w> <w n="47.2">peut</w> <w n="47.3">bien</w> <w n="47.4">manger</w> <w n="47.5">sans</w> <w n="47.6">nappe</w> ;</l>
					<l n="48" num="12.4"><w n="48.1">Sur</w> <w n="48.2">la</w> <w n="48.3">paille</w> <w n="48.4">on</w> <w n="48.5">peut</w> <w n="48.6">dormir</w>.</l>
				</lg>
				<lg n="13">
					<l n="49" num="13.1"><space unit="char" quantity="6"></space><w n="49.1">Les</w> <w n="49.2">gueux</w>, <w n="49.3">les</w> <w n="49.4">gueux</w>,</l>
					<l n="50" num="13.2"><space unit="char" quantity="4"></space><w n="50.1">Sont</w> <w n="50.2">les</w> <w n="50.3">gens</w> <w n="50.4">heureux</w> ;</l>
					<l n="51" num="13.3"><space unit="char" quantity="4"></space><w n="51.1">Ils</w> <w n="51.2">s</w>’<w n="51.3">aiment</w> <w n="51.4">entre</w> <w n="51.5">eux</w>.</l>
					<l n="52" num="13.4"><space unit="char" quantity="6"></space><w n="52.1">Vivent</w> <w n="52.2">les</w> <w n="52.3">gueux</w> !</l>
				</lg>
				<lg n="14">
					<l n="53" num="14.1"><w n="53.1">Quel</w> <w n="53.2">dieu</w> <w n="53.3">se</w> <w n="53.4">plaît</w> <w n="53.5">et</w> <w n="53.6">s</w>’<w n="53.7">agite</w></l>
					<l n="54" num="14.2"><w n="54.1">Sur</w> <w n="54.2">ce</w> <w n="54.3">grabat</w> <w n="54.4">qu</w>’<w n="54.5">il</w> <w n="54.6">fleurit</w> ?</l>
					<l n="55" num="14.3"><w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">l</w>’<w n="55.4">amour</w> <w n="55.5">qui</w> <w n="55.6">rend</w> <w n="55.7">visite</w></l>
					<l n="56" num="14.4"><w n="56.1">À</w> <w n="56.2">la</w> <w n="56.3">pauvreté</w> <w n="56.4">qui</w> <w n="56.5">rit</w>.</l>
				</lg>
				<lg n="15">
					<l n="57" num="15.1"><space unit="char" quantity="6"></space><w n="57.1">Les</w> <w n="57.2">gueux</w>, <w n="57.3">les</w> <w n="57.4">gueux</w>,</l>
					<l n="58" num="15.2"><space unit="char" quantity="4"></space><w n="58.1">Sont</w> <w n="58.2">les</w> <w n="58.3">gens</w> <w n="58.4">heureux</w> ;</l>
					<l n="59" num="15.3"><space unit="char" quantity="4"></space><w n="59.1">Ils</w> <w n="59.2">s</w>’<w n="59.3">aiment</w> <w n="59.4">entre</w> <w n="59.5">eux</w>.</l>
					<l n="60" num="15.4"><space unit="char" quantity="6"></space><w n="60.1">Vivent</w> <w n="60.2">les</w> <w n="60.3">gueux</w> !</l>
				</lg>
				<lg n="16">
					<l n="61" num="16.1"><w n="61.1">L</w>’<w n="61.2">amitié</w> <w n="61.3">que</w> <w n="61.4">l</w>’<w n="61.5">on</w> <w n="61.6">regrette</w></l>
					<l n="62" num="16.2"><w n="62.1">N</w>’<w n="62.2">a</w> <w n="62.3">point</w> <w n="62.4">quitté</w> <w n="62.5">nos</w> <w n="62.6">climats</w> ;</l>
					<l n="63" num="16.3"><w n="63.1">Elle</w> <w n="63.2">trinque</w> <w n="63.3">à</w> <w n="63.4">la</w> <w n="63.5">guinguette</w>,</l>
					<l n="64" num="16.4"><w n="64.1">Assise</w> <w n="64.2">entre</w> <w n="64.3">deux</w> <w n="64.4">soldats</w>.</l>
				</lg>
				<lg n="17">
					<l n="65" num="17.1"><space unit="char" quantity="6"></space><w n="65.1">Les</w> <w n="65.2">gueux</w>, <w n="65.3">les</w> <w n="65.4">gueux</w>,</l>
					<l n="66" num="17.2"><space unit="char" quantity="4"></space><w n="66.1">Sont</w> <w n="66.2">les</w> <w n="66.3">gens</w> <w n="66.4">heureux</w> ;</l>
					<l n="67" num="17.3"><space unit="char" quantity="4"></space><w n="67.1">Ils</w> <w n="67.2">s</w>’<w n="67.3">aiment</w> <w n="67.4">entre</w> <w n="67.5">eux</w>.</l>
					<l n="68" num="17.4"><space unit="char" quantity="6"></space><w n="68.1">Vivent</w> <w n="68.2">les</w> <w n="68.3">gueux</w> !</l>
				</lg>
			</div></body></text></TEI>