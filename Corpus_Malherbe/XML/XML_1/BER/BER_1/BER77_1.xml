<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER77">
				<head type="main">A MON AMI DÉSAUGIERS</head>
				<head type="sub_2">PRÉSIDENT DU CAVEAU MODERNE ET DIRECTEUR DU VAUDEVILLE</head>
				<opener>
					<dateline>
						<date when="1815">1815</date>
					</dateline>
				</opener>
				<head type="tune">AIR de la Catacoua</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="4"></space><w n="1.1">Bon</w> <w n="1.2">Désaugiers</w>, <w n="1.3">mon</w> <w n="1.4">camarade</w>,</l>
					<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">Mets</w> <w n="2.2">dans</w> <w n="2.3">tes</w> <w n="2.4">poches</w> <w n="2.5">deux</w> <w n="2.6">flacons</w> ;</l>
					<l n="3" num="1.3"><space unit="char" quantity="4"></space><w n="3.1">Puis</w> <w n="3.2">rassemble</w>, <w n="3.3">en</w> <w n="3.4">versant</w> <w n="3.5">rasade</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="4"></space><w n="4.1">Nos</w> <w n="4.2">auteurs</w> <w n="4.3">piquants</w> <w n="4.4">et</w> <w n="4.5">féconds</w>.</l>
					<l n="5" num="1.5"><space unit="char" quantity="4"></space><w n="5.1">Ramène</w>-<w n="5.2">les</w> <w n="5.3">dans</w> <w n="5.4">l</w>’<w n="5.5">humble</w> <w n="5.6">asile</w></l>
					<l n="6" num="1.6"><space unit="char" quantity="4"></space><w n="6.1">Où</w> <w n="6.2">renaît</w> <w n="6.3">le</w> <w n="6.4">joyeux</w> <w n="6.5">refrain</w>.</l>
					<l n="7" num="1.7"><space unit="char" quantity="12"></space><w n="7.1">Eh</w> ! <w n="7.2">Va</w> <w n="7.3">ton</w> <w n="7.4">train</w>,</l>
					<l n="8" num="1.8"><space unit="char" quantity="12"></space><w n="8.1">Gai</w> <w n="8.2">boute</w>-<w n="8.3">en</w>-<w n="8.4">train</w> !</l>
					<l n="9" num="1.9"><w n="9.1">Mets</w>-<w n="9.2">nous</w> <w n="9.3">en</w> <w n="9.4">train</w>, <w n="9.5">bien</w> <w n="9.6">en</w> <w n="9.7">train</w>, <w n="9.8">tous</w> <w n="9.9">en</w> <w n="9.10">train</w>,</l>
					<l n="10" num="1.10"><space unit="char" quantity="4"></space><w n="10.1">Et</w> <w n="10.2">rends</w> <w n="10.3">enfin</w> <w n="10.4">au</w> <w n="10.5">vaudeville</w></l>
					<l n="11" num="1.11"><space unit="char" quantity="4"></space><w n="11.1">Ses</w> <w n="11.2">grelots</w> <w n="11.3">et</w> <w n="11.4">son</w> <w n="11.5">tambourin</w>.</l>
				</lg>
				<lg n="2">
					<l n="12" num="2.1"><space unit="char" quantity="4"></space><w n="12.1">Rends</w>-<w n="12.2">lui</w>, <w n="12.3">s</w>’<w n="12.4">il</w> <w n="12.5">se</w> <w n="12.6">peut</w>, <w n="12.7">le</w> <w n="12.8">cortège</w></l>
					<l n="13" num="2.2"><space unit="char" quantity="4"></space><w n="13.1">Qu</w>’<w n="13.2">à</w> <w n="13.3">la</w> <w n="13.4">foire</w> <w n="13.5">il</w> <w n="13.6">a</w> <w n="13.7">fait</w> <w n="13.8">briller</w> :</l>
					<l n="14" num="2.3"><space unit="char" quantity="4"></space><w n="14.1">L</w>’<w n="14.2">ombre</w> <w n="14.3">de</w> <w n="14.4">Panard</w> <w n="14.5">te</w> <w n="14.6">protège</w> ;</l>
					<l n="15" num="2.4"><space unit="char" quantity="4"></space><w n="15.1">Vadé</w> <w n="15.2">semble</w> <w n="15.3">te</w> <w n="15.4">conseiller</w>.</l>
					<l n="16" num="2.5"><space unit="char" quantity="4"></space><w n="16.1">Fais</w>-<w n="16.2">nous</w> <w n="16.3">apparaître</w> <w n="16.4">à</w> <w n="16.5">la</w> <w n="16.6">file</w></l>
					<l n="17" num="2.6"><space unit="char" quantity="4"></space><w n="17.1">Jusqu</w>’<w n="17.2">aux</w> <w n="17.3">enfants</w> <w n="17.4">de</w> <w n="17.5">Tabarin</w>.</l>
					<l n="18" num="2.7"><space unit="char" quantity="12"></space><w n="18.1">Eh</w> ! <w n="18.2">Va</w> <w n="18.3">ton</w> <w n="18.4">train</w>,</l>
					<l n="19" num="2.8"><space unit="char" quantity="12"></space><w n="19.1">Gai</w> <w n="19.2">boute</w>-<w n="19.3">en</w>-<w n="19.4">train</w> !</l>
					<l n="20" num="2.9"><space unit="char" quantity="4"></space><w n="20.1">Eh</w> ! <w n="20.2">Va</w> <w n="20.3">ton</w> <w n="20.4">train</w>,<w n="20.5">gai</w> <w n="20.6">boute</w>-<w n="20.7">en</w>-<w n="20.8">train</w> !</l>
					<l n="21" num="2.10"><w n="21.1">Mets</w>-<w n="21.2">nous</w> <w n="21.3">en</w> <w n="21.4">train</w>, <w n="21.5">bien</w> <w n="21.6">en</w> <w n="21.7">train</w>, <w n="21.8">tous</w> <w n="21.9">en</w> <w n="21.10">train</w>,</l>
					<l n="22" num="2.11"><space unit="char" quantity="4"></space><w n="22.1">Et</w> <w n="22.2">rends</w> <w n="22.3">enfin</w> <w n="22.4">au</w> <w n="22.5">vaudeville</w></l>
					<l n="23" num="2.12"><space unit="char" quantity="4"></space><w n="23.1">Ses</w> <w n="23.2">grelots</w> <w n="23.3">et</w> <w n="23.4">son</w> <w n="23.5">tambourin</w>.</l>
				</lg>
				<lg n="3">
					<l n="24" num="3.1"><space unit="char" quantity="4"></space><w n="24.1">Au</w> <w n="24.2">lieu</w> <w n="24.3">de</w> <w n="24.4">fades</w> <w n="24.5">épigrammes</w>,</l>
					<l n="25" num="3.2"><space unit="char" quantity="4"></space><w n="25.1">Qu</w>’<w n="25.2">il</w> <w n="25.3">aiguise</w> <w n="25.4">un</w> <w n="25.5">couplet</w> <w n="25.6">gaillard</w> :</l>
					<l n="26" num="3.3"><space unit="char" quantity="4"></space><w n="26.1">Collé</w>, <w n="26.2">quoi</w> <w n="26.3">qu</w>’<w n="26.4">en</w> <w n="26.5">disent</w> <w n="26.6">nos</w> <w n="26.7">dames</w>,</l>
					<l n="27" num="3.4"><space unit="char" quantity="4"></space><w n="27.1">Est</w> <w n="27.2">un</w> <w n="27.3">fort</w> <w n="27.4">honnête</w> <w n="27.5">égrillard</w>.</l>
					<l n="28" num="3.5"><space unit="char" quantity="4"></space><w n="28.1">La</w> <w n="28.2">gaudriole</w>, <w n="28.3">qu</w>’<w n="28.4">on</w> <w n="28.5">exile</w>,</l>
					<l n="29" num="3.6"><space unit="char" quantity="4"></space><w n="29.1">Doit</w> <w n="29.2">refleurir</w> <w n="29.3">sur</w> <w n="29.4">son</w> <w n="29.5">terrain</w>.</l>
					<l n="30" num="3.7"><space unit="char" quantity="12"></space><w n="30.1">Eh</w> ! <w n="30.2">Va</w> <w n="30.3">ton</w> <w n="30.4">train</w>,</l>
					<l n="31" num="3.8"><space unit="char" quantity="12"></space><w n="31.1">Gai</w> <w n="31.2">boute</w>-<w n="31.3">en</w>-<w n="31.4">train</w> !</l>
					<l n="32" num="3.9"><w n="32.1">Mets</w>-<w n="32.2">nous</w> <w n="32.3">en</w> <w n="32.4">train</w>, <w n="32.5">bien</w> <w n="32.6">en</w> <w n="32.7">train</w>, <w n="32.8">tous</w> <w n="32.9">en</w> <w n="32.10">train</w>,</l>
					<l n="33" num="3.10"><space unit="char" quantity="4"></space><w n="33.1">Et</w> <w n="33.2">rends</w> <w n="33.3">enfin</w> <w n="33.4">au</w> <w n="33.5">vaudeville</w></l>
					<l n="34" num="3.11"><space unit="char" quantity="4"></space><w n="34.1">Ses</w> <w n="34.2">grelots</w> <w n="34.3">et</w> <w n="34.4">son</w> <w n="34.5">tambourin</w>.</l>
				</lg>
				<lg n="4">
					<l n="35" num="4.1"><space unit="char" quantity="4"></space><w n="35.1">Malgré</w> <w n="35.2">messieurs</w> <w n="35.3">de</w> <w n="35.4">la</w> <w n="35.5">police</w>,</l>
					<l n="36" num="4.2"><space unit="char" quantity="4"></space><w n="36.1">Le</w> <w n="36.2">vaudeville</w> <w n="36.3">est</w> <w n="36.4">né</w> <w n="36.5">frondeur</w> :</l>
					<l n="37" num="4.3"><space unit="char" quantity="4"></space><w n="37.1">Des</w> <w n="37.2">abus</w> <w n="37.3">fais</w> <w n="37.4">ton</w> <w n="37.5">bénéfice</w> ;</l>
					<l n="38" num="4.4"><space unit="char" quantity="4"></space><w n="38.1">Force</w> <w n="38.2">les</w> <w n="38.3">grands</w> <w n="38.4">à</w> <w n="38.5">la</w> <w n="38.6">pudeur</w> ;</l>
					<l n="39" num="4.5"><space unit="char" quantity="4"></space><w n="39.1">Dénonce</w> <w n="39.2">tout</w> <w n="39.3">flatteur</w> <w n="39.4">servile</w></l>
					<l n="40" num="4.6"><space unit="char" quantity="4"></space><w n="40.1">À</w> <w n="40.2">la</w> <w n="40.3">gaîté</w> <w n="40.4">du</w> <w n="40.5">souverain</w>.</l>
					<l n="41" num="4.7"><space unit="char" quantity="12"></space><w n="41.1">Eh</w> ! <w n="41.2">Va</w> <w n="41.3">ton</w> <w n="41.4">train</w>,</l>
					<l n="42" num="4.8"><space unit="char" quantity="12"></space><w n="42.1">Gai</w> <w n="42.2">boute</w>-<w n="42.3">en</w>-<w n="42.4">train</w> !</l>
					<l n="43" num="4.9"><w n="43.1">Mets</w>-<w n="43.2">nous</w> <w n="43.3">en</w> <w n="43.4">train</w>, <w n="43.5">bien</w> <w n="43.6">en</w> <w n="43.7">train</w>, <w n="43.8">tous</w> <w n="43.9">en</w> <w n="43.10">train</w>,</l>
					<l n="44" num="4.10"><space unit="char" quantity="4"></space><w n="44.1">Et</w> <w n="44.2">rends</w> <w n="44.3">enfin</w> <w n="44.4">au</w> <w n="44.5">vaudeville</w></l>
					<l n="45" num="4.11"><space unit="char" quantity="4"></space><w n="45.1">Ses</w> <w n="45.2">grelots</w> <w n="45.3">et</w> <w n="45.4">son</w> <w n="45.5">tambourin</w>.</l>
				</lg>
				<lg n="5">
					<l n="46" num="5.1"><space unit="char" quantity="4"></space><w n="46.1">Sur</w> <w n="46.2">la</w> <w n="46.3">scène</w>, <w n="46.4">où</w> <w n="46.5">plus</w> <w n="46.6">à</w> <w n="46.7">son</w> <w n="46.8">aise</w></l>
					<l n="47" num="5.2"><space unit="char" quantity="4"></space><w n="47.1">Avec</w> <w n="47.2">toi</w> <w n="47.3">Momus</w> <w n="47.4">va</w> <w n="47.5">siéger</w>,</l>
					<l n="48" num="5.3"><space unit="char" quantity="4"></space><w n="48.1">Relève</w> <w n="48.2">la</w> <w n="48.3">gaîté</w> <w n="48.4">française</w></l>
					<l n="49" num="5.4"><space unit="char" quantity="4"></space><w n="49.1">À</w> <w n="49.2">la</w> <w n="49.3">barbe</w> <w n="49.4">de</w> <w n="49.5">l</w>’<w n="49.6">étranger</w>.</l>
					<l n="50" num="5.5"><space unit="char" quantity="4"></space><w n="50.1">La</w> <w n="50.2">chanson</w> <w n="50.3">est</w> <w n="50.4">une</w> <w n="50.5">arme</w> <w n="50.6">utile</w></l>
					<l n="51" num="5.6"><space unit="char" quantity="4"></space><w n="51.1">Qu</w>’<w n="51.2">on</w> <w n="51.3">oppose</w> <w n="51.4">à</w> <w n="51.5">plus</w> <w n="51.6">d</w>’<w n="51.7">un</w> <w n="51.8">chagrin</w>.</l>
					<l n="52" num="5.7"><space unit="char" quantity="12"></space><w n="52.1">Eh</w> ! <w n="52.2">Va</w> <w n="52.3">ton</w> <w n="52.4">train</w>,</l>
					<l n="53" num="5.8"><space unit="char" quantity="12"></space><w n="53.1">Gai</w> <w n="53.2">boute</w>-<w n="53.3">en</w>-<w n="53.4">train</w> !</l>
					<l n="54" num="5.9"><w n="54.1">Mets</w>-<w n="54.2">nous</w> <w n="54.3">en</w> <w n="54.4">train</w>, <w n="54.5">bien</w> <w n="54.6">en</w> <w n="54.7">train</w>, <w n="54.8">tous</w> <w n="54.9">en</w> <w n="54.10">train</w>,</l>
					<l n="55" num="5.10"><space unit="char" quantity="4"></space><w n="55.1">Et</w> <w n="55.2">rends</w> <w n="55.3">enfin</w> <w n="55.4">au</w> <w n="55.5">vaudeville</w></l>
					<l n="56" num="5.11"><space unit="char" quantity="4"></space><w n="56.1">Ses</w> <w n="56.2">grelots</w> <w n="56.3">et</w> <w n="56.4">son</w> <w n="56.5">tambourin</w>.</l>
				</lg>
				<lg n="6">
					<l n="57" num="6.1"><space unit="char" quantity="4"></space><w n="57.1">Verse</w>, <w n="57.2">ami</w>, <w n="57.3">verse</w> <w n="57.4">donc</w> <w n="57.5">à</w> <w n="57.6">boire</w> ;</l>
					<l n="58" num="6.2"><space unit="char" quantity="4"></space><w n="58.1">Que</w> <w n="58.2">nos</w> <w n="58.3">chants</w> <w n="58.4">reprennent</w> <w n="58.5">leur</w> <w n="58.6">cours</w>.</l>
					<l n="59" num="6.3"><space unit="char" quantity="4"></space><w n="59.1">Il</w> <w n="59.2">nous</w> <w n="59.3">faut</w> <w n="59.4">consoler</w> <w n="59.5">la</w> <w n="59.6">gloire</w> ;</l>
					<l n="60" num="6.4"><space unit="char" quantity="4"></space><w n="60.1">Il</w> <w n="60.2">faut</w> <w n="60.3">rassurer</w> <w n="60.4">les</w> <w n="60.5">amours</w>.</l>
					<l n="61" num="6.5"><space unit="char" quantity="4"></space><w n="61.1">Nous</w> <w n="61.2">cultivons</w> <w n="61.3">un</w> <w n="61.4">champ</w> <w n="61.5">fertile</w></l>
					<l n="62" num="6.6"><space unit="char" quantity="4"></space><w n="62.1">Qui</w> <w n="62.2">n</w>’<w n="62.3">attend</w> <w n="62.4">qu</w>’<w n="62.5">un</w> <w n="62.6">ciel</w> <w n="62.7">plus</w> <w n="62.8">serein</w>.</l>
					<l n="63" num="6.7"><space unit="char" quantity="12"></space><w n="63.1">Eh</w> ! <w n="63.2">Va</w> <w n="63.3">ton</w> <w n="63.4">train</w>,</l>
					<l n="64" num="6.8"><space unit="char" quantity="12"></space><w n="64.1">Gai</w> <w n="64.2">boute</w>-<w n="64.3">en</w>-<w n="64.4">train</w> !</l>
					<l n="65" num="6.9"><w n="65.1">Mets</w>-<w n="65.2">nous</w> <w n="65.3">en</w> <w n="65.4">train</w>, <w n="65.5">bien</w> <w n="65.6">en</w> <w n="65.7">train</w>, <w n="65.8">tous</w> <w n="65.9">en</w> <w n="65.10">train</w>,</l>
					<l n="66" num="6.10"><space unit="char" quantity="4"></space><w n="66.1">Et</w> <w n="66.2">rends</w> <w n="66.3">enfin</w> <w n="66.4">au</w> <w n="66.5">vaudeville</w></l>
					<l n="67" num="6.11"><space unit="char" quantity="4"></space><w n="67.1">Ses</w> <w n="67.2">grelots</w> <w n="67.3">et</w> <w n="67.4">son</w> <w n="67.5">tambourin</w>.</l>
				</lg>
			</div></body></text></TEI>