<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER28">
				<head type="main">VOYAGE AU PAYS DE COCAGNE</head>
				<head type="tune">AIR : Contre-danse de la Rosière, <hi rend="ital">ou</hi> L’ombre s’évapore</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ah</w> ! <w n="1.2">Vers</w> <w n="1.3">une</w> <w n="1.4">rive</w></l>
					<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">sans</w> <w n="2.3">peine</w> <w n="2.4">on</w> <w n="2.5">vive</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Qui</w> <w n="3.2">m</w>’<w n="3.3">aime</w> <w n="3.4">me</w> <w n="3.5">suive</w> !</l>
					<l n="4" num="1.4"><w n="4.1">Voyageons</w> <w n="4.2">gaîment</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Ivre</w> <w n="5.2">de</w> <w n="5.3">champagne</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Je</w> <w n="6.2">bats</w> <w n="6.3">la</w> <w n="6.4">campagne</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">vois</w> <w n="7.3">de</w> <w n="7.4">cocagne</w></l>
					<l n="8" num="1.8"><w n="8.1">Le</w> <w n="8.2">pays</w> <w n="8.3">charmant</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="2"></space><w n="9.1">Terre</w> <w n="9.2">chérie</w>,</l>
					<l n="10" num="2.2"><space unit="char" quantity="2"></space><w n="10.1">Sois</w> <w n="10.2">ma</w> <w n="10.3">patrie</w> :</l>
					<l n="11" num="2.3"><space unit="char" quantity="2"></space><w n="11.1">Qu</w>’<w n="11.2">ici</w> <w n="11.3">je</w> <w n="11.4">rie</w></l>
					<l n="12" num="2.4"><w n="12.1">Du</w> <w n="12.2">sort</w> <w n="12.3">inconstant</w>.</l>
					<l n="13" num="2.5"><space unit="char" quantity="2"></space><w n="13.1">Pour</w> <w n="13.2">moi</w> <w n="13.3">tout</w> <w n="13.4">change</w> :</l>
					<l n="14" num="2.6"><space unit="char" quantity="2"></space><w n="14.1">Bonheur</w> <w n="14.2">étrange</w> !</l>
					<l n="15" num="2.7"><space unit="char" quantity="2"></space><w n="15.1">Je</w> <w n="15.2">bois</w> <w n="15.3">et</w> <w n="15.4">mange</w></l>
					<l n="16" num="2.8"><w n="16.1">Sans</w> <w n="16.2">un</w> <w n="16.3">sou</w> <w n="16.4">comptant</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Mon</w> <w n="17.2">appétit</w> <w n="17.3">s</w>’<w n="17.4">ouvre</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Et</w> <w n="18.2">mon</w> <w n="18.3">œil</w> <w n="18.4">découvre</w></l>
					<l n="19" num="3.3"><w n="19.1">Les</w> <w n="19.2">portes</w> <w n="19.3">d</w>’<w n="19.4">un</w> <w n="19.5">louvre</w></l>
					<l n="20" num="3.4"><w n="20.1">En</w> <w n="20.2">tourte</w> <w n="20.3">arrondi</w> ;</l>
					<l n="21" num="3.5"><w n="21.1">J</w>’<w n="21.2">y</w> <w n="21.3">vois</w> <w n="21.4">de</w> <w n="21.5">gros</w> <w n="21.6">gardes</w>,</l>
					<l n="22" num="3.6"><w n="22.1">Cuirassés</w> <w n="22.2">de</w> <w n="22.3">bardes</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Portant</w> <w n="23.2">hallebardes</w></l>
					<l n="24" num="3.8"><w n="24.1">De</w> <w n="24.2">sucre</w> <w n="24.3">candi</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="2"></space><w n="25.1">Bon</w> <w n="25.2">dieu</w> ! <w n="25.3">Que</w> <w n="25.4">j</w>’<w n="25.5">aime</w></l>
					<l n="26" num="4.2"><space unit="char" quantity="2"></space><w n="26.1">Ce</w> <w n="26.2">doux</w> <w n="26.3">système</w> !</l>
					<l n="27" num="4.3"><space unit="char" quantity="2"></space><w n="27.1">Les</w> <w n="27.2">canons</w> <w n="27.3">même</w></l>
					<l n="28" num="4.4"><w n="28.1">De</w> <w n="28.2">sucre</w> <w n="28.3">sont</w> <w n="28.4">faits</w>.</l>
					<l n="29" num="4.5"><space unit="char" quantity="2"></space><w n="29.1">Belles</w> <w n="29.2">sculptures</w>,</l>
					<l n="30" num="4.6"><space unit="char" quantity="2"></space><w n="30.1">Riches</w> <w n="30.2">peintures</w></l>
					<l n="31" num="4.7"><space unit="char" quantity="2"></space><w n="31.1">En</w> <w n="31.2">confitures</w>,</l>
					<l n="32" num="4.8"><w n="32.1">Ornent</w> <w n="32.2">les</w> <w n="32.3">buffets</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Pierrots</w> <w n="33.2">et</w> <w n="33.3">paillasses</w>,</l>
					<l n="34" num="5.2"><w n="34.1">Beaux</w> <w n="34.2">esprits</w> <w n="34.3">cocasses</w>,</l>
					<l n="35" num="5.3"><w n="35.1">Charment</w> <w n="35.2">sur</w> <w n="35.3">les</w> <w n="35.4">places</w></l>
					<l n="36" num="5.4"><w n="36.1">Le</w> <w n="36.2">peuple</w> <w n="36.3">ébahi</w>,</l>
					<l n="37" num="5.5"><w n="37.1">Pour</w> <w n="37.2">qui</w> <w n="37.3">cent</w> <w n="37.4">fontaines</w>,</l>
					<l n="38" num="5.6"><w n="38.1">Au</w> <w n="38.2">lieu</w> <w n="38.3">d</w>’<w n="38.4">eaux</w> <w n="38.5">malsaines</w>,</l>
					<l n="39" num="5.7"><w n="39.1">Versent</w>, <w n="39.2">toujours</w> <w n="39.3">pleines</w>,</l>
					<l n="40" num="5.8"><w n="40.1">Le</w> <w n="40.2">beaune</w> <w n="40.3">et</w> <w n="40.4">l</w>’<w n="40.5">aï</w>.</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><space unit="char" quantity="2"></space><w n="41.1">Des</w> <w n="41.2">gens</w> <w n="41.3">enfournent</w>,</l>
					<l n="42" num="6.2"><space unit="char" quantity="2"></space><w n="42.1">D</w>’<w n="42.2">autres</w> <w n="42.3">défournent</w> ;</l>
					<l n="43" num="6.3"><space unit="char" quantity="2"></space><w n="43.1">Aux</w> <w n="43.2">broches</w> <w n="43.3">tournent</w></l>
					<l n="44" num="6.4"><w n="44.1">Veau</w>, <w n="44.2">bœuf</w> <w n="44.3">et</w> <w n="44.4">mouton</w>.</l>
					<l n="45" num="6.5"><space unit="char" quantity="2"></space><w n="45.1">Des</w> <w n="45.2">lois</w> <w n="45.3">de</w> <w n="45.4">table</w></l>
					<l n="46" num="6.6"><space unit="char" quantity="2"></space><w n="46.1">L</w>’<w n="46.2">ordre</w> <w n="46.3">équitable</w></l>
					<l n="47" num="6.7"><space unit="char" quantity="2"></space><w n="47.1">De</w> <w n="47.2">tout</w> <w n="47.3">coupable</w></l>
					<l n="48" num="6.8"><w n="48.1">Fait</w> <w n="48.2">un</w> <w n="48.3">marmiton</w>.</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1"><w n="49.1">Dans</w> <w n="49.2">un</w> <w n="49.3">palais</w> <w n="49.4">j</w>’<w n="49.5">entre</w>,</l>
					<l n="50" num="7.2"><w n="50.1">Et</w> <w n="50.2">je</w> <w n="50.3">m</w>’<w n="50.4">assieds</w> <w n="50.5">entre</w></l>
					<l n="51" num="7.3"><w n="51.1">Des</w> <w n="51.2">grands</w> <w n="51.3">dont</w> <w n="51.4">le</w> <w n="51.5">ventre</w></l>
					<l n="52" num="7.4"><w n="52.1">Se</w> <w n="52.2">porte</w> <w n="52.3">un</w> <w n="52.4">défi</w> ;</l>
					<l n="53" num="7.5"><w n="53.1">Je</w> <w n="53.2">trouve</w> <w n="53.3">en</w> <w n="53.4">ce</w> <w n="53.5">monde</w>,</l>
					<l n="54" num="7.6"><w n="54.1">Où</w> <w n="54.2">la</w> <w n="54.3">graisse</w> <w n="54.4">abonde</w>,</l>
					<l n="55" num="7.7"><w n="55.1">Vénus</w> <w n="55.2">toute</w> <w n="55.3">ronde</w></l>
					<l n="56" num="7.8"><w n="56.1">Et</w> <w n="56.2">l</w>’<w n="56.3">amour</w> <w n="56.4">bouffi</w>.</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1"><space unit="char" quantity="2"></space><w n="57.1">Nul</w> <w n="57.2">front</w> <w n="57.3">sinistre</w> ;</l>
					<l n="58" num="8.2"><space unit="char" quantity="2"></space><w n="58.1">Propos</w> <w n="58.2">de</w> <w n="58.3">cuistre</w>,</l>
					<l n="59" num="8.3"><space unit="char" quantity="2"></space><w n="59.1">Airs</w> <w n="59.2">de</w> <w n="59.3">ministre</w>,</l>
					<l n="60" num="8.4"><w n="60.1">N</w>’<w n="60.2">y</w> <w n="60.3">sont</w> <w n="60.4">point</w> <w n="60.5">permis</w>.</l>
					<l n="61" num="8.5"><space unit="char" quantity="2"></space><w n="61.1">La</w> <w n="61.2">table</w> <w n="61.3">est</w> <w n="61.4">mise</w>,</l>
					<l n="62" num="8.6"><space unit="char" quantity="2"></space><w n="62.1">La</w> <w n="62.2">chère</w> <w n="62.3">exquise</w> ;</l>
					<l n="63" num="8.7"><space unit="char" quantity="2"></space><w n="63.1">Que</w> <w n="63.2">l</w>’<w n="63.3">on</w> <w n="63.4">se</w> <w n="63.5">grise</w> :</l>
					<l n="64" num="8.8"><w n="64.1">Trinquons</w>, <w n="64.2">mes</w> <w n="64.3">amis</w> !</l>
				</lg>
				<lg n="9">
					<l n="65" num="9.1"><w n="65.1">Mais</w> <w n="65.2">parlons</w> <w n="65.3">d</w>’<w n="65.4">affaires</w>.</l>
					<l n="66" num="9.2"><w n="66.1">Beautés</w> <w n="66.2">peu</w> <w n="66.3">sévères</w>,</l>
					<l n="67" num="9.3"><w n="67.1">Qu</w>’<w n="67.2">au</w> <w n="67.3">doux</w> <w n="67.4">bruit</w> <w n="67.5">des</w> <w n="67.6">verres</w></l>
					<l n="68" num="9.4"><w n="68.1">D</w>’<w n="68.2">un</w> <w n="68.3">dessert</w> <w n="68.4">friand</w>,</l>
					<l n="69" num="9.5"><w n="69.1">On</w> <w n="69.2">chante</w> <w n="69.3">et</w> <w n="69.4">l</w>’<w n="69.5">on</w> <w n="69.6">dise</w></l>
					<l n="70" num="9.6"><w n="70.1">Quelque</w> <w n="70.2">gaillardise</w></l>
					<l n="71" num="9.7"><w n="71.1">Qui</w> <w n="71.2">nous</w> <w n="71.3">scandalise</w></l>
					<l n="72" num="9.8"><w n="72.1">En</w> <w n="72.2">nous</w> <w n="72.3">égayant</w>.</l>
				</lg>
				<lg n="10">
					<l n="73" num="10.1"><space unit="char" quantity="2"></space><w n="73.1">Quand</w> <w n="73.2">le</w> <w n="73.3">vin</w> <w n="73.4">tape</w></l>
					<l n="74" num="10.2"><space unit="char" quantity="2"></space><w n="74.1">L</w>’<w n="74.2">époux</w> <w n="74.3">qu</w>’<w n="74.4">on</w> <w n="74.5">drape</w>,</l>
					<l n="75" num="10.3"><space unit="char" quantity="2"></space><w n="75.1">Que</w> <w n="75.2">sur</w> <w n="75.3">la</w> <w n="75.4">nappe</w></l>
					<l n="76" num="10.4"><w n="76.1">Il</w> <w n="76.2">s</w>’<w n="76.3">endort</w> <w n="76.4">à</w> <w n="76.5">point</w> ;</l>
					<l n="77" num="10.5"><space unit="char" quantity="2"></space><w n="77.1">De</w> <w n="77.2">femme</w> <w n="77.3">aimable</w></l>
					<l n="78" num="10.6"><space unit="char" quantity="2"></space><w n="78.1">Mère</w> <w n="78.2">intraitable</w>,</l>
					<l n="79" num="10.7"><space unit="char" quantity="2"></space><w n="79.1">Ah</w> ! <w n="79.2">Sous</w> <w n="79.3">la</w> <w n="79.4">table</w></l>
					<l n="80" num="10.8"><w n="80.1">Ne</w> <w n="80.2">regardez</w> <w n="80.3">point</w>.</l>
				</lg>
				<lg n="11">
					<l n="81" num="11.1"><w n="81.1">Folle</w> <w n="81.2">et</w> <w n="81.3">tendre</w> <w n="81.4">orgie</w> !</l>
					<l n="82" num="11.2"><w n="82.1">La</w> <w n="82.2">face</w> <w n="82.3">rougie</w>,</l>
					<l n="83" num="11.3"><w n="83.1">La</w> <w n="83.2">panse</w> <w n="83.3">élargie</w>,</l>
					<l n="84" num="11.4"><w n="84.1">Là</w>, <w n="84.2">chacun</w> <w n="84.3">est</w> <w n="84.4">roi</w> ;</l>
					<l n="85" num="11.5"><w n="85.1">Et</w> <w n="85.2">quand</w> <w n="85.3">l</w>’<w n="85.4">heure</w> <w n="85.5">invite</w></l>
					<l n="86" num="11.6"><w n="86.1">À</w> <w n="86.2">gagner</w> <w n="86.3">son</w> <w n="86.4">gîte</w>,</l>
					<l n="87" num="11.7"><w n="87.1">L</w>’<w n="87.2">on</w> <w n="87.3">rentre</w> <w n="87.4">bien</w> <w n="87.5">vite</w></l>
					<l n="88" num="11.8"><w n="88.1">Ailleurs</w> <w n="88.2">que</w> <w n="88.3">chez</w> <w n="88.4">soi</w>.</l>
				</lg>
				<lg n="12">
					<l n="89" num="12.1"><space unit="char" quantity="2"></space><w n="89.1">Que</w> <w n="89.2">de</w> <w n="89.3">goguettes</w> !</l>
					<l n="90" num="12.2"><space unit="char" quantity="2"></space><w n="90.1">Que</w> <w n="90.2">d</w>’<w n="90.3">amourettes</w> !</l>
					<l n="91" num="12.3"><space unit="char" quantity="2"></space><w n="91.1">Jamais</w> <w n="91.2">de</w> <w n="91.3">dettes</w> :</l>
					<l n="92" num="12.4"><w n="92.1">Point</w> <w n="92.2">de</w> <w n="92.3">nœuds</w> <w n="92.4">constants</w>.</l>
					<l n="93" num="12.5"><space unit="char" quantity="2"></space><w n="93.1">Entre</w> <w n="93.2">l</w>’<w n="93.3">ivresse</w></l>
					<l n="94" num="12.6"><space unit="char" quantity="2"></space><w n="94.1">Et</w> <w n="94.2">la</w> <w n="94.3">paresse</w>,</l>
					<l n="95" num="12.7"><space unit="char" quantity="2"></space><w n="95.1">Notre</w> <w n="95.2">jeunesse</w></l>
					<l n="96" num="12.8"><w n="96.1">Va</w> <w n="96.2">jusqu</w>’<w n="96.3">à</w> <w n="96.4">cent</w> <w n="96.5">ans</w>.</l>
				</lg>
				<lg n="13">
					<l n="97" num="13.1"><w n="97.1">Oui</w>, <w n="97.2">dans</w> <w n="97.3">ton</w> <w n="97.4">empire</w>,</l>
					<l n="98" num="13.2"><w n="98.1">Cocagne</w>, <w n="98.2">on</w> <w n="98.3">respire</w>…</l>
					<l n="99" num="13.3"><w n="99.1">Mais</w>, <w n="99.2">qui</w> <w n="99.3">vient</w> <w n="99.4">détruire</w></l>
					<l n="100" num="13.4"><w n="100.1">Ce</w> <w n="100.2">rêve</w> <w n="100.3">enchanteur</w> ?</l>
					<l n="101" num="13.5"><w n="101.1">Ami</w>, <w n="101.2">j</w>’<w n="101.3">en</w> <w n="101.4">ai</w> <w n="101.5">honte</w> ;</l>
					<l n="102" num="13.6"><w n="102.1">C</w>’<w n="102.2">est</w> <w n="102.3">quelqu</w>’<w n="102.4">un</w> <w n="102.5">qui</w> <w n="102.6">monte</w></l>
					<l n="103" num="13.7"><w n="103.1">Apporter</w> <w n="103.2">le</w> <w n="103.3">compte</w></l>
					<l n="104" num="13.8"><w n="104.1">Du</w> <w n="104.2">restaurateur</w>.</l>
				</lg>
			</div></body></text></TEI>