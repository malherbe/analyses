<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER56">
				<head type="main">LE VOISIN</head>
				<head type="tune">AIR : Eh ! qu’est-ce que ça m’fait à moi !</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">veux</w>, <w n="1.3">voisin</w> <w n="1.4">et</w> <w n="1.5">voisine</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Quitter</w> <w n="2.2">le</w> <w n="2.3">ton</w> <w n="2.4">libertin</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">ai</w> <w n="3.3">pour</w> <w n="3.4">oncle</w> <w n="3.5">un</w> <w n="3.6">sacristain</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">pour</w> <w n="4.3">sœur</w> <w n="4.4">une</w> <w n="4.5">béguine</w>.</l>
					<l n="5" num="1.5"><space unit="char" quantity="2"></space><w n="5.1">Mais</w> <w n="5.2">le</w> <w n="5.3">diable</w> <w n="5.4">est</w> <w n="5.5">bien</w> <w n="5.6">fin</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">Qu</w>’<w n="6.2">en</w> <w n="6.3">dites</w>-<w n="6.4">vous</w>, <w n="6.5">ma</w> <w n="6.6">voisine</w> ?</l>
					<l n="7" num="1.7"><space unit="char" quantity="2"></space><w n="7.1">Mais</w> <w n="7.2">le</w> <w n="7.3">diable</w> <w n="7.4">est</w> <w n="7.5">bien</w> <w n="7.6">fin</w> ;</l>
					<l n="8" num="1.8"><w n="8.1">Qu</w>’<w n="8.2">en</w> <w n="8.3">dites</w>-<w n="8.4">vous</w>, <w n="8.5">mon</w> <w n="8.6">voisin</w> ?</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Paul</w>, <w n="9.2">docteur</w> <w n="9.3">en</w> <w n="9.4">médecine</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Craint</w> <w n="10.2">pour</w> <w n="10.3">le</w> <w n="10.4">fil</w> <w n="10.5">de</w> <w n="10.6">nos</w> <w n="10.7">jours</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Que</w> <w n="11.2">le</w> <w n="11.3">vin</w> <w n="11.4">et</w> <w n="11.5">les</w> <w n="11.6">amours</w></l>
					<l n="12" num="2.4"><w n="12.1">N</w>’<w n="12.2">usent</w> <w n="12.3">trop</w> <w n="12.4">tôt</w> <w n="12.5">la</w> <w n="12.6">bobine</w> :</l>
					<l n="13" num="2.5"><space unit="char" quantity="2"></space><w n="13.1">Eh</w> ! <w n="13.2">Fi</w> <w n="13.3">du</w> <w n="13.4">médecin</w> ;</l>
					<l n="14" num="2.6"><w n="14.1">Qu</w>’<w n="14.2">en</w> <w n="14.3">dites</w>-<w n="14.4">vous</w>, <w n="14.5">ma</w> <w n="14.6">voisine</w> ?</l>
					<l n="15" num="2.7"><space unit="char" quantity="2"></space><w n="15.1">Eh</w> ! <w n="15.2">Fi</w> <w n="15.3">du</w> <w n="15.4">médecin</w> ;</l>
					<l n="16" num="2.8"><w n="16.1">Qu</w>’<w n="16.2">en</w> <w n="16.3">dites</w>-<w n="16.4">vous</w>, <w n="16.5">mon</w> <w n="16.6">voisin</w> ?</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">L</w>’<w n="17.2">embonpoint</w> <w n="17.3">de</w> <w n="17.4">Joséphine</w></l>
					<l n="18" num="3.2"><w n="18.1">Fait</w> <w n="18.2">demander</w> <w n="18.3">ce</w> <w n="18.4">que</w> <w n="18.5">c</w>’<w n="18.6">est</w> ;</l>
					<l n="19" num="3.3"><w n="19.1">Moi</w>, <w n="19.2">je</w> <w n="19.3">crois</w> <w n="19.4">que</w> <w n="19.5">son</w> <w n="19.6">corset</w></l>
					<l n="20" num="3.4"><w n="20.1">Lui</w> <w n="20.2">rend</w> <w n="20.3">la</w> <w n="20.4">taille</w> <w n="20.5">moins</w> <w n="20.6">fine</w>.</l>
					<l n="21" num="3.5"><space unit="char" quantity="2"></space><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">l</w>’<w n="21.4">effet</w> <w n="21.5">du</w> <w n="21.6">basin</w> ;</l>
					<l n="22" num="3.6"><w n="22.1">Qu</w>’<w n="22.2">en</w> <w n="22.3">dites</w>-<w n="22.4">vous</w>, <w n="22.5">ma</w> <w n="22.6">voisine</w> ?</l>
					<l n="23" num="3.7"><space unit="char" quantity="2"></space><w n="23.1">C</w>’<w n="23.2">est</w> <w n="23.3">l</w>’<w n="23.4">effet</w> <w n="23.5">du</w> <w n="23.6">basin</w> ;</l>
					<l n="24" num="3.8"><w n="24.1">Qu</w>’<w n="24.2">en</w> <w n="24.3">dites</w>-<w n="24.4">vous</w>, <w n="24.5">mon</w> <w n="24.6">voisin</w> ?</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Mademoiselle</w> <w n="25.2">Justine</w></l>
					<l n="26" num="4.2"><w n="26.1">Met</w> <w n="26.2">au</w> <w n="26.3">monde</w> <w n="26.4">un</w> <w n="26.5">gros</w> <w n="26.6">poupon</w> :</l>
					<l n="27" num="4.3"><w n="27.1">L</w>’<w n="27.2">un</w> <w n="27.3">dit</w> <w n="27.4">que</w> <w n="27.5">c</w>’<w n="27.6">est</w> <w n="27.7">un</w> <w n="27.8">dragon</w>,</l>
					<l n="28" num="4.4"><w n="28.1">L</w>’<w n="28.2">autre</w> <w n="28.3">un</w> <w n="28.4">soldat</w> <w n="28.5">de</w> <w n="28.6">marine</w>.</l>
					<l n="29" num="4.5"><space unit="char" quantity="2"></space><w n="29.1">Je</w> <w n="29.2">le</w> <w n="29.3">crois</w> <w n="29.4">fantassin</w> ;</l>
					<l n="30" num="4.6"><w n="30.1">Qu</w>’<w n="30.2">en</w> <w n="30.3">dites</w>-<w n="30.4">vous</w>, <w n="30.5">ma</w> <w n="30.6">voisine</w> ?</l>
					<l n="31" num="4.7"><space unit="char" quantity="2"></space><w n="31.1">Je</w> <w n="31.2">le</w> <w n="31.3">crois</w> <w n="31.4">fantassin</w> ;</l>
					<l n="32" num="4.8"><w n="32.1">Qu</w>’<w n="32.2">en</w> <w n="32.3">dites</w>-<w n="32.4">vous</w>, <w n="32.5">mon</w> <w n="32.6">voisin</w> ?</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">Depuis</w> <w n="33.2">peu</w> <w n="33.3">chez</w> <w n="33.4">ma</w> <w n="33.5">cousine</w>,</l>
					<l n="34" num="5.2"><w n="34.1">Qui</w> <w n="34.2">jeûnait</w> <w n="34.3">en</w> <w n="34.4">carnaval</w>,</l>
					<l n="35" num="5.3"><w n="35.1">Je</w> <w n="35.2">vois</w> <w n="35.3">certain</w> <w n="35.4">cardinal</w>,</l>
					<l n="36" num="5.4"><w n="36.1">Et</w> <w n="36.2">trouve</w> <w n="36.3">bonne</w> <w n="36.4">cuisine</w> :</l>
					<l n="37" num="5.5"><space unit="char" quantity="2"></space><w n="37.1">Serait</w>-<w n="37.2">il</w> <w n="37.3">mon</w> <w n="37.4">cousin</w> ?</l>
					<l n="38" num="5.6"><w n="38.1">Qu</w>’<w n="38.2">en</w> <w n="38.3">dites</w>-<w n="38.4">vous</w>, <w n="38.5">ma</w> <w n="38.6">voisine</w> ?</l>
					<l n="39" num="5.7"><space unit="char" quantity="2"></space><w n="39.1">Serait</w>-<w n="39.2">il</w> <w n="39.3">mon</w> <w n="39.4">cousin</w> ?</l>
					<l n="40" num="5.8"><w n="40.1">Qu</w>’<w n="40.2">en</w> <w n="40.3">dites</w>-<w n="40.4">vous</w>, <w n="40.5">mon</w> <w n="40.6">voisin</w> ?</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">Une</w> <w n="41.2">actrice</w> <w n="41.3">qu</w>’<w n="41.4">on</w> <w n="41.5">devine</w></l>
					<l n="42" num="6.2"><w n="42.1">Veut</w>, <w n="42.2">pour</w> <w n="42.3">plaire</w> <w n="42.4">à</w> <w n="42.5">dix</w> <w n="42.6">rivaux</w>,</l>
					<l n="43" num="6.3"><w n="43.1">Inventer</w> <w n="43.2">des</w> <w n="43.3">coups</w> <w n="43.4">nouveaux</w></l>
					<l n="44" num="6.4"><w n="44.1">Au</w> <w n="44.2">doux</w> <w n="44.3">jeu</w> <w n="44.4">qui</w> <w n="44.5">les</w> <w n="44.6">ruine</w> :</l>
					<l n="45" num="6.5"><space unit="char" quantity="2"></space><w n="45.1">C</w>’<w n="45.2">est</w> <w n="45.3">un</w> <w n="45.4">fort</w> <w n="45.5">beau</w> <w n="45.6">dessein</w> ;</l>
					<l n="46" num="6.6"><w n="46.1">Qu</w>’<w n="46.2">en</w> <w n="46.3">dites</w>-<w n="46.4">vous</w>, <w n="46.5">ma</w> <w n="46.6">voisine</w> ?</l>
					<l n="47" num="6.7"><space unit="char" quantity="2"></space><w n="47.1">C</w>’<w n="47.2">est</w> <w n="47.3">un</w> <w n="47.4">fort</w> <w n="47.5">beau</w> <w n="47.6">dessein</w> ;</l>
					<l n="48" num="6.8"><w n="48.1">Qu</w>’<w n="48.2">en</w> <w n="48.3">dites</w>-<w n="48.4">vous</w>, <w n="48.5">mon</w> <w n="48.6">voisin</w> ?</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1"><w n="49.1">Faut</w>-<w n="49.2">il</w> <w n="49.3">qu</w>’<w n="49.4">une</w> <w n="49.5">affreuse</w> <w n="49.6">épine</w></l>
					<l n="50" num="7.2"><w n="50.1">Se</w> <w n="50.2">mêle</w> <w n="50.3">aux</w> <w n="50.4">fleurs</w> <w n="50.5">de</w> <w n="50.6">Cypris</w> !</l>
					<l n="51" num="7.3"><w n="51.1">Pour</w> <w n="51.2">ce</w> <w n="51.3">poison</w> <w n="51.4">de</w> <w n="51.5">Paris</w></l>
					<l n="52" num="7.4"><w n="52.1">Que</w> <w n="52.2">n</w>’<w n="52.3">est</w>-<w n="52.4">il</w> <w n="52.5">une</w> <w n="52.6">vaccine</w> !</l>
					<l n="53" num="7.5"><space unit="char" quantity="2"></space><w n="53.1">Cela</w> <w n="53.2">serait</w> <w n="53.3">divin</w> ;</l>
					<l n="54" num="7.6"><w n="54.1">Qu</w>’<w n="54.2">en</w> <w n="54.3">dites</w>-<w n="54.4">vous</w>, <w n="54.5">ma</w> <w n="54.6">voisine</w> ?</l>
					<l n="55" num="7.7"><space unit="char" quantity="2"></space><w n="55.1">Cela</w> <w n="55.2">serait</w> <w n="55.3">divin</w> ;</l>
					<l n="56" num="7.8"><w n="56.1">Qu</w>’<w n="56.2">en</w> <w n="56.3">dites</w>-<w n="56.4">vous</w>, <w n="56.5">mon</w> <w n="56.6">voisin</w> ?</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1"><w n="57.1">D</w>’<w n="57.2">aucun</w> <w n="57.3">mal</w>, <w n="57.4">je</w> <w n="57.5">l</w>’<w n="57.6">imagine</w>,</l>
					<l n="58" num="8.2"><w n="58.1">Notre</w> <w n="58.2">quartier</w> <w n="58.3">n</w>’<w n="58.4">est</w> <w n="58.5">frappé</w>.</l>
					<l n="59" num="8.3"><w n="59.1">Là</w> <w n="59.2">point</w> <w n="59.3">de</w> <w n="59.4">mari</w> <w n="59.5">trompé</w>,</l>
					<l n="60" num="8.4"><w n="60.1">Point</w> <w n="60.2">de</w> <w n="60.3">femme</w> <w n="60.4">libertine</w>.</l>
					<l n="61" num="8.5"><space unit="char" quantity="2"></space><w n="61.1">C</w>’<w n="61.2">est</w> <w n="61.3">un</w> <w n="61.4">quartier</w> <w n="61.5">fort</w> <w n="61.6">sain</w> ;</l>
					<l n="62" num="8.6"><w n="62.1">Qu</w>’<w n="62.2">en</w> <w n="62.3">dites</w>-<w n="62.4">vous</w>, <w n="62.5">ma</w> <w n="62.6">voisine</w> ?</l>
					<l n="63" num="8.7"><space unit="char" quantity="2"></space><w n="63.1">C</w>’<w n="63.2">est</w> <w n="63.3">un</w> <w n="63.4">quartier</w> <w n="63.5">fort</w> <w n="63.6">sain</w> ;</l>
				</lg>
			</div></body></text></TEI>