<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Chansons</title>
				<title type="sub_2">Tome I</title>
				<title type="medium">Une édition électronique</title>
				<author key="BER">
					<name>
						<forename>Pierre-Jean</forename>
						<nameLink>de</nameLink>
						<surname>BÉRANGER</surname>
					</name>
					<date from="1780" to="1857">1780-1857</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3963 vers92 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">BER_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Chansons</title>
						<author>Pierre-Jean De Béranger</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/pierreberanger.html</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Chansons</title>
								<author>Pierre-Jean de Béranger</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k54921665.r=pierre-jean+de+b%C3%A9ranger.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Perrotin, Libraire</publisher>
									<date when="1867">1867</date>
								</imprint>
								<biblScope unit="tome">I</biblScope>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1815">1815</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>
					Les retraits de vers ont été introduits automatiquement en fonction de la longueur métrique maximale du poème
					(application d’une transformation xsl dans le cadre du traitement métrique automatique)
				</p>
				<normalization>
					<p>Les traits d’union utilisés comme tirets ont été remplacés par des tirets demi-cadratin.</p>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-02-25" who="RR">Révision de l’entête pour validation TEI (TEI_corpus_Malherbe.xsd)</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="BER65">
				<head type="main">LE SCANDALE</head>
				<head type="tune">AIR : La farira dondaine, gai !</head>
				<lg n="1">
					<l n="1" num="1.1"><space unit="char" quantity="2"></space><w n="1.1">Aux</w> <w n="1.2">drames</w> <w n="1.3">du</w> <w n="1.4">jour</w></l>
					<l n="2" num="1.2"><space unit="char" quantity="2"></space><w n="2.1">Laissons</w> <w n="2.2">la</w> <w n="2.3">morale</w> :</l>
					<l n="3" num="1.3"><space unit="char" quantity="2"></space><w n="3.1">Sans</w> <w n="3.2">vivre</w> <w n="3.3">à</w> <w n="3.4">la</w> <w n="3.5">cour</w>,</l>
					<l n="4" num="1.4"><space unit="char" quantity="2"></space><w n="4.1">J</w>’<w n="4.2">aime</w> <w n="4.3">le</w> <w n="4.4">scandale</w>.</l>
					<l n="5" num="1.5"><space unit="char" quantity="10"></space><w n="5.1">Bon</w> !</l>
					<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">farira</w> <w n="6.3">dondaine</w>,</l>
					<l n="7" num="1.7"><space unit="char" quantity="10"></space><w n="7.1">Gai</w> !</l>
					<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">farira</w> <w n="8.3">dondé</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space unit="char" quantity="2"></space><w n="9.1">Nargue</w> <w n="9.2">des</w> <w n="9.3">vertus</w> !</l>
					<l n="10" num="2.2"><space unit="char" quantity="2"></space><w n="10.1">On</w> <w n="10.2">n</w>’<w n="10.3">en</w> <w n="10.4">sait</w> <w n="10.5">que</w> <w n="10.6">faire</w>.</l>
					<l n="11" num="2.3"><space unit="char" quantity="2"></space><w n="11.1">Aux</w> <w n="11.2">sots</w> <w n="11.3">revêtus</w></l>
					<l n="12" num="2.4"><space unit="char" quantity="2"></space><w n="12.1">Le</w> <w n="12.2">tout</w> <w n="12.3">est</w> <w n="12.4">de</w> <w n="12.5">plaire</w>.</l>
					<l n="13" num="2.5"><space unit="char" quantity="10"></space><w n="13.1">Bon</w> !</l>
					<l n="14" num="2.6"><w n="14.1">La</w> <w n="14.2">farira</w> <w n="14.3">dondaine</w>,</l>
					<l n="15" num="2.7"><space unit="char" quantity="10"></space><w n="15.1">Gai</w> !</l>
					<l n="16" num="2.8"><w n="16.1">La</w> <w n="16.2">farira</w> <w n="16.3">dondé</w>.</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><space unit="char" quantity="2"></space><w n="17.1">De</w> <w n="17.2">ses</w> <w n="17.3">contes</w> <w n="17.4">bleus</w></l>
					<l n="18" num="3.2"><space unit="char" quantity="2"></space><w n="18.1">L</w>’<w n="18.2">honneur</w> <w n="18.3">nous</w> <w n="18.4">assomme</w>.</l>
					<l n="19" num="3.3"><space unit="char" quantity="2"></space><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">un</w> <w n="19.4">vice</w> <w n="19.5">ou</w> <w n="19.6">deux</w></l>
					<l n="20" num="3.4"><space unit="char" quantity="2"></space><w n="20.1">Qui</w> <w n="20.2">font</w> <w n="20.3">l</w>’<w n="20.4">honnête</w> <w n="20.5">homme</w>.</l>
					<l n="21" num="3.5"><space unit="char" quantity="10"></space><w n="21.1">Bon</w> !</l>
					<l n="22" num="3.6"><w n="22.1">La</w> <w n="22.2">farira</w> <w n="22.3">dondaine</w>,</l>
					<l n="23" num="3.7"><space unit="char" quantity="10"></space><w n="23.1">Gai</w> !</l>
					<l n="24" num="3.8"><w n="24.1">La</w> <w n="24.2">farira</w> <w n="24.3">dondé</w>.</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><space unit="char" quantity="2"></space><w n="25.1">Pour</w> <w n="25.2">des</w> <w n="25.3">vins</w> <w n="25.4">de</w> <w n="25.5">prix</w></l>
					<l n="26" num="4.2"><space unit="char" quantity="2"></space><w n="26.1">Vendons</w> <w n="26.2">tous</w> <w n="26.3">nos</w> <w n="26.4">livres</w>.</l>
					<l n="27" num="4.3"><space unit="char" quantity="2"></space><w n="27.1">C</w>’<w n="27.2">est</w> <w n="27.3">peu</w> <w n="27.4">d</w>’<w n="27.5">être</w> <w n="27.6">gris</w> ;</l>
					<l n="28" num="4.4"><space unit="char" quantity="2"></space><w n="28.1">Amis</w>, <w n="28.2">soyons</w> <w n="28.3">ivres</w>.</l>
					<l n="29" num="4.5"><space unit="char" quantity="10"></space><w n="29.1">Bon</w> !</l>
					<l n="30" num="4.6"><w n="30.1">La</w> <w n="30.2">farira</w> <w n="30.3">dondaine</w>,</l>
					<l n="31" num="4.7"><space unit="char" quantity="10"></space><w n="31.1">Gai</w> !</l>
					<l n="32" num="4.8"><w n="32.1">La</w> <w n="32.2">farira</w> <w n="32.3">dondé</w>.</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><space unit="char" quantity="2"></space><w n="33.1">Grands</w> <w n="33.2">réformateurs</w>,</l>
					<l n="34" num="5.2"><space unit="char" quantity="2"></space><w n="34.1">Piliers</w> <w n="34.2">de</w> <w n="34.3">coulisses</w>,</l>
					<l n="35" num="5.3"><space unit="char" quantity="2"></space><w n="35.1">Chassez</w> <w n="35.2">les</w> <w n="35.3">erreurs</w> ;</l>
					<l n="36" num="5.4"><space unit="char" quantity="2"></space><w n="36.1">Nous</w> <w n="36.2">gardons</w> <w n="36.3">nos</w> <w n="36.4">vices</w>.</l>
					<l n="37" num="5.5"><space unit="char" quantity="10"></space><w n="37.1">Bon</w> !</l>
					<l n="38" num="5.6"><w n="38.1">La</w> <w n="38.2">farira</w> <w n="38.3">dondaine</w>,</l>
					<l n="39" num="5.7"><space unit="char" quantity="10"></space><w n="39.1">Gai</w> !</l>
					<l n="40" num="5.8"><w n="40.1">La</w> <w n="40.2">farira</w> <w n="40.3">dondé</w>.</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><space unit="char" quantity="2"></space><w n="41.1">Paix</w> ! <w n="41.2">Dit</w> <w n="41.3">à</w> <w n="41.4">ce</w> <w n="41.5">mot</w></l>
					<l n="42" num="6.2"><space unit="char" quantity="2"></space><w n="42.1">Caton</w>, <w n="42.2">qui</w> <w n="42.3">fait</w> <w n="42.4">rage</w> ;</l>
					<l n="43" num="6.3"><space unit="char" quantity="2"></space><w n="43.1">Mais</w> <w n="43.2">il</w> <w n="43.3">prêche</w> <w n="43.4">en</w> <w n="43.5">sot</w>,</l>
					<l n="44" num="6.4"><space unit="char" quantity="2"></space><w n="44.1">Moi</w>, <w n="44.2">je</w> <w n="44.3">ris</w> <w n="44.4">en</w> <w n="44.5">sage</w>.</l>
					<l n="45" num="6.5"><space unit="char" quantity="10"></space><w n="45.1">Bon</w> !</l>
					<l n="46" num="6.6"><w n="46.1">La</w> <w n="46.2">farira</w> <w n="46.3">dondaine</w>,</l>
					<l n="47" num="6.7"><space unit="char" quantity="10"></space><w n="47.1">Gai</w> !</l>
					<l n="48" num="6.8"><w n="48.1">La</w> <w n="48.2">farira</w> <w n="48.3">dondé</w>.</l>
				</lg>
			</div></body></text></TEI>