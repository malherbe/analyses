<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN25">
					<head type="main">Sur une voye de bois.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">PENDANT</w> <w n="1.2">ce</w> <w n="1.3">froid</w> <w n="1.4">cuisant</w>, <w n="1.5">vous</w> <w n="1.6">me</w> <w n="1.7">comblez</w> <w n="1.8">de</w> <w n="1.9">joye</w></l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">me</w> <w n="2.3">vouloir</w> <w n="2.4">ainsi</w> <w n="2.5">parer</w> <w n="2.6">de</w> <w n="2.7">sa</w> <w n="2.8">rigueur</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">quand</w> <w n="3.3">je</w> <w n="3.4">suis</w> <w n="3.5">sans</w> <w n="3.6">bois</w>, <w n="3.7">m</w>’<w n="3.8">en</w> <w n="3.9">promettre</w> <w n="3.10">une</w> <w n="3.11">voye</w>,</l>
						<l n="4" num="1.4"><w n="4.1">C</w>’<w n="4.2">est</w> <w n="4.3">une</w> <w n="4.4">douce</w> <w n="4.5">voye</w> <w n="4.6">à</w> <w n="4.7">me</w> <w n="4.8">gagner</w> <w n="4.9">le</w> <w n="4.10">cœur</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Quoique</w> <w n="5.2">je</w> <w n="5.3">ne</w> <w n="5.4">possède</w> <w n="5.5">encor</w> <w n="5.6">qu</w>’<w n="5.7">en</w> <w n="5.8">espérance</w></l>
						<l n="6" num="2.2"><w n="6.1">Un</w> <w n="6.2">trésor</w> <w n="6.3">en</w> <w n="6.4">hyver</w> <w n="6.5">si</w> <w n="6.6">doux</w> <w n="6.7">et</w> <w n="6.8">si</w> <w n="6.9">plaisant</w>,</l>
						<l n="7" num="2.3"><w n="7.1">J</w>’<w n="7.2">en</w> <w n="7.3">ressens</w> <w n="7.4">toutefois</w> <w n="7.5">des</w> <w n="7.6">effets</w> <w n="7.7">par</w> <w n="7.8">avance</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">l</w>’<w n="8.3">offre</w> <w n="8.4">me</w> <w n="8.5">réchauffe</w> <w n="8.6">au</w> <w n="8.7">défaut</w> <w n="8.8">du</w> <w n="8.9">présent</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">sçay</w> <w n="9.3">que</w>, <w n="9.4">l</w>’<w n="9.5">acceptant</w>, <w n="9.6">ma</w> <w n="9.7">honte</w> <w n="9.8">est</w> <w n="9.9">évidente</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">qu</w>’<w n="10.3">un</w> <w n="10.4">autre</w> <w n="10.5">que</w> <w n="10.6">moy</w> <w n="10.7">seroit</w> <w n="10.8">plus</w> <w n="10.9">circonspect</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Mais</w> <w n="11.2">j</w>’<w n="11.3">avouë</w> <w n="11.4">à</w> <w n="11.5">vos</w> <w n="11.6">pieds</w>, <w n="11.7">aimable</w> <w n="11.8">Présidente</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Que</w> <w n="12.2">je</w> <w n="12.3">tremble</w> <w n="12.4">de</w> <w n="12.5">froid</w> <w n="12.6">autant</w> <w n="12.7">que</w> <w n="12.8">de</w> <w n="12.9">respect</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Un</w> <w n="13.2">amour</w> <w n="13.3">effectif</w> <w n="13.4">en</w> <w n="13.5">mon</w> <w n="13.6">âme</w> <w n="13.7">préside</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Qui</w> <w n="14.2">tient</w> <w n="14.3">la</w> <w n="14.4">bagatelle</w> <w n="14.5">indigne</w> <w n="14.6">de</w> <w n="14.7">ses</w> <w n="14.8">vœux</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">c</w>’<w n="15.3">est</w> <w n="15.4">bien</w>, <w n="15.5">ce</w> <w n="15.6">me</w> <w n="15.7">semble</w>, <w n="15.8">aller</w> <w n="15.9">droit</w> <w n="15.10">au</w> <w n="15.11">solide</w></l>
						<l n="16" num="4.4"><w n="16.1">Que</w> <w n="16.2">prendre</w> <w n="16.3">des</w> <w n="16.4">cottrets</w> <w n="16.5">plutôt</w> <w n="16.6">que</w> <w n="16.7">des</w> <w n="16.8">cheveux</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Pour</w> <w n="17.2">un</w> <w n="17.3">si</w> <w n="17.4">grand</w> <w n="17.5">bien</w>-<w n="17.6">fait</w>, <w n="17.7">dont</w> <w n="17.8">je</w> <w n="17.9">m</w>’<w n="17.10">efforce</w> <w n="17.11">d</w>’<w n="17.12">être</w></l>
						<l n="18" num="5.2"><w n="18.1">Reconnoissant</w> <w n="18.2">vers</w> <w n="18.3">vous</w> <w n="18.4">autant</w> <w n="18.5">que</w> <w n="18.6">je</w> <w n="18.7">le</w> <w n="18.8">puis</w>,</l>
						<l n="19" num="5.3"><w n="19.1">J</w>’<w n="19.2">en</w> <w n="19.3">useray</w> <w n="19.4">des</w> <w n="19.5">mieux</w>, <w n="19.6">et</w> <w n="19.7">feray</w> <w n="19.8">bien</w> <w n="19.9">connoître</w></l>
						<l n="20" num="5.4"><w n="20.1">De</w> <w n="20.2">quel</w> <w n="20.3">bois</w> <w n="20.4">je</w> <w n="20.5">me</w> <w n="20.6">chauffe</w>, <w n="20.7">et</w> <w n="20.8">quel</w> <w n="20.9">homme</w> <w n="20.10">je</w> <w n="20.11">suis</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">À</w> <w n="21.2">tous</w> <w n="21.3">autres</w> <w n="21.4">objets</w> <w n="21.5">je</w> <w n="21.6">feray</w> <w n="21.7">banqueroute</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Mes</w> <w n="22.2">flâmes</w> <w n="22.3">brûleront</w> <w n="22.4">sous</w> <w n="22.5">vôtre</w> <w n="22.6">digne</w> <w n="22.7">aveu</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">vous</w> <w n="23.3">n</w>’<w n="23.4">aurez</w> <w n="23.5">pas</w> <w n="23.6">lieu</w> <w n="23.7">de</w> <w n="23.8">révoquer</w> <w n="23.9">en</w> <w n="23.10">doute</w></l>
						<l n="24" num="6.4"><w n="24.1">Que</w> <w n="24.2">vôtre</w> <w n="24.3">seule</w> <w n="24.4">grâce</w> <w n="24.5">ait</w> <w n="24.6">allumé</w> <w n="24.7">mon</w> <w n="24.8">feu</w> ;</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Qu</w>’<w n="25.2">auprés</w> <w n="25.3">de</w> <w n="25.4">vos</w> <w n="25.5">tisons</w>, <w n="25.6">d</w>’<w n="25.7">une</w> <w n="25.8">veine</w> <w n="25.9">ampoullée</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Pour</w> <w n="26.2">vous</w> <w n="26.3">je</w> <w n="26.4">traceray</w> <w n="26.5">des</w> <w n="26.6">vers</w> <w n="26.7">nobles</w> <w n="26.8">et</w> <w n="26.9">hauts</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">Car</w> <w n="27.2">il</w> <w n="27.3">n</w>’<w n="27.4">est</w> <w n="27.5">rien</w> <w n="27.6">si</w> <w n="27.7">doux</w>, <w n="27.8">au</w> <w n="27.9">fort</w> <w n="27.10">de</w> <w n="27.11">la</w> <w n="27.12">gelée</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Que</w> <w n="28.2">de</w> <w n="28.3">songer</w> <w n="28.4">en</w> <w n="28.5">vous</w> <w n="28.6">quand</w> <w n="28.7">on</w> <w n="28.8">a</w> <w n="28.9">les</w> <w n="28.10">pieds</w> <w n="28.11">chauds</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Tenez</w>-<w n="29.2">moy</w> <w n="29.3">donc</w> <w n="29.4">parole</w>, <w n="29.5">et</w> <w n="29.6">vous</w> <w n="29.7">donnez</w> <w n="29.8">la</w> <w n="29.9">peine</w></l>
						<l n="30" num="8.2"><w n="30.1">D</w>’<w n="30.2">envoyer</w>, <w n="30.3">s</w>’<w n="30.4">il</w> <w n="30.5">vous</w> <w n="30.6">plaist</w>, <w n="30.7">vos</w> <w n="30.8">faveurs</w> <w n="30.9">jusqu</w>’<w n="30.10">icy</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">songez</w> <w n="31.3">qu</w>’<w n="31.4">il</w> <w n="31.5">en</w> <w n="31.6">faut</w> <w n="31.7">une</w> <w n="31.8">charette</w> <w n="31.9">pleine</w></l>
						<l n="32" num="8.4"><w n="32.1">Pour</w> <w n="32.2">le</w> <w n="32.3">soulagement</w> <w n="32.4">d</w>’<w n="32.5">un</w> <w n="32.6">amoureux</w> <w n="32.7">transy</w>.</l>
					</lg>
				</div></body></text></TEI>