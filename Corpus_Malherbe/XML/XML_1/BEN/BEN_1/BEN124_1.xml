<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CY GIST</head><head type="sub_part">
					OU 
					Diverses Épitaphes pour toute sorte de personnes 
					de l’un et de l’autre sexe, et pour l’Auteur même, 
					quoique vivant.
				</head><div type="poem" key="BEN124">
					<head type="main">Épitaphe d’un Commis.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Cy</w> <w n="1.2">gist</w> <w n="1.3">le</w> <w n="1.4">beau</w> <w n="1.5">Commis</w> <w n="1.6">d</w>’<w n="1.7">un</w> <w n="1.8">vieil</w> <w n="1.9">Homme</w> <w n="1.10">d</w>’<w n="1.11">affaire</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Possesseur</w> <w n="2.2">d</w>’<w n="2.3">un</w> <w n="2.4">trésor</w> <w n="2.5">dont</w> <w n="2.6">il</w> <w n="2.7">n</w>’<w n="2.8">avoit</w> <w n="2.9">que</w> <w n="2.10">faire</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">À</w> <w n="3.2">l</w>’<w n="3.3">honneur</w> <w n="3.4">de</w> <w n="3.5">son</w> <w n="3.6">Maître</w> <w n="3.7">il</w> <w n="3.8">attenta</w>, <w n="3.9">dit</w>-<w n="3.10">on</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">petit</w> <w n="4.3">poil</w> <w n="4.4">folet</w> <w n="4.5">ombrageoit</w> <w n="4.6">son</w> <w n="4.7">menton</w>,</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">Et</w> <w n="5.2">l</w>’<w n="5.3">autre</w> <w n="5.4">avoit</w> <w n="5.5">la</w> <w n="5.6">barbe</w> <w n="5.7">grise</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">L</w>’<w n="6.2">un</w> <w n="6.3">étoit</w> <w n="6.4">un</w> <w n="6.5">badin</w>, <w n="6.6">l</w>’<w n="6.7">autre</w> <w n="6.8">étoit</w> <w n="6.9">un</w> <w n="6.10">Caton</w>.</l>
						<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">Épouse</w>, <w n="7.3">jeune</w> <w n="7.4">et</w> <w n="7.5">fière</w>, <w n="7.6">en</w> <w n="7.7">parut</w> <w n="7.8">fort</w> <w n="7.9">surprise</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">Avec</w> <w n="8.2">elle</w> <w n="8.3">il</w> <w n="8.4">osa</w> <w n="8.5">le</w> <w n="8.6">prendre</w> <w n="8.7">sur</w> <w n="8.8">le</w> <w n="8.9">ton</w></l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">D</w>’<w n="9.2">un</w> <w n="9.3">Icare</w> <w n="9.4">et</w> <w n="9.5">d</w>’<w n="9.6">un</w> <w n="9.7">Phaéton</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Hormis</w> <w n="10.2">qu</w>’<w n="10.3">il</w> <w n="10.4">vint</w> <w n="10.5">à</w> <w n="10.6">bout</w> <w n="10.7">de</w> <w n="10.8">sa</w> <w n="10.9">haute</w> <w n="10.10">entreprise</w>.</l>
					</lg>
				</div></body></text></TEI>