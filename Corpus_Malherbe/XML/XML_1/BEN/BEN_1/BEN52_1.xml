<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN52">
					<head type="main">Sur la mort du Perroquet <lb></lb>de Madame du Plessis-Bellièvre.</head>
					<head type="form">SONNET EN BOUTS-RIMEZ.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">PHILIS</w>, <w n="1.2">contre</w> <w n="1.3">la</w> <w n="1.4">Mort</w> <w n="1.5">vainement</w> <w n="1.6">on</w> ── <w n="1.7">chicane</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Tôt</w> <w n="2.2">ou</w> <w n="2.3">tard</w> <w n="2.4">qui</w> <w n="2.5">s</w>’<w n="2.6">y</w> <w n="2.7">joue</w> <w n="2.8">est</w> <w n="2.9">fait</w> <w n="2.10">pic</w> <w n="2.11">ou</w> ── <w n="2.12">capot</w>,</l>
						<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">a</w> <w n="3.3">beau</w> <w n="3.4">quelque</w>-<w n="3.5">temps</w> <w n="3.6">tourner</w> <w n="3.7">autour</w> <w n="3.8">du</w> ── <w n="3.9">pot</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">Médecin</w> <w n="4.3">lui</w>-<w n="4.4">même</w> <w n="4.5">y</w> <w n="4.6">laisse</w> <w n="4.7">sa</w> ── <w n="4.8">soûtane</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Nous</w> <w n="5.2">verrions</w> <w n="5.3">vôtre</w> <w n="5.4">cœur</w>, <w n="5.5">s</w>’<w n="5.6">il</w> <w n="5.7">étoit</w> ── <w n="5.8">diaphane</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Du</w> <w n="6.2">deuil</w> <w n="6.3">d</w>’<w n="6.4">un</w> <w n="6.5">Perroquet</w>, <w n="6.6">noirci</w> <w n="6.7">comme</w> <w n="6.8">un</w> ── <w n="6.9">tripot</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Rohan</w> <w n="7.2">ne</w> <w n="7.3">pouvoit</w> <w n="7.4">pas</w> <w n="7.5">plus</w> <w n="7.6">plaindre</w> <w n="7.7">son</w> ── <w n="7.8">Chabot</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">quant</w> <w n="8.3">à</w> <w n="8.4">vos</w> <w n="8.5">soupirs</w>, <w n="8.6">ce</w> <w n="8.7">sujet</w> <w n="8.8">les</w> ── <w n="8.9">profane</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Croyez</w>-<w n="9.2">vous</w> <w n="9.3">qu</w>’<w n="9.4">en</w> <w n="9.5">pleurant</w> <w n="9.6">tout</w> <w n="9.7">plein</w> <w n="9.8">un</w> ── <w n="9.9">coquemart</w>,</l>
						<l n="10" num="3.2"><w n="10.1">On</w> <w n="10.2">ramène</w> <w n="10.3">un</w> <w n="10.4">Héros</w>, <w n="10.5">non</w> <w n="10.6">plus</w> <w n="10.7">qu</w>’<w n="10.8">un</w> ── <w n="10.9">Jaquemart</w> ?</l>
						<l n="11" num="3.3"><w n="11.1">C</w>’<w n="11.2">est</w> <w n="11.3">vouloir</w> <w n="11.4">prendre</w> <w n="11.5">enfin</w> <w n="11.6">le</w> <w n="11.7">grand</w> <w n="11.8">Turc</w> <w n="11.9">à</w> <w n="11.10">la</w> ── <w n="11.11">barbe</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Si</w> <w n="12.2">les</w> <w n="12.3">pleurs</w> <w n="12.4">réparoient</w> <w n="12.5">de</w> <w n="12.6">semblables</w> ── <w n="12.7">débris</w>,</l>
						<l n="13" num="4.2"><w n="13.1">L</w>’<w n="13.2">on</w> <w n="13.3">eût</w> <w n="13.4">fait</w> <w n="13.5">revenir</w>, <w n="13.6">le</w> <w n="13.7">jour</w> <w n="13.8">de</w> <w n="13.9">Sainte</w> ── <w n="13.10">Barbe</w>,</l>
						<l n="14" num="4.3"><w n="14.1">L</w>’<w n="14.2">âme</w> <w n="14.3">de</w> <w n="14.4">Richelieu</w> <w n="14.5">du</w> <w n="14.6">céleste</w> ── <w n="14.7">lambris</w>.</l>
					</lg>
				</div></body></text></TEI>