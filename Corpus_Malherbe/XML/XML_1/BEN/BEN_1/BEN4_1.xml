<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS SUR LA BEAUTÉ ET SUR LA LAIDEUR</head><div type="poem" key="BEN4">
					<head type="main">Sur la Beauté.</head>
					<head type="form">SONNET III.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">BEAUX</w> <w n="1.2">yeux</w> <w n="1.3">dont</w> <w n="1.4">l</w>’<w n="1.5">atteinte</w> <w n="1.6">profonde</w></l>
						<l n="2" num="1.2"><w n="2.1">Trouble</w> <w n="2.2">des</w> <w n="2.3">cœurs</w> <w n="2.4">incessamment</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">doux</w> <w n="3.3">repos</w> <w n="3.4">qui</w> <w n="3.5">ne</w> <w n="3.6">se</w> <w n="3.7">fonde</w></l>
						<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">sur</w> <w n="4.3">un</w> <w n="4.4">si</w> <w n="4.5">doux</w> <w n="4.6">mouvement</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">De</w> <w n="5.2">tout</w> <w n="5.3">ce</w> <w n="5.4">qu</w>’<w n="5.5">on</w> <w n="5.6">dit</w> <w n="5.7">en</w> <w n="5.8">aimant</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Beaux</w> <w n="6.2">yeux</w>, <w n="6.3">source</w> <w n="6.4">vive</w> <w n="6.5">et</w> <w n="6.6">féconde</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Beau</w> <w n="7.2">refrain</w>, <w n="7.3">doux</w> <w n="7.4">commencement</w></l>
						<l n="8" num="2.4"><w n="8.1">Des</w> <w n="8.2">plus</w> <w n="8.3">belles</w> <w n="8.4">chansons</w> <w n="8.5">du</w> <w n="8.6">monde</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Beaux</w> <w n="9.2">yeux</w> <w n="9.3">qui</w> <w n="9.4">sur</w> <w n="9.5">les</w> <w n="9.6">cœurs</w> <w n="9.7">avez</w></l>
						<l n="10" num="3.2"><w n="10.1">Tant</w> <w n="10.2">de</w> <w n="10.3">puissance</w>, <w n="10.4">et</w> <w n="10.5">qui</w> <w n="10.6">sçavez</w></l>
						<l n="11" num="3.3"><w n="11.1">Si</w> <w n="11.2">bien</w> <w n="11.3">jouer</w> <w n="11.4">de</w> <w n="11.5">la</w> <w n="11.6">prunelle</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Beaux</w> <w n="12.2">yeux</w>, <w n="12.3">divin</w> <w n="12.4">charme</w> <w n="12.5">des</w> <w n="12.6">sens</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Vôtre</w> <w n="13.2">amour</w> <w n="13.3">est</w> <w n="13.4">en</w> <w n="13.5">sentinelle</w></l>
						<l n="14" num="4.3"><w n="14.1">Pour</w> <w n="14.2">attraper</w> <w n="14.3">tous</w> <w n="14.4">les</w> <w n="14.5">passans</w>.</l>
					</lg>
				</div></body></text></TEI>