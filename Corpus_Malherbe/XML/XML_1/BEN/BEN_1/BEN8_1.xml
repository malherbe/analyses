<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">SONNETS SUR LA BEAUTÉ ET SUR LA LAIDEUR</head><div type="poem" key="BEN8">
					<head type="main">Sur la Beauté.</head>
					<head type="form">SONNET VII.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">TEINT</w> <w n="1.2">d</w>’<w n="1.3">une</w> <w n="1.4">merveille</w> <w n="1.5">naissante</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Teint</w> <w n="2.2">d</w>’<w n="2.3">où</w> <w n="2.4">rejaillit</w> <w n="2.5">sur</w> <w n="2.6">ses</w> <w n="2.7">pas</w></l>
						<l n="3" num="1.3"><w n="3.1">Une</w> <w n="3.2">lumière</w> <w n="3.3">éblouïssante</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Teint</w> <w n="4.2">qui</w> <w n="4.3">couronne</w> <w n="4.4">tant</w> <w n="4.5">d</w>’<w n="4.6">appas</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Teint</w> <w n="5.2">de</w> <w n="5.3">fraîcheur</w> <w n="5.4">réjoüissante</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Teint</w> <w n="6.2">vif</w>, <w n="6.3">et</w> <w n="6.4">des</w> <w n="6.5">plus</w> <w n="6.6">délicas</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Teint</w> <w n="7.2">de</w> <w n="7.3">jeunesse</w> <w n="7.4">appétissante</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Teint</w> <w n="8.2">qui</w> <w n="8.3">fait</w> <w n="8.4">par</w> <w n="8.5">tout</w> <w n="8.6">du</w> <w n="8.7">fracas</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Teint</w> <w n="9.2">qui</w> <w n="9.3">les</w> <w n="9.4">autres</w> <w n="9.5">teints</w> <w n="9.6">surpasse</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Teint</w> <w n="10.2">qui</w>, <w n="10.3">du</w> <w n="10.4">moment</w> <w n="10.5">qu</w>’<w n="10.6">elle</w> <w n="10.7">passe</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Rend</w> <w n="11.2">tous</w> <w n="11.3">les</w> <w n="11.4">chemins</w> <w n="11.5">embellis</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Teint</w> <w n="12.2">pur</w> <w n="12.3">où</w> <w n="12.4">l</w>’<w n="12.5">incarnat</w> <w n="12.6">des</w> <w n="12.7">roses</w></l>
						<l n="13" num="4.2"><w n="13.1">Se</w> <w n="13.2">mêle</w> <w n="13.3">à</w> <w n="13.4">la</w> <w n="13.5">blancheur</w> <w n="13.6">des</w> <w n="13.7">lys</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">confond</w> <w n="14.3">les</w> <w n="14.4">plus</w> <w n="14.5">belles</w> <w n="14.6">choses</w>.</l>
					</lg>
				</div></body></text></TEI>