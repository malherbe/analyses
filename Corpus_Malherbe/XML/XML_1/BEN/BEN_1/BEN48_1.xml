<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN48">
					<head type="main">Pour la même qui me fit dire qu’elle avoit la fièvre,</head>
					<head type="sub_1">et quelle ne me pouvoit voir.</head>
					<head type="form">SONNET.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">SANS</w> <w n="1.2">vous</w> <w n="1.3">tâter</w> <w n="1.4">le</w> <w n="1.5">poulx</w>, <w n="1.6">et</w> <w n="1.7">sans</w> <w n="1.8">voir</w> <w n="1.9">au</w> <w n="1.10">bassin</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ingrate</w>, <w n="2.2">je</w> <w n="2.3">sçay</w> <w n="2.4">trop</w> <w n="2.5">ce</w> <w n="2.6">qui</w> <w n="2.7">vous</w> <w n="2.8">rend</w> <w n="2.9">malade</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Le</w> <w n="3.2">quinquina</w> <w n="3.3">pour</w> <w n="3.4">vous</w>, <w n="3.5">est</w> <w n="3.6">un</w> <w n="3.7">remède</w> <w n="3.8">fade</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">connois</w> <w n="4.3">vôtre</w> <w n="4.4">fièvre</w>, <w n="4.5">et</w> <w n="4.6">vôtre</w> <w n="4.7">médecin</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Vous</w> <w n="5.2">luy</w> <w n="5.3">pardonnerez</w>, <w n="5.4">fût</w>-<w n="5.5">il</w> <w n="5.6">vôtre</w> <w n="5.7">assassin</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Mais</w> <w n="6.2">vous</w> <w n="6.3">ne</w> <w n="6.4">voulez</w> <w n="6.5">pas</w> <w n="6.6">qu</w>’<w n="6.7">on</w> <w n="6.8">se</w> <w n="6.9">le</w> <w n="6.10">persuade</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">quand</w> <w n="7.3">il</w> <w n="7.4">vous</w> <w n="7.5">dérobe</w> <w n="7.6">un</w> <w n="7.7">soupir</w>, <w n="7.8">une</w> <w n="7.9">œillade</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Vous</w> <w n="8.2">êtes</w> <w n="8.3">la</w> <w n="8.4">première</w> <w n="8.5">à</w> <w n="8.6">cacher</w> <w n="8.7">son</w> <w n="8.8">larcin</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">luy</w> <w n="9.4">qui</w> <w n="9.5">cause</w> <w n="9.6">en</w> <w n="9.7">vous</w> <w n="9.8">une</w> <w n="9.9">langueur</w> <w n="9.10">secrète</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Vainement</w> <w n="10.2">avec</w> <w n="10.3">moy</w> <w n="10.4">vous</w> <w n="10.5">faites</w> <w n="10.6">la</w> <w n="10.7">discrète</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Mais</w> <w n="11.2">sur</w> <w n="11.3">cette</w> <w n="11.4">langueur</w>, <w n="11.5">que</w> <w n="11.6">ne</w> <w n="11.7">suis</w>-<w n="11.8">je</w> <w n="11.9">en</w> <w n="11.10">repos</w> ?</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Que</w> <w n="12.2">m</w>’<w n="12.3">importe</w>, <w n="12.4">que</w> <w n="12.5">tel</w> <w n="12.6">ou</w> <w n="12.7">tel</w> <w n="12.8">y</w> <w n="12.9">remédie</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Pourquoy</w> <w n="13.2">m</w>’<w n="13.3">embarrasser</w> <w n="13.4">icy</w> <w n="13.5">mal</w> <w n="13.6">à</w> <w n="13.7">propos</w></l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">de</w> <w n="14.3">la</w> <w n="14.4">médecine</w>, <w n="14.5">et</w> <w n="14.6">de</w> <w n="14.7">la</w> <w n="14.8">maladie</w> ?</l>
					</lg>
				</div></body></text></TEI>