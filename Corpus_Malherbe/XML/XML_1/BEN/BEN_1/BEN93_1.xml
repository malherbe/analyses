<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN93">
					<head type="form">MADRIGAL.</head>
					<head type="main">Pour Mademoiselle.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">AVEC</w> <w n="1.2">impatience</w> <w n="1.3">un</w> <w n="1.4">grand</w> <w n="1.5">Roy</w> <w n="1.6">vous</w> <w n="1.7">attend</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Vous</w> <w n="2.2">êtes</w>, <w n="2.3">vû</w> <w n="2.4">le</w> <w n="2.5">tems</w>, <w n="2.6">honnêtement</w> <w n="2.7">pourveuë</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Devant</w> <w n="3.2">que</w> <w n="3.3">de</w> <w n="3.4">vous</w> <w n="3.5">voir</w>, <w n="3.6">puisqu</w>’<w n="3.7">il</w> <w n="3.8">vous</w> <w n="3.9">aime</w> <w n="3.10">tant</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Que</w> <w n="4.2">sera</w>-<w n="4.3">ce</w>, <w n="4.4">bon</w> <w n="4.5">Dieu</w>, <w n="4.6">quand</w> <w n="4.7">il</w> <w n="4.8">vous</w> <w n="4.9">aura</w> <w n="4.10">veuë</w> ?</l>
						<l n="5" num="1.5"><w n="5.1">Ah</w> ! <w n="5.2">qu</w>’<w n="5.3">il</w> <w n="5.4">soupçonne</w> <w n="5.5">en</w> <w n="5.6">vous</w> <w n="5.7">de</w> <w n="5.8">charmes</w> <w n="5.9">inconnus</w> !</l>
						<l n="6" num="1.6"><w n="6.1">Princesse</w>, <w n="6.2">où</w> <w n="6.3">n</w>’<w n="6.4">ira</w> <w n="6.5">point</w> <w n="6.6">pour</w> <w n="6.7">vous</w> <w n="6.8">sa</w> <w n="6.9">complaisance</w> ?</l>
						<l n="7" num="1.7"><w n="7.1">Lorsque</w> <w n="7.2">vous</w> <w n="7.3">vous</w> <w n="7.4">serez</w> <w n="7.5">tous</w> <w n="7.6">deux</w> <w n="7.7">entretenus</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Que</w> <w n="8.2">ne</w> <w n="8.3">produiront</w> <w n="8.4">point</w> <w n="8.5">des</w> <w n="8.6">doutes</w>, <w n="8.7">soûtenus</w></l>
						<l n="9" num="1.9"><space unit="char" quantity="12"></space><w n="9.1">D</w>’<w n="9.2">une</w> <w n="9.3">telle</w> <w n="9.4">présence</w> ?</l>
					</lg>
				</div></body></text></TEI>