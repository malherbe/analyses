<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE CY GIST</head><head type="sub_part">
					OU 
					Diverses Épitaphes pour toute sorte de personnes 
					de l’un et de l’autre sexe, et pour l’Auteur même, 
					quoique vivant.
				</head><div type="poem" key="BEN123">
					<head type="main">Épitaphe de Mahomet IV.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Cy</w> <w n="1.2">gist</w> <w n="1.3">le</w> <w n="1.4">Grand</w> <w n="1.5">Seigneur</w> <w n="1.6">qui</w> <w n="1.7">fut</w> <w n="1.8">dépossédé</w> :</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">N</w>’<w n="2.2">est</w>-<w n="2.3">ce</w> <w n="2.4">pas</w> <w n="2.5">être</w> <w n="2.6">décédé</w> ?</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Il</w> <w n="3.2">crut</w> <w n="3.3">avoir</w> <w n="3.4">bien</w> <w n="3.5">fait</w> <w n="3.6">des</w> <w n="3.7">siennes</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">D</w>’<w n="4.2">étrangler</w> <w n="4.3">son</w> <w n="4.4">premier</w> <w n="4.5">Visir</w> ;</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space><w n="5.1">De</w> <w n="5.2">noyer</w> <w n="5.3">ses</w> <w n="5.4">chiens</w> <w n="5.5">et</w> <w n="5.6">ses</w> <w n="5.7">chiennes</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">D</w>’<w n="6.2">avoir</w> <w n="6.3">tant</w> <w n="6.4">pris</w> <w n="6.5">sur</w> <w n="6.6">son</w> <w n="6.7">plaisir</w> :</l>
						<l n="7" num="1.7"><space unit="char" quantity="8"></space><w n="7.1">Est</w>-<w n="7.2">il</w> <w n="7.3">cruauté</w> <w n="7.4">qu</w>’<w n="7.5">il</w> <w n="7.6">n</w>’<w n="7.7">ait</w> <w n="7.8">faite</w>,</l>
						<l n="8" num="1.8"><space unit="char" quantity="8"></space><w n="8.1">Afin</w> <w n="8.2">d</w>’<w n="8.3">apaiser</w> <w n="8.4">son</w> <w n="8.5">Prophète</w> ?</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space><w n="9.1">Il</w> <w n="9.2">s</w>’<w n="9.3">attendoit</w>, <w n="9.4">après</w> <w n="9.5">cela</w>,</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">À</w> <w n="10.2">vaincre</w> <w n="10.3">tout</w>, <w n="10.4">et</w> <w n="10.5">le</w> <w n="10.6">voilà</w>.</l>
					</lg>
				</div></body></text></TEI>