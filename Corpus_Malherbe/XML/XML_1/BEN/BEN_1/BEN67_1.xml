<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN67">
					<head type="main">Glose de Monsieur Sarrasin, <lb></lb>sur le Sonnet de M. de Benserade à M. Esprit.</head>
					<head type="form">STANCES.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">MONSIEUR</w> <w n="1.2">Esprit</w>, <w n="1.3">de</w> <w n="1.4">l</w>’<w n="1.5">Oratoire</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Vous</w> <w n="2.2">agissez</w> <w n="2.3">en</w> <w n="2.4">homme</w> <w n="2.5">saint</w>,</l>
						<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">couronner</w> <w n="3.3">avecque</w> <w n="3.4">gloire</w></l>
						<l n="4" num="1.4"><hi rend="ital"><w n="4.1">Job</w> <w n="4.2">de</w> <w n="4.3">mille</w> <w n="4.4">tourmens</w> <w n="4.5">atteint</w></hi>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">L</w>’<w n="5.2">Ombre</w> <w n="5.3">de</w> <w n="5.4">Voiture</w> <w n="5.5">en</w> <w n="5.6">fait</w> <w n="5.7">bruit</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">s</w>’<w n="6.3">étant</w> <w n="6.4">enfin</w> <w n="6.5">résoluë</w></l>
						<l n="7" num="2.3"><w n="7.1">De</w> <w n="7.2">vous</w> <w n="7.3">aller</w> <w n="7.4">voir</w> <w n="7.5">cette</w> <w n="7.6">nuit</w>,</l>
						<l n="8" num="2.4"><hi rend="ital"><w n="8.1">Vous</w> <w n="8.2">rendra</w> <w n="8.3">sa</w> <w n="8.4">douleur</w> <w n="8.5">connuë</w></hi>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">C</w>’<w n="9.2">est</w> <w n="9.3">une</w> <w n="9.4">assez</w> <w n="9.5">fâcheuse</w> <w n="9.6">vûë</w>,</l>
						<l n="10" num="3.2"><w n="10.1">La</w> <w n="10.2">nuit</w>, <w n="10.3">qu</w>’<w n="10.4">une</w> <w n="10.5">ombre</w> <w n="10.6">qui</w> <w n="10.7">se</w> <w n="10.8">plaint</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Vôtre</w> <w n="11.2">esprit</w> <w n="11.3">craint</w> <w n="11.4">cette</w> <w n="11.5">venuë</w>,</l>
						<l n="12" num="3.4"><hi rend="ital"><w n="12.1">Et</w> <w n="12.2">raisonnablement</w> <w n="12.3">il</w> <w n="12.4">craint</w></hi>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Pour</w> <w n="13.2">l</w>’<w n="13.3">appaiser</w>, <w n="13.4">d</w>’<w n="13.5">un</w> <w n="13.6">ton</w> <w n="13.7">fort</w> <w n="13.8">doux</w></l>
						<l n="14" num="4.2"><w n="14.1">Dites</w> : <w n="14.2">J</w>’<w n="14.3">ay</w> <w n="14.4">fait</w> <w n="14.5">une</w> <w n="14.6">bévuë</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">je</w> <w n="15.3">vous</w> <w n="15.4">conjure</w> <w n="15.5">à</w> <w n="15.6">genoux</w></l>
						<l n="16" num="4.4"><hi rend="ital"><w n="16.1">Que</w> <w n="16.2">vous</w> <w n="16.3">n</w>’<w n="16.4">en</w> <w n="16.5">soyez</w> <w n="16.6">point</w> <w n="16.7">émûë</w></hi>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Mettez</w>, <w n="17.2">mettez</w> <w n="17.3">vôtre</w> <w n="17.4">bonnet</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Reprendra</w> <w n="18.2">l</w>’<w n="18.3">Ombre</w>, <w n="18.4">et</w> <w n="18.5">sans</w> <w n="18.6">berluë</w></l>
						<l n="19" num="5.3"><w n="19.1">Examinez</w> <w n="19.2">ce</w> <w n="19.3">beau</w> <w n="19.4">sonnet</w>,</l>
						<l n="20" num="5.4"><hi rend="ital"><w n="20.1">Vous</w> <w n="20.2">verrez</w> <w n="20.3">sa</w> <w n="20.4">misère</w> <w n="20.5">nuë</w></hi>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Diriez</w>-<w n="21.2">vous</w>, <w n="21.3">voyant</w> <w n="21.4">Job</w> <w n="21.5">malade</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">Benserade</w> <w n="22.3">en</w> <w n="22.4">son</w> <w n="22.5">beau</w> <w n="22.6">teint</w> :</l>
						<l n="23" num="6.3"><w n="23.1">Ces</w> <w n="23.2">vers</w> <w n="23.3">sont</w> <w n="23.4">faits</w> <w n="23.5">pour</w> <w n="23.6">Benserade</w>,</l>
						<l n="24" num="6.4"><hi rend="ital"><w n="24.1">Il</w> <w n="24.2">s</w>’<w n="24.3">est</w> <w n="24.4">luy</w>-<w n="24.5">même</w> <w n="24.6">ici</w> <w n="24.7">dépeint</w></hi> ?</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Quoy</w>, <w n="25.2">vous</w> <w n="25.3">tremblez</w>, <w n="25.4">Monsieur</w> <w n="25.5">Esprit</w> !</l>
						<l n="26" num="7.2"><w n="26.1">Avez</w>-<w n="26.2">vous</w> <w n="26.3">peur</w> <w n="26.4">que</w> <w n="26.5">je</w> <w n="26.6">vous</w> <w n="26.7">tuë</w> ?</l>
						<l n="27" num="7.3"><w n="27.1">De</w> <w n="27.2">Voiture</w> <w n="27.3">qui</w> <w n="27.4">vous</w> <w n="27.5">chérit</w></l>
						<l n="28" num="7.4"><hi rend="ital"><w n="28.1">Accoutumez</w>-<w n="28.2">vous</w> <w n="28.3">à</w> <w n="28.4">la</w> <w n="28.5">vûë</w></hi>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Qu</w>’<w n="29.2">ay</w>-<w n="29.3">je</w> <w n="29.4">dit</w> <w n="29.5">qui</w> <w n="29.6">vous</w> <w n="29.7">peut</w> <w n="29.8">surprendre</w></l>
						<l n="30" num="8.2"><w n="30.1">Et</w> <w n="30.2">faire</w> <w n="30.3">pâlir</w> <w n="30.4">votre</w> <w n="30.5">teint</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">que</w> <w n="31.3">deviez</w>-<w n="31.4">vous</w> <w n="31.5">moins</w> <w n="31.6">attendre</w></l>
						<l n="32" num="8.4"><hi rend="ital"><w n="32.1">D</w>’<w n="32.2">un</w> <w n="32.3">homme</w> <w n="32.4">qui</w> <w n="32.5">souffre</w> <w n="32.6">et</w> <w n="32.7">se</w> <w n="32.8">plaint</w></hi> ?</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Un</w> <w n="33.2">auteur</w> <w n="33.3">qui</w> <w n="33.4">dans</w> <w n="33.5">son</w> <w n="33.6">écrit</w></l>
						<l n="34" num="9.2"><w n="34.1">Comme</w> <w n="34.2">moy</w>, <w n="34.3">reçoit</w> <w n="34.4">des</w> <w n="34.5">offenses</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Souffre</w> <w n="35.2">plus</w> <w n="35.3">que</w> <w n="35.4">Job</w> <w n="35.5">ne</w> <w n="35.6">souffrit</w>,</l>
						<l n="36" num="9.4"><hi rend="ital"><w n="36.1">Bien</w> <w n="36.2">qu</w>’<w n="36.3">il</w> <w n="36.4">eut</w> <w n="36.5">d</w>’<w n="36.6">extrêmes</w> <w n="36.7">souffrances</w></hi>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Avec</w> <w n="37.2">mes</w> <w n="37.3">vers</w>, <w n="37.4">une</w> <w n="37.5">autre</w> <w n="37.6">fois</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Ne</w> <w n="38.2">mettez</w> <w n="38.3">plus</w> <w n="38.4">dans</w> <w n="38.5">vos</w> <w n="38.6">balances</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Des</w> <w n="39.2">vers</w> <w n="39.3">où</w> <w n="39.4">sur</w> <w n="39.5">des</w> <w n="39.6">palefrois</w></l>
						<l n="40" num="10.4"><hi rend="ital"><w n="40.1">On</w> <w n="40.2">voit</w> <w n="40.3">aller</w> <w n="40.4">des</w> <w n="40.5">patiences</w></hi>.</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">L</w>’<w n="41.2">Herty</w>, <w n="41.3">le</w> <w n="41.4">Roy</w> <w n="41.5">des</w> <w n="41.6">gens</w> <w n="41.7">qu</w>’<w n="41.8">on</w> <w n="41.9">lie</w>,</l>
						<l n="42" num="11.2"><w n="42.1">En</w> <w n="42.2">son</w> <w n="42.3">temps</w> <w n="42.4">auroit</w> <w n="42.5">dit</w> <w n="42.6">cela</w> ;</l>
						<l n="43" num="11.3"><w n="43.1">Ne</w> <w n="43.2">poussez</w> <w n="43.3">pas</w> <w n="43.4">vostre</w> <w n="43.5">folie</w></l>
						<l n="44" num="11.4"><hi rend="ital"><w n="44.1">Plus</w> <w n="44.2">loin</w> <w n="44.3">que</w> <w n="44.4">la</w> <w n="44.5">sienne</w> <w n="44.6">n</w>’<w n="44.7">alla</w></hi>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Alors</w> <w n="45.2">l</w>’<w n="45.3">Ombre</w> <w n="45.4">vous</w> <w n="45.5">quittera</w></l>
						<l n="46" num="12.2"><w n="46.1">Pour</w> <w n="46.2">aller</w> <w n="46.3">voir</w> <w n="46.4">tous</w> <w n="46.5">vos</w> <w n="46.6">semblables</w> ;</l>
						<l n="47" num="12.3"><w n="47.1">Et</w> <w n="47.2">puis</w> <w n="47.3">chaque</w> <w n="47.4">Job</w> <w n="47.5">vous</w> <w n="47.6">dira</w></l>
						<l n="48" num="12.4"><hi rend="ital"><w n="48.1">S</w>’<w n="48.2">il</w> <w n="48.3">souffrit</w> <w n="48.4">des</w> <w n="48.5">maux</w> <w n="48.6">incroyables</w></hi>.</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">Mais</w> <w n="49.2">à</w> <w n="49.3">propos</w>, <w n="49.4">hier</w>, <w n="49.5">au</w> <w n="49.6">Parnasse</w>,</l>
						<l n="50" num="13.2"><w n="50.1">Des</w> <w n="50.2">sonnets</w> <w n="50.3">Phœbus</w> <w n="50.4">se</w> <w n="50.5">mêla</w>,</l>
						<l n="51" num="13.3"><w n="51.1">Et</w> <w n="51.2">l</w>’<w n="51.3">on</w> <w n="51.4">dit</w> <w n="51.5">que</w> <w n="51.6">de</w> <w n="51.7">bonne</w> <w n="51.8">grâce</w></l>
						<l n="52" num="13.4"><hi rend="ital"><w n="52.1">Il</w> <w n="52.2">s</w>’<w n="52.3">en</w> <w n="52.4">plaignit</w>, <w n="52.5">il</w> <w n="52.6">en</w> <w n="52.7">parla</w></hi>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">J</w>’<w n="53.2">aime</w> <w n="53.3">les</w> <w n="53.4">vers</w> <w n="53.5">des</w> <w n="53.6">Uranins</w>,</l>
						<l n="54" num="14.2"><w n="54.1">Dit</w>-<w n="54.2">il</w> ; <w n="54.3">mais</w> <w n="54.4">je</w> <w n="54.5">me</w> <w n="54.6">donne</w> <w n="54.7">aux</w> <w n="54.8">diables</w></l>
						<l n="55" num="14.3"><w n="55.1">Si</w>, <w n="55.2">pour</w> <w n="55.3">les</w> <w n="55.4">vers</w> <w n="55.5">des</w> <w n="55.6">Jobelins</w>,</l>
						<l n="56" num="14.4"><hi rend="ital"><w n="56.1">J</w>’<w n="56.2">en</w> <w n="56.3">connais</w> <w n="56.4">de</w> <w n="56.5">plus</w> <w n="56.6">misérables</w></hi>.</l>
					</lg>
				</div></body></text></TEI>