<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Poésies de Benserade</title>
				<title type="medium">Une édition électronique</title>
				<author key="BEN">
					<name>
						<forename>Isaac</forename>
						<nameLink>de</nameLink>
						<surname>BENSERADE</surname>
					</name>
					<date from="1613" to="1691">1613-1691</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2994 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2020">2020</date>
				<idno type="local">BEN_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Poésies de Benserade</title>
						<author>Isaac de Benserade</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<date when="2020">Exporté de Wikisource le 02/08/2020</date>
						<idno type="URL">https://fr.wikisource.org/wiki/Po%C3%A9sies_de_Benserade</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies de Benserade</title>
								<author>Isaac de Benserade</author>
								<editor>Poésies publiées par Octave Uzanne</editor>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Librairie des bibliophiles</publisher>
									<date when="1875">1875</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
						<title>Les œuvres</title>
						<author>Isaac de Benserade</author>
						<imprint>
							<pubPlace>Genève-Paris</pubPlace>
							<publisher>Slatkine</publisher>
							<date when="1981">1981</date>
						</imprint>
						<biblScope unit="tome">1</biblScope>
					</monogr>
					<note>Édition de référence pour les corrections métriques (fac-similé de l’édition de 1698)</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1697">1697</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les notes de l’éditeur ne sont pas incluses.</p>
				<p>La justification à droite des poèmes en bouts-rimés a été remplacée par un tiret long devant le mot-rime.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<normalization>
					<p>Les faux points de suspension (séquence de 3 ou 4 points) ont été remplacés par le signe typographique approprié.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2020-08-06" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.2.xsd)</change>
				<change when="2020-08-08" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">STANCES, SONNETS, ÉPIGRAMMES, ETC.</head><div type="poem" key="BEN82">
					<head type="form">SONNET.</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">CE</w> <w n="1.2">qu</w>’<w n="1.3">il</w> <w n="1.4">faut</w> <w n="1.5">pour</w> <w n="1.6">un</w> <w n="1.7">poëte</w>, <w n="1.8">Homère</w> <w n="1.9">enfin</w> <w n="1.10">l</w>’<w n="1.11">avoit</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">des</w> <w n="2.3">autres</w> <w n="2.4">il</w> <w n="2.5">fut</w> <w n="2.6">comme</w> <w n="2.7">le</w> <w n="2.8">pédagogue</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Virgile</w> <w n="3.2">vint</w> <w n="3.3">ensuite</w>, <w n="3.4">et</w> <w n="3.5">pas</w> <w n="3.6">moins</w> <w n="3.7">n</w>’<w n="3.8">en</w> <w n="3.9">sçavoit</w>,</l>
						<l n="4" num="1.4"><w n="4.1">La</w> <w n="4.2">secte</w> <w n="4.3">de</w> <w n="4.4">ces</w> <w n="4.5">gens</w> <w n="4.6">est</w> <w n="4.7">envieuse</w> <w n="4.8">et</w> <w n="4.9">rogue</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Aristote</w>, <w n="5.2">invincible</w> <w n="5.3">en</w> <w n="5.4">tout</w> <w n="5.5">ce</w> <w n="5.6">qu</w>’<w n="5.7">il</w> <w n="5.8">prouvoit</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Aprés</w> <w n="6.2">avoir</w> <w n="6.3">esté</w> <w n="6.4">tant</w> <w n="6.5">de</w> <w n="6.6">siècles</w> <w n="6.7">en</w> <w n="6.8">vogue</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Descartes</w> <w n="7.2">vient</w>, <w n="7.3">qui</w> <w n="7.4">dit</w> <w n="7.5">qu</w>’<w n="7.6">Aristote</w> <w n="7.7">rêvoit</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">débite</w>, <w n="8.3">ce</w> <w n="8.4">semble</w>, <w n="8.5">une</w> <w n="8.6">plus</w> <w n="8.7">fine</w> <w n="8.8">drogue</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ce</w> <w n="9.2">qu</w>’<w n="9.3">il</w> <w n="9.4">faut</w> <w n="9.5">pour</w> <w n="9.6">un</w> <w n="9.7">peintre</w>, <w n="9.8">Apelles</w> <w n="9.9">le</w> <w n="9.10">fit</w> <w n="9.11">voir</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">de</w> <w n="10.3">ses</w> <w n="10.4">descendans</w> <w n="10.5">se</w> <w n="10.6">crut</w> <w n="10.7">le</w> <w n="10.8">désespoir</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Il</w> <w n="11.2">en</w> <w n="11.3">est</w> <w n="11.4">toutefois</w> <w n="11.5">qui</w> <w n="11.6">partagent</w> <w n="11.7">sa</w> <w n="11.8">gloire</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Ce</w> <w n="12.2">qu</w>’<w n="12.3">il</w> <w n="12.4">faut</w> <w n="12.5">pour</w> <w n="12.6">un</w> <w n="12.7">roy</w>, <w n="12.8">le</w> <w n="12.9">nôtre</w> <w n="12.10">nous</w> <w n="12.11">l</w>’<w n="12.12">apprend</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Et</w> <w n="13.2">nous</w> <w n="13.3">a</w> <w n="13.4">dés</w> <w n="13.5">longtemps</w> <w n="13.6">mis</w> <w n="13.7">en</w> <w n="13.8">état</w> <w n="13.9">de</w> <w n="13.10">croire</w></l>
						<l n="14" num="4.3"><w n="14.1">Qu</w>’<w n="14.2">il</w> <w n="14.3">ne</w> <w n="14.4">sçauroit</w> <w n="14.5">jamais</w> <w n="14.6">en</w> <w n="14.7">venir</w> <w n="14.8">un</w> <w n="14.9">si</w> <w n="14.10">grand</w>.</l>
					</lg>
				</div></body></text></TEI>