<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC35">
					<head type="number">I</head>
					<head type="main">LA JEUNESSE</head>
					<opener>
						<salute><hi rend="ital">À M. L. Laurent-Pichat.</hi></salute>
						<epigraph>
							<cit>
								<quote>
									La jeunesse à l’heure où nous sommes <lb></lb>
									Doit rendre l’espérance aux hommes.
								</quote>
								<bibl>
									<name>L. Laurent-Pichat</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Oui</w>, <w n="1.2">nous</w> <w n="1.3">sommes</w> <w n="1.4">les</w> <w n="1.5">fiers</w>, <w n="1.6">nous</w> <w n="1.7">sommes</w> <w n="1.8">la</w> <w n="1.9">jeunesse</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">siècle</w> <w n="2.3">nous</w> <w n="2.4">a</w> <w n="2.5">faits</w> <w n="2.6">tristes</w>, <w n="2.7">vaillants</w> <w n="2.8">et</w> <w n="2.9">forts</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Condamnant</w> <w n="3.2">sans</w> <w n="3.3">pitié</w> <w n="3.4">la</w> <w n="3.5">peur</w> <w n="3.6">et</w> <w n="3.7">la</w> <w n="3.8">faiblesse</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">plaignons</w> <w n="4.3">les</w> <w n="4.4">vivants</w> <w n="4.5">sans</w> <w n="4.6">gémir</w> <w n="4.7">sur</w> <w n="4.8">les</w> <w n="4.9">morts</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">S</w>’<w n="5.2">il</w> <w n="5.3">tombe</w> <w n="5.4">de</w> <w n="5.5">nos</w> <w n="5.6">yeux</w> <w n="5.7">quelques</w> <w n="5.8">vains</w> <w n="5.9">pleurs</w> <w n="5.10">de</w> <w n="5.11">femme</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Nous</w> <w n="6.2">les</w> <w n="6.3">laissons</w> <w n="6.4">couler</w> <w n="6.5">paisibles</w> ; <w n="6.6">mais</w>, <w n="6.7">après</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Meilleurs</w>, <w n="7.2">nous</w> <w n="7.3">voulons</w> <w n="7.4">voir</w> <w n="7.5">plus</w> <w n="7.6">haut</w> <w n="7.7">monter</w> <w n="7.8">notre</w> <w n="7.9">âme</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Des</w> <w n="8.2">larmes</w> <w n="8.3">à</w> <w n="8.4">l</w>’<w n="8.5">espoir</w>, <w n="8.6">du</w> <w n="8.7">progrès</w> <w n="8.8">au</w> <w n="8.9">progrès</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Nous</w> <w n="9.2">aimons</w> <w n="9.3">la</w> <w n="9.4">justice</w> <w n="9.5">et</w> <w n="9.6">la</w> <w n="9.7">clémence</w> <w n="9.8">sainte</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Nous</w> <w n="10.2">poursuivons</w> <w n="10.3">le</w> <w n="10.4">mal</w> <w n="10.5">plus</w> <w n="10.6">que</w> <w n="10.7">le</w> <w n="10.8">malfaiteur</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Nous</w> <w n="11.2">embrassons</w> <w n="11.3">le</w> <w n="11.4">pauvre</w> <w n="11.5">en</w> <w n="11.6">une</w> <w n="11.7">ferme</w> <w n="11.8">étreinte</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Afin</w> <w n="12.2">qu</w>’<w n="12.3">il</w> <w n="12.4">sente</w> <w n="12.5">un</w> <w n="12.6">cœur</w> <w n="12.7">de</w> <w n="12.8">frère</w> <w n="12.9">sur</w> <w n="12.10">son</w> <w n="12.11">cœur</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Arraché</w> <w n="13.2">au</w> <w n="13.3">repos</w>, <w n="13.4">lancés</w> <w n="13.5">dans</w> <w n="13.6">la</w> <w n="13.7">bataille</w></l>
						<l n="14" num="4.2"><w n="14.1">Par</w> <w n="14.2">un</w> <w n="14.3">pouvoir</w> <w n="14.4">secret</w>… <w n="14.5">qui</w> <w n="14.6">nous</w> <w n="14.7">importe</w> <w n="14.8">peu</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Nous</w> <w n="15.2">vivons</w> ! <w n="15.3">et</w> <w n="15.4">chacun</w> <w n="15.5">de</w> <w n="15.6">nous</w> <w n="15.7">lutte</w>, <w n="15.8">et</w> <w n="15.9">travaille</w></l>
						<l n="16" num="4.4"><w n="16.1">À</w> <w n="16.2">dresser</w> <w n="16.3">sur</w> <w n="16.4">l</w>’<w n="16.5">autel</w> <w n="16.6">la</w> <w n="16.7">Liberté</w>, <w n="16.8">son</w> <w n="16.9">dieu</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Loin</w> <w n="17.2">de</w> <w n="17.3">l</w>’<w n="17.4">humilité</w>, <w n="17.5">la</w> <w n="17.6">doctrine</w> <w n="17.7">inféconde</w></l>
						<l n="18" num="5.2"><w n="18.1">Qui</w> <w n="18.2">fait</w> <w n="18.3">courber</w> <w n="18.4">le</w> <w n="18.5">front</w> <w n="18.6">à</w> <w n="18.7">l</w>’<w n="18.8">auguste</w> <w n="18.9">Vertu</w>,</l>
						<l n="19" num="5.3"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">pour</w> <w n="19.4">vivre</w> <w n="19.5">debout</w> <w n="19.6">et</w> <w n="19.7">citoyens</w> <w n="19.8">du</w> <w n="19.9">monde</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Que</w> <w n="20.2">nos</w> <w n="20.3">pères</w>, <w n="20.4">martyrs</w> <w n="20.5">saignants</w>, <w n="20.6">ont</w> <w n="20.7">combattu</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">nous</w> <w n="21.3">qu</w>’<w n="21.4">ils</w> <w n="21.5">ont</w> <w n="21.6">grandis</w>, <w n="21.7">au</w> <w n="21.8">fond</w> <w n="21.9">des</w> <w n="21.10">cieux</w> <w n="21.11">splendides</w></l>
						<l n="22" num="6.2"><w n="22.1">Nous</w> <w n="22.2">pouvons</w>, <w n="22.3">par</w>-<w n="22.4">dessus</w> <w n="22.5">les</w> <w n="22.6">monts</w> <w n="22.7">de</w> <w n="22.8">l</w>’<w n="22.9">horizon</w></l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">par</w>-<w n="23.3">dessus</w> <w n="23.4">l</w>’<w n="23.5">amas</w> <w n="23.6">des</w> <w n="23.7">préjugés</w> <w n="23.8">stupides</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Entrevoir</w> <w n="24.2">l</w>’<w n="24.3">éclatant</w> <w n="24.4">lever</w> <w n="24.5">de</w> <w n="24.6">la</w> <w n="24.7">Raison</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Nos</w> <w n="25.2">aînés</w> <w n="25.3">sont</w> <w n="25.4">tous</w> <w n="25.5">là</w>, <w n="25.6">devant</w> <w n="25.7">nous</w>, <w n="25.8">sur</w> <w n="25.9">la</w> <w n="25.10">route</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Mais</w> <w n="26.2">l</w>’<w n="26.3">un</w> <w n="26.4">d</w>’<w n="26.5">eux</w> <w n="26.6">quelquefois</w> <w n="26.7">s</w>’<w n="26.8">arrête</w> <w n="26.9">pour</w> <w n="26.10">mourir</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">Parfois</w> <w n="27.2">l</w>’<w n="27.3">un</w> <w n="27.4">d</w>’<w n="27.5">entre</w> <w n="27.6">nous</w>, <w n="27.7">pâle</w>, <w n="27.8">chancelle</w> <w n="27.9">et</w> <w n="27.10">doute</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">la</w> <w n="28.3">foule</w> <w n="28.4">en</w> <w n="28.5">révolte</w> <w n="28.6">est</w> <w n="28.7">lasse</w> <w n="28.8">de</w> <w n="28.9">souffrir</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Alors</w>, <w n="29.2">vous</w> <w n="29.3">le</w> <w n="29.4">savez</w>, <w n="29.5">vous</w>, <w n="29.6">soldat</w> <w n="29.7">jeune</w> <w n="29.8">encore</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Penseur</w> <w n="30.2">au</w> <w n="30.3">chant</w> <w n="30.4">superbe</w> <w n="30.5">et</w> <w n="30.6">mâle</w> <w n="30.7">travailleur</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Vous</w> <w n="31.2">dont</w> <w n="31.3">l</w>’<w n="31.4">âme</w> <w n="31.5">rayonne</w> <w n="31.6">en</w> <w n="31.7">attendant</w> <w n="31.8">l</w>’<w n="31.9">aurore</w></l>
						<l n="32" num="8.4"><w n="32.1">Qui</w> <w n="32.2">doit</w> <w n="32.3">illuminer</w> <w n="32.4">notre</w> <w n="32.5">nuit</w> <w n="32.6">de</w> <w n="32.7">malheur</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Alors</w>, <w n="33.2">serf</w> <w n="33.3">du</w> <w n="33.4">devoir</w>, <w n="33.5">confiant</w> <w n="33.6">dans</w> <w n="33.7">son</w> <w n="33.8">âge</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Un</w> <w n="34.2">volontaire</w> <w n="34.3">est</w> <w n="34.4">là</w> <w n="34.5">qui</w> <w n="34.6">sort</w> <w n="34.7">des</w> <w n="34.8">rangs</w> <w n="34.9">épais</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Et</w> <w n="35.2">jette</w> <w n="35.3">un</w> <w n="35.4">cri</w> <w n="35.5">vibrant</w> <w n="35.6">d</w>’<w n="35.7">amour</w> <w n="35.8">et</w> <w n="35.9">de</w> <w n="35.10">courage</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Poëte</w> <w n="36.2">du</w> <w n="36.3">combat</w>, <w n="36.4">combattant</w> <w n="36.5">de</w> <w n="36.6">la</w> <w n="36.7">paix</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">8 octobre 1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>