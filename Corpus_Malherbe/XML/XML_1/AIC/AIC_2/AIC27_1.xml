<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">III</head><div type="poem" key="AIC27">
					<head type="number">I</head>
					<head type="main">AMOURS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">tout</w> <w n="1.3">temps</w> <w n="1.4">mes</w> <w n="1.5">amours</w> <w n="1.6">furent</w> <w n="1.7">des</w> <w n="1.8">songes</w> <w n="1.9">vagues</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">n</w>’<w n="2.3">ai</w> <w n="2.4">causé</w> <w n="2.5">tout</w> <w n="2.6">bas</w> <w n="2.7">qu</w>’<w n="2.8">aux</w> <w n="2.9">nymphes</w>, <w n="2.10">dans</w> <w n="2.11">les</w> <w n="2.12">bois</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w>, <w n="3.2">sur</w> <w n="3.3">le</w> <w n="3.4">bord</w> <w n="3.5">des</w> <w n="3.6">mers</w>, <w n="3.7">ces</w> <w n="3.8">sirènes</w>, <w n="3.9">les</w> <w n="3.10">vagues</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Me</w> <w n="4.2">font</w> <w n="4.3">seules</w> <w n="4.4">vibrer</w> <w n="4.5">aux</w> <w n="4.6">accords</w> <w n="4.7">de</w> <w n="4.8">leur</w> <w n="4.9">voix</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Mon</w> <w n="5.2">âme</w> <w n="5.3">est</w> <w n="5.4">fiancée</w> <w n="5.5">à</w> <w n="5.6">l</w>’<w n="5.7">humble</w> <w n="5.8">solitude</w> :</l>
						<l n="6" num="2.2"><w n="6.1">Son</w> <w n="6.2">chaste</w> <w n="6.3">baiser</w> <w n="6.4">plaît</w> <w n="6.5">à</w> <w n="6.6">mon</w> <w n="6.7">front</w> <w n="6.8">sérieux</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Je</w> <w n="7.2">connais</w> <w n="7.3">de</w> <w n="7.4">profonds</w> <w n="7.5">ombrages</w> <w n="7.6">où</w> <w n="7.7">l</w>’<w n="7.8">étude</w></l>
						<l n="8" num="2.4"><w n="8.1">A</w> <w n="8.2">des</w> <w n="8.3">charmes</w> <w n="8.4">plus</w> <w n="8.5">doux</w> <w n="8.6">pour</w> <w n="8.7">l</w>’<w n="8.8">esprit</w> <w n="8.9">et</w> <w n="8.10">les</w> <w n="8.11">yeux</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">suis</w> <w n="9.3">l</w>’<w n="9.4">amant</w> <w n="9.5">rêveur</w> <w n="9.6">des</w> <w n="9.7">récifs</w> <w n="9.8">et</w> <w n="9.9">des</w> <w n="9.10">grèves</w>,</l>
						<l n="10" num="3.2"><w n="10.1">L</w>’<w n="10.2">insatiable</w> <w n="10.3">amant</w> <w n="10.4">du</w> <w n="10.5">grand</w> <w n="10.6">ciel</w> <w n="10.7">inconnu</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">ne</w> <w n="11.3">retrouverai</w> <w n="11.4">la</w> <w n="11.5">vierge</w> <w n="11.6">de</w> <w n="11.7">mes</w> <w n="11.8">rêves</w></l>
						<l n="12" num="3.4"><w n="12.1">Qu</w>’<w n="12.2">en</w> <w n="12.3">l</w>’<w n="12.4">immortel</w> <w n="12.5">pays</w> <w n="12.6">d</w>’<w n="12.7">où</w> <w n="12.8">mon</w> <w n="12.9">cœur</w> <w n="12.10">est</w> <w n="12.11">venu</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">vertu</w> <w n="13.3">de</w> <w n="13.4">l</w>’<w n="13.5">amour</w>, <w n="13.6">l</w>’<w n="13.7">homme</w> <w n="13.8">en</w> <w n="13.9">a</w> <w n="13.10">fait</w> <w n="13.11">un</w> <w n="13.12">crime</w> !</l>
						<l n="14" num="4.2"><w n="14.1">Je</w> <w n="14.2">ne</w> <w n="14.3">veux</w> <w n="14.4">pas</w> <w n="14.5">aimer</w> <w n="14.6">comme</w> <w n="14.7">on</w> <w n="14.8">aime</w> <w n="14.9">ici</w>-<w n="14.10">bas</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w> <w n="15.2">ce</w> <w n="15.3">cœur</w>, <w n="15.4">façonné</w> <w n="15.5">pour</w> <w n="15.6">un</w> <w n="15.7">élan</w> <w n="15.8">sublime</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Tant</w> <w n="16.2">qu</w>’<w n="16.3">il</w> <w n="16.4">pourra</w> <w n="16.5">monter</w> <w n="16.6">ne</w> <w n="16.7">se</w> <w n="16.8">posera</w> <w n="16.9">pas</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J</w>’<w n="17.2">ai</w> <w n="17.3">pourtant</w> <w n="17.4">vu</w> <w n="17.5">passer</w> <w n="17.6">dans</w> <w n="17.7">le</w> <w n="17.8">vol</w> <w n="17.9">de</w> <w n="17.10">mes</w> <w n="17.11">stances</w></l>
						<l n="18" num="5.2"><w n="18.1">De</w> <w n="18.2">blanches</w> <w n="18.3">visions</w>, <w n="18.4">filles</w> <w n="18.5">de</w> <w n="18.6">mon</w> <w n="18.7">désir</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Mais</w> <w n="19.2">je</w> <w n="19.3">n</w>’<w n="19.4">aime</w> <w n="19.5">d</w>’<w n="19.6">amour</w> <w n="19.7">que</w> <w n="19.8">mes</w> <w n="19.9">jeunes</w> <w n="19.10">croyances</w> :</l>
						<l n="20" num="5.4"><w n="20.1">Espoir</w> <w n="20.2">dans</w> <w n="20.3">le</w> <w n="20.4">printemps</w>, <w n="20.5">et</w> <w n="20.6">foi</w> <w n="20.7">dans</w> <w n="20.8">l</w>’<w n="20.9">avenir</w> !</l>
					</lg>
				</div></body></text></TEI>