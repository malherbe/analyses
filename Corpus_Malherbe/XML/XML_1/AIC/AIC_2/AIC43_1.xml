<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC43">
					<head type="number">IX</head>
					<head type="main">L’HISTORIEN</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Ô Révolution, ma mère que vous <lb></lb>
									étiez lente à venir !
								</quote>
								<bibl>
									<name>Michelet</name>, <hi rend="ital"></hi>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Parfois</w> <w n="1.2">l</w>’<w n="1.3">historien</w> <w n="1.4">qui</w> <w n="1.5">sonde</w></l>
						<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">grands</w> <w n="2.3">règnes</w> <w n="2.4">évanouis</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ou</w> <w n="3.2">sur</w> <w n="3.3">les</w> <w n="3.4">horizons</w> <w n="3.5">du</w> <w n="3.6">monde</w></l>
						<l n="4" num="1.4"><w n="4.1">Fixe</w> <w n="4.2">ses</w> <w n="4.3">regards</w> <w n="4.4">éblouis</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Voyant</w> <w n="5.2">dans</w> <w n="5.3">quelle</w> <w n="5.4">nuit</w> <w n="5.5">profonde</w></l>
						<l n="6" num="2.2"><w n="6.1">Les</w> <w n="6.2">esprits</w> <w n="6.3">dormaient</w> <w n="6.4">enfouis</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">quelle</w> <w n="7.3">tempête</w> <w n="7.4">féconde</w></l>
						<l n="8" num="2.4"><w n="8.1">Les</w> <w n="8.2">fit</w> <w n="8.3">surgir</w> <w n="8.4">épanouis</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Cet</w> <w n="9.2">homme</w> <w n="9.3">enthousiaste</w> <w n="9.4">pleure</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Superbe</w>, <w n="10.2">impatient</w> <w n="10.3">de</w> <w n="10.4">l</w>’<w n="10.5">heure</w></l>
						<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">l</w>’<w n="11.3">ignorance</w> <w n="11.4">doit</w> <w n="11.5">périr</w>,</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Il</w> <w n="12.2">crie</w> <w n="12.3">en</w> <w n="12.4">sa</w> <w n="12.5">sainte</w> <w n="12.6">colère</w> :</l>
						<l n="13" num="4.2">« <w n="13.1">Ô</w> <w n="13.2">Révolution</w>, <w n="13.3">ma</w> <w n="13.4">mère</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Que</w> <w n="14.2">vous</w> <w n="14.3">étiez</w> <w n="14.4">lente</w> <w n="14.5">à</w> <w n="14.6">venir</w> ! »</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>