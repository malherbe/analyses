<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC46">
					<head type="number">XII</head>
					<head type="main">CINCINNATUS</head>
					<opener>
						<salute><hi rend="ital">Hommage aux ouvriers de Bandol.</hi></salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">temps</w> <w n="1.3">sont</w> <w n="1.4">accomplis</w>. <w n="1.5">L</w>’<w n="1.6">aube</w> <w n="1.7">sereine</w> <w n="1.8">dore</w>,</l>
						<l n="2" num="1.2"><w n="2.1">À</w> <w n="2.2">l</w>’<w n="2.3">horizon</w> <w n="2.4">lointain</w>, <w n="2.5">les</w> <w n="2.6">paisibles</w> <w n="2.7">sommets</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">la</w> <w n="3.3">terrible</w> <w n="3.4">nuit</w> <w n="3.5">qui</w> <w n="3.6">nous</w> <w n="3.7">oppresse</w> <w n="3.8">encore</w></l>
						<l n="4" num="1.4"><w n="4.1">Doit</w> <w n="4.2">insensiblement</w> <w n="4.3">disparaître</w> <w n="4.4">à</w> <w n="4.5">jamais</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">La</w> <w n="5.2">Science</w> <w n="5.3">a</w> <w n="5.4">creusé</w> <w n="5.5">les</w> <w n="5.6">plus</w> <w n="5.7">profonds</w> <w n="5.8">secrets</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">L</w>’<w n="6.2">ignorance</w> <w n="6.3">d</w>’<w n="6.4">esprit</w> <w n="6.5">et</w> <w n="6.6">de</w> <w n="6.7">cœur</w> <w n="6.8">s</w>’<w n="6.9">évapore</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">le</w> <w n="7.3">monde</w> <w n="7.4">bientôt</w>, <w n="7.5">de</w> <w n="7.6">progrès</w> <w n="7.7">en</w> <w n="7.8">progrès</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Ne</w> <w n="8.2">verra</w> <w n="8.3">dans</w> <w n="8.4">son</w> <w n="8.5">ciel</w> <w n="8.6">qu</w>’<w n="8.7">une</w> <w n="8.8">éternelle</w> <w n="8.9">aurore</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Tous</w> <w n="9.2">les</w> <w n="9.3">hommes</w> <w n="9.4">seront</w> <w n="9.5">frères</w>, <w n="9.6">et</w> <w n="9.7">tous</w> <w n="9.8">égaux</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Ils</w> <w n="10.2">vivront</w> <w n="10.3">sous</w> <w n="10.4">l</w>’<w n="10.5">azur</w>, <w n="10.6">bons</w>, <w n="10.7">paisibles</w> <w n="10.8">et</w> <w n="10.9">beaux</w>,</l>
						<l n="11" num="3.3"><w n="11.1">N</w>’<w n="11.2">ayant</w> <w n="11.3">plus</w> <w n="11.4">de</w> <w n="11.5">mortel</w> <w n="11.6">que</w> <w n="11.7">l</w>’<w n="11.8">enveloppe</w> <w n="11.9">d</w>’<w n="11.10">homme</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Ni</w> <w n="12.2">valets</w>, <w n="12.3">ni</w> <w n="12.4">seigneurs</w> ! <w n="12.5">tous</w> <w n="12.6">seront</w> <w n="12.7">artisans</w> !</l>
						<l n="13" num="4.2"><w n="13.1">Et</w> <w n="13.2">les</w> <w n="13.3">meilleurs</w> <w n="13.4">d</w>’<w n="13.5">entre</w> <w n="13.6">eux</w> <w n="13.7">se</w> <w n="13.8">feront</w> <w n="13.9">paysans</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Comme</w> <w n="14.2">Cincinnatus</w>,… <w n="14.3">le</w> <w n="14.4">plus</w> <w n="14.5">grand</w> <w n="14.6">fils</w> <w n="14.7">de</w> <w n="14.8">Rome</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Bandol</placeName>,
							<date when="1866">27 décembre 1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>