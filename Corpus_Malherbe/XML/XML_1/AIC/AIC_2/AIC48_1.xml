<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC48">
					<head type="number">XIV</head>
					<head type="main">SOLIDARITÉ</head>
					<opener>
						<salute><hi rend="ital">À M. A. A.</hi></salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">ceint</w> <w n="1.4">mes</w> <w n="1.5">reins</w>, <w n="1.6">j</w>’<w n="1.7">ai</w> <w n="1.8">pris</w> <w n="1.9">le</w> <w n="1.10">bâton</w> <w n="1.11">voyageur</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Car</w> <w n="2.2">mon</w> <w n="2.3">âme</w> <w n="2.4">souvent</w> <w n="2.5">n</w>’<w n="2.6">est</w> <w n="2.7">qu</w>’<w n="2.8">une</w> <w n="2.9">plaie</w> <w n="2.10">ouverte</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">je</w> <w n="3.3">vais</w>, <w n="3.4">demandant</w> <w n="3.5">sans</w> <w n="3.6">trêve</w> <w n="3.7">un</w> <w n="3.8">air</w> <w n="3.9">meilleur</w>,</l>
						<l n="4" num="1.4"><w n="4.1">En</w> <w n="4.2">tous</w> <w n="4.3">lieux</w> <w n="4.4">où</w> <w n="4.5">l</w>’<w n="4.6">on</w> <w n="4.7">trouve</w> <w n="4.8">une</w> <w n="4.9">route</w> <w n="4.10">déserte</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Or</w>, <w n="5.2">hier</w>, <w n="5.3">j</w>’<w n="5.4">ai</w> <w n="5.5">gravi</w> <w n="5.6">l</w>’<w n="5.7">escarpement</w> <w n="5.8">d</w>’<w n="5.9">un</w> <w n="5.10">mont</w> :</l>
						<l n="6" num="2.2"><w n="6.1">J</w>’<w n="6.2">escaladais</w> <w n="6.3">les</w> <w n="6.4">pics</w> <w n="6.5">par</w> <w n="6.6">des</w> <w n="6.7">sentiers</w> <w n="6.8">de</w> <w n="6.9">chèvre</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Une</w> <w n="7.2">étrange</w> <w n="7.3">frayeur</w> <w n="7.4">faisait</w> <w n="7.5">pâlir</w> <w n="7.6">mon</w> <w n="7.7">front</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Quand</w> <w n="8.2">la</w> <w n="8.3">nue</w>, <w n="8.4">en</w> <w n="8.5">passant</w>, <w n="8.6">frémissait</w> <w n="8.7">sur</w> <w n="8.8">ma</w> <w n="8.9">lèvre</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Là</w>, <w n="9.2">dans</w> <w n="9.3">les</w> <w n="9.4">rochers</w> <w n="9.5">gris</w>, <w n="9.6">immuable</w> <w n="9.7">comme</w> <w n="9.8">eux</w>,</l>
						<l n="10" num="3.2"><w n="10.1">S</w>’<w n="10.2">élève</w> <w n="10.3">le</w> <w n="10.4">sapin</w> <w n="10.5">rêveur</w> <w n="10.6">auprès</w> <w n="10.7">du</w> <w n="10.8">chêne</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">souffles</w> <w n="11.3">ennemis</w> <w n="11.4">passent</w> <w n="11.5">dans</w> <w n="11.6">ses</w> <w n="11.7">cheveux</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Même</w> <w n="12.2">sans</w> <w n="12.3">émouvoir</w> <w n="12.4">sa</w> <w n="12.5">force</w> <w n="12.6">souveraine</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Sur</w> <w n="13.2">ces</w> <w n="13.3">pures</w> <w n="13.4">hauteurs</w> <w n="13.5">règne</w> <w n="13.6">l</w>’<w n="13.7">éternité</w> ;</l>
						<l n="14" num="4.2"><w n="14.1">L</w>’<w n="14.2">horreur</w> <w n="14.3">religieuse</w> <w n="14.4">habite</w> <w n="14.5">cette</w> <w n="14.6">cime</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Et</w>, <w n="15.2">qu</w>’<w n="15.3">on</w> <w n="15.4">ait</w> <w n="15.5">devant</w> <w n="15.6">soi</w> <w n="15.7">la</w> <w n="15.8">nuit</w> <w n="15.9">ou</w> <w n="15.10">la</w> <w n="15.11">clarté</w>,</l>
						<l n="16" num="4.4"><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">toujours</w> <w n="16.4">l</w>’<w n="16.5">infini</w> <w n="16.6">béant</w>, <w n="16.7">toujours</w> <w n="16.8">l</w>’<w n="16.9">abîme</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">J</w>’<w n="17.2">ai</w> <w n="17.3">promené</w> <w n="17.4">mes</w> <w n="17.5">yeux</w> <w n="17.6">sur</w> <w n="17.7">les</w> <w n="17.8">grands</w> <w n="17.9">horizons</w> ;</l>
						<l n="18" num="5.2"><w n="18.1">C</w>’<w n="18.2">étaient</w> <w n="18.3">des</w> <w n="18.4">monts</w> <w n="18.5">houleux</w>, <w n="18.6">c</w>’<w n="18.7">était</w> <w n="18.8">la</w> <w n="18.9">mer</w> <w n="18.10">immense</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">j</w>’<w n="19.3">aperçus</w> <w n="19.4">à</w> <w n="19.5">peine</w> <w n="19.6">un</w> <w n="19.7">groupe</w> <w n="19.8">de</w> <w n="19.9">maisons</w>…</l>
						<l n="20" num="5.4"><w n="20.1">Mon</w> <w n="20.2">âme</w> <w n="20.3">alors</w> <w n="20.4">se</w> <w n="20.5">prit</w> <w n="20.6">à</w> <w n="20.7">pleurer</w> <w n="20.8">en</w> <w n="20.9">silence</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Mon</w> <w n="21.2">âme</w> <w n="21.3">alors</w> <w n="21.4">se</w> <w n="21.5">prit</w> <w n="21.6">à</w> <w n="21.7">pleurer</w> <w n="21.8">les</w> <w n="21.9">vivants</w></l>
						<l n="22" num="6.2"><w n="22.1">Qui</w> <w n="22.2">sont</w> <w n="22.3">si</w> <w n="22.4">peu</w> <w n="22.5">de</w> <w n="22.6">chose</w> <w n="22.7">au</w> <w n="22.8">sommet</w> <w n="22.9">des</w> <w n="22.10">montagnes</w> !</l>
						<l n="23" num="6.3"><w n="23.1">Que</w> <w n="23.2">trouble</w> <w n="23.3">le</w> <w n="23.4">vertige</w>, <w n="23.5">et</w> <w n="23.6">qui</w> <w n="23.7">tremblent</w> <w n="23.8">aux</w> <w n="23.9">vents</w></l>
						<l n="24" num="6.4"><w n="24.1">Plus</w> <w n="24.2">que</w> <w n="24.3">l</w>’<w n="24.4">épi</w> <w n="24.5">de</w> <w n="24.6">blé</w> <w n="24.7">par</w> <w n="24.8">les</w> <w n="24.9">blondes</w> <w n="24.10">campagnes</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Mais</w>, <w n="25.2">dans</w> <w n="25.3">un</w> <w n="25.4">creux</w> <w n="25.5">de</w> <w n="25.6">roche</w>, <w n="25.7">une</w> <w n="25.8">bête</w> <w n="25.9">à</w> <w n="25.10">bon</w> <w n="25.11">Dieu</w>,</l>
						<l n="26" num="7.2"><w n="26.1">Confiante</w>, <w n="26.2">courait</w> <w n="26.3">sous</w> <w n="26.4">l</w>’<w n="26.5">herbe</w> <w n="26.6">fraîche</w> <w n="26.7">et</w> <w n="26.8">douce</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">je</w> <w n="27.3">compris</w> <w n="27.4">que</w> <w n="27.5">même</w> <w n="27.6">en</w> <w n="27.7">ce</w> <w n="27.8">farouche</w> <w n="27.9">lieu</w></l>
						<l n="28" num="7.4"><w n="28.1">Vivent</w>, <w n="28.2">sans</w> <w n="28.3">nul</w> <w n="28.4">effroi</w>, <w n="28.5">l</w>’<w n="28.6">insecte</w> <w n="28.7">et</w> <w n="28.8">l</w>’<w n="28.9">humble</w> <w n="28.10">mousse</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">tout</w> <w n="29.3">à</w> <w n="29.4">coup</w> <w n="29.5">j</w>’<w n="29.6">ai</w> <w n="29.7">vu</w>, <w n="29.8">comme</w> <w n="29.9">je</w> <w n="29.10">vois</w> <w n="29.11">le</w> <w n="29.12">jour</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Des</w> <w n="30.2">yeux</w> <w n="30.3">de</w> <w n="30.4">mon</w> <w n="30.5">esprit</w>, <w n="30.6">la</w> <w n="30.7">Clémence</w> <w n="30.8">éternelle</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Et</w> <w n="31.2">j</w>’<w n="31.3">ai</w> <w n="31.4">pu</w> <w n="31.5">pénétrer</w> <w n="31.6">l</w>’<w n="31.7">universel</w> <w n="31.8">Amour</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Ainsi</w> <w n="32.2">que</w> <w n="32.3">l</w>’<w n="32.4">aigle</w> <w n="32.5">monte</w> <w n="32.6">aux</w> <w n="32.7">cieux</w>, <w n="32.8">d</w>’<w n="32.9">un</w> <w n="32.10">seul</w> <w n="32.11">coup</w> <w n="32.12">d</w>’<w n="32.13">aile</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Comme</w> <w n="33.2">par</w> <w n="33.3">un</w> <w n="33.4">miracle</w> <w n="33.5">auguste</w>, <w n="33.6">j</w>’<w n="33.7">ai</w> <w n="33.8">senti</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Distinctement</w>, <w n="34.2">ma</w> <w n="34.3">vie</w> <w n="34.4">éparse</w> <w n="34.5">en</w> <w n="34.6">la</w> <w n="34.7">nature</w> ;</l>
						<l n="35" num="9.3"><w n="35.1">C</w>’<w n="35.2">est</w> <w n="35.3">un</w> <w n="35.4">songe</w> <w n="35.5">puissant</w> <w n="35.6">qui</w> <w n="35.7">ne</w> <w n="35.8">m</w>’<w n="35.9">a</w> <w n="35.10">pas</w> <w n="35.11">menti</w> :</l>
						<l n="36" num="9.4"><w n="36.1">Je</w> <w n="36.2">suis</w> <w n="36.3">ombre</w> ! <w n="36.4">je</w> <w n="36.5">suis</w> <w n="36.6">soleil</w> ! <w n="36.7">je</w> <w n="36.8">suis</w> <w n="36.9">murmure</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Je</w> <w n="37.2">me</w> <w n="37.3">sens</w> <w n="37.4">palpiter</w> <w n="37.5">sous</w> <w n="37.6">l</w>’<w n="37.7">haleine</w> <w n="37.8">du</w> <w n="37.9">vent</w> !</l>
						<l n="38" num="10.2"><w n="38.1">Je</w> <w n="38.2">suis</w> <w n="38.3">le</w> <w n="38.4">chêne</w> <w n="38.5">vert</w> ! <w n="38.6">je</w> <w n="38.7">suis</w> <w n="38.8">la</w> <w n="38.9">jeune</w> <w n="38.10">sève</w> !</l>
						<l n="39" num="10.3"><w n="39.1">Je</w> <w n="39.2">suis</w> <w n="39.3">l</w>’<w n="39.4">Homme</w> ! <w n="39.5">je</w> <w n="39.6">suis</w> <w n="39.7">le</w> <w n="39.8">suprême</w> <w n="39.9">Vivant</w> !</l>
						<l n="40" num="10.4"><w n="40.1">Dans</w> <w n="40.2">tous</w> <w n="40.3">les</w> <w n="40.4">vols</w> <w n="40.5">mon</w> <w n="40.6">âme</w> <w n="40.7">au</w> <w n="40.8">vol</w> <w n="40.9">ardent</w> <w n="40.10">s</w>’<w n="40.11">élève</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Ô</w> <w n="41.2">feu</w> <w n="41.3">du</w> <w n="41.4">ciel</w> <w n="41.5">tombé</w> <w n="41.6">dans</w> <w n="41.7">le</w> <w n="41.8">sein</w> <w n="41.9">des</w> <w n="41.10">cailloux</w>,</l>
						<l n="42" num="11.2"><w n="42.1">Pistils</w> <w n="42.2">des</w> <w n="42.3">fleurs</w>, <w n="42.4">parfums</w> <w n="42.5">sacrés</w> <w n="42.6">de</w> <w n="42.7">la</w> <w n="42.8">bruyère</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Je</w> <w n="43.2">me</w> <w n="43.3">sens</w> <w n="43.4">frissonner</w> <w n="43.5">d</w>’<w n="43.6">extase</w> <w n="43.7">comme</w> <w n="43.8">vous</w></l>
						<l n="44" num="11.4"><w n="44.1">Aux</w> <w n="44.2">baisers</w> <w n="44.3">virginaux</w> <w n="44.4">de</w> <w n="44.5">la</w> <w n="44.6">blanche</w> <w n="44.7">lumière</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">J</w>’<w n="45.2">aime</w>, <w n="45.3">je</w> <w n="45.4">vis</w> ! <w n="45.5">La</w> <w n="45.6">Mort</w> <w n="45.7">est</w> <w n="45.8">morte</w> ; <w n="45.9">elle</w> <w n="45.10">n</w>’<w n="45.11">est</w> <w n="45.12">rien</w> !</l>
						<l n="46" num="12.2"><w n="46.1">Allez</w>, <w n="46.2">vous</w> <w n="46.3">dont</w> <w n="46.4">la</w> <w n="46.5">foi</w> <w n="46.6">débile</w> <w n="46.7">s</w>’<w n="46.8">est</w> <w n="46.9">éteinte</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Vous</w> <w n="47.2">tous</w> <w n="47.3">qui</w> <w n="47.4">poursuivez</w> <w n="47.5">le</w> <w n="47.6">bonheur</w> <w n="47.7">et</w> <w n="47.8">le</w> <w n="47.9">bien</w>,</l>
						<l n="48" num="12.4"><w n="48.1">Respirer</w> <w n="48.2">sur</w> <w n="48.3">les</w> <w n="48.4">monts</w> <w n="48.5">la</w> <w n="48.6">Fraternité</w> <w n="48.7">sainte</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1867">février 1867</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>