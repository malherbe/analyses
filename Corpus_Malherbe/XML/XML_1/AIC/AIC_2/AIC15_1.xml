<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><div type="poem" key="AIC15">
					<head type="number">XII</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Il</w> <w n="1.2">pleut</w>, <w n="1.3">nous</w> <w n="1.4">pleurons</w>,… <w n="1.5">Vive</w> <w n="1.6">le</w> <w n="1.7">soleil</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Nous</w> <w n="2.2">sommes</w> <w n="2.3">le</w> <w n="2.4">ciel</w>, <w n="2.5">essayons</w> <w n="2.6">de</w> <w n="2.7">luire</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Nous</w> <w n="3.2">sommes</w> <w n="3.3">enfants</w>, <w n="3.4">essayons</w> <w n="3.5">de</w> <w n="3.6">rire</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">rire</w> <w n="4.3">est</w> <w n="4.4">toujours</w> <w n="4.5">d</w>’<w n="4.6">or</w>… <w n="4.7">ou</w> <w n="4.8">de</w> <w n="4.9">vermeil</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">D</w>’<w n="5.2">or</w> <w n="5.3">ou</w> <w n="5.4">de</w> <w n="5.5">vermeil</w> ! <w n="5.6">on</w> <w n="5.7">se</w> <w n="5.8">tient</w> <w n="5.9">les</w> <w n="5.10">hanches</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">On</w> <w n="6.2">rit</w> <w n="6.3">bien</w> <w n="6.4">fort</w>, <w n="6.5">mais</w> — <w n="6.6">on</w> <w n="6.7">a</w> <w n="6.8">sa</w> <w n="6.9">douleur</w> !</l>
						<l n="7" num="2.3"><w n="7.1">Hélas</w> ! <w n="7.2">oui</w>, <w n="7.3">toujours</w>, <w n="7.4">ma</w> <w n="7.5">petite</w> <w n="7.6">sœur</w>,</l>
						<l n="8" num="2.4"><w n="8.1">On</w> <w n="8.2">rit</w> <w n="8.3">jaune</w>, <w n="8.4">même</w> <w n="8.5">avec</w> <w n="8.6">des</w> <w n="8.7">dents</w> <w n="8.8">blanches</w> !</l>
					</lg>
					<closer>
						<dateline>
							<date when="1866">10 août 1866</date>.
							</dateline>
					</closer>
				</div></body></text></TEI>