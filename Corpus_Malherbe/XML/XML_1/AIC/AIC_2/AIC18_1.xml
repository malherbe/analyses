<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="AIC18">
					<head type="number">II</head>
					<head type="main">LE PARFUM DES PERVENCHES</head>
					<ab type="dash">——</ab>
					<head type="sub">ENFANTINE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Bonne</w> <w n="1.2">Vierge</w>, <w n="1.3">écoutez</w> <w n="1.4">ma</w> <w n="1.5">voix</w>, <w n="1.6">je</w> <w n="1.7">vous</w> <w n="1.8">en</w> <w n="1.9">prie</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Hier</w>, <w n="2.2">parmi</w> <w n="2.3">les</w> <w n="2.4">bouquets</w> <w n="2.5">vivants</w> <w n="2.6">de</w> <w n="2.7">la</w> <w n="2.8">prairie</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">cueillis</w>, <w n="3.3">en</w> <w n="3.4">tressant</w> <w n="3.5">ma</w> <w n="3.6">guirlande</w>, <w n="3.7">une</w> <w n="3.8">fleur</w></l>
						<l n="4" num="1.4"><w n="4.1">Dont</w> <w n="4.2">le</w> <w n="4.3">calice</w> <w n="4.4">bleu</w> <w n="4.5">n</w>’<w n="4.6">exhalait</w> <w n="4.7">nulle</w> <w n="4.8">odeur</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">La</w> <w n="5.2">pervenche</w>, <w n="5.3">pour</w> <w n="5.4">nous</w>, <w n="5.5">dit</w> <w n="5.6">ma</w> <w n="5.7">mère</w> <w n="5.8">chérie</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Est</w> <w n="6.2">toujours</w> <w n="6.3">sans</w> <w n="6.4">parfums</w> <w n="6.5">célestes</w>, <w n="6.6">car</w> <w n="6.7">Marie</w></l>
						<l n="7" num="2.3"><w n="7.1">Par</w> <w n="7.2">les</w> <w n="7.3">anges</w> <w n="7.4">en</w> <w n="7.5">fait</w> <w n="7.6">dérober</w> <w n="7.7">la</w> <w n="7.8">senteur</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">leurs</w> <w n="8.3">tremblantes</w> <w n="8.4">mains</w> <w n="8.5">la</w> <w n="8.6">portent</w> <w n="8.7">à</w> <w n="8.8">son</w> <w n="8.9">cœur</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">Mais</w> <w n="9.2">quand</w> <w n="9.3">l</w>’<w n="9.4">hiver</w> <w n="9.5">flétrit</w> <w n="9.6">les</w> <w n="9.7">plantes</w> <w n="9.8">qui</w> <w n="9.9">frissonnent</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Pour</w> <w n="10.2">embaumer</w> <w n="10.3">les</w> <w n="10.4">cieux</w> <w n="10.5">les</w> <w n="10.6">chérubins</w> <w n="10.7">moissonnent</w></l>
						<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">âmes</w> <w n="11.3">des</w> <w n="11.4">petits</w> <w n="11.5">innocents</w> <w n="11.6">comme</w> <w n="11.7">toi</w>. »</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Vierge</w>, <w n="12.2">ayant</w> <w n="12.3">écouté</w>, <w n="12.4">tout</w> <w n="12.5">joyeux</w>, <w n="12.6">ces</w> <w n="12.7">paroles</w>,</l>
						<l n="13" num="4.2"><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">des</w> <w n="13.4">fleurs</w> <w n="13.5">du</w> <w n="13.6">jardin</w> <w n="13.7">ravagé</w> <w n="13.8">les</w> <w n="13.9">corolles</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Pour</w> <w n="14.2">que</w> <w n="14.3">tes</w> <w n="14.4">messagers</w> <w n="14.5">n</w>’<w n="14.6">y</w> <w n="14.7">trouvent</w> <w n="14.8">plus</w> <w n="14.9">que</w> <w n="14.10">moi</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Nîmes</placeName>,
							<date when="1864">1864</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>