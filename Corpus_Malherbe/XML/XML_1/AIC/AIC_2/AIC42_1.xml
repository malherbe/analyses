<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IV</head><div type="poem" key="AIC42">
					<head type="number">VIII</head>
					<head type="main">ΑΣΘΜΑ</head>
					<opener>
						<salute>
							<hi rend="ital">
								À mon père Jean AICARD <lb></lb>Mort le 16 mai 1853.
							</hi>
						</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ne</w> <w n="1.2">pourrai</w>-<w n="1.3">je</w> <w n="1.4">saisir</w> <w n="1.5">un</w> <w n="1.6">espoir</w> <w n="1.7">qui</w> <w n="1.8">m</w>’<w n="1.9">apaise</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ni</w> <w n="2.2">voir</w> <w n="2.3">luire</w> <w n="2.4">la</w> <w n="2.5">foi</w> <w n="2.6">dans</w> <w n="2.7">la</w> <w n="2.8">clarté</w> <w n="2.9">du</w> <w n="2.10">jour</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Dis</w>, <w n="3.2">ô</w> <w n="3.3">joyeux</w> <w n="3.4">soleil</w> <w n="3.5">dont</w> <w n="3.6">le</w> <w n="3.7">rayon</w> <w n="3.8">me</w> <w n="3.9">baise</w> ?</l>
						<l n="4" num="1.4"><w n="4.1">Réponds</w>, <w n="4.2">toi</w> <w n="4.3">que</w> <w n="4.4">je</w> <w n="4.5">sens</w> <w n="4.6">dans</w> <w n="4.7">la</w> <w n="4.8">lumière</w>, — <w n="4.9">Amour</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">ne</w> <w n="5.3">sais</w> <w n="5.4">si</w> <w n="5.5">je</w> <w n="5.6">crois</w> <w n="5.7">en</w> <w n="5.8">Dieu</w> ! <w n="5.9">L</w>’<w n="5.10">azur</w> <w n="5.11">me</w> <w n="5.12">pèse</w>.</l>
						<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">voudrais</w> <w n="6.3">d</w>’<w n="6.4">un</w> <w n="6.5">élan</w> <w n="6.6">crever</w> <w n="6.7">ce</w> <w n="6.8">plafond</w> <w n="6.9">lourd</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Depuis</w> <w n="7.2">longtemps</w> <w n="7.3">je</w> <w n="7.4">marche</w>, <w n="7.5">et</w> <w n="7.6">la</w> <w n="7.7">route</w> <w n="7.8">est</w> <w n="7.9">mauvaise</w> ;</l>
						<l n="8" num="2.4"><w n="8.1">Ma</w> <w n="8.2">fatigue</w> <w n="8.3">en</w> <w n="8.4">vain</w> <w n="8.5">jette</w> <w n="8.6">un</w> <w n="8.7">appel</w> <w n="8.8">au</w> <w n="8.9">ciel</w> <w n="8.10">sourd</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Pourtant</w> <w n="9.2">je</w> <w n="9.3">veux</w> <w n="9.4">donner</w> <w n="9.5">à</w> <w n="9.6">quelqu</w>’<w n="9.7">un</w> <w n="9.8">ma</w> <w n="9.9">prière</w> !…</l>
						<l n="10" num="3.2"><w n="10.1">Les</w> <w n="10.2">ailes</w> <w n="10.3">de</w> <w n="10.4">mon</w> <w n="10.5">cœur</w> <w n="10.6">me</w> <w n="10.7">soulèvent</w> <w n="10.8">de</w> <w n="10.9">terre</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Sans</w> <w n="11.2">trouver</w> <w n="11.3">aucun</w> <w n="11.4">but</w> <w n="11.5">à</w> <w n="11.6">leurs</w> <w n="11.7">brûlants</w> <w n="11.8">efforts</w> ;</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Mais</w>, <w n="12.2">aux</w> <w n="12.3">vagues</w> <w n="12.4">désirs</w> <w n="12.5">quand</w> <w n="12.6">mon</w> <w n="12.7">être</w> <w n="12.8">se</w> <w n="12.9">livre</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Je</w> <w n="13.2">ne</w> <w n="13.3">puis</w> <w n="13.4">m</w>’<w n="13.5">affirmer</w> <w n="13.6">qu</w>’<w n="13.7">on</w> <w n="13.8">puisse</w> <w n="13.9">ne</w> <w n="13.10">plus</w> <w n="13.11">vivre</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">l</w>’<w n="14.3">Aspiration</w> <w n="14.4">m</w>’<w n="14.5">emporte</w> <w n="14.6">vers</w> <w n="14.7">les</w> <w n="14.8">morts</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>