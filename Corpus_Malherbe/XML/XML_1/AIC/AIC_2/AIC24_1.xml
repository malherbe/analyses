<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">II</head><div type="poem" key="AIC24">
					<head type="number">VIII</head>
					<head type="main">PROMENADE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Nous</w> <w n="1.2">qui</w> <w n="1.3">croyons</w> <w n="1.4">souffrir</w>, <w n="1.5">songeons</w> <w n="1.6">à</w> <w n="1.7">la</w> <w n="1.8">souffrance</w></l>
						<l n="2" num="1.2"><w n="2.1">De</w> <w n="2.2">ceux</w> <w n="2.3">qui</w> <w n="2.4">vivent</w> <w n="2.5">seuls</w>, <w n="2.6">sans</w> <w n="2.7">même</w> <w n="2.8">une</w> <w n="2.9">espérance</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="12"></space><w n="3.1">Et</w> <w n="3.2">qui</w> <w n="3.3">mourront</w> <w n="3.4">tout</w> <w n="3.5">seuls</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Regardons</w> <w n="4.2">les</w> <w n="4.3">méchants</w> <w n="4.4">et</w> <w n="4.5">ceux</w> <w n="4.6">de</w> <w n="4.7">qui</w> <w n="4.8">la</w> <w n="4.9">vie</w></l>
						<l n="5" num="1.5"><w n="5.1">N</w>’<w n="5.2">a</w> <w n="5.3">d</w>’<w n="5.4">autre</w> <w n="5.5">but</w> <w n="5.6">que</w> <w n="5.7">d</w>’<w n="5.8">être</w> <w n="5.9">à</w> <w n="5.10">jamais</w> <w n="5.11">asservie</w></l>
						<l n="6" num="1.6"><w n="6.1">Aux</w> <w n="6.2">choses</w> <w n="6.3">dont</w> <w n="6.4">la</w> <w n="6.5">mort</w> <w n="6.6">fait</w> <w n="6.7">les</w> <w n="6.8">vers</w> <w n="6.9">des</w> <w n="6.10">linceuls</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Vois</w> <w n="7.2">les</w> <w n="7.3">hommes</w> <w n="7.4">des</w> <w n="7.5">champs</w> ; <w n="7.6">vois</w> <w n="7.7">les</w> <w n="7.8">hommes</w> <w n="7.9">des</w> <w n="7.10">villes</w> :</l>
						<l n="8" num="2.2"><w n="8.1">Les</w> <w n="8.2">combats</w> <w n="8.3">étrangers</w> <w n="8.4">ou</w> <w n="8.5">les</w> <w n="8.6">guerres</w> <w n="8.7">civiles</w></l>
						<l n="9" num="2.3"><space unit="char" quantity="12"></space><w n="9.1">Déchirer</w> <w n="9.2">leurs</w> <w n="9.3">esprits</w> ;</l>
						<l n="10" num="2.4"><w n="10.1">Jette</w> <w n="10.2">un</w> <w n="10.3">profond</w> <w n="10.4">regard</w> <w n="10.5">sur</w> <w n="10.6">l</w>’<w n="10.7">histoire</w> <w n="10.8">profonde</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Et</w> <w n="11.2">devant</w> <w n="11.3">les</w> <w n="11.4">forfaits</w> <w n="11.5">entrevus</w> <w n="11.6">sous</w> <w n="11.7">cette</w> <w n="11.8">onde</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Dis</w>-<w n="12.2">moi</w> <w n="12.3">ce</w> <w n="12.4">que</w> <w n="12.5">ressent</w> <w n="12.6">ton</w> <w n="12.7">pauvre</w> <w n="12.8">cœur</w> <w n="12.9">surpris</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Après</w> <w n="13.2">avoir</w> <w n="13.3">sondé</w> <w n="13.4">toutes</w> <w n="13.5">ces</w> <w n="13.6">noires</w> <w n="13.7">choses</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Regarde</w>, <w n="14.2">là</w>, <w n="14.3">tout</w> <w n="14.4">près</w>, <w n="14.5">les</w> <w n="14.6">fleurs</w> <w n="14.7">blanches</w> <w n="14.8">ou</w> <w n="14.9">roses</w></l>
						<l n="15" num="3.3"><space unit="char" quantity="12"></space><w n="15.1">Sourire</w> <w n="15.2">au</w> <w n="15.3">grand</w> <w n="15.4">ciel</w> <w n="15.5">bleu</w> ;</l>
						<l n="16" num="3.4"><w n="16.1">L</w>’<w n="16.2">arbre</w> <w n="16.3">étend</w> <w n="16.4">ses</w> <w n="16.5">longs</w> <w n="16.6">bras</w>, <w n="16.7">lorsque</w> <w n="16.8">avec</w> <w n="16.9">toi</w> <w n="16.10">je</w> <w n="16.11">passe</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Pour</w> <w n="17.2">nous</w> <w n="17.3">bénir</w>, <w n="17.4">et</w> <w n="17.5">Dieu</w> <w n="17.6">rayonne</w> <w n="17.7">dans</w> <w n="17.8">l</w>’<w n="17.9">espace</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Car</w> <w n="18.2">l</w>’<w n="18.3">arbre</w> <w n="18.4">nous</w> <w n="18.5">connaît</w> <w n="18.6">et</w> <w n="18.7">nous</w> <w n="18.8">connaissons</w> <w n="18.9">Dieu</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Amie</w>, <w n="19.2">et</w> <w n="19.3">délivrés</w> <w n="19.4">de</w> <w n="19.5">la</w> <w n="19.6">ville</w> <w n="19.7">lointaine</w></l>
						<l n="20" num="4.2"><w n="20.1">Dont</w> <w n="20.2">le</w> <w n="20.3">bruit</w> <w n="20.4">nous</w> <w n="20.5">arrive</w> <w n="20.6">ainsi</w> <w n="20.7">qu</w>’<w n="20.8">un</w> <w n="20.9">bruit</w> <w n="20.10">de</w> <w n="20.11">chaîne</w>,</l>
						<l n="21" num="4.3"><space unit="char" quantity="12"></space><w n="21.1">Essuie</w> <w n="21.2">enfin</w> <w n="21.3">tes</w> <w n="21.4">pleurs</w> !</l>
						<l n="22" num="4.4"><w n="22.1">Vois</w> : <w n="22.2">la</w> <w n="22.3">brise</w> <w n="22.4">s</w>’<w n="22.5">endort</w> ; <w n="22.6">l</w>’<w n="22.7">eau</w> <w n="22.8">paisible</w> <w n="22.9">s</w>’<w n="22.10">écoule</w> ;</l>
						<l n="23" num="4.5"><w n="23.1">Est</w>-<w n="23.2">il</w> <w n="23.3">bonheur</w> <w n="23.4">plus</w> <w n="23.5">grand</w> <w n="23.6">que</w> <w n="23.7">d</w>’<w n="23.8">oublier</w> <w n="23.9">la</w> <w n="23.10">foule</w>,</l>
						<l n="24" num="4.6"><w n="24.1">D</w>’<w n="24.2">être</w> <w n="24.3">aimé</w> <w n="24.4">des</w> <w n="24.5">oiseaux</w>, <w n="24.6">et</w> <w n="24.7">d</w>’<w n="24.8">être</w> <w n="24.9">aimé</w> <w n="24.10">des</w> <w n="24.11">fleurs</w> ?</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">18-19 janvier 1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>