<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><div type="poem" key="AIC14">
					<head type="number">XI</head>
					<head type="main">CHARITÉ</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Si</w> <w n="1.2">vous</w> <w n="1.3">croyez</w> <w n="1.4">que</w> <w n="1.5">j</w>’<w n="1.6">ai</w> <w n="1.7">l</w>’<w n="1.8">âme</w> <w n="1.9">assez</w> <w n="1.10">abaissée</w></l>
						<l n="2" num="1.2"><w n="2.1">Pour</w> <w n="2.2">porter</w> <w n="2.3">vos</w> <w n="2.4">dédains</w> <w n="2.5">sans</w> <w n="2.6">me</w> <w n="2.7">lever</w> <w n="2.8">un</w> <w n="2.9">jour</w></l>
						<l n="3" num="1.3"><w n="3.1">Si</w> <w n="3.2">vous</w> <w n="3.3">croyez</w> <w n="3.4">en</w> <w n="3.5">moi</w> <w n="3.6">tuer</w> <w n="3.7">toute</w> <w n="3.8">pensée</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">sous</w> <w n="4.3">la</w> <w n="4.4">haine</w> <w n="4.5">froide</w> <w n="4.6">engloutir</w> <w n="4.7">mon</w> <w n="4.8">amour</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Détrompez</w>-<w n="5.2">vous</w> ! <w n="5.3">Sans</w> <w n="5.4">fin</w> <w n="5.5">je</w> <w n="5.6">m</w>’<w n="5.7">élève</w>, <w n="5.8">je</w> <w n="5.9">monte</w> !</l>
						<l n="6" num="2.2"><w n="6.1">Pour</w> <w n="6.2">vous</w> <w n="6.3">voir</w> <w n="6.4">par</w>-<w n="6.5">dessus</w> <w n="6.6">l</w>’<w n="6.7">épaule</w>, <w n="6.8">humiliés</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Moi</w>, <w n="7.2">je</w> <w n="7.3">n</w>’<w n="7.4">ai</w> <w n="7.5">pas</w> <w n="7.6">besoin</w>, <w n="7.7">comme</w> <w n="7.8">vous</w>, <w n="7.9">dans</w> <w n="7.10">la</w> <w n="7.11">honte</w>,</l>
						<l n="8" num="2.4"><w n="8.1">De</w> <w n="8.2">me</w> <w n="8.3">hisser</w>, <w n="8.4">furtif</w>, <w n="8.5">sur</w> <w n="8.6">la</w> <w n="8.7">pointe</w> <w n="8.8">des</w> <w n="8.9">pieds</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">vais</w> <w n="9.3">à</w> <w n="9.4">l</w>’<w n="9.5">Idéal</w>, <w n="9.6">dans</w> <w n="9.7">un</w> <w n="9.8">élan</w> <w n="9.9">suprême</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Mais</w> <w n="10.2">vous</w> <w n="10.3">êtes</w> <w n="10.4">si</w> <w n="10.5">bas</w>, <w n="10.6">je</w> <w n="10.7">vous</w> <w n="10.8">en</w> <w n="10.9">avertis</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Qu</w>’<w n="11.2">on</w> <w n="11.3">ne</w> <w n="11.4">peut</w> <w n="11.5">parmi</w> <w n="11.6">vous</w> <w n="11.7">rester</w>, <w n="11.8">bien</w> <w n="11.9">qu</w>’<w n="11.10">on</w> <w n="11.11">vous</w> <w n="11.12">aime</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Ni</w>, <w n="12.2">lorsqu</w>’<w n="12.3">on</w> <w n="12.4">se</w> <w n="12.5">fait</w> <w n="12.6">grand</w>, <w n="12.7">vous</w> <w n="12.8">faire</w> <w n="12.9">moins</w> <w n="12.10">petits</w>.</l>
					</lg>
					<closer>
						<placeName>Toulon</placeName>.
					</closer>
				</div></body></text></TEI>