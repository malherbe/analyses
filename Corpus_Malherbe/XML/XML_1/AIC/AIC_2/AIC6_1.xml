<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Les Jeunes Croyances</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>1274 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Jeunes Croyances</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Les_Jeunes_Croyances</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Les Jeunes Croyances</title>
								<author>Jean Aicard</author>
								<idno type="URI">https://archive.org/details/lesjeunescroyan00aicagoog</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Alphonse Lemerre Éditeur</publisher>
									<date when="1867">1867</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1867">1867</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">I</head><div type="poem" key="AIC6">
					<head type="number">III</head>
					<head type="main">AIMER-PENSER</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Cœur</w> <w n="1.2">naïf</w> ! <w n="1.3">j</w>’<w n="1.4">avais</w> <w n="1.5">cru</w> <w n="1.6">pouvoir</w> <w n="1.7">à</w> <w n="1.8">tous</w> <w n="1.9">les</w> <w n="1.10">yeux</w></l>
						<l n="2" num="1.2"><w n="2.1">Dévoiler</w> <w n="2.2">mes</w> <w n="2.3">douleurs</w> <w n="2.4">comme</w> <w n="2.5">en</w> <w n="2.6">face</w> <w n="2.7">des</w> <w n="2.8">cieux</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space><w n="3.1">Et</w> <w n="3.2">trouver</w> <w n="3.3">pour</w> <w n="3.4">mon</w> <w n="3.5">âme</w> <w n="3.6">une</w> <w n="3.7">âme</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Une</w> <w n="4.2">seule</w> <w n="4.3">parmi</w> <w n="4.4">la</w> <w n="4.5">foule</w> <w n="4.6">des</w> <w n="4.7">humains</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Un</w> <w n="5.2">inconnu</w> <w n="5.3">qui</w> <w n="5.4">vînt</w> <w n="5.5">me</w> <w n="5.6">prendre</w> <w n="5.7">les</w> <w n="5.8">deux</w> <w n="5.9">mains</w>,</l>
						<l n="6" num="1.6"><space unit="char" quantity="8"></space><w n="6.1">Un</w> <w n="6.2">seul</w> <w n="6.3">amour</w> <w n="6.4">d</w>’<w n="6.5">homme</w> <w n="6.6">ou</w> <w n="6.7">de</w> <w n="6.8">femme</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Pauvre</w> <w n="7.2">fou</w> ! <w n="7.3">je</w> <w n="7.4">croyais</w> <w n="7.5">à</w> <w n="7.6">la</w> <w n="7.7">sainte</w> <w n="7.8">pitié</w></l>
						<l n="8" num="2.2"><w n="8.1">Qui</w> <w n="8.2">verse</w> <w n="8.3">doucement</w> <w n="8.4">et</w> <w n="8.5">longtemps</w> <w n="8.6">l</w>’<w n="8.7">amitié</w></l>
						<l n="9" num="2.3"><space unit="char" quantity="8"></space><w n="9.1">Sur</w> <w n="9.2">les</w> <w n="9.3">blessures</w> <w n="9.4">d</w>’<w n="9.5">un</w> <w n="9.6">cœur</w> <w n="9.7">triste</w>,</l>
						<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">je</w> <w n="10.3">ne</w> <w n="10.4">savais</w> <w n="10.5">pas</w>, — <w n="10.6">honte</w> ! — <w n="10.7">qu</w>’<w n="10.8">au</w> <w n="10.9">lieu</w> <w n="10.10">de</w> <w n="10.11">pleurs</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Le</w> <w n="11.2">monde</w>, <w n="11.3">gai</w> <w n="11.4">toujours</w>, <w n="11.5">donne</w> <w n="11.6">à</w> <w n="11.7">toutes</w> <w n="11.8">douleurs</w></l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">Un</w> <w n="12.2">éclat</w> <w n="12.3">de</w> <w n="12.4">rire</w> <w n="12.5">égoïste</w> !</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">bien</w> ; — <w n="13.4">je</w> <w n="13.5">garderai</w> <w n="13.6">pour</w> <w n="13.7">toi</w>, <w n="13.8">dont</w> <w n="13.9">je</w> <w n="13.10">suis</w> <w n="13.11">sûr</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Pour</w> <w n="14.2">toi</w> <w n="14.3">seule</w> <w n="14.4">et</w> <w n="14.5">pour</w> <w n="14.6">Dieu</w> <w n="14.7">mon</w> <w n="14.8">malheur</w> <w n="14.9">calme</w> <w n="14.10">et</w> <w n="14.11">pur</w></l>
						<l n="15" num="3.3"><space unit="char" quantity="8"></space><w n="15.1">Que</w> <w n="15.2">salirait</w> <w n="15.3">la</w> <w n="15.4">foule</w> <w n="15.5">avare</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Et</w> <w n="16.2">grand</w> <w n="16.3">par</w> <w n="16.4">ma</w> <w n="16.5">douleur</w>, <w n="16.6">et</w> <w n="16.7">grand</w> <w n="16.8">par</w> <w n="16.9">mon</w> <w n="16.10">orgueil</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Si</w> <w n="17.2">dans</w> <w n="17.3">des</w> <w n="17.4">vers</w> <w n="17.5">badins</w> <w n="17.6">je</w> <w n="17.7">lui</w> <w n="17.8">cache</w> <w n="17.9">mon</w> <w n="17.10">deuil</w>,</l>
						<l n="18" num="3.6"><space unit="char" quantity="8"></space><w n="18.1">Elle</w> <w n="18.2">me</w> <w n="18.3">joûra</w> <w n="18.4">sa</w> <w n="18.5">fanfare</w> !</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Et</w> <w n="19.2">quand</w> <w n="19.3">mes</w> <w n="19.4">chants</w> <w n="19.5">auront</w> <w n="19.6">amusé</w> <w n="19.7">les</w> <w n="19.8">pervers</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Toujours</w> <w n="20.2">contents</w> <w n="20.3">de</w> <w n="20.4">voir</w> <w n="20.5">apparaître</w> <w n="20.6">en</w> <w n="20.7">des</w> <w n="20.8">vers</w></l>
						<l n="21" num="4.3"><space unit="char" quantity="8"></space><w n="21.1">Des</w> <w n="21.2">inutilités</w> <w n="21.3">impies</w>,</l>
						<l n="22" num="4.4"><w n="22.1">Je</w> <w n="22.2">crîrai</w>, <w n="22.3">me</w> <w n="22.4">dressant</w>, <w n="22.5">sage</w>, <w n="22.6">au</w>-<w n="22.7">dessus</w> <w n="22.8">des</w> <w n="22.9">fous</w>,</l>
						<l n="23" num="4.5"><w n="23.1">La</w> <w n="23.2">justice</w> <w n="23.3">en</w> <w n="23.4">mes</w> <w n="23.5">mains</w>, <w n="23.6">et</w> <w n="23.7">les</w> <w n="23.8">fustigeant</w> <w n="23.9">tous</w></l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">D</w>’<w n="24.2">un</w> <w n="24.3">fouet</w> <w n="24.4">d</w>’<w n="24.5">ïambe</w> <w n="24.6">et</w> <w n="24.7">d</w>’<w n="24.8">utopies</w> :</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">« <w n="25.1">Ô</w> <w n="25.2">monstres</w> ! <w n="25.3">vous</w> <w n="25.4">avez</w> <w n="25.5">devant</w> <w n="25.6">Dieu</w>, <w n="25.7">devant</w> <w n="25.8">Dieu</w> !</l>
						<l n="26" num="5.2"><space unit="char" quantity="8"></space><w n="26.1">Devant</w> <w n="26.2">le</w> <w n="26.3">firmament</w> <w n="26.4">auguste</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Dressé</w> <w n="27.2">vos</w> <w n="27.3">tréteaux</w> <w n="27.4">vils</w> <w n="27.5">et</w> <w n="27.6">fait</w> <w n="27.7">un</w> <w n="27.8">mauvais</w> <w n="27.9">lieu</w></l>
						<l n="28" num="5.4"><space unit="char" quantity="8"></space><w n="28.1">De</w> <w n="28.2">la</w> <w n="28.3">nature</w> <w n="28.4">belle</w> <w n="28.5">et</w> <w n="28.6">juste</w> !</l>
					</lg>
					<lg n="6">
						<l n="29" num="6.1">« <w n="29.1">Votre</w> <w n="29.2">société</w>, <w n="29.3">sous</w> <w n="29.4">les</w> <w n="29.5">noirs</w> <w n="29.6">préjugés</w>,</l>
						<l n="30" num="6.2"><space unit="char" quantity="8"></space><w n="30.1">Penche</w> <w n="30.2">comme</w> <w n="30.3">un</w> <w n="30.4">vaisseau</w> <w n="30.5">qui</w> <w n="30.6">sombre</w> ;</l>
						<l n="31" num="6.3"><w n="31.1">Rien</w> <w n="31.2">de</w> <w n="31.3">vous</w> <w n="31.4">ne</w> <w n="31.5">vivra</w> ! <w n="31.6">Navire</w> <w n="31.7">et</w> <w n="31.8">naufragés</w>,</l>
						<l n="32" num="6.4"><space unit="char" quantity="8"></space><w n="32.1">Vous</w> <w n="32.2">serez</w> <w n="32.3">engloutis</w> <w n="32.4">par</w> <w n="32.5">l</w>’<w n="32.6">ombre</w> !</l>
					</lg>
					<lg n="7">
						<l n="33" num="7.1">« <w n="33.1">Ah</w> ! <w n="33.2">vous</w> <w n="33.3">vous</w> <w n="33.4">êtes</w> <w n="33.5">dit</w>, <w n="33.6">en</w> <w n="33.7">votre</w> <w n="33.8">lâcheté</w>,</l>
						<l n="34" num="7.2"><space unit="char" quantity="8"></space><w n="34.1">Que</w> <w n="34.2">le</w> <w n="34.3">mal</w> <w n="34.4">sur</w> <w n="34.5">le</w> <w n="34.6">monde</w> <w n="34.7">règne</w> ;</l>
						<l n="35" num="7.3"><w n="35.1">Qu</w>’<w n="35.2">il</w> <w n="35.3">doit</w> <w n="35.4">régner</w> <w n="35.5">toujours</w> ; <w n="35.6">qu</w>’<w n="35.7">une</w> <w n="35.8">fatalité</w></l>
						<l n="36" num="7.4"><space unit="char" quantity="8"></space><w n="36.1">Veut</w> <w n="36.2">que</w> <w n="36.3">toujours</w> <w n="36.4">un</w> <w n="36.5">Jésus</w> <w n="36.6">saigne</w> !</l>
					</lg>
					<lg n="8">
						<l n="37" num="8.1">« <w n="37.1">Ah</w> ! <w n="37.2">vous</w> <w n="37.3">traitez</w> <w n="37.4">encor</w> <w n="37.5">d</w>’<w n="37.6">insensés</w> <w n="37.7">les</w> <w n="37.8">penseurs</w>,</l>
						<l n="38" num="8.2"><space unit="char" quantity="8"></space><w n="38.1">Les</w> <w n="38.2">libres</w> <w n="38.3">rêveurs</w>, <w n="38.4">les</w> <w n="38.5">poëtes</w>,</l>
						<l n="39" num="8.3"><w n="39.1">Qui</w>, — <w n="39.2">lorsque</w> <w n="39.3">vous</w> <w n="39.4">croisez</w> <w n="39.5">vos</w> <w n="39.6">haines</w>, — <w n="39.7">âmes</w> <w n="39.8">sœurs</w></l>
						<l n="40" num="8.4"><space unit="char" quantity="8"></space><w n="40.1">Gémissent</w> <w n="40.2">sur</w> <w n="40.3">ce</w> <w n="40.4">que</w> <w n="40.5">vous</w> <w n="40.6">faites</w> !</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1">« <w n="41.1">Ah</w> ! <w n="41.2">vous</w> <w n="41.3">pourriez</w> <w n="41.4">trouver</w> <w n="41.5">dans</w> <w n="41.6">l</w>’<w n="41.7">éternelle</w> <w n="41.8">paix</w></l>
						<l n="42" num="9.2"><space unit="char" quantity="8"></space><w n="42.1">Une</w> <w n="42.2">félicité</w> <w n="42.3">profonde</w>,</l>
						<l n="43" num="9.3"><w n="43.1">Et</w> <w n="43.2">vous</w> <w n="43.3">ne</w> <w n="43.4">voulez</w> <w n="43.5">pas</w>, <w n="43.6">et</w> <w n="43.7">vos</w> <w n="43.8">esprits</w> <w n="43.9">épais</w></l>
						<l n="44" num="9.4"><space unit="char" quantity="8"></space><w n="44.1">Se</w> <w n="44.2">vautrent</w> <w n="44.3">dans</w> <w n="44.4">la</w> <w n="44.5">nuit</w> <w n="44.6">immonde</w> !</l>
					</lg>
					<lg n="10">
						<l n="45" num="10.1">« <w n="45.1">Vous</w> <w n="45.2">célébrez</w> <w n="45.3">en</w> <w n="45.4">chœur</w> <w n="45.5">arlequins</w> <w n="45.6">et</w> <w n="45.7">bouffons</w> ;</l>
						<l n="46" num="10.2"><space unit="char" quantity="8"></space><w n="46.1">Vous</w> <w n="46.2">pensiez</w> <w n="46.3">que</w>, <w n="46.4">bête</w> <w n="46.5">acrobate</w>,</l>
						<l n="47" num="10.3"><w n="47.1">J</w>’<w n="47.2">avais</w> <w n="47.3">fait</w> <w n="47.4">pour</w> <w n="47.5">mon</w> <w n="47.6">âme</w> <w n="47.7">un</w> <w n="47.8">habit</w> <w n="47.9">de</w> <w n="47.10">chiffons</w> ;</l>
						<l n="48" num="10.4"><space unit="char" quantity="8"></space><w n="48.1">Que</w> <w n="48.2">mon</w> <w n="48.3">vers</w> <w n="48.4">était</w> <w n="48.5">une</w> <w n="48.6">batte</w> ?</l>
					</lg>
					<lg n="11">
						<l n="49" num="11.1">« <w n="49.1">Eh</w> <w n="49.2">bien</w>, <w n="49.3">détrompez</w>-<w n="49.4">vous</w> : <w n="49.5">quand</w> <w n="49.6">j</w>’<w n="49.7">ai</w> <w n="49.8">pleuré</w>, <w n="49.9">méchants</w>,</l>
						<l n="50" num="11.2"><space unit="char" quantity="8"></space><w n="50.1">Contre</w> <w n="50.2">moi</w> <w n="50.3">vous</w> <w n="50.4">tourniez</w> <w n="50.5">vos</w> <w n="50.6">armes</w> ;</l>
						<l n="51" num="11.3"><w n="51.1">Lorsqu</w>’<w n="51.2">ils</w> <w n="51.3">semblaient</w> <w n="51.4">rieurs</w>, <w n="51.5">vous</w> <w n="51.6">admiriez</w> <w n="51.7">mes</w> <w n="51.8">chants</w>,</l>
						<l n="52" num="11.4"><space unit="char" quantity="8"></space><w n="52.1">Ignorant</w> <w n="52.2">qu</w>’<w n="52.3">ils</w> <w n="52.4">étaient</w> <w n="52.5">des</w> <w n="52.6">larmes</w> !</l>
					</lg>
					<lg n="12">
						<l n="53" num="12.1">« <w n="53.1">Votre</w> <w n="53.2">immense</w> <w n="53.3">mépris</w>, <w n="53.4">je</w> <w n="53.5">le</w> <w n="53.6">compte</w> <w n="53.7">pour</w> <w n="53.8">rien</w>,</l>
						<l n="54" num="12.2"><space unit="char" quantity="8"></space><w n="54.1">Pour</w> <w n="54.2">rien</w> <w n="54.3">vos</w> <w n="54.4">paroles</w> <w n="54.5">amères</w> !</l>
						<l n="55" num="12.3">« <w n="55.1">Je</w> <w n="55.2">suis</w> <w n="55.3">plus</w> <w n="55.4">grand</w> <w n="55.5">que</w> <w n="55.6">vous</w>, <w n="55.7">car</w> <w n="55.8">je</w> <w n="55.9">travaille</w> <w n="55.10">au</w> <w n="55.11">Bien</w> !</l>
						<l n="56" num="12.4"><space unit="char" quantity="8"></space><w n="56.1">J</w>’<w n="56.2">ai</w> <w n="56.3">pitié</w>, <w n="56.4">moi</w>, <w n="56.5">de</w> <w n="56.6">vos</w> <w n="56.7">misères</w> !</l>
					</lg>
					<lg n="13">
						<l n="57" num="13.1"><w n="57.1">Et</w> <w n="57.2">je</w> <w n="57.3">vais</w> <w n="57.4">seul</w>… <w n="57.5">j</w>’<w n="57.6">avance</w> : <w n="57.7">en</w> <w n="57.8">ma</w> <w n="57.9">force</w> <w n="57.10">j</w>’<w n="57.11">ai</w> <w n="57.12">foi</w> ;</l>
						<l n="58" num="13.2"><space unit="char" quantity="8"></space><w n="58.1">Je</w> <w n="58.2">suis</w> <w n="58.3">l</w>’<w n="58.4">homme</w> <w n="58.5">du</w> <w n="58.6">sacrifice</w> !</l>
						<l n="59" num="13.3"><w n="59.1">Et</w> <w n="59.2">quand</w> <w n="59.3">vous</w> <w n="59.4">serez</w> <w n="59.5">tous</w> <w n="59.6">insensés</w> <w n="59.7">comme</w> <w n="59.8">moi</w>,</l>
						<l n="60" num="13.4"><space unit="char" quantity="8"></space><w n="60.1">Alors</w> <w n="60.2">régnera</w> <w n="60.3">la</w> <w n="60.4">justice</w> ! »</l>
					</lg>
					<lg n="14">
						<l n="61" num="14.1"><w n="61.1">C</w>’<w n="61.2">est</w> <w n="61.3">afin</w> <w n="61.4">de</w> <w n="61.5">plus</w> <w n="61.6">tôt</w> <w n="61.7">les</w> <w n="61.8">accabler</w> <w n="61.9">ainsi</w></l>
						<l n="62" num="14.2"><w n="62.1">Que</w> <w n="62.2">je</w> <w n="62.3">ne</w> <w n="62.4">veux</w> <w n="62.5">pas</w> <w n="62.6">mettre</w> <w n="62.7">à</w> <w n="62.8">leur</w> <w n="62.9">folle</w> <w n="62.10">merci</w></l>
						<l n="63" num="14.3"><space unit="char" quantity="8"></space><w n="63.1">Plus</w> <w n="63.2">longtemps</w> <w n="63.3">mon</w> <w n="63.4">âme</w> <w n="63.5">brisée</w> ;</l>
						<l n="64" num="14.4"><w n="64.1">Désormais</w> <w n="64.2">nul</w> <w n="64.3">d</w>’<w n="64.4">entre</w> <w n="64.5">eux</w> <w n="64.6">ne</w> <w n="64.7">saura</w> <w n="64.8">ma</w> <w n="64.9">douleur</w> :</l>
						<l n="65" num="14.5"><w n="65.1">À</w> <w n="65.2">toi</w> <w n="65.3">je</w> <w n="65.4">veux</w> <w n="65.5">livrer</w> <w n="65.6">ma</w> <w n="65.7">pensée</w> <w n="65.8">et</w> <w n="65.9">mon</w> <w n="65.10">cœur</w> !…</l>
						<l n="66" num="14.6"><space unit="char" quantity="8"></space><w n="66.1">Ils</w> <w n="66.2">n</w>’<w n="66.3">auront</w>, <w n="66.4">eux</w>, <w n="66.5">que</w> <w n="66.6">ma</w> <w n="66.7">pensée</w> !</l>
					</lg>
					<closer>
						<dateline>
							<placeName>Toulon</placeName>,
							<date when="1866">13-14 janvier 1866</date>.
						</dateline>
					</closer>
				</div></body></text></TEI>