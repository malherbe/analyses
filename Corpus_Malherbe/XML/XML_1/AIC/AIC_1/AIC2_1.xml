<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Jeanne Darc</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>152 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Jeanne Darc</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Jeanne_d%E2%80%99Arc_:_le_rachat_de_la_tour</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Jeanne Darc</title>
								<author>Jean Aicard</author>
								<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54710859/f5.image</idno>
								<imprint>
									<pubPlace>Toulon</pubPlace>
									<publisher>mpr. de E. Aurel</publisher>
									<date when="1866">1866</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1866">1866</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE MARTYRE</head><div type="poem" key="AIC2">
					<head type="number">II</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">ces</w> <w n="1.3">jours</w> <w n="1.4">en</w> <w n="1.5">deuil</w> <w n="1.6">où</w> <w n="1.7">la</w> <w n="1.8">France</w></l>
						<l n="2" num="1.2"><w n="2.1">Courbait</w> <w n="2.2">son</w> <w n="2.3">front</w> <w n="2.4">pâle</w>, <w n="2.5">abattu</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Ils</w> <w n="3.2">murmuraient</w> : « <w n="3.3">Pleins</w> <w n="3.4">d</w>’<w n="3.5">espérance</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Longtemps</w> <w n="4.2">nous</w> <w n="4.3">avons</w> <w n="4.4">combattu</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Toi</w>, <w n="5.2">quel</w> <w n="5.3">est</w> <w n="5.4">ton</w> <w n="5.5">espoir</w>, <w n="5.6">ô</w> <w n="5.7">femme</w></l>
						<l n="6" num="1.6"><w n="6.1">Dont</w> <w n="6.2">un</w> <w n="6.3">souffle</w> <w n="6.4">briserait</w> <w n="6.5">l</w>’<w n="6.6">âme</w> ? »</l>
						<l n="7" num="1.7"><w n="7.1">Elle</w> <w n="7.2">dit</w> : « <w n="7.3">Je</w> <w n="7.4">veux</w> <w n="7.5">un</w> <w n="7.6">drapeau</w> !</l>
						<l n="8" num="1.8"><w n="8.1">Je</w> <w n="8.2">veux</w> <w n="8.3">t</w>’<w n="8.4">aimer</w>, <w n="8.5">France</w>, <w n="8.6">ma</w> <w n="8.7">mère</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">dans</w> <w n="9.3">la</w> <w n="9.4">mêlée</w> <w n="9.5">en</w> <w n="9.6">colère</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Que</w> <w n="10.2">mon</w> <w n="10.3">glaive</w> <w n="10.4">dorme</w> <w n="10.5">au</w> <w n="10.6">fourreau</w> ! »</l>
					</lg>
					<lg n="2">
						<l n="11" num="2.1"><w n="11.1">Jeanne</w>, <w n="11.2">merci</w> ! — <w n="11.3">Comme</w> <w n="11.4">une</w> <w n="11.5">Idée</w>,</l>
						<l n="12" num="2.2"><w n="12.1">Glaive</w> <w n="12.2">au</w> <w n="12.3">repos</w>, <w n="12.4">bannière</w> <w n="12.5">au</w> <w n="12.6">vent</w>,</l>
						<l n="13" num="2.3"><w n="13.1">Luis</w> <w n="13.2">sur</w> <w n="13.3">la</w> <w n="13.4">France</w> <w n="13.5">fécondée</w></l>
						<l n="14" num="2.4"><w n="14.1">Où</w> <w n="14.2">n</w>’<w n="14.3">est</w> <w n="14.4">plus</w> <w n="14.5">un</w> <w n="14.6">anglais</w> <w n="14.7">vivant</w> !</l>
						<l n="15" num="2.5"><w n="15.1">Pour</w> <w n="15.2">tant</w> <w n="15.3">de</w> <w n="15.4">victoires</w> <w n="15.5">divines</w>,</l>
						<l n="16" num="2.6"><w n="16.1">Que</w> <w n="16.2">veux</w>-<w n="16.3">tu</w> ? — « <w n="16.4">Revoir</w> <w n="16.5">mes</w> <w n="16.6">collines</w> ! »</l>
						<l n="17" num="2.7"><w n="17.1">Ô</w> <w n="17.2">Jeanne</w>, <w n="17.3">suprême</w> <w n="17.4">soutien</w>,</l>
						<l n="18" num="2.8"><w n="18.1">Ton</w> <w n="18.2">peuple</w>, <w n="18.3">formidable</w> <w n="18.4">armée</w></l>
						<l n="19" num="2.9"><w n="19.1">À</w> <w n="19.2">ta</w> <w n="19.3">vue</w> <w n="19.4">enthousiasmée</w>,</l>
						<l n="20" num="2.10"><w n="20.1">Si</w> <w n="20.2">tu</w> <w n="20.3">disparais</w> <w n="20.4">n</w>’<w n="20.5">est</w> <w n="20.6">plus</w> <w n="20.7">rien</w> !</l>
					</lg>
					<lg n="3">
						<l n="21" num="3.1"><w n="21.1">Elle</w> <w n="21.2">resta</w>, <w n="21.3">tuant</w> <w n="21.4">en</w> <w n="21.5">elle</w></l>
						<l n="22" num="3.2"><w n="22.1">Les</w> <w n="22.2">jeunes</w> <w n="22.3">songes</w> <w n="22.4">du</w> <w n="22.5">bonheur</w>.</l>
						<l n="23" num="3.3"><w n="23.1">France</w> ! <w n="23.2">qu</w>’<w n="23.3">elle</w> <w n="23.4">était</w> <w n="23.5">grande</w> <w n="23.6">et</w> <w n="23.7">belle</w>,</l>
						<l n="24" num="3.4"><w n="24.1">L</w>’<w n="24.2">enfant</w> <w n="24.3">sans</w> <w n="24.4">reproche</w> <w n="24.5">et</w> <w n="24.6">sans</w> <w n="24.7">peur</w> !</l>
						<l n="25" num="3.5"><w n="25.1">Le</w> <w n="25.2">Seigneur</w> <w n="25.3">jettera</w> <w n="25.4">sans</w> <w n="25.5">doute</w></l>
						<l n="26" num="3.6"><w n="26.1">Tous</w> <w n="26.2">les</w> <w n="26.3">paradis</w> <w n="26.4">en</w> <w n="26.5">sa</w> <w n="26.6">route</w> ?</l>
						<l n="27" num="3.7"><w n="27.1">Non</w>, <w n="27.2">mais</w> <w n="27.3">l</w>’<w n="27.4">horreur</w>, <w n="27.5">la</w> <w n="27.6">trahison</w> ;</l>
						<l n="28" num="3.8"><w n="28.1">Et</w> <w n="28.2">sur</w> <w n="28.3">la</w> <w n="28.4">vierge</w> <w n="28.5">qu</w>’<w n="28.6">on</w> <w n="28.7">insulte</w>,</l>
						<l n="29" num="3.9"><w n="29.1">Après</w> <w n="29.2">la</w> <w n="29.3">guerre</w> <w n="29.4">et</w> <w n="29.5">son</w> <w n="29.6">tumulte</w>,</l>
						<l n="30" num="3.10"><w n="30.1">La</w> <w n="30.2">solitude</w> <w n="30.3">et</w> <w n="30.4">la</w> <w n="30.5">prison</w> !</l>
					</lg>
					<lg n="4">
						<l n="31" num="4.1"><w n="31.1">Le</w> <w n="31.2">roi</w> <w n="31.3">dort</w> <w n="31.4">dans</w> <w n="31.5">sa</w> <w n="31.6">nonchalance</w> ;</l>
						<l n="32" num="4.2"><w n="32.1">Tes</w> <w n="32.2">chevaliers</w> <w n="32.3">vont</w> <w n="32.4">accourir</w> ;</l>
						<l n="33" num="4.3"><w n="33.1">Tu</w> <w n="33.2">ne</w> <w n="33.3">peux</w>, <w n="33.4">sous</w> <w n="33.5">tant</w> <w n="33.6">de</w> <w n="33.7">souffrance</w>,</l>
						<l n="34" num="4.4"><w n="34.1">Fille</w> <w n="34.2">de</w> <w n="34.3">Dieu</w>, <w n="34.4">vivre</w> <w n="34.5">et</w> <w n="34.6">périr</w> !</l>
						<l n="35" num="4.5"><w n="35.1">Ah</w> ! <w n="35.2">ton</w> <w n="35.3">peuple</w> <w n="35.4">grandi</w> <w n="35.5">se</w> <w n="35.6">lève</w> ;</l>
						<l n="36" num="4.6"><w n="36.1">Il</w> <w n="36.2">va</w> <w n="36.3">broyer</w> <w n="36.4">ces</w> <w n="36.5">murs</w> !… <w n="36.6">vain</w> <w n="36.7">rêve</w> !</l>
						<l n="37" num="4.7"><w n="37.1">Roi</w>, <w n="37.2">Chevaliers</w>, <w n="37.3">Peuple</w>, — <w n="37.4">tout</w> <w n="37.5">dort</w>.</l>
						<l n="38" num="4.8"><w n="38.1">À</w> <w n="38.2">quoi</w> <w n="38.3">bon</w> <w n="38.4">te</w> <w n="38.5">fier</w> <w n="38.6">aux</w> <w n="38.7">hommes</w> ?</l>
						<l n="39" num="4.9"><w n="39.1">Tu</w> <w n="39.2">ne</w> <w n="39.3">sais</w> <w n="39.4">quels</w> <w n="39.5">ingrats</w> <w n="39.6">nous</w> <w n="39.7">sommes</w> !</l>
						<l n="40" num="4.10"><w n="40.1">Ta</w> <w n="40.2">délivrance</w>, <w n="40.3">c</w>’<w n="40.4">est</w> <w n="40.5">la</w> <w n="40.6">mort</w> !</l>
					</lg>
					<lg n="5">
						<l n="41" num="5.1"><w n="41.1">Un</w> <w n="41.2">conseil</w> <w n="41.3">de</w> <w n="41.4">prêtres</w> <w n="41.5">s</w>’<w n="41.6">assemble</w> ;</l>
						<l n="42" num="5.2"><w n="42.1">Les</w> <w n="42.2">Anglais</w> <w n="42.3">tiennent</w> <w n="42.4">leur</w> <w n="42.5">vainqueur</w>.</l>
						<l n="43" num="5.3"><w n="43.1">La</w> <w n="43.2">Pucelle</w> <w n="43.3">s</w>’<w n="43.4">avance</w> <w n="43.5">et</w> <w n="43.6">tremble</w></l>
						<l n="44" num="5.4"><w n="44.1">Timide</w>, <w n="44.2">la</w> <w n="44.3">main</w> <w n="44.4">sur</w> <w n="44.5">le</w> <w n="44.6">cœur</w>.</l>
						<l n="45" num="5.5"><w n="45.1">Meurtrissant</w> <w n="45.2">son</w> <w n="45.3">âme</w> <w n="45.4">meurtrie</w> :</l>
						<l n="46" num="5.6">« <w n="46.1">Il</w> <w n="46.2">faut</w> <w n="46.3">renier</w> <w n="46.4">ta</w> <w n="46.5">patrie</w>,</l>
						<l n="47" num="5.7"><w n="47.1">Ton</w> <w n="47.2">roi</w>, <w n="47.3">criaient</w>-<w n="47.4">ils</w>, <w n="47.5">et</w> <w n="47.6">ton</w> <w n="47.7">Dieu</w> ! ».</l>
						<l n="48" num="5.8">« <w n="48.1">Non</w> ! » <w n="48.2">répond</w>-<w n="48.3">elle</w>, <w n="48.4">faible</w> <w n="48.5">et</w> <w n="48.6">forte</w>,</l>
						<l n="49" num="5.9"><w n="49.1">Et</w> <w n="49.2">du</w> <w n="49.3">cachot</w> <w n="49.4">passant</w> <w n="49.5">la</w> <w n="49.6">porte</w>,</l>
						<l n="50" num="5.10"><w n="50.1">Sublime</w>, <w n="50.2">elle</w> <w n="50.3">se</w> <w n="50.4">livre</w> <w n="50.5">au</w> <w n="50.6">feu</w> !</l>
					</lg>
					<lg n="6">
						<l n="51" num="6.1"><w n="51.1">Avec</w> <w n="51.2">tous</w> <w n="51.3">ses</w> <w n="51.4">rayons</w>, <w n="51.5">ta</w> <w n="51.6">gloire</w></l>
						<l n="52" num="6.2"><w n="52.1">Ici</w> <w n="52.2">nous</w> <w n="52.3">apparaît</w>, <w n="52.4">enfant</w> !</l>
						<l n="53" num="6.3"><w n="53.1">Ce</w> <w n="53.2">n</w>’<w n="53.3">est</w> <w n="53.4">point</w> <w n="53.5">ta</w> <w n="53.6">longue</w> <w n="53.7">victoire</w>,</l>
						<l n="54" num="6.4"><w n="54.1">Reims</w>, <w n="54.2">ni</w> <w n="54.3">le</w> <w n="54.4">sacre</w> <w n="54.5">triomphant</w> ;</l>
						<l n="55" num="6.5"><w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">de</w> <w n="55.4">faire</w> <w n="55.5">pâlir</w> <w n="55.6">ces</w> <w n="55.7">traîtres</w>,</l>
						<l n="56" num="6.6"><w n="56.1">D</w>’<w n="56.2">effrayer</w> <w n="56.3">ces</w> <w n="56.4">bourreaux</w>, <w n="56.5">tes</w> <w n="56.6">maîtres</w> ;</l>
						<l n="57" num="6.7"><w n="57.1">D</w>’<w n="57.2">avilir</w> <w n="57.3">leur</w> <w n="57.4">orgueil</w> <w n="57.5">brutal</w> ;</l>
						<l n="58" num="6.8"><w n="58.1">Par</w> <w n="58.2">ta</w> <w n="58.3">mort</w> <w n="58.4">ta</w> <w n="58.5">vie</w> <w n="58.6">est</w> <w n="58.7">complète</w> !</l>
						<l n="59" num="6.9"><w n="59.1">C</w>’<w n="59.2">est</w> <w n="59.3">un</w> <w n="59.4">triomphe</w>, <w n="59.5">ta</w> <w n="59.6">défaite</w> !</l>
						<l n="60" num="6.10"><w n="60.1">Ton</w> <w n="60.2">bûcher</w>, <w n="60.3">c</w>’<w n="60.4">est</w> <w n="60.5">un</w> <w n="60.6">piédestal</w> !</l>
					</lg>
					<lg n="7">
						<l n="61" num="7.1"><w n="61.1">Tout</w> <w n="61.2">est</w> <w n="61.3">consommé</w> : <w n="61.4">le</w> <w n="61.5">supplice</w></l>
						<l n="62" num="7.2"><w n="62.1">L</w>’<w n="62.2">a</w> <w n="62.3">prise</w> <w n="62.4">à</w> <w n="62.5">la</w> <w n="62.6">face</w> <w n="62.7">des</w> <w n="62.8">cieux</w> ;</l>
						<l n="63" num="7.3"><w n="63.1">Son</w> <w n="63.2">grandiose</w> <w n="63.3">sacrifice</w></l>
						<l n="64" num="7.4"><w n="64.1">S</w>’<w n="64.2">efface</w> <w n="64.3">des</w> <w n="64.4">cœurs</w> <w n="64.5">oublieux</w>.</l>
						<l n="65" num="7.5"><w n="65.1">Avec</w> <w n="65.2">son</w> <w n="65.3">échafaud</w> <w n="65.4">s</w>’<w n="65.5">écroule</w></l>
						<l n="66" num="7.6"><w n="66.1">Le</w> <w n="66.2">souvenir</w>, — <w n="66.3">et</w> <w n="66.4">de</w> <w n="66.5">la</w> <w n="66.6">foule</w></l>
						<l n="67" num="7.7"><w n="67.1">S</w>’<w n="67.2">éteint</w> <w n="67.3">la</w> <w n="67.4">honte</w> <w n="67.5">et</w> <w n="67.6">le</w> <w n="67.7">remord</w> ;</l>
						<l n="68" num="7.8"><w n="68.1">Sur</w> <w n="68.2">ces</w> <w n="68.3">cendres</w>, <w n="68.4">nulle</w> <w n="68.5">statue</w>,</l>
						<l n="69" num="7.9"><w n="69.1">Magnifique</w>, <w n="69.2">ne</w> <w n="69.3">perpétue</w></l>
						<l n="70" num="7.10"><w n="70.1">Cette</w> <w n="70.2">existence</w> <w n="70.3">et</w> <w n="70.4">cette</w> <w n="70.5">mort</w> !</l>
					</lg>
					<lg n="8">
						<l n="71" num="8.1"><w n="71.1">Mais</w> <w n="71.2">Quelqu</w>’<w n="71.3">un</w> <w n="71.4">a</w> <w n="71.5">veillé</w>, <w n="71.6">qui</w> <w n="71.7">laisse</w>,</l>
						<l n="72" num="8.2"><w n="72.1">Lorsque</w> <w n="72.2">le</w> <w n="72.3">temps</w> <w n="72.4">écrase</w> <w n="72.5">tout</w>,</l>
						<l n="73" num="8.3"><w n="73.1">Telle</w> <w n="73.2">qu</w>’<w n="73.3">une</w> <w n="73.4">ombre</w> <w n="73.5">vengeresse</w>,</l>
						<l n="74" num="8.4"><w n="74.1">La</w> <w n="74.2">Prison</w> <w n="74.3">de</w> <w n="74.4">Jeanne</w> <w n="74.5">debout</w>.</l>
						<l n="75" num="8.5"><w n="75.1">Voilà</w> <w n="75.2">le</w> <w n="75.3">monument</w>, <w n="75.4">ô</w> <w n="75.5">France</w> !</l>
						<l n="76" num="8.6"><w n="76.1">Voilà</w> <w n="76.2">le</w> <w n="76.3">piédestal</w> <w n="76.4">immense</w> !</l>
						<l n="77" num="8.7"><w n="77.1">Et</w> <w n="77.2">quand</w> <w n="77.3">tu</w> <w n="77.4">l</w>’<w n="77.5">auras</w> <w n="77.6">acheté</w>,</l>
						<l n="78" num="8.8"><w n="78.1">Consolidant</w> <w n="78.2">cette</w> <w n="78.3">ruine</w>,</l>
						<l n="79" num="8.9"><w n="79.1">Rouen</w>, <w n="79.2">tombeau</w> <w n="79.3">de</w> <w n="79.4">l</w>’<w n="79.5">héroïne</w>,</l>
						<l n="80" num="8.10"><w n="80.1">Sera</w> <w n="80.2">son</w> <w n="80.3">immortalité</w> !</l>
					</lg>
				</div></body></text></TEI>