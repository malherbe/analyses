<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">Jeanne Darc</title>
				<title type="sub">Le Rachat de la Tour</title>
				<title type="medium">Édition électronique</title>
				<author key="AIC">
					<name>
						<forename>Jean</forename>
						<surname>AICARD</surname>
					</name>
					<date from="1848" to="1921">1848-1921</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					 (EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, application des programmes d’analyse, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>152 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2017">2017</date>
				<idno type="local">AIC_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Jeanne Darc</title>
						<author>Jean Aicard</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URI">https://fr.wikisource.org/wiki/Jeanne_d%E2%80%99Arc_:_le_rachat_de_la_tour</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Jeanne Darc</title>
								<author>Jean Aicard</author>
								<idno type="URI">http://gallica.bnf.fr/ark:/12148/bpt6k54710859/f5.image</idno>
								<imprint>
									<pubPlace>Toulon</pubPlace>
									<publisher>mpr. de E. Aurel</publisher>
									<date when="1866">1866</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1866">1866</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Extraction à partir d’une version non définitive du texte sur wikisource.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2017-11-01" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.1.xsd)</change>
				<change when="2017-11-01" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">LE MARTYRE</head><div type="poem" key="AIC1">
					<head type="number">I</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Par</w> <w n="1.2">de</w> <w n="1.3">cruels</w> <w n="1.4">enfants</w> <w n="1.5">une</w> <w n="1.6">femme</w> <w n="1.7">suivie</w></l>
						<l n="2" num="1.2"><w n="2.1">Se</w> <w n="2.2">traînait</w>. <w n="2.3">En</w> <w n="2.4">ses</w> <w n="2.5">yeux</w> <w n="2.6">la</w> <w n="2.7">mort</w> <w n="2.8">avec</w> <w n="2.9">la</w> <w n="2.10">vie</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Hagardes</w>, <w n="3.2">s</w>’<w n="3.3">éteignaient</w>, <w n="3.4">se</w> <w n="3.5">rallumaient</w> <w n="3.6">toujours</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Les</w> <w n="4.2">enfants</w> <w n="4.3">la</w> <w n="4.4">heurtaient</w> <w n="4.5">au</w> <w n="4.6">choc</w> <w n="4.7">de</w> <w n="4.8">leurs</w> <w n="4.9">batailles</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Elle</w>, <w n="5.2">pour</w> <w n="5.3">se</w> <w n="5.4">venger</w>, <w n="5.5">déchirait</w> <w n="5.6">ses</w> <w n="5.7">entrailles</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Folle</w>, <w n="6.2">qui</w> <w n="6.3">s</w>’<w n="6.4">arrachait</w> <w n="6.5">à</w> <w n="6.6">son</w> <w n="6.7">propre</w> <w n="6.8">secours</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Or</w>, <w n="7.2">la</w> <w n="7.3">France</w> <w n="7.4">a</w> <w n="7.5">jadis</w> <w n="7.6">souffert</w> <w n="7.7">cette</w> <w n="7.8">souffrance</w> ;</l>
						<l n="8" num="2.2"><w n="8.1">L</w>’<w n="8.2">Anglais</w> <w n="8.3">la</w> <w n="8.4">dépeçait</w> <w n="8.5">longuement</w>. <w n="8.6">Pauvre</w> <w n="8.7">France</w> !</l>
						<l n="9" num="2.3"><w n="9.1">Elle</w>-<w n="9.2">même</w> <w n="9.3">mordait</w> <w n="9.4">ses</w> <w n="9.5">bras</w>, <w n="9.6">crevait</w> <w n="9.7">son</w> <w n="9.8">sein</w>,</l>
						<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">tandis</w> <w n="10.3">que</w>, <w n="10.4">tombée</w>, <w n="10.5">elle</w> <w n="10.6">rendait</w> <w n="10.7">son</w> <w n="10.8">râle</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Le</w> <w n="11.2">roi</w> <w n="11.3">dansait</w> <w n="11.4">au</w> <w n="11.5">bruit</w> <w n="11.6">d</w>’<w n="11.7">une</w> <w n="11.8">chanson</w> <w n="11.9">banale</w>,</l>
						<l n="12" num="2.6"><space unit="char" quantity="8"></space><w n="12.1">Froid</w> <w n="12.2">complice</w> <w n="12.3">de</w> <w n="12.4">l</w>’<w n="12.5">assassin</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Et</w> <w n="13.2">parmi</w> <w n="13.3">les</w> <w n="13.4">enfants</w> <w n="13.5">de</w> <w n="13.6">la</w> <w n="13.7">mère</w>-<w n="13.8">patrie</w></l>
						<l n="14" num="3.2"><w n="14.1">Pas</w> <w n="14.2">un</w> <w n="14.3">qui</w> <w n="14.4">se</w> <w n="14.5">levât</w>, <w n="14.6">fier</w>, <w n="14.7">et</w> <w n="14.8">l</w>’<w n="14.9">âme</w> <w n="14.10">attendrie</w>,</l>
						<l n="15" num="3.3"><w n="15.1">Chassant</w> <w n="15.2">le</w> <w n="15.3">léopard</w> <w n="15.4">vil</w> <w n="15.5">qui</w> <w n="15.6">la</w> <w n="15.7">dévorait</w>.</l>
						<l n="16" num="3.4"><w n="16.1">Sentant</w> <w n="16.2">plus</w> <w n="16.3">de</w> <w n="16.4">pitié</w> <w n="16.5">que</w> <w n="16.6">ses</w> <w n="16.7">frères</w> <w n="16.8">dans</w> <w n="16.9">l</w>’<w n="16.10">âme</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Ne</w> <w n="17.2">voyant</w> <w n="17.3">que</w> <w n="17.4">des</w> <w n="17.5">cœurs</w> <w n="17.6">efféminés</w>, <w n="17.7">la</w> <w n="17.8">femme</w></l>
						<l n="18" num="3.6"><w n="18.1">Se</w> <w n="18.2">fait</w> <w n="18.3">homme</w> — <w n="18.4">et</w> <w n="18.5">soudain</w> <w n="18.6">Jeanne</w> <w n="18.7">Darc</w> <w n="18.8">apparaît</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Vous</w> <w n="19.2">la</w> <w n="19.3">connaissez</w> <w n="19.4">tous</w>, <w n="19.5">cette</w> <w n="19.6">figure</w> <w n="19.7">étrange</w>,</l>
						<l n="20" num="4.2"><w n="20.1">Cette</w> <w n="20.2">vierge</w> <w n="20.3">domptant</w> <w n="20.4">les</w> <w n="20.5">vieux</w> <w n="20.6">guerriers</w>, — <w n="20.7">cet</w> <w n="20.8">ange</w>,</l>
						<l n="21" num="4.3"><w n="21.1">Qui</w>, <w n="21.2">l</w>’<w n="21.3">auréole</w> <w n="21.4">au</w> <w n="21.5">front</w>, <w n="21.6">traverse</w>, <w n="21.7">tout</w>-<w n="21.8">puissant</w>,</l>
						<l n="22" num="4.4"><w n="22.1">Les</w> <w n="22.2">livides</w> <w n="22.3">lueurs</w> <w n="22.4">de</w> <w n="22.5">l</w>’<w n="22.6">orage</w> <w n="22.7">des</w> <w n="22.8">armes</w>,</l>
						<l n="23" num="4.5"><w n="23.1">Et</w>, <w n="23.2">faible</w> <w n="23.3">enfant</w>, <w n="23.4">parfois</w> <w n="23.5">se</w> <w n="23.6">prend</w> <w n="23.7">à</w> <w n="23.8">fondre</w> <w n="23.9">en</w> <w n="23.10">larmes</w></l>
						<l n="24" num="4.6"><space unit="char" quantity="8"></space><w n="24.1">Devant</w> <w n="24.2">tant</w> <w n="24.3">d</w>’<w n="24.4">horreur</w> <w n="24.5">et</w> <w n="24.6">de</w> <w n="24.7">sang</w> !</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">De</w> <w n="25.2">la</w> <w n="25.3">France</w> <w n="25.4">vaincue</w> <w n="25.5">elle</w> <w n="25.6">est</w> <w n="25.7">le</w> <w n="25.8">bon</w> <w n="25.9">génie</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Et</w> <w n="26.2">quand</w>, <w n="26.3">vainqueur</w> <w n="26.4">lassé</w> <w n="26.5">dont</w> <w n="26.6">la</w> <w n="26.7">tâche</w> <w n="26.8">est</w> <w n="26.9">finie</w>,</l>
						<l n="27" num="5.3"><w n="27.1">Elle</w> <w n="27.2">voudrait</w> <w n="27.3">revoir</w> <w n="27.4">sa</w> <w n="27.5">chaumière</w> <w n="27.6">et</w> <w n="27.7">ses</w> <w n="27.8">bois</w>,</l>
						<l n="28" num="5.4"><w n="28.1">On</w> <w n="28.2">la</w> <w n="28.3">jette</w> <w n="28.4">aux</w> <w n="28.5">bourreaux</w> <w n="28.6">en</w> <w n="28.7">repoussant</w> <w n="28.8">sa</w> <w n="28.9">mère</w> !…</l>
						<l n="29" num="5.5"><w n="29.1">Oh</w> ! <w n="29.2">tandis</w> <w n="29.3">qu</w>’<w n="29.4">on</w> <w n="29.5">la</w> <w n="29.6">brûle</w> <w n="29.7">aux</w> <w n="29.8">longs</w> <w n="29.9">cris</w> <w n="29.10">de</w> : <w n="29.11">sorcière</w> !</l>
						<l n="30" num="5.6"><w n="30.1">Grand</w> <w n="30.2">Dieu</w>, <w n="30.3">que</w> <w n="30.4">faites</w>-<w n="30.5">vous</w> ? <w n="30.6">et</w> <w n="30.7">toi</w>, <w n="30.8">peuple</w> ? <w n="30.9">et</w> <w n="30.10">vous</w>, <w n="30.11">rois</w> ?</l>
					</lg>
				</div></body></text></TEI>