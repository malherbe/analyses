<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">TABLEAUX PARISIENS</head><div type="poem" key="BAU94">
					<head type="number">XC</head>
					<head type="main">Le Soleil</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Le</w> <w n="1.2">long</w> <w n="1.3">du</w> <w n="1.4">vieux</w> <w n="1.5">faubourg</w>, <w n="1.6">où</w> <w n="1.7">pendent</w> <w n="1.8">aux</w> <w n="1.9">masures</w></l>
						<l n="2" num="1.2"><w n="2.1">Les</w> <w n="2.2">persiennes</w>, <w n="2.3">abri</w> <w n="2.4">des</w> <w n="2.5">secrètes</w> <w n="2.6">luxures</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Quand</w> <w n="3.2">le</w> <w n="3.3">soleil</w> <w n="3.4">cruel</w> <w n="3.5">frappe</w> <w n="3.6">à</w> <w n="3.7">traits</w> <w n="3.8">redoublés</w></l>
						<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">la</w> <w n="4.3">ville</w> <w n="4.4">et</w> <w n="4.5">les</w> <w n="4.6">champs</w>, <w n="4.7">sur</w> <w n="4.8">les</w> <w n="4.9">toits</w> <w n="4.10">et</w> <w n="4.11">les</w> <w n="4.12">blés</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">vais</w> <w n="5.3">m</w>’<w n="5.4">exercer</w> <w n="5.5">seul</w> <w n="5.6">à</w> <w n="5.7">ma</w> <w n="5.8">fantasque</w> <w n="5.9">escrime</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Flairant</w> <w n="6.2">dans</w> <w n="6.3">tous</w> <w n="6.4">les</w> <w n="6.5">coins</w> <w n="6.6">les</w> <w n="6.7">hasards</w> <w n="6.8">de</w> <w n="6.9">la</w> <w n="6.10">rime</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Trébuchant</w> <w n="7.2">sur</w> <w n="7.3">les</w> <w n="7.4">mots</w> <w n="7.5">comme</w> <w n="7.6">sur</w> <w n="7.7">les</w> <w n="7.8">pavés</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Heurtant</w> <w n="8.2">parfois</w> <w n="8.3">des</w> <w n="8.4">vers</w> <w n="8.5">depuis</w> <w n="8.6">longtemps</w> <w n="8.7">rêvés</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Ce</w> <w n="9.2">père</w> <w n="9.3">nourricier</w>, <w n="9.4">ennemi</w> <w n="9.5">des</w> <w n="9.6">chloroses</w>,</l>
						<l n="10" num="2.2"><w n="10.1">Éveille</w> <w n="10.2">dans</w> <w n="10.3">les</w> <w n="10.4">champs</w> <w n="10.5">les</w> <w n="10.6">vers</w> <w n="10.7">comme</w> <w n="10.8">les</w> <w n="10.9">roses</w> ;</l>
						<l n="11" num="2.3"><w n="11.1">Il</w> <w n="11.2">fait</w> <w n="11.3">s</w>’<w n="11.4">évaporer</w> <w n="11.5">les</w> <w n="11.6">soucis</w> <w n="11.7">vers</w> <w n="11.8">le</w> <w n="11.9">ciel</w>,</l>
						<l n="12" num="2.4"><w n="12.1">Et</w> <w n="12.2">remplit</w> <w n="12.3">les</w> <w n="12.4">cerveaux</w> <w n="12.5">et</w> <w n="12.6">les</w> <w n="12.7">ruches</w> <w n="12.8">de</w> <w n="12.9">miel</w>.</l>
						<l n="13" num="2.5"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">lui</w> <w n="13.4">qui</w> <w n="13.5">rajeunit</w> <w n="13.6">les</w> <w n="13.7">porteurs</w> <w n="13.8">de</w> <w n="13.9">béquilles</w></l>
						<l n="14" num="2.6"><w n="14.1">Et</w> <w n="14.2">les</w> <w n="14.3">rend</w> <w n="14.4">gais</w> <w n="14.5">et</w> <w n="14.6">doux</w> <w n="14.7">comme</w> <w n="14.8">des</w> <w n="14.9">jeunes</w> <w n="14.10">filles</w>,</l>
						<l n="15" num="2.7"><w n="15.1">Et</w> <w n="15.2">commande</w> <w n="15.3">aux</w> <w n="15.4">moissons</w> <w n="15.5">de</w> <w n="15.6">croître</w> <w n="15.7">et</w> <w n="15.8">de</w> <w n="15.9">mûrir</w></l>
						<l n="16" num="2.8"><w n="16.1">Dans</w> <w n="16.2">le</w> <w n="16.3">cœur</w> <w n="16.4">immortel</w> <w n="16.5">qui</w> <w n="16.6">toujours</w> <w n="16.7">veut</w> <w n="16.8">fleurir</w> !</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Quand</w>, <w n="17.2">ainsi</w> <w n="17.3">qu</w>’<w n="17.4">un</w> <w n="17.5">poëte</w>, <w n="17.6">il</w> <w n="17.7">descend</w> <w n="17.8">dans</w> <w n="17.9">les</w> <w n="17.10">villes</w>,</l>
						<l n="18" num="3.2"><w n="18.1">Il</w> <w n="18.2">ennoblit</w> <w n="18.3">le</w> <w n="18.4">sort</w> <w n="18.5">des</w> <w n="18.6">choses</w> <w n="18.7">les</w> <w n="18.8">plus</w> <w n="18.9">viles</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Et</w> <w n="19.2">s</w>’<w n="19.3">introduit</w> <w n="19.4">en</w> <w n="19.5">roi</w>, <w n="19.6">sans</w> <w n="19.7">bruit</w> <w n="19.8">et</w> <w n="19.9">sans</w> <w n="19.10">valets</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Dans</w> <w n="20.2">tous</w> <w n="20.3">les</w> <w n="20.4">hôpitaux</w> <w n="20.5">et</w> <w n="20.6">dans</w> <w n="20.7">tous</w> <w n="20.8">les</w> <w n="20.9">palais</w>.</l>
					</lg>
				</div></body></text></TEI>