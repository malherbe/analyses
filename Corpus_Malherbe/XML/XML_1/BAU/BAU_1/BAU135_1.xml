<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MORT</head><div type="poem" key="BAU135">
					<head type="number">CXXXI</head>
					<head type="main">Le Rêve d’un curieux</head>
					<opener><salute>À F. N.</salute></opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Connais</w>-<w n="1.2">tu</w>, <w n="1.3">comme</w> <w n="1.4">moi</w>, <w n="1.5">la</w> <w n="1.6">douleur</w> <w n="1.7">savoureuse</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">de</w> <w n="2.3">toi</w> <w n="2.4">fais</w>-<w n="2.5">tu</w> <w n="2.6">dire</w> : « <w n="2.7">Oh</w> ! <w n="2.8">l</w>’<w n="2.9">homme</w> <w n="2.10">singulier</w> ! »</l>
						<l n="3" num="1.3">— <w n="3.1">J</w>’<w n="3.2">allais</w> <w n="3.3">mourir</w>. <w n="3.4">C</w>’<w n="3.5">était</w> <w n="3.6">dans</w> <w n="3.7">mon</w> <w n="3.8">âme</w> <w n="3.9">amoureuse</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Désir</w> <w n="4.2">mêlé</w> <w n="4.3">d</w>’<w n="4.4">horreur</w>, <w n="4.5">un</w> <w n="4.6">mal</w> <w n="4.7">particulier</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Angoisse</w> <w n="5.2">et</w> <w n="5.3">vif</w> <w n="5.4">espoir</w>, <w n="5.5">sans</w> <w n="5.6">humeur</w> <w n="5.7">factieuse</w>.</l>
						<l n="6" num="2.2"><w n="6.1">Plus</w> <w n="6.2">allait</w> <w n="6.3">se</w> <w n="6.4">vidant</w> <w n="6.5">le</w> <w n="6.6">fatal</w> <w n="6.7">sablier</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Plus</w> <w n="7.2">ma</w> <w n="7.3">torture</w> <w n="7.4">était</w> <w n="7.5">âpre</w> <w n="7.6">et</w> <w n="7.7">délicieuse</w> ;</l>
						<l n="8" num="2.4"><w n="8.1">Tout</w> <w n="8.2">mon</w> <w n="8.3">cœur</w> <w n="8.4">s</w>’<w n="8.5">arrachait</w> <w n="8.6">au</w> <w n="8.7">monde</w> <w n="8.8">familier</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">J</w>’<w n="9.2">étais</w> <w n="9.3">comme</w> <w n="9.4">l</w>’<w n="9.5">enfant</w> <w n="9.6">avide</w> <w n="9.7">du</w> <w n="9.8">spectacle</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Haïssant</w> <w n="10.2">le</w> <w n="10.3">rideau</w> <w n="10.4">comme</w> <w n="10.5">on</w> <w n="10.6">hait</w> <w n="10.7">un</w> <w n="10.8">obstacle</w>…</l>
						<l n="11" num="3.3"><w n="11.1">Enfin</w> <w n="11.2">la</w> <w n="11.3">vérité</w> <w n="11.4">froide</w> <w n="11.5">se</w> <w n="11.6">révéla</w> :</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">J</w>’<w n="12.2">étais</w> <w n="12.3">mort</w> <w n="12.4">sans</w> <w n="12.5">surprise</w>, <w n="12.6">et</w> <w n="12.7">la</w> <w n="12.8">terrible</w> <w n="12.9">aurore</w></l>
						<l n="13" num="4.2"><w n="13.1">M</w>’<w n="13.2">enveloppait</w>. — <w n="13.3">Eh</w> <w n="13.4">quoi</w> ! <w n="13.5">n</w>’<w n="13.6">est</w>-<w n="13.7">ce</w> <w n="13.8">donc</w> <w n="13.9">que</w> <w n="13.10">cela</w> ?</l>
						<l n="14" num="4.3"><w n="14.1">La</w> <w n="14.2">toile</w> <w n="14.3">était</w> <w n="14.4">levée</w> <w n="14.5">et</w> <w n="14.6">j</w>’<w n="14.7">attendais</w> <w n="14.8">encore</w>.</l>
					</lg>
				</div></body></text></TEI>