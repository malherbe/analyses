<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SUPPLÉMENT AUX FLEURS DU MAL</head><div type="poem" key="BAU143">
					<head type="number">VII</head>
					<head type="main">La Voix</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mon</w> <w n="1.2">berceau</w> <w n="1.3">s</w>’<w n="1.4">adossait</w> <w n="1.5">à</w> <w n="1.6">la</w> <w n="1.7">bibliothèque</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Babel</w> <w n="2.2">sombre</w>, <w n="2.3">où</w> <w n="2.4">roman</w>, <w n="2.5">science</w>, <w n="2.6">fabliau</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Tout</w>, <w n="3.2">la</w> <w n="3.3">cendre</w> <w n="3.4">latine</w> <w n="3.5">et</w> <w n="3.6">la</w> <w n="3.7">poussière</w> <w n="3.8">grecque</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Se</w> <w n="4.2">mêlaient</w>. <w n="4.3">J</w>’<w n="4.4">étais</w> <w n="4.5">haut</w> <w n="4.6">comme</w> <w n="4.7">un</w> <w n="4.8">in</w>-<w n="4.9">folio</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Deux</w> <w n="5.2">voix</w> <w n="5.3">me</w> <w n="5.4">parlaient</w>. <w n="5.5">L</w>’<w n="5.6">une</w>, <w n="5.7">insidieuse</w> <w n="5.8">et</w> <w n="5.9">ferme</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Disait</w> : « <w n="6.2">La</w> <w n="6.3">Terre</w> <w n="6.4">est</w> <w n="6.5">un</w> <w n="6.6">gâteau</w> <w n="6.7">plein</w> <w n="6.8">de</w> <w n="6.9">douceur</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">puis</w> (<w n="7.3">et</w> <w n="7.4">ton</w> <w n="7.5">plaisir</w> <w n="7.6">serait</w> <w n="7.7">alors</w> <w n="7.8">sans</w> <w n="7.9">terme</w> !)</l>
						<l n="8" num="1.8"><w n="8.1">Te</w> <w n="8.2">faire</w> <w n="8.3">un</w> <w n="8.4">appétit</w> <w n="8.5">d</w>’<w n="8.6">une</w> <w n="8.7">égale</w> <w n="8.8">grosseur</w>. »</l>
						<l n="9" num="1.9"><w n="9.1">Et</w> <w n="9.2">l</w>’<w n="9.3">autre</w> : « <w n="9.4">Viens</w> ! <w n="9.5">oh</w> ! <w n="9.6">viens</w> <w n="9.7">voyager</w> <w n="9.8">dans</w> <w n="9.9">les</w> <w n="9.10">rêves</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Au</w> <w n="10.2">delà</w> <w n="10.3">du</w> <w n="10.4">possible</w>, <w n="10.5">au</w> <w n="10.6">delà</w> <w n="10.7">du</w> <w n="10.8">connu</w> ! »</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">celle</w>-<w n="11.3">là</w> <w n="11.4">chantait</w> <w n="11.5">comme</w> <w n="11.6">le</w> <w n="11.7">vent</w> <w n="11.8">des</w> <w n="11.9">grèves</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Fantôme</w> <w n="12.2">vagissant</w>, <w n="12.3">on</w> <w n="12.4">ne</w> <w n="12.5">sait</w> <w n="12.6">d</w>’<w n="12.7">où</w> <w n="12.8">venu</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Qui</w> <w n="13.2">caresse</w> <w n="13.3">l</w>’<w n="13.4">oreille</w> <w n="13.5">et</w> <w n="13.6">cependant</w> <w n="13.7">l</w>’<w n="13.8">effraie</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Je</w> <w n="14.2">te</w> <w n="14.3">répondis</w> : « <w n="14.4">Oui</w> ! <w n="14.5">douce</w> <w n="14.6">voix</w> ! » <w n="14.7">C</w>’<w n="14.8">est</w> <w n="14.9">d</w>’<w n="14.10">alors</w></l>
						<l n="15" num="1.15"><w n="15.1">Que</w> <w n="15.2">date</w> <w n="15.3">ce</w> <w n="15.4">qu</w>’<w n="15.5">on</w> <w n="15.6">peut</w>, <w n="15.7">hélas</w> ! <w n="15.8">nommer</w> <w n="15.9">ma</w> <w n="15.10">plaie</w></l>
						<l n="16" num="1.16"><w n="16.1">Et</w> <w n="16.2">ma</w> <w n="16.3">fatalité</w>. <w n="16.4">Derrière</w> <w n="16.5">les</w> <w n="16.6">décors</w></l>
						<l n="17" num="1.17"><w n="17.1">De</w> <w n="17.2">l</w>’<w n="17.3">existence</w> <w n="17.4">immense</w>, <w n="17.5">au</w> <w n="17.6">plus</w> <w n="17.7">noir</w> <w n="17.8">de</w> <w n="17.9">l</w>’<w n="17.10">abîme</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Je</w> <w n="18.2">vois</w> <w n="18.3">distinctement</w> <w n="18.4">des</w> <w n="18.5">mondes</w> <w n="18.6">singuliers</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Et</w>, <w n="19.2">de</w> <w n="19.3">ma</w> <w n="19.4">clairvoyance</w> <w n="19.5">extatique</w> <w n="19.6">victime</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Je</w> <w n="20.2">traîne</w> <w n="20.3">des</w> <w n="20.4">serpents</w> <w n="20.5">qui</w> <w n="20.6">mordent</w> <w n="20.7">mes</w> <w n="20.8">souliers</w>.</l>
						<l n="21" num="1.21"><w n="21.1">Et</w> <w n="21.2">c</w>’<w n="21.3">est</w> <w n="21.4">depuis</w> <w n="21.5">ce</w> <w n="21.6">temps</w> <w n="21.7">que</w>, <w n="21.8">pareil</w> <w n="21.9">aux</w> <w n="21.10">prophètes</w>,</l>
						<l n="22" num="1.22"><w n="22.1">J</w>’<w n="22.2">aime</w> <w n="22.3">si</w> <w n="22.4">tendrement</w> <w n="22.5">le</w> <w n="22.6">désert</w> <w n="22.7">et</w> <w n="22.8">la</w> <w n="22.9">mer</w> ;</l>
						<l n="23" num="1.23"><w n="23.1">Que</w> <w n="23.2">je</w> <w n="23.3">ris</w> <w n="23.4">dans</w> <w n="23.5">les</w> <w n="23.6">deuils</w> <w n="23.7">et</w> <w n="23.8">pleure</w> <w n="23.9">dans</w> <w n="23.10">les</w> <w n="23.11">fêtes</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Et</w> <w n="24.2">trouve</w> <w n="24.3">un</w> <w n="24.4">goût</w> <w n="24.5">suave</w> <w n="24.6">au</w> <w n="24.7">vin</w> <w n="24.8">le</w> <w n="24.9">plus</w> <w n="24.10">amer</w> ;</l>
						<l n="25" num="1.25"><w n="25.1">Que</w> <w n="25.2">je</w> <w n="25.3">prends</w> <w n="25.4">très</w> <w n="25.5">souvent</w> <w n="25.6">les</w> <w n="25.7">faits</w> <w n="25.8">pour</w> <w n="25.9">des</w> <w n="25.10">mensonges</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Et</w> <w n="26.2">que</w>, <w n="26.3">les</w> <w n="26.4">yeux</w> <w n="26.5">au</w> <w n="26.6">ciel</w>, <w n="26.7">je</w> <w n="26.8">tombe</w> <w n="26.9">dans</w> <w n="26.10">des</w> <w n="26.11">trous</w>.</l>
						<l n="27" num="1.27"><w n="27.1">Mais</w> <w n="27.2">la</w> <w n="27.3">Voix</w> <w n="27.4">me</w> <w n="27.5">console</w> <w n="27.6">et</w> <w n="27.7">dit</w> : « <w n="27.8">Garde</w> <w n="27.9">tes</w> <w n="27.10">songes</w> ;</l>
						<l n="28" num="1.28"><w n="28.1">Les</w> <w n="28.2">sages</w> <w n="28.3">n</w>’<w n="28.4">en</w> <w n="28.5">ont</w> <w n="28.6">pas</w> <w n="28.7">d</w>’<w n="28.8">aussi</w> <w n="28.9">beaux</w> <w n="28.10">que</w> <w n="28.11">les</w> <w n="28.12">fous</w> ! »</l>
					</lg>
				</div></body></text></TEI>