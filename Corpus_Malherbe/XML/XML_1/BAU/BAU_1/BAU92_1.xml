<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU92">
					<head type="number">LXXXVIII</head>
					<head type="main">L’Horloge</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Horloge</w> ! <w n="1.2">dieu</w> <w n="1.3">sinistre</w>, <w n="1.4">effrayant</w>, <w n="1.5">impassible</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Dont</w> <w n="2.2">le</w> <w n="2.3">doigt</w> <w n="2.4">nous</w> <w n="2.5">menace</w> <w n="2.6">et</w> <w n="2.7">nous</w> <w n="2.8">dit</w> : « <hi rend="ital"><w n="2.9">Souviens</w>-<w n="2.10">toi</w> !</hi></l>
						<l n="3" num="1.3"><w n="3.1">Les</w> <w n="3.2">vibrantes</w> <w n="3.3">Douleurs</w> <w n="3.4">dans</w> <w n="3.5">ton</w> <w n="3.6">cœur</w> <w n="3.7">plein</w> <w n="3.8">d</w>’<w n="3.9">effroi</w></l>
						<l n="4" num="1.4"><w n="4.1">Se</w> <w n="4.2">planteront</w> <w n="4.3">bientôt</w> <w n="4.4">comme</w> <w n="4.5">dans</w> <w n="4.6">une</w> <w n="4.7">cible</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Le</w> <w n="5.2">Plaisir</w> <w n="5.3">vaporeux</w> <w n="5.4">fuira</w> <w n="5.5">vers</w> <w n="5.6">l</w>’<w n="5.7">horizon</w></l>
						<l n="6" num="2.2"><w n="6.1">Ainsi</w> <w n="6.2">qu</w>’<w n="6.3">une</w> <w n="6.4">sylphide</w> <w n="6.5">au</w> <w n="6.6">fond</w> <w n="6.7">de</w> <w n="6.8">la</w> <w n="6.9">coulisse</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Chaque</w> <w n="7.2">instant</w> <w n="7.3">te</w> <w n="7.4">dévore</w> <w n="7.5">un</w> <w n="7.6">morceau</w> <w n="7.7">du</w> <w n="7.8">délice</w></l>
						<l n="8" num="2.4"><w n="8.1">À</w> <w n="8.2">chaque</w> <w n="8.3">homme</w> <w n="8.4">accordé</w> <w n="8.5">pour</w> <w n="8.6">toute</w> <w n="8.7">sa</w> <w n="8.8">saison</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Trois</w> <w n="9.2">mille</w> <w n="9.3">six</w> <w n="9.4">cents</w> <w n="9.5">fois</w> <w n="9.6">par</w> <w n="9.7">heure</w>, <w n="9.8">la</w> <w n="9.9">Seconde</w></l>
						<l n="10" num="3.2"><w n="10.1">Chuchote</w> : <hi rend="ital"><w n="10.2">Souviens</w>-<w n="10.3">toi</w> !</hi> — <w n="10.4">Rapide</w>, <w n="10.5">avec</w> <w n="10.6">sa</w> <w n="10.7">voix</w></l>
						<l n="11" num="3.3"><w n="11.1">D</w>’<w n="11.2">insecte</w>, <w n="11.3">Maintenant</w> <w n="11.4">dit</w> : <w n="11.5">Je</w> <w n="11.6">suis</w> <w n="11.7">Autrefois</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">j</w>’<w n="12.3">ai</w> <w n="12.4">pompé</w> <w n="12.5">ta</w> <w n="12.6">vie</w> <w n="12.7">avec</w> <w n="12.8">ma</w> <w n="12.9">trompe</w> <w n="12.10">immonde</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><foreign lang="ANG"><w n="13.1">Remember</w> !</foreign><hi rend="ital"><w n="13.2">Souviens</w>-<w n="13.3">toi</w> !</hi> <w n="13.4">prodigue</w> ! <foreign lang="LAT"><w n="13.5">Esto</w> <w n="13.6">memor</w> !</foreign></l>
						<l n="14" num="4.2">(<w n="14.1">Mon</w> <w n="14.2">gosier</w> <w n="14.3">de</w> <w n="14.4">métal</w> <w n="14.5">parle</w> <w n="14.6">toutes</w> <w n="14.7">les</w> <w n="14.8">langues</w>.)</l>
						<l n="15" num="4.3"><w n="15.1">Les</w> <w n="15.2">minutes</w>, <w n="15.3">mortel</w> <w n="15.4">folâtre</w>, <w n="15.5">sont</w> <w n="15.6">des</w> <w n="15.7">gangues</w></l>
						<l n="16" num="4.4"><w n="16.1">Qu</w>’<w n="16.2">il</w> <w n="16.3">ne</w> <w n="16.4">faut</w> <w n="16.5">pas</w> <w n="16.6">lâcher</w> <w n="16.7">sans</w> <w n="16.8">en</w> <w n="16.9">extraire</w> <w n="16.10">l</w>’<w n="16.11">or</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><hi rend="ital"><w n="17.1">Souviens</w>-<w n="17.2">toi</w></hi> <w n="17.3">que</w> <w n="17.4">le</w> <w n="17.5">Temps</w> <w n="17.6">est</w> <w n="17.7">un</w> <w n="17.8">joueur</w> <w n="17.9">avide</w></l>
						<l n="18" num="5.2"><w n="18.1">Qui</w> <w n="18.2">gagne</w> <w n="18.3">sans</w> <w n="18.4">tricher</w>, <w n="18.5">à</w> <w n="18.6">tout</w> <w n="18.7">coup</w> ! <w n="18.8">c</w>’<w n="18.9">est</w> <w n="18.10">la</w> <w n="18.11">loi</w>.</l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">jour</w> <w n="19.3">décroît</w> ; <w n="19.4">la</w> <w n="19.5">nuit</w> <w n="19.6">augmente</w> ; <hi rend="ital"><w n="19.7">souviens</w>-<w n="19.8">toi</w> !</hi></l>
						<l n="20" num="5.4"><w n="20.1">Le</w> <w n="20.2">gouffre</w> <w n="20.3">a</w> <w n="20.4">toujours</w> <w n="20.5">soif</w> ; <w n="20.6">la</w> <w n="20.7">clepsydre</w> <w n="20.8">se</w> <w n="20.9">vide</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Tantôt</w> <w n="21.2">sonnera</w> <w n="21.3">l</w>’<w n="21.4">heure</w> <w n="21.5">où</w> <w n="21.6">le</w> <w n="21.7">divin</w> <w n="21.8">Hasard</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Où</w> <w n="22.2">l</w>’<w n="22.3">auguste</w> <w n="22.4">Vertu</w>, <w n="22.5">ton</w> <w n="22.6">épouse</w> <w n="22.7">encor</w> <w n="22.8">vierge</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Où</w> <w n="23.2">le</w> <w n="23.3">Repentir</w> <w n="23.4">même</w> (<w n="23.5">oh</w> ! <w n="23.6">la</w> <w n="23.7">dernière</w> <w n="23.8">auberge</w> !),</l>
						<l n="24" num="6.4"><w n="24.1">Où</w> <w n="24.2">tout</w> <w n="24.3">te</w> <w n="24.4">dira</w> : <w n="24.5">Meurs</w>, <w n="24.6">vieux</w> <w n="24.7">lâche</w> ! <w n="24.8">il</w> <w n="24.9">est</w> <w n="24.10">trop</w> <w n="24.11">tard</w> ! »</l>
					</lg>
				</div></body></text></TEI>