<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU15">
					<head type="number">XIV</head>
					<head type="main">L’Homme et la mer</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Homme</w> <w n="1.2">libre</w>, <w n="1.3">toujours</w> <w n="1.4">tu</w> <w n="1.5">chériras</w> <w n="1.6">la</w> <w n="1.7">mer</w></l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">mer</w> <w n="2.3">est</w> <w n="2.4">ton</w> <w n="2.5">miroir</w> ; <w n="2.6">tu</w> <w n="2.7">contemples</w> <w n="2.8">ton</w> <w n="2.9">âme</w></l>
						<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">le</w> <w n="3.3">déroulement</w> <w n="3.4">infini</w> <w n="3.5">de</w> <w n="3.6">sa</w> <w n="3.7">lame</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">ton</w> <w n="4.3">esprit</w> <w n="4.4">n</w>’<w n="4.5">est</w> <w n="4.6">pas</w> <w n="4.7">un</w> <w n="4.8">gouffre</w> <w n="4.9">moins</w> <w n="4.10">amer</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Tu</w> <w n="5.2">te</w> <w n="5.3">plais</w> <w n="5.4">à</w> <w n="5.5">plonger</w> <w n="5.6">au</w> <w n="5.7">sein</w> <w n="5.8">de</w> <w n="5.9">ton</w> <w n="5.10">image</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Tu</w> <w n="6.2">l</w>’<w n="6.3">embrasses</w> <w n="6.4">des</w> <w n="6.5">yeux</w> <w n="6.6">et</w> <w n="6.7">des</w> <w n="6.8">bras</w>, <w n="6.9">et</w> <w n="6.10">ton</w> <w n="6.11">cœur</w></l>
						<l n="7" num="2.3"><w n="7.1">Se</w> <w n="7.2">distrait</w> <w n="7.3">quelquefois</w> <w n="7.4">de</w> <w n="7.5">sa</w> <w n="7.6">propre</w> <w n="7.7">rumeur</w></l>
						<l n="8" num="2.4"><w n="8.1">Au</w> <w n="8.2">bruit</w> <w n="8.3">de</w> <w n="8.4">cette</w> <w n="8.5">plainte</w> <w n="8.6">indomptable</w> <w n="8.7">et</w> <w n="8.8">sauvage</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Vous</w> <w n="9.2">êtes</w> <w n="9.3">tous</w> <w n="9.4">les</w> <w n="9.5">deux</w> <w n="9.6">ténébreux</w> <w n="9.7">et</w> <w n="9.8">discrets</w> :</l>
						<l n="10" num="3.2"><w n="10.1">Homme</w>, <w n="10.2">nul</w> <w n="10.3">n</w>’<w n="10.4">a</w> <w n="10.5">sondé</w> <w n="10.6">le</w> <w n="10.7">fond</w> <w n="10.8">de</w> <w n="10.9">tes</w> <w n="10.10">abîmes</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Ô</w> <w n="11.2">mer</w>, <w n="11.3">nul</w> <w n="11.4">ne</w> <w n="11.5">connaît</w> <w n="11.6">tes</w> <w n="11.7">richesses</w> <w n="11.8">intimes</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Tant</w> <w n="12.2">vous</w> <w n="12.3">êtes</w> <w n="12.4">jaloux</w> <w n="12.5">de</w> <w n="12.6">garder</w> <w n="12.7">vos</w> <w n="12.8">secrets</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">cependant</w> <w n="13.3">voilà</w> <w n="13.4">des</w> <w n="13.5">siècles</w> <w n="13.6">innombrables</w></l>
						<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">vous</w> <w n="14.3">vous</w> <w n="14.4">combattez</w> <w n="14.5">sans</w> <w n="14.6">pitié</w> <w n="14.7">ni</w> <w n="14.8">remord</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Tellement</w> <w n="15.2">vous</w> <w n="15.3">aimez</w> <w n="15.4">le</w> <w n="15.5">carnage</w> <w n="15.6">et</w> <w n="15.7">la</w> <w n="15.8">mort</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Ô</w> <w n="16.2">lutteurs</w> <w n="16.3">éternels</w>, <w n="16.4">ô</w> <w n="16.5">frères</w> <w n="16.6">implacables</w> !</l>
					</lg>
				</div></body></text></TEI>