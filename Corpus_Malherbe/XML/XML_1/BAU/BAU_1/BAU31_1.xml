<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU31">
					<head type="number">XXX</head>
					<head type="main">Une charogne</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Rappelez</w>-<w n="1.2">vous</w> <w n="1.3">l</w>’<w n="1.4">objet</w> <w n="1.5">que</w> <w n="1.6">nous</w> <w n="1.7">vîmes</w>, <w n="1.8">mon</w> <w n="1.9">âme</w>,</l>
						<l n="2" num="1.2"><space quantity="16" unit="char"></space><w n="2.1">Ce</w> <w n="2.2">beau</w> <w n="2.3">matin</w> <w n="2.4">d</w>’<w n="2.5">été</w> <w n="2.6">si</w> <w n="2.7">doux</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Au</w> <w n="3.2">détour</w> <w n="3.3">d</w>’<w n="3.4">un</w> <w n="3.5">sentier</w> <w n="3.6">une</w> <w n="3.7">charogne</w> <w n="3.8">infâme</w></l>
						<l n="4" num="1.4"><space quantity="16" unit="char"></space><w n="4.1">Sur</w> <w n="4.2">un</w> <w n="4.3">lit</w> <w n="4.4">semé</w> <w n="4.5">de</w> <w n="4.6">cailloux</w>,</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Les</w> <w n="5.2">jambes</w> <w n="5.3">en</w> <w n="5.4">l</w>’<w n="5.5">air</w>, <w n="5.6">comme</w> <w n="5.7">une</w> <w n="5.8">femme</w> <w n="5.9">lubrique</w>,</l>
						<l n="6" num="2.2"><space quantity="16" unit="char"></space><w n="6.1">Brûlante</w> <w n="6.2">et</w> <w n="6.3">suant</w> <w n="6.4">les</w> <w n="6.5">poisons</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Ouvrait</w> <w n="7.2">d</w>’<w n="7.3">une</w> <w n="7.4">façon</w> <w n="7.5">nonchalante</w> <w n="7.6">et</w> <w n="7.7">cynique</w></l>
						<l n="8" num="2.4"><space quantity="16" unit="char"></space><w n="8.1">Son</w> <w n="8.2">ventre</w> <w n="8.3">plein</w> <w n="8.4">d</w>’<w n="8.5">exhalaisons</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">soleil</w> <w n="9.3">rayonnait</w> <w n="9.4">sur</w> <w n="9.5">cette</w> <w n="9.6">pourriture</w>,</l>
						<l n="10" num="3.2"><space quantity="16" unit="char"></space><w n="10.1">Comme</w> <w n="10.2">afin</w> <w n="10.3">de</w> <w n="10.4">la</w> <w n="10.5">cuire</w> <w n="10.6">à</w> <w n="10.7">point</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">de</w> <w n="11.3">rendre</w> <w n="11.4">au</w> <w n="11.5">centuple</w> <w n="11.6">à</w> <w n="11.7">la</w> <w n="11.8">grande</w> <w n="11.9">Nature</w></l>
						<l n="12" num="3.4"><space quantity="16" unit="char"></space><w n="12.1">Tout</w> <w n="12.2">ce</w> <w n="12.3">qu</w>’<w n="12.4">ensemble</w> <w n="12.5">elle</w> <w n="12.6">avait</w> <w n="12.7">joint</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">le</w> <w n="13.3">ciel</w> <w n="13.4">regardait</w> <w n="13.5">la</w> <w n="13.6">carcasse</w> <w n="13.7">superbe</w></l>
						<l n="14" num="4.2"><space quantity="16" unit="char"></space><w n="14.1">Comme</w> <w n="14.2">une</w> <w n="14.3">fleur</w> <w n="14.4">s</w>’<w n="14.5">épanouir</w>.</l>
						<l n="15" num="4.3"><w n="15.1">La</w> <w n="15.2">puanteur</w> <w n="15.3">était</w> <w n="15.4">si</w> <w n="15.5">forte</w>, <w n="15.6">que</w> <w n="15.7">sur</w> <w n="15.8">l</w>’<w n="15.9">herbe</w></l>
						<l n="16" num="4.4"><space quantity="16" unit="char"></space><w n="16.1">Vous</w> <w n="16.2">crûtes</w> <w n="16.3">vous</w> <w n="16.4">évanouir</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Les</w> <w n="17.2">mouches</w> <w n="17.3">bourdonnaient</w> <w n="17.4">sur</w> <w n="17.5">ce</w> <w n="17.6">ventre</w> <w n="17.7">putride</w>,</l>
						<l n="18" num="5.2"><space quantity="16" unit="char"></space><w n="18.1">D</w>’<w n="18.2">où</w> <w n="18.3">sortaient</w> <w n="18.4">de</w> <w n="18.5">noirs</w> <w n="18.6">bataillons</w></l>
						<l n="19" num="5.3"><w n="19.1">De</w> <w n="19.2">larves</w>, <w n="19.3">qui</w> <w n="19.4">coulaient</w> <w n="19.5">comme</w> <w n="19.6">un</w> <w n="19.7">épais</w> <w n="19.8">liquide</w></l>
						<l n="20" num="5.4"><space quantity="16" unit="char"></space><w n="20.1">Le</w> <w n="20.2">long</w> <w n="20.3">de</w> <w n="20.4">ces</w> <w n="20.5">vivants</w> <w n="20.6">haillons</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Tout</w> <w n="21.2">cela</w> <w n="21.3">descendait</w>, <w n="21.4">montait</w> <w n="21.5">comme</w> <w n="21.6">une</w> <w n="21.7">vague</w>,</l>
						<l n="22" num="6.2"><space quantity="16" unit="char"></space><w n="22.1">Ou</w> <w n="22.2">s</w>’<w n="22.3">élançait</w> <w n="22.4">en</w> <w n="22.5">pétillant</w> ;</l>
						<l n="23" num="6.3"><w n="23.1">On</w> <w n="23.2">eût</w> <w n="23.3">dit</w> <w n="23.4">que</w> <w n="23.5">le</w> <w n="23.6">corps</w>, <w n="23.7">enflé</w> <w n="23.8">d</w>’<w n="23.9">un</w> <w n="23.10">souffle</w> <w n="23.11">vague</w>,</l>
						<l n="24" num="6.4"><space quantity="16" unit="char"></space><w n="24.1">Vivait</w> <w n="24.2">en</w> <w n="24.3">se</w> <w n="24.4">multipliant</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">ce</w> <w n="25.3">monde</w> <w n="25.4">rendait</w> <w n="25.5">une</w> <w n="25.6">étrange</w> <w n="25.7">musique</w>,</l>
						<l n="26" num="7.2"><space quantity="16" unit="char"></space><w n="26.1">Comme</w> <w n="26.2">l</w>’<w n="26.3">eau</w> <w n="26.4">courante</w> <w n="26.5">et</w> <w n="26.6">le</w> <w n="26.7">vent</w>,</l>
						<l n="27" num="7.3"><w n="27.1">Ou</w> <w n="27.2">le</w> <w n="27.3">grain</w> <w n="27.4">qu</w>’<w n="27.5">un</w> <w n="27.6">vanneur</w> <w n="27.7">d</w>’<w n="27.8">un</w> <w n="27.9">mouvement</w> <w n="27.10">rhythmique</w></l>
						<l n="28" num="7.4"><space quantity="16" unit="char"></space><w n="28.1">Agite</w> <w n="28.2">et</w> <w n="28.3">tourne</w> <w n="28.4">dans</w> <w n="28.5">son</w> <w n="28.6">van</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Les</w> <w n="29.2">formes</w> <w n="29.3">s</w>’<w n="29.4">effaçaient</w> <w n="29.5">et</w> <w n="29.6">n</w>’<w n="29.7">étaient</w> <w n="29.8">plus</w> <w n="29.9">qu</w>’<w n="29.10">un</w> <w n="29.11">rêve</w>,</l>
						<l n="30" num="8.2"><space quantity="16" unit="char"></space><w n="30.1">Une</w> <w n="30.2">ébauche</w> <w n="30.3">lente</w> <w n="30.4">à</w> <w n="30.5">venir</w></l>
						<l n="31" num="8.3"><w n="31.1">Sur</w> <w n="31.2">la</w> <w n="31.3">toile</w> <w n="31.4">oubliée</w>, <w n="31.5">et</w> <w n="31.6">que</w> <w n="31.7">l</w>’<w n="31.8">artiste</w> <w n="31.9">achève</w></l>
						<l n="32" num="8.4"><space quantity="16" unit="char"></space><w n="32.1">Seulement</w> <w n="32.2">par</w> <w n="32.3">le</w> <w n="32.4">souvenir</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Derrière</w> <w n="33.2">les</w> <w n="33.3">rochers</w> <w n="33.4">une</w> <w n="33.5">chienne</w> <w n="33.6">inquiète</w></l>
						<l n="34" num="9.2"><space quantity="16" unit="char"></space><w n="34.1">Nous</w> <w n="34.2">regardait</w> <w n="34.3">d</w>’<w n="34.4">un</w> <w n="34.5">œil</w> <w n="34.6">fâché</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Épiant</w> <w n="35.2">le</w> <w n="35.3">moment</w> <w n="35.4">de</w> <w n="35.5">reprendre</w> <w n="35.6">au</w> <w n="35.7">squelette</w></l>
						<l n="36" num="9.4"><space quantity="16" unit="char"></space><w n="36.1">Le</w> <w n="36.2">morceau</w> <w n="36.3">qu</w>’<w n="36.4">elle</w> <w n="36.5">avait</w> <w n="36.6">lâché</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">— <w n="37.1">Et</w> <w n="37.2">pourtant</w> <w n="37.3">vous</w> <w n="37.4">serez</w> <w n="37.5">semblable</w> <w n="37.6">à</w> <w n="37.7">cette</w> <w n="37.8">ordure</w>,</l>
						<l n="38" num="10.2"><space quantity="16" unit="char"></space><w n="38.1">À</w> <w n="38.2">cette</w> <w n="38.3">horrible</w> <w n="38.4">infection</w>,</l>
						<l n="39" num="10.3"><w n="39.1">Étoile</w> <w n="39.2">de</w> <w n="39.3">mes</w> <w n="39.4">yeux</w>, <w n="39.5">soleil</w> <w n="39.6">de</w> <w n="39.7">ma</w> <w n="39.8">nature</w>,</l>
						<l n="40" num="10.4"><space quantity="16" unit="char"></space><w n="40.1">Vous</w>, <w n="40.2">mon</w> <w n="40.3">ange</w> <w n="40.4">et</w> <w n="40.5">ma</w> <w n="40.6">passion</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Oui</w> ! <w n="41.2">telle</w> <w n="41.3">vous</w> <w n="41.4">serez</w>, <w n="41.5">ô</w> <w n="41.6">la</w> <w n="41.7">reine</w> <w n="41.8">des</w> <w n="41.9">grâces</w>,</l>
						<l n="42" num="11.2"><space quantity="16" unit="char"></space><w n="42.1">Après</w> <w n="42.2">les</w> <w n="42.3">derniers</w> <w n="42.4">sacrements</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Quand</w> <w n="43.2">vous</w> <w n="43.3">irez</w>, <w n="43.4">sous</w> <w n="43.5">l</w>’<w n="43.6">herbe</w> <w n="43.7">et</w> <w n="43.8">les</w> <w n="43.9">floraisons</w> <w n="43.10">grasses</w>,</l>
						<l n="44" num="11.4"><space quantity="16" unit="char"></space><w n="44.1">Moisir</w> <w n="44.2">parmi</w> <w n="44.3">les</w> <w n="44.4">ossements</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Alors</w>, <w n="45.2">ô</w> <w n="45.3">ma</w> <w n="45.4">beauté</w> ! <w n="45.5">dites</w> <w n="45.6">à</w> <w n="45.7">la</w> <w n="45.8">vermine</w></l>
						<l n="46" num="12.2"><space quantity="16" unit="char"></space><w n="46.1">Qui</w> <w n="46.2">vous</w> <w n="46.3">mangera</w> <w n="46.4">de</w> <w n="46.5">baisers</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Que</w> <w n="47.2">j</w>’<w n="47.3">ai</w> <w n="47.4">gardé</w> <w n="47.5">la</w> <w n="47.6">forme</w> <w n="47.7">et</w> <w n="47.8">l</w>’<w n="47.9">essence</w> <w n="47.10">divine</w></l>
						<l n="48" num="12.4"><space quantity="16" unit="char"></space><w n="48.1">De</w> <w n="48.2">mes</w> <w n="48.3">amours</w> <w n="48.4">décomposés</w> !</l>
					</lg>
				</div></body></text></TEI>