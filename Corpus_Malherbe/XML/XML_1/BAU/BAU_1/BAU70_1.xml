<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU70">
					<head type="number">LXVI</head>
					<head type="main">Le Revenant</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">les</w> <w n="1.3">anges</w> <w n="1.4">à</w> <w n="1.5">l</w>’<w n="1.6">œil</w> <w n="1.7">fauve</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">reviendrai</w> <w n="2.3">dans</w> <w n="2.4">ton</w> <w n="2.5">alcôve</w></l>
						<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">vers</w> <w n="3.3">toi</w> <w n="3.4">glisserai</w> <w n="3.5">sans</w> <w n="3.6">bruit</w></l>
						<l n="4" num="1.4"><w n="4.1">Avec</w> <w n="4.2">les</w> <w n="4.3">ombres</w> <w n="4.4">de</w> <w n="4.5">la</w> <w n="4.6">nuit</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Et</w> <w n="5.2">je</w> <w n="5.3">te</w> <w n="5.4">donnerai</w>, <w n="5.5">ma</w> <w n="5.6">brune</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Des</w> <w n="6.2">baisers</w> <w n="6.3">froids</w> <w n="6.4">comme</w> <w n="6.5">la</w> <w n="6.6">lune</w></l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">des</w> <w n="7.3">caresses</w> <w n="7.4">de</w> <w n="7.5">serpent</w></l>
						<l n="8" num="2.4"><w n="8.1">Autour</w> <w n="8.2">d</w>’<w n="8.3">une</w> <w n="8.4">fosse</w> <w n="8.5">rampant</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Quand</w> <w n="9.2">viendra</w> <w n="9.3">le</w> <w n="9.4">matin</w> <w n="9.5">livide</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Tu</w> <w n="10.2">trouveras</w> <w n="10.3">ma</w> <w n="10.4">place</w> <w n="10.5">vide</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">jusqu</w>’<w n="11.3">au</w> <w n="11.4">soir</w> <w n="11.5">il</w> <w n="11.6">fera</w> <w n="11.7">froid</w>.</l>
					</lg>
					<lg n="4">
						<l n="12" num="4.1"><w n="12.1">Comme</w> <w n="12.2">d</w>’<w n="12.3">autres</w> <w n="12.4">par</w> <w n="12.5">la</w> <w n="12.6">tendresse</w>,</l>
						<l n="13" num="4.2"><w n="13.1">Sur</w> <w n="13.2">ta</w> <w n="13.3">vie</w> <w n="13.4">et</w> <w n="13.5">sur</w> <w n="13.6">ta</w> <w n="13.7">jeunesse</w>,</l>
						<l n="14" num="4.3"><w n="14.1">Moi</w>, <w n="14.2">je</w> <w n="14.3">veux</w> <w n="14.4">régner</w> <w n="14.5">par</w> <w n="14.6">l</w>’<w n="14.7">effroi</w> !</l>
					</lg>
				</div></body></text></TEI>