<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU30">
					<head type="number">XXIX</head>
					<head type="main">Le Serpent qui danse</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">j</w>’<w n="1.3">aime</w> <w n="1.4">voir</w>, <w n="1.5">chère</w> <w n="1.6">indolente</w>,</l>
						<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">De</w> <w n="2.2">ton</w> <w n="2.3">corps</w> <w n="2.4">si</w> <w n="2.5">beau</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">une</w> <w n="3.3">étoile</w> <w n="3.4">vacillante</w>,</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Miroiter</w> <w n="4.2">la</w> <w n="4.3">peau</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Sur</w> <w n="5.2">ta</w> <w n="5.3">chevelure</w> <w n="5.4">profonde</w></l>
						<l n="6" num="2.2"><space quantity="8" unit="char"></space><w n="6.1">Aux</w> <w n="6.2">âcres</w> <w n="6.3">parfums</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Mer</w> <w n="7.2">odorante</w> <w n="7.3">et</w> <w n="7.4">vagabonde</w></l>
						<l n="8" num="2.4"><space quantity="8" unit="char"></space><w n="8.1">Aux</w> <w n="8.2">flots</w> <w n="8.3">bleus</w> <w n="8.4">et</w> <w n="8.5">bruns</w>,</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Comme</w> <w n="9.2">un</w> <w n="9.3">navire</w> <w n="9.4">qui</w> <w n="9.5">s</w>’<w n="9.6">éveille</w></l>
						<l n="10" num="3.2"><space quantity="8" unit="char"></space><w n="10.1">Au</w> <w n="10.2">vent</w> <w n="10.3">du</w> <w n="10.4">matin</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Mon</w> <w n="11.2">âme</w> <w n="11.3">rêveuse</w> <w n="11.4">appareille</w></l>
						<l n="12" num="3.4"><space quantity="8" unit="char"></space><w n="12.1">Pour</w> <w n="12.2">un</w> <w n="12.3">ciel</w> <w n="12.4">lointain</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Tes</w> <w n="13.2">yeux</w>, <w n="13.3">où</w> <w n="13.4">rien</w> <w n="13.5">ne</w> <w n="13.6">se</w> <w n="13.7">révèle</w></l>
						<l n="14" num="4.2"><space quantity="8" unit="char"></space><w n="14.1">De</w> <w n="14.2">doux</w> <w n="14.3">ni</w> <w n="14.4">d</w>’<w n="14.5">amer</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Sont</w> <w n="15.2">deux</w> <w n="15.3">bijoux</w> <w n="15.4">froids</w> <w n="15.5">où</w> <w n="15.6">se</w> <w n="15.7">mêle</w></l>
						<l n="16" num="4.4"><space quantity="8" unit="char"></space><w n="16.1">L</w>’<w n="16.2">or</w> <w n="16.3">avec</w> <w n="16.4">le</w> <w n="16.5">fer</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">À</w> <w n="17.2">te</w> <w n="17.3">voir</w> <w n="17.4">marcher</w> <w n="17.5">en</w> <w n="17.6">cadence</w>,</l>
						<l n="18" num="5.2"><space quantity="8" unit="char"></space><w n="18.1">Belle</w> <w n="18.2">d</w>’<w n="18.3">abandon</w>,</l>
						<l n="19" num="5.3"><w n="19.1">On</w> <w n="19.2">dirait</w> <w n="19.3">un</w> <w n="19.4">serpent</w> <w n="19.5">qui</w> <w n="19.6">danse</w></l>
						<l n="20" num="5.4"><space quantity="8" unit="char"></space><w n="20.1">Au</w> <w n="20.2">bout</w> <w n="20.3">d</w>’<w n="20.4">un</w> <w n="20.5">bâton</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Sous</w> <w n="21.2">le</w> <w n="21.3">fardeau</w> <w n="21.4">de</w> <w n="21.5">ta</w> <w n="21.6">paresse</w></l>
						<l n="22" num="6.2"><space quantity="8" unit="char"></space><w n="22.1">Ta</w> <w n="22.2">tête</w> <w n="22.3">d</w>’<w n="22.4">enfant</w></l>
						<l n="23" num="6.3"><w n="23.1">Se</w> <w n="23.2">balance</w> <w n="23.3">avec</w> <w n="23.4">la</w> <w n="23.5">mollesse</w></l>
						<l n="24" num="6.4"><space quantity="8" unit="char"></space><w n="24.1">D</w>’<w n="24.2">un</w> <w n="24.3">jeune</w> <w n="24.4">éléphant</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Et</w> <w n="25.2">ton</w> <w n="25.3">corps</w> <w n="25.4">se</w> <w n="25.5">penche</w> <w n="25.6">et</w> <w n="25.7">s</w>’<w n="25.8">allonge</w></l>
						<l n="26" num="7.2"><space quantity="8" unit="char"></space><w n="26.1">Comme</w> <w n="26.2">un</w> <w n="26.3">fin</w> <w n="26.4">vaisseau</w></l>
						<l n="27" num="7.3"><w n="27.1">Qui</w> <w n="27.2">roule</w> <w n="27.3">bord</w> <w n="27.4">sur</w> <w n="27.5">bord</w> <w n="27.6">et</w> <w n="27.7">plonge</w></l>
						<l n="28" num="7.4"><space quantity="8" unit="char"></space><w n="28.1">Ses</w> <w n="28.2">vergues</w> <w n="28.3">dans</w> <w n="28.4">l</w>’<w n="28.5">eau</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Comme</w> <w n="29.2">un</w> <w n="29.3">flot</w> <w n="29.4">grossi</w> <w n="29.5">par</w> <w n="29.6">la</w> <w n="29.7">fonte</w></l>
						<l n="30" num="8.2"><space quantity="8" unit="char"></space><w n="30.1">Des</w> <w n="30.2">glaciers</w> <w n="30.3">grondants</w>,</l>
						<l n="31" num="8.3"><w n="31.1">Quand</w> <w n="31.2">l</w>’<w n="31.3">eau</w> <w n="31.4">de</w> <w n="31.5">ta</w> <w n="31.6">bouche</w> <w n="31.7">remonte</w></l>
						<l n="32" num="8.4"><space quantity="8" unit="char"></space><w n="32.1">Au</w> <w n="32.2">bord</w> <w n="32.3">de</w> <w n="32.4">tes</w> <w n="32.5">dents</w>,</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Je</w> <w n="33.2">crois</w> <w n="33.3">boire</w> <w n="33.4">un</w> <w n="33.5">vin</w> <w n="33.6">de</w> <w n="33.7">Bohême</w>,</l>
						<l n="34" num="9.2"><space quantity="8" unit="char"></space><w n="34.1">Amer</w> <w n="34.2">et</w> <w n="34.3">vainqueur</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Un</w> <w n="35.2">ciel</w> <w n="35.3">liquide</w> <w n="35.4">qui</w> <w n="35.5">parsème</w></l>
						<l n="36" num="9.4"><space quantity="8" unit="char"></space><w n="36.1">D</w>’<w n="36.2">étoiles</w> <w n="36.3">mon</w> <w n="36.4">cœur</w> !</l>
					</lg>
				</div></body></text></TEI>