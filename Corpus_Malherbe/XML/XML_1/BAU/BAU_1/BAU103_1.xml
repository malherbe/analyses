<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">TABLEAUX PARISIENS</head><div type="poem" key="BAU103">
					<head type="number">XCIX</head>
					<head type="main">Le Jeu</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">des</w> <w n="1.3">fauteuils</w> <w n="1.4">fanés</w> <w n="1.5">des</w> <w n="1.6">courtisanes</w> <w n="1.7">vieilles</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Pâles</w>, <w n="2.2">le</w> <w n="2.3">sourcil</w> <w n="2.4">peint</w>, <w n="2.5">l</w>’<w n="2.6">œil</w> <w n="2.7">câlin</w> <w n="2.8">et</w> <w n="2.9">fatal</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Minaudant</w>, <w n="3.2">et</w> <w n="3.3">faisant</w> <w n="3.4">de</w> <w n="3.5">leurs</w> <w n="3.6">maigres</w> <w n="3.7">oreilles</w></l>
						<l n="4" num="1.4"><w n="4.1">Tomber</w> <w n="4.2">un</w> <w n="4.3">cliquetis</w> <w n="4.4">de</w> <w n="4.5">pierre</w> <w n="4.6">et</w> <w n="4.7">de</w> <w n="4.8">métal</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Autour</w> <w n="5.2">des</w> <w n="5.3">verts</w> <w n="5.4">tapis</w> <w n="5.5">des</w> <w n="5.6">visages</w> <w n="5.7">sans</w> <w n="5.8">lèvre</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Des</w> <w n="6.2">lèvres</w> <w n="6.3">sans</w> <w n="6.4">couleur</w>, <w n="6.5">des</w> <w n="6.6">mâchoires</w> <w n="6.7">sans</w> <w n="6.8">dent</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">des</w> <w n="7.3">doigts</w> <w n="7.4">convulsés</w> <w n="7.5">d</w>’<w n="7.6">une</w> <w n="7.7">infernale</w> <w n="7.8">fièvre</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Fouillant</w> <w n="8.2">la</w> <w n="8.3">poche</w> <w n="8.4">vide</w> <w n="8.5">ou</w> <w n="8.6">le</w> <w n="8.7">sein</w> <w n="8.8">palpitant</w> ;</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Sous</w> <w n="9.2">de</w> <w n="9.3">sales</w> <w n="9.4">plafonds</w> <w n="9.5">un</w> <w n="9.6">rang</w> <w n="9.7">de</w> <w n="9.8">pâles</w> <w n="9.9">lustres</w></l>
						<l n="10" num="3.2"><w n="10.1">Et</w> <w n="10.2">d</w>’<w n="10.3">énormes</w> <w n="10.4">quinquets</w> <w n="10.5">projetant</w> <w n="10.6">leurs</w> <w n="10.7">lueurs</w></l>
						<l n="11" num="3.3"><w n="11.1">Sur</w> <w n="11.2">des</w> <w n="11.3">fronts</w> <w n="11.4">ténébreux</w> <w n="11.5">de</w> <w n="11.6">poëtes</w> <w n="11.7">illustres</w></l>
						<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">viennent</w> <w n="12.3">gaspiller</w> <w n="12.4">leurs</w> <w n="12.5">sanglantes</w> <w n="12.6">sueurs</w> ;</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Voilà</w> <w n="13.2">le</w> <w n="13.3">noir</w> <w n="13.4">tableau</w> <w n="13.5">qu</w>’<w n="13.6">en</w> <w n="13.7">un</w> <w n="13.8">rêve</w> <w n="13.9">nocturne</w></l>
						<l n="14" num="4.2"><w n="14.1">Je</w> <w n="14.2">vis</w> <w n="14.3">se</w> <w n="14.4">dérouler</w> <w n="14.5">sous</w> <w n="14.6">mon</w> <w n="14.7">œil</w> <w n="14.8">clairvoyant</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Moi</w>-<w n="15.2">même</w>, <w n="15.3">dans</w> <w n="15.4">un</w> <w n="15.5">coin</w> <w n="15.6">de</w> <w n="15.7">l</w>’<w n="15.8">antre</w> <w n="15.9">taciturne</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Je</w> <w n="16.2">me</w> <w n="16.3">vis</w> <w n="16.4">accoudé</w>, <w n="16.5">froid</w>, <w n="16.6">muet</w>, <w n="16.7">enviant</w>,</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Enviant</w> <w n="17.2">de</w> <w n="17.3">ces</w> <w n="17.4">gens</w> <w n="17.5">la</w> <w n="17.6">passion</w> <w n="17.7">tenace</w>,</l>
						<l n="18" num="5.2"><w n="18.1">De</w> <w n="18.2">ces</w> <w n="18.3">vieilles</w> <w n="18.4">putains</w> <w n="18.5">la</w> <w n="18.6">funèbre</w> <w n="18.7">gaîté</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">tous</w> <w n="19.3">gaillardement</w> <w n="19.4">trafiquant</w> <w n="19.5">à</w> <w n="19.6">ma</w> <w n="19.7">face</w>,</l>
						<l n="20" num="5.4"><w n="20.1">L</w>’<w n="20.2">un</w> <w n="20.3">de</w> <w n="20.4">son</w> <w n="20.5">vieil</w> <w n="20.6">honneur</w>, <w n="20.7">l</w>’<w n="20.8">autre</w> <w n="20.9">de</w> <w n="20.10">sa</w> <w n="20.11">beauté</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">mon</w> <w n="21.3">cœur</w> <w n="21.4">s</w>’<w n="21.5">effraya</w> <w n="21.6">d</w>’<w n="21.7">envier</w> <w n="21.8">maint</w> <w n="21.9">pauvre</w> <w n="21.10">homme</w></l>
						<l n="22" num="6.2"><w n="22.1">Courant</w> <w n="22.2">avec</w> <w n="22.3">ferveur</w> <w n="22.4">à</w> <w n="22.5">l</w>’<w n="22.6">abîme</w> <w n="22.7">béant</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Et</w> <w n="23.2">qui</w>, <w n="23.3">soûl</w> <w n="23.4">de</w> <w n="23.5">son</w> <w n="23.6">sang</w>, <w n="23.7">préférerait</w> <w n="23.8">en</w> <w n="23.9">somme</w></l>
						<l n="24" num="6.4"><w n="24.1">La</w> <w n="24.2">douleur</w> <w n="24.3">à</w> <w n="24.4">la</w> <w n="24.5">mort</w> <w n="24.6">et</w> <w n="24.7">l</w>’<w n="24.8">enfer</w> <w n="24.9">au</w> <w n="24.10">néant</w> !</l>
					</lg>
				</div></body></text></TEI>