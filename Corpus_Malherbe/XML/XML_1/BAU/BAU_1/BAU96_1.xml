<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">TABLEAUX PARISIENS</head><div type="poem" key="BAU96">
					<head type="number">XCII</head>
					<head type="main">Le Cygne</head>
					<opener><salute>À <name>Victor Hugo</name></salute></opener>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Andromaque</w>, <w n="1.2">je</w> <w n="1.3">pense</w> <w n="1.4">à</w> <w n="1.5">vous</w> ! — <w n="1.6">Ce</w> <w n="1.7">petit</w> <w n="1.8">fleuve</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Pauvre</w> <w n="2.2">et</w> <w n="2.3">triste</w> <w n="2.4">miroir</w> <w n="2.5">où</w> <w n="2.6">jadis</w> <w n="2.7">resplendit</w></l>
							<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">immense</w> <w n="3.3">majesté</w> <w n="3.4">de</w> <w n="3.5">vos</w> <w n="3.6">douleurs</w> <w n="3.7">de</w> <w n="3.8">veuve</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">Simoïs</w> <w n="4.3">menteur</w> <w n="4.4">qui</w> <w n="4.5">par</w> <w n="4.6">vos</w> <w n="4.7">pleurs</w> <w n="4.8">grandit</w>,</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">A</w> <w n="5.2">fécondé</w> <w n="5.3">soudain</w> <w n="5.4">ma</w> <w n="5.5">mémoire</w> <w n="5.6">fertile</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Comme</w> <w n="6.2">je</w> <w n="6.3">traversais</w> <w n="6.4">le</w> <w n="6.5">nouveau</w> <w n="6.6">Carrousel</w>.</l>
							<l n="7" num="2.3">— <w n="7.1">Le</w> <w n="7.2">vieux</w> <w n="7.3">Paris</w> <w n="7.4">n</w>’<w n="7.5">est</w> <w n="7.6">plus</w> (<w n="7.7">la</w> <w n="7.8">forme</w> <w n="7.9">d</w>’<w n="7.10">une</w> <w n="7.11">ville</w></l>
							<l n="8" num="2.4"><w n="8.1">Change</w> <w n="8.2">plus</w> <w n="8.3">vite</w>, <w n="8.4">hélas</w> ! <w n="8.5">que</w> <w n="8.6">le</w> <w n="8.7">cœur</w> <w n="8.8">d</w>’<w n="8.9">un</w> <w n="8.10">mortel</w>) ;</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">ne</w> <w n="9.3">vois</w> <w n="9.4">qu</w>’<w n="9.5">en</w> <w n="9.6">esprit</w> <w n="9.7">tout</w> <w n="9.8">ce</w> <w n="9.9">camp</w> <w n="9.10">de</w> <w n="9.11">baraques</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Ces</w> <w n="10.2">tas</w> <w n="10.3">de</w> <w n="10.4">chapiteaux</w> <w n="10.5">ébauchés</w> <w n="10.6">et</w> <w n="10.7">de</w> <w n="10.8">fûts</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">herbes</w>, <w n="11.3">les</w> <w n="11.4">gros</w> <w n="11.5">blocs</w> <w n="11.6">verdis</w> <w n="11.7">par</w> <w n="11.8">l</w>’<w n="11.9">eau</w> <w n="11.10">des</w> <w n="11.11">flaques</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Et</w>, <w n="12.2">brillant</w> <w n="12.3">aux</w> <w n="12.4">carreaux</w>, <w n="12.5">le</w> <w n="12.6">bric</w>-<w n="12.7">à</w>-<w n="12.8">brac</w> <w n="12.9">confus</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Là</w> <w n="13.2">s</w>’<w n="13.3">étalait</w> <w n="13.4">jadis</w> <w n="13.5">une</w> <w n="13.6">ménagerie</w> ;</l>
							<l n="14" num="4.2"><w n="14.1">Là</w> <w n="14.2">je</w> <w n="14.3">vis</w> <w n="14.4">un</w> <w n="14.5">matin</w>, <w n="14.6">à</w> <w n="14.7">l</w>’<w n="14.8">heure</w> <w n="14.9">où</w> <w n="14.10">sous</w> <w n="14.11">les</w> <w n="14.12">cieux</w></l>
							<l n="15" num="4.3"><w n="15.1">Clairs</w> <w n="15.2">et</w> <w n="15.3">froids</w> <w n="15.4">le</w> <w n="15.5">Travail</w> <w n="15.6">s</w>’<w n="15.7">éveille</w>, <w n="15.8">où</w> <w n="15.9">la</w> <w n="15.10">voirie</w></l>
							<l n="16" num="4.4"><w n="16.1">Pousse</w> <w n="16.2">un</w> <w n="16.3">sombre</w> <w n="16.4">ouragan</w> <w n="16.5">dans</w> <w n="16.6">l</w>’<w n="16.7">air</w> <w n="16.8">silencieux</w>,</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Un</w> <w n="17.2">cygne</w> <w n="17.3">qui</w> <w n="17.4">s</w>’<w n="17.5">était</w> <w n="17.6">évadé</w> <w n="17.7">de</w> <w n="17.8">sa</w> <w n="17.9">cage</w>,</l>
							<l n="18" num="5.2"><w n="18.1">Et</w>, <w n="18.2">de</w> <w n="18.3">ses</w> <w n="18.4">pieds</w> <w n="18.5">palmés</w> <w n="18.6">frottant</w> <w n="18.7">le</w> <w n="18.8">pavé</w> <w n="18.9">sec</w>,</l>
							<l n="19" num="5.3"><w n="19.1">Sur</w> <w n="19.2">le</w> <w n="19.3">sol</w> <w n="19.4">raboteux</w> <w n="19.5">traînait</w> <w n="19.6">son</w> <w n="19.7">blanc</w> <w n="19.8">plumage</w>.</l>
							<l n="20" num="5.4"><w n="20.1">Près</w> <w n="20.2">d</w>’<w n="20.3">un</w> <w n="20.4">ruisseau</w> <w n="20.5">sans</w> <w n="20.6">eau</w> <w n="20.7">la</w> <w n="20.8">bête</w> <w n="20.9">ouvrant</w> <w n="20.10">le</w> <w n="20.11">bec</w></l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Baignait</w> <w n="21.2">nerveusement</w> <w n="21.3">ses</w> <w n="21.4">ailes</w> <w n="21.5">dans</w> <w n="21.6">la</w> <w n="21.7">poudre</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">disait</w>, <w n="22.3">le</w> <w n="22.4">cœur</w> <w n="22.5">plein</w> <w n="22.6">de</w> <w n="22.7">son</w> <w n="22.8">beau</w> <w n="22.9">lac</w> <w n="22.10">natal</w> :</l>
							<l n="23" num="6.3">« <w n="23.1">Eau</w>, <w n="23.2">quand</w> <w n="23.3">donc</w> <w n="23.4">pleuvras</w>-<w n="23.5">tu</w> ? <w n="23.6">quand</w> <w n="23.7">tonneras</w>-<w n="23.8">tu</w>, <w n="23.9">foudre</w> ? »</l>
							<l n="24" num="6.4"><w n="24.1">Je</w> <w n="24.2">vois</w> <w n="24.3">ce</w> <w n="24.4">malheureux</w>, <w n="24.5">mythe</w> <w n="24.6">étrange</w> <w n="24.7">et</w> <w n="24.8">fatal</w>,</l>
						</lg>
						<lg n="7">
							<l n="25" num="7.1"><w n="25.1">Vers</w> <w n="25.2">le</w> <w n="25.3">ciel</w> <w n="25.4">quelquefois</w>, <w n="25.5">comme</w> <w n="25.6">l</w>’<w n="25.7">homme</w> <w n="25.8">d</w>’<w n="25.9">Ovide</w>,</l>
							<l n="26" num="7.2"><w n="26.1">Vers</w> <w n="26.2">le</w> <w n="26.3">ciel</w> <w n="26.4">ironique</w> <w n="26.5">et</w> <w n="26.6">cruellement</w> <w n="26.7">bleu</w>,</l>
							<l n="27" num="7.3"><w n="27.1">Sur</w> <w n="27.2">son</w> <w n="27.3">cou</w> <w n="27.4">convulsif</w> <w n="27.5">tendant</w> <w n="27.6">sa</w> <w n="27.7">tête</w> <w n="27.8">avide</w>,</l>
							<l n="28" num="7.4"><w n="28.1">Comme</w> <w n="28.2">s</w>’<w n="28.3">il</w> <w n="28.4">adressait</w> <w n="28.5">des</w> <w n="28.6">reproches</w> <w n="28.7">à</w> <w n="28.8">Dieu</w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="29" num="1.1"><w n="29.1">Paris</w> <w n="29.2">change</w> ! <w n="29.3">mais</w> <w n="29.4">rien</w> <w n="29.5">dans</w> <w n="29.6">ma</w> <w n="29.7">mélancolie</w></l>
							<l n="30" num="1.2"><w n="30.1">N</w>’<w n="30.2">a</w> <w n="30.3">bougé</w> ! <w n="30.4">palais</w> <w n="30.5">neufs</w>, <w n="30.6">échafaudages</w>, <w n="30.7">blocs</w>,</l>
							<l n="31" num="1.3"><w n="31.1">Vieux</w> <w n="31.2">faubourgs</w>, <w n="31.3">tout</w> <w n="31.4">pour</w> <w n="31.5">moi</w> <w n="31.6">devient</w> <w n="31.7">allégorie</w>,</l>
							<l n="32" num="1.4"><w n="32.1">Et</w> <w n="32.2">mes</w> <w n="32.3">chers</w> <w n="32.4">souvenirs</w> <w n="32.5">sont</w> <w n="32.6">plus</w> <w n="32.7">lourds</w> <w n="32.8">que</w> <w n="32.9">des</w> <w n="32.10">rocs</w>.</l>
						</lg>
						<lg n="2">
							<l n="33" num="2.1"><w n="33.1">Aussi</w> <w n="33.2">devant</w> <w n="33.3">ce</w> <w n="33.4">Louvre</w> <w n="33.5">une</w> <w n="33.6">image</w> <w n="33.7">m</w>’<w n="33.8">opprime</w> :</l>
							<l n="34" num="2.2"><w n="34.1">Je</w> <w n="34.2">pense</w> <w n="34.3">à</w> <w n="34.4">mon</w> <w n="34.5">grand</w> <w n="34.6">cygne</w>, <w n="34.7">avec</w> <w n="34.8">ses</w> <w n="34.9">gestes</w> <w n="34.10">fous</w>,</l>
							<l n="35" num="2.3"><w n="35.1">Comme</w> <w n="35.2">les</w> <w n="35.3">exilés</w>, <w n="35.4">ridicule</w> <w n="35.5">et</w> <w n="35.6">sublime</w>,</l>
							<l n="36" num="2.4"><w n="36.1">Et</w> <w n="36.2">rongé</w> <w n="36.3">d</w>’<w n="36.4">un</w> <w n="36.5">désir</w> <w n="36.6">sans</w> <w n="36.7">trêve</w> ! <w n="36.8">et</w> <w n="36.9">puis</w> <w n="36.10">à</w> <w n="36.11">vous</w>,</l>
						</lg>
						<lg n="3">
							<l n="37" num="3.1"><w n="37.1">Andromaque</w>, <w n="37.2">des</w> <w n="37.3">bras</w> <w n="37.4">d</w>’<w n="37.5">un</w> <w n="37.6">grand</w> <w n="37.7">époux</w> <w n="37.8">tombée</w>,</l>
							<l n="38" num="3.2"><w n="38.1">Vil</w> <w n="38.2">bétail</w>, <w n="38.3">sous</w> <w n="38.4">la</w> <w n="38.5">main</w> <w n="38.6">du</w> <w n="38.7">superbe</w> <w n="38.8">Pyrrhus</w>,</l>
							<l n="39" num="3.3"><w n="39.1">Auprès</w> <w n="39.2">d</w>’<w n="39.3">un</w> <w n="39.4">tombeau</w> <w n="39.5">vide</w> <w n="39.6">en</w> <w n="39.7">extase</w> <w n="39.8">courbée</w> ;</l>
							<l n="40" num="3.4"><w n="40.1">Veuve</w> <w n="40.2">d</w>’<w n="40.3">Hector</w>, <w n="40.4">hélas</w> ! <w n="40.5">et</w> <w n="40.6">femme</w> <w n="40.7">d</w>’<w n="40.8">Hélénus</w> !</l>
						</lg>
						<lg n="4">
							<l n="41" num="4.1"><w n="41.1">Je</w> <w n="41.2">pense</w> <w n="41.3">à</w> <w n="41.4">la</w> <w n="41.5">négresse</w>, <w n="41.6">amaigrie</w> <w n="41.7">et</w> <w n="41.8">phtisique</w>,</l>
							<l n="42" num="4.2"><w n="42.1">Piétinant</w> <w n="42.2">dans</w> <w n="42.3">la</w> <w n="42.4">boue</w>, <w n="42.5">et</w> <w n="42.6">cherchant</w>, <w n="42.7">l</w>’<w n="42.8">œil</w> <w n="42.9">hagard</w>,</l>
							<l n="43" num="4.3"><w n="43.1">Les</w> <w n="43.2">cocotiers</w> <w n="43.3">absents</w> <w n="43.4">de</w> <w n="43.5">la</w> <w n="43.6">superbe</w> <w n="43.7">Afrique</w></l>
							<l n="44" num="4.4"><w n="44.1">Derrière</w> <w n="44.2">la</w> <w n="44.3">muraille</w> <w n="44.4">immense</w> <w n="44.5">du</w> <w n="44.6">brouillard</w> ;</l>
						</lg>
						<lg n="5">
							<l n="45" num="5.1"><w n="45.1">À</w> <w n="45.2">quiconque</w> <w n="45.3">a</w> <w n="45.4">perdu</w> <w n="45.5">ce</w> <w n="45.6">qui</w> <w n="45.7">ne</w> <w n="45.8">se</w> <w n="45.9">retrouve</w></l>
							<l n="46" num="5.2"><w n="46.1">Jamais</w> ! <w n="46.2">jamais</w> ! <w n="46.3">à</w> <w n="46.4">ceux</w> <w n="46.5">qui</w> <w n="46.6">s</w>’<w n="46.7">abreuvent</w> <w n="46.8">de</w> <w n="46.9">pleurs</w></l>
							<l n="47" num="5.3"><w n="47.1">Et</w> <w n="47.2">tettent</w> <w n="47.3">la</w> <w n="47.4">Douleur</w> <w n="47.5">comme</w> <w n="47.6">une</w> <w n="47.7">bonne</w> <w n="47.8">louve</w> !</l>
							<l n="48" num="5.4"><w n="48.1">Aux</w> <w n="48.2">maigres</w> <w n="48.3">orphelins</w> <w n="48.4">séchant</w> <w n="48.5">comme</w> <w n="48.6">des</w> <w n="48.7">fleurs</w> !</l>
						</lg>
						<lg n="6">
							<l n="49" num="6.1"><w n="49.1">Ainsi</w> <w n="49.2">dans</w> <w n="49.3">la</w> <w n="49.4">forêt</w> <w n="49.5">où</w> <w n="49.6">mon</w> <w n="49.7">esprit</w> <w n="49.8">s</w>’<w n="49.9">exile</w></l>
							<l n="50" num="6.2"><w n="50.1">Un</w> <w n="50.2">vieux</w> <w n="50.3">Souvenir</w> <w n="50.4">sonne</w> <w n="50.5">à</w> <w n="50.6">plein</w> <w n="50.7">souffle</w> <w n="50.8">du</w> <w n="50.9">cor</w> !</l>
							<l n="51" num="6.3"><w n="51.1">Je</w> <w n="51.2">pense</w> <w n="51.3">aux</w> <w n="51.4">matelots</w> <w n="51.5">oubliés</w> <w n="51.6">dans</w> <w n="51.7">une</w> <w n="51.8">île</w>,</l>
							<l n="52" num="6.4"><w n="52.1">Aux</w> <w n="52.2">captifs</w>, <w n="52.3">aux</w> <w n="52.4">vaincus</w> !… <w n="52.5">à</w> <w n="52.6">bien</w> <w n="52.7">d</w>’<w n="52.8">autres</w> <w n="52.9">encor</w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>