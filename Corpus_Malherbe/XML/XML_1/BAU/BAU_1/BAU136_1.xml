<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">LA MORT</head><div type="poem" key="BAU136">
					<head type="number">CXXXII</head>
					<head type="main">Le Voyage</head>
					<opener><salute>À <name>Maxime du Camp</name>.</salute></opener>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Pour</w> <w n="1.2">l</w>’<w n="1.3">enfant</w>, <w n="1.4">amoureux</w> <w n="1.5">de</w> <w n="1.6">cartes</w> <w n="1.7">et</w> <w n="1.8">d</w>’<w n="1.9">estampes</w>,</l>
							<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">univers</w> <w n="2.3">est</w> <w n="2.4">égal</w> <w n="2.5">à</w> <w n="2.6">son</w> <w n="2.7">vaste</w> <w n="2.8">appétit</w>.</l>
							<l n="3" num="1.3"><w n="3.1">Ah</w> ! <w n="3.2">que</w> <w n="3.3">le</w> <w n="3.4">monde</w> <w n="3.5">est</w> <w n="3.6">grand</w> <w n="3.7">à</w> <w n="3.8">la</w> <w n="3.9">clarté</w> <w n="3.10">des</w> <w n="3.11">lampes</w> !</l>
							<l n="4" num="1.4"><w n="4.1">Aux</w> <w n="4.2">yeux</w> <w n="4.3">du</w> <w n="4.4">souvenir</w> <w n="4.5">que</w> <w n="4.6">le</w> <w n="4.7">monde</w> <w n="4.8">est</w> <w n="4.9">petit</w> !</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Un</w> <w n="5.2">matin</w> <w n="5.3">nous</w> <w n="5.4">partons</w>, <w n="5.5">le</w> <w n="5.6">cerveau</w> <w n="5.7">plein</w> <w n="5.8">de</w> <w n="5.9">flamme</w>,</l>
							<l n="6" num="2.2"><w n="6.1">Le</w> <w n="6.2">cœur</w> <w n="6.3">gros</w> <w n="6.4">de</w> <w n="6.5">rancune</w> <w n="6.6">et</w> <w n="6.7">de</w> <w n="6.8">désirs</w> <w n="6.9">amers</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">nous</w> <w n="7.3">allons</w>, <w n="7.4">suivant</w> <w n="7.5">le</w> <w n="7.6">rhythme</w> <w n="7.7">de</w> <w n="7.8">la</w> <w n="7.9">lame</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Berçant</w> <w n="8.2">notre</w> <w n="8.3">infini</w> <w n="8.4">sur</w> <w n="8.5">le</w> <w n="8.6">fini</w> <w n="8.7">des</w> <w n="8.8">mers</w> :</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Les</w> <w n="9.2">uns</w>, <w n="9.3">joyeux</w> <w n="9.4">de</w> <w n="9.5">fuir</w> <w n="9.6">une</w> <w n="9.7">patrie</w> <w n="9.8">infâme</w> ;</l>
							<l n="10" num="3.2"><w n="10.1">D</w>’<w n="10.2">autres</w>, <w n="10.3">l</w>’<w n="10.4">horreur</w> <w n="10.5">de</w> <w n="10.6">leurs</w> <w n="10.7">berceaux</w>, <w n="10.8">et</w> <w n="10.9">quelques</w>-<w n="10.10">uns</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Astrologues</w> <w n="11.2">noyés</w> <w n="11.3">dans</w> <w n="11.4">les</w> <w n="11.5">yeux</w> <w n="11.6">d</w>’<w n="11.7">une</w> <w n="11.8">femme</w>,</l>
							<l n="12" num="3.4"><w n="12.1">La</w> <w n="12.2">Circé</w> <w n="12.3">tyrannique</w> <w n="12.4">aux</w> <w n="12.5">dangereux</w> <w n="12.6">parfums</w>.</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Pour</w> <w n="13.2">n</w>’<w n="13.3">être</w> <w n="13.4">pas</w> <w n="13.5">changés</w> <w n="13.6">en</w> <w n="13.7">bêtes</w>, <w n="13.8">ils</w> <w n="13.9">s</w>’<w n="13.10">enivrent</w></l>
							<l n="14" num="4.2"><w n="14.1">D</w>’<w n="14.2">espace</w> <w n="14.3">et</w> <w n="14.4">de</w> <w n="14.5">lumière</w> <w n="14.6">et</w> <w n="14.7">de</w> <w n="14.8">cieux</w> <w n="14.9">embrasés</w> ;</l>
							<l n="15" num="4.3"><w n="15.1">La</w> <w n="15.2">glace</w> <w n="15.3">qui</w> <w n="15.4">les</w> <w n="15.5">mord</w>, <w n="15.6">les</w> <w n="15.7">soleils</w> <w n="15.8">qui</w> <w n="15.9">les</w> <w n="15.10">cuivrent</w>,</l>
							<l n="16" num="4.4"><w n="16.1">Effacent</w> <w n="16.2">lentement</w> <w n="16.3">la</w> <w n="16.4">marque</w> <w n="16.5">des</w> <w n="16.6">baisers</w>.</l>
						</lg>
						<lg n="5">
							<l n="17" num="5.1"><w n="17.1">Mais</w> <w n="17.2">les</w> <w n="17.3">vrais</w> <w n="17.4">voyageurs</w> <w n="17.5">sont</w> <w n="17.6">ceux</w>-<w n="17.7">là</w> <w n="17.8">seuls</w> <w n="17.9">qui</w> <w n="17.10">partent</w></l>
							<l n="18" num="5.2"><w n="18.1">Pour</w> <w n="18.2">partir</w> ; <w n="18.3">cœurs</w> <w n="18.4">légers</w>, <w n="18.5">semblables</w> <w n="18.6">aux</w> <w n="18.7">ballons</w>,</l>
							<l n="19" num="5.3"><w n="19.1">De</w> <w n="19.2">leur</w> <w n="19.3">fatalité</w> <w n="19.4">jamais</w> <w n="19.5">ils</w> <w n="19.6">ne</w> <w n="19.7">s</w>’<w n="19.8">écartent</w>,</l>
							<l n="20" num="5.4"><w n="20.1">Et</w>, <w n="20.2">sans</w> <w n="20.3">savoir</w> <w n="20.4">pourquoi</w>, <w n="20.5">disent</w> <w n="20.6">toujours</w> : <w n="20.7">Allons</w> !</l>
						</lg>
						<lg n="6">
							<l n="21" num="6.1"><w n="21.1">Ceux</w>-<w n="21.2">là</w> <w n="21.3">dont</w> <w n="21.4">les</w> <w n="21.5">désirs</w> <w n="21.6">ont</w> <w n="21.7">la</w> <w n="21.8">forme</w> <w n="21.9">des</w> <w n="21.10">nues</w>,</l>
							<l n="22" num="6.2"><w n="22.1">Et</w> <w n="22.2">qui</w> <w n="22.3">rêvent</w>, <w n="22.4">ainsi</w> <w n="22.5">qu</w>’<w n="22.6">un</w> <w n="22.7">conscrit</w> <w n="22.8">le</w> <w n="22.9">canon</w>,</l>
							<l n="23" num="6.3"><w n="23.1">De</w> <w n="23.2">vastes</w> <w n="23.3">voluptés</w>, <w n="23.4">changeantes</w>, <w n="23.5">inconnues</w>,</l>
							<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">dont</w> <w n="24.3">l</w>’<w n="24.4">esprit</w> <w n="24.5">humain</w> <w n="24.6">n</w>’<w n="24.7">a</w> <w n="24.8">jamais</w> <w n="24.9">su</w> <w n="24.10">le</w> <w n="24.11">nom</w> !</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="25" num="1.1"><w n="25.1">Nous</w> <w n="25.2">imitons</w>, <w n="25.3">horreur</w> ! <w n="25.4">la</w> <w n="25.5">toupie</w> <w n="25.6">et</w> <w n="25.7">la</w> <w n="25.8">boule</w></l>
							<l n="26" num="1.2"><w n="26.1">Dans</w> <w n="26.2">leur</w> <w n="26.3">valse</w> <w n="26.4">et</w> <w n="26.5">leurs</w> <w n="26.6">bonds</w> ; <w n="26.7">même</w> <w n="26.8">dans</w> <w n="26.9">nos</w> <w n="26.10">sommeils</w></l>
							<l n="27" num="1.3"><w n="27.1">La</w> <w n="27.2">Curiosité</w> <w n="27.3">nous</w> <w n="27.4">tourmente</w> <w n="27.5">et</w> <w n="27.6">nous</w> <w n="27.7">roule</w>,</l>
							<l n="28" num="1.4"><w n="28.1">Comme</w> <w n="28.2">un</w> <w n="28.3">Ange</w> <w n="28.4">cruel</w> <w n="28.5">qui</w> <w n="28.6">fouette</w> <w n="28.7">des</w> <w n="28.8">soleils</w>.</l>
						</lg>
						<lg n="2">
							<l n="29" num="2.1"><w n="29.1">Singulière</w> <w n="29.2">fortune</w> <w n="29.3">où</w> <w n="29.4">le</w> <w n="29.5">but</w> <w n="29.6">se</w> <w n="29.7">déplace</w>,</l>
							<l n="30" num="2.2"><w n="30.1">Et</w>, <w n="30.2">n</w>’<w n="30.3">étant</w> <w n="30.4">nulle</w> <w n="30.5">part</w>, <w n="30.6">peut</w> <w n="30.7">être</w> <w n="30.8">n</w>’<w n="30.9">importe</w> <w n="30.10">où</w> !</l>
							<l n="31" num="2.3"><w n="31.1">Où</w> <w n="31.2">l</w>’<w n="31.3">Homme</w>, <w n="31.4">dont</w> <w n="31.5">jamais</w> <w n="31.6">l</w>’<w n="31.7">espérance</w> <w n="31.8">n</w>’<w n="31.9">est</w> <w n="31.10">lasse</w>,</l>
							<l n="32" num="2.4"><w n="32.1">Pour</w> <w n="32.2">trouver</w> <w n="32.3">le</w> <w n="32.4">repos</w> <w n="32.5">court</w> <w n="32.6">toujours</w> <w n="32.7">comme</w> <w n="32.8">un</w> <w n="32.9">fou</w> !</l>
						</lg>
						<lg n="3">
							<l n="33" num="3.1"><w n="33.1">Notre</w> <w n="33.2">âme</w> <w n="33.3">est</w> <w n="33.4">un</w> <w n="33.5">trois</w>-<w n="33.6">mâts</w> <w n="33.7">cherchant</w> <w n="33.8">son</w> <w n="33.9">Icarie</w> ;</l>
							<l n="34" num="3.2"><w n="34.1">Une</w> <w n="34.2">voix</w> <w n="34.3">retentit</w> <w n="34.4">sur</w> <w n="34.5">le</w> <w n="34.6">pont</w> : « <w n="34.7">Ouvre</w> <w n="34.8">l</w>’<w n="34.9">œil</w> ! »</l>
							<l n="35" num="3.3"><w n="35.1">Une</w> <w n="35.2">voix</w> <w n="35.3">de</w> <w n="35.4">la</w> <w n="35.5">hune</w>, <w n="35.6">ardente</w> <w n="35.7">et</w> <w n="35.8">folle</w>, <w n="35.9">crie</w> :</l>
							<l n="36" num="3.4">« <w n="36.1">Amour</w>… <w n="36.2">gloire</w>… <w n="36.3">bonheur</w> ! » <w n="36.4">Enfer</w> ! <w n="36.5">c</w>’<w n="36.6">est</w> <w n="36.7">un</w> <w n="36.8">écueil</w></l>
						</lg>
						<lg n="4">
							<l n="37" num="4.1"><w n="37.1">Chaque</w> <w n="37.2">îlot</w> <w n="37.3">signalé</w> <w n="37.4">par</w> <w n="37.5">l</w>’<w n="37.6">homme</w> <w n="37.7">de</w> <w n="37.8">vigie</w></l>
							<l n="38" num="4.2"><w n="38.1">Est</w> <w n="38.2">un</w> <w n="38.3">Eldorado</w> <w n="38.4">promis</w> <w n="38.5">par</w> <w n="38.6">le</w> <w n="38.7">Destin</w> ;</l>
							<l n="39" num="4.3"><w n="39.1">L</w>’<w n="39.2">Imagination</w> <w n="39.3">qui</w> <w n="39.4">dresse</w> <w n="39.5">son</w> <w n="39.6">orgie</w></l>
							<l n="40" num="4.4"><w n="40.1">Ne</w> <w n="40.2">trouve</w> <w n="40.3">qu</w>’<w n="40.4">un</w> <w n="40.5">récif</w> <w n="40.6">aux</w> <w n="40.7">clartés</w> <w n="40.8">du</w> <w n="40.9">matin</w>.</l>
						</lg>
						<lg n="5">
							<l n="41" num="5.1"><w n="41.1">Ô</w> <w n="41.2">le</w> <w n="41.3">pauvre</w> <w n="41.4">amoureux</w> <w n="41.5">des</w> <w n="41.6">pays</w> <w n="41.7">chimériques</w> !</l>
							<l n="42" num="5.2"><w n="42.1">Faut</w>-<w n="42.2">il</w> <w n="42.3">le</w> <w n="42.4">mettre</w> <w n="42.5">aux</w> <w n="42.6">fers</w>, <w n="42.7">le</w> <w n="42.8">jeter</w> <w n="42.9">à</w> <w n="42.10">la</w> <w n="42.11">mer</w>,</l>
							<l n="43" num="5.3"><w n="43.1">Ce</w> <w n="43.2">matelot</w> <w n="43.3">ivrogne</w>, <w n="43.4">inventeur</w> <w n="43.5">d</w>’<w n="43.6">Amériques</w></l>
							<l n="44" num="5.4"><w n="44.1">Dont</w> <w n="44.2">le</w> <w n="44.3">mirage</w> <w n="44.4">rend</w> <w n="44.5">le</w> <w n="44.6">gouffre</w> <w n="44.7">plus</w> <w n="44.8">amer</w> ?</l>
						</lg>
						<lg n="6">
							<l n="45" num="6.1"><w n="45.1">Tel</w> <w n="45.2">le</w> <w n="45.3">vieux</w> <w n="45.4">vagabond</w>, <w n="45.5">piétinant</w> <w n="45.6">dans</w> <w n="45.7">la</w> <w n="45.8">boue</w>,</l>
							<l n="46" num="6.2"><w n="46.1">Rêve</w>, <w n="46.2">le</w> <w n="46.3">nez</w> <w n="46.4">en</w> <w n="46.5">l</w>’<w n="46.6">air</w>, <w n="46.7">de</w> <w n="46.8">brillants</w> <w n="46.9">paradis</w> ;</l>
							<l n="47" num="6.3"><w n="47.1">Son</w> <w n="47.2">œil</w> <w n="47.3">ensorcelé</w> <w n="47.4">découvre</w> <w n="47.5">une</w> <w n="47.6">Capoue</w></l>
							<l n="48" num="6.4"><w n="48.1">Partout</w> <w n="48.2">où</w> <w n="48.3">la</w> <w n="48.4">chandelle</w> <w n="48.5">illumine</w> <w n="48.6">un</w> <w n="48.7">taudis</w>.</l>
						</lg>
					</div>
					<div type="section" n="3">
						<head type="number">III</head>
						<lg n="1">
							<l n="49" num="1.1"><w n="49.1">Étonnants</w> <w n="49.2">voyageurs</w> ! <w n="49.3">quelles</w> <w n="49.4">nobles</w> <w n="49.5">histoires</w></l>
							<l n="50" num="1.2"><w n="50.1">Nous</w> <w n="50.2">lisons</w> <w n="50.3">dans</w> <w n="50.4">vos</w> <w n="50.5">yeux</w> <w n="50.6">profonds</w> <w n="50.7">comme</w> <w n="50.8">les</w> <w n="50.9">mers</w> !</l>
							<l n="51" num="1.3"><w n="51.1">Montrez</w>-<w n="51.2">nous</w> <w n="51.3">les</w> <w n="51.4">écrins</w> <w n="51.5">de</w> <w n="51.6">vos</w> <w n="51.7">riches</w> <w n="51.8">mémoires</w>,</l>
							<l n="52" num="1.4"><w n="52.1">Ces</w> <w n="52.2">bijoux</w> <w n="52.3">merveilleux</w>, <w n="52.4">faits</w> <w n="52.5">d</w>’<w n="52.6">astres</w> <w n="52.7">et</w> <w n="52.8">d</w>’<w n="52.9">éthers</w>.</l>
						</lg>
						<lg n="2">
							<l n="53" num="2.1"><w n="53.1">Nous</w> <w n="53.2">voulons</w> <w n="53.3">voyager</w> <w n="53.4">sans</w> <w n="53.5">vapeur</w> <w n="53.6">et</w> <w n="53.7">sans</w> <w n="53.8">voile</w> !</l>
							<l n="54" num="2.2"><w n="54.1">Faites</w>, <w n="54.2">pour</w> <w n="54.3">égayer</w> <w n="54.4">l</w>’<w n="54.5">ennui</w> <w n="54.6">de</w> <w n="54.7">nos</w> <w n="54.8">prisons</w>,</l>
							<l n="55" num="2.3"><w n="55.1">Passer</w> <w n="55.2">sur</w> <w n="55.3">nos</w> <w n="55.4">esprits</w>, <w n="55.5">tendus</w> <w n="55.6">comme</w> <w n="55.7">une</w> <w n="55.8">toile</w>,</l>
							<l n="56" num="2.4"><w n="56.1">Vos</w> <w n="56.2">souvenirs</w> <w n="56.3">avec</w> <w n="56.4">leurs</w> <w n="56.5">cadres</w> <w n="56.6">d</w>’<w n="56.7">horizons</w>.</l>
						</lg>
						<lg n="3">
							<l part="I" n="57" num="3.1"><w n="57.1">Dites</w>, <w n="57.2">qu</w>’<w n="57.3">avez</w>-<w n="57.4">vous</w> <w n="57.5">vu</w> ? </l>
						</lg>
					</div>
					<div type="section" n="4">
						<head type="number">IV</head>
						<lg n="1">
							<l part="F" n="57">« <w n="57.6">Nous</w> <w n="57.7">avons</w> <w n="57.8">vu</w> <w n="57.9">des</w> <w n="57.10">astres</w></l>
							<l n="58" num="1.1"><w n="58.1">Et</w> <w n="58.2">des</w> <w n="58.3">flots</w> ; <w n="58.4">nous</w> <w n="58.5">avons</w> <w n="58.6">vu</w> <w n="58.7">des</w> <w n="58.8">sables</w> <w n="58.9">aussi</w> ;</l>
							<l n="59" num="1.2"><w n="59.1">Et</w>, <w n="59.2">malgré</w> <w n="59.3">bien</w> <w n="59.4">des</w> <w n="59.5">chocs</w> <w n="59.6">et</w> <w n="59.7">d</w>’<w n="59.8">imprévus</w> <w n="59.9">désastres</w>,</l>
							<l n="60" num="1.3"><w n="60.1">Nous</w> <w n="60.2">nous</w> <w n="60.3">sommes</w> <w n="60.4">souvent</w> <w n="60.5">ennuyés</w>, <w n="60.6">comme</w> <w n="60.7">ici</w>.</l>
						</lg>
						<lg n="2">
							<l n="61" num="2.1"><w n="61.1">La</w> <w n="61.2">gloire</w> <w n="61.3">du</w> <w n="61.4">soleil</w> <w n="61.5">sur</w> <w n="61.6">la</w> <w n="61.7">mer</w> <w n="61.8">violette</w>,</l>
							<l n="62" num="2.2"><w n="62.1">La</w> <w n="62.2">gloire</w> <w n="62.3">des</w> <w n="62.4">cités</w> <w n="62.5">dans</w> <w n="62.6">le</w> <w n="62.7">soleil</w> <w n="62.8">couchant</w>,</l>
							<l n="63" num="2.3"><w n="63.1">Allumaient</w> <w n="63.2">dans</w> <w n="63.3">nos</w> <w n="63.4">cœurs</w> <w n="63.5">une</w> <w n="63.6">ardeur</w> <w n="63.7">inquiète</w></l>
							<l n="64" num="2.4"><w n="64.1">De</w> <w n="64.2">plonger</w> <w n="64.3">dans</w> <w n="64.4">un</w> <w n="64.5">ciel</w> <w n="64.6">au</w> <w n="64.7">reflet</w> <w n="64.8">alléchant</w>.</l>
						</lg>
						<lg n="3">
							<l n="65" num="3.1"><w n="65.1">Les</w> <w n="65.2">plus</w> <w n="65.3">riches</w> <w n="65.4">cités</w>, <w n="65.5">les</w> <w n="65.6">plus</w> <w n="65.7">grands</w> <w n="65.8">paysages</w>,</l>
							<l n="66" num="3.2"><w n="66.1">Jamais</w> <w n="66.2">ne</w> <w n="66.3">contenaient</w> <w n="66.4">l</w>’<w n="66.5">attrait</w> <w n="66.6">mystérieux</w></l>
							<l n="67" num="3.3"><w n="67.1">De</w> <w n="67.2">ceux</w> <w n="67.3">que</w> <w n="67.4">le</w> <w n="67.5">hasard</w> <w n="67.6">fait</w> <w n="67.7">avec</w> <w n="67.8">les</w> <w n="67.9">nuages</w>.</l>
							<l n="68" num="3.4"><w n="68.1">Et</w> <w n="68.2">toujours</w> <w n="68.3">le</w> <w n="68.4">désir</w> <w n="68.5">nous</w> <w n="68.6">rendait</w> <w n="68.7">soucieux</w> !</l>
						</lg>
						<lg n="4">
							<l n="69" num="4.1">— <w n="69.1">La</w> <w n="69.2">jouissance</w> <w n="69.3">ajoute</w> <w n="69.4">au</w> <w n="69.5">désir</w> <w n="69.6">de</w> <w n="69.7">la</w> <w n="69.8">force</w>.</l>
							<l n="70" num="4.2"><w n="70.1">Désir</w>, <w n="70.2">vieil</w> <w n="70.3">arbre</w> <w n="70.4">à</w> <w n="70.5">qui</w> <w n="70.6">le</w> <w n="70.7">plaisir</w> <w n="70.8">sert</w> <w n="70.9">d</w>’<w n="70.10">engrais</w>,</l>
							<l n="71" num="4.3"><w n="71.1">Cependant</w> <w n="71.2">que</w> <w n="71.3">grossit</w> <w n="71.4">et</w> <w n="71.5">durcit</w> <w n="71.6">ton</w> <w n="71.7">écorce</w>,</l>
							<l n="72" num="4.4"><w n="72.1">Tes</w> <w n="72.2">branches</w> <w n="72.3">veulent</w> <w n="72.4">voir</w> <w n="72.5">le</w> <w n="72.6">soleil</w> <w n="72.7">de</w> <w n="72.8">plus</w> <w n="72.9">près</w> !</l>
						</lg>
						<lg n="5">
							<l n="73" num="5.1"><w n="73.1">Grandiras</w>-<w n="73.2">tu</w> <w n="73.3">toujours</w>, <w n="73.4">grand</w> <w n="73.5">arbre</w> <w n="73.6">plus</w> <w n="73.7">vivace</w></l>
							<l n="74" num="5.2"><w n="74.1">Que</w> <w n="74.2">le</w> <w n="74.3">cyprès</w> ? — <w n="74.4">Pourtant</w> <w n="74.5">nous</w> <w n="74.6">avons</w>, <w n="74.7">avec</w> <w n="74.8">soin</w>,</l>
							<l n="75" num="5.3"><w n="75.1">Cueilli</w> <w n="75.2">quelques</w> <w n="75.3">croquis</w> <w n="75.4">pour</w> <w n="75.5">votre</w> <w n="75.6">album</w> <w n="75.7">vorace</w>,</l>
							<l n="76" num="5.4"><w n="76.1">Frères</w> <w n="76.2">qui</w> <w n="76.3">trouvez</w> <w n="76.4">beau</w> <w n="76.5">tout</w> <w n="76.6">ce</w> <w n="76.7">qui</w> <w n="76.8">vient</w> <w n="76.9">de</w> <w n="76.10">loin</w> !</l>
						</lg>
						<lg n="6">
							<l n="77" num="6.1"><w n="77.1">Nous</w> <w n="77.2">avons</w> <w n="77.3">salué</w> <w n="77.4">des</w> <w n="77.5">idoles</w> <w n="77.6">à</w> <w n="77.7">trompe</w> ;</l>
							<l n="78" num="6.2"><w n="78.1">Des</w> <w n="78.2">trônes</w> <w n="78.3">constellés</w> <w n="78.4">de</w> <w n="78.5">joyaux</w> <w n="78.6">lumineux</w> ;</l>
							<l n="79" num="6.3"><w n="79.1">Des</w> <w n="79.2">palais</w> <w n="79.3">ouvragés</w> <w n="79.4">dont</w> <w n="79.5">la</w> <w n="79.6">féerique</w> <w n="79.7">pompe</w></l>
							<l n="80" num="6.4"><w n="80.1">Serait</w> <w n="80.2">pour</w> <w n="80.3">vos</w> <w n="80.4">banquiers</w> <w n="80.5">un</w> <w n="80.6">rêve</w> <w n="80.7">ruineux</w> ;</l>
						</lg>
						<lg n="7">
							<l n="81" num="7.1"><w n="81.1">Des</w> <w n="81.2">costumes</w> <w n="81.3">qui</w> <w n="81.4">sont</w> <w n="81.5">pour</w> <w n="81.6">les</w> <w n="81.7">yeux</w> <w n="81.8">une</w> <w n="81.9">ivresse</w> ;</l>
							<l n="82" num="7.2"><w n="82.1">Des</w> <w n="82.2">femmes</w> <w n="82.3">dont</w> <w n="82.4">les</w> <w n="82.5">dents</w> <w n="82.6">et</w> <w n="82.7">les</w> <w n="82.8">ongles</w> <w n="82.9">sont</w> <w n="82.10">teints</w>,</l>
							<l n="83" num="7.3"><w n="83.1">Et</w> <w n="83.2">des</w> <w n="83.3">jongleurs</w> <w n="83.4">savants</w> <w n="83.5">que</w> <w n="83.6">le</w> <w n="83.7">serpent</w> <w n="83.8">caresse</w>. »</l>
						</lg>
					</div>
					<div type="section" n="5">
						<head type="number">V</head>
						<lg n="1">
							<l part="I" n="84" num="1.1"><w n="84.1">Et</w> <w n="84.2">puis</w>, <w n="84.3">et</w> <w n="84.4">puis</w> <w n="84.5">encore</w> ? </l>
						</lg>
					</div>
					<div type="section" n="6">
						<head type="number">VI</head>
						<lg n="1">
						<l part="F" n="84">« <w n="84.6">Ô</w> <w n="84.7">cerveaux</w> <w n="84.8">enfantins</w> !</l>
						</lg>
						<lg n="2">
							<l n="85" num="2.1"><w n="85.1">Pour</w> <w n="85.2">ne</w> <w n="85.3">pas</w> <w n="85.4">oublier</w> <w n="85.5">la</w> <w n="85.6">chose</w> <w n="85.7">capitale</w>,</l>
							<l n="86" num="2.2"><w n="86.1">Nous</w> <w n="86.2">avons</w> <w n="86.3">vu</w> <w n="86.4">partout</w>, <w n="86.5">et</w> <w n="86.6">sans</w> <w n="86.7">l</w>’<w n="86.8">avoir</w> <w n="86.9">cherché</w>,</l>
							<l n="87" num="2.3"><w n="87.1">Du</w> <w n="87.2">haut</w> <w n="87.3">jusques</w> <w n="87.4">en</w> <w n="87.5">bas</w> <w n="87.6">de</w> <w n="87.7">l</w>’<w n="87.8">échelle</w> <w n="87.9">fatale</w>,</l>
							<l n="88" num="2.4"><w n="88.1">Le</w> <w n="88.2">spectacle</w> <w n="88.3">ennuyeux</w> <w n="88.4">de</w> <w n="88.5">l</w>’<w n="88.6">immortel</w> <w n="88.7">péché</w> :</l>
						</lg>
						<lg n="3">
							<l n="89" num="3.1"><w n="89.1">La</w> <w n="89.2">femme</w>, <w n="89.3">esclave</w> <w n="89.4">vile</w>, <w n="89.5">orgueilleuse</w> <w n="89.6">et</w> <w n="89.7">stupide</w>,</l>
							<l n="90" num="3.2"><w n="90.1">Sans</w> <w n="90.2">rire</w> <w n="90.3">s</w>’<w n="90.4">adorant</w> <w n="90.5">et</w> <w n="90.6">s</w>’<w n="90.7">aimant</w> <w n="90.8">sans</w> <w n="90.9">dégoût</w> ;</l>
							<l n="91" num="3.3"><w n="91.1">L</w>’<w n="91.2">homme</w>, <w n="91.3">tyran</w> <w n="91.4">goulu</w>, <w n="91.5">paillard</w>, <w n="91.6">dur</w> <w n="91.7">et</w> <w n="91.8">cupide</w>,</l>
							<l n="92" num="3.4"><w n="92.1">Esclave</w> <w n="92.2">de</w> <w n="92.3">l</w>’<w n="92.4">esclave</w> <w n="92.5">et</w> <w n="92.6">ruisseau</w> <w n="92.7">dans</w> <w n="92.8">l</w>’<w n="92.9">égout</w> ;</l>
						</lg>
						<lg n="4">
							<l n="93" num="4.1"><w n="93.1">Le</w> <w n="93.2">bourreau</w> <w n="93.3">qui</w> <w n="93.4">jouit</w>, <w n="93.5">le</w> <w n="93.6">martyr</w> <w n="93.7">qui</w> <w n="93.8">sanglote</w> ;</l>
							<l n="94" num="4.2"><w n="94.1">La</w> <w n="94.2">fête</w> <w n="94.3">qu</w>’<w n="94.4">assaisonne</w> <w n="94.5">et</w> <w n="94.6">parfume</w> <w n="94.7">le</w> <w n="94.8">sang</w> ;</l>
							<l n="95" num="4.3"><w n="95.1">Le</w> <w n="95.2">poison</w> <w n="95.3">du</w> <w n="95.4">pouvoir</w> <w n="95.5">énervant</w> <w n="95.6">le</w> <w n="95.7">despote</w>,</l>
							<l n="96" num="4.4"><w n="96.1">Et</w> <w n="96.2">le</w> <w n="96.3">peuple</w> <w n="96.4">amoureux</w> <w n="96.5">du</w> <w n="96.6">fouet</w> <w n="96.7">abrutissant</w> ;</l>
						</lg>
						<lg n="5">
							<l n="97" num="5.1"><w n="97.1">Plusieurs</w> <w n="97.2">religions</w> <w n="97.3">semblables</w> <w n="97.4">à</w> <w n="97.5">la</w> <w n="97.6">nôtre</w>,</l>
							<l n="98" num="5.2"><w n="98.1">Toutes</w> <w n="98.2">escaladant</w> <w n="98.3">le</w> <w n="98.4">ciel</w> ; <w n="98.5">la</w> <w n="98.6">Sainteté</w>,</l>
							<l n="99" num="5.3"><w n="99.1">Comme</w> <w n="99.2">en</w> <w n="99.3">un</w> <w n="99.4">lit</w> <w n="99.5">de</w> <w n="99.6">plume</w> <w n="99.7">un</w> <w n="99.8">délicat</w> <w n="99.9">se</w> <w n="99.10">vautre</w>,</l>
							<l n="100" num="5.4"><w n="100.1">Dans</w> <w n="100.2">les</w> <w n="100.3">clous</w> <w n="100.4">et</w> <w n="100.5">le</w> <w n="100.6">crin</w> <w n="100.7">cherchant</w> <w n="100.8">la</w> <w n="100.9">volupté</w> ;</l>
						</lg>
						<lg n="6">
							<l n="101" num="6.1"><w n="101.1">L</w>’<w n="101.2">Humanité</w> <w n="101.3">bavarde</w>, <w n="101.4">ivre</w> <w n="101.5">de</w> <w n="101.6">son</w> <w n="101.7">génie</w>,</l>
							<l n="102" num="6.2"><w n="102.1">Et</w>, <w n="102.2">folle</w> <w n="102.3">maintenant</w> <w n="102.4">comme</w> <w n="102.5">elle</w> <w n="102.6">était</w> <w n="102.7">jadis</w>,</l>
							<l n="103" num="6.3"><w n="103.1">Criant</w> <w n="103.2">à</w> <w n="103.3">Dieu</w>, <w n="103.4">dans</w> <w n="103.5">sa</w> <w n="103.6">furibonde</w> <w n="103.7">agonie</w> :</l>
							<l n="104" num="6.4">« <w n="104.1">Ô</w> <w n="104.2">mon</w> <w n="104.3">semblable</w>, <w n="104.4">ô</w> <w n="104.5">mon</w> <w n="104.6">maître</w>, <w n="104.7">je</w> <w n="104.8">te</w> <w n="104.9">maudis</w> ! »</l>
						</lg>
						<lg n="7">
							<l n="105" num="7.1"><w n="105.1">Et</w> <w n="105.2">les</w> <w n="105.3">moins</w> <w n="105.4">sots</w>, <w n="105.5">hardis</w> <w n="105.6">amants</w> <w n="105.7">de</w> <w n="105.8">la</w> <w n="105.9">Démence</w>,</l>
							<l n="106" num="7.2"><w n="106.1">Fuyant</w> <w n="106.2">le</w> <w n="106.3">grand</w> <w n="106.4">troupeau</w> <w n="106.5">parqué</w> <w n="106.6">par</w> <w n="106.7">le</w> <w n="106.8">Destin</w>,</l>
							<l n="107" num="7.3"><w n="107.1">Et</w> <w n="107.2">se</w> <w n="107.3">réfugiant</w> <w n="107.4">dans</w> <w n="107.5">l</w>’<w n="107.6">opium</w> <w n="107.7">immense</w> !</l>
							<l n="108" num="7.4">— <w n="108.1">Tel</w> <w n="108.2">est</w> <w n="108.3">du</w> <w n="108.4">globe</w> <w n="108.5">entier</w> <w n="108.6">l</w>’<w n="108.7">éternel</w> <w n="108.8">bulletin</w>. »</l>
						</lg>
					</div>
					<div type="section" n="7">
						<head type="number">VII</head>
						<lg n="1">
							<l n="109" num="1.1"><w n="109.1">Amer</w> <w n="109.2">savoir</w>, <w n="109.3">celui</w> <w n="109.4">qu</w>’<w n="109.5">on</w> <w n="109.6">tire</w> <w n="109.7">du</w> <w n="109.8">voyage</w> !</l>
							<l n="110" num="1.2"><w n="110.1">Le</w> <w n="110.2">monde</w>, <w n="110.3">monotone</w> <w n="110.4">et</w> <w n="110.5">petit</w>, <w n="110.6">aujourd</w>’<w n="110.7">hui</w>,</l>
							<l n="111" num="1.3"><w n="111.1">Hier</w>, <w n="111.2">demain</w>, <w n="111.3">toujours</w>, <w n="111.4">nous</w> <w n="111.5">fait</w> <w n="111.6">voir</w> <w n="111.7">notre</w> <w n="111.8">image</w> :</l>
							<l n="112" num="1.4"><w n="112.1">Une</w> <w n="112.2">oasis</w> <w n="112.3">d</w>’<w n="112.4">horreur</w> <w n="112.5">dans</w> <w n="112.6">un</w> <w n="112.7">désert</w> <w n="112.8">d</w>’<w n="112.9">ennui</w> !</l>
						</lg>
						<lg n="2">
							<l n="113" num="2.1"><w n="113.1">Faut</w>-<w n="113.2">il</w> <w n="113.3">partir</w> ? <w n="113.4">rester</w> ? <w n="113.5">Si</w> <w n="113.6">tu</w> <w n="113.7">peux</w> <w n="113.8">rester</w>, <w n="113.9">reste</w> ;</l>
							<l n="114" num="2.2"><w n="114.1">Pars</w>, <w n="114.2">s</w>’<w n="114.3">il</w> <w n="114.4">le</w> <w n="114.5">faut</w>. <w n="114.6">L</w>’<w n="114.7">un</w> <w n="114.8">court</w>, <w n="114.9">et</w> <w n="114.10">l</w>’<w n="114.11">autre</w> <w n="114.12">se</w> <w n="114.13">tapit</w></l>
							<l n="115" num="2.3"><w n="115.1">Pour</w> <w n="115.2">tromper</w> <w n="115.3">l</w>’<w n="115.4">ennemi</w> <w n="115.5">vigilant</w> <w n="115.6">et</w> <w n="115.7">funeste</w>,</l>
							<l n="116" num="2.4"><w n="116.1">Le</w> <w n="116.2">Temps</w> ! <w n="116.3">Il</w> <w n="116.4">est</w>, <w n="116.5">hélas</w> ! <w n="116.6">des</w> <w n="116.7">coureurs</w> <w n="116.8">sans</w> <w n="116.9">répit</w>,</l>
						</lg>
						<lg n="3">
							<l n="117" num="3.1"><w n="117.1">Comme</w> <w n="117.2">le</w> <w n="117.3">Juif</w> <w n="117.4">errant</w> <w n="117.5">et</w> <w n="117.6">comme</w> <w n="117.7">les</w> <w n="117.8">apôtres</w>,</l>
							<l n="118" num="3.2"><w n="118.1">À</w> <w n="118.2">qui</w> <w n="118.3">rien</w> <w n="118.4">ne</w> <w n="118.5">suffit</w>, <w n="118.6">ni</w> <w n="118.7">wagon</w> <w n="118.8">ni</w> <w n="118.9">vaisseau</w>,</l>
							<l n="119" num="3.3"><w n="119.1">Pour</w> <w n="119.2">fuir</w> <w n="119.3">ce</w> <w n="119.4">rétiaire</w> <w n="119.5">infâme</w> ; <w n="119.6">il</w> <w n="119.7">en</w> <w n="119.8">est</w> <w n="119.9">d</w>’<w n="119.10">autres</w></l>
							<l n="120" num="3.4"><w n="120.1">Qui</w> <w n="120.2">savent</w> <w n="120.3">le</w> <w n="120.4">tuer</w> <w n="120.5">sans</w> <w n="120.6">quitter</w> <w n="120.7">leur</w> <w n="120.8">berceau</w>.</l>
						</lg>
						<lg n="4">
							<l n="121" num="4.1"><w n="121.1">Lorsque</w> <w n="121.2">enfin</w> <w n="121.3">il</w> <w n="121.4">mettra</w> <w n="121.5">le</w> <w n="121.6">pied</w> <w n="121.7">sur</w> <w n="121.8">notre</w> <w n="121.9">échine</w>,</l>
							<l n="122" num="4.2"><w n="122.1">Nous</w> <w n="122.2">pourrons</w> <w n="122.3">espérer</w> <w n="122.4">et</w> <w n="122.5">crier</w> : <w n="122.6">En</w> <w n="122.7">avant</w> !</l>
							<l n="123" num="4.3"><w n="123.1">De</w> <w n="123.2">même</w> <w n="123.3">qu</w>’<w n="123.4">autrefois</w> <w n="123.5">nous</w> <w n="123.6">partions</w> <w n="123.7">pour</w> <w n="123.8">la</w> <w n="123.9">Chine</w>,</l>
							<l n="124" num="4.4"><w n="124.1">Les</w> <w n="124.2">yeux</w> <w n="124.3">fixés</w> <w n="124.4">au</w> <w n="124.5">large</w> <w n="124.6">et</w> <w n="124.7">les</w> <w n="124.8">cheveux</w> <w n="124.9">au</w> <w n="124.10">vent</w>,</l>
						</lg>
						<lg n="5">
							<l n="125" num="5.1"><w n="125.1">Nous</w> <w n="125.2">nous</w> <w n="125.3">embarquerons</w> <w n="125.4">sur</w> <w n="125.5">la</w> <w n="125.6">mer</w> <w n="125.7">des</w> <w n="125.8">Ténèbres</w></l>
							<l n="126" num="5.2"><w n="126.1">Avec</w> <w n="126.2">le</w> <w n="126.3">cœur</w> <w n="126.4">joyeux</w> <w n="126.5">d</w>’<w n="126.6">un</w> <w n="126.7">jeune</w> <w n="126.8">passager</w>.</l>
							<l n="127" num="5.3"><w n="127.1">Entendez</w>-<w n="127.2">vous</w> <w n="127.3">ces</w> <w n="127.4">voix</w>, <w n="127.5">charmantes</w> <w n="127.6">et</w> <w n="127.7">funèbres</w>,</l>
							<l n="128" num="5.4"><w n="128.1">Qui</w> <w n="128.2">chantent</w> : « <w n="128.3">Par</w> <w n="128.4">ici</w> ! <w n="128.5">vous</w> <w n="128.6">qui</w> <w n="128.7">voulez</w> <w n="128.8">manger</w></l>
						</lg>
						<lg n="6">
							<l n="129" num="6.1"><w n="129.1">Le</w> <w n="129.2">Lotus</w> <w n="129.3">parfumé</w> ! <w n="129.4">c</w>’<w n="129.5">est</w> <w n="129.6">ici</w> <w n="129.7">qu</w>’<w n="129.8">on</w> <w n="129.9">vendange</w></l>
							<l n="130" num="6.2"><w n="130.1">Les</w> <w n="130.2">fruits</w> <w n="130.3">miraculeux</w> <w n="130.4">dont</w> <w n="130.5">votre</w> <w n="130.6">cœur</w> <w n="130.7">a</w> <w n="130.8">faim</w> ;</l>
							<l n="131" num="6.3"><w n="131.1">Venez</w> <w n="131.2">vous</w> <w n="131.3">enivrer</w> <w n="131.4">de</w> <w n="131.5">la</w> <w n="131.6">douceur</w> <w n="131.7">étrange</w></l>
							<l n="132" num="6.4"><w n="132.1">De</w> <w n="132.2">cette</w> <w n="132.3">après</w>-<w n="132.4">midi</w> <w n="132.5">qui</w> <w n="132.6">n</w>’<w n="132.7">a</w> <w n="132.8">jamais</w> <w n="132.9">de</w> <w n="132.10">fin</w> ? »</l>
						</lg>
						<lg n="7">
							<l n="133" num="7.1"><w n="133.1">À</w> <w n="133.2">l</w>’<w n="133.3">accent</w> <w n="133.4">familier</w> <w n="133.5">nous</w> <w n="133.6">devinons</w> <w n="133.7">le</w> <w n="133.8">spectre</w> ;</l>
							<l n="134" num="7.2"><w n="134.1">Nos</w> <w n="134.2">Pylades</w> <w n="134.3">là</w>-<w n="134.4">bas</w> <w n="134.5">tendent</w> <w n="134.6">leurs</w> <w n="134.7">bras</w> <w n="134.8">vers</w> <w n="134.9">nous</w>.</l>
							<l n="135" num="7.3">« <w n="135.1">Pour</w> <w n="135.2">rafraîchir</w> <w n="135.3">ton</w> <w n="135.4">cœur</w> <w n="135.5">nage</w> <w n="135.6">vers</w> <w n="135.7">ton</w> <w n="135.8">Électre</w> ! »</l>
							<l n="136" num="7.4"><w n="136.1">Dit</w> <w n="136.2">celle</w> <w n="136.3">dont</w> <w n="136.4">jadis</w> <w n="136.5">nous</w> <w n="136.6">baisions</w> <w n="136.7">les</w> <w n="136.8">genoux</w>.</l>
						</lg>
					</div>
					<div type="section" n="8">
						<head type="number">VIII</head>
						<lg n="1">
							<l n="137" num="1.1"><w n="137.1">Ô</w> <w n="137.2">Mort</w>, <w n="137.3">vieux</w> <w n="137.4">capitaine</w>, <w n="137.5">il</w> <w n="137.6">est</w> <w n="137.7">temps</w> ! <w n="137.8">levons</w> <w n="137.9">l</w>’<w n="137.10">ancre</w> !</l>
							<l n="138" num="1.2"><w n="138.1">Ce</w> <w n="138.2">pays</w> <w n="138.3">nous</w> <w n="138.4">ennuie</w>, <w n="138.5">ô</w> <w n="138.6">Mort</w> ! <w n="138.7">Appareillons</w> !</l>
							<l n="139" num="1.3"><w n="139.1">Si</w> <w n="139.2">le</w> <w n="139.3">ciel</w> <w n="139.4">et</w> <w n="139.5">la</w> <w n="139.6">mer</w> <w n="139.7">sont</w> <w n="139.8">noirs</w> <w n="139.9">comme</w> <w n="139.10">de</w> <w n="139.11">l</w>’<w n="139.12">encre</w>,</l>
							<l n="140" num="1.4"><w n="140.1">Nos</w> <w n="140.2">cœurs</w> <w n="140.3">que</w> <w n="140.4">tu</w> <w n="140.5">connais</w> <w n="140.6">sont</w> <w n="140.7">remplis</w> <w n="140.8">de</w> <w n="140.9">rayons</w> !</l>
						</lg>
						<lg n="2">
							<l n="141" num="2.1"><w n="141.1">Verse</w>-<w n="141.2">nous</w> <w n="141.3">ton</w> <w n="141.4">poison</w> <w n="141.5">pour</w> <w n="141.6">qu</w>’<w n="141.7">il</w> <w n="141.8">nous</w> <w n="141.9">réconforte</w> !</l>
							<l n="142" num="2.2"><w n="142.1">Nous</w> <w n="142.2">voulons</w>, <w n="142.3">tant</w> <w n="142.4">ce</w> <w n="142.5">feu</w> <w n="142.6">nous</w> <w n="142.7">brûle</w> <w n="142.8">le</w> <w n="142.9">cerveau</w>,</l>
							<l n="143" num="2.3"><w n="143.1">Plonger</w> <w n="143.2">au</w> <w n="143.3">fond</w> <w n="143.4">du</w> <w n="143.5">gouffre</w>, <w n="143.6">Enfer</w> <w n="143.7">ou</w> <w n="143.8">Ciel</w>, <w n="143.9">qu</w>’<w n="143.10">importe</w> ?</l>
							<l n="144" num="2.4"><w n="144.1">Au</w> <w n="144.2">fond</w> <w n="144.3">de</w> <w n="144.4">l</w>’<w n="144.5">Inconnu</w> <w n="144.6">pour</w> <w n="144.7">trouver</w> <w n="144.8">du</w> <w n="144.9">nouveau</w> !</l>
						</lg>
					</div>
				</div></body></text></TEI>