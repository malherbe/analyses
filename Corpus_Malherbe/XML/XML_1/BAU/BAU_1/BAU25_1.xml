<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU25">
					<head type="number">XXIV</head>
					<head type="main">La Chevelure</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ô</w> <w n="1.2">toison</w>, <w n="1.3">moutonnant</w> <w n="1.4">jusque</w> <w n="1.5">sur</w> <w n="1.6">l</w>’<w n="1.7">encolure</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Ô</w> <w n="2.2">boucles</w> ! <w n="2.3">Ô</w> <w n="2.4">parfum</w> <w n="2.5">chargé</w> <w n="2.6">de</w> <w n="2.7">nonchaloir</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Extase</w> ! <w n="3.2">Pour</w> <w n="3.3">peupler</w> <w n="3.4">ce</w> <w n="3.5">soir</w> <w n="3.6">l</w>’<w n="3.7">alcôve</w> <w n="3.8">obscure</w></l>
						<l n="4" num="1.4"><w n="4.1">Des</w> <w n="4.2">souvenirs</w> <w n="4.3">dormant</w> <w n="4.4">dans</w> <w n="4.5">cette</w> <w n="4.6">chevelure</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">la</w> <w n="5.3">veux</w> <w n="5.4">agiter</w> <w n="5.5">dans</w> <w n="5.6">l</w>’<w n="5.7">air</w> <w n="5.8">comme</w> <w n="5.9">un</w> <w n="5.10">mouchoir</w> !</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">La</w> <w n="6.2">langoureuse</w> <w n="6.3">Asie</w> <w n="6.4">et</w> <w n="6.5">la</w> <w n="6.6">brûlante</w> <w n="6.7">Afrique</w>,</l>
						<l n="7" num="2.2"><w n="7.1">Tout</w> <w n="7.2">un</w> <w n="7.3">monde</w> <w n="7.4">lointain</w>, <w n="7.5">absent</w>, <w n="7.6">presque</w> <w n="7.7">défunt</w>,</l>
						<l n="8" num="2.3"><w n="8.1">Vit</w> <w n="8.2">dans</w> <w n="8.3">tes</w> <w n="8.4">profondeurs</w>, <w n="8.5">forêt</w> <w n="8.6">aromatique</w> !</l>
						<l n="9" num="2.4"><w n="9.1">Comme</w> <w n="9.2">d</w>’<w n="9.3">autres</w> <w n="9.4">esprits</w> <w n="9.5">voguent</w> <w n="9.6">sur</w> <w n="9.7">la</w> <w n="9.8">musique</w>,</l>
						<l n="10" num="2.5"><w n="10.1">Le</w> <w n="10.2">mien</w>, <w n="10.3">ô</w> <w n="10.4">mon</w> <w n="10.5">amour</w> ! <w n="10.6">nage</w> <w n="10.7">sur</w> <w n="10.8">ton</w> <w n="10.9">parfum</w>.</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">J</w>’<w n="11.2">irai</w> <w n="11.3">là</w>-<w n="11.4">bas</w> <w n="11.5">où</w> <w n="11.6">l</w>’<w n="11.7">arbre</w> <w n="11.8">et</w> <w n="11.9">l</w>’<w n="11.10">homme</w>, <w n="11.11">pleins</w> <w n="11.12">de</w> <w n="11.13">sève</w>,</l>
						<l n="12" num="3.2"><w n="12.1">Se</w> <w n="12.2">pâment</w> <w n="12.3">longuement</w> <w n="12.4">sous</w> <w n="12.5">l</w>’<w n="12.6">ardeur</w> <w n="12.7">des</w> <w n="12.8">climats</w> ;</l>
						<l n="13" num="3.3"><w n="13.1">Fortes</w> <w n="13.2">tresses</w>, <w n="13.3">soyez</w> <w n="13.4">la</w> <w n="13.5">houle</w> <w n="13.6">qui</w> <w n="13.7">m</w>’<w n="13.8">enlève</w> !</l>
						<l n="14" num="3.4"><w n="14.1">Tu</w> <w n="14.2">contiens</w>, <w n="14.3">mer</w> <w n="14.4">d</w>’<w n="14.5">ébène</w>, <w n="14.6">un</w> <w n="14.7">éblouissant</w> <w n="14.8">rêve</w></l>
						<l n="15" num="3.5"><w n="15.1">De</w> <w n="15.2">voiles</w>, <w n="15.3">de</w> <w n="15.4">rameurs</w>, <w n="15.5">de</w> <w n="15.6">flammes</w> <w n="15.7">et</w> <w n="15.8">de</w> <w n="15.9">mâts</w> :</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">Un</w> <w n="16.2">port</w> <w n="16.3">retentissant</w> <w n="16.4">où</w> <w n="16.5">mon</w> <w n="16.6">âme</w> <w n="16.7">peut</w> <w n="16.8">boire</w></l>
						<l n="17" num="4.2"><w n="17.1">À</w> <w n="17.2">grands</w> <w n="17.3">flots</w> <w n="17.4">le</w> <w n="17.5">parfum</w>, <w n="17.6">le</w> <w n="17.7">son</w> <w n="17.8">et</w> <w n="17.9">la</w> <w n="17.10">couleur</w> ;</l>
						<l n="18" num="4.3"><w n="18.1">Où</w> <w n="18.2">les</w> <w n="18.3">vaisseaux</w>, <w n="18.4">glissant</w> <w n="18.5">dans</w> <w n="18.6">l</w>’<w n="18.7">or</w> <w n="18.8">et</w> <w n="18.9">dans</w> <w n="18.10">la</w> <w n="18.11">moire</w>,</l>
						<l n="19" num="4.4"><w n="19.1">Ouvrent</w> <w n="19.2">leurs</w> <w n="19.3">vastes</w> <w n="19.4">bras</w> <w n="19.5">pour</w> <w n="19.6">embrasser</w> <w n="19.7">la</w> <w n="19.8">gloire</w></l>
						<l n="20" num="4.5"><w n="20.1">D</w>’<w n="20.2">un</w> <w n="20.3">ciel</w> <w n="20.4">pur</w> <w n="20.5">où</w> <w n="20.6">frémit</w> <w n="20.7">l</w>’<w n="20.8">éternelle</w> <w n="20.9">chaleur</w>.</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">Je</w> <w n="21.2">plongerai</w> <w n="21.3">ma</w> <w n="21.4">tête</w> <w n="21.5">amoureuse</w> <w n="21.6">d</w>’<w n="21.7">ivresse</w></l>
						<l n="22" num="5.2"><w n="22.1">Dans</w> <w n="22.2">ce</w> <w n="22.3">noir</w> <w n="22.4">océan</w> <w n="22.5">où</w> <w n="22.6">l</w>’<w n="22.7">autre</w> <w n="22.8">est</w> <w n="22.9">enfermé</w> ;</l>
						<l n="23" num="5.3"><w n="23.1">Et</w> <w n="23.2">mon</w> <w n="23.3">esprit</w> <w n="23.4">subtil</w> <w n="23.5">que</w> <w n="23.6">le</w> <w n="23.7">roulis</w> <w n="23.8">caresse</w></l>
						<l n="24" num="5.4"><w n="24.1">Saura</w> <w n="24.2">vous</w> <w n="24.3">retrouver</w>, <w n="24.4">ô</w> <w n="24.5">féconde</w> <w n="24.6">paresse</w>,</l>
						<l n="25" num="5.5"><w n="25.1">Infinis</w> <w n="25.2">bercements</w> <w n="25.3">du</w> <w n="25.4">loisir</w> <w n="25.5">embaumé</w> !</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1"><w n="26.1">Cheveux</w> <w n="26.2">bleus</w>, <w n="26.3">pavillon</w> <w n="26.4">de</w> <w n="26.5">ténèbres</w> <w n="26.6">tendues</w>,</l>
						<l n="27" num="6.2"><w n="27.1">Vous</w> <w n="27.2">me</w> <w n="27.3">rendez</w> <w n="27.4">l</w>’<w n="27.5">azur</w> <w n="27.6">du</w> <w n="27.7">ciel</w> <w n="27.8">immense</w> <w n="27.9">et</w> <w n="27.10">rond</w> ;</l>
						<l n="28" num="6.3"><w n="28.1">Sur</w> <w n="28.2">les</w> <w n="28.3">bords</w> <w n="28.4">duvetés</w> <w n="28.5">de</w> <w n="28.6">vos</w> <w n="28.7">mèches</w> <w n="28.8">tordues</w></l>
						<l n="29" num="6.4"><w n="29.1">Je</w> <w n="29.2">m</w>’<w n="29.3">enivre</w> <w n="29.4">ardemment</w> <w n="29.5">des</w> <w n="29.6">senteurs</w> <w n="29.7">confondues</w></l>
						<l n="30" num="6.5"><w n="30.1">De</w> <w n="30.2">l</w>’<w n="30.3">huile</w> <w n="30.4">de</w> <w n="30.5">coco</w>, <w n="30.6">du</w> <w n="30.7">musc</w> <w n="30.8">et</w> <w n="30.9">du</w> <w n="30.10">goudron</w>.</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><w n="31.1">Longtemps</w> ! <w n="31.2">toujours</w> ! <w n="31.3">ma</w> <w n="31.4">main</w> <w n="31.5">dans</w> <w n="31.6">ta</w> <w n="31.7">crinière</w> <w n="31.8">lourde</w></l>
						<l n="32" num="7.2"><w n="32.1">Sèmera</w> <w n="32.2">le</w> <w n="32.3">rubis</w>, <w n="32.4">la</w> <w n="32.5">perle</w> <w n="32.6">et</w> <w n="32.7">le</w> <w n="32.8">saphir</w>,</l>
						<l n="33" num="7.3"><w n="33.1">Afin</w> <w n="33.2">qu</w>’<w n="33.3">à</w> <w n="33.4">mon</w> <w n="33.5">désir</w> <w n="33.6">tu</w> <w n="33.7">ne</w> <w n="33.8">sois</w> <w n="33.9">jamais</w> <w n="33.10">sourde</w> !</l>
						<l n="34" num="7.4"><w n="34.1">N</w>’<w n="34.2">es</w>-<w n="34.3">tu</w> <w n="34.4">pas</w> <w n="34.5">l</w>’<w n="34.6">oasis</w> <w n="34.7">où</w> <w n="34.8">je</w> <w n="34.9">rêve</w>, <w n="34.10">et</w> <w n="34.11">la</w> <w n="34.12">gourde</w></l>
						<l n="35" num="7.5"><w n="35.1">Où</w> <w n="35.2">je</w> <w n="35.3">hume</w> <w n="35.4">à</w> <w n="35.5">longs</w> <w n="35.6">traits</w> <w n="35.7">le</w> <w n="35.8">vin</w> <w n="35.9">du</w> <w n="35.10">souvenir</w> ?</l>
					</lg>
				</div></body></text></TEI>