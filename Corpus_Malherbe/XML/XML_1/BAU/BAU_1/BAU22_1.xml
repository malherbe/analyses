<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU22">
					<head type="number">XXI</head>
					<head type="main">Le Masque</head>
					<head type="sub_1">Statue allégorique dans le goût de la Renaissance</head>
					<opener><salute><name>À Ernest Christophe</name>, statuaire.</salute></opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Contemplons</w> <w n="1.2">ce</w> <w n="1.3">trésor</w> <w n="1.4">de</w> <w n="1.5">grâces</w> <w n="1.6">florentines</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">l</w>’<w n="2.3">ondulation</w> <w n="2.4">de</w> <w n="2.5">ce</w> <w n="2.6">corps</w> <w n="2.7">musculeux</w></l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">Élégance</w> <w n="3.3">et</w> <w n="3.4">la</w> <w n="3.5">Force</w> <w n="3.6">abondent</w>, <w n="3.7">sœurs</w> <w n="3.8">divines</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Cette</w> <w n="4.2">femme</w>, <w n="4.3">morceau</w> <w n="4.4">vraiment</w> <w n="4.5">miraculeux</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Divinement</w> <w n="5.2">robuste</w>, <w n="5.3">adorablement</w> <w n="5.4">mince</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Est</w> <w n="6.2">faite</w> <w n="6.3">pour</w> <w n="6.4">trôner</w> <w n="6.5">sur</w> <w n="6.6">des</w> <w n="6.7">lits</w> <w n="6.8">somptueux</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">charmer</w> <w n="7.3">les</w> <w n="7.4">loisirs</w> <w n="7.5">d</w>’<w n="7.6">un</w> <w n="7.7">pontife</w> <w n="7.8">ou</w> <w n="7.9">d</w>’<w n="7.10">un</w> <w n="7.11">prince</w>.</l>
					</lg>
					<lg n="2">
						<l n="8" num="2.1">— <w n="8.1">Aussi</w>, <w n="8.2">vois</w> <w n="8.3">ce</w> <w n="8.4">souris</w> <w n="8.5">fin</w> <w n="8.6">et</w> <w n="8.7">voluptueux</w></l>
						<l n="9" num="2.2"><w n="9.1">Où</w> <w n="9.2">la</w> <w n="9.3">Fatuité</w> <w n="9.4">promène</w> <w n="9.5">son</w> <w n="9.6">extase</w> ;</l>
						<l n="10" num="2.3"><w n="10.1">Ce</w> <w n="10.2">long</w> <w n="10.3">regard</w> <w n="10.4">sournois</w>, <w n="10.5">langoureux</w> <w n="10.6">et</w> <w n="10.7">moqueur</w> ;</l>
						<l n="11" num="2.4"><w n="11.1">Ce</w> <w n="11.2">visage</w> <w n="11.3">mignard</w>, <w n="11.4">tout</w> <w n="11.5">encadré</w> <w n="11.6">de</w> <w n="11.7">gaze</w>,</l>
						<l n="12" num="2.5"><w n="12.1">Dont</w> <w n="12.2">chaque</w> <w n="12.3">trait</w> <w n="12.4">nous</w> <w n="12.5">dit</w> <w n="12.6">avec</w> <w n="12.7">un</w> <w n="12.8">air</w> <w n="12.9">vainqueur</w> :</l>
						<l n="13" num="2.6">« <w n="13.1">La</w> <w n="13.2">Volupté</w> <w n="13.3">m</w>’<w n="13.4">appelle</w> <w n="13.5">et</w> <w n="13.6">l</w>’<w n="13.7">Amour</w> <w n="13.8">me</w> <w n="13.9">couronne</w> ! »</l>
						<l n="14" num="2.7"><w n="14.1">À</w> <w n="14.2">cet</w> <w n="14.3">être</w> <w n="14.4">doué</w> <w n="14.5">de</w> <w n="14.6">tant</w> <w n="14.7">de</w> <w n="14.8">majesté</w></l>
						<l n="15" num="2.8"><w n="15.1">Vois</w> <w n="15.2">quel</w> <w n="15.3">charme</w> <w n="15.4">excitant</w> <w n="15.5">la</w> <w n="15.6">gentillesse</w> <w n="15.7">donne</w> !</l>
						<l n="16" num="2.9"><w n="16.1">Approchons</w>, <w n="16.2">et</w> <w n="16.3">tournons</w> <w n="16.4">autour</w> <w n="16.5">de</w> <w n="16.6">sa</w> <w n="16.7">beauté</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">O</w> <w n="17.2">blasphème</w> <w n="17.3">de</w> <w n="17.4">l</w>’<w n="17.5">art</w> ! <w n="17.6">ô</w> <w n="17.7">surprise</w> <w n="17.8">fatale</w> !</l>
						<l n="18" num="3.2"><w n="18.1">La</w> <w n="18.2">femme</w> <w n="18.3">au</w> <w n="18.4">corps</w> <w n="18.5">divin</w>, <w n="18.6">promettant</w> <w n="18.7">le</w> <w n="18.8">bonheur</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Par</w> <w n="19.2">le</w> <w n="19.3">haut</w> <w n="19.4">se</w> <w n="19.5">termine</w> <w n="19.6">en</w> <w n="19.7">monstre</w> <w n="19.8">bicéphale</w> !</l>
					</lg>
					<lg n="4">
						<l n="20" num="4.1"><w n="20.1">Mais</w> <w n="20.2">non</w> ! <w n="20.3">Ce</w> <w n="20.4">n</w>’<w n="20.5">est</w> <w n="20.6">qu</w>’<w n="20.7">un</w> <w n="20.8">masque</w>, <w n="20.9">un</w> <w n="20.10">décor</w> <w n="20.11">suborneur</w>,</l>
						<l n="21" num="4.2"><w n="21.1">Ce</w> <w n="21.2">visage</w> <w n="21.3">éclairé</w> <w n="21.4">d</w>’<w n="21.5">une</w> <w n="21.6">exquise</w> <w n="21.7">grimace</w>,</l>
						<l n="22" num="4.3"><w n="22.1">Et</w>, <w n="22.2">regarde</w>, <w n="22.3">voici</w>, <w n="22.4">crispée</w> <w n="22.5">atrocement</w>,</l>
						<l n="23" num="4.4"><w n="23.1">La</w> <w n="23.2">véritable</w> <w n="23.3">tête</w>, <w n="23.4">et</w> <w n="23.5">la</w> <w n="23.6">sincère</w> <w n="23.7">face</w></l>
						<l n="24" num="4.5"><w n="24.1">Renversée</w> <w n="24.2">à</w> <w n="24.3">l</w>’<w n="24.4">abri</w> <w n="24.5">de</w> <w n="24.6">la</w> <w n="24.7">face</w> <w n="24.8">qui</w> <w n="24.9">ment</w>.</l>
						<l n="25" num="4.6">— <w n="25.1">Pauvre</w> <w n="25.2">grande</w> <w n="25.3">beauté</w> ! <w n="25.4">le</w> <w n="25.5">magnifique</w> <w n="25.6">fleuve</w></l>
						<l n="26" num="4.7"><w n="26.1">De</w> <w n="26.2">tes</w> <w n="26.3">pleurs</w> <w n="26.4">aboutit</w> <w n="26.5">dans</w> <w n="26.6">mon</w> <w n="26.7">cœur</w> <w n="26.8">soucieux</w> ;</l>
						<l n="27" num="4.8"><w n="27.1">Ton</w> <w n="27.2">mensonge</w> <w n="27.3">m</w>’<w n="27.4">enivre</w>, <w n="27.5">et</w> <w n="27.6">mon</w> <w n="27.7">âme</w> <w n="27.8">s</w>’<w n="27.9">abreuve</w></l>
						<l n="28" num="4.9"><w n="28.1">Aux</w> <w n="28.2">flots</w> <w n="28.3">que</w> <w n="28.4">la</w> <w n="28.5">Douleur</w> <w n="28.6">fait</w> <w n="28.7">jaillir</w> <w n="28.8">de</w> <w n="28.9">tes</w> <w n="28.10">yeux</w> !</l>
					</lg>
					<lg n="5">
						<l n="29" num="5.1">— <w n="29.1">Mais</w> <w n="29.2">pourquoi</w> <w n="29.3">pleure</w>-<w n="29.4">t</w>-<w n="29.5">elle</w> ? <w n="29.6">Elle</w>, <w n="29.7">beauté</w> <w n="29.8">parfaite</w></l>
						<l n="30" num="5.2"><w n="30.1">Qui</w> <w n="30.2">mettrait</w> <w n="30.3">à</w> <w n="30.4">ses</w> <w n="30.5">pieds</w> <w n="30.6">le</w> <w n="30.7">genre</w> <w n="30.8">humain</w> <w n="30.9">vaincu</w>,</l>
						<l n="31" num="5.3"><w n="31.1">Quel</w> <w n="31.2">mal</w> <w n="31.3">mystérieux</w> <w n="31.4">ronge</w> <w n="31.5">son</w> <w n="31.6">flanc</w> <w n="31.7">d</w>’<w n="31.8">athlète</w> ?</l>
					</lg>
					<lg n="6">
						<l n="32" num="6.1">— <w n="32.1">Elle</w> <w n="32.2">pleure</w>, <w n="32.3">insensé</w>, <w n="32.4">parce</w> <w n="32.5">qu</w>’<w n="32.6">elle</w> <w n="32.7">a</w> <w n="32.8">vécu</w> !</l>
						<l n="33" num="6.2"><w n="33.1">Et</w> <w n="33.2">parce</w> <w n="33.3">qu</w>’<w n="33.4">elle</w> <w n="33.5">vit</w> ! <w n="33.6">Mais</w> <w n="33.7">ce</w> <w n="33.8">qu</w>’<w n="33.9">elle</w> <w n="33.10">déplore</w></l>
						<l n="34" num="6.3"><w n="34.1">Surtout</w>, <w n="34.2">ce</w> <w n="34.3">qui</w> <w n="34.4">la</w> <w n="34.5">fait</w> <w n="34.6">frémir</w> <w n="34.7">jusqu</w>’<w n="34.8">aux</w> <w n="34.9">genoux</w>,</l>
						<l n="35" num="6.4"><w n="35.1">C</w>’<w n="35.2">est</w> <w n="35.3">que</w> <w n="35.4">demain</w>, <w n="35.5">hélas</w> ! <w n="35.6">il</w> <w n="35.7">faudra</w> <w n="35.8">vivre</w> <w n="35.9">encore</w> !</l>
						<l n="36" num="6.5"><w n="36.1">Demain</w>, <w n="36.2">après</w>-<w n="36.3">demain</w> <w n="36.4">et</w> <w n="36.5">toujours</w> ! — <w n="36.6">comme</w> <w n="36.7">nous</w> !</l>
					</lg>
				</div></body></text></TEI>