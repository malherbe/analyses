<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">FLEURS DU MAL</head><div type="poem" key="BAU118">
					<head type="number">CXIV</head>
					<head type="main">Lesbos</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Mère</w> <w n="1.2">des</w> <w n="1.3">jeux</w> <w n="1.4">latins</w> <w n="1.5">et</w> <w n="1.6">des</w> <w n="1.7">voluptés</w> <w n="1.8">grecques</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Lesbos</w>, <w n="2.2">où</w> <w n="2.3">les</w> <w n="2.4">baisers</w>, <w n="2.5">languissants</w> <w n="2.6">ou</w> <w n="2.7">joyeux</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Chauds</w> <w n="3.2">comme</w> <w n="3.3">les</w> <w n="3.4">soleils</w>, <w n="3.5">frais</w> <w n="3.6">comme</w> <w n="3.7">les</w> <w n="3.8">pastèques</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Font</w> <w n="4.2">l</w>’<w n="4.3">ornement</w> <w n="4.4">des</w> <w n="4.5">nuits</w> <w n="4.6">et</w> <w n="4.7">des</w> <w n="4.8">jours</w> <w n="4.9">glorieux</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Mère</w> <w n="5.2">des</w> <w n="5.3">jeux</w> <w n="5.4">latins</w> <w n="5.5">et</w> <w n="5.6">des</w> <w n="5.7">voluptés</w> <w n="5.8">grecques</w>,</l>
					</lg>
					<lg n="2">
						<l n="6" num="2.1"><w n="6.1">Lesbos</w>, <w n="6.2">où</w> <w n="6.3">les</w> <w n="6.4">baisers</w> <w n="6.5">sont</w> <w n="6.6">comme</w> <w n="6.7">les</w> <w n="6.8">cascades</w></l>
						<l n="7" num="2.2"><w n="7.1">Qui</w> <w n="7.2">se</w> <w n="7.3">jettent</w> <w n="7.4">sans</w> <w n="7.5">peur</w> <w n="7.6">dans</w> <w n="7.7">les</w> <w n="7.8">gouffres</w> <w n="7.9">sans</w> <w n="7.10">fonds</w></l>
						<l n="8" num="2.3"><w n="8.1">Et</w> <w n="8.2">courent</w>, <w n="8.3">sanglotant</w> <w n="8.4">et</w> <w n="8.5">gloussant</w> <w n="8.6">par</w> <w n="8.7">saccades</w>,</l>
						<l n="9" num="2.4"><w n="9.1">Orageux</w> <w n="9.2">et</w> <w n="9.3">secrets</w>, <w n="9.4">fourmillants</w> <w n="9.5">et</w> <w n="9.6">profonds</w> ;</l>
						<l n="10" num="2.5"><w n="10.1">Lesbos</w>, <w n="10.2">où</w> <w n="10.3">les</w> <w n="10.4">baisers</w> <w n="10.5">sont</w> <w n="10.6">comme</w> <w n="10.7">les</w> <w n="10.8">cascades</w> !</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">Lesbos</w>, <w n="11.2">où</w> <w n="11.3">les</w> <w n="11.4">Phrynés</w> <w n="11.5">l</w>’<w n="11.6">une</w> <w n="11.7">l</w>’<w n="11.8">autre</w> <w n="11.9">s</w>’<w n="11.10">attirent</w>,</l>
						<l n="12" num="3.2"><w n="12.1">Où</w> <w n="12.2">jamais</w> <w n="12.3">un</w> <w n="12.4">soupir</w> <w n="12.5">ne</w> <w n="12.6">resta</w> <w n="12.7">sans</w> <w n="12.8">écho</w>,</l>
						<l n="13" num="3.3"><w n="13.1">A</w> <w n="13.2">l</w>’<w n="13.3">égal</w> <w n="13.4">de</w> <w n="13.5">Paphos</w> <w n="13.6">les</w> <w n="13.7">étoiles</w> <w n="13.8">t</w>’<w n="13.9">admirent</w>,</l>
						<l n="14" num="3.4"><w n="14.1">Et</w> <w n="14.2">Vénus</w> <w n="14.3">à</w> <w n="14.4">bon</w> <w n="14.5">droit</w> <w n="14.6">peut</w> <w n="14.7">jalouser</w> <w n="14.8">Sapho</w> !</l>
						<l n="15" num="3.5"><w n="15.1">Lesbos</w>, <w n="15.2">où</w> <w n="15.3">les</w> <w n="15.4">Phrynés</w> <w n="15.5">l</w>’<w n="15.6">une</w> <w n="15.7">l</w>’<w n="15.8">autre</w> <w n="15.9">s</w>’<w n="15.10">attirent</w>,</l>
					</lg>
					<lg n="4">
						<l n="16" num="4.1"><w n="16.1">Lesbos</w>, <w n="16.2">terre</w> <w n="16.3">des</w> <w n="16.4">nuits</w> <w n="16.5">chaudes</w> <w n="16.6">et</w> <w n="16.7">langoureuses</w>,</l>
						<l n="17" num="4.2"><w n="17.1">Qui</w> <w n="17.2">font</w> <w n="17.3">qu</w>’<w n="17.4">à</w> <w n="17.5">leurs</w> <w n="17.6">miroirs</w>, <w n="17.7">stérile</w> <w n="17.8">volupté</w> !</l>
						<l n="18" num="4.3"><w n="18.1">Les</w> <w n="18.2">filles</w> <w n="18.3">aux</w> <w n="18.4">yeux</w> <w n="18.5">creux</w>, <w n="18.6">de</w> <w n="18.7">leur</w> <w n="18.8">corps</w> <w n="18.9">amoureuses</w>,</l>
						<l n="19" num="4.4"><w n="19.1">Caressent</w> <w n="19.2">les</w> <w n="19.3">fruits</w> <w n="19.4">mûrs</w> <w n="19.5">de</w> <w n="19.6">leur</w> <w n="19.7">nubilité</w> ;</l>
						<l n="20" num="4.5"><w n="20.1">Lesbos</w>, <w n="20.2">terre</w> <w n="20.3">des</w> <w n="20.4">nuits</w> <w n="20.5">chaudes</w> <w n="20.6">et</w> <w n="20.7">langoureuses</w>,</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">Laisse</w> <w n="21.2">du</w> <w n="21.3">vieux</w> <w n="21.4">Platon</w> <w n="21.5">se</w> <w n="21.6">froncer</w> <w n="21.7">l</w>’<w n="21.8">œil</w> <w n="21.9">austère</w> ;</l>
						<l n="22" num="5.2"><w n="22.1">Tu</w> <w n="22.2">tires</w> <w n="22.3">ton</w> <w n="22.4">pardon</w> <w n="22.5">de</w> <w n="22.6">l</w>’<w n="22.7">excès</w> <w n="22.8">des</w> <w n="22.9">baisers</w>,</l>
						<l n="23" num="5.3"><w n="23.1">Reine</w> <w n="23.2">du</w> <w n="23.3">doux</w> <w n="23.4">empire</w>, <w n="23.5">aimable</w> <w n="23.6">et</w> <w n="23.7">noble</w> <w n="23.8">terre</w>,</l>
						<l n="24" num="5.4"><w n="24.1">Et</w> <w n="24.2">des</w> <w n="24.3">raffinements</w> <w n="24.4">toujours</w> <w n="24.5">inépuisés</w>.</l>
						<l n="25" num="5.5"><w n="25.1">Laisse</w> <w n="25.2">du</w> <w n="25.3">vieux</w> <w n="25.4">Platon</w> <w n="25.5">se</w> <w n="25.6">froncer</w> <w n="25.7">l</w>’<w n="25.8">œil</w> <w n="25.9">austère</w>.</l>
					</lg>
					<lg n="6">
						<l n="26" num="6.1"><w n="26.1">Tu</w> <w n="26.2">tires</w> <w n="26.3">ton</w> <w n="26.4">pardon</w> <w n="26.5">de</w> <w n="26.6">l</w>’<w n="26.7">éternel</w> <w n="26.8">martyre</w>,</l>
						<l n="27" num="6.2"><w n="27.1">Infligé</w> <w n="27.2">sans</w> <w n="27.3">relâche</w> <w n="27.4">aux</w> <w n="27.5">cœurs</w> <w n="27.6">ambitieux</w>,</l>
						<l n="28" num="6.3"><w n="28.1">Qu</w>’<w n="28.2">attire</w> <w n="28.3">loin</w> <w n="28.4">de</w> <w n="28.5">nous</w> <w n="28.6">le</w> <w n="28.7">radieux</w> <w n="28.8">sourire</w></l>
						<l n="29" num="6.4"><w n="29.1">Entrevu</w> <w n="29.2">vaguement</w> <w n="29.3">au</w> <w n="29.4">bord</w> <w n="29.5">des</w> <w n="29.6">autres</w> <w n="29.7">cieux</w> !</l>
						<l n="30" num="6.5"><w n="30.1">Tu</w> <w n="30.2">tires</w> <w n="30.3">ton</w> <w n="30.4">pardon</w> <w n="30.5">de</w> <w n="30.6">l</w>’<w n="30.7">éternel</w> <w n="30.8">martyre</w> !</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><w n="31.1">Qui</w> <w n="31.2">des</w> <w n="31.3">Dieux</w> <w n="31.4">osera</w>, <w n="31.5">Lesbos</w>, <w n="31.6">être</w> <w n="31.7">ton</w> <w n="31.8">juge</w></l>
						<l n="32" num="7.2"><w n="32.1">Et</w> <w n="32.2">condamner</w> <w n="32.3">ton</w> <w n="32.4">front</w> <w n="32.5">pâli</w> <w n="32.6">dans</w> <w n="32.7">les</w> <w n="32.8">travaux</w>,</l>
						<l n="33" num="7.3"><w n="33.1">Si</w> <w n="33.2">ses</w> <w n="33.3">balances</w> <w n="33.4">d</w>’<w n="33.5">or</w> <w n="33.6">n</w>’<w n="33.7">ont</w> <w n="33.8">pesé</w> <w n="33.9">le</w> <w n="33.10">déluge</w></l>
						<l n="34" num="7.4"><w n="34.1">De</w> <w n="34.2">larmes</w> <w n="34.3">qu</w>’<w n="34.4">à</w> <w n="34.5">la</w> <w n="34.6">mer</w> <w n="34.7">ont</w> <w n="34.8">versé</w> <w n="34.9">tes</w> <w n="34.10">ruisseaux</w> ?</l>
						<l n="35" num="7.5"><w n="35.1">Qui</w> <w n="35.2">des</w> <w n="35.3">Dieux</w> <w n="35.4">osera</w>, <w n="35.5">Lesbos</w>, <w n="35.6">être</w> <w n="35.7">ton</w> <w n="35.8">juge</w> ?</l>
					</lg>
					<lg n="8">
						<l n="36" num="8.1"><w n="36.1">Que</w> <w n="36.2">nous</w> <w n="36.3">veulent</w> <w n="36.4">les</w> <w n="36.5">lois</w> <w n="36.6">du</w> <w n="36.7">juste</w> <w n="36.8">et</w> <w n="36.9">de</w> <w n="36.10">l</w>’<w n="36.11">injuste</w> ?</l>
						<l n="37" num="8.2"><w n="37.1">Vierges</w> <w n="37.2">au</w> <w n="37.3">cœur</w> <w n="37.4">sublime</w>, <w n="37.5">honneur</w> <w n="37.6">de</w> <w n="37.7">l</w>’<w n="37.8">Archipel</w>,</l>
						<l n="38" num="8.3"><w n="38.1">Votre</w> <w n="38.2">religion</w> <w n="38.3">comme</w> <w n="38.4">une</w> <w n="38.5">autre</w> <w n="38.6">est</w> <w n="38.7">auguste</w>,</l>
						<l n="39" num="8.4"><w n="39.1">Et</w> <w n="39.2">l</w>’<w n="39.3">amour</w> <w n="39.4">se</w> <w n="39.5">rira</w> <w n="39.6">de</w> <w n="39.7">l</w>’<w n="39.8">Enfer</w> <w n="39.9">et</w> <w n="39.10">du</w> <w n="39.11">Ciel</w> !</l>
						<l n="40" num="8.5"><w n="40.1">Que</w> <w n="40.2">nous</w> <w n="40.3">veulent</w> <w n="40.4">les</w> <w n="40.5">lois</w> <w n="40.6">du</w> <w n="40.7">juste</w> <w n="40.8">et</w> <w n="40.9">de</w> <w n="40.10">l</w>’<w n="40.11">injuste</w> ?</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1"><w n="41.1">Car</w> <w n="41.2">Lesbos</w> <w n="41.3">entre</w> <w n="41.4">tous</w> <w n="41.5">m</w>’<w n="41.6">a</w> <w n="41.7">choisi</w> <w n="41.8">sur</w> <w n="41.9">la</w> <w n="41.10">terre</w></l>
						<l n="42" num="9.2"><w n="42.1">Pour</w> <w n="42.2">chanter</w> <w n="42.3">le</w> <w n="42.4">secret</w> <w n="42.5">de</w> <w n="42.6">ses</w> <w n="42.7">vierges</w> <w n="42.8">en</w> <w n="42.9">fleurs</w>,</l>
						<l n="43" num="9.3"><w n="43.1">Et</w> <w n="43.2">je</w> <w n="43.3">fus</w> <w n="43.4">dès</w> <w n="43.5">l</w>’<w n="43.6">enfance</w> <w n="43.7">admis</w> <w n="43.8">au</w> <w n="43.9">noir</w> <w n="43.10">mystère</w></l>
						<l n="44" num="9.4"><w n="44.1">Des</w> <w n="44.2">rires</w> <w n="44.3">effrénés</w> <w n="44.4">mêlés</w> <w n="44.5">aux</w> <w n="44.6">sombres</w> <w n="44.7">pleurs</w> ;</l>
						<l n="45" num="9.5"><w n="45.1">Car</w> <w n="45.2">Lesbos</w> <w n="45.3">entre</w> <w n="45.4">tous</w> <w n="45.5">m</w>’<w n="45.6">a</w> <w n="45.7">choisi</w> <w n="45.8">sur</w> <w n="45.9">la</w> <w n="45.10">terre</w>.</l>
					</lg>
					<lg n="10">
						<l n="46" num="10.1"><w n="46.1">Et</w> <w n="46.2">depuis</w> <w n="46.3">lors</w> <w n="46.4">je</w> <w n="46.5">veille</w> <w n="46.6">au</w> <w n="46.7">sommet</w> <w n="46.8">de</w> <w n="46.9">Leucate</w>,</l>
						<l n="47" num="10.2"><w n="47.1">Comme</w> <w n="47.2">une</w> <w n="47.3">sentinelle</w> <w n="47.4">à</w> <w n="47.5">l</w>’<w n="47.6">œil</w> <w n="47.7">perçant</w> <w n="47.8">et</w> <w n="47.9">sûr</w>,</l>
						<l n="48" num="10.3"><w n="48.1">Qui</w> <w n="48.2">guette</w> <w n="48.3">nuit</w> <w n="48.4">et</w> <w n="48.5">jour</w> <w n="48.6">brick</w>, <w n="48.7">tartane</w> <w n="48.8">ou</w> <w n="48.9">frégate</w>,</l>
						<l n="49" num="10.4"><w n="49.1">Dont</w> <w n="49.2">les</w> <w n="49.3">formes</w> <w n="49.4">au</w> <w n="49.5">loin</w> <w n="49.6">frissonnent</w> <w n="49.7">dans</w> <w n="49.8">l</w>’<w n="49.9">azur</w> ;</l>
						<l n="50" num="10.5"><w n="50.1">Et</w> <w n="50.2">depuis</w> <w n="50.3">lors</w> <w n="50.4">je</w> <w n="50.5">veille</w> <w n="50.6">au</w> <w n="50.7">sommet</w> <w n="50.8">de</w> <w n="50.9">Leucate</w>,</l>
					</lg>
					<lg n="11">
						<l n="51" num="11.1"><w n="51.1">Pour</w> <w n="51.2">savoir</w> <w n="51.3">si</w> <w n="51.4">la</w> <w n="51.5">mer</w> <w n="51.6">est</w> <w n="51.7">indulgente</w> <w n="51.8">et</w> <w n="51.9">bonne</w>,</l>
						<l n="52" num="11.2"><w n="52.1">Et</w> <w n="52.2">parmi</w> <w n="52.3">les</w> <w n="52.4">sanglots</w> <w n="52.5">dont</w> <w n="52.6">le</w> <w n="52.7">roc</w> <w n="52.8">retentit</w></l>
						<l n="53" num="11.3"><w n="53.1">Un</w> <w n="53.2">soir</w> <w n="53.3">ramènera</w> <w n="53.4">vers</w> <w n="53.5">Lesbos</w>, <w n="53.6">qui</w> <w n="53.7">pardonne</w>,</l>
						<l n="54" num="11.4"><w n="54.1">Le</w> <w n="54.2">cadavre</w> <w n="54.3">adoré</w> <w n="54.4">de</w> <w n="54.5">Sapho</w>, <w n="54.6">qui</w> <w n="54.7">partit</w></l>
						<l n="55" num="11.5"><w n="55.1">Pour</w> <w n="55.2">savoir</w> <w n="55.3">si</w> <w n="55.4">la</w> <w n="55.5">mer</w> <w n="55.6">est</w> <w n="55.7">indulgente</w> <w n="55.8">et</w> <w n="55.9">bonne</w> !</l>
					</lg>
					<lg n="12">
						<l n="56" num="12.1"><w n="56.1">De</w> <w n="56.2">la</w> <w n="56.3">mâle</w> <w n="56.4">Sapho</w>, <w n="56.5">l</w>’<w n="56.6">amante</w> <w n="56.7">et</w> <w n="56.8">le</w> <w n="56.9">poète</w>,</l>
						<l n="57" num="12.2"><w n="57.1">Plus</w> <w n="57.2">belle</w> <w n="57.3">que</w> <w n="57.4">Vénus</w> <w n="57.5">par</w> <w n="57.6">ses</w> <w n="57.7">mornes</w> <w n="57.8">pâleurs</w> !</l>
						<l n="58" num="12.3">— <w n="58.1">L</w>’<w n="58.2">œil</w> <w n="58.3">d</w>’<w n="58.4">azur</w> <w n="58.5">est</w> <w n="58.6">vaincu</w> <w n="58.7">par</w> <w n="58.8">l</w>’<w n="58.9">œil</w> <w n="58.10">noir</w> <w n="58.11">que</w> <w n="58.12">tachète</w></l>
						<l n="59" num="12.4"><w n="59.1">Le</w> <w n="59.2">cercle</w> <w n="59.3">ténébreux</w> <w n="59.4">tracé</w> <w n="59.5">par</w> <w n="59.6">les</w> <w n="59.7">douleurs</w></l>
						<l n="60" num="12.5"><w n="60.1">De</w> <w n="60.2">la</w> <w n="60.3">mâle</w> <w n="60.4">Sapho</w>, <w n="60.5">l</w>’<w n="60.6">amante</w> <w n="60.7">et</w> <w n="60.8">le</w> <w n="60.9">poète</w> !</l>
					</lg>
					<lg n="13">
						<l n="61" num="13.1">— <w n="61.1">Plus</w> <w n="61.2">belle</w> <w n="61.3">que</w> <w n="61.4">Vénus</w> <w n="61.5">se</w> <w n="61.6">dressant</w> <w n="61.7">sur</w> <w n="61.8">le</w> <w n="61.9">monde</w></l>
						<l n="62" num="13.2"><w n="62.1">Et</w> <w n="62.2">versant</w> <w n="62.3">les</w> <w n="62.4">trésors</w> <w n="62.5">de</w> <w n="62.6">sa</w> <w n="62.7">sérénité</w></l>
						<l n="63" num="13.3"><w n="63.1">Et</w> <w n="63.2">le</w> <w n="63.3">rayonnement</w> <w n="63.4">de</w> <w n="63.5">sa</w> <w n="63.6">jeunesse</w> <w n="63.7">blonde</w></l>
						<l n="64" num="13.4"><w n="64.1">Sur</w> <w n="64.2">le</w> <w n="64.3">vieil</w> <w n="64.4">Océan</w> <w n="64.5">de</w> <w n="64.6">sa</w> <w n="64.7">fille</w> <w n="64.8">enchanté</w> ;</l>
						<l n="65" num="13.5"><w n="65.1">Plus</w> <w n="65.2">belle</w> <w n="65.3">que</w> <w n="65.4">Vénus</w> <w n="65.5">se</w> <w n="65.6">dressant</w> <w n="65.7">sur</w> <w n="65.8">le</w> <w n="65.9">monde</w> !</l>
					</lg>
					<lg n="14">
						<l n="66" num="14.1">— <w n="66.1">De</w> <w n="66.2">Sapho</w> <w n="66.3">qui</w> <w n="66.4">mourut</w> <w n="66.5">le</w> <w n="66.6">jour</w> <w n="66.7">de</w> <w n="66.8">son</w> <w n="66.9">blasphème</w>,</l>
						<l n="67" num="14.2"><w n="67.1">Quand</w>, <w n="67.2">insultant</w> <w n="67.3">le</w> <w n="67.4">rite</w> <w n="67.5">et</w> <w n="67.6">le</w> <w n="67.7">culte</w> <w n="67.8">inventé</w>,</l>
						<l n="68" num="14.3"><w n="68.1">Elle</w> <w n="68.2">fit</w> <w n="68.3">son</w> <w n="68.4">beau</w> <w n="68.5">corps</w> <w n="68.6">la</w> <w n="68.7">pâture</w> <w n="68.8">suprême</w></l>
						<l n="69" num="14.4"><w n="69.1">D</w>’<w n="69.2">un</w> <w n="69.3">brutal</w> <w n="69.4">dont</w> <w n="69.5">l</w>’<w n="69.6">orgueil</w> <w n="69.7">punit</w> <w n="69.8">l</w>’<w n="69.9">impiété</w></l>
						<l n="70" num="14.5"><w n="70.1">De</w> <w n="70.2">celle</w> <w n="70.3">qui</w> <w n="70.4">mourut</w> <w n="70.5">le</w> <w n="70.6">jour</w> <w n="70.7">de</w> <w n="70.8">son</w> <w n="70.9">blasphème</w>.</l>
					</lg>
					<lg n="15">
						<l n="71" num="15.1"><w n="71.1">Et</w> <w n="71.2">c</w>’<w n="71.3">est</w> <w n="71.4">depuis</w> <w n="71.5">ce</w> <w n="71.6">temps</w> <w n="71.7">que</w> <w n="71.8">Lesbos</w> <w n="71.9">se</w> <w n="71.10">lamente</w>,</l>
						<l n="72" num="15.2"><w n="72.1">Et</w>, <w n="72.2">malgré</w> <w n="72.3">les</w> <w n="72.4">honneurs</w> <w n="72.5">que</w> <w n="72.6">lui</w> <w n="72.7">rend</w> <w n="72.8">l</w>’<w n="72.9">univers</w>,</l>
						<l n="73" num="15.3"><w n="73.1">S</w>’<w n="73.2">enivre</w> <w n="73.3">chaque</w> <w n="73.4">nuit</w> <w n="73.5">du</w> <w n="73.6">cri</w> <w n="73.7">de</w> <w n="73.8">la</w> <w n="73.9">tourmente</w></l>
						<l n="74" num="15.4"><w n="74.1">Que</w> <w n="74.2">poussent</w> <w n="74.3">vers</w> <w n="74.4">les</w> <w n="74.5">cieux</w> <w n="74.6">ses</w> <w n="74.7">rivages</w> <w n="74.8">déserts</w>.</l>
						<l n="75" num="15.5"><w n="75.1">Et</w> <w n="75.2">c</w>’<w n="75.3">est</w> <w n="75.4">depuis</w> <w n="75.5">ce</w> <w n="75.6">temps</w> <w n="75.7">que</w> <w n="75.8">Lesbos</w> <w n="75.9">se</w> <w n="75.10">lamente</w> !</l>
					</lg>
				</div></body></text></TEI>