<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">SPLEEN ET IDÉAL</head><div type="poem" key="BAU34">
					<head type="number">XXXIII</head>
					<head type="main">Le Léthé</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Viens</w> <w n="1.2">sur</w> <w n="1.3">mon</w> <w n="1.4">cœur</w>, <w n="1.5">âme</w> <w n="1.6">cruelle</w> <w n="1.7">et</w> <w n="1.8">sourde</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Tigre</w> <w n="2.2">adoré</w>, <w n="2.3">monstre</w> <w n="2.4">aux</w> <w n="2.5">airs</w> <w n="2.6">indolents</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">veux</w> <w n="3.3">longtemps</w> <w n="3.4">plonger</w> <w n="3.5">mes</w> <w n="3.6">doigts</w> <w n="3.7">tremblants</w></l>
						<l n="4" num="1.4"><w n="4.1">Dans</w> <w n="4.2">l</w>’<w n="4.3">épaisseur</w> <w n="4.4">de</w> <w n="4.5">ta</w> <w n="4.6">crinière</w> <w n="4.7">lourde</w> ;</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Dans</w> <w n="5.2">tes</w> <w n="5.3">jupons</w> <w n="5.4">remplis</w> <w n="5.5">de</w> <w n="5.6">ton</w> <w n="5.7">parfum</w></l>
						<l n="6" num="2.2"><w n="6.1">Ensevelir</w> <w n="6.2">ma</w> <w n="6.3">tête</w> <w n="6.4">endolorie</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">respirer</w>, <w n="7.3">comme</w> <w n="7.4">une</w> <w n="7.5">fleur</w> <w n="7.6">flétrie</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Le</w> <w n="8.2">doux</w> <w n="8.3">relent</w> <w n="8.4">de</w> <w n="8.5">mon</w> <w n="8.6">amour</w> <w n="8.7">défunt</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Je</w> <w n="9.2">veux</w> <w n="9.3">dormir</w> ! <w n="9.4">dormir</w> <w n="9.5">plutôt</w> <w n="9.6">que</w> <w n="9.7">vivre</w> !</l>
						<l n="10" num="3.2"><w n="10.1">Dans</w> <w n="10.2">un</w> <w n="10.3">sommeil</w> <w n="10.4">aussi</w> <w n="10.5">doux</w> <w n="10.6">que</w> <w n="10.7">la</w> <w n="10.8">mort</w>,</l>
						<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2">étalerai</w> <w n="11.3">mes</w> <w n="11.4">baisers</w> <w n="11.5">sans</w> <w n="11.6">remord</w></l>
						<l n="12" num="3.4"><w n="12.1">Sur</w> <w n="12.2">ton</w> <w n="12.3">beau</w> <w n="12.4">corps</w> <w n="12.5">poli</w> <w n="12.6">comme</w> <w n="12.7">le</w> <w n="12.8">cuivre</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Pour</w> <w n="13.2">engloutir</w> <w n="13.3">mes</w> <w n="13.4">sanglots</w> <w n="13.5">apaisés</w></l>
						<l n="14" num="4.2"><w n="14.1">Rien</w> <w n="14.2">ne</w> <w n="14.3">me</w> <w n="14.4">vaut</w> <w n="14.5">l</w>’<w n="14.6">abîme</w> <w n="14.7">de</w> <w n="14.8">ta</w> <w n="14.9">couche</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">L</w>’<w n="15.2">oubli</w> <w n="15.3">puissant</w> <w n="15.4">habite</w> <w n="15.5">sur</w> <w n="15.6">ta</w> <w n="15.7">bouche</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">le</w> <w n="16.3">Léthé</w> <w n="16.4">coule</w> <w n="16.5">dans</w> <w n="16.6">tes</w> <w n="16.7">baisers</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">A</w> <w n="17.2">mon</w> <w n="17.3">destin</w>, <w n="17.4">désormais</w> <w n="17.5">mon</w> <w n="17.6">délice</w>,</l>
						<l n="18" num="5.2"><w n="18.1">J</w>’<w n="18.2">obéirai</w> <w n="18.3">comme</w> <w n="18.4">un</w> <w n="18.5">prédestiné</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Martyr</w> <w n="19.2">docile</w>, <w n="19.3">innocent</w> <w n="19.4">condamné</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Dont</w> <w n="20.2">la</w> <w n="20.3">ferveur</w> <w n="20.4">attise</w> <w n="20.5">le</w> <w n="20.6">supplice</w>,</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Je</w> <w n="21.2">sucerai</w>, <w n="21.3">pour</w> <w n="21.4">noyer</w> <w n="21.5">ma</w> <w n="21.6">rancœur</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Le</w> <w n="22.2">népenthès</w> <w n="22.3">et</w> <w n="22.4">la</w> <w n="22.5">bonne</w> <w n="22.6">ciguë</w></l>
						<l n="23" num="6.3"><w n="23.1">Aux</w> <w n="23.2">bouts</w> <w n="23.3">charmants</w> <w n="23.4">de</w> <w n="23.5">cette</w> <w n="23.6">gorge</w> <w n="23.7">aiguë</w></l>
						<l n="24" num="6.4"><w n="24.1">Qui</w> <w n="24.2">n</w>’<w n="24.3">a</w> <w n="24.4">jamais</w> <w n="24.5">emprisonné</w> <w n="24.6">de</w> <w n="24.7">cœur</w>.</l>
					</lg>
				</div></body></text></TEI>