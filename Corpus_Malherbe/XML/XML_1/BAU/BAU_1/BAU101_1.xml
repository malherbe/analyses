<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES FLEURS DU MAL</title>
				<title type="medium">Une édition électronique</title>
				<author key="BAU">
					<name>
						<forename>Charles</forename>
						<surname>BAUDELAIRE</surname>
					</name>
					<date from="1821" to="1867">1821-1867</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage en XML</resp>
					<name id="BB">
						<forename>Benoît</forename>
						<surname>Brard</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4145 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2010">2010</date>
				<idno type="local">BAU_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Les Fleurs du mal</title>
						<author>Charles Baudelaire</author>
					</titleStmt>
					<publicationStmt>
						<publisher>wikisource.org</publisher>
						<idno type="URL">http://fr.wikisource.org/wiki/Les_Fleurs_du_mal_(1868)</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>LES FLEURS DU MAL</title>
								<author>Charles Baudelaire</author>
								<imprint>
									<publisher>Michel Lévy</publisher>
									<date when="1868">1868</date>
								</imprint>
							</monogr>
							<note>3ème édition des Fleurs du mal (posthume).</note>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
			<biblStruct>
				<monogr>
					<title>LES FLEURS DU MAL</title>
					<author>Charles Baudelaire</author>
					<editor>Édouard Maynial</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Société Les Belles Lettres</publisher>
						<date when="1952">1952</date>
					</imprint>
				</monogr>
			</biblStruct>
		</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1861">1857-1861</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de Gautier de Théophile Gautier et l’appendice n’ont pas été encodés.</p>
			</samplingDecl>
			<editorialDecl>
				<p>Importation semi-automatique depuis fr.wikisource.org. Les vers ont été importés au kilomètre. La structuration en poèmes et en strophes a été rajoutée manuellement</p>
				<normalization>
					<p>Normalisation des titres.</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
					<change when="2011-09-19" who="RR">Modification en fonction de l’édition de référence (Les Belles Lettres) :insertion des pièces censurées et report des pièces ajoutées dans une partie « Supplément". Les pièces manquantes ont été insérées à partir de la version électronique d’Enclitt avec plusieurs corrections.</change>
					<change when="2011-09-22" who="RR">La balise « space" pour le retrait des vers a été placée dans la balise « l" (dans le vers) et non dans « lg" (entre les vers)</change>
					<change when="2011-09-26" who="RR">Insertion du poème en latin « Franciscae meae laudes"</change>
					<change when="2011-09-27" who="RR">modification du poème « les petites vieilles" : De frascati défunt…</change>
					<change when="2011-09-27" who="RR">découpage en strophes de « Le crépuscule du soir"</change>
					<change when="2011-09-27" who="RR">correction inutile de BB dans « Le vin de l’assassin"</change>
					<change when="2011-09-27" who="RR">correction du titre de « Lesbos" et vers « nuits chauds"</change>
					<change when="2011-09-27" who="RR">correction dans « femmes damnées" loin des peuples…</change>
					<change when="2011-09-27" who="RR">suppression du numéro d’ordre ; remplacé par key</change>
					<change when="2016-01-23" who="RR">Utilisation de l’attribut « lang" pour marquer les vers inanalysables par les programmes Malherbe (BAU64 :vers en latin)</change>
					<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
					<change when="2017-06-27" who="RR">Révision des titres et sous-titres pour une cohérence avec la table des matières.</change>
				</listChange>
			</revisionDesc>
	</teiHeader><text><body><head type="main_part">TABLEAUX PARISIENS</head><div type="poem" key="BAU101">
					<head type="number">XCVII</head>
					<head type="main">Le Squelette laboureur</head>
					<div type="section" n="1">
						<head type="number">I</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">les</w> <w n="1.3">planches</w> <w n="1.4">d</w>’<w n="1.5">anatomie</w></l>
							<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">traînent</w> <w n="2.3">sur</w> <w n="2.4">ces</w> <w n="2.5">quais</w> <w n="2.6">poudreux</w></l>
							<l n="3" num="1.3"><w n="3.1">Où</w> <w n="3.2">maint</w> <w n="3.3">livre</w> <w n="3.4">cadavéreux</w></l>
							<l n="4" num="1.4"><w n="4.1">Dort</w> <w n="4.2">comme</w> <w n="4.3">une</w> <w n="4.4">antique</w> <w n="4.5">momie</w>,</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Dessins</w> <w n="5.2">auxquels</w> <w n="5.3">la</w> <w n="5.4">gravité</w></l>
							<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">le</w> <w n="6.3">savoir</w> <w n="6.4">d</w>’<w n="6.5">un</w> <w n="6.6">vieil</w> <w n="6.7">artiste</w>,</l>
							<l n="7" num="2.3"><w n="7.1">Bien</w> <w n="7.2">que</w> <w n="7.3">le</w> <w n="7.4">sujet</w> <w n="7.5">en</w> <w n="7.6">soit</w> <w n="7.7">triste</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Ont</w> <w n="8.2">communiqué</w> <w n="8.3">la</w> <w n="8.4">Beauté</w>,</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">On</w> <w n="9.2">voit</w>, <w n="9.3">ce</w> <w n="9.4">qui</w> <w n="9.5">rend</w> <w n="9.6">plus</w> <w n="9.7">complètes</w></l>
							<l n="10" num="3.2"><w n="10.1">Ces</w> <w n="10.2">mystérieuses</w> <w n="10.3">horreurs</w>,</l>
							<l n="11" num="3.3"><w n="11.1">Bêchant</w> <w n="11.2">comme</w> <w n="11.3">des</w> <w n="11.4">laboureurs</w>,</l>
							<l n="12" num="3.4"><w n="12.1">Des</w> <w n="12.2">Écorchés</w> <w n="12.3">et</w> <w n="12.4">des</w> <w n="12.5">Squelettes</w>.</l>
						</lg>
					</div>
					<div type="section" n="2">
						<head type="number">II</head>
						<lg n="1">
							<l n="13" num="1.1"><w n="13.1">De</w> <w n="13.2">ce</w> <w n="13.3">terrain</w> <w n="13.4">que</w> <w n="13.5">vous</w> <w n="13.6">fouillez</w>,</l>
							<l n="14" num="1.2"><w n="14.1">Manants</w> <w n="14.2">résignés</w> <w n="14.3">et</w> <w n="14.4">funèbres</w>,</l>
							<l n="15" num="1.3"><w n="15.1">De</w> <w n="15.2">tout</w> <w n="15.3">l</w>’<w n="15.4">effort</w> <w n="15.5">de</w> <w n="15.6">vos</w> <w n="15.7">vertèbres</w>,</l>
							<l n="16" num="1.4"><w n="16.1">Ou</w> <w n="16.2">de</w> <w n="16.3">vos</w> <w n="16.4">muscles</w> <w n="16.5">dépouillés</w>,</l>
						</lg>
						<lg n="2">
							<l n="17" num="2.1"><w n="17.1">Dites</w>, <w n="17.2">quelle</w> <w n="17.3">moisson</w> <w n="17.4">étrange</w>,</l>
							<l n="18" num="2.2"><w n="18.1">Forçats</w> <w n="18.2">arrachés</w> <w n="18.3">au</w> <w n="18.4">charnier</w>,</l>
							<l n="19" num="2.3"><w n="19.1">Tirez</w>-<w n="19.2">vous</w>, <w n="19.3">et</w> <w n="19.4">de</w> <w n="19.5">quel</w> <w n="19.6">fermier</w></l>
							<l n="20" num="2.4"><w n="20.1">Avez</w>-<w n="20.2">vous</w> <w n="20.3">à</w> <w n="20.4">remplir</w> <w n="20.5">la</w> <w n="20.6">grange</w> ?</l>
						</lg>
						<lg n="3">
							<l n="21" num="3.1"><w n="21.1">Voulez</w>-<w n="21.2">vous</w> (<w n="21.3">d</w>’<w n="21.4">un</w> <w n="21.5">destin</w> <w n="21.6">trop</w> <w n="21.7">dur</w></l>
							<l n="22" num="3.2"><w n="22.1">Épouvantable</w> <w n="22.2">et</w> <w n="22.3">clair</w> <w n="22.4">emblème</w> !)</l>
							<l n="23" num="3.3"><w n="23.1">Montrer</w> <w n="23.2">que</w> <w n="23.3">dans</w> <w n="23.4">la</w> <w n="23.5">fosse</w> <w n="23.6">même</w></l>
							<l n="24" num="3.4"><w n="24.1">Le</w> <w n="24.2">sommeil</w> <w n="24.3">promis</w> <w n="24.4">n</w>’<w n="24.5">est</w> <w n="24.6">pas</w> <w n="24.7">sûr</w> ;</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1"><w n="25.1">Qu</w>’<w n="25.2">envers</w> <w n="25.3">nous</w> <w n="25.4">le</w> <w n="25.5">Néant</w> <w n="25.6">est</w> <w n="25.7">traître</w> ;</l>
							<l n="26" num="4.2"><w n="26.1">Que</w> <w n="26.2">tout</w>, <w n="26.3">même</w> <w n="26.4">la</w> <w n="26.5">Mort</w>, <w n="26.6">nous</w> <w n="26.7">ment</w>,</l>
							<l n="27" num="4.3"><w n="27.1">Et</w> <w n="27.2">que</w> <w n="27.3">sempiternellement</w>,</l>
							<l n="28" num="4.4"><w n="28.1">Hélas</w> ! <w n="28.2">il</w> <w n="28.3">nous</w> <w n="28.4">faudra</w> <w n="28.5">peut</w>-<w n="28.6">être</w></l>
						</lg>
						<lg n="5">
							<l n="29" num="5.1"><w n="29.1">Dans</w> <w n="29.2">quelque</w> <w n="29.3">pays</w> <w n="29.4">inconnu</w></l>
							<l n="30" num="5.2"><w n="30.1">Écorcher</w> <w n="30.2">la</w> <w n="30.3">terre</w> <w n="30.4">revêche</w></l>
							<l n="31" num="5.3"><w n="31.1">Et</w> <w n="31.2">pousser</w> <w n="31.3">une</w> <w n="31.4">lourde</w> <w n="31.5">bêche</w></l>
							<l n="32" num="5.4"><w n="32.1">Sous</w> <w n="32.2">notre</w> <w n="32.3">pied</w> <w n="32.4">sanglant</w> <w n="32.5">et</w> <w n="32.6">nu</w> ?</l>
						</lg>
					</div>
				</div></body></text></TEI>