<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO46">
					<head type="main">L’ESPRIT FORT</head>
					<head type="form">CONTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Aux</w> <w n="1.2">pieds</w> <w n="1.3">d</w>’<w n="1.4">un</w> <w n="1.5">Directeur</w>, <w n="1.6">Climène</w>, <w n="1.7">un</w> <w n="1.8">beau</w> <w n="1.9">matin</w>,</l>
						<l n="2" num="1.2"><space unit="char" quantity="8"></space><w n="2.1">Avec</w> <w n="2.2">un</w> <w n="2.3">repentir</w> <w n="2.4">sincère</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Déclara</w> <w n="3.2">nettement</w> <w n="3.3">que</w> <w n="3.4">le</w> <w n="3.5">petit</w> <w n="3.6">Colin</w></l>
						<l n="4" num="1.4"><space unit="char" quantity="8"></space><w n="4.1">N</w>’<w n="4.2">étoit</w> <w n="4.3">pas</w> <w n="4.4">le</w> <w n="4.5">fils</w> <w n="4.6">de</w> <w n="4.7">son</w> <w n="4.8">père</w>.</l>
						<l n="5" num="1.5"><space unit="char" quantity="8"></space>‒ « <w n="5.1">Halte</w> <w n="5.2">là</w> ! » <w n="5.3">dit</w> <w n="5.4">le</w> <w n="5.5">Confesseur</w>,</l>
						<l n="6" num="1.6">« <w n="6.1">Pour</w> <w n="6.2">un</w> <hi rend="ital"><w n="6.3">Confiteor</w></hi> <w n="6.4">vous</w> <w n="6.5">n</w>’<w n="6.6">en</w> <w n="6.7">serez</w> <w n="6.8">pas</w> <w n="6.9">quitte</w> ;</l>
						<l n="7" num="1.7">» <w n="7.1">Il</w> <w n="7.2">en</w> <w n="7.3">faut</w> <w n="7.4">deux</w> <w n="7.5">au</w> <w n="7.6">moins</w>, <w n="7.7">ce</w> <w n="7.8">crime</w> <w n="7.9">fait</w> <w n="7.10">horreur</w>.</l>
						<l n="8" num="1.8">» <w n="8.1">Faut</w>-<w n="8.2">il</w> <w n="8.3">qu</w>’<w n="8.4">injustement</w> <w n="8.5">votre</w> <w n="8.6">enfant</w> <w n="8.7">déshérite</w></l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>» <w n="9.1">Un</w> <w n="9.2">légitime</w> <w n="9.3">successeur</w> ?</l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space>» <w n="10.1">Il</w> <w n="10.2">faut</w>, <w n="10.3">Madame</w>, <w n="10.4">vous</w> <w n="10.5">résoudre</w></l>
						<l rhyme="none" n="11" num="1.11"><space unit="char" quantity="4"></space>» <w n="11.1">A</w> <w n="11.2">confesser</w> <w n="11.3">le</w> <w n="11.4">fait</w> <w n="11.5">à</w> <w n="11.6">votre</w> <w n="11.7">époux</w>,</l>
						<l n="12" num="1.12"><space unit="char" quantity="8"></space>» <w n="12.1">Sans</w> <w n="12.2">quoi</w> <w n="12.3">je</w> <w n="12.4">ne</w> <w n="12.5">puis</w> <w n="12.6">vous</w> <w n="12.7">absoudre</w>. »</l>
						<l n="13" num="1.13"><space unit="char" quantity="8"></space><w n="13.1">L</w>’<w n="13.2">avouer</w> <w n="13.3">ne</w> <w n="13.4">se</w> <w n="13.5">pouvoit</w> <w n="13.6">pas</w>.</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">La</w> <w n="14.2">voilà</w> <w n="14.3">dans</w> <w n="14.4">un</w> <w n="14.5">embarras</w></l>
						<l n="15" num="1.15"><w n="15.1">Qu</w>’<w n="15.2">on</w> <w n="15.3">ne</w> <w n="15.4">peut</w> <w n="15.5">exprimer</w>, <w n="15.6">car</w> <w n="15.7">enfin</w> <w n="15.8">l</w>’<w n="15.9">aventure</w></l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">Étoit</w> <w n="16.2">à</w> <w n="16.3">digérer</w> <w n="16.4">trop</w> <w n="16.5">dure</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Il</w> <w n="17.2">fallut</w> <w n="17.3">succomber</w>, <w n="17.4">et</w>, <w n="17.5">d</w>’<w n="17.6">un</w> <w n="17.7">mortel</w> <w n="17.8">chagrin</w>,</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Tomber</w> <w n="18.2">dans</w> <w n="18.3">une</w> <w n="18.4">maladie</w></l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Qui</w> <w n="19.2">pensa</w> <w n="19.3">lui</w> <w n="19.4">coûter</w> <w n="19.5">la</w> <w n="19.6">vie</w>.</l>
						<l n="20" num="1.20"><space unit="char" quantity="8"></space><w n="20.1">Sur</w> <w n="20.2">le</w> <w n="20.3">rapport</w> <w n="20.4">du</w> <w n="20.5">Médecin</w>,</l>
						<l n="21" num="1.21"><w n="21.1">Son</w> <w n="21.2">époux</w> <w n="21.3">connoissant</w> <w n="21.4">que</w> <w n="21.5">la</w> <w n="21.6">mélancolie</w></l>
						<l n="22" num="1.22"><space unit="char" quantity="4"></space><w n="22.1">Alloit</w> <w n="22.2">couper</w> <w n="22.3">la</w> <w n="22.4">trame</w> <w n="22.5">de</w> <w n="22.6">ses</w> <w n="22.7">jours</w>,</l>
						<l n="23" num="1.23"><space unit="char" quantity="8"></space><w n="23.1">La</w> <w n="23.2">pria</w> <w n="23.3">d</w>’<w n="23.4">en</w> <w n="23.5">dire</w> <w n="23.6">la</w> <w n="23.7">cause</w>.</l>
						<l n="24" num="1.24"><w n="24.1">Elle</w> <w n="24.2">veut</w> <w n="24.3">l</w>’<w n="24.4">en</w> <w n="24.5">instruire</w>, <w n="24.6">et</w> <w n="24.7">jamais</w> <w n="24.8">elle</w> <w n="24.9">n</w>’<w n="24.10">ose</w>.</l>
						<l n="25" num="1.25"><space unit="char" quantity="8"></space>‒ « <w n="25.1">Ose</w> <w n="25.2">tout</w>, » <w n="25.3">dit</w>-<w n="25.4">il</w>, « <w n="25.5">mes</w> <w n="25.6">amours</w> :</l>
						<l n="26" num="1.26">« <w n="26.1">Rien</w> <w n="26.2">ne</w> <w n="26.3">me</w> <w n="26.4">déplaira</w>, <w n="26.5">pourvu</w> <w n="26.6">qu</w>’<w n="26.7">on</w> <w n="26.8">te</w> <w n="26.9">guérisse</w> ;</l>
						<l n="27" num="1.27">» <w n="27.1">Quoi</w> ! <w n="27.2">faut</w>-<w n="27.3">il</w> <w n="27.4">qu</w>’<w n="27.5">un</w> <w n="27.6">secret</w> <w n="27.7">te</w> <w n="27.8">donne</w> <w n="27.9">la</w> <w n="27.10">jaunisse</w>,</l>
						<l n="28" num="1.28">» <w n="28.1">Et</w> <w n="28.2">qu</w>’<w n="28.3">une</w> <w n="28.4">femme</w> <w n="28.5">meure</w>, <w n="28.6">à</w> <w n="28.7">faute</w> <w n="28.8">de</w> <w n="28.9">parler</w> ?</l>
						<l n="29" num="1.29">» <w n="29.1">Cela</w> <w n="29.2">seroit</w> <w n="29.3">nouveau</w>. ‒ <w n="29.4">Je</w> <w n="29.5">vais</w> <w n="29.6">tout</w> <w n="29.7">révéler</w>,</l>
						<l n="30" num="1.30">» <w n="30.1">Puisqu</w>’<w n="30.2">aussi</w> <w n="30.3">bien</w>, » <w n="30.4">dit</w>-<w n="30.5">elle</w>, « <w n="30.6">un</w> <w n="30.7">repos</w> <w n="30.8">favorable</w></l>
						<l n="31" num="1.31">» <w n="31.1">Doit</w> <w n="31.2">terminer</w> <w n="31.3">bientôt</w> <w n="31.4">mon</w> <w n="31.5">état</w> <w n="31.6">déplorable</w>.</l>
						<l n="32" num="1.32"><space unit="char" quantity="8"></space>» <w n="32.1">J</w>’<w n="32.2">étois</w> <w n="32.3">à</w> <w n="32.4">la</w> <w n="32.5">maison</w> <w n="32.6">des</w> <w n="32.7">champs</w>,</l>
						<l n="33" num="1.33"><space unit="char" quantity="8"></space>» <w n="33.1">Où</w> <w n="33.2">je</w> <w n="33.3">faisois</w> <w n="33.4">la</w> <w n="33.5">ménagère</w>,</l>
						<l n="34" num="1.34">» <w n="34.1">Quand</w> <w n="34.2">la</w> <w n="34.3">voisine</w> <w n="34.4">Alix</w>, <w n="34.5">par</w> <w n="34.6">des</w> <w n="34.7">discours</w> <w n="34.8">touchants</w>,</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space>» <w n="35.1">Auxquels</w> <w n="35.2">on</w> <w n="35.3">ne</w> <w n="35.4">résiste</w> <w n="35.5">guère</w>,</l>
						<l n="36" num="1.36"><space unit="char" quantity="8"></space>» <w n="36.1">Me</w> <w n="36.2">prouva</w> <w n="36.3">qu</w>’<w n="36.4">avoir</w> <w n="36.5">des</w> <w n="36.6">enfants</w></l>
						<l n="37" num="1.37"><space unit="char" quantity="8"></space>» <w n="37.1">Étoit</w> <w n="37.2">à</w> <w n="37.3">vous</w> <w n="37.4">chose</w> <w n="37.5">impossible</w> ;</l>
						<l n="38" num="1.38">» <w n="38.1">Me</w> <w n="38.2">prôna</w> <w n="38.3">les</w> <w n="38.4">malheurs</w> <w n="38.5">de</w> <w n="38.6">la</w> <w n="38.7">stérilité</w>,</l>
						<l n="39" num="1.39">» <w n="39.1">Qui</w> <w n="39.2">chez</w> <w n="39.3">les</w> <w n="39.4">Juifs</w> <w n="39.5">passoit</w> <w n="39.6">pour</w> <w n="39.7">un</w> <w n="39.8">défaut</w> <w n="39.9">terrible</w> ;</l>
						<l n="40" num="1.40">» <w n="40.1">Puis</w> <w n="40.2">dans</w> <w n="40.3">un</w> <w n="40.4">jour</w> <w n="40.5">charmant</w> <w n="40.6">me</w> <w n="40.7">fit</w> <w n="40.8">voir</w> <w n="40.9">la</w> <w n="40.10">beauté</w></l>
						<l n="41" num="1.41"><space unit="char" quantity="8"></space>» <w n="41.1">D</w>’<w n="41.2">une</w> <w n="41.3">heureuse</w> <w n="41.4">fécondité</w>.</l>
						<l n="42" num="1.42">» <w n="42.1">Je</w> <w n="42.2">me</w> <w n="42.3">rendis</w>, <w n="42.4">hélas</w> ! <w n="42.5">à</w> <w n="42.6">cette</w> <w n="42.7">douce</w> <w n="42.8">amorce</w>,</l>
						<l n="43" num="1.43">» <w n="43.1">Et</w> <w n="43.2">Lucas</w>, <w n="43.3">le</w> <w n="43.4">Valet</w> <w n="43.5">de</w> <w n="43.6">notre</w> <w n="43.7">Métayer</w>,</l>
						<l n="44" num="1.44">» <w n="44.1">Avec</w> <w n="44.2">moi</w> <w n="44.3">se</w> <w n="44.4">trouvant</w> <w n="44.5">un</w> <w n="44.6">jour</w> <w n="44.7">dans</w> <w n="44.8">le</w> <w n="44.9">grenier</w>,</l>
						<l n="45" num="1.45">» <w n="45.1">Je</w> <w n="45.2">me</w> <w n="45.3">souvins</w> <w n="45.4">d</w>’<w n="45.5">Alix</w>, <w n="45.6">et</w> <w n="45.7">je</w> <w n="45.8">manquai</w> <w n="45.9">de</w> <w n="45.10">force</w>.</l>
						<l n="46" num="1.46">» <w n="46.1">Il</w> <w n="46.2">est</w>, <w n="46.3">cela</w> <w n="46.4">soit</w> <w n="46.5">dit</w> <w n="46.6">sans</w> <w n="46.7">vous</w> <w n="46.8">mettre</w> <w n="46.9">en</w> <w n="46.10">courroux</w>,</l>
						<l n="47" num="1.47">» <w n="47.1">A</w> <w n="47.2">faire</w> <w n="47.3">des</w> <w n="47.4">enfants</w> <w n="47.5">plus</w> <w n="47.6">habile</w> <w n="47.7">que</w> <w n="47.8">vous</w>.</l>
						<l n="48" num="1.48">» <w n="48.1">Je</w> <w n="48.2">lui</w> <w n="48.3">parlai</w> <w n="48.4">d</w>’<w n="48.5">amour</w>, <w n="48.6">il</w> <w n="48.7">comprit</w> <w n="48.8">mon</w> <w n="48.9">langage</w>,</l>
						<l n="49" num="1.49">» <w n="49.1">Et</w> <w n="49.2">sur</w> <w n="49.3">un</w> <w n="49.4">sac</w> <w n="49.5">de</w> <w n="49.6">blé</w>, <w n="49.7">sac</w> <w n="49.8">funeste</w> <w n="49.9">et</w> <w n="49.10">maudit</w> !</l>
						<l n="50" num="1.50"><space unit="char" quantity="8"></space>» <w n="50.1">Faut</w>-<w n="50.2">il</w> <w n="50.3">en</w> <w n="50.4">dire</w> <w n="50.5">davantage</w> ?</l>
						<l n="51" num="1.51">» <w n="51.1">De</w> <w n="51.2">ce</w> <w n="51.3">malheureux</w> <w n="51.4">sac</w>, <w n="51.5">notre</w> <w n="51.6">Colin</w> <w n="51.7">sortit</w>.</l>
						<l n="52" num="1.52"><space unit="char" quantity="8"></space>» <w n="52.1">A</w> <w n="52.2">Lucas</w> <w n="52.3">je</w> <w n="52.4">donnai</w>, <w n="52.5">je</w> <w n="52.6">pense</w>,</l>
						<l n="53" num="1.53">» <w n="53.1">Quelques</w> <w n="53.2">boisseaux</w> <w n="53.3">de</w> <w n="53.4">blé</w> <w n="53.5">pour</w> <w n="53.6">toute</w> <w n="53.7">récompense</w>.</l>
						<l n="54" num="1.54">» <w n="54.1">Si</w> <w n="54.2">je</w> <w n="54.3">vous</w> <w n="54.4">ai</w> <w n="54.5">trahi</w>, <w n="54.6">je</w> <w n="54.7">meurs</w>, <w n="54.8">pardonnez</w>-<w n="54.9">moi</w> ;</l>
						<l n="55" num="1.55">» <w n="55.1">A</w> <w n="55.2">cela</w> <w n="55.3">près</w>, <w n="55.4">toujours</w> <w n="55.5">je</w> <w n="55.6">vous</w> <w n="55.7">gardai</w> <w n="55.8">ma</w> <w n="55.9">foi</w>.</l>
						<l n="56" num="1.56">» ‒ <w n="56.1">N</w>’<w n="56.2">est</w>-<w n="56.3">ce</w> <w n="56.4">pas</w> <w n="56.5">de</w> <w n="56.6">mon</w> <w n="56.7">blé</w> <w n="56.8">que</w> <w n="56.9">tu</w> <w n="56.10">payas</w> <w n="56.11">l</w>’<w n="56.12">ouvrage</w> ? »</l>
						<l n="57" num="1.57"><w n="57.1">Lui</w> <w n="57.2">répondit</w> <w n="57.3">Damis</w>, <w n="57.4">nullement</w> <w n="57.5">effrayé</w>.</l>
						<l n="58" num="1.58">« <w n="58.1">Cet</w> <w n="58.2">enfant</w> <w n="58.3">est</w> <w n="58.4">à</w> <w n="58.5">moi</w>, <w n="58.6">puisque</w> <w n="58.7">je</w> <w n="58.8">l</w>’<w n="58.9">ai</w> <w n="58.10">payé</w> ;</l>
						<l n="59" num="1.59"><space unit="char" quantity="8"></space>» <w n="59.1">Ne</w> <w n="59.2">m</w>’<w n="59.3">en</w> <w n="59.4">parle</w> <w n="59.5">pas</w> <w n="59.6">davantage</w>. »</l>
					</lg>
				</div></body></text></TEI>