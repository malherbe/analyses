<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO30">
					<head type="main">LA DÉFENSE BIEN OBSERVÉE</head>
					<lg n="1">
						<l n="1" num="1.1">« <w n="1.1">Quoi</w> ! <w n="1.2">maman</w> <w n="1.3">me</w> <w n="1.4">laisse</w> <w n="1.5">seulette</w> ?</l>
						<l n="2" num="1.2">» <w n="2.1">Pour</w> <w n="2.2">moi</w> <w n="2.3">j</w>’<w n="2.4">en</w> <w n="2.5">suis</w> <w n="2.6">presque</w> <w n="2.7">en</w> <w n="2.8">courroux</w> ;</l>
						<l n="3" num="1.3">» <w n="3.1">Il</w> <w n="3.2">semble</w> <w n="3.3">qu</w>’<w n="3.4">exprès</w> <w n="3.5">avec</w> <w n="3.6">vous</w></l>
						<l n="4" num="1.4">» <w n="4.1">Je</w> <w n="4.2">voulois</w> <w n="4.3">rester</w> <w n="4.4">tête</w> <w n="4.5">à</w> <w n="4.6">tête</w> ;</l>
						<l n="5" num="1.5">» <w n="5.1">Mais</w> <w n="5.2">non</w>, <w n="5.3">Monsieur</w>, <w n="5.4">n</w>’<w n="5.5">en</w> <w n="5.6">croyez</w> <w n="5.7">rien</w> ;</l>
						<l n="6" num="1.6">» <w n="6.1">Vraiment</w> <w n="6.2">je</w> <w n="6.3">vous</w> <w n="6.4">le</w> <w n="6.5">défends</w> <w n="6.6">bien</w>.</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1">» <w n="7.1">Pour</w> <w n="7.2">favoriser</w> <w n="7.3">le</w> <w n="7.4">mystère</w>,</l>
						<l n="8" num="2.2">» <w n="8.1">Ma</w> <w n="8.2">porte</w> <w n="8.3">est</w> <w n="8.4">fermée</w> <w n="8.5">aux</w> <w n="8.6">verroux</w> ;</l>
						<l n="9" num="2.3">» <w n="9.1">Ici</w>, <w n="9.2">sans</w> <w n="9.3">crainte</w> <w n="9.4">des</w> <w n="9.5">jaloux</w>,</l>
						<l n="10" num="2.4">» <w n="10.1">On</w> <w n="10.2">pourroit</w> <w n="10.3">jouir</w> <w n="10.4">et</w> <w n="10.5">se</w> <w n="10.6">taire</w> ;</l>
						<l n="11" num="2.5">» <w n="11.1">Mais</w> <w n="11.2">non</w>, <w n="11.3">Monsieur</w>, <w n="11.4">n</w>’<w n="11.5">en</w> <w n="11.6">faites</w> <w n="11.7">rien</w> ;</l>
						<l n="12" num="2.6">» <w n="12.1">Vraiment</w> <w n="12.2">je</w> <w n="12.3">vous</w> <w n="12.4">le</w> <w n="12.5">défends</w> <w n="12.6">bien</w>.</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1">» <w n="13.1">Prêt</w> <w n="13.2">à</w> <w n="13.3">rire</w> <w n="13.4">de</w> <w n="13.5">ma</w> <w n="13.6">colère</w>,</l>
						<l n="14" num="3.2">» <w n="14.1">Peut</w>-<w n="14.2">être</w> <w n="14.3">que</w> <w n="14.4">mon</w> <w n="14.5">négligé</w>,</l>
						<l n="15" num="3.3">» <w n="15.1">Mon</w> <w n="15.2">mouchoir</w> <w n="15.3">un</w> <w n="15.4">peu</w> <w n="15.5">dérangé</w>,</l>
						<l n="16" num="3.4">» <w n="16.1">Vont</w> <w n="16.2">vous</w> <w n="16.3">rendre</w> <w n="16.4">trop</w> <w n="16.5">téméraire</w> ;</l>
						<l n="17" num="3.5">» <w n="17.1">Mais</w> <w n="17.2">non</w>, <w n="17.3">Monsieur</w>, <w n="17.4">qu</w>’<w n="17.5">il</w> <w n="17.6">n</w>’<w n="17.7">en</w> <w n="17.8">soit</w> <w n="17.9">rien</w> ;</l>
						<l n="18" num="3.6">» <w n="18.1">Vraiment</w> <w n="18.2">je</w> <w n="18.3">vous</w> <w n="18.4">le</w> <w n="18.5">défends</w> <w n="18.6">bien</w>.</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1">» <w n="19.1">Dans</w> <w n="19.2">vos</w> <w n="19.3">yeux</w> <w n="19.4">je</w> <w n="19.5">lis</w> <w n="19.6">votre</w> <w n="19.7">audace</w>,</l>
						<l n="20" num="4.2">» <w n="20.1">Vos</w> <w n="20.2">regards</w> <w n="20.3">dévorent</w> <w n="20.4">mon</w> <w n="20.5">sein</w> ;</l>
						<l n="21" num="4.3">» <w n="21.1">Vous</w> <w n="21.2">allez</w> <w n="21.3">y</w> <w n="21.4">porter</w> <w n="21.5">la</w> <w n="21.6">main</w>,</l>
						<l n="22" num="4.4">» <w n="22.1">Votre</w> <w n="22.2">bouche</w> <w n="22.3">en</w> <w n="22.4">prendra</w> <w n="22.5">la</w> <w n="22.6">place</w> ;</l>
						<l n="23" num="4.5">» <w n="23.1">Mais</w> <w n="23.2">non</w>, <w n="23.3">Monsieur</w>, <w n="23.4">n</w>’<w n="23.5">en</w> <w n="23.6">faites</w> <w n="23.7">rien</w> ;</l>
						<l n="24" num="4.6">» <w n="24.1">Vraiment</w> <w n="24.2">je</w> <w n="24.3">vous</w> <w n="24.4">le</w> <w n="24.5">défends</w> <w n="24.6">bien</w>.</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1">» <w n="25.1">Mais</w> <w n="25.2">que</w> <w n="25.3">vois</w>-<w n="25.4">je</w> ? <w n="25.5">ma</w> <w n="25.6">jarretière</w></l>
						<l n="26" num="5.2">» <w n="26.1">Se</w> <w n="26.2">défait</w> <w n="26.3">et</w> <w n="26.4">tombe</w> <w n="26.5">à</w> <w n="26.6">mes</w> <w n="26.7">pieds</w> ;</l>
						<l n="27" num="5.3">» <w n="27.1">Souffrir</w> <w n="27.2">que</w> <w n="27.3">vous</w> <w n="27.4">la</w> <w n="27.5">rattachiez</w> !</l>
						<l n="28" num="5.4">» <w n="28.1">Oh</w> ! <w n="28.2">pour</w> <w n="28.3">cela</w> <w n="28.4">je</w> <w n="28.5">suis</w> <w n="28.6">trop</w> <w n="28.7">fière</w> !</l>
						<l n="29" num="5.5">» <w n="29.1">Non</w>, <w n="29.2">non</w>, <w n="29.3">Monsieur</w>, <w n="29.4">n</w>’<w n="29.5">en</w> <w n="29.6">faites</w> <w n="29.7">rien</w> ;</l>
						<l n="30" num="5.6">» <w n="30.1">Vraiment</w> <w n="30.2">je</w> <w n="30.3">vous</w> <w n="30.4">le</w> <w n="30.5">défends</w> <w n="30.6">bien</w>. »</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Comprenant</w> <w n="31.2">enfin</w> <w n="31.3">la</w> <w n="31.4">défense</w>,</l>
						<l n="32" num="6.2"><w n="32.1">Par</w> <w n="32.2">degré</w> <w n="32.3">Damon</w> <w n="32.4">s</w>’<w n="32.5">enhardit</w>,</l>
						<l n="33" num="6.3"><w n="33.1">A</w> <w n="33.2">la</w> <w n="33.3">belle</w> <w n="33.4">il</w> <w n="33.5">désobéit</w>,</l>
						<l n="34" num="6.4"><w n="34.1">Pour</w> <w n="34.2">prouver</w> <w n="34.3">son</w> <w n="34.4">obéissance</w>.</l>
						<l n="35" num="6.5"><w n="35.1">Jusques</w> <w n="35.2">au</w> <w n="35.3">bout</w> <w n="35.4">il</w> <w n="35.5">fit</w> <w n="35.6">si</w> <w n="35.7">bien</w>,</l>
						<l n="36" num="6.6"><w n="36.1">Qu</w>’<w n="36.2">on</w> <w n="36.3">ne</w> <w n="36.4">lui</w> <w n="36.5">défendit</w> <w n="36.6">plus</w> <w n="36.7">rien</w>.</l>
					</lg>
				</div></body></text></TEI>