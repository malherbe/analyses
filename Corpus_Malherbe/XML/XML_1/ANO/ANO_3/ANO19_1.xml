<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO19">
					<head type="main">LA FENTE</head>
					<head type="main">CONTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Orante</w> <w n="1.2">avoit</w> <w n="1.3">fait</w> <w n="1.4">emplette</w></l>
						<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2">un</w> <w n="2.3">quarteau</w> <w n="2.4">de</w> <w n="2.5">vieux</w> <w n="2.6">Rota</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Sa</w> <w n="3.2">chambrière</w> <w n="3.3">Pâquette</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">beau</w> <w n="4.3">matin</w> <w n="4.4">le</w> <w n="4.5">goûta</w></l>
						<l n="5" num="1.5"><w n="5.1">Et</w> <w n="5.2">le</w> <w n="5.3">trouva</w> <w n="5.4">bon</w> <w n="5.5">sans</w> <w n="5.6">doute</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Elle</w> <w n="6.2">y</w> <w n="6.3">revint</w> : <w n="6.4">Jean</w> <w n="6.5">l</w>’<w n="6.6">aida</w>.</l>
						<l n="7" num="1.7"><w n="7.1">Verre</w> <w n="7.2">à</w> <w n="7.3">verre</w>, <w n="7.4">goutte</w> <w n="7.5">à</w> <w n="7.6">goutte</w></l>
						<l n="8" num="1.8"><w n="8.1">La</w> <w n="8.2">feuillette</w> <w n="8.3">se</w> <w n="8.4">vida</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Au</w> <w n="9.2">bout</w> <w n="9.3">d</w>’<w n="9.4">une</w> <w n="9.5">quarantaine</w></l>
						<l n="10" num="1.10"><w n="10.1">Il</w> <w n="10.2">advint</w> <w n="10.3">que</w> <w n="10.4">le</w> <w n="10.5">Patron</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Qui</w> <w n="11.2">croit</w> <w n="11.3">la</w> <w n="11.4">feuillette</w> <w n="11.5">pleine</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Va</w> <w n="12.2">pour</w> <w n="12.3">en</w> <w n="12.4">prendre</w> <w n="12.5">l</w>’<w n="12.6">étrenne</w>.</l>
						<l n="13" num="1.13"><w n="13.1">L</w>’<w n="13.2">eut</w>-<w n="13.3">il</w> ? <w n="13.4">Vous</w> <w n="13.5">savez</w> <w n="13.6">que</w> <w n="13.7">non</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Abusé</w> <w n="14.2">dans</w> <w n="14.3">son</w> <w n="14.4">attente</w>,</l>
						<l n="15" num="1.15"><w n="15.1">D</w>’<w n="15.2">abord</w> <w n="15.3">il</w> <w n="15.4">est</w> <w n="15.5">stupéfait</w>,</l>
						<l n="16" num="1.16"><w n="16.1">Puis</w> <w n="16.2">songeant</w> <w n="16.3">que</w> <w n="16.4">le</w> <w n="16.5">vin</w> <w n="16.6">tente</w></l>
						<l n="17" num="1.17"><w n="17.1">Et</w> <w n="17.2">se</w> <w n="17.3">doutant</w> <w n="17.4">du</w> <w n="17.5">méfait</w>,</l>
						<l n="18" num="1.18"><w n="18.1">Il</w> <w n="18.2">appelle</w> <w n="18.3">sa</w> <w n="18.4">servante</w></l>
						<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">lui</w> <w n="19.3">dit</w> <w n="19.4">ce</w> <w n="19.5">qu</w>’<w n="19.6">elle</w> <w n="19.7">sait</w>.</l>
						<l n="20" num="1.20"><w n="20.1">Pourtant</w> <w n="20.2">elle</w> <w n="20.3">s</w>’<w n="20.4">émerveille</w> :</l>
						<l n="21" num="1.21"><w n="21.1">Jamais</w>, <w n="21.2">jamais</w> <w n="21.3">on</w> <w n="21.4">n</w>’<w n="21.5">a</w> <w n="21.6">vu</w></l>
						<l n="22" num="1.22"><w n="22.1">Une</w> <w n="22.2">aventure</w> <w n="22.3">pareille</w> !</l>
						<l n="23" num="1.23">‒ « <w n="23.1">Certe</w>, <w n="23.2">qui</w> <w n="23.3">l</w>’<w n="23.4">auroit</w> <w n="23.5">prévu</w> ? »</l>
						<l n="24" num="1.24"><w n="24.1">Répondit</w>-<w n="24.2">elle</w> <w n="24.3">à</w> <w n="24.4">son</w> <w n="24.5">maître</w>,</l>
						<l n="25" num="1.25">« <w n="25.1">D</w>’<w n="25.2">où</w> <w n="25.3">peut</w> <w n="25.4">provenir</w> <w n="25.5">cela</w> ?</l>
						<l n="26" num="1.26">» <w n="26.1">Quelque</w> <w n="26.2">fente</w> <w n="26.3">aura</w> <w n="26.4">peut</w>-<w n="26.5">être</w></l>
						<l n="27" num="1.27">» <w n="27.1">Causé</w> <w n="27.2">cet</w> <w n="27.3">accident</w>-<w n="27.4">là</w> ;</l>
						<l n="28" num="1.28">» <w n="28.1">Nous</w> <w n="28.2">pourrons</w> <w n="28.3">le</w> <w n="28.4">reconnoître</w>. »</l>
						<l n="29" num="1.29"><w n="29.1">Elle</w> <w n="29.2">va</w> <w n="29.3">prendre</w> <w n="29.4">un</w> <w n="29.5">flambeau</w>.</l>
						<l rhyme="none" n="30" num="1.30"><w n="30.1">L</w>’<w n="30.2">allume</w>, <w n="30.3">vient</w>, <w n="30.4">fait</w> <w n="30.5">sa</w> <w n="30.6">ronde</w> :</l>
						<l n="31" num="1.31"><w n="31.1">Rien</w> <w n="31.2">ne</w> <w n="31.3">manquoit</w> <w n="31.4">au</w> <w n="31.5">tonneau</w>.</l>
						<l n="32" num="1.32">‒ « <w n="32.1">Morgué</w> ! <w n="32.2">le</w> <w n="32.3">tour</w> <w n="32.4">est</w> <w n="32.5">nouveau</w> ;</l>
						<l n="33" num="1.33">» <w n="33.1">Voyons</w> <w n="33.2">par</w>-<w n="33.3">dessous</w> », <w n="33.4">dit</w>-<w n="33.5">elle</w>.</l>
						<l n="34" num="1.34"><w n="34.1">Au</w> <w n="34.2">même</w> <w n="34.3">instant</w> <w n="34.4">la</w> <w n="34.5">donzelle</w>,</l>
						<l n="35" num="1.35"><w n="35.1">En</w> <w n="35.2">se</w> <w n="35.3">baissant</w>, <w n="35.4">met</w> <w n="35.5">au</w> <w n="35.6">jour</w></l>
						<l n="36" num="1.36"><w n="36.1">Ce</w> <w n="36.2">qui</w> <w n="36.3">plaît</w> <w n="36.4">dans</w> <w n="36.5">une</w> <w n="36.6">belle</w>,</l>
						<l n="37" num="1.37"><w n="37.1">Morceau</w> <w n="37.2">digne</w> <w n="37.3">de</w> <w n="37.4">l</w>’<w n="37.5">amour</w>.</l>
						<l n="38" num="1.38"><w n="38.1">Et</w> <w n="38.2">pour</w> <w n="38.3">parler</w> <w n="38.4">sans</w> <w n="38.5">détour</w>,</l>
						<l n="39" num="1.39"><w n="39.1">Le</w> <w n="39.2">parois</w> <w n="39.3">de</w> <w n="39.4">sa</w> <w n="39.5">Chapelle</w></l>
						<l n="40" num="1.40"><w n="40.1">Que</w> <w n="40.2">couvroit</w> <w n="40.3">un</w> <w n="40.4">jupon</w> <w n="40.5">court</w>.</l>
						<l n="41" num="1.41">‒ « <w n="41.1">C</w>’<w n="41.2">est</w> <w n="41.3">assez</w> », <w n="41.4">lui</w> <w n="41.5">dit</w> <w n="41.6">Orante</w>,</l>
						<l n="42" num="1.42"><w n="42.1">En</w> <w n="42.2">lorgnant</w> <w n="42.3">le</w> <w n="42.4">défilé</w>,</l>
						<l n="43" num="1.43">« <w n="43.1">Viens</w> <w n="43.2">que</w> <w n="43.3">je</w> <w n="43.4">bouche</w> <w n="43.5">la</w> <w n="43.6">fente</w></l>
						<l n="44" num="1.44">» <w n="44.1">Par</w> <w n="44.2">où</w> <w n="44.3">mon</w> <w n="44.4">vin</w> <w n="44.5">a</w> <w n="44.6">coulé</w>. »</l>
					</lg>
				</div></body></text></TEI>