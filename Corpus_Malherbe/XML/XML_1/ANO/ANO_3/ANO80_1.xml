<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO80">
					<head type="main">LA QUESTION RÉSOLUE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Trois</w> <w n="1.2">rivaux</w> <w n="1.3">voyant</w> <w n="1.4">leur</w> <w n="1.5">maîtresse</w></l>
						<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">l</w>’<w n="2.3">on</w> <w n="2.4">vient</w> <w n="2.5">de</w> <w n="2.6">blesser</w> <w n="2.7">au</w> <w n="2.8">sein</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Aussitôt</w> <w n="3.2">l</w>’<w n="3.3">un</w> <w n="3.4">tombe</w> <w n="3.5">en</w> <w n="3.6">faiblesse</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">L</w>’<w n="4.2">autre</w> <w n="4.3">court</w> <w n="4.4">après</w> <w n="4.5">l</w>’<w n="4.6">assassin</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">troisième</w> <w n="5.3">bande</w> <w n="5.4">la</w> <w n="5.5">plaie</w>.</l>
						<l n="6" num="1.6"><w n="6.1">Par</w> <w n="6.2">ce</w> <w n="6.3">moyen</w> <w n="6.4">chacun</w> <w n="6.5">essaie</w></l>
						<l n="7" num="1.7"><w n="7.1">De</w> <w n="7.2">montrer</w> <w n="7.3">qui</w> <w n="7.4">l</w>’<w n="7.5">aime</w> <w n="7.6">le</w> <w n="7.7">mieux</w>.</l>
						<l n="8" num="1.8"><w n="8.1">Si</w> <w n="8.2">mon</w> <w n="8.3">avis</w> <w n="8.4">on</w> <w n="8.5">me</w> <w n="8.6">demande</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Je</w> <w n="9.2">répondrois</w> <w n="9.3">qu</w>’<w n="9.4">il</w> <w n="9.5">saute</w> <w n="9.6">aux</w> <w n="9.7">yeux</w> :</l>
						<l n="10" num="1.10"><w n="10.1">Car</w> <w n="10.2">je</w> <w n="10.3">suis</w> <w n="10.4">pour</w> <w n="10.5">celui</w> <w n="10.6">qui</w> <w n="10.7">bande</w>.</l>
					</lg>
				</div></body></text></TEI>