<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO72">
					<head type="main">LES SOULIERS</head>
					<head type="form">CONTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">tous</w> <w n="1.3">ses</w> <w n="1.4">amoureux</w>, <w n="1.5">Babet</w>, <w n="1.6">dans</w> <w n="1.7">son</w> <w n="1.8">printemps</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Exigeoit</w>, <w n="2.2">pour</w> <w n="2.3">le</w> <w n="2.4">prix</w> <w n="2.5">de</w> <w n="2.6">ses</w> <w n="2.7">faveurs</w> <w n="2.8">secrètes</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Deux</w> <w n="3.2">paires</w> <w n="3.3">de</w> <w n="3.4">souliers</w> : <w n="3.5">aujourd</w>’<w n="3.6">hui</w> <w n="3.7">les</w> <w n="3.8">grisettes</w></l>
						<l n="4" num="1.4"><w n="4.1">Rougiroient</w> <w n="4.2">d</w>’<w n="4.3">accepter</w> <w n="4.4">de</w> <w n="4.5">si</w> <w n="4.6">minces</w> <w n="4.7">présents</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Babet</w> <w n="5.2">s</w>’<w n="5.3">en</w> <w n="5.4">contentoit</w>, <w n="5.5">souliers</w> <w n="5.6">alloient</w> <w n="5.7">pleuvants</w>.</l>
						<l n="6" num="1.6"><w n="6.1">L</w>’<w n="6.2">or</w>, <w n="6.3">quand</w> <w n="6.4">on</w> <w n="6.5">est</w> <w n="6.6">jolie</w>, <w n="6.7">est</w> <w n="6.8">fugace</w>, <w n="6.9">il</w> <w n="6.10">va</w> <w n="6.11">vite</w> :</l>
						<l n="7" num="1.7"><w n="7.1">On</w> <w n="7.2">le</w> <w n="7.3">gagne</w> <w n="7.4">aisément</w>, <w n="7.5">on</w> <w n="7.6">le</w> <w n="7.7">ménage</w> <w n="7.8">peu</w> ;</l>
						<l n="8" num="1.8"><w n="8.1">Babet</w> <w n="8.2">l</w>’<w n="8.3">avoit</w> <w n="8.4">senti</w> ; <w n="8.5">souliers</w> <w n="8.6">restoient</w> <w n="8.7">au</w> <w n="8.8">gîte</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Ils</w> <w n="9.2">devenoient</w> <w n="9.3">ressource</w>. <w n="9.4">On</w> <w n="9.5">conçoit</w> <w n="9.6">qu</w>’<w n="9.7">à</w> <w n="9.8">ce</w> <w n="9.9">jeu</w></l>
						<l n="10" num="1.10"><space unit="char" quantity="8"></space><w n="10.1">Fallut</w> <w n="10.2">bientôt</w> <w n="10.3">à</w> <w n="10.4">la</w> <w n="10.5">commère</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Pour</w> <w n="11.2">loger</w> <w n="11.3">les</w> <w n="11.4">souliers</w>, <w n="11.5">une</w> <w n="11.6">maison</w> <w n="11.7">entière</w>.</l>
						<l n="12" num="1.12"><w n="12.1">Le</w> <w n="12.2">cuir</w> <w n="12.3">haussa</w> <w n="12.4">de</w> <w n="12.5">prix</w> : <w n="12.6">le</w> <w n="12.7">Prince</w> <w n="12.8">le</w> <w n="12.9">taxa</w>,</l>
						<l n="13" num="1.13"><w n="13.1">Mainte</w> <w n="13.2">bourse</w> <w n="13.3">s</w>’<w n="13.4">emplit</w>, <w n="13.5">maint</w> <w n="13.6">fermier</w> <w n="13.7">s</w>’<w n="13.8">engraissa</w> ;</l>
						<l n="14" num="1.14"><space unit="char" quantity="8"></space><w n="14.1">Tel</w> <w n="14.2">est</w> <w n="14.3">chez</w> <w n="14.4">nous</w> <w n="14.5">le</w> <w n="14.6">train</w> <w n="14.7">des</w> <w n="14.8">choses</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Toujours</w> <w n="15.2">les</w> <w n="15.3">grands</w> <w n="15.4">effets</w> <w n="15.5">ont</w> <w n="15.6">de</w> <w n="15.7">petites</w> <w n="15.8">causes</w>.</l>
						<l n="16" num="1.16"><space unit="char" quantity="8"></space><w n="16.1">Babet</w> <w n="16.2">vieillit</w>, <w n="16.3">le</w> <w n="16.4">cuir</w> <w n="16.5">baissa</w> ;</l>
						<l n="17" num="1.17"><space unit="char" quantity="8"></space><w n="17.1">Adieu</w> <w n="17.2">vous</w> <w n="17.3">dit</w> <w n="17.4">joli</w> <w n="17.5">visage</w>,</l>
						<l n="18" num="1.18"><space unit="char" quantity="8"></space><w n="18.1">Taille</w> <w n="18.2">fine</w>, <w n="18.3">élégant</w> <w n="18.4">corsage</w>,</l>
						<l n="19" num="1.19"><space unit="char" quantity="8"></space><w n="19.1">Enfin</w> <w n="19.2">adieu</w> <w n="19.3">tous</w> <w n="19.4">ses</w> <w n="19.5">appas</w> !</l>
						<l n="20" num="1.20"><w n="20.1">L</w>’<w n="20.2">âge</w> <w n="20.3">a</w> <w n="20.4">beau</w> <w n="20.5">nous</w> <w n="20.6">rider</w>, <w n="20.7">il</w> <w n="20.8">ne</w> <w n="20.9">nous</w> <w n="20.10">change</w> <w n="20.11">pas</w>.</l>
						<l n="21" num="1.21"><w n="21.1">On</w> <w n="21.2">se</w> <w n="21.3">travaille</w> <w n="21.4">en</w> <w n="21.5">vain</w>, <w n="21.6">le</w> <w n="21.7">goût</w> <w n="21.8">reste</w> <w n="21.9">le</w> <w n="21.10">même</w>.</l>
						<l n="22" num="1.22"><space unit="char" quantity="8"></space><w n="22.1">Celui</w> <w n="22.2">de</w> <w n="22.3">Babet</w> <w n="22.4">pour</w> <w n="22.5">l</w>’<w n="22.6">amour</w>,</l>
						<l n="23" num="1.23"><w n="23.1">Bien</w> <w n="23.2">loin</w> <w n="23.3">de</w> <w n="23.4">s</w>’<w n="23.5">affoiblir</w>, <w n="23.6">avoit</w> <w n="23.7">crû</w> <w n="23.8">chaque</w> <w n="23.9">jour</w>.</l>
						<l n="24" num="1.24"><space unit="char" quantity="8"></space><w n="24.1">Que</w> <w n="24.2">faire</w> <w n="24.3">en</w> <w n="24.4">ce</w> <w n="24.5">besoin</w> <w n="24.6">extrême</w> ?</l>
						<l n="25" num="1.25"><w n="25.1">Le</w> <w n="25.2">temps</w> <w n="25.3">de</w> <w n="25.4">but</w> <w n="25.5">à</w> <w n="25.6">but</w> <w n="25.7">étoit</w> <w n="25.8">plus</w> <w n="25.9">que</w> <w n="25.10">passé</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Il</w> <w n="26.2">fallut</w> <w n="26.3">des</w> <w n="26.4">souliers</w> <w n="26.5">implorer</w> <w n="26.6">l</w>’<w n="26.7">assistance</w> :</l>
						<l n="27" num="1.27"><space unit="char" quantity="8"></space><w n="27.1">Grâce</w> <w n="27.2">à</w> <w n="27.3">sa</w> <w n="27.4">sage</w> <w n="27.5">prévoyance</w>,</l>
						<l n="28" num="1.28"><w n="28.1">L</w>’<w n="28.2">Amant</w> <w n="28.3">venu</w> <w n="28.4">nus</w> <w n="28.5">pieds</w>, <w n="28.6">s</w>’<w n="28.7">en</w> <w n="28.8">retournait</w> <w n="28.9">chaussé</w> ;</l>
						<l n="29" num="1.29"><w n="29.1">Elle</w> <w n="29.2">habilla</w> <w n="29.3">par</w> <w n="29.4">bas</w> <w n="29.5">les</w> <w n="29.6">deux</w> <w n="29.7">tiers</w> <w n="29.8">de</w> <w n="29.9">Florence</w>.</l>
						<l n="30" num="1.30"><w n="30.1">Sur</w> <w n="30.2">quoi</w> <w n="30.3">certain</w> <w n="30.4">voisin</w>, <w n="30.5">d</w>’<w n="30.6">elle</w> <w n="30.7">un</w> <w n="30.8">jour</w> <w n="30.9">s</w>’<w n="30.10">enquérant</w></l>
						<l n="31" num="1.31"><w n="31.1">De</w> <w n="31.2">ce</w> <w n="31.3">tas</w> <w n="31.4">de</w> <w n="31.5">souliers</w> <w n="31.6">qu</w>’<w n="31.7">elle</w> <w n="31.8">alloit</w> <w n="31.9">répandant</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Babet</w> <w n="32.2">que</w> <w n="32.3">le</w> <w n="32.4">métier</w> <w n="32.5">n</w>’<w n="32.6">avait</w> <w n="32.7">point</w> <w n="32.8">rendu</w> <w n="32.9">fausse</w>,</l>
						<l n="33" num="1.33"><w n="33.1">Lui</w> <w n="33.2">dit</w> : ‒ « <w n="33.3">Mon</w> <w n="33.4">cher</w> <w n="33.5">ami</w>, <w n="33.6">l</w>’<w n="33.7">hiver</w> <w n="33.8">vit</w> <w n="33.9">de</w> <w n="33.10">l</w>’<w n="33.11">été</w>.</l>
						<l n="34" num="1.34">» <w n="34.1">Je</w> <w n="34.2">rends</w> <w n="34.3">à</w> <w n="34.4">mes</w> <w n="34.5">Amants</w> <w n="34.6">ce</w> <w n="34.7">qu</w>’<w n="34.8">ils</w> <w n="34.9">m</w>’<w n="34.10">avoient</w> <w n="34.11">prêté</w> :</l>
						<l n="35" num="1.35"><space unit="char" quantity="8"></space>» <w n="35.1">Je</w> <w n="35.2">les</w> <w n="35.3">déchaussois</w>, <w n="35.4">je</w> <w n="35.5">les</w> <w n="35.6">chausse</w>. »</l>
					</lg>
				</div></body></text></TEI>