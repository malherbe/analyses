<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO58">
					<head type="main">LE DÉLUGE</head>
					<lg n="1">
						<l n="1" num="1.1"><space unit="char" quantity="8"></space>« <w n="1.1">Cap</w> <w n="1.2">dé</w> <w n="1.3">bious</w> ! » <w n="1.4">disoit</w> <w n="1.5">un</w> <w n="1.6">Gascon</w></l>
						<l n="2" num="1.2"><space unit="char" quantity="4"></space><w n="2.1">A</w> <w n="2.2">sa</w> <w n="2.3">moitié</w>, <w n="2.4">qui</w> <w n="2.5">faisoit</w> <w n="2.6">la</w> <w n="2.7">niaise</w>,</l>
						<l n="3" num="1.3"><space unit="char" quantity="8"></space>« <w n="3.1">Pour</w> <w n="3.2">la</w> <w n="3.3">première</w> <w n="3.4">fois</w>, <w n="3.5">Fanchon</w>,</l>
						<l n="4" num="1.4">» <w n="4.1">Il</w> <w n="4.2">me</w> <w n="4.3">semble</w> <w n="4.4">qu</w>’<w n="4.5">ici</w> <w n="4.6">je</w> <w n="4.7">suis</w> <w n="4.8">bien</w> <w n="4.9">à</w> <w n="4.10">mon</w> <w n="4.11">aise</w>.</l>
						<l n="5" num="1.5">» ‒ <w n="5.1">Las</w> ! » <w n="5.2">dit</w>-<w n="5.3">elle</w>, « <w n="5.4">mon</w> <w n="5.5">cher</w>, <w n="5.6">je</w> <w n="5.7">suis</w> <w n="5.8">neuve</w> <w n="5.9">à</w> <w n="5.10">tel</w> <w n="5.11">jeu</w> ;</l>
						<l n="6" num="1.6">» <w n="6.1">Appelez</w> <w n="6.2">un</w> <w n="6.3">Frater</w>, <w n="6.4">et</w> <w n="6.5">je</w> <w n="6.6">le</w> <w n="6.7">ferai</w> <w n="6.8">juge</w></l>
						<l n="7" num="1.7">» <w n="7.1">Que</w> <w n="7.2">mes</w> <w n="7.3">eaux</w> <w n="7.4">seulement</w> <w n="7.5">ont</w> <w n="7.6">passé</w> <w n="7.7">par</w> <w n="7.8">ce</w> <w n="7.9">lieu</w>.</l>
						<l n="8" num="1.8">» ‒ <w n="8.1">Vos</w> <w n="8.2">eaux</w> ! <w n="8.3">sandis</w> ! » <w n="8.4">repart</w> <w n="8.5">le</w> <w n="8.6">Gascon</w> <w n="8.7">qui</w> <w n="8.8">prend</w> <w n="8.9">feu</w> ;</l>
						<l n="9" num="1.9"><space unit="char" quantity="8"></space>« <w n="9.1">Dites</w> <w n="9.2">donc</w> <w n="9.3">les</w> <w n="9.4">eaux</w> <w n="9.5">du</w> <w n="9.6">déluge</w>. »</l>
					</lg>
				</div></body></text></TEI>