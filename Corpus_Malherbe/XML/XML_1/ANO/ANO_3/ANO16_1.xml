<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">Le petit-neveu de Grécourt</title>
				<title type="sub_2">ou</title>
				<title type="sub_1">Étrennes gaillardes</title>
				<title type="medium">Édition électronique</title>
				<author key="ANO">
					<name type="anonymous">(anonyme)</name>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Mise en forme XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				</titleStmt>
			<extent>2164 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine</ref> 
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2021">2021</date>
				<idno type="local">ANO_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
						<author>(anonyme)</author>
					</titleStmt>
					<publicationStmt>
						<publisher>Project Gutenberg</publisher>
						<idno type="URI">https://www.gutenberg.org/ebooks/60896</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Le petit-neveu de Grécourt ou Étrennes gaillardes</title>
								<author>(anonyme)</author>
								<idno type="URI">https://archive.org/details/lepetitneveudegr00pari/mode/2up</idno>
								<imprint>
									<pubPlace>PARIS</pubPlace>
									<publisher>Isidore LISEUX, Éditeur</publisher>
									<date when="1883">1883</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1883">1883</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Les parties liminaires en prose ne sont pas reprises dans la présente édition.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<correction>
					<p>L’orthographe du texte a été vérifié avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant une ponctuation forte).</p>
					<p>Les faux points de suspension ont été remplacés par de vrais points de suspension (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2021-08-03" who="RR">Révision et validation XML-TEI (TEI_Corpus_Malherbe_1.3.xsd)</change>
				<change when="2021-08-03" who="RR">Insertion automatique des balises des retraits de vers (après analyse de la longueur métrique).</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="ANO16">
					<head type="main">LE PAIEMENT D’AVANCE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Dans</w> <w n="1.2">Paris</w> <w n="1.3">plus</w> <w n="1.4">d</w>’<w n="1.5">un</w> <w n="1.6">Bourgeois</w>,</l>
						<l n="2" num="1.2"><w n="2.1">N</w>’<w n="2.2">ayant</w> <w n="2.3">maîtresse</w> <w n="2.4">ni</w> <w n="2.5">femme</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Pour</w> <w n="3.2">un</w> <w n="3.3">écu</w> <w n="3.4">tous</w> <w n="3.5">les</w> <w n="3.6">mois</w></l>
						<l n="4" num="1.4"><w n="4.1">S</w>’<w n="4.2">en</w> <w n="4.3">va</w> <w n="4.4">rafraîchir</w> <w n="4.5">sa</w> <w n="4.6">flamme</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Témoin</w> <w n="5.2">Monsieur</w> <w n="5.3">Rogaton</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">sait</w> <w n="6.3">où</w> <w n="6.4">le</w> <w n="6.5">bât</w> <w n="6.6">le</w> <w n="6.7">blesse</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">de</w> <w n="7.3">temps</w> <w n="7.4">en</w> <w n="7.5">temps</w>, <w n="7.6">dit</w>-<w n="7.7">on</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Cède</w> <w n="8.2">à</w> <w n="8.3">l</w>’<w n="8.4">humaine</w> <w n="8.5">faiblesse</w>.</l>
						<l n="9" num="1.9"><w n="9.1">L</w>’<w n="9.2">autre</w> <w n="9.3">jour</w> <w n="9.4">une</w> <w n="9.5">drôlesse</w></l>
						<l n="10" num="1.10"><w n="10.1">L</w>’<w n="10.2">aperçut</w> <w n="10.3">de</w> <w n="10.4">son</w> <w n="10.5">balcon</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">la</w> <w n="11.3">voilà</w> <w n="11.4">qui</w> <w n="11.5">l</w>’<w n="11.6">invite</w></l>
						<l n="12" num="1.12"><w n="12.1">Par</w> <w n="12.2">un</w> <hi rend="ital"><subst hand="RR" reason="analysis" type="phonemization"><del>st</del><add rend="hidden"><w n="12.3">stı</w></add></subst>, <subst hand="RR" reason="analysis" type="phonemization"><del>st</del><add rend="hidden"><w n="12.4">stı</w></add></subst></hi> <w n="12.5">redoublé</w>.</l>
						<l n="13" num="1.13"><w n="13.1">Mon</w> <w n="13.2">homme</w> <w n="13.3">de</w> <w n="13.4">monter</w> <w n="13.5">vite</w></l>
						<l n="14" num="1.14"><w n="14.1">Sitôt</w> <w n="14.2">qu</w>’<w n="14.3">il</w> <w n="14.4">est</w> <w n="14.5">appelé</w> ;</l>
						<l n="15" num="1.15"><w n="15.1">Il</w> <w n="15.2">entre</w> ; <w n="15.3">elle</w> <w n="15.4">de</w> <w n="15.5">lui</w> <w n="15.6">dire</w> :</l>
						<l n="16" num="1.16">« <w n="16.1">Mon</w> <w n="16.2">fils</w>, <w n="16.3">sois</w> <w n="16.4">le</w> <w n="16.5">bienvenu</w> ;</l>
						<l n="17" num="1.17">» <w n="17.1">C</w>’<w n="17.2">est</w> <w n="17.3">moi</w> <w n="17.4">qu</w>’<w n="17.5">on</w> <w n="17.6">nomme</w> <w n="17.7">Zelmire</w> :</l>
						<l n="18" num="1.18">» <w n="18.1">Ce</w> <w n="18.2">nom</w>, <w n="18.3">je</w> <w n="18.4">crois</w>, <w n="18.5">est</w> <w n="18.6">connu</w>.</l>
						<l n="19" num="1.19">» <w n="19.1">Ici</w> <w n="19.2">l</w>’<w n="19.3">on</w> <w n="19.4">trouve</w> <w n="19.5">à</w> <w n="19.6">sa</w> <w n="19.7">guise</w>,</l>
						<l n="20" num="1.20">» <w n="20.1">Blancheur</w>, <w n="20.2">fraîcheur</w>, <w n="20.3">fermeté</w> ;</l>
						<l n="21" num="1.21">» <w n="21.1">Ces</w> <w n="21.2">trois</w> <w n="21.3">mots</w> <w n="21.4">sont</w> <w n="21.5">ma</w> <w n="21.6">devise</w>.</l>
						<l n="22" num="1.22">» <w n="22.1">Je</w> <w n="22.2">suis</w> <w n="22.3">en</w> <w n="22.4">bonne</w> <w n="22.5">santé</w> ;</l>
						<l n="23" num="1.23">» <w n="23.1">Dans</w> <w n="23.2">mes</w> <w n="23.3">bras</w> <w n="23.4">tout</w> <w n="23.5">Paris</w> <w n="23.6">tombe</w> ;</l>
						<l n="24" num="1.24">» <w n="24.1">J</w>’<w n="24.2">ai</w> <w n="24.3">la</w> <w n="24.4">gorge</w> <w n="24.5">de</w> <w n="24.6">Duté</w></l>
						<l n="25" num="1.25">» <w n="25.1">Et</w> <w n="25.2">les</w> <w n="25.3">fesses</w> <w n="25.4">de</w> <w n="25.5">Colombe</w>.</l>
						<l n="26" num="1.26">» <w n="26.1">Viens</w> <w n="26.2">t</w>’<w n="26.3">asseoir</w> <w n="26.4">à</w> <w n="26.5">mon</w> <w n="26.6">côté</w></l>
						<l n="27" num="1.27">» <w n="27.1">Et</w> <w n="27.2">mets</w>-<w n="27.3">moi</w> <w n="27.4">vite</w> <w n="27.5">à</w> <w n="27.6">l</w>’<w n="27.7">épreuve</w> ;</l>
						<l n="28" num="1.28">» <w n="28.1">Mais</w> <w n="28.2">auparavant</w> <w n="28.3">fais</w> <w n="28.4">preuve</w></l>
						<l n="29" num="1.29">» <w n="29.1">De</w> <w n="29.2">ta</w> <w n="29.3">générosité</w>.</l>
						<l n="30" num="1.30">» ‒ <w n="30.1">Dis</w>-<w n="30.2">moi</w> <w n="30.3">combien</w> <w n="30.4">tu</w> <w n="30.5">demandes</w> ?</l>
						<l n="31" num="1.31">» ‒ <w n="31.1">Combien</w> ? <w n="31.2">Six</w> <w n="31.3">livres</w>, <w n="31.4">mon</w> <w n="31.5">cher</w>,</l>
						<l n="32" num="1.32">» <w n="32.1">Et</w> <w n="32.2">douze</w> <w n="32.3">si</w> <w n="32.4">tu</w> <w n="32.5">marchandes</w> ;</l>
						<l n="33" num="1.33">» <w n="33.1">C</w>’<w n="33.2">est</w> <w n="33.3">un</w> <w n="33.4">prix</w> <w n="33.5">fait</w> <w n="33.6">en</w> <w n="33.7">hiver</w>.</l>
						<l n="34" num="1.34"><w n="34.1">Mons</w> <w n="34.2">Rogaton</w> <w n="34.3">sur</w> <w n="34.4">la</w> <w n="34.5">bouche</w></l>
						<l n="35" num="1.35"><w n="35.1">Un</w> <w n="35.2">gros</w> <w n="35.3">baiser</w> <w n="35.4">lui</w> <w n="35.5">colla</w> ;</l>
						<l n="36" num="1.36"><w n="36.1">Zelmire</w>, <w n="36.2">d</w>’<w n="36.3">un</w> <w n="36.4">air</w> <w n="36.5">farouche</w> :</l>
						<l n="37" num="1.37">‒ « <w n="37.1">Il</w> <w n="37.2">faut</w> <w n="37.3">mettre</w> <w n="37.4">six</w> <w n="37.5">francs</w> <w n="37.6">là</w>,</l>
						<l n="38" num="1.38">» <w n="38.1">Et</w> <w n="38.2">sois</w> <w n="38.3">sûr</w> <w n="38.4">que</w> <w n="38.5">sans</w> <w n="38.6">cela</w></l>
						<l n="39" num="1.39">» <w n="39.1">Je</w> <w n="39.2">ne</w> <w n="39.3">veux</w> <w n="39.4">pas</w> <w n="39.5">qu</w>’<w n="39.6">on</w> <w n="39.7">me</w> <w n="39.8">touche</w>.</l>
						<l n="40" num="1.40">» <w n="40.1">Dépêchons</w>, <w n="40.2">il</w> <w n="40.3">se</w> <w n="40.4">fait</w> <w n="40.5">tard</w> ;</l>
						<l n="41" num="1.41">» <w n="41.1">Six</w> <w n="41.2">francs</w>, <w n="41.3">ou</w> <w n="41.4">bats</w> <w n="41.5">en</w> <w n="41.6">retraite</w>. »</l>
						<l n="42" num="1.42"><w n="42.1">Rogaton</w> <w n="42.2">les</w> <w n="42.3">lui</w> <w n="42.4">départ</w>.</l>
						<l n="43" num="1.43"><w n="43.1">La</w> <w n="43.2">Commère</w>, <w n="43.3">satisfaite</w>,</l>
						<l n="44" num="1.44"><w n="44.1">Ses</w> <w n="44.2">charmes</w> <w n="44.3">lors</w> <w n="44.4">dévoila</w>,</l>
						<l n="45" num="1.45"><w n="45.1">En</w> <w n="45.2">lui</w> <w n="45.3">disant</w> : « <w n="45.4">Me</w> <w n="45.5">voilà</w></l>
						<l n="46" num="1.46">» <w n="46.1">Comme</w> <w n="46.2">le</w> <w n="46.3">bon</w> <w n="46.4">Dieu</w> <w n="46.5">m</w>’<w n="46.6">a</w> <w n="46.7">faite</w>.</l>
						<l n="47" num="1.47">» ‒ <w n="47.1">Ah</w> ! <w n="47.2">Ciel</w> ! <w n="47.3">je</w> <w n="47.4">suis</w> <w n="47.5">infecté</w> !</l>
						<l n="48" num="1.48">» <w n="48.1">Ici</w> <w n="48.2">que</w> <w n="48.3">n</w>’<w n="48.4">ai</w>-<w n="48.5">je</w> <w n="48.6">apporté</w></l>
						<l n="49" num="1.49">» <w n="49.1">De</w> <w n="49.2">l</w>’<w n="49.3">ambre</w> <w n="49.4">ou</w> <w n="49.5">de</w> <w n="49.6">la</w> <w n="49.7">civette</w> ?</l>
						<l n="50" num="1.50">» <w n="50.1">Cache</w>, <w n="50.2">cache</w> <w n="50.3">tes</w> <w n="50.4">attraits</w>, »</l>
						<l n="51" num="1.51"><w n="51.1">Dit</w> <w n="51.2">l</w>’<w n="51.3">autre</w>, « <w n="51.4">je</w> <w n="51.5">gagerois</w></l>
						<l n="52" num="1.52">» <w n="52.1">Que</w> <w n="52.2">tu</w> <w n="52.3">n</w>’<w n="52.4">as</w> <w n="52.5">pas</w> <w n="52.6">fait</w> <w n="52.7">toilette</w>.</l>
						<l n="53" num="1.53">» <w n="53.1">Fi</w> ! ‒ <w n="53.2">Si</w> <w n="53.3">tu</w> <w n="53.4">n</w>’<w n="53.5">es</w> <w n="53.6">pas</w> <w n="53.7">content</w>,</l>
						<l n="54" num="1.54">» <w n="54.1">Tu</w> <w n="54.2">peux</w> <w n="54.3">regagner</w> <w n="54.4">la</w> <w n="54.5">porte</w>.</l>
						<l n="55" num="1.55">» ‒ <w n="55.1">Eh</w> <w n="55.2">bien</w> ! <w n="55.3">rends</w>-<w n="55.4">moi</w> <w n="55.5">vitement</w></l>
						<l n="56" num="1.56">» <w n="56.1">Mes</w> <w n="56.2">six</w> <w n="56.3">francs</w>, <w n="56.4">et</w> <w n="56.5">que</w> <w n="56.6">je</w> <w n="56.7">sorte</w>.</l>
						<l n="57" num="1.57">» ‒ <w n="57.1">Tes</w> <w n="57.2">six</w> <w n="57.3">francs</w> ? <w n="57.4">oh</w> ! <w n="57.5">doucement</w> :</l>
						<l n="58" num="1.58">» <w n="58.1">Je</w> <w n="58.2">ne</w> <w n="58.3">fais</w> <w n="58.4">point</w> <w n="58.5">de</w> <w n="58.6">corvée</w> ;</l>
						<l n="59" num="1.59">» <w n="59.1">On</w> <w n="59.2">ne</w> <w n="59.3">rend</w> <w n="59.4">jamais</w> <w n="59.5">l</w>’<w n="59.6">argent</w></l>
						<l n="60" num="1.60">» <w n="60.1">Lorsque</w> <w n="60.2">la</w> <w n="60.3">toile</w> <w n="60.4">est</w> <w n="60.5">levée</w>. »</l>
					</lg>
				</div></body></text></TEI>