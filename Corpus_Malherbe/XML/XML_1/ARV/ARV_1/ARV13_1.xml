<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ARV">
					<name>
						<forename>Félix</forename>
						<surname>ARVERS</surname>
					</name>
					<date from="1806" to="1850">1806-1850</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4560 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ARV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/felixarverspoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Félix Arvers: mes heures perdues. Pièces inédites</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">http://www.archive.org/details/posiesdeflixOOarve </idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Félix Arvers</author>
								<imprint>
									<publisher>H. Floury, éditeur</publisher>
									<date when="1900">1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MES HEURES PERDUES</head><div type="poem" key="ARV13">
					<head type="main">A Madame <hi rend="sup">***</hi></head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Madame</w>, <w n="1.2">croyez</w>-<w n="1.3">moi</w> ; <w n="1.4">bien</w> <w n="1.5">qu</w>’<w n="1.6">une</w> <w n="1.7">autre</w> <w n="1.8">patrie</w></l>
						<l n="2" num="1.2"><w n="2.1">Vous</w> <w n="2.2">ait</w> <w n="2.3">ravie</w> <w n="2.4">à</w> <w n="2.5">ceux</w> <w n="2.6">qui</w> <w n="2.7">vous</w> <w n="2.8">ont</w> <w n="2.9">tant</w> <w n="2.10">chérie</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Allez</w>, <w n="3.2">consolez</w>-<w n="3.3">vous</w>, <w n="3.4">ne</w> <w n="3.5">pleurez</w> <w n="3.6">point</w> <w n="3.7">ainsi</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Votre</w> <w n="4.2">corps</w> <w n="4.3">est</w> <w n="4.4">là</w>-<w n="4.5">bas</w>, <w n="4.6">mais</w> <w n="4.7">votre</w> <w n="4.8">âme</w> <w n="4.9">est</w> <w n="4.10">ici</w> :</l>
						<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">la</w> <w n="5.4">moindre</w> <w n="5.5">moitié</w> <w n="5.6">que</w> <w n="5.7">l</w>’<w n="5.8">exil</w> <w n="5.9">nous</w> <w n="5.10">a</w> <w n="5.11">prise</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">La</w> <w n="6.2">tige</w> <w n="6.3">s</w>’<w n="6.4">est</w> <w n="6.5">rompue</w> <w n="6.6">au</w> <w n="6.7">souffle</w> <w n="6.8">de</w> <w n="6.9">la</w> <w n="6.10">brise</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Mais</w> <w n="7.2">l</w>’<w n="7.3">ouragan</w> <w n="7.4">jaloux</w>, <w n="7.5">qui</w> <w n="7.6">ternit</w> <w n="7.7">sa</w> <w n="7.8">splendeur</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Jeta</w> <w n="8.2">la</w> <w n="8.3">fleur</w> <w n="8.4">au</w> <w n="8.5">vent</w> <w n="8.6">et</w> <w n="8.7">nous</w> <w n="8.8">laissa</w> <w n="8.9">l</w>’<w n="8.10">odeur</w>.</l>
						<l n="9" num="1.9"><w n="9.1">A</w> <w n="9.2">moins</w>, <w n="9.3">à</w> <w n="9.4">moins</w> <w n="9.5">pourtant</w> <w n="9.6">que</w> <w n="9.7">dans</w> <w n="9.8">cette</w> <w n="9.9">retraite</w></l>
						<l n="10" num="1.10"><w n="10.1">Vous</w> <w n="10.2">n</w>’<w n="10.3">ayez</w> <w n="10.4">apporté</w> <w n="10.5">quelque</w> <w n="10.6">peine</w> <w n="10.7">secrète</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Et</w> <w n="11.2">que</w> <w n="11.3">là</w>, <w n="11.4">comme</w> <w n="11.5">ici</w>, <w n="11.6">quelque</w> <w n="11.7">ennui</w> <w n="11.8">voyageur</w></l>
						<l n="12" num="1.12"><w n="12.1">Se</w> <w n="12.2">cramponne</w> <w n="12.3">à</w> <w n="12.4">votre</w> <w n="12.5">âme</w>, <w n="12.6">inflexible</w> <w n="12.7">et</w> <w n="12.8">rongeur</w> :</l>
						<l n="13" num="1.13"><w n="13.1">Car</w> <w n="13.2">bien</w> <w n="13.3">souvent</w>, <w n="13.4">un</w> <w n="13.5">mot</w>, <w n="13.6">un</w> <w n="13.7">geste</w> <w n="13.8">involontaire</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Des</w> <w n="14.2">maux</w> <w n="14.3">que</w> <w n="14.4">vous</w> <w n="14.5">souffrez</w> <w n="14.6">a</w> <w n="14.7">trahi</w> <w n="14.8">le</w> <w n="14.9">mystère</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Et</w> <w n="15.2">j</w>’<w n="15.3">ai</w> <w n="15.4">vu</w> <w n="15.5">sous</w> <w n="15.6">ces</w> <w n="15.7">pleurs</w> <w n="15.8">et</w> <w n="15.9">cet</w> <w n="15.10">abattement</w></l>
						<l n="16" num="1.16"><w n="16.1">La</w> <w n="16.2">blessure</w> <w n="16.3">d</w>’<w n="16.4">un</w> <w n="16.5">cœur</w> <w n="16.6">qui</w> <w n="16.7">saigne</w> <w n="16.8">longuement</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Vous</w> <w n="17.2">avez</w> <w n="17.3">épuisé</w> <w n="17.4">tout</w> <w n="17.5">ce</w> <w n="17.6">que</w> <w n="17.7">la</w> <w n="17.8">nature</w></l>
						<l n="18" num="1.18"><w n="18.1">A</w> <w n="18.2">permis</w> <w n="18.3">de</w> <w n="18.4">bonheur</w> <w n="18.5">à</w> <w n="18.6">l</w>’<w n="18.7">humble</w> <w n="18.8">créature</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Et</w> <w n="19.2">votre</w> <w n="19.3">pauvre</w> <w n="19.4">cœur</w>, <w n="19.5">lentement</w> <w n="19.6">consumé</w>,</l>
						<l n="20" num="1.20"><w n="20.1">S</w>’<w n="20.2">est</w> <w n="20.3">fait</w> <w n="20.4">vieux</w> <w n="20.5">en</w> <w n="20.6">un</w> <w n="20.7">jour</w>, <w n="20.8">pour</w> <w n="20.9">avoir</w> <w n="20.10">trop</w> <w n="20.11">aimé</w> :</l>
						<l n="21" num="1.21"><w n="21.1">Vous</w> <w n="21.2">seule</w>, <w n="21.3">n</w>’<w n="21.4">est</w>-<w n="21.5">ce</w> <w n="21.6">pas</w>, <w n="21.7">vous</w> <w n="21.8">êtes</w> <w n="21.9">demeurée</w></l>
						<l n="22" num="1.22"><w n="22.1">Fidèle</w> <w n="22.2">à</w> <w n="22.3">cette</w> <w n="22.4">amour</w> <w n="22.5">que</w> <w n="22.6">deux</w> <w n="22.7">avaient</w> <w n="22.8">jurée</w>.</l>
						<l n="23" num="1.23"><w n="23.1">Et</w> <w n="23.2">seule</w>, <w n="23.3">jusqu</w>’<w n="23.4">au</w> <w n="23.5">bout</w>, <w n="23.6">avez</w> <w n="23.7">pieusement</w></l>
						<l n="24" num="1.24"><w n="24.1">Accompli</w> <w n="24.2">votre</w> <w n="24.3">part</w> <w n="24.4">de</w> <w n="24.5">ce</w> <w n="24.6">double</w> <w n="24.7">serment</w>.</l>
						<l n="25" num="1.25"><w n="25.1">Consolez</w>-<w n="25.2">vous</w> <w n="25.3">encor</w> ; <w n="25.4">car</w> <w n="25.5">vous</w> <w n="25.6">avez</w>. <w n="25.7">Madame</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Achevé</w> <w n="26.2">saintement</w> <w n="26.3">votre</w> <w n="26.4">rôle</w> <w n="26.5">de</w> <w n="26.6">femme</w> ;</l>
						<l n="27" num="1.27"><w n="27.1">Vous</w> <w n="27.2">avez</w> <w n="27.3">ici</w>-<w n="27.4">bas</w> <w n="27.5">rempli</w> <w n="27.6">la</w> <w n="27.7">mission</w></l>
						<l n="28" num="1.28"><w n="28.1">Faite</w> <w n="28.2">à</w> <w n="28.3">l</w>’<w n="28.4">être</w> <w n="28.5">créé</w> <w n="28.6">par</w> <w n="28.7">la</w> <w n="28.8">création</w>.</l>
						<l n="29" num="1.29"><w n="29.1">Aimer</w>, <w n="29.2">et</w> <w n="29.3">puis</w> <w n="29.4">souffrir</w>, <w n="29.5">voilà</w> <w n="29.6">toute</w> <w n="29.7">la</w> <w n="29.8">vie</w> :</l>
						<l n="30" num="1.30"><w n="30.1">Dieu</w> <w n="30.2">vous</w> <w n="30.3">donna</w> <w n="30.4">long</w>-<w n="30.5">temps</w> <w n="30.6">des</w> <w n="30.7">jours</w> <w n="30.8">dignes</w> <w n="30.9">d</w>’<w n="30.10">envie</w></l>
						<l n="31" num="1.31"><w n="31.1">Aujourd</w>’<w n="31.2">hui</w>, <w n="31.3">c</w>’<w n="31.4">est</w> <w n="31.5">la</w> <w n="31.6">loi</w>, <w n="31.7">vous</w> <w n="31.8">payez</w> <w n="31.9">chèrement</w></l>
						<l n="32" num="1.32"><w n="32.1">Par</w> <w n="32.2">des</w> <w n="32.3">larmes</w> <w n="32.4">sans</w> <w n="32.5">fin</w> <w n="32.6">ce</w> <w n="32.7">bonheur</w> <w n="32.8">d</w>’<w n="32.9">un</w> <w n="32.10">moment</w>.</l>
						<l n="33" num="1.33"><w n="33.1">Certes</w>, <w n="33.2">tant</w> <w n="33.3">de</w> <w n="33.4">chagrins</w>, <w n="33.5">et</w> <w n="33.6">tant</w> <w n="33.7">de</w> <w n="33.8">nuits</w> <w n="33.9">passées</w></l>
						<l n="34" num="1.34"><w n="34.1">A</w> <w n="34.2">couver</w> <w n="34.3">tristement</w> <w n="34.4">de</w> <w n="34.5">lugubres</w> <w n="34.6">pensées</w>.</l>
						<l n="35" num="1.35"><w n="35.1">Tant</w> <w n="35.2">et</w> <w n="35.3">de</w> <w n="35.4">si</w> <w n="35.5">longs</w> <w n="35.6">pleurs</w> <w n="35.7">n</w>’<w n="35.8">ont</w> <w n="35.9">pas</w> <w n="35.10">si</w> <w n="35.11">bien</w> <w n="35.12">éteint</w></l>
						<l n="36" num="1.36"><w n="36.1">Les</w> <w n="36.2">éclairs</w> <w n="36.3">de</w> <w n="36.4">vos</w> <w n="36.5">yeux</w> <w n="36.6">et</w> <w n="36.7">pâli</w> <w n="36.8">votre</w> <w n="36.9">teint</w>.</l>
						<l n="37" num="1.37"><w n="37.1">Que</w> <w n="37.2">mainte</w> <w n="37.3">ambition</w> <w n="37.4">ne</w> <w n="37.5">se</w> <w n="37.6">fût</w> <w n="37.7">contentée</w>,</l>
						<l n="38" num="1.38"><w n="38.1">Madame</w>, <w n="38.2">de</w> <w n="38.3">la</w> <w n="38.4">part</w> <w n="38.5">qui</w> <w n="38.6">vous</w> <w n="38.7">en</w> <w n="38.8">est</w> <w n="38.9">restée</w>.</l>
						<l n="39" num="1.39"><w n="39.1">Et</w> <w n="39.2">que</w> <w n="39.3">plus</w> <w n="39.4">d</w>’<w n="39.5">un</w> <w n="39.6">encor</w> <w n="39.7">n</w>’<w n="39.8">y</w> <w n="39.9">laissât</w> <w n="39.10">sa</w> <w n="39.11">raison</w>.</l>
						<l n="40" num="1.40"><w n="40.1">Ainsi</w> <w n="40.2">qu</w>’<w n="40.3">aux</w> <w n="40.4">églantiers</w> <w n="40.5">l</w>’<w n="40.6">agneau</w> <w n="40.7">fait</w> <w n="40.8">sa</w> <w n="40.9">toison</w>.</l>
						<l n="41" num="1.41"><w n="41.1">Mais</w> <w n="41.2">votre</w> <w n="41.3">âme</w> <w n="41.4">est</w> <w n="41.5">plus</w> <w n="41.6">haute</w>, <w n="41.7">et</w> <w n="41.8">ne</w> <w n="41.9">s</w>’<w n="41.10">arrange</w> <w n="41.11">guère</w></l>
						<l n="42" num="1.42"><w n="42.1">Des</w> <w n="42.2">consolations</w> <w n="42.3">d</w>’<w n="42.4">un</w> <w n="42.5">bonheur</w> <w n="42.6">si</w> <w n="42.7">vulgaire</w> ;</l>
						<l n="43" num="1.43"><w n="43.1">Madame</w>, <w n="43.2">ce</w> <w n="43.3">n</w>’<w n="43.4">est</w> <w n="43.5">point</w> <w n="43.6">un</w> <w n="43.7">vase</w> <w n="43.8">où</w>, <w n="43.9">tour</w> <w n="43.10">à</w> <w n="43.11">tour</w>,</l>
						<l n="44" num="1.44"><w n="44.1">Chacun</w> <w n="44.2">puisse</w> <w n="44.3">étancher</w> <w n="44.4">la</w> <w n="44.5">soif</w> <w n="44.6">de</w> <w n="44.7">son</w> <w n="44.8">amour</w> ;</l>
						<l n="45" num="1.45"><w n="45.1">Mais</w> <w n="45.2">Dieu</w> <w n="45.3">la</w> <w n="45.4">fit</w> <w n="45.5">semblable</w> <w n="45.6">à</w> <w n="45.7">la</w> <w n="45.8">coupe</w> <w n="45.9">choisie</w>,</l>
						<l n="46" num="1.46"><w n="46.1">Dans</w> <w n="46.2">les</w> <w n="46.3">plus</w> <w n="46.4">purs</w> <w n="46.5">cristaux</w> <w n="46.6">des</w> <w n="46.7">rochers</w> <w n="46.8">de</w> <w n="46.9">l</w>’<w n="46.10">Asie</w>,</l>
						<l n="47" num="1.47"><w n="47.1">Où</w> <w n="47.2">l</w>’<w n="47.3">on</w> <w n="47.4">verse</w> <w n="47.5">au</w> <w n="47.6">sultan</w> <w n="47.7">le</w> <w n="47.8">Chypre</w> <w n="47.9">et</w> <w n="47.10">le</w> <w n="47.11">Xérès</w>,</l>
						<l n="48" num="1.48"><w n="48.1">Qui</w> <w n="48.2">ne</w> <w n="48.3">sert</w> <w n="48.4">qu</w>’<w n="48.5">une</w> <w n="48.6">fois</w>, <w n="48.7">et</w> <w n="48.8">qui</w> <w n="48.9">se</w> <w n="48.10">brise</w> <w n="48.11">après</w>.</l>
						<l n="49" num="1.49"><w n="49.1">Gardez</w>-<w n="49.2">la</w> <w n="49.3">donc</w> <w n="49.4">toujours</w> <w n="49.5">cette</w> <w n="49.6">triste</w> <w n="49.7">pensée</w></l>
						<l n="50" num="1.50"><w n="50.1">D</w>’<w n="50.2">une</w> <w n="50.3">amour</w> <w n="50.4">méconnue</w> <w n="50.5">et</w> <w n="50.6">d</w>’<w n="50.7">une</w> <w n="50.8">âme</w> <w n="50.9">froissée</w> :</l>
						<l n="51" num="1.51"><w n="51.1">Que</w> <w n="51.2">le</w> <w n="51.3">prêtre</w> <w n="51.4">debout</w>, <w n="51.5">sur</w> <w n="51.6">l</w>’<w n="51.7">autel</w> <w n="51.8">aboli</w>,</l>
						<l n="52" num="1.52"><w n="52.1">Reste</w> <w n="52.2">fidèle</w> <w n="52.3">au</w> <w n="52.4">Dieu</w> <w n="52.5">dont</w> <w n="52.6">il</w> <w n="52.7">était</w> <w n="52.8">rempli</w> ;</l>
						<l n="53" num="1.53"><w n="53.1">Que</w> <w n="53.2">le</w> <w n="53.3">temple</w> <w n="53.4">désert</w>, <w n="53.5">aux</w> <w n="53.6">vitraux</w> <w n="53.7">de</w> <w n="53.8">l</w>’<w n="53.9">enceinte</w></l>
						<l n="54" num="1.54"><w n="54.1">Garde</w> <w n="54.2">un</w> <w n="54.3">dernier</w> <w n="54.4">rayon</w> <w n="54.5">de</w> <w n="54.6">l</w>’<w n="54.7">auréole</w> <w n="54.8">sainte</w>.</l>
						<l n="55" num="1.55"><w n="55.1">Et</w> <w n="55.2">que</w> <w n="55.3">l</w>’<w n="55.4">encensoir</w> <w n="55.5">d</w>’<w n="55.6">or</w> <w n="55.7">ne</w> <w n="55.8">cesse</w> <w n="55.9">d</w>’<w n="55.10">exhaler</w></l>
						<l n="56" num="1.56"><w n="56.1">Le</w> <w n="56.2">parfum</w> <w n="56.3">d</w>’<w n="56.4">un</w> <w n="56.5">encens</w> <w n="56.6">qui</w> <w n="56.7">cessa</w> <w n="56.8">de</w> <w n="56.9">brûler</w> !</l>
						<l n="57" num="1.57"><w n="57.1">Il</w> <w n="57.2">n</w>’<w n="57.3">est</w> <w n="57.4">si</w> <w n="57.5">triste</w> <w n="57.6">nuit</w> <w n="57.7">qu</w>’<w n="57.8">au</w> <w n="57.9">crêpe</w> <w n="57.10">de</w> <w n="57.11">son</w> <w n="57.12">voile</w></l>
						<l n="58" num="1.58"><w n="58.1">Dieu</w> <w n="58.2">ne</w> <w n="58.3">fasse</w> <w n="58.4">parfois</w> <w n="58.5">luire</w> <w n="58.6">une</w> <w n="58.7">blanche</w> <w n="58.8">étoile</w>,</l>
						<l n="59" num="1.59"><w n="59.1">Et</w> <w n="59.2">le</w> <w n="59.3">ciel</w> <w n="59.4">mit</w> <w n="59.5">au</w> <w n="59.6">fond</w> <w n="59.7">des</w> <w n="59.8">amours</w> <w n="59.9">malheureux</w></l>
						<l n="60" num="1.60"><w n="60.1">Certains</w> <w n="60.2">bonheurs</w> <w n="60.3">cachés</w> <w n="60.4">qu</w>’<w n="60.5">il</w> <w n="60.6">a</w> <w n="60.7">gardés</w> <w n="60.8">pour</w> <w n="60.9">eux</w>.</l>
						<l n="61" num="1.61"><w n="61.1">Supportez</w> <w n="61.2">donc</w> <w n="61.3">vos</w> <w n="61.4">maux</w>, <w n="61.5">car</w> <w n="61.6">plus</w> <w n="61.7">d</w>’<w n="61.8">un</w> <w n="61.9">les</w> <w n="61.10">envie</w> ;</l>
						<l n="62" num="1.62"><w n="62.1">Car</w>, <w n="62.2">moi</w> <w n="62.3">qui</w> <w n="62.4">parle</w>, <w n="62.5">au</w> <w n="62.6">prix</w> <w n="62.7">du</w> <w n="62.8">repos</w> <w n="62.9">de</w> <w n="62.10">ma</w> <w n="62.11">vie</w>.</l>
						<l n="63" num="1.63"><w n="63.1">Au</w> <w n="63.2">prix</w> <w n="63.3">de</w> <w n="63.4">tout</w> <w n="63.5">mon</w> <w n="63.6">sang</w>. <w n="63.7">Madame</w>, <w n="63.8">je</w> <w n="63.9">voudrais</w></l>
						<l n="64" num="1.64"><w n="64.1">Les</w> <w n="64.2">éprouver</w> <w n="64.3">un</w> <w n="64.4">jour</w>, <w n="64.5">quitte</w> <w n="64.6">à</w> <w n="64.7">mourir</w> <w n="64.8">après</w>.</l>
					</lg>
				</div></body></text></TEI>