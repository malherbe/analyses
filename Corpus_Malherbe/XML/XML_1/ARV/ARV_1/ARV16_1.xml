<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Une édition électronique</title>
				<author key="ARV">
					<name>
						<forename>Félix</forename>
						<surname>ARVERS</surname>
					</name>
					<date from="1806" to="1850">1806-1850</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Encodage XML, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Signalement d’erreurs de numérisation</resp>
					<name id="FD">
						<forename>François</forename>
						<surname>Demay</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>4560 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">ARV_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>poesies.net</publisher>
						<idno type="URL">http://www.poesies.net/felixarverspoesies.txt</idno>
					</publicationStmt>
					<sourceDesc>
						<p>L’édition électronique ne mentionne pas l’édition imprimée d’origine.</p>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies de Félix Arvers: mes heures perdues. Pièces inédites</title>
						<author>Félix Arvers</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">http://www.archive.org/details/posiesdeflixOOarve </idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies</title>
								<author>Félix Arvers</author>
								<imprint>
									<publisher>H. Floury, éditeur</publisher>
									<date when="1900">1900</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1833">1833</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques.</p>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
					<p>Les faux points de suspension (3 points) ont été remplacés par le caractère approprié (…).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">MES HEURES PERDUES</head><div type="poem" key="ARV16">
					<head type="main">La Vie</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Je sais que vous riez amèrement de cette nécessité où <lb></lb>
									l’on est, en France, de prendre un état. Ne méprisez pas tant <lb></lb>
									l’expérience et la sagesse de nos pères. Il vaut mieux, mon <lb></lb>
									cher René, ressembler un peu plus au commun des hommes, <lb></lb>
									et avoir un peu moins de malheur.
								</quote>
								<bibl>
									<name>M. de Chateaubriand</name>.
								</bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Amis</w>, <w n="1.2">accueillez</w>-<w n="1.3">moi</w>, <w n="1.4">j</w>’<w n="1.5">arrive</w> <w n="1.6">dans</w> <w n="1.7">la</w> <w n="1.8">vie</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Dépensons</w> <w n="2.2">l</w>’<w n="2.3">existence</w> <w n="2.4">au</w> <w n="2.5">gré</w> <w n="2.6">de</w> <w n="2.7">notre</w> <w n="2.8">envie</w> :</l>
						<l n="3" num="1.3"><w n="3.1">Vivre</w>, <w n="3.2">c</w>’<w n="3.3">est</w> <w n="3.4">être</w> <w n="3.5">libre</w>, <w n="3.6">et</w> <w n="3.7">pouvoir</w> <w n="3.8">à</w> <w n="3.9">loisir</w></l>
						<l n="4" num="1.4"><w n="4.1">Abandonner</w> <w n="4.2">son</w> <w n="4.3">âme</w> <w n="4.4">à</w> <w n="4.5">l</w>’<w n="4.6">attrait</w> <w n="4.7">du</w> <w n="4.8">plaisir</w> ;</l>
						<l n="5" num="1.5"><w n="5.1">C</w>’<w n="5.2">est</w> <w n="5.3">chanter</w>, <w n="5.4">s</w>’<w n="5.5">enivrer</w> <w n="5.6">des</w> <w n="5.7">cieux</w>, <w n="5.8">des</w> <w n="5.9">bois</w>, <w n="5.10">de</w> <w n="5.11">l</w>’<w n="5.12">onde</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Ou</w>, <w n="6.2">parmi</w> <w n="6.3">les</w> <w n="6.4">tilleuls</w>, <w n="6.5">suivre</w> <w n="6.6">une</w> <w n="6.7">vierge</w> <w n="6.8">blonde</w> !</l>
						<l n="7" num="1.7">— <w n="7.1">C</w>’<w n="7.2">est</w> <w n="7.3">bien</w> <w n="7.4">là</w> <w n="7.5">le</w> <w n="7.6">discours</w> <w n="7.7">d</w>’<w n="7.8">un</w> <w n="7.9">enfant</w>. <w n="7.10">Écoutez</w> :</l>
						<l n="8" num="1.8"><w n="8.1">Vous</w> <w n="8.2">avez</w> <w n="8.3">de</w> <w n="8.4">l</w>’<w n="8.5">esprit</w>. — <w n="8.6">Trop</w> <w n="8.7">bon</w>. — <w n="8.8">Et</w> <w n="8.9">méritez</w></l>
						<l n="9" num="1.9"><w n="9.1">Qu</w>’<w n="9.2">un</w> <w n="9.3">ami</w> <w n="9.4">plus</w> <w n="9.5">mûr</w> <w n="9.6">vienne</w>, <w n="9.7">en</w> <w n="9.8">cette</w> <w n="9.9">circonstance</w>,</l>
						<l n="10" num="1.10"><w n="10.1">D</w>’<w n="10.2">un</w> <w n="10.3">utile</w> <w n="10.4">conseil</w> <w n="10.5">vous</w> <w n="10.6">prêter</w> <w n="10.7">l</w>’<w n="10.8">assistance</w>.</l>
						<l n="11" num="1.11"><w n="11.1">Il</w> <w n="11.2">ne</w> <w n="11.3">faut</w> <w n="11.4">pas</w> <w n="11.5">se</w> <w n="11.6">faire</w> <w n="11.7">illusion</w> <w n="11.8">ici</w> ;</l>
						<l n="12" num="1.12"><w n="12.1">Avant</w> <w n="12.2">d</w>’<w n="12.3">être</w> <w n="12.4">poète</w>, <w n="12.5">et</w> <w n="12.6">de</w> <w n="12.7">livrer</w> <w n="12.8">ainsi</w></l>
						<l n="13" num="1.13"><w n="13.1">Votre</w> <w n="13.2">âme</w> <w n="13.3">à</w> <w n="13.4">tout</w> <w n="13.5">le</w> <w n="13.6">feu</w> <w n="13.7">de</w> <w n="13.8">l</w>’<w n="13.9">ardeur</w> <w n="13.10">qui</w> <w n="13.11">l</w>’<w n="13.12">emporte</w>.</l>
						<l n="14" num="1.14"><w n="14.1">Avez</w>-<w n="14.2">vous</w> <w n="14.3">de</w> <w n="14.4">l</w>’<w n="14.5">argent</w> ? — <w n="14.6">Que</w> <w n="14.7">sais</w>-<w n="14.8">je</w> ? <w n="14.9">et</w> <w n="14.10">que</w> <w n="14.11">m</w>’<w n="14.12">importe</w> ?</l>
						<l n="15" num="1.15">— <w n="15.1">Il</w> <w n="15.2">importe</w> <w n="15.3">beaucoup</w> ; <w n="15.4">et</w> <w n="15.5">c</w>’<w n="15.6">est</w> <w n="15.7">précisément</w></l>
						<l n="16" num="1.16"><w n="16.1">Ce</w> <w n="16.2">qu</w>’<w n="16.3">il</w> <w n="16.4">faut</w>, <w n="16.5">avant</w> <w n="16.6">tout</w>, <w n="16.7">considérer</w>. — <w n="16.8">Vraiment</w> ?</l>
						<l n="17" num="1.17">— <w n="17.1">S</w>’<w n="17.2">il</w> <w n="17.3">fut</w> <w n="17.4">des</w> <w n="17.5">jours</w> <w n="17.6">heureux</w>, <w n="17.7">où</w> <w n="17.8">la</w> <w n="17.9">voix</w> <w n="17.10">des</w> <w n="17.11">poètes</w></l>
						<l n="18" num="1.18"><w n="18.1">Enchaînait</w> <w n="18.2">à</w> <w n="18.3">son</w> <w n="18.4">gré</w> <w n="18.5">les</w> <w n="18.6">nations</w> <w n="18.7">muettes</w>,</l>
						<l n="19" num="1.19"><w n="19.1">Ces</w> <w n="19.2">jours</w>-<w n="19.3">là</w> <w n="19.4">ne</w> <w n="19.5">sont</w> <w n="19.6">plus</w>, <w n="19.7">et</w> <w n="19.8">depuis</w> <w n="19.9">bien</w> <w n="19.10">long</w>-<w n="19.11">temps</w> :</l>
						<l n="20" num="1.20"><w n="20.1">Est</w>-<w n="20.2">ce</w> <w n="20.3">un</w> <w n="20.4">bien</w>, <w n="20.5">est</w>-<w n="20.6">ce</w> <w n="20.7">un</w> <w n="20.8">mal</w>, <w n="20.9">je</w> <w n="20.10">l</w>’<w n="20.11">ignore</w>, <w n="20.12">et</w> <w n="20.13">n</w>’<w n="20.14">entends</w></l>
						<l n="21" num="1.21"><w n="21.1">Que</w> <w n="21.2">vous</w> <w n="21.3">prouver</w> <w n="21.4">un</w> <w n="21.5">fait</w>, <w n="21.6">et</w> <w n="21.7">vous</w> <w n="21.8">faire</w> <w n="21.9">comprendre</w></l>
						<l n="22" num="1.22"><w n="22.1">Que</w> <w n="22.2">si</w> <w n="22.3">le</w> <w n="22.4">monde</w> <w n="22.5">est</w> <w n="22.6">tel</w>, <w n="22.7">tel</w> <w n="22.8">il</w> <w n="22.9">faut</w> <w n="22.10">bien</w> <w n="22.11">le</w> <w n="22.12">prendre</w>.</l>
						<l n="23" num="1.23"><w n="23.1">Le</w> <w n="23.2">poète</w> <w n="23.3">n</w>’<w n="23.4">est</w> <w n="23.5">plus</w> <w n="23.6">l</w>’<w n="23.7">enfant</w> <w n="23.8">des</w> <w n="23.9">immortels</w>,</l>
						<l n="24" num="1.24"><w n="24.1">A</w> <w n="24.2">qui</w> <w n="24.3">l</w>’<w n="24.4">homme</w> <w n="24.5">à</w> <w n="24.6">genoux</w> <w n="24.7">élevait</w> <w n="24.8">des</w> <w n="24.9">autels</w> ;</l>
						<l n="25" num="1.25"><w n="25.1">Ce</w> <w n="25.2">culte</w> <w n="25.3">d</w>’<w n="25.4">un</w> <w n="25.5">autre</w> <w n="25.6">âge</w> <w n="25.7">est</w> <w n="25.8">perdu</w> <w n="25.9">dans</w> <w n="25.10">le</w> <w n="25.11">nôtre</w>,</l>
						<l n="26" num="1.26"><w n="26.1">Et</w> <w n="26.2">c</w>’<w n="26.3">est</w> <w n="26.4">tout</w> <w n="26.5">simplement</w> <w n="26.6">un</w> <w n="26.7">homme</w> <w n="26.8">comme</w> <w n="26.9">un</w> <w n="26.10">autre</w>.</l>
						<l n="27" num="1.27"><w n="27.1">Si</w> <w n="27.2">donc</w> <w n="27.3">vous</w> <w n="27.4">n</w>’<w n="27.5">avez</w> <w n="27.6">rien</w>, <w n="27.7">travaillez</w> <w n="27.8">pour</w> <w n="27.9">avoir</w> ;</l>
						<l n="28" num="1.28"><w n="28.1">Embrassez</w> <w n="28.2">un</w> <w n="28.3">état</w> : <w n="28.4">le</w> <w n="28.5">tout</w> <w n="28.6">est</w> <w n="28.7">de</w> <w n="28.8">savoir</w></l>
						<l n="29" num="1.29"><w n="29.1">Choisir</w>, <w n="29.2">et</w> <w n="29.3">sans</w> <w n="29.4">jamais</w> <w n="29.5">regarder</w> <w n="29.6">en</w> <w n="29.7">arrière</w>,</l>
						<l n="30" num="1.30"><w n="30.1">D</w>’<w n="30.2">un</w> <w n="30.3">pas</w> <w n="30.4">ferme</w> <w n="30.5">et</w> <w n="30.6">hardi</w> <w n="30.7">poursuivre</w> <w n="30.8">sa</w> <w n="30.9">carrière</w>.</l>
						<l n="31" num="1.31">— <w n="31.1">Et</w> <w n="31.2">ce</w> <w n="31.3">monde</w> <w n="31.4">idéal</w> <w n="31.5">que</w> <w n="31.6">je</w> <w n="31.7">me</w> <w n="31.8">figurais</w> !</l>
						<l n="32" num="1.32"><w n="32.1">Et</w> <w n="32.2">ces</w> <w n="32.3">accens</w> <w n="32.4">lointains</w> <w n="32.5">du</w> <w n="32.6">cor</w> <w n="32.7">dans</w> <w n="32.8">les</w> <w n="32.9">forêts</w> !</l>
						<l n="33" num="1.33"><w n="33.1">Et</w> <w n="33.2">ce</w> <w n="33.3">bel</w> <w n="33.4">avenir</w>, <w n="33.5">et</w> <w n="33.6">ces</w> <w n="33.7">chants</w> <w n="33.8">d</w>’<w n="33.9">innocence</w> !</l>
						<l n="34" num="1.34"><w n="34.1">Et</w> <w n="34.2">ces</w> <w n="34.3">rêves</w> <w n="34.4">dorés</w> <w n="34.5">de</w> <w n="34.6">mon</w> <w n="34.7">adolescence</w> !</l>
						<l n="35" num="1.35"><w n="35.1">Et</w> <w n="35.2">ces</w> <w n="35.3">lacs</w>, <w n="35.4">et</w> <w n="35.5">ces</w> <w n="35.6">mers</w>, <w n="35.7">et</w> <w n="35.8">ces</w> <w n="35.9">champs</w> <w n="35.10">émaillés</w>,</l>
						<l n="36" num="1.36"><w n="36.1">Et</w> <w n="36.2">ces</w> <w n="36.3">grands</w> <w n="36.4">peupliers</w>, <w n="36.5">et</w> <w n="36.6">ces</w> <w n="36.7">fleurs</w> ! — <w n="36.8">Travaillez</w>.</l>
						<l n="37" num="1.37"><w n="37.1">Apprenez</w> <w n="37.2">donc</w> <w n="37.3">un</w> <w n="37.4">peu</w>, <w n="37.5">jeune</w> <w n="37.6">homme</w>, <w n="37.7">à</w> <w n="37.8">vous</w> <w n="37.9">connaître</w> :</l>
						<l n="38" num="1.38"><w n="38.1">Vous</w> <w n="38.2">croyez</w> <w n="38.3">que</w> <w n="38.4">l</w>’<w n="38.5">on</w> <w n="38.6">n</w>’<w n="38.7">a</w> <w n="38.8">que</w> <w n="38.9">la</w> <w n="38.10">peine</w> <w n="38.11">de</w> <w n="38.12">naître</w>,</l>
						<l n="39" num="1.39"><w n="39.1">Et</w> <w n="39.2">qu</w>’<w n="39.3">on</w> <w n="39.4">est</w> <w n="39.5">ici</w>-<w n="39.6">bas</w> <w n="39.7">pour</w> <w n="39.8">dormir</w>, <w n="39.9">se</w> <w n="39.10">lever</w>,</l>
						<l n="40" num="1.40"><w n="40.1">Passer</w>, <w n="40.2">les</w> <w n="40.3">bras</w> <w n="40.4">croisés</w>, <w n="40.5">tout</w> <w n="40.6">le</w> <w n="40.7">jour</w> <w n="40.8">à</w> <w n="40.9">rêver</w> ;</l>
						<l n="41" num="1.41"><w n="41.1">C</w>’<w n="41.2">est</w> <w n="41.3">ainsi</w> <w n="41.4">qu</w>’<w n="41.5">on</w> <w n="41.6">se</w> <w n="41.7">perd</w>, <w n="41.8">c</w>’<w n="41.9">est</w> <w n="41.10">ainsi</w> <w n="41.11">qu</w>’<w n="41.12">on</w> <w n="41.13">végète</w> :</l>
						<l n="42" num="1.42"><w n="42.1">Pauvre</w>, <w n="42.2">inutile</w> <w n="42.3">à</w> <w n="42.4">tous</w>, <w n="42.5">le</w> <w n="42.6">monde</w> <w n="42.7">vous</w> <w n="42.8">rejette</w> :</l>
						<l n="43" num="1.43"><w n="43.1">Contre</w> <w n="43.2">la</w> <w n="43.3">faim</w>, <w n="43.4">le</w> <w n="43.5">froid</w>, <w n="43.6">on</w> <w n="43.7">lutte</w>, <w n="43.8">on</w> <w n="43.9">se</w> <w n="43.10">débat</w></l>
						<l n="44" num="1.44"><w n="44.1">Quelque</w> <w n="44.2">temps</w>, <w n="44.3">et</w> <w n="44.4">l</w>’<w n="44.5">on</w> <w n="44.6">va</w> <w n="44.7">mourir</w> <w n="44.8">sur</w> <w n="44.9">un</w> <w n="44.10">grabat</w>.</l>
						<l n="45" num="1.45"><w n="45.1">Ce</w> <w n="45.2">tableau</w> <w n="45.3">n</w>’<w n="45.4">est</w> <w n="45.5">pas</w> <w n="45.6">gai</w>, <w n="45.7">ce</w> <w n="45.8">discours</w> <w n="45.9">n</w>’<w n="45.10">est</w> <w n="45.11">pas</w> <w n="45.12">tendre</w>.</l>
						<l n="46" num="1.46"><w n="46.1">C</w>’<w n="46.2">est</w> <w n="46.3">vrai</w> ; <w n="46.4">mais</w> <w n="46.5">j</w>’<w n="46.6">ai</w> <w n="46.7">voulu</w> <w n="46.8">vous</w> <w n="46.9">faire</w> <w n="46.10">bien</w> <w n="46.11">entendre</w>,</l>
						<l n="47" num="1.47"><w n="47.1">Par</w> <w n="47.2">amitié</w> <w n="47.3">pour</w> <w n="47.4">vous</w>, <w n="47.5">et</w> <w n="47.6">dans</w> <w n="47.7">votre</w> <w n="47.8">intérêt</w>,</l>
						<l n="48" num="1.48"><w n="48.1">Où</w> <w n="48.2">votre</w> <w n="48.3">poésie</w> <w n="48.4">un</w> <w n="48.5">jour</w> <w n="48.6">vous</w> <w n="48.7">conduirait</w>.</l>
					</lg>
					<lg n="2">
						<l n="49" num="2.1"><w n="49.1">Cet</w> <w n="49.2">homme</w> <w n="49.3">avait</w> <w n="49.4">raison</w>, <w n="49.5">au</w> <w n="49.6">fait</w> : <w n="49.7">j</w>’<w n="49.8">ai</w> <w n="49.9">dû</w> <w n="49.10">me</w> <w n="49.11">taire</w>.</l>
						<l n="50" num="2.2"><w n="50.1">Je</w> <w n="50.2">me</w> <w n="50.3">croyais</w> <w n="50.4">poète</w>, <w n="50.5">et</w> <w n="50.6">me</w> <w n="50.7">voici</w> <w n="50.8">notaire</w>.</l>
						<l n="51" num="2.3"><w n="51.1">J</w>’<w n="51.2">ai</w> <w n="51.3">suivi</w> <w n="51.4">ses</w> <w n="51.5">conseils</w>, <w n="51.6">et</w> <w n="51.7">j</w>’<w n="51.8">ai</w>, <w n="51.9">sans</w> <w n="51.10">m</w>’<w n="51.11">effrayer</w>,</l>
						<l n="52" num="2.4"><w n="52.1">Subi</w> <w n="52.2">le</w> <w n="52.3">lourd</w> <w n="52.4">fardeau</w> <w n="52.5">d</w>’<w n="52.6">une</w> <w n="52.7">charge</w> <w n="52.8">à</w> <w n="52.9">payer</w>.</l>
						<l n="53" num="2.5"><w n="53.1">Je</w> <w n="53.2">dois</w> <w n="53.3">être</w> <w n="53.4">content</w> : <w n="53.5">c</w>’<w n="53.6">est</w> <w n="53.7">un</w> <w n="53.8">très</w> <w n="53.9">bel</w> <w n="53.10">office</w> ;</l>
						<l n="54" num="2.6"><w n="54.1">C</w>’<w n="54.2">est</w> <w n="54.3">magnifique</w>, <w n="54.4">à</w> <w n="54.5">part</w> <w n="54.6">même</w> <w n="54.7">le</w> <w n="54.8">bénéfice</w>.</l>
						<l n="55" num="2.7"><w n="55.1">On</w> <w n="55.2">a</w> <w n="55.3">bonne</w> <w n="55.4">maison</w>, <w n="55.5">on</w> <w n="55.6">reçoit</w> <w n="55.7">les</w> <w n="55.8">jeudis</w> ;</l>
						<l n="56" num="2.8"><w n="56.1">On</w> <w n="56.2">a</w> <w n="56.3">des</w> <w n="56.4">clercs</w>, <w n="56.5">qu</w>’<w n="56.6">on</w> <w n="56.7">loge</w> <w n="56.8">en</w> <w n="56.9">haut</w>, <w n="56.10">dans</w> <w n="56.11">un</w> <w n="56.12">taudis</w>.</l>
						<l n="57" num="2.9"><w n="57.1">Il</w> <w n="57.2">est</w> <w n="57.3">vrai</w> <w n="57.4">que</w> <w n="57.5">l</w>’<w n="57.6">état</w> <w n="57.7">n</w>’<w n="57.8">est</w> <w n="57.9">pas</w> <w n="57.10">fort</w> <w n="57.11">poétique</w>.</l>
						<l n="58" num="2.10"><w n="58.1">Et</w> <w n="58.2">rien</w> <w n="58.3">n</w>’<w n="58.4">est</w> <w n="58.5">positif</w> <w n="58.6">comme</w> <w n="58.7">l</w>’<w n="58.8">acte</w> <w n="58.9">authentique</w>.</l>
						<l n="59" num="2.11"><w n="59.1">Mais</w> <w n="59.2">il</w> <w n="59.3">faut</w> <w n="59.4">pourtant</w> <w n="59.5">bien</w> <w n="59.6">se</w> <w n="59.7">faire</w> <w n="59.8">une</w> <w n="59.9">raison</w>,</l>
						<l n="60" num="2.12"><w n="60.1">Et</w> <w n="60.2">tous</w> <w n="60.3">ces</w> <w n="60.4">contes</w> <w n="60.5">bleus</w> <w n="60.6">ne</w> <w n="60.7">sont</w> <w n="60.8">plus</w> <w n="60.9">de</w> <w n="60.10">saison</w> :</l>
						<l n="61" num="2.13"><w n="61.1">Il</w> <w n="61.2">faut</w> <w n="61.3">que</w> <w n="61.4">le</w> <w n="61.5">notaire</w>, <w n="61.6">homme</w> <w n="61.7">d</w>’<w n="61.8">exactitude</w>,</l>
						<l n="62" num="2.14"><w n="62.1">D</w>’<w n="62.2">un</w> <w n="62.3">travail</w> <w n="62.4">assidu</w> <w n="62.5">se</w> <w n="62.6">fasse</w> <w n="62.7">l</w>’<w n="62.8">habitude</w> ;</l>
						<l n="63" num="2.15"><w n="63.1">Va</w>, <w n="63.2">malheureux</w> ! <w n="63.3">et</w> <w n="63.4">si</w> <w n="63.5">quelquefois</w> <w n="63.6">il</w> <w n="63.7">advient</w></l>
						<l n="64" num="2.16"><w n="64.1">Qu</w>’<w n="64.2">un</w> <w n="64.3">riant</w> <w n="64.4">souvenir</w> <w n="64.5">d</w>’<w n="64.6">enfance</w> <w n="64.7">vous</w> <w n="64.8">revient</w>,</l>
						<l n="65" num="2.17"><w n="65.1">Si</w> <w n="65.2">vous</w> <w n="65.3">vous</w> <w n="65.4">rappelez</w> <w n="65.5">que</w> <w n="65.6">la</w> <w n="65.7">voix</w> <w n="65.8">des</w> <w n="65.9">génies</w></l>
						<l n="66" num="2.18"><w n="66.1">Vous</w> <w n="66.2">berçait</w>, <w n="66.3">tout</w> <w n="66.4">petit</w>, <w n="66.5">de</w> <w n="66.6">vagues</w> <w n="66.7">harmonies</w> ;</l>
						<l n="67" num="2.19"><w n="67.1">Si</w>, <w n="67.2">poursuivant</w> <w n="67.3">encor</w> <w n="67.4">un</w> <w n="67.5">bonheur</w> <w n="67.6">qu</w>’<w n="67.7">il</w> <w n="67.8">rêva</w>.</l>
						<l n="68" num="2.20"><w n="68.1">L</w>’<w n="68.2">esprit</w> <w n="68.3">vers</w> <w n="68.4">d</w>’<w n="68.5">autres</w> <w n="68.6">temps</w> <w n="68.7">veut</w> <w n="68.8">se</w> <w n="68.9">retourner</w> : <w n="68.10">Va</w> !</l>
						<l n="69" num="2.21"><w n="69.1">Est</w>-<w n="69.2">ce</w> <w n="69.3">avec</w> <w n="69.4">tout</w> <w n="69.5">cela</w> <w n="69.6">qu</w>’<w n="69.7">on</w> <w n="69.8">mène</w> <w n="69.9">son</w> <w n="69.10">affaire</w> ?</l>
						<l n="70" num="2.22"><w n="70.1">N</w>’<w n="70.2">as</w>-<w n="70.3">tu</w> <w n="70.4">pas</w> <w n="70.5">ce</w> <w n="70.6">matin</w> <w n="70.7">un</w> <w n="70.8">testament</w> <w n="70.9">à</w> <w n="70.10">faire</w> ?</l>
						<l n="71" num="2.23"><w n="71.1">Le</w> <w n="71.2">client</w> <w n="71.3">est</w> <w n="71.4">fort</w> <w n="71.5">mal</w>, <w n="71.6">et</w> <w n="71.7">serait</w> <w n="71.8">en</w> <w n="71.9">état</w>,</l>
						<l n="72" num="2.24"><w n="72.1">Si</w> <w n="72.2">tu</w> <w n="72.3">tardais</w> <w n="72.4">encor</w>, <w n="72.5">de</w> <w n="72.6">mourir</w> <w n="72.7">intestat</w>.</l>
					</lg>
					<lg n="3">
						<l n="73" num="3.1"><w n="73.1">Mais</w> <w n="73.2">j</w>’<w n="73.3">ai</w> <w n="73.4">trente</w>-<w n="73.5">deux</w> <w n="73.6">ans</w> <w n="73.7">accomplis</w> ; <w n="73.8">à</w> <w n="73.9">mon</w> <w n="73.10">âge</w></l>
						<l n="74" num="3.2"><w n="74.1">Il</w> <w n="74.2">faut</w> <w n="74.3">songer</w> <w n="74.4">pourtant</w> <w n="74.5">à</w> <w n="74.6">se</w> <w n="74.7">mettre</w> <w n="74.8">en</w> <w n="74.9">ménage</w> ;</l>
						<l n="75" num="3.3"><w n="75.1">Il</w> <w n="75.2">faut</w> <w n="75.3">faire</w> <w n="75.4">une</w> <w n="75.5">fin</w>, <w n="75.6">tôt</w> <w n="75.7">ou</w> <w n="75.8">tard</w>. <w n="75.9">Dans</w> <w n="75.10">le</w> <w n="75.11">temps</w>.</l>
						<l n="76" num="3.4"><w n="76.1">J</w>’<w n="76.2">y</w> <w n="76.3">songeais</w> <w n="76.4">bien</w> <w n="76.5">aussi</w>, <w n="76.6">quand</w> <w n="76.7">j</w>’<w n="76.8">avais</w> <w n="76.9">dix</w>-<w n="76.10">huit</w> <w n="76.11">ans</w>.</l>
						<l n="77" num="3.5"><w n="77.1">Je</w> <w n="77.2">voyais</w> <w n="77.3">chaque</w> <w n="77.4">nuit</w>, <w n="77.5">de</w> <w n="77.6">la</w> <w n="77.7">voûte</w> <w n="77.8">étoilée</w>,</l>
						<l n="78" num="3.6"><w n="78.1">Descendre</w> <w n="78.2">sur</w> <w n="78.3">ma</w> <w n="78.4">couche</w> <w n="78.5">une</w> <w n="78.6">vierge</w> <w n="78.7">voilée</w> ;</l>
						<l n="79" num="3.7"><w n="79.1">Je</w> <w n="79.2">la</w> <w n="79.3">sentais</w>, <w n="79.4">craintive</w>, <w n="79.5">et</w> <w n="79.6">cédant</w> <w n="79.7">à</w> <w n="79.8">mes</w> <w n="79.9">vœux</w>.</l>
						<l n="80" num="3.8"><w n="80.1">D</w>’<w n="80.2">un</w> <w n="80.3">souffle</w> <w n="80.4">caressant</w> <w n="80.5">effleurer</w> <w n="80.6">mes</w> <w n="80.7">cheveux</w> ;</l>
						<l n="81" num="3.9"><w n="81.1">Et</w> <w n="81.2">cette</w> <w n="81.3">vision</w> <w n="81.4">que</w> <w n="81.5">j</w>’<w n="81.6">avais</w> <w n="81.7">tant</w> <w n="81.8">rêvée</w>.</l>
						<l n="82" num="3.10"><w n="82.1">Sur</w> <w n="82.2">la</w> <w n="82.3">terre</w>, <w n="82.4">une</w> <w n="82.5">fois</w>, <w n="82.6">je</w> <w n="82.7">l</w>’<w n="82.8">avais</w> <w n="82.9">retrouvée</w>.</l>
						<l n="83" num="3.11"><w n="83.1">Oh</w> ! <w n="83.2">qui</w> <w n="83.3">me</w> <w n="83.4">les</w> <w n="83.5">rendra</w> <w n="83.6">ces</w> <w n="83.7">rapides</w> <w n="83.8">instans</w>,</l>
						<l n="84" num="3.12"><w n="84.1">Et</w> <w n="84.2">ces</w> <w n="84.3">illusions</w> <w n="84.4">d</w>’<w n="84.5">un</w> <w n="84.6">amour</w> <w n="84.7">de</w> <w n="84.8">vingt</w> <w n="84.9">ans</w> !</l>
						<l n="85" num="3.13"><w n="85.1">L</w>’<w n="85.2">automne</w> <w n="85.3">à</w> <w n="85.4">la</w> <w n="85.5">campagne</w>, <w n="85.6">et</w> <w n="85.7">ses</w> <w n="85.8">longues</w> <w n="85.9">soirées</w>,</l>
						<l n="86" num="3.14"><w n="86.1">Les</w> <w n="86.2">mères</w>, <w n="86.3">dans</w> <w n="86.4">un</w> <w n="86.5">coin</w> <w n="86.6">du</w> <w n="86.7">salon</w> <w n="86.8">retirées</w>,</l>
						<l n="87" num="3.15"><w n="87.1">Ces</w> <w n="87.2">regards</w> <w n="87.3">pleins</w> <w n="87.4">de</w> <w n="87.5">feu</w>, <w n="87.6">ces</w> <w n="87.7">gestes</w> <w n="87.8">si</w> <w n="87.9">connus</w>,</l>
						<l n="88" num="3.16"><w n="88.1">Et</w> <w n="88.2">ces</w> <w n="88.3">airs</w> <w n="88.4">si</w> <w n="88.5">touchans</w> <w n="88.6">que</w> <w n="88.7">j</w>’<w n="88.8">ai</w> <w n="88.9">tous</w> <w n="88.10">retenus</w> ?</l>
						<l n="89" num="3.17"><w n="89.1">Tout</w> <w n="89.2">à</w> <w n="89.3">coup</w> <w n="89.4">une</w> <w n="89.5">voix</w> <w n="89.6">d</w>’<w n="89.7">en</w> <w n="89.8">haut</w> <w n="89.9">l</w>’<w n="89.10">a</w> <w n="89.11">rappelée</w> :</l>
						<l n="90" num="3.18"><w n="90.1">Cette</w> <w n="90.2">vie</w> <w n="90.3">est</w> <w n="90.4">si</w> <w n="90.5">triste</w> ! <w n="90.6">elle</w> <w n="90.7">s</w>’<w n="90.8">en</w> <w n="90.9">est</w> <w n="90.10">allée</w> ;</l>
						<l n="91" num="3.19"><w n="91.1">Elle</w> <w n="91.2">a</w> <w n="91.3">fermé</w> <w n="91.4">les</w> <w n="91.5">yeux</w>, <w n="91.6">sans</w> <w n="91.7">crainte</w>, <w n="91.8">sans</w> <w n="91.9">remords</w> ;</l>
						<l n="92" num="3.20"><w n="92.1">Mais</w> <w n="92.2">pensent</w>-<w n="92.3">ils</w> <w n="92.4">encore</w> <w n="92.5">à</w> <w n="92.6">nous</w> <w n="92.7">ceux</w> <w n="92.8">qui</w> <w n="92.9">sont</w> <w n="92.10">morts</w> ?</l>
					</lg>
					<lg n="4">
						<l n="93" num="4.1"><w n="93.1">Il</w> <w n="93.2">s</w>’<w n="93.3">agit</w> <w n="93.4">bien</w> <w n="93.5">ici</w> <w n="93.6">d</w>’<w n="93.7">un</w> <w n="93.8">amour</w> <w n="93.9">platonique</w> !</l>
						<l n="94" num="4.2"><w n="94.1">Me</w> <w n="94.2">voici</w> <w n="94.3">marié</w> : <w n="94.4">ma</w> <w n="94.5">femme</w> <w n="94.6">est</w> <w n="94.7">fille</w> <w n="94.8">unique</w> ;</l>
						<l n="95" num="4.3"><w n="95.1">Son</w> <w n="95.2">père</w> <w n="95.3">est</w> <w n="95.4">épicier</w>-<w n="95.5">droguiste</w> <w n="95.6">retiré</w>,</l>
						<l n="96" num="4.4"><w n="96.1">Et</w> <w n="96.2">riche</w>, <w n="96.3">qui</w> <w n="96.4">plus</w> <w n="96.5">est</w> : <w n="96.6">je</w> <w n="96.7">le</w> <w n="96.8">trouve</w> <w n="96.9">à</w> <w n="96.10">mon</w> <w n="96.11">gré</w>.</l>
						<l n="97" num="4.5"><w n="97.1">Il</w> <w n="97.2">n</w>’<w n="97.3">est</w> <w n="97.4">correspondant</w> <w n="97.5">d</w>’<w n="97.6">aucune</w> <w n="97.7">académie</w>.</l>
						<l n="98" num="4.6"><w n="98.1">C</w>’<w n="98.2">est</w> <w n="98.3">vrai</w> ; <w n="98.4">mais</w> <w n="98.5">il</w> <w n="98.6">est</w> <w n="98.7">rond</w>, <w n="98.8">et</w> <w n="98.9">plein</w> <w n="98.10">de</w> <w n="98.11">bonhomie</w> :</l>
						<l n="99" num="4.7"><w n="99.1">Et</w> <w n="99.2">puis</w> <w n="99.3">j</w>’<w n="99.4">aime</w> <w n="99.5">ma</w> <w n="99.6">femme</w>, <w n="99.7">et</w> <w n="99.8">je</w> <w n="99.9">crois</w> <w n="99.10">en</w> <w n="99.11">effet</w>,</l>
						<l n="100" num="4.8"><w n="100.1">En</w> <w n="100.2">demandant</w> <w n="100.3">sa</w> <w n="100.4">main</w>, <w n="100.5">avoir</w> <w n="100.6">sagement</w> <w n="100.7">fait</w>.</l>
						<l n="101" num="4.9"><w n="101.1">Est</w>-<w n="101.2">il</w> <w n="101.3">un</w> <w n="101.4">sort</w> <w n="101.5">plus</w> <w n="101.6">doux</w>, <w n="101.7">et</w> <w n="101.8">plus</w> <w n="101.9">digne</w> <w n="101.10">d</w>’<w n="101.11">envie</w> ?</l>
						<l n="102" num="4.10"><w n="102.1">On</w> <w n="102.2">passe</w>, <w n="102.3">au</w> <w n="102.4">coin</w> <w n="102.5">du</w> <w n="102.6">feu</w>, <w n="102.7">tranquillement</w> <w n="102.8">sa</w> <w n="102.9">vie</w> :</l>
						<l n="103" num="4.11"><w n="103.1">On</w> <w n="103.2">boit</w>, <w n="103.3">on</w> <w n="103.4">mange</w>, <w n="103.5">on</w> <w n="103.6">dort</w>, <w n="103.7">et</w> <w n="103.8">l</w>’<w n="103.9">on</w> <w n="103.10">voit</w> <w n="103.11">arriver</w></l>
						<l n="104" num="4.12"><w n="104.1">Des</w> <w n="104.2">enfans</w> <w n="104.3">qu</w>’<w n="104.4">il</w> <w n="104.5">faut</w> <w n="104.6">mettre</w> <w n="104.7">en</w> <w n="104.8">nourrice</w>, <w n="104.9">élever</w>,</l>
						<l n="105" num="4.13"><w n="105.1">Puis</w> <w n="105.2">établir</w> <w n="105.3">enfin</w> : <w n="105.4">puis</w> <w n="105.5">viennent</w> <w n="105.6">les</w> <w n="105.7">années</w>,</l>
						<l n="106" num="4.14"><w n="106.1">Les</w> <w n="106.2">rides</w> <w n="106.3">au</w> <w n="106.4">visage</w> <w n="106.5">et</w> <w n="106.6">les</w> <w n="106.7">couleurs</w> <w n="106.8">fanées</w>,</l>
						<l n="107" num="4.15"><w n="107.1">Puis</w> <w n="107.2">les</w> <w n="107.3">maux</w>, <w n="107.4">puis</w> <w n="107.5">la</w> <w n="107.6">goutte</w>. <w n="107.7">On</w> <w n="107.8">vit</w> <w n="107.9">comme</w> <w n="107.10">cela</w></l>
						<l n="108" num="4.16"><w n="108.1">Cinquante</w> <w n="108.2">ou</w> <w n="108.3">soixante</w> <w n="108.4">ans</w>, <w n="108.5">et</w> <w n="108.6">puis</w> <w n="108.7">on</w> <w n="108.8">meurt</w>. <w n="108.9">Voilà</w>.</l>
					</lg>
				</div></body></text></TEI>