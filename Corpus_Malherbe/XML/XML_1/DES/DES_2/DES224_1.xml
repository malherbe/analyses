<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES224">
					<head type="main">L’ADIEU TOUT BAS</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Quoi ! chanter ! quand l’amour, quand la douleur déchire ! <lb></lb>
									Chanter, la mort dans l’âme et les pleurs dans les yeux !
								</quote>
								 <bibl><hi rend="smallcap">Jean Polonius.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Autant</w> <w n="1.2">que</w> <w n="1.3">moi</w>-<w n="1.4">même</w>,</l>
						<l n="2" num="1.2"><w n="2.1">En</w> <w n="2.2">quittant</w> <w n="2.3">ces</w> <w n="2.4">lieux</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Cherchez</w> <w n="3.2">qui</w> <w n="3.3">vous</w> <w n="3.4">aime</w></l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">vous</w> <w n="4.3">plaise</w> <w n="4.4">mieux</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Éloignez</w> <w n="5.2">la</w> <w n="5.3">flamme</w></l>
						<l n="6" num="2.2"><w n="6.1">Qui</w> <w n="6.2">nourrit</w> <w n="6.3">mes</w> <w n="6.4">pleurs</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Car</w> <w n="7.2">je</w> <w n="7.3">n</w>’<w n="7.4">ai</w> <w n="7.5">qu</w>’<w n="7.6">une</w> <w n="7.7">âme</w></l>
						<l n="8" num="2.4"><w n="8.1">Pour</w> <w n="8.2">tant</w> <w n="8.3">de</w> <w n="8.4">douleurs</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">La</w> <w n="9.2">raison</w> <w n="9.3">regarde</w></l>
						<l n="10" num="3.2"><w n="10.1">A</w> <w n="10.2">trop</w> <w n="10.3">d</w>’<w n="10.4">amitié</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2">en</w> <w n="11.3">pris</w>, <w n="11.4">par</w> <w n="11.5">mégarde</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Plus</w> <w n="12.2">de</w> <w n="12.3">la</w> <w n="12.4">moitié</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Dormez</w> <w n="13.2">à</w> <w n="13.3">ma</w> <w n="13.4">plainte</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Quand</w> <w n="14.2">j</w>’<w n="14.3">écris</w> <w n="14.4">tout</w> <w n="14.5">bas</w></l>
						<l n="15" num="4.3"><w n="15.1">Ces</w> <w n="15.2">mots</w> <w n="15.3">que</w> <w n="15.4">ma</w> <w n="15.5">crainte</w></l>
						<l n="16" num="4.4"><w n="16.1">N</w>’<w n="16.2">exhalera</w> <w n="16.3">pas</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">La</w> <w n="17.2">femme</w> <w n="17.3">qui</w> <w n="17.4">pleure</w></l>
						<l n="18" num="5.2"><w n="18.1">Trahit</w> <w n="18.2">son</w> <w n="18.3">pouvoir</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Il</w> <w n="19.2">faut</w> <w n="19.3">qu</w>’<w n="19.4">elle</w> <w n="19.5">meure</w></l>
						<l n="20" num="5.4"><w n="20.1">Sans</w> <w n="20.2">le</w> <w n="20.3">laisser</w> <w n="20.4">voir</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Quand</w> <w n="21.2">le</w> <w n="21.3">cœur</w> <w n="21.4">sommeille</w></l>
						<l n="22" num="6.2"><w n="22.1">Frappé</w> <w n="22.2">de</w> <w n="22.3">langueur</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Ce</w> <w n="23.2">n</w>’<w n="23.3">est</w> <w n="23.4">pas</w> <w n="23.5">l</w>’<w n="23.6">oreille</w></l>
						<l n="24" num="6.4"><w n="24.1">Qui</w> <w n="24.2">comprend</w> <w n="24.3">un</w> <w n="24.4">cœur</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Il</w> <w n="25.2">est</w> <w n="25.3">un</w> <w n="25.4">langage</w></l>
						<l n="26" num="7.2"><w n="26.1">Appris</w> <w n="26.2">par</w> <w n="26.3">les</w> <w n="26.4">yeux</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">Nos</w> <w n="27.2">yeux</w>, <w n="27.3">page</w> <w n="27.4">à</w> <w n="27.5">page</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Y</w> <w n="28.2">trouvent</w> <w n="28.3">les</w> <w n="28.4">cieux</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">C</w>’<w n="29.2">est</w> <w n="29.3">un</w> <w n="29.4">livre</w> <w n="29.5">d</w>’<w n="29.6">ange</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Quand</w> <w n="30.2">on</w> <w n="30.3">est</w> <w n="30.4">aimé</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">Si</w> <w n="31.2">l</w>’<w n="31.3">un</w> <w n="31.4">des</w> <w n="31.5">deux</w> <w n="31.6">change</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Le</w> <w n="32.2">livre</w> <w n="32.3">est</w> <w n="32.4">fermé</w> !</l>
					</lg>
				</div></body></text></TEI>