<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES257">
					<head type="main">LA FIANCÉE POLONAISE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Et de tous leurs bienfaits écartant la mémoire. <lb></lb>
									Vont demander à Dieu le pardon de leur gloire.
								</quote>
								 <bibl><hi rend="smallcap">Delphine Gay (de Girardin),</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1">« <w n="1.1">Ouvrez</w> ! » — <w n="1.2">Qui</w> <w n="1.3">frappe</w> <w n="1.4">à</w> <w n="1.5">l</w>’<w n="1.6">heure</w></l>
						<l n="2" num="1.2"><w n="2.1">Où</w> <w n="2.2">l</w>’<w n="2.3">homme</w> <w n="2.4">dort</w> <w n="2.5">souvent</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Est</w>-<w n="3.2">ce</w> <w n="3.3">un</w> <w n="3.4">blessé</w> <w n="3.5">qui</w> <w n="3.6">pleure</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">revenir</w> <w n="4.3">vivant</w> ?</l>
						<l n="5" num="1.5">— « <w n="5.1">Ouvrez</w> ! <w n="5.2">je</w> <w n="5.3">vous</w> <w n="5.4">en</w> <w n="5.5">prie</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">De</w> <w n="6.2">mon</w> <w n="6.3">lointain</w> <w n="6.4">hameau</w>,</l>
						<l n="7" num="1.7"><w n="7.1">J</w>’<w n="7.2">apporte</w> <w n="7.3">à</w> <w n="7.4">la</w> <w n="7.5">patrie</w></l>
						<l n="8" num="1.8"><w n="8.1">Ce</w> <w n="8.2">que</w> <w n="8.3">j</w>’<w n="8.4">ai</w> <w n="8.5">de</w> <w n="8.6">plus</w> <w n="8.7">beau</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">« <w n="9.1">Des</w> <w n="9.2">anges</w> <w n="9.3">sentinelles</w>,</l>
						<l n="10" num="2.2"><w n="10.1">Envolés</w> <w n="10.2">sans</w> <w n="10.3">remords</w>,</l>
						<l n="11" num="2.3"><w n="11.1">J</w>’<w n="11.2">ai</w> <w n="11.3">vu</w> <w n="11.4">les</w> <w n="11.5">blanches</w> <w n="11.6">ailes</w></l>
						<l n="12" num="2.4"><w n="12.1">Envelopper</w> <w n="12.2">vos</w> <w n="12.3">morts</w> !</l>
						<l n="13" num="2.5"><w n="13.1">Regardez</w> ! <w n="13.2">Nulles</w> <w n="13.3">toiles</w></l>
						<l n="14" num="2.6"><w n="14.1">Ne</w> <w n="14.2">doublent</w> <w n="14.3">leurs</w> <w n="14.4">cercueils</w> ;</l>
						<l n="15" num="2.7"><w n="15.1">Pitié</w>, <w n="15.2">jette</w> <w n="15.3">tes</w> <w n="15.4">voiles</w> !</l>
						<l n="16" num="2.8"><w n="16.1">Ils</w> <w n="16.2">n</w>’<w n="16.3">ont</w> <w n="16.4">pas</w> <w n="16.5">de</w> <w n="16.6">linceuls</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Et</w> <w n="17.2">la</w> <w n="17.3">femme</w> <w n="17.4">au</w> <w n="17.5">front</w> <w n="17.6">d</w>’<w n="17.7">ange</w>,</l>
						<l n="18" num="3.2"><w n="18.1">Aux</w> <w n="18.2">yeux</w> <w n="18.3">tristes</w> <w n="18.4">sans</w> <w n="18.5">pleurs</w>,</l>
						<l n="19" num="3.3"><w n="19.1">De</w> <w n="19.2">la</w> <w n="19.3">terre</w> <w n="19.4">où</w> <w n="19.5">tout</w> <w n="19.6">change</w></l>
						<l n="20" num="3.4"><w n="20.1">Essayant</w> <w n="20.2">les</w> <w n="20.3">douleurs</w>,</l>
						<l n="21" num="3.5"><w n="21.1">Au</w> <w n="21.2">nom</w> <w n="21.3">du</w> <w n="21.4">Dieu</w> <w n="21.5">qui</w> <w n="21.6">donne</w>,</l>
						<l n="22" num="3.6"><w n="22.1">Sur</w> <w n="22.2">de</w> <w n="22.3">chastes</w> <w n="22.4">autels</w></l>
						<l n="23" num="3.7"><w n="23.1">Apporte</w> <w n="23.2">une</w> <w n="23.3">humble</w> <w n="23.4">aumône</w></l>
						<l n="24" num="3.8"><w n="24.1">À</w> <w n="24.2">ses</w> <w n="24.3">frères</w> <w n="24.4">mortels</w> !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1">« <w n="25.1">Je</w> <w n="25.2">suis</w>… <w n="25.3">je</w> <w n="25.4">fus</w> <w n="25.5">promise</w></l>
						<l n="26" num="4.2"><w n="26.1">À</w> <w n="26.2">qui</w> <w n="26.3">défend</w> <w n="26.4">vos</w> <w n="26.5">dieux</w> ;</l>
						<l n="27" num="4.3"><w n="27.1">Mais</w> <w n="27.2">la</w> <w n="27.3">noce</w> <w n="27.4">est</w> <w n="27.5">remise</w> :</l>
						<l n="28" num="4.4"><w n="28.1">On</w> <w n="28.2">se</w> <w n="28.3">retrouve</w> <w n="28.4">aux</w> <w n="28.5">cieux</w> !</l>
						<l n="29" num="4.5"><w n="29.1">Cet</w> <w n="29.2">anneau</w> <w n="29.3">qui</w> <w n="29.4">me</w> <w n="29.5">lie</w></l>
						<l n="30" num="4.6"><w n="30.1">Entraînera</w> <w n="30.2">mon</w> <w n="30.3">cœur</w> :</l>
						<l n="31" num="4.7"><w n="31.1">C</w>’<w n="31.2">est</w> <w n="31.3">le</w> <w n="31.4">don</w> <w n="31.5">de</w> <w n="31.6">ma</w> <w n="31.7">vie</w> !…</l>
						<l n="32" num="4.8"><w n="32.1">Qu</w>’<w n="32.2">il</w> <w n="32.3">vous</w> <w n="32.4">porte</w> <w n="32.5">bonheur</w>. »</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Et</w>, <w n="33.2">comme</w> <w n="33.3">la</w> <w n="33.4">colombe</w></l>
						<l n="34" num="5.2"><w n="34.1">Vient</w> <w n="34.2">d</w>’<w n="34.3">un</w> <w n="34.4">autre</w> <w n="34.5">séjour</w>,</l>
						<l n="35" num="5.3"><w n="35.1">Jeter</w> <w n="35.2">sur</w> <w n="35.3">une</w> <w n="35.4">tombe</w></l>
						<l n="36" num="5.4"><w n="36.1">Quelque</w> <w n="36.2">secret</w> <w n="36.3">d</w>’<w n="36.4">amour</w>,</l>
						<l n="37" num="5.5"><w n="37.1">Fidèle</w> <w n="37.2">à</w> <w n="37.3">son</w> <w n="37.4">épreuve</w>,</l>
						<l n="38" num="5.6"><w n="38.1">Sur</w> <w n="38.2">un</w> <w n="38.3">drapeau</w> <w n="38.4">sanglant</w></l>
						<l n="39" num="5.7"><w n="39.1">La</w> <w n="39.2">jeune</w> <w n="39.3">vierge</w> <w n="39.4">veuve</w></l>
						<l n="40" num="5.8"><w n="40.1">Posa</w> <w n="40.2">l</w>’<w n="40.3">anneau</w> <w n="40.4">tremblant</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">Ces</w> <w n="41.2">dons</w> <w n="41.3">que</w> <w n="41.4">le</w> <w n="41.5">cœur</w> <w n="41.6">sème</w></l>
						<l n="42" num="6.2"><w n="42.1">Aux</w> <w n="42.2">blessés</w> <w n="42.3">du</w> <w n="42.4">chemin</w>,</l>
						<l n="43" num="6.3"><w n="43.1">Dieu</w> <w n="43.2">les</w> <w n="43.3">voit</w>, <w n="43.4">Dieu</w> <w n="43.5">les</w> <w n="43.6">aime</w>,</l>
						<l n="44" num="6.4"><w n="44.1">Dieu</w> <w n="44.2">les</w> <w n="44.3">pèse</w> <w n="44.4">en</w> <w n="44.5">sa</w> <w n="44.6">main</w> ;</l>
						<l n="45" num="6.5"><w n="45.1">Et</w> <w n="45.2">de</w> <w n="45.3">vieux</w> <w n="45.4">prêtres</w> <w n="45.5">d</w>’<w n="45.6">armes</w>,</l>
						<l n="46" num="6.6"><w n="46.1">En</w> <w n="46.2">baisant</w> <w n="46.3">l</w>’<w n="46.4">anneau</w> <w n="46.5">d</w>’<w n="46.6">or</w>,</l>
						<l n="47" num="6.7"><w n="47.1">L</w>’<w n="47.2">enrichirent</w> <w n="47.3">de</w> <w n="47.4">larmes</w> :</l>
						<l n="48" num="6.8"><w n="48.1">Rois</w>, <w n="48.2">craignez</w> <w n="48.3">ce</w> <w n="48.4">trésor</w> !</l>
					</lg>
				</div></body></text></TEI>