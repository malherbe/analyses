<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES251">
					<head type="main">LES FLEURS</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Il est si beau de mourir jeune et de rendre <lb></lb>au Dieu qui nous juge, une vie encore pleine <lb></lb>d’illusions !
								</quote>
								 <bibl><hi rend="smallcap">H. de Latouche.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Oh</w> ! <w n="1.2">de</w> <w n="1.3">l</w>’<w n="1.4">air</w> ! <w n="1.5">des</w> <w n="1.6">parfums</w> ! <w n="1.7">des</w> <w n="1.8">fleurs</w> <w n="1.9">pour</w> <w n="1.10">me</w> <w n="1.11">nourrir</w> !</l>
						<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">semble</w> <w n="2.3">que</w> <w n="2.4">les</w> <w n="2.5">fleurs</w> <w n="2.6">alimentent</w> <w n="2.7">ma</w> <w n="2.8">vie</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">elles</w> <w n="3.3">vont</w> <w n="3.4">mourir</w>… <w n="3.5">Ah</w> ! <w n="3.6">je</w> <w n="3.7">leur</w> <w n="3.8">porte</w> <w n="3.9">envie</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Mourir</w> <w n="4.2">jeune</w>, <w n="4.3">au</w> <w n="4.4">soleil</w>, <w n="4.5">Dieu</w> ! <w n="4.6">que</w> <w n="4.7">c</w>’<w n="4.8">est</w> <w n="4.9">bien</w> <w n="4.10">mourir</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Pour</w> <w n="5.2">éteindre</w> <w n="5.3">une</w> <w n="5.4">fleur</w> <w n="5.5">il</w> <w n="5.6">faut</w> <w n="5.7">moins</w> <w n="5.8">qu</w>’<w n="5.9">un</w> <w n="5.10">orage</w> :</l>
						<l n="6" num="2.2"><w n="6.1">Moi</w>, <w n="6.2">je</w> <w n="6.3">sais</w> <w n="6.4">qu</w>’<w n="6.5">une</w> <w n="6.6">larme</w> <w n="6.7">effeuille</w> <w n="6.8">le</w> <w n="6.9">bonheur</w>.</l>
						<l n="7" num="2.3"><w n="7.1">À</w> <w n="7.2">la</w> <w n="7.3">fleur</w> <w n="7.4">qu</w>’<w n="7.5">on</w> <w n="7.6">va</w> <w n="7.7">fuir</w> <w n="7.8">qu</w>’<w n="7.9">importe</w> <w n="7.10">un</w> <w n="7.11">long</w> <w n="7.12">courage</w> ?</l>
						<l n="8" num="2.4"><w n="8.1">Heureuse</w> ! <w n="8.2">elle</w> <w n="8.3">succombe</w> <w n="8.4">à</w> <w n="8.5">son</w> <w n="8.6">premier</w> <w n="8.7">malheur</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Roseaux</w> <w n="9.2">moins</w> <w n="9.3">fortunés</w>, <w n="9.4">les</w> <w n="9.5">vents</w>, <w n="9.6">dans</w> <w n="9.7">leur</w> <w n="9.8">furie</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Vous</w> <w n="10.2">outragent</w> <w n="10.3">longtemps</w> <w n="10.4">sans</w> <w n="10.5">briser</w> <w n="10.6">votre</w> <w n="10.7">sort</w> !</l>
						<l n="11" num="3.3"><w n="11.1">Ainsi</w>, <w n="11.2">roseau</w> <w n="11.3">qui</w> <w n="11.4">marche</w> <w n="11.5">en</w> <w n="11.6">sa</w> <w n="11.7">gloire</w> <w n="11.8">flétrie</w>,</l>
						<l n="12" num="3.4"><w n="12.1">L</w>’<w n="12.2">homme</w> <w n="12.3">achète</w> <w n="12.4">longtemps</w> <w n="12.5">le</w> <w n="12.6">bienfait</w> <w n="12.7">de</w> <w n="12.8">la</w> <w n="12.9">mort</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Et</w> <w n="13.2">moi</w>, <w n="13.3">je</w> <w n="13.4">veux</w> <w n="13.5">des</w> <w n="13.6">fleurs</w> <w n="13.7">pour</w> <w n="13.8">appuyer</w> <w n="13.9">ma</w> <w n="13.10">vie</w> ;</l>
						<l n="14" num="4.2"><w n="14.1">À</w> <w n="14.2">leurs</w> <w n="14.3">frêles</w> <w n="14.4">parfums</w> <w n="14.5">j</w>’<w n="14.6">ai</w> <w n="14.7">de</w> <w n="14.8">quoi</w> <w n="14.9">me</w> <w n="14.10">nourrir</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Mais</w> <w n="15.2">elles</w> <w n="15.3">vont</w> <w n="15.4">mourir</w>… <w n="15.5">Ah</w> ! <w n="15.6">je</w> <w n="15.7">leur</w> <w n="15.8">porte</w> <w n="15.9">envie</w> ;</l>
						<l n="16" num="4.4"><w n="16.1">Mourir</w> <w n="16.2">jeune</w>, <w n="16.3">au</w> <w n="16.4">soleil</w>, <w n="16.5">Dieu</w> ! <w n="16.6">que</w> <w n="16.7">c</w>’<w n="16.8">est</w> <w n="16.9">bien</w> <w n="16.10">mourir</w> !</l>
					</lg>
				</div></body></text></TEI>