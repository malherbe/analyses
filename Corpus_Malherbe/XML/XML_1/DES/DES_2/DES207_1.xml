<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES207">
					<head type="main">LA VIE ET LA MORT DU RAMIER</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Hélas ! Nous n’avons pas juré de vivre ensemble, <lb></lb>
									Mais nous avons promis de nous aimer toujours !
								</quote>
								 <bibl><hi rend="smallcap">Jules de Resseguier.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">De</w> <w n="1.2">la</w> <w n="1.3">colombe</w> <w n="1.4">au</w> <w n="1.5">bois</w> <w n="1.6">c</w>’<w n="1.7">est</w> <w n="1.8">le</w> <w n="1.9">ramier</w> <w n="1.10">fidèle</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">S</w>’<w n="2.2">il</w> <w n="2.3">vole</w> <w n="2.4">sans</w> <w n="2.5">repos</w>, <w n="2.6">c</w>’<w n="2.7">est</w> <w n="2.8">qu</w>’<w n="2.9">il</w> <w n="2.10">vole</w> <w n="2.11">auprès</w> <w n="2.12">d</w>’<w n="2.13">elle</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Il</w> <w n="3.2">ne</w> <w n="3.3">peut</w> <w n="3.4">s</w>’<w n="3.5">appuyer</w> <w n="3.6">qu</w>’<w n="3.7">au</w> <w n="3.8">nid</w> <w n="3.9">de</w> <w n="3.10">ses</w> <w n="3.11">amours</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Car</w> <w n="4.2">des</w> <w n="4.3">ailes</w> <w n="4.4">de</w> <w n="4.5">feu</w> <w n="4.6">l</w>’<w n="4.7">y</w> <w n="4.8">réchauffent</w> <w n="4.9">toujours</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Laissez</w> <w n="5.2">battre</w> <w n="5.3">et</w> <w n="5.4">brûler</w> <w n="5.5">deux</w> <w n="5.6">cœurs</w> <w n="5.7">si</w> <w n="5.8">bien</w> <w n="5.9">ensemble</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Leur</w> <w n="6.2">vie</w> <w n="6.3">est</w> <w n="6.4">un</w> <w n="6.5">fil</w> <w n="6.6">d</w>’<w n="6.7">or</w> <w n="6.8">qu</w>’<w n="6.9">un</w> <w n="6.10">nœud</w> <w n="6.11">secret</w> <w n="6.12">assemble</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Il</w> <w n="7.2">traverse</w> <w n="7.3">le</w> <w n="7.4">monde</w> <w n="7.5">et</w> <w n="7.6">ce</w> <w n="7.7">qu</w>’<w n="7.8">il</w> <w n="7.9">fait</w> <w n="7.10">souffrir</w> :</l>
						<l n="8" num="2.4"><w n="8.1">Ne</w> <w n="8.2">le</w> <w n="8.3">déliez</w> <w n="8.4">pas</w> ! <w n="8.5">Vous</w> <w n="8.6">les</w> <w n="8.7">feriez</w> <w n="8.8">mourir</w> !</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ils</w> <w n="9.2">ne</w> <w n="9.3">veulent</w> <w n="9.4">à</w> <w n="9.5">deux</w> <w n="9.6">qu</w>’<w n="9.7">un</w> <w n="9.8">peu</w> <w n="9.9">d</w>’<w n="9.10">air</w>, <w n="9.11">un</w> <w n="9.12">peu</w> <w n="9.13">d</w>’<w n="9.14">ombre</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Une</w> <w n="10.2">place</w> <w n="10.3">au</w> <w n="10.4">ruisseau</w> <w n="10.5">qui</w> <w n="10.6">rafraîchit</w> <w n="10.7">le</w> <w n="10.8">cœur</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Seuls</w>, <w n="11.2">entre</w> <w n="11.3">ciel</w> <w n="11.4">et</w> <w n="11.5">terre</w>, <w n="11.6">un</w> <w n="11.7">nid</w> <w n="11.8">suave</w> <w n="11.9">et</w> <w n="11.10">sombre</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Pour</w> <w n="12.2">s</w>’<w n="12.3">entre</w>-<w n="12.4">aider</w> <w n="12.5">à</w> <w n="12.6">vivre</w>, <w n="12.7">ou</w> <w n="12.8">cacher</w> <w n="12.9">leur</w> <w n="12.10">bonheur</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Quand</w> <w n="13.2">vous</w> <w n="13.3">ne</w> <w n="13.4">verrez</w> <w n="13.5">plus</w> <w n="13.6">passer</w> <w n="13.7">par</w> <w n="13.8">ce</w> <w n="13.9">rivage</w></l>
						<l n="14" num="4.2"><w n="14.1">Cette</w> <w n="14.2">blanche</w> <w n="14.3">moitié</w> <w n="14.4">de</w> <w n="14.5">la</w> <w n="14.6">colombe</w> <w n="14.7">aux</w> <w n="14.8">bois</w>,</l>
						<l n="15" num="4.3"><w n="15.1">N</w>’<w n="15.2">allez</w> <w n="15.3">pas</w> <w n="15.4">croire</w> <w n="15.5">au</w> <w n="15.6">moins</w> <w n="15.7">que</w> <w n="15.8">l</w>’<w n="15.9">un</w> <w n="15.10">d</w>’<w n="15.11">eux</w> <w n="15.12">soit</w> <w n="15.13">volage</w></l>
						<l n="16" num="4.4"><w n="16.1">Bien</w> <w n="16.2">qu</w>’<w n="16.3">ils</w> <w n="16.4">aiment</w> <w n="16.5">toujours</w>, <w n="16.6">ils</w> <w n="16.7">n</w>’<w n="16.8">aiment</w> <w n="16.9">qu</w>’<w n="16.10">une</w> <w n="16.11">fois</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Laissez</w>-<w n="17.2">vous</w> <w n="17.3">entraîner</w> <w n="17.4">sur</w> <w n="17.5">leurs</w> <w n="17.6">traces</w> <w n="17.7">perdues</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Vers</w> <w n="18.2">le</w> <w n="18.3">nid</w>, <w n="18.4">doux</w> <w n="18.5">sépulcre</w> <w n="18.6">alors</w> <w n="18.7">silencieux</w>,</l>
						<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">vous</w> <w n="19.3">y</w> <w n="19.4">trouverez</w> <w n="19.5">quatre</w> <w n="19.6">ailes</w> <w n="19.7">détendues</w></l>
						<l n="20" num="5.4"><w n="20.1">Sur</w> <w n="20.2">deux</w> <w n="20.3">cœurs</w> <w n="20.4">mal</w> <w n="20.5">éteints</w> <w n="20.6">rallumés</w> <w n="20.7">dans</w> <w n="20.8">les</w> <w n="20.9">cieux</w> !</l>
					</lg>
				</div></body></text></TEI>