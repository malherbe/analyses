<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES208">
					<head type="main">L’ATTENTE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">je</w> <w n="1.3">ne</w> <w n="1.4">te</w> <w n="1.5">vois</w> <w n="1.6">pas</w>, <w n="1.7">le</w> <w n="1.8">temps</w> <w n="1.9">m</w>’<w n="1.10">accable</w>, <w n="1.11">et</w> <w n="1.12">l</w>’<w n="1.13">heure</w></l>
						<l n="2" num="1.2"><w n="2.1">A</w> <w n="2.2">je</w> <w n="2.3">ne</w> <w n="2.4">sais</w> <w n="2.5">quel</w> <w n="2.6">poids</w> <w n="2.7">impossible</w> <w n="2.8">à</w> <w n="2.9">porter</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">sens</w> <w n="3.3">languir</w> <w n="3.4">mon</w> <w n="3.5">cœur</w>, <w n="3.6">qui</w> <w n="3.7">cherche</w> <w n="3.8">à</w> <w n="3.9">me</w> <w n="3.10">quitter</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">ma</w> <w n="4.3">tête</w> <w n="4.4">se</w> <w n="4.5">penche</w>, <w n="4.6">et</w> <w n="4.7">je</w> <w n="4.8">souffre</w>, <w n="4.9">et</w> <w n="4.10">je</w> <w n="4.11">pleure</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Quand</w> <w n="5.2">ta</w> <w n="5.3">voix</w> <w n="5.4">saisissante</w> <w n="5.5">atteint</w> <w n="5.6">mon</w> <w n="5.7">souvenir</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Je</w> <w n="6.2">tressaille</w>, <w n="6.3">j</w>’<w n="6.4">écoute</w>… <w n="6.5">et</w> <w n="6.6">j</w>’<w n="6.7">espère</w> <w n="6.8">immobile</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">on</w> <w n="7.4">dirait</w> <w n="7.5">que</w> <w n="7.6">Dieu</w> <w n="7.7">touche</w> <w n="7.8">un</w> <w n="7.9">roseau</w> <w n="7.10">débile</w> ;</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">moi</w>, <w n="8.3">tout</w> <w n="8.4">moi</w> <w n="8.5">répond</w> : « <w n="8.6">Dieu</w> ! <w n="8.7">faites</w>-<w n="8.8">le</w> <w n="8.9">venir</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Quand</w> <w n="9.2">sur</w> <w n="9.3">tes</w> <w n="9.4">traits</w> <w n="9.5">charmants</w> <w n="9.6">j</w>’<w n="9.7">arrête</w> <w n="9.8">ma</w> <w n="9.9">pensée</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Tous</w> <w n="10.2">mes</w> <w n="10.3">traits</w> <w n="10.4">sont</w> <w n="10.5">empreints</w> <w n="10.6">de</w> <w n="10.7">crainte</w> <w n="10.8">et</w> <w n="10.9">de</w> <w n="10.10">bonheur</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2">ai</w> <w n="11.3">froid</w> <w n="11.4">dans</w> <w n="11.5">mes</w> <w n="11.6">cheveux</w> ; <w n="11.7">ma</w> <w n="11.8">vie</w> <w n="11.9">est</w> <w n="11.10">oppressée</w>,</l>
						<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">ton</w> <w n="12.3">nom</w>, <w n="12.4">tout</w> <w n="12.5">à</w> <w n="12.6">coup</w>, <w n="12.7">s</w>’<w n="12.8">échappe</w> <w n="12.9">de</w> <w n="12.10">mon</w> <w n="12.11">cœur</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Quand</w> <w n="13.2">c</w>’<w n="13.3">est</w> <w n="13.4">toi</w>-<w n="13.5">même</w>, <w n="13.6">enfin</w> ! <w n="13.7">quand</w> <w n="13.8">j</w>’<w n="13.9">ai</w> <w n="13.10">cessé</w> <w n="13.11">d</w>’<w n="13.12">attendre</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Tremblante</w>, <w n="14.2">je</w> <w n="14.3">me</w> <w n="14.4">sauve</w> <w n="14.5">en</w> <w n="14.6">te</w> <w n="14.7">tendant</w> <w n="14.8">les</w> <w n="14.9">bras</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Je</w> <w n="15.2">n</w>’<w n="15.3">ose</w> <w n="15.4">te</w> <w n="15.5">parler</w>, <w n="15.6">et</w> <w n="15.7">j</w>’<w n="15.8">ai</w> <w n="15.9">peur</w> <w n="15.10">de</w> <w n="15.11">t</w>’<w n="15.12">entendre</w> ;</l>
						<l n="16" num="4.4"><w n="16.1">Mais</w> <w n="16.2">tu</w> <w n="16.3">cherches</w> <w n="16.4">mon</w> <w n="16.5">âme</w>, <w n="16.6">et</w> <w n="16.7">toi</w> <w n="16.8">seul</w> <w n="16.9">l</w>’<w n="16.10">obtiendras</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Suis</w>-<w n="17.2">je</w> <w n="17.3">une</w> <w n="17.4">sœur</w> <w n="17.5">tardive</w> <w n="17.6">à</w> <w n="17.7">tes</w> <w n="17.8">vœux</w> <w n="17.9">accordée</w> ?</l>
						<l n="18" num="5.2"><w n="18.1">Es</w>-<w n="18.2">tu</w> <w n="18.3">l</w>’<w n="18.4">ombre</w> <w n="18.5">promise</w> <w n="18.6">à</w> <w n="18.7">mes</w> <w n="18.8">timides</w> <w n="18.9">pas</w> ?</l>
						<l n="19" num="5.3"><w n="19.1">Mais</w> <w n="19.2">je</w> <w n="19.3">me</w> <w n="19.4">sens</w> <w n="19.5">frémir</w>. <w n="19.6">Moi</w>, <w n="19.7">ta</w> <w n="19.8">sœur</w> ! <w n="19.9">quelle</w> <w n="19.10">idée</w> !</l>
						<l n="20" num="5.4"><w n="20.1">Toi</w>, <w n="20.2">mon</w> <w n="20.3">frère</w> !… <w n="20.4">ô</w> <w n="20.5">terreur</w> ! <w n="20.6">Dis</w> <w n="20.7">que</w> <w n="20.8">tu</w> <w n="20.9">ne</w> <w n="20.10">l</w>’<w n="20.11">es</w> <w n="20.12">pas</w> !</l>
					</lg>
				</div></body></text></TEI>