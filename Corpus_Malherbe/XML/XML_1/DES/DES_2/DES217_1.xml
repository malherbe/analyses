<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES217">
					<head type="main">LA JALOUSE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Pour la dernière fois je veux tromper mon cœur, <lb></lb>
									L’enivrer d’espérance, hélas ! et de mensonges !
								</quote>
								 <bibl><hi rend="smallcap">Charles Nodier.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Sans</w> <w n="1.2">signer</w> <w n="1.3">ma</w> <w n="1.4">tristesse</w>, <w n="1.5">un</w> <w n="1.6">jour</w>, <w n="1.7">au</w> <w n="1.8">seul</w> <w n="1.9">que</w> <w n="1.10">j</w>’<w n="1.11">aime</w></l>
						<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2">écrivis</w> <w n="2.3">en</w> <w n="2.4">secret</w> : « <w n="2.5">Elle</w> <w n="2.6">attend</w> : <w n="2.7">cherche</w>-<w n="2.8">la</w> !</l>
						<l n="3" num="1.3"><w n="3.1">Devine</w> <w n="3.2">qui</w> <w n="3.3">t</w>’<w n="3.4">appelle</w>, <w n="3.5">et</w> <w n="3.6">réponds</w> : « <w n="3.7">Me</w> <w n="3.8">voilà</w> ! »</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">quand</w> <w n="4.3">il</w> <w n="4.4">accourut</w>, <w n="4.5">quand</w> <w n="4.6">je</w> <w n="4.7">venais</w> <w n="4.8">moi</w>-<w n="4.9">même</w>,</l>
						<l n="5" num="1.5"><w n="5.1">Quand</w> <w n="5.2">je</w> <w n="5.3">retins</w> <w n="5.4">le</w> <w n="5.5">cri</w> <w n="5.6">d</w>’<w n="5.7">un</w> <w n="5.8">bonheur</w> <w n="5.9">plein</w> <w n="5.10">d</w>’<w n="5.11">effroi</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Il</w> <w n="6.2">n</w>’<w n="6.3">a</w> <w n="6.4">pas</w> <w n="6.5">dit</w> : « <w n="6.6">C</w>’<w n="6.7">est</w> <w n="6.8">elle</w> ! » <w n="6.9">il</w> <w n="6.10">n</w>’<w n="6.11">a</w> <w n="6.12">pas</w> <w n="6.13">dit</w> : « <w n="6.14">C</w>’<w n="6.15">est</w> <w n="6.16">toi</w> ! »</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Sans</w> <w n="7.2">me</w> <w n="7.3">nommer</w>, <w n="7.4">craintive</w> <w n="7.5">en</w> <w n="7.6">livrant</w> <w n="7.7">mes</w> <w n="7.8">alarmes</w>,</l>
						<l n="8" num="2.2"><w n="8.1">J</w>’<w n="8.2">écrivis</w> : « <w n="8.3">J</w>’<w n="8.4">ai</w> <w n="8.5">pleuré</w>. <w n="8.6">Je</w> <w n="8.7">pleure</w>… <w n="8.8">C</w>’<w n="8.9">est</w> <w n="8.10">pour</w> <w n="8.11">vous</w> !</l>
						<l n="9" num="2.3"><w n="9.1">Que</w> <w n="9.2">l</w>’<w n="9.3">amour</w> <w n="9.4">vous</w> <w n="9.5">éclaire</w> <w n="9.6">et</w> <w n="9.7">demeure</w> <w n="9.8">entre</w> <w n="9.9">nous</w> ! »</l>
						<l n="10" num="2.4"><w n="10.1">Et</w> <w n="10.2">quand</w> <w n="10.3">il</w> <w n="10.4">vit</w> <w n="10.5">mes</w> <w n="10.6">yeux</w> <w n="10.7">encor</w> <w n="10.8">voilés</w> <w n="10.9">de</w> <w n="10.10">larmes</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Quand</w> <w n="11.2">il</w> <w n="11.3">toucha</w> <w n="11.4">ma</w> <w n="11.5">main</w> <w n="11.6">qui</w> <w n="11.7">lui</w> <w n="11.8">rendait</w> <w n="11.9">ma</w> <w n="11.10">foi</w>,</l>
						<l n="12" num="2.6"><w n="12.1">Il</w> <w n="12.2">n</w>’<w n="12.3">a</w> <w n="12.4">pas</w> <w n="12.5">dit</w> : « <w n="12.6">C</w>’<w n="12.7">est</w> <w n="12.8">elle</w> ! » <w n="12.9">il</w> <w n="12.10">n</w>’<w n="12.11">a</w> <w n="12.12">pas</w> <w n="12.13">dit</w> : « <w n="12.14">C</w>’<w n="12.15">est</w> <w n="12.16">toi</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Sans</w> <w n="13.2">dire</w> : « <w n="13.3">C</w>’<w n="13.4">était</w> <w n="13.5">moi</w> ! » <w n="13.6">je</w> <w n="13.7">m</w>’<w n="13.8">enfuis</w>, <w n="13.9">je</w> <w n="13.10">succombe</w> ;</l>
						<l n="14" num="3.2"><w n="14.1">Bientôt</w> <w n="14.2">je</w> <w n="14.3">n</w>’<w n="14.4">aurai</w> <w n="14.5">plus</w> <w n="14.6">de</w> <w n="14.7">secret</w> <w n="14.8">à</w> <w n="14.9">cacher</w>.</l>
						<l n="15" num="3.3"><w n="15.1">S</w>’<w n="15.2">il</w> <w n="15.3">rêve</w> <w n="15.4">alors</w> <w n="15.5">au</w> <w n="15.6">nom</w> <w n="15.7">qui</w> <w n="15.8">courut</w> <w n="15.9">le</w> <w n="15.10">chercher</w>,</l>
						<l n="16" num="3.4"><w n="16.1">Il</w> <w n="16.2">le</w> <w n="16.3">devinera</w> <w n="16.4">peut</w>-<w n="16.5">être</w> <w n="16.6">sur</w> <w n="16.7">ma</w> <w n="16.8">tombe</w> ;</l>
						<l n="17" num="3.5"><w n="17.1">Et</w>, <w n="17.2">soulevant</w> <w n="17.3">enfin</w> <w n="17.4">ma</w> <w n="17.5">vie</w> <w n="17.6">avec</w> <w n="17.7">effroi</w>,</l>
						<l n="18" num="3.6"><w n="18.1">Qu</w>’<w n="18.2">il</w> <w n="18.3">dise</w> <w n="18.4">au</w> <w n="18.5">moins</w> : « <w n="18.6">C</w>’<w n="18.7">est</w> <w n="18.8">elle</w> ! <w n="18.9">ô</w> <w n="18.10">pitié</w> ! <w n="18.11">c</w>’<w n="18.12">était</w> <w n="18.13">toi</w> ! »</l>
					</lg>
				</div></body></text></TEI>