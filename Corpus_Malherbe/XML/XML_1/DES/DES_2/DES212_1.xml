<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES212">
					<head type="main">LES MOTS TRISTES</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									Quoi ! je mourrai ! Quoi ! le temps à sa suite <lb></lb>
									Amènera l’irrévocable jour, <lb></lb>
									Le jour muet et sombre, où sans retour <lb></lb>
									S’arrêtera ce cœur qui bat si vite !
								</quote>
								 <bibl><hi rend="smallcap">Madame Amable Tastu.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Souvent</w> <w n="1.2">toute</w> <w n="1.3">plongée</w> <w n="1.4">au</w> <w n="1.5">fond</w> <w n="1.6">de</w> <w n="1.7">ma</w> <w n="1.8">tendresse</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Expiant</w>, <w n="2.2">Dieu</w> ]<w n="2.3">e</w> <w n="2.4">veut</w> ! <w n="2.5">le</w> <w n="2.6">nom</w> <w n="2.7">de</w> <w n="2.8">ta</w> <w n="2.9">maîtresse</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Je</w> <w n="3.2">pense</w> <w n="3.3">que</w> <w n="3.4">je</w> <w n="3.5">souffre</w> (<w n="3.6">aimer</w> <w n="3.7">tant</w>, <w n="3.8">c</w>’<w n="3.9">est</w> <w n="3.10">souffrir</w>),</l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">un</w> <w n="4.3">jour</w> <w n="4.4">je</w> <w n="4.5">t</w>’<w n="4.6">ai</w> <w n="4.7">vu</w> <w n="4.8">pâle</w>, <w n="4.9">et</w> <w n="4.10">que</w> <w n="4.11">l</w>’<w n="4.12">on</w> <w n="4.13">peut</w> <w n="4.14">mourir</w></l>
						<l n="5" num="1.5"><w n="5.1">Jeune</w>, <w n="5.2">entends</w>-<w n="5.3">tu</w> ? <w n="5.4">Je</w> <w n="5.5">meurs</w> <w n="5.6">pour</w> <w n="5.7">mourir</w> <w n="5.8">la</w> <w n="5.9">première</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Pour</w> <w n="6.2">braver</w> <w n="6.3">avant</w> <w n="6.4">toi</w> <w n="6.5">la</w> <w n="6.6">nuit</w> <w n="6.7">ou</w> <w n="6.8">la</w> <w n="6.9">lumière</w>.</l>
						<l n="7" num="1.7"><w n="7.1">J</w>’<w n="7.2">entends</w> <w n="7.3">des</w> <w n="7.4">mots</w> <w n="7.5">affreux</w> <w n="7.6">tinter</w> <w n="7.7">autour</w> <w n="7.8">de</w> <w n="7.9">moi</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Ces</w> <w n="8.2">mots</w> <w n="8.3">que</w> <w n="8.4">dans</w> <w n="8.5">l</w>’<w n="8.6">enfance</w> <w n="8.7">on</w> <w n="8.8">apprend</w> <w n="8.9">sans</w> <w n="8.10">les</w> <w n="8.11">croire</w>,</l>
						<l n="9" num="1.9"><w n="9.1">Roulant</w>, <w n="9.2">sans</w> <w n="9.3">la</w> <w n="9.4">troubler</w>, <w n="9.5">au</w> <w n="9.6">fond</w> <w n="9.7">de</w> <w n="9.8">la</w> <w n="9.9">mémoire</w>,</l>
						<l n="10" num="1.10"><w n="10.1">Inécoutés</w> <w n="10.2">longtemps</w>, <w n="10.3">longtemps</w> <w n="10.4">vides</w> <w n="10.5">d</w>’<w n="10.6">effroi</w>,</l>
						<l n="11" num="1.11"><w n="11.1">Tout</w> <w n="11.2">à</w> <w n="11.3">coup</w> <w n="11.4">pleins</w> <w n="11.5">d</w>’<w n="11.6">accents</w>, <w n="11.7">pleins</w> <w n="11.8">de</w> <w n="11.9">deuil</w>, <w n="11.10">pleins</w> <w n="11.11">de</w> <w n="11.12">larmes</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Bondissant</w> <w n="12.2">sur</w> <w n="12.3">le</w> <w n="12.4">cœur</w> <w n="12.5">comme</w> <w n="12.6">un</w> <w n="12.7">tocsin</w> <w n="12.8">d</w>’<w n="12.9">alarmes</w>.</l>
						<l n="13" num="1.13"><w n="13.1">C</w>’<w n="13.2">est</w> <w n="13.3">la</w> <w n="13.4">cloche</w> <w n="13.5">effrayée</w> <w n="13.6">au</w> <w n="13.7">cri</w> <w n="13.8">sinistre</w> <w n="13.9">et</w> <w n="13.10">prompt</w>,</l>
						<l n="14" num="1.14"><w n="14.1">Dont</w> <w n="14.2">le</w> <w n="14.3">pouls</w> <w n="14.4">bat</w> <w n="14.5">rapide</w> <w n="14.6">et</w> <w n="14.7">fiévreux</w> <w n="14.8">dans</w> <w n="14.9">l</w>’<w n="14.10">espace</w>,</l>
						<l n="15" num="1.15"><w n="15.1">Redoublant</w> <w n="15.2">son</w> <w n="15.3">frisson</w> <w n="15.4">avec</w> <w n="15.5">la</w> <w n="15.6">mort</w> <w n="15.7">qui</w> <w n="15.8">passe</w> ;</l>
						<l n="16" num="1.16"><w n="16.1">De</w> <w n="16.2">pâleur</w> <w n="16.3">et</w> <w n="16.4">de</w> <w n="16.5">crainte</w> <w n="16.6">elle</w> <w n="16.7">cerne</w> <w n="16.8">mon</w> <w n="16.9">front</w> ;</l>
						<l n="17" num="1.17"><w n="17.1">Sous</w> <w n="17.2">mes</w> <w n="17.3">cheveux</w> <w n="17.4">levés</w> <w n="17.5">une</w> <w n="17.6">eau</w> <w n="17.7">froide</w> <w n="17.8">circule</w> :</l>
						<l n="18" num="1.18"><w n="18.1">Ah</w> ! <w n="18.2">ne</w> <w n="18.3">t</w>’<w n="18.4">étonne</w> <w n="18.5">pas</w>. <w n="18.6">J</w>’<w n="18.7">aime</w> ! <w n="18.8">je</w> <w n="18.9">suis</w> <w n="18.10">crédule</w> ;</l>
						<l n="19" num="1.19"><w n="19.1">Ou</w> <w n="19.2">plutôt</w> <w n="19.3">j</w>’<w n="19.4">ai</w> <w n="19.5">des</w> <w n="19.6">yeux</w> <w n="19.7">qui</w> <w n="19.8">plongent</w> <w n="19.9">sous</w> <w n="19.10">les</w> <w n="19.11">fleurs</w> ;</l>
						<l n="20" num="1.20"><w n="20.1">Au</w> <w n="20.2">fond</w> <w n="20.3">de</w> <w n="20.4">nos</w> <w n="20.5">baisers</w> <w n="20.6">je</w> <w n="20.7">sens</w> <w n="20.8">rouler</w> <w n="20.9">des</w> <w n="20.10">pleurs</w>.</l>
					</lg>
					<lg n="2">
						<l n="21" num="2.1"><w n="21.1">L</w>’<w n="21.2">avenir</w> <w n="21.3">sonne</w> ; <w n="21.4">arrête</w> ! <w n="21.5">Oh</w> ! <w n="21.6">que</w> <w n="21.7">nous</w> <w n="21.8">marchons</w> <w n="21.9">vite</w> !</l>
						<l n="22" num="2.2"><w n="22.1">Qu</w>’<w n="22.2">une</w> <w n="22.3">heure</w> <w n="22.4">a</w> <w n="22.5">peu</w> <w n="22.6">de</w> <w n="22.7">poids</w> <w n="22.8">sur</w> <w n="22.9">un</w> <w n="22.10">cœur</w> <w n="22.11">qui</w> <w n="22.12">palpite</w> !</l>
						<l n="23" num="2.3"><w n="23.1">Ne</w> <w n="23.2">peut</w>-<w n="23.3">on</w> <w n="23.4">lentement</w> <w n="23.5">respirer</w> <w n="23.6">le</w> <w n="23.7">bonheur</w>,</l>
						<l n="24" num="2.4"><w n="24.1">Vivre</w> <w n="24.2">sans</w> <w n="24.3">éveiller</w> <w n="24.4">le</w> <w n="24.5">temps</w> <w n="24.6">et</w> <w n="24.7">le</w> <w n="24.8">malheur</w> ?</l>
						<l n="25" num="2.5"><w n="25.1">Embrasse</w>-<w n="25.2">moi</w> : <w n="25.3">plus</w> <w n="25.4">près</w> <w n="25.5">de</w> <w n="25.6">ta</w> <w n="25.7">moitié</w> <w n="25.8">qui</w> <w n="25.9">tremble</w>,</l>
						<l n="26" num="2.6"><w n="26.1">Laisse</w> <w n="26.2">passer</w> <w n="26.3">la</w> <w n="26.4">vie</w> ; <w n="26.5">elle</w> <w n="26.6">nous</w> <w n="26.7">aime</w> <w n="26.8">ensemble</w> !</l>
						<l n="27" num="2.7"><w n="27.1">Quand</w> <w n="27.2">tu</w> <w n="27.3">m</w>’<w n="27.4">as</w> <w n="27.5">dit</w> <w n="27.6">adieu</w>, <w n="27.7">je</w> <w n="27.8">me</w> <w n="27.9">donne</w> <w n="27.10">à</w> <w n="27.11">rêver</w>,</l>
						<l n="28" num="2.8"><w n="28.1">Et</w> <w n="28.2">les</w> <w n="28.3">mots</w> <w n="28.4">qui</w> <w n="28.5">font</w> <w n="28.6">peur</w> <w n="28.7">reviennent</w> <w n="28.8">me</w> <w n="28.9">trouver</w> :</l>
						<l n="29" num="2.9"><w n="29.1">Ils</w> <w n="29.2">disent</w> <w n="29.3">que</w> <w n="29.4">l</w>’<w n="29.5">on</w> <w n="29.6">meurt</w> <w n="29.7">en</w> <w n="29.8">sortant</w> <w n="29.9">d</w>’<w n="29.10">une</w> <w n="29.11">fête</w>,</l>
						<l n="30" num="2.10"><w n="30.1">Et</w> <w n="30.2">je</w> <w n="30.3">t</w>’<w n="30.4">y</w> <w n="30.5">vois</w> <w n="30.6">courir</w>, <w n="30.7">et</w> <w n="30.8">je</w> <w n="30.9">cache</w> <w n="30.10">ma</w> <w n="30.11">tête</w>,</l>
						<l n="31" num="2.11"><w n="31.1">Et</w> <w n="31.2">leurs</w> <w n="31.3">sons</w> <w n="31.4">plus</w> <w n="31.5">aigus</w> <w n="31.6">sifflent</w> <w n="31.7">entre</w> <w n="31.8">mes</w> <w n="31.9">doigts</w> :</l>
						<l n="32" num="2.12">« <w n="32.1">On</w> <w n="32.2">meurt</w> ! <w n="32.3">on</w> <w n="32.4">meurt</w> ! <w n="32.5">on</w> <w n="32.6">meurt</w> ! <w n="32.7">on</w> <w n="32.8">se</w> <w n="32.9">quitte</w> <w n="32.10">une</w> <w n="32.11">fois</w> ! »</l>
						<l n="33" num="2.13"><w n="33.1">Puis</w> <w n="33.2">ton</w> <w n="33.3">nom</w> !… <w n="33.4">Ah</w> ! <w n="33.5">ce</w> <w n="33.6">nom</w> <w n="33.7">m</w>’<w n="33.8">éveille</w>, <w n="33.9">il</w> <w n="33.10">me</w> <w n="33.11">rassure</w> ;</l>
						<l n="34" num="2.14"><w n="34.1">Ton</w> <w n="34.2">baiser</w> <w n="34.3">presse</w> <w n="34.4">encor</w> <w n="34.5">mes</w> <w n="34.6">lèvres</w>, <w n="34.7">j</w>’<w n="34.8">en</w> <w n="34.9">suis</w> <w n="34.10">sûre</w> !</l>
						<l n="35" num="2.15"><w n="35.1">Et</w> <w n="35.2">je</w> <w n="35.3">m</w>’<w n="35.4">appelle</w> <w n="35.5">folle</w> <w n="35.6">en</w> <w n="35.7">me</w> <w n="35.8">sentant</w> <w n="35.9">frémir</w>.</l>
						<l n="36" num="2.16"><w n="36.1">Vois</w> ! <w n="36.2">qu</w>’<w n="36.3">un</w> <w n="36.4">portrait</w> <w n="36.5">de</w> <w n="36.6">toi</w> <w n="36.7">serait</w> <w n="36.8">doux</w> <w n="36.9">sous</w> <w n="36.10">mes</w> <w n="36.11">larmes</w>.</l>
						<l n="37" num="2.17"><w n="37.1">Et</w> <w n="37.2">je</w> <w n="37.3">n</w>’<w n="37.4">ai</w> <w n="37.5">que</w> <w n="37.6">ton</w> <w n="37.7">nom</w> ! <w n="37.8">ton</w> <w n="37.9">nom</w> ! <w n="37.10">pas</w> <w n="37.11">d</w>’<w n="37.12">autres</w> <w n="37.13">armes</w> !</l>
						<l n="38" num="2.18"><w n="38.1">Si</w> <w n="38.2">je</w> <w n="38.3">chantais</w>, <w n="38.4">ma</w> <w n="38.5">voix</w> <w n="38.6">sortirait</w> <w n="38.7">pour</w> <w n="38.8">gémir</w> ;</l>
						<l n="39" num="2.19"><w n="39.1">À</w> <w n="39.2">mon</w> <w n="39.3">âme</w> <w n="39.4">qui</w> <w n="39.5">pense</w> <w n="39.6">elle</w> <w n="39.7">reste</w> <w n="39.8">attachée</w> ;</l>
						<l n="40" num="2.20"><w n="40.1">Dans</w> <w n="40.2">mes</w> <w n="40.3">pâles</w> <w n="40.4">tourments</w> <w n="40.5">je</w> <w n="40.6">demeure</w> <w n="40.7">cachée</w> :</l>
						<l n="41" num="2.21"><w n="41.1">Alors</w> <w n="41.2">je</w> <w n="41.3">rêve</w> <w n="41.4">un</w> <w n="41.5">monde</w> <w n="41.6">où</w> <w n="41.7">dureront</w> <w n="41.8">toujours</w></l>
						<l n="42" num="2.22"><w n="42.1">Les</w> <w n="42.2">caresses</w> <w n="42.3">du</w> <w n="42.4">cœur</w> <w n="42.5">et</w> <w n="42.6">les</w> <w n="42.7">libres</w> <w n="42.8">amours</w>.</l>
						<l n="43" num="2.23"><w n="43.1">Prends</w> <w n="43.2">mes</w> <w n="43.3">ailes</w>, <w n="43.4">viens</w> ! <w n="43.5">viens</w> <w n="43.6">où</w> <w n="43.7">jamais</w> <w n="43.8">la</w> <w n="43.9">pensée</w></l>
						<l n="44" num="2.24"><w n="44.1">N</w>’<w n="44.2">est</w> <w n="44.3">un</w> <w n="44.4">poignard</w> <w n="44.5">armé</w> <w n="44.6">contre</w> <w n="44.7">une</w> <w n="44.8">âme</w> <w n="44.9">oppressée</w>.</l>
						<l n="45" num="2.25"><w n="45.1">Songes</w>-<w n="45.2">y</w> ! <w n="45.3">plus</w> <w n="45.4">d</w>’<w n="45.5">absence</w>, <w n="45.6">et</w> <w n="45.7">personne</w> <w n="45.8">entre</w> <w n="45.9">nous</w>.</l>
						<l n="46" num="2.26"><w n="46.1">Là</w>, <w n="46.2">nos</w> <w n="46.3">trames</w> <w n="46.4">d</w>’<w n="46.5">amour</w> <w n="46.6">n</w>’<w n="46.7">ont</w> <w n="46.8">plus</w> <w n="46.9">de</w> <w n="46.10">nœuds</w> <w n="46.11">jaloux</w> :</l>
						<l n="47" num="2.27"><w n="47.1">Là</w>, <w n="47.2">jamais</w> <w n="47.3">un</w> <w n="47.4">fil</w> <w n="47.5">noir</w> <w n="47.6">ne</w> <w n="47.7">traverse</w> <w n="47.8">la</w> <w n="47.9">joie</w></l>
						<l n="48" num="2.28"><w n="48.1">Des</w> <w n="48.2">fuseaux</w> <w n="48.3">toujours</w> <w n="48.4">pleins</w> <w n="48.5">d</w>’<w n="48.6">or</w> <w n="48.7">et</w> <w n="48.8">de</w> <w n="48.9">pure</w> <w n="48.10">soie</w> !</l>
					</lg>
					<lg n="3">
						<l n="49" num="3.1"><w n="49.1">Avant</w> <w n="49.2">de</w> <w n="49.3">t</w>’<w n="49.4">avoir</w> <w n="49.5">vu</w>, <w n="49.6">devines</w>-<w n="49.7">tu</w> <w n="49.8">comment</w></l>
						<l n="50" num="3.2"><w n="50.1">J</w>’<w n="50.2">entrevoyais</w> <w n="50.3">du</w> <w n="50.4">ciel</w> <w n="50.5">le</w> <w n="50.6">vague</w> <w n="50.7">enchantement</w> ?</l>
						<l n="51" num="3.3"><w n="51.1">Je</w> <w n="51.2">regardais</w> <w n="51.3">toujours</w>, <w n="51.4">comme</w> <w n="51.5">à</w> <w n="51.6">travers</w> <w n="51.7">un</w> <w n="51.8">voile</w></l>
						<l n="52" num="3.4"><w n="52.1">On</w> <w n="52.2">s</w>’<w n="52.3">amuse</w> <w n="52.4">à</w> <w n="52.5">chercher</w> <w n="52.6">la</w> <w n="52.7">forme</w> <w n="52.8">d</w>’<w n="52.9">une</w> <w n="52.10">étoile</w>.</l>
						<l n="53" num="3.5"><w n="53.1">Sous</w> <w n="53.2">l</w>’<w n="53.3">immense</w> <w n="53.4">rideau</w> <w n="53.5">je</w> <w n="53.6">ne</w> <w n="53.7">pouvais</w> <w n="53.8">saisir</w></l>
						<l n="54" num="3.6"><w n="54.1">Que</w> <w n="54.2">des</w> <w n="54.3">objets</w> <w n="54.4">sans</w> <w n="54.5">traits</w> <w n="54.6">pour</w> <w n="54.7">mes</w> <w n="54.8">yeux</w> <w n="54.9">sans</w> <w n="54.10">désir</w>.</l>
						<l n="55" num="3.7"><w n="55.1">Trop</w> <w n="55.2">faible</w> <w n="55.3">à</w> <w n="55.4">m</w>’<w n="55.5">élancer</w> <w n="55.6">au</w> <w n="55.7">delà</w> <w n="55.8">de</w> <w n="55.9">mon</w> <w n="55.10">être</w>,</l>
						<l n="56" num="3.8"><w n="56.1">Je</w> <w n="56.2">rentrais</w> <w n="56.3">dans</w> <w n="56.4">ma</w> <w n="56.5">vie</w>, <w n="56.6">en</w> <w n="56.7">te</w> <w n="56.8">cherchant</w> <w n="56.9">peut</w>-<w n="56.10">être</w> ;</l>
						<l n="57" num="3.9"><w n="57.1">Car</w>, <w n="57.2">toujours</w> <w n="57.3">comme</w> <w n="57.4">toi</w> <w n="57.5">brûlante</w> <w n="57.6">avec</w> <w n="57.7">langueur</w>,</l>
						<l n="58" num="3.10"><w n="58.1">Sans</w> <w n="58.2">t</w>’<w n="58.3">avoir</w> <w n="58.4">vu</w> <w n="58.5">des</w> <w n="58.6">yeux</w>, <w n="58.7">je</w> <w n="58.8">te</w> <w n="58.9">cherchais</w> <w n="58.10">du</w> <w n="58.11">cœur</w> !</l>
					</lg>
					<lg n="4">
						<l n="59" num="4.1"><w n="59.1">Et</w> <w n="59.2">je</w> <w n="59.3">disais</w> <w n="59.4">le</w> <w n="59.5">soir</w> <w n="59.6">aux</w> <w n="59.7">vives</w> <w n="59.8">étincelles</w></l>
						<l n="60" num="4.2"><w n="60.1">Qui</w> <w n="60.2">dans</w> <w n="60.3">l</w>’<w n="60.4">ombre</w> <w n="60.5">éclairaient</w> <w n="60.6">mes</w> <w n="60.7">doutes</w> <w n="60.8">à</w> <w n="60.9">genoux</w> :</l>
						<l n="61" num="4.3">« <w n="61.1">Dieu</w> <w n="61.2">jette</w>-<w n="61.3">t</w>-<w n="61.4">il</w> <w n="61.5">aux</w> <w n="61.6">nuits</w> <w n="61.7">de</w> <w n="61.8">si</w> <w n="61.9">douces</w> <w n="61.10">parcelles</w>,</l>
						<l n="62" num="4.4"><w n="62.1">Pour</w> <w n="62.2">écrire</w> <w n="62.3">son</w> <w n="62.4">nom</w> <w n="62.5">entre</w> <w n="62.6">le</w> <w n="62.7">ciel</w> <w n="62.8">et</w> <w n="62.9">nous</w> ? »</l>
						<l n="63" num="4.5"><w n="63.1">Et</w> <w n="63.2">je</w> <w n="63.3">rêvais</w> <w n="63.4">le</w> <w n="63.5">bruit</w> <w n="63.6">de</w> <w n="63.7">feuilles</w> <w n="63.8">immortelles</w></l>
						<l n="64" num="4.6"><w n="64.1">Qui</w> <w n="64.2">ne</w> <w n="64.3">s</w>’<w n="64.4">envolent</w> <w n="64.5">plus</w> <w n="64.6">sous</w> <w n="64.7">l</w>’<w n="64.8">haleine</w> <w n="64.9">de</w> <w n="64.10">l</w>’<w n="64.11">air</w>,</l>
						<l n="65" num="4.7"><w n="65.1">Sans</w> <w n="65.2">nuit</w>, <w n="65.3">sans</w> <w n="65.4">froid</w>, <w n="65.5">sans</w> <w n="65.6">peur</w> <w n="65.7">d</w>’<w n="65.8">expier</w> <w n="65.9">par</w> <w n="65.10">l</w>’<w n="65.11">hiver</w></l>
						<l n="66" num="4.8"><w n="66.1">De</w> <w n="66.2">longs</w> <w n="66.3">jours</w> <w n="66.4">transparents</w> <w n="66.5">comme</w> <w n="66.6">les</w> <w n="66.7">cœurs</w> <w n="66.8">fidèles</w>.</l>
						<l n="67" num="4.9"><w n="67.1">Et</w> <w n="67.2">puis</w>, <w n="67.3">en</w> <w n="67.4">frissonnant</w>, <w n="67.5">j</w>’<w n="67.6">osais</w> <w n="67.7">rêver</w> <w n="67.8">encor</w></l>
						<l n="68" num="4.10"><w n="68.1">Je</w> <w n="68.2">ne</w> <w n="68.3">sais</w> <w n="68.4">quel</w> <w n="68.5">appui</w> <w n="68.6">qui</w> <w n="68.7">manquait</w> <w n="68.8">à</w> <w n="68.9">mon</w> <w n="68.10">sort</w> !</l>
					</lg>
					<lg n="5">
						<l n="69" num="5.1"><w n="69.1">Là</w>, <w n="69.2">du</w> <w n="69.3">moins</w>, <w n="69.4">je</w> <w n="69.5">voyais</w> <w n="69.6">les</w> <w n="69.7">pauvres</w> <w n="69.8">sans</w> <w n="69.9">alarmes</w>,</l>
						<l n="70" num="5.2"><w n="70.1">Sortis</w> <w n="70.2">de</w> <w n="70.3">leurs</w> <w n="70.4">lambeaux</w> <w n="70.5">que</w> <w n="70.6">Dieu</w> <w n="70.7">n</w>’<w n="70.8">a</w> <w n="70.9">pas</w> <w n="70.10">perdus</w>,</l>
						<l n="71" num="5.3"><w n="71.1">Rassasiés</w> <w n="71.2">d</w>’<w n="71.3">un</w> <w n="71.4">pain</w> <w n="71.5">qui</w> <w n="71.6">ne</w> <w n="71.7">s</w>’<w n="71.8">épuise</w> <w n="71.9">plus</w>,</l>
						<l n="72" num="5.4"><w n="72.1">À</w> <w n="72.2">l</w>’<w n="72.3">immense</w> <w n="72.4">festin</w> <w n="72.5">payé</w> <w n="72.6">de</w> <w n="72.7">tant</w> <w n="72.8">de</w> <w n="72.9">larmes</w> !</l>
					</lg>
					<lg n="6">
						<l n="73" num="6.1"><w n="73.1">Un</w> <w n="73.2">roi</w>, <w n="73.3">de</w> <w n="73.4">l</w>’<w n="73.5">homme</w> <w n="73.6">nu</w> <w n="73.7">devinant</w> <w n="73.8">les</w> <w n="73.9">douleurs</w>,</l>
						<l n="74" num="6.2"><w n="74.1">Sans</w> <w n="74.2">sceptre</w>, <w n="74.3">sans</w> <w n="74.4">couronne</w>, <w n="74.5">à</w> <w n="74.6">la</w> <w n="74.7">pitié</w> <w n="74.8">sensible</w>,</l>
						<l n="75" num="6.3"><w n="75.1">Agenouillé</w> <w n="75.2">devant</w> <w n="75.3">sa</w> <w n="75.4">victime</w> <w n="75.5">paisible</w>,</l>
						<l n="76" num="6.4"><w n="76.1">Pesant</w> <w n="76.2">ses</w> <w n="76.3">fers</w> <w n="76.4">tombés</w> <w n="76.5">et</w> <w n="76.6">les</w> <w n="76.7">mouillant</w> <w n="76.8">de</w> <w n="76.9">pleurs</w> !</l>
					</lg>
					<lg n="7">
						<l n="77" num="7.1"><w n="77.1">Du</w> <w n="77.2">riche</w> <w n="77.3">repentant</w> <w n="77.4">l</w>’<w n="77.5">âme</w> <w n="77.6">enfin</w> <w n="77.7">éclairée</w>,</l>
						<l n="78" num="7.2"><w n="78.1">Versant</w> <w n="78.2">un</w> <w n="78.3">doux</w> <w n="78.4">breuvage</w> <w n="78.5">à</w> <w n="78.6">quelque</w> <w n="78.7">âme</w> <w n="78.8">altérée</w> :</l>
						<l n="79" num="7.3"><w n="79.1">C</w>’<w n="79.2">était</w> <w n="79.3">beau</w> ! <w n="79.4">C</w>’<w n="79.5">était</w> <w n="79.6">tout</w>. <w n="79.7">Quand</w> <w n="79.8">ta</w> <w n="79.9">voix</w> <w n="79.10">me</w> <w n="79.11">parla</w>,</l>
						<l n="80" num="7.4"><w n="80.1">Le</w> <w n="80.2">rideau</w> <w n="80.3">s</w>’<w n="80.4">entr</w>’<w n="80.5">ouvrit</w>, <w n="80.6">l</w>’<w n="80.7">éternité</w> <w n="80.8">brûla</w> !</l>
						<l n="81" num="7.5"><w n="81.1">Le</w> <w n="81.2">ciel</w> <w n="81.3">illuminé</w> <w n="81.4">s</w>’<w n="81.5">emplit</w> <w n="81.6">de</w> <w n="81.7">ta</w> <w n="81.8">présence</w> ;</l>
						<l n="82" num="7.6"><w n="82.1">Dieu</w> <w n="82.2">te</w> <w n="82.3">mit</w> <w n="82.4">devant</w> <w n="82.5">moi</w>, <w n="82.6">je</w> <w n="82.7">compris</w> <w n="82.8">sa</w> <w n="82.9">puissance</w>.</l>
						<l n="83" num="7.7"><w n="83.1">En</w> <w n="83.2">passant</w> <w n="83.3">par</w> <w n="83.4">tes</w> <w n="83.5">yeux</w> <w n="83.6">mon</w> <w n="83.7">âme</w> <w n="83.8">a</w> <w n="83.9">tout</w> <w n="83.10">prévu</w> :</l>
						<l n="84" num="7.8"><w n="84.1">Dieu</w>, <w n="84.2">c</w>’<w n="84.3">est</w> <w n="84.4">toi</w> <w n="84.5">pour</w> <w n="84.6">mon</w> <w n="84.7">cœur</w> ; <w n="84.8">j</w>’<w n="84.9">ai</w> <w n="84.10">vu</w> <w n="84.11">Dieu</w>, <w n="84.12">je</w> <w n="84.13">t</w>’<w n="84.14">ai</w> <w n="84.15">vu</w> !</l>
						<l n="85" num="7.9"><w n="85.1">Mais</w> <w n="85.2">pour</w> <w n="85.3">te</w> <w n="85.4">retrouver</w> <w n="85.5">dans</w> <w n="85.6">cette</w> <w n="85.7">joie</w> <w n="85.8">immense</w>,</l>
						<l n="86" num="7.10"><w n="86.1">Il</w> <w n="86.2">faut</w> <w n="86.3">franchir</w> <w n="86.4">l</w>’<w n="86.5">espace</w>, <w n="86.6">et</w> <w n="86.7">la</w> <w n="86.8">mort</w> <w n="86.9">le</w> <w n="86.10">commence</w>.</l>
						<l n="87" num="7.11"><w n="87.1">Horreur</w> ! <w n="87.2">Il</w> <w n="87.3">faut</w> <w n="87.4">passer</w> <w n="87.5">par</w> <w n="87.6">un</w> <w n="87.7">étroit</w> <w n="87.8">cercueil</w>,</l>
						<l n="88" num="7.12"><w n="88.1">Quitter</w> <w n="88.2">ta</w> <w n="88.3">main</w> <w n="88.4">qui</w> <w n="88.5">brûle</w>, <w n="88.6">et</w> <w n="88.7">ta</w> <w n="88.8">voix</w> <w n="88.9">toujours</w> <w n="88.10">tendre</w>.</l>
						<l n="89" num="7.13"><w n="89.1">Ah</w> ! <w n="89.2">dans</w> <w n="89.3">le</w> <w n="89.4">désespoir</w> <w n="89.5">d</w>’<w n="89.6">être</w> <w n="89.7">un</w> <w n="89.8">jour</w> <w n="89.9">sans</w> <w n="89.10">l</w>’<w n="89.11">entendre</w>,</l>
						<l n="90" num="7.14"><w n="90.1">Tout</w> <w n="90.2">mon</w> <w n="90.3">ciel</w> <w n="90.4">se</w> <w n="90.5">referme</w>… <w n="90.6">En</w> <w n="90.7">tremblant</w>, <w n="90.8">sur</w> <w n="90.9">le</w> <w n="90.10">seuil</w></l>
						<l n="91" num="7.15"><w n="91.1">Où</w> <w n="91.2">la</w> <w n="91.3">cloche</w> <w n="91.4">qui</w> <w n="91.5">pleure</w> <w n="91.6">est</w> <w n="91.7">encore</w> <w n="91.8">entendue</w>,</l>
						<l n="92" num="7.16"><w n="92.1">Pour</w> <w n="92.2">nous</w> <w n="92.3">éteindre</w> <w n="92.4">à</w> <w n="92.5">deux</w> <w n="92.6">je</w> <w n="92.7">suis</w> <w n="92.8">redescendue</w>.</l>
					</lg>
					<lg n="8">
						<l n="93" num="8.1"><w n="93.1">Où</w> <w n="93.2">ces</w> <w n="93.3">signaux</w> <w n="93.4">de</w> <w n="93.5">mort</w> <w n="93.6">envoyés</w> <w n="93.7">devant</w> <w n="93.8">moi</w></l>
						<l n="94" num="8.2"><w n="94.1">S</w>’<w n="94.2">allument</w>, <w n="94.3">et</w> <w n="94.4">longtemps</w> <w n="94.5">tremblent</w> <w n="94.6">comme</w> <w n="94.7">des</w> <w n="94.8">lampes</w></l>
						<l n="95" num="8.3"><w n="95.1">Qu</w>’<w n="95.2">on</w> <w n="95.3">voit</w> <w n="95.4">glisser</w> <w n="95.5">au</w> <w n="95.6">loin</w> <w n="95.7">sur</w> <w n="95.8">les</w> <w n="95.9">gothiques</w> <w n="95.10">rampes</w></l>
						<l n="96" num="8.4"><w n="96.1">D</w>’<w n="96.2">une</w> <w n="96.3">église</w> <w n="96.4">où</w> <w n="96.5">je</w> <w n="96.6">vais</w> <w n="96.7">le</w> <w n="96.8">soir</w> <w n="96.9">prier</w> <w n="96.10">pour</w> <w n="96.11">toi</w>,</l>
						<l n="97" num="8.5"><w n="97.1">Dis</w> ! <w n="97.2">cette</w> <w n="97.3">ombre</w> <w n="97.4">qui</w> <w n="97.5">passe</w> <w n="97.6">auprès</w> <w n="97.7">de</w> <w n="97.8">la</w> <w n="97.9">chapelle</w>,</l>
						<l n="98" num="8.6"><w n="98.1">Est</w>-<w n="98.2">ce</w> <w n="98.3">ton</w> <w n="98.4">âme</w> <w n="98.5">en</w> <w n="98.6">peine</w>, <w n="98.7">en</w> <w n="98.8">quête</w> <w n="98.9">de</w> <w n="98.10">mon</w> <w n="98.11">sort</w>,</l>
						<l n="99" num="8.7"><w n="99.1">Sous</w> <w n="99.2">une</w> <w n="99.3">aile</w> <w n="99.4">traînante</w> <w n="99.5">et</w> <w n="99.6">paresseuse</w> <w n="99.7">encor</w>,</l>
						<l n="100" num="8.8"><w n="100.1">Dont</w> <w n="100.2">le</w> <w n="100.3">doux</w> <w n="100.4">bruit</w> <w n="100.5">de</w> <w n="100.6">plume</w> <w n="100.7">et</w> <w n="100.8">m</w>’<w n="100.9">effleure</w> <w n="100.10">et</w> <w n="100.11">m</w>’<w n="100.12">appelle</w> ?</l>
						<l n="101" num="8.9">« <w n="101.1">Heureux</w> <w n="101.2">qui</w> <w n="101.3">s</w>’<w n="101.4">abandonne</w>, » <w n="101.5">oh</w> ! <w n="101.6">tu</w> <w n="101.7">l</w>’<w n="101.8">as</w> <w n="101.9">dit</w> <w n="101.10">souvent</w>,</l>
						<l n="102" num="8.10">« <w n="102.1">Et</w> <w n="102.2">qui</w> <w n="102.3">s</w>’<w n="102.4">envole</w> <w n="102.5">à</w> <w n="102.6">Dieu</w> <w n="102.7">comme</w> <w n="102.8">la</w> <w n="102.9">plume</w> <w n="102.10">au</w> <w n="102.11">vent</w> ! »</l>
						<l n="103" num="8.11"><w n="103.1">Mais</w>, <w n="103.2">tiens</w> ! <w n="103.3">pour</w> <w n="103.4">remonter</w>, <w n="103.5">intrépide</w> <w n="103.6">hirondelle</w>,</l>
						<l n="104" num="8.12"><w n="104.1">Le</w> <w n="104.2">chemin</w> <w n="104.3">lumineux</w> <w n="104.4">qui</w> <w n="104.5">ramène</w> <w n="104.6">au</w> <w n="104.7">soleil</w>,</l>
						<l n="105" num="8.13"><w n="105.1">Pour</w> <w n="105.2">partir</w> <w n="105.3">en</w> <w n="105.4">aveugle</w>, <w n="105.5">en</w> <w n="105.6">joie</w> ! <w n="105.7">en</w> <w n="105.8">tire</w>-<w n="105.9">d</w>’<w n="105.10">aile</w>,</l>
						<l n="106" num="8.14"><w n="106.1">Et</w> <w n="106.2">ne</w> <w n="106.3">voir</w> <w n="106.4">devant</w> <w n="106.5">soi</w> <w n="106.6">que</w> <w n="106.7">l</w>’<w n="106.8">horizon</w> <w n="106.9">vermeil</w>,</l>
						<l n="107" num="8.15"><w n="107.1">Il</w> <w n="107.2">faut</w> <w n="107.3">mourir</w> <w n="107.4">enfant</w> ! <w n="107.5">Il</w> <w n="107.6">faut</w>, <w n="107.7">doux</w> <w n="107.8">somnambule</w>,</l>
						<l n="108" num="8.16"><w n="108.1">S</w>’<w n="108.2">élançant</w> <w n="108.3">par</w> <w n="108.4">la</w> <w n="108.5">tombe</w> <w n="108.6">aux</w> <w n="108.7">jardins</w> <w n="108.8">sans</w> <w n="108.9">hivers</w>,</l>
						<l n="109" num="8.17"><w n="109.1">Ne</w> <w n="109.2">pas</w> <w n="109.3">se</w> <w n="109.4">réveiller</w> <w n="109.5">à</w> <w n="109.6">la</w> <w n="109.7">voix</w> <w n="109.8">des</w> <w n="109.9">pervers</w>,</l>
						<l n="110" num="8.18"><w n="110.1">Et</w> <w n="110.2">du</w> <w n="110.3">sein</w> <w n="110.4">maternel</w> <w n="110.5">s</w>’<w n="110.6">en</w> <w n="110.7">retourner</w>, <w n="110.8">crédule</w> ;</l>
						<l n="111" num="8.19"><w n="111.1">Comme</w> <w n="111.2">un</w> <w n="111.3">doux</w> <w n="111.4">rossignol</w> <w n="111.5">sort</w> <w n="111.6">du</w> <w n="111.7">fond</w> <w n="111.8">d</w>’<w n="111.9">une</w> <w n="111.10">fleur</w>,</l>
						<l n="112" num="8.20"><w n="112.1">Sans</w> <w n="112.2">avoir</w> <w n="112.3">répandu</w> <w n="112.4">sa</w> <w n="112.5">voix</w> <w n="112.6">sur</w> <w n="112.7">la</w> <w n="112.8">vallée</w>,</l>
						<l n="113" num="8.21"><w n="113.1">Et</w> <w n="113.2">va</w> <w n="113.3">frapper</w> <w n="113.4">aux</w> <w n="113.5">cieux</w> <w n="113.6">pour</w> <w n="113.7">son</w> <w n="113.8">hymne</w> <w n="113.9">exilée</w></l>
						<l n="114" num="8.22"><w n="114.1">Qui</w> <w n="114.2">ne</w> <w n="114.3">veut</w> <w n="114.4">pas</w> <w n="114.5">apprendre</w> <w n="114.6">à</w> <w n="114.7">chanter</w> <w n="114.8">la</w> <w n="114.9">douleur</w>.</l>
						<l n="115" num="8.23"><w n="115.1">Beaux</w> <w n="115.2">enfants</w> ! <w n="115.3">tout</w> <w n="115.4">pétris</w> <w n="115.5">de</w> <w n="115.6">baisers</w>, <w n="115.7">de</w> <w n="115.8">prières</w> !</l>
						<l n="116" num="8.24"><w n="116.1">Faibles</w> <w n="116.2">cygnes</w> <w n="116.3">tombés</w> <w n="116.4">des</w> <w n="116.5">célestes</w> <w n="116.6">bruyères</w>,</l>
						<l n="117" num="8.25"><w n="117.1">Au</w> <w n="117.2">duvet</w> <w n="117.3">encor</w> <w n="117.4">chaud</w> <w n="117.5">de</w> <w n="117.6">la</w> <w n="117.7">main</w> <w n="117.8">du</w> <w n="117.9">Seigneur</w>,</l>
						<l n="118" num="8.26"><w n="118.1">Et</w> <w n="118.2">qui</w> <w n="118.3">ne</w> <w n="118.4">voulez</w> <w n="118.5">pas</w> <w n="118.6">ramper</w> <w n="118.7">vers</w> <w n="118.8">le</w> <w n="118.9">malheur</w>,</l>
						<l n="119" num="8.27"><w n="119.1">Vous</w> <w n="119.2">faites</w> <w n="119.3">bien</w> ! <w n="119.4">Restez</w> <w n="119.5">à</w> <w n="119.6">l</w>’<w n="119.7">alphabet</w> <w n="119.8">d</w>’<w n="119.9">un</w> <w n="119.10">ange</w>,</l>
						<l n="120" num="8.28"><w n="120.1">Dont</w> <w n="120.2">chaque</w> <w n="120.3">lettre</w> <w n="120.4">sainte</w> <w n="120.5">est</w> <w n="120.6">un</w> <w n="120.7">signe</w> <w n="120.8">d</w>’<w n="120.9">amour</w> ;</l>
						<l n="121" num="8.29"><w n="121.1">Solfège</w> <w n="121.2">harmonieux</w> <w n="121.3">où</w> <w n="121.4">nul</w> <w n="121.5">accord</w> <w n="121.6">ne</w> <w n="121.7">change</w>,</l>
						<l n="122" num="8.30"><w n="122.1">Et</w> <w n="122.2">dont</w> <w n="122.3">la</w> <w n="122.4">clé</w> <w n="122.5">sonore</w> <w n="122.6">ouvre</w> <w n="122.7">un</w> <w n="122.8">autre</w> <w n="122.9">séjour</w>.</l>
						<l n="123" num="8.31"><w n="123.1">Mais</w>, <w n="123.2">quand</w> <w n="123.3">Dieu</w> <w n="123.4">nous</w> <w n="123.5">reprend</w> <w n="123.6">vos</w> <w n="123.7">ailes</w> <w n="123.8">et</w> <w n="123.9">vos</w> <w n="123.10">charmes</w>,</l>
						<l n="124" num="8.32"><w n="124.1">Que</w> <w n="124.2">dit</w>-<w n="124.3">il</w> <w n="124.4">de</w> <w n="124.5">les</w> <w n="124.6">voir</w> <w n="124.7">humides</w> <w n="124.8">de</w> <w n="124.9">nos</w> <w n="124.10">larmes</w> ?</l>
					</lg>
					<lg n="9">
						<l n="125" num="9.1"><w n="125.1">Et</w> <w n="125.2">toi</w> ! <w n="125.3">viens</w>-<w n="125.4">tu</w> ? <w n="125.5">Viens</w> <w n="125.6">donc</w> ! <w n="125.7">Car</w> <w n="125.8">au</w> <w n="125.9">bruit</w> <w n="125.10">de</w> <w n="125.11">tes</w> <w n="125.12">pas</w></l>
						<l n="126" num="9.2"><w n="126.1">Ma</w> <w n="126.2">peur</w> <w n="126.3">s</w>’<w n="126.4">envolerait</w> : <w n="126.5">je</w> <w n="126.6">ne</w> <w n="126.7">les</w> <w n="126.8">entends</w> <w n="126.9">pas</w> !</l>
						<l n="127" num="9.3"><w n="127.1">J</w>’<w n="127.2">étends</w> <w n="127.3">mes</w> <w n="127.4">mains</w> <w n="127.5">au</w> <w n="127.6">jour</w>, <w n="127.7">et</w> <w n="127.8">je</w> <w n="127.9">le</w> <w n="127.10">trouve</w> <w n="127.11">sombre</w> ;</l>
						<l n="128" num="9.4"><w n="128.1">Je</w> <w n="128.2">cherche</w> <w n="128.3">à</w> <w n="128.4">m</w>’<w n="128.5">appuyer</w> <w n="128.6">comme</w> <w n="128.7">un</w> <w n="128.8">enfant</w> <w n="128.9">dans</w> <w n="128.10">l</w>’<w n="128.11">ombre</w> ;</l>
						<l n="129" num="9.5"><w n="129.1">Je</w> <w n="129.2">lis</w>, <w n="129.3">ou</w> <w n="129.4">je</w> <w n="129.5">crois</w> <w n="129.6">lire</w> ; <w n="129.7">et</w> <w n="129.8">les</w> <w n="129.9">lugubres</w> <w n="129.10">mots</w>,</l>
						<l n="130" num="9.6"><w n="130.1">En</w> <w n="130.2">oracles</w> <w n="130.3">rangés</w> <w n="130.4">décrivent</w> <w n="130.5">deux</w> <w n="130.6">tombeaux</w></l>
						<l n="131" num="9.7"><w n="131.1">Qui</w>, <w n="131.2">retenant</w> <w n="131.3">sur</w> <w n="131.4">eux</w> <w n="131.5">ma</w> <w n="131.6">frayeur</w> <w n="131.7">arrêtée</w>,</l>
						<l n="132" num="9.8"><w n="132.1">Sortent</w> <w n="132.2">en</w> <w n="132.3">traits</w> <w n="132.4">de</w> <w n="132.5">plomb</w> <w n="132.6">de</w> <w n="132.7">la</w> <w n="132.8">page</w> <w n="132.9">irritée</w> :</l>
						<l n="133" num="9.9"><w n="133.1">Il</w> <w n="133.2">faut</w> <w n="133.3">fermer</w> <w n="133.4">le</w> <w n="133.5">livre</w> <w n="133.6">et</w> <w n="133.7">tomber</w> <w n="133.8">à</w> <w n="133.9">genoux</w> ;</l>
						<l n="134" num="9.10"><w n="134.1">Il</w> <w n="134.2">faut</w> <w n="134.3">dire</w> : « <w n="134.4">Mon</w> <w n="134.5">Dieu</w> ! <w n="134.6">priez</w> <w n="134.7">pour</w> <w n="134.8">lui</w>… <w n="134.9">pour</w> <w n="134.10">nous</w> ! »</l>
					</lg>
					<lg n="10">
						<l n="135" num="10.1"><w n="135.1">Et</w> <w n="135.2">me</w> <w n="135.3">voilà</w> ! <w n="135.4">voilà</w> <w n="135.5">comme</w> <w n="135.6">tu</w> <w n="135.7">m</w>’<w n="135.8">as</w> <w n="135.9">rendue</w> :</l>
						<l n="136" num="10.2"><w n="136.1">À</w> <w n="136.2">deux</w> <w n="136.3">pas</w> <w n="136.4">de</w> <w n="136.5">tes</w> <w n="136.6">pas</w>, <w n="136.7">je</w> <w n="136.8">suis</w>, <w n="136.9">seule</w>, <w n="136.10">perdue</w> ;</l>
						<l n="137" num="10.3"><w n="137.1">Je</w> <w n="137.2">dépends</w> <w n="137.3">d</w>’<w n="137.4">un</w> <w n="137.5">nuage</w> <w n="137.6">ou</w> <w n="137.7">du</w> <w n="137.8">vol</w> <w n="137.9">d</w>’<w n="137.10">un</w> <w n="137.11">oiseau</w>,</l>
						<l n="138" num="10.4"><w n="138.1">Et</w> <w n="138.2">j</w>’<w n="138.3">ai</w> <w n="138.4">semé</w> <w n="138.5">ma</w> <w n="138.6">joie</w> <w n="138.7">au</w> <w n="138.8">sommet</w> <w n="138.9">d</w>’<w n="138.10">un</w> <w n="138.11">roseau</w> !</l>
					</lg>
				</div></body></text></TEI>