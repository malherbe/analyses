<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES247">
					<head type="main">AGAR</head>
					<head type="sub">FRAGMENT</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									— Elle jeta de grands cris et se mit à pleurer. <lb></lb>
									— Or, Dieu écouta la voix de l’enfant ; et <lb></lb>un ange de Dieu appela Agar du ciel, et lui <lb></lb>dit : « Agar, qu’avez-vous ? Ne craignez point, <lb></lb>car Dieu a écouté la voix de l’enfant du lieu où <lb></lb>il est. »
								</quote>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quelle</w> <w n="1.2">mère</w> <w n="1.3">un</w> <w n="1.4">moment</w> <w n="1.5">ne</w> <w n="1.6">fut</w> <w n="1.7">ambitieuse</w> ?</l>
						<l n="2" num="1.2"><w n="2.1">Quelle</w> <w n="2.2">mère</w>, <w n="2.3">en</w> <w n="2.4">plongeant</w> <w n="2.5">son</w> <w n="2.6">âme</w> <w n="2.7">curieuse</w></l>
						<l n="3" num="1.3"><w n="3.1">Dans</w> <w n="3.2">les</w> <w n="3.3">jours</w> <w n="3.4">où</w> <w n="3.5">son</w> <w n="3.6">fils</w> <w n="3.7">ira</w> <w n="3.8">chercher</w> <w n="3.9">ses</w> <w n="3.10">droits</w>,</l>
						<l n="4" num="1.4"><w n="4.1">N</w>’<w n="4.2">a</w> <w n="4.3">dit</w> : « <w n="4.4">Voilà</w> <w n="4.5">mon</w> <w n="4.6">fils</w> ! <w n="4.7">Que</w> <w n="4.8">sont</w> <w n="4.9">les</w> <w n="4.10">fils</w> <w n="4.11">des</w> <w n="4.12">rois</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">Vents</w> ! <w n="5.2">portez</w> <w n="5.3">dans</w> <w n="5.4">les</w> <w n="5.5">cieux</w> <w n="5.6">la</w> <w n="5.7">voix</w> <w n="5.8">de</w> <w n="5.9">ma</w> <w n="5.10">prière</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Dieu</w> ! <w n="6.2">versez</w> <w n="6.3">le</w> <w n="6.4">pardon</w> <w n="6.5">sur</w> <w n="6.6">l</w>’<w n="6.7">orgueil</w> <w n="6.8">à</w> <w n="6.9">genoux</w>.</l>
						<l n="7" num="2.3"><w n="7.1">Oui</w>, <w n="7.2">l</w>’<w n="7.3">orgueil</w> <w n="7.4">m</w>’<w n="7.5">a</w> <w n="7.6">saisie</w>, <w n="7.7">ô</w> <w n="7.8">mon</w> <w n="7.9">Dieu</w> ! <w n="7.10">j</w>’<w n="7.11">étais</w> <w n="7.12">mère</w> ;</l>
						<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">la</w> <w n="8.3">mère</w> <w n="8.4">et</w> <w n="8.5">l</w>’<w n="8.6">enfant</w> <w n="8.7">tendent</w> <w n="8.8">les</w> <w n="8.9">bras</w> <w n="8.10">vers</w> <w n="8.11">vous</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">Enfant</w>, <w n="9.2">ne</w> <w n="9.3">pleure</w> <w n="9.4">pas</w>. <w n="9.5">Voici</w> <w n="9.6">des</w> <w n="9.7">fleurs</w>. <w n="9.8">Je</w> <w n="9.9">t</w>’<w n="9.10">aime</w>.</l>
						<l n="10" num="3.2"><w n="10.1">Nous</w> <w n="10.2">trouverons</w> <w n="10.3">là</w>-<w n="10.4">bas</w>, <w n="10.5">peut</w>-<w n="10.6">être</w>, <w n="10.7">un</w> <w n="10.8">frais</w> <w n="10.9">ruisseau</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Tu</w> <w n="11.2">dormiras</w> <w n="11.3">content</w> <w n="11.4">sous</w> <w n="11.5">un</w> <w n="11.6">jeune</w> <w n="11.7">arbrisseau</w> ;</l>
						<l n="12" num="3.4"><w n="12.1">Et</w> <w n="12.2">peut</w>-<w n="12.3">être</w> <w n="12.4">avec</w> <w n="12.5">toi</w> <w n="12.6">j</w>’<w n="12.7">y</w> <w n="12.8">dormirai</w> <w n="12.9">moi</w>-<w n="12.10">même</w> ! »</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ainsi</w> <w n="13.2">la</w> <w n="13.3">triste</w> <w n="13.4">Agar</w>, <w n="13.5">un</w> <w n="13.6">enfant</w> <w n="13.7">par</w> <w n="13.8">la</w> <w n="13.9">main</w>,</l>
						<l n="14" num="4.2"><w n="14.1">De</w> <w n="14.2">son</w> <w n="14.3">cœur</w> <w n="14.4">oppressé</w> <w n="14.5">brise</w> <w n="14.6">le</w> <w n="14.7">long</w> <w n="14.8">silence</w>.</l>
						<l n="15" num="4.3"><w n="15.1">L</w>’<w n="15.2">enfant</w> <w n="15.3">rit</w> <w n="15.4">à</w> <w n="15.5">sa</w> <w n="15.6">mère</w> ; <w n="15.7">et</w>, <w n="15.8">plein</w> <w n="15.9">d</w>’<w n="15.10">obéissance</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Cueille</w> <w n="16.2">une</w> <w n="16.3">fleur</w> <w n="16.4">mourante</w> <w n="16.5">et</w> <w n="16.6">poursuit</w> <w n="16.7">son</w> <w n="16.8">chemin</w>.</l>
						<l n="17" num="4.5"><w n="17.1">Ce</w> <w n="17.2">chemin</w> <w n="17.3">est</w> <w n="17.4">brûlant</w> ; <w n="17.5">le</w> <w n="17.6">soleil</w> <w n="17.7">le</w> <w n="17.8">dévore</w> :</l>
						<l n="18" num="4.6"><w n="18.1">L</w>’<w n="18.2">enfant</w> <w n="18.3">poursuit</w> <w n="18.4">en</w> <w n="18.5">vain</w>, <w n="18.6">de</w> <w n="18.7">chaleur</w> <w n="18.8">obsédé</w>,</l>
						<l n="19" num="4.7"><w n="19.1">L</w>’<w n="19.2">arbre</w> <w n="19.3">vert</w>, <w n="19.4">l</w>’<w n="19.5">ombre</w> <w n="19.6">et</w> <w n="19.7">l</w>’<w n="19.8">eau</w> ! <w n="19.9">Triste</w>, <w n="19.10">il</w> <w n="19.11">a</w> <w n="19.12">demandé</w> :</l>
						<l n="20" num="4.8">« <w n="20.1">Ce</w> <w n="20.2">frais</w> <w n="20.3">ruisseau</w>, <w n="20.4">ma</w> <w n="20.5">mère</w>, <w n="20.6">est</w>-<w n="20.7">il</w> <w n="20.8">bien</w> <w n="20.9">loin</w> <w n="20.10">encore</w> ? »</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1">— « <w n="21.1">Là</w>-<w n="21.2">bas</w> ! » <w n="21.3">répond</w> <w n="21.4">Agar</w>. — « <w n="21.5">Oh</w> ! <w n="21.6">que</w> <w n="21.7">c</w>’<w n="21.8">est</w> <w n="21.9">loin</w>, <w n="21.10">là</w>-<w n="21.11">bas</w>,</l>
						<l n="22" num="5.2"><w n="22.1">Ma</w> <w n="22.2">mère</w> ! » <w n="22.3">Elle</w> <w n="22.4">se</w> <w n="22.5">tait</w>, <w n="22.6">détourne</w> <w n="22.7">son</w> <w n="22.8">visage</w> ;</l>
						<l n="23" num="5.3"><w n="23.1">Du</w> <w n="23.2">voile</w> <w n="23.3">qui</w> <w n="23.4">la</w> <w n="23.5">couvre</w> <w n="23.6">elle</w> <w n="23.7">forme</w> <w n="23.8">un</w> <w n="23.9">nuage</w>,</l>
						<l n="24" num="5.4"><w n="24.1">Comme</w> <w n="24.2">un</w> <w n="24.3">linceul</w> <w n="24.4">mouvant</w> <w n="24.5">où</w> <w n="24.6">se</w> <w n="24.7">traînent</w> <w n="24.8">leurs</w> <w n="24.9">pas</w>.</l>
					</lg>
					<lg n="6">
						<l n="25" num="6.1"><w n="25.1">Ses</w> <w n="25.2">premiers</w> <w n="25.3">pas</w>, <w n="25.4">à</w> <w n="25.5">lui</w>, <w n="25.6">l</w>’<w n="25.7">éloignent</w> <w n="25.8">de</w> <w n="25.9">son</w> <w n="25.10">père</w> !</l>
						<l n="26" num="6.2">« <w n="26.1">Ô</w> <w n="26.2">Sarah</w> ! <w n="26.3">de</w> <w n="26.4">ton</w> <w n="26.5">fils</w> <w n="26.6">le</w> <w n="26.7">sort</w> <w n="26.8">est</w> <w n="26.9">plus</w> <w n="26.10">prospère</w>.</l>
						<l n="27" num="6.3"><w n="27.1">Ô</w> <w n="27.2">Sarah</w> ! <w n="27.3">cet</w> <w n="27.4">enfant</w> <w n="27.5">pâle</w>, <w n="27.6">nu</w>, <w n="27.7">sans</w> <w n="27.8">soutien</w>,</l>
						<l n="28" num="6.4"><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">le</w> <w n="28.4">fils</w> <w n="28.5">d</w>’<w n="28.6">Abraham</w>… <w n="28.7">Non</w>, <w n="28.8">mon</w> <w n="28.9">Dieu</w> ! <w n="28.10">c</w>’<w n="28.11">est</w> <w n="28.12">le</w> <w n="28.13">tien</w> !</l>
						<l n="29" num="6.5"><w n="29.1">Sauve</w>-<w n="29.2">le</w> ! <w n="29.3">sauve</w>-<w n="29.4">nous</w>. <w n="29.5">Un</w> <w n="29.6">peu</w> <w n="29.7">d</w>’<w n="29.8">air</w> ! <w n="29.9">un</w> <w n="29.10">peu</w> <w n="29.11">d</w>’<w n="29.12">ombre</w> !</l>
						<l n="30" num="6.6"><space quantity="6" unit="char"></space><w n="30.1">Dieu</w> ! <w n="30.2">ta</w> <w n="30.3">main</w> <w n="30.4">devant</w> <w n="30.5">le</w> <w n="30.6">soleil</w> !</l>
						<l n="31" num="6.7"><w n="31.1">Le</w> <w n="31.2">bruit</w> <w n="31.3">frais</w> <w n="31.4">de</w> <w n="31.5">l</w>’<w n="31.6">eau</w> <w n="31.7">vive</w>, <w n="31.8">un</w> <w n="31.9">arbre</w> <w n="31.10">au</w> <w n="31.11">rideau</w> <w n="31.12">sombre</w>,</l>
						<l n="32" num="6.8"><w n="32.1">Une</w> <w n="32.2">pierre</w> <w n="32.3">mouillée</w>, <w n="32.4">un</w> <w n="32.5">fruit</w>, <w n="32.6">et</w> <w n="32.7">du</w> <w n="32.8">sommeil</w> ! »</l>
					</lg>
					<lg n="7">
						<l n="33" num="7.1"><w n="33.1">Et</w> <w n="33.2">l</w>’<w n="33.3">enfant</w> <w n="33.4">tout</w> <w n="33.5">à</w> <w n="33.6">coup</w> <w n="33.7">s</w>’<w n="33.8">arrête</w>. <w n="33.9">Elle</w> <w n="33.10">s</w>’<w n="33.11">arrête</w>.</l>
						<l n="34" num="7.2"><w n="34.1">Du</w> <w n="34.2">voile</w> <w n="34.3">qui</w> <w n="34.4">l</w>’<w n="34.5">étouffe</w> <w n="34.6">il</w> <w n="34.7">dégage</w> <w n="34.8">sa</w> <w n="34.9">tête</w> ;</l>
						<l rhyme="none" n="35" num="7.3"><w n="35.1">De</w> <w n="35.2">ses</w> <w n="35.3">cheveux</w> <w n="35.4">touffus</w> <w n="35.5">lent</w> <w n="35.6">à</w> <w n="35.7">se</w> <w n="35.8">découvrir</w>,</l>
						<l n="36" num="7.4"><w n="36.1">Il</w> <w n="36.2">tremble</w>. <w n="36.3">Il</w> <w n="36.4">jette</w> <w n="36.5">enfin</w> <w n="36.6">d</w>’<w n="36.7">une</w> <w n="36.8">lèvre</w> <w n="36.9">altérée</w> :</l>
						<l n="37" num="7.5">« <w n="37.1">J</w>’<w n="37.2">ai</w> <w n="37.3">soif</w> ! » — <w n="37.4">Et</w> <w n="37.5">dans</w> <w n="37.6">le</w> <w n="37.7">ciel</w> <w n="37.8">l</w>’<w n="37.9">espérance</w> <w n="37.10">est</w> <w n="37.11">rentrée</w>…</l>
					</lg>
						<ab type="dot">..........................................................................................................</ab>
						<ab type="dot">..........................................................................................................</ab>
				</div></body></text></TEI>