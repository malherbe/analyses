<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES252">
					<head type="main">L’IMPOSSIBLE</head>
					<opener>
						<epigraph>
							<cit>
								<quote>
									On ne jette point l’ancre dans le fleuve de <lb></lb>la vie. Il emporte également celui qui lutte <lb></lb>contre son cours et celui qui s’y abandonne.
								</quote>
								 <bibl><hi rend="smallcap">Bernardin de Saint-Pierre.</hi></bibl>
							</cit>
						</epigraph>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Qui</w> <w n="1.2">me</w> <w n="1.3">rendra</w> <w n="1.4">ce</w> <w n="1.5">jour</w> <w n="1.6">où</w> <w n="1.7">la</w> <w n="1.8">vie</w> <w n="1.9">a</w> <w n="1.10">des</w> <w n="1.11">ailes</w></l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">vole</w>, <w n="2.3">vole</w> <w n="2.4">ainsi</w> <w n="2.5">que</w> <w n="2.6">l</w>’<w n="2.7">alouette</w> <w n="2.8">aux</w> <w n="2.9">cieux</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Lorsque</w> <w n="3.2">tant</w> <w n="3.3">de</w> <w n="3.4">clarté</w> <w n="3.5">passe</w> <w n="3.6">devant</w> <w n="3.7">ses</w> <w n="3.8">yeux</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Qu</w>’<w n="4.2">elle</w> <w n="4.3">tombe</w> <w n="4.4">éblouie</w> <w n="4.5">au</w> <w n="4.6">fond</w> <w n="4.7">des</w> <w n="4.8">fleurs</w>, <w n="4.9">de</w> <w n="4.10">celles</w></l>
						<l n="5" num="1.5"><w n="5.1">Qui</w> <w n="5.2">parfument</w> <w n="5.3">son</w> <w n="5.4">nid</w>, <w n="5.5">son</w> <w n="5.6">âme</w>, <w n="5.7">son</w> <w n="5.8">sommeil</w>,</l>
						<l n="6" num="1.6"><w n="6.1">Et</w> <w n="6.2">lustrent</w> <w n="6.3">son</w> <w n="6.4">plumage</w> <w n="6.5">ardé</w> <w n="6.6">par</w> <w n="6.7">le</w> <w n="6.8">soleil</w> !</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Ciel</w> ! <w n="7.2">un</w> <w n="7.3">de</w> <w n="7.4">ces</w> <w n="7.5">fils</w> <w n="7.6">d</w>’<w n="7.7">or</w> <w n="7.8">pour</w> <w n="7.9">ourdir</w> <w n="7.10">ma</w> <w n="7.11">journée</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Un</w> <w n="8.2">débris</w> <w n="8.3">de</w> <w n="8.4">ce</w> <w n="8.5">prisme</w> <w n="8.6">aux</w> <w n="8.7">brillantes</w> <w n="8.8">couleurs</w> !</l>
						<l n="9" num="2.3"><w n="9.1">Au</w> <w n="9.2">fond</w> <w n="9.3">de</w> <w n="9.4">ces</w> <w n="9.5">beaux</w> <w n="9.6">jours</w> <w n="9.7">et</w> <w n="9.8">de</w> <w n="9.9">ces</w> <w n="9.10">belles</w> <w n="9.11">fleurs</w>,</l>
						<l n="10" num="2.4"><w n="10.1">Un</w> <w n="10.2">rêve</w> ! <w n="10.3">où</w> <w n="10.4">je</w> <w n="10.5">sois</w> <w n="10.6">libre</w>, <w n="10.7">enfant</w>, <w n="10.8">à</w> <w n="10.9">peine</w> <w n="10.10">née</w>,</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">Quand</w> <w n="11.2">l</w>’<w n="11.3">amour</w> <w n="11.4">de</w> <w n="11.5">ma</w> <w n="11.6">mère</w> <w n="11.7">était</w> <w n="11.8">mon</w> <w n="11.9">avenir</w>,</l>
						<l n="12" num="3.2"><w n="12.1">Quand</w> <w n="12.2">on</w> <w n="12.3">ne</w> <w n="12.4">mourait</w> <w n="12.5">pas</w> <w n="12.6">encor</w> <w n="12.7">dans</w> <w n="12.8">ma</w> <w n="12.9">famille</w>,</l>
						<l n="13" num="3.3"><w n="13.1">Quand</w> <w n="13.2">tout</w> <w n="13.3">vivait</w> <w n="13.4">pour</w> <w n="13.5">moi</w>, <w n="13.6">vaine</w> <w n="13.7">petite</w> <w n="13.8">fille</w> !</l>
						<l n="14" num="3.4"><w n="14.1">Quand</w> <w n="14.2">vivre</w> <w n="14.3">était</w> <w n="14.4">le</w> <w n="14.5">ciel</w>, <w n="14.6">ou</w> <w n="14.7">s</w>’<w n="14.8">en</w> <w n="14.9">ressouvenir</w> ;</l>
						<l n="15" num="3.5"><w n="15.1">Quand</w> <w n="15.2">j</w>’<w n="15.3">aimais</w> <w n="15.4">sans</w> <w n="15.5">savoir</w> <w n="15.6">ce</w> <w n="15.7">que</w> <w n="15.8">j</w>’<w n="15.9">aimais</w>, <w n="15.10">quand</w> <w n="15.11">l</w>’<w n="15.12">âme</w></l>
						<l n="16" num="3.6"><w n="16.1">Me</w> <w n="16.2">palpitait</w> <w n="16.3">heureuse</w>, <w n="16.4">et</w> <w n="16.5">de</w> <w n="16.6">quoi</w> ? <w n="16.7">Je</w> <w n="16.8">ne</w> <w n="16.9">sais</w> ;</l>
						<l n="17" num="3.7"><w n="17.1">Quand</w> <w n="17.2">toute</w> <w n="17.3">la</w> <w n="17.4">nature</w> <w n="17.5">était</w> <w n="17.6">parfum</w> <w n="17.7">et</w> <w n="17.8">flamme</w>,</l>
						<l n="18" num="3.8"><w n="18.1">Quand</w> <w n="18.2">mes</w> <w n="18.3">deux</w> <w n="18.4">bras</w> <w n="18.5">s</w>’<w n="18.6">ouvraient</w> <w n="18.7">devant</w> <w n="18.8">ces</w> <w n="18.9">jours</w>… <w n="18.10">passés</w>.</l>
					</lg>
				</div></body></text></TEI>