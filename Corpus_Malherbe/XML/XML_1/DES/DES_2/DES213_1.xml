<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">LES PLEURS</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>2802 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_2</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>2802 vers9617 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>L’Aurore en fuite (poèmes choisis)</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition établie par Christine Planté</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions Points</publisher>
						<date when="2010">2010</date>
					</imprint>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<p>Ajout de l’attribut rhyme="none" dans le poème MINUIT (lacune confirmée par l’éditon Bertrand 1973).</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème AGAR.</p>
				<p>Ajout de l’attribut rhyme="none" pour un vers dans le poème LE PETIT RIEUR.</p>
				<p>Ajout d’un vers dans le poème LOUISE LABÉ (voir l’édition  Bertrand 1973).</p>
				<p>Ajout d’un vers dans le poème Révélation (voir l’édition de 2010).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
				<change when="2018-09-03" who="RR">Ajout de l’attribut rhyme="none" pour les vers sans rime.</change>
				<change when="2018-09-03" who="RR">Ajout des vers manquants.</change>
				<change when="2018-11-02" who="RR">Ajout des vers manquants.</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES213">
					<head type="main">TOI ! ME HAIS-TU ?</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">tu</w> <w n="1.3">souris</w> <w n="1.4">en</w> <w n="1.5">homme</w> <w n="1.6">à</w> <w n="1.7">ces</w> <w n="1.8">tendres</w> <w n="1.9">orages</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">troublent</w> <w n="2.3">dans</w> <w n="2.4">l</w>’<w n="2.5">amour</w> <w n="2.6">de</w> <w n="2.7">plus</w> <w n="2.8">faibles</w> <w n="2.9">courages</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Que</w> <w n="3.2">j</w>’<w n="3.3">aime</w>, <w n="3.4">de</w> <w n="3.5">ta</w> <w n="3.6">voix</w> <w n="3.7">démentant</w> <w n="3.8">la</w> <w n="3.9">gaîté</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Ce</w> <w n="4.2">nuage</w> <w n="4.3">qui</w> <w n="4.4">passe</w> <w n="4.5">à</w> <w n="4.6">ton</w> <w n="4.7">front</w> <w n="4.8">attristé</w> !</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Après</w> <w n="5.2">que</w> <w n="5.3">je</w> <w n="5.4">t</w>’<w n="5.5">ai</w> <w n="5.6">dit</w> <w n="5.7">ma</w> <w n="5.8">plainte</w> <w n="5.9">toute</w> <w n="5.10">entière</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Calmée</w> <w n="6.2">à</w> <w n="6.3">ton</w> <w n="6.4">silence</w> <w n="6.5">éloquent</w> <w n="6.6">et</w> <w n="6.7">rêveur</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Quand</w> <w n="7.2">je</w> <w n="7.3">sens</w> <w n="7.4">tes</w> <w n="7.5">doux</w> <w n="7.6">yeux</w> <w n="7.7">brûler</w> <w n="7.8">sur</w> <w n="7.9">ma</w> <w n="7.10">paupière</w>,</l>
						<l n="8" num="2.4"><w n="8.1">Dis</w> ! <w n="8.2">n</w>’<w n="8.3">est</w>-<w n="8.4">ce</w> <w n="8.5">pas</w> <w n="8.6">ton</w> <w n="8.7">cœur</w> <w n="8.8">qui</w> <w n="8.9">regarde</w> <w n="8.10">mon</w> <w n="8.11">cœur</w> ?</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">m</w>’<w n="9.3">éblouit</w> <w n="9.4">de</w> <w n="9.5">joie</w> ! <w n="9.6">il</w> <w n="9.7">endort</w> <w n="9.8">mes</w> <w n="9.9">alarmes</w>.</l>
						<l n="10" num="3.2"><w n="10.1">Sais</w>-<w n="10.2">tu</w> <w n="10.3">de</w> <w n="10.4">quel</w> <w n="10.5">espoir</w> <w n="10.6">il</w> <w n="10.7">relève</w> <w n="10.8">mon</w> <w n="10.9">sort</w> ?</l>
						<l n="11" num="3.3"><w n="11.1">J</w>’<w n="11.2">y</w> <w n="11.3">vois</w> <w n="11.4">toute</w> <w n="11.5">une</w> <w n="11.6">vie</w>, <w n="11.7">et</w> <w n="11.8">je</w> <w n="11.9">la</w> <w n="11.10">vois</w> <w n="11.11">sans</w> <w n="11.12">larmes</w>,</l>
						<l n="12" num="3.4"><space quantity="6" unit="char"></space><w n="12.1">Et</w> <w n="12.2">je</w> <w n="12.3">n</w>’<w n="12.4">ai</w> <w n="12.5">plus</w> <w n="12.6">peur</w> <w n="12.7">de</w> <w n="12.8">la</w> <w n="12.9">mort</w> !</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Toi</w> <w n="13.2">qui</w> <w n="13.3">m</w>’<w n="13.4">as</w> <w n="13.5">seule</w> <w n="13.6">aimée</w>, <w n="13.7">écoute</w> : <w n="13.8">si</w> <w n="13.9">tu</w> <w n="13.10">changes</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Je</w> <w n="14.2">te</w> <w n="14.3">pardonnerai</w> <w n="14.4">sans</w> <w n="14.5">t</w>’<w n="14.6">imiter</w> <w n="14.7">jamais</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Car</w> <w n="15.2">de</w> <w n="15.3">cet</w> <w n="15.4">amour</w> <w n="15.5">vrai</w> <w n="15.6">dont</w> <w n="15.7">s</w>’<w n="15.8">adorent</w> <w n="15.9">les</w> <w n="15.10">anges</w></l>
						<l n="16" num="4.4"><space quantity="10" unit="char"></space><w n="16.1">Je</w> <w n="16.2">sens</w> <w n="16.3">que</w> <w n="16.4">je</w> <w n="16.5">t</w>’<w n="16.6">aimais</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Et</w> <w n="17.2">sans</w> <w n="17.3">ton</w> <w n="17.4">cœur</w>, <w n="17.5">mon</w> <w n="17.6">cœur</w> <w n="17.7">comme</w> <w n="17.8">un</w> <w n="17.9">poids</w> <w n="17.10">inutile</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Tel</w> <w n="18.2">qu</w>’<w n="18.3">en</w> <w n="18.4">ce</w> <w n="18.5">froid</w> <w n="18.6">cadran</w> <w n="18.7">palpite</w> <w n="18.8">un</w> <w n="18.9">plomb</w> <w n="18.10">mobile</w>,</l>
						<l n="19" num="5.3"><w n="19.1">De</w> <w n="19.2">la</w> <w n="19.3">nuit</w> <w n="19.4">à</w> <w n="19.5">l</w>’<w n="19.6">aurore</w> <w n="19.7">et</w> <w n="19.8">de</w> <w n="19.9">l</w>’<w n="19.10">aurore</w> <w n="19.11">au</w> <w n="19.12">soir</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Battra</w> <w n="20.2">jusqu</w>’<w n="20.3">au</w> <w n="20.4">tombeau</w>, <w n="20.5">sans</w> <w n="20.6">joie</w> <w n="20.7">et</w> <w n="20.8">sans</w> <w n="20.9">espoir</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w>, <w n="21.2">j</w>’<w n="21.3">en</w> <w n="21.4">demande</w> <w n="21.5">à</w> <w n="21.6">Dieu</w> <w n="21.7">pardon</w> <w n="21.8">plus</w> <w n="21.9">qu</w>’<w n="21.10">à</w> <w n="21.11">toi</w>-<w n="21.12">même</w>,</l>
						<l n="22" num="6.2"><w n="22.1">Je</w> <w n="22.2">ne</w> <w n="22.3">veux</w> <w n="22.4">pas</w> <w n="22.5">revivre</w> <w n="22.6">où</w> <w n="22.7">l</w>’<w n="22.8">on</w> <w n="22.9">dit</w> <w n="22.10">que</w> <w n="22.11">l</w>’<w n="22.12">on</w> <w n="22.13">aime</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Si</w> <w n="23.2">l</w>’<w n="23.3">on</w> <w n="23.4">t</w>’<w n="23.5">y</w> <w n="23.6">donne</w> <w n="23.7">un</w> <w n="23.8">bien</w> <w n="23.9">qui</w> <w n="23.10">ne</w> <w n="23.11">sera</w> <w n="23.12">plus</w> <w n="23.13">moi</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">si</w> <w n="24.3">Dieu</w> <w n="24.4">m</w>’<w n="24.5">y</w> <w n="24.6">destine</w> <w n="24.7">un</w> <w n="24.8">autre</w> <w n="24.9">ange</w> <w n="24.10">que</w> <w n="24.11">toi</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Le</w> <w n="25.2">néant</w> <w n="25.3">me</w> <w n="25.4">plaît</w> <w n="25.5">mieux</w> ; <w n="25.6">son</w> <w n="25.7">horreur</w> <w n="25.8">me</w> <w n="25.9">soulage</w> :</l>
						<l n="26" num="7.2"><w n="26.1">Jamais</w> <w n="26.2">je</w> <w n="26.3">ne</w> <w n="26.4">t</w>’<w n="26.5">ai</w> <w n="26.6">vu</w> <w n="26.7">sans</w> <w n="26.8">t</w>’<w n="26.9">aimer</w> <w n="26.10">davantage</w> ;</l>
						<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">jamais</w>, <w n="27.3">plus</w> <w n="27.4">rêveuse</w> <w n="27.5">en</w> <w n="27.6">te</w> <w n="27.7">quittant</w> <w n="27.8">le</w> <w n="27.9">soir</w>,</l>
						<l n="28" num="7.4"><w n="28.1">Sans</w> <w n="28.2">pâlir</w> <w n="28.3">dans</w> <w n="28.4">l</w>’<w n="28.5">effroi</w> <w n="28.6">de</w> <w n="28.7">ne</w> <w n="28.8">te</w> <w n="28.9">plus</w> <w n="28.10">revoir</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">C</w>’<w n="29.2">est</w> <w n="29.3">que</w> <w n="29.4">Dieu</w> <w n="29.5">pour</w> <w n="29.6">nos</w> <w n="29.7">jours</w> <w n="29.8">n</w>’<w n="29.9">alluma</w> <w n="29.10">point</w> <w n="29.11">deux</w> <w n="29.12">flammes</w> ;</l>
						<l n="30" num="8.2"><w n="30.1">C</w>’<w n="30.2">est</w> <w n="30.3">qu</w>’<w n="30.4">un</w> <w n="30.5">même</w> <w n="30.6">baiser</w> <w n="30.7">fit</w> <w n="30.8">éclore</w> <w n="30.9">deux</w> <w n="30.10">âmes</w> ;</l>
						<l n="31" num="8.3"><w n="31.1">Que</w> <w n="31.2">partout</w> <w n="31.3">où</w> <w n="31.4">je</w> <w n="31.5">passe</w> <w n="31.6">en</w> <w n="31.7">appelant</w> <w n="31.8">ta</w> <w n="31.9">main</w>,</l>
						<l n="32" num="8.4"><w n="32.1">Le</w> <w n="32.2">doux</w> <w n="32.3">poids</w> <w n="32.4">de</w> <w n="32.5">tes</w> <w n="32.6">pieds</w> <w n="32.7">a</w> <w n="32.8">creusé</w> <w n="32.9">mon</w> <w n="32.10">chemin</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Enfin</w>, <w n="33.2">que</w> <w n="33.3">ma</w> <w n="33.4">pensée</w>, <w n="33.5">orageuse</w> <w n="33.6">ou</w> <w n="33.7">calmée</w>,</l>
						<l n="34" num="9.2"><w n="34.1">Se</w> <w n="34.2">dévoile</w> <w n="34.3">riante</w>, <w n="34.4">ou</w> <w n="34.5">s</w>’<w n="34.6">enferme</w> <w n="34.7">alarmée</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Comme</w> <w n="35.2">on</w> <w n="35.3">voit</w> <w n="35.4">la</w> <w n="35.5">cigale</w> <w n="35.6">au</w> <w n="35.7">front</w> <w n="35.8">tremblant</w> <w n="35.9">des</w> <w n="35.10">blés</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Craintive</w>, <w n="36.2">au</w> <w n="36.3">moindre</w> <w n="36.4">bruit</w> <w n="36.5">tarir</w> <w n="36.6">ses</w> <w n="36.7">chants</w> <w n="36.8">troublés</w> ;</l>
						<l n="37" num="9.5"><space quantity="6" unit="char"></space><w n="37.1">Toujours</w> <w n="37.2">teinte</w> <w n="37.3">de</w> <w n="37.4">ton</w> <w n="37.5">image</w>,</l>
						<l n="38" num="9.6"><w n="38.1">C</w>’<w n="38.2">est</w> <w n="38.3">l</w>’<w n="38.4">eau</w> <w n="38.5">sous</w> <w n="38.6">le</w> <w n="38.7">soleil</w>, <w n="38.8">quand</w> <w n="38.9">j</w>’<w n="38.10">y</w> <w n="38.11">sens</w> <w n="38.12">ton</w> <w n="38.13">amour</w> ;</l>
						<l n="39" num="9.7"><w n="39.1">Et</w> <w n="39.2">si</w> <w n="39.3">pour</w> <w n="39.4">d</w>’<w n="39.5">autres</w> <w n="39.6">yeux</w> <w n="39.7">tes</w> <w n="39.8">yeux</w> <w n="39.9">ont</w> <w n="39.10">un</w> <w n="39.11">langage</w>,</l>
						<l n="40" num="9.8"><w n="40.1">C</w>’<w n="40.2">est</w> <w n="40.3">l</w>’<w n="40.4">eau</w>, <w n="40.5">miroir</w> <w n="40.6">éteint</w> <w n="40.7">d</w>’<w n="40.8">où</w> <w n="40.9">s</w>’<w n="40.10">efface</w> <w n="40.11">le</w> <w n="40.12">jour</w>.</l>
					</lg>
					<lg n="10">
						<l n="41" num="10.1"><w n="41.1">Toi</w> ! <w n="41.2">me</w> <w n="41.3">hais</w>-<w n="41.4">tu</w> ? <w n="41.5">dis</w> <w n="41.6">vrai</w> ! <w n="41.7">t</w>’<w n="41.8">ai</w>-<w n="41.9">je</w> <w n="41.10">offensé</w>, <w n="41.11">mon</w> <w n="41.12">âme</w> ?</l>
						<l n="42" num="10.2"><w n="42.1">Dis</w> : <w n="42.2">quelque</w> <w n="42.3">mot</w> <w n="42.4">amer</w> <w n="42.5">dans</w> <w n="42.6">un</w> <w n="42.7">pli</w> <w n="42.8">de</w> <w n="42.9">ton</w> <w n="42.10">cœur</w></l>
						<l n="43" num="10.3"><w n="43.1">Parle</w>-<w n="43.2">t</w>-<w n="43.3">il</w> <w n="43.4">contre</w> <w n="43.5">moi</w>, <w n="43.6">ta</w> <w n="43.7">sœur</w>, <w n="43.8">ta</w> <w n="43.9">faible</w> <w n="43.10">femme</w> ?</l>
						<l n="44" num="10.4"><w n="44.1">Oh</w> ! <w n="44.2">parle</w> : <w n="44.3">as</w>-<w n="44.4">tu</w> <w n="44.5">jamais</w> <w n="44.6">compris</w> <w n="44.7">une</w> <w n="44.8">autre</w> <w n="44.9">sœur</w> ?</l>
					</lg>
					<lg n="11">
						<l n="45" num="11.1"><w n="45.1">Non</w> ! <w n="45.2">j</w>’<w n="45.3">ai</w> <w n="45.4">froid</w> <w n="45.5">d</w>’<w n="45.6">y</w> <w n="45.7">penser</w>. <w n="45.8">Tendresse</w> <w n="45.9">inexprimable</w> !</l>
						<l n="46" num="11.2"><w n="46.1">Ignores</w>-<w n="46.2">en</w> <w n="46.3">toujours</w> <w n="46.4">les</w> <w n="46.5">effrois</w> <w n="46.6">douloureux</w>.</l>
						<l n="47" num="11.3"><w n="47.1">Ne</w> <w n="47.2">prends</w> <w n="47.3">de</w> <w n="47.4">mon</w> <w n="47.5">amour</w> <w n="47.6">que</w> <w n="47.7">ce</w> <w n="47.8">qu</w>’<w n="47.9">il</w> <w n="47.10">a</w> <w n="47.11">d</w>’<w n="47.12">aimable</w>,</l>
						<l n="48" num="11.4"><w n="48.1">Et</w> <w n="48.2">ne</w> <w n="48.3">garde</w> <w n="48.4">du</w> <w n="48.5">tien</w> <w n="48.6">que</w> <w n="48.7">ce</w> <w n="48.8">qui</w> <w n="48.9">rend</w> <w n="48.10">heureux</w> !</l>
					</lg>
					<lg n="12">
						<l n="49" num="12.1"><w n="49.1">Mais</w>, <w n="49.2">laisse</w>-<w n="49.3">moi</w> <w n="49.4">t</w>’<w n="49.5">aimer</w> ! <w n="49.6">Laisse</w>-<w n="49.7">moi</w> <w n="49.8">vivre</w> <w n="49.9">encore</w> !</l>
						<l n="50" num="12.2"><w n="50.1">Laisse</w> <w n="50.2">ton</w> <w n="50.3">nom</w> <w n="50.4">sur</w> <w n="50.5">moi</w> <w n="50.6">comme</w> <w n="50.7">un</w> <w n="50.8">rayon</w> <w n="50.9">d</w>’<w n="50.10">espoir</w> !</l>
						<l n="51" num="12.3"><w n="51.1">Mais</w>, <w n="51.2">dans</w> <w n="51.3">le</w> <w n="51.4">mot</w> <w n="51.5">demain</w> <w n="51.6">laisse</w>-<w n="51.7">moi</w> <w n="51.8">t</w>’<w n="51.9">entrevoir</w>,</l>
						<l n="52" num="12.4"><w n="52.1">Et</w>, <w n="52.2">si</w> <w n="52.3">j</w>’<w n="52.4">ai</w> <w n="52.5">d</w>’<w n="52.6">autres</w> <w n="52.7">jours</w>, <w n="52.8">viens</w> <w n="52.9">me</w> <w n="52.10">les</w> <w n="52.11">faire</w> <w n="52.12">éclore</w> !</l>
					</lg>
				</div></body></text></TEI>