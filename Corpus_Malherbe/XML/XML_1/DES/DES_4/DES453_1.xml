<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES453">
					<head type="main">LALY GALINE SEULE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Jardin</w> <w n="1.2">de</w> <w n="1.3">ma</w> <w n="1.4">fenêtre</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ma</w> <w n="2.2">seule</w> <w n="2.3">terre</w> <w n="2.4">à</w> <w n="2.5">moi</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Avril</w> <w n="3.2">t</w>’<w n="3.3">a</w> <w n="3.4">fait</w> <w n="3.5">renaître</w>…</l>
						<l n="4" num="1.4"><w n="4.1">N</w> ’<w n="4.2">est</w>-<w n="4.3">il</w> <w n="4.4">bon</w> <w n="4.5">que</w> <w n="4.6">pour</w> <w n="4.7">toi</w> ?</l>
						<l n="5" num="1.5"><w n="5.1">Tes</w> <w n="5.2">fleurs</w> <w n="5.3">moins</w> <w n="5.4">chancelantes</w></l>
						<l n="6" num="1.6"><w n="6.1">Se</w> <w n="6.2">reparlent</w> <w n="6.3">tout</w> <w n="6.4">bas</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">moi</w>, <w n="7.3">je</w> <w n="7.4">sais</w> <w n="7.5">deux</w> <w n="7.6">plantes</w></l>
						<l n="8" num="1.8"><w n="8.1">Qu</w>’<w n="8.2">il</w> <w n="8.3">ne</w> <w n="8.4">réunit</w> <w n="8.5">pas</w> !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">Combien</w> <w n="9.2">de</w> <w n="9.3">jours</w> <w n="9.4">de</w> <w n="9.5">fête</w></l>
						<l n="10" num="2.2"><w n="10.1">Ont</w> <w n="10.2">regardé</w> <w n="10.3">mes</w> <w n="10.4">pleurs</w></l>
						<l n="11" num="2.3"><w n="11.1">Sans</w> <w n="11.2">relever</w> <w n="11.3">ma</w> <w n="11.4">tête</w></l>
						<l n="12" num="2.4"><w n="12.1">Pensive</w> <w n="12.2">sur</w> <w n="12.3">tes</w> <w n="12.4">fleurs</w> !</l>
						<l n="13" num="2.5"><w n="13.1">Mais</w> <w n="13.2">celui</w> <w n="13.3">qui</w> <w n="13.4">fait</w> <w n="13.5">l</w>’<w n="13.6">heure</w></l>
						<l n="14" num="2.6"><w n="14.1">Compte</w> <w n="14.2">mon</w> <w n="14.3">temps</w> <w n="14.4">amer</w> ;</l>
						<l n="15" num="2.7"><w n="15.1">Il</w> <w n="15.2">voit</w> <w n="15.3">dans</w> <w n="15.4">ma</w> <w n="15.5">demeure</w></l>
						<l n="16" num="2.8"><w n="16.1">Comme</w> <w n="16.2">il</w> <w n="16.3">voit</w> <w n="16.4">dans</w> <w n="16.5">la</w> <w n="16.6">mer</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Ce</w> <w n="17.2">soir</w> <w n="17.3">une</w> <w n="17.4">hirondelle</w></l>
						<l n="18" num="3.2"><w n="18.1">Qui</w> <w n="18.2">revenait</w> <w n="18.3">des</w> <w n="18.4">cieux</w></l>
						<l n="19" num="3.3"><w n="19.1">À</w> <w n="19.2">frôlé</w> <w n="19.3">de</w> <w n="19.4">son</w> <w n="19.5">aile</w></l>
						<l n="20" num="3.4"><w n="20.1">Tes</w> <w n="20.2">bouquets</w> <w n="20.3">gracieux</w>.</l>
						<l n="21" num="3.5"><w n="21.1">Ta</w> <w n="21.2">fraîche</w> <w n="21.3">palissade</w></l>
						<l n="22" num="3.6"><w n="22.1">A</w> <w n="22.2">tremblé</w> <w n="22.3">sous</w> <w n="22.4">son</w> <w n="22.5">cœur</w> :</l>
						<l n="23" num="3.7"><w n="23.1">Vient</w>-<w n="23.2">elle</w> <w n="23.3">en</w> <w n="23.4">ambassade</w></l>
						<l n="24" num="3.8"><w n="24.1">De</w> <w n="24.2">la</w> <w n="24.3">part</w> <w n="24.4">du</w> <w n="24.5">bonheur</w> ?</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Sans</w> <w n="25.2">lune</w> <w n="25.3">et</w> <w n="25.4">sans</w> <w n="25.5">étoile</w></l>
						<l n="26" num="4.2"><w n="26.1">Quand</w> <w n="26.2">la</w> <w n="26.3">nuit</w> <w n="26.4">teint</w> <w n="26.5">les</w> <w n="26.6">flots</w></l>
						<l n="27" num="4.3"><w n="27.1">J</w>’<w n="27.2">allume</w> <w n="27.3">sous</w> <w n="27.4">ton</w> <w n="27.5">voile</w></l>
						<l n="28" num="4.4"><w n="28.1">Ma</w> <w n="28.2">lampe</w> <w n="28.3">aux</w> <w n="28.4">matelots</w> ;</l>
						<l n="29" num="4.5"><w n="29.1">Afin</w> <w n="29.2">que</w> <w n="29.3">l</w>’<w n="29.4">humble</w> <w n="29.5">flamme</w></l>
						<l n="30" num="4.6"><w n="30.1">Qui</w> <w n="30.2">s</w>’<w n="30.3">épuise</w> <w n="30.4">ardemment</w></l>
						<l n="31" num="4.7"><w n="31.1">Comme</w> <w n="31.2">un</w> <w n="31.3">peu</w> <w n="31.4">de</w> <w n="31.5">mon</w> <w n="31.6">âme</w></l>
						<l n="32" num="4.8"><w n="32.1">Attire</w> <w n="32.2">mon</w> <w n="32.3">amant</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Mais</w> <w n="33.2">du</w> <w n="33.3">port</w>, <w n="33.4">si</w> <w n="33.5">le</w> <w n="33.6">phare</w></l>
						<l n="34" num="5.2"><w n="34.1">Mourait</w> <w n="34.2">avant</w> <w n="34.3">le</w> <w n="34.4">jour</w>,</l>
						<l n="35" num="5.3"><w n="35.1">Au</w> <w n="35.2">marin</w> <w n="35.3">qui</w> <w n="35.4">s</w>’<w n="35.5">égare</w></l>
						<l n="36" num="5.4"><w n="36.1">Montre</w> <w n="36.2">au</w> <w n="36.3">loin</w> <w n="36.4">mon</w> <w n="36.5">séjour</w>.</l>
						<l n="37" num="5.5"><w n="37.1">Dis</w>-<w n="37.2">lui</w> <w n="37.3">qu</w>’<w n="37.4">à</w> <w n="37.5">ma</w> <w n="37.6">fenêtre</w>,</l>
						<l n="38" num="5.6"><w n="38.1">Toujours</w> <w n="38.2">comme</w> <w n="38.3">aujourd</w>’<w n="38.4">hui</w>.</l>
						<l n="39" num="5.7"><w n="39.1">Les</w> <w n="39.2">fleurs</w> <w n="39.3">qu</w>’<w n="39.4">il</w> <w n="39.5">a</w> <w n="39.6">fait</w> <w n="39.7">naître</w></l>
						<l n="40" num="5.8"><w n="40.1">S</w>’<w n="40.2">illuminent</w> <w n="40.3">pour</w> <w n="40.4">lui</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">Dans</w> <w n="41.2">la</w> <w n="41.3">nuit</w> <w n="41.4">implorée</w></l>
						<l n="42" num="6.2"><w n="42.1">Qui</w> <w n="42.2">le</w> <w n="42.3">ramènera</w>,</l>
						<l n="43" num="6.3"><w n="43.1">Vers</w> <w n="43.2">ma</w> <w n="43.3">vitre</w> <w n="43.4">éclairée</w></l>
						<l n="44" num="6.4"><w n="44.1">Son</w> <w n="44.2">âme</w> <w n="44.3">montera</w>.</l>
						<l n="45" num="6.5"><w n="45.1">Fais</w> <w n="45.2">qu</w>’<w n="45.3">après</w> <w n="45.4">ma</w> <w n="45.5">neuvaine</w>,</l>
						<l n="46" num="6.6"><w n="46.1">Au</w> <w n="46.2">bout</w> <w n="46.3">d</w>’<w n="46.4">un</w> <w n="46.5">an</w> <w n="46.6">perdu</w>,</l>
						<l n="47" num="6.7"><w n="47.1">Ma</w> <w n="47.2">lampe</w> <w n="47.3">le</w> <w n="47.4">ramène</w></l>
						<l n="48" num="6.8"><w n="48.1">À</w> <w n="48.2">mes</w> <w n="48.3">bras</w> <w n="48.4">suspendu</w>.</l>
					</lg>
					<closer>
						<placeName>Rochefort.</placeName>
					</closer>
				</div></body></text></TEI>