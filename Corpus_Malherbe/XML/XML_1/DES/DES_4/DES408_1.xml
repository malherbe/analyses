<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FOI</head><div type="poem" key="DES408">
					<head type="main">LES PRISONS ET LES PRIÈRES</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Pleurez</w> : <w n="1.2">comptez</w> <w n="1.3">les</w> <w n="1.4">noms</w> <w n="1.5">des</w> <w n="1.6">bannis</w> <w n="1.7">de</w> <w n="1.8">la</w> <w n="1.9">France</w> :</l>
						<l n="2" num="1.2"><w n="2.1">L</w>’<w n="2.2">air</w> <w n="2.3">manque</w> <w n="2.4">à</w> <w n="2.5">ces</w> <w n="2.6">grands</w> <w n="2.7">cœurs</w> <w n="2.8">où</w> <w n="2.9">brûle</w> <w n="2.10">tant</w> <w n="2.11">d</w>’<w n="2.12">espoir</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Jetez</w> <w n="3.2">la</w> <w n="3.3">palme</w> <w n="3.4">en</w> <w n="3.5">deuil</w>, <w n="3.6">au</w> <w n="3.7">pied</w> <w n="3.8">de</w> <w n="3.9">leur</w> <w n="3.10">souffrance</w> ;</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">passons</w> : <w n="4.3">les</w> <w n="4.4">geôliers</w> <w n="4.5">seuls</w> <w n="4.6">ont</w> <w n="4.7">droit</w> <w n="4.8">de</w> <w n="4.9">les</w> <w n="4.10">voir</w> !</l>
						<l n="5" num="1.5"><w n="5.1">Passons</w> : <w n="5.2">nos</w> <w n="5.3">bras</w> <w n="5.4">pieux</w> <w n="5.5">sont</w> <w n="5.6">sans</w> <w n="5.7">force</w> <w n="5.8">et</w> <w n="5.9">sans</w> <w n="5.10">armes</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">Nous</w> <w n="6.2">n</w>’<w n="6.3">allons</w> <w n="6.4">point</w> <w n="6.5">traînant</w> <w n="6.6">de</w> <w n="6.7">fratricides</w> <w n="6.8">vœux</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Mais</w>, <w n="7.2">femmes</w>, <w n="7.3">nous</w> <w n="7.4">portons</w> <w n="7.5">la</w> <w n="7.6">prière</w> <w n="7.7">et</w> <w n="7.8">les</w> <w n="7.9">larmes</w>,</l>
						<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">Dieu</w>, <w n="8.3">le</w> <w n="8.4">Dieu</w> <w n="8.5">du</w> <w n="8.6">peuple</w> <w n="8.7">en</w> <w n="8.8">demande</w> <w n="8.9">pour</w> <w n="8.10">eux</w>.</l>
						<l n="9" num="1.9"><w n="9.1">Voyez</w> <w n="9.2">vers</w> <w n="9.3">la</w> <w n="9.4">prison</w> <w n="9.5">glisser</w> <w n="9.6">de</w> <w n="9.7">saintes</w> <w n="9.8">âmes</w> ;</l>
						<l n="10" num="1.10"><w n="10.1">Salut</w> ! <w n="10.2">vous</w> <w n="10.3">qui</w> <w n="10.4">cachez</w> <w n="10.5">vos</w> <w n="10.6">ailes</w> <w n="10.7">ici</w>-<w n="10.8">bas</w> :</l>
						<l n="11" num="1.11"><w n="11.1">Sous</w> <w n="11.2">vos</w> <w n="11.3">manteaux</w> <w n="11.4">mouillés</w> <w n="11.5">et</w> <w n="11.6">vos</w> <w n="11.7">pâleurs</w> <w n="11.8">de</w> <w n="11.9">femmes</w>,</l>
						<l n="12" num="1.12"><w n="12.1">Que</w> <w n="12.2">de</w> <w n="12.3">cendre</w> <w n="12.4">et</w> <w n="12.5">de</w> <w n="12.6">boue</w> <w n="12.7">ont</w> <w n="12.8">entravé</w> <w n="12.9">vos</w> <w n="12.10">pas</w> !</l>
						<l n="13" num="1.13"><w n="13.1">Salut</w> ! <w n="13.2">vos</w> <w n="13.3">yeux</w> <w n="13.4">divins</w> <w n="13.5">rougis</w> <w n="13.6">de</w> <w n="13.7">larmes</w> <w n="13.8">vives</w></l>
						<l n="14" num="1.14"><w n="14.1">Reviennent</w> <w n="14.2">se</w> <w n="14.3">noyer</w> <w n="14.4">dans</w> <w n="14.5">ce</w> <w n="14.6">monde</w> <w n="14.7">étouffant</w> ;</l>
						<l n="15" num="1.15"><w n="15.1">Vous</w> <w n="15.2">errez</w>, <w n="15.3">comme</w> <w n="15.4">alors</w>, <w n="15.5">au</w> <w n="15.6">Jardin</w> <w n="15.7">des</w> <w n="15.8">Olives</w> ;</l>
						<l n="16" num="1.16"><w n="16.1">Car</w> <w n="16.2">le</w> <w n="16.3">Christ</w> <w n="16.4">est</w> <w n="16.5">en</w> <w n="16.6">peine</w> <w n="16.7">et</w> <w n="16.8">Judas</w> <w n="16.9">triomphant</w>.</l>
						<l n="17" num="1.17"><w n="17.1">Oui</w>, <w n="17.2">le</w> <w n="17.3">Christ</w> <w n="17.4">est</w> <w n="17.5">en</w> <w n="17.6">peine</w> ; <w n="17.7">il</w> <w n="17.8">prévoit</w> <w n="17.9">tant</w> <w n="17.10">de</w> <w n="17.11">crimes</w> !</l>
						<l n="18" num="1.18"><w n="18.1">Lui</w> <w n="18.2">dont</w> <w n="18.3">les</w> <w n="18.4">bras</w> <w n="18.5">cloués</w> <w n="18.6">ont</w> <w n="18.7">brisé</w> <w n="18.8">tant</w> <w n="18.9">de</w> <w n="18.10">fers</w> !</l>
						<l n="19" num="1.19"><w n="19.1">Il</w> <w n="19.2">revoit</w> <w n="19.3">dans</w> <w n="19.4">son</w> <w n="19.5">sang</w> <w n="19.6">nager</w> <w n="19.7">tant</w> <w n="19.8">de</w> <w n="19.9">victimes</w>,</l>
						<l n="20" num="1.20"><w n="20.1">Qu</w>’<w n="20.2">il</w> <w n="20.3">veut</w> <w n="20.4">mourir</w> <w n="20.5">encor</w> <w n="20.6">pour</w> <w n="20.7">fermer</w> <w n="20.8">les</w> <w n="20.9">enfers</w> !</l>
						<l n="21" num="1.21"><w n="21.1">Courez</w>, <w n="21.2">doux</w> <w n="21.3">orphelins</w>, <w n="21.4">montez</w> <w n="21.5">dans</w> <w n="21.6">la</w> <w n="21.7">balance</w> ;</l>
						<l n="22" num="1.22"><w n="22.1">Priez</w> <w n="22.2">pour</w> <w n="22.3">les</w> <w n="22.4">méchants</w> <w n="22.5">qui</w> <w n="22.6">vivent</w> <w n="22.7">sans</w> <w n="22.8">remords</w> ;</l>
						<l n="23" num="1.23"><w n="23.1">Rachetez</w> <w n="23.2">les</w> <w n="23.3">forfaits</w> <w n="23.4">des</w> <w n="23.5">pleurs</w> <w n="23.6">de</w> <w n="23.7">l</w>’<w n="23.8">innocence</w>,</l>
						<l n="24" num="1.24"><w n="24.1">Et</w> <w n="24.2">dans</w> <w n="24.3">un</w> <w n="24.4">flot</w> <w n="24.5">amer</w> <w n="24.6">lavez</w> <w n="24.7">nos</w> <w n="24.8">pauvres</w> <w n="24.9">morts</w> !</l>
						<l n="25" num="1.25"><w n="25.1">Et</w> <w n="25.2">nous</w>, <w n="25.3">n</w>’<w n="25.4">envoyons</w> <w n="25.5">plus</w> <w n="25.6">à</w> <w n="25.7">des</w> <w n="25.8">guerres</w> <w n="25.9">impies</w></l>
						<l n="26" num="1.26"><w n="26.1">Nos</w> <w n="26.2">fils</w> <w n="26.3">adolescents</w> <w n="26.4">et</w> <w n="26.5">nos</w> <w n="26.6">drapeaux</w> <w n="26.7">vainqueurs</w> ;</l>
						<l n="27" num="1.27"><w n="27.1">Avons</w>-<w n="27.2">nous</w> <w n="27.3">amassé</w> <w n="27.4">nos</w> <w n="27.5">pieuses</w> <w n="27.6">charpies</w></l>
						<l n="28" num="1.28"><w n="28.1">Pour</w> <w n="28.2">les</w> <w n="28.3">baigner</w> <w n="28.4">du</w> <w n="28.5">sang</w> <w n="28.6">le</w> <w n="28.7">plus</w> <w n="28.8">pur</w> <w n="28.9">de</w> <w n="28.10">nos</w> <w n="28.11">cœurs</w> ?</l>
						<l n="29" num="1.29"><w n="29.1">Pitié</w> ! <w n="29.2">nous</w> <w n="29.3">n</w>’<w n="29.4">avons</w> <w n="29.5">plus</w> <w n="29.6">le</w> <w n="29.7">temps</w> <w n="29.8">des</w> <w n="29.9">longues</w> <w n="29.10">haines</w> :</l>
						<l n="30" num="1.30"><w n="30.1">La</w> <w n="30.2">haine</w> <w n="30.3">est</w> <w n="30.4">basse</w> <w n="30.5">et</w> <w n="30.6">sombre</w> ; <w n="30.7">il</w> <w n="30.8">fait</w> <w n="30.9">jour</w> ! <w n="30.10">il</w> <w n="30.11">fait</w> <w n="30.12">jour</w> !</l>
						<l n="31" num="1.31"><w n="31.1">France</w> <w n="31.2">l</w> <w n="31.3">il</w> <w n="31.4">faut</w> <w n="31.5">aimer</w>, <w n="31.6">il</w> <w n="31.7">faut</w> <w n="31.8">rompre</w> <w n="31.9">les</w> <w n="31.10">chaînes</w>,</l>
						<l n="32" num="1.32"><w n="32.1">Ton</w> <w n="32.2">Dieu</w>, <w n="32.3">le</w> <w n="32.4">Dieu</w> <w n="32.5">du</w> <w n="32.6">peuple</w> <w n="32.7">a</w> <w n="32.8">tant</w> <w n="32.9">besoin</w> <w n="32.10">d</w>’<w n="32.11">amour</w> !</l>
					</lg>
				</div></body></text></TEI>