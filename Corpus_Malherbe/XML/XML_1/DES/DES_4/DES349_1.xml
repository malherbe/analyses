<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">AMOUR</head><div type="poem" key="DES349">
					<head type="main">UNE LETTRE DE FEMME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Les</w> <w n="1.2">femmes</w>, <w n="1.3">je</w> <w n="1.4">le</w> <w n="1.5">sais</w>, <w n="1.6">ne</w> <w n="1.7">doivent</w> <w n="1.8">pas</w> <w n="1.9">écrire</w>,</l>
						<l n="2" num="1.2"><space quantity="10" unit="char"></space><w n="2.1">J</w>’<w n="2.2">écris</w> <w n="2.3">pourtant</w>,</l>
						<l n="3" num="1.3"><w n="3.1">Afin</w> <w n="3.2">que</w> <w n="3.3">dans</w> <w n="3.4">mon</w> <w n="3.5">cœur</w> <w n="3.6">au</w> <w n="3.7">loin</w> <w n="3.8">tu</w> <w n="3.9">puisses</w> <w n="3.10">lire</w></l>
						<l n="4" num="1.4"><space quantity="10" unit="char"></space><w n="4.1">Comme</w> <w n="4.2">en</w> <w n="4.3">partant</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Je</w> <w n="5.2">ne</w> <w n="5.3">tracerai</w> <w n="5.4">rien</w> <w n="5.5">qui</w> <w n="5.6">ne</w> <w n="5.7">soit</w> <w n="5.8">dans</w> <w n="5.9">toi</w>-<w n="5.10">même</w></l>
						<l n="6" num="2.2"><space quantity="10" unit="char"></space><w n="6.1">Beaucoup</w> <w n="6.2">plus</w> <w n="6.3">beau</w> :</l>
						<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">le</w> <w n="7.3">mot</w> <w n="7.4">cent</w> <w n="7.5">fois</w> <w n="7.6">dit</w>, <w n="7.7">venant</w> <w n="7.8">de</w> <w n="7.9">ce</w> <w n="7.10">qu</w>’<w n="7.11">on</w> <w n="7.12">aime</w>,</l>
						<l n="8" num="2.4"><space quantity="10" unit="char"></space><w n="8.1">Semble</w> <w n="8.2">nouveau</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Qu</w>’<w n="9.2">il</w> <w n="9.3">te</w> <w n="9.4">porte</w> <w n="9.5">au</w> <w n="9.6">bonheur</w> ! <w n="9.7">Moi</w>, <w n="9.8">je</w> <w n="9.9">reste</w> <w n="9.10">à</w> <w n="9.11">l</w>’<w n="9.12">attendre</w>,</l>
						<l n="10" num="3.2"><space quantity="10" unit="char"></space><w n="10.1">Bien</w> <w n="10.2">que</w>, <w n="10.3">là</w> <w n="10.4">bas</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">sens</w> <w n="11.3">que</w> <w n="11.4">je</w> <w n="11.5">m</w>’<w n="11.6">en</w> <w n="11.7">vais</w>, <w n="11.8">pour</w> <w n="11.9">voir</w> <w n="11.10">et</w> <w n="11.11">pour</w> <w n="11.12">entendre</w></l>
						<l n="12" num="3.4"><space quantity="10" unit="char"></space><w n="12.1">Errer</w> <w n="12.2">tes</w> <w n="12.3">pas</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Ne</w> <w n="13.2">te</w> <w n="13.3">détourne</w> <w n="13.4">point</w> <w n="13.5">s</w>’<w n="13.6">il</w> <w n="13.7">passe</w> <w n="13.8">une</w> <w n="13.9">hirondelle</w></l>
						<l n="14" num="4.2"><space quantity="10" unit="char"></space><w n="14.1">Par</w> <w n="14.2">le</w> <w n="14.3">chemin</w>,</l>
						<l n="15" num="4.3"><w n="15.1">Car</w> <w n="15.2">je</w> <w n="15.3">crois</w> <w n="15.4">que</w> <w n="15.5">c</w>’<w n="15.6">est</w> <w n="15.7">moi</w> <w n="15.8">qui</w> <w n="15.9">passerai</w>, <w n="15.10">fidèle</w>,</l>
						<l n="16" num="4.4"><space quantity="10" unit="char"></space><w n="16.1">Toucher</w> <w n="16.2">ta</w> <w n="16.3">main</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Tu</w> <w n="17.2">t</w>’<w n="17.3">en</w> <w n="17.4">vas</w>, <w n="17.5">tout</w> <w n="17.6">s</w>’<w n="17.7">en</w> <w n="17.8">va</w> ! <w n="17.9">Tout</w> <w n="17.10">se</w> <w n="17.11">met</w> <w n="17.12">en</w> <w n="17.13">voyage</w>,</l>
						<l n="18" num="5.2"><space quantity="10" unit="char"></space><w n="18.1">Lumière</w> <w n="18.2">et</w> <w n="18.3">fleurs</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">bel</w> <w n="19.3">été</w> <w n="19.4">te</w> <w n="19.5">suit</w>, <w n="19.6">me</w> <w n="19.7">laissant</w> <w n="19.8">à</w> <w n="19.9">l</w>’<w n="19.10">orage</w>.</l>
						<l n="20" num="5.4"><space quantity="10" unit="char"></space><w n="20.1">Lourde</w> <w n="20.2">de</w> <w n="20.3">pleurs</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Mais</w> <w n="21.2">si</w> <w n="21.3">l</w>’<w n="21.4">on</w> <w n="21.5">ne</w> <w n="21.6">vit</w> <w n="21.7">plus</w> <w n="21.8">que</w> <w n="21.9">d</w>’<w n="21.10">espoir</w> <w n="21.11">et</w> <w n="21.12">d</w>’<w n="21.13">alarmes</w></l>
						<l n="22" num="6.2"><space quantity="10" unit="char"></space><w n="22.1">Cessant</w> <w n="22.2">de</w> <w n="22.3">voir</w>,</l>
						<l n="23" num="6.3"><w n="23.1">Partageons</w> <w n="23.2">pour</w> <w n="23.3">le</w> <w n="23.4">mieux</w> : <w n="23.5">moi</w>, <w n="23.6">je</w> <w n="23.7">retiens</w> <w n="23.8">les</w> <w n="23.9">larmes</w>,</l>
						<l n="24" num="6.4"><space quantity="10" unit="char"></space><w n="24.1">Garde</w> <w n="24.2">l</w>’<w n="24.3">espoir</w>,</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Non</w>, <w n="25.2">je</w> <w n="25.3">ne</w> <w n="25.4">voudrais</w> <w n="25.5">pas</w>, <w n="25.6">tant</w> <w n="25.7">je</w> <w n="25.8">te</w> <w n="25.9">suis</w> <w n="25.10">unie</w>,</l>
						<l n="26" num="7.2"><space quantity="10" unit="char"></space><w n="26.1">Te</w> <w n="26.2">voir</w> <w n="26.3">souffrir</w> :</l>
						<l n="27" num="7.3"><w n="27.1">Souhaiter</w> <w n="27.2">la</w> <w n="27.3">douleur</w> <w n="27.4">à</w> <w n="27.5">sa</w> <w n="27.6">moitié</w> <w n="27.7">bénie</w>.</l>
						<l n="28" num="7.4"><space quantity="10" unit="char"></space><w n="28.1">C</w>’<w n="28.2">est</w> <w n="28.3">se</w> <w n="28.4">haïr</w>.</l>
					</lg>
				</div></body></text></TEI>