<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FOI</head><div type="poem" key="DES407">
					<head type="main">UNE NUIT DE MON ÂME</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Par</w> <w n="1.2">un</w> <w n="1.3">rêve</w> <w n="1.4">dont</w> <w n="1.5">la</w> <w n="1.6">flamme</w></l>
						<l n="2" num="1.2"><w n="2.1">Éclairait</w> <w n="2.2">mes</w> <w n="2.3">yeux</w> <w n="2.4">fermés</w>,</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">nuit</w> <w n="3.3">emporta</w> <w n="3.4">mon</w> <w n="3.5">âme</w></l>
						<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">dorment</w> <w n="4.3">nos</w> <w n="4.4">morts</w> <w n="4.5">aimés</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Sous</w> <w n="5.2">ma</w> <w n="5.3">fervente</w> <w n="5.4">lumière</w></l>
						<l n="6" num="1.6"><w n="6.1">Le</w> <w n="6.2">sol</w> <w n="6.3">tressaille</w> <w n="6.4">et</w> <w n="6.5">se</w> <w n="6.6">fend</w>,</l>
						<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">je</w> <w n="7.3">ressaisis</w> <w n="7.4">ma</w> <w n="7.5">mère</w></l>
						<l n="8" num="1.8"><w n="8.1">Qui</w> <w n="8.2">renaît</w> <w n="8.3">pour</w> <w n="8.4">son</w> <w n="8.5">enfant</w> !</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1">« <w n="9.1">Tu</w> <w n="9.2">viens</w> <w n="9.3">donc</w> ! » <w n="9.4">dit</w> <w n="9.5">la</w> <w n="9.6">chère</w> <w n="9.7">ombre</w></l>
						<l n="10" num="2.2"><w n="10.1">Dont</w> <w n="10.2">la</w> <w n="10.3">voix</w> <w n="10.4">m</w>’<w n="10.5">ouvre</w> <w n="10.6">le</w> <w n="10.7">cœur</w> ;</l>
						<l n="11" num="2.3">« <w n="11.1">Tu</w> <w n="11.2">sais</w> <w n="11.3">donc</w> <w n="11.4">qu</w>’<w n="11.5">en</w> <w n="11.6">ce</w> <w n="11.7">lieu</w> <w n="11.8">sombre</w></l>
						<l n="12" num="2.4"><w n="12.1">Tout</w> <w n="12.2">spectre</w> <w n="12.3">attend</w> <w n="12.4">le</w> <w n="12.5">bonheur</w> ?</l>
						<l n="13" num="2.5"><w n="13.1">Viens</w>, <w n="13.2">ne</w> <w n="13.3">crains</w> <w n="13.4">pas</w> <w n="13.5">leur</w> <w n="13.6">silence</w></l>
						<l n="14" num="2.6"><w n="14.1">Ni</w> <w n="14.2">leurs</w> <w n="14.3">yeux</w> <w n="14.4">ouverts</w> <w n="14.5">sans</w> <w n="14.6">voir</w>,</l>
						<l n="15" num="2.7"><w n="15.1">Le</w> <w n="15.2">sommeil</w> <w n="15.3">qui</w> <w n="15.4">les</w> <w n="15.5">balance</w></l>
						<l n="16" num="2.8"><w n="16.1">N</w>’<w n="16.2">a</w> <w n="16.3">de</w> <w n="16.4">vivant</w> <w n="16.5">que</w> <w n="16.6">l</w>’<w n="16.7">espoir</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">L</w>’<w n="17.2">espoir</w>, <w n="17.3">ô</w> <w n="17.4">ma</w> <w n="17.5">bien</w>-<w n="17.6">aimée</w>,</l>
						<l n="18" num="3.2"><w n="18.1">Sève</w> <w n="18.2">qui</w> <w n="18.3">remonte</w> <w n="18.4">à</w> <w n="18.5">Dieu</w>.</l>
						<l n="19" num="3.3"><w n="19.1">Vigne</w> <w n="19.2">errante</w> <w n="19.3">et</w> <w n="19.4">parfumée</w></l>
						<l n="20" num="3.4"><w n="20.1">Qui</w> <w n="20.2">fleurit</w>, <w n="20.3">même</w> <w n="20.4">en</w> <w n="20.5">ce</w> <w n="20.6">lieu</w> ;</l>
						<l n="21" num="3.5"><w n="21.1">L</w>’<w n="21.2">espoir</w>, <w n="21.3">cette</w> <w n="21.4">étreinte</w> <w n="21.5">immense</w></l>
						<l n="22" num="3.6"><w n="22.1">Qui</w> <w n="22.2">joint</w> <w n="22.3">tous</w> <w n="22.4">les</w> <w n="22.5">univers</w>,</l>
						<l n="23" num="3.7"><w n="23.1">Ne</w> <w n="23.2">sens</w>-<w n="23.3">tu</w> <w n="23.4">pas</w> <w n="23.5">qu</w>’<w n="23.6">il</w> <w n="23.7">commence</w></l>
						<l n="24" num="3.8"><w n="24.1">D</w>’<w n="24.2">unir</w> <w n="24.3">au</w> <w n="24.4">moins</w> <w n="24.5">nos</w> <w n="24.6">revers</w> ?</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Comme</w> <w n="25.2">aux</w> <w n="25.3">chaleurs</w> <w n="25.4">d</w>’<w n="25.5">une</w> <w n="25.6">serre</w></l>
						<l n="26" num="4.2"><w n="26.1">L</w>’<w n="26.2">homme</w> <w n="26.3">fait</w> <w n="26.4">germer</w> <w n="26.5">ses</w> <w n="26.6">fleurs</w>,</l>
						<l n="27" num="4.3"><w n="27.1">Le</w> <w n="27.2">trépas</w> <w n="27.3">qui</w> <w n="27.4">nous</w> <w n="27.5">enserre</w></l>
						<l n="28" num="4.4"><w n="28.1">Ici</w> <w n="28.2">fait</w> <w n="28.3">germer</w> <w n="28.4">nos</w> <w n="28.5">cœurs</w>.</l>
						<l n="29" num="4.5"><w n="29.1">À</w> <w n="29.2">travers</w> <w n="29.3">le</w> <w n="29.4">dernier</w> <w n="29.5">voile</w></l>
						<l n="30" num="4.6"><w n="30.1">Tendu</w> <w n="30.2">sur</w> <w n="30.3">l</w>’<w n="30.4">autre</w> <w n="30.5">avenir</w></l>
						<l n="31" num="4.7"><w n="31.1">Nous</w> <w n="31.2">voyons</w> <w n="31.3">la</w> <w n="31.4">double</w> <w n="31.5">étoile</w></l>
						<l n="32" num="4.8"><w n="32.1">De</w> <w n="32.2">l</w>’<w n="32.3">aube</w> <w n="32.4">et</w> <w n="32.5">du</w> <w n="32.6">souvenir</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Que</w> <w n="33.2">de</w> <w n="33.3">sources</w> <w n="33.4">éternelles</w></l>
						<l n="34" num="5.2"><w n="34.1">Dans</w> <w n="34.2">ces</w> <w n="34.3">lointains</w> <w n="34.4">toujours</w> <w n="34.5">beaux</w></l>
						<l n="35" num="5.3"><w n="35.1">Que</w> <w n="35.2">d</w>’<w n="35.3">arbres</w> <w n="35.4">aux</w> <w n="35.5">fleurs</w> <w n="35.6">nouvelles</w></l>
						<l n="36" num="5.4"><w n="36.1">Sur</w> <w n="36.2">ces</w> <w n="36.3">routes</w> <w n="36.4">sans</w> <w n="36.5">tombeaux</w> !</l>
						<l n="37" num="5.5"><w n="37.1">Vois</w> <w n="37.2">que</w> <w n="37.3">d</w>’<w n="37.4">immortelles</w> <w n="37.5">vies</w></l>
						<l n="38" num="5.6"><w n="38.1">Te</w> <w n="38.2">recevront</w> <w n="38.3">avec</w> <w n="38.4">moi</w> :</l>
						<l n="39" num="5.7"><w n="39.1">Vois</w> <w n="39.2">que</w> <w n="39.3">de</w> <w n="39.4">mères</w> <w n="39.5">suivies</w></l>
						<l n="40" num="5.8"><w n="40.1">D</w>’<w n="40.2">enfants</w> <w n="40.3">aimés</w> <w n="40.4">comme</w> <w n="40.5">toi</w> !</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">Sous</w> <w n="41.2">une</w> <w n="41.3">forme</w> <w n="41.4">reprise</w></l>
						<l n="42" num="6.2"><w n="42.1">Et</w> <w n="42.2">qui</w> <w n="42.3">nous</w> <w n="42.4">ressemblera</w>,</l>
						<l n="43" num="6.3"><w n="43.1">Avec</w> <w n="43.2">un</w> <w n="43.3">cri</w> <w n="43.4">de</w> <w n="43.5">surprise</w></l>
						<l n="44" num="6.4"><w n="44.1">Chacun</w> <w n="44.2">se</w> <w n="44.3">reconnaîtra</w>.</l>
						<l n="45" num="6.5">« <w n="45.1">Quoi</w>, <w n="45.2">c</w>’<w n="45.3">est</w> <w n="45.4">lui</w> ! <w n="45.5">c</w>’<w n="45.6">est</w> <w n="45.7">toi</w> ! <w n="45.8">c</w>’<w n="45.9">est</w> <w n="45.10">elle</w> ! »</l>
						<l n="46" num="6.6"><w n="46.1">Retentira</w> <w n="46.2">de</w> <w n="46.3">partout</w>,</l>
						<l n="47" num="6.7"><w n="47.1">Et</w> <w n="47.2">l</w>’<w n="47.3">on</w> <w n="47.4">proclamera</w> <w n="47.5">belle</w></l>
						<l n="48" num="6.8"><w n="48.1">La</w> <w n="48.2">mort</w> <w n="48.3">vivante</w> <w n="48.4">et</w> <w n="48.5">debout</w> !</l>
					</lg>
					<lg n="7">
						<l n="49" num="7.1"><w n="49.1">Jette</w> <w n="49.2">donc</w> <w n="49.3">loin</w> <w n="49.4">des</w> <w n="49.5">colères</w></l>
						<l n="50" num="7.2"><w n="50.1">Contre</w> <w n="50.2">d</w>’<w n="50.3">innocents</w> <w n="50.4">ingrats</w> ;</l>
						<l n="51" num="7.3"><w n="51.1">Le</w> <w n="51.2">flambeau</w> <w n="51.3">dont</w> <w n="51.4">tu</w> <w n="51.5">t</w>’<w n="51.6">éclaires</w></l>
						<l n="52" num="7.4"><w n="52.1">Te</w> <w n="52.2">voit</w> <w n="52.3">si</w> <w n="52.4">tendre</w> <w n="52.5">en</w> <w n="52.6">mes</w> <w n="52.7">bras</w>.</l>
						<l n="53" num="7.5"><w n="53.1">Cesse</w> <w n="53.2">d</w>’<w n="53.3">essayer</w> <w n="53.4">la</w> <w n="53.5">haine</w>,</l>
						<l n="54" num="7.6"><w n="54.1">Faite</w> <w n="54.2">pour</w> <w n="54.3">la</w> <w n="54.4">mépriser</w> :</l>
						<l n="55" num="7.7"><w n="55.1">C</w>’<w n="55.2">est</w> <w n="55.3">perdre</w> <w n="55.4">à</w> <w n="55.5">river</w> <w n="55.6">ta</w> <w n="55.7">chaîne</w></l>
						<l n="56" num="7.8"><w n="56.1">La</w> <w n="56.2">force</w> <w n="56.3">de</w> <w n="56.4">la</w> <w n="56.5">briser</w>.</l>
					</lg>
					<lg n="8">
						<l n="57" num="8.1"><w n="57.1">Adieu</w>, <w n="57.2">fille</w> <w n="57.3">de</w> <w n="57.4">mes</w> <w n="57.5">larmes</w>,</l>
						<l n="58" num="8.2"><w n="58.1">Revue</w> <w n="58.2">à</w> <w n="58.3">force</w> <w n="58.4">d</w>’<w n="58.5">amour</w>,</l>
						<l n="59" num="8.3"><w n="59.1">Quand</w> <w n="59.2">le</w> <w n="59.3">temps</w> <w n="59.4">rompra</w> <w n="59.5">ses</w> <w n="59.6">armes</w>,</l>
						<l n="60" num="8.4"><w n="60.1">Tu</w> <w n="60.2">me</w> <w n="60.3">suivras</w> <w n="60.4">au</w> <w n="60.5">grand</w> <w n="60.6">jour</w>.</l>
						<l n="61" num="8.5"><w n="61.1">À</w> <w n="61.2">ton</w> <w n="61.3">épreuve</w> <w n="61.4">asservie</w>,</l>
						<l n="62" num="8.6"><w n="62.1">Va</w> <w n="62.2">plaindre</w> <w n="62.3">les</w> <w n="62.4">plus</w> <w n="62.5">souffrants</w>,</l>
						<l n="63" num="8.7"><w n="63.1">Et</w> <w n="63.2">pour</w> <w n="63.3">gagner</w> <w n="63.4">l</w>’<w n="63.5">autre</w> <w n="63.6">vie</w>,</l>
						<l n="64" num="8.8"><w n="64.1">Retourne</w> <w n="64.2">avec</w> <w n="64.3">les</w> <w n="64.4">mourants</w>. »</l>
					</lg>
					<lg n="9">
						<l n="65" num="9.1"><w n="65.1">L</w>’<w n="65.2">ombre</w> <w n="65.3">alors</w> <w n="65.4">pressa</w> <w n="65.5">ma</w> <w n="65.6">lèvre</w></l>
						<l n="66" num="9.2"><w n="66.1">D</w>’<w n="66.2">un</w> <w n="66.3">baiser</w> <w n="66.4">lent</w> <w n="66.5">et</w> <w n="66.6">profond</w></l>
						<l n="67" num="9.3"><w n="67.1">Qui</w> <w n="67.2">d</w>’<w n="67.3">une</w> <w n="67.4">indicible</w> <w n="67.5">fièvre</w></l>
						<l n="68" num="9.4"><w n="68.1">Fait</w> <choice reason="analysis" type="false_verse" hand="CA"><sic>encore</sic><corr source="édition_1973"><w n="68.2">encor</w></corr></choice> <w n="68.3">battre</w> <w n="68.4">mon</w> <w n="68.5">front</w>.</l>
						<l n="69" num="9.5"><w n="69.1">Montez</w>, <w n="69.2">mon</w> <w n="69.3">humble</w> <w n="69.4">courage</w>.</l>
						<l n="70" num="9.6"><w n="70.1">Sous</w> <w n="70.2">les</w> <w n="70.3">insultes</w> <w n="70.4">du</w> <w n="70.5">sort</w> :</l>
						<l n="71" num="9.7"><w n="71.1">J</w>’<w n="71.2">irai</w> <w n="71.3">plus</w> <w n="71.4">haut</w> <w n="71.5">que</w> <w n="71.6">l</w>’<w n="71.7">orage</w></l>
						<l n="72" num="9.8"><w n="72.1">Dans</w> <w n="72.2">les</w> <w n="72.3">ailes</w> <w n="72.4">de</w> <w n="72.5">la</w> <w n="72.6">mort</w> !</l>
					</lg>
				</div></body></text></TEI>