<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES389">
					<head type="main">À MA SŒUR CÉCILE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">orage</w> <w n="1.3">avait</w> <w n="1.4">grondé</w>, <w n="1.5">ma</w> <w n="1.6">tête</w> <w n="1.7">était</w> <w n="1.8">brûlante</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Et</w> <w n="2.2">ma</w> <w n="2.3">tête</w> <w n="2.4">vers</w> <w n="2.5">loi</w> <w n="2.6">se</w> <w n="2.7">tourna</w> <w n="2.8">sans</w> <w n="2.9">effort</w> ;</l>
						<l n="3" num="1.3"><w n="3.1">Tu</w> <w n="3.2">ne</w> <w n="3.3">m</w>’<w n="3.4">avais</w> <w n="3.5">pas</w> <w n="3.6">dit</w> : « <w n="3.7">Je</w> <w n="3.8">veille</w> <w n="3.9">sur</w> <w n="3.10">ton</w> <w n="3.11">sort</w> : »</l>
						<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">l</w>’<w n="4.3">entendis</w> <w n="4.4">en</w> <w n="4.5">moi</w> <w n="4.6">dans</w> <w n="4.7">cette</w> <w n="4.8">heure</w> <w n="4.9">accablante</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Plus</w> <w n="5.2">tard</w>, <w n="5.3">quand</w> <w n="5.4">le</w> <w n="5.5">soleil</w> <w n="5.6">et</w> <w n="5.7">sa</w> <w n="5.8">tendre</w> <w n="5.9">pitié</w></l>
						<l n="6" num="2.2"><w n="6.1">De</w> <w n="6.2">mon</w> <w n="6.3">front</w> <w n="6.4">pâle</w> <w n="6.5">encore</w> <w n="6.6">essuyèrent</w> <w n="6.7">les</w> <w n="6.8">charmes</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Si</w> <w n="7.2">l</w>’<w n="7.3">ombre</w> <w n="7.4">du</w> <w n="7.5">passé</w> <w n="7.6">me</w> <w n="7.7">ramenait</w> <w n="7.8">des</w> <w n="7.9">larmes</w>.</l>
						<l n="8" num="2.4"><w n="8.1">Ta</w> <w n="8.2">tendresse</w> <w n="8.3">fidèle</w> <w n="8.4">en</w> <w n="8.5">prenait</w> <w n="8.6">la</w> <w n="8.7">moitié</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Bientôt</w> <w n="9.2">seule</w>, <w n="9.3">et</w> <w n="9.4">rendue</w> <w n="9.5">au</w> <w n="9.6">vent</w> <w n="9.7">de</w> <w n="9.8">la</w> <w n="9.9">tempête</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Roseau</w> <w n="10.2">toujours</w> <w n="10.3">à</w> <w n="10.4">terre</w> <w n="10.5">et</w> <w n="10.6">toujours</w> <w n="10.7">étonné</w>,</l>
						<l n="11" num="3.3"><w n="11.1">Quand</w> <w n="11.2">tous</w> <w n="11.3">m</w>’<w n="11.4">offraient</w> <w n="11.5">leur</w> <w n="11.6">vie</w> <w n="11.7">en</w> <w n="11.8">courant</w> <w n="11.9">à</w> <w n="11.10">la</w> <w n="11.11">fête</w>.</l>
						<l n="12" num="3.4"><w n="12.1">Tu</w> <w n="12.2">ne</w> <w n="12.3">m</w>’<w n="12.4">offris</w> <w n="12.5">rien</w>, <w n="12.6">toi</w>, <w n="12.7">mais</w> <w n="12.8">tu</w> <w n="12.9">m</w>’<w n="12.10">as</w> <w n="12.11">tout</w> <w n="12.12">donné</w>.</l>
					</lg>
				</div></body></text></TEI>