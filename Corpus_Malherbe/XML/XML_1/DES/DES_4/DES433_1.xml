<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES433">
					<head type="main">LES PROMENEURS</head>
					<lg n="1">
						<l n="1" num="1.1">« <w n="1.1">Pourquoi</w> <w n="1.2">vous</w> <w n="1.3">a</w>-<w n="1.4">t</w>-<w n="1.5">on</w> <w n="1.6">mis</w> <w n="1.7">ce</w> <w n="1.8">casque</w> <w n="1.9">sur</w> <w n="1.10">la</w> <w n="1.11">tête</w> ?</l>
						<l n="2" num="1.2"><w n="2.1">Allez</w>-<w n="2.2">vous</w> <w n="2.3">à</w> <w n="2.4">la</w> <w n="2.5">guerre</w> <w n="2.6">ou</w> <w n="2.7">bien</w> <w n="2.8">dans</w> <w n="2.9">les</w> <w n="2.10">tournois</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">Cet</w> <w n="3.2">appareil</w> <w n="3.3">grillé</w> <w n="3.4">vous</w> <w n="3.5">donne</w> <w n="3.6">un</w> <w n="3.7">air</w> <w n="3.8">sournois</w>.</l>
						<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">vous</w> <w n="4.3">ai</w> <w n="4.4">vu</w> <w n="4.5">moins</w> <w n="4.6">laid</w> <w n="4.7">dans</w> <w n="4.8">nos</w> <w n="4.9">jours</w> <w n="4.10">de</w> <w n="4.11">conquête</w>… »</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">— « <w n="5.1">Mon</w> <w n="5.2">Dieu</w> ! » <w n="5.3">dit</w> <w n="5.4">l</w>’<w n="5.5">autre</w> <w n="5.6">chien</w> (<w n="5.7">c</w>’<w n="5.8">était</w> <w n="5.9">deux</w> <w n="5.10">chiens</w> <w n="5.11">errants</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Cherchant</w> <w n="6.2">aux</w> <w n="6.3">carrefours</w> <w n="6.4">à</w> <w n="6.5">distraire</w> <w n="6.6">leur</w> <w n="6.7">vie</w>),</l>
						<l n="7" num="2.3">« <w n="7.1">Peut</w>-<w n="7.2">on</w>, <w n="7.3">quand</w> <w n="7.4">on</w> <w n="7.5">est</w> <w n="7.6">chien</w>, <w n="7.7">se</w> <w n="7.8">mettre</w> <w n="7.9">à</w> <w n="7.10">son</w> <w n="7.11">envie</w> !</l>
						<l n="8" num="2.4"><w n="8.1">Tout</w> <w n="8.2">maître</w> <w n="8.3">a</w> <w n="8.4">son</w> <w n="8.5">caprice</w> <w n="8.6">et</w> <w n="8.7">nous</w> <w n="8.8">sommes</w> <w n="8.9">aux</w> <w n="8.10">grands</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1">« <w n="9.1">Nous</w> <w n="9.2">leur</w> <w n="9.3">appartenons</w> <w n="9.4">de</w> <w n="9.5">la</w> <w n="9.6">queue</w> <w n="9.7">aux</w> <w n="9.8">oreilles</w> ;</l>
						<l n="10" num="3.2"><w n="10.1">Ce</w> <w n="10.2">qu</w>’<w n="10.3">ils</w> <w n="10.4">en</w> <w n="10.5">font</w>, <w n="10.6">c</w>’<w n="10.7">est</w> <w n="10.8">triste</w>, <w n="10.9">et</w> <w n="10.10">vous</w> <w n="10.11">n</w>’<w n="10.12">avez</w> <w n="10.13">qu</w>’<w n="10.14">à</w> <w n="10.15">voir</w>.</l>
						<l n="11" num="3.3"><w n="11.1">Ils</w> <w n="11.2">ont</w> <w n="11.3">raison</w> <w n="11.4">pourtant</w> <w n="11.5">puisqu</w>’<w n="11.6">ils</w> <w n="11.7">ont</w> <w n="11.8">le</w> <w n="11.9">pouvoir</w>.</l>
						<l n="12" num="3.4"><w n="12.1">N</w>’<w n="12.2">avez</w>-<w n="12.3">vous</w> <w n="12.4">pas</w> <w n="12.5">subi</w> <w n="12.6">des</w> <w n="12.7">justices</w> <w n="12.8">pareilles</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« <w n="13.1">On</w> <w n="13.2">est</w> <w n="13.3">gai</w> <w n="13.4">de</w> <w n="13.5">naissance</w> ; <w n="13.6">eh</w> <w n="13.7">bien</w>, <w n="13.8">on</w> <w n="13.9">ne</w> <w n="13.10">rit</w> <w n="13.11">plus</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Les</w> <w n="14.2">sens</w> <w n="14.3">ainsi</w> <w n="14.4">gênés</w> <w n="14.5">ne</w> <w n="14.6">trouvent</w> <w n="14.7">plus</w> <w n="14.8">leurs</w> <w n="14.9">voies</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Étouffer</w> <w n="15.2">notre</w> <w n="15.3">souffle</w> <w n="15.4">est</w> <w n="15.5">une</w> <w n="15.6">de</w> <w n="15.7">leurs</w> <w n="15.8">joies</w> ;</l>
						<l n="16" num="4.4"><w n="16.1">Ces</w> <w n="16.2">faits</w> <w n="16.3">contre</w> <w n="16.4">nature</w> <w n="16.5">où</w> <w n="16.6">les</w> <w n="16.7">avions</w>-<w n="16.8">nous</w> <w n="16.9">lus</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« <w n="17.1">Venez</w> <w n="17.2">causer</w> <w n="17.3">plus</w> <w n="17.4">loin</w>… <w n="17.5">je</w> <w n="17.6">crois</w> <w n="17.7">qu</w>’<w n="17.8">on</w> <w n="17.9">nous</w> <w n="17.10">regarde</w>.</l>
						<l n="18" num="5.2"><w n="18.1">Nos</w> <w n="18.2">maîtres</w> <w n="18.3">si</w> <w n="18.4">hautains</w> <w n="18.5">sont</w> <w n="18.6">lâchés</w> <w n="18.7">par</w> <w n="18.8">moment</w>.</l>
						<l n="19" num="5.3"><w n="19.1">On</w> <w n="19.2">pourrait</w> <w n="19.3">nous</w> <w n="19.4">traiter</w> <w n="19.5">comme</w> <w n="19.6">un</w> <w n="19.7">rassemblement</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">pour</w> <w n="20.3">nous</w> <w n="20.4">disperser</w> <w n="20.5">faire</w> <w n="20.6">venir</w> <w n="20.7">la</w> <w n="20.8">garde</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« <w n="21.1">Contre</w> <w n="21.2">ce</w> <w n="21.3">lourd</w> <w n="21.4">bonnet</w> <w n="21.5">qui</w> <w n="21.6">n</w>’<w n="21.7">est</w> <w n="21.8">pas</w> <w n="21.9">de</w> <w n="21.10">mon</w> <w n="21.11">goût</w></l>
						<l n="22" num="6.2"><w n="22.1">J</w>’<w n="22.2">ai</w> <w n="22.3">beaucoup</w> <w n="22.4">aboyé</w>, <w n="22.5">mais</w> <w n="22.6">c</w>’<w n="22.7">est</w> <w n="22.8">comme</w> <w n="22.9">qui</w> <w n="22.10">chante</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Tout</w> <w n="23.2">cadenas</w> <w n="23.3">tient</w> <w n="23.4">bon</w> <w n="23.5">sous</w> <w n="23.6">une</w> <w n="23.7">main</w> <w n="23.8">méchante</w> !</l>
						<l n="24" num="6.4"><w n="24.1">Je</w> <w n="24.2">ne</w> <w n="24.3">peux</w> <w n="24.4">plus</w> <w n="24.5">toucher</w>, <w n="24.6">mon</w> <w n="24.7">frère</w>, <w n="24.8">à</w> <w n="24.9">rien</w> <w n="24.10">du</w> <w n="24.11">tout</w> ! »</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Durant</w> <w n="25.2">cet</w> <w n="25.3">entretien</w> <w n="25.4">le</w> <w n="25.5">plus</w> <w n="25.6">libre</w> <w n="25.7">s</w>’<w n="25.8">arrête</w> :</l>
						<l n="26" num="7.2"><w n="26.1">Un</w> <w n="26.2">régal</w> <w n="26.3">imprévu</w> <w n="26.4">l</w>’<w n="26.5">a</w> <w n="26.6">séduit</w> <w n="26.7">en</w> <w n="26.8">marchant</w>.</l>
						<l n="27" num="7.3">— « <w n="27.1">Voyez</w> ! <w n="27.2">l</w>’<w n="27.3">homme</w> <w n="27.4">envers</w> <w n="27.5">nous</w> <w n="27.6">n</w>’<w n="27.7">est</w> <w n="27.8">pas</w> <w n="27.9">toujours</w> <w n="27.10">méchant</w></l>
						<l n="28" num="7.4"><w n="28.1">Il</w> <w n="28.2">jette</w> <w n="28.3">sur</w> <w n="28.4">nos</w> <w n="28.5">pas</w> <w n="28.6">des</w> <w n="28.7">vestiges</w> <w n="28.8">de</w> <w n="28.9">fête</w> !</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« <w n="29.1">Celui</w>-<w n="29.2">ci</w>, <w n="29.3">partagé</w>, <w n="29.4">vous</w> <w n="29.5">remettrait</w> <w n="29.6">le</w> <w n="29.7">cœur</w> ;</l>
						<l n="30" num="8.2"><w n="30.1">Mais</w> <w n="30.2">pour</w> <w n="30.3">thésauriser</w> <w n="30.4">nous</w> <w n="30.5">n</w>’<w n="30.6">avons</w> <w n="30.7">point</w> <w n="30.8">d</w>’<w n="30.9">armoire</w>.</l>
						<l n="31" num="8.3"><w n="31.1">Il</w> <w n="31.2">faut</w> <w n="31.3">vider</w> <w n="31.4">les</w> <w n="31.5">plats</w> <w n="31.6">sans</w> <w n="31.7">payer</w> <w n="31.8">le</w> <w n="31.9">mémoire</w> ;</l>
						<l n="32" num="8.4"><w n="32.1">Nous</w> <w n="32.2">sommes</w> <w n="32.3">à</w> <w n="32.4">la</w> <w n="32.5">chasse</w> <w n="32.6">et</w> <w n="32.7">je</w> <w n="32.8">me</w> <w n="32.9">fais</w> <w n="32.10">piqueur</w> ! »</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Il</w> <w n="33.2">mourut</w>, <w n="33.3">car</w> <w n="33.4">la</w> <w n="33.5">fête</w> <w n="33.6">était</w> <w n="33.7">empoisonnée</w>.</l>
						<l n="34" num="9.2"><w n="34.1">Ô</w> <w n="34.2">mémoire</w> <w n="34.3">flottante</w> ! <w n="34.4">Ô</w> <w n="34.5">candeur</w> <w n="34.6">des</w> <w n="34.7">petits</w> !</l>
						<l n="35" num="9.3"><w n="35.1">Ô</w> <w n="35.2">perfides</w> <w n="35.3">éveils</w> <w n="35.4">d</w>’<w n="35.5">incessants</w> <w n="35.6">appétits</w> !</l>
						<l n="36" num="9.4"><w n="36.1">Ô</w> <w n="36.2">vie</w> <w n="36.3">à</w> <w n="36.4">tout</w> <w n="36.5">propos</w> <w n="36.6">dans</w> <w n="36.7">ta</w> <w n="36.8">fleur</w> <w n="36.9">moissonnée</w> !</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">L</w>’<w n="37.2">empoisonneur</w> <w n="37.3">sifflait</w>, <w n="37.4">écorchant</w> <w n="37.5">sans</w> <w n="37.6">remords</w></l>
						<l n="38" num="10.2"><w n="38.1">Le</w> <w n="38.2">chien</w>, <w n="38.3">bon</w> <w n="38.4">pour</w> <w n="38.5">des</w> <w n="38.6">gants</w>. <w n="38.7">Sous</w> <w n="38.8">son</w> <w n="38.9">casque</w>, <w n="38.10">et</w> <w n="38.11">plus</w> <w n="38.12">sombre</w>,</l>
						<l n="39" num="10.3"><w n="39.1">L</w>’<w n="39.2">autre</w> <w n="39.3">disait</w> <w n="39.4">tout</w> <w n="39.5">bas</w>, <w n="39.6">trottant</w> <w n="39.7">seul</w> <w n="39.8">et</w> <w n="39.9">dans</w> <w n="39.10">l</w>’<w n="39.11">ombre</w> :</l>
						<l n="40" num="10.4">«<w n="40.1">Heureux</w> <w n="40.2">les</w> <w n="40.3">muselés</w> !… <w n="40.4">Mais</w> <w n="40.5">plus</w> <w n="40.6">heureux</w> <w n="40.7">les</w> <w n="40.8">morts</w> !»</l>
					</lg>
				</div></body></text></TEI>