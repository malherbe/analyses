<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES413">
					<head type="main">POUR ENDORMIR L’ENFANT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Ah</w> ! <w n="1.2">si</w> <w n="1.3">j</w>’<w n="1.4">étais</w> <w n="1.5">le</w> <w n="1.6">cher</w> <w n="1.7">petit</w> <w n="1.8">enfant</w></l>
						<l n="2" num="1.2"><w n="2.1">Qu</w>’<w n="2.2">on</w> <w n="2.3">aime</w> <w n="2.4">bien</w>, <w n="2.5">mais</w> <w n="2.6">qui</w> <w n="2.7">pleure</w> <w n="2.8">souvent</w>,</l>
						<l n="3" num="1.3"><space quantity="8" unit="char"></space><w n="3.1">Gai</w> <w n="3.2">comme</w> <w n="3.3">un</w> <w n="3.4">charme</w>,</l>
						<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Sans</w> <w n="4.2">une</w> <w n="4.3">larme</w>,</l>
						<l n="5" num="1.5"><w n="5.1">J</w>’<w n="5.2">écouterais</w> <w n="5.3">chanter</w> <w n="5.4">l</w>’<w n="5.5">heure</w> <w n="5.6">et</w> <w n="5.7">le</w> <w n="5.8">vent</w>…</l>
						<l n="6" num="1.6">(<w n="6.1">Je</w> <w n="6.2">dis</w> <w n="6.3">cela</w> <w n="6.4">pour</w> <w n="6.5">le</w> <w n="6.6">petit</w> <w n="6.7">enfant</w>).</l>
					</lg>
					<lg n="2">
						<l n="7" num="2.1"><w n="7.1">Si</w> <w n="7.2">je</w> <w n="7.3">logeais</w> <w n="7.4">dans</w> <w n="7.5">ce</w> <w n="7.6">mouvant</w> <w n="7.7">berceau</w>,</l>
						<l n="8" num="2.2"><w n="8.1">Pour</w> <w n="8.2">mériter</w> <w n="8.3">qu</w>’<w n="8.4">on</w> <w n="8.5">m</w>’<w n="8.6">apporte</w> <w n="8.7">un</w> <w n="8.8">cerceau</w>,</l>
						<l n="9" num="2.3"><space quantity="8" unit="char"></space><w n="9.1">Je</w> <w n="9.2">serais</w> <w n="9.3">sage</w></l>
						<l n="10" num="2.4"><space quantity="8" unit="char"></space><w n="10.1">Comme</w> <w n="10.2">une</w> <w n="10.3">image</w>,</l>
						<l n="11" num="2.5"><w n="11.1">Et</w> <w n="11.2">je</w> <w n="11.3">ferais</w> <w n="11.4">moins</w> <w n="11.5">de</w> <w n="11.6">bruit</w> <w n="11.7">qu</w>’<w n="11.8">un</w> <w n="11.9">oiseau</w>…</l>
						<l n="12" num="2.6">(<w n="12.1">Je</w> <w n="12.2">dis</w> <w n="12.3">cela</w> <w n="12.4">pour</w> <w n="12.5">l</w>’<w n="12.6">enfant</w> <w n="12.7">du</w> <w n="12.8">berceau</w>).</l>
					</lg>
					<lg n="3">
						<l n="13" num="3.1"><w n="13.1">Ah</w> ! <w n="13.2">si</w> <w n="13.3">j</w>’<w n="13.4">étais</w> <w n="13.5">notre</w> <w n="13.6">blanc</w> <w n="13.7">nourrisson</w>,</l>
						<l n="14" num="3.2"><w n="14.1">Pour</w> <w n="14.2">qui</w> <w n="14.3">je</w> <w n="14.4">fais</w> <w n="14.5">cette</w> <w n="14.6">belle</w> <w n="14.7">chanson</w>,</l>
						<l n="15" num="3.3"><space quantity="8" unit="char"></space><w n="15.1">Tranquille</w> <w n="15.2">à</w> <w n="15.3">l</w>’<w n="15.4">ombre</w>,</l>
						<l n="16" num="3.4"><space quantity="8" unit="char"></space><w n="16.1">Comme</w> <w n="16.2">au</w> <w n="16.3">bois</w> <w n="16.4">sombre</w>,</l>
						<l n="17" num="3.5"><w n="17.1">Je</w> <w n="17.2">rêverais</w> <w n="17.3">que</w> <w n="17.4">j</w>’<w n="17.5">entends</w> <w n="17.6">le</w> <w n="17.7">pinson</w>…</l>
						<l n="18" num="3.6">(<w n="18.1">Je</w> <w n="18.2">dis</w> <w n="18.3">cela</w> <w n="18.4">pour</w> <w n="18.5">le</w> <w n="18.6">blanc</w> <w n="18.7">nourrisson</w>).</l>
					</lg>
					<lg n="4">
						<l n="19" num="4.1"><w n="19.1">Ah</w> ! <w n="19.2">si</w> <w n="19.3">j</w>’<w n="19.4">étais</w> <w n="19.5">l</w>’<w n="19.6">ami</w> <w n="19.7">des</w> <w n="19.8">blancs</w> <w n="19.9">poussins</w></l>
						<l n="20" num="4.2"><w n="20.1">Dormant</w> <w n="20.2">entre</w> <w n="20.3">eux</w>, <w n="20.4">doux</w> <w n="20.5">et</w> <w n="20.6">vivants</w> <w n="20.7">coussins</w></l>
						<l n="21" num="4.3"><space quantity="8" unit="char"></space><w n="21.1">Sans</w> <w n="21.2">que</w> <w n="21.3">je</w> <w n="21.4">pleure</w>,</l>
						<l n="22" num="4.4"><space quantity="8" unit="char"></space><w n="22.1">J</w>’<w n="22.2">irais</w> <w n="22.3">sur</w> <w n="22.4">l</w>’<w n="22.5">heure</w></l>
						<l n="23" num="4.5"><w n="23.1">Faire</w> <w n="23.2">chorus</w> <w n="23.3">avec</w> <w n="23.4">ces</w> <w n="23.5">petits</w> <w n="23.6">saints</w>…</l>
						<l n="24" num="4.6">(<w n="24.1">Je</w> <w n="24.2">dis</w> <w n="24.3">cela</w> <w n="24.4">pour</w> <w n="24.5">l</w>’<w n="24.6">ami</w> <w n="24.7">des</w> <w n="24.8">poussins</w>).</l>
					</lg>
					<lg n="5">
						<l n="25" num="5.1"><w n="25.1">Si</w> <w n="25.2">le</w> <w n="25.3">cheval</w> <w n="25.4">demandait</w> <w n="25.5">à</w> <w n="25.6">nous</w> <w n="25.7">voir</w>,</l>
						<l n="26" num="5.2"><w n="26.1">Riant</w> <w n="26.2">d</w>’<w n="26.3">aller</w> <w n="26.4">nager</w> <w n="26.5">à</w> <w n="26.6">l</w>’<w n="26.7">abreuvoir</w>,</l>
						<l n="27" num="5.3"><space quantity="8" unit="char"></space><w n="27.1">Fermant</w> <w n="27.2">le</w> <w n="27.3">gîte</w>,</l>
						<l n="28" num="5.4"><space quantity="8" unit="char"></space><w n="28.1">Je</w> <w n="28.2">crîrais</w> <w n="28.3">vite</w> :</l>
						<l n="29" num="5.5">« <w n="29.1">Demain</w> <w n="29.2">l</w>’<w n="29.3">enfant</w> <w n="29.4">pourra</w> <w n="29.5">vous</w> <w n="29.6">recevoir</w> »…</l>
						<l n="30" num="5.6">(<w n="30.1">Je</w> <w n="30.2">dis</w> <w n="30.3">cela</w> <w n="30.4">pour</w> <w n="30.5">l</w>’<w n="30.6">enfant</w> <w n="30.7">qu</w>’<w n="30.8">il</w> <w n="30.9">vient</w> <w n="30.10">voir</w>).</l>
					</lg>
					<lg n="6">
						<l n="31" num="6.1"><w n="31.1">Si</w> <w n="31.2">j</w>’<w n="31.3">entendais</w> <w n="31.4">les</w> <w n="31.5">loups</w> <w n="31.6">hurler</w> <w n="31.7">dehors</w>,</l>
						<l n="32" num="6.2"><w n="32.1">Bien</w> <w n="32.2">défendu</w> <w n="32.3">par</w> <w n="32.4">les</w> <w n="32.5">grands</w> <w n="32.6">et</w> <w n="32.7">les</w> <w n="32.8">forts</w>,</l>
						<l n="33" num="6.3"><space quantity="8" unit="char"></space><w n="33.1">Fier</w> <w n="33.2">comme</w> <w n="33.3">un</w> <w n="33.4">homme</w></l>
						<l n="34" num="6.4"><space quantity="8" unit="char"></space><w n="34.1">Qui</w> <w n="34.2">fait</w> <w n="34.3">un</w> <w n="34.4">somme</w>,</l>
						<l n="35" num="6.5"><w n="35.1">Je</w> <w n="35.2">répondrais</w> : « <w n="35.3">Passez</w>, <w n="35.4">Messieurs</w>, <w n="35.5">je</w> <w n="35.6">dors</w> !…»</l>
						<l n="36" num="6.6">(<w n="36.1">Je</w> <w n="36.2">dis</w> <w n="36.3">cela</w> <w n="36.4">pour</w> <w n="36.5">les</w> <w n="36.6">loups</w> <w n="36.7">du</w> <w n="36.8">dehors</w>)</l>
					</lg>
					<lg n="7">
						<l n="37" num="7.1"><w n="37.1">On</w> <w n="37.2">n</w>’<w n="37.3">entendit</w> <w n="37.4">plus</w> <w n="37.5">rien</w> <w n="37.6">dans</w> <w n="37.7">la</w> <w n="37.8">maison</w></l>
						<l n="38" num="7.2"><w n="38.1">Ni</w> <w n="38.2">le</w> <w n="38.3">rouet</w>, <w n="38.4">ni</w> <w n="38.5">l</w>’<w n="38.6">égale</w> <w n="38.7">chanson</w> ;</l>
						<l n="39" num="7.3"><space quantity="8" unit="char"></space><w n="39.1">La</w> <w n="39.2">mère</w> <w n="39.3">ardente</w></l>
						<l n="40" num="7.4"><space quantity="8" unit="char"></space><w n="40.1">Fine</w> <w n="40.2">et</w> <w n="40.3">prudente</w>,</l>
						<l n="41" num="7.5"><w n="41.1">Fit</w> <w n="41.2">rendormie</w> <w n="41.3">auprès</w> <w n="41.4">de</w> <w n="41.5">la</w> <w n="41.6">cloison</w>,</l>
						<l n="42" num="7.6"><w n="42.1">Et</w> <w n="42.2">suspendit</w> <w n="42.3">tout</w> <w n="42.4">bruit</w> <w n="42.5">dans</w> <w n="42.6">la</w> <w n="42.7">maison</w>.</l>
					</lg>
					<p>(Sur une mélodie allemande.)</p>
				</div></body></text></TEI>