<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES382">
					<head type="main">UN RUISSEAU DE LA SCARPE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Oui</w>, <w n="1.2">j</w>’<w n="1.3">avais</w> <w n="1.4">des</w> <w n="1.5">trésors</w>… <w n="1.6">j</w>’<w n="1.7">en</w> <w n="1.8">ai</w> <w n="1.9">plein</w> <w n="1.10">ma</w> <w n="1.11">mémoire</w>.</l>
						<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2">ai</w> <w n="2.3">des</w> <w n="2.4">banquets</w> <w n="2.5">rêvés</w> <w n="2.6">où</w> <w n="2.7">l</w>’<w n="2.8">orphelin</w> <w n="2.9">va</w> <w n="2.10">boire</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Oh</w> ! <w n="3.2">quel</w> <w n="3.3">enfant</w> <w n="3.4">des</w> <w n="3.5">bleds</w>, <w n="3.6">le</w> <w n="3.7">long</w> <w n="3.8">des</w> <w n="3.9">chemins</w> <w n="3.10">verts</w>,</l>
						<l n="4" num="1.4"><w n="4.1">N</w>’<w n="4.2">a</w>, <w n="4.3">dans</w> <w n="4.4">ses</w> <w n="4.5">jeux</w> <w n="4.6">errants</w>, <w n="4.7">possédé</w> <w n="4.8">l</w>’<w n="4.9">univers</w> ?</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Emmenez</w>-<w n="5.2">moi</w>, <w n="5.3">chemins</w> !…<w n="5.4">Mais</w> <w n="5.5">non</w>, <w n="5.6">ce</w> <w n="5.7">n</w>’<w n="5.8">est</w> <w n="5.9">plus</w> <w n="5.10">l</w>’<w n="5.11">heure</w>,</l>
						<l n="6" num="2.2"><w n="6.1">Il</w> <w n="6.2">faudrait</w> <w n="6.3">revenir</w> <w n="6.4">en</w> <w n="6.5">courant</w> <w n="6.6">où</w> <w n="6.7">l</w>’<w n="6.8">on</w> <w n="6.9">pleure</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Sans</w> <w n="7.2">avoir</w> <w n="7.3">regardé</w> <w n="7.4">jusqu</w>’<w n="7.5">au</w> <w n="7.6">fond</w> <w n="7.7">le</w> <w n="7.8">ruisseau</w></l>
						<l n="8" num="2.4"><w n="8.1">Dont</w> <w n="8.2">la</w> <w n="8.3">vague</w> <w n="8.4">mouilla</w> <w n="8.5">l</w>’<w n="8.6">osier</w> <w n="8.7">de</w> <w n="8.8">mon</w> <w n="8.9">berceau</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">courait</w> <w n="9.3">vers</w> <w n="9.4">la</w> <w n="9.5">Scarpe</w> <w n="9.6">en</w> <w n="9.7">traversant</w> <w n="9.8">nos</w> <w n="9.9">rues</w></l>
						<l n="10" num="3.2"><w n="10.1">Qu</w>’<w n="10.2">épurait</w> <w n="10.3">la</w> <w n="10.4">fraîcheur</w> <w n="10.5">de</w> <w n="10.6">ses</w> <w n="10.7">ondes</w> <w n="10.8">accrues</w> ;</l>
						<l n="11" num="3.3"><w n="11.1">Et</w> <w n="11.2">l</w>’<w n="11.3">enfance</w> <w n="11.4">aux</w> <w n="11.5">longs</w> <w n="11.6">cris</w> <w n="11.7">saluait</w> <w n="11.8">son</w> <w n="11.9">retour</w></l>
						<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">faisait</w> <w n="12.3">déborder</w> <w n="12.4">tous</w> <w n="12.5">les</w> <w n="12.6">puits</w> <w n="12.7">d</w>’<w n="12.8">alentour</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Écoliers</w> <w n="13.2">de</w> <w n="13.3">ce</w> <w n="13.4">temps</w>, <w n="13.5">troupe</w> <w n="13.6">alerte</w> <w n="13.7">et</w> <w n="13.8">bruyante</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Où</w> <w n="14.2">sont</w>-<w n="14.3">ils</w> <w n="14.4">vos</w> <w n="14.5">présents</w> <w n="14.6">jetés</w> <w n="14.7">à</w> <w n="14.8">l</w>’<w n="14.9">eau</w> <w n="14.10">fuyante</w> ?</l>
						<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">livre</w> <w n="15.3">ouvert</w>, <w n="15.4">parfois</w> <w n="15.5">vos</w> <w n="15.6">souliers</w> <w n="15.7">pour</w> <w n="15.8">vaisseaux</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Et</w> <w n="16.2">vos</w> <w n="16.3">petits</w> <w n="16.4">jardins</w> <w n="16.5">de</w> <w n="16.6">mousse</w> <w n="16.7">et</w> <w n="16.8">d</w>’<w n="16.9">arbrisseaux</w> ?</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Air</w> <w n="17.2">natal</w> ! <w n="17.3">aliment</w> <w n="17.4">de</w> <w n="17.5">saveur</w> <w n="17.6">sans</w> <w n="17.7">seconde</w>,</l>
						<l n="18" num="5.2"><w n="18.1">Qui</w> <w n="18.2">nourris</w> <w n="18.3">tes</w> <w n="18.4">enfants</w> <w n="18.5">et</w> <w n="18.6">les</w> <w n="18.7">baise</w> <w n="18.8">à</w> <w n="18.9">la</w> <w n="18.10">ronde</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Air</w> <w n="19.2">natal</w> <w n="19.3">imprégné</w> <w n="19.4">des</w> <w n="19.5">souffles</w> <w n="19.6">de</w> <w n="19.7">nos</w> <w n="19.8">champs</w>,</l>
						<l n="20" num="5.4"><w n="20.1">Qui</w> <w n="20.2">fais</w> <w n="20.3">les</w> <w n="20.4">cœurs</w> <w n="20.5">pareils</w> <w n="20.6">et</w> <w n="20.7">pareils</w> <w n="20.8">les</w> <w n="20.9">penchants</w> !</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">la</w> <w n="21.3">longue</w> <w n="21.4">innocence</w>, <w n="21.5">et</w> <w n="21.6">le</w> <w n="21.7">joyeux</w> <w n="21.8">sourire</w></l>
						<l n="22" num="6.2"><w n="22.1">Des</w> <w n="22.2">nôtres</w>, <w n="22.3">qui</w> <w n="22.4">n</w>’<w n="22.5">ont</w> <w n="22.6">pas</w> <w n="22.7">de</w> <w n="22.8">plus</w> <w n="22.9">beau</w> <w n="22.10">livre</w> <w n="22.11">à</w> <w n="22.12">lire</w></l>
						<l n="23" num="6.3"><w n="23.1">Que</w> <w n="23.2">leur</w> <w n="23.3">visage</w> <w n="23.4">ouvert</w> <w n="23.5">et</w> <w n="23.6">leurs</w> <w n="23.7">grands</w> <w n="23.8">yeux</w> <w n="23.9">d</w>’<w n="23.10">azur</w>,</l>
						<l n="24" num="6.4"><w n="24.1">Et</w> <w n="24.2">leur</w> <w n="24.3">timbre</w> <w n="24.4">profond</w> <w n="24.5">d</w>’<w n="24.6">où</w> <w n="24.7">sort</w> <w n="24.8">l</w>’<w n="24.9">entretien</w> <w n="24.10">sûr</w> !</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1"><w n="25.1">Depuis</w> <w n="25.2">que</w> <w n="25.3">j</w>’<w n="25.4">ai</w> <w n="25.5">quitté</w> <w n="25.6">tes</w> <w n="25.7">haleines</w> <w n="25.8">bénies</w>.</l>
						<l n="26" num="7.2"><w n="26.1">Tes</w> <w n="26.2">familles</w> <w n="26.3">aux</w> <w n="26.4">mains</w> <w n="26.5">facilement</w> <w n="26.6">unies</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Je</w> <w n="27.2">ne</w> <w n="27.3">sais</w> <w n="27.4">quoi</w> <w n="27.5">d</w>’<w n="27.6">amer</w> <w n="27.7">à</w> <w n="27.8">mon</w> <w n="27.9">pain</w> <w n="27.10">s</w>’<w n="27.11">est</w> <w n="27.12">mêlé</w>.</l>
						<l n="28" num="7.4"><w n="28.1">Et</w> <w n="28.2">partout</w> <w n="28.3">sur</w> <w n="28.4">mon</w> <w n="28.5">jour</w> <w n="28.6">une</w> <w n="28.7">larme</w> <w n="28.8">a</w> <w n="28.9">tremblé</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1"><w n="29.1">Et</w> <w n="29.2">je</w> <w n="29.3">n</w>’<w n="29.4">ai</w> <w n="29.5">plus</w> <w n="29.6">osé</w> <w n="29.7">vivre</w> <w n="29.8">à</w> <w n="29.9">poitrine</w> <w n="29.10">pleine</w></l>
						<l n="30" num="8.2"><w n="30.1">Ni</w> <w n="30.2">respirer</w> <w n="30.3">tout</w> <w n="30.4">l</w>’<w n="30.5">air</w> <w n="30.6">qu</w>’<w n="30.7">il</w> <w n="30.8">faut</w> <w n="30.9">à</w> <w n="30.10">mon</w> <w n="30.11">haleine</w>.</l>
						<l n="31" num="8.3"><w n="31.1">On</w> <w n="31.2">eût</w> <w n="31.3">dit</w> <w n="31.4">qu</w>’<w n="31.5">un</w> <w n="31.6">témoin</w> <w n="31.7">s</w>’<w n="31.8">y</w> <w n="31.9">serait</w> <w n="31.10">opposé</w>…</l>
						<l n="32" num="8.4"><w n="32.1">Vivre</w> <w n="32.2">pour</w> <w n="32.3">vivre</w>, <w n="32.4">oh</w> <w n="32.5">non</w> ! <w n="32.6">je</w> <w n="32.7">ne</w> <w n="32.8">l</w>’<w n="32.9">ai</w> <w n="32.10">plus</w> <w n="32.11">osé</w> !</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1"><w n="33.1">Non</w>, <w n="33.2">le</w> <w n="33.3">cher</w> <w n="33.4">souvenir</w> <w n="33.5">n</w>’<w n="33.6">est</w> <w n="33.7">qu</w>’<w n="33.8">un</w> <w n="33.9">cri</w> <w n="33.10">de</w> <w n="33.11">souffrance</w> !</l>
						<l n="34" num="9.2"><w n="34.1">Viens</w> <w n="34.2">donc</w>, <w n="34.3">toi</w>, <w n="34.4">dont</w> <w n="34.5">le</w> <w n="34.6">cours</w> <w n="34.7">peut</w> <w n="34.8">traverser</w> <w n="34.9">la</w> <w n="34.10">France</w> ;</l>
						<l n="35" num="9.3"><w n="35.1">À</w> <w n="35.2">ta</w> <w n="35.3">molle</w> <w n="35.4">clarté</w> <w n="35.5">je</w> <w n="35.6">livrerai</w> <w n="35.7">mon</w> <w n="35.8">front</w>,</l>
						<l n="36" num="9.4"><w n="36.1">Et</w> <w n="36.2">dans</w> <w n="36.3">tes</w> <w n="36.4">flots</w> <w n="36.5">du</w> <w n="36.6">moins</w> <w n="36.7">mes</w> <w n="36.8">larmes</w> <w n="36.9">se</w> <w n="36.10">perdront</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1"><w n="37.1">Viens</w> <w n="37.2">ranimer</w> <w n="37.3">le</w> <w n="37.4">cœur</w> <w n="37.5">séché</w> <w n="37.6">de</w> <w n="37.7">nostalgie</w>,</l>
						<l n="38" num="10.2"><w n="38.1">Le</w> <w n="38.2">prendre</w>, <w n="38.3">et</w> <w n="38.4">l</w>’<w n="38.5">inonder</w> <w n="38.6">d</w>’<w n="38.7">une</w> <w n="38.8">fraîche</w> <w n="38.9">énergie</w>.</l>
						<l n="39" num="10.3"><w n="39.1">En</w> <w n="39.2">sortant</w> <w n="39.3">d</w>’<w n="39.4">abreuver</w> <w n="39.5">l</w>’<w n="39.6">herbe</w> <w n="39.7">de</w> <w n="39.8">nos</w> <w n="39.9">guérets</w>,</l>
						<l n="40" num="10.4"><w n="40.1">Viens</w>, <w n="40.2">ne</w> <w n="40.3">fût</w>-<w n="40.4">ce</w> <w n="40.5">qu</w>’<w n="40.6">une</w> <w n="40.7">heure</w>, <w n="40.8">abreuver</w> <w n="40.9">mes</w> <w n="40.10">regrets</w> !</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Amène</w> <w n="41.2">avec</w> <w n="41.3">ton</w> <w n="41.4">bruit</w> <w n="41.5">une</w> <w n="41.6">de</w> <w n="41.7">nos</w> <w n="41.8">abeilles</w></l>
						<l n="42" num="11.2"><w n="42.1">Dont</w> <w n="42.2">l</w>’<w n="42.3">essaim</w>, <w n="42.4">quoique</w> <w n="42.5">absent</w>, <w n="42.6">bourdonne</w> <w n="42.7">en</w> <w n="42.8">mes</w> <w n="42.9">oreilles</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Elle</w> <w n="43.2">en</w> <w n="43.3">parle</w> <w n="43.4">toujours</w> ! <w n="43.5">diront</w>-<w n="43.6">ils</w>… <w n="43.7">Mais</w>, <w n="43.8">mon</w> <w n="43.9">Dieu</w>,</l>
						<l n="44" num="11.4"><w n="44.1">Jeune</w>, <w n="44.2">on</w> <w n="44.3">a</w> <w n="44.4">tant</w> <w n="44.5">aimé</w> <w n="44.6">ces</w> <w n="44.7">parcelles</w> <w n="44.8">de</w> <w n="44.9">feu</w> !</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1"><w n="45.1">Ces</w> <w n="45.2">gouttes</w> <w n="45.3">de</w> <w n="45.4">soleil</w> <w n="45.5">dans</w> <w n="45.6">notre</w> <w n="45.7">azur</w> <w n="45.8">qui</w> <w n="45.9">brille</w>.</l>
						<l n="46" num="12.2"><w n="46.1">Dansant</w> <w n="46.2">sur</w> <w n="46.3">le</w> <w n="46.4">tableau</w> <w n="46.5">lointain</w> <w n="46.6">de</w> <w n="46.7">la</w> <w n="46.8">famille</w>,</l>
						<l n="47" num="12.3"><w n="47.1">Visiteuses</w> <w n="47.2">des</w> <w n="47.3">bleds</w> <w n="47.4">où</w> <w n="47.5">logent</w> <w n="47.6">tant</w> <w n="47.7">de</w> <w n="47.8">fleurs</w>.</l>
						<l n="48" num="12.4"><w n="48.1">Miel</w> <w n="48.2">qui</w> <w n="48.3">vole</w> <w n="48.4">émané</w> <w n="48.5">des</w> <w n="48.6">célestes</w> <w n="48.7">chaleurs</w> !</l>
					</lg>
					<lg n="13">
						<l n="49" num="13.1"><w n="49.1">J</w>’<w n="49.2">en</w> <w n="49.3">ai</w> <w n="49.4">tant</w> <w n="49.5">vu</w> <w n="49.6">passer</w> <w n="49.7">dans</w> <w n="49.8">l</w>’<w n="49.9">enclos</w> <w n="49.10">de</w> <w n="49.11">mon</w> <w n="49.12">père</w></l>
						<l n="50" num="13.2"><w n="50.1">Qu</w>’<w n="50.2">il</w> <w n="50.3">en</w> <w n="50.4">fourmille</w> <w n="50.5">au</w> <w n="50.6">fond</w> <w n="50.7">de</w> <w n="50.8">tout</w> <w n="50.9">ce</w> <w n="50.10">que</w> <w n="50.11">j</w>’<w n="50.12">espère</w> ;</l>
						<l n="51" num="13.3"><w n="51.1">Sur</w> <w n="51.2">toi</w> <w n="51.3">dont</w> <w n="51.4">l</w>’<w n="51.5">eau</w> <w n="51.6">rapide</w> <w n="51.7">a</w> <w n="51.8">délecté</w> <w n="51.9">mes</w> <w n="51.10">jours</w>.</l>
						<l n="52" num="13.4"><w n="52.1">Et</w> <w n="52.2">m</w>’<w n="52.3">a</w> <w n="52.4">fait</w> <w n="52.5">cette</w> <w n="52.6">voix</w> <w n="52.7">qui</w> <w n="52.8">soupire</w> <w n="52.9">toujours</w>.</l>
					</lg>
					<lg n="14">
						<l n="53" num="14.1"><w n="53.1">Dans</w> <w n="53.2">ce</w> <w n="53.3">poignant</w> <w n="53.4">amour</w> <w n="53.5">que</w> <w n="53.6">je</w> <w n="53.7">m</w>’<w n="53.8">efforce</w> <w n="53.9">à</w> <w n="53.10">rendre</w>,</l>
						<l n="54" num="14.2"><w n="54.1">Dont</w> <w n="54.2">j</w>’<w n="54.3">ai</w> <w n="54.4">souffert</w> <w n="54.5">longtemps</w> <w n="54.6">avant</w> <w n="54.7">de</w> <w n="54.8">le</w> <w n="54.9">comprendre</w>,</l>
						<l n="55" num="14.3"><w n="55.1">Comme</w> <w n="55.2">d</w>’<w n="55.3">un</w> <w n="55.4">pâle</w> <w n="55.5">enfant</w> <w n="55.6">on</w> <w n="55.7">berce</w> <w n="55.8">le</w> <w n="55.9">souci</w>,</l>
						<l n="56" num="14.4"><w n="56.1">Ruisseau</w>, <w n="56.2">tu</w> <w n="56.3">me</w> <w n="56.4">rendrais</w> <w n="56.5">ce</w> <w n="56.6">qui</w> <w n="56.7">me</w> <w n="56.8">manque</w> <w n="56.9">ici</w>.</l>
					</lg>
					<lg n="15">
						<l n="57" num="15.1"><w n="57.1">Ton</w> <w n="57.2">bruit</w> <w n="57.3">sourd</w>, <w n="57.4">se</w> <w n="57.5">mêlant</w> <w n="57.6">au</w> <w n="57.7">rouet</w> <w n="57.8">de</w> <w n="57.9">ma</w> <w n="57.10">mère</w>,</l>
						<l n="58" num="15.2"><w n="58.1">Enlevant</w> <w n="58.2">à</w> <w n="58.3">son</w> <w n="58.4">cœur</w> <w n="58.5">quelque</w> <w n="58.6">pensée</w> <w n="58.7">amère</w>,</l>
						<l n="59" num="15.3"><w n="59.1">Quand</w> <w n="59.2">pour</w> <w n="59.3">nous</w> <w n="59.4">le</w> <w n="59.5">donner</w> <w n="59.6">elle</w> <w n="59.7">cherchait</w> <w n="59.8">là</w>-<w n="59.9">bas</w></l>
						<l n="60" num="15.4"><w n="60.1">Un</w> <w n="60.2">bonheur</w> <w n="60.3">attardé</w> <w n="60.4">qui</w> <w n="60.5">ne</w> <w n="60.6">revenait</w> <w n="60.7">pas</w>.</l>
					</lg>
					<lg n="16">
						<l n="61" num="16.1"><w n="61.1">Cette</w> <w n="61.2">mère</w>, <w n="61.3">à</w> <w n="61.4">ta</w> <w n="61.5">rive</w> <w n="61.6">elle</w> <w n="61.7">est</w> <w n="61.8">assise</w> <w n="61.9">encore</w> ;</l>
						<l n="62" num="16.2"><w n="62.1">La</w> <w n="62.2">voilà</w> <w n="62.3">qui</w> <w n="62.4">me</w> <w n="62.5">parle</w>, <w n="62.6">ô</w> <w n="62.7">mémoire</w> <w n="62.8">sonore</w> !</l>
						<l n="63" num="16.3"><w n="63.1">Ô</w> <w n="63.2">mes</w> <w n="63.3">palais</w> <w n="63.4">natals</w> <w n="63.5">qu</w>’<w n="63.6">on</w> <w n="63.7">m</w>’<w n="63.8">a</w> <w n="63.9">fermés</w> <w n="63.10">souvent</w> !</l>
						<l n="64" num="16.4"><w n="64.1">La</w> <w n="64.2">voilà</w> <w n="64.3">qui</w> <w n="64.4">les</w> <w n="64.5">rouvre</w> <w n="64.6">à</w> <w n="64.7">son</w> <w n="64.8">heureux</w> <w n="64.9">enfant</w> !</l>
					</lg>
					<lg n="17">
						<l n="65" num="17.1"><w n="65.1">Je</w> <w n="65.2">ressaisis</w> <w n="65.3">sa</w> <w n="65.4">robe</w>, <w n="65.5">et</w> <w n="65.6">ses</w> <w n="65.7">mains</w>, <w n="65.8">et</w> <w n="65.9">son</w> <w n="65.10">âme</w> !</l>
						<l n="66" num="17.2"><w n="66.1">Sur</w> <w n="66.2">ma</w> <w n="66.3">lèvre</w> <w n="66.4">entr</w>’<w n="66.5">ouverte</w> <w n="66.6">elle</w> <w n="66.7">répand</w> <w n="66.8">sa</w> <w n="66.9">flamme</w> !</l>
						<l n="67" num="17.3"><w n="67.1">Non</w> ! <w n="67.2">par</w> <w n="67.3">tout</w> <w n="67.4">l</w>’<w n="67.5">or</w> <w n="67.6">du</w> <w n="67.7">monde</w> <w n="67.8">on</w> <w n="67.9">ne</w> <w n="67.10">me</w> <w n="67.11">paîrait</w> <w n="67.12">pas</w></l>
						<l n="68" num="17.4"><w n="68.1">Ce</w> <w n="68.2">souffle</w>, <w n="68.3">ce</w> <w n="68.4">ruisseau</w> <w n="68.5">qui</w> <w n="68.6">font</w> <w n="68.7">trembler</w> <w n="68.8">mes</w> <w n="68.9">pas</w> !</l>
					</lg>
				</div></body></text></TEI>