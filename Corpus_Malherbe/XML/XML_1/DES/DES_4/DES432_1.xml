<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES432">
					<head type="main">RENCONTRE D’UNE CHÈVRE ET D’UNE BREBIS</head>
					<lg n="1">
						<l n="1" num="1.1">« <w n="1.1">Pardon</w> ! <w n="1.2">n</w>’<w n="1.3">est</w>-<w n="1.4">ce</w> <w n="1.5">pas</w> <w n="1.6">vous</w> <w n="1.7">que</w> <w n="1.8">j</w>’<w n="1.9">ai</w> <w n="1.10">vue</w> <w n="1.11">une</w> <w n="1.12">fois</w> ? »</l>
						<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">Dit</w>, <w n="2.2">en</w> <w n="2.3">faisant</w> <w n="2.4">la</w> <w n="2.5">révérence</w>,</l>
						<l n="3" num="1.3"><w n="3.1">La</w> <w n="3.2">chèvre</w> <w n="3.3">à</w> <w n="3.4">la</w> <w n="3.5">brebis</w> <w n="3.6">de</w> <w n="3.7">chétive</w> <w n="3.8">apparence</w>,</l>
						<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">Liée</w> <w n="4.2">et</w> <w n="4.3">seule</w> <w n="4.4">au</w> <w n="4.5">bord</w> <w n="4.6">d</w>’<w n="4.7">un</w> <w n="4.8">bois</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1">« <w n="5.1">Vous</w> <w n="5.2">étiez</w>, <w n="5.3">si</w> <w n="5.4">c</w>’<w n="5.5">est</w> <w n="5.6">vous</w>, <w n="5.7">si</w> <w n="5.8">charmante</w> <w n="5.9">et</w> <w n="5.10">si</w> <w n="5.11">folle</w></l>
						<l n="6" num="2.2"><space quantity="4" unit="char"></space><w n="6.1">Qu</w>’<w n="6.2">en</w> <w n="6.3">vous</w> <w n="6.4">voyant</w> <w n="6.5">ainsi</w> <w n="6.6">je</w> <w n="6.7">n</w>’<w n="6.8">osais</w> <w n="6.9">vous</w> <w n="6.10">parler</w>.</l>
						<l n="7" num="2.3"><w n="7.1">J</w>’<w n="7.2">accusais</w> <w n="7.3">ma</w> <w n="7.4">mémoire</w>, <w n="7.5">et</w> <w n="7.6">j</w>’<w n="7.7">allais</w> <w n="7.8">m</w>’<w n="7.9">en</w> <w n="7.10">aller</w></l>
						<l n="8" num="2.4"><space quantity="4" unit="char"></space><w n="8.1">Sans</w> <w n="8.2">vous</w> <w n="8.3">adresser</w> <w n="8.4">la</w> <w n="8.5">parole</w>. »</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Et</w> <w n="9.2">la</w> <w n="9.3">brebis</w>, <w n="9.4">levant</w> <w n="9.5">sa</w> <w n="9.6">tête</w> <w n="9.7">avec</w> <w n="9.8">effort</w>,</l>
						<l n="10" num="3.2"><space quantity="4" unit="char"></space><w n="10.1">Bêle</w> <w n="10.2">ce</w> <w n="10.3">sanglot</w> <w n="10.4">de</w> <w n="10.5">son</w> <w n="10.6">âme</w> :</l>
						<l n="11" num="3.3">— «<w n="11.1">Vous</w> <w n="11.2">ne</w> <w n="11.3">vous</w> <w n="11.4">trompez</w> <w n="11.5">pas</w> ; <w n="11.6">c</w>’<w n="11.7">est</w>… <w n="11.8">c</w>’<w n="11.9">était</w> <w n="11.10">moi</w>, <w n="11.11">madame</w> ;</l>
						<l n="12" num="3.4"><space quantity="4" unit="char"></space><w n="12.1">Et</w> <w n="12.2">me</w> <w n="12.3">voilà</w> !… <w n="12.4">voilà</w> <w n="12.5">le</w> <w n="12.6">sort</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">« <w n="13.1">Quand</w> <w n="13.2">j</w>’<w n="13.3">étais</w> <w n="13.4">blanche</w> <w n="13.5">et</w> <w n="13.6">rose</w>, <w n="13.7">on</w> <w n="13.8">m</w>’<w n="13.9">a</w> <w n="13.10">beaucoup</w> <w n="13.11">parée</w>.</l>
						<l n="14" num="4.2"><w n="14.1">Aux</w> <w n="14.2">fêtes</w> <w n="14.3">du</w> <w n="14.4">printemps</w> <w n="14.5">on</w> <w n="14.6">m</w>’<w n="14.7">habillait</w> <w n="14.8">de</w> <w n="14.9">fleurs</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">On</w> <w n="15.2">me</w> <w n="15.3">laissait</w> <w n="15.4">brouter</w> <w n="15.5">sur</w> <w n="15.6">de</w> <w n="15.7">tendres</w> <w n="15.8">couleurs</w>.</l>
						<l n="16" num="4.4"><space quantity="4" unit="char"></space><w n="16.1">Et</w> <w n="16.2">je</w> <w n="16.3">me</w> <w n="16.4">croyais</w> <w n="16.5">adorée</w>.</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1">« <w n="17.1">L</w>’<w n="17.2">eau</w> <w n="17.3">filtrant</w> <w n="17.4">du</w> <w n="17.5">rocher</w> <w n="17.6">pour</w> <w n="17.7">laver</w> <w n="17.8">ma</w> <w n="17.9">toison</w></l>
						<l n="18" num="5.2"><space quantity="4" unit="char"></space><w n="18.1">Ne</w> <w n="18.2">semblait</w> <w n="18.3">jamais</w> <w n="18.4">assez</w> <w n="18.5">claire</w> ;</l>
						<l n="19" num="5.3"><w n="19.1">Oh</w> ! <w n="19.2">madame</w>, <w n="19.3">c</w>’<w n="19.4">est</w> <w n="19.5">doux</w> ! <w n="19.6">oui</w>, <w n="19.7">c</w>’<w n="19.8">est</w> <w n="19.9">si</w> <w n="19.10">doux</w> <w n="19.11">de</w> <w n="19.12">plaire</w></l>
						<l n="20" num="5.4"><space quantity="4" unit="char"></space><w n="20.1">Qu</w>’<w n="20.2">on</w> <w n="20.3">n</w>’<w n="20.4">en</w> <w n="20.5">cherche</w> <w n="20.6">pas</w> <w n="20.7">la</w> <w n="20.8">raison</w>.</l>
					</lg>
					<lg n="6">
						<l n="21" num="6.1">« <w n="21.1">Je</w> <w n="21.2">dansais</w> <w n="21.3">à</w> <w n="21.4">la</w> <w n="21.5">flûte</w> <w n="21.6">une</w> <w n="21.7">couronne</w> <w n="21.8">en</w> <w n="21.9">tête</w> ;</l>
						<l n="22" num="6.2"><w n="22.1">J</w>’<w n="22.2">en</w> <w n="22.3">faisais</w> <w n="22.4">mon</w> <w n="22.5">devoir</w> <w n="22.6">et</w> <w n="22.7">ma</w> <w n="22.8">cour</w> <w n="22.9">au</w> <w n="22.10">pasteur</w>.</l>
						<l n="23" num="6.3"><w n="23.1">Je</w> <w n="23.2">buvais</w> <w n="23.3">dans</w> <w n="23.4">sa</w> <w n="23.5">tasse</w>, <w n="23.6">intrépide</w>, <w n="23.7">sans</w> <w n="23.8">peur</w>,</l>
						<l n="24" num="6.4"><space quantity="4" unit="char"></space><w n="24.1">Et</w> <w n="24.2">ses</w> <w n="24.3">festins</w> <w n="24.4">étaient</w> <w n="24.5">ma</w> <w n="24.6">fête</w>.</l>
					</lg>
					<lg n="7">
						<l n="25" num="7.1">« <w n="25.1">Tout</w> <w n="25.2">changea</w>. <w n="25.3">Le</w> <w n="25.4">pasteur</w>, <w n="25.5">las</w> <w n="25.6">de</w> <w n="25.7">m</w>’<w n="25.8">être</w> <w n="25.9">indulgent</w>,</l>
						<l n="26" num="7.2"><space quantity="4" unit="char"></space><w n="26.1">Me</w> <w n="26.2">fit</w> <w n="26.3">traîner</w> <w n="26.4">au</w> <w n="26.5">sacrifice</w>.</l>
						<l n="27" num="7.3"><w n="27.1">Toutefois</w> <w n="27.2">un</w> <w n="27.3">enfant</w> <w n="27.4">me</w> <w n="27.5">sauva</w> <w n="27.6">du</w> <w n="27.7">supplice</w></l>
						<l n="28" num="7.4"><space quantity="4" unit="char"></space><w n="28.1">Alors</w> <w n="28.2">qu</w>’<w n="28.3">on</w> <w n="28.4">allait</w> <w n="28.5">m</w>’<w n="28.6">égorgeant</w>.</l>
					</lg>
					<lg n="8">
						<l n="29" num="8.1">« <w n="29.1">La</w> <w n="29.2">pitié</w>… <w n="29.3">Je</w> <w n="29.4">le</w> <w n="29.5">crois</w>, <w n="29.6">mais</w> <w n="29.7">on</w> <w n="29.8">m</w>’<w n="29.9">ôta</w> <w n="29.10">ma</w> <w n="29.11">laine</w>,</l>
						<l n="30" num="8.2"><w n="30.1">Ma</w> <w n="30.2">sonnette</w> <w n="30.3">d</w>’<w n="30.4">argent</w>, <w n="30.5">mes</w> <w n="30.6">flots</w> <w n="30.7">de</w> <w n="30.8">rubans</w> <w n="30.9">verts</w>.</l>
						<l n="31" num="8.3"><w n="31.1">Ma</w> <w n="31.2">liberté</w>, <w n="31.3">ma</w> <w n="31.4">part</w> <w n="31.5">dans</w> <w n="31.6">ce</w> <w n="31.7">bel</w> <w n="31.8">univers</w>.</l>
						<l n="32" num="8.4"><space quantity="4" unit="char"></space><w n="32.1">Et</w> <w n="32.2">le</w> <w n="32.3">doux</w> <w n="32.4">lait</w> <w n="32.5">dont</w> <w n="32.6">j</w>’<w n="32.7">étais</w> <w n="32.8">pleine</w>.</l>
					</lg>
					<lg n="9">
						<l n="33" num="9.1">« <w n="33.1">Je</w> <w n="33.2">fus</w> <w n="33.3">liée</w>… » — « <w n="33.4">Horreur</w> ! <w n="33.5">Ah</w> ! <w n="33.6">j</w>’<w n="33.7">aurais</w> <w n="33.8">tant</w> <w n="33.9">mordu</w>,</l>
						<l n="34" num="9.2"><space quantity="4" unit="char"></space><w n="34.1">Tant</w> <w n="34.2">bondi</w> <w n="34.3">pour</w> <w n="34.4">casser</w> <w n="34.5">ma</w> <w n="34.6">corde</w>,</l>
						<l n="35" num="9.3"><w n="35.1">Tant</w> <w n="35.2">bramé</w> <w n="35.3">vers</w> <w n="35.4">le</w> <w n="35.5">ciel</w> : « <w n="35.6">À</w> <w n="35.7">moi</w> ! <w n="35.8">Miséricorde</w> !</l>
						<l n="36" num="9.4"><space quantity="4" unit="char"></space><w n="36.1">Que</w> <w n="36.2">mon</w> <w n="36.3">droit</w> <w n="36.4">m</w>’<w n="36.5">eût</w> <w n="36.6">été</w> <w n="36.7">rendu</w>.</l>
					</lg>
					<lg n="10">
						<l n="37" num="10.1">« <w n="37.1">Aux</w> <w n="37.2">cris</w> <w n="37.3">de</w> <w n="37.4">l</w>’<w n="37.5">innocence</w> <w n="37.6">il</w> <w n="37.7">faut</w> <w n="37.8">que</w> <w n="37.9">Dieu</w> <w n="37.10">réponde</w> !</l>
						<l n="38" num="10.2"><w n="38.1">Oui</w>, <w n="38.2">madame</w>, <w n="38.3">on</w> <w n="38.4">m</w>’<w n="38.5">égorge</w> : <w n="38.6">il</w> <w n="38.7">doit</w> <w n="38.8">me</w> <w n="38.9">secourir</w>.</l>
						<l n="39" num="10.3"><w n="39.1">Il</w> <w n="39.2">doit</w> <w n="39.3">me</w> <w n="39.4">délier</w>, <w n="39.5">moi</w>, <w n="39.6">faite</w> <w n="39.7">pour</w> <w n="39.8">courir</w></l>
						<l n="40" num="10.4"><space quantity="4" unit="char"></space><w n="40.1">Toutes</w> <w n="40.2">les</w> <w n="40.3">montagnes</w> <w n="40.4">du</w> <w n="40.5">monde</w> ! »</l>
					</lg>
					<lg n="11">
						<l n="41" num="11.1"><w n="41.1">Le</w> <w n="41.2">nez</w> <w n="41.3">de</w> <w n="41.4">la</w> <w n="41.5">brebis</w> <w n="41.6">se</w> <w n="41.7">baissa</w> <w n="41.8">consterné</w>.</l>
						<l n="42" num="11.2"><space quantity="4" unit="char"></space><w n="42.1">Humble</w> <w n="42.2">aux</w> <w n="42.3">bonheurs</w>, <w n="42.4">douce</w> <w n="42.5">au</w> <w n="42.6">martyre</w>,</l>
						<l n="43" num="11.3"><w n="43.1">Son</w> <w n="43.2">cœur</w> <w n="43.3">saigne</w> <w n="43.4">et</w> <w n="43.5">pourtant</w> <w n="43.6">sa</w> <w n="43.7">plainte</w> <w n="43.8">se</w> <w n="43.9">retire</w></l>
						<l n="44" num="11.4"><space quantity="4" unit="char"></space><w n="44.1">De</w> <w n="44.2">la</w> <w n="44.3">chèvre</w> <w n="44.4">au</w> <w n="44.5">front</w> <w n="44.6">étonné</w>.</l>
					</lg>
					<lg n="12">
						<l n="45" num="12.1">— « <w n="45.1">Quoi</w> ! <w n="45.2">vous</w> <w n="45.3">ne</w> <w n="45.4">sautez</w> <w n="45.5">pas</w> <w n="45.6">contre</w> <w n="45.7">un</w> <w n="45.8">sort</w> <w n="45.9">si</w> <w n="45.10">funeste</w> ?</l>
						<l n="46" num="12.2"><w n="46.1">Que</w> <w n="46.2">votre</w> <w n="46.3">haine</w> <w n="46.4">est</w> <w n="46.5">molle</w> <w n="46.6">et</w> <w n="46.7">lente</w> <w n="46.8">à</w> <w n="46.9">s</w>’<w n="46.10">enflammer</w> ! »</l>
						<l n="47" num="12.3">— « <w n="47.1">La</w> <w n="47.2">haine</w> <w n="47.3">corromprait</w> <w n="47.4">le</w> <w n="47.5">bonheur</w> <w n="47.6">qui</w> <w n="47.7">me</w> <w n="47.8">reste</w>. »</l>
						<l n="48" num="12.4">— « <w n="48.1">Hé</w>, <w n="48.2">mon</w> <w n="48.3">Dieu</w> ! <w n="48.4">quel</w> <w n="48.5">est</w> <w n="48.6">donc</w> <w n="48.7">votre</w> <w n="48.8">bonheur</w> ? »— « <w n="48.9">D</w>’<w n="48.10">aimer</w>.»</l>
					</lg>
				</div></body></text></TEI>