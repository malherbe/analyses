<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">FAMILLE</head><div type="poem" key="DES378">
					<head type="main">LE NID SOLITAIRE</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Va</w>, <w n="1.2">mon</w> <w n="1.3">âme</w>, <w n="1.4">au</w>-<w n="1.5">dessus</w> <w n="1.6">de</w> <w n="1.7">la</w> <w n="1.8">foule</w> <w n="1.9">qui</w> <w n="1.10">passe</w>,</l>
						<l n="2" num="1.2"><w n="2.1">Ainsi</w> <w n="2.2">qu</w>’<w n="2.3">un</w> <w n="2.4">libre</w> <w n="2.5">oiseau</w> <w n="2.6">te</w> <w n="2.7">baigner</w> <w n="2.8">dans</w> <w n="2.9">l</w>’<w n="2.10">espace</w>.</l>
						<l n="3" num="1.3"><w n="3.1">Va</w> <w n="3.2">voir</w> ! <w n="3.3">et</w> <w n="3.4">ne</w> <w n="3.5">reviens</w> <w n="3.6">qu</w>’<w n="3.7">après</w> <w n="3.8">avoir</w> <w n="3.9">touché</w></l>
						<l n="4" num="1.4"><w n="4.1">Le</w> <w n="4.2">rêve</w>… <w n="4.3">mon</w> <w n="4.4">bon</w> <w n="4.5">rêve</w> <w n="4.6">à</w> <w n="4.7">la</w> <w n="4.8">terre</w> <w n="4.9">caché</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Moi</w>, <w n="5.2">je</w> <w n="5.3">veux</w> <w n="5.4">du</w> <w n="5.5">silence</w>, <w n="5.6">il</w> <w n="5.7">y</w> <w n="5.8">va</w> <w n="5.9">de</w> <w n="5.10">ma</w> <w n="5.11">vie</w> ;</l>
						<l n="6" num="2.2"><w n="6.1">Et</w> <w n="6.2">je</w> <w n="6.3">m</w>’<w n="6.4">enferme</w> <w n="6.5">où</w> <w n="6.6">rien</w>, <w n="6.7">plus</w> <w n="6.8">rien</w> <w n="6.9">ne</w> <w n="6.10">m</w>’<w n="6.11">a</w> <w n="6.12">suivie</w> ;</l>
						<l n="7" num="2.3"><w n="7.1">Et</w> <w n="7.2">de</w> <w n="7.3">mon</w> <w n="7.4">nid</w> <w n="7.5">étroit</w> <w n="7.6">d</w>’<w n="7.7">où</w> <w n="7.8">nul</w> <w n="7.9">sanglot</w> <w n="7.10">ne</w> <w n="7.11">sort</w>,</l>
						<l n="8" num="2.4"><w n="8.1">J</w>’<w n="8.2">entends</w> <w n="8.3">courir</w> <w n="8.4">le</w> <w n="8.5">siècle</w> <w n="8.6">à</w> <w n="8.7">côté</w> <w n="8.8">de</w> <w n="8.9">mon</w> <w n="8.10">sort</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Le</w> <w n="9.2">siècle</w> <w n="9.3">qui</w> <w n="9.4">s</w>’<w n="9.5">enfuit</w> <w n="9.6">grondant</w> <w n="9.7">devant</w> <w n="9.8">nos</w> <w n="9.9">portes</w>,</l>
						<l n="10" num="3.2"><w n="10.1">Entraînant</w> <w n="10.2">dans</w> <w n="10.3">son</w> <w n="10.4">cours</w> <w n="10.5">comme</w> <w n="10.6">des</w> <w n="10.7">algues</w> <w n="10.8">mortes</w></l>
						<l n="11" num="3.3"><w n="11.1">Les</w> <w n="11.2">noms</w> <w n="11.3">ensanglantés</w>, <w n="11.4">les</w> <w n="11.5">vœux</w>, <w n="11.6">les</w> <w n="11.7">vains</w> <w n="11.8">serments</w>.</l>
						<l n="12" num="3.4"><w n="12.1">Les</w> <w n="12.2">bouquets</w> <w n="12.3">purs</w>, <w n="12.4">noués</w> <w n="12.5">de</w> <w n="12.6">noms</w> <w n="12.7">doux</w> <w n="12.8">et</w> <w n="12.9">charmants</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Va</w>, <w n="13.2">mon</w> <w n="13.3">âme</w>, <w n="13.4">au</w>-<w n="13.5">dessus</w> <w n="13.6">de</w> <w n="13.7">la</w> <w n="13.8">foule</w> <w n="13.9">qui</w> <w n="13.10">passe</w>,</l>
						<l n="14" num="4.2"><w n="14.1">Ainsi</w> <w n="14.2">qu</w>’<w n="14.3">un</w> <w n="14.4">libre</w> <w n="14.5">oiseau</w> <w n="14.6">te</w> <w n="14.7">baigner</w> <w n="14.8">dans</w> <w n="14.9">l</w>’<w n="14.10">espace</w>.</l>
						<l n="15" num="4.3"><w n="15.1">Va</w> <w n="15.2">voir</w> ! <w n="15.3">et</w> <w n="15.4">ne</w> <w n="15.5">reviens</w> <w n="15.6">qu</w>’<w n="15.7">après</w> <w n="15.8">avoir</w> <w n="15.9">touché</w></l>
						<l n="16" num="4.4"><w n="16.1">Le</w> <w n="16.2">rêve</w>… <w n="16.3">mon</w> <w n="16.4">beau</w> <w n="16.5">rêve</w> <w n="16.6">à</w> <w n="16.7">la</w> <w n="16.8">terre</w> <w n="16.9">caché</w></l>
					</lg>
				</div></body></text></TEI>