<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES421">
					<head type="main">LA PRIÈRE DES ORPHELINS</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Voix</w> <w n="1.2">d</w>’<w n="1.3">enfants</w>, <w n="1.4">ô</w> <w n="1.5">voix</w> <w n="1.6">qui</w> <w n="1.7">chantez</w>.</l>
						<l n="2" num="1.2"><w n="2.1">Dites</w>-<w n="2.2">nous</w> <w n="2.3">vers</w> <w n="2.4">qui</w> <w n="2.5">vous</w> <w n="2.6">montez</w> ?</l>
					</lg>
					<lg n="2">
						<l n="3" num="2.1">— « <w n="3.1">Nous</w> <w n="3.2">cherchons</w> <w n="3.3">Dieu</w> <w n="3.4">qui</w> <w n="3.5">nous</w> <w n="3.6">rassemble</w>,</l>
						<l n="4" num="2.2"><w n="4.1">Dieu</w> <w n="4.2">qui</w> <w n="4.3">nous</w> <w n="4.4">donna</w> <w n="4.5">votre</w> <w n="4.6">appui</w>.</l>
						<l n="5" num="2.3"><w n="5.1">Et</w> <w n="5.2">pour</w> <w n="5.3">arriver</w> <w n="5.4">jusqu</w>’<w n="5.5">à</w> <w n="5.6">lui</w></l>
						<l n="6" num="2.4"><w n="6.1">Nous</w> <w n="6.2">mêlons</w> <w n="6.3">nos</w> <w n="6.4">souffles</w> <w n="6.5">ensemble</w>.</l>
						<l n="7" num="2.5"><w n="7.1">Dieu</w> ! <w n="7.2">qui</w> <w n="7.3">soutenez</w> <w n="7.4">le</w> <w n="7.5">roseau</w>,</l>
						<l n="8" num="2.6"><w n="8.1">Dieu</w> <w n="8.2">qui</w> <w n="8.3">donnez</w> <w n="8.4">l</w>’<w n="8.5">aile</w> <w n="8.6">à</w> <w n="8.7">l</w>’<w n="8.8">oiseau</w>,</l>
						<l n="9" num="2.7"><w n="9.1">Donnez</w> <w n="9.2">l</w>’<w n="9.3">âme</w> <w n="9.4">à</w> <w n="9.5">notre</w> <w n="9.6">prière</w></l>
						<l n="10" num="2.8"><w n="10.1">Pour</w> <w n="10.2">qu</w>’<w n="10.3">elle</w> <w n="10.4">monte</w> <w n="10.5">à</w> <w n="10.6">vous</w>, <w n="10.7">mon</w> <w n="10.8">père</w> ! »</l>
					</lg>
					<lg n="3">
						<l n="11" num="3.1"><w n="11.1">Voix</w> <w n="11.2">d</w>’<w n="11.3">enfants</w>, <w n="11.4">ô</w> <w n="11.5">voix</w> <w n="11.6">qui</w> <w n="11.7">pleurez</w>,</l>
						<l n="12" num="3.2"><w n="12.1">Dites</w>-<w n="12.2">nous</w> <w n="12.3">qui</w> <w n="12.4">vous</w> <w n="12.5">implorez</w> ?</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1">— « <w n="13.1">Nous</w> <w n="13.2">pleurons</w> <w n="13.3">pour</w> <w n="13.4">l</w>’<w n="13.5">enfant</w> <w n="13.6">sans</w> <w n="13.7">mère</w></l>
						<l n="14" num="4.2"><w n="14.1">Que</w> <w n="14.2">nous</w> <w n="14.3">voyons</w> <w n="14.4">errer</w> <w n="14.5">là</w>-<w n="14.6">bas</w> ;</l>
						<l n="15" num="4.3"><w n="15.1">Nous</w> <w n="15.2">voulons</w> <w n="15.3">un</w> <w n="15.4">guide</w> <w n="15.5">à</w> <w n="15.6">ses</w> <w n="15.7">pas</w>,</l>
						<l n="16" num="4.4"><w n="16.1">Un</w> <w n="16.2">refuge</w> <w n="16.3">à</w> <w n="16.4">sa</w> <w n="16.5">vie</w> <w n="16.6">amère</w>.</l>
						<l n="17" num="4.5"><w n="17.1">Dieu</w>, <w n="17.2">qui</w> <w n="17.3">soutenez</w> <w n="17.4">le</w> <w n="17.5">roseau</w>,</l>
						<l n="18" num="4.6"><w n="18.1">Dieu</w>, <w n="18.2">qui</w> <w n="18.3">donnez</w> <w n="18.4">l</w>’<w n="18.5">aile</w> <w n="18.6">à</w> <w n="18.7">l</w>’<w n="18.8">oiseau</w>.</l>
						<l n="19" num="4.7"><w n="19.1">Donnez</w> <w n="19.2">l</w>’<w n="19.3">âme</w> <w n="19.4">à</w> <w n="19.5">notre</w> <w n="19.6">prière</w>.</l>
						<l n="20" num="4.8"><w n="20.1">Pour</w> <w n="20.2">qu</w>’<w n="20.3">elle</w> <w n="20.4">vous</w> <w n="20.5">plaise</w>, <w n="20.6">ô</w> <w n="20.7">mon</w> <w n="20.8">père</w> ! »</l>
					</lg>
					<lg n="5">
						<l n="21" num="5.1"><w n="21.1">Voix</w> <w n="21.2">sans</w> <w n="21.3">audace</w> <w n="21.4">et</w> <w n="21.5">sans</w> <w n="21.6">frayeur</w>,</l>
						<l n="22" num="5.2"><w n="22.1">Que</w> <w n="22.2">demandez</w>-<w n="22.3">vous</w> <w n="22.4">au</w> <w n="22.5">Seigneur</w> ?</l>
					</lg>
					<lg n="6">
						<l n="23" num="6.1">— « <w n="23.1">Le</w> <w n="23.2">doux</w> <w n="23.3">pardon</w>, <w n="23.4">l</w>’<w n="23.5">amour</w> <w n="23.6">immense</w>,</l>
						<l n="24" num="6.2"><w n="24.1">Pour</w> <w n="24.2">le</w> <w n="24.3">prisonnier</w> <w n="24.4">palpitant</w>,</l>
						<l n="25" num="6.3"><w n="25.1">Pour</w> <w n="25.2">le</w> <w n="25.3">coupable</w> <w n="25.4">repentant</w>,</l>
						<l n="26" num="6.4"><w n="26.1">Et</w> <w n="26.2">pour</w> <w n="26.3">les</w> <w n="26.4">méchants</w> <w n="26.5">en</w> <w n="26.6">démence</w>.</l>
						<l n="27" num="6.5"><w n="27.1">Dieu</w>, <w n="27.2">qui</w> <w n="27.3">soutenez</w> <w n="27.4">le</w> <w n="27.5">roseau</w>,</l>
						<l n="28" num="6.6"><w n="28.1">Dieu</w> <w n="28.2">qui</w> <w n="28.3">donnez</w> <w n="28.4">l</w>’<w n="28.5">aile</w> <w n="28.6">à</w> <w n="28.7">l</w>’<w n="28.8">oiseau</w>.</l>
						<l n="29" num="6.7"><w n="29.1">Donnez</w> <w n="29.2">l</w>’<w n="29.3">âme</w> <w n="29.4">à</w> <w n="29.5">notre</w> <w n="29.6">prière</w></l>
						<l n="30" num="6.8"><w n="30.1">Pour</w> <w n="30.2">qu</w>’<w n="30.3">elle</w> <w n="30.4">monte</w> <w n="30.5">à</w> <w n="30.6">vous</w>, <w n="30.7">mon</w> <w n="30.8">père</w> ! »</l>
					</lg>
					<lg n="7">
						<l n="31" num="7.1"><w n="31.1">Voix</w> <w n="31.2">d</w>’<w n="31.3">enfants</w>, <w n="31.4">dites</w>-<w n="31.5">nous</w> <w n="31.6">encor</w></l>
						<l n="32" num="7.2"><w n="32.1">Où</w> <w n="32.2">s</w>’<w n="32.3">en</w> <w n="32.4">va</w> <w n="32.5">votre</w> <w n="32.6">tendre</w> <w n="32.7">essor</w> ?</l>
					</lg>
					<lg n="8">
						<l n="33" num="8.1">— « <w n="33.1">Il</w> <w n="33.2">s</w>’<w n="33.3">en</w> <w n="33.4">va</w> <w n="33.5">plus</w> <w n="33.6">haut</w> <w n="33.7">que</w> <w n="33.8">l</w>’<w n="33.9">orage</w></l>
						<l n="34" num="8.2"><w n="34.1">Chercher</w> <w n="34.2">les</w> <w n="34.3">saintes</w> <w n="34.4">charités</w>.</l>
						<l n="35" num="8.3"><w n="35.1">Un</w> <w n="35.2">oiseau</w> <w n="35.3">nous</w> <w n="35.4">a</w> <w n="35.5">dit</w> : « <w n="35.6">Chantez</w> ! »</l>
						<l n="36" num="8.4"><w n="36.1">Un</w> <w n="36.2">roseau</w> <w n="36.3">nous</w> <w n="36.4">a</w> <w n="36.5">dit</w> : « <w n="36.6">Courage</w> ! »</l>
						<l n="37" num="8.5"><w n="37.1">Dieu</w> ! <w n="37.2">qui</w> <w n="37.3">soutenez</w> <w n="37.4">le</w> <w n="37.5">roseau</w>,</l>
						<l n="38" num="8.6"><w n="38.1">Dieu</w> ! <w n="38.2">qui</w> <w n="38.3">donnez</w> <w n="38.4">l</w>’<w n="38.5">aile</w> <w n="38.6">à</w> <w n="38.7">l</w>’<w n="38.8">oiseau</w>,</l>
						<l n="39" num="8.7"><w n="39.1">Donnez</w> <w n="39.2">l</w>’<w n="39.3">âme</w> <w n="39.4">à</w> <w n="39.5">notre</w> <w n="39.6">prière</w>.</l>
						<l n="40" num="8.8"><w n="40.1">Pour</w> <w n="40.2">qu</w>’<w n="40.3">elle</w> <w n="40.4">vous</w> <w n="40.5">plaise</w>, <w n="40.6">ô</w> <w n="40.7">mon</w> <w n="40.8">père</w> ! »</l>
					</lg>
					<lg n="9">
						<l n="41" num="9.1"><w n="41.1">Voix</w> <w n="41.2">d</w>’<w n="41.3">enfants</w>, <w n="41.4">priez</w> <w n="41.5">donc</w> <w n="41.6">pour</w> <w n="41.7">nous</w>,</l>
						<l n="42" num="9.2"><w n="42.1">Car</w> <w n="42.2">l</w>’<w n="42.3">innocence</w> <w n="42.4">est</w> <w n="42.5">avec</w> <w n="42.6">vous</w> !</l>
					</lg>
					<lg n="10">
						<l n="43" num="10.1">— « <w n="43.1">Dieu</w> <w n="43.2">juste</w>, <w n="43.3">écartez</w> <w n="43.4">les</w> <w n="43.5">alarmes</w></l>
						<l n="44" num="10.2"><w n="44.1">Des</w> <w n="44.2">heureux</w> <w n="44.3">qui</w> <w n="44.4">donnent</w> <w n="44.5">toujours</w> !</l>
						<l n="45" num="10.3"><w n="45.1">Donnez</w>-<w n="45.2">leur</w> <w n="45.3">autant</w> <w n="45.4">de</w> <w n="45.5">beaux</w> <w n="45.6">jours</w></l>
						<l n="46" num="10.4"><w n="46.1">Qu</w>’<w n="46.2">ils</w> <w n="46.3">nous</w> <w n="46.4">ont</w> <w n="46.5">épargné</w> <w n="46.6">de</w> <w n="46.7">larmes</w> !</l>
						<l n="47" num="10.5"><w n="47.1">Dieu</w>, <w n="47.2">qui</w> <w n="47.3">soutenez</w> <w n="47.4">le</w> <w n="47.5">roseau</w>,</l>
						<l n="48" num="10.6"><w n="48.1">Dieu</w>, <w n="48.2">qui</w> <w n="48.3">donnez</w> <w n="48.4">l</w>’<w n="48.5">aile</w> <w n="48.6">à</w> <w n="48.7">l</w>’<w n="48.8">oiseau</w>.</l>
						<l n="49" num="10.7"><w n="49.1">Donnez</w> <w n="49.2">l</w>’<w n="49.3">âme</w> <w n="49.4">à</w> <w n="49.5">notre</w> <w n="49.6">prière</w>,</l>
						<l n="50" num="10.8"><w n="50.1">Pour</w> <w n="50.2">qu</w>’<w n="50.3">elle</w> <w n="50.4">vous</w> <w n="50.5">touche</w>, <w n="50.6">ô</w> <w n="50.7">mon</w> <w n="50.8">père</w> ! »</l>
					</lg>
				</div></body></text></TEI>