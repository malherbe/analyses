<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES424">
					<head type="main">L’ENFANT AU MIROIR</head>
					<opener>
						<salute>À Mlle Émilie Bascans.</salute>
					</opener>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Si</w> <w n="1.2">j</w>’<w n="1.3">étais</w> <w n="1.4">assez</w> <w n="1.5">grande</w>,</l>
						<l n="2" num="1.2"><space quantity="2" unit="char"></space><w n="2.1">Je</w> <w n="2.2">voudrais</w> <w n="2.3">voir</w></l>
						<l n="3" num="1.3"><w n="3.1">L</w>’<w n="3.2">effet</w> <w n="3.3">de</w> <w n="3.4">ma</w> <w n="3.5">guirlande</w></l>
						<l n="4" num="1.4"><space quantity="2" unit="char"></space><w n="4.1">Dans</w> <w n="4.2">le</w> <w n="4.3">miroir</w>.</l>
						<l n="5" num="1.5"><w n="5.1">En</w> <w n="5.2">montant</w> <w n="5.3">sur</w> <w n="5.4">la</w> <w n="5.5">chaise</w>,</l>
						<l n="6" num="1.6"><space quantity="2" unit="char"></space><w n="6.1">Je</w> <w n="6.2">l</w>’<w n="6.3">atteindrais</w> ;</l>
						<l n="7" num="1.7"><w n="7.1">Mais</w> <w n="7.2">sans</w> <w n="7.3">aide</w> <w n="7.4">et</w> <w n="7.5">sans</w> <w n="7.6">aise</w>,</l>
						<l n="8" num="1.8"><space quantity="2" unit="char"></space><w n="8.1">Je</w> <w n="8.2">tomberais</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">La</w> <w n="9.2">dame</w> <w n="9.3">plus</w> <w n="9.4">heureuse</w>,</l>
						<l n="10" num="2.2"><space quantity="2" unit="char"></space><w n="10.1">Sans</w> <w n="10.2">faire</w> <w n="10.3">un</w> <w n="10.4">pas</w>,</l>
						<l n="11" num="2.3"><w n="11.1">Sans</w> <w n="11.2">quitter</w> <w n="11.3">sa</w> <w n="11.4">causeuse</w>,</l>
						<l n="12" num="2.4"><space quantity="2" unit="char"></space><w n="12.1">De</w> <w n="12.2">haut</w> <w n="12.3">en</w> <w n="12.4">bas</w>,</l>
						<l n="13" num="2.5"><w n="13.1">Dans</w> <w n="13.2">une</w> <w n="13.3">glace</w> <w n="13.4">claire</w>,</l>
						<l n="14" num="2.6"><space quantity="2" unit="char"></space><w n="14.1">Comme</w> <w n="14.2">au</w> <w n="14.3">hasard</w>,</l>
						<l n="15" num="2.7"><w n="15.1">Pour</w> <w n="15.2">apprendre</w> <w n="15.3">à</w> <w n="15.4">se</w> <w n="15.5">plaire</w></l>
						<l n="16" num="2.8"><space quantity="2" unit="char"></space><w n="16.1">Jette</w> <w n="16.2">un</w> <w n="16.3">regard</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Ah</w> ! <w n="17.2">c</w>’<w n="17.3">est</w> <w n="17.4">bien</w> <w n="17.5">incommode</w></l>
						<l n="18" num="3.2"><space quantity="2" unit="char"></space><w n="18.1">D</w>’<w n="18.2">avoir</w> <w n="18.3">huit</w> <w n="18.4">ans</w> !</l>
						<l n="19" num="3.3"><w n="19.1">Il</w> <w n="19.2">faut</w> <w n="19.3">suivre</w> <w n="19.4">la</w> <w n="19.5">mode</w></l>
						<l n="20" num="3.4"><space quantity="2" unit="char"></space><w n="20.1">Et</w> <w n="20.2">perdre</w> <w n="20.3">un</w> <w n="20.4">temps</w> !…</l>
						<l n="21" num="3.5"><w n="21.1">Peut</w>-<w n="21.2">on</w> <w n="21.3">aimer</w> <w n="21.4">la</w> <w n="21.5">ville</w></l>
						<l n="22" num="3.6"><space quantity="2" unit="char"></space><w n="22.1">Et</w> <w n="22.2">les</w> <w n="22.3">salons</w> !</l>
						<l n="23" num="3.7"><w n="23.1">On</w> <w n="23.2">s</w>’<w n="23.3">en</w> <w n="23.4">va</w> <w n="23.5">si</w> <w n="23.6">tranquille</w></l>
						<l n="24" num="3.8"><space quantity="2" unit="char"></space><w n="24.1">Dans</w> <w n="24.2">les</w> <w n="24.3">vallons</w> !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Quand</w> <w n="25.2">ma</w> <w n="25.3">mère</w> <w n="25.4">qui</w> <w n="25.5">m</w>’<w n="25.6">aime</w></l>
						<l n="26" num="4.2"><space quantity="2" unit="char"></space><w n="26.1">Et</w> <w n="26.2">me</w> <w n="26.3">défend</w>,</l>
						<l n="27" num="4.3"><w n="27.1">Et</w> <w n="27.2">qui</w> <w n="27.3">veille</w> <w n="27.4">elle</w>-<w n="27.5">même</w></l>
						<l n="28" num="4.4"><space quantity="2" unit="char"></space><w n="28.1">Sur</w> <w n="28.2">son</w> <w n="28.3">enfant</w>,</l>
						<l n="29" num="4.5"><w n="29.1">M</w>’<w n="29.2">emporte</w> <w n="29.3">où</w> <w n="29.4">l</w>’<w n="29.5">on</w> <w n="29.6">respire</w></l>
						<l n="30" num="4.6"><space quantity="2" unit="char"></space><w n="30.1">Les</w> <w n="30.2">fleurs</w> <w n="30.3">et</w> <w n="30.4">l</w>’<w n="30.5">air</w>.</l>
						<l n="31" num="4.7"><w n="31.1">Si</w> <w n="31.2">son</w> <w n="31.3">enfant</w> <w n="31.4">soupire</w>.</l>
						<l n="32" num="4.8"><space quantity="2" unit="char"></space><w n="32.1">C</w>’<w n="32.2">est</w> <w n="32.3">un</w> <w n="32.4">éclair</w> !</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Les</w> <w n="33.2">ruisseaux</w> <w n="33.3">des</w> <w n="33.4">prairies</w></l>
						<l n="34" num="5.2"><space quantity="2" unit="char"></space><w n="34.1">Font</w> <w n="34.2">des</w> <w n="34.3">psychés</w></l>
						<l n="35" num="5.3"><w n="35.1">Où</w> <w n="35.2">libres</w> <w n="35.3">et</w> <w n="35.4">fleuries</w>,</l>
						<l n="36" num="5.4"><space quantity="2" unit="char"></space><w n="36.1">Les</w> <w n="36.2">fronts</w> <w n="36.3">penchés</w>,</l>
						<l n="37" num="5.5"><w n="37.1">Dans</w> <w n="37.2">l</w>’<w n="37.3">eau</w> <w n="37.4">qui</w> <w n="37.5">se</w> <w n="37.6">balance</w>,</l>
						<l n="38" num="5.6"><space quantity="2" unit="char"></space><w n="38.1">Sans</w> <w n="38.2">nous</w> <w n="38.3">hausser</w>,</l>
						<l n="39" num="5.7"><w n="39.1">Nous</w> <w n="39.2">allons</w> <w n="39.3">en</w> <w n="39.4">silence</w></l>
						<l n="40" num="5.8"><space quantity="2" unit="char"></space><w n="40.1">Nous</w> <w n="40.2">voir</w> <w n="40.3">passer</w>.</l>
					</lg>
					<lg n="6">
						<l n="41" num="6.1"><w n="41.1">C</w>’<w n="41.2">est</w> <w n="41.3">frais</w> <w n="41.4">dans</w> <w n="41.5">le</w> <w n="41.6">bois</w> <w n="41.7">sombre</w></l>
						<l n="42" num="6.2"><space quantity="2" unit="char"></space><w n="42.1">Et</w> <w n="42.2">puis</w> <w n="42.3">c</w>’<w n="42.4">est</w> <w n="42.5">beau</w></l>
						<l n="43" num="6.3"><w n="43.1">De</w> <w n="43.2">danser</w> <w n="43.3">comme</w> <w n="43.4">une</w> <w n="43.5">ombre</w></l>
						<l n="44" num="6.4"><space quantity="2" unit="char"></space><w n="44.1">Au</w> <w n="44.2">bord</w> <w n="44.3">de</w> <w n="44.4">l</w>’<w n="44.5">eau</w> !</l>
						<l n="45" num="6.5"><w n="45.1">Les</w> <w n="45.2">enfants</w> <w n="45.3">de</w> <w n="45.4">mon</w> <w n="45.5">âge</w>,</l>
						<l n="46" num="6.6"><space quantity="2" unit="char"></space><w n="46.1">Courant</w> <w n="46.2">toujours</w>,</l>
						<l n="47" num="6.7"><w n="47.1">Devraient</w> <w n="47.2">tous</w> <w n="47.3">au</w> <w n="47.4">village</w></l>
						<l n="48" num="6.8"><space quantity="2" unit="char"></space><w n="48.1">Passer</w> <w n="48.2">leurs</w> <w n="48.3">jours</w> !</l>
					</lg>
				</div></body></text></TEI>