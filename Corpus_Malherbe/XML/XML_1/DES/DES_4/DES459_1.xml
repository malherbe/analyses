<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES459">
					<head type="main">LE RÊVE À DEUX</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Entends</w>-<w n="1.2">tu</w> <w n="1.3">sonner</w> <w n="1.4">l</w>’<w n="1.5">heure</w></l>
						<l n="2" num="1.2"><w n="2.1">Qui</w> <w n="2.2">t</w>’<w n="2.3">appelait</w> <w n="2.4">vers</w> <w n="2.5">moi</w> ?</l>
						<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">dirait</w> <w n="3.3">qu</w>’<w n="3.4">elle</w> <w n="3.5">pleure</w></l>
						<l n="4" num="1.4"><w n="4.1">De</w> <w n="4.2">me</w> <w n="4.3">trouver</w> <w n="4.4">sans</w> <w n="4.5">toi</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Elle</w> <w n="5.2">aimait</w> <w n="5.3">à</w> <w n="5.4">renaître</w></l>
						<l n="6" num="1.6"><w n="6.1">Sous</w> <w n="6.2">nos</w> <w n="6.3">chants</w> <w n="6.4">amoureux</w>.</l>
						<l n="7" num="1.7"><w n="7.1">C</w>’<w n="7.2">était</w> <w n="7.3">rêver</w> <w n="7.4">peut</w>-<w n="7.5">être</w> :</l>
						<l n="8" num="1.8"><w n="8.1">Mais</w> <w n="8.2">nous</w> <w n="8.3">rêvions</w> <w n="8.4">à</w> <w n="8.5">deux</w>.</l>
					</lg>
					<lg n="2">
						<l n="9" num="2.1"><w n="9.1">D</w>’<w n="9.2">une</w> <w n="9.3">voix</w> <w n="9.4">souveraine</w></l>
						<l n="10" num="2.2"><w n="10.1">Tout</w> <w n="10.2">se</w> <w n="10.3">laisse</w> <w n="10.4">enchanter</w>.</l>
						<l n="11" num="2.3"><w n="11.1">Tu</w> <w n="11.2">soumettrais</w> <w n="11.3">la</w> <w n="11.4">reine</w></l>
						<l n="12" num="2.4"><w n="12.1">Qui</w> <w n="12.2">t</w>’<w n="12.3">entendrait</w> <w n="12.4">chanter</w>.</l>
						<l n="13" num="2.5"><w n="13.1">Dans</w> <w n="13.2">ses</w> <w n="13.3">ennuis</w> <w n="13.4">sans</w> <w n="13.5">trêves</w>,</l>
						<l n="14" num="2.6"><w n="14.1">Cette</w> <w n="14.2">dame</w> <w n="14.3">aux</w> <w n="14.4">longs</w> <w n="14.5">yeux</w></l>
						<l n="15" num="2.7"><w n="15.1">Donnerait</w> <w n="15.2">tous</w> <w n="15.3">ses</w> <w n="15.4">rêves</w></l>
						<l n="16" num="2.8"><w n="16.1">Pour</w> <w n="16.2">notre</w> <w n="16.3">rêve</w> <w n="16.4">à</w> <w n="16.5">deux</w>.</l>
					</lg>
					<lg n="3">
						<l n="17" num="3.1"><w n="17.1">Mais</w> <w n="17.2">depuis</w> <w n="17.3">que</w> <w n="17.4">l</w>’<w n="17.5">absence</w></l>
						<l n="18" num="3.2"><w n="18.1">Tourmente</w> <w n="18.2">ma</w> <w n="18.3">raison</w>,</l>
						<l n="19" num="3.3"><w n="19.1">Mon</w> <w n="19.2">âme</w> <w n="19.3">est</w> <w n="19.4">en</w> <w n="19.5">démence</w>,</l>
						<l n="20" num="3.4"><w n="20.1">Le</w> <w n="20.2">monde</w> <w n="20.3">est</w> <w n="20.4">ma</w> <w n="20.5">prison</w>.</l>
						<l n="21" num="3.5"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">la</w> <w n="21.4">cage</w> <w n="21.5">affligée</w></l>
						<l n="22" num="3.6"><w n="22.1">Où</w> <w n="22.2">se</w> <w n="22.3">heurtent</w> <w n="22.4">mes</w> <w n="22.5">vœux</w>.</l>
						<l n="23" num="3.7"><w n="23.1">J</w>’<w n="23.2">étais</w> <w n="23.3">si</w> <w n="23.4">protégée</w></l>
						<l n="24" num="3.8"><w n="24.1">Dans</w> <w n="24.2">notre</w> <w n="24.3">rêve</w> <w n="24.4">à</w> <w n="24.5">deux</w> !</l>
					</lg>
					<lg n="4">
						<l n="25" num="4.1"><w n="25.1">Hors</w> <w n="25.2">de</w> <w n="25.3">tes</w> <w n="25.4">bras</w> <w n="25.5">fidèles</w>,</l>
						<l n="26" num="4.2"><w n="26.1">Froide</w> <w n="26.2">à</w> <w n="26.3">tous</w> <w n="26.4">les</w> <w n="26.5">accords</w>,</l>
						<l n="27" num="4.3"><w n="27.1">La</w> <w n="27.2">danse</w> <w n="27.3">n</w>’<w n="27.4">a</w> <w n="27.5">plus</w> <w n="27.6">d</w>’<w n="27.7">ailes</w></l>
						<l n="28" num="4.4"><w n="28.1">Pour</w> <w n="28.2">soulever</w> <w n="28.3">mon</w> <w n="28.4">corps</w>.</l>
						<l n="29" num="4.5"><w n="29.1">À</w> <w n="29.2">moi</w>-<w n="29.3">même</w> <w n="29.4">ravie</w>,</l>
						<l n="30" num="4.6"><w n="30.1">Tout</w> <w n="30.2">bien</w> <w n="30.3">m</w>’<w n="30.4">est</w> <w n="30.5">douloureux</w>,</l>
						<l n="31" num="4.7"><w n="31.1">Le</w> <w n="31.2">jour</w> <w n="31.3">même</w> <w n="31.4">est</w> <w n="31.5">sans</w> <w n="31.6">vie</w></l>
						<l n="32" num="4.8"><w n="32.1">Après</w> <w n="32.2">le</w> <w n="32.3">rêve</w> <w n="32.4">à</w> <w n="32.5">deux</w>.</l>
					</lg>
					<lg n="5">
						<l n="33" num="5.1"><w n="33.1">Comme</w> <w n="33.2">un</w> <w n="33.3">orage</w> <w n="33.4">emporte</w></l>
						<l n="34" num="5.2"><w n="34.1">Tous</w> <w n="34.2">les</w> <w n="34.3">oiseaux</w> <w n="34.4">d</w>’<w n="34.5">un</w> <w n="34.6">bois</w>.</l>
						<l n="35" num="5.3"><w n="35.1">Rien</w> <w n="35.2">ne</w> <w n="35.3">chante</w> <w n="35.4">à</w> <w n="35.5">ma</w> <w n="35.6">porte</w></l>
						<l n="36" num="5.4"><w n="36.1">Où</w> <w n="36.2">ne</w> <w n="36.3">vient</w> <w n="36.4">plus</w> <w n="36.5">ta</w> <w n="36.6">voix</w>.</l>
						<l n="37" num="5.5"><w n="37.1">Ah</w> ! <w n="37.2">si</w> <w n="37.3">le</w> <w n="37.4">ciel</w> <w n="37.5">écoute</w></l>
						<l n="38" num="5.6"><w n="38.1">Les</w> <w n="38.2">amants</w> <w n="38.3">malheureux</w>,</l>
						<l n="39" num="5.7"><w n="39.1">La</w> <w n="39.2">douce</w> <w n="39.3">mort</w> <w n="39.4">sans</w> <w n="39.5">doute</w></l>
						<l n="40" num="5.8"><w n="40.1">Sera</w> <w n="40.2">le</w> <w n="40.3">rêve</w> <w n="40.4">à</w> <w n="40.5">deux</w> !</l>
					</lg>
				</div></body></text></TEI>