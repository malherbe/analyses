<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ENFANTS ET JEUNES FILLES</head><div type="poem" key="DES415">
					<head type="main">AUX NOUVEAUX NÉS HEUREUX</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">Petits</w> <w n="1.2">enfants</w> <w n="1.3">heureux</w>, <w n="1.4">que</w> <w n="1.5">vous</w> <w n="1.6">savez</w> <w n="1.7">de</w> <w n="1.8">choses</w>,</l>
						<l n="2" num="1.2"><space quantity="10" unit="char"></space><w n="2.1">En</w> <w n="2.2">naissant</w> !</l>
						<l n="3" num="1.3"><w n="3.1">On</w> <w n="3.2">dirait</w> <w n="3.3">qu</w>’<w n="3.4">on</w> <w n="3.5">entend</w> <w n="3.6">s</w>’<w n="3.7">entreparler</w> <w n="3.8">des</w> <w n="3.9">roses</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Et</w> <w n="4.2">que</w> <w n="4.3">vous</w> <w n="4.4">racontez</w> <w n="4.5">votre</w> <w n="4.6">ciel</w> <w n="4.7">au</w> <w n="4.8">passant</w>.</l>
					</lg>
					<lg n="2">
						<l n="5" num="2.1"><w n="5.1">Vos</w> <w n="5.2">rires</w> <w n="5.3">sont</w> <w n="5.4">vainqueurs</w> <w n="5.5">en</w> <w n="5.6">buvant</w> <w n="5.7">de</w> <w n="5.8">vos</w> <w n="5.9">mères</w></l>
						<l n="6" num="2.2"><space quantity="10" unit="char"></space><w n="6.1">Le</w> <w n="6.2">doux</w> <w n="6.3">lait</w>,</l>
						<l n="7" num="2.3"><w n="7.1">Vous</w> <w n="7.2">qui</w> <w n="7.3">ne</w> <w n="7.4">sentez</w> <w n="7.5">pas</w> <w n="7.6">que</w> <w n="7.7">des</w> <w n="7.8">larmes</w> <w n="7.9">amères</w></l>
						<l n="8" num="2.4"><w n="8.1">Coulent</w> <w n="8.2">dans</w> <w n="8.3">ce</w> <w n="8.4">nectar</w> <w n="8.5">tiède</w> <w n="8.6">et</w> <w n="8.7">blanc</w> <w n="8.8">qui</w> <w n="8.9">vous</w> <w n="8.10">plaît</w>.</l>
					</lg>
					<lg n="3">
						<l n="9" num="3.1"><w n="9.1">Ah</w> ! <w n="9.2">c</w>’<w n="9.3">est</w> <w n="9.4">pourtant</w> <w n="9.5">ainsi</w>, <w n="9.6">mes</w> <w n="9.7">charmants</w> <w n="9.8">camarades</w>,</l>
						<l n="10" num="3.2"><space quantity="10" unit="char"></space><w n="10.1">Mais</w> <w n="10.2">buvez</w> !</l>
						<l n="11" num="3.3"><w n="11.1">La</w> <w n="11.2">source</w> <w n="11.3">où</w> <w n="11.4">vous</w> <w n="11.5">puisez</w> <w n="11.6">d</w>’<w n="11.7">abondantes</w> <w n="11.8">rasades</w></l>
						<l n="12" num="3.4"><w n="12.1">Ne</w> <w n="12.2">peut</w> <w n="12.3">vivre</w> <w n="12.4">et</w> <w n="12.5">courir</w> <w n="12.6">qu</w>’<w n="12.7">autant</w> <w n="12.8">que</w> <w n="12.9">vous</w> <w n="12.10">vivez</w>.</l>
					</lg>
					<lg n="4">
						<l n="13" num="4.1"><w n="13.1">Buvez</w> ! <w n="13.2">délectez</w>-<w n="13.3">vous</w> <w n="13.4">sans</w> <w n="13.5">labeur</w> <w n="13.6">et</w> <w n="13.7">sans</w> <w n="13.8">honte</w>,</l>
						<l n="14" num="4.2"><space quantity="10" unit="char"></space><w n="14.1">Car</w> <w n="14.2">un</w> <w n="14.3">jour</w></l>
						<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">sort</w> <w n="15.3">qui</w> <w n="15.4">reprend</w> <w n="15.5">tout</w> <w n="15.6">vous</w> <w n="15.7">demandera</w> <w n="15.8">compte</w></l>
						<l n="16" num="4.4"><w n="16.1">De</w> <w n="16.2">ce</w> <w n="16.3">lait</w> <w n="16.4">qu</w>’<w n="16.5">une</w> <w n="16.6">mère</w> <w n="16.7">offre</w> <w n="16.8">avec</w> <w n="16.9">tant</w> <w n="16.10">d</w>’<w n="16.11">amour</w> !</l>
					</lg>
					<lg n="5">
						<l n="17" num="5.1"><w n="17.1">Buvez</w> ! <w n="17.2">En</w> <w n="17.3">étreignant</w> <w n="17.4">cette</w> <w n="17.5">femme</w> <w n="17.6">penchée</w></l>
						<l n="18" num="5.2"><space quantity="10" unit="char"></space><w n="18.1">Sur</w> <w n="18.2">son</w> <w n="18.3">fruit</w>,</l>
						<l n="19" num="5.3"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">la</w> <w n="19.4">vigne</w> <w n="19.5">céleste</w> <w n="19.6">à</w> <w n="19.7">la</w> <w n="19.8">terre</w> <w n="19.9">attachée</w></l>
						<l n="20" num="5.4"><w n="20.1">Dont</w> <w n="20.2">la</w> <w n="20.3">sève</w> <w n="20.4">s</w>’<w n="20.5">épanche</w> <w n="20.6">éternelle</w> <w n="20.7">et</w> <w n="20.8">sans</w> <w n="20.9">bruit</w>.</l>
					</lg>
				</div></body></text></TEI>