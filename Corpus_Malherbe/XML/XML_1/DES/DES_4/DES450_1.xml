<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES INÉDITES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3950 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_4</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Poésies inédites</title>
						<author>Madame Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>archive.org</publisher>
						<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Poésies Inédites</title>
								<author>Madame Desbordes-Valmore</author>
								<editor>Gustave Revilliod</editor>
								<idno type="URL">https://archive.org/details/posiesindite00desb</idno>
								<imprint>
									<pubPlace>Genève</pubPlace>
									<publisher>Impr. de J. Fick</publisher>
									<date when="1860">1860</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3950 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1860">1860</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les corrections mentionnées en errata ont été intégrées.</p>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES450">
					<head type="main">LE VOISIN BLESSÉ</head>
					<lg n="1">
						<l n="1" num="1.1"><space quantity="4" unit="char"></space><w n="1.1">L</w>’<w n="1.2">autre</w> <w n="1.3">nuit</w> <w n="1.4">le</w> <w n="1.5">voisin</w> <w n="1.6">qui</w> <w n="1.7">pleure</w></l>
						<l n="2" num="1.2"><space quantity="4" unit="char"></space><w n="2.1">Frappa</w> <w n="2.2">pour</w> <w n="2.3">me</w> <w n="2.4">dire</w> <w n="2.5">bonsoir</w> :</l>
						<l n="3" num="1.3"><space quantity="4" unit="char"></space>« <w n="3.1">Dormez</w>, <w n="3.2">voisin</w>, <w n="3.3">ce</w> <w n="3.4">n</w>’<w n="3.5">est</w> <w n="3.6">plus</w> <w n="3.7">l</w>’<w n="3.8">heure</w> ;</l>
						<l n="4" num="1.4"><space quantity="4" unit="char"></space><w n="4.1">On</w> <w n="4.2">n</w>’<w n="4.3">y</w> <w n="4.4">voit</w> <w n="4.5">plus</w> : <w n="4.6">il</w> <w n="4.7">faut</w> <w n="4.8">se</w> <w n="4.9">voir</w>.</l>
						<l n="5" num="1.5"><w n="5.1">Je</w> <w n="5.2">suis</w> <w n="5.3">vous</w> <w n="5.4">le</w> <w n="5.5">savez</w> <w n="5.6">une</w> <w n="5.7">pauvre</w> <w n="5.8">orpheline</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">Je</w> <w n="6.2">n</w>’<w n="6.3">ai</w> <w n="6.4">d</w>’<w n="6.5">autre</w> <w n="6.6">gardien</w> <w n="6.7">que</w> <w n="6.8">la</w> <w n="6.9">vierge</w> <w n="6.10">divine</w>. »</l>
						<l n="7" num="1.7"><space quantity="4" unit="char"></space><w n="7.1">Mais</w> <w n="7.2">il</w> <w n="7.3">reprit</w> <w n="7.4">si</w> <w n="7.5">tristement</w> :</l>
						<l n="8" num="1.8"><space quantity="4" unit="char"></space>« <w n="8.1">Au</w> <w n="8.2">pécheur</w> <w n="8.3">Dieu</w> <w n="8.4">donne</w> <w n="8.5">un</w> <w n="8.6">moment</w></l>
						<l n="9" num="1.9"><space quantity="4" unit="char"></space><w n="9.1">De</w> <w n="9.2">grâce</w> <w n="9.3">avant</w> <w n="9.4">le</w> <w n="9.5">châtiment</w> !… »</l>
					</lg>
					<lg n="2">
						<l n="10" num="2.1"><space quantity="4" unit="char"></space><w n="10.1">Il</w> <w n="10.2">dit</w> <w n="10.3">cela</w> <w n="10.4">d</w>’<w n="10.5">un</w> <w n="10.6">ton</w> <w n="10.7">si</w> <w n="10.8">grave</w></l>
						<l n="11" num="2.2"><space quantity="4" unit="char"></space><w n="11.1">Que</w> <w n="11.2">sa</w> <w n="11.3">voix</w> <w n="11.4">me</w> <w n="11.5">troubla</w> <w n="11.6">le</w> <w n="11.7">cœur</w>,</l>
						<l n="12" num="2.3"><space quantity="4" unit="char"></space><w n="12.1">Et</w> <w n="12.2">qu</w>’<w n="12.3">à</w> <w n="12.4">ce</w> <w n="12.5">blessé</w> <w n="12.6">doux</w> <w n="12.7">et</w> <w n="12.8">grave</w></l>
						<l n="13" num="2.4"><space quantity="4" unit="char"></space><w n="13.1">Je</w> <w n="13.2">répondis</w> <w n="13.3">malgré</w> <w n="13.4">ma</w> <w n="13.5">peur</w> :</l>
						<l n="14" num="2.5">« <w n="14.1">Vous</w> <w n="14.2">avez</w> <w n="14.3">votre</w> <w n="14.4">mère</w>, <w n="14.5">et</w> <w n="14.6">moi</w>, <w n="14.7">pauvre</w> <w n="14.8">orpheline</w></l>
						<l n="15" num="2.6"><w n="15.1">J</w>’<w n="15.2">en</w> <w n="15.3">vais</w> <w n="15.4">demander</w> <w n="15.5">une</w> <w n="15.6">à</w> <w n="15.7">la</w> <w n="15.8">vierge</w> <w n="15.9">divine</w>.</l>
						<l n="16" num="2.7"><space quantity="4" unit="char"></space><w n="16.1">Pourquoi</w> <w n="16.2">dites</w>-<w n="16.3">vous</w> <w n="16.4">tristement</w> :</l>
						<l n="17" num="2.8"><space quantity="4" unit="char"></space><w n="17.1">Au</w> <w n="17.2">pécheur</w> <w n="17.3">Dieu</w> <w n="17.4">donne</w> <w n="17.5">un</w> <w n="17.6">moment</w></l>
						<l n="18" num="2.9"><space quantity="4" unit="char"></space><w n="18.1">De</w> <w n="18.2">grâce</w> <w n="18.3">avant</w> <w n="18.4">le</w> <w n="18.5">châtiment</w> ?… »</l>
					</lg>
					<lg n="3">
						<l n="19" num="3.1"><space quantity="4" unit="char"></space>« <w n="19.1">La</w> <w n="19.2">grâce</w>, <w n="19.3">c</w>’<w n="19.4">est</w> <w n="19.5">votre</w> <w n="19.6">présence</w> ! »</l>
						<l n="20" num="3.2"><space quantity="4" unit="char"></space><w n="20.1">Cria</w>-<w n="20.2">t</w>-<w n="20.3">il</w> <w n="20.4">contre</w> <w n="20.5">la</w> <w n="20.6">cloison</w>.</l>
						<l n="21" num="3.3"><space quantity="4" unit="char"></space>« <w n="21.1">Le</w> <w n="21.2">châtiment</w>, <w n="21.3">c</w>’<w n="21.4">est</w> <w n="21.5">votre</w> <w n="21.6">absence</w>.</l>
						<l n="22" num="3.4"><space quantity="4" unit="char"></space><w n="22.1">Et</w> <w n="22.2">le</w> <w n="22.3">ciel</w>, <w n="22.4">c</w>’<w n="22.5">est</w> <w n="22.6">votre</w> <w n="22.7">maison</w> !</l>
						<l n="23" num="3.5"><w n="23.1">Je</w> <w n="23.2">suis</w> <w n="23.3">l</w>’<w n="23.4">heureux</w> <w n="23.5">voisin</w> <w n="23.6">de</w> <w n="23.7">la</w> <w n="23.8">jeune</w> <w n="23.9">orpheline</w></l>
						<l n="24" num="3.6"><w n="24.1">Qui</w> <w n="24.2">demande</w> <w n="24.3">une</w> <w n="24.4">mère</w> <w n="24.5">à</w> <w n="24.6">la</w> <w n="24.7">vierge</w> <w n="24.8">divine</w> ;</l>
						<l n="25" num="3.7"><space quantity="4" unit="char"></space><w n="25.1">C</w>’<w n="25.2">est</w> <w n="25.3">pourquoi</w> <w n="25.4">je</w> <w n="25.5">dis</w> <w n="25.6">tristement</w> :</l>
						<l n="26" num="3.8"><space quantity="4" unit="char"></space><w n="26.1">Au</w> <w n="26.2">pécheur</w> <w n="26.3">Dieu</w> <w n="26.4">donne</w> <w n="26.5">un</w> <w n="26.6">moment</w></l>
						<l n="27" num="3.9"><space quantity="4" unit="char"></space><w n="27.1">De</w> <w n="27.2">grâce</w> <w n="27.3">avant</w> <w n="27.4">le</w> <w n="27.5">châtiment</w> !</l>
					</lg>
					<lg n="4">
						<l n="28" num="4.1"><space quantity="4" unit="char"></space>« <w n="28.1">Car</w> <w n="28.2">vous</w> <w n="28.3">partez</w> <w n="28.4">avec</w> <w n="28.5">l</w>’<w n="28.6">aurore</w>,</l>
						<l n="29" num="4.2"><space quantity="4" unit="char"></space><w n="29.1">Et</w> <w n="29.2">moi</w>, <w n="29.3">blessé</w>, <w n="29.4">je</w> <w n="29.5">vais</w> <w n="29.6">mourir</w>… »</l>
						<l n="30" num="4.3"><space quantity="4" unit="char"></space>— « <w n="30.1">Voisin</w>, <w n="30.2">je</w> <w n="30.3">ne</w> <w n="30.4">pars</w> <w n="30.5">pas</w> <w n="30.6">encore</w></l>
						<l n="31" num="4.4"><space quantity="4" unit="char"></space><w n="31.1">Et</w> <w n="31.2">si</w> <w n="31.3">l</w>’<w n="31.4">on</w> <w n="31.5">pouvait</w> <w n="31.6">vous</w> <w n="31.7">guérir</w>…</l>
						<l n="32" num="4.5"><w n="32.1">Donnez</w>-<w n="32.2">moi</w> <w n="32.3">votre</w> <w n="32.4">mère</w>, <w n="32.5">et</w> <w n="32.6">la</w> <w n="32.7">pauvre</w> <w n="32.8">orpheline</w></l>
						<l n="33" num="4.6"><w n="33.1">Ne</w> <w n="33.2">demandera</w> <w n="33.3">rien</w> <w n="33.4">à</w> <w n="33.5">la</w> <w n="33.6">vierge</w> <w n="33.7">divine</w>.</l>
						<l n="34" num="4.7"><space quantity="4" unit="char"></space><w n="34.1">Ne</w> <w n="34.2">dites</w> <w n="34.3">donc</w> <w n="34.4">plus</w> <w n="34.5">tristement</w> :</l>
						<l n="35" num="4.8"><space quantity="4" unit="char"></space><w n="35.1">Au</w> <w n="35.2">pécheur</w> <w n="35.3">Dieu</w> <w n="35.4">donne</w> <w n="35.5">un</w> <w n="35.6">moment</w></l>
						<l n="36" num="4.9"><space quantity="4" unit="char"></space><w n="36.1">De</w> <w n="36.2">grâce</w> <w n="36.3">avant</w> <w n="36.4">le</w> <w n="36.5">châtiment</w> ! »</l>
					</lg>
				</div></body></text></TEI>