<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES304">
				<head type="main">AU POÈTE PROLÉTAIRE</head>
				<head type="sub_1">LE BRETON</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Vous</w>, <w n="1.2">que</w> <w n="1.3">j</w>’<w n="1.4">ai</w> <w n="1.5">vu</w> <w n="1.6">passer</w> <w n="1.7">dans</w> <w n="1.8">l</w>’<w n="1.9">été</w> <w n="1.10">de</w> <w n="1.11">votre</w> <w n="1.12">âge</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Portant</w> <w n="2.2">vos</w> <w n="2.3">jours</w> <w n="2.4">avec</w> <w n="2.5">un</w> <w n="2.6">digne</w> <w n="2.7">et</w> <w n="2.8">haut</w> <w n="2.9">courage</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Excitant</w> <w n="3.2">de</w> <w n="3.3">vos</w> <w n="3.4">bras</w> <w n="3.5">les</w> <w n="3.6">débiles</w> <w n="3.7">ressorts</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Chanter</w> <w n="4.2">sous</w> <w n="4.3">la</w> <w n="4.4">sueur</w> <w n="4.5">des</w> <w n="4.6">paternels</w> <w n="4.7">efforts</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Vous</w>, <w n="5.2">que</w> <w n="5.3">j</w>’<w n="5.4">ai</w> <w n="5.5">vu</w> <w n="5.6">sublime</w> <w n="5.7">et</w> <w n="5.8">renfermant</w> <w n="5.9">vos</w> <w n="5.10">ailes</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Vous</w> <w n="6.2">résigner</w> <w n="6.3">au</w> <w n="6.4">sol</w>, <w n="6.5">pareil</w> <w n="6.6">aux</w> <w n="6.7">hirondelles</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Qui</w>, <w n="7.2">pour</w> <w n="7.3">nourrir</w> <w n="7.4">leurs</w> <w n="7.5">nids</w>, <w n="7.6">percent</w> <w n="7.7">les</w> <w n="7.8">durs</w> <w n="7.9">sillons</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Et</w> <w n="8.2">partagent</w> <w n="8.3">le</w> <w n="8.4">grain</w> <w n="8.5">de</w> <w n="8.6">milliers</w> <w n="8.7">d</w>’<w n="8.8">oisillons</w> :</l>
					<l n="9" num="1.9">« <w n="9.1">Pourquoi</w> <w n="9.2">vous</w> <w n="9.3">ai</w>-<w n="9.4">je</w> <w n="9.5">vu</w> <w n="9.6">tout</w>-<w n="9.7">à</w>-<w n="9.8">coup</w> <w n="9.9">triste</w> <w n="9.10">et</w> <w n="9.11">pâle</w>,</l>
					<l n="10" num="1.10"><w n="10.1">Couvrir</w> <w n="10.2">de</w> <w n="10.3">vos</w> <w n="10.4">deux</w> <w n="10.5">mains</w> <w n="10.6">vos</w> <w n="10.7">traits</w> <w n="10.8">brûlés</w> <w n="10.9">de</w> <w n="10.10">hâle</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Tel</w> <w n="11.2">qu</w>’<w n="11.3">un</w> <w n="11.4">homme</w> <w n="11.5">hâté</w> <w n="11.6">s</w>’<w n="11.7">arrête</w> <w n="11.8">de</w> <w n="11.9">courir</w>,</l>
					<l n="12" num="1.12"><w n="12.1">Et</w> <w n="12.2">dit</w> <w n="12.3">en</w> <w n="12.4">lui</w> : <w n="12.5">C</w>’<w n="12.6">est</w> <w n="12.7">vrai</w>, <w n="12.8">pourtant</w>, <w n="12.9">il</w> <w n="12.10">faut</w> <w n="12.11">mourir</w> !</l>
					<l n="13" num="1.13"><w n="13.1">Puis</w> <w n="13.2">qui</w> <w n="13.3">reprend</w> <w n="13.4">sa</w> <w n="13.5">route</w> <w n="13.6">avec</w> <w n="13.7">la</w> <w n="13.8">tête</w> <w n="13.9">basse</w>,</l>
					<l n="14" num="1.14"><w n="14.1">Comme</w> <w n="14.2">si</w> <w n="14.3">d</w>’<w n="14.4">un</w> <w n="14.5">fardeau</w> <w n="14.6">son</w> <w n="14.7">épaule</w> <w n="14.8">était</w> <w n="14.9">lasse</w> ?</l>
					<l n="15" num="1.15"><w n="15.1">Ah</w> <w n="15.2">C</w>’<w n="15.3">est</w> <w n="15.4">que</w> <w n="15.5">des</w> <w n="15.6">points</w> <w n="15.7">noirs</w> <w n="15.8">troublent</w> <w n="15.9">un</w> <w n="15.10">ciel</w> <w n="15.11">vermeil</w>,</l>
					<l n="16" num="1.16"><w n="16.1">Quand</w> <w n="16.2">nos</w> <w n="16.3">yeux</w> <w n="16.4">éblouis</w> <w n="16.5">ont</w> <w n="16.6">vu</w> <w n="16.7">trop</w> <w n="16.8">de</w> <w n="16.9">soleil</w>.</l>
				</lg>
				<lg n="2">
					<l n="17" num="2.1"><w n="17.1">C</w>’<w n="17.2">est</w> <w n="17.3">qu</w>’<w n="17.4">on</w> <w n="17.5">n</w>’<w n="17.6">a</w> <w n="17.7">pas</w> <w n="17.8">encore</w>, <w n="17.9">à</w> <w n="17.10">chaque</w> <w n="17.11">âme</w> <w n="17.12">qui</w> <w n="17.13">tombe</w>,</l>
					<l n="18" num="2.2"><w n="18.1">Aplani</w> <w n="18.2">le</w> <w n="18.3">chemin</w> <w n="18.4">du</w> <w n="18.5">baptême</w> <w n="18.6">à</w> <w n="18.7">la</w> <w n="18.8">tombe</w> ;</l>
					<l n="19" num="2.3"><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">qu</w>’<w n="19.4">à</w> <w n="19.5">cette</w> <w n="19.6">âme</w> <w n="19.7">en</w> <w n="19.8">pleurs</w> <w n="19.9">de</w> <w n="19.10">sa</w> <w n="19.11">chute</w> <w n="19.12">du</w> <w n="19.13">ciel</w>,</l>
					<l n="20" num="2.4"><w n="20.1">On</w> <w n="20.2">refuse</w> <w n="20.3">déjà</w> <w n="20.4">le</w> <w n="20.5">froment</w> <w n="20.6">et</w> <w n="20.7">le</w> <w n="20.8">sel</w> ;</l>
					<l n="21" num="2.5"><w n="21.1">C</w>’<w n="21.2">est</w> <w n="21.3">qu</w>’<w n="21.4">il</w> <w n="21.5">ne</w> <w n="21.6">passe</w> <w n="21.7">pas</w> <w n="21.8">franc</w>, <w n="21.9">de</w> <w n="21.10">port</w> <w n="21.11">sur</w> <w n="21.12">la</w> <w n="21.13">terre</w>,</l>
					<l n="22" num="2.6"><w n="22.1">Ce</w> <w n="22.2">problème</w>, <w n="22.3">scellé</w> <w n="22.4">d</w>’<w n="22.5">espoir</w> <w n="22.6">et</w> <w n="22.7">de</w> <w n="22.8">mystère</w> ;</l>
					<l n="23" num="2.7"><w n="23.1">C</w>’<w n="23.2">est</w> <w n="23.3">que</w> <w n="23.4">Ton</w> <w n="23.5">ne</w> <w n="23.6">veut</w> <w n="23.7">pas</w>, <w n="23.8">même</w> <w n="23.9">an</w> <w n="23.10">prix</w> <w n="23.11">de</w> <w n="23.12">travail</w>,</l>
					<l n="24" num="2.8"><w n="24.1">Laisser</w> <w n="24.2">l</w>’<w n="24.3">herbe</w> <w n="24.4">au</w> <w n="24.5">troupeau</w> <w n="24.6">sans</w> <w n="24.7">pâtre</w> <w n="24.8">et</w> <w n="24.9">sans</w> <w n="24.10">bercail</w>.</l>
					<l n="25" num="2.9"><w n="25.1">Le</w> <w n="25.2">travail</w> ! <w n="25.3">le</w> <w n="25.4">travail</w>, <w n="25.5">et</w> <w n="25.6">le</w> <w n="25.7">pain</w> <w n="25.8">sans</w> <w n="25.9">aumône</w>,</l>
					<l n="26" num="2.10"><w n="26.1">Dieu</w> <w n="26.2">l</w>’<w n="26.3">a</w> <w n="26.4">semé</w> <w n="26.5">pour</w>-<w n="26.6">tous</w> : <w n="26.7">on</w> <w n="26.8">nous</w>-<w n="26.9">prend</w> <w n="26.10">ce</w> <w n="26.11">qu</w>’<w n="26.12">il</w> <w n="26.13">donne</w>.</l>
					<l n="27" num="2.11"><w n="27.1">Hélas</w> ! <w n="27.2">hélas</w> ! <w n="27.3">ma</w> <w n="27.4">mère</w> <w n="27.5">a</w> <w n="27.6">pleuré</w> <w n="27.7">pour</w> <w n="27.8">du</w> <w n="27.9">pain</w> !</l>
					<l n="28" num="2.12"><w n="28.1">Hélas</w> ! <w n="28.2">j</w>’<w n="28.3">ai</w> <w n="28.4">vu</w> <w n="28.5">mourir</w> <w n="28.6">de</w> <w n="28.7">froidure</w> <w n="28.8">et</w> <w n="28.9">de</w> <w n="28.10">faim</w> !</l>
					<l n="29" num="2.13"><w n="29.1">Hélas</w> ! <w n="29.2">quand</w> <w n="29.3">la</w> <w n="29.4">faim</w> <w n="29.5">gronde</w> <w n="29.6">au</w> <w n="29.7">cœur</w> <w n="29.8">d</w>’<w n="29.9">une</w> <w n="29.10">famille</w>,</l>
					<l n="30" num="2.14"><w n="30.1">Quand</w> <w n="30.2">la</w> <w n="30.3">mère</w> <w n="30.4">au</w> <w n="30.5">foyer</w> <w n="30.6">voit</w> <w n="30.7">chanceler</w> <w n="30.8">sa</w> <w n="30.9">fille</w>,</l>
					<l n="31" num="2.15"><w n="31.1">Quand</w> <w n="31.2">tout</w> <w n="31.3">y</w> <w n="31.4">devient</w> <w n="31.5">froid</w>, <w n="31.6">jusqu</w>’<w n="31.7">aux</w> <w n="31.8">pleurs</w> <w n="31.9">de</w> <w n="31.10">leurs</w> <w n="31.11">yeux</w>,</l>
					<l n="32" num="2.16"><w n="32.1">Qu</w>’<w n="32.2">elles</w> <w n="32.3">n</w>’<w n="32.4">ont</w> <w n="32.5">plus</w> <w n="32.6">de</w> <w n="32.7">voix</w> <w n="32.8">pour</w> <w n="32.9">l</w>’<w n="32.10">élever</w> <w n="32.11">aux</w> <w n="32.12">cieux</w> ;</l>
					<l n="33" num="2.17"><w n="33.1">Quand</w> <w n="33.2">les</w> <w n="33.3">petits</w> <w n="33.4">enfans</w>, <w n="33.5">bégayant</w> <w n="33.6">leurs</w> <w n="33.7">prières</w>,</l>
					<l n="34" num="2.18"><w n="34.1">Alors</w> <w n="34.2">qu</w>’<w n="34.3">un</w> <w n="34.4">doigt</w> <w n="34.5">de</w> <w n="34.6">plomb</w> <w n="34.7">pèse</w> <w n="34.8">sur</w> <w n="34.9">leurs</w> <w n="34.10">paupières</w>,</l>
					<l n="35" num="2.19"><w n="35.1">Tâchent</w> <w n="35.2">de</w> <w n="35.3">dire</w> <w n="35.4">encore</w> <w n="35.5">à</w> <w n="35.6">leur</w> <w n="35.7">ange</w> <w n="35.8">gardien</w> :</l>
					<l n="36" num="2.20">« <w n="36.1">Donnez</w>-<w n="36.2">nous</w> <w n="36.3">aujourd</w>’<w n="36.4">hui</w> <w n="36.5">notre</w> <w n="36.6">pain</w> <w n="36.7">quotidien</w> ! »</l>
					<l n="37" num="2.21"><w n="37.1">Mon</w> <w n="37.2">frère</w>, <w n="37.3">n</w>’<w n="37.4">est</w>-<w n="37.5">ce</w> <w n="37.6">pas</w> <w n="37.7">que</w> <w n="37.8">la</w> <w n="37.9">mère</w> <w n="37.10">est</w> <w n="37.11">sublime</w>,</l>
					<l n="38" num="2.22"><w n="38.1">Si</w> <w n="38.2">ses</w> <w n="38.3">flancs</w> <w n="38.4">déchirés</w> <w n="38.5">n</w>’<w n="38.6">enfantent</w> <w n="38.7">pas</w> <w n="38.8">un</w> <w n="38.9">crime</w> ;</l>
					<l n="39" num="2.23"><w n="39.1">Si</w> <w n="39.2">F</w> <w n="39.3">air</w> <w n="39.4">ne</w> <w n="39.5">bondit</w> <w n="39.6">pas</w> <w n="39.7">des</w> <w n="39.8">sanglots</w> <w n="39.9">du</w> <w n="39.10">tocsin</w>,</l>
					<l n="40" num="2.24"><w n="40.1">Que</w> <w n="40.2">son</w> <w n="40.3">remords</w> <w n="40.4">alors</w> <w n="40.5">ne</w> <w n="40.6">peut</w> <w n="40.7">plus</w> <w n="40.8">interrompre</w> ;</l>
					<l n="41" num="2.25"><w n="41.1">Et</w> <w n="41.2">si</w> <w n="41.3">comme</w> <w n="41.4">une</w> <w n="41.5">épée</w>, <w n="41.6">ils</w> <w n="41.7">frappent</w> <w n="41.8">à</w> <w n="41.9">le</w> <w n="41.10">rompre</w></l>
					<l n="42" num="2.26"><space quantity="6" unit="char"></space><w n="42.1">Les</w> <w n="42.2">fibres</w> <w n="42.3">tendus</w> <w n="42.4">de</w> <w n="42.5">son</w> <w n="42.6">sein</w> !</l>
				</lg>
				<lg n="3">
					<l n="43" num="3.1"><w n="43.1">Mais</w>, <w n="43.2">vous</w> <w n="43.3">savez</w> <w n="43.4">aussi</w> <w n="43.5">comment</w> <w n="43.6">la</w> <w n="43.7">pitié</w> <w n="43.8">vole</w>,</l>
					<l n="44" num="3.2"><w n="44.1">Et</w> <w n="44.2">suspend</w> <w n="44.3">la</w> <w n="44.4">révolte</w>, <w n="44.5">et</w> <w n="44.6">s</w>’<w n="44.7">empresse</w> <w n="44.8">au</w> <w n="44.9">secours</w> ;</l>
					<l n="45" num="3.3"><w n="45.1">Et</w> <w n="45.2">comment</w>, <w n="45.3">jusqu</w>’<w n="45.4">au</w> <w n="45.5">cœur</w>, <w n="45.6">sa</w> <w n="45.7">limpide</w> <w n="45.8">parole</w></l>
					<l n="46" num="3.4"><w n="46.1">Infiltre</w> <w n="46.2">l</w>’<w n="46.3">eau</w> <w n="46.4">divine</w> <w n="46.5">avec</w> <w n="46.6">les</w> <w n="46.7">saints</w> <w n="46.8">discours</w>.</l>
					<l n="47" num="3.5"><w n="47.1">Moi</w>, <w n="47.2">je</w> <w n="47.3">sais</w> <w n="47.4">qu</w>’<w n="47.5">elle</w> <w n="47.6">a</w> <w n="47.7">dit</w> <w n="47.8">au</w> <w n="47.9">lit</w> <w n="47.10">de</w> <w n="47.11">mon</w> <w n="47.12">aïeule</w> :</l>
					<l n="48" num="3.6">« <w n="48.1">Femme</w>, <w n="48.2">ne</w> <w n="48.3">mourez</w> <w n="48.4">pas</w> : <w n="48.5">levez</w>-<w n="48.6">vous</w>, <w n="48.7">me</w> <w n="48.8">voici</w> ! »</l>
					<l n="49" num="3.7"><w n="49.1">Et</w> <w n="49.2">plus</w> <w n="49.3">tendre</w>, <w n="49.4">et</w> <w n="49.5">plus</w> <w n="49.6">bas</w>, <w n="49.7">pour</w> <w n="49.8">le</w> <w n="49.9">dire</w> <w n="49.10">à</w> <w n="49.11">moi</w> <w n="49.12">seule</w> :</l>
					<l n="50" num="3.8">« <w n="50.1">Toi</w> <w n="50.2">qui</w> <w n="50.3">devais</w> <w n="50.4">donner</w>, <w n="50.5">pauvre</w> <w n="50.6">ange</w>, <w n="50.7">prends</w> <w n="50.8">aussi</w>. »</l>
					<l n="51" num="3.9"><w n="51.1">C</w>’<w n="51.2">était</w>… <w n="51.3">oh</w> ! <w n="51.4">c</w>’<w n="51.5">était</w> <w n="51.6">vous</w>, <w n="51.7">mon</w> <w n="51.8">Dieu</w> ! <w n="51.9">j</w>’<w n="51.10">y</w> <w n="51.11">crois</w> <w n="51.12">encore</w>.</l>
					<l n="52" num="3.10"><w n="52.1">Oui</w>, <w n="52.2">frère</w>, <w n="52.3">c</w>’<w n="52.4">était</w> <w n="52.5">Dieu</w>, <w n="52.6">ce</w> <w n="52.7">Père</w> <w n="52.8">que</w> <w n="52.9">j</w>’<w n="52.10">adore</w>,</l>
					<l n="53" num="3.11"><w n="53.1">Ce</w> <w n="53.2">Roi</w>, <w n="53.3">que</w> <w n="53.4">son</w> <w n="53.5">enfant</w> <w n="53.6">n</w>’<w n="53.7">appelle</w> <w n="53.8">pas</w> <w n="53.9">en</w> <w n="53.10">vain</w> ;</l>
					<l n="54" num="3.12"><w n="54.1">Dont</w> <w n="54.2">le</w> <w n="54.3">sang</w> <w n="54.4">a</w> <w n="54.5">coulé</w> <w n="54.6">dans</w> <w n="54.7">ma</w> <w n="54.8">soif</w> <w n="54.9">et</w> <w n="54.10">ma</w> <w n="54.11">faim</w> !</l>
				</lg>
				<lg n="4">
					<l n="55" num="4.1"><w n="55.1">Ainsi</w> <w n="55.2">vous</w> <w n="55.3">avez</w> <w n="55.4">beau</w> <w n="55.5">chanter</w>, <w n="55.6">père</w> <w n="55.7">et</w> <w n="55.8">poète</w>,</l>
					<l n="56" num="4.2"><w n="56.1">Beau</w> <w n="56.2">mesurer</w> <w n="56.3">votre</w> <w n="56.4">âme</w> <w n="56.5">et</w> <w n="56.6">relever</w> <w n="56.7">la</w> <w n="56.8">tète</w>,</l>
					<l n="57" num="4.3"><w n="57.1">Beau</w> <w n="57.2">crier</w>, <w n="57.3">plein</w> <w n="57.4">d</w>’<w n="57.5">haleine</w> : <w n="57.6">À</w> <w n="57.7">moi</w> <w n="57.8">la</w> <w n="57.9">vie</w> ! <w n="57.10">à</w> <w n="57.11">moi</w> !</l>
					<l n="58" num="4.4"><w n="58.1">La</w> <w n="58.2">terre</w> <w n="58.3">est</w> <w n="58.4">toute</w> <w n="58.5">à</w> <w n="58.6">l</w>’<w n="58.7">homme</w>, <w n="58.8">et</w> <w n="58.9">l</w>’<w n="58.10">homme</w> <w n="58.11">en</w> <w n="58.12">est</w> <w n="58.13">le</w> <w n="58.14">roi</w> ;</l>
					<l n="59" num="4.5"><w n="59.1">Vous</w> <w n="59.2">sentez</w> <w n="59.3">par</w> <w n="59.4">secousse</w> <w n="59.5">une</w> <w n="59.6">chaîne</w> <w n="59.7">inconnue</w>,</l>
					<l n="60" num="4.6"><w n="60.1">Dans</w> <w n="60.2">la</w> <w n="60.3">prison</w> <w n="60.4">de</w> <w n="60.5">chair</w> <w n="60.6">où</w> <w n="60.7">votre</w> <w n="60.8">âme</w> <w n="60.9">est</w> <w n="60.10">venue</w>.</l>
					<l n="61" num="4.7"><w n="61.1">Vous</w> <w n="61.2">ayez</w> <w n="61.3">beau</w> <w n="61.4">prétendre</w> <w n="61.5">à</w> <w n="61.6">vos</w> <w n="61.7">trésors</w> <w n="61.8">épars</w> ;</l>
					<l n="62" num="4.8"><w n="62.1">Vos</w> <w n="62.2">trésors</w> <w n="62.3">envahis</w> <w n="62.4">glissent</w> <w n="62.5">de</w> <w n="62.6">toutes</w> <w n="62.7">parts</w>.</l>
					<l n="63" num="4.9"><w n="63.1">Dans</w> <w n="63.2">la</w> <w n="63.3">foule<hi rend="ital">de</hi></w> <w n="63.4">rois</w> <w n="63.5">où</w> <w n="63.6">vous</w> <w n="63.7">perdez</w> <w n="63.8">votre</w> <w n="63.9">ange</w>,</l>
					<l n="64" num="4.10"><w n="64.1">Il</w> <w n="64.2">ne</w> <w n="64.3">vous</w> <w n="64.4">reste</w> <w n="64.5">rien</w> <w n="64.6">qu</w>’<w n="64.7">une</w> <w n="64.8">fatigue</w> <w n="64.9">étrange</w>,</l>
					<l n="65" num="4.11"><w n="65.1">Une</w> <w n="65.2">langueur</w> <w n="65.3">de</w> <w n="65.4">vivre</w>, <w n="65.5">une</w> <w n="65.6">soif</w> <w n="65.7">de</w> <w n="65.8">sommeil</w>,</l>
					<l n="66" num="4.12"><w n="66.1">Et</w> <w n="66.2">toujours</w> <w n="66.3">la</w> <w n="66.4">misère</w> <w n="66.5">assidue</w> <w n="66.6">an</w> <w n="66.7">réveil</w> ;</l>
					<l n="67" num="4.13"><w n="67.1">La</w> <w n="67.2">misère</w> <w n="67.3">au</w> <w n="67.4">milieu</w> <w n="67.5">du</w> <w n="67.6">grand</w> <w n="67.7">Éden</w> ! <w n="67.8">méchante</w></l>
					<l n="68" num="4.14"><w n="68.1">Au</w> <w n="68.2">passereau</w> <w n="68.3">qui</w> <w n="68.4">vole</w>, <w n="68.5">au</w> <w n="68.6">rossignol</w> <w n="68.7">qui</w> <w n="68.8">chante</w>,</l>
					<l n="69" num="4.15"><w n="69.1">À</w> <w n="69.2">la</w> <w n="69.3">fleur</w> <w n="69.4">qui</w> <w n="69.5">veut</w> <w n="69.6">naître</w> <w n="69.7">et</w> <w n="69.8">qui</w> <w n="69.9">n</w>’<w n="69.10">ose</w> <w n="69.11">éclater</w>,</l>
					<l n="70" num="4.16"><w n="70.1">Au</w> <w n="70.2">germe</w> <w n="70.3">qui</w> <w n="70.4">veut</w> <w n="70.5">vivre</w> <w n="70.6">et</w> <w n="70.7">ne</w> <w n="70.8">peut</w> <w n="70.9">palpiter</w> ;</l>
					<l n="71" num="4.17"><w n="71.1">L</w>’<w n="71.2">âpre</w> <w n="71.3">misère</w> <w n="71.4">enfin</w>, <w n="71.5">cette</w> <w n="71.6">bise</w> <w n="71.7">inflexible</w>,</l>
					<l n="72" num="4.18"><w n="72.1">Qui</w> <w n="72.2">détruit</w> <w n="72.3">lentement</w> <w n="72.4">ce</w> <w n="72.5">que</w> <w n="72.6">Dieu</w> <w n="72.7">fit</w> <w n="72.8">sensible</w> ;</l>
					<l n="73" num="4.19"><w n="73.1">Dont</w> <w n="73.2">le</w> <w n="73.3">pâle</w> <w n="73.4">baiser</w> <w n="73.5">gèle</w> <w n="73.6">l</w>’<w n="73.7">arbre</w> <w n="73.8">et</w> <w n="73.9">le</w> <w n="73.10">fruit</w> ;</l>
					<l n="74" num="4.20"><w n="74.1">Elle</w> <w n="74.2">pousse</w> <w n="74.3">ma</w> <w n="74.4">porte</w>, <w n="74.5">où</w> <w n="74.6">s</w>’<w n="74.7">élève</w> <w n="74.8">sans</w> <w n="74.9">bruit</w></l>
					<l n="75" num="4.21"><w n="75.1">La</w> <w n="75.2">prière</w> <w n="75.3">toujours</w> <w n="75.4">allumant</w> <w n="75.5">son</w> <w n="75.6">sourire</w>,</l>
					<l n="76" num="4.22"><w n="76.1">Quand</w> <w n="76.2">l</w>’<w n="76.3">ange</w> <w n="76.4">gardien</w> <w n="76.5">passe</w> <w n="76.6">et</w> <w n="76.7">m</w>’<w n="76.8">aide</w> <w n="76.9">à</w> <w n="76.10">la</w> <w n="76.11">mieux</w> <w n="76.12">dire</w>.</l>
					<l n="77" num="4.23"><w n="77.1">Moi</w>, <w n="77.2">j</w>’<w n="77.3">ai</w> <w n="77.4">toujours</w> <w n="77.5">au</w> <w n="77.6">cœur</w> <w n="77.7">le</w> <w n="77.8">répit</w> <w n="77.9">d</w>’<w n="77.10">un</w> <w n="77.11">tourment</w>,</l>
					<l n="78" num="4.24"><w n="78.1">Quand</w> <w n="78.2">ma</w> <w n="78.3">pensée</w> <w n="78.4">à</w> <w n="78.5">Dieu</w> <w n="78.6">s</w>’<w n="78.7">envole</w> <w n="78.8">librement</w> !</l>
				</lg>
				<lg n="5">
					<l n="79" num="5.1"><w n="79.1">Allez</w> ! <w n="79.2">je</w> <w n="79.3">vous</w> <w n="79.4">devine</w>, <w n="79.5">et</w> <w n="79.6">j</w>’<w n="79.7">ai</w> <w n="79.8">ma</w> <w n="79.9">soif</w> <w n="79.10">déçue</w>,</l>
					<l n="80" num="5.2"><w n="80.1">Ma</w> <w n="80.2">douce</w> <w n="80.3">royauté</w> <w n="80.4">vainement</w> <w n="80.5">aperçue</w> ;</l>
					<l n="81" num="5.3"><w n="81.1">J</w>’<w n="81.2">ai</w> <w n="81.3">mes</w> <w n="81.4">chants</w> <w n="81.5">commencés</w> <w n="81.6">qui</w> <w n="81.7">s</w>’<w n="81.8">écoulent</w> <w n="81.9">en</w> <w n="81.10">pleurs</w> ;</l>
					<l n="82" num="5.4"><w n="82.1">Mes</w> <w n="82.2">épines</w> <w n="82.3">au</w> <w n="82.4">front</w> <w n="82.5">que</w> <w n="82.6">je</w> <w n="82.7">croyais</w> <w n="82.8">des</w> <w n="82.9">fleurs</w> !</l>
					<l n="83" num="5.5"><w n="83.1">L</w>’<w n="83.2">amour</w> <w n="83.3">m</w>’<w n="83.4">a</w> <w n="83.5">fait</w> <w n="83.6">présent</w> <w n="83.7">d</w>’<w n="83.8">une</w> <w n="83.9">tendresse</w> <w n="83.10">amère</w>,</l>
					<l n="84" num="5.6"><w n="84.1">Du</w> <w n="84.2">doux</w> <w n="84.3">remords</w> <w n="84.4">d</w>’<w n="84.5">aimer</w> <w n="84.6">et</w> <w n="84.7">d</w>’<w n="84.8">oser</w> <w n="84.9">être</w> <w n="84.10">mère</w>.</l>
					<l n="85" num="5.7"><w n="85.1">En</w> <w n="85.2">regardant</w> <w n="85.3">pâlir</w> <w n="85.4">des</w> <w n="85.5">fronts</w> <w n="85.6">purs</w> <w n="85.7">et</w> <w n="85.8">charmans</w>,</l>
					<l n="86" num="5.8"><w n="86.1">On</w> <w n="86.2">a</w> <w n="86.3">peur</w> <w n="86.4">d</w>’<w n="86.5">attirer</w> <w n="86.6">sur</w> <w n="86.7">eux</w> <w n="86.8">ses</w> <w n="86.9">châtimens</w> ;</l>
					<l n="87" num="5.9"><w n="87.1">On</w> <w n="87.2">a</w> <w n="87.3">peur</w> <w n="87.4">d</w>’<w n="87.5">égarer</w> <w n="87.6">une</w> <w n="87.7">âme</w> <w n="87.8">aux</w> <w n="87.9">blanches</w> <w n="87.10">ailes</w></l>
					<l n="88" num="5.10"><w n="88.1">Dans</w> <w n="88.2">les</w> <w n="88.3">sentiers</w> <w n="88.4">souillés</w> <w n="88.5">par</w> <w n="88.6">tant</w> <w n="88.7">d</w>’<w n="88.8">âmes</w> <w n="88.9">cruelles</w>,</l>
					<l n="89" num="5.11"><w n="89.1">Ou</w> <w n="89.2">de</w> <w n="89.3">les</w> <w n="89.4">voir</w> <w n="89.5">nager</w> <w n="89.6">dans</w> <w n="89.7">les</w> <w n="89.8">flots</w> <w n="89.9">turbulens</w></l>
					<l n="90" num="5.12"><w n="90.1">Qui</w> <w n="90.2">viennent</w> <w n="90.3">d</w>’<w n="90.4">épuiser</w> <w n="90.5">nos</w> <w n="90.6">bras</w> <w n="90.7">vains</w> <w n="90.8">et</w> <w n="90.9">tremblans</w>.</l>
				</lg>
				<lg n="6">
					<l n="91" num="6.1"><w n="91.1">Ces</w> <w n="91.2">beaux</w> <w n="91.3">enfans</w>, <w n="91.4">si</w> <w n="91.5">fiers</w> <w n="91.6">d</w>’<w n="91.7">entrer</w> <w n="91.8">dans</w> <w n="91.9">nos</w> <w n="91.10">orages</w>,</l>
					<l n="92" num="6.2"><w n="92.1">Rêvant</w> <w n="92.2">leurs</w> <w n="92.3">horizons</w>, <w n="92.4">leurs</w> <w n="92.5">jardins</w>, <w n="92.6">leurs</w> <w n="92.7">ombrages</w>,</l>
					<l n="93" num="6.3"><w n="93.1">Moi</w>, <w n="93.2">quand</w> <w n="93.3">je</w> <w n="93.4">les</w> <w n="93.5">vois</w> <w n="93.6">rire</w> <w n="93.7">à</w> <w n="93.8">ce</w> <w n="93.9">prisme</w> <w n="93.10">trompeur</w>,</l>
					<l n="94" num="6.4"><w n="94.1">Je</w> <w n="94.2">veux</w> <w n="94.3">rire</w>, <w n="94.4">et</w> <w n="94.5">je</w> <w n="94.6">fonds</w> <w n="94.7">en</w> <w n="94.8">larmes</w> <w n="94.9">dans</w> <w n="94.10">mon</w> <w n="94.11">cœur</w>.</l>
					<l n="95" num="6.5"><w n="95.1">Et</w> <w n="95.2">vous</w>, <w n="95.3">n</w>’<w n="95.4">avez</w>-<w n="95.5">vous</w> <w n="95.6">pas</w> <w n="95.7">de</w> <w n="95.8">ces</w> <w n="95.9">pitiés</w> <w n="95.10">profondes</w></l>
					<l n="96" num="6.6"><w n="96.1">Qui</w> <w n="96.2">vous</w> <w n="96.3">percent</w> <w n="96.4">le</w> <w n="96.5">sein</w>, <w n="96.6">comme</w> <w n="96.7">feraient</w> <w n="96.8">les</w> <w n="96.9">ondes</w></l>
					<l n="97" num="6.7"><w n="97.1">En</w> <w n="97.2">creusant</w> <w n="97.3">goutte</w> <w n="97.4">à</w> <w n="97.5">goutte</w> <w n="97.6">un</w> <w n="97.7">caillou</w> ? <w n="97.8">Mille</w> <w n="97.9">fois</w></l>
					<l n="98" num="6.8"><w n="98.1">J</w>’<w n="98.2">ai</w> <w n="98.3">voulu</w> <w n="98.4">les</w> <w n="98.5">instruire</w>, <w n="98.6">et</w> <w n="98.7">j</w>’<w n="98.8">ai</w> <w n="98.9">gardé</w> <w n="98.10">ma</w> <w n="98.11">voix</w>.</l>
					<l n="99" num="6.9"><w n="99.1">Que</w> <w n="99.2">fait</w> <w n="99.3">la</w> <w n="99.4">chèvre</w> <w n="99.5">errante</w> <w n="99.6">au</w> <w n="99.7">rocher</w> <w n="99.8">suspendue</w>,</l>
					<l n="100" num="6.10"><w n="100.1">Qui</w> <w n="100.2">rêve</w> <w n="100.3">et</w> <w n="100.4">se</w> <w n="100.5">repent</w> <w n="100.6">de</w> <w n="100.7">sa</w> <w n="100.8">route</w> <w n="100.9">perdue</w> ?</l>
					<l n="101" num="6.11"><w n="101.1">Ose</w>-<w n="101.2">t</w>-<w n="101.3">elle</w> <w n="101.4">effrayer</w>, <w n="101.5">penchés</w> <w n="101.6">sur</w> <w n="101.7">le</w> <w n="101.8">torrent</w></l>
					<l n="102" num="6.12"><w n="102.1">Les</w> <w n="102.2">chevreaux</w> <w n="102.3">pris</w> <w n="102.4">aux</w> <w n="102.5">fleurs</w> <w n="102.6">qu</w>’<w n="102.7">emporte</w> <w n="102.8">le</w> <w n="102.9">courant</w>.</l>
				</lg>
				<lg n="7">
					<l n="103" num="7.1"><w n="103.1">Qu</w>’<w n="103.2">irions</w>-<w n="103.3">nous</w> <w n="103.4">raconter</w> <w n="103.5">à</w> <w n="103.6">leurs</w> <w n="103.7">jeunes</w> <w n="103.8">oreilles</w> ?</l>
					<l n="104" num="7.2"><w n="104.1">Que</w> <w n="104.2">sert</w> <w n="104.3">d</w>’<w n="104.4">en</w> <w n="104.5">soulever</w> <w n="104.6">les</w> <w n="104.7">couronnes</w> <w n="104.8">vermeilles</w>,</l>
					<l n="105" num="7.3"><w n="105.1">Dont</w> <w n="105.2">il</w> <w n="105.3">plaît</w> <w n="105.4">au</w> <w n="105.5">printemps</w> <w n="105.6">d</w>’<w n="105.7">assourdir</w> <w n="105.8">leur</w> <w n="105.9">raison</w> ;</l>
					<l n="106" num="7.4"><w n="106.1">Ils</w> <w n="106.2">ont</w> <w n="106.3">le</w> <w n="106.4">temps</w>, <w n="106.5">pas</w> <w n="106.6">vrai</w> : <w n="106.7">tout</w> <w n="106.8">vient</w> <w n="106.9">dans</w> <w n="106.10">sa</w> <w n="106.11">saison</w>.</l>
					<l n="107" num="7.5"><w n="107.1">Oh</w> ! <w n="107.2">laissons</w>-<w n="107.3">les</w> <w n="107.4">aller</w> <w n="107.5">sans</w> <w n="107.6">gêner</w> <w n="107.7">leur</w> <w n="107.8">croissance</w> ;</l>
					<l n="108" num="7.6"><w n="108.1">Oh</w> ! <w n="108.2">dans</w> <w n="108.3">leur</w> <w n="108.4">vie</w> <w n="108.5">à</w> <w n="108.6">jour</w> <w n="108.7">n</w>’<w n="108.8">ont</w>-<w n="108.9">ils</w> <w n="108.10">pas</w> <w n="108.11">l</w>’<w n="108.12">innocence</w> ?</l>
					<l n="109" num="7.7"><w n="109.1">Au</w> <w n="109.2">pied</w> <w n="109.3">d</w>’<w n="109.4">un</w> <w n="109.5">nid</w> <w n="109.6">chantant</w> <w n="109.7">parle</w>-<w n="109.8">t</w>-<w n="109.9">on</w> <w n="109.10">d</w>’<w n="109.11">oiseleur</w> ?</l>
					<l n="110" num="7.8"><w n="110.1">Tournons</w>-<w n="110.2">les</w> <w n="110.3">au</w> <w n="110.4">soleil</w> <w n="110.5">et</w> <w n="110.6">restons</w> <w n="110.7">au</w> <w n="110.8">malheur</w> !</l>
				</lg>
				<lg n="8">
					<l n="111" num="8.1"><w n="111.1">Ou</w> <w n="111.2">plutôt</w>, <w n="111.3">suivons</w>-<w n="111.4">les</w> : <w n="111.5">quelle</w> <w n="111.6">que</w> <w n="111.7">soit</w> <w n="111.8">la</w> <w n="111.9">route</w>,</l>
					<l n="112" num="8.2"><w n="112.1">Nous</w> <w n="112.2">montons</w>, <w n="112.3">j</w>’<w n="112.4">en</w> <w n="112.5">suis</w> <w n="112.6">sûre</w> <w n="112.7">et</w> <w n="112.8">jamais</w> <w n="112.9">je</w> <w n="112.10">ne</w> <w n="112.11">doute</w> ;</l>
					<l n="113" num="8.3"><w n="113.1">J</w>’<w n="113.2">épèle</w>, <w n="113.3">comme</w> <w n="113.4">vous</w>, <w n="113.5">avec</w> <w n="113.6">humilité</w>,</l>
					<l n="114" num="8.4"><w n="114.1">Un</w> <w n="114.2">mot</w> <w n="114.3">qui</w> <w n="114.4">contient</w> <w n="114.5">tout</w>, <w n="114.6">poète</w> : <w n="114.7">éternité</w> !</l>
					<l n="115" num="8.5"><w n="115.1">De</w> <w n="115.2">chaque</w> <w n="115.3">jour</w> <w n="115.4">tombé</w> <w n="115.5">mon</w> <w n="115.6">épaule</w> <w n="115.7">est</w> <w n="115.8">légère</w> ;</l>
					<l n="116" num="8.6"><w n="116.1">L</w>’<w n="116.2">aile</w> <w n="116.3">pousse</w> <w n="116.4">et</w> <w n="116.5">me</w> <w n="116.6">tourne</w> <w n="116.7">à</w> <w n="116.8">ma</w> <w n="116.9">nouvelle</w> <w n="116.10">sphère</w> :</l>
					<l n="117" num="8.7"><w n="117.1">À</w> <w n="117.2">tous</w> <w n="117.3">les</w> <w n="117.4">biens</w> <w n="117.5">ravis</w> <w n="117.6">qui</w> <w n="117.7">me</w> <w n="117.8">disent</w> <w n="117.9">adieu</w>,</l>
					<l n="118" num="8.8"><w n="118.1">Je</w> <w n="118.2">réponds</w> <w n="118.3">doucement</w> : <w n="118.4">va</w> <w n="118.5">m</w>’<w n="118.6">attendre</w> <w n="118.7">chez</w> <w n="118.8">Dieu</w> !</l>
					<l n="119" num="8.9"><w n="119.1">Qu</w>’<w n="119.2">en</w> <w n="119.3">ferais</w>-<w n="119.4">je</w> ; <w n="119.5">après</w> <w n="119.6">tout</w>, <w n="119.7">de</w> <w n="119.8">ces</w> <w n="119.9">biens</w> <w n="119.10">que</w> <w n="119.11">j</w>’<w n="119.12">adore</w> ?</l>
					<l n="120" num="8.10"><w n="120.1">Rien</w> <w n="120.2">que</w> <w n="120.3">les</w> <w n="120.4">adorer</w> ; <w n="120.5">rien</w> <w n="120.6">que</w> <w n="120.7">les</w> <w n="120.8">perdre</w> <w n="120.9">encore</w> !</l>
					<l n="121" num="8.11"><w n="121.1">J</w>’<w n="121.2">attends</w>. <w n="121.3">Pour</w> <w n="121.4">ces</w> <w n="121.5">trésors</w> <w n="121.6">donnés</w>, <w n="121.7">repris</w> <w n="121.8">si</w> <w n="121.9">tôt</w>,</l>
					<l n="122" num="8.12"><w n="122.1">Mon</w> <w n="122.2">cœur</w> <w n="122.3">n</w>’<w n="122.4">est</w> <w n="122.5">pas</w> <w n="122.6">éteint</w> : <w n="122.7">il</w> <w n="122.8">est</w> <w n="122.9">monté</w> <w n="122.10">plus</w> <w n="122.11">haut</w> !</l>
				</lg>
			</div></body></text></TEI>