<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES296">
				<head type="main">UN PRÉSAGE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">J</w>’<w n="1.2">ai</w> <w n="1.3">vu</w> <w n="1.4">dans</w> <w n="1.5">l</w>’<w n="1.6">air</w> <w n="1.7">passer</w> <w n="1.8">deux</w> <w n="1.9">ailes</w> <w n="1.10">blanches</w>.</l>
					<l n="2" num="1.2"><w n="2.1">Est</w>-<w n="2.2">ce</w> <w n="2.3">pour</w> <w n="2.4">moi</w> <w n="2.5">que</w> <w n="2.6">ce</w> <w n="2.7">présage</w> <w n="2.8">a</w> <w n="2.9">lui</w> ?</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">entends</w> <w n="3.3">chanter</w> <w n="3.4">tout</w> <w n="3.5">un</w> <w n="3.6">nid</w> <w n="3.7">dans</w> <w n="3.8">les</w> <w n="3.9">branches</w> :</l>
					<l n="4" num="1.4"><w n="4.1">Trop</w> <w n="4.2">de</w> <w n="4.3">bonheur</w> <w n="4.4">me</w> <w n="4.5">menace</w> <w n="4.6">aujourd</w>’<w n="4.7">hui</w> !</l>
					<l n="5" num="1.5"><w n="5.1">Pour</w> <w n="5.2">le</w> <w n="5.3">braver</w> <w n="5.4">je</w> <w n="5.5">suis</w> <w n="5.6">trop</w> <w n="5.7">faible</w> <w n="5.8">encore</w> ;</l>
					<l n="6" num="1.6"><w n="6.1">Arrêtez</w>-<w n="6.2">vous</w>, <w n="6.3">ambassadeurs</w> <w n="6.4">des</w> <w n="6.5">deux</w> !</l>
					<l n="7" num="1.7"><w n="7.1">L</w>’<w n="7.2">épi</w> <w n="7.3">fléchit</w> <w n="7.4">que</w> <w n="7.5">trop</w> <w n="7.6">de</w> <w n="7.7">soleil</w> <w n="7.8">dore</w> :</l>
					<l n="8" num="1.8"><w n="8.1">Bonheur</w>, <w n="8.2">bonheur</w>, <w n="8.3">ne</w> <w n="8.4">venez</w> <w n="8.5">pas</w> <w n="8.6">encore</w> ;</l>
					<l n="9" num="1.9"><w n="9.1">Éclairez</w>-<w n="9.2">moi</w>, <w n="9.3">ne</w> <w n="9.4">brûlez</w> <w n="9.5">pas</w> <w n="9.6">mes</w> <w n="9.7">yeux</w> !</l>
				</lg>
				<lg n="2">
					<l n="10" num="2.1"><w n="10.1">Tournée</w> <w n="10.2">au</w> <w n="10.3">Nord</w> <w n="10.4">une</w> <w n="10.5">cage</w> <w n="10.6">est</w> <w n="10.7">si</w> <w n="10.8">sombre</w> !</l>
					<l n="11" num="2.2"><w n="11.1">Dieu</w> <w n="11.2">l</w>’<w n="11.3">ouvre</w>-<w n="11.4">t</w>-<w n="11.5">il</w> <w n="11.6">aux</w> <w n="11.7">plaintes</w> <w n="11.8">de</w> <w n="11.9">l</w>’<w n="11.10">oiseau</w>,</l>
					<l n="12" num="2.3"><w n="12.1">L</w>’<w n="12.2">aile</w> <w n="12.3">incertaine</w>, <w n="12.4">avant</w> <w n="12.5">de</w> <w n="12.6">quitter</w> <w n="12.7">l</w>’<w n="12.8">ombre</w>,</l>
					<l n="13" num="2.4"><w n="13.1">Hésite</w> <w n="13.2">et</w> <w n="13.3">plane</w> <w n="13.4">au</w>-<w n="13.5">dessus</w> <w n="13.6">du</w> <w n="13.7">réseau</w>.</l>
					<l n="14" num="2.5"><w n="14.1">La</w> <w n="14.2">liberté</w> <w n="14.3">cause</w> <w n="14.4">un</w> <w n="14.5">brillant</w> <w n="14.6">vertige</w> ;</l>
					<l n="15" num="2.6"><w n="15.1">L</w>’<w n="15.2">anneau</w> <w n="15.3">tombé</w> <w n="15.4">gêne</w> <w n="15.5">encor</w> <w n="15.6">pour</w> <w n="15.7">courir</w>.</l>
					<l n="16" num="2.7"><w n="16.1">Survivra</w>-<w n="16.2">t</w>-<w n="16.3">on</w> <w n="16.4">si</w> <w n="16.5">ce</w> <w n="16.6">n</w>’<w n="16.7">est</w> <w n="16.8">qu</w>’<w n="16.9">un</w> <w n="16.10">prestige</w> ?</l>
					<l n="17" num="2.8"><w n="17.1">L</w>’<w n="17.2">âme</w> <w n="17.3">recule</w> <w n="17.4">à</w> <w n="17.5">l</w>’<w n="17.6">aspect</w> <w n="17.7">du</w> <w n="17.8">prodige</w> ;</l>
					<l n="18" num="2.9"><w n="18.1">Fût</w>-<w n="18.2">ce</w> <w n="18.3">de</w> <w n="18.4">joie</w>, <w n="18.5">on</w> <w n="18.6">a</w> <w n="18.7">peur</w> <w n="18.8">de</w> <w n="18.9">mourir</w> !</l>
				</lg>
				<lg n="3">
					<l n="19" num="3.1"><w n="19.1">Mais</w> <w n="19.2">ce</w> <w n="19.3">bouquet</w> <w n="19.4">apparu</w> <w n="19.5">sur</w> <w n="19.6">ma</w> <w n="19.7">porte</w>,</l>
					<l n="20" num="3.2"><w n="20.1">Dit</w>-<w n="20.2">il</w> <w n="20.3">assez</w> <w n="20.4">ce</w> <w n="20.5">que</w> <w n="20.6">j</w>’<w n="20.7">entends</w> <w n="20.8">tout</w> <w n="20.9">bas</w> ?</l>
					<l n="21" num="3.3"><w n="21.1">Dernier</w> <w n="21.2">rayon</w> <w n="21.3">d</w>’<w n="21.4">une</w> <w n="21.5">âme</w> <w n="21.6">presque</w> <w n="21.7">morte</w>,</l>
					<l n="22" num="3.4"><w n="22.1">Premier</w> <w n="22.2">amour</w>, <w n="22.3">vous</w> <w n="22.4">ne</w> <w n="22.5">mourez</w> <w n="22.6">donc</w> <w n="22.7">pas</w> !</l>
					<l n="23" num="3.5"><w n="23.1">Ces</w> <w n="23.2">fleurs</w> <w n="23.3">toujours</w> <w n="23.4">m</w>’<w n="23.5">annonçaient</w> <w n="23.6">sa</w> <w n="23.7">présence</w>,</l>
					<l n="24" num="3.6"><w n="24.1">C</w>’<w n="24.2">était</w> <w n="24.3">son</w> <w n="24.4">nom</w> <w n="24.5">quand</w> <w n="24.6">il</w> <w n="24.7">allait</w> <w n="24.8">venir</w> :</l>
					<l n="25" num="3.7"><w n="25.1">Comme</w> <w n="25.2">on</w> <w n="25.3">s</w>’<w n="25.4">aimait</w> <w n="25.5">dans</w> <w n="25.6">ce</w> <w n="25.7">temps</w> <w n="25.8">d</w>’<w n="25.9">innocence</w> !</l>
					<l n="26" num="3.8"><w n="26.1">Comme</w> <w n="26.2">un</w> <w n="26.3">rameau</w> <w n="26.4">rouvre</w> <w n="26.5">toute</w> <w n="26.6">l</w>’<w n="26.7">absence</w> !</l>
					<l n="27" num="3.9"><w n="27.1">Que</w> <w n="27.2">de</w> <w n="27.3">parfums</w> <w n="27.4">sortent</w> <w n="27.5">du</w> <w n="27.6">souvenir</w> !</l>
				</lg>
				<lg n="4">
					<l n="28" num="4.1"><w n="28.1">Je</w> <w n="28.2">ne</w> <w n="28.3">sais</w> <w n="28.4">pas</w> <w n="28.5">d</w>’<w n="28.6">où</w> <w n="28.7">souffle</w> <w n="28.8">l</w>’<w n="28.9">espérance</w>,</l>
					<l n="29" num="4.2"><w n="29.1">Mais</w> <w n="29.2">je</w> <w n="29.3">l</w>’<w n="29.4">entends</w> <w n="29.5">rire</w> <w n="29.6">au</w> <w n="29.7">fond</w> <w n="29.8">de</w> <w n="29.9">mes</w> <w n="29.10">pleurs</w>.</l>
					<l n="30" num="4.3"><w n="30.1">Dieu</w> ! <w n="30.2">qu</w>’<w n="30.3">elle</w> <w n="30.4">est</w> <w n="30.5">fraîche</w> <w n="30.6">où</w> <w n="30.7">brûlait</w> <w n="30.8">la</w> <w n="30.9">souffrance</w> !</l>
					<l n="31" num="4.4"><w n="31.1">Que</w> <w n="31.2">son</w> <w n="31.3">haleine</w> <w n="31.4">étanche</w> <w n="31.5">de</w> <w n="31.6">douleurs</w> !</l>
					<l n="32" num="4.5"><w n="32.1">Passante</w> <w n="32.2">ailée</w>, <w n="32.3">au</w> <w n="32.4">coin</w> <w n="32.5">du</w> <w n="32.6">toit</w> <w n="32.7">blottie</w>,</l>
					<l n="33" num="4.6"><w n="33.1">Y</w> <w n="33.2">rattachant</w> <w n="33.3">ses</w> <w n="33.4">fils</w> <w n="33.5">longs</w> <w n="33.6">et</w> <w n="33.7">dorés</w>,</l>
					<l n="34" num="4.7"><w n="34.1">Grâce</w> <w n="34.2">à</w> <w n="34.3">son</w> <w n="34.4">vol</w>, <w n="34.5">ma</w> <w n="34.6">force</w> <w n="34.7">est</w> <w n="34.8">avertie</w> :</l>
					<l n="35" num="4.8"><w n="35.1">Bonheur</w> ! <w n="35.2">bonheur</w> ! <w n="35.3">Je</w> <w n="35.4">ne</w> <w n="35.5">suis</w> <w n="35.6">pas</w> <w n="35.7">sortie</w> ;</l>
					<l n="36" num="4.9"><w n="36.1">J</w>’<w n="36.2">attends</w> <w n="36.3">le</w> <w n="36.4">ciel</w> ; <w n="36.5">c</w>’<w n="36.6">est</w> <w n="36.7">vous</w>, <w n="36.8">bonheur</w> : <w n="36.9">Entrez</w> !</l>
				</lg>
			</div></body></text></TEI>