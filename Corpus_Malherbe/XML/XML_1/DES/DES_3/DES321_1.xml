<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES321">
				<head type="main">LOUISE DE LA VALLIÈRE</head>
				<head type="sub">À GENOUX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Comme</w> <w n="1.2">ils</w> <w n="1.3">s</w>’<w n="1.4">aiment</w> <w n="1.5">là</w>-<w n="1.6">bas</w> ! <w n="1.7">Mon</w> <w n="1.8">père</w>, <w n="1.9">qu</w>’<w n="1.10">elle</w> <w n="1.11">est</w> <w n="1.12">belle</w> !</l>
					<l n="2" num="1.2"><w n="2.1">Pardon</w>… <w n="2.2">rendez</w> <w n="2.3">à</w> <w n="2.4">Dieu</w> <w n="2.5">ce</w> <w n="2.6">cœur</w> <w n="2.7">lâche</w> <w n="2.8">et</w> <w n="2.9">rebelle</w> :</l>
					<l n="3" num="1.3"><w n="3.1">Dieu</w> <w n="3.2">seul</w> <w n="3.3">peut</w> <w n="3.4">me</w> <w n="3.5">guérir</w> <w n="3.6">de</w> <w n="3.7">cet</w> <w n="3.8">immense</w> <w n="3.9">amour</w></l>
					<l n="4" num="1.4"><w n="4.1">Qui</w> <w n="4.2">fut</w> <w n="4.3">pour</w> <w n="4.4">moi</w> <w n="4.5">le</w> <w n="4.6">monde</w>, <w n="4.7">et</w> <w n="4.8">la</w> <w n="4.9">vie</w>, <w n="4.10">et</w> <w n="4.11">le</w> <w n="4.12">jour</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Dieu</w> <w n="5.2">seul</w> <w n="5.3">peut</w> <w n="5.4">me</w> <w n="5.5">cacher</w> <w n="5.6">ces</w> <w n="5.7">fronts</w> <w n="5.8">pleins</w> <w n="5.9">de</w> <w n="5.10">lumières</w></l>
					<l n="6" num="1.6"><w n="6.1">Qui</w> <w n="6.2">viennent</w> <w n="6.3">m</w>’<w n="6.4">éblouir</w> <w n="6.5">jusque</w> <w n="6.6">dans</w> <w n="6.7">mes</w> <w n="6.8">prières</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Oui</w>, <w n="7.2">jusqu</w>’<w n="7.3">aux</w> <w n="7.4">pieds</w> <w n="7.5">du</w> <w n="7.6">Christ</w> <w n="7.7">imploré</w> <w n="7.8">tant</w> <w n="7.9">de</w> <w n="7.10">fois</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Jusque</w> <w n="8.2">dans</w> <w n="8.3">vos</w> <w n="8.4">regards</w>, <w n="8.5">mon</w> <w n="8.6">père</w>, <w n="8.7">je</w> <w n="8.8">les</w> <w n="8.9">vois</w>.</l>
					<l n="9" num="1.9"><w n="9.1">Un</w> <w n="9.2">cloître</w>, <w n="9.3">s</w>’<w n="9.4">il</w> <w n="9.5">vous</w> <w n="9.6">plaît</w>, <w n="9.7">sur</w> <w n="9.8">ces</w> <w n="9.9">ombres</w> <w n="9.10">heureuses</w> !</l>
					<l n="10" num="1.10"><w n="10.1">Un</w> <w n="10.2">cloître</w> <w n="10.3">n</w>’<w n="10.4">aura</w> <w n="10.5">pas</w> <w n="10.6">des</w> <w n="10.7">nuits</w> <w n="10.8">plus</w> <w n="10.9">ténébreuses</w>,</l>
					<l n="11" num="1.11"><w n="11.1">Plus</w> <w n="11.2">tristes</w> <w n="11.3">que</w> <w n="11.4">les</w> <w n="11.5">nuits</w> <w n="11.6">où</w> <w n="11.7">j</w>’<w n="11.8">ai</w> <w n="11.9">tant</w>, <w n="11.10">tant</w> <w n="11.11">souffert</w> !</l>
					<l n="12" num="1.12"><w n="12.1">Venez</w>, <w n="12.2">je</w> <w n="12.3">n</w>’<w n="12.4">ai</w> <w n="12.5">plus</w> <w n="12.6">peur</w>, <w n="12.7">j</w>’<w n="12.8">ai</w> <w n="12.9">passé</w> <w n="12.10">par</w> <w n="12.11">l</w>’<w n="12.12">enfer</w>.</l>
				</lg>
					<p>SON FIANCÉ,<hi rend="ital">qu’elle ne reconnaît pas sous l’habit religieux</hi></p>
				<lg n="2">
					<l n="13" num="2.1"><w n="13.1">Dieu</w> ! <w n="13.2">pesez</w> <w n="13.3">de</w> <w n="13.4">nos</w> <w n="13.5">maux</w> <w n="13.6">l</w>’<w n="13.7">étrange</w> <w n="13.8">ressemblance</w> ;</l>
					<l n="14" num="2.2"><w n="14.1">Alors</w>, <w n="14.2">vers</w> <w n="14.3">le</w> <w n="14.4">plus</w> <w n="14.5">faible</w> <w n="14.6">inclinez</w> <w n="14.7">la</w> <w n="14.8">balance</w> :</l>
					<l n="15" num="2.3"><w n="15.1">L</w>’<w n="15.2">homme</w> <w n="15.3">qu</w>’<w n="15.4">elle</w> <w n="15.5">a</w> <w n="15.6">brisé</w> <w n="15.7">la</w> <w n="15.8">plaint</w> <w n="15.9">et</w> <w n="15.10">la</w> <w n="15.11">défend</w> ;</l>
					<l n="16" num="2.4"><w n="16.1">Elle</w>, <w n="16.2">c</w>’<w n="16.3">est</w> <w n="16.4">une</w> <w n="16.5">femme</w> <w n="16.6">avec</w> <w n="16.7">un</w> <w n="16.8">cœur</w> <w n="16.9">d</w>’<w n="16.10">enfant</w> !</l>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
					<ab type="dot">. . . . . . . . . . . . . . . . . . . . . . . . . . . .</ab>
				</lg>
			</div></body></text></TEI>