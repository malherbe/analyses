<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES277">
				<head type="main">UNE PLACE POUR DEUX</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Entends</w>-<w n="1.2">tu</w> <w n="1.3">l</w>’<w n="1.4">orage</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Que</w> <w n="2.2">j</w>’<w n="2.3">entends</w> <w n="2.4">toujours</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Lorsqu</w>’<w n="3.2">un</w> <w n="3.3">long</w> <w n="3.4">voyage</w></l>
					<l n="4" num="1.4"><w n="4.1">Sépare</w> <w n="4.2">nos</w> <w n="4.3">jours</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Des</w> <w n="5.2">chaînes</w> <w n="5.3">fidèles</w></l>
					<l n="6" num="2.2"><w n="6.1">Ont</w> <w n="6.2">rivé</w> <w n="6.3">mes</w> <w n="6.4">pas</w> :</l>
					<l n="7" num="2.3"><w n="7.1">Prends</w> <w n="7.2">toutes</w> <w n="7.3">tes</w> <w n="7.4">ailes</w> ;</l>
					<l n="8" num="2.4"><w n="8.1">Moi</w> <w n="8.2">je</w> <w n="8.3">n</w>’<w n="8.4">en</w> <w n="8.5">ai</w> <w n="8.6">pas</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Au</w> <w n="9.2">seuil</w> <w n="9.3">arrêtée</w></l>
					<l n="10" num="3.2"><w n="10.1">Sous</w> <w n="10.2">le</w> <w n="10.3">pied</w> <w n="10.4">du</w> <w n="10.5">temps</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Fidèle</w> <w n="11.2">et</w> <w n="11.3">quittée</w>,</l>
					<l n="12" num="3.4"><w n="12.1">J</w>’<w n="12.2">espère</w>… <w n="12.3">j</w>’<w n="12.4">attends</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Toute</w> <w n="13.2">âme</w> <w n="13.3">épuisée</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Pour</w> <w n="14.2">se</w> <w n="14.3">soutenir</w></l>
					<l n="15" num="4.3"><w n="15.1">Sur</w> <w n="15.2">sa</w> <w n="15.3">croix</w> <w n="15.4">brisée</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Vit</w> <w n="16.2">dans</w> <w n="16.3">l</w>’<w n="16.4">avenir</w> :</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">L</w>’<w n="17.2">avenir</w>, <w n="17.3">la</w> <w n="17.4">vie</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Le</w> <w n="18.2">monde</w>, <w n="18.3">le</w> <w n="18.4">jour</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Le</w> <w n="19.2">ciel</w> <w n="19.3">que</w> <w n="19.4">j</w>’<w n="19.5">envie</w>,</l>
					<l n="20" num="5.4"><w n="20.1">C</w>’<w n="20.2">est</w> <w n="20.3">toi</w>, <w n="20.4">mon</w> <w n="20.5">amour</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Va</w> <w n="21.2">donc</w> ! <w n="21.3">car</w> <w n="21.4">Dieu</w> <w n="21.5">même</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Contraire</w> <w n="22.2">aux</w> <w n="22.3">méchans</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Pour</w> <w n="23.2">l</w>’<w n="23.3">oiseau</w> <w n="23.4">qu</w>’<w n="23.5">il</w> <w n="23.6">aime</w></l>
					<l n="24" num="6.4"><w n="24.1">Fit</w> <w n="24.2">la</w> <w n="24.3">clé</w> <w n="24.4">des</w> <w n="24.5">champs</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Des</w> <w n="25.2">méchans</w> ! <w n="25.3">que</w> <w n="25.4">dis</w>-<w n="25.5">je</w> :</l>
					<l n="26" num="7.2"><w n="26.1">Dieu</w> <w n="26.2">l</w>’<w n="26.3">a</w> <w n="26.4">donc</w> <w n="26.5">prévu</w> ?</l>
					<l n="27" num="7.3"><w n="27.1">Ce</w> <w n="27.2">triste</w> <w n="27.3">prodige</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Nous</w> <w n="28.2">l</w>’<w n="28.3">avons</w> <w n="28.4">donc</w> <w n="28.5">vu</w> ?</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">Leur</w> <w n="29.2">souffle</w> <w n="29.3">me</w> <w n="29.4">glace</w> ;</l>
					<l n="30" num="8.2"><w n="30.1">Va</w> <w n="30.2">vite</w> <w n="30.3">loin</w> <w n="30.4">d</w>’<w n="30.5">eux</w>,</l>
					<l n="31" num="8.3"><w n="31.1">Nous</w> <w n="31.2">faire</w> <w n="31.3">une</w> <w n="31.4">place</w></l>
					<l n="32" num="8.4"><w n="32.1">Où</w> <w n="32.2">l</w>’<w n="32.3">on</w> <w n="32.4">tienne</w> <w n="32.5">deux</w> !</l>
				</lg>
			</div></body></text></TEI>