<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES312">
				<head type="main">LA PAGE BLANCHE</head>
				<head type="sub_1">À</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Ondine</w> ! <w n="1.2">prends</w> <w n="1.3">cette</w> <w n="1.4">page</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Dans</w> <w n="2.2">ton</w> <w n="2.3">livre</w> <w n="2.4">vierge</w> <w n="2.5">encor</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Ta</w> <w n="3.2">plume</w> <w n="3.3">éloquente</w> <w n="3.4">et</w> <w n="3.5">sage</w></l>
					<l n="4" num="1.4"><w n="4.1">Peut</w> <w n="4.2">m</w>’<w n="4.3">y</w> <w n="4.4">verser</w> <w n="4.5">un</w> <w n="4.6">trésor</w>.</l>
					<l n="5" num="1.5"><w n="5.1">Sur</w> <w n="5.2">sa</w> <w n="5.3">blancheur</w> <w n="5.4">que</w> <w n="5.5">j</w>’<w n="5.6">envie</w></l>
					<l n="6" num="1.6"><w n="6.1">Ton</w> <w n="6.2">âme</w> <w n="6.3">se</w> <w n="6.4">répandra</w>,</l>
					<l n="7" num="1.7"><w n="7.1">Et</w> <w n="7.2">du</w> <w n="7.3">trouble</w> <w n="7.4">de</w> <w n="7.5">ma</w> <w n="7.6">vie</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Un</w> <w n="8.2">jour</w> <w n="8.3">me</w> <w n="8.4">consolera</w>.</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Seule</w> <w n="9.2">en</w> <w n="9.3">mon</w> <w n="9.4">sentier</w> <w n="9.5">mobile</w>,</l>
					<l n="10" num="2.2"><w n="10.1">Au</w> <w n="10.2">vaisseau</w> <w n="10.3">navigateur</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Sous</w> <w n="11.2">la</w> <w n="11.3">lumière</w> <w n="11.4">tranquille</w>,</l>
					<l n="12" num="2.4"><w n="12.1">D</w>’<w n="12.2">un</w> <w n="12.3">jeune</w> <w n="12.4">astre</w> <w n="12.5">protecteur</w>,</l>
					<l n="13" num="2.5"><w n="13.1">J</w>’<w n="13.2">écrirai</w> <w n="13.3">de</w> <w n="13.4">mon</w> <w n="13.5">voyage</w>,</l>
					<l n="14" num="2.6"><w n="14.1">Les</w> <w n="14.2">écueils</w> <w n="14.3">et</w> <w n="14.4">les</w> <w n="14.5">ennuis</w>,</l>
					<l n="15" num="2.7"><w n="15.1">Et</w> <w n="15.2">tu</w> <w n="15.3">sauras</w>, <w n="15.4">dans</w> <w n="15.5">l</w>’<w n="15.6">orage</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Quelle</w> <w n="16.2">étoile</w> <w n="16.3">j</w> <w n="16.4">e</w> <w n="16.5">poursuis</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Chère</w> ! <w n="17.2">adieu</w>. <w n="17.3">Ce</w> <w n="17.4">mot</w> <w n="17.5">d</w>’<w n="17.6">alarme</w>,</l>
					<l n="18" num="3.2"><w n="18.1">Que</w> <w n="18.2">vient</w> <w n="18.3">d</w>’<w n="18.4">essayer</w> <w n="18.5">ma</w> <w n="18.6">main</w>,</l>
					<l n="19" num="3.3"><w n="19.1">Ce</w> <w n="19.2">mot</w> <w n="19.3">trempé</w> <w n="19.4">d</w>’<w n="19.5">une</w> <w n="19.6">larme</w></l>
					<l n="20" num="3.4"><w n="20.1">Ouvre</w> <w n="20.2">mon</w> <w n="20.3">triste</w> <w n="20.4">chemin</w> !…</l>
					<l n="21" num="3.5"><w n="21.1">Mais</w> <w n="21.2">ton</w> <w n="21.3">regard</w> <w n="21.4">qui</w> <w n="21.5">m</w>’<w n="21.6">écoute</w></l>
					<l n="22" num="3.6"><w n="22.1">Ne</w> <w n="22.2">veut</w> <w n="22.3">pas</w> <w n="22.4">répondre</w> <w n="22.5">adieu</w> ;</l>
					<l n="23" num="3.7"><w n="23.1">Étends</w>-<w n="23.2">le</w> <w n="23.3">donc</w> <w n="23.4">sur</w> <w n="23.5">ma</w> <w n="23.6">route</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Comme</w> <w n="24.2">un</w> <w n="24.3">doux</w> <w n="24.4">rayon</w> <w n="24.5">de</w> <w n="24.6">feu</w> !</l>
				</lg>
			</div></body></text></TEI>