<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES348">
				<head type="main">PLUS DE CHANTS</head>
				<head type="sub_1">À MADAME SIMONIS (ELISA DE KNYFF)</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Enfant</w> <w n="1.2">d</w>’<w n="1.3">un</w> <w n="1.4">nid</w> <w n="1.5">loin</w> <w n="1.6">du</w> <w n="1.7">soleil</w> <w n="1.8">éclos</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Tombée</w> <w n="2.2">un</w> <w n="2.3">jour</w> <w n="2.4">du</w> <w n="2.5">faîte</w> <w n="2.6">des</w> <w n="2.7">collines</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Ouvrant</w> <w n="3.2">à</w> <w n="3.3">Dieu</w> <w n="3.4">mes</w> <w n="3.5">ailes</w> <w n="3.6">orphelines</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Poussée</w> <w n="4.2">aux</w> <w n="4.3">vents</w> <w n="4.4">sur</w> <w n="4.5">la</w> <w n="4.6">terre</w> <w n="4.7">ou</w> <w n="4.8">les</w> <w n="4.9">flots</w>,</l>
					<l n="5" num="1.5"><w n="5.1">Mon</w> <w n="5.2">cœur</w> <w n="5.3">chantait</w>, <w n="5.4">mais</w> <w n="5.5">avec</w> <w n="5.6">des</w> <w n="5.7">sanglots</w>.</l>
				</lg>
				<lg n="2">
					<l n="6" num="2.1"><w n="6.1">Frères</w> <w n="6.2">quittés</w>, <w n="6.3">doux</w> <w n="6.4">frères</w>, <w n="6.5">au</w> <w n="6.6">revoir</w> !</l>
					<l n="7" num="2.2"><w n="7.1">En</w> <w n="7.2">parcourant</w> <w n="7.3">nos</w> <w n="7.4">chemins</w> <w n="7.5">sans</w> <w n="7.6">barrière</w>,</l>
					<l n="8" num="2.3"><w n="8.1">Tous</w> <w n="8.2">attirés</w> <w n="8.3">vers</w> <w n="8.4">la</w> <w n="8.5">même</w> <w n="8.6">lumière</w>,</l>
					<l n="9" num="2.4"><w n="9.1">Pour</w> <w n="9.2">remonter</w> <w n="9.3">au</w> <w n="9.4">céleste</w> <w n="9.5">pouvoir</w>,</l>
					<l n="10" num="2.5"><w n="10.1">Allons</w> <w n="10.2">tremper</w> <w n="10.3">nos</w> <w n="10.4">ailes</w> <w n="10.5">dans</w> <w n="10.6">l</w>’<w n="10.7">espoir</w> !</l>
				</lg>
				<lg n="3">
					<l n="11" num="3.1"><w n="11.1">Pour</w> <w n="11.2">louer</w> <w n="11.3">Dieu</w>, <w n="11.4">dès</w> <w n="11.5">que</w> <w n="11.6">je</w> <w n="11.7">pus</w> <w n="11.8">chanter</w>,</l>
					<l n="12" num="3.2"><w n="12.1">Que</w> <w n="12.2">m</w>’<w n="12.3">importait</w> <w n="12.4">ma</w> <w n="12.5">frêle</w> <w n="12.6">voix</w> <w n="12.7">de</w> <w n="12.8">femme</w> ?</l>
					<l n="13" num="3.3"><w n="13.1">Tout</w> <w n="13.2">le</w> <w n="13.3">concert</w> <w n="13.4">se</w> <w n="13.5">tenait</w> <w n="13.6">dans</w> <w n="13.7">mon</w> <w n="13.8">âme</w> ;</l>
					<l n="14" num="3.4"><w n="14.1">Que</w> <w n="14.2">l</w>’<w n="14.3">on</w> <w n="14.4">passât</w> <w n="14.5">sans</w> <w n="14.6">daigner</w> <w n="14.7">m</w>’<w n="14.8">écouter</w>,</l>
					<l n="15" num="3.5"><w n="15.1">Je</w> <w n="15.2">louais</w> <w n="15.3">Dieu</w> ! <w n="15.4">qui</w> <w n="15.5">pouvait</w> <w n="15.6">m</w>’<w n="15.7">arrêter</w> ?</l>
				</lg>
				<lg n="4">
					<l n="16" num="4.1"><w n="16.1">Le</w> <w n="16.2">front</w> <w n="16.3">vibrant</w> <w n="16.4">d</w>’<w n="16.5">étranges</w> <w n="16.6">et</w> <w n="16.7">doux</w> <w n="16.8">sons</w>,</l>
					<l n="17" num="4.2"><w n="17.1">Toute</w> <w n="17.2">ravie</w> <w n="17.3">et</w> <w n="17.4">jeune</w> <w n="17.5">en</w> <w n="17.6">solitude</w>,</l>
					<l n="18" num="4.3"><w n="18.1">Trouvant</w> <w n="18.2">le</w> <w n="18.3">monde</w> <w n="18.4">assez</w> <w n="18.5">beau</w> <w n="18.6">sans</w> <w n="18.7">l</w>’<w n="18.8">étude</w>,</l>
					<l n="19" num="4.4"><w n="19.1">Je</w> <w n="19.2">souriais</w>, <w n="19.3">rebelle</w> <w n="19.4">à</w> <w n="19.5">ses</w> <w n="19.6">leçons</w>,</l>
					<l n="20" num="4.5"><w n="20.1">Le</w> <w n="20.2">cœur</w> <w n="20.3">gonflé</w> <w n="20.4">d</w>’<w n="20.5">inédites</w> <w n="20.6">chansons</w> !</l>
				</lg>
				<lg n="5">
					<l n="21" num="5.1"><w n="21.1">J</w>’<w n="21.2">étais</w> <w n="21.3">l</w>’<w n="21.4">oiseau</w> <w n="21.5">dans</w> <w n="21.6">les</w> <w n="21.7">branches</w> <w n="21.8">caché</w>,</l>
					<l n="22" num="5.2"><w n="22.1">S</w>’<w n="22.2">émerveillant</w> <w n="22.3">tout</w> <w n="22.4">seul</w>, <w n="22.5">sans</w> <w n="22.6">qu</w>’<w n="22.7">il</w> <w n="22.8">se</w> <w n="22.9">doute</w></l>
					<l n="23" num="5.3"><w n="23.1">Que</w> <w n="23.2">le</w> <w n="23.3">faneur</w> <w n="23.4">fatigué</w> <w n="23.5">qui</w> <w n="23.6">l</w>’<w n="23.7">écoute</w>,</l>
					<l n="24" num="5.4"><w n="24.1">Dont</w> <w n="24.2">le</w> <w n="24.3">sommeil</w> <w n="24.4">à</w> <w n="24.5">l</w>’<w n="24.6">ombre</w> <w n="24.7">est</w> <w n="24.8">empêché</w>,</l>
					<l n="25" num="5.5"><choice reason="analysis" type="false_verse" hand="CA"><sic> </sic><corr source="édition_1973"><w n="25.1">S</w>’<w n="25.2">en</w> <w n="25.3">va</w></corr></choice> <w n="25.4">plus</w> <w n="25.5">loin</w> <w n="25.6">tout</w> <w n="25.7">morose</w> <w n="25.8">et</w> <w n="25.9">fâché</w>.</l>
				</lg>
				<lg n="6">
					<l n="26" num="6.1"><w n="26.1">Convive</w> <w n="26.2">sobre</w> <w n="26.3">et</w> <w n="26.4">suspendue</w> <w n="26.5">aux</w> <w n="26.6">fleurs</w>,</l>
					<l n="27" num="6.2"><w n="27.1">J</w>’<w n="27.2">ai</w> <w n="27.3">pris</w> <w n="27.4">longtemps</w> <w n="27.5">mon</w> <w n="27.6">sort</w> <w n="27.7">pour</w> <w n="27.8">une</w> <w n="27.9">fête</w> ;</l>
					<l n="28" num="6.3"><w n="28.1">Mais</w> <w n="28.2">l</w>’<w n="28.3">ouragan</w> <w n="28.4">a</w> <w n="28.5">sifflé</w> <w n="28.6">sur</w> <w n="28.7">ma</w> <w n="28.8">tête</w>,</l>
					<l n="29" num="6.4"><w n="29.1">Les</w> <w n="29.2">grands</w> <w n="29.3">échos</w> <w n="29.4">m</w>’<w n="29.5">ont</w> <w n="29.6">crié</w> <w n="29.7">leurs</w> <w n="29.8">douleurs</w> :</l>
					<l n="30" num="6.5"><w n="30.1">Et</w> <w n="30.2">je</w> <w n="30.3">les</w> <w n="30.4">chante</w> <w n="30.5">affaiblis</w> <w n="30.6">de</w> <w n="30.7">mes</w> <w n="30.8">pleurs</w>.</l>
				</lg>
				<lg n="7">
					<l n="31" num="7.1"><w n="31.1">La</w> <w n="31.2">solitude</w> <w n="31.3">est</w> <w n="31.4">encor</w> <w n="31.5">de</w> <w n="31.6">mon</w> <w n="31.7">goût</w> ;</l>
					<l n="32" num="7.2"><w n="32.1">Je</w> <w n="32.2">crois</w> <w n="32.3">toujours</w> <w n="32.4">à</w> <w n="32.5">l</w>’<w n="32.6">Auteur</w> <w n="32.7">de</w> <w n="32.8">mon</w> <w n="32.9">être</w> :</l>
					<l n="33" num="7.3"><w n="33.1">Mes</w> <w n="33.2">beaux</w> <w n="33.3">enfans</w> <w n="33.4">me</w> <w n="33.5">l</w>’<w n="33.6">ont</w> <w n="33.7">tant</w> <w n="33.8">fait</w> <w n="33.9">connaître</w> !</l>
					<l n="34" num="7.4"><w n="34.1">Je</w> <w n="34.2">monte</w> <w n="34.3">à</w> <w n="34.4">lui</w>, <w n="34.5">je</w> <w n="34.6">le</w> <w n="34.7">cherche</w> <w n="34.8">partout</w> ;</l>
					<l n="35" num="7.5"><w n="35.1">Mais</w> <w n="35.2">de</w> <w n="35.3">chansons</w>, <w n="35.4">plus</w> <w n="35.5">une</w>, <w n="35.6">oh</w> ! <w n="35.7">plus</w> <w n="35.8">du</w> <w n="35.9">tout</w> !</l>
				</lg>
			</div></body></text></TEI>