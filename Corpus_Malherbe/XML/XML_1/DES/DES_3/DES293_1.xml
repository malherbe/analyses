<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES293">
				<head type="main">LE MAUVAIS JOUR</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">N</w>’<w n="1.2">entend</w>-<w n="1.3">elle</w> <w n="1.4">jamais</w> <w n="1.5">une</w> <w n="1.6">voix</w> <w n="1.7">me</w>-<w n="1.8">défendre</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Un</w> <w n="2.2">conseil</w> <w n="2.3">attendri</w> <w n="2.4">rappeler</w> <w n="2.5">son</w> <w n="2.6">devoir</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">Une</w> <w n="3.2">larme</w> <w n="3.3">furtive</w> ; <w n="3.4">un</w> <w n="3.5">feu</w> <w n="3.6">sous</w> <w n="3.7">cette</w> <w n="3.8">cendre</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">Un</w> <w n="4.2">reproche</w> <w n="4.3">d</w>’<w n="4.4">en</w> <w n="4.5">haut</w> <w n="4.6">lui</w> <w n="4.7">crier</w> : <w n="4.8">va</w> <w n="4.9">la</w> <w n="4.10">voir</w> !</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Moi</w>, <w n="5.2">je</w> <w n="5.3">n</w>’<w n="5.4">y</w> <w n="5.5">peux</w> <w n="5.6">courir</w> : <w n="5.7">Sa</w> <w n="5.8">clameur</w> <w n="5.9">m</w>’<w n="5.10">a</w> <w n="5.11">noircie</w> ;</l>
					<l n="6" num="2.2"><w n="6.1">Mon</w> <w n="6.2">nom</w> <w n="6.3">percé</w> <w n="6.4">d</w>’<w n="6.5">outrage</w> <w n="6.6">a</w> <w n="6.7">rempli</w> <w n="6.8">sa</w> <w n="6.9">maison</w>.</l>
					<l n="7" num="2.3"><w n="7.1">Contre</w> <w n="7.2">elle</w>-<w n="7.3">même</w>, <w n="7.4">hélas</w> ! <w n="7.5">qui</w> <w n="7.6">l</w>’<w n="7.7">a</w> <w n="7.8">donc</w> <w n="7.9">endurcie</w> ?</l>
					<l n="8" num="2.4"><w n="8.1">Injuste</w>, <w n="8.2">à</w> <w n="8.3">qui</w> <w n="8.4">m</w>’<w n="8.5">accuse</w> <w n="8.6">elle</w> <w n="8.7">n</w>’<w n="8.8">a</w> <w n="8.9">pas</w> <w n="8.10">dit</w> <w n="8.11">non</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Que</w> <w n="9.2">s</w>’<w n="9.3">est</w>-<w n="9.4">il</w> <w n="9.5">donc</w> <w n="9.6">passé</w> ? <w n="9.7">quelle</w> <w n="9.8">bise</w> <w n="9.9">inconnue</w>,</l>
					<l n="10" num="3.2"><w n="10.1">A</w> <w n="10.2">glacé</w> <w n="10.3">cette</w> <w n="10.4">fleur</w> <w n="10.5">attachée</w> <w n="10.6">à</w> <w n="10.7">mes</w> <w n="10.8">jours</w> ?</l>
					<l n="11" num="3.3"><w n="11.1">Elle</w> <w n="11.2">était</w> <w n="11.3">la</w> <w n="11.4">moins</w> <w n="11.5">pauvre</w> <w n="11.6">et</w> <w n="11.7">n</w>’<w n="11.8">est</w> <w n="11.9">pas</w> <w n="11.10">revenue</w> :</l>
					<l n="12" num="3.4"><w n="12.1">Qui</w> <w n="12.2">dit</w> <w n="12.3">aimer</w> <w n="12.4">le</w> <w n="12.5">plus</w> <w n="12.6">n</w>’<w n="12.7">aime</w> <w n="12.8">donc</w> <w n="12.9">pas</w> <w n="12.10">toujours</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Elle</w> <w n="13.2">a</w> <w n="13.3">mis</w> <w n="13.4">bien</w> <w n="13.5">des</w> <w n="13.6">pleurs</w> <w n="13.7">dans</w> <w n="13.8">ma</w> <w n="13.9">reconnaissance</w> !</l>
					<l n="14" num="4.2"><w n="14.1">Ne</w> <w n="14.2">lui</w> <w n="14.3">direz</w>-<w n="14.4">vous</w> <w n="14.5">pas</w> <w n="14.6">la</w> <w n="14.7">vérité</w>. <w n="14.8">Seigneur</w> ?</l>
					<l n="15" num="4.3"><w n="15.1">N</w>’<w n="15.2">entendra</w>-<w n="15.3">t</w>-<w n="15.4">elle</w> <w n="15.5">plus</w> <w n="15.6">mon</w> <w n="15.7">passé</w> <w n="15.8">d</w>’<w n="15.9">innocence</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Comme</w> <w n="16.2">un</w> <w n="16.3">oiseau</w> <w n="16.4">sans</w> <w n="16.5">fiel</w> <w n="16.6">plaider</w> <w n="16.7">avec</w> <w n="16.8">son</w> <w n="16.9">cœur</w> ?</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Seigneur</w> ! <w n="17.2">j</w>’<w n="17.3">ai</w> <w n="17.4">des</w> <w n="17.5">enfans</w> ; <w n="17.6">Seigneur</w>, <w n="17.7">j</w>’<w n="17.8">ose</w> <w n="17.9">être</w> <w n="17.10">mère</w> ;</l>
					<l n="18" num="5.2"><w n="18.1">Seigneur</w> ! <w n="18.2">qui</w> <w n="18.3">n</w>’<w n="18.4">a</w> <w n="18.5">cherché</w> <w n="18.6">votre</w> <w n="18.7">amour</w> <w n="18.8">dans</w> <w n="18.9">l</w>’<w n="18.10">amour</w> ?</l>
					<l n="19" num="5.3"><w n="19.1">Sauvez</w> <w n="19.2">à</w> <w n="19.3">mes</w> <w n="19.4">enfans</w> <w n="19.5">cette</w> <w n="19.6">blessure</w> <w n="19.7">amère</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Ce</w> <w n="20.2">long</w> <w n="20.3">étonnement</w>, <w n="20.4">ce</w> <w n="20.5">poids</w> <w n="20.6">d</w>’<w n="20.7">un</w> <w n="20.8">mauvais</w> <w n="20.9">jour</w> !</l>
				</lg>
			</div></body></text></TEI>