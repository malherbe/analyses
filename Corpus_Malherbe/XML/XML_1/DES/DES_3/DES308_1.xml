<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES308">
				<head type="main">L’ENFANT ET LA FOI</head>
				<head type="sub_1">ITALIE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Prompt</w> <w n="1.2">ramier</w>, <w n="1.3">fleur</w> <w n="1.4">des</w> <w n="1.5">toits</w>, <w n="1.6">d</w>’<w n="1.7">où</w> <w n="1.8">viens</w>-<w n="1.9">tu</w> <w n="1.10">ce</w> <w n="1.11">malin</w> ?</l>
					<l n="2" num="1.2"><w n="2.1">Quel</w> <w n="2.2">espoir</w> <w n="2.3">l</w>’<w n="2.4">enlevait</w> <w n="2.5">par</w> <w n="2.6">ce</w> <w n="2.7">temps</w> <w n="2.8">incertain</w>,</l>
					<l n="3" num="1.3"><space quantity="20" unit="char"></space><w n="3.1">Lourd</w> <w n="3.2">de</w> <w n="3.3">pluie</w>,</l>
					<l n="4" num="1.4"><space quantity="20" unit="char"></space><w n="4.1">Chaud</w> <w n="4.2">d</w>’<w n="4.3">éclairs</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Le</w> <w n="5.2">printemps</w> <w n="5.3">descend</w>-<w n="5.4">il</w> <w n="5.5">sur</w> <w n="5.6">ton</w> <w n="5.7">aile</w> <w n="5.8">qui</w> <w n="5.9">plie</w> ?</l>
					<l n="6" num="1.6"><w n="6.1">Tes</w> <w n="6.2">amours</w> <w n="6.3">logent</w>-<w n="6.4">ils</w> <w n="6.5">dans</w> <w n="6.6">un</w> <w n="6.7">nid</w> <w n="6.8">haut</w> <w n="6.9">et</w> <w n="6.10">clair</w> ?</l>
					<l n="7" num="1.7"><w n="7.1">D</w>’<w n="7.2">où</w> <w n="7.3">viens</w> <w n="7.4">tu</w> : <w n="7.5">de</w> <w n="7.6">chez</w> <w n="7.7">toi</w> ; <w n="7.8">car</w> <w n="7.9">ton</w> <w n="7.10">sol</w> <w n="7.11">est</w> <w n="7.12">dans</w> <w n="7.13">l</w>’<w n="7.14">air</w> !</l>
				</lg>
				<lg n="2">
					<l n="8" num="2.1"><w n="8.1">Voyageur</w> <w n="8.2">des</w> <w n="8.3">grands</w> <w n="8.4">cieux</w> ! <w n="8.5">souffle</w> <w n="8.6">errant</w> ! <w n="8.7">esprit</w> <w n="8.8">pur</w> !</l>
					<l n="9" num="2.2"><w n="9.1">N</w>’<w n="9.2">as</w>-<w n="9.3">tu</w> <w n="9.4">pas</w> <w n="9.5">rencontré</w> <w n="9.6">dans</w> <w n="9.7">tes</w> <w n="9.8">sillons</w> <w n="9.9">d</w>’<w n="9.10">azur</w>,</l>
					<l n="10" num="2.3"><space quantity="20" unit="char"></space><w n="10.1">Albertine</w>,</l>
					<l n="11" num="2.4"><space quantity="20" unit="char"></space><w n="11.1">Âme</w> <w n="11.2">en</w> <w n="11.3">fleur</w> ?</l>
					<l n="12" num="2.5"><w n="12.1">Assise</w> <w n="12.2">au</w> <w n="12.3">seuil</w> <w n="12.4">de</w> <w n="12.5">Dieu</w> <w n="12.6">cette</w> <w n="12.7">pâle</w> <w n="12.8">églantine</w>,</l>
					<l n="13" num="2.6"><w n="13.1">Qui</w> <w n="13.2">m</w>’<w n="13.3">attend</w>, <w n="13.4">inclinée</w> <w n="13.5">au</w> <w n="13.6">bruit</w> <w n="13.7">de</w> <w n="13.8">nos</w> <w n="13.9">malheurs</w>,</l>
					<l n="14" num="2.7"><w n="14.1">À</w>-<w n="14.2">t</w>-<w n="14.3">elle</w> <w n="14.4">encor</w> <w n="14.5">des</w> <w n="14.6">yeux</w> <w n="14.7">pour</w> <w n="14.8">regarder</w> <w n="14.9">mes</w> <w n="14.10">pleurs</w> ?</l>
				</lg>
				<lg n="3">
					<l n="15" num="3.1"><w n="15.1">Sur</w> <w n="15.2">ses</w> <w n="15.3">chastes</w> <w n="15.4">genoux</w> <w n="15.5">tient</w>-<w n="15.6">elle</w> <w n="15.7">un</w> <w n="15.8">jeune</w> <w n="15.9">enfant</w>,</l>
					<l n="16" num="3.2"><w n="16.1">Envolé</w> <w n="16.2">par</w> <w n="16.3">la</w> <w n="16.4">mort</w> <w n="16.5">vers</w> <w n="16.6">son</w> <w n="16.7">Dieu</w> <w n="16.8">triomphant</w> ?</l>
					<l n="17" num="3.3"><space quantity="20" unit="char"></space><w n="17.1">Ce</w> <w n="17.2">bel</w> <w n="17.3">ange</w></l>
					<l n="18" num="3.4"><space quantity="20" unit="char"></space><w n="18.1">Fut</w> <w n="18.2">à</w> <w n="18.3">moi</w> !</l>
					<l n="19" num="3.5"><w n="19.1">En</w> <w n="19.2">te</w> <w n="19.3">voyant</w> <w n="19.4">monter</w> <w n="19.5">de</w> <w n="19.6">la</w> <w n="19.7">terre</w>, <w n="19.8">où</w> <w n="19.9">tout</w> <w n="19.10">change</w>,</l>
					<l n="20" num="3.6"><w n="20.1">Tend</w>-<w n="20.2">il</w> <w n="20.3">ses</w> <w n="20.4">douces</w> <w n="20.5">mains</w> <w n="20.6">pour</w> <w n="20.7">jouer</w> <w n="20.8">avec</w> <w n="20.9">toi</w>,</l>
					<l n="21" num="3.7"><w n="21.1">Comme</w> <w n="21.2">l</w>’<w n="21.3">enfant</w> <w n="21.4">Jésus</w> <w n="21.5">qui</w> <w n="21.6">relève</w> <w n="21.7">ma</w> <w n="21.8">foi</w> !</l>
				</lg>
				<lg n="4">
					<l n="22" num="4.1"><w n="22.1">Toi</w> <w n="22.2">qui</w> <w n="22.3">flottes</w> <w n="22.4">vivant</w> <w n="22.5">dans</w> <w n="22.6">les</w> <w n="22.7">mondes</w> <w n="22.8">plus</w> <w n="22.9">beaux</w>,</l>
					<l n="23" num="4.2"><w n="23.1">Sans</w> <w n="23.2">passer</w> <w n="23.3">comme</w> <w n="23.4">nous</w> <w n="23.5">par</w> <w n="23.6">l</w>’<w n="23.7">effroi</w> <w n="23.8">des</w> <w n="23.9">tombeaux</w>,</l>
					<l n="24" num="4.3"><space quantity="20" unit="char"></space><w n="24.1">Prends</w>, <w n="24.2">et</w> <w n="24.3">donne</w></l>
					<l n="25" num="4.4"><space quantity="20" unit="char"></space><w n="25.1">Cet</w> <w n="25.2">écrit</w>,</l>
					<l n="26" num="4.5"><w n="26.1">À</w> <w n="26.2">celle</w> <w n="26.3">que</w> <w n="26.4">le</w> <w n="26.5">pauvre</w> <w n="26.6">appelait</w> <w n="26.7">sa</w> <w n="26.8">Madone</w> ;</l>
					<l n="27" num="4.6"><w n="27.1">Porte</w> <w n="27.2">mon</w> <w n="27.3">baiser</w> <w n="27.4">triste</w> <w n="27.5">à</w> <w n="27.6">l</w>’<w n="27.7">enfant</w> <w n="27.8">qui</w> <w n="27.9">sourit</w>,</l>
					<l n="28" num="4.7"><w n="28.1">Et</w> <w n="28.2">qui</w> <w n="28.3">me</w> <w n="28.4">laissa</w> <w n="28.5">seule</w> <w n="28.6">aux</w> <w n="28.7">pieds</w> <w n="28.8">de</w> <w n="28.9">Jésus</w>-<w n="28.10">Christ</w>.</l>
				</lg>
				<lg n="5">
					<l n="29" num="5.1"><w n="29.1">Oh</w> ! <w n="29.2">qui</w> <w n="29.3">me</w> <w n="29.4">les</w> <w n="29.5">rendra</w>, <w n="29.6">mes</w> <w n="29.7">divines</w> <w n="29.8">amours</w> !</l>
					<l n="30" num="5.2"><w n="30.1">Oh</w> ! <w n="30.2">que</w> <w n="30.3">faut</w>-<w n="30.4">il</w> <w n="30.5">donner</w> <w n="30.6">pour</w> <w n="30.7">les</w> <w n="30.8">garder</w> <w n="30.9">toujours</w> !</l>
					<l n="31" num="5.3"><space quantity="20" unit="char"></space><w n="31.1">Ce</w> <w n="31.2">que</w> <w n="31.3">j</w>’<w n="31.4">aime</w></l>
					<l n="32" num="5.4"><space quantity="20" unit="char"></space><w n="32.1">Change</w>, <w n="32.2">ou</w> <w n="32.3">meurt</w> !</l>
					<l n="33" num="5.5"><w n="33.1">Mais</w>, <w n="33.2">la</w> <w n="33.3">vie</w> <w n="33.4">a</w> <w n="33.5">des</w> <w n="33.6">flots</w> <w n="33.7">qui</w> <w n="33.8">m</w>’<w n="33.9">enlèvent</w> <w n="33.10">moi</w>-<w n="33.11">même</w>,</l>
					<l n="34" num="5.6"><w n="34.1">Et</w> <w n="34.2">chaque</w> <w n="34.3">battement</w> <w n="34.4">de</w> <w n="34.5">mon</w> <w n="34.6">sein</w> <w n="34.7">en</w> <w n="34.8">rumeur</w>,</l>
					<l n="35" num="5.7"><w n="35.1">Est</w> <w n="35.2">un</w> <w n="35.3">pas</w> <w n="35.4">vers</w> <w n="35.5">ton</w> <w n="35.6">ciel</w> <w n="35.7">où</w> <w n="35.8">frappe</w> <w n="35.9">ma</w> <w n="35.10">clameur</w>.</l>
				</lg>
				<lg n="6">
					<l n="36" num="6.1"><w n="36.1">Que</w> <w n="36.2">tu</w> <w n="36.3">sois</w> <w n="36.4">la</w> <w n="36.5">foi</w> <w n="36.6">vive</w>, <w n="36.7">ou</w> <w n="36.8">sa</w> <w n="36.9">sœur</w> <w n="36.10">charité</w>,</l>
					<l n="37" num="6.2"><w n="37.1">Ou</w> <w n="37.2">l</w>’<w n="37.3">enfant</w>, <w n="37.4">dont</w> <w n="37.5">ta</w> <w n="37.6">forme</w> <w n="37.7">enferme</w> <w n="37.8">la</w> <w n="37.9">beauté</w>,</l>
					<l n="38" num="6.3"><space quantity="20" unit="char"></space><w n="38.1">Reparue</w></l>
					<l n="39" num="6.4"><space quantity="20" unit="char"></space><w n="39.1">Ici</w>-<w n="39.2">bas</w>,</l>
					<l n="40" num="6.5"><w n="40.1">Aide</w> <w n="40.2">une</w> <w n="40.3">âme</w> <w n="40.4">à</w> <w n="40.5">franchir</w> <w n="40.6">les</w> <w n="40.7">pavés</w> <w n="40.8">de</w> <w n="40.9">la</w> <w n="40.10">rue</w> ;</l>
					<l n="41" num="6.6"><w n="41.1">La</w> <w n="41.2">fange</w> <w n="41.3">des</w> <w n="41.4">ruisseaux</w> <w n="41.5">qui</w> <w n="41.6">consterne</w> <w n="41.7">mes</w> <w n="41.8">pas</w> ;</l>
					<l n="42" num="6.7"><w n="42.1">Et</w> <w n="42.2">la</w> <w n="42.3">foule</w> <w n="42.4">déserte</w>, <w n="42.5">où</w> <w n="42.6">tu</w> <w n="42.7">ne</w> <w n="42.8">descends</w> <w n="42.9">pas</w> !</l>
				</lg>
			</div></body></text></TEI>