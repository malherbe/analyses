<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES280">
				<head type="main">ENVOI DU LIVRE DES PLEURS</head>
				<head type="sub_1">(AU BAZAR POLONAIS)</head>
				<opener>
					<salute>À Celui qui peut donner pour la Pologne.</salute>
				</opener>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Achète</w>-<w n="1.2">moi</w>, <w n="1.3">si</w> <w n="1.4">l</w>’<w n="1.5">or</w> <w n="1.6">est</w> <w n="1.7">ton</w> <w n="1.8">partage</w> ;</l>
					<l n="2" num="1.2"><w n="2.1">Donne</w> <w n="2.2">une</w> <w n="2.3">fois</w> <w n="2.4">un</w> <w n="2.5">doux</w> <w n="2.6">prix</w> <w n="2.7">à</w> <w n="2.8">mes</w> <w n="2.9">vers</w> !</l>
					<l n="3" num="1.3"><w n="3.1">Dieu</w> <w n="3.2">bénit</w> <w n="3.3">l</w>’<w n="3.4">or</w> <w n="3.5">qui</w> <w n="3.6">fait</w> <w n="3.7">tomber</w> <w n="3.8">des</w> <w n="3.9">fers</w> ;</l>
					<l n="4" num="1.4"><w n="4.1">J</w>’<w n="4.2">offre</w> <w n="4.3">mes</w> <w n="4.4">pleurs</w> : <w n="4.5">je</w> <w n="4.6">n</w>’<w n="4.7">ai</w> <w n="4.8">pas</w> <w n="4.9">davantage</w>.</l>
					<l n="5" num="1.5">— <w n="5.1">Achète</w>-<w n="5.2">les</w> ; <w n="5.3">je</w> <w n="5.4">les</w> <w n="5.5">dédie</w> <w n="5.6">à</w> <w n="5.7">toi</w></l>
					<l n="6" num="1.6"><w n="6.1">Dont</w> <w n="6.2">la</w> <w n="6.3">pitié</w> <w n="6.4">fait</w> <w n="6.5">palpiter</w> <w n="6.6">les</w> <w n="6.7">veines</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">veux</w> <w n="7.3">aussi</w>, <w n="7.4">je</w> <w n="7.5">veux</w> <w n="7.6">briser</w> <w n="7.7">des</w> <w n="7.8">chaînes</w> :</l>
					<l n="8" num="1.8"><w n="8.1">Mais</w> <w n="8.2">je</w> <w n="8.3">suis</w> <w n="8.4">pauvre</w> ; <w n="8.5">ô</w> <w n="8.6">riche</w> ! <w n="8.7">achète</w>-<w n="8.8">moi</w>.</l>
				</lg>
			</div></body></text></TEI>