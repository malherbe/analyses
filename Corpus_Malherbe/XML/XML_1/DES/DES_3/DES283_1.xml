<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES283">
				<head type="main">AU LIVRE DES CONSOLATIONS</head>
				<head type="sub_1">PAR M. SAINTE-BEUVE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Quand</w> <w n="1.2">je</w> <w n="1.3">touche</w> <w n="1.4">rêveuse</w> <w n="1.5">à</w> <w n="1.6">ces</w> <w n="1.7">feuilles</w> <w n="1.8">sonores</w></l>
					<l n="2" num="1.2"><w n="2.1">D</w>’<w n="2.2">où</w> <w n="2.3">montent</w> <w n="2.4">les</w> <w n="2.5">parfums</w> <w n="2.6">des</w> <w n="2.7">divines</w> <w n="2.8">amphores</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Prise</w> <w n="3.2">par</w> <w n="3.3">tout</w> <w n="3.4">mon</w> <w n="3.5">corps</w> <w n="3.6">d</w>’<w n="3.7">un</w> <w n="3.8">long</w> <w n="3.9">tressaillement</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Je</w> <w n="4.2">m</w>’<w n="4.3">incline</w>, <w n="4.4">et</w> <w n="4.5">j</w>’<w n="4.6">écoute</w> <w n="4.7">avec</w> <w n="4.8">saisissement</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">O</w> <w n="5.2">fièvre</w> <w n="5.3">poétique</w> ! <w n="5.4">ô</w> <w n="5.5">sainte</w> <w n="5.6">maladie</w> !</l>
					<l n="6" num="2.2"><w n="6.1">O</w> <w n="6.2">jeunesse</w> <w n="6.3">éternelle</w> ! <w n="6.4">ô</w> <w n="6.5">vaste</w> <w n="6.6">mélodie</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Voix</w> <w n="7.2">limpide</w> <w n="7.3">et</w> <w n="7.4">profonde</w> ! <w n="7.5">invisible</w> <w n="7.6">instrument</w> !</l>
					<l n="8" num="2.4"><w n="8.1">Nid</w> <w n="8.2">d</w>’<w n="8.3">abeille</w> <w n="8.4">enfermé</w> <w n="8.5">dans</w> <w n="8.6">un</w> <w n="8.7">livre</w> <w n="8.8">charmant</w> !</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Trésor</w> <w n="9.2">tombé</w> <w n="9.3">des</w> <w n="9.4">mains</w> <w n="9.5">du</w> <w n="9.6">meilleur</w> <w n="9.7">de</w> <w n="9.8">mes</w> <w n="9.9">frères</w> !</l>
					<l n="10" num="3.2"><w n="10.1">Doux</w> <w n="10.2">Memnon</w> ! <w n="10.3">chaste</w> <w n="10.4">ami</w> <w n="10.5">de</w> <w n="10.6">mes</w> <w n="10.7">tendres</w> <w n="10.8">misères</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Chantez</w> ! <w n="11.2">nourrissez</w>-<w n="11.3">moi</w> <w n="11.4">d</w>’<w n="11.5">impérissable</w> <w n="11.6">miel</w> :</l>
				</lg>
				<lg n="4">
					<l n="12" num="4.1"><w n="12.1">Car</w>, <w n="12.2">je</w> <w n="12.3">suis</w> <w n="12.4">indigente</w> <w n="12.5">à</w> <w n="12.6">me</w> <w n="12.7">nourrir</w> <w n="12.8">moi</w>-<w n="12.9">même</w> ;</l>
					<l n="13" num="4.2"><w n="13.1">Source</w> <w n="13.2">fraîche</w>, <w n="13.3">ouvrez</w>-<w n="13.4">vous</w> <w n="13.5">à</w> <w n="13.6">ma</w> <w n="13.7">douleur</w> <w n="13.8">suprême</w>,</l>
					<l n="14" num="4.3"><w n="14.1">Et</w> <w n="14.2">m</w>’<w n="14.3">aidez</w>, <w n="14.4">par</w> <w n="14.5">ce</w> <w n="14.6">monde</w>, <w n="14.7">à</w> <w n="14.8">retrouver</w> <w n="14.9">mon</w> <w n="14.10">ciel</w> !</l>
				</lg>
			</div></body></text></TEI>