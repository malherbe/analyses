<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES318">
				<head type="main">LES POISSONS D’OR</head>
				<head type="sub_1">À M. ALIBERT, MÉDECIN</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Que</w> <w n="1.2">font</w> <w n="1.3">les</w> <w n="1.4">poissons</w> <w n="1.5">d</w>’<w n="1.6">or</w> <w n="1.7">sous</w> <w n="1.8">la</w> <w n="1.9">prison</w> <w n="1.10">de</w> <w n="1.11">verre</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Asile</w> <w n="2.2">transparent</w> <w n="2.3">rafraîchi</w> <w n="2.4">de</w> <w n="2.5">fougère</w> :</l>
					<l n="3" num="1.3"><w n="3.1">Nagent</w>-<w n="3.2">ils</w> <w n="3.3">au</w> <w n="3.4">soleil</w> <w n="3.5">dans</w> <w n="3.6">ce</w> <w n="3.7">frêle</w> <w n="3.8">vaisseau</w></l>
					<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">vous</w> <w n="4.3">leur</w> <w n="4.4">répandez</w> <w n="4.5">un</w> <w n="4.6">éternel</w> <w n="4.7">ruisseau</w> ?</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Pour</w> <w n="5.2">respirer</w> <w n="5.3">la</w> <w n="5.4">fleur</w> <w n="5.5">que</w> <w n="5.6">vous</w> <w n="5.7">avez</w> <w n="5.8">cueillie</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Dès</w> <w n="6.2">que</w> <w n="6.3">vous</w> <w n="6.4">y</w> <w n="6.5">penchez</w> <w n="6.6">votre</w> <w n="6.7">ombre</w> <w n="6.8">recueillie</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Ces</w> <w n="7.2">mobiles</w> <w n="7.3">esprits</w> <w n="7.4">du</w> <w n="7.5">fluide</w> <w n="7.6">élément</w></l>
					<l n="8" num="2.4"><w n="8.1">Remontent</w>-<w n="8.2">ils</w> <w n="8.3">joyeux</w> <w n="8.4">au</w> <w n="8.5">bord</w> <w n="8.6">du</w> <w n="8.7">lac</w> <w n="8.8">dormant</w> ?</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Égayez</w>-<w n="9.2">vous</w> <w n="9.3">leur</w> <w n="9.4">temps</w> <w n="9.5">d</w>’<w n="9.6">exil</w> <w n="9.7">sous</w> <w n="9.8">la</w> <w n="9.9">rivière</w>,</l>
					<l n="10" num="3.2"><w n="10.1">En</w> <w n="10.2">garnissant</w> <w n="10.3">d</w>’<w n="10.4">oiseaux</w> <w n="10.5">la</w> <w n="10.6">fragile</w> <w n="10.7">barrière</w></l>
					<l n="11" num="3.3"><w n="11.1">Où</w> <w n="11.2">vous</w> <w n="11.3">allez</w> <w n="11.4">suspendre</w> <w n="11.5">et</w> <w n="11.6">baigner</w> <w n="11.7">vos</w> <w n="11.8">ennuis</w>,</l>
					<l n="12" num="3.4"><w n="12.1">Pour</w> <w n="12.2">rafraîchir</w> <w n="12.3">vos</w> <w n="12.4">jours</w>, <w n="12.5">rêveurs</w> <w n="12.6">comme</w> <w n="12.7">vos</w> <w n="12.8">nuits</w> ?</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Parfois</w> <w n="13.2">l</w>’<w n="13.3">aigle</w> <w n="13.4">sur</w> <w n="13.5">Fonde</w> <w n="13.6">attache</w> <w n="13.7">sa</w> <w n="13.8">paupière</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Et</w> <w n="14.2">s</w>’<w n="14.3">inonde</w> <w n="14.4">à</w> <w n="14.5">plaisir</w> <w n="14.6">d</w>’<w n="14.7">une</w> <w n="14.8">calme</w> <w n="14.9">lumière</w> :</l>
					<l n="15" num="4.3"><w n="15.1">Ainsi</w>, <w n="15.2">près</w> <w n="15.3">du</w> <w n="15.4">miroir</w> <w n="15.5">inspirateur</w> <w n="15.6">de</w> <w n="15.7">l</w>’<w n="15.8">eau</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Le</w> <w n="16.2">génie</w>, <w n="16.3">aigle</w> <w n="16.4">ardent</w>, <w n="16.5">sort</w> <w n="16.6">libre</w> <w n="16.7">du</w> <w n="16.8">cerveau</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Comme</w> <w n="17.2">dans</w> <w n="17.3">l</w>’<w n="17.4">Orient</w>, <w n="17.5">au</w> <w n="17.6">fond</w> <w n="17.7">de</w> <w n="17.8">votre</w> <w n="17.9">chambre</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Où</w> <w n="18.2">ne</w> <w n="18.3">gèle</w> <w n="18.4">jamais</w> <w n="18.5">l</w>’<w n="18.6">haleine</w> <w n="18.7">de</w> <w n="18.8">décembre</w>,</l>
					<l n="19" num="5.3"><w n="19.1">Voit</w>-<w n="19.2">on</w> <w n="19.3">ce</w> <w n="19.4">filet</w> <w n="19.5">d</w>’<w n="19.6">eau</w> <w n="19.7">circuler</w> <w n="19.8">nuit</w> <w n="19.9">et</w> <w n="19.10">jour</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Pour</w> <w n="20.2">faire</w> <w n="20.3">aux</w> <w n="20.4">poissons</w> <w n="20.5">d</w>’<w n="20.6">or</w> <w n="20.7">un</w> <w n="20.8">tiède</w> <w n="20.9">et</w> <w n="20.10">clair</w> <w n="20.11">séjour</w> ?</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Oh</w> ! <w n="21.2">que</w> <w n="21.3">ne</w> <w n="21.4">puis</w>-<w n="21.5">je</w> <w n="21.6">atteindre</w> <w n="21.7">à</w> <w n="21.8">ces</w> <w n="21.9">molles</w> <w n="21.10">demeures</w></l>
					<l n="22" num="6.2"><w n="22.1">Pour</w> <w n="22.2">glisser</w> <w n="22.3">alentour</w> <w n="22.4">de</w> <w n="22.5">vos</w> <w n="22.6">limpides</w> <w n="22.7">heures</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Que</w> <w n="23.2">n</w>’<w n="23.3">altéra</w> <w n="23.4">jamais</w> <w n="23.5">la</w> <w n="23.6">haine</w> <w n="23.7">au</w> <w n="23.8">poulx</w> <w n="23.9">fiévreux</w> ;</l>
					<l n="24" num="6.4"><w n="24.1">Vos</w> <w n="24.2">heures</w> ! <w n="24.3">dons</w> <w n="24.4">du</w> <w n="24.5">ciel</w> <w n="24.6">voués</w> <w n="24.7">aux</w> <w n="24.8">malheureux</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Vos</w> <w n="25.2">heures</w>, <w n="25.3">d</w>’<w n="25.4">où</w> <w n="25.5">coula</w> <w n="25.6">comme</w> <w n="25.7">un</w> <w n="25.8">divin</w> <w n="25.9">breuvage</w></l>
					<l n="26" num="7.2"><w n="26.1">La</w> <w n="26.2">guérison</w> <w n="26.3">des</w> <w n="26.4">sens</w> <w n="26.5">par</w> <w n="26.6">la</w> <w n="26.7">raison</w> <w n="26.8">du</w> <w n="26.9">sage</w> ;</l>
					<l n="27" num="7.3"><w n="27.1">Vos</w> <w n="27.2">heures</w>, <w n="27.3">que</w> <w n="27.4">longtemps</w> <w n="27.5">puissions</w>-<w n="27.6">nous</w> <w n="27.7">voir</w> <w n="27.8">encor</w></l>
					<l n="28" num="7.4"><w n="28.1">Briller</w> <w n="28.2">sous</w> <w n="28.3">le</w> <w n="28.4">soleil</w> <w n="28.5">comme</w> <w n="28.6">les</w> <w n="28.7">poissons</w> <w n="28.8">d</w>’<w n="28.9">or</w> !</l>
				</lg>
			</div></body></text></TEI>