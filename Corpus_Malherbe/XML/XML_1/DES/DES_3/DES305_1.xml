<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES305">
				<head type="main">MERCI POUR MA FILLE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Au</w> <w n="1.2">sort</w> <w n="1.3">de</w> <w n="1.4">votre</w> <w n="1.5">père</w> <w n="1.6">en</w> <w n="1.7">étoile</w> <w n="1.8">attachée</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Lampe</w> <w n="2.2">de</w> <w n="2.3">sa</w> <w n="2.4">maison</w>, <w n="2.5">lumineuse</w> <w n="2.6">et</w> <w n="2.7">cachée</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Paisible</w> <w n="3.2">sur</w> <w n="3.3">les</w> <w n="3.4">bords</w> <w n="3.5">d</w>’<w n="3.6">un</w> <w n="3.7">profond</w> <w n="3.8">avenir</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Penchez</w> <w n="4.2">un</w> <w n="4.3">peu</w> <w n="4.4">l</w>’<w n="4.5">oreille</w> <w n="4.6">au</w> <w n="4.7">jeune</w> <w n="4.8">souvenir</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Une</w> <w n="5.2">lisière</w> <w n="5.3">encor</w> <w n="5.4">vous</w> <w n="5.5">tenait</w> ; <w n="5.6">vos</w> <w n="5.7">pieds</w> <w n="5.8">d</w>’<w n="5.9">ange</w>,</l>
					<l n="6" num="2.2"><w n="6.1">Trop</w> <w n="6.2">faibles</w> <w n="6.3">pour</w> <w n="6.4">la</w> <w n="6.5">terre</w>, <w n="6.6">où</w> <w n="6.7">tout</w> <w n="6.8">blesse</w>, <w n="6.9">où</w> <w n="6.10">tout</w> <w n="6.11">change</w>,</l>
					<l n="7" num="2.3"><w n="7.1">Ne</w> <w n="7.2">marchaient</w> <w n="7.3">pas</w> <w n="7.4">sans</w> <w n="7.5">guide</w> <w n="7.6">en</w> <w n="7.7">nos</w> <w n="7.8">rudes</w> <w n="7.9">chemins</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Et</w> <w n="8.2">vous</w> <w n="8.3">aviez</w> <w n="8.4">du</w> <w n="8.5">ciel</w> <w n="8.6">plein</w> <w n="8.7">vos</w> <w n="8.8">petites</w> <w n="8.9">mains</w>,</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Pour</w> <w n="9.2">une</w> <w n="9.3">âme</w> <w n="9.4">nouvelle</w> <w n="9.5">à</w> <w n="9.6">cette</w> <w n="9.7">vie</w> <w n="9.8">amère</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Un</w> <w n="10.2">enfant</w> <w n="10.3">dans</w> <w n="10.4">sa</w> <w n="10.5">crèche</w> (<w n="10.6">où</w> <w n="10.7">n</w>’<w n="10.8">était</w> <w n="10.9">pas</w> <w n="10.10">sa</w> <w n="10.11">mère</w>),</l>
					<l n="11" num="3.3"><w n="11.1">Ébloui</w> <w n="11.2">de</w> <w n="11.3">votre</w> <w n="11.4">âge</w> <w n="11.5">et</w> <w n="11.6">du</w> <w n="11.7">regard</w> <w n="11.8">charmant</w></l>
					<l n="12" num="3.4"><w n="12.1">Dont</w> <w n="12.2">vous</w> <w n="12.3">illuminiez</w> <w n="12.4">son</w> <w n="12.5">jeune</w> <w n="12.6">sentiment</w>.</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">La</w> <w n="13.2">mère</w>, <w n="13.3">c</w>’<w n="13.4">était</w> <w n="13.5">moi</w> ; <w n="13.6">l</w>’<w n="13.7">enfant</w>, <w n="13.8">c</w>’<w n="13.9">était</w> <w n="13.10">ma</w> <w n="13.11">fille</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Qui</w> <w n="14.2">manquait</w> <w n="14.3">au</w> <w n="14.4">cortège</w> <w n="14.5">errant</w> <w n="14.6">de</w> <w n="14.7">la</w> <w n="14.8">famille</w>.</l>
					<l n="15" num="4.3"><w n="15.1">Au</w> <w n="15.2">pied</w> <w n="15.3">de</w> <w n="15.4">la</w> <w n="15.5">croix</w> <w n="15.6">haute</w> <w n="15.7">et</w> <w n="15.8">d</w>’<w n="15.9">un</w> <w n="15.10">frais</w> <w n="15.11">arbrisseau</w>,</l>
					<l n="16" num="4.4"><w n="16.1">Nous</w> <w n="16.2">avions</w> <w n="16.3">à</w> <w n="16.4">la</w> <w n="16.5">hâte</w> <w n="16.6">élevé</w> <w n="16.7">son</w> <w n="16.8">berceau</w>.</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Pâle</w> <w n="17.2">de</w> <w n="17.3">mon</w> <w n="17.4">courage</w> <w n="17.5">et</w> <w n="17.6">d</w>’<w n="17.7">angoisse</w> <w n="17.8">suivie</w>,</l>
					<l n="18" num="5.2"><w n="18.1">Je</w> <w n="18.2">la</w> <w n="18.3">cédais</w> <w n="18.4">au</w> <w n="18.5">sein</w> <w n="18.6">qui</w> <w n="18.7">lui</w> <w n="18.8">versait</w> <w n="18.9">la</w> <w n="18.10">vie</w> ;</l>
					<l n="19" num="5.3"><w n="19.1">D</w>’<w n="19.2">une</w> <w n="19.3">aile</w> <w n="19.4">sans</w> <w n="19.5">repos</w> <w n="19.6">le</w> <w n="19.7">malheur</w> <w n="19.8">m</w>’<w n="19.9">enlevait</w>,</l>
					<l n="20" num="5.4"><w n="20.1">Et</w> <w n="20.2">votre</w> <w n="20.3">père</w> <w n="20.4">ému</w>, <w n="20.5">d</w>’<w n="20.6">un</w> <w n="20.7">regard</w> <w n="20.8">me</w> <w n="20.9">suivait</w>.</l>
				</lg>
				<lg n="6">
					<l n="21" num="6.1"><w n="21.1">Et</w> <w n="21.2">vous</w>, <w n="21.3">sur</w> <w n="21.4">votre</w> <w n="21.5">père</w> <w n="21.6">incessamment</w> <w n="21.7">penchée</w>,</l>
					<l n="22" num="6.2"><w n="22.1">Visitant</w> <w n="22.2">ma</w> <w n="22.3">jeune</w> <w n="22.4">âme</w> <w n="22.5">où</w> <w n="22.6">Dieu</w> <w n="22.7">l</w>’<w n="22.8">avait</w> <w n="22.9">cachée</w>,</l>
					<l n="23" num="6.3"><w n="23.1">Comme</w> <w n="23.2">au</w> <w n="23.3">nid</w> <w n="23.4">d</w>’<w n="23.5">un</w> <w n="23.6">oiseau</w>, <w n="23.7">vous</w> <w n="23.8">montiez</w> <w n="23.9">quelquefois</w></l>
					<l n="24" num="6.4"><w n="24.1">Lui</w> <w n="24.2">bégayer</w> <w n="24.3">mon</w> <w n="24.4">nom</w> <w n="24.5">d</w>’<w n="24.6">une</w> <w n="24.7">angélique</w> <w n="24.8">voix</w>.</l>
				</lg>
				<lg n="7">
					<l n="25" num="7.1"><w n="25.1">Elle</w>, <w n="25.2">la</w> <w n="25.3">joue</w> <w n="25.4">encore</w> <w n="25.5">humide</w> <w n="25.6">de</w> <w n="25.7">mes</w> <w n="25.8">larmes</w>,</l>
					<l n="26" num="7.2"><w n="26.1">Vous</w> <w n="26.2">connaissant</w> <w n="26.3">du</w> <w n="26.4">ciel</w>, <w n="26.5">jouait</w> <w n="26.6">avec</w> <w n="26.7">vos</w> <w n="26.8">charmes</w> ;</l>
					<l n="27" num="7.3"><w n="27.1">Et</w> <w n="27.2">son</w> <w n="27.3">cœur</w> <w n="27.4">dilaté</w>, <w n="27.5">d</w>’<w n="27.6">un</w> <w n="27.7">instinct</w> <w n="27.8">tendre</w> <w n="27.9">et</w> <w n="27.10">prompt</w>,</l>
					<l n="28" num="7.4"><w n="28.1">Allait</w> <w n="28.2">s</w>’<w n="28.3">épanouir</w> <w n="28.4">aux</w> <w n="28.5">lys</w> <w n="28.6">de</w> <w n="28.7">votre</w> <w n="28.8">front</w> :</l>
				</lg>
				<lg n="8">
					<l n="29" num="8.1"><w n="29.1">L</w>’<w n="29.2">ange</w> <w n="29.3">blanc</w> <w n="29.4">du</w> <w n="29.5">baptême</w>, <w n="29.6">en</w> <w n="29.7">ondoyant</w> <w n="29.8">votre</w> <w n="29.9">âme</w>,</l>
					<l n="30" num="8.2"><w n="30.1">À</w> <w n="30.2">laissé</w> <w n="30.3">dans</w> <w n="30.4">vos</w> <w n="30.5">lis</w> <w n="30.6">rayonner</w> <w n="30.7">tant</w> <w n="30.8">de</w> <w n="30.9">flamme</w> !</l>
					<l n="31" num="8.3"><w n="31.1">Niez</w>-<w n="31.2">vous</w> <w n="31.3">ce</w> <w n="31.4">passé</w> <w n="31.5">receleur</w> <w n="31.6">de</w> <w n="31.7">ma</w> <w n="31.8">foi</w>,</l>
					<l n="32" num="8.4"><w n="32.1">Et</w> <w n="32.2">ce</w> <w n="32.3">qu</w>’<w n="32.4">il</w> <w n="32.5">dit</w> <w n="32.6">de</w> <w n="32.7">vous</w> <w n="32.8">quand</w> <w n="32.9">il</w> <w n="32.10">s</w>’<w n="32.11">éveille</w> <w n="32.12">en</w> <w n="32.13">moi</w> !</l>
				</lg>
			</div></body></text></TEI>