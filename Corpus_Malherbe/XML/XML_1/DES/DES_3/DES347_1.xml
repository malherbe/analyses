<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES347">
				<head type="main">POINT D’ADIEU</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Jeunesse</w>, <w n="1.2">adieu</w> ! <w n="1.3">car</w> <w n="1.4">j</w>’<w n="1.5">ai</w> <w n="1.6">beau</w> <w n="1.7">faire</w>,</l>
					<l n="2" num="1.2"><w n="2.1">J</w>’<w n="2.2">ai</w> <w n="2.3">beau</w> <w n="2.4">t</w>’<w n="2.5">étreindre</w> <w n="2.6">et</w> <w n="2.7">te</w> <w n="2.8">presser</w>,</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">ai</w> <w n="3.3">beau</w> <w n="3.4">gémir</w> <w n="3.5">et</w> <w n="3.6">t</w>’<w n="3.7">embrasser</w>,</l>
					<l n="4" num="1.4"><w n="4.1">Nous</w> <w n="4.2">fuyons</w> <w n="4.3">en</w> <w n="4.4">pays</w> <w n="4.5">contraire</w>.</l>
				</lg>
				<lg n="2">
					<l n="5" num="2.1"><w n="5.1">Ton</w> <w n="5.2">souffle</w> <w n="5.3">tiède</w> <w n="5.4">est</w> <w n="5.5">si</w> <w n="5.6">charmant</w> !</l>
					<l n="6" num="2.2"><w n="6.1">On</w> <w n="6.2">est</w> <w n="6.3">si</w> <w n="6.4">beau</w> <w n="6.5">sous</w> <w n="6.6">ta</w> <w n="6.7">couronne</w> !</l>
					<l n="7" num="2.3"><w n="7.1">Tiens</w> : <w n="7.2">ce</w> <w n="7.3">baiser</w> <w n="7.4">que</w> <w n="7.5">je</w> <w n="7.6">te</w> <w n="7.7">donne</w>,</l>
					<l n="8" num="2.4"><w n="8.1">Laisse</w>-<w n="8.2">le</w> <w n="8.3">durer</w> <w n="8.4">un</w> <w n="8.5">moment</w>.</l>
				</lg>
				<lg n="3">
					<l n="9" num="3.1"><w n="9.1">Ce</w> <w n="9.2">long</w> <w n="9.3">baiser</w>, <w n="9.4">douce</w> <w n="9.5">chérie</w>,</l>
					<l n="10" num="3.2"><w n="10.1">Si</w> <w n="10.2">c</w>’<w n="10.3">est</w> <w n="10.4">notre</w> <w n="10.5">adieu</w> <w n="10.6">sans</w> <w n="10.7">retour</w>,</l>
					<l n="11" num="3.3"><w n="11.1">Ne</w> <w n="11.2">le</w> <w n="11.3">romps</w> <w n="11.4">pas</w> <w n="11.5">jusqu</w>’<w n="11.6">au</w> <w n="11.7">détour</w></l>
					<l n="12" num="3.4"><w n="12.1">De</w> <w n="12.2">cette</w> <w n="12.3">haie</w> <w n="12.4">encor</w> <w n="12.5">fleurie</w> !</l>
				</lg>
				<lg n="4">
					<l n="13" num="4.1"><w n="13.1">Si</w> <w n="13.2">j</w>’<w n="13.3">ai</w> <w n="13.4">mal</w> <w n="13.5">porté</w> <w n="13.6">tes</w> <w n="13.7">couleurs</w>,</l>
					<l n="14" num="4.2"><w n="14.1">Ce</w> <w n="14.2">n</w>’<w n="14.3">est</w> <w n="14.4">pas</w> <w n="14.5">ma</w> <w n="14.6">faute</w>, <w n="14.7">ô</w> <w n="14.8">jeunesse</w> !</l>
					<l n="15" num="4.3"><w n="15.1">Le</w> <w n="15.2">vent</w> <w n="15.3">glacé</w> <w n="15.4">de</w> <w n="15.5">la</w> <w n="15.6">tristesse</w></l>
					<l n="16" num="4.4"><w n="16.1">Hâte</w> <w n="16.2">bien</w> <w n="16.3">la</w> <w n="16.4">chute</w> <w n="16.5">des</w> <w n="16.6">fleurs</w> !</l>
				</lg>
				<lg n="5">
					<l n="17" num="5.1"><w n="17.1">Mais</w>, <w n="17.2">ô</w> <w n="17.3">Dieu</w> ! <w n="17.4">par</w> <w n="17.5">combien</w> <w n="17.6">de</w> <w n="17.7">portes</w></l>
					<l n="18" num="5.2"><w n="18.1">Reviennent</w> <w n="18.2">tes</w> <w n="18.3">jours</w> <w n="18.4">triomphans</w> !</l>
					<l n="19" num="5.3"><w n="19.1">Et</w> <w n="19.2">que</w> <w n="19.3">de</w> <w n="19.4">fleurs</w> <w n="19.5">tu</w> <w n="19.6">me</w> <w n="19.7">rapportes</w></l>
					<l n="20" num="5.4"><w n="20.1">Sur</w> <w n="20.2">la</w> <w n="20.3">tête</w> <w n="20.4">de</w> <w n="20.5">mes</w> <w n="20.6">enfans</w> !</l>
				</lg>
			</div></body></text></TEI>