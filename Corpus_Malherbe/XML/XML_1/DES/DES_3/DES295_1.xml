<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES295">
				<head type="main">ROUEN</head>
				<head type="sub_1">À MES SŒURS</head>
				<lg n="1">
					<l n="1" num="1.1"><space quantity="16" unit="char"></space><w n="1.1">Dans</w> <w n="1.2">la</w> <w n="1.3">ville</w> <w n="1.4">tout</w> <w n="1.5">églises</w>,</l>
					<l n="2" num="1.2"><space quantity="16" unit="char"></space><w n="2.1">Où</w> <w n="2.2">je</w> <w n="2.3">descends</w> <w n="2.4">quelquefois</w>,</l>
					<l n="3" num="1.3"><space quantity="16" unit="char"></space><w n="3.1">Où</w> <w n="3.2">devant</w> <w n="3.3">le</w> <w n="3.4">seuil</w> <w n="3.5">assises</w>,</l>
					<l n="4" num="1.4"><space quantity="16" unit="char"></space><w n="4.1">Les</w> <w n="4.2">femmes</w> <w n="4.3">lèvent</w> <w n="4.4">leurs</w> <w n="4.5">voix</w> ;</l>
					<l n="5" num="1.5"><space quantity="16" unit="char"></space><w n="5.1">Dans</w> <w n="5.2">cette</w> <w n="5.3">ville</w> <w n="5.4">où</w> <w n="5.5">bourdonne</w>,</l>
					<l n="6" num="1.6"><space quantity="16" unit="char"></space><w n="6.1">Toute</w> <w n="6.2">idée</w> <w n="6.3">allant</w> <w n="6.4">aux</w> <w n="6.5">cieux</w>,</l>
					<l n="7" num="1.7"><space quantity="16" unit="char"></space><w n="7.1">Où</w> <w n="7.2">les</w> <w n="7.3">yeux</w> <w n="7.4">d</w>’<w n="7.5">une</w> <w n="7.6">madone</w>,</l>
					<l n="8" num="1.8"><space quantity="16" unit="char"></space><w n="8.1">À</w> <w n="8.2">tous</w> <w n="8.3">coins</w> <w n="8.4">cherchent</w> <w n="8.5">vos</w> <w n="8.6">yeux</w> :</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><space quantity="16" unit="char"></space><w n="9.1">Il</w> <w n="9.2">est</w> <w n="9.3">une</w> <w n="9.4">étroite</w> <w n="9.5">porte</w>,</l>
					<l n="10" num="2.2"><space quantity="16" unit="char"></space><w n="10.1">Palais</w> <w n="10.2">de</w> <w n="10.3">mes</w> <w n="10.4">ans</w> <w n="10.5">passés</w>,</l>
					<l n="11" num="2.3"><space quantity="16" unit="char"></space><w n="11.1">Où</w> <w n="11.2">le</w> <w n="11.3">même</w> <w n="11.4">amour</w> <w n="11.5">emporte</w>,</l>
					<l n="12" num="2.4"><space quantity="16" unit="char"></space><w n="12.1">Mon</w> <w n="12.2">âme</w> <w n="12.3">et</w> <w n="12.4">mes</w> <w n="12.5">pieds</w> <w n="12.6">lassés</w>,</l>
					<l n="13" num="2.5"><space quantity="16" unit="char"></space><w n="13.1">Chez</w> <w n="13.2">mes</w> <w n="13.3">sœurs</w> ! <w n="13.4">séjour</w> <w n="13.5">crédule</w>,</l>
					<l n="14" num="2.6"><space quantity="16" unit="char"></space><w n="14.1">Où</w> <w n="14.2">l</w>’<w n="14.3">air</w> <w n="14.4">est</w> <w n="14.5">encor</w> <w n="14.6">si</w> <w n="14.7">pur</w> ;</l>
					<l n="15" num="2.7"><space quantity="16" unit="char"></space><w n="15.1">Où</w> <w n="15.2">Dieu</w> <w n="15.3">gardait</w> <w n="15.4">la</w> <w n="15.5">cellule</w>,</l>
					<l n="16" num="2.8"><space quantity="16" unit="char"></space><w n="16.1">Quand</w> <w n="16.2">j</w>’<w n="16.3">écoutais</w> <w n="16.4">la</w> <w n="16.5">pendule</w>,</l>
					<l n="17" num="2.9"><space quantity="16" unit="char"></space><w n="17.1">Qui</w> <w n="17.2">vit</w> <w n="17.3">et</w> <w n="17.4">bat</w> <w n="17.5">sur</w> <w n="17.6">le</w> <w n="17.7">mur</w>.</l>
				</lg>
				<lg n="3">
					<l n="18" num="3.1"><space quantity="16" unit="char"></space><w n="18.1">Là</w>, <w n="18.2">comme</w> <w n="18.3">la</w> <w n="18.4">sainte</w> <w n="18.5">femme</w></l>
					<l n="19" num="3.2"><space quantity="16" unit="char"></space><w n="19.1">Ouvre</w> <w n="19.2">au</w> <w n="19.3">pauvre</w> <w n="19.4">son</w> <w n="19.5">verger</w>,</l>
					<l n="20" num="3.3"><space quantity="16" unit="char"></space><w n="20.1">Mes</w> <w n="20.2">sœurs</w> <w n="20.3">ont</w> <w n="20.4">toujours</w> <w n="20.5">dans</w> <w n="20.6">l</w>’<w n="20.7">âme</w></l>
					<l n="21" num="3.4"><space quantity="16" unit="char"></space><w n="21.1">Un</w> <w n="21.2">doux</w> <w n="21.3">coin</w> <w n="21.4">pour</w> <w n="21.5">me</w> <w n="21.6">loger</w> ;</l>
					<l n="22" num="3.5"><space quantity="16" unit="char"></space><w n="22.1">Pour</w> <w n="22.2">rappeler</w> <w n="22.3">de</w> <w n="22.4">l</w>’<w n="22.5">enfance</w></l>
					<l n="23" num="3.6"><space quantity="16" unit="char"></space><w n="23.1">Les</w> <w n="23.2">nuits</w> <w n="23.3">qui</w> <w n="23.4">chantaient</w> <w n="23.5">tout</w> <w n="23.6">bas</w> ;</l>
					<l n="24" num="3.7"><space quantity="16" unit="char"></space><w n="24.1">Pour</w> <w n="24.2">me</w> <w n="24.3">rendre</w> <w n="24.4">après</w> <w n="24.5">l</w>’<w n="24.6">absence</w>,</l>
					<l n="25" num="3.8"><space quantity="16" unit="char"></space><w n="25.1">Le</w> <w n="25.2">miroir</w> <w n="25.3">de</w> <w n="25.4">l</w>’<w n="25.5">innocence</w></l>
					<l n="26" num="3.9"><space quantity="16" unit="char"></space><w n="26.1">Que</w> <w n="26.2">mes</w> <w n="26.3">sœurs</w> <w n="26.4">ne</w> <w n="26.5">brisent</w> <w n="26.6">pas</w>.</l>
				</lg>
				<lg n="4">
					<l n="27" num="4.1"><space quantity="16" unit="char"></space><w n="27.1">Le</w> <w n="27.2">long</w> <w n="27.3">de</w> <w n="27.4">l</w>’<w n="27.5">étroite</w> <w n="27.6">rue</w></l>
					<l n="28" num="4.2"><space quantity="16" unit="char"></space><w n="28.1">Où</w> <w n="28.2">tout</w> <w n="28.3">est</w> <w n="28.4">calme</w> <w n="28.5">et</w> <w n="28.6">pensant</w>,</l>
					<l n="29" num="4.3"><space quantity="16" unit="char"></space><w n="29.1">Faible</w> <w n="29.2">étoile</w> <w n="29.3">reparue</w>,</l>
					<l n="30" num="4.4"><space quantity="16" unit="char"></space><w n="30.1">Je</w> <w n="30.2">regarde</w> <w n="30.3">le</w> <w n="30.4">passant</w> ;</l>
					<l n="31" num="4.5"><space quantity="16" unit="char"></space><w n="31.1">Puis</w>, <w n="31.2">tout</w> <w n="31.3">distrait</w>, <w n="31.4">tout</w> <w n="31.5">frivole</w>,</l>
					<l n="32" num="4.6"><space quantity="16" unit="char"></space><w n="32.1">Tout</w> <w n="32.2">léger</w> <w n="32.3">de</w> <w n="32.4">souvenir</w>,</l>
					<l n="33" num="4.7"><space quantity="16" unit="char"></space><w n="33.1">L</w>’<w n="33.2">enfant</w> <w n="33.3">qui</w> <w n="33.4">monte</w> <w n="33.5">à</w> <w n="33.6">l</w>’<w n="33.7">école</w>,</l>
					<l n="34" num="4.8"><space quantity="16" unit="char"></space><w n="34.1">Chercher</w> <w n="34.2">la</w> <w n="34.3">douce</w> <w n="34.4">parole</w>,</l>
					<l n="35" num="4.9"><space quantity="16" unit="char"></space><w n="35.1">Doux</w> <w n="35.2">pain</w> <w n="35.3">de</w> <w n="35.4">son</w> <w n="35.5">avenir</w> !</l>
				</lg>
				<lg n="5">
					<l n="36" num="5.1"><space quantity="16" unit="char"></space><w n="36.1">À</w> <w n="36.2">Rouen</w>, <w n="36.3">ville</w> <w n="36.4">encensée</w></l>
					<l n="37" num="5.2"><space quantity="16" unit="char"></space><w n="37.1">Par</w> <w n="37.2">la</w> <w n="37.3">prière</w> <w n="37.4">et</w> <w n="37.5">les</w> <w n="37.6">flots</w>,</l>
					<l n="38" num="5.3"><space quantity="16" unit="char"></space><w n="38.1">S</w>’<w n="38.2">ouvrirent</w> <w n="38.3">de</w> <w n="38.4">ma</w> <w n="38.5">pensée</w></l>
					<l n="39" num="5.4"><space quantity="16" unit="char"></space><w n="39.1">Les</w> <w n="39.2">hymnes</w> <w n="39.3">et</w> <w n="39.4">les</w> <w n="39.5">sanglots</w> ;</l>
					<l n="40" num="5.5"><space quantity="16" unit="char"></space><w n="40.1">Comme</w> <w n="40.2">la</w> <w n="40.3">brise</w> <w n="40.4">inconnue</w></l>
					<l n="41" num="5.6"><space quantity="16" unit="char"></space><w n="41.1">Chante</w> <w n="41.2">à</w> <w n="41.3">quelque</w> <w n="41.4">vieux</w> <w n="41.5">créneau</w>,</l>
					<l n="42" num="5.7"><space quantity="16" unit="char"></space><w n="42.1">Sur</w> <w n="42.2">la</w> <w n="42.3">grande</w> <w n="42.4">église</w> <w n="42.5">nue</w>,</l>
					<l n="43" num="5.8"><space quantity="16" unit="char"></space><w n="43.1">Qui</w> <w n="43.2">met</w> <w n="43.3">son</w> <w n="43.4">front</w> <w n="43.5">dans</w> <w n="43.6">la</w> <w n="43.7">nue</w>,</l>
					<l n="44" num="5.9"><space quantity="16" unit="char"></space><w n="44.1">Et</w> <w n="44.2">lave</w> <w n="44.3">ses</w> <w n="44.4">pieds</w> <w n="44.5">dans</w> <w n="44.6">l</w>’<w n="44.7">eau</w>.</l>
				</lg>
				<lg n="6">
					<l n="45" num="6.1"><space quantity="16" unit="char"></space><w n="45.1">Mais</w>, <w n="45.2">l</w>’<w n="45.3">église</w> <w n="45.4">de</w> <w n="45.5">mon</w> <w n="45.6">âme</w>,</l>
					<l n="46" num="6.2"><space quantity="16" unit="char"></space><w n="46.1">Où</w> <w n="46.2">pleure</w> <w n="46.3">un</w> <w n="46.4">humble</w> <w n="46.5">métal</w>,</l>
					<l n="47" num="6.3"><space quantity="16" unit="char"></space><w n="47.1">Reflète</w> <w n="47.2">sa</w> <w n="47.3">pure</w> <w n="47.4">flamme</w>,</l>
					<l n="48" num="6.4"><space quantity="16" unit="char"></space><w n="48.1">Dans</w> <w n="48.2">un</w> <w n="48.3">long</w> <w n="48.4">flot</w> <w n="48.5">de</w> <w n="48.6">cristal</w></l>
					<l n="49" num="6.5"><space quantity="16" unit="char"></space><w n="49.1">Cette</w> <w n="49.2">sainte</w> <w n="49.3">au</w> <w n="49.4">flanc</w> <w n="49.5">percée</w>,</l>
					<l n="50" num="6.6"><space quantity="16" unit="char"></space><w n="50.1">Lavant</w> <w n="50.2">ses</w> <w n="50.3">humbles</w> <w n="50.4">pavés</w>,</l>
					<l n="51" num="6.7"><space quantity="16" unit="char"></space><w n="51.1">Semble</w> <w n="51.2">une</w> <w n="51.3">mère</w> <w n="51.4">empressée</w>,</l>
					<l n="52" num="6.8"><space quantity="16" unit="char"></space><w n="52.1">Sur</w> <w n="52.2">ses</w> <w n="52.3">enfans</w> <w n="52.4">abaissée</w>,</l>
					<l n="53" num="6.9"><space quantity="16" unit="char"></space><w n="53.1">Qui</w> <w n="53.2">dit</w> : <w n="53.3">Puisez</w> <w n="53.4">et</w> <w n="53.5">buvez</w> !</l>
				</lg>
				<lg n="7">
					<l n="54" num="7.1"><space quantity="16" unit="char"></space><w n="54.1">C</w>’<w n="54.2">est</w> <w n="54.3">là</w> <w n="54.4">que</w> <w n="54.5">la</w> <w n="54.6">cathédrale</w></l>
					<l n="55" num="7.2"><space quantity="16" unit="char"></space><w n="55.1">Abreuve</w> <w n="55.2">ses</w> <w n="55.3">bénitiers</w> ;</l>
					<l n="56" num="7.3"><space quantity="16" unit="char"></space><w n="56.1">C</w>’<w n="56.2">est</w> <w n="56.3">l</w>’<w n="56.4">éternelle</w> <w n="56.5">lustrale</w></l>
					<l n="57" num="7.4"><space quantity="16" unit="char"></space><w n="57.1">Sauvant</w> <w n="57.2">les</w> <w n="57.3">siècles</w> <w n="57.4">entiers</w>.</l>
					<l n="58" num="7.5"><space quantity="16" unit="char"></space><w n="58.1">Tout</w> <w n="58.2">meurt</w> : <w n="58.3">la</w> <w n="58.4">source</w> <w n="58.5">est</w> <w n="58.6">la</w> <w n="58.7">même</w>,</l>
					<l n="59" num="7.6"><space quantity="16" unit="char"></space><w n="59.1">Dieu</w> <w n="59.2">nourrit</w> <w n="59.3">sa</w> <w n="59.4">fraîche</w> <w n="59.5">voix</w> :</l>
					<l n="60" num="7.7"><space quantity="16" unit="char"></space><w n="60.1">Aussi</w> <w n="60.2">tout</w> <w n="60.3">le</w> <w n="60.4">peuple</w> <w n="60.5">l</w>’<w n="60.6">aime</w></l>
					<l n="61" num="7.8"><space quantity="16" unit="char"></space><w n="61.1">Plus</w> <w n="61.2">que</w> <w n="61.3">le</w> <w n="61.4">dôme</w> <w n="61.5">suprême</w></l>
					<l n="62" num="7.9"><space quantity="16" unit="char"></space><w n="62.1">Où</w> <w n="62.2">se</w> <w n="62.3">font</w> <w n="62.4">sacrer</w> <w n="62.5">les</w> <w n="62.6">rois</w></l>
				</lg>
				<lg n="8">
					<l n="63" num="8.1"><space quantity="16" unit="char"></space><w n="63.1">Par</w> <w n="63.2">un</w> <w n="63.3">hiver</w> <w n="63.4">dur</w> <w n="63.5">et</w> <w n="63.6">sombre</w>,</l>
					<l n="64" num="8.2"><space quantity="16" unit="char"></space><w n="64.1">J</w>’<w n="64.2">ai</w> <w n="64.3">cherché</w> <w n="64.4">ses</w> <w n="64.5">vieux</w> <w n="64.6">autels</w>,</l>
					<l n="65" num="8.3"><space quantity="16" unit="char"></space><w n="65.1">Qui</w> <w n="65.2">dans</w> <w n="65.3">l</w>’<w n="65.4">été</w> <w n="65.5">font</w> <w n="65.6">tant</w> <w n="65.7">d</w>’<w n="65.8">ombre</w></l>
					<l n="66" num="8.4"><space quantity="16" unit="char"></space><w n="66.1">Aux</w> <w n="66.2">fronts</w> <w n="66.3">des</w> <w n="66.4">pauvres</w> <w n="66.5">mortels</w> :</l>
					<l n="67" num="8.5"><space quantity="16" unit="char"></space><w n="67.1">Là</w>, <w n="67.2">pour</w> <w n="67.3">mon</w> <w n="67.4">âme</w> <w n="67.5">exilée</w>,</l>
					<l n="68" num="8.6"><space quantity="16" unit="char"></space><w n="68.1">Couvait</w> <w n="68.2">un</w> <w n="68.3">nouvel</w> <w n="68.4">affront</w> ;</l>
					<l n="69" num="8.7"><space quantity="16" unit="char"></space><w n="69.1">L</w>’<w n="69.2">eau</w> <w n="69.3">bénite</w> <w n="69.4">était</w> <w n="69.5">gelée</w>,</l>
					<l n="70" num="8.8"><space quantity="16" unit="char"></space><w n="70.1">Et</w> <w n="70.2">je</w> <w n="70.3">me</w> <w n="70.4">suis</w> <w n="70.5">en</w> <w n="70.6">allée</w>,</l>
					<l n="71" num="8.9"><space quantity="16" unit="char"></space><w n="71.1">Sans</w> <w n="71.2">désaltérer</w> <w n="71.3">mon</w> <w n="71.4">front</w>.</l>
				</lg>
				<lg n="9">
					<l n="72" num="9.1"><space quantity="16" unit="char"></space><w n="72.1">À</w> <w n="72.2">travers</w> <w n="72.3">les</w> <w n="72.4">brumes</w> <w n="72.5">grises</w></l>
					<l n="73" num="9.2"><space quantity="16" unit="char"></space><w n="73.1">Qui</w> <w n="73.2">resserrent</w> <w n="73.3">l</w>’<w n="73.4">horizon</w>,</l>
					<l n="74" num="9.3"><space quantity="16" unit="char"></space><w n="74.1">Dans</w> <w n="74.2">la</w> <w n="74.3">ville</w> <w n="74.4">tout</w> <w n="74.5">églises</w></l>
					<l n="75" num="9.4"><space quantity="16" unit="char"></space><w n="75.1">Où</w> <w n="75.2">Corneille</w> <w n="75.3">eut</w> <w n="75.4">sa</w> <w n="75.5">maison</w> :</l>
					<l n="76" num="9.5"><space quantity="16" unit="char"></space><w n="76.1">Parmi</w> <w n="76.2">les</w> <w n="76.3">fleurs</w>, <w n="76.4">les</w> <w n="76.5">fontaines</w>,</l>
					<l n="77" num="9.6"><space quantity="16" unit="char"></space><w n="77.1">Les</w> <w n="77.2">clochers</w> <w n="77.3">vibrans</w>, <w n="77.4">les</w> <w n="77.5">tours</w>,</l>
					<l n="78" num="9.7"><space quantity="16" unit="char"></space><w n="78.1">Les</w> <w n="78.2">voilures</w> <w n="78.3">toutes</w> <w n="78.4">pleines</w></l>
					<l n="79" num="9.8"><space quantity="16" unit="char"></space><w n="79.1">Des</w> <w n="79.2">vents</w> <w n="79.3">aux</w> <w n="79.4">moites</w> <w n="79.5">haleines</w>,</l>
					<l n="80" num="9.9"><space quantity="16" unit="char"></space><w n="80.1">Qui</w> <w n="80.2">frôlent</w> <w n="80.3">ses</w> <w n="80.4">verts</w> <w n="80.5">entours</w> :</l>
				</lg>
				<lg n="10">
					<l n="81" num="10.1"><w n="81.1">Dans</w> <w n="81.2">ce</w> <w n="81.3">pays</w> <w n="81.4">aimé</w> <w n="81.5">qui</w> <w n="81.6">me</w> <w n="81.7">fut</w> <w n="81.8">trop</w> <w n="81.9">barbare</w>,</l>
					<l n="82" num="10.2"><w n="82.1">Donnez</w> <w n="82.2">à</w> <w n="82.3">mon</w> <w n="82.4">image</w> <w n="82.5">un</w> <w n="82.6">coin</w> <w n="82.7">rêveur</w> <w n="82.8">et</w> <w n="82.9">doux</w> ;</l>
					<l n="83" num="10.3"><w n="83.1">J</w>’<w n="83.2">ai</w> <w n="83.3">bien</w> <w n="83.4">assez</w> <w n="83.5">pleuré</w> <w n="83.6">l</w>’<w n="83.7">arrêt</w> <w n="83.8">qui</w> <w n="83.9">nous</w> <w n="83.10">sépare</w>,</l>
					<l n="84" num="10.4"><w n="84.1">Pour</w> <w n="84.2">que</w> <w n="84.3">mon</w> <w n="84.4">ombre</w> <w n="84.5">au</w> <w n="84.6">moins</w> <w n="84.7">soit</w> <w n="84.8">heureuse</w> <w n="84.9">avec</w> <w n="84.10">vous</w> !</l>
				</lg>
			</div></body></text></TEI>