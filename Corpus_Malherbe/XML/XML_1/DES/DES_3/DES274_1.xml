<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES274">
				<head type="main">MARGUERITE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Je</w> <w n="1.2">suis</w> <w n="1.3">fleur</w> <w n="1.4">des</w> <w n="1.5">champs</w>,</l>
					<l n="2" num="1.2"><w n="2.1">Mon</w> <w n="2.2">parfum</w> <w n="2.3">m</w>’<w n="2.4">enivre</w> ;</l>
					<l n="3" num="1.3"><w n="3.1">J</w>’<w n="3.2">ai</w> <w n="3.3">trois</w> <w n="3.4">jours</w> <w n="3.5">à</w> <w n="3.6">vivre</w></l>
					<l n="4" num="1.4"><w n="4.1">D</w>’<w n="4.2">arôme</w> <w n="4.3">et</w> <w n="4.4">de</w> <w n="4.5">chants</w> :</l>
					<l n="5" num="1.5"><w n="5.1">J</w>’<w n="5.2">ai</w>, <w n="5.3">comme</w> <w n="5.4">la</w> <w n="5.5">reine</w>,</l>
					<l n="6" num="1.6"><w n="6.1">Ma</w> <w n="6.2">couronne</w> <w n="6.3">au</w> <w n="6.4">front</w> ;</l>
					<l n="7" num="1.7"><w n="7.1">Si</w> <w n="7.2">le</w> <w n="7.3">vent</w> <w n="7.4">l</w>’<w n="7.5">entraîne</w>,</l>
					<l n="8" num="1.8"><w n="8.1">Mon</w> <w n="8.2">deuil</w> <w n="8.3">en</w> <w n="8.4">est</w> <w n="8.5">prompt</w> !</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">Assise</w> <w n="9.2">au</w> <w n="9.3">festin</w></l>
					<l n="10" num="2.2"><w n="10.1">Où</w> <w n="10.2">l</w>’<w n="10.3">été</w> <w n="10.4">m</w>’<w n="10.5">invite</w>,</l>
					<l n="11" num="2.3"><w n="11.1">Je</w> <w n="11.2">vis</w>, <w n="11.3">je</w> <w n="11.4">meurs</w> <w n="11.5">vite</w> ;</l>
					<l n="12" num="2.4"><w n="12.1">Merci</w>, <w n="12.2">mon</w> <w n="12.3">Destin</w> !</l>
					<l n="13" num="2.5"><w n="13.1">Le</w> <w n="13.2">chêne</w> <w n="13.3">superbe</w></l>
					<l n="14" num="2.6"><w n="14.1">Parle</w> <w n="14.2">de</w> <w n="14.3">l</w>’<w n="14.4">hiver</w> :</l>
					<l n="15" num="2.7"><w n="15.1">L</w>’<w n="15.2">été</w> <w n="15.3">seul</w> <w n="15.4">dans</w> <w n="15.5">l</w>’<w n="15.6">herbe</w>,</l>
					<l n="16" num="2.8"><w n="16.1">Baise</w> <w n="16.2">mon</w> <w n="16.3">pied</w> <w n="16.4">vert</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Je</w> <w n="17.2">nais</w> <w n="17.3">dans</w> <w n="17.4">un</w> <w n="17.5">lieu</w></l>
					<l n="18" num="3.2"><w n="18.1">Où</w> <w n="18.2">meurt</w> <w n="18.3">la</w> <w n="18.4">tempête</w> ;</l>
					<l n="19" num="3.3"><w n="19.1">J</w>’<w n="19.2">entends</w> <w n="19.3">sur</w> <w n="19.4">ma</w> <w n="19.5">tête</w></l>
					<l n="20" num="3.4"><w n="20.1">L</w>’<w n="20.2">oiseau</w> <w n="20.3">du</w> <w n="20.4">bon</w> <w n="20.5">Dieu</w>.</l>
					<l n="21" num="3.5"><w n="21.1">Je</w> <w n="21.2">vois</w> <w n="21.3">ma</w> <w n="21.4">peinture</w></l>
					<l n="22" num="3.6"><w n="22.1">Dans</w> <w n="22.2">le</w> <w n="22.3">ruisseau</w> <w n="22.4">clair</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Et</w> <w n="23.2">pour</w> <w n="23.3">nourriture</w>,</l>
					<l n="24" num="3.8"><w n="24.1">Je</w> <w n="24.2">moissonne</w> <w n="24.3">l</w>’<w n="24.4">air</w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">J</w>’<w n="25.2">assiste</w> <w n="25.3">trois</w> <w n="25.4">fois</w>.</l>
					<l n="26" num="4.2"><w n="26.1">Aux</w> <w n="26.2">nuits</w> <w n="26.3">de</w> <w n="26.4">la</w> <w n="26.5">terre</w>,</l>
					<l n="27" num="4.3"><w n="27.1">À</w> <w n="27.2">l</w>’<w n="27.3">ardent</w> <w n="27.4">mystère</w></l>
					<l n="28" num="4.4"><w n="28.1">De</w> <w n="28.2">ses</w> <w n="28.3">mille</w> <w n="28.4">voix</w> ;</l>
					<l n="29" num="4.5"><w n="29.1">Qu</w>’<w n="29.2">apprendrais</w>-<w n="29.3">je</w> <w n="29.4">encore</w> ?</l>
					<l n="30" num="4.6"><w n="30.1">Trop</w> <w n="30.2">savoir</w> <w n="30.3">fait</w> <w n="30.4">peur</w> :</l>
					<l n="31" num="4.7"><w n="31.1">J</w>’<w n="31.2">éprouve</w> <w n="31.3">et</w> <w n="31.4">j</w>’<w n="31.5">ignore</w>,</l>
					<l n="32" num="4.8"><w n="32.1">Je</w> <w n="32.2">sais</w> <w n="32.3">le</w> <w n="32.4">bonheur</w> !</l>
				</lg>
			</div></body></text></TEI>