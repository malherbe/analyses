<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader>
		<fileDesc>
			<titleStmt>
				<title type="main">BOUQUETS ET PRIÈRES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>3255 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_3</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblFull>
					<titleStmt>
						<title type="main">Bouquets et Prières</title>
						<author>Marceline Desbordes-Valmore</author>
					</titleStmt>
					<publicationStmt>
						<publisher>gallica.bnf.fr</publisher>
						<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p/f1n316.texteBrut</idno>
					</publicationStmt>
					<sourceDesc>
						<biblStruct>
							<monogr>
								<title>Bouquets et Prières</title>
								<author>Marceline Desbordes-Valmore</author>
								<idno type="URL">http://gallica.bnf.fr/ark:/12148/bpt6k5510985p.r=bouquets+et+pri%C3%A8res.langFR</idno>
								<imprint>
									<pubPlace>Paris</pubPlace>
									<publisher>Dumont</publisher>
									<date when="1843">1843</date>
								</imprint>
							</monogr>
						</biblStruct>
					</sourceDesc>
				</biblFull>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>3255 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1843">1843</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique s’inscrit dans un projet de constitution d’un corpus de textes versifiés
					pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>La préface de l’auteur n’a pas été intégrée.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées par le correcteur orthographique.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-01-23" who="RR">Révision de l’entête pour validation</change>
				<change when="2016-12-20" who="RR">insertion des corrections métriques dans le corps du texte</change>
			</listChange>
		</revisionDesc>




	</teiHeader><text><body><div type="poem" key="DES273">
				<head type="main">ÂME ET JEUNESSE</head>
				<lg n="1">
					<l n="1" num="1.1"><w n="1.1">Puisque</w> <w n="1.2">de</w> <w n="1.3">l</w>’<w n="1.4">enfance</w> <w n="1.5">envolée</w>,</l>
					<l n="2" num="1.2"><space quantity="8" unit="char"></space><w n="2.1">Le</w> <w n="2.2">rêve</w> <w n="2.3">blanc</w>,</l>
					<l n="3" num="1.3"><w n="3.1">Comme</w> <w n="3.2">l</w>’<w n="3.3">oiseau</w> <w n="3.4">dans</w> <w n="3.5">la</w> <w n="3.6">vallée</w>,</l>
					<l n="4" num="1.4"><space quantity="8" unit="char"></space><w n="4.1">Fuit</w> <w n="4.2">d</w>’<w n="4.3">un</w> <w n="4.4">élan</w> ;</l>
					<l n="5" num="1.5"><w n="5.1">Puisque</w> <w n="5.2">mon</w> <w n="5.3">Auteur</w> <w n="5.4">adorable</w></l>
					<l n="6" num="1.6"><space quantity="8" unit="char"></space><w n="6.1">Me</w> <w n="6.2">fait</w> <w n="6.3">errer</w></l>
					<l n="7" num="1.7"><w n="7.1">Sur</w> <w n="7.2">la</w> <w n="7.3">terre</w>, <w n="7.4">ou</w> <w n="7.5">rien</w> <w n="7.6">n</w>’<w n="7.7">est</w> <w n="7.8">durable</w>,</l>
					<l n="8" num="1.8"><space quantity="8" unit="char"></space><w n="8.1">Que</w> <w n="8.2">d</w>’<w n="8.3">espérer</w> ;</l>
				</lg>
				<lg n="2">
					<l n="9" num="2.1"><w n="9.1">À</w> <w n="9.2">moi</w> <w n="9.3">jeunesse</w>, <w n="9.4">abeille</w> <w n="9.5">blonde</w>,</l>
					<l n="10" num="2.2"><space quantity="8" unit="char"></space><w n="10.1">Aux</w> <w n="10.2">ailes</w> <w n="10.3">d</w>’<w n="10.4">or</w> !</l>
					<l n="11" num="2.3"><w n="11.1">Prenez</w> <w n="11.2">une</w> <w n="11.3">âme</w>, <w n="11.4">et</w> <w n="11.5">par</w> <w n="11.6">le</w> <w n="11.7">monde</w></l>
					<l n="12" num="2.4"><space quantity="8" unit="char"></space><w n="12.1">Prenons</w> <w n="12.2">l</w>’<w n="12.3">essor</w> :</l>
					<l n="13" num="2.5"><w n="13.1">Avançons</w>, <w n="13.2">l</w>’<w n="13.3">une</w> <w n="13.4">emportant</w> <w n="13.5">l</w>’<w n="13.6">autre</w>,</l>
					<l n="14" num="2.6"><space quantity="8" unit="char"></space><w n="14.1">Lumière</w> <w n="14.2">et</w> <w n="14.3">fleur</w>,</l>
					<l n="15" num="2.7"><w n="15.1">Vous</w> <w n="15.2">sur</w> <w n="15.3">ma</w> <w n="15.4">foi</w>, <w n="15.5">moi</w> <w n="15.6">sur</w> <w n="15.7">la</w> <w n="15.8">vôtre</w>,</l>
					<l n="16" num="2.8"><space quantity="8" unit="char"></space><w n="16.1">Vers</w> <w n="16.2">le</w> <w n="16.3">bonheur</w> !</l>
				</lg>
				<lg n="3">
					<l n="17" num="3.1"><w n="17.1">Vous</w> <w n="17.2">êtes</w>, <w n="17.3">belle</w> <w n="17.4">enfant</w>, <w n="17.5">ma</w> <w n="17.6">robe</w>,</l>
					<l n="18" num="3.2"><space quantity="8" unit="char"></space><w n="18.1">Perles</w> <w n="18.2">et</w> <w n="18.3">fil</w> ;</l>
					<l n="19" num="3.3"><w n="19.1">Le</w> <w n="19.2">fin</w> <w n="19.3">voile</w> <w n="19.4">où</w> <w n="19.5">je</w> <w n="19.6">me</w> <w n="19.7">dérobe</w></l>
					<l n="20" num="3.4"><space quantity="8" unit="char"></space><w n="20.1">Dans</w> <w n="20.2">mon</w> <w n="20.3">exil</w>.</l>
					<l n="21" num="3.5"><w n="21.1">Comme</w> <w n="21.2">la</w> <w n="21.3">mésange</w> <w n="21.4">s</w>’<w n="21.5">appuie</w></l>
					<l n="22" num="3.6"><space quantity="8" unit="char"></space><w n="22.1">Au</w> <w n="22.2">vert</w> <w n="22.3">roseau</w>,</l>
					<l n="23" num="3.7"><w n="23.1">Vous</w> <w n="23.2">êtes</w> <w n="23.3">le</w> <w n="23.4">soutien</w> <w n="23.5">qui</w> <w n="23.6">plie</w> ;</l>
					<l n="24" num="3.8"><space quantity="8" unit="char"></space><w n="24.1">Je</w> <w n="24.2">suis</w> <w n="24.3">l</w>’<w n="24.4">oiseau</w> !</l>
				</lg>
				<lg n="4">
					<l n="25" num="4.1"><w n="25.1">Bouquets</w> <w n="25.2">défaits</w>, <w n="25.3">tête</w> <w n="25.4">penchée</w>,</l>
					<l n="26" num="4.2"><space quantity="8" unit="char"></space><w n="26.1">Du</w> <w n="26.2">soir</w> <w n="26.3">au</w> <w n="26.4">jour</w>,</l>
					<l n="27" num="4.3"><w n="27.1">Jeunesse</w> ! <w n="27.2">on</w> <w n="27.3">vous</w> <w n="27.4">dirait</w> <w n="27.5">fâchée</w></l>
					<l n="28" num="4.4"><space quantity="8" unit="char"></space><w n="28.1">Contre</w> <w n="28.2">l</w>’<w n="28.3">amour</w> :</l>
					<l n="29" num="4.5"><w n="29.1">L</w>’<w n="29.2">amour</w> <w n="29.3">luit</w> <w n="29.4">d</w>’<w n="29.5">orage</w> <w n="29.6">en</w> <w n="29.7">orage</w> ;</l>
					<l n="30" num="4.6"><space quantity="8" unit="char"></space><w n="30.1">Il</w> <w n="30.2">faut</w> <w n="30.3">souvent</w>,</l>
					<l n="31" num="4.7"><w n="31.1">Pour</w> <w n="31.2">l</w>’<w n="31.3">aborder</w>, <w n="31.4">bien</w> <w n="31.5">du</w> <w n="31.6">courage</w></l>
					<l n="32" num="4.8"><space quantity="8" unit="char"></space><w n="32.1">Contre</w> <w n="32.2">le</w> <w n="32.3">vent</w> !</l>
				</lg>
				<lg n="5">
					<l n="33" num="5.1"><w n="33.1">L</w>’<w n="33.2">amour</w> <w n="33.3">c</w>’<w n="33.4">est</w> <w n="33.5">Dieu</w>, <w n="33.6">jeunesse</w> <w n="33.7">aimée</w> ;</l>
					<l n="34" num="5.2"><space quantity="8" unit="char"></space><w n="34.1">Oh</w> ! <w n="34.2">n</w>’<w n="34.3">allez</w> <w n="34.4">pas</w>,</l>
					<l n="35" num="5.3"><w n="35.1">Pour</w> <w n="35.2">trouver</w> <w n="35.3">sa</w> <w n="35.4">trace</w> <w n="35.5">enflammée</w>,</l>
					<l n="36" num="5.4"><space quantity="8" unit="char"></space><w n="36.1">Chercher</w> <w n="36.2">en</w> <w n="36.3">bas</w> :</l>
					<l n="37" num="5.5"><w n="37.1">En</w> <w n="37.2">bas</w> <w n="37.3">tout</w> <w n="37.4">se</w> <w n="37.5">corrompt</w>, <w n="37.6">tout</w> <w n="37.7">tombe</w>,</l>
					<l n="38" num="5.6"><space quantity="8" unit="char"></space><w n="38.1">Roses</w> <w n="38.2">et</w> <w n="38.3">miel</w> ;</l>
					<l n="39" num="5.7"><w n="39.1">Les</w> <w n="39.2">couronnes</w> <w n="39.3">vont</w> <w n="39.4">à</w> <w n="39.5">la</w> <w n="39.6">tombe</w>,</l>
					<l n="40" num="5.8"><space quantity="8" unit="char"></space><w n="40.1">L</w>’<w n="40.2">amour</w> <w n="40.3">au</w> <w n="40.4">ciel</w> !</l>
				</lg>
				<lg n="6">
					<l n="41" num="6.1"><w n="41.1">Dans</w> <w n="41.2">peu</w>, <w n="41.3">bien</w> <w n="41.4">peu</w>, <w n="41.5">j</w>’<w n="41.6">aurai</w> <w n="41.7">beau</w> <w n="41.8">faire</w> ;</l>
					<l n="42" num="6.2"><space quantity="8" unit="char"></space><w n="42.1">Chemin</w> <w n="42.2">courant</w>,</l>
					<l n="43" num="6.3"><w n="43.1">Nous</w> <w n="43.2">prendrons</w> <w n="43.3">un</w> <w n="43.4">chemin</w> <w n="43.5">contraire</w>,</l>
					<l n="44" num="6.4"><space quantity="8" unit="char"></space><w n="44.1">En</w> <w n="44.2">nous</w> <w n="44.3">pleurant</w>-.</l>
					<l n="45" num="6.5"><w n="45.1">Vous</w> <w n="45.2">habillerez</w> <w n="45.3">une</w> <w n="45.4">autre</w> <w n="45.5">âme</w></l>
					<l n="46" num="6.6"><space quantity="8" unit="char"></space><w n="46.1">Qui</w> <w n="46.2">descendra</w>,</l>
					<l n="47" num="6.7"><w n="47.1">Et</w> <w n="47.2">toujours</w> <w n="47.3">l</w>’<w n="47.4">éternelle</w> <w n="47.5">flamme</w></l>
					<l n="48" num="6.8"><space quantity="8" unit="char"></space><w n="48.1">Vous</w> <w n="48.2">nourrira</w> !</l>
				</lg>
				<lg n="7">
					<l n="49" num="7.1"><w n="49.1">Vous</w> <w n="49.2">irez</w> <w n="49.3">où</w> <w n="49.4">va</w> <w n="49.5">chanter</w> <w n="49.6">l</w>’<w n="49.7">heure</w>,</l>
					<l n="50" num="7.2"><space quantity="8" unit="char"></space><w n="50.1">Volant</w> <w n="50.2">toujours</w> ;</l>
					<l n="51" num="7.3"><w n="51.1">Vous</w> <w n="51.2">irez</w> <w n="51.3">où</w> <w n="51.4">va</w> <w n="51.5">l</w>’<w n="51.6">eau</w> <w n="51.7">qui</w> <w n="51.8">pleure</w>,</l>
					<l n="52" num="7.4"><space quantity="8" unit="char"></space><w n="52.1">Où</w> <w n="52.2">vont</w> <w n="52.3">les</w> <w n="52.4">jours</w> ;</l>
					<l n="53" num="7.5"><w n="53.1">Jeunesse</w> ! <w n="53.2">vous</w> <w n="53.3">irez</w> <w n="53.4">dansante</w>,</l>
					<l n="54" num="7.6"><space quantity="8" unit="char"></space><w n="54.1">À</w> <w n="54.2">qui</w> <w n="54.3">rira</w>,</l>
					<l n="55" num="7.7"><w n="55.1">Quand</w> <w n="55.2">la</w> <w n="55.3">vieillesse</w> <w n="55.4">pâlissante</w></l>
					<l n="56" num="7.8"><space quantity="8" unit="char"></space><w n="56.1">M</w>’<w n="56.2">enfermera</w> !</l>
				</lg>
				<lg n="8">
					<l n="57" num="8.1"><w n="57.1">Mais</w>, <w n="57.2">pour</w> <w n="57.3">que</w> <w n="57.4">je</w> <w n="57.5">rentre</w> <w n="57.6">légère</w></l>
					<l n="58" num="8.2"><space quantity="8" unit="char"></space><w n="58.1">Au</w> <w n="58.2">nid</w> <w n="58.3">divin</w>,</l>
					<l n="59" num="8.3"><w n="59.1">Je</w> <w n="59.2">ne</w> <w n="59.3">viens</w> <w n="59.4">pas</w> <w n="59.5">chez</w> <w n="59.6">vous</w>, <w n="59.7">ma</w> <w n="59.8">chère</w>,</l>
					<l n="60" num="8.4"><space quantity="8" unit="char"></space><w n="60.1">Loger</w> <w n="60.2">en</w> <w n="60.3">vain</w> :</l>
					<l n="61" num="8.5"><w n="61.1">Il</w> <w n="61.2">faut</w> <w n="61.3">que</w> <w n="61.4">j</w>’<w n="61.5">aime</w> <w n="61.6">et</w> <w n="61.7">que</w> <w n="61.8">je</w> <w n="61.9">pleure</w></l>
					<l n="62" num="8.6"><space quantity="8" unit="char"></space><w n="62.1">Avec</w> <w n="62.2">vos</w> <w n="62.3">yeux</w>,</l>
					<l n="63" num="8.7"><w n="63.1">Pour</w> <w n="63.2">racheter</w>, <w n="63.3">heure</w> <w n="63.4">par</w> <w n="63.5">heure</w>,</l>
					<l n="64" num="8.8"><space quantity="8" unit="char"></space><w n="64.1">Quelque</w> <w n="64.2">âme</w> <w n="64.3">aux</w> <w n="64.4">cieux</w> !</l>
				</lg>
			</div></body></text></TEI>