<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES162">
						<head type="main">AUX ENFANTS QUI NE SONT PLUS</head>
						<opener>
							<epigraph>
								<cit>
									<quote>
										Bien plus heureux que nous, vous n’avez <lb></lb>fait que tremper vos lèvres dans cette <lb></lb>coupe d’amertume qu’il nous faut <lb></lb>épuiser.
									</quote>
									<bibl><hi rend="smallcap">M. Chessière</hi><hi rend="ital">, Ministre protestant.</hi></bibl>
								</cit>
							</epigraph>
						</opener>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Vous</w> ! <w n="1.2">à</w> <w n="1.3">peine</w> <w n="1.4">entrevus</w> <w n="1.5">au</w> <w n="1.6">terrestre</w> <w n="1.7">séjour</w>,</l>
							<l n="2" num="1.2"><space quantity="6" unit="char"></space><w n="2.1">Beaux</w> <w n="2.2">enfants</w> ! <w n="2.3">voyageurs</w> <w n="2.4">d</w>’<w n="2.5">un</w> <w n="2.6">jour</w>,</l>
							<l n="3" num="1.3"><w n="3.1">Quand</w> <w n="3.2">les</w> <w n="3.3">astres</w> <w n="3.4">sont</w> <w n="3.5">purs</w>, <w n="3.6">dans</w> <w n="3.7">leurs</w> <w n="3.8">tremblantes</w> <w n="3.9">flammes</w></l>
							<l n="4" num="1.4"><space quantity="6" unit="char"></space><w n="4.1">Voit</w>-<w n="4.2">on</w> <w n="4.3">flotter</w> <w n="4.4">vos</w> <w n="4.5">jeunes</w> <w n="4.6">âmes</w> ?</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><space quantity="6" unit="char"></space><w n="5.1">Vous</w> <w n="5.2">qui</w> <w n="5.3">passez</w> <w n="5.4">comme</w> <w n="5.5">les</w> <w n="5.6">fleurs</w>,</l>
							<l n="6" num="2.2"><space quantity="6" unit="char"></space><w n="6.1">Qui</w> <w n="6.2">ne</w> <w n="6.3">semblez</w> <w n="6.4">toucher</w> <w n="6.5">la</w> <w n="6.6">terre</w></l>
							<l n="7" num="2.3"><w n="7.1">Que</w> <w n="7.2">pour</w> <w n="7.3">vous</w> <w n="7.4">envoler</w> <w n="7.5">tout</w> <w n="7.6">baignés</w> <w n="7.7">de</w> <w n="7.8">nos</w> <w n="7.9">pleurs</w>,</l>
							<l n="8" num="2.4"><w n="8.1">Enfants</w>, <w n="8.2">révélez</w>-<w n="8.3">nous</w> <w n="8.4">le</w> <w n="8.5">triste</w> <w n="8.6">et</w> <w n="8.7">doux</w> <w n="8.8">mystère</w></l>
							<l n="9" num="2.5"><w n="9.1">D</w>’<w n="9.2">une</w> <w n="9.3">apparition</w> <w n="9.4">qui</w> <w n="9.5">fait</w> <w n="9.6">rêver</w> <w n="9.7">le</w> <w n="9.8">ciel</w>,</l>
							<l n="10" num="2.6"><w n="10.1">Et</w> <w n="10.2">de</w> <w n="10.3">votre</w> <w n="10.4">départ</w> <w n="10.5">si</w> <w n="10.6">prompt</w> <w n="10.7">et</w> <w n="10.8">si</w> <w n="10.9">cruel</w>.</l>
						</lg>
						<lg n="3">
							<l n="11" num="3.1"><w n="11.1">Eh</w> ! <w n="11.2">comment</w> <w n="11.3">voyons</w>-<w n="11.4">nous</w> <w n="11.5">nos</w> <w n="11.6">plus</w> <w n="11.7">pures</w> <w n="11.8">délices</w></l>
							<l n="12" num="3.2"><space quantity="6" unit="char"></space><w n="12.1">Se</w> <w n="12.2">changer</w> <w n="12.3">en</w> <w n="12.4">amers</w> <w n="12.5">calices</w></l>
							<l n="13" num="3.3"><space quantity="6" unit="char"></space><w n="13.1">Pleins</w> <w n="13.2">d</w>’<w n="13.3">inépuisables</w> <w n="13.4">regrets</w> ?</l>
							<l n="14" num="3.4"><w n="14.1">De</w> <w n="14.2">ces</w> <w n="14.3">sources</w> <w n="14.4">de</w> <w n="14.5">pleurs</w> <w n="14.6">contez</w>-<w n="14.7">nous</w> <w n="14.8">les</w> <w n="14.9">secrets</w>.</l>
							<l n="15" num="3.5"><w n="15.1">Fleurs</w> <w n="15.2">des</w> <w n="15.3">tendres</w> <w n="15.4">amours</w> ! <w n="15.5">ne</w> <w n="15.6">laissez</w>-<w n="15.7">vous</w> <w n="15.8">de</w> <w n="15.9">traces</w></l>
							<l n="16" num="3.6"><w n="16.1">Que</w> <w n="16.2">vos</w> <w n="16.3">chastes</w> <w n="16.4">baisers</w>, <w n="16.5">que</w> <w n="16.6">vos</w> <w n="16.7">tranquilles</w> <w n="16.8">grâces</w>,</l>
							<l n="17" num="3.7"><w n="17.1">Vos</w> <w n="17.2">larmes</w> <w n="17.3">sans</w> <w n="17.4">remords</w>, <w n="17.5">vos</w> <w n="17.6">voix</w> <w n="17.7">d</w>’<w n="17.8">anges</w> <w n="17.9">mortels</w>,</l>
							<l n="18" num="3.8"><w n="18.1">Qui</w> <w n="18.2">font</w> <w n="18.3">des</w> <w n="18.4">cœurs</w> <w n="18.5">aimants</w> <w n="18.6">vos</w> <w n="18.7">douloureux</w> <w n="18.8">autels</w> ?</l>
							<l n="19" num="3.9"><space quantity="6" unit="char"></space><w n="19.1">Sous</w> <w n="19.2">une</w> <w n="19.3">forme</w> <w n="19.4">périssable</w>,</l>
							<l n="20" num="3.10"><w n="20.1">N</w>’<w n="20.2">êtes</w>-<w n="20.3">vous</w> <w n="20.4">pas</w> <w n="20.5">des</w> <w n="20.6">cieux</w> <w n="20.7">les</w> <w n="20.8">jeunes</w> <w n="20.9">messagers</w> ?</l>
							<l n="21" num="3.11"><space quantity="6" unit="char"></space><w n="21.1">Et</w> <w n="21.2">vos</w> <w n="21.3">sourires</w> <w n="21.4">passagers</w></l>
							<l n="22" num="3.12"><w n="22.1">Portent</w>-<w n="22.2">ils</w> <w n="22.3">de</w> <w n="22.4">la</w> <w n="22.5">foi</w> <w n="22.6">l</w>’<w n="22.7">empreinte</w> <w n="22.8">ineffaçable</w> ?</l>
						</lg>
						<lg n="4">
							<l n="23" num="4.1"><w n="23.1">Venez</w>-<w n="23.2">vous</w> <w n="23.3">en</w> <w n="23.4">courant</w> <w n="23.5">dire</w> : « <w n="23.6">Préparez</w>-<w n="23.7">vous</w> !</l>
							<l n="24" num="4.2"><w n="24.1">Bientôt</w> <w n="24.2">vous</w> <w n="24.3">quitterez</w> <w n="24.4">ce</w> <w n="24.5">que</w> <w n="24.6">l</w>’<w n="24.7">on</w> <w n="24.8">croit</w> <w n="24.9">la</w> <w n="24.10">vie</w> ;</l>
							<l n="25" num="4.3"><w n="25.1">Celle</w> <w n="25.2">qui</w> <w n="25.3">vous</w> <w n="25.4">attend</w> <w n="25.5">seule</w> <w n="25.6">est</w> <w n="25.7">digne</w> <w n="25.8">d</w>’<w n="25.9">envie</w> :</l>
							<l n="26" num="4.4"><w n="26.1">Oh</w> ! <w n="26.2">venez</w> <w n="26.3">dans</w> <w n="26.4">le</w> <w n="26.5">ciel</w> <w n="26.6">la</w> <w n="26.7">goûter</w> <w n="26.8">avec</w> <w n="26.9">nous</w> !</l>
							<l n="27" num="4.5"><w n="27.1">Ne</w> <w n="27.2">craignez</w> <w n="27.3">pas</w>, <w n="27.4">venez</w> ! <w n="27.5">Dieu</w> <w n="27.6">règne</w> <w n="27.7">sans</w> <w n="27.8">colère</w> ;</l>
							<l n="28" num="4.6"><w n="28.1">De</w> <w n="28.2">nos</w> <w n="28.3">destins</w> <w n="28.4">charmants</w> <w n="28.5">vous</w> <w n="28.6">aurez</w> <w n="28.7">la</w> <w n="28.8">moitié</w>.</l>
							<l n="29" num="4.7"><w n="29.1">Celui</w> <w n="29.2">qui</w> <w n="29.3">pleure</w>, <w n="29.4">hélas</w> ! <w n="29.5">ne</w> <w n="29.6">peut</w> <w n="29.7">plus</w> <w n="29.8">lui</w> <w n="29.9">déplaire</w> ;</l>
							<l n="30" num="4.8"><space quantity="2" unit="char"></space><w n="30.1">Le</w> <w n="30.2">méchant</w> <w n="30.3">même</w> <w n="30.4">a</w> <w n="30.5">part</w> <w n="30.6">dans</w> <w n="30.7">sa</w> <w n="30.8">pitié</w>.</l>
							<l n="31" num="4.9"><w n="31.1">Sous</w> <w n="31.2">sa</w> <w n="31.3">main</w> <w n="31.4">qu</w>’<w n="31.5">il</w> <w n="31.6">étend</w> <w n="31.7">toute</w> <w n="31.8">plaie</w> <w n="31.9">est</w> <w n="31.10">fermée</w> ;</l>
							<l n="32" num="4.10"><w n="32.1">Qui</w> <w n="32.2">se</w> <w n="32.3">jette</w> <w n="32.4">en</w> <w n="32.5">son</w> <w n="32.6">sein</w> <w n="32.7">ne</w> <w n="32.8">craint</w> <w n="32.9">plus</w> <w n="32.10">l</w>’<w n="32.11">abandon</w> ;</l>
							<l n="33" num="4.11"><w n="33.1">Et</w> <w n="33.2">le</w> <w n="33.3">sillon</w> <w n="33.4">cuisant</w> <w n="33.5">d</w>’<w n="33.6">une</w> <w n="33.7">larme</w> <w n="33.8">enflammée</w></l>
							<l n="34" num="4.12"><space quantity="6" unit="char"></space><w n="34.1">S</w>’<w n="34.2">efface</w> <w n="34.3">au</w> <w n="34.4">souffle</w> <w n="34.5">du</w> <w n="34.6">pardon</w>.</l>
							<l n="35" num="4.13"><space quantity="6" unit="char"></space><w n="35.1">Embrassez</w>-<w n="35.2">nous</w> ! <w n="35.3">Dieu</w> <w n="35.4">nous</w> <w n="35.5">rappelle</w> :</l>
							<l n="36" num="4.14"><w n="36.1">Nous</w> <w n="36.2">allons</w> <w n="36.3">devant</w> <w n="36.4">vous</w> ; <w n="36.5">mères</w>, <w n="36.6">ne</w> <w n="36.7">pleurez</w> <w n="36.8">pas</w> !</l>
							<l n="37" num="4.15"><w n="37.1">Car</w> <w n="37.2">vous</w> <w n="37.3">aurez</w> <w n="37.4">un</w> <w n="37.5">jour</w> <w n="37.6">une</w> <w n="37.7">joie</w> <w n="37.8">immortelle</w>,</l>
							<l n="38" num="4.16"><w n="38.1">Et</w> <w n="38.2">vos</w> <w n="38.3">petits</w> <w n="38.4">enfants</w> <w n="38.5">souriront</w> <w n="38.6">dans</w> <w n="38.7">vos</w> <w n="38.8">bras</w>. »</l>
							<l n="39" num="4.17"><w n="39.1">Ainsi</w> <w n="39.2">vous</w> <w n="39.3">nous</w> <w n="39.4">quittez</w>, <w n="39.5">innocentes</w> <w n="39.6">colombes</w>,</l>
							<l n="40" num="4.18"><w n="40.1">Et</w> <w n="40.2">sur</w> <w n="40.3">nos</w> <w n="40.4">toits</w> <w n="40.5">d</w>’<w n="40.6">exil</w> <w n="40.7">vous</w> <w n="40.8">planez</w> <w n="40.9">un</w> <w n="40.10">moment</w>,</l>
							<l n="41" num="4.19"><w n="41.1">Pour</w> <w n="41.2">écouter</w> <w n="41.3">peut</w>-<w n="41.4">être</w> <w n="41.5">avec</w> <w n="41.6">étonnement</w></l>
							<l n="42" num="4.20"><w n="42.1">Les</w> <w n="42.2">cris</w> <w n="42.3">que</w> <w n="42.4">nous</w> <w n="42.5">jetons</w> <w n="42.6">à</w> <w n="42.7">l</w>’<w n="42.8">entour</w> <w n="42.9">de</w> <w n="42.10">vos</w> <w n="42.11">tombes</w>.</l>
							<l n="43" num="4.21"><w n="43.1">Ah</w> ! <w n="43.2">du</w> <w n="43.3">moins</w> <w n="43.4">emportez</w> <w n="43.5">au</w> <w n="43.6">sein</w> <w n="43.7">de</w> <w n="43.8">notre</w> <w n="43.9">Dieu</w></l>
							<l n="44" num="4.22"><w n="44.1">Les</w> <w n="44.2">sanglots</w> <w n="44.3">dont</w> <w n="44.4">la</w> <w n="44.5">terre</w> <w n="44.6">escorte</w> <w n="44.7">votre</w> <w n="44.8">adieu</w>.</l>
							<l n="45" num="4.23"><w n="45.1">Allez</w> <w n="45.2">du</w> <w n="45.3">moins</w> <w n="45.4">lui</w> <w n="45.5">dire</w> : « <w n="45.6">Il</w> <w n="45.7">est</w> <w n="45.8">toujours</w> <w n="45.9">des</w> <w n="45.10">mères</w>,</l>
							<l n="46" num="4.24"><w n="46.1">Des</w> <w n="46.2">femmes</w> <w n="46.3">pour</w> <w n="46.4">aimer</w>, <w n="46.5">pour</w> <w n="46.6">attendre</w> <w n="46.7">et</w> <w n="46.8">souffrir</w>,</l>
							<l n="47" num="4.25"><w n="47.1">Pour</w> <w n="47.2">acheter</w> <w n="47.3">longtemps</w>, <w n="47.4">par</w> <w n="47.5">des</w> <w n="47.6">peines</w> <w n="47.7">amères</w>,</l>
							<l n="48" num="4.26"><space quantity="10" unit="char"></space><w n="48.1">Le</w> <w n="48.2">bonheur</w> <w n="48.3">de</w> <w n="48.4">mourir</w> ! »</l>
							<l n="49" num="4.27"><w n="49.1">Ah</w> ! <w n="49.2">dites</w>-<w n="49.3">lui</w> : « <w n="49.4">Toujours</w> <w n="49.5">les</w> <w n="49.6">hommes</w> <w n="49.7">sont</w> <w n="49.8">à</w> <w n="49.9">plaindre</w> ;</l>
							<l n="50" num="4.28"><w n="50.1">En</w> <w n="50.2">vous</w> <w n="50.3">nommant</w>, <w n="50.4">Seigneur</w>, <w n="50.5">ils</w> <w n="50.6">ne</w> <w n="50.7">s</w>’<w n="50.8">entendent</w> <w n="50.9">pas</w> :</l>
							<l n="51" num="4.29"><w n="51.1">Plus</w> <w n="51.2">faibles</w> <w n="51.3">que</w> <w n="51.4">l</w>’<w n="51.5">enfant</w> <w n="51.6">dont</w> <w n="51.7">vous</w> <w n="51.8">guidez</w> <w n="51.9">les</w> <w n="51.10">pas</w>,</l>
							<l n="52" num="4.30"><space quantity="6" unit="char"></space><w n="52.1">On</w> <w n="52.2">ne</w> <w n="52.3">leur</w> <w n="52.4">apprend</w> <w n="52.5">qu</w>’<w n="52.6">à</w> <w n="52.7">vous</w> <w n="52.8">craindre</w>.</l>
							<l n="53" num="4.31"><w n="53.1">Et</w> <w n="53.2">nous</w> <w n="53.3">avons</w> <w n="53.4">tremblé</w> <w n="53.5">de</w> <w n="53.6">demeurer</w> <w n="53.7">longtemps</w>,</l>
							<l n="54" num="4.32"><w n="54.1">De</w> <w n="54.2">nous</w> <w n="54.3">perdre</w> <w n="54.4">sans</w> <w n="54.5">vous</w> <w n="54.6">dans</w> <w n="54.7">leurs</w> <w n="54.8">sombres</w> <w n="54.9">vallées</w> ;</l>
							<l n="55" num="4.33"><w n="55.1">Et</w> <w n="55.2">nous</w> <w n="55.3">avons</w> <w n="55.4">quitté</w> <w n="55.5">nos</w> <w n="55.6">mères</w> <w n="55.7">désolées</w> :</l>
							<l n="56" num="4.34"><w n="56.1">Dieu</w> ! <w n="56.2">versez</w> <w n="56.3">quelque</w> <w n="56.4">espoir</w> <w n="56.5">dans</w> <w n="56.6">leurs</w> <w n="56.7">cœurs</w> <w n="56.8">palpitants</w>,</l>
							<l n="57" num="4.35"><w n="57.1">Elles</w> <w n="57.2">pleurent</w> <w n="57.3">encore</w> ! » <w n="57.4">Il</w> <w n="57.5">est</w> <w n="57.6">trop</w> <w n="57.7">véritable</w> :</l>
							<l n="58" num="4.36"><w n="58.1">De</w> <w n="58.2">vos</w> <w n="58.3">berceaux</w> <w n="58.4">déserts</w> <w n="58.5">le</w> <w n="58.6">vide</w> <w n="58.7">épouvantable</w></l>
							<l n="59" num="4.37"><w n="59.1">Les</w> <w n="59.2">fait</w> <w n="59.3">longtemps</w> <w n="59.4">mourir</w>, <w n="59.5">et</w> <w n="59.6">crier</w> <w n="59.7">à</w> <w n="59.8">genoux</w> :</l>
							<l n="60" num="4.38">« <w n="60.1">Nous</w> <w n="60.2">voulons</w> <w n="60.3">nos</w> <w n="60.4">enfants</w> ! <w n="60.5">Nos</w> <w n="60.6">enfants</w> <w n="60.7">sont</w> <w n="60.8">à</w> <w n="60.9">nous</w> ! »</l>
						</lg>
						<lg n="5">
							<l n="61" num="5.1"><w n="61.1">Mais</w> <w n="61.2">Dieu</w> <w n="61.3">pose</w> <w n="61.4">sa</w> <w n="61.5">main</w> <w n="61.6">sur</w> <w n="61.7">leurs</w> <w n="61.8">yeux</w> <w n="61.9">pleins</w> <w n="61.10">de</w> <w n="61.11">larmes</w> ;</l>
							<l n="62" num="5.2"><w n="62.1">Il</w> <w n="62.2">éclaire</w>, <w n="62.3">il</w> <w n="62.4">console</w>, <w n="62.5">il</w> <w n="62.6">montre</w> <w n="62.7">l</w>’<w n="62.8">avenir</w> ;</l>
							<l n="63" num="5.3"><w n="63.1">L</w>’<w n="63.2">avenir</w> <w n="63.3">dévoilé</w> <w n="63.4">resplendit</w> <w n="63.5">de</w> <w n="63.6">vos</w> <w n="63.7">charmes</w>,</l>
							<l n="64" num="5.4"><w n="64.1">Et</w> <w n="64.2">l</w>’<w n="64.3">espoir</w>, <w n="64.4">goutte</w> <w n="64.5">à</w> <w n="64.6">goutte</w>, <w n="64.7">endort</w> <w n="64.8">le</w> <w n="64.9">souvenir</w>.</l>
							<l n="65" num="5.5"><space quantity="6" unit="char"></space><w n="65.1">La</w> <w n="65.2">promesse</w> <w n="65.3">qui</w> <w n="65.4">les</w> <w n="65.5">enchante</w></l>
							<l n="66" num="5.6"><space quantity="6" unit="char"></space><w n="66.1">Les</w> <w n="66.2">suit</w> <w n="66.3">jusque</w> <w n="66.4">dans</w> <w n="66.5">leur</w> <w n="66.6">sommeil</w> ;</l>
							<l n="67" num="5.7"><space quantity="6" unit="char"></space><w n="67.1">Et</w> <w n="67.2">cette</w> <w n="67.3">parole</w> <w n="67.4">touchante</w></l>
							<l n="68" num="5.8"><space quantity="6" unit="char"></space><w n="68.1">Les</w> <w n="68.2">soutient</w> <w n="68.3">encore</w> <w n="68.4">au</w> <w n="68.5">réveil</w> :</l>
							<l n="69" num="5.9">« <w n="69.1">Laissez</w> <w n="69.2">venir</w> <w n="69.3">à</w> <w n="69.4">moi</w> <w n="69.5">ces</w> <w n="69.6">jeunes</w> <w n="69.7">créatures</w>,</l>
							<l n="70" num="5.10"><w n="70.1">Et</w> <w n="70.2">je</w> <w n="70.3">vous</w> <w n="70.4">les</w> <w n="70.5">rendrai</w> ; <w n="70.6">mères</w>, <w n="70.7">ne</w> <w n="70.8">pleurez</w> <w n="70.9">pas</w> !</l>
							<l n="71" num="5.11"><w n="71.1">Priez</w> ! <w n="71.2">Dieu</w> <w n="71.3">vous</w> <w n="71.4">rendra</w> <w n="71.5">vos</w> <w n="71.6">amours</w> <w n="71.7">les</w> <w n="71.8">plus</w> <w n="71.9">pures</w>,</l>
							<l n="72" num="5.12"><w n="72.1">Et</w> <w n="72.2">vos</w> <w n="72.3">petits</w> <w n="72.4">enfants</w> <w n="72.5">souriront</w> <w n="72.6">dans</w> <w n="72.7">vos</w> <w n="72.8">bras</w>. »</l>
						</lg>
					</div></body></text></TEI>