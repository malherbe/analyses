<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>ROMANCES</head><div type="poem" key="DES203">
						<head type="main">LE PRISONNIER DE GUERRE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Tu</w> <w n="1.2">t</w>’<w n="1.3">en</w> <w n="1.4">vas</w> ? <w n="1.5">Reste</w> <w n="1.6">encore</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">Je</w> <w n="2.2">te</w> <w n="2.3">perds</w> <w n="2.4">pour</w> <w n="2.5">longtemps</w> !</l>
							<l n="3" num="1.3"><w n="3.1">Et</w> <w n="3.2">tu</w> <w n="3.3">vois</w> <w n="3.4">que</w> <w n="3.5">l</w>’<w n="3.6">aurore</w></l>
							<l n="4" num="1.4"><w n="4.1">Luit</w> <w n="4.2">depuis</w> <w n="4.3">peu</w> <w n="4.4">d</w>’<w n="4.5">instants</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Tantôt</w> <w n="5.2">sur</w> <w n="5.3">le</w> <w n="5.4">rivage</w></l>
							<l n="6" num="1.6"><w n="6.1">Je</w> <w n="6.2">marcherai</w> <w n="6.3">sans</w> <w n="6.4">toi</w> ;</l>
							<l n="7" num="1.7"><w n="7.1">J</w>’<w n="7.2">y</w> <w n="7.3">reste</w> <w n="7.4">en</w> <w n="7.5">esclavage</w>,</l>
							<l n="8" num="1.8"><space quantity="2" unit="char"></space><w n="8.1">Pauvre</w> <w n="8.2">de</w> <w n="8.3">moi</w> !</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Nous</w> <w n="9.2">avons</w> <w n="9.3">vu</w> <w n="9.4">la</w> <w n="9.5">vie</w></l>
							<l n="10" num="2.2"><w n="10.1">Sous</w> <w n="10.2">les</w> <w n="10.3">mêmes</w> <w n="10.4">couleurs</w> ;</l>
							<l n="11" num="2.3"><w n="11.1">Elle</w> <w n="11.2">a</w> <w n="11.3">pu</w> <w n="11.4">faire</w> <w n="11.5">envie</w>,</l>
							<l n="12" num="2.4"><w n="12.1">Car</w> <w n="12.2">elle</w> <w n="12.3">eut</w> <w n="12.4">bien</w> <w n="12.5">des</w> <w n="12.6">fleurs</w>.</l>
							<l n="13" num="2.5"><w n="13.1">La</w> <w n="13.2">guerre</w> <w n="13.3">était</w> <w n="13.4">la</w> <w n="13.5">gloire</w>,</l>
							<l n="14" num="2.6"><w n="14.1">J</w>’<w n="14.2">y</w> <w n="14.3">courus</w> <w n="14.4">avec</w> <w n="14.5">toi</w> ;</l>
							<l n="15" num="2.7"><w n="15.1">J</w>’<w n="15.2">ai</w> <w n="15.3">payé</w> <w n="15.4">la</w> <w n="15.5">victoire</w>,</l>
							<l n="16" num="2.8"><space quantity="2" unit="char"></space><w n="16.1">Pauvre</w> <w n="16.2">de</w> <w n="16.3">moi</w> !</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">Sur</w> <w n="17.2">combien</w> <w n="17.3">de</w> <w n="17.4">blessures</w></l>
							<l n="18" num="3.2"><w n="18.1">A</w>-<w n="18.2">t</w>-<w n="18.3">on</w> <w n="18.4">rivé</w> <w n="18.5">nos</w> <w n="18.6">fers</w> !</l>
							<l n="19" num="3.3"><w n="19.1">Ils</w> <w n="19.2">en</w> <w n="19.3">font</w> <w n="19.4">de</w> <w n="19.5">plus</w> <w n="19.6">sûres</w>,</l>
							<l n="20" num="3.4"><w n="20.1">Dans</w> <w n="20.2">leurs</w> <w n="20.3">prisons</w> <w n="20.4">d</w>’<w n="20.5">enfer</w>.</l>
							<l n="21" num="3.5"><w n="21.1">J</w>’<w n="21.2">ai</w> <w n="21.3">raillé</w> <w n="21.4">ma</w> <w n="21.5">souffrance</w>,</l>
							<l n="22" num="3.6"><w n="22.1">Enchaîné</w> <w n="22.2">près</w> <w n="22.3">de</w> <w n="22.4">toi</w> ;</l>
							<l n="23" num="3.7"><w n="23.1">Mais</w> <w n="23.2">tu</w> <w n="23.3">pars</w> <w n="23.4">pour</w> <w n="23.5">la</w> <w n="23.6">France</w>,</l>
							<l n="24" num="3.8"><space quantity="2" unit="char"></space><w n="24.1">Pauvre</w> <w n="24.2">de</w> <w n="24.3">moi</w> !</l>
						</lg>
						<lg n="4">
							<l n="25" num="4.1"><w n="25.1">Ma</w> <w n="25.2">plaie</w> <w n="25.3">envenimée</w></l>
							<l n="26" num="4.2"><w n="26.1">Arrête</w> <w n="26.2">ici</w> <w n="26.3">mes</w> <w n="26.4">pas</w> ;</l>
							<l n="27" num="4.3"><w n="27.1">Mortelle</w> <w n="27.2">et</w> <w n="27.3">renfermée</w>,</l>
							<l n="28" num="4.4"><w n="28.1">Elle</w> <w n="28.2">s</w>’<w n="28.3">aigrit</w> <w n="28.4">tout</w> <w n="28.5">bas</w>.</l>
							<l n="29" num="4.5"><w n="29.1">Sur</w> <w n="29.2">un</w> <w n="29.3">ponton</w> <w n="29.4">de</w> <w n="29.5">guerre</w></l>
							<l n="30" num="4.6"><w n="30.1">Faut</w>-<w n="30.2">il</w> <w n="30.3">languir</w> <w n="30.4">sans</w> <w n="30.5">toi</w> ?</l>
							<l n="31" num="4.7"><w n="31.1">Je</w> <w n="31.2">te</w> <w n="31.3">suivais</w> <w n="31.4">naguère</w>,</l>
							<l n="32" num="4.8"><space quantity="2" unit="char"></space><w n="32.1">Pauvre</w> <w n="32.2">de</w> <w n="32.3">moi</w> !</l>
						</lg>
						<lg n="5">
							<l n="33" num="5.1"><w n="33.1">Si</w> <w n="33.2">ma</w> <w n="33.3">blonde</w> <w n="33.4">Angeline</w>,</l>
							<l n="34" num="5.2"><w n="34.1">En</w> <w n="34.2">te</w> <w n="34.3">voyant</w> <w n="34.4">passer</w>,</l>
							<l n="35" num="5.3"><w n="35.1">Inquiète</w> <w n="35.2">s</w>’<w n="35.3">incline</w>,</l>
							<l n="36" num="5.4"><w n="36.1">Timide</w> <w n="36.2">à</w> <w n="36.3">t</w>’<w n="36.4">embrasser</w> ;</l>
							<l n="37" num="5.5"><w n="37.1">À</w> <w n="37.2">cet</w> <w n="37.3">ange</w> <w n="37.4">modeste</w>,</l>
							<l n="38" num="5.6"><w n="38.1">Qui</w> <w n="38.2">rn</w> <w n="38.3">attend</w> <w n="38.4">avec</w> <w n="38.5">toi</w>,</l>
							<l n="39" num="5.7"><w n="39.1">Ne</w> <w n="39.2">dis</w> <w n="39.3">pas</w> <w n="39.4">où</w> <w n="39.5">je</w> <w n="39.6">reste</w></l>
							<l n="40" num="5.8"><space quantity="2" unit="char"></space><w n="40.1">Pauvre</w> <w n="40.2">de</w> <w n="40.3">moi</w> !</l>
						</lg>
						<lg n="6">
							<l n="41" num="6.1"><w n="41.1">Au</w> <w n="41.2">foyer</w> <w n="41.3">de</w> <w n="41.4">ton</w> <w n="41.5">père</w></l>
							<l n="42" num="6.2"><w n="42.1">Si</w> <w n="42.2">le</w> <w n="42.3">mien</w> <w n="42.4">va</w> <w n="42.5">s</w>’<w n="42.6">asseoir</w>,</l>
							<l n="43" num="6.3"><w n="43.1">Mon</w> <w n="43.2">nom</w> <w n="43.3">sera</w>, <w n="43.4">j</w>’<w n="43.5">espère</w>,</l>
							<l n="44" num="6.4"><w n="44.1">Dans</w> <w n="44.2">vos</w> <w n="44.3">récits</w> <w n="44.4">du</w> <w n="44.5">soir</w> ;</l>
							<l n="45" num="6.5"><w n="45.1">Quand</w> <w n="45.2">ses</w> <w n="45.3">yeux</w> <w n="45.4">pleins</w> <w n="45.5">de</w> <w n="45.6">larmes</w></l>
							<l n="46" num="6.6"><w n="46.1">S</w>’<w n="46.2">attacheront</w> <w n="46.3">sur</w> <w n="46.4">toi</w>,</l>
							<l n="47" num="6.7"><w n="47.1">Fais</w>-<w n="47.2">lui</w> <w n="47.3">bénir</w> <w n="47.4">nos</w> <w n="47.5">armes</w>,</l>
							<l n="48" num="6.8"><space quantity="2" unit="char"></space><w n="48.1">Pauvre</w> <w n="48.2">de</w> <w n="48.3">moi</w> !</l>
						</lg>
					</div></body></text></TEI>