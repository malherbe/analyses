<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES97">
						<head type="main">À LA NUIT</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Douce</w> <w n="1.2">Nuit</w>, <w n="1.3">ton</w> <w n="1.4">charme</w> <w n="1.5">paisible</w></l>
							<l n="2" num="1.2"><w n="2.1">Du</w> <w n="2.2">malheureux</w> <w n="2.3">suspend</w> <w n="2.4">les</w> <w n="2.5">pleurs</w> ;</l>
							<l n="3" num="1.3"><space quantity="2" unit="char"></space><w n="3.1">Nul</w> <w n="3.2">mortel</w> <w n="3.3">n</w>’<w n="3.4">est</w> <w n="3.5">insensible</w></l>
							<l n="4" num="1.4"><w n="4.1">À</w> <w n="4.2">tes</w> <w n="4.3">bienfaisantes</w> <w n="4.4">erreurs</w>.</l>
							<l n="5" num="1.5"><space quantity="2" unit="char"></space><w n="5.1">Souvent</w> <w n="5.2">dans</w> <w n="5.3">un</w> <w n="5.4">cœur</w> <w n="5.5">rebelle</w></l>
							<l n="6" num="1.6"><space quantity="2" unit="char"></space><w n="6.1">Tu</w> <w n="6.2">fais</w> <w n="6.3">naître</w> <w n="6.4">les</w> <w n="6.5">désirs</w> ;</l>
							<l n="7" num="1.7"><space quantity="2" unit="char"></space><w n="7.1">Et</w> <w n="7.2">l</w>’<w n="7.3">amour</w> <w n="7.4">tendre</w> <w n="7.5">et</w> <w n="7.6">fidèle</w></l>
							<l n="8" num="1.8"><space quantity="2" unit="char"></space><w n="8.1">Te</w> <w n="8.2">doit</w> <w n="8.3">ses</w> <w n="8.4">plus</w> <w n="8.5">doux</w> <w n="8.6">plaisirs</w>.</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><w n="9.1">Tu</w> <w n="9.2">sais</w> <w n="9.3">par</w> <w n="9.4">un</w> <w n="9.5">riant</w> <w n="9.6">mensonge</w>,</l>
							<l n="10" num="2.2"><w n="10.1">Calmer</w> <w n="10.2">un</w> <w n="10.3">amant</w> <w n="10.4">agité</w>,</l>
							<l n="11" num="2.3"><space quantity="2" unit="char"></space><w n="11.1">Et</w> <w n="11.2">le</w> <w n="11.3">consoler</w>, <w n="11.4">en</w> <w n="11.5">songe</w>,</l>
							<l n="12" num="2.4"><w n="12.1">D</w>’<w n="12.2">une</w> <w n="12.3">triste</w> <w n="12.4">réalité</w>.</l>
							<l n="13" num="2.5"><space quantity="2" unit="char"></space><w n="13.1">Ô</w> <w n="13.2">Nuit</w> ! <w n="13.3">pour</w> <w n="13.4">la</w> <w n="13.5">douleur</w> <w n="13.6">sombre</w>,</l>
							<l n="14" num="2.6"><space quantity="2" unit="char"></space><w n="14.1">Et</w> <w n="14.2">pour</w> <w n="14.3">le</w> <w n="14.4">plaisir</w> <w n="14.5">d</w>’<w n="14.6">amour</w></l>
							<l n="15" num="2.7"><space quantity="2" unit="char"></space><w n="15.1">On</w> <w n="15.2">doit</w> <w n="15.3">préférer</w> <w n="15.4">ton</w> <w n="15.5">ombre</w></l>
							<l n="16" num="2.8"><space quantity="2" unit="char"></space><w n="16.1">À</w> <w n="16.2">l</w>’<w n="16.3">éclat</w> <w n="16.4">du</w> <w n="16.5">plus</w> <w n="16.6">beau</w> <w n="16.7">jour</w>.</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">Comme</w> <w n="17.2">dans</w> <w n="17.3">le</w> <w n="17.4">sein</w> <w n="17.5">d</w>’<w n="17.6">une</w> <w n="17.7">amie</w></l>
							<l n="18" num="3.2"><w n="18.1">On</w> <w n="18.2">aime</w> <w n="18.3">à</w> <w n="18.4">verser</w> <w n="18.5">sa</w> <w n="18.6">douleur</w>,</l>
							<l n="19" num="3.3"><space quantity="2" unit="char"></space><w n="19.1">C</w>’<w n="19.2">est</w> <w n="19.3">à</w> <w n="19.4">toi</w> <w n="19.5">que</w> <w n="19.6">je</w> <w n="19.7">confie</w></l>
							<l n="20" num="3.4"><w n="20.1">Les</w> <w n="20.2">premiers</w> <w n="20.3">soupirs</w> <w n="20.4">de</w> <w n="20.5">mon</w> <w n="20.6">cœur</w>.</l>
							<l n="21" num="3.5"><space quantity="2" unit="char"></space><w n="21.1">Cache</w>-<w n="21.2">moi</w>, <w n="21.3">s</w>’<w n="21.4">il</w> <w n="21.5">est</w> <w n="21.6">possible</w>,</l>
							<l n="22" num="3.6"><space quantity="2" unit="char"></space><w n="22.1">L</w>’<w n="22.2">objet</w> <w n="22.3">de</w> <w n="22.4">mon</w> <w n="22.5">tendre</w> <w n="22.6">effroi</w>.</l>
							<l n="23" num="3.7"><space quantity="2" unit="char"></space><w n="23.1">Comme</w> <w n="23.2">moi</w> <w n="23.3">s</w>’<w n="23.4">il</w> <w n="23.5">est</w> <w n="23.6">sensible</w>,</l>
							<l n="24" num="3.8"><space quantity="2" unit="char"></space><w n="24.1">Qu</w>’<w n="24.2">il</w> <w n="24.3">soit</w> <w n="24.4">discret</w> <w n="24.5">comme</w> <w n="24.6">toi</w> !</l>
						</lg>
					</div></body></text></TEI>