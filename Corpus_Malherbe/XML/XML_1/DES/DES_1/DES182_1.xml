<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part"> POÉSIES INÉDITES <lb></lb>MÉLANGES</head><div type="poem" key="DES182">
						<head type="main">LE PETIT PEUREUX</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Quoi</w>, <w n="1.2">Daniel</w> ! <w n="1.3">à</w> <w n="1.4">six</w> <w n="1.5">ans</w>, <w n="1.6">vous</w> <w n="1.7">faites</w> <w n="1.8">le</w> <w n="1.9">faux</w> <w n="1.10">brave</w> ;</l>
							<l n="2" num="1.2"><space quantity="6" unit="char"></space><w n="2.1">Vous</w> <w n="2.2">insultez</w> <w n="2.3">un</w> <w n="2.4">chien</w> <w n="2.5">qui</w> <w n="2.6">dort</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Vous</w> <w n="3.2">lui</w> <w n="3.3">tirez</w> <w n="3.4">l</w>’<w n="3.5">oreille</w> ! <w n="3.6">et</w>, <w n="3.7">raillant</w> <w n="3.8">votre</w> <w n="3.9">esclave</w>,</l>
							<l n="4" num="1.4"><w n="4.1">Sur</w> <w n="4.2">ses</w> <w n="4.3">pas</w> <w n="4.4">endormis</w> <w n="4.5">vous</w> <w n="4.6">dressez</w> <w n="4.7">une</w> <w n="4.8">entrave</w> !</l>
							<l n="5" num="1.5"><w n="5.1">L</w>’<w n="5.2">esclave</w> <w n="5.3">qui</w> <w n="5.4">sommeille</w>, <w n="5.5">ô</w> <w n="5.6">Daniel</w>, <w n="5.7">n</w>’<w n="5.8">est</w> <w n="5.9">pas</w> <w n="5.10">mort</w> ;</l>
							<l n="6" num="1.6"><w n="6.1">Son</w> <w n="6.2">réveil</w> <w n="6.3">s</w>’<w n="6.4">armera</w> <w n="6.5">d</w>’<w n="6.6">une</w> <w n="6.7">dent</w> <w n="6.8">meurtrière</w> :</l>
							<l n="7" num="1.7"><w n="7.1">La</w> <w n="7.2">preuve</w> <w n="7.3">en</w> <w n="7.4">a</w> <w n="7.5">rougi</w> <w n="7.6">votre</w> <w n="7.7">linge</w> <w n="7.8">en</w> <w n="7.9">lambeaux</w>.</l>
							<l n="8" num="1.8"><w n="8.1">Oui</w>, <w n="8.2">vous</w> <w n="8.3">voilà</w> <w n="8.4">blessé</w>, <w n="8.5">mais</w> <w n="8.6">blessé</w> <w n="8.7">par</w> <w n="8.8">derrière</w> !</l>
							<l n="9" num="1.9"><w n="9.1">Malgré</w> <w n="9.2">la</w> <w n="9.3">nuit</w>, <w n="9.4">j</w>’<w n="9.5">y</w> <w n="9.6">vois</w>. <w n="9.7">Sauvons</w>-<w n="9.8">nous</w> <w n="9.9">des</w> <w n="9.10">flambeaux</w>,</l>
							<l n="10" num="1.10"><w n="10.1">Sauvons</w>-<w n="10.2">nous</w> <w n="10.3">des</w> <w n="10.4">témoins</w> !… <w n="10.5">Moi</w>, <w n="10.6">je</w> <w n="10.7">suis</w> <w n="10.8">votre</w> <w n="10.9">mère</w>…</l>
							<l n="11" num="1.11"><w n="11.1">Je</w> <w n="11.2">cacherai</w> <w n="11.3">ta</w> <w n="11.4">honte</w>, <w n="11.5">enfant</w>, <w n="11.6">dans</w> <w n="11.7">mon</w> <w n="11.8">amour</w> !</l>
							<l n="12" num="1.12"><w n="12.1">Viens</w> ! <w n="12.2">j</w>’<w n="12.3">ai</w> <w n="12.4">pitié</w> <w n="12.5">de</w> <w n="12.6">toi</w>, <w n="12.7">car</w> <w n="12.8">la</w> <w n="12.9">honte</w> <w n="12.10">est</w> <w n="12.11">amère</w>.</l>
							<l n="13" num="1.13"><w n="13.1">Bénis</w> <w n="13.2">Dieu</w> : <w n="13.3">sa</w> <w n="13.4">bonté</w> <w n="13.5">vient</w> <w n="13.6">d</w>’<w n="13.7">éteindre</w> <w n="13.8">le</w> <w n="13.9">jour</w>.</l>
						</lg>
						<lg n="2">
							<l n="14" num="2.1"><w n="14.1">Personne</w> <w n="14.2">ne</w> <w n="14.3">t</w>’<w n="14.4">a</w> <w n="14.5">vu</w> <w n="14.6">lâche</w> <w n="14.7">et</w> <w n="14.8">méchant</w>… <w n="14.9">Écoute</w> :</l>
							<l n="15" num="2.2"><w n="15.1">Pour</w> <w n="15.2">t</w>’<w n="15.3">appeler</w> <w n="15.4">méchant</w>, <w n="15.5">sais</w>-<w n="15.6">tu</w> <w n="15.7">ce</w> <w n="15.8">qu</w>’<w n="15.9">il</w> <w n="15.10">m</w>’<w n="15.11">en</w> <w n="15.12">coûte</w> ?</l>
							<l n="16" num="2.3"><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">ton</w> <w n="16.4">nom</w> <w n="16.5">pour</w> <w n="16.6">ce</w> <w n="16.7">soir</w> ; <w n="16.8">subis</w>-<w n="16.9">le</w> <w n="16.10">devant</w> <w n="16.11">moi</w> :</l>
							<l n="17" num="2.4"><w n="17.1">Va</w> ! <w n="17.2">personne</w> <w n="17.3">jamais</w> <w n="17.4">ne</w> <w n="17.5">l</w>’<w n="17.6">entendra</w> <w n="17.7">que</w> <w n="17.8">toi</w>.</l>
							<l n="18" num="2.5"><w n="18.1">Personne</w> <w n="18.2">ne</w> <w n="18.3">t</w>’<w n="18.4">a</w> <w n="18.5">vu</w> <w n="18.6">d</w>’<w n="18.7">une</w> <w n="18.8">bête</w> <w n="18.9">innocente</w></l>
							<l n="19" num="2.6"><space quantity="6" unit="char"></space><w n="19.1">Tourmenter</w> <w n="19.2">l</w>’<w n="19.3">indolent</w> <w n="19.4">sommeil</w>,</l>
							<l n="20" num="2.7"><space quantity="6" unit="char"></space><w n="20.1">Et</w>, <w n="20.2">pour</w> <w n="20.3">irriter</w> <w n="20.4">son</w> <w n="20.5">réveil</w>,</l>
							<l n="21" num="2.8"><space quantity="6" unit="char"></space><w n="21.1">Lui</w> <w n="21.2">simuler</w> <w n="21.3">sa</w> <w n="21.4">chaîne</w> <w n="21.5">absente</w>.</l>
							<l n="22" num="2.9"><w n="22.1">Cher</w> <w n="22.2">petit</w> <w n="22.3">fanfaron</w>, <w n="22.4">c</w>’<w n="22.5">est</w> <w n="22.6">lui</w> <w n="22.7">qui</w> <w n="22.8">t</w>’<w n="22.9">a</w> <w n="22.10">fait</w> <w n="22.11">peur</w>.</l>
							<l n="23" num="2.10"><w n="23.1">Sa</w> <w n="23.2">gueule</w> <w n="23.3">était</w> <w n="23.4">immense</w>, <w n="23.5">ouverte</w> <w n="23.6">à</w> <w n="23.7">la</w> <w n="23.8">vengeance</w>.</l>
							<l n="24" num="2.11"><w n="24.1">Il</w> <w n="24.2">te</w> <w n="24.3">mangeait</w>, <w n="24.4">Daniel</w>, <w n="24.5">sans</w> <w n="24.6">ma</w> <w n="24.7">tendre</w> <w n="24.8">indulgence</w>,</l>
							<l n="25" num="2.12"><w n="25.1">Et</w> <w n="25.2">tu</w> <w n="25.3">fuyais</w> <w n="25.4">en</w> <w n="25.5">vain</w>, <w n="25.6">lié</w> <w n="25.7">par</w> <w n="25.8">la</w> <w n="25.9">stupeur</w>.</l>
							<l n="26" num="2.13"><w n="26.1">Il</w> <w n="26.2">m</w>’<w n="26.3">a</w> <w n="26.4">cédé</w> <w n="26.5">sa</w> <w n="26.6">proie</w>, <w n="26.7">il</w> <w n="26.8">a</w> <w n="26.9">compris</w> <w n="26.10">mes</w> <w n="26.11">larmes</w> ;</l>
							<l n="27" num="2.14"><w n="27.1">Et</w> <w n="27.2">peut</w>-<w n="27.3">être</w> <w n="27.4">un</w> <w n="27.5">gâteau</w> <w n="27.6">que</w> <w n="27.7">préparait</w> <w n="27.8">ma</w> <w n="27.9">main</w></l>
							<l n="28" num="2.15"><space quantity="6" unit="char"></space><w n="28.1">Pour</w> <w n="28.2">charmer</w> <w n="28.3">ton</w> <w n="28.4">loisir</w> <w n="28.5">demain</w>,</l>
							<l n="29" num="2.16"><w n="29.1">L</w>’<w n="29.2">a</w> <w n="29.3">rendu</w> <w n="29.4">tout</w> <w n="29.5">à</w> <w n="29.6">fait</w> <w n="29.7">clément</w> <w n="29.8">à</w> <w n="29.9">mes</w> <w n="29.10">alarmes</w>.</l>
							<l n="30" num="2.17"><w n="30.1">Je</w> <w n="30.2">l</w>’<w n="30.3">avais</w> <w n="30.4">fait</w> <w n="30.5">si</w> <w n="30.6">beau</w>, <w n="30.7">si</w> <w n="30.8">grand</w> ! <w n="30.9">Ne</w> <w n="30.10">pleure</w> <w n="30.11">plus</w> ;</l>
							<l n="31" num="2.18"><w n="31.1">De</w> <w n="31.2">tes</w> <w n="31.3">habits</w> <w n="31.4">l</w>’<w n="31.5">eau</w> <w n="31.6">pure</w> <w n="31.7">effacera</w> <w n="31.8">la</w> <w n="31.9">tache</w> ;</l>
							<l n="32" num="2.19"><w n="32.1">Ton</w> <w n="32.2">âge</w> <w n="32.3">n</w>’<w n="32.4">en</w> <w n="32.5">a</w> <w n="32.6">pas</w> <w n="32.7">où</w> <w n="32.8">le</w> <w n="32.9">remords</w> <w n="32.10">s</w>’<w n="32.11">attache</w> !</l>
							<l n="33" num="2.20"><w n="33.1">Tout</w> <w n="33.2">ce</w> <w n="33.3">qui</w> <w n="33.4">doit</w> <w n="33.5">survivre</w> <w n="33.6">à</w> <w n="33.7">tes</w> <w n="33.8">cris</w> <w n="33.9">superflus</w>,</l>
							<l n="34" num="2.21"><w n="34.1">Ce</w> <w n="34.2">qu</w>’<w n="34.3">il</w> <w n="34.4">faut</w> <w n="34.5">regretter</w> <w n="34.6">par</w> <w n="34.7">delà</w> <w n="34.8">ton</w> <w n="34.9">enfance</w>,</l>
							<l n="35" num="2.22"><w n="35.1">C</w>’<w n="35.2">est</w> <w n="35.3">mon</w> <w n="35.4">sang</w>…, <w n="35.5">oui</w>, <w n="35.6">le</w> <w n="35.7">mien</w> ! <w n="35.8">lâchement</w> <w n="35.9">répandu</w>.</l>
							<l n="36" num="2.23"><w n="36.1">Quoi</w> ! <w n="36.2">sous</w> <w n="36.3">la</w> <w n="36.4">dent</w> <w n="36.5">d</w>’<w n="36.6">un</w> <w n="36.7">chien</w> <w n="36.8">tu</w> <w n="36.9">l</w>’<w n="36.10">as</w> <w n="36.11">déjà</w> <w n="36.12">perdu</w>,</l>
							<l n="37" num="2.24"><w n="37.1">Daniel</w>, <w n="37.2">et</w> <w n="37.3">ton</w> <w n="37.4">pays</w> <w n="37.5">l</w>’<w n="37.6">attend</w> <w n="37.7">pour</w> <w n="37.8">sa</w> <w n="37.9">défense</w> !</l>
						</lg>
					</div></body></text></TEI>