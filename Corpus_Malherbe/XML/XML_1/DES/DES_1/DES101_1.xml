<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES101">
						<head type="main">LE BAL</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">L</w>’<w n="1.2">heure</w> <w n="1.3">du</w> <w n="1.4">bal</w>, <w n="1.5">enfin</w>, <w n="1.6">se</w> <w n="1.7">fait</w> <w n="1.8">entendre</w>,</l>
							<l n="2" num="1.2"><w n="2.1">Le</w> <w n="2.2">plaisir</w> <w n="2.3">sonne</w>, <w n="2.4">et</w> <w n="2.5">tu</w> <w n="2.6">le</w> <w n="2.7">fais</w> <w n="2.8">attendre</w> !</l>
							<l n="3" num="1.3"><w n="3.1">Depuis</w> <w n="3.2">huit</w> <w n="3.3">jours</w>, <w n="3.4">il</w> <w n="3.5">a</w> <w n="3.6">pris</w> <w n="3.7">pour</w> <w n="3.8">signal</w></l>
							<l n="4" num="1.4"><space quantity="10" unit="char"></space><w n="4.1">L</w>’<w n="4.2">heure</w> <w n="4.3">du</w> <w n="4.4">bal</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><w n="5.1">Où</w> <w n="5.2">sont</w> <w n="5.3">les</w> <w n="5.4">fleurs</w> <w n="5.5">dont</w> <w n="5.6">l</w>’<w n="5.7">éclat</w> <w n="5.8">étincelle</w> ?</l>
							<l n="6" num="2.2"><w n="6.1">Elles</w> <w n="6.2">mourront</w> <w n="6.3">en</w> <w n="6.4">te</w> <w n="6.5">voyant</w> <w n="6.6">si</w> <w n="6.7">belle</w>.</l>
							<l n="7" num="2.3"><w n="7.1">Mais</w>, <w n="7.2">sous</w> <w n="7.3">ta</w> <w n="7.4">main</w>, <w n="7.5">je</w> <w n="7.6">vois</w> <w n="7.7">rouler</w> <w n="7.8">des</w> <w n="7.9">pleurs</w>…</l>
							<l n="8" num="2.4"><space quantity="10" unit="char"></space><w n="8.1">Où</w> <w n="8.2">sont</w> <w n="8.3">les</w> <w n="8.4">fleurs</w> ?</l>
						</lg>
						<lg n="3">
							<l n="9" num="3.1"><w n="9.1">Il</w> <w n="9.2">est</w> <w n="9.3">absent</w> ! <w n="9.4">l</w>’<w n="9.5">espérance</w> <w n="9.6">est</w> <w n="9.7">voilée</w>,</l>
							<l n="10" num="3.2"><w n="10.1">Ou</w>, <w n="10.2">pour</w> <w n="10.3">le</w> <w n="10.4">suivre</w>, <w n="10.5">elle</w> <w n="10.6">s</w>’<w n="10.7">est</w> <w n="10.8">envolée</w>.</l>
							<l n="11" num="3.3"><w n="11.1">Je</w> <w n="11.2">le</w> <w n="11.3">devine</w> <w n="11.4">à</w> <w n="11.5">ton</w> <w n="11.6">plaintif</w> <w n="11.7">accent</w> :</l>
							<l n="12" num="3.4"><space quantity="10" unit="char"></space><w n="12.1">Il</w> <w n="12.2">est</w> <w n="12.3">absent</w> !</l>
						</lg>
						<lg n="4">
							<l n="13" num="4.1"><w n="13.1">Je</w> <w n="13.2">n</w>’<w n="13.3">irai</w> <w n="13.4">pas</w> ! <w n="13.5">la</w> <w n="13.6">danse</w>, <w n="13.7">mon</w> <w n="13.8">amie</w>,</l>
							<l n="14" num="4.2"><w n="14.1">Est</w>, <w n="14.2">sans</w> <w n="14.3">l</w>’<w n="14.4">Amour</w>, <w n="14.5">une</w> <w n="14.6">grâce</w> <w n="14.7">endormie</w>.</l>
							<l n="15" num="4.3"><w n="15.1">Loin</w> <w n="15.2">de</w> <w n="15.3">la</w> <w n="15.4">fête</w> <w n="15.5">il</w> <w n="15.6">enchaîne</w> <w n="15.7">tes</w> <w n="15.8">pas</w> :</l>
							<l n="16" num="4.4"><space quantity="10" unit="char"></space><w n="16.1">Je</w> <w n="16.2">n</w>’<w n="16.3">irai</w> <w n="16.4">pas</w> !</l>
						</lg>
					</div></body></text></TEI>