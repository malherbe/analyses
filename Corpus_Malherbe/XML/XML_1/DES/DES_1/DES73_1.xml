<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ÉLÉGIES</head><div type="poem" key="DES73">
						<head type="main">LA SUITE <lb></lb>DU VIEUX CRIEUR DU RHÔNE</head>
						<head type="sub_1">À M. JARS</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="2" unit="char"></space><w n="1.1">Le</w> <w n="1.2">vieux</w> <w n="1.3">crieur</w> <w n="1.4">allait</w> <w n="1.5">contant</w> <w n="1.6">l</w>’<w n="1.7">histoire</w></l>
							<l n="2" num="1.2"><space quantity="2" unit="char"></space><w n="2.1">Du</w> <w n="2.2">faible</w> <w n="2.3">enfant</w> <w n="2.4">vers</w> <w n="2.5">le</w> <w n="2.6">Rhône</w> <w n="2.7">égaré</w> ;</l>
							<l n="3" num="1.3"><space quantity="2" unit="char"></space><w n="3.1">Un</w> <w n="3.2">vieux</w> <w n="3.3">soldat</w>, <w n="3.4">tout</w> <w n="3.5">cuirassé</w> <w n="3.6">de</w> <w n="3.7">gloire</w>,</l>
							<l n="4" num="1.4"><space quantity="2" unit="char"></space><w n="4.1">En</w> <w n="4.2">l</w>’<w n="4.3">écoutant</w> <w n="4.4">sous</w> <w n="4.5">son</w> <w n="4.6">casque</w> <w n="4.7">a</w> <w n="4.8">pleuré</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1"><space quantity="2" unit="char"></space><w n="5.1">Ce</w> <w n="5.2">n</w>’<w n="5.3">était</w> <w n="5.4">plus</w> <w n="5.5">quand</w> <w n="5.6">l</w>’<w n="5.7">été</w> <w n="5.8">se</w> <w n="5.9">couronne</w></l>
							<l n="6" num="2.2"><space quantity="2" unit="char"></space><w n="6.1">De</w> <w n="6.2">rayons</w> <w n="6.3">d</w>’<w n="6.4">or</w>, <w n="6.5">de</w> <w n="6.6">pampres</w> <w n="6.7">et</w> <w n="6.8">de</w> <w n="6.9">fleurs</w> ;</l>
							<l n="7" num="2.3"><space quantity="2" unit="char"></space><w n="7.1">C</w>’<w n="7.2">était</w> <w n="7.3">au</w> <w n="7.4">temps</w> <w n="7.5">où</w> <w n="7.6">l</w>’<w n="7.7">hiver</w> <w n="7.8">s</w>’<w n="7.9">environne</w></l>
							<l n="8" num="2.4"><space quantity="2" unit="char"></space><w n="8.1">De</w> <w n="8.2">longues</w> <w n="8.3">nuits</w> <w n="8.4">et</w> <w n="8.5">de</w> <w n="8.6">mornes</w> <w n="8.7">couleurs</w>.</l>
							<l n="9" num="2.5"><space quantity="2" unit="char"></space><w n="9.1">Ce</w> <w n="9.2">n</w>’<w n="9.3">était</w> <w n="9.4">plus</w> <w n="9.5">quand</w> <w n="9.6">ma</w> <w n="9.7">voix</w> <w n="9.8">lamentable</w></l>
							<l n="10" num="2.6"><space quantity="2" unit="char"></space><w n="10.1">Cria</w> <w n="10.2">partout</w> <w n="10.3">l</w>’<w n="10.4">enfant</w> <w n="10.5">sans</w> <w n="10.6">l</w>’<w n="10.7">obtenir</w> ;</l>
							<l n="11" num="2.7"><w n="11.1">Mais</w> <w n="11.2">aux</w> <w n="11.3">mères</w> <w n="11.4">toujours</w> <w n="11.5">ce</w> <w n="11.6">triste</w> <w n="11.7">souvenir</w></l>
							<l n="12" num="2.8"><space quantity="2" unit="char"></space><w n="12.1">Apparaissait</w> <w n="12.2">lugubre</w> <w n="12.3">et</w> <w n="12.4">redoutable</w>.</l>
						</lg>
						<lg n="3">
							<l n="13" num="3.1"><w n="13.1">Celle</w> <w n="13.2">que</w> <w n="13.3">l</w>’<w n="13.4">on</w> <w n="13.5">crut</w> <w n="13.6">morte</w> <w n="13.7">en</w> <w n="13.8">ses</w> <w n="13.9">cris</w> <w n="13.10">superflus</w>,</l>
							<l n="14" num="3.2"><w n="14.1">Qu</w>’<w n="14.2">on</w> <w n="14.3">emporta</w> <w n="14.4">le</w> <w n="14.5">soir</w>, <w n="14.6">de</w> <w n="14.7">larmes</w> <w n="14.8">épuisée</w>…</l>
							<l n="15" num="3.3"><w n="15.1">Elle</w> <w n="15.2">vit</w> ; <w n="15.3">mais</w>, <w n="15.4">semblable</w> <w n="15.5">à</w> <w n="15.6">sa</w> <w n="15.7">plainte</w> <w n="15.8">brisée</w>,</l>
							<l n="16" num="3.4"><w n="16.1">Sa</w> <w n="16.2">mémoire</w> <w n="16.3">au</w> <w n="16.4">malheur</w> <w n="16.5">ne</w> <w n="16.6">se</w> <w n="16.7">réveille</w> <w n="16.8">plus</w>.</l>
							<l n="17" num="3.5"><w n="17.1">La</w> <w n="17.2">moisson</w>, <w n="17.3">le</w> <w n="17.4">rivage</w> <w n="17.5">et</w> <w n="17.6">le</w> <w n="17.7">Rhône</w> <w n="17.8">rapide</w></l>
							<l n="18" num="3.6"><w n="18.1">Dans</w> <w n="18.2">ses</w> <w n="18.3">esprits</w> <w n="18.4">confus</w> <w n="18.5">ne</w> <w n="18.6">viennent</w> <w n="18.7">plus</w> <w n="18.8">s</w>’<w n="18.9">offrir</w>.</l>
							<l n="19" num="3.7"><space quantity="6" unit="char"></space><w n="19.1">Ainsi</w> <w n="19.2">se</w> <w n="19.3">trouble</w> <w n="19.4">une</w> <w n="19.5">eau</w> <w n="19.6">limpide</w>,</l>
							<l n="20" num="3.8"><space quantity="6" unit="char"></space><w n="20.1">Dont</w> <w n="20.2">la</w> <w n="20.3">source</w> <w n="20.4">va</w> <w n="20.5">se</w> <w n="20.6">tarir</w>.</l>
						</lg>
						<lg n="4">
							<l n="21" num="4.1"><w n="21.1">Ses</w> <w n="21.2">yeux</w> <w n="21.3">sans</w> <w n="21.4">s</w>’<w n="21.5">étonner</w> <w n="21.6">ont</w> <w n="21.7">revu</w> <w n="21.8">sa</w> <w n="21.9">demeure</w>,</l>
							<l n="22" num="4.2"><space quantity="6" unit="char"></space><w n="22.1">Où</w> <w n="22.2">la</w> <w n="22.3">foule</w> <w n="22.4">a</w> <w n="22.5">suivi</w> <w n="22.6">ses</w> <w n="22.7">pas</w> ;</l>
							<l n="23" num="4.3"><space quantity="6" unit="char"></space><w n="23.1">On</w> <w n="23.2">l</w>’<w n="23.3">entoure</w>, <w n="23.4">on</w> <w n="23.5">frémit</w>, <w n="23.6">on</w> <w n="23.7">pleure</w> :</l>
							<l n="24" num="4.4"><space quantity="6" unit="char"></space><w n="24.1">Elle</w> <w n="24.2">seule</w> <w n="24.3">ne</w> <w n="24.4">pleure</w> <w n="24.5">pas</w>.</l>
							<l n="25" num="4.5"><space quantity="6" unit="char"></space><w n="25.1">Dieu</w> <w n="25.2">la</w> <w n="25.3">bénit</w> <w n="25.4">d</w>’<w n="25.5">un</w> <w n="25.6">long</w> <w n="25.7">délire</w> :</l>
							<l n="26" num="4.6"><space quantity="6" unit="char"></space><w n="26.1">Son</w> <w n="26.2">fils</w> <w n="26.3">est</w> <w n="26.4">là</w>, <w n="26.5">dit</w>-<w n="26.6">elle</w>… <w n="26.7">il</w> <w n="26.8">dort</w>.</l>
							<l n="27" num="4.7"><space quantity="6" unit="char"></space><w n="27.1">Elle</w> <w n="27.2">a</w> <w n="27.3">rapporté</w> <w n="27.4">son</w> <w n="27.5">sourire</w></l>
							<l n="28" num="4.8"><space quantity="6" unit="char"></space><w n="28.1">À</w> <w n="28.2">son</w> <w n="28.3">fils</w>… <w n="28.4">que</w> <w n="28.5">l</w>’<w n="28.6">on</w> <w n="28.7">cherche</w> <w n="28.8">encor</w> !</l>
						</lg>
						<lg n="5">
							<l n="29" num="5.1"><w n="29.1">Balançant</w> <w n="29.2">un</w> <w n="29.3">berceau</w>, <w n="29.4">dans</w> <w n="29.5">ces</w> <w n="29.6">nuits</w> <w n="29.7">rigoureuses</w>,</l>
							<l n="30" num="5.2"><w n="30.1">Seule</w> <w n="30.2">elle</w> <w n="30.3">dit</w> <w n="30.4">encor</w> : « <w n="30.5">Les</w> <w n="30.6">mères</w> <w n="30.7">sont</w> <w n="30.8">heureuses</w> ! »</l>
							<l n="31" num="5.3"><w n="31.1">Seule</w> <w n="31.2">elle</w> <w n="31.3">ne</w> <w n="31.4">sait</w> <w n="31.5">plus</w> <w n="31.6">son</w> <w n="31.7">malheur</w> <w n="31.8">si</w> <w n="31.9">récent</w> ;</l>
							<l n="32" num="5.4"><w n="32.1">Calme</w>, <w n="32.2">elle</w> <w n="32.3">n</w>’<w n="32.4">offre</w> <w n="32.5">à</w> <w n="32.6">Dieu</w> <w n="32.7">qu</w>’<w n="32.8">un</w> <w n="32.9">cœur</w> <w n="32.10">reconnaissant</w>.</l>
							<l n="33" num="5.5"><w n="33.1">À</w> <w n="33.2">travers</w> <w n="33.3">le</w> <w n="33.4">rideau</w> <w n="33.5">que</w> <w n="33.6">sa</w> <w n="33.7">main</w> <w n="33.8">vient</w> <w n="33.9">d</w>’<w n="33.10">étendre</w>,</l>
							<l n="34" num="5.6"><w n="34.1">Elle</w> <w n="34.2">entend</w> <w n="34.3">respirer</w> <w n="34.4">l</w>’<w n="34.5">enfant</w> <w n="34.6">dans</w> <w n="34.7">son</w> <w n="34.8">sommeil</w>.</l>
							<l n="35" num="5.7"><w n="35.1">Qui</w> <w n="35.2">voudrait</w> <w n="35.3">l</w>’<w n="35.4">arracher</w> <w n="35.5">à</w> <w n="35.6">cette</w> <w n="35.7">erreur</w> <w n="35.8">si</w> <w n="35.9">tendre</w> ?</l>
							<l n="36" num="5.8"><w n="36.1">Elle</w> <w n="36.2">écoute</w> <w n="36.3">son</w> <w n="36.4">souffle</w> ; <w n="36.5">elle</w> <w n="36.6">attend</w> <w n="36.7">son</w> <w n="36.8">réveil</w>.</l>
							<l n="37" num="5.9"><w n="37.1">Ah</w> ! <w n="37.2">ne</w> <w n="37.3">soulevez</w> <w n="37.4">pas</w> <w n="37.5">ce</w> <w n="37.6">rideau</w> <w n="37.7">qui</w> <w n="37.8">l</w>’<w n="37.9">enchante</w>,</l>
							<l n="38" num="5.10"><w n="38.1">Pareil</w> <w n="38.2">au</w> <w n="38.3">voile</w> <w n="38.4">épais</w> <w n="38.5">tombé</w> <w n="38.6">sur</w> <w n="38.7">sa</w> <w n="38.8">raison</w>.</l>
							<l n="39" num="5.11"><w n="39.1">L</w>’<w n="39.2">enfant</w>, <w n="39.3">s</w>’<w n="39.4">il</w> <w n="39.5">vit</w> <w n="39.6">encore</w>, <w n="39.7">est</w> <w n="39.8">loin</w> <w n="39.9">de</w> <w n="39.10">sa</w> <w n="39.11">maison</w> ;</l>
							<l n="40" num="5.12"><w n="40.1">Et</w> <w n="40.2">près</w> <w n="40.3">d</w>’<w n="40.4">un</w> <w n="40.5">berceau</w> <w n="40.6">vide</w> <w n="40.7">elle</w> <w n="40.8">prie</w>… <w n="40.9">elle</w> <w n="40.10">chante</w>.</l>
						</lg>
						<lg n="6">
							<l n="41" num="6.1"><w n="41.1">Dans</w> <w n="41.2">sa</w> <w n="41.3">vague</w> <w n="41.4">tristesse</w>, <w n="41.5">on</w> <w n="41.6">la</w> <w n="41.7">voit</w> <w n="41.8">tout</w> <w n="41.9">le</w> <w n="41.10">jour</w>,</l>
							<l n="42" num="6.2"><space quantity="6" unit="char"></space><w n="42.1">Et</w>, <w n="42.2">sans</w> <w n="42.3">nous</w> <w n="42.4">reconnaître</w> <w n="42.5">à</w> <w n="42.6">peine</w>,</l>
							<l n="43" num="6.3"><space quantity="2" unit="char"></space><w n="43.1">Contre</w> <w n="43.2">son</w> <w n="43.3">sein</w> <w n="43.4">bercer</w> <w n="43.5">une</w> <w n="43.6">ombre</w> <w n="43.7">vaine</w></l>
							<l n="44" num="6.4"><space quantity="6" unit="char"></space><w n="44.1">Et</w> <w n="44.2">lui</w> <w n="44.3">parler</w> <w n="44.4">avec</w> <w n="44.5">amour</w>.</l>
							<l n="45" num="6.5"><space quantity="2" unit="char"></space><w n="45.1">Durant</w> <w n="45.2">la</w> <w n="45.3">nuit</w>, <w n="45.4">tranquille</w> <w n="45.5">et</w> <w n="45.6">demi</w>-<w n="45.7">nue</w>,</l>
							<l n="46" num="6.6"><space quantity="2" unit="char"></space><w n="46.1">Auprès</w> <w n="46.2">des</w> <w n="46.3">feux</w> <w n="46.4">négligés</w> <w n="46.5">et</w> <w n="46.6">mourants</w>,</l>
							<l n="47" num="6.7"><w n="47.1">Elle</w> <w n="47.2">charme</w> <w n="47.3">sa</w> <w n="47.4">veille</w> <w n="47.5">au</w> <w n="47.6">berceau</w> <w n="47.7">retenue</w>,</l>
							<l n="48" num="6.8"><w n="48.1">En</w> <w n="48.2">regardant</w> <w n="48.3">courir</w> <w n="48.4">les</w> <w n="48.5">nuages</w> <w n="48.6">errants</w>.</l>
						</lg>
						<lg n="7">
							<l n="49" num="7.1"><w n="49.1">Un</w> <w n="49.2">soir</w>, <w n="49.3">la</w> <w n="49.4">lune</w> <w n="49.5">absente</w> <w n="49.6">abandonne</w> <w n="49.7">la</w> <w n="49.8">terre</w></l>
							<l n="50" num="7.2"><space quantity="2" unit="char"></space><w n="50.1">Au</w> <w n="50.2">sombre</w> <w n="50.3">autan</w> <w n="50.4">qui</w> <w n="50.5">règne</w> <w n="50.6">avec</w> <w n="50.7">fureur</w> :</l>
							<l n="51" num="7.3"><space quantity="6" unit="char"></space><w n="51.1">Des</w> <w n="51.2">éléments</w> <w n="51.3">la</w> <w n="51.4">lutte</w> <w n="51.5">austère</w></l>
							<l n="52" num="7.4"><space quantity="2" unit="char"></space><w n="52.1">Glace</w> <w n="52.2">les</w> <w n="52.3">sens</w> <w n="52.4">d</w>’<w n="52.5">une</w> <w n="52.6">muette</w> <w n="52.7">horreur</w>.</l>
							<l n="53" num="7.5"><space quantity="2" unit="char"></space><w n="53.1">On</w> <w n="53.2">ne</w> <w n="53.3">voit</w> <w n="53.4">plus</w> <w n="53.5">que</w> <w n="53.6">de</w> <w n="53.7">faibles</w> <w n="53.8">lumières</w> ;</l>
							<l n="54" num="7.6"><space quantity="2" unit="char"></space><w n="54.1">Les</w> <w n="54.2">chiens</w> <w n="54.3">hurlants</w> <w n="54.4">menacent</w> <w n="54.5">les</w> <w n="54.6">chaumières</w> ;</l>
							<l n="55" num="7.7"><space quantity="2" unit="char"></space><w n="55.1">L</w>’<w n="55.2">eau</w> <w n="55.3">dans</w> <w n="55.4">sa</w> <w n="55.5">chute</w> <w n="55.6">entraîne</w> <w n="55.7">l</w>’<w n="55.8">arbrisseau</w> :</l>
							<l n="56" num="7.8"><space quantity="2" unit="char"></space><w n="56.1">De</w> <w n="56.2">cette</w> <w n="56.3">mère</w>, <w n="56.4">immobile</w> <w n="56.5">et</w> <w n="56.6">charmée</w>,</l>
							<l n="57" num="7.9"><space quantity="2" unit="char"></space><w n="57.1">La</w> <w n="57.2">faible</w> <w n="57.3">main</w> <w n="57.4">s</w>’<w n="57.5">endort</w> <w n="57.6">sur</w> <w n="57.7">le</w> <w n="57.8">berceau</w></l>
							<l n="58" num="7.10"><w n="58.1">Que</w> <w n="58.2">semble</w> <w n="58.3">suivre</w> <w n="58.4">encor</w> <w n="58.5">sa</w> <w n="58.6">paupière</w> <w n="58.7">fermée</w>.</l>
						</lg>
						<lg n="8">
							<l n="59" num="8.1"><space quantity="2" unit="char"></space><w n="59.1">Paix</w> ! <w n="59.2">elle</w> <w n="59.3">dort</w> <w n="59.4">pour</w> <w n="59.5">la</w> <w n="59.6">première</w> <w n="59.7">fois</w></l>
							<l n="60" num="8.2"><w n="60.1">Depuis</w> <w n="60.2">le</w> <w n="60.3">jour</w> <w n="60.4">éteint</w> <w n="60.5">dans</w> <w n="60.6">sa</w> <w n="60.7">raison</w> <w n="60.8">perdue</w>,</l>
							<l n="61" num="8.3"><space quantity="2" unit="char"></space><w n="61.1">Qui</w> <w n="61.2">la</w> <w n="61.3">laissa</w> <w n="61.4">sur</w> <w n="61.5">la</w> <w n="61.6">terre</w> <w n="61.7">étendue</w>,</l>
							<l n="62" num="8.4"><space quantity="2" unit="char"></space><w n="62.1">Sans</w> <w n="62.2">souvenir</w>, <w n="62.3">sans</w> <w n="62.4">larmes</w> <w n="62.5">et</w> <w n="62.6">sans</w> <w n="62.7">voix</w>.</l>
							<l n="63" num="8.5"><space quantity="2" unit="char"></space><w n="63.1">Mais</w> <w n="63.2">l</w>’<w n="63.3">ouragan</w>, <w n="63.4">dont</w> <w n="63.5">gémit</w> <w n="63.6">la</w> <w n="63.7">nature</w>,</l>
							<l n="64" num="8.6"><space quantity="2" unit="char"></space><w n="64.1">Semble</w> <w n="64.2">jaloux</w> <w n="64.3">de</w> <w n="64.4">cette</w> <w n="64.5">longue</w> <w n="64.6">erreur</w> ;</l>
							<l n="65" num="8.7"><space quantity="2" unit="char"></space><w n="65.1">Dans</w> <w n="65.2">son</w> <w n="65.3">sommeil</w> <w n="65.4">il</w> <w n="65.5">souffle</w> <w n="65.6">la</w> <w n="65.7">terreur</w>,</l>
							<l n="66" num="8.8"><space quantity="2" unit="char"></space><w n="66.1">Et</w>, <w n="66.2">de</w> <w n="66.3">son</w> <w n="66.4">sein</w> <w n="66.5">réveillant</w> <w n="66.6">la</w> <w n="66.7">torture</w>,</l>
							<l n="67" num="8.9"><space quantity="2" unit="char"></space><w n="67.1">Y</w> <w n="67.2">jette</w> <w n="67.3">un</w> <w n="67.4">cri</w> <w n="67.5">dès</w> <w n="67.6">longtemps</w> <w n="67.7">expiré</w> :</l>
							<l n="68" num="8.10">« <w n="68.1">Rendez</w>, <w n="68.2">rendez</w> <w n="68.3">l</w>’<w n="68.4">enfant</w> <w n="68.5">dans</w> <w n="68.6">la</w> <w n="68.7">foule</w> <w n="68.8">égaré</w> ! »</l>
							<l n="69" num="8.11"><w n="69.1">Comme</w> <w n="69.2">l</w>’<w n="69.3">écho</w> <w n="69.4">frappé</w> <w n="69.5">d</w>’<w n="69.6">une</w> <w n="69.7">clameur</w> <w n="69.8">terrible</w>,</l>
							<l n="70" num="8.12"><w n="70.1">Sa</w> <w n="70.2">raison</w> <w n="70.3">qui</w> <w n="70.4">renaît</w> <w n="70.5">répond</w> <w n="70.6">au</w> <w n="70.7">cri</w> <w n="70.8">d</w>’<w n="70.9">effroi</w> :</l>
							<l n="71" num="8.13">« <w n="71.1">Rendez</w>, <w n="71.2">rendez</w> <w n="71.3">l</w>’<w n="71.4">enfant</w> ! <w n="71.5">rendez</w>… » <w n="71.6">Réveil</w> <w n="71.7">horrible</w> !</l>
							<l n="72" num="8.14"><w n="72.1">Ce</w> <w n="72.2">berceau</w> <w n="72.3">découvert</w>, <w n="72.4">il</w> <w n="72.5">est</w> <w n="72.6">vide</w>, <w n="72.7">il</w> <w n="72.8">est</w> <w n="72.9">froid</w> !</l>
						</lg>
						<lg n="9">
							<l n="73" num="9.1"><space quantity="2" unit="char"></space><w n="73.1">Pâle</w>, <w n="73.2">muette</w>, <w n="73.3">en</w> <w n="73.4">ses</w> <w n="73.5">larmes</w> <w n="73.6">glacée</w>,</l>
							<l n="74" num="9.2"><space quantity="2" unit="char"></space><w n="74.1">Elle</w> <w n="74.2">repousse</w> <w n="74.3">et</w> <w n="74.4">combat</w> <w n="74.5">sa</w> <w n="74.6">pensée</w> ;</l>
							<l n="75" num="9.3"><space quantity="2" unit="char"></space><w n="75.1">Puis</w> <w n="75.2">elle</w> <w n="75.3">dit</w>, <w n="75.4">en</w> <w n="75.5">se</w> <w n="75.6">cachant</w> <w n="75.7">les</w> <w n="75.8">yeux</w> :</l>
							<l n="76" num="9.4">« <w n="76.1">Je</w> <w n="76.2">reconnais</w> <w n="76.3">la</w> <w n="76.4">terre</w>, <w n="76.5">et</w> <w n="76.6">j</w>’<w n="76.7">ai</w> <w n="76.8">perdu</w> <w n="76.9">les</w> <w n="76.10">cieux</w> !</l>
							<l n="77" num="9.5"><w n="77.1">Dieu</w> <w n="77.2">des</w> <w n="77.3">mères</w> ! <w n="77.4">mon</w> <w n="77.5">Dieu</w> ! <w n="77.6">vous</w> <w n="77.7">savez</w> <w n="77.8">s</w>’<w n="77.9">il</w> <w n="77.10">respire</w>.</l>
							<l n="78" num="9.6"><w n="78.1">Rendez</w>-<w n="78.2">le</w> ! <w n="78.3">guidez</w>-<w n="78.4">moi</w>… <w n="78.5">je</w> <w n="78.6">ne</w> <w n="78.7">sais</w> <w n="78.8">où</w>… <w n="78.9">j</w>’<w n="78.10">expire</w> !</l>
							<l n="79" num="9.7"><space quantity="2" unit="char"></space><w n="79.1">Il</w> <w n="79.2">n</w>’<w n="79.3">est</w> <w n="79.4">plus</w> <w n="79.5">là</w>… <w n="79.6">je</w> <w n="79.7">n</w>’<w n="79.8">y</w> <w n="79.9">peux</w> <w n="79.10">plus</w> <w n="79.11">rester</w>.</l>
							<l n="80" num="9.8"><w n="80.1">Eh</w> <w n="80.2">bien</w> ! <w n="80.3">puisque</w> <w n="80.4">la</w> <w n="80.5">mort</w> <w n="80.6">ne</w> <w n="80.7">veut</w> <w n="80.8">pas</w> <w n="80.9">m</w>’<w n="80.10">arrêter</w>,</l>
							<l n="81" num="9.9"><w n="81.1">J</w>’<w n="81.2">irai</w>, <w n="81.3">par</w> <w n="81.4">les</w> <w n="81.5">chemins</w>, <w n="81.6">traîner</w>, <w n="81.7">finir</w> <w n="81.8">ma</w> <w n="81.9">vie</w>. »</l>
							<l n="82" num="9.10"><w n="82.1">Et</w> <w n="82.2">le</w> <w n="82.3">jour</w>, <w n="82.4">sur</w> <w n="82.5">la</w> <w n="82.6">neige</w> <w n="82.7">on</w> <w n="82.8">reconnaît</w> <w n="82.9">ses</w> <w n="82.10">pas</w>.</l>
							<l n="83" num="9.11"><w n="83.1">Elle</w> <w n="83.2">était</w> <w n="83.3">douce</w> <w n="83.4">et</w> <w n="83.5">faible</w> ; <w n="83.6">on</w> <w n="83.7">ne</w> <w n="83.8">l</w>’<w n="83.9">observait</w> <w n="83.10">pas</w>,</l>
							<l n="84" num="9.12"><space quantity="6" unit="char"></space><w n="84.1">Et</w> <w n="84.2">personne</w> <w n="84.3">ne</w> <w n="84.4">l</w>’<w n="84.5">a</w> <w n="84.6">suivie</w>.</l>
							<l n="85" num="9.13"><w n="85.1">Dans</w> <w n="85.2">les</w> <w n="85.3">sentiers</w> <w n="85.4">déserts</w> <w n="85.5">Dieu</w> <w n="85.6">seul</w> <w n="85.7">l</w>’<w n="85.8">entend</w> <w n="85.9">gémir</w> ;</l>
							<l n="86" num="9.14"><space quantity="2" unit="char"></space><w n="86.1">Mais</w> <w n="86.2">l</w>’<w n="86.3">aquilon</w> <w n="86.4">a</w> <w n="86.5">cessé</w> <w n="86.6">de</w> <w n="86.7">frémir</w>.</l>
						</lg>
						<lg n="10">
							<l n="87" num="10.1"><w n="87.1">Elle</w> <w n="87.2">marche</w>, <w n="87.3">elle</w> <w n="87.4">dit</w> : « <w n="87.5">Je</w> <w n="87.6">veux</w> <w n="87.7">voir</w> <w n="87.8">la</w> <w n="87.9">chapelle</w></l>
							<l n="88" num="10.2"><w n="88.1">Qu</w>’<w n="88.2">au</w> <w n="88.3">temps</w> <w n="88.4">de</w> <w n="88.5">la</w> <w n="88.6">moisson</w> <w n="88.7">j</w>’<w n="88.8">embellis</w> <w n="88.9">une</w> <w n="88.10">fois</w>,</l>
							<l n="89" num="10.3"><w n="89.1">Où</w> <w n="89.2">mon</w> <w n="89.3">fils</w>… <w n="89.4">jour</w> <w n="89.5">trompeur</w> <w n="89.6">qu</w>’<w n="89.7">à</w> <w n="89.8">présent</w> <w n="89.9">tout</w> <w n="89.10">rappelle</w> !</l>
							<l n="90" num="10.4"><w n="90.1">Sur</w> <w n="90.2">ma</w> <w n="90.3">voix</w>, <w n="90.4">qui</w> <w n="90.5">chantait</w>, <w n="90.6">voulait</w> <w n="90.7">former</w> <w n="90.8">sa</w> <w n="90.9">voix</w>.</l>
							<l n="91" num="10.5"><w n="91.1">J</w>’<w n="91.2">y</w> <w n="91.3">porte</w> <w n="91.4">son</w> <w n="91.5">berceau</w>, <w n="91.6">c</w>’<w n="91.7">est</w> <w n="91.8">mon</w> <w n="91.9">dernier</w> <w n="91.10">hommage</w> ;</l>
							<l n="92" num="10.6"><w n="92.1">Douloureux</w> <w n="92.2">pour</w> <w n="92.3">sa</w> <w n="92.4">mère</w>, <w n="92.5">inutile</w> <w n="92.6">pour</w> <w n="92.7">lui</w>,</l>
							<l n="93" num="10.7"><w n="93.1">Ce</w> <w n="93.2">n</w>’<w n="93.3">est</w> <w n="93.4">plus</w> <w n="93.5">qu</w>’<w n="93.6">un</w> <w n="93.7">tombeau</w> <w n="93.8">que</w> <w n="93.9">j</w>’<w n="93.10">y</w> <w n="93.11">vois</w> <w n="93.12">aujourd</w>’<w n="93.13">hui</w>,</l>
							<l n="94" num="10.8"><w n="94.1">Et</w> <w n="94.2">dans</w> <w n="94.3">mon</w> <w n="94.4">âme</w> <w n="94.5">en</w> <w n="94.6">deuil</w> <w n="94.7">j</w>’<w n="94.8">offrirai</w> <w n="94.9">son</w> <w n="94.10">image</w>.</l>
							<l n="95" num="10.9"><w n="95.1">Des</w> <w n="95.2">fleurs</w>… <w n="95.3">je</w> <w n="95.4">n</w>’<w n="95.5">en</w> <w n="95.6">ai</w> <w n="95.7">plus</w>… <w n="95.8">Ah</w> ! <w n="95.9">j</w>’<w n="95.10">ai</w> <w n="95.11">trop</w> <w n="95.12">peu</w> <w n="95.13">de</w> <w n="95.14">temps</w> ;</l>
							<l n="96" num="10.10"><space quantity="6" unit="char"></space><w n="96.1">On</w> <w n="96.2">meurt</w> <w n="96.3">jeune</w> <w n="96.4">sans</w> <w n="96.5">l</w>’<w n="96.6">espérance</w> ;</l>
							<l n="97" num="10.11"><w n="97.1">Mais</w> <w n="97.2">tant</w> <w n="97.3">que</w> <w n="97.4">je</w> <w n="97.5">vivrai</w>, <w n="97.6">fût</w>-<w n="97.7">ce</w> <w n="97.8">jusqu</w>’<w n="97.9">au</w> <w n="97.10">printemps</w>,</l>
							<l n="98" num="10.12"><space quantity="6" unit="char"></space><w n="98.1">J</w>’<w n="98.2">y</w> <w n="98.3">viendrai</w> <w n="98.4">cacher</w> <w n="98.5">ma</w> <w n="98.6">souffrance</w> ! »</l>
						</lg>
						<lg n="11">
							<l n="99" num="11.1"><w n="99.1">Alors</w> <w n="99.2">un</w> <w n="99.3">saint</w> <w n="99.4">pasteur</w>, <w n="99.5">triste</w> <w n="99.6">de</w> <w n="99.7">souvenir</w>,</l>
							<l n="100" num="11.2"><w n="100.1">Prend</w> <w n="100.2">le</w> <w n="100.3">berceau</w> <w n="100.4">léger</w> <w n="100.5">qu</w>’<w n="100.6">il</w> <w n="100.7">promet</w> <w n="100.8">de</w> <w n="100.9">bénir</w>.</l>
						</lg>
						<lg n="12">
							<l n="101" num="12.1"><w n="101.1">Une</w> <w n="101.2">autre</w> <w n="101.3">femme</w> <w n="101.4">approche</w> <w n="101.5">en</w> <w n="101.6">sa</w> <w n="101.7">misère</w> <w n="101.8">errante</w> ;</l>
							<l n="102" num="12.2"><w n="102.1">Sa</w> <w n="102.2">voix</w> <w n="102.3">n</w>’<w n="102.4">a</w> <w n="102.5">qu</w>’<w n="102.6">un</w> <w n="102.7">accent</w> <w n="102.8">qui</w> <w n="102.9">murmure</w> : « <w n="102.10">Donnez</w> ! »</l>
							<l n="103" num="12.3"><w n="103.1">Elle</w> <w n="103.2">indique</w> <w n="103.3">un</w> <w n="103.4">enfant</w> <w n="103.5">aux</w> <w n="103.6">regards</w> <w n="103.7">consternés</w>,</l>
							<l n="104" num="12.4"><w n="104.1">Et</w> <w n="104.2">cet</w> <w n="104.3">objet</w> <w n="104.4">voilé</w> <w n="104.5">la</w> <w n="104.6">rend</w> <w n="104.7">plus</w> <w n="104.8">déchirante</w>.</l>
							<l n="105" num="12.5">« <w n="105.1">Femme</w>, <w n="105.2">dit</w> <w n="105.3">l</w>’<w n="105.4">autre</w> <w n="105.5">mère</w>, <w n="105.6">il</w> <w n="105.7">faut</w> <w n="105.8">vous</w> <w n="105.9">secourir</w> :</l>
							<l n="106" num="12.6"><w n="106.1">Vous</w> <w n="106.2">cachez</w> <w n="106.3">un</w> <w n="106.4">enfant</w> ; <w n="106.5">sa</w> <w n="106.6">misère</w> <w n="106.7">est</w> <w n="106.8">affreuse</w> !</l>
							<l n="107" num="12.7"><w n="107.1">Ne</w> <w n="107.2">souffrez</w> <w n="107.3">pas</w> <w n="107.4">pour</w> <w n="107.5">lui</w>, <w n="107.6">femme</w> ! <w n="107.7">Soyez</w> <w n="107.8">heureuse</w> !</l>
							<l n="108" num="12.8"><w n="108.1">Moi</w>, <w n="108.2">je</w> <w n="108.3">n</w>’<w n="108.4">ai</w> <w n="108.5">plus</w> <w n="108.6">d</w>’<w n="108.7">enfant</w>… <w n="108.8">moi</w>, <w n="108.9">je</w> <w n="108.10">n</w>’<w n="108.11">ai</w> <w n="108.12">qu</w>’<w n="108.13">à</w> <w n="108.14">mourir</w> ! »</l>
						</lg>
						<lg n="13">
							<l n="109" num="13.1"><space quantity="2" unit="char"></space><w n="109.1">Un</w> <w n="109.2">cri</w> <w n="109.3">perçant</w> <w n="109.4">rompt</w> <w n="109.5">cette</w> <w n="109.6">plainte</w> <w n="109.7">amère</w>,</l>
							<l n="110" num="13.2"><w n="110.1">Et</w> <w n="110.2">le</w> <w n="110.3">lambeau</w> <w n="110.4">s</w>’<w n="110.5">agite</w>, <w n="110.6">et</w> <w n="110.7">le</w> <w n="110.8">cri</w> <w n="110.9">dit</w> : « <w n="110.10">Ma</w> <w n="110.11">mère</w> ! »</l>
							<l n="111" num="13.3"><w n="111.1">Et</w> <w n="111.2">la</w> <w n="111.3">mère</w> <w n="111.4">éperdue</w> <w n="111.5">a</w> <w n="111.6">saisi</w> <w n="111.7">son</w> <w n="111.8">enfant</w> ;</l>
							<l n="112" num="13.4"><w n="112.1">Et</w> <w n="112.2">l</w>’<w n="112.3">affreuse</w> <w n="112.4">étrangère</w> <w n="112.5">à</w> <w n="112.6">peine</w> <w n="112.7">le</w> <w n="112.8">défend</w> ;</l>
							<l n="113" num="13.5"><w n="113.1">Elle</w> <w n="113.2">fuit</w>, <w n="113.3">elle</w> <w n="113.4">roule</w> <w n="113.5">au</w> <w n="113.6">bas</w> <w n="113.7">de</w> <w n="113.8">la</w> <w n="113.9">montagne</w>,</l>
							<l n="114" num="13.6"><w n="114.1">Et</w>, <w n="114.2">comme</w> <w n="114.3">un</w> <w n="114.4">noir</w> <w n="114.5">corbeau</w>, <w n="114.6">se</w> <w n="114.7">perd</w> <w n="114.8">dans</w> <w n="114.9">la</w> <w n="114.10">campagne</w>.</l>
							<l n="115" num="13.7"><w n="115.1">La</w> <w n="115.2">mère</w> <w n="115.3">véritable</w> <w n="115.4">écarte</w> <w n="115.5">les</w> <w n="115.6">lambeaux</w> ;</l>
							<l n="116" num="13.8"><w n="116.1">Ses</w> <w n="116.2">yeux</w> <w n="116.3">longtemps</w> <w n="116.4">éteints</w>, <w n="116.5">pareils</w> <w n="116.6">à</w> <w n="116.7">deux</w> <w n="116.8">flambeaux</w>,</l>
							<l n="117" num="13.9"><w n="117.1">S</w>’<w n="117.2">allument</w> : « <w n="117.3">C</w>’<w n="117.4">est</w> <w n="117.5">mon</w> <w n="117.6">fils</w> !… <w n="117.7">qu</w>’<w n="117.8">il</w> <w n="117.9">est</w> <w n="117.10">pâle</w> ! » <w n="117.11">Elle</w> <w n="117.12">tombe</w> :</l>
							<l n="118" num="13.10"><w n="118.1">Sous</w> <w n="118.2">l</w>’<w n="118.3">excès</w> <w n="118.4">du</w> <w n="118.5">bonheur</w> <w n="118.6">la</w> <w n="118.7">nature</w> <w n="118.8">succombe</w> ;</l>
							<l n="119" num="13.11"><space quantity="2" unit="char"></space><w n="119.1">Car</w> <w n="119.2">on</w> <w n="119.3">dirait</w> <w n="119.4">que</w>, <w n="119.5">créés</w> <w n="119.6">pour</w> <w n="119.7">souffrir</w>,</l>
							<l n="120" num="13.12"><w n="120.1">Nous</w> <w n="120.2">ne</w> <w n="120.3">pouvons</w> <w n="120.4">qu</w>’<w n="120.5">à</w> <w n="120.6">peine</w> <w n="120.7">être</w> <w n="120.8">heureux</w> <w n="120.9">sans</w> <w n="120.10">mourir</w>.</l>
							<l n="121" num="13.13"><w n="121.1">Mais</w> <w n="121.2">l</w>’<w n="121.3">enfant</w> <w n="121.4">la</w> <w n="121.5">caresse</w> ; <w n="121.6">il</w> <w n="121.7">la</w> <w n="121.8">rappelle</w>, <w n="121.9">il</w> <w n="121.10">pleure</w> ;</l>
							<l n="122" num="13.14"><w n="122.1">Il</w> <w n="122.2">arrête</w> <w n="122.3">son</w> <w n="122.4">âme</w> <w n="122.5">aux</w> <w n="122.6">lèvres</w> <w n="122.7">qu</w>’<w n="122.8">il</w> <w n="122.9">effleure</w>,</l>
							<l n="123" num="13.15"><w n="123.1">Et</w> <w n="123.2">son</w> <w n="123.3">corps</w> <w n="123.4">délicat</w>, <w n="123.5">par</w> <w n="123.6">sa</w> <w n="123.7">mère</w> <w n="123.8">entouré</w>,</l>
							<l n="124" num="13.16"><w n="124.1">Palpite</w> <w n="124.2">et</w> <w n="124.3">tremble</w> <w n="124.4">encor</w> <w n="124.5">d</w>’<w n="124.6">en</w> <w n="124.7">être</w> <w n="124.8">séparé</w>.</l>
							<l n="125" num="13.17">« <w n="125.1">Ne</w> <w n="125.2">tremble</w> <w n="125.3">plus</w> ; <w n="125.4">c</w>’<w n="125.5">est</w> <w n="125.6">moi</w>. <w n="125.7">Vois</w>-<w n="125.8">tu</w> ; <w n="125.9">je</w> <w n="125.10">suis</w> <w n="125.11">ta</w> <w n="125.12">mère</w>.</l>
							<l n="126" num="13.18"><w n="126.1">Ô</w> ! <w n="126.2">mon</w> <w n="126.3">fils</w> ! <w n="126.4">C</w>’<w n="126.5">est</w> <w n="126.6">mon</w> <w n="126.7">fils</w> ! <w n="126.8">regarde</w>-<w n="126.9">le</w>, <w n="126.10">mon</w> <w n="126.11">père</w> ;</l>
							<l n="127" num="13.19"><w n="127.1">C</w>’<w n="127.2">est</w> <w n="127.3">mon</w> <w n="127.4">fils</w> ! <w n="127.5">Ce</w> <w n="127.6">n</w>’<w n="127.7">est</w> <w n="127.8">plus</w> <w n="127.9">son</w> <w n="127.10">fantôme</w> <w n="127.11">trompeur</w> ;</l>
							<l n="128" num="13.20"><w n="128.1">C</w>’<w n="128.2">est</w> <w n="128.3">mon</w> <w n="128.4">enfant</w> <w n="128.5">qui</w> <w n="128.6">m</w>’<w n="128.7">aime</w>, <w n="128.8">et</w> <w n="128.9">qui</w> <w n="128.10">vit</w> <w n="128.11">sur</w> <w n="128.12">mon</w> <w n="128.13">cœur</w>. »</l>
						</lg>
						<lg n="14">
							<l n="129" num="14.1"><w n="129.1">Le</w> <w n="129.2">pasteur</w> <w n="129.3">pour</w> <w n="129.4">le</w> <w n="129.5">voir</w> <w n="129.6">se</w> <w n="129.7">courbe</w> <w n="129.8">devant</w> <w n="129.9">elle</w> ;</l>
							<l n="130" num="14.2"><w n="130.1">Il</w> <w n="130.2">sent</w> <w n="130.3">couler</w> <w n="130.4">ses</w> <w n="130.5">pleurs</w> <w n="130.6">à</w> <w n="130.7">son</w> <w n="130.8">récit</w> <w n="130.9">fidèle</w> ;</l>
							<l n="131" num="14.3"><space quantity="2" unit="char"></space><w n="131.1">Elle</w> <w n="131.2">dit</w> <w n="131.3">tout</w> <w n="131.4">en</w> <w n="131.5">paroles</w> <w n="131.6">de</w> <w n="131.7">feu</w> ;</l>
							<l n="132" num="14.4"><w n="132.1">De</w> <w n="132.2">baisers</w>, <w n="132.3">de</w> <w n="132.4">sanglots</w>, <w n="132.5">son</w> <w n="132.6">récit</w> <w n="132.7">se</w> <w n="132.8">compose</w>.</l>
							<l n="133" num="14.5"><w n="133.1">En</w> <w n="133.2">vain</w> <w n="133.3">pour</w> <w n="133.4">sa</w> <w n="133.5">vengeance</w> <w n="133.6">elle</w> <w n="133.7">bégaie</w> <w n="133.8">un</w> <w n="133.9">vœu</w> ;</l>
							<l n="134" num="14.6"><w n="134.1">Sortira</w>-<w n="134.2">t</w>-<w n="134.3">il</w> <w n="134.4">du</w> <w n="134.5">cœur</w> <w n="134.6">où</w> <w n="134.7">son</w> <w n="134.8">fils</w> <w n="134.9">se</w> <w n="134.10">repose</w> ?</l>
							<l n="135" num="14.7"><w n="135.1">Sans</w> <w n="135.2">doute</w> <w n="135.3">il</w> <w n="135.4">a</w> <w n="135.5">souffert</w>, <w n="135.6">l</w>’<w n="135.7">enfant</w> <w n="135.8">infortuné</w> !</l>
							<l n="136" num="14.8"><w n="136.1">Sans</w> <w n="136.2">doute</w>… <w n="136.3">il</w> <w n="136.4">vit</w> <w n="136.5">encor</w> ; <w n="136.6">sa</w> <w n="136.7">mère</w> <w n="136.8">a</w> <w n="136.9">pardonné</w>.</l>
						</lg>
					</div></body></text></TEI>