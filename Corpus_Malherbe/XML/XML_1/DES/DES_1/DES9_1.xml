<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">IDYLLES</head><div type="poem" key="DES9">
						<head type="main">LE RETOUR AUX CHAMPS</head>
						<lg n="1">
							<l n="1" num="1.1"><space quantity="6" unit="char"></space><w n="1.1">Que</w> <w n="1.2">ce</w> <w n="1.3">lieu</w> <w n="1.4">me</w> <w n="1.5">semble</w> <w n="1.6">attristé</w> !</l>
							<l n="2" num="1.2"><space quantity="6" unit="char"></space><w n="2.1">Tout</w> <w n="2.2">a</w> <w n="2.3">changé</w> <w n="2.4">dans</w> <w n="2.5">la</w> <w n="2.6">nature</w> ;</l>
							<l n="3" num="1.3"><space quantity="6" unit="char"></space><w n="3.1">Le</w> <w n="3.2">printemps</w> <w n="3.3">n</w>’<w n="3.4">a</w> <w n="3.5">plus</w> <w n="3.6">de</w> <w n="3.7">verdure</w> ;</l>
							<l n="4" num="1.4"><space quantity="6" unit="char"></space><w n="4.1">Le</w> <w n="4.2">bocage</w> <w n="4.3">est</w> <w n="4.4">désenchanté</w> !</l>
							<l n="5" num="1.5"><space quantity="6" unit="char"></space><w n="5.1">Autrefois</w>, <w n="5.2">l</w>’<w n="5.3">onde</w> <w n="5.4">fugitive</w></l>
							<l n="6" num="1.6"><w n="6.1">Arrosait</w>, <w n="6.2">en</w> <w n="6.3">courant</w>, <w n="6.4">les</w> <w n="6.5">cailloux</w> <w n="6.6">et</w> <w n="6.7">les</w> <w n="6.8">fleurs</w> :</l>
							<l n="7" num="1.7"><w n="7.1">Je</w> <w n="7.2">ne</w> <w n="7.3">vois</w> <w n="7.4">qu</w>’<w n="7.5">un</w> <w n="7.6">roseau</w> <w n="7.7">languissant</w> <w n="7.8">sur</w> <w n="7.9">la</w> <w n="7.10">rive</w>,</l>
							<l n="8" num="1.8"><space quantity="6" unit="char"></space><w n="8.1">Et</w> <w n="8.2">mes</w> <w n="8.3">yeux</w> <w n="8.4">se</w> <w n="8.5">couvrent</w> <w n="8.6">de</w> <w n="8.7">pleurs</w> !</l>
						</lg>
						<lg n="2">
							<l n="9" num="2.1"><space quantity="6" unit="char"></space><w n="9.1">Hélas</w> ! <w n="9.2">on</w> <w n="9.3">a</w> <w n="9.4">changé</w> <w n="9.5">ta</w> <w n="9.6">course</w>,</l>
							<l n="10" num="2.2"><w n="10.1">Ruisseau</w>, <w n="10.2">de</w> <w n="10.3">l</w>’<w n="10.4">inconstance</w> <w n="10.5">on</w> <w n="10.6">te</w> <w n="10.7">fait</w> <w n="10.8">une</w> <w n="10.9">loi</w>,</l>
							<l n="11" num="2.3"><w n="11.1">Et</w> <w n="11.2">je</w> <w n="11.3">n</w>’<w n="11.4">espère</w> <w n="11.5">plus</w> <w n="11.6">retrouver</w> <w n="11.7">à</w> <w n="11.8">ta</w> <w n="11.9">source</w></l>
							<l n="12" num="2.4"><space quantity="6" unit="char"></space><w n="12.1">Les</w> <w n="12.2">serments</w> <w n="12.3">emportés</w> <w n="12.4">par</w> <w n="12.5">toi</w>.</l>
							<l n="13" num="2.5"><w n="13.1">Ah</w> ! <w n="13.2">si</w> <w n="13.3">pour</w> <w n="13.4">rafraîchir</w> <w n="13.5">une</w> <w n="13.6">âme</w> <w n="13.7">désolée</w></l>
							<l n="14" num="2.6"><space quantity="6" unit="char"></space><w n="14.1">Il</w> <w n="14.2">suffit</w> <w n="14.3">d</w>’<w n="14.4">un</w> <w n="14.5">doux</w> <w n="14.6">souvenir</w>,</l>
							<l n="15" num="2.7"><w n="15.1">Ruisseau</w>, <w n="15.2">pour</w> <w n="15.3">ranimer</w> <w n="15.4">l</w>’<w n="15.5">herbe</w> <w n="15.6">de</w> <w n="15.7">la</w> <w n="15.8">vallée</w>,</l>
							<l n="16" num="2.8"><space quantity="6" unit="char"></space><w n="16.1">Parfois</w> <w n="16.2">n</w>’<w n="16.3">y</w> <w n="16.4">peux</w>-<w n="16.5">tu</w> <w n="16.6">revenir</w> ?</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">J</w>’<w n="17.2">entends</w> <w n="17.3">du</w> <w n="17.4">vieux</w> <w n="17.5">berger</w> <w n="17.6">la</w> <w n="17.7">plaintive</w> <w n="17.8">musette</w> ;</l>
							<l n="18" num="3.2"><space quantity="6" unit="char"></space><w n="18.1">Mais</w> <w n="18.2">qu</w>’<w n="18.3">est</w> <w n="18.4">devenu</w> <w n="18.5">le</w> <w n="18.6">troupeau</w> ?</l>
							<l n="19" num="3.3"><space quantity="6" unit="char"></space><w n="19.1">Sous</w> <w n="19.2">l</w>’<w n="19.3">empire</w> <w n="19.4">de</w> <w n="19.5">sa</w> <w n="19.6">houlette</w>,</l>
							<l n="20" num="3.4"><space quantity="2" unit="char"></space><w n="20.1">Il</w> <w n="20.2">n</w>’<w n="20.3">a</w> <w n="20.4">plus</w> <w n="20.5">même</w> <w n="20.6">un</w> <w n="20.7">innocent</w> <w n="20.8">agneau</w>.</l>
							<l n="21" num="3.5"><space quantity="2" unit="char"></space><w n="21.1">Tout</w> <w n="21.2">en</w> <w n="21.3">rêvant</w> <w n="21.4">il</w> <w n="21.5">gravit</w> <w n="21.6">la</w> <w n="21.7">montagne</w> :</l>
							<l n="22" num="3.6"><w n="22.1">Il</w> <w n="22.2">traîne</w> <w n="22.3">avec</w> <w n="22.4">effort</w> <w n="22.5">son</w> <w n="22.6">âge</w> <w n="22.7">et</w> <w n="22.8">son</w> <w n="22.9">ennui</w> ;</l>
							<l n="23" num="3.7"><w n="23.1">Les</w> <w n="23.2">moutons</w> <w n="23.3">ont</w> <w n="23.4">quitté</w> <w n="23.5">la</w> <w n="23.6">stérile</w> <w n="23.7">campagne</w> ;</l>
							<l n="24" num="3.8"><space quantity="6" unit="char"></space><w n="24.1">Le</w> <w n="24.2">chien</w> <w n="24.3">est</w> <w n="24.4">resté</w> <w n="24.5">près</w> <w n="24.6">de</w> <w n="24.7">lui</w>.</l>
							<l n="25" num="3.9"><space quantity="2" unit="char"></space><w n="25.1">Mais</w> <w n="25.2">que</w> <w n="25.3">sa</w> <w n="25.4">peine</w> <w n="25.5">est</w> <w n="25.6">facile</w> <w n="25.7">et</w> <w n="25.8">légère</w> !</l>
							<l n="26" num="3.10"><w n="26.1">Du</w> <w n="26.2">bonheur</w> <w n="26.3">qui</w> <w n="26.4">n</w>’<w n="26.5">est</w> <w n="26.6">plus</w> <w n="26.7">il</w> <w n="26.8">n</w>’<w n="26.9">a</w> <w n="26.10">point</w> <w n="26.11">à</w> <w n="26.12">rougir</w> ;</l>
							<l n="27" num="3.11"><w n="27.1">Sans</w> <w n="27.2">trouble</w>, <w n="27.3">sur</w> <w n="27.4">un</w> <w n="27.5">lit</w> <w n="27.6">de</w> <w n="27.7">mousse</w> <w n="27.8">ou</w> <w n="27.9">de</w> <w n="27.10">fougère</w>,</l>
							<l n="28" num="3.12"><space quantity="6" unit="char"></space><w n="28.1">Quand</w> <w n="28.2">la</w> <w n="28.3">nuit</w> <w n="28.4">vient</w>, <w n="28.5">il</w> <w n="28.6">peut</w> <w n="28.7">dormir</w>.</l>
						</lg>
						<lg n="4">
							<l n="29" num="4.1"><w n="29.1">Que</w> <w n="29.2">de</w> <w n="29.3">riches</w> <w n="29.4">pasteurs</w> <w n="29.5">lui</w> <w n="29.6">porteraient</w> <w n="29.7">envie</w> !</l>
							<l n="30" num="4.2"><w n="30.1">Combien</w> <w n="30.2">voudraient</w> <w n="30.3">donner</w> <w n="30.4">les</w> <w n="30.5">plus</w> <w n="30.6">nombreux</w> <w n="30.7">troupeaux</w>,</l>
							<l n="31" num="4.3"><space quantity="6" unit="char"></space><w n="31.1">La</w> <w n="31.2">houlette</w>, <w n="31.3">la</w> <w n="31.4">bergerie</w>,</l>
							<l n="32" num="4.4"><space quantity="6" unit="char"></space><w n="32.1">Pour</w> <w n="32.2">une</w> <w n="32.3">nuit</w> <w n="32.4">d</w>’<w n="32.5">un</w> <w n="32.6">doux</w> <w n="32.7">repos</w> !</l>
						</lg>
						<lg n="5">
							<l n="33" num="5.1"><w n="33.1">Et</w> <w n="33.2">moi</w>, <w n="33.3">d</w>’<w n="33.4">amis</w> <w n="33.5">aussi</w> <w n="33.6">je</w> <w n="33.7">fus</w> <w n="33.8">environnée</w> ;</l>
							<l n="34" num="5.2"><w n="34.1">Mon</w> <w n="34.2">avenir</w> <w n="34.3">alors</w> <w n="34.4">était</w> <w n="34.5">brillant</w> <w n="34.6">et</w> <w n="34.7">sûr</w>.</l>
							<l n="35" num="5.3"><w n="35.1">Vieux</w> <w n="35.2">berger</w>, <w n="35.3">comme</w> <w n="35.4">toi</w> <w n="35.5">je</w> <w n="35.6">suis</w> <w n="35.7">abandonnée</w> ;</l>
							<l n="36" num="5.4"><w n="36.1">Le</w> <w n="36.2">songe</w> <w n="36.3">est</w> <w n="36.4">dissipé</w>, <w n="36.5">mais</w> <w n="36.6">le</w> <w n="36.7">réveil</w> <w n="36.8">est</w> <w n="36.9">pur</w> !</l>
						</lg>
						<lg n="6">
							<l n="37" num="6.1"><space quantity="6" unit="char"></space><w n="37.1">Me</w> <w n="37.2">voici</w> <w n="37.3">devant</w> <w n="37.4">la</w> <w n="37.5">chapelle</w></l>
							<l n="38" num="6.2"><w n="38.1">Où</w> <w n="38.2">mon</w> <w n="38.3">cœur</w> <w n="38.4">sans</w> <w n="38.5">détour</w> <w n="38.6">jura</w> <w n="38.7">ses</w> <w n="38.8">premiers</w> <w n="38.9">vœux</w>.</l>
							<l n="39" num="6.3"><space quantity="6" unit="char"></space><w n="39.1">Déjà</w> <w n="39.2">mon</w> <w n="39.3">cœur</w> <w n="39.4">n</w>’<w n="39.5">est</w> <w n="39.6">plus</w> <w n="39.7">heureux</w>,</l>
							<l n="40" num="6.4"><w n="40.1">Mais</w> <w n="40.2">à</w> <w n="40.3">ses</w> <w n="40.4">vœux</w> <w n="40.5">trahis</w> <w n="40.6">il</w> <w n="40.7">est</w> <w n="40.8">encor</w> <w n="40.9">fidèle</w>.</l>
							<l n="41" num="6.5"><space quantity="6" unit="char"></space><w n="41.1">J</w>’<w n="41.2">y</w> <w n="41.3">vins</w> <w n="41.4">offrir</w>, <w n="41.5">l</w>’<w n="41.6">autre</w> <w n="41.7">printemps</w>,</l>
							<l n="42" num="6.6"><w n="42.1">Une</w> <w n="42.2">fraîche</w> <w n="42.3">couronne</w>, <w n="42.4">aujourd</w>’<w n="42.5">hui</w> <w n="42.6">desséchée</w>.</l>
							<l n="43" num="6.7"><w n="43.1">Cette</w> <w n="43.2">chapelle</w>, <w n="43.3">hélas</w> ! <w n="43.4">dans</w> <w n="43.5">les</w> <w n="43.6">ronces</w> <w n="43.7">cachée</w>,</l>
							<l n="44" num="6.8"><w n="44.1">N</w>’<w n="44.2">est</w>-<w n="44.3">elle</w> <w n="44.4">plus</w> <w n="44.5">l</w>’<w n="44.6">amour</w> <w n="44.7">des</w> <w n="44.8">simples</w> <w n="44.9">habitants</w> ?</l>
							<l n="45" num="6.9"><space quantity="6" unit="char"></space><w n="45.1">Seule</w>, <w n="45.2">j</w>’<w n="45.3">y</w> <w n="45.4">ferai</w> <w n="45.5">ma</w> <w n="45.6">prière</w> :</l>
							<l n="46" num="6.10"><w n="46.1">Mon</w> <w n="46.2">sort</w>, <w n="46.3">je</w> <w n="46.4">le</w> <w n="46.5">sais</w> <w n="46.6">trop</w>, <w n="46.7">me</w> <w n="46.8">défend</w> <w n="46.9">d</w>’<w n="46.10">espérer</w> ;</l>
							<l n="47" num="6.11"><w n="47.1">Eh</w> <w n="47.2">bien</w> ! <w n="47.3">sans</w> <w n="47.4">espérance</w>, <w n="47.5">à</w> <w n="47.6">genoux</w> <w n="47.7">sur</w> <w n="47.8">la</w> <w n="47.9">pierre</w>,</l>
							<l n="48" num="6.12"><space quantity="2" unit="char"></space><w n="48.1">J</w>’<w n="48.2">aurai</w> <w n="48.3">du</w> <w n="48.4">moins</w> <w n="48.5">la</w> <w n="48.6">douceur</w> <w n="48.7">de</w> <w n="48.8">pleurer</w>.</l>
						</lg>
					</div></body></text></TEI>