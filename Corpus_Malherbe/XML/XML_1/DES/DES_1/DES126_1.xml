<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">ROMANCES</head><div type="poem" key="DES126">
						<head type="main">UNE REINE</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">Barde</w> <w n="1.3">a</w> <w n="1.4">vu</w> <w n="1.5">sa</w> <w n="1.6">reine</w> <w n="1.7">fugitive</w> ;</l>
							<l n="2" num="1.2"><w n="2.1">Il</w> <w n="2.2">dit</w> <w n="2.3">qu</w>’<w n="2.4">un</w> <w n="2.5">luth</w>, <w n="2.6">exprimant</w> <w n="2.7">sa</w> <w n="2.8">douleur</w>,</l>
							<l n="3" num="1.3"><w n="3.1">De</w> <w n="3.2">son</w> <w n="3.3">retour</w> <w n="3.4">avertissait</w> <w n="3.5">la</w> <w n="3.6">rive</w></l>
							<l n="4" num="1.4"><w n="4.1">Où</w> <w n="4.2">la</w> <w n="4.3">rappelle</w> <w n="4.4">un</w> <w n="4.5">trône</w>… <w n="4.6">ou</w> <w n="4.7">le</w> <w n="4.8">malheur</w>.</l>
							<l n="5" num="1.5"><w n="5.1">Lorsque</w> <w n="5.2">sa</w> <w n="5.3">voix</w>, <w n="5.4">et</w> <w n="5.5">peut</w>-<w n="5.6">être</w> <w n="5.7">ses</w> <w n="5.8">larmes</w>,</l>
							<l n="6" num="1.6"><w n="6.1">Faisaient</w> <w n="6.2">pleurer</w> <w n="6.3">les</w> <w n="6.4">tristes</w> <w n="6.5">matelots</w>,</l>
							<l n="7" num="1.7"><w n="7.1">Elle</w> <w n="7.2">n</w>’<w n="7.3">oppose</w> <w n="7.4">à</w> <w n="7.5">de</w> <w n="7.6">perfides</w> <w n="7.7">armes</w></l>
							<l n="8" num="1.8"><w n="8.1">Que</w> <w n="8.2">ce</w> <w n="8.3">murmure</w> <w n="8.4">apporté</w> <w n="8.5">par</w> <w n="8.6">les</w> <w n="8.7">flots</w> :</l>
							<l rhyme="none" n="9" num="1.9"><space quantity="10" unit="char"></space><w n="9.1">God</w> <w n="9.2">save</w> <w n="9.3">the</w> <w n="9.4">King</w> !</l>
						</lg>
						<lg n="2">
							<l n="10" num="2.1"><w n="10.1">J</w>’<w n="10.2">avais</w> <w n="10.3">quitté</w> <w n="10.4">les</w> <w n="10.5">liens</w> <w n="10.6">de</w> <w n="10.7">l</w>’<w n="10.8">enfance</w>,</l>
							<l n="11" num="2.2"><w n="11.1">Pour</w> <w n="11.2">me</w> <w n="11.3">parer</w> <w n="11.4">des</w> <w n="11.5">chaînes</w> <w n="11.6">de</w> <w n="11.7">l</w>’<w n="11.8">amour</w>.</l>
							<l n="12" num="2.3"><w n="12.1">Aimer</w> <w n="12.2">son</w> <w n="12.3">maître</w> <w n="12.4">est</w> <w n="12.5">sans</w> <w n="12.6">doute</w> <w n="12.7">une</w> <w n="12.8">offense</w>,</l>
							<l n="13" num="2.4"><w n="13.1">Puisqu</w>’<w n="13.2">à</w> <w n="13.3">ma</w> <w n="13.4">vie</w> <w n="13.5">il</w> <w n="13.6">n</w>’<w n="13.7">a</w> <w n="13.8">souri</w> <w n="13.9">qu</w>’<w n="13.10">un</w> <w n="13.11">jour</w>.</l>
							<l n="14" num="2.5"><w n="14.1">Lorsque</w> <w n="14.2">des</w> <w n="14.3">pleurs</w> <w n="14.4">roulaient</w> <w n="14.5">sous</w> <w n="14.6">ma</w> <w n="14.7">paupière</w></l>
							<l n="15" num="2.6"><w n="15.1">Et</w> <w n="15.2">retombaient</w> <w n="15.3">lentement</w> <w n="15.4">sur</w> <w n="15.5">mon</w> <w n="15.6">cœur</w>,</l>
							<l n="16" num="2.7"><w n="16.1">Mon</w> <w n="16.2">cœur</w> <w n="16.3">tout</w> <w n="16.4">bas</w> <w n="16.5">mêlait</w> <w n="16.6">à</w> <w n="16.7">sa</w> <w n="16.8">prière</w></l>
							<l n="17" num="2.8"><w n="17.1">Cette</w> <w n="17.2">prière</w> <w n="17.3">encor</w> <w n="17.4">pour</w> <w n="17.5">mon</w> <w n="17.6">vainqueur</w> :</l>
							<l rhyme="none" n="18" num="2.9"><space quantity="10" unit="char"></space><w n="18.1">God</w> <w n="18.2">save</w> <w n="18.3">the</w> <w n="18.4">King</w> !</l>
						</lg>
						<lg n="3">
							<l n="19" num="3.1"><w n="19.1">Seule</w> <w n="19.2">souvent</w> <w n="19.3">au</w> <w n="19.4">berceau</w> <w n="19.5">de</w> <w n="19.6">sa</w> <w n="19.7">fille</w>,</l>
							<l n="20" num="3.2"><w n="20.1">Formant</w> <w n="20.2">des</w> <w n="20.3">vœux</w> <w n="20.4">qui</w> <w n="20.5">n</w>’<w n="20.6">étaient</w> <w n="20.7">plus</w> <w n="20.8">pour</w> <w n="20.9">moi</w>,</l>
							<l n="21" num="3.3"><w n="21.1">Je</w> <w n="21.2">lui</w> <w n="21.3">disais</w> : « <w n="21.4">À</w> <w n="21.5">ma</w> <w n="21.6">noble</w> <w n="21.7">famille</w></l>
							<l n="22" num="3.4"><w n="22.1">Mon</w> <w n="22.2">jeune</w> <w n="22.3">hymen</w> <w n="22.4">n</w>’<w n="22.5">offrira</w>-<w n="22.6">t</w>-<w n="22.7">il</w> <w n="22.8">que</w> <w n="22.9">toi</w> ? »</l>
							<l n="23" num="3.5"><w n="23.1">Cachant</w> <w n="23.2">alors</w> <w n="23.3">mes</w> <w n="23.4">pleurs</w> <w n="23.5">sous</w> <w n="23.6">ma</w> <w n="23.7">couronne</w>,</l>
							<l n="24" num="3.6"><w n="24.1">D</w>’<w n="24.2">un</w> <w n="24.3">chant</w> <w n="24.4">d</w>’<w n="24.5">amour</w> <w n="24.6">je</w> <w n="24.7">berçais</w> <w n="24.8">son</w> <w n="24.9">sommeil</w> ;</l>
							<l n="25" num="3.7"><w n="25.1">Et</w> <w n="25.2">de</w> <w n="25.3">ce</w> <w n="25.4">chant</w> <w n="25.5">dont</w> <w n="25.6">la</w> <w n="25.7">rive</w> <w n="25.8">résonne</w>,</l>
							<l n="26" num="3.8"><w n="26.1">Ma</w> <w n="26.2">voix</w> <w n="26.3">toujours</w> <w n="26.4">salua</w> <w n="26.5">son</w> <w n="26.6">réveil</w> :</l>
							<l rhyme="none" n="27" num="3.9"><space quantity="10" unit="char"></space><w n="27.1">God</w> <w n="27.2">save</w> <w n="27.3">the</w> <w n="27.4">King</w> !</l>
						</lg>
						<lg n="4">
							<l n="28" num="4.1"><w n="28.1">Sur</w> <w n="28.2">mon</w> <w n="28.3">front</w> <w n="28.4">triste</w>, <w n="28.5">abattu</w>, <w n="28.6">mais</w> <w n="28.7">sans</w> <w n="28.8">crainte</w>,</l>
							<l n="29" num="4.2"><w n="29.1">On</w> <w n="29.2">cherche</w> <w n="29.3">en</w> <w n="29.4">vain</w> <w n="29.5">la</w> <w n="29.6">trace</w> <w n="29.7">d</w>’<w n="29.8">un</w> <w n="29.9">remord</w> ;</l>
							<l n="30" num="4.3"><w n="30.1">Jamais</w> <w n="30.2">mon</w> <w n="30.3">front</w> <w n="30.4">n</w>’<w n="30.5">en</w> <w n="30.6">recevra</w> <w n="30.7">l</w>’<w n="30.8">empreinte</w>,</l>
							<l n="31" num="4.4"><w n="31.1">Et</w> <w n="31.2">je</w> <w n="31.3">la</w> <w n="31.4">laisse</w> <w n="31.5">à</w> <w n="31.6">qui</w> <w n="31.7">rêve</w> <w n="31.8">ma</w> <w n="31.9">mort</w>.</l>
							<l n="32" num="4.5"><w n="32.1">Qu</w>’<w n="32.2">au</w> <w n="32.3">moins</w> <w n="32.4">la</w> <w n="32.5">mort</w> <w n="32.6">m</w>’<w n="32.7">attende</w> <w n="32.8">à</w> <w n="32.9">ton</w> <w n="32.10">rivage</w>,</l>
							<l n="33" num="4.6"><w n="33.1">Ô</w> <w n="33.2">beau</w> <w n="33.3">pays</w> <w n="33.4">qui</w> <w n="33.5">vit</w> <w n="33.6">mes</w> <w n="33.7">plus</w> <w n="33.8">beaux</w> <w n="33.9">jours</w> !</l>
							<l n="34" num="4.7"><w n="34.1">En</w> <w n="34.2">d</w>’<w n="34.3">autres</w> <w n="34.4">jours</w> <w n="34.5">si</w> <w n="34.6">tu</w> <w n="34.7">vois</w> <w n="34.8">mon</w> <w n="34.9">naufrage</w>,</l>
							<l n="35" num="4.8"><w n="35.1">Dis</w> <w n="35.2">que</w> <w n="35.3">ta</w> <w n="35.4">reine</w> <w n="35.5">au</w> <w n="35.6">moins</w> <w n="35.7">chanta</w> <w n="35.8">toujours</w> :</l>
							<l rhyme="none" n="36" num="4.9"><space quantity="10" unit="char"></space><w n="36.1">God</w> <w n="36.2">save</w> <w n="36.3">the</w> <w n="36.4">King</w> !</l>
						</lg>
					</div></body></text></TEI>