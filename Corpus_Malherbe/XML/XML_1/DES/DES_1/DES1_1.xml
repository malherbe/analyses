<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><div type="poem" key="DES1">
					<head type="main">L’ARBRISSEAU</head>
					<head type="sub_1">À MONSIEUR ALIBERT</head>
					<lg n="1">
						<l n="1" num="1.1"><w n="1.1">La</w> <w n="1.2">tristesse</w> <w n="1.3">est</w> <w n="1.4">rêveuse</w>, <w n="1.5">et</w> <w n="1.6">je</w> <w n="1.7">rêve</w> <w n="1.8">souvent</w> ;</l>
						<l n="2" num="1.2"><w n="2.1">La</w> <w n="2.2">nature</w> <w n="2.3">m</w>’<w n="2.4">y</w> <w n="2.5">porte</w>, <w n="2.6">on</w> <w n="2.7">la</w> <w n="2.8">trompe</w> <w n="2.9">avec</w> <w n="2.10">peine</w> :</l>
						<l n="3" num="1.3"><space quantity="2" unit="char"></space><w n="3.1">Je</w> <w n="3.2">rêve</w> <w n="3.3">au</w> <w n="3.4">bruit</w> <w n="3.5">de</w> <w n="3.6">l</w>’<w n="3.7">eau</w> <w n="3.8">qui</w> <w n="3.9">se</w> <w n="3.10">promène</w>,</l>
						<l n="4" num="1.4"><w n="4.1">Au</w> <w n="4.2">murmure</w> <w n="4.3">du</w> <w n="4.4">saule</w> <w n="4.5">agité</w> <w n="4.6">par</w> <w n="4.7">le</w> <w n="4.8">vent</w>.</l>
						<l n="5" num="1.5"><w n="5.1">J</w>’<w n="5.2">écoute</w> : <w n="5.3">un</w> <w n="5.4">souvenir</w> <w n="5.5">répond</w> <w n="5.6">à</w> <w n="5.7">ma</w> <w n="5.8">tristesse</w> ;</l>
						<l n="6" num="1.6"><w n="6.1">Un</w> <w n="6.2">autre</w> <w n="6.3">souvenir</w> <w n="6.4">s</w>’<w n="6.5">éveille</w> <w n="6.6">dans</w> <w n="6.7">mon</w> <w n="6.8">cœur</w> :</l>
						<l n="7" num="1.7"><w n="7.1">Chaque</w> <w n="7.2">objet</w> <w n="7.3">me</w> <w n="7.4">pénètre</w>, <w n="7.5">et</w> <w n="7.6">répand</w> <w n="7.7">sa</w> <w n="7.8">couleur</w></l>
						<l n="8" num="1.8"><space quantity="6" unit="char"></space><w n="8.1">Sur</w> <w n="8.2">le</w> <w n="8.3">sentiment</w> <w n="8.4">qui</w> <w n="8.5">m</w>’<w n="8.6">oppresse</w>.</l>
						<l n="9" num="1.9"><space quantity="6" unit="char"></space><w n="9.1">Ainsi</w> <w n="9.2">le</w> <w n="9.3">nuage</w> <w n="9.4">s</w>’<w n="9.5">enfuit</w>,</l>
						<l n="10" num="1.10"><space quantity="6" unit="char"></space><w n="10.1">Pressé</w> <w n="10.2">par</w> <w n="10.3">un</w> <w n="10.4">autre</w> <w n="10.5">nuage</w> ;</l>
						<l n="11" num="1.11"><space quantity="6" unit="char"></space><w n="11.1">Ainsi</w> <w n="11.2">le</w> <w n="11.3">flot</w> <w n="11.4">fuit</w> <w n="11.5">le</w> <w n="11.6">rivage</w>,</l>
						<l n="12" num="1.12"><space quantity="6" unit="char"></space><w n="12.1">Cédant</w> <w n="12.2">au</w> <w n="12.3">flot</w> <w n="12.4">qui</w> <w n="12.5">le</w> <w n="12.6">poursuit</w>.</l>
					</lg>
					<lg n="2">
						<l n="13" num="2.1"><space quantity="2" unit="char"></space><w n="13.1">J</w>’<w n="13.2">ai</w> <w n="13.3">vu</w> <w n="13.4">languir</w>, <w n="13.5">au</w> <w n="13.6">fond</w> <w n="13.7">de</w> <w n="13.8">la</w> <w n="13.9">vallée</w>,</l>
						<l n="14" num="2.2"><space quantity="2" unit="char"></space><w n="14.1">Un</w> <w n="14.2">arbrisseau</w> <w n="14.3">qu</w>’<w n="14.4">oubliait</w> <w n="14.5">le</w> <w n="14.6">bonheur</w> ;</l>
						<l n="15" num="2.3"><w n="15.1">L</w>’<w n="15.2">aurore</w> <w n="15.3">se</w> <w n="15.4">levait</w> <w n="15.5">sans</w> <w n="15.6">éclairer</w> <w n="15.7">sa</w> <w n="15.8">fleur</w>,</l>
						<l n="16" num="2.4"><w n="16.1">Et</w> <w n="16.2">pour</w> <w n="16.3">lui</w> <w n="16.4">la</w> <w n="16.5">nature</w> <w n="16.6">était</w> <w n="16.7">sombre</w> <w n="16.8">et</w> <w n="16.9">voilée</w>.</l>
						<l n="17" num="2.5"><w n="17.1">Ses</w> <w n="17.2">printemps</w> <w n="17.3">ignorés</w> <w n="17.4">s</w>’<w n="17.5">écoulaient</w> <w n="17.6">dans</w> <w n="17.7">la</w> <w n="17.8">nuit</w> ;</l>
						<l n="18" num="2.6"><space quantity="2" unit="char"></space><w n="18.1">L</w>’<w n="18.2">amour</w> <w n="18.3">jamais</w> <w n="18.4">d</w>’<w n="18.5">une</w> <w n="18.6">fraîche</w> <w n="18.7">guirlande</w></l>
						<l n="19" num="2.7"><space quantity="2" unit="char"></space><w n="19.1">À</w> <w n="19.2">ses</w> <w n="19.3">rameaux</w> <w n="19.4">n</w>’<w n="19.5">avait</w> <w n="19.6">laissé</w> <w n="19.7">l</w>’<w n="19.8">offrande</w> :</l>
						<l n="20" num="2.8"><space quantity="6" unit="char"></space><w n="20.1">Il</w> <w n="20.2">fait</w> <w n="20.3">froid</w> <w n="20.4">aux</w> <w n="20.5">lieux</w> <w n="20.6">qu</w>’<w n="20.7">Amour</w> <w n="20.8">fuit</w>.</l>
						<l n="21" num="2.9"><w n="21.1">L</w>’<w n="21.2">ombre</w> <w n="21.3">humide</w> <w n="21.4">éteignait</w> <w n="21.5">sa</w> <w n="21.6">force</w> <w n="21.7">languissante</w> ;</l>
						<l n="22" num="2.10"><w n="22.1">Son</w> <w n="22.2">front</w> <w n="22.3">pour</w> <w n="22.4">s</w>’<w n="22.5">élever</w> <w n="22.6">faisait</w> <w n="22.7">un</w> <w n="22.8">vain</w> <w n="22.9">effort</w> ;</l>
						<l n="23" num="2.11"><w n="23.1">Un</w> <w n="23.2">éternel</w> <w n="23.3">hiver</w>, <w n="23.4">une</w> <w n="23.5">eau</w> <w n="23.6">triste</w> <w n="23.7">et</w> <w n="23.8">dormante</w></l>
						<l n="24" num="2.12"><w n="24.1">Jusque</w> <w n="24.2">dans</w> <w n="24.3">sa</w> <w n="24.4">racine</w> <w n="24.5">allait</w> <w n="24.6">porter</w> <w n="24.7">la</w> <w n="24.8">mort</w>.</l>
					</lg>
					<lg n="3">
						<l n="25" num="3.1">« <w n="25.1">Hélas</w> ! <w n="25.2">faut</w>-<w n="25.3">il</w> <w n="25.4">mourir</w> <w n="25.5">sans</w> <w n="25.6">connaître</w> <w n="25.7">la</w> <w n="25.8">vie</w> !</l>
						<l n="26" num="3.2">« <w n="26.1">Sans</w> <w n="26.2">avoir</w> <w n="26.3">vu</w> <w n="26.4">des</w> <w n="26.5">cieux</w> <w n="26.6">briller</w> <w n="26.7">les</w> <w n="26.8">doux</w> <w n="26.9">flambeaux</w> !</l>
						<l n="27" num="3.3">« <w n="27.1">Je</w> <w n="27.2">n</w>’<w n="27.3">atteindrai</w> <w n="27.4">jamais</w> <w n="27.5">de</w> <w n="27.6">ces</w> <w n="27.7">arbres</w> <w n="27.8">si</w> <w n="27.9">beaux</w></l>
						<l n="28" num="3.4"><space quantity="6" unit="char"></space>« <w n="28.1">La</w> <w n="28.2">couronne</w> <w n="28.3">verte</w> <w n="28.4">et</w> <w n="28.5">fleurie</w> !</l>
						<l n="29" num="3.5">« <w n="29.1">Ils</w> <w n="29.2">dominent</w> <w n="29.3">au</w> <w n="29.4">loin</w> <w n="29.5">sur</w> <w n="29.6">les</w> <w n="29.7">champs</w> <w n="29.8">d</w>’<w n="29.9">alentour</w> :</l>
						<l n="30" num="3.6">« <w n="30.1">On</w> <w n="30.2">dit</w> <w n="30.3">que</w> <w n="30.4">le</w> <w n="30.5">soleil</w> <w n="30.6">dore</w> <w n="30.7">leur</w> <w n="30.8">beau</w> <w n="30.9">feuillage</w> ;</l>
						<l n="31" num="3.7"><space quantity="2" unit="char"></space>« <w n="31.1">Et</w> <w n="31.2">moi</w>, <w n="31.3">sous</w> <w n="31.4">leur</w> <w n="31.5">impénétrable</w> <w n="31.6">ombrage</w>,</l>
						<l n="32" num="3.8"><space quantity="6" unit="char"></space>«<w n="32.1">Je</w> <w n="32.2">devine</w> <w n="32.3">à</w> <w n="32.4">peine</w> <w n="32.5">le</w> <w n="32.6">jour</w> !</l>
						<l n="33" num="3.9">« <w n="33.1">Vallon</w> <w n="33.2">où</w> <w n="33.3">je</w> <w n="33.4">me</w> <w n="33.5">meurs</w>, <w n="33.6">votre</w> <w n="33.7">triste</w> <w n="33.8">influence</w></l>
						<l n="34" num="3.10">« <w n="34.1">À</w> <w n="34.2">préparé</w> <w n="34.3">ma</w> <w n="34.4">chute</w> <w n="34.5">auprès</w> <w n="34.6">de</w> <w n="34.7">ma</w> <w n="34.8">naissance</w>.</l>
						<l n="35" num="3.11"><space quantity="2" unit="char"></space>« <w n="35.1">Bientôt</w>, <w n="35.2">hélas</w> ! <w n="35.3">je</w> <w n="35.4">ne</w> <w n="35.5">dois</w> <w n="35.6">plus</w> <w n="35.7">gémir</w> !</l>
						<l n="36" num="3.12"><space quantity="2" unit="char"></space>« <w n="36.1">Déjà</w> <w n="36.2">ma</w> <w n="36.3">feuille</w> <w n="36.4">a</w> <w n="36.5">cessé</w> <w n="36.6">de</w> <w n="36.7">frémir</w>…</l>
						<l n="37" num="3.13"><space quantity="2" unit="char"></space>«<w n="37.1">Je</w> <w n="37.2">meurs</w>, <w n="37.3">je</w> <w n="37.4">meurs</w> ! » <w n="37.5">Ce</w> <w n="37.6">douloureux</w> <w n="37.7">murmure</w></l>
						<l n="38" num="3.14"><space quantity="2" unit="char"></space><w n="38.1">Toucha</w> <w n="38.2">le</w> <w n="38.3">dieu</w> <w n="38.4">protecteur</w> <w n="38.5">du</w> <w n="38.6">vallon</w>.</l>
						<l n="39" num="3.15"><space quantity="2" unit="char"></space><w n="39.1">C</w>’<w n="39.2">était</w> <w n="39.3">le</w> <w n="39.4">temps</w> <w n="39.5">où</w> <w n="39.6">le</w> <w n="39.7">noir</w> <w n="39.8">Aquilon</w></l>
						<l n="40" num="3.16"><space quantity="2" unit="char"></space><w n="40.1">Laisse</w>, <w n="40.2">en</w> <w n="40.3">fuyant</w>, <w n="40.4">respirer</w> <w n="40.5">la</w> <w n="40.6">nature</w>.</l>
						<l n="41" num="3.17"><space quantity="2" unit="char"></space>« <w n="41.1">Non</w> ! <w n="41.2">dit</w> <w n="41.3">le</w> <w n="41.4">dieu</w> ; <w n="41.5">qu</w>’<w n="41.6">un</w> <w n="41.7">souffle</w> <w n="41.8">de</w> <w n="41.9">chaleur</w></l>
						<l n="42" num="3.18"><space quantity="2" unit="char"></space>« <w n="42.1">Pénètre</w> <w n="42.2">au</w> <w n="42.3">sein</w> <w n="42.4">de</w> <w n="42.5">ta</w> <w n="42.6">tige</w> <w n="42.7">glacée</w> !</l>
						<l n="43" num="3.19"><space quantity="2" unit="char"></space>« <w n="43.1">Ta</w> <w n="43.2">vie</w> <w n="43.3">heureuse</w> <w n="43.4">est</w> <w n="43.5">enfin</w> <w n="43.6">commencée</w> ;</l>
						<l n="44" num="3.20"><space quantity="2" unit="char"></space>« <w n="44.1">Relève</w>-<w n="44.2">toi</w>, <w n="44.3">j</w>’<w n="44.4">ai</w> <w n="44.5">ranimé</w> <w n="44.6">ta</w> <w n="44.7">fleur</w>.</l>
						<l n="45" num="3.21"><space quantity="2" unit="char"></space>« <w n="45.1">Je</w> <w n="45.2">te</w> <w n="45.3">consacre</w> <w n="45.4">aux</w> <w n="45.5">nymphes</w> <w n="45.6">des</w> <w n="45.7">bocages</w> ;</l>
						<l n="46" num="3.22"><space quantity="2" unit="char"></space>« <w n="46.1">À</w> <w n="46.2">mes</w> <w n="46.3">lauriers</w> <w n="46.4">tes</w> <w n="46.5">rameaux</w> <w n="46.6">vont</w> <w n="46.7">s</w>’<w n="46.8">unir</w>,</l>
						<l n="47" num="3.23">« <w n="47.1">Et</w> <w n="47.2">j</w>’<w n="47.3">irai</w> <w n="47.4">quelque</w> <w n="47.5">jour</w> <w n="47.6">sous</w> <w n="47.7">leurs</w> <w n="47.8">jeunes</w> <w n="47.9">ombrages</w></l>
						<l n="48" num="3.24"><space quantity="10" unit="char"></space>« <w n="48.1">Chercher</w> <w n="48.2">un</w> <w n="48.3">souvenir</w>. »</l>
					</lg>
					<lg n="4">
						<l n="49" num="4.1"><w n="49.1">L</w>’<w n="49.2">arbrisseau</w>, <w n="49.3">faible</w> <w n="49.4">encor</w>, <w n="49.5">tressaillit</w> <w n="49.6">d</w>’<w n="49.7">espérance</w> ;</l>
						<l n="50" num="4.2"><w n="50.1">Dans</w> <w n="50.2">le</w> <w n="50.3">pressentiment</w> <w n="50.4">il</w> <w n="50.5">goûta</w> <w n="50.6">l</w>’<w n="50.7">existence</w> ;</l>
						<l n="51" num="4.3"><w n="51.1">Comme</w> <w n="51.2">l</w>’<w n="51.3">aveugle</w>-<w n="51.4">né</w>, <w n="51.5">saisi</w> <w n="51.6">d</w>’<w n="51.7">un</w> <w n="51.8">doux</w> <w n="51.9">transport</w>,</l>
						<l n="52" num="4.4"><w n="52.1">Voit</w> <w n="52.2">fuir</w> <w n="52.3">sa</w> <w n="52.4">longue</w> <w n="52.5">nuit</w>, <w n="52.6">image</w> <w n="52.7">de</w> <w n="52.8">la</w> <w n="52.9">mort</w>,</l>
						<l n="53" num="4.5"><w n="53.1">Quand</w> <w n="53.2">une</w> <w n="53.3">main</w> <w n="53.4">divine</w> <w n="53.5">entr</w>’<w n="53.6">ouvre</w> <w n="53.7">sa</w> <w n="53.8">paupière</w>,</l>
						<l n="54" num="4.6"><w n="54.1">Et</w> <w n="54.2">conduit</w> <w n="54.3">à</w> <w n="54.4">son</w> <w n="54.5">âme</w> <w n="54.6">un</w> <w n="54.7">rayon</w> <w n="54.8">de</w> <w n="54.9">lumière</w> :</l>
						<l n="55" num="4.7"><w n="55.1">L</w>’<w n="55.2">air</w> <w n="55.3">qu</w>’<w n="55.4">il</w> <w n="55.5">respire</w> <w n="55.6">alors</w> <w n="55.7">est</w> <w n="55.8">un</w> <w n="55.9">bienfait</w> <w n="55.10">nouveau</w> ;</l>
						<l n="56" num="4.8"><space quantity="2" unit="char"></space><w n="56.1">Il</w> <w n="56.2">est</w> <w n="56.3">plus</w> <w n="56.4">pur</w> : <w n="56.5">il</w> <w n="56.6">vient</w> <w n="56.7">d</w>’<w n="56.8">un</w> <w n="56.9">ciel</w> <w n="56.10">si</w> <w n="56.11">beau</w> !</l>
					</lg>
				</div></body></text></TEI>