<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:lang="fra"><teiHeader xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
		<fileDesc>
			<titleStmt>
				<title type="main">POÉSIES</title>
				<title type="medium">Édition électronique</title>
				<author key="DES">
					<name>
						<forename>Marceline</forename>
						<surname>DESBORDES-VALMORE</surname>
					</name>
					<date from="1786" to="1859">1786-1859</date>
				</author>
				<editor>
					Équipe du projet de recherche en traitement automatique des textes versifiés (corpus Malherbe)
					<choice>
						<abbr>CRISCO, Université de Caen Normandie</abbr>
						<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
					</choice>
					(EA 4255)
				</editor>
				<respStmt>
					<resp>Numérisation, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="RR">
						<forename>Richard</forename>
						<surname>Renault</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>OCR, encodage XML (première version)</resp>
					<name id="RS">
						<forename>Robin</forename>
						<surname>Seguy</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Encodage XML, application des programmes de traitement automatique, vérification et correction des données analysées</resp>
					<name id="CA">
						<forename>Coline</forename>
						<surname>Auvray</surname>
					</name>
				</respStmt>
				<respStmt>
					<resp>Vérification des données analysées</resp>
					<name id="ED">
						<forename>Éliane</forename>
						<surname>Delente</surname>
					</name>
				</respStmt>
			</titleStmt>
			<extent>9618 vers</extent>
			<publicationStmt>
				<publisher>
					<orgname>
						<choice>
							<abbr>CRISCO, Université de Caen Normandie</abbr>
							<expan>Centre de Recherche Inter-langues sur la signification en contexte</expan>
						</choice>
					</orgname>
					<address>
						<addrLine>Université de Caen Normandie</addrLine>
						<addrLine>14032 CAEN CEDEX</addrLine>
						<addrLine>FRANCE</addrLine>
					</address>
					<email>crisco.incipit@unicaen.fr</email>
					<ref type="URL">http://www.crisco.unicaen.fr/verlaine/</ref>
				</publisher>
				<pubPlace>Caen</pubPlace>
				<date when="2014">2014</date>
				<idno type="local">DES_1</idno>
				<availability status="free">
					<licence target="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">Licence Creative Commons : CC-BY-NC-SA</licence>
					<p>
						Ce document est distribué sous licence libre Creative Commons CC-BY-NC-SA :
						CC = Licence Creative Commons
						BY = Attribution : Inclure les informations sur la licence, les créateurs et contributeurs,
							 sur les sources et sur les modifications introduites.
						NC = Pas d’utilisation commerciale.
						SA = Partage dans les mêmes conditions.
					</p>
				</availability>
			</publicationStmt>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies complètes</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>Publiées par Bernard Guégan avec des notes et des variantes</editor>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>Éditions du Trianon</publisher>
						<date when="1931">1931</date>
					</imprint>
					<biblScope unit="tome">1 et 2</biblScope>
					</monogr>
					<note>Édition numérisée</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Œuvres poétiques</title>
					<author>Marceline Desbordes-Valmore</author>
					<editor>édition complète établie et commentée par Marc Bertrand</editor>
					<imprint>
						<pubPlace>Grenoble</pubPlace>
						<publisher>Presses Universitaires de Grenoble</publisher>
						<date when="1973">1973</date>
					</imprint>
					<extent>9618 vers2 vols.</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
			<sourceDesc>
				<biblStruct>
					<monogr>
					<title>Poésies</title>
					<author>Marceline Desbordes-Valmore</author>
					<imprint>
						<pubPlace>Paris</pubPlace>
						<publisher>A. Boulland, libraire-éditeur</publisher>
						<date when="1830">1830</date>
					</imprint>
					<extent>9618 verstome second</extent>
					</monogr>
					<note>Édition de référence pour les corrections métriques</note>
				</biblStruct>
			</sourceDesc>
		</fileDesc>
		<profileDesc>
			<creation>
				<date when="1830">1830</date>
			</creation>
		</profileDesc>
		<encodingDesc>
			<projectDesc>
				<p>
					Cette édition électronique a été préparée pour le corpus Malherbe ;
					corpus de textes versifiés pour le traitement automatique des formes et structures métriques.
				</p>
			</projectDesc>
			<samplingDecl>
				<p>Le recueil POÉSIES, qui se trouve à cheval sur les deux premiers tomes de l’édition de Guégan, est ici restitué dans son unité.</p>
				<p>Les notes de l’éditeur n’ont pas été encodées.</p>
			</samplingDecl>
			<editorialDecl>
				<p>L’insertion des balises XML, délimitant les différentes unités du texte a été faite de manière semi-automatique.</p>
				<p>Les retraits ont été adaptés aux différentes longueurs métriques (procédure automatique après application du programme de calcul de la longueur métrique des vers).</p>
				<correction>
					<p>Les erreurs métriques ont été corrigées en se fondant sur l’édition de Marc Bertrand de 1973.</p>
					<p>L’orthographe du texte a été vérifiée avec le correcteur GNU Aspell.</p>
				</correction>
				<normalization>
					<p>Les majuscules accentuées ont été restituées.</p>
					<p>Les lettres ligaturées (œ et æ) ont été restituées.</p>
					<p>La ponctuation a été normalisée (espace devant un signe de ponctuation double).</p>
				</normalization>
			</editorialDecl>
		</encodingDesc>
		<revisionDesc>
			<listChange>
				<change when="2016-12-20" who="RR">Insertion des corrections métriques dans le corps du texte.</change>
				<change when="2017-02-28" who="RR">Révision de l’entête et de l’encodage du texte pour la validation XML-TEI (TEI_corpus_Malherbe.xsd).</change>
				<change when="2018-11-02" who="RR">Correction dans le poème "Dors, ma Mère".</change>
			</listChange>
		</revisionDesc>
	</teiHeader><text><body><head type="main_part">POÉSIES DIVERSES</head><div type="poem" key="DES140">
						<head type="main">L’ÉCOLIER</head>
						<lg n="1">
							<l n="1" num="1.1"><w n="1.1">Un</w> <w n="1.2">tout</w> <w n="1.3">petit</w> <w n="1.4">enfant</w> <w n="1.5">s</w>’<w n="1.6">en</w> <w n="1.7">allait</w> <w n="1.8">à</w> <w n="1.9">l</w>’<w n="1.10">école</w>.</l>
							<l n="2" num="1.2"><w n="2.1">On</w> <w n="2.2">avait</w> <w n="2.3">dit</w> : « <w n="2.4">Allez</w> !… » <w n="2.5">Il</w> <w n="2.6">tâchait</w> <w n="2.7">d</w>’<w n="2.8">obéir</w> ;</l>
							<l n="3" num="1.3"><w n="3.1">Mais</w> <w n="3.2">son</w> <w n="3.3">livre</w> <w n="3.4">était</w> <w n="3.5">lourd</w>, <w n="3.6">il</w> <w n="3.7">ne</w> <w n="3.8">pouvait</w> <w n="3.9">courir</w>.</l>
							<l n="4" num="1.4"><w n="4.1">Il</w> <w n="4.2">pleure</w>, <w n="4.3">et</w> <w n="4.4">suit</w> <w n="4.5">des</w> <w n="4.6">yeux</w> <w n="4.7">une</w> <w n="4.8">Abeille</w> <w n="4.9">qui</w> <w n="4.10">vole</w>.</l>
						</lg>
						<lg n="2">
							<l n="5" num="2.1">« <w n="5.1">Abeille</w>, <w n="5.2">lui</w> <w n="5.3">dit</w>-<w n="5.4">il</w>, <w n="5.5">voulez</w>-<w n="5.6">vous</w> <w n="5.7">me</w> <w n="5.8">parler</w> ?</l>
							<l n="6" num="2.2"><w n="6.1">Moi</w>, <w n="6.2">je</w> <w n="6.3">vais</w> <w n="6.4">à</w> <w n="6.5">l</w>’<w n="6.6">école</w> : <w n="6.7">il</w> <w n="6.8">faut</w> <w n="6.9">apprendre</w> <w n="6.10">à</w> <w n="6.11">lire</w> ;</l>
							<l n="7" num="2.3"><w n="7.1">Mais</w> <w n="7.2">le</w> <w n="7.3">maître</w> <w n="7.4">est</w> <w n="7.5">tout</w> <w n="7.6">noir</w>, <w n="7.7">et</w> <w n="7.8">je</w> <w n="7.9">n</w>’<w n="7.10">ose</w> <w n="7.11">pas</w> <w n="7.12">rire</w> :</l>
							<l n="8" num="2.4"><w n="8.1">Voulez</w>-<w n="8.2">vous</w> <w n="8.3">rire</w>, <w n="8.4">abeille</w>, <w n="8.5">et</w> <w n="8.6">m</w>’<w n="8.7">apprendre</w> <w n="8.8">à</w> <w n="8.9">voler</w> ?</l>
							<l n="9" num="2.5">— <w n="9.1">Non</w>, <w n="9.2">dit</w>-<w n="9.3">elle</w> ; <w n="9.4">j</w>’<w n="9.5">arrive</w> <w n="9.6">et</w> <w n="9.7">je</w> <w n="9.8">suis</w> <w n="9.9">très</w> <w n="9.10">pressée</w>.</l>
							<l n="10" num="2.6"><w n="10.1">J</w>’<w n="10.2">avais</w> <w n="10.3">froid</w> ; <w n="10.4">l</w>’<w n="10.5">Aquilon</w> <w n="10.6">m</w>’<w n="10.7">a</w> <w n="10.8">longtemps</w> <w n="10.9">oppressée</w> :</l>
							<l n="11" num="2.7"><w n="11.1">Enfin</w>, <w n="11.2">j</w>’<w n="11.3">ai</w> <w n="11.4">vu</w> <w n="11.5">les</w> <w n="11.6">fleurs</w>, <w n="11.7">je</w> <w n="11.8">redescends</w> <w n="11.9">du</w> <w n="11.10">ciel</w>,</l>
							<l n="12" num="2.8"><w n="12.1">Et</w> <w n="12.2">je</w> <w n="12.3">vais</w> <w n="12.4">commencer</w> <w n="12.5">mon</w> <w n="12.6">doux</w> <w n="12.7">rayon</w> <w n="12.8">de</w> <w n="12.9">miel</w>.</l>
							<l n="13" num="2.9"><w n="13.1">Voyez</w> ! <w n="13.2">j</w>’<w n="13.3">en</w> <w n="13.4">ai</w> <w n="13.5">déjà</w> <w n="13.6">puisé</w> <w n="13.7">dans</w> <w n="13.8">quatre</w> <w n="13.9">roses</w> ;</l>
							<l n="14" num="2.10"><w n="14.1">Avant</w> <w n="14.2">une</w> <w n="14.3">heure</w> <w n="14.4">encor</w> <w n="14.5">nous</w> <w n="14.6">en</w> <w n="14.7">aurons</w> <w n="14.8">d</w>’<w n="14.9">écloses</w>.</l>
							<l n="15" num="2.11"><w n="15.1">Vite</w>, <w n="15.2">vite</w> <w n="15.3">à</w> <w n="15.4">la</w> <w n="15.5">ruche</w> ! <w n="15.6">on</w> <w n="15.7">ne</w> <w n="15.8">rit</w> <w n="15.9">pas</w> <w n="15.10">toujours</w> :</l>
							<l n="16" num="2.12"><w n="16.1">C</w>’<w n="16.2">est</w> <w n="16.3">pour</w> <w n="16.4">faire</w> <w n="16.5">le</w> <w n="16.6">miel</w> <w n="16.7">qu</w>’<w n="16.8">on</w> <w n="16.9">nous</w> <w n="16.10">rend</w> <w n="16.11">les</w> <w n="16.12">beaux</w> <w n="16.13">jours</w>. »</l>
						</lg>
						<lg n="3">
							<l n="17" num="3.1"><w n="17.1">Elle</w> <w n="17.2">fuit</w> <w n="17.3">et</w> <w n="17.4">se</w> <w n="17.5">perd</w> <w n="17.6">sur</w> <w n="17.7">la</w> <w n="17.8">route</w> <w n="17.9">embaumée</w>.</l>
							<l n="18" num="3.2"><w n="18.1">Le</w> <w n="18.2">frais</w> <w n="18.3">lilas</w> <w n="18.4">sortait</w> <w n="18.5">d</w>’<w n="18.6">un</w> <w n="18.7">vieux</w> <w n="18.8">mur</w> <w n="18.9">entr</w>’<w n="18.10">ouvert</w> ;</l>
							<l n="19" num="3.3"><w n="19.1">Il</w> <w n="19.2">saluait</w> <w n="19.3">l</w>’<w n="19.4">aurore</w>, <w n="19.5">et</w> <w n="19.6">l</w>’<w n="19.7">aurore</w> <w n="19.8">charmée</w></l>
							<l n="20" num="3.4"><w n="20.1">Se</w> <w n="20.2">montrait</w> <w n="20.3">sans</w> <w n="20.4">nuage</w> <w n="20.5">et</w> <w n="20.6">riait</w> <w n="20.7">de</w> <w n="20.8">l</w>’<w n="20.9">hiver</w>.</l>
						</lg>
						<lg n="4">
							<l n="21" num="4.1"><w n="21.1">Une</w> <w n="21.2">Hirondelle</w> <w n="21.3">passe</w> : <w n="21.4">elle</w> <w n="21.5">effleure</w> <w n="21.6">la</w> <w n="21.7">joue</w></l>
							<l n="22" num="4.2"><w n="22.1">Du</w> <w n="22.2">petit</w> <w n="22.3">nonchalant</w> <w n="22.4">qui</w> <w n="22.5">s</w>’<w n="22.6">attriste</w> <w n="22.7">et</w> <w n="22.8">qui</w> <w n="22.9">joue</w> ;</l>
							<l n="23" num="4.3"><w n="23.1">Et</w> <w n="23.2">dans</w> <w n="23.3">l</w>’<w n="23.4">air</w> <w n="23.5">suspendue</w>, <w n="23.6">en</w> <w n="23.7">redoublant</w> <w n="23.8">sa</w> <w n="23.9">voix</w>,</l>
							<l n="24" num="4.4"><w n="24.1">Fait</w> <w n="24.2">tressaillir</w> <w n="24.3">l</w>’<w n="24.4">écho</w> <w n="24.5">qui</w> <w n="24.6">dort</w> <w n="24.7">au</w> <w n="24.8">fond</w> <w n="24.9">des</w> <w n="24.10">bois</w>.</l>
						</lg>
						<lg n="5">
							<l n="25" num="5.1">« <w n="25.1">Oh</w> ! <w n="25.2">bonjour</w> ! <w n="25.3">dit</w> <w n="25.4">l</w>’<w n="25.5">enfant</w>, <w n="25.6">qui</w> <w n="25.7">se</w> <w n="25.8">souvenait</w> <w n="25.9">d</w>’<w n="25.10">elle</w> ;</l>
							<l n="26" num="5.2"><w n="26.1">Je</w> <w n="26.2">t</w>’<w n="26.3">ai</w> <w n="26.4">vue</w> <w n="26.5">à</w> <w n="26.6">l</w>’<w n="26.7">automne</w>. <w n="26.8">Oh</w> ! <w n="26.9">bonjour</w>, <w n="26.10">hirondelle</w> !</l>
							<l n="27" num="5.3"><w n="27.1">Viens</w> ! <w n="27.2">tu</w> <w n="27.3">portais</w> <w n="27.4">bonheur</w> <w n="27.5">à</w> <w n="27.6">ma</w> <w n="27.7">maison</w>, <w n="27.8">et</w> <w n="27.9">moi</w></l>
							<l n="28" num="5.4"><w n="28.1">Je</w> <w n="28.2">voudrais</w> <w n="28.3">du</w> <w n="28.4">bonheur</w>. <w n="28.5">Veux</w>-<w n="28.6">tu</w> <w n="28.7">m</w>’<w n="28.8">en</w> <w n="28.9">donner</w>, <w n="28.10">toi</w> ?</l>
							<l n="29" num="5.5"><w n="29.1">Jouons</w>. — <w n="29.2">Je</w> <w n="29.3">le</w> <w n="29.4">voudrais</w>, <w n="29.5">répond</w> <w n="29.6">la</w> <w n="29.7">voyageuse</w>,</l>
							<l n="30" num="5.6"><w n="30.1">Car</w> <w n="30.2">je</w> <w n="30.3">respire</w> <w n="30.4">à</w> <w n="30.5">peine</w>, <w n="30.6">et</w> <w n="30.7">je</w> <w n="30.8">me</w> <w n="30.9">sens</w> <w n="30.10">joyeuse</w>.</l>
							<l n="31" num="5.7"><w n="31.1">Mais</w> <w n="31.2">j</w>’<w n="31.3">ai</w> <w n="31.4">beaucoup</w> <w n="31.5">d</w>’<w n="31.6">amis</w> <w n="31.7">qui</w> <w n="31.8">doutent</w> <w n="31.9">du</w> <w n="31.10">printemps</w> ;</l>
							<l n="32" num="5.8"><w n="32.1">Ils</w> <w n="32.2">rêveraient</w> <w n="32.3">ma</w> <w n="32.4">mort</w> <w n="32.5">si</w> <w n="32.6">je</w> <w n="32.7">tardais</w> <w n="32.8">longtemps</w>.</l>
							<l n="33" num="5.9"><w n="33.1">Non</w>, <w n="33.2">je</w> <w n="33.3">ne</w> <w n="33.4">puis</w> <w n="33.5">jouer</w>. <w n="33.6">Pour</w> <w n="33.7">finir</w> <w n="33.8">leur</w> <w n="33.9">souffrance</w>,</l>
							<l n="34" num="5.10"><w n="34.1">J</w>’<w n="34.2">emporte</w> <w n="34.3">un</w> <w n="34.4">brin</w> <w n="34.5">de</w> <w n="34.6">mousse</w> <w n="34.7">en</w> <w n="34.8">signe</w> <w n="34.9">d</w>’<w n="34.10">espérance</w>.</l>
							<l n="35" num="5.11"><w n="35.1">Nous</w> <w n="35.2">allons</w> <w n="35.3">relever</w> <w n="35.4">nos</w> <w n="35.5">palais</w> <w n="35.6">dégarnis</w> ;</l>
							<l n="36" num="5.12"><w n="36.1">L</w>’<w n="36.2">herbe</w> <w n="36.3">croit</w>, <w n="36.4">c</w>’<w n="36.5">est</w> <w n="36.6">l</w>’<w n="36.7">instant</w> <w n="36.8">des</w> <w n="36.9">amours</w> <w n="36.10">et</w> <w n="36.11">des</w> <w n="36.12">nids</w>.</l>
							<l n="37" num="5.13"><w n="37.1">J</w>’<w n="37.2">ai</w> <w n="37.3">tout</w> <w n="37.4">vu</w>. <w n="37.5">Maintenant</w>, <w n="37.6">fidèle</w> <w n="37.7">messagère</w>,</l>
							<l n="38" num="5.14"><w n="38.1">Je</w> <w n="38.2">vais</w> <w n="38.3">chercher</w> <w n="38.4">mes</w> <w n="38.5">sœurs</w>, <w n="38.6">là</w>-<w n="38.7">bas</w> <w n="38.8">sur</w> <w n="38.9">le</w> <w n="38.10">chemin</w>.</l>
							<l n="39" num="5.15"><w n="39.1">Ainsi</w> <w n="39.2">que</w> <w n="39.3">nous</w>, <w n="39.4">enfant</w>, <w n="39.5">la</w> <w n="39.6">vie</w> <w n="39.7">est</w> <w n="39.8">passagère</w>,</l>
							<l n="40" num="5.16"><w n="40.1">Il</w> <w n="40.2">en</w> <w n="40.3">faut</w> <w n="40.4">profiter</w>. <w n="40.5">Je</w> <w n="40.6">me</w> <w n="40.7">sauve</w>… <w n="40.8">À</w> <w n="40.9">demain</w> ! »</l>
							<l n="41" num="5.17"><w n="41.1">L</w>’<w n="41.2">enfant</w> <w n="41.3">reste</w> <w n="41.4">muet</w> ; <w n="41.5">et</w>, <w n="41.6">la</w> <w n="41.7">tête</w> <w n="41.8">baissée</w>,</l>
							<l n="42" num="5.18"><w n="42.1">Rêve</w> <w n="42.2">et</w> <w n="42.3">compte</w> <w n="42.4">ses</w> <w n="42.5">pas</w> <w n="42.6">pour</w> <w n="42.7">tromper</w> <w n="42.8">son</w> <w n="42.9">ennui</w>,</l>
							<l n="43" num="5.19"><w n="43.1">Quand</w> <w n="43.2">le</w> <w n="43.3">livre</w> <w n="43.4">importun</w>, <w n="43.5">dont</w> <w n="43.6">sa</w> <w n="43.7">main</w> <w n="43.8">est</w> <w n="43.9">lassée</w>,</l>
							<l n="44" num="5.20"><w n="44.1">Rompt</w> <w n="44.2">ses</w> <w n="44.3">fragiles</w> <w n="44.4">nœuds</w>, <w n="44.5">et</w> <w n="44.6">tombe</w> <w n="44.7">auprès</w> <w n="44.8">de</w> <w n="44.9">lui</w>.</l>
						</lg>
						<lg n="6">
							<l n="45" num="6.1"><w n="45.1">Un</w> <w n="45.2">dogue</w> <w n="45.3">l</w>’<w n="45.4">observait</w> <w n="45.5">du</w> <w n="45.6">seuil</w> <w n="45.7">de</w> <w n="45.8">sa</w> <w n="45.9">demeure</w> ;</l>
							<l n="46" num="6.2"><w n="46.1">Stentor</w>, <w n="46.2">gardien</w> <w n="46.3">sévère</w> <w n="46.4">et</w> <w n="46.5">prudent</w> <w n="46.6">à</w> <w n="46.7">la</w> <w n="46.8">fois</w>,</l>
							<l n="47" num="6.3"><w n="47.1">De</w> <w n="47.2">peur</w> <w n="47.3">de</w> <w n="47.4">l</w>’<w n="47.5">effrayer</w> <w n="47.6">retient</w> <w n="47.7">sa</w> <w n="47.8">grosse</w> <w n="47.9">voix</w>.</l>
							<l n="48" num="6.4"><w n="48.1">Hélas</w> ! <w n="48.2">peut</w>-<w n="48.3">on</w> <w n="48.4">crier</w> <w n="48.5">contre</w> <w n="48.6">un</w> <w n="48.7">enfant</w> <w n="48.8">qui</w> <w n="48.9">pleure</w> ?</l>
							<l n="49" num="6.5">« <w n="49.1">Bon</w> <w n="49.2">dogue</w>, <w n="49.3">voulez</w>-<w n="49.4">vous</w> <w n="49.5">que</w> <w n="49.6">je</w> <w n="49.7">m</w>’<w n="49.8">approche</w> <w n="49.9">un</w> <w n="49.10">peu</w> ?</l>
							<l n="50" num="6.6"><w n="50.1">Dit</w> <w n="50.2">l</w>’<w n="50.3">écolier</w> <w n="50.4">plaintif</w>. <w n="50.5">Je</w> <w n="50.6">n</w>’<w n="50.7">aime</w> <w n="50.8">pas</w> <w n="50.9">mon</w> <w n="50.10">livre</w> ;</l>
							<l n="51" num="6.7"><w n="51.1">Voyez</w> ! <w n="51.2">ma</w> <w n="51.3">main</w> <w n="51.4">est</w> <w n="51.5">rouge</w>, <w n="51.6">il</w> <w n="51.7">en</w> <w n="51.8">est</w> <w n="51.9">cause</w>. <w n="51.10">Au</w> <w n="51.11">jeu</w></l>
							<l n="52" num="6.8"><w n="52.1">Rien</w> <w n="52.2">ne</w> <w n="52.3">fatigue</w>, <w n="52.4">on</w> <w n="52.5">rit</w> ; <w n="52.6">et</w> <w n="52.7">moi</w> <w n="52.8">je</w> <w n="52.9">voudrais</w> <w n="52.10">vivre</w></l>
							<l n="53" num="6.9"><w n="53.1">Sans</w> <w n="53.2">aller</w> <w n="53.3">à</w> <w n="53.4">l</w>’<w n="53.5">école</w>, <w n="53.6">où</w> <w n="53.7">l</w>’<w n="53.8">on</w> <w n="53.9">tremble</w> <w n="53.10">toujours</w>.</l>
							<l n="54" num="6.10"><w n="54.1">Je</w> <w n="54.2">m</w>’<w n="54.3">en</w> <w n="54.4">plains</w> <w n="54.5">tous</w> <w n="54.6">les</w> <w n="54.7">soirs</w>, <w n="54.8">et</w> <w n="54.9">j</w>’<w n="54.10">y</w> <w n="54.11">vais</w> <w n="54.12">tous</w> <w n="54.13">les</w> <w n="54.14">jours</w>.</l>
							<l n="55" num="6.11"><w n="55.1">J</w>’<w n="55.2">en</w> <w n="55.3">suis</w> <w n="55.4">très</w> <w n="55.5">mécontent</w>. <w n="55.6">Je</w> <w n="55.7">n</w>’<w n="55.8">aime</w> <w n="55.9">aucune</w> <w n="55.10">affaire</w>.</l>
							<l n="56" num="6.12"><w n="56.1">Le</w> <w n="56.2">sort</w> <w n="56.3">des</w> <w n="56.4">chiens</w> <w n="56.5">me</w> <w n="56.6">plaît</w>, <w n="56.7">car</w> <w n="56.8">ils</w> <w n="56.9">n</w>’<w n="56.10">ont</w> <w n="56.11">rien</w> <w n="56.12">à</w> <w n="56.13">faire</w>.</l>
						</lg>
						<lg n="7">
							<l n="57" num="7.1">— <w n="57.1">Écolier</w> ! <w n="57.2">voyez</w>-<w n="57.3">vous</w> <w n="57.4">ce</w> <w n="57.5">laboureur</w> <w n="57.6">aux</w> <w n="57.7">champs</w> ?</l>
							<l n="58" num="7.2"><w n="58.1">Eh</w> <w n="58.2">bien</w> ! <w n="58.3">ce</w> <w n="58.4">laboureur</w>, <w n="58.5">dit</w> <w n="58.6">Stentor</w>, <w n="58.7">c</w>’<w n="58.8">est</w> <w n="58.9">mon</w> <w n="58.10">maître</w>.</l>
							<l n="59" num="7.3"><w n="59.1">Il</w> <w n="59.2">est</w> <w n="59.3">très</w> <w n="59.4">vigilant</w> ; <w n="59.5">je</w> <w n="59.6">le</w> <w n="59.7">suis</w> <w n="59.8">plus</w>, <w n="59.9">peut</w>-<w n="59.10">être</w>.</l>
							<l n="60" num="7.4"><w n="60.1">Il</w> <w n="60.2">dort</w> <w n="60.3">la</w> <w n="60.4">nuit</w>, <w n="60.5">et</w> <w n="60.6">moi</w> <w n="60.7">j</w>’<w n="60.8">écarte</w> <w n="60.9">les</w> <w n="60.10">méchants</w>.</l>
							<l n="61" num="7.5"><w n="61.1">J</w>’<w n="61.2">éveille</w> <w n="61.3">aussi</w> <w n="61.4">ce</w> <w n="61.5">bœuf</w> <w n="61.6">qui</w>, <w n="61.7">d</w>’<w n="61.8">un</w> <w n="61.9">pied</w> <w n="61.10">lent</w>, <w n="61.11">mais</w> <w n="61.12">ferme</w>,</l>
							<l n="62" num="7.6"><w n="62.1">Va</w> <w n="62.2">creuser</w> <w n="62.3">les</w> <w n="62.4">sillons</w> <w n="62.5">quand</w> <w n="62.6">je</w> <w n="62.7">garde</w> <w n="62.8">la</w> <w n="62.9">ferme</w>.</l>
							<l n="63" num="7.7"><w n="63.1">Pour</w> <w n="63.2">vous</w>-<w n="63.3">même</w> <w n="63.4">on</w> <w n="63.5">travaille</w> ; <w n="63.6">et</w>, <w n="63.7">grâce</w> <w n="63.8">à</w> <w n="63.9">vos</w> <w n="63.10">brebis</w>,</l>
							<l n="64" num="7.8"><w n="64.1">Votre</w> <w n="64.2">mère</w>, <w n="64.3">en</w> <w n="64.4">chantant</w>, <w n="64.5">vous</w> <w n="64.6">file</w> <w n="64.7">des</w> <w n="64.8">habits</w>.</l>
							<l n="65" num="7.9"><w n="65.1">Par</w> <w n="65.2">le</w> <w n="65.3">travail</w> <w n="65.4">tout</w> <w n="65.5">plaît</w>, <w n="65.6">tout</w> <w n="65.7">s</w>’<w n="65.8">unit</w>, <w n="65.9">tout</w> <w n="65.10">s</w>’<w n="65.11">arrange</w>.</l>
						</lg>
						<lg n="8">
							<l n="66" num="8.1"><w n="66.1">Allez</w> <w n="66.2">donc</w> <w n="66.3">à</w> <w n="66.4">l</w>’<w n="66.5">école</w> ; <w n="66.6">allez</w>, <w n="66.7">mon</w> <w n="66.8">petit</w> <w n="66.9">ange</w> !</l>
							<l n="67" num="8.2"><w n="67.1">Les</w> <w n="67.2">chiens</w> <w n="67.3">ne</w> <w n="67.4">lisent</w> <w n="67.5">pas</w>, <w n="67.6">mais</w> <w n="67.7">la</w> <w n="67.8">chaîne</w> <w n="67.9">est</w> <w n="67.10">pour</w> <w n="67.11">eux</w> :</l>
							<l n="68" num="8.3"><w n="68.1">L</w>’<w n="68.2">ignorance</w> <w n="68.3">toujours</w> <w n="68.4">mène</w> <w n="68.5">à</w> <w n="68.6">la</w> <w n="68.7">servitude</w>.</l>
							<l n="69" num="8.4"><w n="69.1">L</w>’<w n="69.2">homme</w> <w n="69.3">est</w> <w n="69.4">fin</w>, <w n="69.5">l</w>’<w n="69.6">homme</w> <w n="69.7">est</w> <w n="69.8">sage</w>, <w n="69.9">il</w> <w n="69.10">nous</w> <w n="69.11">défend</w> <w n="69.12">l</w>’<w n="69.13">étude</w> ;</l>
							<l n="70" num="8.5"><w n="70.1">Enfant</w>, <w n="70.2">vous</w> <w n="70.3">serez</w> <w n="70.4">homme</w>, <w n="70.5">et</w> <w n="70.6">vous</w> <w n="70.7">serez</w> <w n="70.8">heureux</w> ;</l>
							<l n="71" num="8.6"><w n="71.1">Les</w> <w n="71.2">chiens</w> <w n="71.3">vous</w> <w n="71.4">serviront</w>. » <w n="71.5">L</w>’<w n="71.6">enfant</w> <w n="71.7">l</w>’<w n="71.8">écouta</w> <w n="71.9">dire</w> ;</l>
							<l n="72" num="8.7"><w n="72.1">Et</w> <w n="72.2">même</w> <w n="72.3">il</w> <w n="72.4">le</w> <w n="72.5">baisa</w>. <w n="72.6">Son</w> <w n="72.7">livre</w> <w n="72.8">était</w> <w n="72.9">moins</w> <w n="72.10">lourd</w>.</l>
							<l n="73" num="8.8"><w n="73.1">En</w> <w n="73.2">quittant</w> <w n="73.3">le</w> <w n="73.4">bon</w> <w n="73.5">dogue</w>, <w n="73.6">il</w> <w n="73.7">pense</w>, <w n="73.8">il</w> <w n="73.9">marche</w>, <w n="73.10">il</w> <w n="73.11">court</w>.</l>
							<l n="74" num="8.9"><w n="74.1">L</w>’<w n="74.2">espoir</w> <w n="74.3">d</w>’<w n="74.4">être</w> <w n="74.5">homme</w> <w n="74.6">un</w> <w n="74.7">jour</w> <w n="74.8">lui</w> <w n="74.9">ramène</w> <w n="74.10">un</w> <w n="74.11">sourire</w>.</l>
							<l n="75" num="8.10"><w n="75.1">À</w> <w n="75.2">l</w>’<w n="75.3">école</w>, <w n="75.4">un</w> <w n="75.5">peu</w> <w n="75.6">tard</w>, <w n="75.7">il</w> <w n="75.8">arrive</w> <w n="75.9">gaîment</w>,</l>
							<l n="76" num="8.11"><w n="76.1">Et</w> <w n="76.2">dans</w> <w n="76.3">le</w> <w n="76.4">mois</w> <w n="76.5">des</w> <w n="76.6">fruits</w> <w n="76.7">il</w> <w n="76.8">lisait</w> <w n="76.9">couramment</w>.</l>
						</lg>
					</div></body></text></TEI>